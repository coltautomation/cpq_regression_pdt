package ncTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.ncFunctions.AccountNavigation;
import testHarness.ncFunctions.CarNorOrder;
import testHarness.ncFunctions.NcLogin;

public class TS_03_Carnor_Order extends SeleniumUtils{
	
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	
	private static GlobalVariables g;
	
	NcLogin Login = new NcLogin();
	CarNorOrder CarNor = new CarNorOrder();
	AccountNavigation NcAccounts = new AccountNavigation();
		
	Properties prop = new Properties();
	
	@Parameters({"scriptNo","dataSetNo","dataSheet","ScenarioName"})
	@Test
	public void CarNorCreateAndProcess(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws Exception
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } 
		catch (IOException ex) 
		{
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        
        try
        {
        	Login.Login(Configuration.NC_Username, Configuration.NC_Password);
			Login.VerifySuccessLogin();
			NcAccounts.GotoDocument();
			NcAccounts.GotoAccount();
			CarNor.NcCarnorOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
        }
	catch(Exception e)
	{
        Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
        ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
        ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	} 
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
	}
	
}
