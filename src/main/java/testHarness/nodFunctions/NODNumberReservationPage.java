package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODNumberReservationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODNumberReservationPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder=new NODMyOrderPage();
	private static HashMap<String, String> Number= new HashMap<String, String>();
	String OrderStatus;
	String OrderType;
	


	public void NumReservation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
		String NumberType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_Type");
		String BlockSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_Size");
		String QuantitySize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity_Size");
		String LocalAreaCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Local_AreaCode");
		
		verifyExists(NODNumberReservationObj.NumReservation.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODNumberReservationObj.NumReservation.QuickLinkDrpDwnArw,"Quick Link Drop down");
		click(NODNumberReservationObj.NumReservation.ReservationLink,"Activation link click");
		waitForAjax();
		waitForElementToBeVisible(NODNumberReservationObj.NumReservation.ReservationPage,15);
		
		verifyExists(NODNumberReservationObj.NumReservation.NumberTypeRadioBtn.replace("NoType",NumberType),"Select Number Type radio btn");
	    javaScriptclick(NODNumberReservationObj.NumReservation.NumberTypeRadioBtn.replace("NoType",NumberType),"Number Type radio btn");
	    
	    if(isElementPresent(By.xpath("//label[contains(text(),'Your Local Area Code (LAC)')]")))
	    {
	    	verifyExists(NODNumberReservationObj.NumReservation.LocalAreaCode,"LocalAreaCode");
		    javaScriptclick(NODNumberReservationObj.NumReservation.LocalAreaCode,"LocalAreaCode");
		    javaScriptclick(NODNumberReservationObj.NumReservation.LocalAreaCodeOption.replace("option",LocalAreaCode),"LocalAreaCode");
	    }
		Reusable.Select(NODNumberReservationObj.NumReservation.BlockSize,BlockSize);
		Reusable.Select(NODNumberReservationObj.NumReservation.QuantitySize,QuantitySize);
		javaScriptclick(NODNumberReservationObj.NumReservation.ProvideNumbersBtn," Provide Numbers Btn");
		
		waitForAjax();
		waitForElementToBeVisible(NODNumberReservationObj.NumReservation.NumberRangeChkBox,15);
		javaScriptclick(NODNumberReservationObj.NumReservation.NumberRangeChkBox," Number Range CheckBox");
		waitForAjax();
		if(isElementPresent(By.xpath("//button[contains(text(),'Reserve')]"))){
		waitForElementToBeVisible(NODNumberReservationObj.NumReservation.ReserveBtn,15);
		javaScriptclick(NODNumberReservationObj.NumReservation.ReserveBtn," Number Reservation Btn");
		}
		else{
			waitForElementToBeVisible(NODNumberReservationObj.NumReservation.OrderReview,15);
			javaScriptclick(NODNumberReservationObj.NumReservation.OrderReview," Order Review");
		}
		WaitForAjax();
		waitForElementToBeVisible(NODNumberReservationObj.NumReservation.OrderConfirmationBtn,15);
		javaScriptclick(NODNumberReservationObj.NumReservation.OrderConfirmationBtn," Order Confirmation Btn");
		
		waitForElementToBeVisible(NODNumberReservationObj.NumReservation.ResrvationConfirmationMsg,15);
	    if(isElementPresent(By.xpath("//div[@id='test-l-4']")))
	    {
	    	String SuccessMessage=getTextFrom(NODNumberReservationObj.NumReservation.SuccessMessage, "SuccessMessage");
	    	//String emailNotification=getTextFrom(NODNumberReservationObj.NumReservation.EmailNotification, "SuccessMessage");
	    	ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
	    }
	    else{
	    	ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Activation is Not successful");
	    }
	    String DirectActivationOrderId=getTextFrom(NODNumberReservationObj.NumReservation.ReservationOrderId, "Order Id");
	    WaitForAjax();
	    javaScriptclick(NODNumberReservationObj.NumReservation.CheckMyOrder,"Check MyOrder Btn");
	    WaitForAjax();
	    waitForElementToDisappear(NODNumberReservationObj.NumReservation.TableRecordLoader,120);
	    click(NODNumberReservationObj.NumReservation.SearchTxtFld, "Send Order ID");
	    sendKeys(NODNumberReservationObj.NumReservation.SearchTxtFld,DirectActivationOrderId , "Send Order ID");
	    WaitForAjax();
		click(NODNumberReservationObj.NumReservation.SearchIcon,"Search Icon");
		waitForElementToDisappear(NODNumberReservationObj.NumReservation.TableRecordLoader,120);
		waitForAjax();
		
		//String OrderStatus;
		if(isElementPresent(By.xpath("//a[@class='transaction']"))){
		 OrderStatus = getTextFrom(NODNumberReservationObj.NumReservation.orderStatusVal,"Order Status");
		 OrderType = getTextFrom(NODNumberReservationObj.NumReservation.orderTypeVal,"Order Type");
		 
		}else
		{  
		    for(int i=0;i<5;i++)
            {          
		    	WaitForAjax();
				sleep(60000);
				refreshPage();
				waitForElementToDisappear(NODNumberReservationObj.NumReservation.TableRecordLoader,120);
			    click(NODNumberReservationObj.NumReservation.SearchTxtFld, "Send Order ID");
			    sendKeys(NODNumberReservationObj.NumReservation.SearchTxtFld,DirectActivationOrderId , "Send Order ID");
			    WaitForAjax();
				click(NODNumberReservationObj.NumReservation.SearchIcon,"Search Icon");
				//waitForAjax();
				waitForElementToDisappear(NODNumberReservationObj.NumReservation.TableRecordLoader,120);
                                                         
                 if (isElementPresent(By.xpath("//a[@class='transaction']")))
                 {
                	 OrderStatus = getTextFrom(NODNumberReservationObj.NumReservation.orderStatusVal,"Order Status"); 
                	 OrderType = getTextFrom(NODNumberReservationObj.NumReservation.orderTypeVal,"Order Type");
                     break;         	
                 }
                 
   	        }
   		   }
   		if(OrderType.equals("Number Reservation")) 
   		{
   			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Type and Order Status Is: "+OrderType);
   		}else{
   			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Status is not Present");	
   		}
   		
   		}
           
   	
                          
	}

		














