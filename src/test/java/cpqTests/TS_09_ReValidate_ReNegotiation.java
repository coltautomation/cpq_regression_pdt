package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_09_ReValidate_ReNegotiation extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testReValidateReNegotiation(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			String Flow_Type=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String sResult;
			if(ProductName.equalsIgnoreCase("Wave")) {
				
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Priced");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method to add Legal and Technical Contact Info
				String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
				sResult = Approvals.SwitchCPQUser("SE_User", Quote_ID);

				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//				calling the below method to configure the product
				sResult = AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method to update and save the product in cpq
				sResult = AddProduct.updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
			}
			
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
//			calling the below method to verify quote stage and status
			AddProduct.verifyQuoteStage("Waiting for 3rd Party");
			CPQLogin.CPQLogout();
			
			//Login with CPQ SE user
			close();
			openBrowser(g.getBrowser());		
			openurl(Configuration.EXPLORE_URL);
			
			sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
		
	//		Calling the Below method to perform C4C Login	
			close();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
	//		calling the below method to navigate to quotes from c4c main page
			AddProduct.navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
	//		calling the below method to search the quote from c4c
			sResult = AddProduct.searchQuoteC4C(Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
				
	//		calling the below method reconfigure the product
			sResult = AddProduct.clickProductConfigurationBtn(ProductName);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
	//		calling the below method to update and save the product in cpq
			sResult = AddProduct.updateSaveProductCPQ("Save");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
	//      calling the below method to verify quote stage and status
			sResult = AddProduct.verifyQuoteStage("Priced");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
			if(ProductName.equalsIgnoreCase("Wave")) 
			{
				sResult = Approvals.SwitchCPQUser("SE_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
				
			}
				//		calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below method to configure the product
				sResult = AddProduct.reValidatereNegotiateQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
				
				//		calling the below method to update and save the product in cpq
				sResult = AddProduct.updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				AddProduct.verifyQuoteStage("Waiting for 3rd Party");
				CPQLogin.CPQLogout();
				
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.EXPLORE_URL);
				sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
			
		//		Calling the Below method to perform C4C Login	
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				Login.C4CLogin();
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
				
		//		calling the below method to navigate to quotes from c4c main page
				AddProduct.navigateQuotesFromHomepage();
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
				
		//		calling the below method to search the quote from c4c
				sResult = AddProduct.searchQuoteC4C(Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
					
		//		calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
		//		calling the below method to update and save the product in cpq
				sResult = AddProduct.updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
				String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
				System.out.println("quoteStage: "+quoteStage);
				if (quoteStage.equalsIgnoreCase("SE Required")) 
				{
					AddProduct.verifyQuoteStage("SE Required");
					AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
						            
					//Login with CPQ SE user
					close();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
					Approvals.SwitchCPQUser("Sales_User", Quote_ID);
				}
				
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Priced");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
				{
				String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
				Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
				} else {
				String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
				Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
				}
				
				//String CPE_Site_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Site_BCN");
				//Submit.addBillingInformation("CPE Solutions Site", CPE_Site_BCN);
				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
				Submit.SubmitOrder();
				AddProduct.verifyQuoteStage("Ordered");
				Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	
	
}
