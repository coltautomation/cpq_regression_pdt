package testHarness.cpqFunctions;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.GridHelper;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CLoginObj;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.commonFunctions.ReusableFunctions;

public class CPQQuoteApprovals extends SeleniumUtils {
	
	CPQLoginPage Login = new CPQLoginPage();
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQQuoteCreation CreateQuote = new CPQQuoteCreation();
	ReusableFunctions Reusable = new ReusableFunctions();
	GridHelper Grid = new GridHelper();
	GlobalVariables g = new GlobalVariables();
	
	public String QuoteSEApproval(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{			
		
		waitForElementToAppear(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink, 10);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink,"Technical Approval link");		
		click(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink,"Technical Approval link");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		waitForElementToAppear(CPQQuoteApprovalsObj.QuoteApprovals.SubmittoCSTAppvlBtn, 10);
		ScrollIntoViewByString(CPQQuoteApprovalsObj.QuoteApprovals.SubmittoCSTAppvlBtn);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.SubmittoCSTAppvlBtn,"Submit to CST Appoval button");
		click(CPQQuoteApprovalsObj.QuoteApprovals.SubmittoCSTAppvlBtn,"Submit to CST Appoval button");
		Reusable.WaitforCPQloader();
		waitForAjax();		
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		scrollUp();
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.WaitforCPQloader();
		waitForAjax();
		 return "True";
	}
	
	public void SetLeadTimeDays(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
		String Leadtime_Days = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Leadtime_Days");
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable,"Line Item Table");
		ScrollIntoViewByString(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable);
		
		switch (Product_Name) 
		{	
		case "EthernetLine":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Ethernet Line", "Lead Time (Days)*", Leadtime_Days);
			break;		
		case "EthernetHub":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Ethernet Hub", "Lead Time (Days)*", Leadtime_Days);
			break;
		case "EthernetSpoke":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Ethernet Spoke", "Lead Time (Days)*", Leadtime_Days);
			break;		
		case "Wave":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Wave", "Lead Time (Days)*", Leadtime_Days);
			break;
		case "ColtIpAccess":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Colt IP Access", "Lead Time (Days)*", Leadtime_Days);
			break;		
		case "ColtIpDomain":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Colt IP Domain", "Lead Time (Days)*", Leadtime_Days);
			break;
		case "ColtIpGuardian":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Colt IP Guardian", "Lead Time (Days)*", Leadtime_Days);
			break;		
		case "ColtManagedVirtualFirewall":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Colt Managed Virtual Firewall", "Lead Time (Days)*", Leadtime_Days);
			break;
		case "ColtManagedDedicatedFirewall":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "Colt Managed Dedicated Firewall", "Lead Time (Days)*", Leadtime_Days);
			break;
		case "CPESolutionsSite":
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "CPE Solutions Service", "Lead Time (Days)*", Leadtime_Days);
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, "CPE Solutions Site", "Lead Time (Days)*", Leadtime_Days);
			break;		
		}		
		
	}
	
	public void QuoteCSTApproval() throws Exception 
	{
		waitForElementToAppear(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink, 10);
		ScrollIntoViewByString(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink,"Technical Approval link");
		click(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink,"Technical Approval link");
		
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteApprovalsObj.QuoteApprovals.ApproveBtn, 10);
		ScrollIntoViewByString(CPQQuoteApprovalsObj.QuoteApprovals.ApproveBtn);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.ApproveBtn,"CST User Appoval button");
		javaScriptclick(CPQQuoteApprovalsObj.QuoteApprovals.ApproveBtn,"CST User Appoval button");
		Reusable.waitForpageloadmask();
				
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.waitForpageloadmask();
				
	}
	
	public String SwitchCPQUser(String sUser, String QuoteID) throws IOException, InterruptedException, AWTException 
	{
		
			Login.CPQLogout();
			
//			Login with CPQ SE user
			close();
			openBrowser(g.getBrowser());
			openurl(Configuration.CPQ_URL);
			try {
				
				if(sUser == "SE_User") { Login.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password); }
				else if(sUser == "CST_User") { Login.CPQLogin(Configuration.CSTUser_Username, Configuration.CSTUser_Password); }
				else if(sUser == "PS_User") { Login.CPQLogin(Configuration.PSUser_Username, Configuration.PSUser_Password); }
				else if(sUser == "Sales_User") { Login.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password); }
				else if(sUser == "DP_User") { Login.CPQLogin(Configuration.DPUser_Username, Configuration.DPUser_Password); }
				else if(sUser == "VPSales1_User") { Login.CPQLogin(Configuration.VP_SalesUser1_Username, Configuration.VP_SalesUser1_Password); }
				else if(sUser == "VPSales2_User") { Login.CPQLogin(Configuration.VP_SalesUser2_Username, Configuration.VP_SalesUser2_Password); }
				else if(sUser == "AD_User") { Login.CPQLogin(Configuration.ADUser_Username, Configuration.SalesUser1_Password);}
				else if(sUser == "AD_User") { Login.CPQLogin(Configuration.ADUser_Username, Configuration.SalesUser1_Password);}
				
			} catch (AWTException e) {				
				e.printStackTrace();
			}						
			
					
			if(verifyExists(CPQLoginObj.Login.GearIcon))
			{
			verifyExists(CPQLoginObj.Login.GearIcon,"Gear Icon on Header of Landing Page");
			click(CPQLoginObj.Login.GearIcon,"Gear Icon on Header of Landing Page");
	
			Reusable.waitToPageLoad();
		
			verifyExists(CPQLoginObj.Login.TransactionIcon,"TransactionIcon");
			click(CPQLoginObj.Login.TransactionIcon,"TransactionIcon");
			
			}
			else{
			
			waitForElementToAppear(CPQLoginObj.Login.headerOracleQuotelink, 10);
			verifyExists(CPQLoginObj.Login.headerOracleQuotelink,"Header Oracle Quote link on Landing Page");
			
			//CPQ SE Approval Process
			click(CPQLoginObj.Login.headerOracleQuotelink,"Header Oracle Quote link");
			Reusable.waitForpageloadmask();
			Reusable.waitToPageLoad();
			}
			
			//String Quote_ID = DataMiner.fngetcolvalue(file_name, tsSheetName, scriptNo, dataSetNo,"Quote_ID");
			String quoteIdObj = CPQLoginObj.Login.QuoteID1+ QuoteID +CPQLoginObj.Login.QuoteID2;
			
			waitForElementToAppear(quoteIdObj, 10);
			verifyExists(quoteIdObj,"Quote ID");
			click(quoteIdObj,"Quote ID");
			
			Reusable.WaitforCPQloader();
			waitForAjax();
			Reusable.waitForpageloadmask();			
			return "True";
		}
			
	
	public String psNewValidationforPRS(String file_name, String Sheet_Name, String iScript, String iSubScript) throws Exception 
	{
//		Initializing the Variable
		String sResult;
		String PR_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PR_Type");
		String PR_SubProduct = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PR_SubProduct");
		String PS_Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PS_Product_Name");
		String Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");

		
		switch(PR_Type.toUpperCase()) {
		
		case "SE_CE_NOT_AVALIABLE":
//			Calling below method to validate SE CST and Technical bread cums
			sResult= psStandaloneSEAndCST(file_name, Sheet_Name, iScript, iSubScript,"NotDisplayed");
			if(sResult.equalsIgnoreCase("false")) {return "False";}
			break;
		
		case "DELETE_P2P":
//			Calling the below method to add the product in CPQ
			sResult = CreateQuote.AddProductToQuote(PR_SubProduct);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to enter the site address
			sResult = CreateQuote.AddSiteDetails(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to configure the product
			sResult = CreateQuote.productConfiguration(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }	
			
//			calling the below method to verify quote stage and status
			sResult = CreateQuote.verifyQuoteStage("Priced");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			Calling below method to validate SE CST and Technical bread cums
			sResult= psStandaloneSEAndCST(file_name, Sheet_Name, iScript, iSubScript,"Displayed");
			if(sResult.equalsIgnoreCase("false")) {return "False";}
			
//			calling the below method to delete the product
			sResult = deleteProductCPQ(PR_SubProduct);
			if (sResult.equalsIgnoreCase("False")){ return "False";}
			
//			Calling below method to validate SE CST and Technical bread cums
			sResult= psStandaloneSEAndCST(file_name, Sheet_Name, iScript, iSubScript,"NotDisplayed");
			if(sResult.equalsIgnoreCase("false")) {return "False";}
			
			break;
			
		case"SE_CE_AVALIABLE":
//			Calling below method to validate SE CST and Technical bread cums
			sResult= psStandaloneSEAndCST(file_name, Sheet_Name, iScript, iSubScript,"Displayed");
			if(sResult.equalsIgnoreCase("false")) {return "False";}
			break;
		
		
		case "DELETE_PS":
//			Calling below method to validate SE CST and Technical bread cums
			sResult= psStandaloneSEAndCST(file_name, Sheet_Name, iScript, iSubScript,"Displayed");
			if(sResult.equalsIgnoreCase("false")) {return "False";}
			
//			calling the below method to delete the product
			sResult = deleteProductCPQ(PS_Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False";}
			
//			Calling below method to validate SE CST and Technical bread cums
			sResult= psStandaloneSEAndCST(file_name, Sheet_Name, iScript, iSubScript,"Displayed");
			if(sResult.equalsIgnoreCase("false")) {return "False";}
			
//			Calling the below method to add the product in CPQ
			sResult = CreateQuote.AddProductToQuote(PS_Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False";}
			
//			calling the below method to process PS product configuration
			sResult =CreateQuote.professionalServicesConfiguration(file_name, Sheet_Name, iScript, iSubScript, "Sales_User",PS_Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to verify quote stage and status
			sResult =CreateQuote.verifyQuoteStage("Created");
			if (sResult.equalsIgnoreCase("False")){ return "False";}
			
//			Calling the Below method to return to CPQ from C4C
			sResult = CreateQuote.returnC4CFromCPQ();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			Calling the Below method to PS Engagement
			sResult = CreateQuote.editPSEngagement(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to navigate to quotes from c4c main page
			sResult=CreateQuote.navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }	
			
//			calling the below method to search the quote from c4c
			sResult=CreateQuote.searchQuoteC4C(Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to login as PS User
			sResult = SwitchCPQUser("PS_User", Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to edit the product configuration
			sResult = CreateQuote.clickProductConfigurationBtn(PS_Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to process PS product configuration
			sResult = CreateQuote.professionalServicesConfiguration(file_name, Sheet_Name, iScript, iSubScript, "PS_User",PS_Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False";}
			
//			calling the below method to verify quote stage and status
			sResult = CreateQuote.logoutCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }	
			
//			Calling the Below method to perform C4C Login
			C4CLogin.C4CLogin();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to navigate to quotes from c4c main page
			sResult=CreateQuote.navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to search the quote from c4c
			sResult = CreateQuote.searchQuoteC4C(Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to verify quote stage and status
			sResult = CreateQuote.verifyQuoteStage("Priced");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		return "True";
	}
	
	public String psStandaloneSEAndCST(String file_name, String Sheet_Name, String iScript, String iSubScript, String present) throws IOException, InterruptedException 
	{

		Reusable.waitForpageloadmask();
		switch(present.toUpperCase()) {
		case "NOTDISPLAYED":
			
			try {
				findWebElement(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink).isDisplayed();
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Technical tab is displayed for the PS standlone product ,Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}catch(Exception e) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Technical tab is not displayed for the product ");
			}
			
//			Validate SE Breadcrums should not display
			try {
				findWebElement(CPQQuoteApprovalsObj.QuoteApprovals.seRevLnk).isDisplayed();
				ExtentTestManager.getTest().log(LogStatus.FAIL, "SE Breadcum is displayed for the PS standlone product ,Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}catch(Exception e) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "SE Breadcum is not displayed for the product ");
			}
			
//			Validate CST Breadcrums should not display
			try {
				findWebElement(CPQQuoteApprovalsObj.QuoteApprovals.CSTRevLnk).isDisplayed();
				ExtentTestManager.getTest().log(LogStatus.FAIL, "CST Breadcum is displayed for the PS standlone product ,Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}catch(Exception e) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "CST Breadcum is not displayed for the product ");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}
			
			break;
		
		case "DISPLAYED":
//			Validate Technical approval Tab should not display
			Reusable.waitToPageLoad();
			verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.TechnicalApprovalLink,"Technical Approval Link");
			
//			Validate SE Breadcrums should not display
			Reusable.waitToPageLoad();
			verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.seRevLnk, "SE Review Link");
			
//			Validate CST Breadcrums should not display
			Reusable.waitToPageLoad();
			verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.CSTRevLnk,"CST Review Link");
		
			break;		
		}
		return "True";
	}
	
	public String deleteProductCPQ(String ProductName) throws IOException, InterruptedException
	{
//		Initializing the Variable
		String sResult, sProduct_Name;
//		Selecting the product
		if (ProductName.contains("Data -") || ProductName.contains("Voice -")) { sProduct_Name = ProductName; } 
		else { sProduct_Name = ProductName.replaceAll("(?!^)([A-Z])", " $1"); }
		
		
		sResult = Reusable.WebTableCellAction("Product", sProduct_Name, null,"Click", null);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		clicking on Reconfiguration link
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryConnectionPage.deleteProductBtn, 45);
		verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.deleteProductBtn, "Delete Product Button");
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.PrimaryConnectionPage.deleteProductBtn, "Delete Product Button");
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.okdialogButton, "OK Popup Button");
    	click(CPQQuoteCreationObj.PrimaryConnectionPage.okdialogButton,"OK Popup");
    	Reusable.waitForpageloadmask();    	    
	
		return "True";
	}
	
	public String psApproval() throws IOException, InterruptedException 
	{
		
		waitForElementToBeVisible(CPQQuoteApprovalsObj.QuoteApprovals.ApprovalLnk, 60);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.ApprovalLnk, "Approval Link");		
		Reusable.WaitforCPQloader();
		
		ScrollIntoViewByString(CPQQuoteApprovalsObj.QuoteApprovals.ApprovalLnk);
		click(CPQQuoteApprovalsObj.QuoteApprovals.ApprovalLnk, "Approval Link");
		Reusable.WaitforCPQloader();
		
		waitForElementToBeVisible(CPQQuoteApprovalsObj.QuoteApprovals.SubmitToApprovalBtn, 60);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.SubmitToApprovalBtn, "Approval Link");
		click(CPQQuoteApprovalsObj.QuoteApprovals.SubmitToApprovalBtn, "Approval Link");
		waitToPageLoad();
		verifyDoesNotExist(CPQQuoteApprovalsObj.QuoteApprovals.SubmitToApprovalBtn);
		Reusable.WaitforCPQloader();
		Reusable.Waittilljquesryupdated();
		
		String sResult = validateCPQErrorMsg();
		if (sResult.equalsIgnoreCase("False")) { return "False"; }
		
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"General Information Link");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"General Information Link");
		Reusable.WaitforCPQloader();

		return "True";
	}
	
	public String validateCPQErrorMsg() throws IOException, InterruptedException 
	{

		if (verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.CPQSummaryErrorMsgElem)) {
			String Main_Err = getTextFrom(CPQQuoteApprovalsObj.QuoteApprovals.CPQSummaryErrorMsgElem);
			String Sub_Err = getTextFrom(CPQQuoteApprovalsObj.QuoteApprovals.CPQErrorMsgElem);
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Process the quote due to the Error: "+Main_Err+" and "+Sub_Err);
			System.out.println("Unable to Process the quote due to the Error: "+Main_Err+" and "+Sub_Err);
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
    	} 
		
		return "True";
		
	}
}
