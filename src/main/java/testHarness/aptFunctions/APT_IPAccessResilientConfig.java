package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.aptObjects.APT_IPAccessResilientConfigObj;
import pageObjects.aptObjects.APT_IPAccess_VCPEResilientConfigObj;

public class APT_IPAccessResilientConfig extends SeleniumUtils {

	public void EditServiceFunction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String BillingTypeEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BillingTypeEdit");
		String EmailServiceEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EmailServiceEdit");
		String PhoneServiceEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PhoneServiceEdit");
		String RemarksEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RemarksEdit");
		String ManageServiceEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManageServiceEdit");
		String RouterConfigurationViewIPv4Edit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv4Edit");
		String RouterConfigurationViewIPv6Edit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv6Edit");
		String PerformanceReportingEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PerformanceReportingEdit");
		String IPGuardianEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IPGuardianEdit");
		String SmartMonitoringEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SmartMonitoringEdit");
		String CloudPrioritizationEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CloudPrioritizationEdit");
		String ProactiveNotificationEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ProactiveNotificationEdit");
		String SNMPNotificationEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SNMPNotificationEdit");
		String TrapTargetAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TrapTargetAddressEdit");
		String RouterBasedFirewallEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RouterBasedFirewallEdit");
		String QosEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "QosEdit");
		String ActelisBasedEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ActelisBasedEdit");
		String BGPASNumberEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGPASNumberEdit");
		String BGPPasswordEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGPPasswordEdit");

		ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Edit Service Functionality");
		// Cancel edit service

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);
		// ScrolltoElement(application, "orderpanelheader", xml);

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
		// click_commonMethod(application, "Action dropdown",
		// "serviceactiondropdown", xml);

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cancelbutton);
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cancelbutton, "Cancel");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cancelbutton, "Cancel");
		// click_commonMethod(application, "Cancel", "cancelbutton", xml);

		// ScrolltoElement(application, "orderpanelheader", xml);
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.servicepanel_header)) {
			Report.LogInfo("INFO", "Step : Navigated to view service page", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Navigated to view service page");
		} else
			//Report.LogInfo("INFO", "Step : Didn't navigate to view service page", "FAIL");
			//ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Didn't navigate to view service page");

		// Edit service
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.billingtype_dropdown);
		SelectDropdownValueUnderSelectTag("Billing Type", BillingTypeEdit,
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.billingtype_dropdown);

		// *ClearAndEnterTextValue(application, "termination date",
		// "terminationdate_field", TerminationDateEdit, xml);

		// ClearAndEnterTextValue("Secondary Circuit Order",
		// APT_IPAccessResilientConfigObj.IPAccessResilientConfig.secondarycircuitorder_field,
		// SecondaryCircuitOrderEdit);
		ClearAndEnterTextValue("Email", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.emailtextfield_Service,
				EmailServiceEdit);
		ClearAndEnterTextValue("Phone Contact",
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.phonecontacttextfield, PhoneServiceEdit);
		ClearAndEnterTextValue("Remark", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.remarktextarea,
				RemarksEdit);

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managementoptions_header);// ConfigurationOptionEditServicePage_Header
		// management options panel
		if (ManageServiceEdit.equalsIgnoreCase("Yes")) {
			editcheckbox_commonMethod(ManageServiceEdit,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ManageServicecheckbox, "Manage Service");
		} else {
			Report.LogInfo("INFO", "Step : Manage Service checkbox is not checked", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Manage Service checkbox is not checked");
		}

		if (RouterConfigurationViewIPv4Edit.equalsIgnoreCase("Yes")) {
			editcheckbox_commonMethod(RouterConfigurationViewIPv4Edit,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.RouterConfigurationViewIPv4checkbox,
					"Router Configuration View IPv4");
		} else {
			Report.LogInfo("INFO", "Step : Router Configuration View IPv4 checkbox is not checked", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"Step : Router Configuration View IPv4 checkbox is not checked");
		}

		if (RouterConfigurationViewIPv6Edit.equalsIgnoreCase("Yes")) {
			editcheckbox_commonMethod(RouterConfigurationViewIPv6Edit,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.RouterConfigurationViewIPv6checkbox,
					"Router Configuration View IPv6");
		} else {
			Report.LogInfo("INFO", "Step : Router Configuration View IPv6 checkbox is not checked", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"Step : Router Configuration View IPv6 checkbox is not checked");
		}

		if (PerformanceReportingEdit.equalsIgnoreCase("Yes")) {
			editcheckbox_commonMethod(PerformanceReportingEdit,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.performancereportingcheckbox,
					"Performance Reporting");
		} else {
			Report.LogInfo("INFO", "Step : Performance Reporting checkbox is not checked", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Performance Reporting checkbox is not checked");
		}

		if (IPGuardianEdit.equalsIgnoreCase("Yes")) {
			editcheckbox_commonMethod(IPGuardianEdit,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.IPGuardiancheckbox, "IP Guardian");
		} else {
			Report.LogInfo("INFO", "Step : IP Guardian checkbox is not checked", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : IP Guardian checkbox is not checked");
		}

		editcheckbox_commonMethod(SmartMonitoringEdit,
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SmartMonitoringcheckbox, "Smart Monitoring");
		
		if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CloudPrioritizationCheckbox)){
		editcheckbox_commonMethod(CloudPrioritizationEdit,
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CloudPrioritizationCheckbox,
				"Cloud Prioritization");
		}

		// Pro-active Notification checkbox and Notification Management Team
		// Dropdown
		editcheckbox_commonMethod(ProactiveNotificationEdit,
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ProactiveNotificationcheckbox,
				"Pro-active Notification");
		if (isSelected(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ProactiveNotificationcheckbox, "")) {
			// SelectDropdownValueUnderSelectTag("Notification Management Team",
			// APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NotificationManagementTeamEdit,
			// "NotificationManagementTeamDropdown");
		} else {
			Report.LogInfo("INFO", "Step : 'Notification Management Team' Dropdown is not Selected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO,
					"Step : 'Notification Management Team' Dropdown is not Selected");
			System.out.println("Step : 'Notification Management Team' Dropdown is not Selected");
		}

		editcheckbox_commonMethod(SNMPNotificationEdit,
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMPNotificationcheckbox, "SNMP Notification");

		if (isSelected(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMPNotificationcheckbox, "")) {
			ClearAndEnterTextValue(ProactiveNotificationEdit,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.TrapTargetAddresstextfield,
					TrapTargetAddressEdit);
		} else {
			Report.LogInfo("INFO", "Step : value not entered in 'Trap Target Address' textfield ", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO,
					"Step : value not entered in 'Trap Target Address' textfield ");
			System.out.println("Step : value not entered in 'Trap Target Address' textfield ");
		}

		// SelectDropdownValueUnderSelectTag("Delivery Channel",
		// APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DeliveryChannelEdit,
		// "DeliveryChannelDropdown");

		// Configuration Options
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.RouterBasedFirewallcheckbox);
		editcheckbox_commonMethod(RouterBasedFirewallEdit,
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.RouterBasedFirewallcheckbox,
				"Router Based Firewall");
		editcheckbox_commonMethod(QosEdit, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Qoscheckbox, "Qos");
		editcheckbox_commonMethod(ActelisBasedEdit,
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ActelisBasedcheckbox, "Actelis Based");
		ClearAndEnterTextValue("BGP AS Number",
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.BGPASNumbertextfield, BGPASNumberEdit);
		ClearAndEnterTextValue("BGP Password",
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.BGPPasswordtextfield, BGPPasswordEdit);
		// SelectDropdownValueUnderSelectTag("Load Shared",
		// APT_IPAccessResilientConfigObj.IPAccessResilientConfig.LoadSharedEdit,
		// "LoadSharedDropdown");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.editservice_okbutton);
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.editservice_okbutton, "Ok");

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.customerdetailsheader)) {
			verifysuccessmessage("Service successfully updated");
		} else {
			System.out.println("Service not updated");
		}
	}

	public void verifysuccessmessage(String expected) throws InterruptedException {

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.alertMsg);

		try {

			// boolean successMsg=getwebelement(xml.getlocator("//locators/" +
			// application + "/alertMsg")).isDisplayed();

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.alertMsg)) {

				// String alrtmsg=getwebelement(xml.getlocator("//locators/" +
				// application +
				// "/AlertForServiceCreationSuccessMessage")).getText();
				String alrtmsg = getTextFrom(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AlertForServiceCreationSuccessMessage,"Alert For Service Creation Success Message");
				if (expected.contains(alrtmsg)) {
					Report.LogInfo("INFO", "Message is verified. It is displaying as: " + alrtmsg, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Message is verified. It is displaying as: " + alrtmsg);

				} else {
					Report.LogInfo("INFO", "Message is displaying and it gets mismatches. It is displaying as: "
							+ alrtmsg + " .The Expected value is: " + expected, "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg
									+ " .The Expected value is: " + expected);
				}

			} else {
				Report.LogInfo("INFO", " Success Message is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");

			}

		} catch (Exception e) {
			Report.LogInfo("INFO", expected + " Message is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, expected + " Message is not displaying");

		}
	}

	public void verifyManageSubnets() throws InterruptedException, IOException {
		Report.LogInfo("INFO", "'Verifying Manage Subnets Functionality", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Manage Subnets Functionality");
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action DropDown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action DropDown");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managesubnets_link, "Manage Subnets");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managesubnets_link, "Manage Subnets");

		// GetText(application, "Manage Subnet header", "managesubnet_header");
		waitforPagetobeenable();
		// GetText(application, "Manage Subnets message",
		// "managesubnet_successmsg");
		compareText("Space Name", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.spacename_column,
				"Space Name");
		compareText("Block Name", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.blockname_column,
				"Block Name");
		compareText("Subnet Name", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.subnetname_column,
				"Subnet Name");
		compareText("Start Address", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.startaddress_column,
				"Start Address");
		compareText("Size", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.size_column, "Size");
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
	}

	public void verifyManageSubnetsIPv6() throws InterruptedException, IOException {
		Report.LogInfo("INFO", "'Verifying Manage Subnets IPV6 Functionality", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Manage Subnets IPV6 Functionality");
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action DropDown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action DropDown");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managesubnetsipv6_link,
				"Manage Subnets Ipv6");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managesubnetsipv6_link, "Manage Subnets Ipv6");

		// GetText(application, "Manage Subnet header", "managesubnet_header");
		// ***compareText(application, "Manage subnet message",
		// "managesubnet_successmsg", "There are no subnets to be managed for
		// this service.", xml);
		// GetText(application, "Manage Subnets message",
		// "managesubnet_successmsg");
		compareText("Space Name", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.spacename_column,
				"Space Name");
		compareText("Block Name", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.blockname_column,
				"Block Name");
		compareText("Subnet Name", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.subnetname_column,
				"Subnet Name");
		compareText("Start Address", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.startaddress_column,
				"Start Address");
		compareText("Size", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.size_column, "Size");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
	}

	public void verifyDump() throws InterruptedException, IOException {
		Report.LogInfo("INFO", "'Verifying Dump Functionality", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Dump Functionality");
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action DropDown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action DropDown");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.dump_link, "Dump");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.dump_link, "Dump");

		// GetText(application, "Dump header", "dumppage_header");
		// GetText(application, "Service retrieved time",
		// "serviceretrieved_text");
		compareText("Service header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.service_header, "Service");
		// GetText(application, "Dump page service details", "dumppage_text");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");

	}

	public void ShowNewInfovistaReport() throws Exception {
		Report.LogInfo("INFO", "'Verifying Show New InfoVista Report Functionality", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Show New InfoVista Report Functionality");
		shownewInfovista();
	}

	public void shownewInfovista() throws Exception {
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.shownewinfovistareport_link,
				"Show New Infovista Report");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.shownewinfovistareport_link,
				"Show New Infovista Report");

		waitforPagetobeenable();
		String expectedPageName = "SSO Login Page";

		// Switch to new tab
		List<String> browserTabs = new ArrayList<String>(webDriver.getWindowHandles());
		webDriver.switchTo().window(browserTabs.get(1));
		waitforPagetobeenable();

		try {
			if ((webDriver.switchTo().window(browserTabs.get(1)).getTitle()).contains(expectedPageName)) {

				// Get Tab name
				String pageTitle = webDriver.switchTo().window(browserTabs.get(1)).getTitle();
				System.out.println("page title displays as: " + pageTitle);

				// assertEquals(pageTitle, expectedPageName, "on clicking 'Show
				// Infovista link', it got naviagted to "+pageTitle);
				// sa.assertEquals(pageTitle, expectedPageName);

				webDriver.close();
				webDriver.switchTo().window(browserTabs.get(0));

				Report.LogInfo("INFO",
						"on clicking 'Show Infovista link', it got naviagted to " + pageTitle + " as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"on clicking 'Show Infovista link', it got naviagted to " + pageTitle + " as expected");

				Report.LogInfo("INFO", "show info vista page actual title: " + pageTitle, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "show info vista page actual title: " + pageTitle);
				// ExtentTestManager.getTest().log(LogStatus.PASS, "show info
				// vista page expected title: "+ expectedPageName);

			} else {

				webDriver.close();
				webDriver.switchTo().window(browserTabs.get(0));
			}
		} catch (Exception e) {

			e.printStackTrace();

			webDriver.close();
			webDriver.switchTo().window(browserTabs.get(0));

			Report.LogInfo("INFO", expectedPageName + " page is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, expectedPageName + " page is not displaying");

		}

	}

	public void verifyManageService1(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String servicestatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceStatus");
		String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceStatusChangeRequired");

		Report.LogInfo("INFO", "'Verifying Manage Service Functionality", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Manage Service Functionality");
		// Manage service
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.manageLink, "Manage");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.manageLink, "Manage");

		waitforPagetobeenable();

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.manageservice_header);
		compareText("Manage service header",
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.manageservice_header, "Manage Service");

		// ***GetText(application, "Order Name", "status_ordername");
		compareText("Service Identification", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.status_servicename,
				sid);
		compareText("Service type", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.status_servicetype,
				servicetype);

		// String ServiceDetails_value =
		// Gettext(getwebelement(xml.getlocator("//locators/" + application +
		// "/status_servicedetails")));
		String ServiceDetails_value = getTextFrom(
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.status_servicedetails,"status_service details");
		if (ServiceDetails_value.isEmpty()) {
			Report.LogInfo("INFO", "Service Details column value is empty as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Service Details column value is empty as expected");
		} else {
			Report.LogInfo("INFO", "Service Details column value should be empty", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Service Details column value should be empty");
		}
		compareText("Service Status", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.status_currentstatus,
				servicestatus);

		// String LastModificationTime_value =
		// Gettext(getwebelement(xml.getlocator("//locators/" + application +
		// "/status_modificationtime")));
		String LastModificationTime_value = getTextFrom(
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.status_modificationtime,"status_modification time");
		if (LastModificationTime_value.contains("BST")) {
			Report.LogInfo("INFO", "Service status is displayed as : " + LastModificationTime_value, "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO,
					"Service status is displayed as : " + LastModificationTime_value);

		} else {
			Report.LogInfo("INFO", "Incorrect modification time format", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Incorrect modification time format");
		}

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.statuslink, "Status");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.statuslink, "Status");

		if (servicestatuschangerequired == "Yes") {
			// WebElement ServiceStatusPage=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/Servicestatus_popup"));
			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Servicestatus_popup)) {
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.changestatus_dropdown,
						"Change Status");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.changestatus_dropdown, "Change Status");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.changestatus_dropdownvalue,
						"Change Status value");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.changestatus_dropdownvalue,
						"Change Status value");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "Ok Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "Ok Button");

				try {
					if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.servicestatushistory)) {
						Report.LogInfo("INFO", "Step : Service status change request logged", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service status change request logged");
					} else
						Report.LogInfo("INFO", "Step : Service status change request is not logged", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Step : Service status change request is not logged");
				} catch (StaleElementReferenceException e) {
					Report.LogInfo("INFO", "No service history to display", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No service history to display");
				}
			} else
				Report.LogInfo("INFO", "Status link is not working", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Status link is not working");
		} else {
			Report.LogInfo("INFO", "Step : Service status change not reqired", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service status change not reqired");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.servicestatus_popupclose,
					"Pop Up Close");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.servicestatus_popupclose, "Pop Up Close");

		}

		// synchronize panel in manage service page

		// GetText(application, "Order Name", "status_ordername");
		compareText("Service Identification", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.sync_servicename,
				sid);
		compareText("Service type", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.sync_servicetype,
				servicetype);

		// String ServiceDetails_value1 =
		// Gettext(getwebelement(xml.getlocator("//locators/" + application +
		// "/sync_servicedetails")));
		String ServiceDetails_value1 = getTextFrom(
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.sync_servicedetails,"status_service details");
		if (ServiceDetails_value1.isEmpty()) {
			Report.LogInfo("INFO", "Service Details column value is empty as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Details column value is empty as expected");

		} else {
			Report.LogInfo("INFO", "Service Details column value should be empty", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Details column value should be empty");

		}

		//scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managepage_backbutton);
		//verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managepage_backbutton, "Back button");
		//click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managepage_backbutton, "Back button");
	}

	public void verifyAddDSLAMandHSLlink() throws InterruptedException, IOException {

		Report.LogInfo("INFO", "Verifying 'Add DSLAM and HSL'", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Add DSLAM and HSL'");

		boolean actelisConfigurationPanel = false;
		try {
			waitforPagetobeenable();
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ConfigurationOptions_Header);
	
			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ActelisConfigurationPanel)) {
				Report.LogInfo("INFO", " 'Actelis Configuration' panel is displaying as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" 'Actelis Configuration' panel is displaying as expected");

				boolean actelisLink = false;
				try {
				
					if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Actelisconfig_addDSLAM)) {
						Report.LogInfo("INFO", " 'Add DSLAM and HSL' link is displaying as expected", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Add DSLAM and HSL' link is displaying as expected");

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Actelisconfig_addDSLAM,
								"Actelisconfig_addDSLAM");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Actelisconfig_addDSLAM,
								"Actelisconfig_addDSLAM");

					} else {
						Report.LogInfo("INFO",
								" 'Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel",
								"FAIl");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("INFO",
							" 'Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel", "FAIl");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" ''Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel ");
				} catch (Exception err) {
					err.printStackTrace();
					Report.LogInfo("INFO", " NOt able to click on 'Add DSLAM and HSL' link", "FAIl");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to click on 'Add DSLAM and HSL' link");
				}
			} else {
				Report.LogInfo("INFO", "'Actelis Configuration' panel is not displaying", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Actelis Configuration' panel is not displaying");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("INFO", " : Field is not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, e + " : Field is not displayed");
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", e + " : Field is not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, e + " : Field is not displayed");
		}
	}

	public void AddDSLAMandHSL(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String DSLMdevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ActelisTech_DSLAMdevice");
		String HSlname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ActelisTech_DSLAMInterfacename");
		
		waitforPagetobeenable();
		if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addDSLAMandHSL_xButton)){
		try {
			waitforPagetobeenable();
			// waitforPagetobeenable();

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addDSLAMandHSL_xButton,
					"addDSLAMandHSL_xButton");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addDSLAMandHSL_xButton,
					"addDSLAMandHSL_xButton");

			sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DSLM_Device_Select, DSLMdevice);

			try {
				if (isVisible(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.selectDSLAMdeviceValue1 + DSLMdevice
								+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.selectDSLAMdeviceValue2)) {
					
					
					waitforPagetobeenable();
					// waitforPagetobeenable();
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.List_HSL_Link, "List_HSL");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.List_HSL_Link, "List_HSL");

					selectRowForAddingInterface_Actelis(HSlname); // select the
																	// Interface

				} else {
					Report.LogInfo("INFO", DSLMdevice + " is not displaying under 'DSLAM device' dropdown", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
				}

				scrollUp();

			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("INFO", DSLMdevice + " is not displaying under 'DSLAM device' dropdown", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, e + " : Field is not displayed");
			System.out.println(e + " : Field is not displayed");
		} catch (Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, e + " : Field is not displayed");
			System.out.println(e + " : Field is not displayed");
		}
		}else{
			Report.LogInfo("INFO", "DSLAM is not availabe to add", "PASS");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DSLAM is not availabe to add");

		}
	}

	public void selectRowForAddingInterface_Actelis(String interfacenumber) throws IOException, InterruptedException {

		// String interfacenumber = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "ActelisTech_DSLAMdevice");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.InterfaceToSelect_actelis_totalpage);

		int TotalPages;

		String TextKeyword = getTextFrom(
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.InterfaceToSelect_actelis_totalpage,"Interface To Select_actelis_total page");
		TotalPages = Integer.parseInt(TextKeyword);

		System.out.println("Total number of pages in table is: " + TotalPages);

		ab:

		if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.InterfaceToSelect_actelis_currentpage,"Interface To Select_actelis_current page");
				int Current_page = Integer.parseInt(CurrentPage);
				System.out.println("Currently we are in page number: " + Current_page);

				// List<WebElement> results =
				// getwebelements("//div[contains(text(),'"+ interfacenumber
				// +"')]");
				List<WebElement> results = webDriver
						.findElements(By.xpath("//div[contains(text(),'" + interfacenumber + "')]"));

				int numofrows = results.size();
				System.out.println("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {

					webDriver.findElement(By.xpath("//button[text()='Next']")).click();
				} else {
					for (int i = 0; i < numofrows; i++) {
						try {
							resultflag = results.get(i).isDisplayed();
							System.out.println("status of result: " + resultflag);
							if (resultflag) {
								// System.out.println(results.get(i).getText());
								results.get(i).click();
								// ExtentTestManager.getTest().log(LogStatus.PASS,
								// interfacenumber + " is selected under 'Add
								// DSLAM and Device' page");
								webDriver.findElement(By.xpath("//button[text()='Next']")).click();
								// Clickon(getwebelement("//span[text()='Next']"));

							}

						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = webDriver
									.findElements(By.xpath("//div[contains(text(),'" + interfacenumber + "')]"));
							numofrows = results.size();
							// results.get(i).click();
							// Log.info("selected row is : " + i);
							// ExtentTestManager.getTest().log(LogStatus.FAIL,
							// "failure while selecting interface to add with
							// service");
						}
					}
					break ab;
				}
			}
		} else {
			Report.LogInfo("INFO", " NO interfaces got fetched", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO, " NO interfaces got fetched");
		}

	}

	public boolean fetchDeviceInterface_viewdevicepage() throws InterruptedException, IOException {

		boolean clickLink = false;
		String actualMessage = null;

		waitforPagetobeenable();
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.viewdevice_Actiondropdown);
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.viewdevice_Actiondropdown,
				"View Device Action Dropdown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.viewdevice_Actiondropdown,
				"View Device Action Dropdown");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.viewdevice_fetchinterfacelink,
				"View Device Fetch Interface");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.viewdevice_fetchinterfacelink,
				"View Device fect Interface");

		// verify success Message
		String expectedValue = "Fetch interfaces started successfully. Please check the sync status of this device";

		boolean successMessage = false;
		try {
			// successMessage=getwebelement(xml.getlocator("//locators/" +
			// application +
			// "/fetchDeviceInterace_SuccessMessage")).isDisplayed();
			// actualMessage=getwebelement(xml.getlocator("//locators/" +
			// application +
			// "/fetchdeviceInterface_successtextMessage")).getText();
			actualMessage = getTextFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.fetchdeviceInterface_successtextMessage,"fetch device  Interface_success text Message");
			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.fetchDeviceInterace_SuccessMessage)) {

				if (actualMessage.contains(expectedValue)) {

					Report.LogInfo("INFO",
							" After clicking on 'Fetch Device Interface' link, success Message is displaying", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" After clicking on 'Fetch Device Interface' link, success Message is displaying");

					clickLink = true;

				} else if (actualMessage.equalsIgnoreCase(expectedValue)) {
					Report.LogInfo("INFO",
							" After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected",
							"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected");
					clickLink = true;
				} else {
					Report.LogInfo("INFO",
							"Can't start the fetch operation because another process is already fetching the interfaces for this device.",
							"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Can't start the fetch operation because another process is already fetching the interfaces for this device.");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO",
					"Can't start the fetch operation because another process is already fetching the interfaces for this device.",
					"PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"Can't start the fetch operation because another process is already fetching the interfaces for this device.");
		}
		return clickLink;
	}

	public void verify_CiscoVendor_AddInterface(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Multilink_ConfigInterface_checkbox");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Multilink_InterfaceName");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String link_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LinkValue");
		// String bearertype_value = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "LinkValue");
		String ciscovendor_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Bandwidth_Value");
		// String framingtype_value = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "Bandwidth_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Multilink_Encapsulation_Value");
		// String vlanID_value = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "Multilink_Encapsulation_Value");
		String bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Multilink_BGPCheckbox");
		String bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGPTemplate_Value");
		String cpewan_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEWAN_Value");
		String cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEWANIPv6_Value");
		String descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Description_Value");
		String ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"AsCustomer_Value");
		String bgppassword_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BGPPassword_Value");

		Report.LogInfo("INFO", "Verifying Add Interface Functionality - Cisco", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Add Interface Functionality - Cisco");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Routes_Header);

		compareText("Interfaces header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaces_header,
				"Interfaces");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacepanel_actiondropdown, "dropdown");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacepanel_actiondropdown, "dropdown");

		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addinterface_link, "Add Interface/Link");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addinterface_link, "Add Interface/Link");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addinterface_header);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addinterface_header)) {
			Report.LogInfo("INFO", "'Add Interface' page navigated as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Interface' page navigated as expected");
			// System.out.println("'Add Interface' page navigated as expected");

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "ok button");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "ok button");

			// click_commonMethod(application, "OK", "okbutton", xml);

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interface_warngmsg);
			// verify warning messages in add interface page
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interface_warngmsg,
					"Interface warning message");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bearertype_warngmsg,
					"Bearer Type warning");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.encapsulation_warngmsg,
					"Encapsulation");

			// Add Interface
			addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configureinterface_checkbox,
					"Configure Interface on Device", configureinterface_checkbox);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacename_textfield, "Interface");
			sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacename_textfield, interfacename);
			// addtextFields_commonMethod(application, "Interface",
			// "interfacename_textfield", interfacename, xml);
			// GetText(application, "Network", "network_fieldvalue");

			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection,
					subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue,
					newinterfaceAddressrange);
			interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection,
					subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.link_textfield);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.link_textfield, "Interface text field");
			sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.link_textfield, link_value);

			// addtextFields_commonMethod(application, "Link", "link_textfield",
			// link_value, xml);
			// SelectDropdownValueUnderSelectTag("Bearer Type",
			// bearertype_value,
			// APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bearertype_dropdown);

			SelectDropdownValueUnderSelectTag("Bandwidth", ciscovendor_bandwidth_value,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bandwidth_dropdown);

			/*
			 * if(bearertype_value.equalsIgnoreCase("E1")) {
			 * //SelectDropdownValueUnderSelectTag("Framing Type",
			 * framingtype_value,
			 * APT_IPAccessResilientConfigObj.IPAccessResilientConfig.
			 * framingtype_dropdown);
			 * SelectDropdownValueUnderSelectTag("Encapsulation",
			 * encapsulation_value,
			 * APT_IPAccessResilientConfigObj.IPAccessResilientConfig.
			 * encapsulation_dropdown); } else
			 * if(bearertype_value.equalsIgnoreCase("Ethernet")) {
			 * verifyExists(APT_IPAccessResilientConfigObj.
			 * IPAccessResilientConfig.vlanid_textfield,"VLAN Id");
			 * sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.
			 * vlanid_textfield,vlanID_value);
			 * 
			 * } else { SelectDropdownValueUnderSelectTag("Encapsulation",
			 * encapsulation_value,
			 * APT_IPAccessResilientConfigObj.IPAccessResilientConfig.
			 * encapsulation_dropdown); }
			 */

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.link_textfield);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgp_checkbox, "BGP");
			sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgp_checkbox, bgp_checkbox);

			if (bgp_checkbox.equalsIgnoreCase("yes")) {
				SelectDropdownValueUnderSelectTag("BGP Templates Generate For", bgptemplate_dropdownvalue,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.multilink_bgptemplate_dropdown);
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewan_textfield, "CPE WAN");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewan_textfield, cpewan_value);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewanipv6_textfield,
						"CPE WAN IPv6 Address");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewanipv6_textfield, cpewanipv6_value);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.descriptionfield, "Description");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.descriptionfield,
						descriptionfield_value);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgp_ascustomerfield, "AS CUSTOMER");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgp_ascustomerfield,
						ascustomerfield_value);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgppassword_field, "BGP PASSWORD");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgppassword_field, bgppassword_value);

			}

			// scrolltoend();
			// configuration panel in add interface page
			generateConfiguration();
			// compareText(application, "Interface Added success message",
			// "AddInterfaceSucessMessage", "Interface successfully created.",
			// xml);
			verifysuccessmessage("Interface successfully created.");

		} else {
			Report.LogInfo("INFO", "'Add Interface' page not navigated", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add Interface' page not navigated");
		}

	}

	public void interfaceAddressRangeIPv4(String existingAddressRangeIPv4selection, String newAddressRangeIpv4selection,
			String subnetSizeValue_IPv4, String eipallocation_city, String existingAddressIPv4DropdownValue,
			String newinterfaceAddressrange) throws InterruptedException, IOException {

		boolean addressValue = false;

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			// verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_button,"EIP
			// Allocation button")
			// click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_button,"EIP
			// Allocation button");

			compareText("EIP Subnet Allocation",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipsubnetallocation_header,
					"EIP Subnet Allocation");

			addDropdownValues_commonMethod("City",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_citydropdown,
					eipallocation_city);
			addDropdownValues_commonMethod("Sub Net Size",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_subnetsize,
					subnetSizeValue_IPv4);

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");

			// waitForpageload();
			waitforPagetobeenable();
			// click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress_button,"Get
			// Address");

			// WebElement el =getwebelement(xml.getlocator("//locators/" +
			// application + "/interfacerange_Address_dropdown"));
			WebElement el = findWebElement(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown);
			Select sel = new Select(el);
			String interfaceAddressRange = sel.getFirstSelectedOption().getText();
			if (interfaceAddressRange.isEmpty()) {
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range' dropdown", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range' dropdown");

			} else {
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);

				waitforPagetobeenable();
				waitForAjax();

				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown)) {
					Report.LogInfo("INFO", "'Address' dropdown is displaying", "PASS");
				} else {
					Report.LogInfo("INFO", "'Address' dropdown is not displaying", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address' dropdown is not displaying");
				}
			}
		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			edittextFields_commonMethod("Interface Address Range",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrange_textfield,
					newinterfaceAddressrange);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow, ">>");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow, ">>");

			// String addressValueIntextField =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/address_textfield")).getAttribute("value");
			String addressValueIntextField = getAttributeFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.address_textfield, "value");
			if (addressValueIntextField.isEmpty()) {
				Report.LogInfo("INFO", "No values dipslaying under 'Address' text field", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address' text field");
			} else {
				Report.LogInfo("INFO", "value in 'Address' text field is displaying as: " + addressValueIntextField,
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address' text field is displaying as: " + addressValueIntextField);
			}

		}
	}

	public void interfaceAddressRangeIPv6(String existingAddressRangeIPv6selection, String newAddressRangeIpv6selection,
			String subnetSizeValue_IPv6, String availableBlocksValue_IPv6, String newinterfaceAddressrangeIPv6)
			throws InterruptedException, IOException {

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.network_fieldvalue);
		// IPv6 Configuration
		if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			// verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocationIPv6_button,"EIP
			// Allocation button");
			// click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocationIPv6_button,"EIP
			// Allocation button");

			selectValueInsideDropdown(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_ipv6_subnetsize,
					"Sub Net Size", subnetSizeValue_IPv6);
			selectValueInsideDropdown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.availableblocks_dropdown,
					"Available Blocks", availableBlocksValue_IPv6);

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.allocatesubnet_button,
					"Allocate subnet");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.allocatesubnet_button, "Allocate subnet");

			// EIPallocationSuccessMessage(application, "successfully
			// allocated");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");

			// verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddressIPv6_button,"Get
			// Address");
			// click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddressIPv6_button,"Get
			// Address");

			// click_commonMethod(application, "Get Address",
			// "getaddressIPv6_button", xml);

			// String
			// interfaceAddressRange=getwebelement(xml.getlocator("//locators/"
			// + application +
			// "/interfacerange_AddressIpv6_dropdown")).getText();
			String interfaceAddressRange = getTextFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_AddressIpv6_dropdown,"interface range_AddressIpv6_dropdown");
			if (interfaceAddressRange.isEmpty()) {
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv6' dropdown");

			} else {
				Report.LogInfo("INFO",
						"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
						">>");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow, ">>");

				// click_commonMethod(application, ">>" ,
				// "interfaceaddressIPv6_Addarrow", xml);

				waitForAjax();
				waitforPagetobeenable();

				// String AddressValueIntextField =
				// getwebelement(xml.getlocator("//locators/" + application +
				// "/addressIPv6_textfield")).getAttribute("value");
				String AddressValueIntextField = getAttributeFrom(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addressIPv6_textfield, "value");
				if (AddressValueIntextField.isEmpty()) {
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
				} else {
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + AddressValueIntextField, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + AddressValueIntextField);
				}
			}
		} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

			edittextFields_commonMethod("Interface Address Range",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrangeIPv6_textfield,
					newinterfaceAddressrangeIPv6);

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow, ">>");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow, ">>");

			// click_commonMethod(application, ">>" ,
			// "interfaceaddressIPv6_Addarrow", xml);

			// String addressValueIntextField =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addressIPv6_textfield")).getAttribute("value");
			String addressValueIntextField = getAttributeFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addressIPv6_textfield, "value");
			if (addressValueIntextField.isEmpty()) {
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv6' text field");
				// System.out.println("No values dipslaying under 'Address_IPv6'
				// text field");
			} else {
				Report.LogInfo("INFO",
						"value in 'Address_IPv6' text field is displaying as: " + addressValueIntextField, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv6' text field is displaying as: " + addressValueIntextField);
			}
		}
	}

	public void generateConfiguration() throws InterruptedException, IOException {

		// perform Generate configuration
		boolean configurationpanel = false;

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configuration_header)) {
			Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.generateconfiguration_button,
					"Generate Configuration");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.generateconfiguration_button,
					"Generate Configuration");

			waitforPagetobeenable();

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configuration_textarea);

			String configurationvalues = getTextFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configuration_textarea,"configuration_textarea");
			if (configurationvalues.isEmpty()) {
				Report.LogInfo("INFO",
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
						"FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
			} else {
				Report.LogInfo("INFO", "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
			}

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.saveconfiguration_button,
					"Save Configuration to File");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.saveconfiguration_button,
					"Save Configuration to File");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.executeandsave_button,
					"Execute and Save");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.executeandsave_button, "Execute and Save");

		} else {
			Report.LogInfo("INFO", "'Configuration' panel is not displaying", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO, "'Configuration' panel is not displaying");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "Ok");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "Ok");

		}
	}

	public void verify_CiscoVendor_EditInterface(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPE_DeviceName");
		String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editdeviceName");
		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String edit_network = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_Network");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_EIPAllocation_Subnetsize");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_BearerType_value");
		String edit_ciscovendor_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_CiscoVendor_Bandwidth_value");
		String edit_framingtype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_FramingTypeValue");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_EncapsulationValue");
		String edit_linkvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_LinkValue");
		String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_VLANIDvalue");
		String edit_bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_BGPCheckbox");
		String edit_cpewan_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_CPEWAN_Value");
		String edit_bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_BGPTemplate_Dropdownvalue");
		String edit_cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_CPEWANIPv6_Value");
		String edit_descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_DescriptionValue");
		String edit_ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_AsCustomerValue");
		String edit_bgppassword_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_BGPPasswordValue");
		String ipsubnetipv6_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_IPSubnetIPv6Value");
		String ipsubnetipv4_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_IPSubnetIPv4Value");

		// edit Interface
		Report.LogInfo("INFO", "Verifying Edit Interface Functionality - Cisco", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Edit Interface Functionality - Cisco");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.portalaccess_header);
		// ScrolltoElement(application, "portalaccess_header",
		// xml);//portalaccess_header//ConfigurationOptions_Header
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.existingdevicegrid)) {

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.selectinterface);

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.selectinterface)) {
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.selectinterface, "Select  Interface");
				Report.LogInfo("INFO", "Clicked on existing Interface radio button", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");

				// if(getwebelement(xml.getlocator("//locators/" + application +
				// "/addeddevice_interface_actiondropdown").replace("value",
				// name)).isDisplayed())
				if (isVisible(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown1
								+ name
								+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown2)) {
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown1
							+ name
							+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown2,
							"Action dropdown add device");

				}
				// else if(getwebelement(xml.getlocator("//locators/" +
				// application +
				// "/addeddevice_interface_actiondropdown").replace("value",
				// editDevicename)).isDisplayed())
				else if (isVisible(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown1
								+ editDevicename
								+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown2)) {
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown1
							+ editDevicename
							+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addeddevice_interface_actiondropdown2,
							"Action dropdown add device");

				}

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");

				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.editinterface_header)) {
					Report.LogInfo("INFO", "'Edit Interface' page navigated as expected", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'Edit Interface' page navigated as expected");

					editcheckbox_commonMethod(edit_configureinterface_checkbox,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configureinterface_checkbox,
							"Configure Interface on Device");
					if (!edit_interfacename.equalsIgnoreCase("null")) {
						// cleartext(application, "Interface",
						// "interfacename_textfield");
						clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacename_textfield);
						edittextFields_commonMethod("Interface",
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacename_textfield,
								edit_interfacename);
					} else {
						// GetText(application, "Interface",
						// "interfacename_textfield");
					}
					addDropdownValues_commonMethod("Network",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.network_fieldvalue, edit_network);

					interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection,
							subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue,
							newinterfaceAddressrange);
					interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection,
							subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);

					// ScrolltoElement(application, "link_textfield", xml);
					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.link_textfield);
					edittextFields_commonMethod("Link",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.link_textfield, edit_linkvalue);
					SelectDropdownValueUnderSelectTag("Bearer Type", edit_bearertype_value,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bearertype_dropdownEdit);
					SelectDropdownValueUnderSelectTag("Bandwidth", edit_ciscovendor_bandwidth_value,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.multilink_bandwidth_dropdown);

					if (edit_bearertype_value.equalsIgnoreCase("E1")) {
						SelectDropdownValueUnderSelectTag("Framing Type", edit_framingtype_value,
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.framingtype_dropdown);
						SelectDropdownValueUnderSelectTag("Encapsulation", edit_encapsulation_value,
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.encapsulation_dropdown);
					} else if (edit_bearertype_value.equalsIgnoreCase("Ethernet")) {
						edittextFields_commonMethod("VLAN Id",
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.vlanid_textfield,
								edit_vlanID_value);
					} else {
						SelectDropdownValueUnderSelectTag("Encapsulation", edit_encapsulation_value,
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.encapsulation_dropdownEdit);
					}

					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.link_textfield);

					editcheckbox_commonMethod(edit_bgp_checkbox,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgp_checkbox, "BGP");
					if (edit_bgp_checkbox.equalsIgnoreCase("yes")) {
						addDropdownValues_commonMethod("BGP Templates Generate For",
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgptemplate_dropdown,
								edit_bgptemplate_dropdownvalue);

						if (!edit_cpewan_value.equalsIgnoreCase("null")) {
							clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewan_textfield);
							edittextFields_commonMethod("CPE WAN",
									APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewan_textfield,
									edit_cpewan_value);
						} else {
							// GetText(application, "CPE WAN",
							// "cpewan_textfield");
						}
						if (!edit_cpewanipv6_value.equalsIgnoreCase("null")) {
							clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewanipv6_textfield);
							edittextFields_commonMethod("CPE WAN IPv6 Address",
									APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cpewanipv6_textfield,
									edit_cpewanipv6_value);
						} else {
							// GetText(application, "CPE WAN IPv6 Address",
							// "cpewanipv6_textfield");
						}
						if (!edit_descriptionfield_value.equalsIgnoreCase("null")) {
							clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.descriptionfield);
							edittextFields_commonMethod("Description",
									APT_IPAccessResilientConfigObj.IPAccessResilientConfig.descriptionfield,
									edit_descriptionfield_value);
						} else {
							// GetText(application, "Description",
							// "bgp-descriptionfield");
						}
						if (!edit_ascustomerfield_value.equalsIgnoreCase("null")) {
							clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgp_ascustomerfield);
							edittextFields_commonMethod("AS CUSTOMER",
									APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgp_ascustomerfield,
									edit_ascustomerfield_value);
						} else {
							// GetText(application, "AS CUSTOMER",
							// "bgp_ascustomerfield");
						}
						if (!edit_bgppassword_value.equalsIgnoreCase("null")) {
							clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgppassword_field);
							edittextFields_commonMethod("BGP PASSWORD",
									APT_IPAccessResilientConfigObj.IPAccessResilientConfig.bgppassword_field,
									edit_bgppassword_value);
						} else {
							// GetText(application, "BGP PASSWORD",
							// "bgppassword_field");
						}
						// Add IP Subnet IPv6
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_addbutton,
								"IP Subnet IPv6 Add");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_addbutton,
								"IP Subnet IPv6 Add");
						edittextFields_commonMethod("IP Subnet IPv6",
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_textfield,
								ipsubnetipv6_value);

						// Remove IP Subnet IPv6
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_removebutton,
								"IP Subnet IPv6 remove");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_removebutton,
								"IP Subnet IPv6 remove");

						// Add IP Subnet IPv6
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_addbutton,
								"IP Subnet IPv6 add");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_addbutton,
								"IP Subnet IPv6 add");

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_textfield,
								"IP Subnet IPv6 ");
						sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv6_textfield,
								ipsubnetipv6_value, "IP Subnet IPv6");

						// Add IP Subnet IPv4
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_addbutton,
								"IP Subnet IPv4 Add");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_addbutton,
								"IP Subnet IPv4 Add");

						edittextFields_commonMethod("IP Subnet IPv4",
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_textfield,
								ipsubnetipv4_value);

						// Remove IP Subnet IPv4
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_removebutton,
								"IP Subnet IPv4 Remove");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_removebutton,
								"IP Subnet IPv4 Remove");

						// Add IP Subnet IPv4
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_addbutton,
								"IP Subnet IPv4 Add");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_addbutton,
								"IP Subnet IPv4 Add");
						edittextFields_commonMethod("IP Subnet IPv4",
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ipsubnetipv4_textfield,
								ipsubnetipv4_value);
					}

					// scrolltoend();
					// configuration panel in edit interface page
					generateConfiguration();

					waitforPagetobeenable();
					// compareText(application, "Interface update success
					// message", "UpdateInterfaceSucessMessage", "Interface
					// successfully updated.", xml);
					verifysuccessmessage("Interface successfully updated.");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Edit Interface' page not navigated");
				System.out.println("'Edit Interface' page not navigated");
			}
		}
	}

	public void VerifyCreatedServiceInformationInViewServicePage(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceIdentification");
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String NetworkConfiguration = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NetworkConfiguration");
		String BillingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BillingType");
		String SecondaryCircuitOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SecondaryCircuitOrder");
		String PhoneService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PhoneService");
		String EmailService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EmailService");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");
		String ManageService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ManageService");
		String SNMPNotification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SNMPNotification");
		String RouterConfigurationViewIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv4");
		String RouterConfigurationViewIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv6");
		String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PerformanceReporting");
		String IPGuardian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IPGuardian");
		String ProactiveNotification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ProactiveNotification");
		String SmartMonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SmartMonitoring");
		String CloudPrioritization = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CloudPrioritization");
		String DeliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DeliveryChannel");
		String RouterBasedFirewall = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RouterBasedFirewall");
		String Qos = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Qos");
		String ActelisBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ActelisBased");
		String LoadShared = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LoadShared");
		String BGPASNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BGPASNumber");
		String BGPPassword = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BGPPassword");
		String PICustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PICustomer");
		String DSL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DSL");
		String BGPDescription = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BGPDescription");

		Report.LogInfo("INFO", "'Verifying Service panel informations", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Service panel informations");
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);
		// scrolltoview(application, "order panel header", "orderpanelheader",
		// xml);
		compareText("Service panel Header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.servicepanel_header,
				"Service");
		compareText_InViewPage("Service Identification", ServiceIdentification);
		compareText_InViewPage("Service Type", ServiceType);
		compareText_InViewPage("Network Configuration", NetworkConfiguration);
		compareText_InViewPage("Billing Type", BillingType);
		// **compareText_InViewPageOptional1(application, "Termination Date",
		// TerminationDate);
		compareText_InViewPage("Secondary Circuit Order", SecondaryCircuitOrder);
		compareText_InViewPage("Phone Contact", PhoneService);
		compareText_InViewPage("Email", EmailService);
		compareText_InViewPage("Remark", Remarks);
		compareText_InViewPage("Managed Service", ManageService);
		compareText_InViewPage("SNMP Notification", SNMPNotification);
		compareText_InViewPage("Router Configuration View IPv4", RouterConfigurationViewIPv4);
		compareText_InViewPage("Router Configuration View IPv6", RouterConfigurationViewIPv6);
		compareText_InViewPage("Performance Reporting", PerformanceReporting);
		compareText_InViewPage("IP Guardian", IPGuardian);
		compareText_InViewPage("Pro-active Notification", ProactiveNotification);
		compareText_InViewPage("Smarts Monitoring", SmartMonitoring);
		compareText_InViewPage("Cloud Prioritization", CloudPrioritization);
		compareText_InViewPage("Delivery Channel", DeliveryChannel);

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.servicepanel_header);
		compareText_InViewPage("Router Based Firewall", RouterBasedFirewall);
		compareText_InViewPage("QoS", Qos);
		compareText_InViewPage("Actelis Based", ActelisBased);
		compareText_InViewPage("Load Shared", LoadShared);
		compareText_InViewPage("BGP AS Number", BGPASNumber);
		compareText_InViewPage("BGP Password", BGPPassword);
		compareText_InViewPage("PI Customer", PICustomer);
		compareText_InViewPage("DSL", DSL);
		compareText_InViewPage("BGP Description", BGPDescription);
	}

	public void verifyAddCPEDeviceFunction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String CPE_DeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_DeviceName");
		String CPE_VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VendorModel");
		String CPE_Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Snmpro");
		String CPE_Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Country");
		String CPE_City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_City");
		String CPE_Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Site");

		// ScrolltoElement(application,
		// "CustomerPremiseEquipment_CPE_Panelheader",
		// xml);//CustomerPremiseEquipment_CPE_Panelheader
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerPremiseEquipment_CPE_Panelheader);
		if (isVisible(
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerPremiseEquipment_CPE_Panelheader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPEDeviceLink,
					"Add CPE Device Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPEDeviceLink, "Add CPE Device Link");
			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPEDevicePageHeader)) {
				Report.LogInfo("INFO", "'Add CPE Device' Page navigated as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Add CPE Device' Page navigated as expected");

				scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton);
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton, "Next Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton, "Next Button");

				try {
					if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_WarningMessage_Country)){
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_WarningMessage_Country,
							"Warning Country");
					}else{
						Report.LogInfo("INFO", "Warning Country Message not displayed", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Warning Country Message not displayed");

					}
					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_WarningMessage_VendorModel,
							"Warning Vendor");

					clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_DeviceNameTextfield);
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_DeviceNameTextfield,
							CPE_DeviceName);

					SelectDropdownValueUnderSelectTag("Vendor/Model", CPE_VendorModel,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VendorModelDropdown);
					// EnterTextValue(application, CPE_ManagementAddress,
					// "Management Address", "CPE_ManagementAddressTextfield");
					clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SnmproTextfield);
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SnmproTextfield, CPE_Snmpro,
							"Snmpro");

					// ClearAndEnterTextValue(application, "Snmpro",
					// "CPE_SnmproTextfield", CPE_Snmpro, xml);

					SelectDropdownValueUnderSelectTag("Country", CPE_Country,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_CountryDropdown);
					SelectDropdownValueUnderSelectTag("City", CPE_City,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_CityDropdown);
					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SiteDropdown);
					SelectDropdownValueUnderSelectTag("Site", CPE_Site,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SiteDropdown);
					// **SelectDropdownValueUnderSelectTag(application,
					// "Premise", CPE_Premise, "CPE_PremiseDropdown", xml);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton,
							"Next Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton, "Next Button");

					waitForAjax();
					waitforPagetobeenable();
					// verifysuccessmessage(application, "CPE Device added
					// successfully");
					verifysuccessmessage("Device successfully created.");

				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("INFO", "Field is not displayed in Add CPE Device page", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							e + " : Field is not displayed in Add CPE Device page");
					// System.out.println( e+ " : Field is not displayed in Add
					// CPE Device page");
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("INFO", "Field is not displayed in Add CPE Device page", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							e + " : Field is not displayed in Add CPE Device page ");
				}

			} else {
				Report.LogInfo("INFO", "'Add CPE Device' page not navigated", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add CPE Device' page not navigated");
			}
		} else {
			Report.LogInfo("INFO", "'Customer Premise Equipment (CPE)' panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Customer Premise Equipment (CPE)' panel not displayed");
		}
	}

	public void verifyAddedCPEDeviceInformation_View(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String CPE_DeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_DeviceName");
		String CPE_VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VendorModel");
		String CPE_ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ManagementAddress");
		String CPE_Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Snmpro");

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header)) {
			Report.LogInfo("INFO", "'View CPE Device' page navigated as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "'View CPE Device' page navigated as expected");

			try {
				// Verify All Device Information under View CPE Device page//Not
				// working

				compareText_InViewPage2("Name", CPE_DeviceName);
				compareText_InViewPage2("Vendor/Model", CPE_VendorModel);
				compareText_InViewPage2("Management Address", CPE_ManagementAddress);
				compareText_InViewPage2("Snmpro", CPE_Snmpro);

			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("INFO", "Field is not displayed in View CPE Device page", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						e + " : Field is not displayed in View CPE Device page");
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("INFO", "Field is not displayed in View CPE Device page", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						e + " : Field is not displayed in View CPE Device page ");
			}

		} else {
			Report.LogInfo("INFO", "'View CPE Device' page not navigated", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'View CPE Device' page not navigated");
		}
		Report.LogInfo("INFO", "All Fields values verified in View CPE Device Page", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "All Fields values verified in View CPE Device Page");
	}

	public void verifyEditCPEDeviceFunction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String CPE_VendorModelEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VendorModelEdit");
		String CPE_SnmproEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_SnmproEdit");
		String CPE_CountryEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_CountryEdit");
		String CPE_CityEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_CityEdit");
		String CPE_SiteEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_SiteEdit");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header);
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header)) {

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_ActionLink, "ACTION Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_ActionLink, "ACTION Link");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_Action_EditLink, "Edit Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_Action_EditLink, "Edit Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EditDevice_Header)) {
				Report.LogInfo("INFO", "'Edit CPE Device' page navigated as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Edit CPE Device' page navigated as expected");

				try {

					SelectDropdownValueUnderSelectTag("Vendor/Model", CPE_VendorModelEdit,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VendorModelDropdown);

					// ***ClearAndEnterTextValue(application, "Management
					// Address", "CPE_ManagementAddressTextfield",
					// CPE_ManagementAddressEdit, xml);
					clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SnmproTextfield);
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SnmproTextfield, CPE_SnmproEdit,
							"Snmpro");

					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_CountryDropdown);

					SelectDropdownValueUnderSelectTag("Country", CPE_CountryEdit,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_CountryDropdown);
					SelectDropdownValueUnderSelectTag("City", CPE_CityEdit,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_CityDropdown);
					SelectDropdownValueUnderSelectTag("Site", CPE_SiteEdit,
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SiteDropdown);
					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditCPE_OKButton);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditCPE_OKButton, "OK Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditCPE_OKButton, "OK Button");
					waitforPagetobeenable();
					compareText("CPE Device Update message",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_UpdateCPEDeviceSuccessMessage,
							"Device successfully updated.");

				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("INFO", "Field is not displayed in Edit CPE Device page", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Field is not displayed in Edit CPE Device page");
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("INFO", "Field is not displayed in Edit CPE Device page", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Field is not displayed in Edit CPE Device page ");
				}

			} else {
				Report.LogInfo("INFO", "'Edit CPE Device' page not  navigated", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Edit CPE Device' page not  navigated");
			}

		} else {
			Report.LogInfo("INFO", "'View CPE Device' page not navigated", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'View CPE Device' page not navigated");
		}

	}

	public void navigateToViewDevicePage_CPE() throws InterruptedException, IOException {

		// ScrolltoElement(application,
		// "CustomerPremiseEquipment_CPE_Panelheader",
		// xml);//CustomerPremiseEquipment_CPE_Panelheader
		// //managementOptionsPanelheader//CustomerPremiseEquipment_CPE_Panelheader
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.existingCPEdevicegrid);
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.existingCPEdevicegrid)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_viewdevice1, "View Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_viewdevice1, "View Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header)) {
				Report.LogInfo("INFO", "'View CPE Device' Page navigated as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "'View CPE Device' Page navigated as expected");

			} else {
				Report.LogInfo("INFO", "'View CPE Device' Page not navigated", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'View CPE Device' Page not navigated");
			}
		} else {
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}

	}

	// Customer Readonly SNMP Panel
	public void addCustomerReadonlySNMPFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String CustomerIPAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CustomerIPAddress");
		String CustomerCommunityString = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CustomerCommunityString");

		Report.LogInfo("INFO", "Verifying Add Customer Readonly SNMP Functionality", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Add Customer Readonly SNMP Functionality");
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Routes_Header);
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerReadonlySNMP_panelHeader)) {

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_Actionbutton, "View Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_Actionbutton, "View Link");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_AddLink, "Add Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_AddLink, "Add Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddSNMP_Header)) {

				compareText("Add SNMP_Header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddSNMP_Header,
						"Add Customer Readonly SNMP");
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_OKButton, "Ok Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_OKButton, "Ok Button");

				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerIPAddress_WarningMessage,
						"Customer IP Address");
				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerCommunityString_WarningMessage,
						"Customer Community String");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerIPAddress_Textfield,
						"Customer IP Address");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerIPAddress_Textfield,
						CustomerIPAddress, "Customer IP Address");

				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerCommunityString_Textfield,
						"Customer Community String");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerCommunityString_Textfield,
						CustomerCommunityString, "Customer Community String");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_OKButton, "Ok Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_OKButton, "Ok Button");

				scrollUp();
				verifysuccessmessage("Customer Readonly SNMP successfully created.");

			} else {
				Report.LogInfo("INFO", "Add Customer Readonly SNMP  panel header is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Add Customer Readonly SNMP  panel header is not displaying");
			}

		}
	}

	public void editCustomerReadonlySNMPFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String CustomerIPAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CustomerIPAddressEdit");
		String CustomerCommunityStringEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CustomerCommunityStringEdit");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Routes_Header);
		// ScrolltoElement(application, "Routes_Header",
		// xml);//interfaces_header//CustomerReadonlyNMP_panelHeader//Routes_Header

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerReadonlySNMP_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_SelectRouteCheckbox,
					"Select SNMP Checkbox");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_SelectRouteCheckbox,
					"Select SNMP Checkbox");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_EditLink, "Edit Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_EditLink, "Edit Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditSNMP_Header)) {

				compareText("Edit SNMP_Header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditSNMP_Header,
						"Edit Customer Readonly SNMP");

				clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerIPAddress_Textfield);
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerIPAddress_Textfield,
						CustomerIPAddressEdit);

				clearTextBox(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerCommunityString_Textfield);
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_CustomerCommunityString_Textfield,
						CustomerCommunityStringEdit);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_OKButton, "OK Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_OKButton, "OK Button");
				scrollUp();
				verifysuccessmessage("Customer Readonly SNMP successfully updated.");
			}
		} else {
			Report.LogInfo("INFO", "Edit Customer Readonly SNMP  panel header is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Edit Customer Readonly SNMP  panel header is not displaying");
		}
	}

	public void deleteCustomerReadonlySNMPFunction_CPE() throws InterruptedException, IOException {

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Routes_Header);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerReadonlySNMP_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_SelectRouteCheckbox,
					"Select SNMP Checkbox");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_SelectRouteCheckbox,
					"Select SNMP Checkbox");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_Actionbutton, "Action");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_DeleteLink, "Delete Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_DeleteLink, "Delete Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.delete_alertpopup)) {
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_DeleteButton, "Delete ");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SNMP_DeleteButton, "Delete ");
			} else {
				Report.LogInfo("INFO", "Step : Delete alert popup is not displayed for SNMP", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed for SNMP");
			}
			scrollUp();
			verifysuccessmessage("Customer Readonly SNMP successfully deleted.");

		} else {
			Report.LogInfo("INFO", "Customer Readonly SNMP  panel header is not displaying in View CPE Device page",
					"FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Customer Readonly SNMP  panel header is not displaying in View CPE Device page");
		}
	}

	// Extra Subnet Panel
	public void addExtraSubnetFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ExtraSubnets_City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExtraSubnets_City");
		String ExtraSubnets_SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExtraSubnets_SubnetSize");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerReadonlySNMP_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_PanelHeader)) {

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_Action, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_Action, "Action");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AllocateExtraSubnets_Link,
					"Allocate Extra Subnets Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AllocateExtraSubnets_Link,
					"Allocate Extra Subnets Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddExtraSubnets_Header)) {
				compareText("Subnet Type Value",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_SubnetTypeValue, "CUST");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_AllocateSubnetButon,
						"Allocate Subnet Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_AllocateSubnetButon,
						"Allocate Subnet Button");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_City_WarningMessage,
						"City");
				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_SubnetSize_WarningMessage,
						"Subnet Size  ");
				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_EIPAllocation_AvilablePoolWarningMessage,
						"Avilable Pool ");

				SelectDropdownValueUnderSpanTag("City dropdown", ExtraSubnets_City,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_CityDropdown,
						"commonDropdownValueSpanTag");
				SelectDropdownValueUnderSpanTag("Subnet Size dropdown", ExtraSubnets_SubnetSize,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_SubnetSizeDropdown,
						"commonDropdownValueSpanTag");

				compareText("Allocate Subnet Button",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_AllocateSubnetButon,
						"Allocate Subnet");
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_CloseButton,
						"Close Extra Subnets window");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_CloseButton,
						"Close Extra Subnets window");
			} else {
				Report.LogInfo("INFO", "Add Extra Subnets popup is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Add Extra Subnets popup is not displaying");
			}

		} else {
			Report.LogInfo("INFO", "Extra Subnets  panel header is not displaying In view CPE Device page", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Extra Subnets  panel header is not displaying In view CPE Device page");
		}
	}

	public void SelectDropdownValueUnderSpanTag(String lebelname, String dropdownToBeSelectedInTheEnd,
			String dropdownXpath, String commonDropdownValueTag) throws InterruptedException, IOException {

		try {
			// Select Country dropdown
			click(dropdownXpath,"Dropdown");
			List<WebElement> dropdownValueList = webDriver.findElements(By.xpath(commonDropdownValueTag));

			for (WebElement dropdownvaluelist : dropdownValueList) {
				System.out.println(
						"Available " + lebelname + " are : " + dropdownvaluelist.getText().toString() + "  ,  ");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : Available  '" + lebelname + "'  is : " + dropdownvaluelist.getText().toString());
				Report.LogInfo("INFO",
						"Step : Available  '" + lebelname + "'  is : " + dropdownvaluelist.getText().toString(),
						"PASS");
			}

			// click on Country dropdown
			WebElement selectDropdownValue = webDriver
					.findElement(By.xpath("//span[@aria-label='" + dropdownToBeSelectedInTheEnd + "']"));
			System.out.println("Selected  '" + lebelname + "'  is : " + selectDropdownValue.getText().toString());
			selectDropdownValue.click();

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("INFO", lebelname + " dropdown is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, lebelname + " dropdown is not displaying");

		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("INFO", lebelname + " dropdown is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, lebelname + " dropdown is not displaying");
		}

	}

	// NAT Configuration
	public void editNATConfigurationFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String StaticNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String DynamicNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ExtraSubnets_PanelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NATConfiguration_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NAT_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NAT_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NAT_EditLink, "Edit Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NAT_EditLink, "Edit Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditNAT_Header)) {

				compareText("Edit NAT Configuration Header",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditNAT_Header, "NAT Configuration");

				// Static NAT Checkbox
				if (isEnable(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Checkbox)) {
					if (StaticNATEdit.equalsIgnoreCase("YES")) {

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Checkbox,
								"'Static NAT' checkbox");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Checkbox,
								"'Static NAT' checkbox");
					} else {
						Report.LogInfo("INFO", "Step : 'Static NAT' checkbox is not Selected", "PASS");
						ExtentTestManager.getTest().log(LogStatus.INFO, "Step : 'Static NAT' checkbox is not Selected");
					}
				} else {
					Report.LogInfo("INFO",
							"Step : 'Static NAT' checkbox is Disabled, So can't make any changes in Checkbox status",
							"PASS");

					ExtentTestManager.getTest().log(LogStatus.INFO,
							"Step : 'Static NAT' checkbox is Disabled, So can't make any changes in Checkbox status");
				}

				// Dynamic NAT Checkbox
				if (isEnable(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Checkbox)) {
					if (DynamicNATEdit.equalsIgnoreCase("YES")) {
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Checkbox,
								"'Dynamic NAT' checkbox");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Checkbox,
								"'Dynamic NAT' checkbox");
					} else {
						Report.LogInfo("INFO", "Step : 'Dynamic NAT' checkbox is not Selected", "PASS");
						ExtentTestManager.getTest().log(LogStatus.INFO,
								"Step : 'Dynamic NAT' checkbox is not Selected");
					}

				} else {
					Report.LogInfo("INFO",
							"Step : 'Dynamic NAT' checkbox is Disabled, So can't make any changes in Checkbox status",
							"PASS");
					ExtentTestManager.getTest().log(LogStatus.INFO,
							"Step : 'Dynamic NAT' checkbox is Disabled, So can't make any changes in Checkbox status");
				}

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NAT_OKButton, "Ok button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NAT_OKButton, "Ok Button");

				scrollUp();

				verifysuccessmessage("Nat Configuration successfully updated.");
			}

		} else {
			Report.LogInfo("INFO", "NAT Configuration  panel header is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT Configuration  panel header is not displaying");
		}

	}

	// Add Static NAT Mapping

	public void addStaticNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String Static_LocalPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_LocalPort");
		String Static_GlobalPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_GlobalPort");
		String Static_LocalIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");
		String Static_GlobalIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_GlobalIP");
		String Static_Protocol = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_Protocol");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NATConfiguration_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNATMapping_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_AddLink, "Add Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_AddLink, "Add Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddStaticNAT_Header)) {
				addDropdownValues_commonMethod("Protocol",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Protocol_Dropdown,
						Static_Protocol);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalPort_WarningMessage,
						"Local Port");
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalPort_WarningMessage,
						"Global Port ");
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalIP_WarningMessage,
						"Local IP");
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalIP_WarningMessage,
						"Global IP ");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalPort_Textfield,
						"LOCAL PORT textfield");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalPort_Textfield,
						Static_LocalPort);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalPort_Textfield,
						"GLOBAL PORT textfield");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalPort_Textfield,
						Static_GlobalPort);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalIP_Textfield,
						"LOCAL IP textfield");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalIP_Textfield,
						Static_LocalIP);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalIP_Textfield,
						"Static Global Ip");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalIP_Textfield,
						Static_GlobalIP);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");

				verifysuccessmessage("Static NAT successfully inserted.");

			} else {
				Report.LogInfo("INFO", "Add Static NAT Mapping Header/popup not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Add Static NAT Mapping Header/popup not displayed");
			}

		} else {
			Report.LogInfo("INFO", "Static NAT Mapping Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Static NAT Mapping Panel not displayed");
		}

	}

	// Edit Static NAT Mapping
	public void editStaticNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Static_ProtocolEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_ProtocolEdit");
		String Static_LocalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_LocalPortEdit");
		String Static_GlobalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_GlobalPortEdit");
		String Static_LocalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_LocalIPEdit");
		String Static_GlobalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_GlobalIPEdit");
		String Static_LocalIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");

		try {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NATConfiguration_panelHeader);

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNATMapping_panelHeader)) {
				scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNATMapping_panelHeader);
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox1+Static_LocalIP+APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox2,
						"Select Static NAT Checkbox");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox1+Static_LocalIP+APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox2,
						"Select Static NAT Checkbox");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Actionbutton, "Action");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Actionbutton, "Action");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Editink, "Edit");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Editink, "Edit");

				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditStaticNAT_Header)) {
					addDropdownValues_commonMethod("Protocol",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Protocol_Dropdown,
							Static_ProtocolEdit);

					edittextFields_commonMethod("LOCAL PORT textfield",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalPort_Textfield,
							Static_LocalPortEdit);
					edittextFields_commonMethod("GLOBAL PORT textfield",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalPort_Textfield,
							Static_GlobalPortEdit);
					edittextFields_commonMethod("LOCAL IP textfield",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_LocalIP_Textfield,
							Static_LocalIPEdit);
					edittextFields_commonMethod("GLOBAL IP textfield",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_GlobalIP_Textfield,
							Static_GlobalIPEdit);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");

					verifysuccessmessage("Static NAT successfully updated.");

				} else {
					Report.LogInfo("INFO", "Edit Static NAT Mapping Header/popup not displayed", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Edit Static NAT Mapping Header/popup not displayed");
				}

			} else {
				Report.LogInfo("INFO", "Static NAT Mapping Panel not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Static NAT Mapping Panel not displayed");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("INFO", ": Field is not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, e + " : Field is not displayed");
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", ": Field is not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, e + " : Field is not displayed");
		}

	}

	// Delete Static NAT Mapping
	public void deleteStaticNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Static_LocalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Static_LocalIPEdit");
		String Static_LocalIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NATConfiguration_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNATMapping_panelHeader)) {

			if (!Static_LocalIPEdit.equalsIgnoreCase("Null")) {
				
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox1
						+ Static_LocalIPEdit
						+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox2, "OK Button");

			} else {
				
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox1 + Static_LocalIP
						+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox2, "OK Button");
			}

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_DeleteLink, "Delete Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNAT_DeleteLink, "Delete Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.delete_alertpopup)) {
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DeleteButton_common, "Delete");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DeleteButton_common, "Delete");
			} else {
				Report.LogInfo("INFO", "Step : Delete alert popup is not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
			}
			scrollUp();
			verifysuccessmessage("Static NAT successfully deleted.");
		} else {
			Report.LogInfo("INFO", "Static NAT Mapping panel header not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Static NAT Mapping panel header not displayed");
		}
	}

	public void addDynamicNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String StaticNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String DynamicNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");
		String Dynamic_PoolMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_PoolMode");
		String Dynamic_InterfaceMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_InterfaceMode");
		String Dynamic_LocalNetwork = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_LocalNetwork");
		String Dynamic_PoolStartAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_PoolStartAddress");
		String Dynamic_PoolEndAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_PoolEndAddress");
		String Dynamic_PoolPrefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_PoolPrefix");
		String Dynamic_MapsToInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_MapsToInterface");

		if (StaticNATEdit.equalsIgnoreCase("no") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NATConfiguration_panelHeader);
		} else if (StaticNATEdit.equalsIgnoreCase("Yes") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNATMapping_panelHeader);
		}
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNATMapping_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_AddLink, "Add Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_AddLink, "Add Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddDynamicNAT_Header)) {

				if (Dynamic_PoolMode.equalsIgnoreCase("Yes") && Dynamic_InterfaceMode.equalsIgnoreCase("No")) {
					try {
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolModeCheckbox,
								"Pool Mode");
						Thread.sleep(1000);
						javaScriptclick(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolModeCheckbox,
								"Pool Mode");

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common,
								"Ok Button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkWarningMessage,
								"Local Network ");
						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolStartAddressWarningMessage,
								"Pool Start Address ");
						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolEndAddressWarningMessage,
								"Pool End Address ");
						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolPrefixWarningMessage,
								"Pool Prefix");

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkTextfield,
								"Local Network textfield");
						sendKeys(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkTextfield,
								Dynamic_LocalNetwork);

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolStartAddressTextfield,
								"Local Start Address textfield");
						sendKeys(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolStartAddressTextfield,
								Dynamic_PoolStartAddress);

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolEndAddressTextfield,
								"Pool End Address textfield");
						sendKeys(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolEndAddressTextfield,
								Dynamic_PoolEndAddress);

						SelectDropdownValueUnderSelectTag("Pool Prefix", Dynamic_PoolPrefix,
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolPrefixDropdown);

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common,
								"Ok Button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

						verifysuccessmessage("Dynamic NAT successfully inserted.");

					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Add Dynamic NAT page", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Add Dynamic NAT page");
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Add Dynamic NAT page ", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Add Dynamic NAT page ");
					}
				}

				if (Dynamic_PoolMode.equalsIgnoreCase("No") && Dynamic_InterfaceMode.equalsIgnoreCase("Yes")) {
					try {
						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_InterfaceModeCheckbox,
								"Interface Mode");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_InterfaceModeCheckbox,
								"Interface Mode");

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common,
								"Ok Button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkWarningMessage,
								"Local Network Warning message");
						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_MapToInterfaceWarningMessage,
								"Map To Interface Warning message");

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkTextfield,
								"Local Network textfield");
						sendKeys(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkTextfield,
								Dynamic_LocalNetwork);
						SelectDropdownValueUnderSelectTag("Maps to Interface", Dynamic_MapsToInterface,
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_MapToInterfaceDropdown);

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common,
								"Ok Button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

						verifysuccessmessage("Dynamic NAT successfully inserted.");

					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Add Dynamic NAT page", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Add Dynamic NAT page");
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Add Dynamic NAT page ", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Add Dynamic NAT page ");
					}
				}
			} else {
				Report.LogInfo("INFO", "Add Dynamic NAT Mapping Header/popup not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Add Dynamic NAT Mapping Header/popup not displayed");
			}

		} else {
			Report.LogInfo("INFO", "Dynamic NAT Mapping Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Dynamic NAT Mapping Panel not displayed");
		}
	}

	// Edit Dynamic NAT Mapping
	public void editDynamicNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String StaticNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String DynamicNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");
		String EditDynamic_PoolMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_PoolMode");
		String EditDynamic_InterfaceMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_InterfaceMode");
		String EditDynamic_LocalNetwork = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_LocalNetwork");
		String EditDynamic_PoolStartAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_PoolStartAddress");
		String EditDynamic_PoolPrefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_PoolPrefix");
		String EditDynamic_PoolEndAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_PoolEndAddress");
		String EditDynamic_MapsToInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_MapsToInterface");

		if (StaticNATEdit.equalsIgnoreCase("no") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NATConfiguration_panelHeader);

		} else if (StaticNATEdit.equalsIgnoreCase("Yes") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNATMapping_panelHeader);
		}
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNATMapping_panelHeader)) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDynamicNATCheckbox);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDynamicNATCheckbox,
					"Select Dynamic NAT Checkbox");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDynamicNATCheckbox,
					"Select Dynamic NAT Checkbox");
			
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Editink, "Edit Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Editink, "Edit Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditDynamicNAT_Header)) {

				if (EditDynamic_PoolMode.equalsIgnoreCase("Yes") && EditDynamic_InterfaceMode.equalsIgnoreCase("No")) {
					try {

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolModeCheckbox,
								"Pool Mode");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolModeCheckbox,
								"Pool Mode");

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkTextfield,
								"Local Network textfield");
						sendKeys(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolLocalNetworkTextfield,
								EditDynamic_LocalNetwork);

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolStartAddressTextfield,
								"Pool Start Address textfield");
						sendKeys(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolStartAddressTextfield,
								EditDynamic_PoolStartAddress);

						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolEndAddressTextfield,
								"Pool End Address textfield");
						sendKeys(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolEndAddressTextfield,
								EditDynamic_PoolEndAddress);

						SelectDropdownValueUnderSelectTag("Pool Prefix", EditDynamic_PoolPrefix,
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_PoolPrefixDropdown);

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common,
								"OK Button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");

						verifysuccessmessage("Dynamic NAT successfully updated.");
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Add Dynamic NAT page", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Add Dynamic NAT page");
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Add Dynamic NAT page ", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Add Dynamic NAT page ");
					}
				}

				if (EditDynamic_PoolMode.equalsIgnoreCase("No") && EditDynamic_InterfaceMode.equalsIgnoreCase("Yes")) {
					try {
						verifyExists(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_InterfaceModeCheckbox,
								"Interface Mode");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_InterfaceModeCheckbox,
								"Interface Mode");

						SelectDropdownValueUnderSelectTag("Maps to Interface", EditDynamic_MapsToInterface,
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_MapToInterfaceDropdown);

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common,
								"Ok Button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

						verifysuccessmessage("Dynamic NAT successfully updated.");
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Edit Dynamic NAT page", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Edit Dynamic NAT page");
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("INFO", " : Field is not displayed in Edit Dynamic NAT page ", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								e + " : Field is not displayed in Edit Dynamic NAT page ");
					}
				}
			} else {
				Report.LogInfo("INFO", "Edit Dynamic NAT Mapping Header/popup not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit Dynamic NAT Mapping Header/popup not displayed");
			}

		} else {
			Report.LogInfo("INFO", "Dynamic NAT Mapping Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Dynamic NAT Mapping Panel not displayed");
		}
	}

	// Delete Dynamic NAT Mapping
	public void deleteDynamicNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String StaticNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String DynamicNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");
		String Dynamic_LocalNetwork = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Dynamic_LocalNetwork");
		String EditDynamic_LocalNetwork = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDynamic_LocalNetwork");

		if (StaticNATEdit.equalsIgnoreCase("no") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.NATConfiguration_panelHeader);
		} else if (StaticNATEdit.equalsIgnoreCase("Yes") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.StaticNATMapping_panelHeader);
		}
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNATMapping_panelHeader)) {
			if (!EditDynamic_LocalNetwork.equalsIgnoreCase("Null")) {

				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox1
						+ EditDynamic_LocalNetwork
						+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox2, "Static Nat checkbox");
			} else {
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox1
						+ Dynamic_LocalNetwork
						+ APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectStaticNATCheckbox2, "Static Nat checkbox");
			}

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_Actionbutton, "Action");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_DeleteLink, "Delete Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNAT_DeleteLink, "Delete Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.delete_alertpopup)) {
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DeleteButton_common, "Delete");
			} else {
				Report.LogInfo("INFO", "Step : Delete alert popup is not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
			}
			scrollUp();
			verifysuccessmessage("Dynamic NAT successfully deleted.");
		} else {
			Report.LogInfo("INFO", "Dynamic NAT Mapping panel header not displayed", "FAIL");
			;
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Dynamic NAT Mapping panel header not displayed");
		}
	}

	// Add DHCP Servers on CPE
	public void addDHCPServersonCPEFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String DHCP_CustomerLANSubnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_CustomerLANSubnet");
		String DHCP_SubnetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_SubnetMask");
		String DHCP_PrimaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_PrimaryDNSServer");
		String DHCP_SecondaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_SecondaryDNSServer");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNATMapping_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_AddLink, "Add Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_AddLink, "Add Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddDHCP_Header)) {
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_CustomerLANSubnet_WarningMessage,
						"Customer LAN Subnet");
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_SubnetMask_WarningMessage,
						"Subnet Mask");
				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_PrimaryDNSServer_WarningMessage,
						"Primary DNS Server ");

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_CustomerLANSubnet_Textfield,
						"Customer LAN Subnet textfield");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_CustomerLANSubnet_Textfield,
						DHCP_CustomerLANSubnet);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_SubnetMask_Textfield,
						"Subnet Mask textfield");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_SubnetMask_Textfield,
						DHCP_SubnetMask);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_PrimaryDNSServer_Textfield,
						"Primary DNS Server textfield");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_PrimaryDNSServer_Textfield,
						DHCP_PrimaryDNSServer);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_SecondaryDNSServer_Textfield,
						"Secondary DNS Server textfield");
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_SecondaryDNSServer_Textfield,
						DHCP_SecondaryDNSServer);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

				verifysuccessmessage("DHCP server successfully created.");

			} else {
				Report.LogInfo("INFO", "Add DHCP Servers on CPE Header/popup not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Add DHCP Servers on CPE Header/popup not displayed");
			}

		} else {
			Report.LogInfo("INFO", "DHCP Servers on CPE Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DHCP Servers on CPE Panel not displayed");
		}
	}

	// Edit DHCP Servers on CPE
	public void editDHCPServersonCPEFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String DHCP_CustomerLANSubnetEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_CustomerLANSubnetEdit");
		String DHCP_SubnetMaskEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_SubnetMaskEdit");
		String DHCP_PrimaryDNSServerEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_PrimaryDNSServerEdit");
		String DHCP_SecondaryDNSServerEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCP_SecondaryDNSServerEdit");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNATMapping_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_panelHeader)) {
			
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPCheckbox);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPCheckbox,
					"Select DHCP Servers on CPE Checkbox");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPCheckbox,
					"Select DHCP Servers on CPE Checkbox");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Editink, "Edit Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Editink, "Edit Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditDHCP_Header)) {

				edittextFields_commonMethod("Customer LAN Subnet textfield",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_CustomerLANSubnet_Textfield,
						DHCP_CustomerLANSubnetEdit);
				edittextFields_commonMethod("Subnet Mask textfield",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_SubnetMask_Textfield,
						DHCP_SubnetMaskEdit);
				edittextFields_commonMethod("Primary DNS Server textfield",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_PrimaryDNSServer_Textfield,
						DHCP_PrimaryDNSServerEdit);
				edittextFields_commonMethod("Secondary DNS Server textfield",
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_SecondaryDNSServer_Textfield,
						DHCP_SecondaryDNSServerEdit);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

				verifysuccessmessage("DHCP server successfully updated.");

			} else {
				Report.LogInfo("INFO", "Edit DHCP Servers on CPE Header/popup not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit DHCP Servers on CPE Header/popup not displayed");
			}

		} else {
			Report.LogInfo("INFO", "DHCP Servers on CPE Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DHCP Servers on CPE Panel not displayed");
		}
	}

	// Delete DHCP Servers on CPE
	public void deleteDHCPServersonCPEFunction_CPE() throws InterruptedException, IOException {

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DynamicNATMapping_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPCheckbox,
					"Select DHCP Servers on CPE Checkbox");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPCheckbox,
					"Select DHCP Servers on CPE Checkbox");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_DeleteLink, "Delete Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_DeleteLink, "Delete Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.delete_alertpopup)) {
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DeleteButton_common,
						"Delete Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DeleteButton_common, "Delete Button");
			} else {
				Report.LogInfo("INFO", "Step : Delete alert popup is not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
			}
			scrollUp();
			verifysuccessmessage("DHCP server successfully deleted.");
		} else {
			Report.LogInfo("INFO", "DHCP Servers on CPE panel header not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DHCP Servers on CPE panel header not displayed");
		}
	}

	// Add DHCP IPV6 Servers on CPE
	public void addDHCPIPV6ServersonCPEFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String DHCPIPV6_DHCPType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCPIPV6_DHCPType");
		String DHCPIPV6_DomainName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCPIPV6_DomainName");
		String DHCPIPV6_SecondaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCPIPV6_SecondaryDNSServer");
		String DHCPIPV6_SubnetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCPIPV6_SubnetMask");
		String DHCPIPV6_LANIPV6Subnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCPIPV6_LANIPV6Subnet");
		String DHCPIPV6_PrimaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DHCPIPV6_PrimaryDNSServer");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_panelHeader)) {

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_AddLink, "Add Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_AddLink, "Add Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddDHCPIPV6_Header)) {
				if (DHCPIPV6_DHCPType.contains("Stateless DHCP")) {
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_WarningMessage,
							"Primary DNS Server ");

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");

					// WebElement
					// SelectDHCPIPV6_DHCPTypeDropdownValue=getwebelement("//div[text()='"+
					// DHCPIPV6_DHCPType +"']");
					webDriver.findElement(By.xpath("//div[text()='" + DHCPIPV6_DHCPType + "']")).click();
					// Clickon(SelectDHCPIPV6_DHCPTypeDropdownValue);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield,
							"Domatiin Name textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield,
							DHCPIPV6_DomainName);

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield,
							"IPV6 Primary DNS Server textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield,
							DHCPIPV6_DomainName);

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield,
							"IPV6 Secondary DNS Server textfield");
					sendKeys(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield,
							DHCPIPV6_SecondaryDNSServer);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");

					// compareText(application, "Add DHCP IPV6 Servers on CPE
					// Success Message", "AddDHCPIPV6SuccessMessage", "DHCP
					// server successfully created.", xml);
					verifysuccessmessage("DHCP server successfully created.");
				}

				if (DHCPIPV6_DHCPType.contains("Statefull DHCP")) {

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");

					webDriver.findElement(By.xpath("//div[text()='" + DHCPIPV6_DHCPType + "']")).click();

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SubnetMask_WarningMessage,
							"Subnet Mask ");
					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_LANIPV6Subnet_WarningMessage,
							"LAN IPV6 Subnet ");
					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_WarningMessage,
							"Primary DNS Server ");

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SubnetMask_Textfield,
							"IPV6 Subnet Mask textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SubnetMask_Textfield,
							DHCPIPV6_SubnetMask);

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_LANIPV6Subnet_Textfield,
							"LAN IPV6 Subnet textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_LANIPV6Subnet_Textfield,
							DHCPIPV6_LANIPV6Subnet);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield,
							"Domatiin Name textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield,
							DHCPIPV6_DomainName);

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield,
							"IPV6 Primary DNS Server textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield,
							DHCPIPV6_PrimaryDNSServer);

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield,
							"IPV6 Secondary DNS Server textfield");
					sendKeys(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield,
							DHCPIPV6_SecondaryDNSServer);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "Ok Button");

					// compareText(application, "Add DHCP IPV6 Servers on CPE
					// Success Message", "AddDHCPIPV6SuccessMessage", "DHCP
					// server successfully created.", xml);
					verifysuccessmessage("DHCP server successfully created.");
				}

			} else {
				Report.LogInfo("INFO", "Add DHCP IPV6 Servers on CPE Header/popup not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Add DHCP IPV6 Servers on CPE Header/popup not displayed");
			}

		} else {
			Report.LogInfo("INFO", "DHCP IPV6 Servers on CPE Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DHCP IPV6 Servers on CPE Panel not displayed");
		}

	}

	// Edit DHCP IPV6 Servers on CPE
	public void editDHCPIPV6ServersonCPEFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String DHCPIPV6_DomainName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDHCPIPV6_DomainName");
		String DHCPIPV6_DHCPType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDHCPIPV6_DHCPType");
		String DHCPIPV6_PrimaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDHCPIPV6_PrimaryDNSServer");
		String DHCPIPV6_SecondaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDHCPIPV6_SecondaryDNSServer");
		String DHCPIPV6_SubnetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDHCPIPV6_SubnetMask");
		String DHCPIPV6_LANIPV6Subnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditDHCPIPV6_LANIPV6Subnet");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_panelHeader)) {
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPIPV6Checkbox);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPIPV6Checkbox,
					"Select DHCP IPV6 Checkbox");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPIPV6Checkbox,
					"Select DHCP IPV6 Checkbox");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Editink, "Edit Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Editink, "Edit Link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.EditDHCPIPV6_Header)) {
				if (DHCPIPV6_DHCPType.contains("Stateless DHCP")) {
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");

					webDriver.findElement(By.xpath("//div[text()='" + DHCPIPV6_DHCPType + "']")).click();

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield,
							"Domatiin Name textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield,
							DHCPIPV6_DomainName);

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield,
							"IPV6 Primary DNS Server textfield");
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield,
							DHCPIPV6_PrimaryDNSServer);

					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield,
							"IPV6 Secondary DNS Server textfield");
					sendKeys(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield,
							DHCPIPV6_SecondaryDNSServer);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");

					verifysuccessmessage("DHCP server successfully updated.");
				}

				if (DHCPIPV6_DHCPType.contains("Statefull DHCP")) {
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_IPV6DHCPType_Dropdown,
							"DHCP Type");

					webDriver.findElement(By.xpath("//div[text()='" + DHCPIPV6_DHCPType + "']")).click();

					clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SubnetMask_Textfield);
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SubnetMask_Textfield,
							DHCPIPV6_SubnetMask);

					clearTextBox(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_LANIPV6Subnet_Textfield);
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_LANIPV6Subnet_Textfield,
							DHCPIPV6_LANIPV6Subnet);

					clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield);
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DomainName_Textfield,
							DHCPIPV6_DomainName);

					clearTextBox(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield);
					sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_PrimaryDNSServer_Textfield,
							DHCPIPV6_PrimaryDNSServer);

					clearTextBox(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield);
					sendKeys(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_SecondaryDNSServer_Textfield,
							DHCPIPV6_SecondaryDNSServer);

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.OKButton_common, "OK Button");

					verifysuccessmessage("DHCP server successfully updated.");
				}

			} else {
				Report.LogInfo("INFO", "Edit DHCP IPV6 Servers on CPE Header/popup not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Edit DHCP IPV6 Servers on CPE Header/popup not displayed");
			}

		} else {
			Report.LogInfo("INFO", "DHCP IPV6 Servers on CPE Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DHCP IPV6 Servers on CPE Panel not displayed");
		}

	}

	// Delete DHCP IPV6 Servers on CPE
	public void deleteDHCPIPV6ServersonCPEFunction_CPE() throws InterruptedException, IOException {

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCP_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPIPV6Checkbox,
					"Select DHCP IPV6 Checkbox");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.SelectDHCPIPV6Checkbox,
					"Select DHCP IPV6 Checkbox");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Actionbutton, "Action");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_Actionbutton, "Action");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DeleteLink, "Delete link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DHCPIPV6_DeleteLink, "Delete link");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.delete_alertpopup)) {
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DeleteButton_common, "Delete");

			} else {
				Report.LogInfo("INFO", "Step : Delete alert popup is not displayed", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
			}
			scrollUp();
			verifysuccessmessage("DHCP server successfully deleted.");
		} else {
			Report.LogInfo("INFO", "DHCP IPV6 Servers on CPE panel header not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "DHCP IPV6 Servers on CPE panel header not displayed");
		}

	}

	public void VerifyConfigurationPanel_CPE() throws InterruptedException, IOException {
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Configuration_panelHeader);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Configuration_panelHeader)) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ConfigurationTextfield,
					"Configuration textfield");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.Configuration_panelHeader,
					"Configuration panel");
			//verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.DiscoveryHistoryLogs_panelHeader,
					//"Discovery History Logs panel");

		} else {
			Report.LogInfo("INFO", "Configuration Panel not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Configuration Panel not displayed");
		}
	}

	public void veriyFetchDeviceInterfacesFunction_CPE(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws Exception {
		
		String CPE_ServiceStatusChangeRequired = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ServiceStatusChangeRequired");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header);

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header)) {
			waitForAjax();
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_ActionLink, "ACTION link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_ActionLink, "ACTION link");

			verifyExists(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_Action_FetchDeviceInterfacesLink,
					"Fetch Device Interfaces Link");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_View_Action_FetchDeviceInterfacesLink,
					"Fetch Device Interfaces Link");

			if (isVisible(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_FetchDeviceInterfacesSuccessMessage)) {
				Report.LogInfo("INFO",
						"Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ",
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ");

				javaScriptclick(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_hereLink_UnderFetchDeviceInterfacesSuccessMessage);

				if (isVisible(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ManageCOLTsNetworkManageNetwork_header)) {
					Report.LogInfo("INFO", "'Manage COLT's Network - Manage Network' page navigated as expected",
							"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Manage COLT's Network - Manage Network' page navigated as expected");

					scrollDown(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Synchronization_SynchronizeLink);
					compareText("Manage COLT's Network - Manage Network header",
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ManageCOLTsNetworkManageNetwork_header,
							"Manage COLT's Network - Manage Network");

					scrollDown(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Status_LastModificationValue);
					String CPE_Manage_Status_LastModificationValue = getTextFrom(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Status_LastModificationValue,"CPE_Manage_Status_Last Modification Value");

					if (CPE_Manage_Status_LastModificationValue.contains("GMT")) {
						Report.LogInfo("INFO",
								"Service status is displayed as : " + CPE_Manage_Status_LastModificationValue, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Service status is displayed as : " + CPE_Manage_Status_LastModificationValue);
					} else {
						Report.LogInfo("INFO", "Incorrect modification time format", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Incorrect modification time format");
					}
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Status_StatusLink,
							"Status");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Status_StatusLink,
							"Status");

					if (CPE_ServiceStatusChangeRequired == "Yes") {
						if (isVisible(
								APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Status_Servicestatus_popup)) {
							scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Device_Status_OK);
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Device_Status_OK,
									"OK");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Device_Status_OK, "OK");

							// WebElement PE_servicestatushistoryValue=
							// getwebelement(xml.getlocator("//locators/" +
							// application + "/CPE_servicestatushistoryValue"));

							if (isVisible(
									APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_servicestatushistoryValue)) {
								Report.LogInfo("INFO", "Step : Service status change request logged", "PASS");
								ExtentTestManager.getTest().log(LogStatus.PASS,
										"Step : Service status change request logged");
							} else {
								Report.LogInfo("INFO", "Step : Service status change request is not logged", "PASS");
								ExtentTestManager.getTest().log(LogStatus.PASS,
										"Step : Service status change request is not logged");
							}

						} else
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Status link is not working");
						Report.LogInfo("INFO", "Status link is not working", "FAIL");

					} else {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service status change not reqired");
						Report.LogInfo("INFO", "Step : Service status change not reqired", "PASS");
					}

					//// synchronize panel in manage colt page
					scrollDown(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Synchronization_SynchronizeLink);
					verifyExists(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Synchronization_SynchronizeLink,
							"Synchronize");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_Manage_Synchronization_SynchronizeLink,
							"Synchronize");

					verifysuccessmessage("Sync started successfully. Please check the sync status of this device.");

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"'Manage COLT's Network - Manage Network' page not navigated");
					Report.LogInfo("INFO", "'Manage COLT's Network - Manage Network' page not navigated", "FAIL");
				}

			} else {
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ");
				Report.LogInfo("INFO",
						"Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ",
						"PASS");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'View CPE Device' page not navigated");
			Report.LogInfo("INFO", "'View CPE Device' page not navigated", "FAIL");
		}

	}
	
	public void verifyAddInterface_CPEDevice(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_InterfaceName");
		String CPE_IPv4LANSUBNET_A = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_IPv4LANSUBNET_A");
		String CPE_IPv4LANSUBNET_B = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_IPv4LANSUBNET_B");
		String getaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_GetAddress_Button");
		String interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_InterfaceAddressRange_Value");
		String ipv6_getaddress_button = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_IPV6_GetAddressButton");
		String CPE_IPv6LANSUBNET_A = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_IPv6LANSUBNET_A");
		String CPE_IPv6LANSUBNET_B = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_IPv6LANSUBNET_B");
		String CPE_EthernetCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_EthernetCheckbox");
		String CPE_InterfaceIsLANCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_InterfaceIsLANCheckbox");
		String CPE_Speed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_Speed");
		String CPE_Duplex = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_Duplex");
		String CPE_ClockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ClockSource");
		String CPE_InterfaceIsWANCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_InterfaceIsWANCheckbox");
		String CPE_VRRP_V2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VRRP_V2");
		String CPE_VRRP_V3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VRRP_V3");
		String CPE_VRRPGroup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VRRPGroup");
		String CPE_VRRPIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VRRPIP");
		String IVManagementCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"IVManagementCheckbox");
		String CPE_VRRPIPV6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VRRPIPV6");
		String CPE_VRRPPriority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VRRPPriority");

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ProviderEquipment_header);
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.existingCPEdevicegrid)) 
			{
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_viewdevice1,
					"View");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_viewdevice1,
					"View");
				waitforPagetobeenable();
				
				scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header);
				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header)) 
				{
					Report.LogInfo("INFO", "'View CPE Device' Page navigated as expected", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'View CPE Device' Page navigated as expected");

					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PE_CPE_Router_IPV4CommandsDropdown);

					compareText("Interfaces header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaces_header, "Interfaces");
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacepanel_actiondropdown,
							"Action dropdown");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacepanel_actiondropdown,
							"Action dropdown");
					
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addinterface_link,
							"Add Interface/Link");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addinterface_link,
							"Add Interface/Link");
				
					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_AddInterfaceHeader);

					
					if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_AddInterfaceHeader)) {
						Report.LogInfo("INFO", "'Add Interface' page navigated as expected", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Interface' page navigated as expected");

						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton);
						
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"Ok");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"OK");
											
						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interface_warngmsg);						//verify warning messages in add interface page
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interface_warngmsg,"Interface warning message");

						//Add Interface
						sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacename_textfield,interfacename);
						if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv4LANSUBNET_A)){
						sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv4LANSUBNET_A,CPE_IPv4LANSUBNET_A);
						}
						if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv4LANSUBNET_B)){

						sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv4LANSUBNET_B,CPE_IPv4LANSUBNET_B);
						}//GetText(application, "Network", "network_fieldvalue");

						//verify EIP Allocation
						
						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation1_button);
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation1_button,
								"EIP Allocation button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation1_button,
								"EIP Allocation button");
						if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocation_header)) {
							compareText("EIP Subnet Allocation", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocation_header, "EIP Subnet Allocation");
								
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol,
									"Close");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol,
									"Close");
						}
						else
						{
							Report.LogInfo("INFO", "EIP Allocation button is not working", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL, "EIP Allocation button is not working");
						}
						
						//verify getaddress
						if(getaddress.equalsIgnoreCase("yes"))
						{
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress1_button,
									"Get Address");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress1_button,
									"Get Address");
							if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown)) {

								SelectDropdownValueUnderSelectTag("Interface Address Range", interfaceaddressrange_value, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown);
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
							}
							else
							{
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrange_textfield,
									"Interface Address Range");
								sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrange_textfield,interfaceaddressrange_value);
								
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
							}
						}

						//verify EIP Allocation for IPV6
						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.network_fieldvalue);
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation2_button,
								"EIP Allocation button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation2_button,
								"EIP Allocation button");
						if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocationIPv6_header)) {

							compareText("EIP Address Allocation header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocationIPv6_header, "EIP Address Allocation For Interface ");
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocationIPv6_CloseButton,
									"Close Button");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocationIPv6_CloseButton,
									"Close Button");
						}
						else
						{
							Report.LogInfo("INFO", "EIP Allocation button is not working", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL, "EIP Allocation button is not working");
						}
					
						//verify getaddress ipv6
						if(ipv6_getaddress_button.equalsIgnoreCase("yes"))
						{
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress2_button,
									"Get Address");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress2_button,
									"Get Address");

							if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_AddressIpv6_dropdown)) {

								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
							}
							else
							{
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
							}
						}
						else
						{
							Report.LogInfo("INFO", "Available Blocks dropdown values are not displaying", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Available Blocks dropdown values are not displaying");
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_popupclose,
									"Close");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_popupclose,
									"Close");
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrangeIPv6_textfield,
									"Interface Address Range IPv6");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrangeIPv6_textfield,interfaceaddressrange_value);
							//addtextFields_commonMethod(application, "Interface Address Range IPv6", "interfaceaddressrangeIPv6_textfield", interfaceaddressrange_value, xml);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
						}
						
						if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv6LANSUBNET_A)){
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv6LANSUBNET_A,CPE_IPv6LANSUBNET_A);

						}
						
						if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv6LANSUBNET_B)){
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_IPv6LANSUBNET_B,CPE_IPv6LANSUBNET_B);
						}

						if(CPE_EthernetCheckbox.equalsIgnoreCase("Yes")) {
							addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EthernetCheckbox, "Ethernet", CPE_EthernetCheckbox);
							SelectDropdownValueUnderSelectTag("Speed", CPE_Speed, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SpeedDropdown);
							SelectDropdownValueUnderSelectTag("Duplex", CPE_Duplex, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_DuplexDropdown);
						}else {
							SelectDropdownValueUnderSelectTag("Duplex", CPE_ClockSource, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ClockSourceDropdown);
						}

						//scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRP_V2Checkbox);
						
						if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRP_V2Checkbox)){
						addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRP_V2Checkbox, "V2", CPE_VRRP_V2);
						}
						
						if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRP_V3Checkbox)){
						addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRP_V3Checkbox, "V3", CPE_VRRP_V3);
						}
						
						if(CPE_InterfaceIsLANCheckbox.equalsIgnoreCase("Yes") && CPE_InterfaceIsWANCheckbox.equalsIgnoreCase("No")) {
							addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.IVManagementCheckbox, "IV Management", IVManagementCheckbox);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPGroupTextfield,
									"VRRP Group");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPGroupTextfield,CPE_VRRPGroup);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPTextfield,
									"VRRP IP");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPTextfield,CPE_VRRPIP);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPV6Textfield,
									"VRRP IPV6");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPV6Textfield,CPE_VRRPIPV6);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPriorityTextfield,
									"VRRP Priority");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPriorityTextfield,CPE_VRRPPriority);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_GroupIPIPV6ForwardArrow,
									"Group Forward button");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_GroupIPIPV6ForwardArrow,
									"Group Forward button");
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPasswordTextfield,
									"VRRP Password");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPasswordTextfield,interfacename);
						
						}else if (CPE_InterfaceIsLANCheckbox.equalsIgnoreCase("No") && CPE_InterfaceIsWANCheckbox.equalsIgnoreCase("Yes")) {
							addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_InterfaceIsWANCheckbox, "WAN", CPE_InterfaceIsWANCheckbox);
							addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.IVManagementCheckbox, "IV Management", IVManagementCheckbox);
						}

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"OK");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"OK");
						verifysuccessmessage("Interface successfully created.");

					}else {
						Report.LogInfo("INFO", "'Add Interface' page not navigated", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add Interface' page not navigated");
					}

				}else {
					Report.LogInfo("INFO", "'View CPE Device' page not navigated", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'View CPE Device' page not navigated");
				}

			}else{
				Report.LogInfo("INFO", "No Device added in grid", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
			}

	}
	
	public void verifyAddMultilink_CPEDevice(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String getaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_GetAddress_Button");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_Name");
		String interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_InterfaceAddressRange_Value");
		String ipv6_getaddress_button = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_IPV6_GetAddressButton");
		String CPE_EthernetCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_EthernetCheckbox");
		String CPE_Speed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_Speed");
		String CPE_Duplex = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_Duplex");
		String CPE_ClockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_ClockSource");
		String CPE_InterfaceIsLANCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_InterfaceIsLANCheckbox");
		String IVManagementCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Multilink_IVManagementCheckbox");
		String CPE_InterfaceIsWANCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_InterfaceIsWANCheckbox");
		String CPE_VRRPGroup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_VRRPGroup");
		String CPE_VRRPIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_VRRPIP");
		String CPE_VRRPIPV6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_VRRPIPV6");
		String CPE_VRRPPriority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEMultilink_VRRPPriority");

		
		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ProviderEquipment_header);
		
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.existingCPEdevicegrid)) 
			{
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_viewdevice1,
					"View");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_viewdevice1,
					"View");
				waitforPagetobeenable();
				scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header);
				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ViewDevice_Header)){
					Report.LogInfo("INFO", "'View CPE Device' Page navigated as expected", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "'View CPE Device' Page navigated as expected");

					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PE_CPE_Router_IPV4CommandsDropdown);

					compareText("Interfaces header", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaces_header, "Interfaces");
					
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacepanel_actiondropdown,
							"Action dropdown");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacepanel_actiondropdown,
							"Action dropdown");
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addmultilink_link,
							"Add Multilink");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addmultilink_link,
							"Add Multilink");
					
					scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_AddMultilinkHeader);
					if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_AddMultilinkHeader)){
						Report.LogInfo("INFO", "'Add Multilink' page navigated as expected", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Multilink' page navigated as expected");

						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton);
						
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"OK");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"Ok");
						
						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interface_warngmsg);	
						//verify warning messages in add interface page
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interface_warngmsg, "Warning message interface");
						//warningMessage_commonMethod(application, "interface_warngmsg", "Multilink", xml);

						//Add Interface
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacename_textfield,"Interface");
						sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacename_textfield,interfacename);
						
						
						//addtextFields_commonMethod(application, "Interface", "interfacename_textfield", interfacename, xml);
						//GetText(application, "Network", "network_fieldvalue");

						//verify EIP Allocation
						
						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation1_button);
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation1_button,
								"EIP Allocation button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation1_button,
								"EIP Allocation button");
						if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocation_header)){

							//compareText("EIP Subnet Allocation", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocation_header, "EIP Subnet Allocation");
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol,
									"Close");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol,
									"Close");
						}
						else
						{
							Report.LogInfo("INFO", "EIP Allocation button is not working", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL, "EIP Allocation button is not working");
						}
						

						//verify getaddress
						if(getaddress.equalsIgnoreCase("yes"))
						{
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress1_button,
									"Get Address");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress1_button,
									"Get Address");
							if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown)){

								SelectDropdownValueUnderSelectTag("Interface Address Range", interfaceaddressrange_value, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown);
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
							}
							else
							{
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrange_textfield,"Interface Address Range");
								sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrange_textfield,interfaceaddressrange_value);
								
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							}
						}
						//verify EIP Allocation for IPV6
						scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.network_fieldvalue);
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation2_button,
								"EIP Allocation button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation2_button,
							"EIP Allocation button");
						if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocationIPv6_header)){
							
							//GetText(application, "EIP Address Allocation header", "CPE_EIPallocationIPv6_header");
							//GetText(application, "Subnet Type", "subnettype_value");
							//GetText(application, "Space Name", "eipallocation_spacename");//Below values are not listed as expected
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocationIPv6_CloseButton,
									"Close Button");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EIPallocationIPv6_CloseButton,
								"Close Button");	
						}
						else
						{
							Report.LogInfo("INFO", "EIP Allocation button is not working", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL, "EIP Allocation button is not working");
						}

						
						//verify getaddress ipv6
						if(ipv6_getaddress_button.equalsIgnoreCase("yes"))
						{
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress2_button,
									"Get Address");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress2_button,
								"Get Address");	
							
							if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_AddressIpv6_dropdown)){
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							}
							else
							{
								verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
								click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							}
						}
						else
						{
							Report.LogInfo("INFO", "Available Blocks dropdown values are not displaying", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Available Blocks dropdown values are not displaying");
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_popupclose,
									"Close");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_popupclose,
								"Close");
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrangeIPv6_textfield,"Interface Address Range IPv6");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrangeIPv6_textfield,interfaceaddressrange_value);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
								"Interface Address Range IPv6 Arrow");
						}

						//ScrolltoElement(application, "IVManagementCheckbox", xml);//CPE_EthernetCheckbox//IVManagementCheckbox
						if(CPE_EthernetCheckbox.equalsIgnoreCase("Yes")) {
							addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_EthernetCheckbox, "Ethernet", CPE_EthernetCheckbox);
							SelectDropdownValueUnderSelectTag("Speed", CPE_Speed, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SpeedDropdown);
							SelectDropdownValueUnderSelectTag("Duplex", CPE_Duplex, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_DuplexDropdown);
						}else {
							SelectDropdownValueUnderSelectTag("Clock Source", CPE_ClockSource, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_ClockSourceDropdown);
						}

						addCheckbox_commonMethod(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.IVManagementCheckbox, "IV Management", IVManagementCheckbox);
						if(CPE_InterfaceIsLANCheckbox.equalsIgnoreCase("Yes") && CPE_InterfaceIsWANCheckbox.equalsIgnoreCase("No")) {
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPGroupTextfield,"VRRP Group");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPGroupTextfield,CPE_VRRPGroup);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPTextfield,"VRRP IP");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPTextfield,CPE_VRRPIP);
						
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPV6Textfield,"VRRP IPV6");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPIPV6Textfield,CPE_VRRPIPV6);
						
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPriorityTextfield,"VRRP Priority");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPriorityTextfield,CPE_VRRPPriority);
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_GroupIPIPV6ForwardArrow,
									"Group Forward button");
							click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_GroupIPIPV6ForwardArrow,
									"Group Forward button");
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPasswordTextfield,"VRRP Password");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VRRPPasswordTextfield,interfacename);
							
						}else if (CPE_InterfaceIsLANCheckbox.equalsIgnoreCase("No") && CPE_InterfaceIsWANCheckbox.equalsIgnoreCase("Yes")) {
							
							verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_InterfaceIsWANCheckbox,"WAN");
							sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_InterfaceIsWANCheckbox,CPE_InterfaceIsWANCheckbox);
				
						}
						
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"OK");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton,
								"OK");

						verifysuccessmessage("Multilink interface successfully created.");
					}else {
						Report.LogInfo("INFO", "'Add Multilink' page not navigated", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add Multilink' page not navigated");
					}


				}else {
					Report.LogInfo("INFO", "'View CPE Device' page not navigated", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'View CPE Device' page not navigated");
				}
			}else{
				Report.LogInfo("INFO", "No Device added in grid", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
			}

	}
	
	public void createorderservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderService");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.order_contractnumber_warngmsg, "order contract number");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.servicetype_warngmsg, "service type");

		if (neworder.equalsIgnoreCase("YES")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.newordertextfield, "new order textfield");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.newrfireqtextfield, "new rfireq textfield");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.createorderbutton, "create order button");
			click(APTIPAccessNoCPE.APTNoCPE.createorderbutton, "create order button");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.OrderCreatedSuccessMsg, "Order Created Success Msg");

		} else if (existingorderservice.equalsIgnoreCase("NO")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.selectorderswitch, "select order switch");
			click(APTIPAccessNoCPE.APTNoCPE.selectorderswitch, "select order switch");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown, "existing order drop down");

			click(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown, "existing order dropdown");
			// sendKeys(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,existingordernumber,"existingordernumber");

			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
					APTIPAccessNoCPE.APTNoCPE.existingorderdropdown, existingordernumber);
			waitForAjax();

		} else {
			Reporter.log("Order not selected");
		}

	}
	
	public void verifyServiceCreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException
	{
		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ServiceIdentification");
		String BillingType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "BillingType");
		String TerminationDate = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TerminationDate");
		String EmailService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EmailService");
		String PhoneService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneService");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");
		String ManageService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManageService");
		String RouterConfigurationViewIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv4");
		String RouterConfigurationViewIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv6");
		String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PerformanceReporting");
		String IPGuardian = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IPGuardian");
		String SNMPNotification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"SNMPNotification");
		String DeliveryChannel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DeliveryChannel");
		String RouterBasedFirewall = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterBasedFirewall");
		String TrapTargetAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TrapTargetAddress");
		String Qos = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Qos");
		String ExtendPErangetocustomerLAN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExtendPErangetocustomerLAN");
		String GeneratePassword = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"GeneratePassword");
		String BGPPassword = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPPassword");
		String BGPTableDistribution = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPTableDistribution");
		
		
		// if(isElementPresent(By.xpath("//div[text()='Create Order /
		// Service'])")))
		// {
		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.sidwarngmsg, "Service Identification");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_warngmsg, "Billing Type");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield, "new order textfield");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield, ServiceIdentification, "newordertextfield");

		Thread.sleep(15000);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, "Billing Type");
		selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, BillingType, "Billing Type");
		// click(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,"billingtype");

		// addDropdownValues_commonMethod("Billing
		// Type",APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,BillingType);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.terminationdate_field, "Terminate Date");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.terminationdate_field, TerminationDate, "Terminate Date");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, "Email Service");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, EmailService, "Email Service");

		//verifyExists(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield, "new order textfield");
		//sendKeys(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield, PhoneService, "newordertextfield");

		//verifyExists(APTIPAccessNoCPE.APTNoCPE.remarktextarea, "new order textfield");
		//sendKeys(APTIPAccessNoCPE.APTNoCPE.remarktextarea, Remarks, "newordertextfield");

		if (ManageService.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageServicecheckbox, "Manage Service checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.ManageServicecheckbox, "Manage Service checkbox");
		} else {
			Reporter.log("Not Manage Service checkbox");
		}
		if (RouterConfigurationViewIPv4.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv4checkbox,
					"Router Configuration View IPv4 checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv4checkbox, "Router Configuration View IPv4");
		} else {
			Reporter.log("Not Router Configuration View IPv4 checkbox ");
		}

		if (RouterConfigurationViewIPv6.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv6checkbox,
					"Router Configuration View IPv6 checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv6checkbox, "Router Configuration View IPv6");
		} else {
			Reporter.log("Not Router Configuration View IPv6 checkbox ");
		}
		if (PerformanceReporting.equalsIgnoreCase("Yes")) {
			//verifyExists(APTIPAccessNoCPE.APTNoCPE.performancereportingcheckbox, "performance reporting checkbox");
			//click(APTIPAccessNoCPE.APTNoCPE.performancereportingcheckbox, "performance reporting checkbox");
		} else {
			Reporter.log("Not performance reporting checkbox");
		}

		if (IPGuardian.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.IPGuardiancheckbox, "IP Guardian checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.IPGuardiancheckbox, "IP Guardian checkbox");
		} else {
			Reporter.log("Not IP Guardian checkbox");
		}
		if (SNMPNotification.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification checkbox");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, TrapTargetAddress);
		} else {
			Reporter.log("Not SNMP Notification checkbox");
		}

		verifyExists(APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown, "Delivery Channel");
		addDropdownValues_commonMethod("Delivery Channel", APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown,
				DeliveryChannel);

		if (RouterBasedFirewall.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterBasedFirewallcheckbox, "Router Based Firewall checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.RouterBasedFirewallcheckbox, "Router Based Firewall checkbox");
		} else {
			Reporter.log("Not Router Based Firewall checkbox");
		}
		if (Qos.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox, "Qos checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox, "Qos checkbox");
		} else {
			Reporter.log("Not Qos checkbox");
		}
		if (ExtendPErangetocustomerLAN.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.ExtendPErangetocustomerLANcheckbox,
					"Extend PE rangeto customer LAN checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.ExtendPErangetocustomerLANcheckbox,
					"Extend PE rangeto customer LAN checkbox");
		} else {
			Reporter.log("Not Extend PE rangeto customer LAN checkbox");
		}
		
		if (GeneratePassword .equalsIgnoreCase("YES")) {
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.GeneratePasswordButton,
					"Generate Password button for BGP Password field");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.GeneratePasswordButton,
					"Generate Password button for BGP Password field");
			} else {
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.BGPPasswordtextfield,
						"BGP Password");	
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.BGPPasswordtextfield,BGPPassword);
			}
		
		//Load Shared Dropdown
		/*if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.LoadSharedDropdown)){
			SelectDropdownValueUnderSelectTag("Load Shared", LoadShared, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.LoadSharedDropdown);
		}*/
		
		//BGP Table Distribution Dropdown
		if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.BGPTableDistributionDropdown)){
			SelectDropdownValueUnderSelectTag("BGP Table Distribution", BGPTableDistribution, APT_IPAccessResilientConfigObj.IPAccessResilientConfig.BGPTableDistributionDropdown);
		}
	
	
		scrollDown(APTIPAccessNoCPE.APTNoCPE.Nextbutton_ServiceCreation);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.Nextbutton_ServiceCreation, "Nextbutton Service Creation");
		click(APTIPAccessNoCPE.APTNoCPE.Nextbutton_ServiceCreation, "Nextbutton Service Creation");

		String serviceCreated = "Service successfully created";
		String orderService = getTextFrom(APTIPAccessNoCPE.APTNoCPE.servicecreationmessage,"service creation message");
		serviceCreated.equalsIgnoreCase(orderService);
		Reporter.log("Service successfully created");

	}
	
	public void deleteDevice_CPE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, AWTException
	{
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_DeviceName");
		scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);
		  if(isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid))
		  {
			  webDriver.findElement(By.xpath("//tr//*[contains(.,'"+name+"')]//following-sibling::span//a[contains(.,'Delete')]")).click();					//Alert alert = webDriver.switchTo().alert();
					
				    Robot r = new Robot();
				    Thread.sleep(1000);
					r.keyPress(KeyEvent. VK_ENTER);
					Thread.sleep(1000);
					r.keyRelease(KeyEvent. VK_ENTER);
					Thread.sleep(1000);
					
					waitForAjax();
				    
					//verifyExists(APTIPAccessNoCPE.APTNoCPE.PEDevicedeletionsuccessmessage);
					Report.LogInfo("INFO", "CPE Device Deleted Successfully", "PASS");
					
					waitForAjax();
				}
		  else
			{
			  Reporter.log("No Device added in grid");
			}
	}
	
	// PI Ranges Panel
			public void addPIRangesFunction_CPE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
				
				String PIAddressRange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PIAddressRange");

				Report.LogInfo("INFO", "Verifying Add PI Ranges  Functionality", "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Add PI Ranges  Functionality");
				
				scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerReadonlySNMP_panelHeader);

				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRangesPanel_Header)) {
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_Action,"PI Range Action");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_Action,"PI Range Action");

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_AddLink,"PI Range Add Link");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_AddLink,"PI Range Add Link");

					if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddPIRanges_Header)) {

						compareText("Add PI Range", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddPIRanges_Header, "PI Ranges");
						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIAddressRangeTextfield,"PI Range Add Link");
						sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIAddressRangeTextfield,PIAddressRange,"PI Address Range");
					

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_OKButton,"OK Button");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_OKButton,"OK Button");

						verifysuccessmessage("PI Range successfully inserted.");

					} else {
						Report.LogInfo("INFO", "Add PI Ranges  panel header is not displaying", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Add PI Ranges  panel header is not displaying");
					}

				}

			}

			public void deletePIRangesFunction_CPE() throws InterruptedException, IOException {
				Report.LogInfo("INFO", "Verifying Delete PI Ranges Functionality", "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Delete PI Ranges Functionality");
				
				scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerReadonlySNMP_panelHeader);

				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRangesPanel_Header)) {
					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRangesCheckbox,"Select PI Ranges Checkbox");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRangesCheckbox,"Select PI Ranges Checkbox");

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_Action,"Action");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_Action,"Action");

					verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_DeleteLink,"Delete Link");
					click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_DeleteLink,"Delete Link");

					if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.delete_alertpopup)) {

						verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_DeleteButton,"Delete");
						click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.PIRanges_DeleteButton,"Delete");

					} else {
						Report.LogInfo("INFO", "Step : Delete alert popup is not displayed for SNMP", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed for SNMP");
					}
					scrollUp();
					verifysuccessmessage("PI Range successfully deleted.");

				} else {
					Report.LogInfo("INFO", "PI Range panel header is not displaying in View CPE Device page", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"PI Range panel header is not displaying in View CPE Device page");
				}
			}


}
