package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSMergeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberDeactivationObj;
import pageObjects.nmtsObjects.NMTSNumberReservationObj;
import pageObjects.nmtsObjects.NMTSPortOutNumberObj;
import pageObjects.nmtsObjects.NMTSPort_IN_NumberObj;
import pageObjects.nmtsObjects.NMTSReturnTransferNumbersObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSReturn_Transfer_Numbers extends SeleniumUtils {
	
	NMTSNumberReservationPage NumberReservation = new NMTSNumberReservationPage();
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSPort_IN_Number PortIn = new NMTSPort_IN_Number();
	private static HashMap<String, String> Number= new HashMap<String, String>();
	
	public void ReturnTransferNumberNMTS(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue(testDataFile,sheetName, scriptNo, dataSetNo,"Country");
		String ColtOrHostedNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt/Hosted_Number");
		String ReturnAvailability = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String Status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_Status/Status");
	//	String RangeStart = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeStart");
		String ProcessingDecision = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Processing_Decision");
		String Operator = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OPERATOR");
		
	
		String AreaName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Area_Code");
		String AreaCodeExtention = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NMTS_Area_Code_Extension");
		String RangeFrom = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeStart");
		String RangeTo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeEnd");
		
			
		NumberReservation.UserGroupforPhnNoMgn(testDataFile, sheetName, scriptNo, dataSetNo);
		
		
		waitForAjax(); 
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMenu");
		waitForAjax(); 
		
		if(Country.toString().equals("CH")) 
		{
			//WaitForAjax();
			ScrollIntoViewByString(NMTSReturnTransferNumbersObj.ReturnTransferNumber.portoutforCH);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.portoutforCH,"portout for CH");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.portoutforCH,"portout for CH");
			
		}else {
		//waitForAjax();
		ScrollIntoViewByString(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransfer);
		verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransfer,"portout NMTS");
		javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransfer,"portout NMTS");
		}
		waitForAjax(); 
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMain");
		waitForAjax(); 
		if(Country.toString().equals("CH")) 
		{
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.wholesaleradiobtn,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.wholesaleradiobtn,"wholesaleradiobtn");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.wholesaleradiobtn,"wholesaleradiobtn");
			WaitForAjax();
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnTransradiobtn,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnTransradiobtn,"wholesaleradiobtn");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnTransradiobtn,"wholesaleradiobtn");
			WaitForAjax();
		}
		//WaitForAjax();
		if(Country.toString().equals("IT") || Country.toString().equals("FR"))
		{
			
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.sELECThOSTEDnO,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.sELECThOSTEDnO,"Colt/Hosted no");
			//javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.sELECThOSTEDnO,"Colt/Hosted no");
			Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.sELECThOSTEDnO, ColtOrHostedNo);
			waitForAjax();
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnAvailabilityIT,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnAvailabilityIT,"Return AvailabilityIT");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnAvailabilityIT,"Return Availability_IT");
			Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnAvailabilityIT, ReturnAvailability);
		}
		//WaitForAjax();
		if(Country.toString().equals("IT") || Country.toString().equals("CH")|| Country.toString().equals("DE NON-GEO"))
			
		{
			Report.LogInfo("Log Report",":","This field is disble for Country - CH");	
			
		}else{
			WaitForAjax();
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnAvailability,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnAvailability,"Return AvailabilityIT");
		    Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ReturnAvailability, ReturnAvailability);
		}
	
		//WaitForAjax();
		waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodePrefix,15);
		verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodePrefix,"AreaCodePrefix");
		sendKeys(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodePrefix, AreaName, "Area Name");
		//Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodePrefix, AreaName);
		
		waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodeExt,15);
		verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodeExt,"AreaCodeExt");
		sendKeys(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodeExt, AreaCodeExtention, "Area Code Extention");
		//Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.AreaCodeExt, AreaCodeExtention);
		
		waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferStatus,15);
		verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferStatus,"Status");
		//sendKeys(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferStatus, Status, "Status");
		Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferStatus,Status );
		
		waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.RangeFrom,15);
		verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.RangeFrom,"RangeFrom");
		sendKeys(NMTSReturnTransferNumbersObj.ReturnTransferNumber.RangeFrom, RangeFrom, "Range From");
		//Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.RangeFrom, RangeFrom);
		
		waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferRangeto,15);
		verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferRangeto,"retandtransferRangeto");
		sendKeys(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferRangeto, RangeTo, "Range to"); 
		//Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.retandtransferRangeto, RangeTo);
		
		waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.Submitbtn,15);
		verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.Submitbtn,"Submitbtn");
		javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.Submitbtn,"Submitbtn");
		WaitForAjax();
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMain");
		//waitForAjax(); 
		
		if(isElementPresent(By.xpath("//table[@id='ucxRange_grdView']//tr[contains(@ondblclick,'hdnIndex')]"))) {
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.checkbox3,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.checkbox3,"checkbox3");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.checkbox3,"checkbox3");
			
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.Selectmark,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.Selectmark,"Select mark");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.Selectmark,"Select mark");
			WaitForAjax();
			
			String page = getTextFrom(NMTSReturnTransferNumbersObj.ReturnTransferNumber.totalpage, "totalpage");
            int len = Integer.parseInt(page);
                   
            if(isElementPresent(By.xpath(("//table[@id='ucxSelectRange_grdView']//td[text()='RangeFrom']/..//td[9]/input").replace("RangeFrom",RangeFrom.toString()))))
           {
            verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.checkforreturn.replace("RangeFrom",RangeFrom),"check for return");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.checkforreturn.replace("RangeFrom",RangeFrom),"check for return");
			//WaitForAjax();
           }
        else {
				if (len > 1) {
					waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.lastbtn,15);
					verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.lastbtn,"last btn");
					javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.lastbtn,"last btn");
				}

				WaitForAjax();
                verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.checkforreturn.replace("RangeFrom",RangeFrom),"check for return");
				javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.checkforreturn.replace("RangeFrom",RangeFrom),"check for return");
				WaitForAjax();
			}
			waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ProcessingDecession,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ProcessingDecession,"Processing Decession");
			Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.ProcessingDecession,ProcessingDecision );
		if (ProcessingDecision.toString().equals("Transferred"))
			{
		/*	if(Country.toString().equals("CH")|| Country.toString().equals("DE NON-GEO"))
			{   
				WaitForAjax();
				waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.SubmitStatus,15);
				verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.SubmitStatus,"Yes Btn");
				javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.SubmitStatus,"Yes Btn");
				
			}else{*/
			    waitForAjax();
			    waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.operator,15);
				verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.operator,"operator");
				Reusable.Select(NMTSReturnTransferNumbersObj.ReturnTransferNumber.operator,Operator );
			//}	
			}
		    // waitForAjax();
	  if(isElementPresent(By.xpath("//input[@id='btnYes']")))	
		{
		    waitForElementToBeVisible(NMTSReturnTransferNumbersObj.ReturnTransferNumber.YesBtn,15);
			verifyExists(NMTSReturnTransferNumbersObj.ReturnTransferNumber.YesBtn,"Yes Btn");
			javaScriptclick(NMTSReturnTransferNumbersObj.ReturnTransferNumber.YesBtn,"Yes Btn");	
		}
			else 
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "please enter the valid input");
			}
			
			//isAlertPresent();
	  waitForAjax();
	  String Number_Transfer_Return = getTextFrom(NMTSReturnTransferNumbersObj.ReturnTransferNumber.SuccessMsg, "Success Msg");
	  
      
	  if(Number_Transfer_Return.equals("Numbers Transferred."))
		{
		  ExtentTestManager.getTest().log(LogStatus.PASS, "Numbers Transferred successfuly");
			
		}else if(Number_Transfer_Return.equals("Numbers Returned.")||Number_Transfer_Return.equals("Numbers Return-Pending."))
		{
		  ExtentTestManager.getTest().log(LogStatus.PASS, "Numbers Returned successfuly");
		}
		else{					
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Numbers Not Transferred/Returned successfuly");		
		}
              }
		else 
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Record Found please enter the valid data");
		}
	}
}



		
		
		
		     
		

	

	
	
	
	
	
	
	
	
	
	
	
	
	
 

	
	
	
	

