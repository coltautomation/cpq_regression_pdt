package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NOD_NHTransactionScreenObj {

	
	public static class NumberDetails
	{ 
		public static final String SearchNumberFldLbl="@xpath=//*[contains(text(),'Search for a number')]"; 
		public static final String SearchNumberFld="@xpath=//*[@formcontrolname='searchNumber']"; 
		public static final String SearchIcon="@xpath=//*[@class='input-group-append']//*[@type='submit']"; 

		public static final String NumberStatus="@xpath=//*[@Class='ct-lozns-buttons ct-newlozns ct-lozns-btn-teal']"; 
		public static final String NumbeHistorySection="@xpath=//div[@id='CompletedcollapseStepper']"; 

		public static final String PortingDetailsLabel="@xpath=//*[contains(text(),'Porting Details')]"; 
		public static final String PortingDateLabel="@xpath=//label[contains(text(),'Porting Date')]"; 
		public static final String PortingDate="@xpath=//label[contains(text(),'Porting Date')]//following-sibling::div"; 

		public static final String PortingWindoeLabel="@xpath=//label[contains(text(),'Porting Window')]"; 
		public static final String PortingWindowDate="@xpath=//label[contains(text(),'Porting Window')]//following-sibling::div"; 

		public static final String ActivationOption="@xpath=//label[contains(text(),'Activation')]"; 
		public static final String ActivationPage="@xpath=//ng-containter[contains(text(),'Activation')]"; 

		public static final String CustNameErr="@xpath=//div[contains(text(),'Customer Name is required')]"; 
		public static final String ValidateAddressbtn="@xpath=//button[contains(text(),'Validate Address')]"; 

		public static final String StreetNameErr="@xpath=//div[contains(text(),'Street Name is required')]"; 
		public static final String CityNameErr="@xpath=//div[contains(text(),'City / Town is required')]"; 
		public static final String PostalCodeErr="@xpath=//div[contains(text(),'Postal Code is required')]"; 

		public static final String OrderIdLbl="@xpath=//label[contains(text(),'Order ID')]"; 
		public static final String OrderId="@xpath=//label[contains(text(),'Order ID')]//following-sibling::p"; 

		public static final String OrderTypeLbl="@xpath=//label[contains(text(),'Order Type')]"; 
		public static final String OrderType="@xpath=//label[contains(text(),'Order Type')]//following-sibling::p"; 

		public static final String OrderStatusLbl="@xpath=//label[contains(text(),'Order Status')]"; 
		public static final String OrderStatus="@xpath=//label[contains(text(),'Order Status')]//following-sibling::p"; 

		public static final String UserNameLbl="@xpath=//label[contains(text(),'User Name')]"; 
		public static final String UserName="@xpath=//label[contains(text(),'User Name')]//following-sibling::p"; 

		public static final String OrderDateLbl="@xpath=//label[contains(text(),'Order Date')]"; 
		public static final String OrderDate="@xpath=//label[contains(text(),'Order Date')]//following-sibling::p"; 

		public static final String OrderDescriptionLbl="@xpath=//label[contains(text(),'Order Description')]"; 
		public static final String OrderDescription="@xpath=//label[contains(text(),'Order Description')]//following-sibling::p"; 

		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}