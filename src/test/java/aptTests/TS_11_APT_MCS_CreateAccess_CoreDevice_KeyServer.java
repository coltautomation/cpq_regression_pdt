package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_CreateAccessCoreDevice_ManageNetwork;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_MCS_CreateAccessCoreDevice_Keyserver;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_11_APT_MCS_CreateAccess_CoreDevice_KeyServer extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	APT_Login Login=new APT_Login();
	APT_MCS_CreateAccessCoreDevice_Keyserver Keyserver = new APT_MCS_CreateAccessCoreDevice_Keyserver();
	APT_CreateAccessCoreDevice_ManageNetwork MCS_ManageNetwork = new APT_CreateAccessCoreDevice_ManageNetwork();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void CreateAccessCoreDevice_KeyServer(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)throws IOException
	{	
		Report.createTestCaseReportHeader(ScenarioName);
	
	try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
	{

        // load a properties file
        prop.load(input);               

    }
	catch (IOException ex) 
	{
        ex.printStackTrace();
    }
	String dataFileName = prop.getProperty("TestDataSheet");
	File path = new File("./TestData/"+dataFileName);
	String testDataFile = path.toString();
	
	try
	{
		openurl(Configuration.APT_URL);
		Login.APT_VoiceService();
		Keyserver.navigatetomanagecoltnetwork();
		Keyserver.navigatetocreateaccesscoredevicepage();
		Keyserver.verifydevicecreation_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
		Keyserver.verifyDeviceCreationMessage();
		Keyserver.verifyenteredValue_forDeviceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);
		Keyserver.verifydeviceEdit_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
		Keyserver.verifyDeviceUpdationSuccessMessage();
		Keyserver.verifEditedValue_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
		Keyserver.testStatus();
		Keyserver.routerPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
		Keyserver.fetchDeviceInterface_viewdevicepage();
		MCS_ManageNetwork.verifyFetchInterface(testDataFile, dataSheet, scriptNo, dataSetNo); 
		Keyserver.verifydeviceDelete_Keyserver();
	}
	catch(Exception e)
	{
		Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
		ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		
	}
	
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
	ExtentTestManager.endTest();
	ExtentManager.getReporter().flush();
	WEB_DRIVER_THREAD_LOCAL.get().close();
	
	}
	
}	
