package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.exploreFunctions.ExploreLoginPage;

public class TS_16_Multi_Quote_Sales extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testMultiQuoteSales(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);			
					
			Opportunity.NavigateToQuotes(testDataFile,dataSheet ,scriptNo, dataSetNo);
			AddProduct.addQuoteInC4CForMulti(testDataFile, dataSheet ,scriptNo, dataSetNo);
			
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			
			String sResult;
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
				
				AddProduct.AddProductToQuote(ProductName);
//				calling the below method to enter the site address
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below method to configure the product
				sResult = AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);	
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
			} else {
//				Calling the below method to add the product in CPQ
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
			}
			
			AddProduct.multiQuoteProcess(testDataFile, dataSheet, scriptNo, dataSetNo,ProductName);
			
			String Multi_Quote_ID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Multi_Quote_ID");
			
//			Creating Opportunity
			CPQLogin.CPQLogout();
			close();
			openBrowser(g.getBrowser());
			openurl(Configuration.C4C_URL);
			CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String Opportunity_ID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Opportunity_ID");
			AddProduct.LinkQuoteWithOpportunity(Opportunity_ID, Multi_Quote_ID);	
			//AddProduct.saveCPQ("main");
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			            
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			
			AddProduct.verifyQuoteStage("Priced");
			AddProduct.addHubrefernce(ProductName);
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
			{
			String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
			Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
			} else {
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
			}
			
			//String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			//Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");

			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.logoutCPQ("Main");
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	

}
