package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSPort_IN_NumberObj {
	
	public static class PortIN
	{
		
		public static final String Port_IN_CH="@xpath=//font[contains(@onclick,'M=IN')]";
		public static final String PORT_INRadiobutton="@xpath=//input[@id='CHNumberTypeSelctorForPortINReclaim1_rdoWholesalePortIn']";
		public static final String Port_INTab="@xpath=//font[contains(@onclick,'M=PI')]";
		

		public static final String Switch="@xpath=//select[@id='cboSwitches']";
		public static final String AreacodePrefix="@xpath=//input[@id='btnArea']";
		public static final String iframe="@xpath=//iframe[@id='popupFrame']";
		public static final String AreaInformationWin="@xpath=//table[@id='grdView']//tr//td[text()='index']";
		public static final String Areacodeother="@xpath=//input[@id='txtNationalCode']";
		public static final String AreaCode="@xpath=//input[@id='txtAreaCode']";
		public static final String RangeStart="@xpath=//input[@id='txtRangeFrom']";
		public static final String RangeEnd="@xpath=//input[@id='txtRangeTo']";
		public static final String AddnumStatus="@xpath=//select[@id='cboStatus']";
		public static final String AddingAvailability="@xpath=//select[@id='cboAvailability']";
		public static final String AddingSubmit="@xpath=//input[@id='btnAddRangeSpec']";
		public static final String AddnumGeo="@xpath=//select[@id='cboGeo']";
		public static final String AddnumCtg="@xpath=//select[@id='cboGolden']";
		public static final String NRN="@xpath=//input[@id='txtNRN']";
		public static final String LosingCommProvider="@xpath=//select[@id='cboRangeOwner']";
		public static final String PortINActivateNumberMessage="@xpath=//*[@id='lblSuccessfulMsg']";
		public static final String PortInTelephoneNumber="@xpath=//input[@id='txtTelPhNo']";
		
		
		

	}

}