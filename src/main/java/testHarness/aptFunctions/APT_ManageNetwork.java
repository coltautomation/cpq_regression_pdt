package testHarness.aptFunctions;



import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.ManageNetwork_Obj;
import pageObjects.aptObjects.ManageNetwork_createAccessCOreDeviceObj;

public class APT_ManageNetwork extends SeleniumUtils
{
	Lanlink_NewTab NewTab = new Lanlink_NewTab();
	
	public void searchdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceName");
		verifyExists(ManageNetwork_Obj.ManageNetwork.managecoltnetworklink,"Manage Network Link");
		mouseMoveOn(ManageNetwork_Obj.ManageNetwork.managecoltnetworklink);
		waitForAjax();
		
		verifyExists(ManageNetwork_Obj.ManageNetwork.searchdevicelink, "Search device link");
		click(ManageNetwork_Obj.ManageNetwork.searchdevicelink,"Search device link");
		
		verifyExists(ManageNetwork_Obj.ManageNetwork.devicenamefield,"Device name");
		sendKeys(ManageNetwork_Obj.ManageNetwork.devicenamefield,devicename,"Device name");
		waitForAjax();
		
		verifyExists(ManageNetwork_Obj.ManageNetwork.searchbutton, "Search button");
		scrollDown(ManageNetwork_Obj.ManageNetwork.searchbutton);
		click(ManageNetwork_Obj.ManageNetwork.searchbutton,"Search button");
		waitToPageLoad();
		
		scrollDown(ManageNetwork_Obj.ManageNetwork.devicechkbox);
		verifyExists(ManageNetwork_Obj.ManageNetwork.devicechkbox, "Device checkbox");
		click(ManageNetwork_Obj.ManageNetwork.devicechkbox,"Device checkbox");
		waitForAjax();
		
		verifyExists(ManageNetwork_Obj.ManageNetwork.searchdevice_actiondropdown, "Search device action dropdown");
		click(ManageNetwork_Obj.ManageNetwork.searchdevice_actiondropdown,"searchdevice_actiondropdown");
		
		verifyExists(ManageNetwork_Obj.ManageNetwork.managelink, "Manage Link");
		click(ManageNetwork_Obj.ManageNetwork.managelink,"Manage link");
		waitForAjax();
		waitForAjax();
	}

	public static String InterfaceAddress;
	public void manageNetwork(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
	String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceName");
	String Inservice_status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InServiceStatus");
	String Inmaintenance_status =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InMaintenanceStatus");
	String vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
	String managementaddress =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManagementAddress");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpro");
	String country =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
	String city = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City");
	String site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site");
	String premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premise");
	String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterfaceName");
	String FMC_column =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FMC_column");
	String SMARTS_column= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SMARTS_column");
	String DCS_column =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DCS_column");
	String FetchInterfaces_column= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FetchInterfaces_column");
	String Vistamart_column=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Vistamart_column");
	String PGW_column=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PGW_column");
	
	
	try {	
		
		//System.out.println("Manage Network");
		//Manage Network
		
		compareText("Manage Network header",ManageNetwork_Obj.ManageNetwork.managenetwork_header,"Managenetwork Header");
		
		compareText("Status header", ManageNetwork_Obj.ManageNetwork.status_header, "Status");
		compareText("Synchronization header",ManageNetwork_Obj.ManageNetwork.synchronization_header, "Synchronization");
		
		//verify column headers
		compareText("Device column header",ManageNetwork_Obj.ManageNetwork.status_devicecolumn, "Device");
		compareText("Status column header",ManageNetwork_Obj.ManageNetwork.status_statuscloumn, "Status");
		compareText("Last Modification column header",ManageNetwork_Obj.ManageNetwork.status_lastmodificationcolumn, "Last Modification");
		compareText("Action column header",ManageNetwork_Obj.ManageNetwork.status_Action, "Action");
		compareText("Device column header",ManageNetwork_Obj.ManageNetwork.synchronization_devicecolumn, "Device");
		compareText("Sync Status column header",ManageNetwork_Obj.ManageNetwork.synchronization_syncstatuscolumn, "Sync Status");
		if(FMC_column.equalsIgnoreCase("Yes")) {
			compareText("FMC column header", ManageNetwork_Obj.ManageNetwork.synchronization_fmccolumn, "FMC");
		}
		else if(SMARTS_column.equalsIgnoreCase("Yes")) {
		compareText("Smarts column header", ManageNetwork_Obj.ManageNetwork.synchronization_smartscolumn, "Smarts");
		}
		else if(DCS_column.equalsIgnoreCase("Yes")) {
		compareText("DCS column header", ManageNetwork_Obj.ManageNetwork.synchronization_dcsdevice, "DCS");
		}
		else if(FetchInterfaces_column.equalsIgnoreCase("Yes")) {
		compareText( "Fetch Interfaces column header", ManageNetwork_Obj.ManageNetwork.synchronization_FetchInterfacescolumn, "Fetch Interfaces");
		}
		else if(Vistamart_column.equalsIgnoreCase("Yes")) {
			compareText("VistaMart Device column header", ManageNetwork_Obj.ManageNetwork.synchronization_vistamartdevicecolumn, "VistaMart Device");
			}
		else if(PGW_column.equalsIgnoreCase("Yes")) {
			compareText( "PGW column header",ManageNetwork_Obj.ManageNetwork.synchronization_pgwcolumn, "PGW");
			}
		compareText("Action column header", ManageNetwork_Obj.ManageNetwork.synchronization_actioncolumn, "Action");
		
		//verify Status panel column values
		compareText("Device", ManageNetwork_Obj.ManageNetwork.status_devicevalue, devicename);
		String ServiceStatus= getTextFrom(ManageNetwork_Obj.ManageNetwork.status_statusvalue,"Status");
		
		if(ServiceStatus.equalsIgnoreCase(Inservice_status))
		{
			compareText("Status",ManageNetwork_Obj.ManageNetwork.status_statusvalue, Inservice_status);
		}
		else
		{
			compareText("Status",ManageNetwork_Obj.ManageNetwork.status_statusvalue, Inmaintenance_status);
		}
		
		//verify last modification
		try {
		String GMTValue;
		String LastModificationvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.status_lastmodificationvalue,"status_last modification value");
		//System.out.println("Last modified date: " +LastModificationvalue);
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
		
		if (LastModificationvalue.length() > 3) 
		{
		    GMTValue = LastModificationvalue.substring(LastModificationvalue.length() - 3);
		} 
		else
		{
			GMTValue = LastModificationvalue;
		}
		//sa.assertEquals(GMTValue, "GMT");
				
		}catch(Exception e)
		{
			e.printStackTrace();
			//ExtentTestManager.getTest().log (LogStatus.FAIL, "Step : Last Modification column value field is not displaying");
		
			Reporter.log("'Fetch Interface' column do not display, if no values passed for 'Management Address'");
		}
		verifyExists(ManageNetwork_Obj.ManageNetwork.status_statuslink,"Status Link");
		click(ManageNetwork_Obj.ManageNetwork.status_statuslink,"Status Link");
		
		waitForAjax();
			compareText("Status page header",ManageNetwork_Obj.ManageNetwork.Statuspage_header, "Device");
			
			//verify field headers in status page
			compareText("Name field header",ManageNetwork_Obj.ManageNetwork.statuspage_nameheader, "Name");
			compareText("Vendor/Model field header",ManageNetwork_Obj.ManageNetwork.statuspage_vendormodelheader, "Vendor/Model");
			compareText("Management Address field header",ManageNetwork_Obj.ManageNetwork.statuspage_managementaddressheader, "Management Address");
			compareText("Snmpro field header",ManageNetwork_Obj.ManageNetwork.statuspage_snmproheader, "Snmpro");
			compareText("Country field header",ManageNetwork_Obj.ManageNetwork.statuspage_countryheader, "Country");
			compareText("City field header",ManageNetwork_Obj.ManageNetwork.statuspage_cityheader, "City");
			compareText("Site field header",ManageNetwork_Obj.ManageNetwork.statuspage_siteheader, "Site");
			compareText("Premise field header",ManageNetwork_Obj.ManageNetwork.statuspage_premiseheader, "Premise");
			
			//verify field values in status page
			compareText("Name",ManageNetwork_Obj.ManageNetwork.statuspage_namevalue, devicename);
			compareText("Vendor/Model",ManageNetwork_Obj.ManageNetwork.statuspage_vendormodelvalue, vendormodel);
			compareText("Management Address",ManageNetwork_Obj.ManageNetwork.statuspage_managementaddressvalue, managementaddress);
			compareText("Snmpro value", ManageNetwork_Obj.ManageNetwork.statuspage_snmprovalue, snmpro);
			compareText("Country", ManageNetwork_Obj.ManageNetwork.statuspage_countryvalue, country);
			compareText("City", ManageNetwork_Obj.ManageNetwork.statuspage_cityvalue,city);
			compareText("Site", ManageNetwork_Obj.ManageNetwork.statuspage_sitevalue,site);
			compareText("Premise", ManageNetwork_Obj.ManageNetwork.statuspage_premisevalue,premise);
		
		compareText("Status header",ManageNetwork_Obj.ManageNetwork.Statuspage_statusheader, "Status");
		compareText("Current Status field header",ManageNetwork_Obj.ManageNetwork.statuspage_currentstatusfieldheader, "Current Status");
		compareText("New Status field header",ManageNetwork_Obj.ManageNetwork.statuspage_newstatusfieldheader, "New Status");
		//compareText("Current Status",ManageNetwork_Obj.ManageNetwork.statuspage_currentstatusvalue, Inservice_status);
		getTextFrom(ManageNetwork_Obj.ManageNetwork.statuspage_currentstatusvalue,"statuspage_currentstatusvalue");
		verifyExists(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusdropdown,"New Status Dropdown");
		
		
		//WebElement selectNewStatusvalue= findWebElement(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusdropdownvalue);
		verifyExists(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusdropdownvalue,"New Status Dropdown Value");
		click(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusdropdownvalue,"New Status Dropdown Value");
		//selectByVisibleText(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusdropdown,Inmaintenance_status,"New Status Dropdown");
		//String NewStatusvalue= getwebelement(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusdropdownvalue).getText();
		String NewStatusvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusdropdownvalue,"statuspage_new status dropdown value");
		
		Reporter.log( "New Status Value is: "+NewStatusvalue);
		verifyExists(ManageNetwork_Obj.ManageNetwork.statuspage_okbutton,"Status Page Ok Button");
		click(ManageNetwork_Obj.ManageNetwork.statuspage_okbutton,"Status Page Ok Button");
		
		waitForAjax();
		scrollUp();
		
		compareText( "Device status update success message",ManageNetwork_Obj.ManageNetwork.Sync_successmsg, "Device Status history successfully changed");
		
		waitForAjax();
		//scrolltoend();
		scrollDown(ManageNetwork_Obj.ManageNetwork.statuspage_statuscolumnheader);
		//verify 'new status' table column headers
		compareText("Status column header",ManageNetwork_Obj.ManageNetwork.statuspage_statuscolumnheader, "Status");
		compareText("Changed On column header",ManageNetwork_Obj.ManageNetwork.statuspage_changedon_columnheader, "Changed On");
		compareText("Changed By column header", ManageNetwork_Obj.ManageNetwork.statuspage_changedby_columnheader, "Changed By");
		
		//verify 'new status' table column values
		//Device status history table
		int TotalPages;
		String TotalPagesText = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbTotal']")).getText();
		TotalPages = Integer.parseInt(TotalPagesText);
		//System.out.println("Total number of pages in table is: " + TotalPages);

		if (TotalPages != 0) {

			outerloop:
			for (int k = 1; k <= TotalPages; k++) {

				String AddedInterface= webDriver.findElement(By.xpath("//div[@role='grid']//div[@ref='eBodyViewport']/div")).getAttribute("style");
				if(!AddedInterface.contains("height: 1px"))
				{
				List<WebElement> results = webDriver.findElements(By.xpath("//div[@class='modal-content']//div[@class='ag-div-margin row']//div[@ref='eBodyContainer']//div[@role='row']"));
				int numofrows = results.size();
				

				if ((numofrows == 0)) {

				
					Reporter.log("Device Status History is empty");
				}
				else {
				// Current page
				String CurrentPage = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent']")).getText();
				int Current_page = Integer.parseInt(CurrentPage);
				
					for (int i = 0; i < numofrows; i++) {
						try {

							String Devicehistorydata = results.get(i).getText();
							
							if (Devicehistorydata.contains(NewStatusvalue)) 
							{
								//ExtentTestManager.getTest().log (LogStatus.PASS, "Device status history table has data");
								Reporter.log("Device status history table has data");
								waitForAjax();
								compareText("Status", ManageNetwork_Obj.ManageNetwork.statuspage_newstatusvalue, NewStatusvalue);
								
								try {
									String GMTValue;
									//String ChangedOnvalue= GetText(application, "Changed On", "statuspage_changedonvalue");
									String ChangedOnvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.statuspage_newstatusvalue,"statuspage_new status value");
									//System.out.println("Changed On value: " +ChangedOnvalue);
									SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
									
									if (ChangedOnvalue.length() > 3) 
									{
									    GMTValue = ChangedOnvalue.substring(ChangedOnvalue.length() - 3);
									} 
									else
									{
										GMTValue = ChangedOnvalue;
									}
									//sa.assertEquals(GMTValue, "GMT");
									//assertEquals( "GMT",GMTValue,"GMT Value verification");
											
									}catch(Exception e)
									{
										e.printStackTrace();
										Reporter.log("Step : Changed on column value field is not displaying");
									}
								
								scrollDown(ManageNetwork_Obj.ManageNetwork.statuspage_changedbyvalue);
							compareText("Changed By", ManageNetwork_Obj.ManageNetwork.statuspage_changedbyvalue, "colttest@colt.net");
								waitForAjax();
								
								verifyExists(ManageNetwork_Obj.ManageNetwork.statuspage_closebutton,"Status Close Button");
								click(ManageNetwork_Obj.ManageNetwork.statuspage_closebutton,"Status Close Button");
								break outerloop;
							}

						} catch (StaleElementReferenceException e) {
							
							e.printStackTrace();
						}
					}
				}
				}
				else
				{
					ExtentTestManager.getTest().log (LogStatus.FAIL, "No interfaces added");
					Reporter.log( "No interfaces added");
					
				}
			}
			
		}else {
			ExtentTestManager.getTest().log (LogStatus.FAIL, "No data available in table");
			Reporter.log("No data available in table");
		}
		
		//verify view interfaces page
		waitForAjax();
		verifyExists(ManageNetwork_Obj.ManageNetwork.status_viewinterfaceslink,"Click on Interface link");
		click(ManageNetwork_Obj.ManageNetwork.status_viewinterfaceslink,"Click on Interface link");
		waitForAjax();
		compareText("Interface page header", ManageNetwork_Obj.ManageNetwork.viewinterfacepage_header,"Interfaces");
		String AddedInterface= webDriver.findElement(By.xpath("//div[@role='grid']//div[@ref='eBodyViewport']/div")).getAttribute("style");
		if(!AddedInterface.contains("height: 1px"))
		{
		compareText("Device Name column header", ManageNetwork_Obj.ManageNetwork.viewinterface_devicenamecolumnheader, "Device Name");
		compareText("Interface Name column header", ManageNetwork_Obj.ManageNetwork.interfacename_columnheader, "Interface Name");
		compareText("Interface Address column header",ManageNetwork_Obj.ManageNetwork.interfaceaddress_columnheader, "Interface Address");
		verifyExists(ManageNetwork_Obj.ManageNetwork.InterfaceAddressRowValue,"InterfaceAddressRowValue");
		click(ManageNetwork_Obj.ManageNetwork.InterfaceAddressRowValue,"InterfaceAddressRowValue");
		//sendKeys(ManageNetwork_Obj.ManageNetwork.InterfaceAddressRowValue.Keys.TAB);
		SendkeyusingAction(Keys.TAB);
		
		compareText("Interface Type column header", ManageNetwork_Obj.ManageNetwork.interfacetype_columnheader, "Interface Type");
		
		//WebElement InterfaceTypeRowValue= findWebElement(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue);
		verifyExists(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
		click(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
		//sendKeys(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue, Keys.TAB);
		SendkeyusingAction(Keys.TAB);
		
		
		compareText("Status column header", ManageNetwork_Obj.ManageNetwork.viewinterface_status_columnheader, "Status");
		//WebElement StatusRowValue= findWebElement(ManageNetwork_Obj.ManageNetwork.viewinterface_status_rowvalue);
		verifyExists(ManageNetwork_Obj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
		click(ManageNetwork_Obj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
		//sendKeys(ManageNetwork_Obj.ManageNetwork.viewinterface_status_rowvalue,Keys.TAB);
		SendkeyusingAction(Keys.TAB);
		
		compareText("Last Modification column header",ManageNetwork_Obj.ManageNetwork.viewinterface_lastmod_columnheader, "Last Modification");
		
		waitForAjax();
		click(ManageNetwork_Obj.ManageNetwork.viewinterface_closebutton,"Status Close Button");
		waitForAjax();
		click(ManageNetwork_Obj.ManageNetwork.status_viewinterfaceslink,"Status Interface Link");
		waitForAjax();
		
		int TotalPages1;
		String TotalPagesText1 = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbTotal']")).getText();
		TotalPages1 = Integer.parseInt(TotalPagesText1);
	//	System.out.println("Total number of pages in table is: " + TotalPages1);

		if (TotalPages1 != 0) {

			outerloop:
			for (int k = 1; k <= TotalPages1; k++) {

				List<WebElement> results = webDriver.findElements(By.xpath("//div[@role='row']//div[@role='gridcell'][@col-id='name']"));

				int numofrows = results.size();
				

				if ((numofrows == 0)) {

					
					Reporter.log( "Interface table is empty");
				}
				else {
				// Current page
				String CurrentPage = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent']")).getText();
				int Current_page = Integer.parseInt(CurrentPage);
				

				
					for (int i = 0; i < numofrows; i++) {
						try {

							String AddedInterfacedata = results.get(i).getText();
							
							if (AddedInterfacedata.equalsIgnoreCase(interfacename)) {

								String InterfaceNameRowID= getAttributeFrom(ManageNetwork_Obj.ManageNetwork.InterfaceNameRowID1 +interfacename+ ManageNetwork_Obj.ManageNetwork.InterfaceNameRowID2,"row-id");
							
								
								//verify interface values in table
								String DeviceNamevalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.DeviceNamevalue1+InterfaceNameRowID+ManageNetwork_Obj.ManageNetwork.DeviceNamevalue2,"Device name");
								//ExtentTestManager.getTest().log (LogStatus.PASS, "Step: Interface Device Name value is displayed as : "+DeviceNamevalue);
								Reporter.log("Step: Interface Device Name value is displayed as : "+DeviceNamevalue);
								
								String InterfaceNamevalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.InterfaceNamevalue1+InterfaceNameRowID+ManageNetwork_Obj.ManageNetwork.InterfaceNamevalue2,"Interface name");
								Reporter.log( "Step: Interface Name value is displayed as : : "+InterfaceNamevalue);
								
								String InterfaceAddressvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.InterfaceAddressvalue1+InterfaceNameRowID+ManageNetwork_Obj.ManageNetwork.InterfaceAddressvalue2,"Interface name value");
								Reporter.log("Step: Interface Address value is displayed as : "+InterfaceAddressvalue);
								//WebElement InterfaceAddressRowValue1= driver.findElement(By.xpath("(//div[@role='gridcell'][@col-id='address'])[1]"));
								
								verifyExists(ManageNetwork_Obj.ManageNetwork.InterfaceAddressRowValue1,"InterfaceAddressRowValue1");
								click(ManageNetwork_Obj.ManageNetwork.InterfaceAddressRowValue1,"InterfaceAddressRowValue1");
								//sendKeys(ManageNetwork_Obj.ManageNetwork.Keys.TAB);
								SendkeyusingAction(Keys.TAB);
								
								String InterfaceTypevalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='type.desc']")).getText();
								
								Reporter.log( "Step: Interface Type value is displayed as : "+InterfaceTypevalue);
								//WebElement InterfaceTypeRowValue1= findWebElement(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue);
								
								verifyExists(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
								click(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
								//sendKeys(ManageNetwork_Obj.ManageNetwork.interfacetype_rowvalue.Keys.TAB);
								SendkeyusingAction(Keys.TAB);
								
								String Statusvalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='currentStatus.desc']")).getText();
								
								Reporter.log( "Step: Status value is displayed as : "+Statusvalue);
								//WebElement StatusRowValue1= getwebelement(xml.getlocator("//locators/" + application + "/viewinterface_status_rowvalue"));
								verifyExists(ManageNetwork_Obj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
								click(ManageNetwork_Obj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
								//sendKeys(ManageNetwork_Obj.ManageNetwork.viewinterface_status_rowvalue.Keys.TAB);
								SendkeyusingAction(Keys.TAB);
								
								String LastModificationvalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='m_time']")).getText();
								
								Reporter.log("Step: Last Modification value is displayed as : "+LastModificationvalue);
								//WebElement LastModificationRowValue= getwebelement(xml.getlocator("//locators/" + application + "/viewinterface_lastmod_rowvalue"));
								
								verifyExists(ManageNetwork_Obj.ManageNetwork.viewinterface_lastmod_rowvalue,"viewinterface_lastmod_rowvalue");
								click(ManageNetwork_Obj.ManageNetwork.viewinterface_lastmod_rowvalue,"viewinterface_lastmod_rowvalue");
								//sendKeys(ManageNetwork_Obj.ManageNetwork.viewinterface_lastmod_rowvalue.Keys.TAB);
								SendkeyusingAction(Keys.TAB);
								
								WebElement StatusLink= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='Status']/div/a"));
								verifyExists(ManageNetwork_Obj.ManageNetwork.StatusLink1+ InterfaceNameRowID +ManageNetwork_Obj.ManageNetwork.StatusLink2,"Status link");
								click(ManageNetwork_Obj.ManageNetwork.StatusLink1+ InterfaceNameRowID +ManageNetwork_Obj.ManageNetwork.StatusLink2,"Status link");
								
								Reporter.log("Step: Clicked on Status link in interface table");
								InterfaceAddress= InterfaceAddressvalue;
								break outerloop;
							}
							
						} catch (StaleElementReferenceException e) {
							
							e.printStackTrace();

						}
					}
					verifyExists(ManageNetwork_Obj.ManageNetwork.nextButton, "Next Button");
					click(ManageNetwork_Obj.ManageNetwork.nextButton, "Next Button");
					waitForAjax();
				}
			}
			
		//verify status page headers & field names
		compareText("Interface header", ManageNetwork_Obj.ManageNetwork.statuspage_interfaceheader, "Interface");
		compareText("Name field header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_namefield, "Name");
		compareText("Interface Address field header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_interfaceaddressfield, "Interface Address");
		compareText("Status header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_statusheader, "Status");
		compareText("Current Status field header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_currentstatusfield, "Current Status");
		compareText("New Status field header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusfield, "New Status");
		
		//verify status page field values
		compareText("Name", ManageNetwork_Obj.ManageNetwork.interface_statuspage_namevalue, interfacename);
		compareText("Interface Address", ManageNetwork_Obj.ManageNetwork.interface_statuspage_interfaceaddressvalue, InterfaceAddress);
		compareText("Current Status", ManageNetwork_Obj.ManageNetwork.interface_statuspage_currentstatusvalue, Inservice_status);
		verifyExists(ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusdropdown,"Interface Status page New Status Dropdown");
		click(ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusdropdown,"Interface Status page New Status Dropdown");
		//WebElement selectNewStatusvalue1= findWebElement(ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusdropdownvalue);
		verifyExists(ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusdropdownvalue,"interface_statuspage_newstatusdropdownvalue");
		click(ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusdropdownvalue,"interface_statuspage_newstatusdropdownvalue");
		String NewStatusvalue1= getTextFrom(ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusdropdownvalue,"interface_statuspage_new status dropdown value");
		//ExtentTestManager.getTest().log (LogStatus.PASS, "New Status Value is: "+NewStatusvalue1);
		Reporter.log( "New Status Value is: "+NewStatusvalue1);
		click(ManageNetwork_Obj.ManageNetwork.interface_statuspage_okbutton,"interface_statuspage_okbutton");
		waitForAjax();
		scrollUp();
		compareText("Interface status update success message", ManageNetwork_Obj.ManageNetwork.Sync_successmsg, "Interface Status History successfully changed.");
		waitForAjax();
		//scrolltoend();
		scrollDown(ManageNetwork_Obj.ManageNetwork.interface_statuspage_statuscolumnheader);
		waitForAjax();
		//verify 'new status' table column headers
		compareText("Status column header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_statuscolumnheader, "Status");
		compareText("Changed On column header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_changedon_columnheader, "Changed On");
		compareText("Changed By column header", ManageNetwork_Obj.ManageNetwork.interface_statuspage_changedby_columnheader, "Changed By");
		
		//verify 'new status' table column values
		//verify interface status history table
		int TotalPages2=0;
		String TotalPagesText2 = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbTotal'])[1]")).getText();
		TotalPages2 = Integer.parseInt(TotalPagesText2);
		

		if (TotalPages2 != 0) {

			outerloop:
			for (int k = 1; k <= TotalPages2; k++) {

				List<WebElement> results = webDriver.findElements(By.xpath("(//div[@ref='eBodyContainer'])[2]//div[@role='row']"));

				int numofrows = results.size();
			

				if ((numofrows == 0)) {

					//ExtentTestManager.getTest().log (LogStatus.PASS, "Interface Status History is empty");
					Reporter.log( "Interface Status History is empty");
				}
				else {
				// Current page
				String CurrentPage = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent'])[1]")).getText();
				int Current_page = Integer.parseInt(CurrentPage);
				

				
					for (int i = 0; i < numofrows; i++) {
						try {
							String Interfacehistorydata = results.get(i).getText();
						
							if (Interfacehistorydata.contains(NewStatusvalue1)) 
							{
								
								Reporter.log( "Interface status history table has data");
								waitForAjax();
								compareText("New Status", ManageNetwork_Obj.ManageNetwork.interface_statuspage_newstatusvalue, NewStatusvalue1);
								try {
									String GMTValue;
									String ChangedOnvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.interface_statuspage_changedonvalue,"interface_statuspage_changedon value");
									
									SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
									
									if (ChangedOnvalue.length() > 3)
									{
									    GMTValue = ChangedOnvalue.substring(ChangedOnvalue.length() - 3);
									} 
									else
									{
										GMTValue = ChangedOnvalue;
									}
									
											
									}catch(Exception e)
									{
										e.printStackTrace();
										
										Reporter.log( "Step : Changed On column value field is not displaying");
									}
								
							    compareText("Changed By", "interface_statuspage_changedbyvalue", "colttest@colt.net");
								waitForAjax();
								verifyExists(ManageNetwork_Obj.ManageNetwork.interface_statuspage_closebutton,"Status Page Close Button");
								click(ManageNetwork_Obj.ManageNetwork.interface_statuspage_closebutton,"Status Page Close Button");
								break outerloop;
							}

						} catch (StaleElementReferenceException e) {
					
							e.printStackTrace();

						}
					}
				}
			}
			
		}else {

			Reporter.log( "No data available in status history table");
		}
		}else {

			Reporter.log( "No data available in Interface table");
			
		}
		}else
		{
			Reporter.log("No Interface added in table");
			
		}
		
		verifyExists(ManageNetwork_Obj.ManageNetwork.viewinterface_closebutton,"Close Interface Popup");
		click(ManageNetwork_Obj.ManageNetwork.viewinterface_closebutton,"Close Interface Popup");
		waitForAjax();
		
		//verify synchronization panel column values
		waitForAjax();
		//scrolltoend();
		scrollDown(ManageNetwork_Obj.ManageNetwork.synchronization_devicevalue);
		compareText("Device", ManageNetwork_Obj.ManageNetwork.synchronization_devicevalue, devicename);
		
		String syncstatusvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.synchronization_syncstatusvalue,"synchronization_syncstatusvalue");
		Reporter.log("Sync Status value is: "+syncstatusvalue);
		
		if(FMC_column.equalsIgnoreCase("Yes"))
		{
		//verify FMC value
				getTextFrom(ManageNetwork_Obj.ManageNetwork.synchronization_fmcvalue,"FMC");
				//verify FMC date time 
				try {
					String GMTValue;
					String FMCvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.fmc_datetimevalue,"fmc_datetimevalue");
					String FMCDateTimevalue= "";
					if (FMCvalue.length() > 20) 
					{
						FMCDateTimevalue = FMCvalue.substring(FMCvalue.length() - 20);
						System.out.println("last 20 characters:"+FMCDateTimevalue);
					} 
					else 
					{
						FMCDateTimevalue = FMCvalue;
					}

					ExtentTestManager.getTest().log(LogStatus.PASS, "FMC date value is displayed as: "+FMCDateTimevalue);
					SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
					if (FMCDateTimevalue.length() > 3) 
					{
					    GMTValue = FMCDateTimevalue.substring(FMCDateTimevalue.length() - 3);
					} 
					else
					{
						GMTValue = FMCDateTimevalue;
					}
				//	assertEquals(GMTValue, "GMT");
					
					}catch(Exception e)
					{
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : FMC date value field is not displaying");
					}
		}
		else if(SMARTS_column.equalsIgnoreCase("Yes"))
		{
		//verify smarts value
		getTextFrom(ManageNetwork_Obj.ManageNetwork.synchronization_smartsvalue,"Smarts");
		//verify smarts date time 
		try {
			String GMTValue;
			String Smartsvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.smarts_datetimevalue,"smarts_datetimevalue");
			String SmartsDateTimevalue= "";
			if (Smartsvalue.length() > 20) 
			{
				SmartsDateTimevalue = Smartsvalue.substring(Smartsvalue.length() - 20);
				System.out.println("last 20 characters:"+SmartsDateTimevalue);
			} 
			else 
			{
				SmartsDateTimevalue = Smartsvalue;
			}

			ExtentTestManager.getTest().log(LogStatus.PASS, "Smarts date value is displayed as: "+SmartsDateTimevalue);
			SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
			if (SmartsDateTimevalue.length() > 3) 
			{
			    GMTValue = SmartsDateTimevalue.substring(SmartsDateTimevalue.length() - 3);
			} 
			else
			{
				GMTValue = SmartsDateTimevalue;
			}
			//assertEquals(GMTValue, "GMT");
			
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		else if(DCS_column.equalsIgnoreCase("Yes"))
		{
			//verify DCS value
			getTextFrom(ManageNetwork_Obj.ManageNetwork.synchronization_dcsvalue,"DCS");
				//verify DCS date time
				try {
					String GMTValue;
					String DCSvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.dcs_datetime,"dcs_datetime");
					String DCS_DateTimevalue= "";
					if (DCSvalue.length() > 20) 
					{
						DCS_DateTimevalue = DCSvalue.substring(DCSvalue.length() - 20);
						System.out.println("last 20 characters:"+DCS_DateTimevalue);
					} 
					else 
					{
						DCS_DateTimevalue = DCSvalue;
					}
					
					ExtentTestManager.getTest().log(LogStatus.PASS, "DCS date value is displayed as: "+DCS_DateTimevalue);
					SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
					if (DCS_DateTimevalue.length() > 3) 
					{
					    GMTValue = DCS_DateTimevalue.substring(DCS_DateTimevalue.length() - 3);
					} 
					else
					{
						GMTValue = DCS_DateTimevalue;
					}
					//assertEquals(GMTValue, "GMT");
					
					}catch(Exception e)
					{
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : DCS date value field is not displaying");
					}
		}
		else if(FetchInterfaces_column.equalsIgnoreCase("Yes"))
		{
		//verify fetch interfaces value
			getTextFrom(ManageNetwork_Obj.ManageNetwork.synchronization_fetchinterfacesvalue,"Fetch Interfaces");
		//verify fetch interfaces date time
		try {
			String GMTValue;
			String FetchInterfacesvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.fetchinterfaces_datetime,"fetchinterfaces_datetime");
			String FetchInterfaces_DateTimevalue= "";
			if (FetchInterfacesvalue.length() > 20) 
			{
				FetchInterfaces_DateTimevalue = FetchInterfacesvalue.substring(FetchInterfacesvalue.length() - 20);
				System.out.println("last 20 characters:"+FetchInterfaces_DateTimevalue);
			} 
			else 
			{
				FetchInterfaces_DateTimevalue = FetchInterfacesvalue;
			}

			ExtentTestManager.getTest().log(LogStatus.PASS, "Fetch Interfaces date value is displayed as: "+FetchInterfaces_DateTimevalue);
			SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
			if (FetchInterfaces_DateTimevalue.length() > 3) 
			{
			    GMTValue = FetchInterfaces_DateTimevalue.substring(FetchInterfaces_DateTimevalue.length() - 3);
			} 
			else
			{
				GMTValue = FetchInterfaces_DateTimevalue;
			}
			//assertEquals(GMTValue, "GMT");
			
			}catch(Exception e)
			{
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Fetch Interfaces date value field is not displaying");
			}
		}
		else if(Vistamart_column.equalsIgnoreCase("Yes")) {
			//verify vistamart device value
			getTextFrom(ManageNetwork_Obj.ManageNetwork.synchronization_vistamartdevicevalue,"VistaMart Device");
			//verify vistamart device date time
			try {
				String GMTValue;
				String VistaMartDevicevalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.vistamartdevice_datetime,"vista mart device_datetime");
				String VistaMartDevice_DateTimevalue= "";
				if (VistaMartDevicevalue.length() > 20) 
				{
					VistaMartDevice_DateTimevalue = VistaMartDevicevalue.substring(VistaMartDevicevalue.length() - 20);
					System.out.println("last 20 characters:"+VistaMartDevice_DateTimevalue);
				} 
				else 
				{
					VistaMartDevice_DateTimevalue = VistaMartDevicevalue;
				}
				
				ExtentTestManager.getTest().log(LogStatus.PASS, "Vistamart Device date value is displayed as: "+VistaMartDevice_DateTimevalue);
				SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
				if (VistaMartDevice_DateTimevalue.length() > 3) 
				{
				    GMTValue = VistaMartDevice_DateTimevalue.substring(VistaMartDevice_DateTimevalue.length() - 3);
				} 
				else
				{
					GMTValue = VistaMartDevice_DateTimevalue;
				}
				//assertEquals(GMTValue, "GMT");
				
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			
		}
		else if(PGW_column.equalsIgnoreCase("Yes")) {
			//verify PGW value
			getTextFrom(ManageNetwork_Obj.ManageNetwork.synchronization_pgwvalue,"PGW");
			//verify PGW date time
			try {
				String GMTValue;
				String PGWvalue= getTextFrom(ManageNetwork_Obj.ManageNetwork.pgw_datetime,"pgw_date time");
				String PGW_DateTimevalue= "";
				if (PGWvalue.length() > 20) 
				{
					PGW_DateTimevalue = PGWvalue.substring(PGWvalue.length() - 20);
					System.out.println("last 20 characters:"+PGW_DateTimevalue);
				} 
				else 
				{
					PGW_DateTimevalue = PGWvalue;
				}
				
				ExtentTestManager.getTest().log(LogStatus.PASS, "PGW date value is displayed as: "+PGW_DateTimevalue);
				SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
				if (PGW_DateTimevalue.length() > 3) 
				{
				    GMTValue = PGW_DateTimevalue.substring(PGW_DateTimevalue.length() - 3);
				} 
				else
				{
					GMTValue = PGW_DateTimevalue;
				}
				//assertEquals(GMTValue, "GMT");
				
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			
		}
		//verify synchronize link
		waitForAjax();
		scrollDown(ManageNetwork_Obj.ManageNetwork.synchronization_synchronizelink);
		click(ManageNetwork_Obj.ManageNetwork.synchronization_synchronizelink,"Synchronize");
		waitToPageLoad();
		waitForAjax();
		NewTab.verifysuccessmessage("Sync started successfully. Please check the sync status of this device.");
		waitForAjax();
		
		//verify device name link in status panel
		scrollDown(ManageNetwork_Obj.ManageNetwork.status_devicevalue);
		click(ManageNetwork_Obj.ManageNetwork.status_devicevalue, "Device");
		waitForAjax();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to 'View Device' page");
		webDriver.navigate().back();
		waitForAjax();
		
		//verify device name link in synchronization panel
		scrollDown(ManageNetwork_Obj.ManageNetwork.status_devicevalue);
		click(ManageNetwork_Obj.ManageNetwork.synchronization_devicevalue,"Device");
		waitForAjax();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to 'View Device' page");
		waitForAjax();
	
		
		
	}catch(Exception e) {
		e.printStackTrace();
		
		//verify device name link in status panel
		click(ManageNetwork_Obj.ManageNetwork.status_devicevalue,"status_devicevalue");
		waitForAjax();
	}
}
	
}
