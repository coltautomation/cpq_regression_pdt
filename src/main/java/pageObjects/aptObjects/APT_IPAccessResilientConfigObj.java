package pageObjects.aptObjects;

public class APT_IPAccessResilientConfigObj {

	public static class IPAccessResilientConfig {
		// !-- ALL COMMON XPATH- DON'T MAKE ANY CHANGE IN COMMON XPATH
		// --="@xpath=
		public static final String OKButton_common = "@xpath=//span[contains(text(),'OK')]";
		public static final String DeleteButton_common = "@xpath=//button[text()='Delete']";

		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";
		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";
		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";

		public static final String mcslink = "@xpath=//a[contains(text(),'Manage Customer')]";
		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";
		public static final String createorderlink = "@xpath=//li[text()='Create Order/Service']";

		// !-- breadcrump common code --="@xpath=
		public static final String breadcrump = "@xpath=//ol[@class='breadcrumb']//a[text()='value']";

		// !-- Create Customer Page --="@xpath=
		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String nametextfield = "@xpath=//input[@id='name']";
		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";
		public static final String country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String ocntextfield = "@xpath=//input[@id='ocn']";
		public static final String referencetextfield = "@xpath=//input[@id='reference']";
		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";
		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";
		public static final String emailtextfield = "@xpath=//input[@id='email']";
		public static final String phonetextfield = "@xpath=//input[@id='phone']";
		public static final String faxtextfield = "@xpath=//input[@id='fax']";
		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";
		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String clearbutton = "@xpath=//span[text()='Clear']";
		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";
		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// !-- Verify warning messages --="@xpath=
		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";
		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";
		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";
		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";
		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";
		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";
		public static final String ServiceTypeWarningMessage = "@xpath=//span[contains(text(),'Service type is required')]";
		public static final String NetworkConfigurationWarningMessage = "@xpath=//span[contains(text(),'Network Configuration is required')]";
		public static final String sidwarngmsg = "@xpath=//div[text()='Service Identification']";
		public static final String billingtype_warngmsg = "@xpath=//div[text()='Billing Type']";
		public static final String BGPPassword_warningMessage = "@xpath=//div[text()='BGP Password']";
		public static final String BGPTableDistribution_warningMessage = "@xpath=//div[contains(text(),'BGP Table Distribution')]";
		public static final String order_contractnumber_warngmsg = "@xpath=//label[contains(text(),'Order/Contract Number(Parent SID)')]/following-sibling::span[3]";
		public static final String servicetype_warngmsg = "@xpath=//div[label[contains(text(),'Service Type')]]/following-sibling::span";

		// !-- Verify Customer details panel --="@xpath=
		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";
		public static final String Name_Text = "@xpath=(//div//label[@for='name'])[1]";
		public static final String Name_Value = "@xpath=//label[text()='Legal Customer Name']/parent::div/parent::div//div[2]";
		public static final String MainDomain_Text = "@xpath=(//div//label[@for='name'])[2]";
		public static final String MainDomain_Value = "@xpath=//label[text()='Main Domain']/parent::div/parent::div//div[2]";
		public static final String Country_Text = "@xpath=(//div//label[@for='name'])[3]";
		public static final String Country_Value = "@xpath=//label[text()='Country']/parent::div/parent::div//div[2]";
		public static final String OCN_Text = "@xpath=(//div//label[@for='name'])[4]";
		public static final String OCN_Value = "@xpath=//label[text()='OCN']/parent::div/parent::div//div[2]";
		public static final String Reference_Text = "@xpath=(//div//label[@for='name'])[5]";
		public static final String Reference_Value = "@xpath=//label[text()='Reference']/parent::div/parent::div//div[2]";
		public static final String Type_Text = "@xpath=(//div//label[@for='name'])[6]";
		public static final String Type_Value = "@xpath=//label[text()='Type']/parent::div/parent::div//div[2]";
		public static final String TechnicalContactName_Text = "@xpath=(//div//label[@for='name'])[7]";
		public static final String TechnicalContactName_Value = "@xpath=//label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";
		public static final String Email_Text = "@xpath=(//div//label[@for='name'])[8]";
		public static final String Email_Value = "@xpath=//label[text()='Email']/parent::div/parent::div//div[2]";
		public static final String Phone_Text = "@xpath=(//div//label[@for='name'])[9]";
		public static final String Phone_Value = "@xpath=//label[text()='Phone']/parent::div/parent::div//div[2]";
		public static final String Fax_Text = "@xpath=(//div//label[@for='name'])[10]";
		public static final String Fax_Value = "@xpath=//label[text()='Fax']/parent::div/parent::div//div[2]";

		// !-- create order/service page --="@xpath=
		// !-- public static final String
		// nametextfield="@xpath=//input[@id='customerSearch']";
		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";
		// public static final String
		// chooseCustomerdropdown="@xpath=//div[label[text()='Choose a
		// customer']]//input";
		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";
		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";
		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";
		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";
		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";
		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";
		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";
		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";
		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";
		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";
		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";
		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";
		public static final String useractionbutton = "@xpath=//button[@id='dropdown-basic-button']";
		public static final String adduserbutton = "@xpath=//a[contains(text(),'Add')]";
		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";

		// !-- Create Order/service panel in view customer page --="@xpath=
		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";
		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";
		public static final String selectorderswitch = "@xpath=(//div[@class='react-switch-bg'])[2]";
		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";
		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";
		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";
		public static final String newordertextfield = "@xpath=//input[@id='orderName']";
		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String servicetypetextfield = "@xpath=//div[div[label[text()='Service Type']]]//input";
		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";
		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String changeorder_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String commonDropdownValueDivTag = "@xpath=//div[@class='sc-htpNat AUGYd']/div";
		public static final String commonDropdownValueSpanTag = "@xpath=//span[@role='option']";

		// !-- Service Creation Form in Service creation page --="@xpath=
		public static final String createorderservice_header = "@xpath=//div[text()='Create Order / Service']";
		public static final String serviceidentificationtextfield = "@xpath=//input[@id='serviceName']";
		public static final String servicetypevalue = "@xpath=//div[label[text()='Service Type']]/div";
		public static final String servicetype_value = "@xpath=//div[label[text()='Service Type']]/following-sibling::div";
		public static final String serviceSubtypevalue = "@xpath=//div[label[text()='Network Configuration']]/div";
		public static final String billingtype_dropdown = "@xpath=//label[text()='Billing Type']/parent::div//select";
		public static final String terminationdate_field = "@xpath=//label[text()='Termination Date']/parent::div//input[@name='terminationDate']";
		public static final String secondaryCircuitOrder_field = "@xpath=//input[@id='secondaryCircuitOrder']";
		public static final String Email_header = "@xpath=//div//label[text()='Email']";
		public static final String emailtextfield_Service = "@xpath=//div[label[text()='Email']]/input";
		public static final String emailtextfieldvalue = "@xpath=//div[label[text()='Email']]//input";
		public static final String emailarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phonecontacttextfield = "@xpath=//input[@id='phoneContactText']";
		public static final String phonecontacttextfieldvalue = "@xpath=//div[label[text()='Phone Contact']]//input";
		public static final String phonearrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String remarktextarea = "@xpath=//textarea[contains(@name,'serviceRmark')]";
		public static final String remarktextareavalue = "@xpath=//div[label[text()='Remark']]//textarea";

		// !-- Management Options in Service creation page --="@xpath=
		public static final String ManageServicecheckbox = "@xpath=//input[@id='managedService']";
		public static final String RouterConfigurationViewIPv4checkbox = "@xpath=//input[@id='routerConfigView']";
		public static final String RouterConfigurationViewIPv6checkbox = "@xpath=//input[@id='routerConfigViewipv6']";
		public static final String performancereportingcheckbox = "@xpath=//input[@id='performanceReporting']";
		public static final String IPGuardiancheckbox = "@xpath=//input[@id='ipGuardian']";
		public static final String SmartMonitoringcheckbox = "@xpath=//input[@id='smartsMonitoring']";
		public static final String CloudPrioritizationCheckbox = "@xpath=//input[@name='microPeering']";
		public static final String ProactiveNotificationcheckbox = "@xpath=//input[@id='proActiveNotification']";
		public static final String NotificationManagementTeamDropdown = "@xpath=//div[label[contains(text(),'Notification Management Team')]]/select";
		public static final String SNMPNotificationcheckbox = "@xpath=//input[@id='snmpNotification']";
		public static final String TrapTargetAddresstextfield = "@xpath=//input[@id='trapTargetAddress']";
		public static final String DeliveryChannelDropdown = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//select";

		// !-- Configuration Options --="@xpath=
		public static final String ConfigurationOptions_ServiceForm = "@xpath=//div[contains(text(),'Configuration Options')]";
		public static final String RouterBasedFirewallcheckbox = "@xpath=//input[@id='routerBasedFirewall']";
		public static final String Qoscheckbox = "@xpath=//input[@id='qos']";
		public static final String ActelisBasedcheckbox = "@xpath=//input[@id='actelisBased']";
		public static final String PICustomerCheckbox = "@xpath=//input[@name='picustomer']";
		public static final String DSLcheckbox = "@xpath=//input[@name='dslValue']";
		public static final String BGPDescriptionTextfield = "@xpath=//input[@name='bgpDescription']";
		public static final String BGPASNumbertextfield = "@xpath=//input[@id='bgpASNumber']";
		public static final String BGPPasswordtextfield = "@xpath=//input[@id='bgpPassword']";
		public static final String GeneratePasswordButton = "@xpath=//span[contains(text(),'Generate Password')]";
		public static final String LoadSharedDropdown = "@xpath=//div[label[contains(text(),'Load Shared')]]//select";
		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String Nextbutton_ServiceCreation = "@xpath=//span[text()='Next']";
		public static final String servicecreationmessage = "@xpath=//div[@role='alert']//span";
		public static final String editservice_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String BGPTableDistributionDropdown = "@xpath=//div[label[contains(text(),'BGP Table Distribution')]]//select";

		public static final String ConfigurationOptionEditServicePage_Header = "@xpath=//div[text()='Configuration Options']";
		// public static final String
		// cancelbutton="@xpath=//button[@type='submit']//span[text()='Cancel']";

		// !-- Search order --="@xpath=
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		// !-- order panel - view service page --="@xpath=
		public static final String orderpanelheader = "@xpath=//div[text()='Order']";
		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath=(//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[1])[1]";
		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";
		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";
		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";
		public static final String orderupdate_successmsg = "@xpath=//div[@role='alert']//span[text()='Order successfully updated']";
		public static final String ChangeOrder_successmsg = "@xpath=//div[@role='alert']//span[text()='Order successfully  changed.']";
		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";
		public static final String changeorder_dropdownvalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false'][1]";

		// !-- Configuration Options Panel in View Service page --="@xpath=
		public static final String ConfigurationOptions_Header = "@xpath=//div[contains(text(),'Configuration Options')]";

		// !-- service panel in View Service Page--="@xpath=
		public static final String servicepanel_header = "@xpath=//div[text()='Service']";
		public static final String servicepanel_serviceidentificationvalue = "@xpath=//label[contains(text(),'Service Identification')]/parent::div/following-sibling::div";
		public static final String servicepanel_servicetypevalue = "@xpath=//label[contains(text(),'Service Type')]/parent::div/following-sibling::div";
		public static final String servicepanel_remarksvalue = "@xpath=//label[contains(text(),'Remark')]/parent::div/following-sibling::div";
		public static final String servicepanel_terminationdate = "@xpath=//label[contains(text(),'Termination Date')]/parent::div/following-sibling::div";
		public static final String servicepanel_email = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Email')]/parent::div/following-sibling::div";
		public static final String servicepanel_phone = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Phone Contact')]/parent::div/following-sibling::div";
		public static final String servicepanel_billingtype = "@xpath=//label[contains(text(),'Billing Type')]/parent::div/following-sibling::div";
		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";
		public static final String serviceupdate_successmsg = "@xpath=//div[@role='alert']//span[text()='Service successfully updated']";
		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";
		public static final String status_ordername = "@xpath=//div/div[1]/div[3]/div[3]/div/label";
		public static final String status_servicename = "@xpath=//div/div[1]/div[3]/div[4]/div[1]/a";
		public static final String status_servicetype = "@xpath=//div/div[1]/div[3]/div[4]/div[2]";
		public static final String status_servicedetails = "@xpath=//div/div[1]/div[3]/div[2]/div[3]/label";
		public static final String status_currentstatus = "@xpath=//div[text()='In Service']";
		public static final String status_modificationtime = "@xpath=//div/div[1]/div[3]/div[4]/div[5]";
		public static final String statuslink = "@xpath=//a[@title='Status']";
		public static final String sync_ordername = "@xpath=//div/div[1]/div[4]/div[3]/div/label";
		public static final String sync_servicename = "@xpath=(//a[@title='Service'])[2]";
		public static final String sync_servicetype = "@xpath=//div/div[1]/div[4]/div[4]/div[2]";
		public static final String sync_servicedetails = "@xpath=//div/div[1]/div[4]/div[2]/div[3]/label";
		public static final String sync_status = "@xpath=//span[text()='Pending']";
		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";
		public static final String changestatus_dropdown = "@xpath=//select[@id='serviceStatus']";
		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";
		public static final String servicestatushistory = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String successmsg = "@xpath=//div[@role='alert']//span";
		public static final String managesubnets_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets']";
		public static final String managesubnet_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Manage')]";
		public static final String managesubnet_successmsg = "@xpath=(//div[@role='alert']//span)[2]";
		public static final String spacename_column = "@xpath=//span[text()='Space Name']";
		public static final String blockname_column = "@xpath=//span[text()='Block Name']";
		public static final String subnetname_column = "@xpath=//span[text()='Subnet Name']";
		public static final String startaddress_column = "@xpath=//span[text()='Start Address']";
		public static final String size_column = "@xpath=//span[text()='Size']";
		public static final String closesymbol = "@xpath=//button[@type='button']//span[text()='�']";
		public static final String dumppage_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Service')]";
		public static final String serviceretrieved_text = "@xpath=//div[@class='div-margin row'][contains(text(),'Service retrieved')]";
		public static final String service_header = "@xpath=//label[text()='Service']";
		public static final String dumppage_text = "@xpath=//label[text()='Service']/following-sibling::textarea";

		public static final String managesubnetsipv6_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets Ipv6']";
		public static final String shownewinfovistareport_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show New Infovista Report']";
		public static final String dump_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='dump']";
		public static final String selectedemail = "@xpath=//select[@name='selectedEmail']//option";
		public static final String selectedphone = "@xpath=//select[@name='selectedPhoneContact']//option";

		// !-- Management Options --="@xpath=
		public static final String managementoptions_performancereporting = "@xpath=//label[contains(text(),'Performance Reporting')]/parent::div/following-sibling::div";
		public static final String managementoptions_ipguardian = "@xpath=//label[contains(text(),'IP Guardian')]/parent::div/following-sibling::div";

		// !-- Users panel in view service page --="@xpath=

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";
		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";
		public static final String EmailColumn = "@xpath=//div[@col-id='email']";
		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";
		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";
		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";
		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";
		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		// public static final String view="@xpath=//div[@class='dropdown-menu
		// show']//a[text()='View']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		// !-- New User creation in view service page --="@xpath=
		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";
		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";
		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String UserName = "@xpath=//input[@id='userName']";
		public static final String FirstName = "@xpath=//input[@id='firstName']";
		public static final String SurName = "@xpath=//input[@id='surname']";
		public static final String PostalAddress = "@xpath=//textarea[@name='postalAddress']";
		public static final String adduser_Email = "@xpath=//input[@id='email']";
		public static final String adduser_Phone = "@xpath=//input[@id='phone']";
		public static final String ipguardianaccountgroup = "@xpath=//input[@id='ipGuardianAccountGrp']";
		public static final String coltonlineuser = "@xpath=//input[@id='onlineUser']";
		public static final String rolesaddarrow = "@xpath=//label[text()='Roles']/parent::div/parent::div//button//span[text()='>>']";

		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";
		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";
		public static final String user_okbutton = "@xpath=//button[@type='submit']//span[text()='OK']";
		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()='Delete']";
		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String userspanel_header = "@xpath=//div[text()='Users']";
		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";
		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";
		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";
		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";
		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";
		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";

		// !-- Management Options panel in view service page --="@xpath=
		public static final String managementoptions_header = "@xpath=//div[text()='Management Options']";
		public static final String portalaccess_header = "@xpath=//label[text()='Portal Access']";

		// !-- Actelis Configuration Panel in view service page --="@xpath=
		public static final String ActelisConfiguration_PanelHeaderInViewServicePage = "@xpath=//div[contains(text(),'Actelis Configuration')]";
		public static final String configure_totalPage = "@xpath=//span[@ref='lbTotal']";
		public static final String configure_currentpage = "@xpath=//span[@ref='lbCurrent']";
		public static final String Actelisconfig_addDSLAM = "@xpath=//div[div[div[text()='Actelis Configuration']]]//a[text()='Add DSLAM and HSL']";
		public static final String DSLM_Device_Select = "@xpath=//div[div[div[label[@class='form-label']]]]//input";
		public static final String List_HSL_Link = "@xpath=//span[contains(text(),'List HSL')]";
		public static final String ActelisConfigurationPanel = "@xpath=//div[div[contains(text(),'Actelis Configuration')]]";
		public static final String actelis_EquipConfig_viewLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='View']";
		public static final String actelis_EquipConfig_deleteLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='Delete']";
		public static final String addDSLAMandHSL_xButton = "@xpath=//div[div[label[text()='DSLAM Device:']]]//following-sibling::div//div[text()='�']";
		public static final String selectDSLAMdeviceValue = "@xpath=//div[text()='value']";
		public static final String selectDSLAMdeviceValue1 = "@xpath=//div[text()='";
		public static final String selectDSLAMdeviceValue2 = "']";
		public static final String InterfaceToSelect_actelis_totalpage = "@xpath=//span[@ref='lbTotal']";
		public static final String InterfaceToSelect_actelis_currentpage = "@xpath=//span[@ref='lbCurrent']";
		public static final String showInterface_ActelisCnfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//a[text()='Show Interfaces']";
		public static final String AcionButton_ActelisConfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//button[text()='Action']";
		public static final String removeButton_ActelisConfiguration = "@xpath=//a[text()='Remove']";
		public static final String popupMessage_forRemove_ActelisConfiguration = "@xpath=//div[@class='modal-body']";
		public static final String deleteInterface_successMessage_ActelisCOnfiguration = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";
		public static final String successMessage_ActelisConfiguration_removeInterface = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

		// !-- Provider Equipment (PE) panel in view service page --="@xpath=
		public static final String addpedevice_link = "@xpath=//a[text()='Add PE Device']";
		public static final String addpedevice_header = "@xpath=//div[text()='Add PE Device']";
		public static final String addnewdevice_togglebutton = "@xpath=(//div[@class='react-switch-handle'])[1]";
		public static final String providerequipment_header = "@xpath=//div[text()='Provider Equipment (PE)']";
		public static final String ProviderEquipment_header = "@xpath=//div[text()='Provider Equipment (PE)']";

		public static final String existingPEdevicegrid = "@xpath=(//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div)[2]";
		public static final String PE_fetchAlldevice_InviewPage = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//parent::div/following-sibling::div/div[@class='div-margin row']//b";

		// !-- Provider Equipment (PE) panel in view service page --="@xpath=
		public static final String PE_AddPEDeviceLink = "@xpath=//a[contains(text(),'Add PE Device')]";
		public static final String PE_showInterfacesLink = "@xpath=//div[a[text()='Add PE Device']]//div[a[text()='Show Interfaces']]";
		public static final String PE_HideInterfaceLink = "@xpath=//div[a[text()='Add PE Device']]//div[a[text()='Hide Interfaces']]";
		public static final String PE_ACTION = "@xpath=(//button[@id='dropdown-basic-button'])[4]";
		public static final String PE_AddPEDevice_header = "@xpath=//div[text()='Add PE Device']";
		public static final String PE_IMSPOPLocationDropdown = "@xpath=//div[input[@placeholder='Select ...']]";
		public static final String PE_Cancelbutton = "@xpath=//button/span[text()='Cancel']";
		public static final String PE_OKbutton = "@xpath=//button//span[text()='OK']";
		public static final String PE_AddPEDeviceWarningMessage = "@xpath=//div/div/div[@style='color: red;']";

		public static final String PE_AddPEDeviceSuccessfulMessage = "@xpath=//span[text()='Device successfully created.']";
		public static final String PE_UpdatePEDeviceSuccessfulMessage = "@xpath=//span[text()='Device successfully updated.']";
		public static final String PE_DeletePEDeviceSuccessfulMessage = "@xpath=//span[text()='Device successfully deleted.']";

		public static final String PE_ViewService_DeletePEDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete?']";
		public static final String PE_ViewService_DeleteButton = "@xpath=//div/button[text()='Delete']";

		// !-- Below are the fields values information under view PE Device page
		// --="@xpath=
		public static final String PE_View_DeviceNameValue = "@xpath=//div[label[text()='Name']]/following-sibling::div";
		public static final String PE_View_VendorModelValue = "@xpath=//div[label[text()='Vendor/Model']]/following-sibling::div";
		public static final String PE_View_ManagementAddressValue = "@xpath=//div[label[text()='Management Address']]/following-sibling::div";
		public static final String PE_View_SnmproValue = "@xpath=//div[label[text()='Snmpro']]/following-sibling::div";
		public static final String PE_View_CountryValue = "@xpath=//div[label[text()='Country']]/following-sibling::div";
		public static final String PE_View_CityValue = "@xpath=//div[label[text()='City']]/following-sibling::div";
		public static final String PE_View_SiteValue = "@xpath=//div[label[text()='Site']]/following-sibling::div";
		public static final String PE_View_PremiseValue = "@xpath=//div[label[text()='Premise']]/following-sibling::div";
		public static final String PE_View_ActionLink = "@xpath=//div[@style='margin-top: -1px;']/button[@id='dropdown-basic-button']";
		public static final String PE_View_ActionLink2 = "@xpath=(//button[@id='dropdown-basic-button'])[1]";
		public static final String PE_View_InterfacesActionLink = "@xpath=(//button[@id='dropdown-basic-button'])[2]";
		public static final String PE_View_TestColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Test']";
		public static final String PE_View_StatusColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Status']";
		public static final String PE_View_LastRefresh = "@xpath=//div/div[text()='Last Refresh:']";
		public static final String PE_View_Action_EditLink = "@xpath=//div//a[text()='Edit']";
		public static final String PE_Edit_PremiseLevel = "@xpath=//div//label[text()='Premise']";
		public static final String PE_View_Action_DeleteLink = "@xpath=//div//a[text()='Delete']";
		// public static final String
		// PE_View_Action_FetchDeviceInterfacesLink="@xpath=//div//a[text()='Fetch
		// Device Interfaces']";
		public static final String PE_ViewDevice_Action_DeletePEDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete this item?']";
		public static final String PE_ViewDevice_Action_DeleteButton = "@xpath=//button//span[text()='Delete']";

		// !-- Below Fetch Device Interface ==="@xpath= Manage Colt Page for PE
		// Device --="@xpath=
		public static final String PE_View_Action_FetchDeviceInterfacesLink = "@xpath=//div//a[text()='Fetch Device Interfaces']";
		// public static final String
		// PE_FetchDeviceInterfacesSuccessMessage="@xpath=//span[contains(text(),'Interfaces
		// are fetched successfully.')]";
		public static final String PE_hereLink_UnderFetchDeviceInterfacesSuccessMessage = "@xpath=//span[contains(text(),'Interfaces are fetched successfully')]//following-sibling::a[text()='here']";
		public static final String PE_ManageCOLTsNetworkManageNetwork_header = "@xpath=//div[contains(text(),'Manage COLT')]";
		public static final String PE_Manage_Status_DeviceValue = "@xpath=(//div[div[label[text()='Device']]]/parent::div//div/a)[4]";
		public static final String PE_Manage_Status_StatusValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-3'])[4]";
		public static final String PE_Manage_Status_LastModificationValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-4'])[2]";
		public static final String PE_Manage_Status_StatusLink = "@xpath=//a[contains(text(),'Status')]";
		public static final String PE_Manage_Status_ViewInterfacesLink = "@xpath=//a[contains(text(),'View Interfaces')]";
		public static final String PE_Manage_Status_Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String PE_Device_Status_OK = "@xpath=//button/span[text()='OK']";
		public static final String PE_servicestatus_popupclose = "@xpath=(//div[text()='Device']/following-sibling::button//span)[1]";
		public static final String PE_servicestatushistory_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String PE_Status_successMessage = "@xpath=//div[@role='alert']//span";
		public static final String PE_servicestatushistoryValue = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String PE_Manage_Synchronization_DeviceValue = "@xpath=(//div[div[label[text()='Device']]]/parent::div//div/a)[4]";
		public static final String PE_Manage_Synchronization_SyncStatusValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[9]";
		public static final String PE_Manage_Synchronization_SmartsValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[10]";
		public static final String PE_Manage_Synchronization_FetchInterfacesValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[11]";
		public static final String PE_Manage_Synchronization_VistaMartDeviceValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[12]";
		public static final String PE_Manage_Synchronization_SynchronizeLink = "@xpath=//a[text()='Synchronize']";
		public static final String PE_Manage_SynchronizeSuccessMessage = "@xpath=//div[@role='alert']//span";

		// !-- Create Device Page --="@xpath=
		// public static final String
		// nametextfield="@xpath=//input[@id='name']";
		public static final String vendormodelinput = "@xpath=//div[label[text()='Vendor/Model']]//select";
		public static final String snmproinputfield = "@xpath=//input[@id='snmpro']";
		public static final String telnetradiobutton = "@xpath=//input[@value='telnet']";
		public static final String sshradiobutton = "@xpath=//input[@value='ssh']";
		public static final String c2cradiobutton = "@xpath=//input[@value='2c']";
		public static final String c3radiobutton = "@xpath=//input[@value='3']";
		public static final String snmprotextfield = "@xpath=//input[@id='snmpro']";
		public static final String snmprwtextfield = "@xpath=//input[@id='snmprw']";
		public static final String snmpv3username = "@xpath=//input[@id='protocolsnmpv3Username']";
		public static final String snmpv3authpassword = "@xpath=//input[@id='protocolsnmpv3Authpasswd']";
		public static final String snmpv3privpassword = "@xpath=//input[@id='protocolsnmpv3Privpasswd']";
		public static final String countryinput = "@xpath=//div[label[contains(text(),'Country')]]//select";
		public static final String managementaddresstextbox = "@xpath=//label[text()='Management Address']/following-sibling::input";
		public static final String citydropdowninput = "@xpath=//div[label[contains(text(),'City')]]//select";
		public static final String sitedropdowninput = "@xpath=//div[label[contains(text(),'Site')]]//select";
		public static final String premisedropdowninput = "@xpath=//div[label[contains(text(),'Premise')]]//select";
		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String successMessage_Alert = "@xpath=//div[@role='alert']//span";

		// !-- Warning Message --="@xpath=
		public static final String warningMessage_Country = "@xpath=//div[text()='Country']";
		public static final String warningMessage_name = "@xpath=//div[text()='Name']";
		public static final String warningMessage_vendor = "@xpath=//div[text()='Vendor/Model']";

		// !-- fetch device Interface --="@xpath=
		public static final String fetchDeviceinterfacelink_viewDevicePage = "@xpath=//a[text()='Fetch device interfaces']";
		public static final String ClickhereLink_fetchInterface = "@xpath=//a[text()='here']";

		public static final String iosxrcheckbox = "@xpath=//input[@id='iosXr']";

		public static final String addCitytogglebutton = "@xpath=//div[div[label[text()='Add City']]]//div[@class='react-switch-bg']";
		public static final String citynameinputfield = "@xpath=//input[@id='cityName']";
		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode']";
		public static final String sitenameinputfield = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String SnmprwLabelAddPEDevicePage = "@xpath=//label[text()='Snmprw']";
		public static final String sitecodeinputfield = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";
		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";

		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2]";
		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2]";
		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";
		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";

		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";
		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		// !-- view device page --="@xpath=
		public static final String viewpage_devicename = "@xpath=//label[text()='Name']/parent::div/div";
		public static final String viewpage_vendormodel = "@xpath=//label[text()='Vendor/Model']/parent::div/div";
		public static final String viewpage_managementaddress = "@xpath=//label[text()='Management Address']/parent::div/div";
		public static final String viewpage_country = "@xpath=//label[text()='Country']/parent::div/div";
		public static final String viewpage_city = "@xpath=//label[text()='City']/parent::div/div";
		public static final String viewpage_site = "@xpath=//label[text()='Site']/parent::div/div";
		public static final String viewpage_premise = "@xpath=//label[text()='Premise']/parent::div/div";
		public static final String viewpage_connectivityprotocol = "@xpath=//label[text()='Connectiviy Protocol']/parent::div/div";
		public static final String viewpage_snmpversion = "@xpath=//label[text()='SNMP Version']/parent::div/div";
		public static final String viewpage_snmpro = "@xpath=//label[text()='Snmpro']/parent::div/div";
		public static final String viewpage_snmprw = "@xpath=//label[text()='Snmprw']/parent::div/div";
		public static final String viewpage_snmpv3username = "@xpath=//label[text()='Snmp v3 Username']/parent::div/div";
		public static final String viewpage_snmpv3privpassword = "@xpath=//label[text()='Snmp v3 Priv password']/parent::div/div";
		public static final String viewpage_snmpv3authpassword = "@xpath=//label[text()='Snmp v3 Auth password']/parent::div/div";
		public static final String BackButtonxpath = "@xpath=//span[text()='Back']";

		// !-- Edit Device --="@xpath=
		public static final String viewdevice_Actiondropdown = "@xpath=//div[text()='Device']/following-sibling::div//button[text()='Action']";
		public static final String viewdevice_Edit = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Edit']";
		public static final String editdeviceheader = "@xpath=//div[@class='heading-green-row row']//div[text()='Edit PE Device']";

		public static final String existingdevicegrid = "@xpath=(//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div)[2]";
		public static final String viewservicepage_viewdevicelink1 = "@xpath=(//span[text()='View'])[1]";
		public static final String viewdevicepage_header = "@xpath=//div[text()='Device']";
		public static final String viewservicepage_editdevicelink = "@xpath=//span[text()='Edit']";

		// !-- Delete Device --="@xpath=
		public static final String viewdevice_delete = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Delete']";
		public static final String deletealertclose = "@xpath=//div[@class='modal-content']//button//span[text()='�']";

		public static final String managementaddressbutton = "@xpath=//button//span[text()='Management Address']";

		// !-- Fetch Interface --="@xpath=
		public static final String viewdevice_fetchinterfacelink = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Fetch Device Interfaces']";
		public static final String fetchsuccessmsg = "@xpath=(//div[@role='alert']//span)[1]";
		public static final String herelink_fetchinterface = "@xpath=(//div[@role='alert']//span)[1]/a";
		public static final String fetchDeviceInterace_SuccessMessage = "@xpath=(//div[contains(@class,'alert-success')]/span)[1]";
		public static final String fetchdeviceInterface_successtextMessage = "@xpath=(//div[@role='alert']/span)[1]s";

		// !-- Manage Network --="@xpath=
		public static final String managenetwork_header = "@xpath=(//div[@class='heading-green-row row'])[1]//div";
		public static final String status_header = "@xpath=(//div[@class='heading-green-row row'])[2]//div";
		public static final String synchronization_header = "@xpath=(//div[@class='heading-green-row row'])[3]//div";
		public static final String status_devicecolumn = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String status_statuscloumn = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Status']";
		public static final String status_lastmodificationcolumn = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Last Modification']";
		public static final String status_Action = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String synchronization_devicecolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String synchronization_syncstatuscolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Sync Status']";
		public static final String synchronization_smartscolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Smarts']";
		public static final String synchronization_FetchInterfacescolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Fetch Interfaces']";
		public static final String synchronization_vistamartdevice = "@xpath=(//label[@class='form-label'])[9]";
		public static final String synchronization_actioncolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String status_devicevalue = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[1]";
		public static final String status_statusvalue = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[2]";
		public static final String status_lastmodificationvalue = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[3]";
		public static final String status_statuslink = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[2]";
		public static final String status_viewinterfaceslink = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[3]";
		public static final String synchronization_devicevalue = "@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div//a)[1]";
		public static final String synchronization_syncstatusvalue = "@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div[1]/span)[1]";
		public static final String synchronization_smartsvalue = "@xpath=(//label[text()='Smarts']/parent::div/parent::div/following-sibling::div//div)[3]/span";
		public static final String synchronization_fetchinterfacesvalue = "@xpath=(//label[text()='Fetch Interfaces']/parent::div/parent::div/following-sibling::div//div)[4]/span";
		public static final String synchronization_vistamartdevicevalue = "@xpath=(//div[@class='div-margin row']//div)[10]//span";
		public static final String synchronization_synchronizelink = "@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div//a)[2]";
		public static final String managenetwork_backbutton = "@xpath=(//div[@class='div-margin row'])[4]//button";
		public static final String smarts_datetimevalue = "@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[4]//span/parent::div";
		public static final String fetchinterfaces_datetime = "@xpath=(//label[text()='Fetch Interfaces']/parent::div/parent::div/following-sibling::div//div)[4]";
		public static final String vistamartdevice_datetime = "@xpath=(//label[text()='VistaMart Device']/parent::div/parent::div/following-sibling::div//div)[5]";
		// public static final String
		// Sync_successmsg="@xpath=//div[@role='alert']//span";
		public static final String staus_statuspopup = "@xpath=//div[@class='modal-content']";
		public static final String Statuspage_header = "@xpath=//div[@class='modal-header']//div";
		public static final String statuspage_nameheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']";
		public static final String statuspage_vendormodelheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']";
		public static final String statuspage_managementaddressheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']";
		public static final String statuspage_snmproheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']";
		public static final String statuspage_countryheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']";
		public static final String statuspage_cityheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']";
		public static final String statuspage_siteheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']";
		public static final String statuspage_premiseheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']";
		public static final String statuspage_namevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']/following-sibling::div";
		public static final String statuspage_vendormodelvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']/following-sibling::div";
		public static final String statuspage_managementaddressvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']/following-sibling::div";
		public static final String statuspage_snmprovalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']/following-sibling::div";
		public static final String statuspage_countryvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']/following-sibling::div";
		public static final String statuspage_cityvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']/following-sibling::div";
		public static final String statuspage_sitevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']/following-sibling::div";
		public static final String statuspage_premisevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']/following-sibling::div";
		public static final String Statuspage_statusheader = "@xpath=//div[@class='modal-content']//div[@class='heading-green-row row']//div";
		public static final String statuspage_currentstatusfieldheader = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']";
		public static final String statuspage_newstatusfieldheader = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='New Status']";
		public static final String statuspage_currentstatusvalue = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']/following-sibling::div";
		public static final String statuspage_newstatusdropdown = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select";
		public static final String statuspage_newstatusdropdownvalue = "@xpath=((//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label)[2]/following-sibling::select//option)[1]";
		public static final String statuspage_okbutton = "@xpath=(//div[@class='modal-content']//button[@type='button']//span)[3]";
		public static final String statuspage_statuscolumnheader = "@xpath=//span[@role='columnheader'][text()='Status']";
		public static final String statuspage_changedon_columnheader = "@xpath=//span[@role='columnheader'][text()='Changed On']";
		public static final String statuspage_changedby_columnheader = "@xpath=//span[@role='columnheader'][text()='Changed By']";
		public static final String statuspage_newstatusvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[1]";
		public static final String statuspage_changedonvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[2]";
		public static final String statuspage_changedbyvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[3]";
		public static final String statuspage_closebutton = "@xpath=(//div[@class='modal-content']//button[@type='button'])[1]";
		public static final String viewinterfacepage_header = "@xpath=//div[@class='modal-header']//div";
		public static final String viewinterfacepage_interfacesubheader = "@xpath=//div[@class='modal-body']//div[@class='heading-green-row row']/div";
		public static final String viewinterface_devicenamecolumnheader = "@xpath=(//div[@col-id='deviceName']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfacename_columnheader = "@xpath=(//div[@col-id='name']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfaceaddress_columnheader = "@xpath=(//div[@col-id='address']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfacetype_columnheader = "@xpath=(//div[@col-id='type.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfaceaddress_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='address'])[1]";
		public static final String interfacetype_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='type.desc'])[1]";
		public static final String viewinterface_status_columnheader = "@xpath=(//div[@col-id='currentStatus.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String viewinterface_status_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='currentStatus.desc'])[1]";
		public static final String viewinterface_lastmod_columnheader = "@xpath=(//div[@col-id='m_time']//div[@ref='eLabel'])[1]/span[1]";
		public static final String viewinterface_lastmod_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='m_time'])[1]";
		public static final String viewinterface_closebutton = "@xpath=(//div[@class='modal-header']//button[@type='button']/span)[1]";
		public static final String statuspage_interfaceheader = "@xpath=(//div[@class='modal-header']//div)[2]";
		public static final String interface_statuspage_namefield = "@xpath=//label[text()='Name']";
		public static final String interface_statuspage_interfaceaddressfield = "@xpath=//label[text()='Interface Address']";
		public static final String interface_statuspage_currentstatusfield = "@xpath=//label[text()='Current Status']";
		public static final String interface_statuspage_newstatusfield = "@xpath=//label[text()='New Status']";
		public static final String interface_statuspage_namevalue = "@xpath=//label[text()='Name']/following-sibling::div";
		public static final String interface_statuspage_interfaceaddressvalue = "@xpath=//label[text()='Interface Address']/following-sibling::div";
		public static final String interface_statuspage_currentstatusvalue = "@xpath=//label[text()='Current Status']/following-sibling::div";
		public static final String interface_statuspage_newstatusdropdown = "@xpath=//label[text()='New Status']/parent::div//select";
		public static final String interface_statuspage_newstatusdropdownvalue = "@xpath=//label[text()='New Status']/parent::div//select/option";
		public static final String interface_statuspage_okbutton = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//button[@type='button']//span";
		public static final String interface_statuspage_statuscolumnheader = "@xpath=(//div[@ref='eLabel']//span[text()='Status'])[2]";
		public static final String interface_statuspage_changedon_columnheader = "@xpath=//div[@ref='eLabel']//span[text()='Changed On']";
		public static final String interface_statuspage_changedby_columnheader = "@xpath=//div[@ref='eLabel']//span[text()='Changed By']";
		public static final String interface_statuspage_newstatusvalue = "@xpath=(//div[@role='gridcell'][@col-id='status.desc'])[1]";
		public static final String interface_statuspage_changedonvalue = "@xpath=(//div[@role='gridcell'][@col-id='changeDate'])[1]";
		public static final String interface_statuspage_changedbyvalue = "@xpath=(//div[@role='gridcell'][@col-id='user'])[1]";
		public static final String interface_statuspage_closebutton = "@xpath=(//div[@class='modal-header']//button[@type='button'])[2]";
		public static final String interface_statuspage_statusheader = "@xpath=(//div[@class='modal-body']//div[@class='heading-green-row row']//div)[2]";
		public static final String searchdevice_header = "@xpath=(//div[@class='heading-green-row row'])[1]//div";

		// !-- Existing device values --="@xpath=
		public static final String typepename_dropdown = "@xpath=//label[text()='Type PE name to filter']/parent::div//div[contains(@class,'react-dropdown-select-content')]";
		public static final String existingdevice_vendormodelvalue = "@xpath=(//div[label[contains(text(),'Vendor / Model')]]/div)[1]";
		public static final String existingdevice_managementaddressvalue = "@xpath=(//div[label[contains(text(),'Management Address')]]/div)[1]";
		public static final String existingdevice_connectivityprotocol = "@xpath=(//div[label[contains(text(),'Connectivity Protocol')]]/div)[1]";
		public static final String existingdevice_snmpversion = "@xpath=(//div[label[contains(text(),'SNMP Version')]]/div)[1]";
		public static final String existingdevice_country = "@xpath=(//div[label[contains(text(),'Country')]]/div)[1]";
		public static final String existingdevice_city = "@xpath=(//div[label[contains(text(),'City')]]/div)[1]";
		public static final String existingdevice_site = "@xpath=(//div[label[contains(text(),'Site')]]/div)[1]";
		public static final String existingdevice_premise = "@xpath=(//div[label[contains(text(),'Premise')]]/div)[1]";
		public static final String existingdevice_snmpro = "@xpath=(//div[label[contains(text(),'Snmpro')]]/div)[1]";
		public static final String existingdevice_snmprw = "@xpath=(//div[label[contains(text(),'Snmprw')]]/div)[1]";
		public static final String existingdevice_snmpv3username = "@xpath=(//div[label[contains(text(),'Snmp v3 Username')]]/div)[1]";
		public static final String existingdevice_snmpv3authpassword = "@xpath=(//div[label[contains(text(),'Snmp v3 Auth password')]]/div)[1]";
		public static final String existingdevice_snmpv3privpassword = "@xpath=(//div[label[contains(text(),'Snmp v3 Priv password')]]/div)[1]";

		// String !-- Device Panel --="@xpath=
		public static final String addeddevices_list = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div//div[@class='div-margin row']//b";
		public static final String addeddevice_viewlink = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='View']";
		public static final String addeddevice_selectinterfaceslink = "@xpath=(//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div)[2]//div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";
		public static final String addeddevice_deletefromservicelink = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div//div/following-sibling::div//span[text()='Delete from Service']";

		public static final String PE_SelectInterfacesdevice1 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//span[text()='Select Interfaces'])[1]";
		public static final String PE_SelectInterfacesdevice2 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//span[text()='Select Interfaces'])[2]";
		public static final String PE_deletefromservicedevice1 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//span[text()='Delete from Service'])[1]";
		public static final String PE_deletefromservicedevice2 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//span[text()='Delete from Service'])[2]";

		// !-- Add Interface --="@xpath=
		public static final String interfaces_header = "@xpath=//div[text()='Interfaces']";
		public static final String interfacepanel_actiondropdown = "@xpath=//div[div[text()='Interfaces']]/parent::div//button[@id='dropdown-basic-button']";
		public static final String addinterface_link = "@xpath=//a[text()='Add Interface/Link']";
		public static final String addmultilink_link = "@xpath=//a[text()='Add Multlink']";
		public static final String addinterface_header = "@xpath=//div[contains(text(),'Add')]";
		public static final String addmultilink_header = "@xpath=//div[text()='Add  Multilink']";
		public static final String configureinterface_checkbox = "@xpath=//input[@id='configInterfaceOnDevice']";
		public static final String interface_warngmsg = "@xpath=//input[@id='interfaceName']/parent::div//div";
		public static final String bearertype_warngmsg = "@xpath=//div[contains(text(),'Bearer Type')]";
		public static final String bandwidth_warngmsg = "@xpath=//div[contains(text(),'Bandwidth')]";
		public static final String encapsulation_warngmsg = "@xpath=//div[contains(text(),'Encapsulation')]";
		public static final String slot_warngmsg = "@xpath=//div[contains(text(),'Slot')]";
		public static final String pic_warngmsg = "@xpath=//div[label[contains(text(),'Pic')]]/div";
		public static final String port_warngmsg = "@xpath=//div[label[contains(text(),'Port')]]/div";
		public static final String stm1number_warngmsg = "@xpath=//div[label[contains(text(),'STM1 Number')]]/div";
		// public static final String
		// interface_warngmsg="@xpath=//input[@id='interfaceName']/following-sibling::div";

		public static final String interfacename_textfield = "@xpath=//input[@id='interfaceName']";
		public static final String network_fieldvalue = "@xpath=//label[text()='Network']/parent::div//select";
		public static final String getaddress1_button = "@xpath=(//span[text()='Get Address'])[1]";
		public static final String getaddress2_button = "@xpath=(//span[text()='Get Address'])[2]";
		public static final String interfacerange_Address_dropdown = "@xpath=(//label[text()='Interface Address Range']/parent::div//input)[1]";
		public static final String interfacerange_AddressIpv6_dropdown = "@xpath=(//label[text()='Interface Address Range IPV6']/parent::div//input)[1]";
		public static final String interfacerange_AddressIpv6_dropdownMultilink = "@xpath=//label[text()='Interface Address Range IPV6']/parent::div//select";
		public static final String interfaceaddress_Addarrow = "@xpath=(//button[@type='button']/span[text()='>>'])[1]";
		public static final String interfaceaddressIPv6_Addarrow = "@xpath=(//button[@type='button']/span[text()='>>'])[2]";
		public static final String eipallocation1_button = "@xpath=(//button[@type='button']/span[text()='EIP Allocation'])[1]";
		public static final String eipallocation2_button = "@xpath=(//button[@type='button']/span[text()='EIP Allocation'])[2]";
		public static final String networkipv6_fieldvalue = "@xpath=//label[text()=' Network']//following-sibling::div";
		public static final String networkipv6_fieldvalueMutilink = "@xpath=//div[text()='WAN (XFER)']";
		public static final String address_textfield = "@xpath=(//div[label[text()='Address']]//input)[1]";
		public static final String addressIPv6_textfield = "@xpath=(//div[label[text()='Address IPv6']]//input)[1]";
		public static final String link_textfield = "@xpath=//input[@id='link']";
		public static final String bearertype_dropdown = "@xpath=//label[text()='Bearer Type']/parent::div//select";
		public static final String bearertype_dropdownEdit = "@xpath=//label[text()='Bearer Type']/parent::div//select";
		public static final String bandwidth_dropdown = "@xpath=//label[text()='Bandwidth']/parent::div//select";
		public static final String encapsulation_dropdown = "@xpath=//label[text()='Encapsulation']/parent::div//select";
		public static final String encapsulation_dropdownEdit = "@xpath=//label[text()='Encapsulation']/parent::div//select";
		public static final String cardtype_dropdown = "@xpath=//label[text()='Card Type']/parent::div//select";
		public static final String ciscovendor_interfaceaddressrange_textfield = "@xpath=//input[@name='interfaceAddressRange']";
		public static final String interfaceaddressrange_textfield = "@xpath=(//label[text()='Interface Address Range']/parent::div//input)[1]";
		public static final String interfaceaddressrange_textfield1 = "@xpath=(//label[text()='Interface Address']/parent::div//input)[1]";
		public static final String interfaceaddressrangeIPv6_textfield = "@xpath=(//label[text()='Interface Address Range IPV6']/parent::div//input)[1]";
		public static final String eipsubnetallocation_header = "@xpath=//div[text()='EIP Subnet Allocation']";
		public static final String subnettype_value = "@xpath=//label[text()='Subnet Type ']/parent::div/following-sibling::div";
		public static final String subnettype_valueMultilink = "@xpath=//label[text()='Sub Net Size']/following-sibling::select";
		public static final String eipallocation_citydropdown = "@xpath=//select[@id='city']";
		public static final String eipallocation_subnetsize = "@xpath=//select[@id='subNetSize']";
		public static final String eipallocation_availablepools_value = "@xpath=//label[contains(text(),'Available Pools')]/parent::div/following-sibling::div";
		public static final String allocatesubnet_button = "@xpath=//span[text()='Allocate Subnet']";
		public static final String eipallocationIPv6_header = "@xpath=//div[text()='EIP Address Allocation For Interface  ']";
		public static final String eipallocation_spacename = "@xpath=//label[text()=' Space Name ']/parent::div/following-sibling::div";
		public static final String eipallocation_ipv6_subnetsize = "@xpath=//label[text()='Sub Net Size']/parent::div//select";
		public static final String availableblocks_dropdown = "@xpath=//label[text()='Available Blocks']/parent::div//select";
		public static final String bgp_checkbox = "@xpath=//input[@id='bgp']";
		public static final String configuration_header = "@xpath=//div[text()='Configuration']";
		public static final String generateconfiguration_button = "@xpath=//button/span[text()='Generate Configuration']";
		public static final String GenerateConfigurationButton2 = "@xpath=//button[text()='Generate Configuration']";
		public static final String configuration_textarea = "@xpath=//textarea[@name='generateConfigText']";
		public static final String saveconfiguration_button = "@xpath=//span[text()='Save Configuration to File']";
		public static final String executeandsave_button = "@xpath=//span[text()='Execute and Save']";
		public static final String framingtype_dropdown = "@xpath=//select[@id='framingType']";
		public static final String vlanid_textfield = "@xpath=//input[@id='VLANId']";
		public static final String unitid_textfield = "@xpath=//label[text()='Unit ID']/parent::div//input";
		public static final String slot_textfield = "@xpath=//label[text()='Slot']/parent::div//input";
		public static final String pic_textfield = "@xpath=//label[text()='Pic']/parent::div//input";
		public static final String port_textfield = "@xpath=//label[text()='Port']/parent::div//input";
		public static final String clocksource_dropdown = "@xpath=//select[@id='clockSource']";
		public static final String bearerno_textfield = "@xpath=//label[text()='Bearer No']/parent::div//input";
		public static final String STM1Number_textfield = "@xpath=//label[text()='STM1 Number']/parent::div//input";
		public static final String ivmanagement_checkbox = "@xpath=//input[@id='ivManagement']";
		public static final String atricaconnected_checkbox = "@xpath=//input[@id='atricaConnected']";
		public static final String bgptemplate_dropdown = "@xpath=//label[text()='BGP Templates Generate For']/parent::div//input";
		public static final String cpewan_textfield = "@xpath=//input[@id='cpeWAN']";
		public static final String cpewanipv6_textfield = "@xpath=//input[@id='cpeWANIpv6Address']";
		public static final String descriptionfield = "@xpath=//input[@id='description']";
		public static final String bgp_ascustomerfield = "@xpath=//input[@id='asCustomer']";
		public static final String bgppassword_field = "@xpath=//input[@id='bgpPassword']";

		public static final String interface_column = "@xpath=//span[text()='Interface']";
		public static final String link_column = "@xpath=//span[text()='Link / Circuit Id']";
		public static final String interfaceaddressrange_column = "@xpath=//span[text()='Interface Address Range']";
		public static final String interfaceaddress_column = "@xpath=//span[text()='Interface Address']";
		public static final String bearertype_column = "@xpath=//span[text()='Bearer Type']";
		public static final String bandwidth_column = "@xpath=//span[text()='Bandwidth']";
		public static final String vlanid_column = "@xpath=//span[text()='VLAN Id']";
		public static final String ifinoctets_column = "@xpath=//span[text()='IfInOctets']";
		public static final String interfacename_value = "@xpath=//div[@role='gridcell'][@col-id='interfaceName']";
		public static final String link_value = "@xpath=//div[@role='gridcell'][@col-id='linkCircuit']";
		public static final String interfaceaddressrange_value = "@xpath=//div[@role='gridcell'][@col-id='addressRange']";
		public static final String interfaceaddress_value = "@xpath=//div[@role='gridcell'][@col-id='address']";
		public static final String bearertype_value = "@xpath=//div[@role='gridcell'][@col-id='bearerType']";
		public static final String bandwidth_value = "@xpath=//div[@role='gridcell'][@col-id='bandwidthDesc']";
		public static final String vlanid_value = "@xpath=//div[@role='gridcell'][@col-id='vlanId']";
		public static final String ifinoctets_value = "@xpath=//div[@role='gridcell'][@col-id='ifInOctets']";

		public static final String selectinterface = "@xpath=//div[div[@role='gridcell'][@col-id='interfaceName']/div[contains(text(),'value')]]//div//span[contains(@class,'unchecked')]";
		public static final String selectinterface2 = "@xpath=//div[div[@role='gridcell'][@col-id='interfaceName']/div[text()='value']]//div//span[contains(@class,'unchecked')]";
		public static final String showinterfaces_link = "@xpath=//span[text()='Show Interfaces']";
		public static final String ViewService_InterfaceAction1 = "@xpath=(//div[div[text()='Provider Equipment (PE)']]/parent::div//button[text()='Action'])[1]";
		public static final String addeddevice_interface_actiondropdown = "@xpath=//div[div[div[b[text()='value']]]]/following-sibling::div//button[text()='Action']";
		public static final String addeddevice_interface_actiondropdown1 = "@xpath=//div[div[div[b[text()='";
		public static final String addeddevice_interface_actiondropdown2 = "@xpath=']]]]/following-sibling::div//button[text()='Action']";

		public static final String editinterface_header = "@xpath=//div[@class='heading-green-row row']//div[text()='Edit']";
		public static final String ipsubnetipv6_addbutton = "@xpath=//label[text()='IP Subnet IPv6']/parent::div/parent::div/following-sibling::div//span[text()='Add']";
		public static final String ipsubnetipv4_addbutton = "@xpath=//label[text()='IP Subnet IPv4']/parent::div/parent::div/following-sibling::div//span[text()='Add']";
		public static final String ipsubnetipv4_textfield = "@xpath=//label[text()='IP Subnet IPv4']/following-sibling::input";
		public static final String ipsubnetipv6_textfield = "@xpath=//label[text()='IP Subnet IPv6']/following-sibling::input";
		public static final String ipsubnetipv6_removebutton = "@xpath=//label[text()='IP Subnet IPv6']/parent::div/parent::div/following-sibling::div//span[text()='Remove']";
		public static final String ipsubnetipv4_removebutton = "@xpath=//label[text()='IP Subnet IPv4']/parent::div/parent::div/following-sibling::div//span[text()='Remove']";

		public static final String multilink_text = "@xpath=//label[text()='Multilink']";
		public static final String multilinkedbearers_header = "@xpath=//div[text()=' Multilink Bearers']";
		public static final String checktoaddinterface_column = "@xpath=//span[@role='columnheader'][text()='Check to add to Multilink']";
		public static final String multilink_interface_column = "@xpath=//span[@role='columnheader'][text()='Interface']";
		public static final String multilink_link_column = "@xpath=//span[@role='columnheader'][text()='Link / Circuit Id']";
		public static final String multilink_BearerType_column = "@xpath=//span[@role='columnheader'][text()='Bearer Type']";
		public static final String multilink_bandwidth_column = "@xpath=//span[@role='columnheader'][text()='Bandwidth']";
		public static final String multilink_vlanid_column = "@xpath=//span[@role='columnheader'][text()='VLAN Id']";
		public static final String multilink_ifinoctets_column = "@xpath=//span[@role='columnheader'][text()='IfInOctets']";

		public static final String addedinterfaces = "@xpath=//div[text()='Interfaces']/parent::div/following-sibling::div//div[@ref='eBodyContainer']";
		public static final String interface_rowid = "@xpath=//div[@col-id='interfaceName'][text()='value']/parent::div";
		public static final String interfacename_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='interfaceName']";
		public static final String link_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='linkId']";
		public static final String interfaceaddressrange_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='interfaceAddressRange']";
		public static final String interfaceaddress_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='interfaceAddress']";
		public static final String bearertype_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='bearerType']";
		public static final String bandwidth_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='bandWidth']";
		public static final String vlanid_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='vlanId']";
		public static final String ifinoctets_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='ifInOctets']";
		// public static final String
		// selectinterface="@xpath=//div[@role='gridcell'][@col-id='interfaceName'][text()='value']/preceding-sibling::div//span[contains(@class,'unchecked')]";
		public static final String multilink_bandwidth_dropdown = "@xpath=//label[text()='Bandwidth']/parent::div//select";
		public static final String multilink_bgptemplate_dropdown = "@xpath=//label[text()='BGP Templates Generate For']/parent::div//input";
		public static final String eipallocation_popupclose = "@xpath=//div[text()='EIP Address Allocation For Interface  ']/following-sibling::button/span[text()='�']";

		public static final String AddInterfaceSucessMessage = "@xpath=//span[text()='Interface successfully created.']";
		public static final String UpdateInterfaceSucessMessage = "@xpath=//span[text()='Interface successfully updated.']";
		public static final String DeleteInterfaceSucessMessage = "@xpath=//span[text()='Interface successfully removed from this service.']";
		public static final String AddMultilinkSucessMessage = "@xpath=//span[text()='Interface successfully created.']";
		public static final String UpdateMultilinkSucessMessage = "@xpath=//span[text()='Interface successfully updated.']";
		public static final String DeleteMultilinkSucessMessage = "@xpath=//span[text()='Interface successfully removed from this service.']";

		// String !-- Select Interfaces --="@xpath=
		public static final String labelname_managementAddresss = "@xpath=//label[text()='Management Address']";
		public static final String InterfaceInselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceInselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceInselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceInselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceInselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";
		public static final String InterfaceInselect_removebuton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";
		public static final String InterfaceToselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceToselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceToselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceToselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";
		public static final String InterfaceToselect_addbuton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";
		public static final String InterfaceToselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceToselect_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String InterfaceInSelect_RemoveSuccessMessage = "@xpath=//span[text()='Interface removed Successfully']";
		public static final String selectinterface_link = "@xpath=//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'value')]]]//span[text()='Select Interfaces']";
		public static final String interfacesinservice_list = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String interfacesToSelect_header = "@xpath=//div[text()='Interfaces to Select']";
		public static final String InteraceColumn_Filter = "@xpath=(//div[div[text()='Interfaces to Select']]//following-sibling::div//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String InterfacefilterTxt = "@xpath=//div[text()='Interfaces to Select']/parent::div/parent::div//div[@class='ag-menu']//div[@class='ag-filter-body']//input[@id='filterText']";
		public static final String InterfaceInService_panelHeader = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]";
		public static final String interface_gridselect = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='value']";
		public static final String InterfaceToSelect_interfaceColumnHeader = "@xpath=//div[div[text()='Interfaces to Select']]//following-sibling::div//span[text()='Interface']";
		public static final String InterfacetoSelect_listOFinterfaceValuesDisplaying = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String interfacesinservice_filter = "@xpath=(//div[div[text()='Interfaces in Service']]//following-sibling::div//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String interfaceinservice_fitertext = "@xpath=//div[text()='Interfaces in Service']/parent::div/parent::div//div[@class='ag-menu']//div[@class='ag-filter-body']//input[@id='filterText']";
		public static final String interfaceinservice_gridselect = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='value']";
		public static final String AddSelectInterfaceSuccessMessage = "@xpath=//span[contains(text(),'Interface Added Successfully.')]";
		public static final String RemoveSelectInterfaceSuccessMessage = "@xpath=//span[contains(text(),'Interface removed Successfully')]";

		// !-- Interface Config history panel --="@xpath=
		public static final String interfaceconfighistory_header = "@xpath=//div[text()='Interface Configuration History']";
		public static final String date_column = "@xpath=//span[@role='columnheader'][text()='Date']";
		public static final String filename_column = "@xpath=//span[@role='columnheader'][text()='File Name']";

		// !-- Multilink --="@xpath=
		public static final String multilink_rowid = "@xpath=//div[@col-id='interfaceName'][text()='value']/parent::div";
		public static final String multilinkbearer_tabledata = "@xpath=//div[text()=' Multilink Bearers']/parent::div/following-sibling::div//div[@ref='eBodyContainer']";
		public static final String checktoaddinterface = "@xpath=//div[@col-id='interfaceName'][text()='value']";

		public static final String breadcrumb = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'value')]";
		public static final String errormessage = "@xpath=//div[contains(@class,'alert-danger')]";

		// !-- success Message --="@xpath=
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

		// !-- Router Tool commands For PE & CPE --="@xpath=
		public static final String RouterTool_header = "@xpath=//div/div[text()='Router Tools']";
		public static final String PE_CPE_CommandIPV4_header = "@xpath=//label[text()='Command IPV4']";
		public static final String PE_CPE_Router_IPV4CommandsDropdown = "@xpath=//div[label[text()='Command IPV4']]//input";
		public static final String PE_CPE_Router_IPV4CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String PE_CPE_Router_IPV4Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[1]";
		public static final String PE_CPE_commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String PE_CPE_commandIPv4_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[1]";

		public static final String PE_CPE_Router_IPV6CommandsDropdown = "@xpath=//div[label[text()='Command IPV6']]//input";
		public static final String PE_CPE_Router_IPV6CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String PE_CPE_Router_IPV6Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[2]";
		public static final String PE_CPE_commandIPv6_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String PE_CPE_commandIPv6_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[2]";
		public static final String PE_CPE_result_textArea = "@xpath=//div[label[text()='Result']]//textarea";
		// public static final String
		// PE_View_VendorModelValue="@xpath=//label[text()='Vendor/Model']/following-sibling::div";
		// public static final String
		// PE_View_ManagementAddressValue="@xpath=//label[text()='Management
		// Address']/following-sibling::div";

		public static final String CPE_deletefromservicedevice1 = "@xpath=(//div[div[div[text()='Customer Premise Equipment (CPE)']]]//span[text()='Delete from Service'])[1]";
		public static final String CPE_deletefromservicedevice2 = "@xpath=(//div[div[div[text()='Customer Premise Equipment (CPE)']]]//span[text()='Delete from Service'])[2]";

		// !-- Customer Premise Equipment (CPE) --="@xpath=
		public static final String existingCPEdevicegrid = "@xpath=//div[text()='Customer Premise Equipment (CPE)']/parent::div";
		public static final String CPE_fetchAlldevice_InviewPage = "@xpath=//div[text()='Customer Premise Equipment (CPE)']/parent::div/following-sibling::div/div[@class='div-margin row']//b";

		// !-- Customer Premise Equipment (CPE) panel in view service page
		// --="@xpath=
		public static final String CustomerPremiseEquipment_CPE_Panelheader = "@xpath=//div[contains(text(),'Customer Premise Equipment (CPE)')]";
		public static final String CPE_viewdevice1 = "@xpath=(//div[div[text()='Customer Premise Equipment (CPE)']]/following-sibling::div//span//a[text()='View'])[1]";
		public static final String CPE_viewdevice2 = "@xpath=(//div[div[text()='Customer Premise Equipment (CPE)']]/following-sibling::div//span[text()='View'])[2]";

		// !-- ADD CPE DEVICE --="@xpath=
		public static final String commonDropdownValueTag = "@xpath=//div[@class='sc-htpNat AUGYd']/div";
		public static final String AddCPEDeviceLink = "@xpath=//a[text()='Add CPE Device']";
		public static final String EditCPE_OKButton = "@xpath=//span[text()='OK']";
		public static final String AddCPE_CancelButton = "@xpath=//span[text()='Cancel']";
		public static final String AddCPE_NextButton = "@xpath=//span[text()='Next']";

		public static final String AddCPE_WarningMessage_DeviceName = "@xpath=//div[text()='Name']";
		public static final String AddCPE_WarningMessage_VendorModel = "@xpath=//div[contains(text(),'Vendor/Model')]";
		public static final String AddCPE_WarningMessage_Country = "@xpath=//div[contains(text(),'Country')]";

		public static final String AddCPEDevicePageHeader = "@xpath=//div[contains(text(),'CPE Device')]";
		public static final String CPE_DeviceNameTextfield = "@xpath=//label[text()='Name']/following-sibling::input";
		public static final String CPE_VendorModelDropdown = "@xpath=//label[text()='Vendor/Model']/following-sibling::select";
		public static final String CPE_ManagementAddressTextfield = "@xpath=//label[text()='Management Address']/following-sibling::input";
		public static final String CPE_SnmproTextfield = "@xpath=//label[text()='Snmpro']/following-sibling::input";
		public static final String CPE_CountryDropdown = "@xpath=//label[text()='Country']/following-sibling::select";
		public static final String CPE_CityDropdown = "@xpath=//label[text()='City']/following-sibling::select";
		public static final String CPE_SiteDropdown = "@xpath=//label[text()='Site']/following-sibling::select";
		public static final String CPE_PremiseDropdown = "@xpath=//label[text()='Premise']/following-sibling::select";
		public static final String CPE_Role1 = "@xpath=//label[text()='CPE Role 1']/following-sibling::select";
		public static final String CPE_Role2 = "@xpath=//label[text()='CPE Role 2']/following-sibling::select";
		public static final String CPE_AddCPEDeviceSuccessMessage = "@xpath=//span[text()='Device successfully created.']";
		public static final String CPE_UpdateCPEDeviceSuccessMessage = "@xpath=//span[text()='Device successfully updated.']";
		public static final String CPE_DeleteCPEDeviceSuccessMessage = "@xpath=//span[text()='Device successfully deleted.']";
		// public static final String
		// alertMsg="@xpath=(//div[@role='alert'])[1]";

		public static final String CPE_ViewDevice_Header = "@xpath=//div[contains(text(),'Device')]";
		public static final String CPE_EditDevice_Header = "@xpath=//div[contains(text(),'Edit')]";

		// !-- Below are the fields values information under view CPE Device
		// page --="@xpath=
		public static final String CPE_View_DeviceNameValue = "@xpath=//label[text()='Name']/following-sibling::div";
		public static final String CPE_View_VendorModelValue = "@xpath=//label[text()='Vendor/Model']/following-sibling::div";
		public static final String CPE_View_ManagementAddressValue = "@xpath=//label[text()='Management Address']/following-sibling::div";
		public static final String CPE_View_SnmproValue = "@xpath=//label[text()='Snmpro']/following-sibling::div";
		public static final String CPE_View_CountryValue = "@xpath=//label[text()='Country']/following-sibling::div";
		public static final String CPE_View_CityValue = "@xpath=//label[text()='City']/following-sibling::div";
		public static final String CPE_View_SiteValue = "@xpath=//label[text()='Site']/following-sibling::div";
		public static final String CPE_View_PremiseValue = "@xpath=//label[text()='Premise']]/following-sibling::div";
		public static final String CPE_View_CPERole1Value = "@xpath=//label[text()='CPE Role 1']/following-sibling::div";
		public static final String CPE_View_CPERole2Value = "@xpath=//label[text()='CPE Role 2']/following-sibling::div";

		public static final String CPE_View_ActionLink = "@xpath=//div[div[text()='Device']]/parent::div//button[text()='Action']";
		public static final String CPE_View_Action_EditLink = "@xpath=//div//a[text()='Edit']";
		public static final String CPE_View_Action_DeleteLink = "@xpath=//div//a[text()='Delete']";

		public static final String CPE_View_TestColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Test']";
		public static final String CPE_View_StatusColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Status']";
		public static final String CPE_View_LastRefresh = "@xpath=//div/div[text()='Last Refresh:']";
		public static final String CPE_Edit_PremiseLevel = "@xpath=//div//label[text()='Premise']";
		public static final String CPE_ViewDevice_Action_DeletePEDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete this item?']";
		public static final String CPE_ViewDevice_Action_DeleteButton = "@xpath=//button//span[text()='Delete']";

		// !-- Below Fetch Device Interface ==="@xpath= Manage Colt Page for PE
		// Device --="@xpath=
		public static final String CPE_View_Action_FetchDeviceInterfacesLink = "@xpath=//div//a[text()='Fetch Device Interfaces']";
		public static final String CPE_FetchDeviceInterfacesSuccessMessage = "@xpath=//span[contains(text(),'Interfaces are fetched successfully.')]";
		public static final String CPE_hereLink_UnderFetchDeviceInterfacesSuccessMessage = "@xpath=//span[contains(text(),'Interfaces are fetched successfully')]//following-sibling::a[text()='here']";
		public static final String CPE_ManageCOLTsNetworkManageNetwork_header = "@xpath=//div[contains(text(),'Manage COLT')]";
		public static final String CPE_Manage_Status_DeviceValue = "@xpath=(//div[div[text()='Status']]/following-sibling::div[2]/div)[1]";
		public static final String CPE_Manage_Status_StatusValue = "@xpath=(//div[div[text()='Status']]/following-sibling::div[2]/div)[2]";
		public static final String CPE_Manage_Status_LastModificationValue = "@xpath=(//div[div[text()='Status']]/following-sibling::div[2]/div)[3]";
		public static final String CPE_Manage_Status_StatusLink = "@xpath=(//div[div[text()='Status']]/following-sibling::div[2]/div)[4]/a[1]";
		public static final String CPE_Manage_Status_ViewInterfacesLink = "@xpath=(//div[div[text()='Status']]/following-sibling::div[2]/div)[4]/a[2]";
		public static final String CPE_Manage_Status_Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String CPE_Device_Status_OK = "@xpath=//button/span[text()='OK']";
		public static final String CPE_servicestatus_popupclose = "@xpath=(//div[text()='Device']/following-sibling::button//span)[1]";
		public static final String CPE_servicestatushistory_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String CPE_Status_successMessage = "@xpath=//div[@role='alert']//span";
		public static final String CPE_servicestatushistoryValue = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String CPE_Manage_Synchronization_DeviceValue = "@xpath=(//div[div[text()='Synchronization']]/following-sibling::div[2]/div)[1]";
		public static final String CPE_Manage_Synchronization_SyncStatusValue = "@xpath=(//div[div[text()='Synchronization']]/following-sibling::div[2]/div)[2]";
		public static final String CPE_Manage_Synchronization_SmartsValue = "@xpath=(//div[div[text()='Synchronization']]/following-sibling::div[2]/div)[3]";
		public static final String CPE_Manage_Synchronization_FetchInterfacesValue = "@xpath=(//div[div[text()='Synchronization']]/following-sibling::div[2]/div)[4]";
		public static final String CPE_Manage_Synchronization_VistaMartDeviceValue = "@xpath=(//div[div[text()='Synchronization']]/following-sibling::div[2]/div)[5]";
		public static final String CPE_Manage_Synchronization_SynchronizeLink = "@xpath=(//div[div[text()='Synchronization']]/following-sibling::div[2]/div)[6]";
		public static final String CPE_Manage_SynchronizeSuccessMessage = "@xpath=//div[@role='alert']//span";
		public static final String ServiceIDonTopOfTheBredcomeNavigation_VOIP = "@xpath=//li/a[text()='VOIP-21-Feb']";

		// !-- Router Tool For CPE Device --="@xpath=
		public static final String CPE_Router_IPV4CommandsDropdown = "@xpath=(//input[@placeholder='Select ...'])[1]";
		public static final String CPE_Router_IPV4CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String CPE_Router_IPV4Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[1]";
		public static final String CPE_Router_IPV6CommandsDropdown = "@xpath=(//input[@placeholder='Select ...'])[2]";
		public static final String CPE_Router_IPV6CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String CPE_Router_IPV6Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[2]";
		public static final String CPE_InterfacesPanel_header = "@xpath=//div[text()='Interfaces']";

		// !-- Add Route panel --="@xpath=
		public static final String Routes_Header = "@xpath=//div[text()='Routes']";
		public static final String Routes_Actionbutton = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']";
		public static final String Routes_AddLink = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Add']";
		public static final String Routes_EditLink = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Edit']";
		public static final String Routes_DeleteLink = "@xpath=//div[div[text()='Routes']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Delete']";
		public static final String Routes_DeleteButton = "@xpath=//button//span[text()='Delete']";
		public static final String Routes_AddRoutes_Header = "@xpath=//div[text()='Add Route']";
		public static final String Routes_EditRoutes_Header = "@xpath=//div[text()='Edit Route']";
		public static final String Routes_EIPAllocationLink = "@xpath=//span[text()='EIP Allocation']";
		public static final String Routes_EIPSubnetAllocationforService_Header = "@xpath=//div[contains(text(),'EIP Subnet Allocation for Service ')]";
		public static final String Routes_EIPAllocation_SubnetTypeValue = "@xpath=//div[contains(text(),'CUST')]";
		public static final String Routes_EIPAllocation_CityWarningMessage = "@xpath=//div[text()='City']";
		public static final String Routes_EIPAllocation_SubnetSizeWarningMessage = "@xpath=//div[text()='Sub Net Size']";
		public static final String Routes_EIPAllocation_AvilablePoolWarningMessage = "@xpath=//div[text()='Available Pools']";
		public static final String CPE_Routes_EIPAllocation_SubnetTypeValue = "@xpath=(//div[label[text()='Subnet Type']]/following-sibling::div)[1]";
		public static final String Routes_EIPAllocation_CityDropdown = "@xpath=//div[label[text()='City']]/parent::div//input";
		public static final String Routes_EIPAllocation_SubnetSizeDropdown = "@xpath=//div[label[text()='Sub Net Size']]/parent::div//input";
		public static final String Routes_EIPAllocation_AvailablePoolsMessage = "@xpath=//div[div[label[text()='Available Pools']]]/span";
		public static final String Routes_EIPAllocation_CloseButton = "@xpath=//button/span[text()='�']";
		public static final String EditRoutes_EIPAllocation_CloseButton = "@xpath=(//button/span[text()='�'])[2]";
		public static final String Routes_Gatewaytextfield = "@xpath=//input[@id='gateway']";
		public static final String Routes_NetworkAddresstextfield = "@xpath=//input[@id='destination']";
		public static final String Routes_NetworkMAStextfield = "@xpath=//input[@id='networkMask']";
		public static final String Routes_Metricstextfield = "@xpath=//input[@id='metrics']";
		public static final String Routes_OKButton = "@xpath=//span[text()='OK']";
		public static final String Routes_Gatway_WarningMessage = "@xpath=//div[contains(text(),'Gateway')]";
		public static final String Routes_NetworkAddress_WarningMessage = "@xpath=//div[contains(text(),'Network Address')]";
		public static final String Routes_NetworkMASK_WarningMessage = "@xpath=//div[contains(text(),'Network Mask')]";
		public static final String Routes_AllocateSubnetButon = "@xpath=//span[text()='Allocate Subnet']";
		public static final String AddRouteSuccessMessage = "@xpath=//span[text()='Static Route successfully created.']";
		public static final String UpdateRouteSuccessMessage = "@xpath=//span[contains(text(),'Static Route successfully updated.')]";
		public static final String DeleteRouteSuccessMessage = "@xpath=//span[contains(text(),'Static Route successfully deleted.')]";
		public static final String Routes_SelectRouteCheckbox = "@xpath=(//span[@class='ag-selection-checkbox']//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";

		// !-- Customer Readonly SNMP Panel --="@xpath=
		public static final String CustomerReadonlySNMP_panelHeader = "@xpath=//div[contains(text(),'Customer Readonly SNMP')]";
		public static final String SNMP_Actionbutton = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']";
		public static final String SNMP_AddLink = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Add']";
		public static final String SNMP_EditLink = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Edit']";
		public static final String SNMP_DeleteLink = "@xpath=//div[div[text()='Customer Readonly SNMP']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Delete']";
		public static final String SNMP_DeleteButton = "@xpath=//button//span[text()='Delete']";
		public static final String AddSNMP_Header = "@xpath=//div[contains(text(),'Add Customer Readonly SNMP')]";
		public static final String EditSNMP_Header = "@xpath=//div[contains(text(),'Edit Customer Readonly SNMP')]";
		public static final String SNMP_CustomerIPAddress_Textfield = "@xpath=//input[@id='snmpInfoAddress']";
		public static final String SNMP_CustomerCommunityString_Textfield = "@xpath=//input[@id='snmpInfoCommunity']";
		public static final String SNMP_CustomerIPAddress_WarningMessage = "@xpath=//div[text()='Customer Ip Address']";
		public static final String SNMP_CustomerCommunityString_WarningMessage = "@xpath=//div[text()='Customer Community String']";
		public static final String SNMP_OKButton = "@xpath=//span[text()='OK']";
		public static final String AddSNMPSuccessMessage = "@xpath=//span[contains(text(),'Customer Readonly SNMP successfully created.')]";
		public static final String UpdateSNMPSuccessMessage = "@xpath=//span[contains(text(),'Customer Readonly SNMP successfully updated.')]";
		public static final String DeleteSNMPSuccessMessage = "@xpath=//span[contains(text(),'Customer Readonly SNMP successfully deleted.')]";
		public static final String SNMP_SelectRouteCheckbox = "@xpath=(//*[@id='root']/div/div/div[2]/div/div[1]/div[1]/div[7]/div/div[2]/div/div/div/div[2]/div/div[1]/div/div[3]/div[2]/div/div/div[1]/div[1]/span/span[1]/span[2])[1]";

		// !-- PI Ranges Panel --="@xpath=
		public static final String PIRangesPanel_Header = "@xpath=//div[contains(text(),'PI Ranges')]";
		public static final String PIRanges_Action = "@xpath=//div[contains(text(),'PI Ranges')]/parent::div//button";
		public static final String PIRanges_OKButton = "@xpath=//span[text()='OK']";
		public static final String PIRanges_AddLink = "@xpath=//div[div[contains(text(),'PI Ranges')]]/parent::div//a[text()='Add']";
		public static final String PIRanges_DeleteLink = "@xpath=//div[div[contains(text(),'PI Ranges')]]/parent::div//a[text()='Delete']";
		public static final String PIRanges_DeleteButton = "@xpath=//button//span[text()='Delete']";
		public static final String AddPIRanges_Header = "@xpath=(//div[text()='PI Ranges'])[2]";
		public static final String PIAddressRangeTextfield = "@xpath=//input[@id='piRangescombined']";
		public static final String PIAddressRange_WarningMessage = "@xpath=//div[text()='PI Ranges']";
		public static final String AddPIRangesSuccessMessage = "@xpath=//span[contains(text(),'PI Range successfully inserted.')]";
		public static final String DeletePIRangesSuccessMessage = "@xpath=//span[contains(text(),'PI Range successfully deleted.')]";
		public static final String PIRangesCheckbox = "@xpath=(//div[8]//div[1]//div[2]//div[1]//div[1]//div[1]//div[2]//div[1]//div[1]//div[1]//div[3]//div[2]//div[1]//div[1]//div[1]//div[1]//span[1]//span[1]//span[2])[1]";

		// !-- Extra Subnets Panels --="@xpath=
		public static final String ExtraSubnets_PanelHeader = "@xpath=//div[contains(text(),'Extra Subnets')]";
		public static final String ExtraSubnets_Action = "@xpath=//div[contains(text(),'Extra Subnet')]/parent::div//button";
		public static final String AllocateExtraSubnets_Link = "@xpath=//a[contains(text(),'Allocate Extra Subnet')]";
		public static final String AddExtraSubnets_Header = "@xpath=(//div[contains(text(),'Extra Subnet')]/parent::div//div[text()='Extra Subnets'])[2]";
		public static final String ExtraSubnets_SubnetTypeValue = "@xpath=//div[contains(text(),'CUST')]";
		public static final String ExtraSubnets_CityDropdown = "@xpath=//div[label[text()='City']]/parent::div//input";
		public static final String ExtraSubnets_SubnetSizeDropdown = "@xpath=//div[label[text()='Subnet Size']]/parent::div//input";
		public static final String ExtraSubnets_ValueSelection = "@xpath=(//div[@class='sc-htpNat AUGYd']/div)[1]";
		public static final String ExtraSubnets_AvailablePoolsMessage = "@xpath=//div[div[label[text()='Available Pools']]]/div[@class='customLabelValue form-label']";
		public static final String ExtraSubnets_CloseButton = "@xpath=//button/span[text()='�']";
		public static final String ExtraSubnets_City_WarningMessage = "@xpath=//div[text()='City']";
		public static final String ExtraSubnets_SubnetSize_WarningMessage = "@xpath=//div[text()='Subnet Size']";
		public static final String ExtraSubnets_EIPAllocation_AvilablePoolWarningMessage = "@xpath=//div[text()='Available Pools']";
		public static final String ExtraSubnets_AllocateSubnetButon = "@xpath=//span[text()='Allocate Subnet']";
		public static final String Extra_ValueSelection = "@xpath=";

		// !-- NAT Configuration Panel --="@xpath=
		public static final String NATConfiguration_panelHeader = "@xpath=//div[contains(text(),'NAT Configuration')]";
		public static final String NAT_Actionbutton = "@xpath=//div[div[text()='NAT Configuration']]/parent::div//button[text()='Action']";
		public static final String NAT_EditLink = "@xpath=//div[div[text()='NAT Configuration']]/parent::div//button[text()='Action']/following-sibling::div/a[text()='Edit']";
		public static final String EditNAT_Header = "@xpath=//div[contains(text(),'NAT Configuration') and @class='modal-title h4']";
		public static final String StaticNAT_Checkbox = "@xpath=//input[@id='staticNatFlag']";
		public static final String DynamicNAT_Checkbox = "@xpath=//input[@id='dynamicNatFlag']";
		public static final String NAT_OKButton = "@xpath=//span[contains(text(),'OK')]";
		public static final String UpdateNATSuccessMessage = "@xpath=//span[contains(text(),'Nat Configuration successfully updated.')]";

		// !-- Static NAT Mapping Panel --="@xpath=
		public static final String StaticNATMapping_panelHeader = "@xpath=//div[contains(text(),'Static NAT Mapping')]";
		public static final String StaticNAT_Actionbutton = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']";
		public static final String StaticNAT_AddLink = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Add']";
		public static final String StaticNAT_Editink = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";
		public static final String StaticNAT_DeleteLink = "@xpath=//div[div[text()='Static NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete']";
		public static final String AddStaticNAT_Header = "@xpath=//div[contains(text(),'Add Static NAT Mapping')]";
		public static final String EditStaticNAT_Header = "@xpath=//div[contains(text(),'Edit Static NAT Mapping')]";
		public static final String StaticNAT_Protocol_Dropdown = "@xpath=//div[label[text()='Protocol']]//input";
		public static final String StaticNAT_LocalPort_Textfield = "@xpath=//input[@id='natlocalPort']";
		public static final String StaticNAT_GlobalPort_Textfield = "@xpath=//input[@id='natglobalPort']";
		public static final String StaticNAT_LocalIP_Textfield = "@xpath=//input[@id='natlocalIp']";
		public static final String StaticNAT_GlobalIP_Textfield = "@xpath=//input[@id='natglobalIp']";
		public static final String StaticNAT_LocalPort_WarningMessage = "@xpath=//li[contains(text(),'Local Port is required when a protocol is selected')]";
		public static final String StaticNAT_GlobalPort_WarningMessage = "@xpath=//li[contains(text(),'Global Port is required when a protocol is selecte')]";
		public static final String StaticNAT_LocalIP_WarningMessage = "@xpath=//li[contains(text(),'Local IP is required.')]";
		public static final String StaticNAT_GlobalIP_WarningMessage = "@xpath=//li[contains(text(),'Global IP is required.')]";
		public static final String AddStaticNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully inserted.')]";
		public static final String UpdateStaticNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully updated.')]";
		public static final String DeleteStaticNATSuccessMessage = "@xpath=//span[contains(text(),'Static NAT successfully deleted.')]";
		public static final String SelectStaticNATCheckbox = "@xpath=(//div[@col-id='natLocalIp'][text()='value'])[1]";
		public static final String SelectStaticNATCheckbox1 = "@xpath=(//div[@col-id='natLocalIp'][text()='";
		public static final String SelectStaticNATCheckbox2 = "'])[1]";

		// !-- Dynamic NAT Mapping Panel --="@xpath=
		public static final String DynamicNATMapping_panelHeader = "@xpath=//div[contains(text(),'Dynamic NAT Mapping')]";
		public static final String DynamicNAT_Actionbutton = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']";
		public static final String DynamicNAT_AddLink = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Add']";
		public static final String DynamicNAT_Editink = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";
		public static final String DynamicNAT_DeleteLink = "@xpath=//div[div[text()='Dynamic NAT Mapping']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete ']";
		public static final String AddDynamicNAT_Header = "@xpath=//div[contains(text(),'Add Dynamic NAT Mapping')]";
		public static final String EditDynamicNAT_Header = "@xpath=//div[contains(text(),'Edit Dynamic NAT Mapping')]";
		public static final String DynamicNAT_PoolModeCheckbox = "@xpath=//label[text()='Pool Mode']";
		public static final String DynamicNAT_PoolLocalNetworkTextfield = "@xpath=//label[text()='Local Network']/following-sibling::input";
		public static final String DynamicNAT_PoolStartAddressTextfield = "@xpath=//label[text()='Pool Start Address']/following-sibling::input";
		public static final String DynamicNAT_PoolEndAddressTextfield = "@xpath=//label[text()='Pool End Address']/following-sibling::input";
		public static final String DynamicNAT_PoolPrefixDropdown = "@xpath=//label[text()='Pool Prefix']/following-sibling::select";
		public static final String DynamicNAT_PoolLocalNetworkWarningMessage = "@xpath=//div[contains(text(),'Local Network')]";
		public static final String DynamicNAT_PoolStartAddressWarningMessage = "@xpath=//div[contains(text(),'Pool Start Address')]";
		public static final String DynamicNAT_PoolEndAddressWarningMessage = "@xpath=//div[contains(text(),'Pool End Address')]";
		public static final String DynamicNAT_PoolPrefixWarningMessage = "@xpath=//div[contains(text(),'Pool Prefix')]";
		public static final String DynamicNAT_MapToInterfaceWarningMessage = "@xpath=//label[text()='Maps to Interface']/following-sibling::select";
		public static final String DynamicNAT_InterfaceModeCheckbox = "@xpath=//label[text()='Interface Mode']";
		public static final String DynamicNAT_InterfaceLocalNetwork = "@xpath=//label[text()='Local Network']/following-sibling::input";
		public static final String DynamicNAT_MapToInterfaceDropdown = "@xpath=//label[text()='Maps to Interface']/following-sibling::select";
		public static final String AddDynamicNATSuccessMessage = "@xpath=//span[contains(text(),'Dynamic NAT successfully inserted.')]";
		public static final String UpdateDynamicNATSuccessMessage = "@xpath=//span[contains(text(),'Dynamic NAT successfully updated.')]";
		public static final String DeleteDynamicNATSuccessMessage = "@xpath=//span[contains(text(),'Dynamic NAT successfully deleted.')]";
		public static final String SelectDynamicNATCheckbox = "@xpath=//div[contains(text(),'Dynamic NAT Mapping')]/parent::div//following-sibling::div//span[@class='ag-selection-checkbox']";

		// !-- DHCP Servers on CPE Panel --="@xpath=
		public static final String DHCP_panelHeader = "@xpath=//div[contains(text(),'DHCP Servers on CPE')]";
		public static final String DHCP_Actionbutton = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']";
		public static final String DHCP_AddLink = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Add']";
		public static final String DHCP_Editink = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";
		public static final String DHCP_DeleteLink = "@xpath=//div[div[text()='DHCP Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete']";
		public static final String AddDHCP_Header = "@xpath=//div[contains(text(),'Add DHCP Server on CPE')]";
		public static final String EditDHCP_Header = "@xpath=//div[contains(text(),'Edit DHCP Server on CPE')]";
		public static final String DHCP_CustomerLANSubnet_Textfield = "@xpath=//input[@id='subnet']";
		public static final String DHCP_SubnetMask_Textfield = "@xpath=//input[@id='subnetMask']";
		public static final String DHCP_PrimaryDNSServer_Textfield = "@xpath=//input[@id='nameserver1']";
		public static final String DHCP_SecondaryDNSServer_Textfield = "@xpath=//input[@id='nameserver2']";
		public static final String DHCP_CustomerLANSubnet_WarningMessage = "@xpath=//div[text()='Customer LAN Subnet']";
		public static final String DHCP_SubnetMask_WarningMessage = "@xpath=//div[text()='Subnet Mask']";
		public static final String DHCP_PrimaryDNSServer_WarningMessage = "@xpath=//div[text()='Primary DNS Server']";
		public static final String AddDHCPSuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully created.')]";
		public static final String UpdateDHCPSuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully updated.')]";
		public static final String DeleteDHCPSuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully deleted.')]";
		public static final String SelectDHCPCheckbox = "@xpath=//div[contains(text(),'DHCP Servers on CPE')]/parent::div//following-sibling::div//span[@class='ag-selection-checkbox']";

		// !-- DHCP IPV6 Servers on CPE Panel --="@xpath=
		public static final String DHCPIPV6_panelHeader = "@xpath=//div[contains(text(),'DHCP IPV6 Servers on CPE')]";
		public static final String DHCPIPV6_Actionbutton = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']";
		public static final String DHCPIPV6_AddLink = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Add']";
		public static final String DHCPIPV6_Editink = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Edit']";
		public static final String DHCPIPV6_DeleteLink = "@xpath=//div[div[text()='DHCP IPV6 Servers on CPE']]/parent::div//button[text()='Action']/parent::div//a[text()='Delete ']";
		public static final String AddDHCPIPV6_Header = "@xpath=//div[contains(text(),'Add IPV6 DHCP Server on CPE')]";
		public static final String EditDHCPIPV6_Header = "@xpath=//div[contains(text(),'Edit IPV6 DHCP Server on CPE')]";
		public static final String DHCPIPV6_IPV6DHCPType_Dropdown = "@xpath=//div[label[contains(text(),'IPV6 DHCP Type')]]//div/span";
		public static final String DHCPIPV6_SubnetMask_Textfield = "@xpath=//label[contains(text(),'IPV6 Subnet Mask')]/following-sibling::input";
		public static final String DHCPIPV6_LANIPV6Subnet_Textfield = "@xpath=//label[contains(text(),'LAN IPV6 Subnet')]/following-sibling::input";
		public static final String DHCPIPV6_DomainName_Textfield = "@xpath=//label[contains(text(),'Domain Name')]/following-sibling::input";
		public static final String DHCPIPV6_PrimaryDNSServer_Textfield = "@xpath=//label[contains(text(),'IPV6 DNS-Server IP (Primary)')]/following-sibling::input";
		public static final String DHCPIPV6_SecondaryDNSServer_Textfield = "@xpath=//label[contains(text(),'IPV6 DNS-Server IP (Secondary)')]/following-sibling::input";
		public static final String DHCPIPV6_LANIPV6Subnet_WarningMessage = "@xpath=//div[contains(text(),'LAN IPV6 Subnet')]";
		public static final String DHCPIPV6_SubnetMask_WarningMessage = "@xpath=//div[contains(text(),'IPV6 Subnet Mask')]";
		public static final String DHCPIPV6_PrimaryDNSServer_WarningMessage = "@xpath=//div[contains(text(),'IPV6 DNS-Server IP (Primary)')]";

		public static final String AddDHCPIPV6SuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully created.')]";
		public static final String UpdateDHCPIPV6SuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully updated.')]";
		public static final String DeleteDHCPIPV6SuccessMessage = "@xpath=//span[contains(text(),'DHCP server successfully deleted.')]";
		public static final String SelectDHCPIPV6Checkbox = "@xpath=//div[contains(text(),'DHCP IPV6 Servers on CPE')]/parent::div//following-sibling::div//span[@class='ag-selection-checkbox']";

		// !-- Configuration Panel --="@xpath=
		public static final String Configuration_panelHeader = "@xpath=//div[text()='Configuration']";
		public static final String ConfigurationTextfield = "@xpath=//div[div[text()='Configuration']]/following-sibling::div//textarea";
		public static final String DiscoveryHistoryLogs_panelHeader = "@xpath=//div[text()=' Discovery History Logs']";

		// !-- CPE Device Interface and Multilink --="@xpath=
		public static final String CPE_AddInterfaceHeader = "@xpath=//div[text()='Add']";
		public static final String CPE_AddMultilinkHeader = "@xpath=//div[text()='Add']";
		public static final String CPE_EIPallocation_header = "@xpath=//div[contains(text(),'EIP Subnet Allocation')]";
		public static final String CPE_EIPallocationIPv6_header = "@xpath=//div[text()='EIP Address Allocation For Interface ']";
		public static final String CPE_EIPallocationIPv6_CloseButton = "@xpath=//span[contains(text(),'�')]";
		public static final String CPE_NetworkDropdownIPV6 = "@xpath=(//label[text()='Network']/parent::div//select)[2]";
		public static final String CPE_AddthisasaPIRangeCheckbox = "@xpath=//div[label[contains(text(),'Add this as a PI Range')]]//input";
		public static final String CPE_AddthisasaPIRangeIPV6Checkbox = "@xpath=//div[label[contains(text(),'Add this as a Ipv6 PI Range')]]//input";
		public static final String CPE_EthernetCheckbox = "@xpath=//div[label[contains(text(),'Ethernet')]]//input";
		public static final String CPE_ClockSourceDropdown = "@xpath=//label[contains(text(),'Clock Source')]/following-sibling::select";
		public static final String CPE_SpeedDropdown = "@xpath=//label[contains(text(),'Speed')]/following-sibling::select";
		public static final String CPE_DuplexDropdown = "@xpath=//label[contains(text(),'Duplex')]/following-sibling::select";
		public static final String CPE_InterfaceIsWANCheckbox = "@xpath=(//div[label[contains(text(),'Interface is')]]/div/label/input)[1]";
		public static final String CPE_InterfaceIsLANCheckbox = "@xpath=(//div[label[contains(text(),'Interface is')]]/div/label/input)[2]";
		public static final String CPE_VRRPGroupTextfield = "@xpath=//input[@id='vrrpGroupText']";
		public static final String CPE_VRRPIPTextfield = "@xpath=//input[@id='vrrpIpText']";
		public static final String CPE_VRRPIPV6Textfield = "@xpath=//input[@id='vrrpIpv6Text']";
		public static final String CPE_VRRPPriorityTextfield = "@xpath=//input[@id='vrrpPriorityText']";
		public static final String CPE_VRRPPasswordTextfield = "@xpath=//input[@id='vrrpPassword']";
		public static final String CPE_GroupIPIPV6ForwardArrow = "@xpath=(//span[text()='>>'])[3]";
		public static final String CPE_GroupIPIPV6BackwardArrow = "@xpath=(//span[contains(text(),'')])";
		public static final String CPE_FailoverCableChekbox = "@xpath=//div[label[contains(text(),'Failover Cable')]]//input";
		public static final String CPE_LinkValueTextfield = "@xpath=";
		public static final String CPE_LinkDropdown = "@xpath=//div[label[contains(text(),'Link')]]//select";
		public static final String IVManagementCheckbox = "@xpath=//div[label[contains(text(),'IV Management')]]//input";
		public static final String CPE_EIPallocation_citydropdown = "@xpath=//div[label[contains(text(),'City')]]/div";
		public static final String CPE_EIPallocation_subnetsizeDropdown = "@xpath=//div[label[contains(text(),'Sub Net Size')]]/div";

		public static final String CPE_IPv4LANSUBNET_A = "@xpath=//label[contains(text(),'IPv4-LAN-SUBNET-A')]/following-sibling::input";
		public static final String CPE_IPv4LANSUBNET_B = "@xpath=//label[contains(text(),'IPv4-LAN-SUBNET-B')]/following-sibling::input";
		public static final String CPE_IPv6LANSUBNET_A = "@xpath=//label[contains(text(),'IPv6-LAN-SUBNET-A')]/following-sibling::input";
		public static final String CPE_IPv6LANSUBNET_B = "@xpath=//label[contains(text(),'IPv6-LAN-SUBNET-B')]/following-sibling::input";
		public static final String CPE_VRRP_V2Checkbox = "@xpath=//input[@value='v2']";
		public static final String CPE_VRRP_V3Checkbox = "@xpath=//input[@value='v3']";

		public static final String CPE_AddInterfaceSucessMessage = "@xpath=//span[text()='Interface successfully created.']";
		public static final String CPE_UpdateInterfaceSucessMessage = "@xpath=//span[text()='Interface successfully updated.']";
		public static final String CPE_DeleteInterfaceSucessMessage = "@xpath=//span[text()='Interface successfully removed from this service.']";
		public static final String CPE_AddMultilinkSucessMessage = "@xpath=//span[text()='Interface successfully created.']";
		public static final String CPE_UpdateMultilinkSucessMessage = "@xpath=//span[text()='Interface successfully updated.']";
		public static final String CPE_DeleteMultilinkSucessMessage = "@xpath=//span[text()='Interface successfully removed from this service.']";

		public static final String PEDevicedeletionsuccessmessage = "@xpath=//span[text()='Device successfully removed from service.']";

	}

}
