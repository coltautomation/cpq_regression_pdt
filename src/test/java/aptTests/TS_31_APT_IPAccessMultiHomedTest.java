package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_IPAccessResilientConfig;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_31_APT_IPAccessMultiHomedTest extends SeleniumUtils {
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	// APT_MCN_Create_Customer CreateCust = new APT_MCN_Create_Customer();
	// APT_MCS_CreateFirewallDevice CreateFirewall = new
	// APT_MCS_CreateFirewallDevice();
	APT_IPAccessNoCPEHelper noCPE = new APT_IPAccessNoCPEHelper();
	APT_IPTransit ipTransit = new APT_IPTransit();
	APT_IPAccessResilientConfig ResilentConfg = new APT_IPAccessResilientConfig();

	APT_Login Login = new APT_Login();


	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test()
	public void APT_IPAccessMultiHomedTest(String scriptNo, String dataSetNo, String dataSheet,
			String ScenarioName) throws Exception {
		
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {
			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			// setup();
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			Report.LogInfo("INFO", "TC - 1 - =====CreateCustomer_Resilient====", "PASS");
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerCreation");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");

			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
				noCPE.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
				Report.LogInfo("INFO", "=====selectExistingCustomer_Resilient====", "PASS");

				noCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			Report.LogInfo("INFO", "TC - 2 - =====verifyNewOrderCreationOrExistingOrderSelection_Resilient====",
					"PASS");
			ResilentConfg.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 3 - =====ServiceTypeAndNetworkConfigurationSelection_Resilient====", "PASS");
			noCPE.verifyServicetypeAndNetworkConfigurationSelection(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 4 - =====verifyServiceCreation_Resilient====", "PASS");
			ResilentConfg.verifyServiceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 5 - =====verifyCustomerDetailsInformation_Resilient====", "PASS");
			//noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyCustomerDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 6 - =====VerifyEditOrderChangeOrderFunction_Resilient====", "PASS");
			noCPE.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 7 - =====VerifyCreatedServiceInformationInViewServicePage_Resilient====",
					"PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 8 - =====VerifyServiceActionsListFunction_Resilient====", "PASS");
			ResilentConfg.EditServiceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyManageSubnets();
			ResilentConfg.verifyManageSubnetsIPv6();
			ResilentConfg.verifyDump();
			ResilentConfg.verifyManageService1(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 9 - =====VerifyActelisConfiguration_AddDSLAMandHSL_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyAddDSLAMandHSLlink();
			ResilentConfg.AddDSLAMandHSL(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 10 - =====VerifyAddDeleteExistingPEDevice_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.addExistingPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyExistingDevice_ViewDevicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.testStatus_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			

			Report.LogInfo("INFO", "TC - 11 - =====VerifyAddNewPEDeviceFunction_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.navigateToAddNewDevicepage_PE();
			noCPE.verifyadddevicefields_PE();
			noCPE.addNewPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 12 - =====VerifyNewDeviceInformation_PE_Resilient====", "PASS");
			noCPE.verifyViewpage_Devicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.testStatus_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 13 - =====updateAddedNewPEDevice_PE_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyEditDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 14 - =====verifyViewpage_UpdatedDevicedetails_PE_Resilient====", "PASS");
			noCPE.navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.verifyViewpage_UpdatedDevicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 15 - =====verifyRouterToolFunction_PE_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.routerPanel_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 16 - =====verifyAddEditDeleteRoutesFunction_PE_Resilient====", "PASS");
			noCPE.addRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.editRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 17 - =====verifyAddEditDeleteRoutesFunction_PE_Resilient====", "PASS");
			noCPE.verifyInterfaceConfigHistory(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 18 - =====VerifyFetchDeviceInterfaceFunction_PE_Resilient====", "PASS");
			noCPE.verifyFetchDeviceInterface_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 19 - =====verify_CiscoVendor_AddEditInterface_Resilient====", "PASS");
			String vendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
			if (vendorModel.contains("Cisco")) {
				noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.verify_CiscoVendor_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.verify_CiscoVendor_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				Report.LogInfo("INFO", " =====VerifyAdd_EditInterface_JuniperVendor_Resilient====", "PASS");
				noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.VerifyAddInterface_JuniperVendor(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.VerifyEditInterface_JuniperVendor(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			Report.LogInfo("INFO", "TC - 20 - =====verify_CiscoVendor_AddMultilink_Resilient====", "PASS");
			if (vendorModel.contains("Cisco")) {
				noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.verify_CiscoVendor_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				noCPE.verify_JuniperVendor_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			
			Report.LogInfo("INFO", "TC - 21 - =====verifyInterfaceConfigurationHistory_Multihomed====", "PASS");
			noCPE.verifyInterfaceConfigHistory(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 22 - =====verifySelectInterfaces_ColtTotal====", "PASS");
			String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RemoveInterface_Selection");
			String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"AddInterface_Selection");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.selectInterfacelinkforDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (RemoveInterface_Selection.equalsIgnoreCase("yes")) {
				noCPE.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				System.out.println("interfaces are not removed");
			}
			if (AddInterface_Selection.equalsIgnoreCase("yes")) {
				noCPE.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else {
				System.out.println("Interfaces are not added");
			}

			Report.LogInfo("INFO", "TC - 23 - =====verifyAddCPEDeviceFunction_Multihomed====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyAddCPEDeviceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 24 - =====verifyAddedCPEDeviceInformation_Multihomed====", "PASS");
			ResilentConfg.verifyAddedCPEDeviceInformation_View(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 25 - =====verifyEditCPEDeviceFunction_Multihomed====", "PASS");
			ResilentConfg.verifyEditCPEDeviceFunction(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 26 - =====verifyRouterToolFunction_CPE_Multihomed====", "PASS");
			noCPE.routerPanel_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 27 - =====verifyAddEditDeleteRouteFunction_CPE_Multihomed====", "PASS");
				noCPE.addRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.editRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 28 - =====verifyCustomerReadonlySNMPFunction_CPE_Multihomed====", "PASS");
			ResilentConfg.addCustomerReadonlySNMPFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.editCustomerReadonlySNMPFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.deleteCustomerReadonlySNMPFunction_CPE();
			
			//Report.LogInfo("INFO", "TC - 29 - =====verifyPIRangesFunction_CPE_Multihomed====", "PASS");
			ResilentConfg.addPIRangesFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.deletePIRangesFunction_CPE();
			
			
			//Report.LogInfo("INFO", "TC - 28 - =====verifyExtraSubnetsFunction_CPE_Resilient====", "PASS");
			Report.LogInfo("INFO", "TC - 30 - =====verifyeditNATConfigurationFunction_CPE_Multihomed====", "PASS");
			ResilentConfg.addExtraSubnetFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.editNATConfigurationFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 31 - =====verifyStaticNATMappingFunction_CPE_Multihomed====", "PASS");
			String StaticNATEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"StaticNATEdit");
			String DynamicNATEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"DynamicNATEdit");

			if (StaticNATEdit.equalsIgnoreCase("yes") && DynamicNATEdit.equalsIgnoreCase("no")) {
				// New Customer Creation
				ResilentConfg.addStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else if (StaticNATEdit.equalsIgnoreCase("no") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
				//noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				//ResilentConfg.navigateToViewDevicePage_CPE();
				ResilentConfg.addDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else if (StaticNATEdit.equalsIgnoreCase("Yes") && DynamicNATEdit.equalsIgnoreCase("Yes")) {
				//noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				//ResilentConfg.navigateToViewDevicePage_CPE();
				ResilentConfg.addStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteStaticNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

				//noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				//ResilentConfg.navigateToViewDevicePage_CPE();
				ResilentConfg.addDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.editDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
				ResilentConfg.deleteDynamicNATMappingFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else if (StaticNATEdit.equalsIgnoreCase("no") && DynamicNATEdit.equalsIgnoreCase("no")) {
				Report.LogInfo("INFO", "'Static NAT & Dynamic NAT is not selected as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"'Static NAT & Dynamic NAT is not selected as expected");

				Report.LogInfo("INFO", "'Static NAT Selection status is :  " + StaticNATEdit, "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO, "'Static NAT Selection status is :  " + StaticNATEdit);

				Report.LogInfo("INFO", "'Dynamic NAT Selection status is :  " + DynamicNATEdit, "PASS");
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"'Dynamic NAT Selection status is :  " + DynamicNATEdit);
			}
			
			Report.LogInfo("INFO", "TC - 32 - =====verifyDHCPServersonCPEFunction_CPE_Multihomed====", "PASS");
			ResilentConfg.addDHCPServersonCPEFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.editDHCPServersonCPEFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.deleteDHCPServersonCPEFunction_CPE();
	
			Report.LogInfo("INFO", "TC - 33 - =====VerifyConfigurationPanel_CPE_Resilient====", "PASS");
			ResilentConfg.VerifyConfigurationPanel_CPE();

			Report.LogInfo("INFO", "TC - 34 - =====verifyFetchDeviceInterfaceFunction_CPE_Resilient====", "PASS");
			//noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			//ResilentConfg.navigateToViewDevicePage_CPE();
			ResilentConfg.veriyFetchDeviceInterfacesFunction_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 35 - =====verifyAddInterface_CPEDevice_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyAddInterface_CPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);

			Report.LogInfo("INFO", "TC - 36 - =====verifyAddMultilink_CPEDevice_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			ResilentConfg.verifyAddMultilink_CPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 37 - =====deletePEdeviceFunction_NewlyCreatedDevice_Resilient====", "PASS");
			noCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			noCPE.deleteDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 38 - =====deleteCPEdeviceFunction_Resilient====", "PASS");
			ResilentConfg.deleteDevice_CPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO", "TC - 39 - =====deleteServiceFunction_Resilient====", "PASS");
			noCPE.deleteSevice1(testDataFile, dataSheet, scriptNo, dataSetNo);
			
		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}

}
