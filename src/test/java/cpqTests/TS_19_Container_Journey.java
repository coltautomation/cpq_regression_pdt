package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_19_Container_Journey extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CLoginPage Login = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	GlobalVariables g = new GlobalVariables();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testContainerJourney(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			//String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"No_Of_Copies");
			
			AddProduct.addProductandQuoteInC4C(testDataFile, dataSheet, scriptNo, dataSetNo);			
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);						
			
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Product_Name");
			String No_Of_Copies = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"No_Of_Copies");
			String Option= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Select_Type");
			String Option1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Select_Type1");
			String sResult;
			System.out.println("Before"+No_Of_Copies);
			
			sResult = AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","EOF(xls)",1 ,Option,"Quote_Level");
			//if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
//			calling the below method to Upload file
			sResult = AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","Confidential", 1,Option,"Quote_Level");
			//if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
//			calling the below method to Upload file
		     sResult = AddProduct.uploadFilesTechnicalTab(Product_Name,"Customer Facing","EOF(pdf)", 1,Option,"Quote_Level");
			//if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
			for(int i=1; i<Integer.parseInt(No_Of_Copies); i++) 
			{
				sResult = AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","EOF(xls)", i,Option1,"Line_Item");
				//if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
				
//				calling the below method to Upload file
				sResult = AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","EOF(xls)", i,Option1,"Line_Item");
				//if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
				
//				calling the below method to Upload file
			     sResult = AddProduct.uploadFilesTechnicalTab(Product_Name,"Internal","EOF(xls)", i,Option1,"Line_Item");
				//if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
				}	
			
			waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, 10);
			verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
			click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
			Reusable.WaitforCPQloader();
			waitForAjax();
			
			AddProduct.Submit4TechnicalApproval();
            CPQLogin.CPQLogout();
			
			//Login with CPQ SE user
			close();
			openBrowser(g.getBrowser());		
			openurl(Configuration.CPQ_URL);
			CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
			CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
			Approvals.QuoteSEApproval(testDataFile, dataSheet, scriptNo, dataSetNo);
			
            CPQLogin.CPQLogout();
			
			//Login with CST Login 		
			close();
			openBrowser(g.getBrowser());			
			openurl(Configuration.CPQ_URL);
			Reusable.WaitforCPQloader();
			CPQLogin.CPQLogin(Configuration.CSTUser_Username, Configuration.CSTUser_Password);
			CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
			Approvals.QuoteCSTApproval();
			
			if(Product_Name.equalsIgnoreCase("CpeSolutionsSite"))
			{
			String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
			Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
			} else {
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(Product_Name, BCN, "BCN NRC");
			}
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
		record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	

}
