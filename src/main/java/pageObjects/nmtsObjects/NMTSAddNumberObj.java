package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSAddNumberObj {
	
	public static class AddNumber
	{ 
		public static final String IEAddButton="@xpath=//font[contains(@onclick,'NumberNew')]"; 
		public static final String Switch="@xpath=//select[@id='cboSwitches']";
		public static final String AreacodePrefix="@xpath=//input[@id='btnArea']";
		public static final String iframe="@xpath=//iframe[@id='popupFrame']";
		public static final String AreaInformationWin="@xpath=//table[@id='grdView']//tr//td[text()='index']";
		public static final String Areacodeother="@xpath=//input[@id='txtNationalCode']";
		public static final String IEAreaCode="@xpath=//input[@id='txtAreaCode']";
		public static final String IEMergeRangeStart="@xpath=//input[@id='txtRangeFrom']";
		public static final String IEMergeRangeEnd="@xpath=//input[@id='txtRangeTo']";
		public static final String AddnumStatus="@xpath=//select[@id='cboStatus']";
		public static final String AddingAvailability="@xpath=//select[@id='cboAvailability']";
		public static final String AddingSubmit="@xpath=//input[@id='btnAddRangeSpec']";
		public static final String AddnumGeo="@xpath=//select[@id='cboGeo']";
		public static final String AddnumCtg="@xpath=//select[@id='cboGolden']";
		public static final String errorforerror="@xpath=//span[contains(text(),'can not be saved, it is already present in the database.')]";
		public static final String successmsg="@xpath=//span[@id='lblSuccessfulMsg']";
		public static final String success="@xpath=//span[contains(text(),'has been saved')]";
		public static final String UserName="@xpath=//span[@id='ucxSystemMenu_lblGroupName']";
		public static final String CountryButton="@xpath=//font[contains(@onclick,'SwitchCountry')]";	
		public static final String CountryName="@xpath=//select[@id='cboSwitchCountry']";
		public static final String CountryNameButton="@id=btnChangeCountry"; 
		public static final String Changegroup="@xpath=//font[contains(@onclick,'SwitchGroup')]";
		public static final String selectPhoneNOmanager="@xpath=//span[text()='Select New Group']/../..//select[@id='cboSelectNewGroup']";
		public static final String ChangeGroupbtn="@xpath=//input[@id='btnChangeGroup']";

		}

}