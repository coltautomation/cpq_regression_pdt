package testHarness.aptFunctions;

import java.io.IOException;

import baseClasses.Configuration;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_LoginObj;
import testHarness.commonFunctions.ReusableFunctions;

public class APT_Login extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void APT_NonVoiceService() throws InterruptedException, IOException 
	{
		//openurl(Application);

		waitForAjax();
		Reusable.WaitforC4Cloader();
		
		verifyExists(APT_LoginObj.APT_NonVoiceService.Username,"UserName TextBox");
		sendKeys(APT_LoginObj.APT_NonVoiceService.Username,"UserName TextBox");
		
		verifyExists(APT_LoginObj.APT_NonVoiceService.Password,"Password TextBox");
		sendKeys(APT_LoginObj.APT_NonVoiceService.Password,"Password TextBox");

		verifyExists(APT_LoginObj.APT_NonVoiceService.Loginbutton,"Login Button");
		click(APT_LoginObj.APT_NonVoiceService.Loginbutton,"Login Button");
	
	}
	
	public void APT_VoiceService() throws InterruptedException, IOException 
	{
		//openurl(Application);

		waitForAjax();
		Reusable.WaitforC4Cloader();
		
		verifyExists(APT_LoginObj.APT_VoiceService.Username,"UserName TextBox");
		sendKeys(APT_LoginObj.APT_VoiceService.Username,Configuration.APT_Username,"UserName TextBox");
		
		verifyExists(APT_LoginObj.APT_VoiceService.Password,"Password TextBox");
		sendKeys(APT_LoginObj.APT_VoiceService.Password,Configuration.APT_Password,"Password TextBox");

		verifyExists(APT_LoginObj.APT_VoiceService.Loginbutton,"Login Button");
		click(APT_LoginObj.APT_VoiceService.Loginbutton,"Login Button");
	
	}

}
