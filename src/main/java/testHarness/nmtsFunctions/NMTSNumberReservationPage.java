package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.GridHelper;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationReservedNumberObj;
import pageObjects.nmtsObjects.NMTSNumberReservationObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSNumberReservationPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSAddNumberPage AddNumberRange = new NMTSAddNumberPage();
	GridHelper helper = new GridHelper();
	
	String NumberReservationSuccessmsg;
	private static HashMap<String, String> Number= new HashMap<String, String>();
	
	public void NumberReservation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String ColtOrHostedNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt/HostedNo");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OCN");
		String EmailNotification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmailNotification");
		String SelectDestination = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SelectDestination");
		String ServiceProfile = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Profile");
		String TransactionReferencenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Transaction_Referencenumber");

		String MadeByName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MADE_BY_NAME");
		String FilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_By");
		String MatchingWithFilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Matching_With_FilterBy");
	
		String AreaName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Area_Code");
		String AreaCodeExtention = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NMTS_Area_Code_Extension");
		String RangeFrom = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeStart");
		String RangeTo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeEnd");
		
		

			    UserGroupforPhnNoMgn(testDataFile, sheetName, scriptNo, dataSetNo);
				waitForAjax(); 
				Reusable.switchtodefault();
				WaitForAjax();
				Reusable.switchtoframe("fmMenu");
				if(Country.toString().equals("CH")) {
					
					waitForAjax();
					ScrollIntoViewByString(NMTSNumberReservationObj.NumberReservation.DirectAllocationCH);
					verifyExists(NMTSNumberReservationObj.NumberReservation.DirectAllocationCH,"Direct Allocation");
					javaScriptclick(NMTSNumberReservationObj.NumberReservation.DirectAllocationCH,"Direct Allocation");
					
					WaitForAjax();
					Reusable.switchtodefault();
					waitForAjax();
					Reusable.switchtoframe("fmMain");
					waitForAjax();
					waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SelectWholesaleCh,15);
					verifyExists(NMTSNumberReservationObj.NumberReservation.SelectWholesaleCh,"Select Wholesale-Ch");
					javaScriptclick(NMTSNumberReservationObj.NumberReservation.SelectWholesaleCh,"Select Wholesale-Ch");
					
					
					
				}else{
					waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.DirectAllocation,15);
					verifyExists(NMTSNumberReservationObj.NumberReservation.DirectAllocation,"Direct Allocation");
					//waitForAjax();
					javaScriptclick(NMTSNumberReservationObj.NumberReservation.DirectAllocation,"Direct Allocation");
					
				}
					
					Reusable.switchtodefault();
					WaitForAjax();
					Reusable.switchtoframe("fmMain");
					WaitForAjax();
					/*
					for(int i=0;i<10;i++){
						if(isElementPresent(By.xpath("//input[@id='ucxSelectRange_grdView__ctl2_chkMark']"))){
							Reusable.switchtodefault();
							Reusable.switchtoframe("fmMain");
							javaScriptclick(NMTSNumberReservationObj.NumberReservation.AllocationCheckboxagain,"Click Allocation Checkbox again");
							waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.RemovedMarkedlink,10);
							javaScriptclick(NMTSNumberReservationObj.NumberReservation.RemovedMarkedlink,"Removed Marked link");
							WaitForAjax();				
						}else{
							break;
						}
						
					}
					*/
					
					
					if(Country.toString().equals("FR") || Country.toString().equals("IT")) {
					//	waitForAjax(); //Atul
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SelectHostedNumber,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.SelectHostedNumber,"Select Wholesale-Ch");
						Reusable.Select(NMTSNumberReservationObj.NumberReservation.SelectHostedNumber, ColtOrHostedNo);
					
					}
					
					if(Country.toString().equals("IT")||Country.toString().equals("FR")) {
						try {
							//waitForAjax(); //Atul
							waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.ReturnAvailabilityIT,15);
							verifyExists(NMTSNumberReservationObj.NumberReservation.ReturnAvailabilityIT,"Return Availability_IT");
							javaScriptclick(NMTSNumberReservationObj.NumberReservation.ReturnAvailabilityIT,"Return Availability_IT");
							Reusable.Select(NMTSNumberReservationObj.NumberReservation.ReturnAvailabilityIT, Availablity);
							
							} catch(StaleElementReferenceException e) {
							waitForAjax();
							waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.ReturnAvailabilityIT,15);
							verifyExists(NMTSNumberReservationObj.NumberReservation.ReturnAvailabilityIT,"Return Availability_IT");
							Reusable.Select(NMTSNumberReservationObj.NumberReservation.ReturnAvailabilityIT, Availablity);
											}
					} else {
						if(Country.toString().equals("UK")||Country.toString().equals("AT")||Country.toString().equals("BE")||Country.toString().equals("DK")||Country.toString().equals("ES")|| Country.toString().equals("IE")|| Country.toString().equals("IT") || Country.toString().equals("NL")|| Country.toString().equals("PT")|| Country.toString().equals("SE")) {
							waitForAjax();
							waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.ReturnAvailability,15);
							verifyExists(NMTSNumberReservationObj.NumberReservation.ReturnAvailability,"Return Availability");
							Reusable.Select(NMTSNumberReservationObj.NumberReservation.ReturnAvailability, Availablity);
						}
					}
					waitForAjax(); //Atul
					
					waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.AreaCodePrefix,15);
					verifyExists(NMTSNumberReservationObj.NumberReservation.AreaCodePrefix,"Return Availability IT");
					sendKeys(NMTSNumberReservationObj.NumberReservation.AreaCodePrefix, AreaName, "Area Name");
				//	waitForAjax();
					waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.AreaCodeExt,15);
					verifyExists(NMTSNumberReservationObj.NumberReservation.AreaCodeExt,"Area Code Extention");
					sendKeys(NMTSNumberReservationObj.NumberReservation.AreaCodeExt, AreaCodeExtention, "Area Code Extention");
					
					waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.RangeFrom,15);
					verifyExists(NMTSNumberReservationObj.NumberReservation.RangeFrom,"Range From");
					sendKeys(NMTSNumberReservationObj.NumberReservation.RangeFrom, RangeFrom, "Range From");
					
					waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.Rangeto,15);
					verifyExists(NMTSNumberReservationObj.NumberReservation.Rangeto,"Range to");
					sendKeys(NMTSNumberReservationObj.NumberReservation.Rangeto, RangeTo, "Range to"); 
					
					waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SubmitButton,15);
					verifyExists(NMTSNumberReservationObj.NumberReservation.SubmitButton,"Submit Button");
					javaScriptclick(NMTSNumberReservationObj.NumberReservation.SubmitButton,"Submit Button");
					waitForAjax();
					/*
					if(isElementPresent(By.xpath("//table[@id='ucxRange_grdView']//td[text()='IP Soft Switch']"))) {
					*/
					if(isElementPresent(By.xpath("//table[@id='ucxRange_grdView']//tr[contains(@ondblclick,'hdnIndex')]"))) {
							
						
						Reusable.switchtodefault();
						Reusable.switchtoframe("fmMain");
				
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.checkbox3,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.checkbox3,"checkbox3");
						javaScriptclick(NMTSNumberReservationObj.NumberReservation.checkbox3,"checkbox3");
						
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.Selectmark,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.Selectmark,"Select mark");
						javaScriptclick(NMTSNumberReservationObj.NumberReservation.Selectmark,"Select mark");
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.AllocationCheckboxagain,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.AllocationCheckboxagain,"Allocation Checkbox again");
						javaScriptclick(NMTSNumberReservationObj.NumberReservation.AllocationCheckboxagain,"Click Allocation Checkbox again");
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.submitSelectedRange,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.submitSelectedRange,"submit Selected Range");
						javaScriptclick(NMTSNumberReservationObj.NumberReservation.submitSelectedRange,"submit Selected Range");
						Reusable.switchtodefault();
						Reusable.switchtoframe("fmMain");
							
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.ocn,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.ocn,"OCN");
						sendKeys(NMTSNumberReservationObj.NumberReservation.ocn, OCN, "OCN");
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SubmitOCN,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.SubmitOCN,"Submit ocn");
						javaScriptclick(NMTSNumberReservationObj.NumberReservation.SubmitOCN,"Submit ocn");
						waitForAjax();
								
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "please check the correct data in Excel sheet for - AreaCode, AreacodeExt and Rangefrom");	
					}
					waitForAjax();
					
					
					if(isElementPresent(By.xpath("(//table[@id='ucxCustomerSearch_grdView']//tr)[2]/td[1]"))) {
						verifyExists(NMTSNumberReservationObj.NumberReservation.doubleClickonOCN,"OCN");
					    javaScriptDoubleclick(NMTSNumberReservationObj.NumberReservation.doubleClickonOCN,"OCN");
					    
				
						} 
					else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "it seems like OCN no is not correct in Excel sheet please check it ");	
							
					}
					
					
					waitForAjax();
					
					Reusable.switchtodefault();
					WaitForAjax();
					Reusable.switchtoframe("fmMain");
					WaitForAjax();
					
						
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.selectDesc,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.selectDesc,"Select Destination");
						Reusable.Select(NMTSNumberReservationObj.NumberReservation.selectDesc, SelectDestination);
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.EmailNotification,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.EmailNotification,"Email Notification");
						Reusable.Select(NMTSNumberReservationObj.NumberReservation.EmailNotification, EmailNotification);
						
						if(isElementPresent(By.xpath("//input[@id='ucxRequestAttributes_txtServiceProfile']")))	{
							verifyExists(NMTSNumberReservationObj.NumberReservation.ServiceProfile,"Service Profile");
							sendKeys(NMTSNumberReservationObj.NumberReservation.ServiceProfile, ServiceProfile, "Service Profile");
							waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.TransactionreferenceNo,15);
							verifyExists(NMTSNumberReservationObj.NumberReservation.TransactionreferenceNo,"Transaction Referencenumber");
							sendKeys(NMTSNumberReservationObj.NumberReservation.TransactionreferenceNo, TransactionReferencenumber, "Transaction Referencenumber");
							
							
						}
						
						verifyExists(NMTSNumberReservationObj.NumberReservation.CheckboxforAllocation,"Checkbox for Allocation");
						javaScriptclick(NMTSNumberReservationObj.NumberReservation.CheckboxforAllocation,"Checkbox for Allocation");
						if(Country.toString().equals("FR")){
							String NMTSID = getTextFrom(NMTSNumberReservationObj.NumberReservation.FR_NMTSReservationNo, "NMTS Reservation Number");
							setvalue("NMTS_ID",NMTSID);
							String FilterText = getvalue("NMTS_ID");
							waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,15);
							verifyExists(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,"Submit Order Btn");
							javaScriptclick(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,"Submit Order Btn");
						
							WaitForAjax();
							waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
							WaitForAjax();
						//	waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.FRCustomerDetailsPage,15);
							Reusable.switchtodefault();
							waitForAjax();
							Reusable.switchtoframe("fmMenu");		
							waitForAjax();
							waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.ProcessReservationtab,15);
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.ProcessReservationtab,"ProcessReservationtab");
							javaScriptclick(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.ProcessReservationtab,"ProcessReservationtab");
							WaitForAjax();
							Reusable.switchtodefault();
						    WaitForAjax();
							Reusable.switchtoframe("fmMain");
							WaitForAjax();
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SelectReservationColt,"Colt");
							Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SelectReservationColt,"Colt");
							WaitForAjax(); 
							waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.madeByNMTS,15);
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.madeByNMTS,"made By");
							Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.madeByNMTS,MadeByName);
							waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.Availablity,10);
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.Availablity,"Availablity");
							Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.Availablity,Availablity);
							WaitForAjax();
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterBy,"Filter By");
							Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterBy,FilterBy);
							waitForAjax(); 
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.matching,"Matching With Filter By");
							Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.matching,MatchingWithFilterBy);
						
							waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterText,15);
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterText,"Filter input");
							sendKeys(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterText,FilterText,"Filter input data");
							waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SubmitSearchbtn,15);
							verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SubmitSearchbtn,"Submit Deactivate Button");
							javaScriptclick(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SubmitSearchbtn,"Submit Deactivate Button");
							waitForAjax();
							
							if(isElementPresent(By.xpath("//table[@id='ucxOrderHistory_grdView']//tr/td[contains(text(),'No Record Found.')]")))
							{
								ExtentTestManager.getTest().log(LogStatus.FAIL, "No record found");	
						
							} 
							else {
								
									waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.OrderId,15);
									verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.OrderId,"OrderId");
									javaScriptDoubleclick(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.OrderId,"OCN double click");	
								
									waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.Reservationsuccessmsg,20);								
									NumberReservationSuccessmsg = getTextFrom(NMTSNumberReservationObj.NumberReservation.Reservationsuccessmsg,"Reservation Message");
									
									if(NumberReservationSuccessmsg.equals("Reservation"))
									{
										ExtentTestManager.getTest().log(LogStatus.PASS, "Number reserved successfuly");
										
									}
									else{					
										ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Not reserved successfuly");		
									}
							
							}
							
						}
						
						
						else{
						
						waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,15);
						verifyExists(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,"Submit Order Btn");
						javaScriptclick(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,"Submit Order Btn");
						WaitForAjax();
						waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
							
						//verify the Reservation status as : Reservation 
						NumberReservationSuccessmsg = getTextFrom(NMTSNumberReservationObj.NumberReservation.Reservationsuccessmsg,"Reservation Message");
						if(NumberReservationSuccessmsg.equals("Reservation"))
						{
							String NMTSID = getTextFrom(NMTSNumberReservationObj.NumberReservation.NMTSReservationNo, "NMTS Reservation Number");
							
							setvalue("NMTS_ID",NMTSID);
							ExtentTestManager.getTest().log(LogStatus.PASS, "Number reserved successfuly");
							
						}
						else{					
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Not reserved successfuly");		
						}
					
				}
						
		
	}
	
	
	public void UserGroupforPhnNoMgn(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException  {
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		
			waitForAjax();
			Reusable.switchtodefault();
			Reusable.switchtoframe("fmMenu");
			String check = getTextFrom(NMTSAddNumberObj.AddNumber.UserName,"User Name");
			if (check.equals("[SuperUser]")) {
				waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.Changegroup,15);
				verifyExists(NMTSNumberReservationObj.NumberReservation.Changegroup,"Change group Btn");
				javaScriptclick(NMTSNumberReservationObj.NumberReservation.Changegroup,"Change group Btn");
				waitForAjax();
				Reusable.switchtodefault();
				Reusable.switchtoframe("fmMain");
				waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.selectPhoneNOmanager,15);
				Reusable.Select(NMTSNumberReservationObj.NumberReservation.selectPhoneNOmanager, "Phone No. Manager");
				waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.ChangeGroupbtn,15);
				javaScriptclick(NMTSNumberReservationObj.NumberReservation.ChangeGroupbtn,"Change Group Button");
				waitForAjax();
			}
			  
			    Reusable.switchtodefault();
			    waitForAjax();
			    Reusable.switchtoframe("fmMenu");
			    waitForAjax();
			    
			  	waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.CountryButton,15);
				ScrollIntoViewByString(NMTSNumberReservationObj.NumberReservation.CountryButton);
				verifyExists(NMTSNumberReservationObj.NumberReservation.CountryButton,"Country Button");
				
				javaScriptclick(NMTSNumberReservationObj.NumberReservation.CountryButton,"Country Button");
				
				Reusable.switchtodefault();
				Reusable.switchtoframe("fmMain");
				
				waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.CountryName,15);
				verifyExists(NMTSNumberReservationObj.NumberReservation.CountryName,"Country Name");
				
				Reusable.Select(NMTSNumberReservationObj.NumberReservation.CountryName,Country);
				waitForAjax();
				waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.CountryNameButton,15);
				verifyExists(NMTSNumberReservationObj.NumberReservation.CountryNameButton,"Country Name Button");
				ScrollIntoViewByString(NMTSNumberReservationObj.NumberReservation.CountryNameButton);
				
				javaScriptclick2(NMTSNumberReservationObj.NumberReservation.CountryNameButton,"Country Name Button");
				WaitForAjax();
					
				}

	public void setvalue(String key, String value) {
		Number.put(key, value);
    }

    public String getvalue(String key) {
        return Number.get(key);       
    }
		
	}
	
	
	
	
	



