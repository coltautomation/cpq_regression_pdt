package testHarness.aptFunctions;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.Manage_PostcodeObj;

public class ManagePostcode extends SeleniumUtils {
	
	public void selectColtnetwork() throws InterruptedException, IOException
	{
		
		waitforPagetobeenable();
		//Moveon(getwebelement(xml.getlocator("//locators/" + application + "/ManageColtNetworkLink")));
		mouseOverAction(Manage_PostcodeObj.ManageColt.ManageColtNetworkLink);
		waitforPagetobeenable();
		
		Report.LogInfo("INFO", "Mouse hovered on Manage Colt Network ", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS,  "Mouse hovered on Manage Colt Network");
		waitforPagetobeenable();
			
		//boolean isDisplayed = getwebelement(xml.getlocator("//locators/" + application + "/ManagePostcode")).isDisplayed();
		verifyExists(Manage_PostcodeObj.ManageColt.ManagePostcode, "Manage Postcode link");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Manage Postcode link verified");

		verifyExists(Manage_PostcodeObj.ManageColt.ManagePostcode, "Manage Postcode link");
		click(Manage_PostcodeObj.ManageColt.ManagePostcode, "Manage Postcode link");
		
		waitforPagetobeenable();
			
		Report.LogInfo("INFO","Manage Postcode link clicked","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Manage Postcode link");

	}
	
	public void verifyManagePostcodepage() throws InterruptedException
	{
		
		String[] CountryList= {"Belgium","Denmark","France","Germany","Portugal","Spain","Sweden","Switzerland"};
		Report.LogInfo("INFO","Verifying list of countries","PASS");
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying list of countries");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Country names displaying are: ");
		
		List<WebElement> CountryPresent = findWebElements(Manage_PostcodeObj.ManageColt.fetchListofCountryNames);
		
		for (WebElement Country: CountryPresent ) {
			boolean match = false;
			Report.LogInfo("INFO","Country list displaying from application:"  +Country.getText(),"INFO");
			

			
			for (int i=0; i < CountryList.length;i++) {
				if (Country.getText().equals(CountryList[i]))
				{
					match = true;
					Report.LogInfo("INFO","Country name : " + Country.getText(),"PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, Country.getText());
				}
								
			}
			
		}
		}

	public void verifyManagePostcodeinternal(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String Countrylist =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Countrylist");
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying fields under 'Manage Postcode' page");
		if (Countrylist.equals("Belgium"))
		{
		waitforPagetobeenable();
		verifyExists(Manage_PostcodeObj.ManageColt.BelMngPostcode, "Belgium Manage Postcode");
		click(Manage_PostcodeObj.ManageColt.BelMngPostcode, "Belgium Manage Postcode");
		
		Report.LogInfo("INFO","Clicked on Belgium Manage Postcode","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on Belgium Manage Postcode");
		
		
		compareText("You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
		
		compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
		
		compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
		
		compareText( "Emergency Area (Subcom)",Manage_PostcodeObj.ManageColt.EmergencyAreaSubcom,"Emergency Area (Subcom)");
		
		compareText( "SubArea1 (Province)",Manage_PostcodeObj.ManageColt.SubAreaProvince,"SubArea1 (Province)");
		
		//compareText( "SubArea 1-ID (SubArea1-ID)","SubArea1-ID","SubArea 1-ID (SubArea1-ID)");
		compareText( "SubArea 1-ID (SubArea1-ID)",Manage_PostcodeObj.ManageColt.SubArea1_ID,"SubArea 1-ID (SubArea1-ID)");
		
		compareText( "SubArea2 (Community)",Manage_PostcodeObj.ManageColt.SubArea2,"SubArea2 (Community)");
		
		compareText( "SubArea 2-ID (SubArea2-ID)",Manage_PostcodeObj.ManageColt.SubArea2_ID,"SubArea 2-ID (SubArea2-ID)");
		
		compareText( "SubArea 3 (b)",Manage_PostcodeObj.ManageColt.SubArea3,"SubArea 3 (b)");
		
		compareText( "SubArea 3-ID (Zipcode)",Manage_PostcodeObj.ManageColt.SubArea3_ID,"SubArea 3-ID (Zipcode)");
		
		compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
		
        compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
		
		compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
		
		compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
		
        compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
		
		compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
		
		compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
		
		compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");
		
		}
		
		else if(Countrylist.equals("Denmark"))
		{
			waitforPagetobeenable();
			verifyExists(Manage_PostcodeObj.ManageColt.DenMngPostcode,"Clicked on Denmark Manage Postcode");
			click(Manage_PostcodeObj.ManageColt.DenMngPostcode,"Clicked on Denmark Manage Postcode");
			
			Report.LogInfo("INFO","Clicked on Denmark Manage Postcode","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on Denmark Manage Postcode");
			waitforPagetobeenable();
			
			compareText( "You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
			
			compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
			
			compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
			
			compareText( "Emergency Area(Kommune)",Manage_PostcodeObj.ManageColt.EmergencyArea_Kommune,"Emergency Area(Kommune)");
			
			compareText( "SubArea1(SubArea1)",Manage_PostcodeObj.ManageColt.SubArea1Den,"SubArea1(SubArea1)");
			
			compareText( "SubArea1-ID(SubArea1-ID)",Manage_PostcodeObj.ManageColt.SubArea1_ID_Denmark,"SubArea1-ID(SubArea1-ID)");
			
			compareText( "SubArea2-ID(SubArea2-ID)",Manage_PostcodeObj.ManageColt.SubArea2_ID_Denmark,"SubArea2-ID(SubArea2-ID)");
			
			compareText( "SubArea2(SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2Den,"SubArea2(SubArea2)");
			
			compareText( "SubArea3(SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Den,"SubArea3(SubArea3)");

			compareText( "SubArea3-ID(SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3DenArea,"SubArea3-ID(SubArea3-ID)");
			
			compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
			
	        compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
			
			compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
			
			compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
			
	        compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
			
			compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
			
			compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
			
			compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");
			
		}

		else if(Countrylist.equals("France"))
		{
			waitforPagetobeenable();
			verifyExists(Manage_PostcodeObj.ManageColt.FraMngPostcode," France Manage Postcode");
			click(Manage_PostcodeObj.ManageColt.FraMngPostcode,"Clicked on France Manage Postcode");
			
			Report.LogInfo("INFO","Clicked on France Manage Postcode","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on France Manage Postcode");
			waitforPagetobeenable();
			
            compareText( "You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
			
			compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
			
			compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
			
            compareText( "Emergency",Manage_PostcodeObj.ManageColt.EmergencyAreaCityArea,"Emergency");
            
            compareText( "SubArea1(Department)",Manage_PostcodeObj.ManageColt.SubArea1Fra,"SubArea1(Department)");

            compareText( "SubArea2(SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2Fra,"SubArea2(SubArea2)");
			
            compareText( "SubArea1-ID(Department-ID)",Manage_PostcodeObj.ManageColt.Department_ID,"SubArea1-ID(Department-ID)");
            			
            compareText( "SubArea2-ID(SubArea2-ID)",Manage_PostcodeObj.ManageColt.SubArea2_ID_France,"SubArea2-ID(SubArea2-ID)");
			
			compareText( "SubArea3(SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Den,"SubArea3(SubArea3)");

			compareText( "SubArea3-ID(SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3DenArea,"SubArea3-ID(SubArea3-ID)");
			
			compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
			
	        compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
			
			compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
			
			compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
			
	        compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
			
			compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
			
			compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
			
			compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");
		
		}
		
		else if(Countrylist.equals("Germany"))
		{
			waitforPagetobeenable();
			verifyExists(Manage_PostcodeObj.ManageColt.GerMngPostcode, "Germany Manage Postcode");
			click(Manage_PostcodeObj.ManageColt.GerMngPostcode,"Clicked on Germany Manage Postcode");
			
			Report.LogInfo("INFO","Clicked on Germany Manage Postcode","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on Germany Manage Postcode");
			waitforPagetobeenable();
			
            compareText( "You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
			
			compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
			
			compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
			
			compareText( "Emergency Area(Local Area Name (U_ON_NAME))",Manage_PostcodeObj.ManageColt.EmergencyAreaLocal,"Emergency Area(Local Area Name (U_ON_NAME))");
			
			compareText( "SubArea1(SubArea1)",Manage_PostcodeObj.ManageColt.SubArea1Ger,"SubArea1(SubArea1)");
			
			compareText( "SubArea1-ID(SubArea1-ID)",Manage_PostcodeObj.ManageColt.SubArea1_ID_Germany,"SubArea1-ID(SubArea1-ID)");

			compareText( "SubArea2(SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2Fra,"SubArea2(SubArea2)");
			
            compareText( "SubArea2-ID(SubArea2-ID)",Manage_PostcodeObj.ManageColt.SubArea2_ID_Germany,"SubArea2-ID(SubArea2-ID)");
            
			compareText( "SubArea3(SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Den,"SubArea3(SubArea3)");
			
			compareText( "SubArea3-ID(SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3DenArea,"SubArea3-ID(SubArea3-ID)");
			
            compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
			
	        compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
			
			compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
			
			compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
			
	        compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
			
			compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
			
			compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
			
			compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");			
			

		}
		else if (Countrylist.equals("Portugal"))
		{
			waitforPagetobeenable();
			verifyExists(Manage_PostcodeObj.ManageColt.PorMngPostcode,"Portugal Manage Postcode");
			click(Manage_PostcodeObj.ManageColt.PorMngPostcode,"Clicked on Portugal Manage Postcode");
			
			Report.LogInfo("INFO","Clicked on Portugal Manage  Postcode","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on Portugal Manage  Postcode");
			waitforPagetobeenable();
			
			compareText( "You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
				
			compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
				
			compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
				
			compareText( "Emergency Area(Province)",Manage_PostcodeObj.ManageColt.EmergencyAreaProvince,"Emergency Area(Province)");
				
			compareText( "SubArea1(SubArea1)",Manage_PostcodeObj.ManageColt.SubArea1Ger,"SubArea1(SubArea1)");
				
			compareText( "SubArea1-ID(SubArea1-ID)",Manage_PostcodeObj.ManageColt.SubArea1_ID_Portugal,"SubArea1-ID(SubArea1-ID)");

			compareText( "SubArea2(SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2Fra,"SubArea2(SubArea2)");
				
	        compareText( "SubArea2-ID(SubArea2-ID)",Manage_PostcodeObj.ManageColt.SubArea2_ID_Portugal,"SubArea2-ID(SubArea2-ID)");
	            
			compareText( "SubArea3(SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Den,"SubArea3(SubArea3)");
				
			compareText( "SubArea3-ID(SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3DenArea,"SubArea3-ID(SubArea3-ID)");
				
			compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
				
		    compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
				
			compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
				
			compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
				
		    compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
				
			compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
				
			compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
				
			compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");			
			
			}
		
		else if (Countrylist.equals("Spain"))
		{
			waitforPagetobeenable();
			verifyExists(Manage_PostcodeObj.ManageColt.SpaMngPostcode,"Spain Manage Postcode");
			click(Manage_PostcodeObj.ManageColt.SpaMngPostcode,"Clicked on Spain Manage Postcode");
			
			Report.LogInfo("INFO","Clicked on Spain Manage Postcode","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Step:Clicked on Spain Manage Postcode");
			waitforPagetobeenable();
			
			compareText( "You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
			
			compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
			
			compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
			
			compareText( "Emergency Area(Municipality)",Manage_PostcodeObj.ManageColt.EmergencyAreaMunicipality,"Emergency Area(Municipality)");
			
			compareText( "SubArea1(Provincia)",Manage_PostcodeObj.ManageColt.SubArea1Provincia,"SubArea1(Provincia)");
			
			compareText( "SubArea1-ID(Prov.Code)",Manage_PostcodeObj.ManageColt.SubArea1DSpa,"SubArea1-ID(Prov.Code)");
			
			compareText( "SubArea2(SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2Fra,"SubArea2(SubArea2)");
			
            compareText( "SubArea2-ID(SubArea2-ID)",Manage_PostcodeObj.ManageColt.SubArea2_ID_Spain,"SubArea2-ID(SubArea2-ID)");
            
			compareText( "SubArea3(SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Den,"SubArea3(SubArea3)");
			
			compareText( "SubArea3-ID(SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3DenArea,"SubArea3-ID(SubArea3-ID)");
		
			compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
			
	        compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
			
			compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
			
			compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
			
	        compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
			
			compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
			
			compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
			
			compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");			
			
		}
		else if (Countrylist.equals("Sweden"))
		{
			waitforPagetobeenable();
			verifyExists(Manage_PostcodeObj.ManageColt.SweMngPostcode,"Sweden Manage Postcode");
			click(Manage_PostcodeObj.ManageColt.SweMngPostcode,"Clicked on Sweden Manage Postcode");
			
			Report.LogInfo("INFO","Clicked on Sweden Manage Postcode","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Step:Clicked on Sweden Manage Postcode");
			waitforPagetobeenable();
			
            compareText( "You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
			
			compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
			
			compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
			
			compareText( "Emergency Area(Kommun)",Manage_PostcodeObj.ManageColt.EmergencyAreaKommun,"Emergency Area(Kommun)");
			
			compareText( "SubArea1(Lan)",Manage_PostcodeObj.ManageColt.SubArea1Lan,"SubArea1(Lan)");
			
			compareText( "SubArea1-ID(SubArea1-ID)",Manage_PostcodeObj.ManageColt.SubArea1_ID_Sweden,"SubArea1-ID(SubArea1-ID)");
			
			compareText( "SubArea2(SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2Fra,"SubArea2(SubArea2)");
			
			compareText( "SubArea3(SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Den,"SubArea3(SubArea3)");
			
			compareText( "SubArea3-ID(SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3DenArea,"SubArea3-ID(SubArea3-ID)");
			
            compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
			
	        compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
			
			compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
			
			compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
			
	        compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
			
			compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
			
			compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
			
			compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");			
			
		}

		else if (Countrylist.equals("Switzerland"))
		{
			waitforPagetobeenable();
			verifyExists(Manage_PostcodeObj.ManageColt.SwitMngPostcode,"Switzerland Manage Postcode");
			click(Manage_PostcodeObj.ManageColt.SwitMngPostcode,"Clicked on Switzerland Manage Postcode");
			
			Report.LogInfo("INFO","Clicked on Switzerland Manage Postcode","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,"Step:Clicked on Switzerland Manage Postcode");
			waitforPagetobeenable();
			
			compareText( "You can use * as wildcard",Manage_PostcodeObj.ManageColt.WildcardText,"You can use * as wildcard");
				
			compareText( "Emergency Area ID",Manage_PostcodeObj.ManageColt.EmergencyAreaID,"Emergency Area ID");
			 	
			compareText( "NT Service Area",Manage_PostcodeObj.ManageColt.NTServiceArea,"NT Service Area");
				 
			compareText( "Emergency Area(Emergency Area)",Manage_PostcodeObj.ManageColt.EmergencyArea,"Emergency Area(Emergency Area)");
				 
			compareText( "SubArea1(Canton)",Manage_PostcodeObj.ManageColt.SubAreaCanton,"SubArea1(Canton)");
				 
			compareText( "SubArea1-ID(GDEKT)",Manage_PostcodeObj.ManageColt.SuAreaGDEKT,"SubArea1-ID(GDEKT)");
				 
			compareText( "SubArea2(District)",Manage_PostcodeObj.ManageColt.SubAreaDistrict,"SubArea2(District)");
				 
			compareText( "SubArea2-ID(GDEBZNR)",Manage_PostcodeObj.ManageColt.SubAreaGDE,"SubArea2-ID(GDEBZNR)");
				 
			compareText( "SubArea3(Municipality)",Manage_PostcodeObj.ManageColt.SubArea3Mun,"SubArea3(Municipality)");
				 
			compareText( "SubArea3-ID(SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3DenArea,"SubArea3-ID(SubArea3-ID)");
				 				 
			compareText( "Last Update",Manage_PostcodeObj.ManageColt.Lastupdate,"Last Update");
					
			compareText( "Add Postcode",Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
					
			compareText( "Upload Update File",Manage_PostcodeObj.ManageColt.Uploadupdatefile,"Upload Update File");
					
			compareText( "View History",Manage_PostcodeObj.ManageColt.ViewHistory,"View History");
					
			compareText( "Add Emergency Numbers",Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Numbers");
					
			compareText( "Synchronize All Postcodes",Manage_PostcodeObj.ManageColt.SynchronizeAllpostcodes,"Synchronize All Postcodes");
					
			compareText( "Download NT Service Area",Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT Service Area");
					
			compareText( "Upload NT Service Area",Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");			

			}
	}
	
	public void addPostcode (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
	String Countrylist =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Countrylist");
	String EmergencyAreaIDSub=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyAreaIDSub");
	//String NtServiceAreaValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpro_Mg_Network");
	String ImplementSwitchValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ImplementSwitchValue");
	String CityTranValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CityTranValue");
	String SubProvinValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubProvinValue");
	String SubAreaValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaValue");
	String SubAreaComValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaComValue");
	String SubAreaIDValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaIDValue");
	String SubAreaBValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaBValue");
	String SubAreaZIpValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaZIpValue");
	String EmergencySUbValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencySUbValue");
	String EmergencyKeyValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyKeyValue");
	String ActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ActualValue");
	String DummyCodeValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DummyCodeValue");
				
				
					ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Add Postcode");
					Report.LogInfo("INFO", "Verifying Add Postcode", "INFO");
					
					verifyExists(Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
					click(Manage_PostcodeObj.ManageColt.Addpostcode,"Add Postcode");
					waitforPagetobeenable();
					
					Report.LogInfo("INFO","Clicked on Add Postcode","PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on Add Postcode");

					if(Countrylist.equalsIgnoreCase("Belgium"))
					{
					
				    	compareText("Emergency Area ID(SUBCOM-ID)",Manage_PostcodeObj.ManageColt.EmergencyAreaIDSubcomLabel,"Emergency Area ID(SUBCOM-ID)");
				    	
				    	compareText("SubArea1 (Province)",Manage_PostcodeObj.ManageColt.SubArea1ProLabel,"SubArea1 (Province)");
				    	
				    	compareText("SubArea2 (Community)",Manage_PostcodeObj.ManageColt.SubArea2CommuLabel,"SubArea2 (Community)");
				    	
				    	compareText("SubArea3 (b)",Manage_PostcodeObj.ManageColt.SubArea3BLabel,"SubArea3 (b)");
				    	
				    	compareText("SubArea3-ID (ZIP code)",Manage_PostcodeObj.ManageColt.SubAreaZipLabel,"SubArea3-ID (ZIP code)");
				    	
				    	compareText("Emergency Area (SUBCOM)",Manage_PostcodeObj.ManageColt.EmergencyAreaSubcomLabel,"Emergency Area (SUBCOM)");
					}
					
				
					else if(Countrylist.equals("Denmark"))
					{
						
				    	compareText("Emergency Area ID (Kommunenummer)",Manage_PostcodeObj.ManageColt.EmergencyKommunerlabel,"Emergency Area ID (Kommunenummer)");
				    	
				    	compareText("SubArea1 (SubArea1)",Manage_PostcodeObj.ManageColt.SubArea1DenLabel,"SubArea1 (SubArea1)");
				    	
				    	compareText("SubArea2 (SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2DenLabel,"SubArea2 (SubArea2)");
				    	
				    	compareText("SubArea3 (SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Denlabel,"SubArea3 (SubArea3)");
				    	
				    	compareText("SubArea3-ID (SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3IDDenLabel,"SubArea3-ID (SubArea3-ID)");
				    	
				    	compareText("Emergency Area (Kommune)",Manage_PostcodeObj.ManageColt.EmergencyArea_Kommunelabel,"Emergency Area (Kommune)");
					}
					
					else if(Countrylist.equals("France"))
					{
			            compareText("Emergency Area ID (INSEE Code)",Manage_PostcodeObj.ManageColt.EmergencyAreaIDFranceLabel,"Emergency Area ID (INSEE Code)");
				    	
				    	compareText("SubArea1 (Department)",Manage_PostcodeObj.ManageColt.SubAreaDepartFrance,"SubArea1 (Department)");
				    	
				    	compareText("SubArea1-ID (Department ID)",Manage_PostcodeObj.ManageColt.SubArea1DFranLabel,"SubArea1-ID (Department ID)");

				    	compareText("SubArea2 (SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2DenLabel,"SubArea2 (SubArea2)");
				    	
				    	compareText("SubArea3 (SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Denlabel,"SubArea3 (SubArea3)");
				    	
				    	compareText("SubArea3-ID (SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3IDDenLabel,"SubArea3-ID (SubArea3-ID)");
				    	
				    	compareText("Emergency Area (City (Area))",Manage_PostcodeObj.ManageColt.EmergencyAreaFranceLabel,"Emergency Area (City (Area))");

				    	
					
					}
					else if(Countrylist.equals("Germany"))
					{
						    compareText("Emergency Area ID (Local Area ID (U_ONKZ))",Manage_PostcodeObj.ManageColt.EmergencyAreaIDGermanyLabel,"Emergency Area ID (INSEE Code)");
						    
					    	compareText("SubArea1 (SubArea1)",Manage_PostcodeObj.ManageColt.SubArea1DenLabel,"SubArea1 (SubArea1)");
					    	
					    	compareText("SubArea2 (SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2DenLabel,"SubArea2 (SubArea2)");
					    	
					    	compareText("SubArea3 (SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Denlabel,"SubArea3 (SubArea3)");
					    	
					    	compareText("SubArea3-ID (SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3IDDenLabel,"SubArea3-ID (SubArea3-ID)");
					    	
					    	
					    	compareText("Emergency Area (Local Area Name (U_ON_NAME))",Manage_PostcodeObj.ManageColt.EmergencyAreaLocalGermany,"Emergency Area (Local Area Name (U_ON_NAME))");		

					}
					else if(Countrylist.equals("Portugal"))
					{
					
						compareText("Emergency Area ID (CLISRVPF INDEX)",Manage_PostcodeObj.ManageColt.EmergencyAreaIDPortugalLabel,"Emergency Area ID (CLISRVPF INDEX)");

						compareText("SubArea1 (SubArea1)",Manage_PostcodeObj.ManageColt.SubArea1DenLabel,"SubArea1 (SubArea1)");
				    	
				    	compareText("SubArea2 (SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2DenLabel,"SubArea2 (SubArea2)");
				    	
				    	compareText("SubArea3 (SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Denlabel,"SubArea3 (SubArea3)");
				    	
				    	compareText("SubArea3-ID (SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3IDDenLabel,"SubArea3-ID (SubArea3-ID)");
				    	
				    	compareText("Emergency Area (Province)",Manage_PostcodeObj.ManageColt.EmergencyAreaProvGermanyLAbel,"Emergency Area (Province)");


					}
					else if(Countrylist.equals("Spain"))
					{
						compareText("Emergency Area ID (Municip.Code)",Manage_PostcodeObj.ManageColt.EmergencyAreaID_SpainLabel,"Emergency Area ID (Municip.Code)");
						
						compareText("SubArea1 (Provincia)",Manage_PostcodeObj.ManageColt.SubAreaProvinciaSpainLabel,"SubArea1 (Provincia)");

						compareText("SubArea1-ID (Prov.Code)",Manage_PostcodeObj.ManageColt.SubArea1DSpainLabel,"SubArea1-ID (Prov.Code)");
				
						compareText("SubArea2 (SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2DenLabel,"SubArea2 (SubArea2)");
				    	
				    	compareText("SubArea3 (SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Denlabel,"SubArea3 (SubArea3)");
				    	
				    	compareText("SubArea3-ID (SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3IDDenLabel,"SubArea3-ID (SubArea3-ID)");
				    	
						compareText("Emergency Area (Municipality)",Manage_PostcodeObj.ManageColt.EmergencyAReaMunc_Label,"Emergency Area (Municipality)");	
					}
					
					else if(Countrylist.equals("Sweden"))
					{
						
						compareText("Emergency Area ID (Kommun-ID/ALARM-ID)",Manage_PostcodeObj.ManageColt.EmergencyAreaIDSweden_Label,"Emergency Area ID (Kommun-ID/ALARM-ID)");
						
						compareText("SubArea1 (Lan)",Manage_PostcodeObj.ManageColt.SubAreaLanSweden_Label,"SubArea1 (Lan)");
						
						//compareText("SubArea2-ID (ZIP)",Manage_PostcodeObj.ManageColt.SubArea2DLanSweden_Label,"SubArea2-ID (ZIP)");
							
						compareText("SubArea2 (SubArea2)",Manage_PostcodeObj.ManageColt.SubArea2DenLabel,"SubArea2 (SubArea2)");
				    	
				    	compareText("SubArea3 (SubArea3)",Manage_PostcodeObj.ManageColt.SubArea3Denlabel,"SubArea3 (SubArea3)");
				    	
				    	compareText("SubArea3-ID (SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3IDDenLabel,"SubArea3-ID (SubArea3-ID)");
				    	
						compareText("Emergency Area (Kommun)",Manage_PostcodeObj.ManageColt.EmergencyAreaKommun_Label,"Emergency Area (Kommun)");

				    	
					}
					else if(Countrylist.equals("Switzerland"))
					{
						
						compareText("Emergency Area ID (Emergency Area ID)",Manage_PostcodeObj.ManageColt.EmergencyAreaIDSwitLabel,"Emergency Area ID (Emergency Area ID)");
						
						compareText("SubArea1 (Canton)",Manage_PostcodeObj.ManageColt.SubArea1Caton_Label,"SubArea1 (Canton)");
						
						compareText("SubArea1-ID (GDEKT)",Manage_PostcodeObj.ManageColt.SubArea1DGDEKT,"SubArea1-ID (GDEKT)");
							
						compareText("SubArea2 (District)",Manage_PostcodeObj.ManageColt.SubAreaDistrictLabel,"SubArea2 (District)");
				    	
				    	compareText("SubArea2-ID (GDEBZNR)",Manage_PostcodeObj.ManageColt.SuArea2IDSwit,"SubArea2-ID (GDEBZNR)");
				    	
				    	compareText("SubArea3-ID (SubArea3-ID)",Manage_PostcodeObj.ManageColt.SubArea3IDDenLabel,"SubArea3-ID (SubArea3-ID)");
				    	
						compareText("SubArea3 (Municipality)",Manage_PostcodeObj.ManageColt.SubArea3MunLabel,"SubArea3 (Municipality)");
						
						compareText("Emergency Area (Emergency Area)",Manage_PostcodeObj.ManageColt.EmergencyAreaSwit,"Emergency Area (Emergency Area)");
	
					}
					
					if(Countrylist.equalsIgnoreCase("Belgium")||(Countrylist.equalsIgnoreCase("Denmark")||(Countrylist.equalsIgnoreCase("Germany")||
							(Countrylist.equalsIgnoreCase("Portugal")))))
									{
							
							WebElement SubArea1D = findWebElement(Manage_PostcodeObj.ManageColt.SubArea1DLabel);
							// sa.assertTrue(SubArea1D.isDisplayed(),"Sub Area 1D Field is not displayed");
							//Report.LogInfo("Sub Area 1D field is displayed");
							Report.LogInfo("INFO","SubArea1D field is displayed as:"+SubArea1D.getText(),"PASS");
							ExtentTestManager.getTest().log(LogStatus.PASS, "Step:SubArea1D field is displayed as:" +SubArea1D.getText());
									}
							
					if(Countrylist.equalsIgnoreCase("Belgium")||(Countrylist.equalsIgnoreCase("Denmark")||(Countrylist.equalsIgnoreCase("France")||
							(Countrylist.equalsIgnoreCase("Germany")||(Countrylist.equalsIgnoreCase("Portugal")||
									(Countrylist.equalsIgnoreCase("Spain")))))))
					  {
					
							WebElement SubArea2ID = findWebElement(Manage_PostcodeObj.ManageColt.SubArea2IDLabel);
							//sa.assertTrue(SubArea2ID.isDisplayed(),"Sub Area2 ID Field is not displayed");
							//Log.info("Sub Area2 ID field is displayed");
							Report.LogInfo("INFO","Sub Area 2 ID field is displayed as:"+SubArea2ID.getText(),"PASS");
							ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Sub Area 2 ID field is displayed as:" +SubArea2ID.getText());
									}

					
					if(Countrylist.equalsIgnoreCase("Belgium")||(Countrylist.equalsIgnoreCase("Denmark")||(Countrylist.equalsIgnoreCase("France")||(Countrylist.contentEquals("Germany")||
							(Countrylist.equalsIgnoreCase("Portugal")||(Countrylist.equalsIgnoreCase("Spain")||
									(Countrylist.equalsIgnoreCase("Sweden")||(Countrylist.equalsIgnoreCase("Switzerland")))))))))
					{
						WebElement NTServiceAreaPostcode = findWebElement(Manage_PostcodeObj.ManageColt.NtServiceAreaLabel);
						//sa.assertTrue(NTServiceAreaPostcode.isDisplayed(),"NT Service Area Postcode Field is not displayed");
						//Log.info("NT Service Area Postcode field is displayed");
						Report.LogInfo("INFO","NT Service Area Post code:"+NTServiceAreaPostcode.getText(),"PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step:NT Service Area Postcode field is displayed as:" +NTServiceAreaPostcode.getText());


						
						WebElement ToimplemetSwitch = findWebElement(Manage_PostcodeObj.ManageColt.ToimplementLabel);
						//sa.assertTrue(ToimplemetSwitch.isDisplayed(),"To implement Switch  Field is not displayed");
						//Log.info("To implement Switch field is displayed");
						Report.LogInfo("INFO","To implement Switch field is displayed as:"+ToimplemetSwitch.getText(),"PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step:To implement Switch field is displayed as:" +ToimplemetSwitch.getText());


						
						WebElement CityTranslatorNmbr = findWebElement(Manage_PostcodeObj.ManageColt.CityTransLabel);
						//sa.assertTrue(CityTranslatorNmbr.isDisplayed(),"City Translator Nmbr Field is not displayed");
						//Log.info("City Translator Nmbr field is displayed");
						Report.LogInfo("INFO","City Translator Nmbr field is displayed as:"+CityTranslatorNmbr.getText(),"PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step:City Translator Nmbr field is displayed as:" +CityTranslatorNmbr.getText());
						
						WebElement EmergencyNmbrKey = findWebElement(Manage_PostcodeObj.ManageColt.EmergencyKeyLabel);
						//sa.assertTrue(EmergencyNmbrKey.isDisplayed(),"Emergency Nmbr Key Text Field is not displayed");
						//Log.info("Emergency Nmbr Key is displayed");
						Report.LogInfo("INFO","Emergency Nmbr Key is displayed","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Emergency Nmbr Key is displayed");
				
								
						WebElement ActualProvider = findWebElement(Manage_PostcodeObj.ManageColt.ActualProviderLabel);
						//sa.assertTrue(ActualProvider.isDisplayed(),"Actual Provider Text Field is not displayed");
						//Log.info("Actual Provider Text Field is displayed");
						Report.LogInfo("INFO","Actual Provider Text Field is displayed","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Actual Provider Text Field is displayed");

						WebElement DummyCode_Text = findWebElement(Manage_PostcodeObj.ManageColt.DummyCodeLAbel);
						//sa.assertTrue(DummyCode_Text.isDisplayed(),"Dummy Code Field is not displayed");
						//Log.info("Dummy Code Field is not displayed");
						Report.LogInfo("INFO","Dummy Code field is displayed","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Dummy Code field is displayed");


						
						WebElement EmptyTextbox = findWebElement(Manage_PostcodeObj.ManageColt.EmptyTextbox);
						//sa.assertTrue(EmptyTextbox.isDisplayed(),"Empty Text box Field is not displayed");
						//Log.info("Empty Text box Field is displayed");
						Report.LogInfo("INFO","Empty Text box field is displayed as:"+EmptyTextbox.getText(),"PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Empty Text box field is displayed as:");


						
						WebElement forwardarrow = findWebElement(Manage_PostcodeObj.ManageColt.forwardarrow);
						// sa.assertTrue(forwardarrow.isDisplayed(),"forward arrow button Field is not displayed");
						//Log.info("forward arrow button Field is  displayed");
						Report.LogInfo("INFO","forward arrow field is displayed as >>","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "forward arrow button field is displayed as: >>");


						
						WebElement BackwardArrow = findWebElement(Manage_PostcodeObj.ManageColt.BackwardArrow);
						//sa.assertTrue(BackwardArrow.isDisplayed(),"Back ward Arrow is not displayed");
						//Log.info("Back ward Arrow is  displayed");
						Report.LogInfo("INFO","Backward Arrow field is displayed as <<","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Backward arrow button field is displayed as: <<");


						
						WebElement OKbtn = findWebElement(Manage_PostcodeObj.ManageColt.OkbtnPostcode);
						//sa.assertTrue(OKbtn.isDisplayed(),"OK btn is not displayed");
						//Log.info("OK btn is displayed");
						Report.LogInfo("INFO","OK btn is displayed","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "OK button is displayed");


						
						WebElement Cancelbtn = findWebElement(Manage_PostcodeObj.ManageColt.Cancelbtn);
						//sa.assertTrue(Cancelbtn.isDisplayed(),"Cancel button is not displayed");
						//Log.info("Cancel button is displayed");
						Report.LogInfo("INFO","Cancel btn is displayed","PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Cancel button is displayed");
					}		

					}
	
	public void fillAddPostCode(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String Countrylist =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Countrylist");
		String EmergencyAreaIDSub=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyAreaIDSub");
		String NtServiceAreaValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpro_Mg_Network");
		String ImplementSwitchValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ImplementSwitchValue");
		String CityTranValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CityTranValue");
		String SubProvinValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubProvinValue");
		String SubAreaValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaValue");
		String SubAreaComValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaComValue");
		String SubAreaIDValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaIDValue");
		String SubAreaBValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaBValue");
		String SubAreaZIpValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaZIpValue");
		String EmergencySUbValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencySUbValue");
		String EmergencyKeyValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyKeyValue");
		String ActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ActualValue");
		String DummyCodeValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DummyCodeValue");
					
	
		if(Countrylist.equalsIgnoreCase("Belgium")||(Countrylist.equalsIgnoreCase("Denmark")||(Countrylist.equalsIgnoreCase("France")
				||(Countrylist.equalsIgnoreCase("Germany")||(Countrylist.equalsIgnoreCase("Portugal")
						||(Countrylist.equalsIgnoreCase("Spain")||(Countrylist.equalsIgnoreCase("Sweden")||(Countrylist.equalsIgnoreCase("Switzerland")))))))))
		{
			
			String countryCode=countryCode(Countrylist);
			String expectedNTserviceArea=countryCode+"-"+EmergencyAreaIDSub;
		
		//Filling value in Mandatory fields
				waitforPagetobeenable();
				
				ExtentTestManager.getTest().log(LogStatus.INFO, "Entering Values to create postcode");
				
				verifyExists(Manage_PostcodeObj.ManageColt.EmergencyAreaIDSubcom_TextField,"Emergency Area ID(SUBCOM-ID)");
				sendKeys(Manage_PostcodeObj.ManageColt.EmergencyAreaIDSubcom_TextField,EmergencyAreaIDSub,"Emergency Area ID(SUBCOM-ID)");
				
				String actualNTserviceAreaValue=findWebElement(Manage_PostcodeObj.ManageColt.NTServiceAreaPostcode_Text).getAttribute("value");
				
				if(expectedNTserviceArea.equals(actualNTserviceAreaValue)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "NT Service Are field value is displaying as "+ actualNTserviceAreaValue +" as expected");
					Report.LogInfo("INFO","NT Service Are field value is displaying as "+ actualNTserviceAreaValue +" as expected","PASS");
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "NT Service Are field value is displaying as "+ actualNTserviceAreaValue+". The expected value is: "+ expectedNTserviceArea);
					Report.LogInfo("INFO","NT Service Are field value is displaying as "+ actualNTserviceAreaValue +" as expected","FAIL");
				}
				
				verifyExists(Manage_PostcodeObj.ManageColt.ToimplemetSwitch_Text, "To Implement Switch");
				sendKeys(Manage_PostcodeObj.ManageColt.ToimplemetSwitch_Text, ImplementSwitchValue, "To Implement Switch");
				
				verifyExists( Manage_PostcodeObj.ManageColt.CityTranslatorNmbr_Text, "City Translator Number");
				sendKeys( Manage_PostcodeObj.ManageColt.CityTranslatorNmbr_Text, CityTranValue, "City Translator Number");
				
				verifyExists( Manage_PostcodeObj.ManageColt.SubAreaProvince_1_Text, "SubArea1");
				sendKeys( Manage_PostcodeObj.ManageColt.SubAreaProvince_1_Text, SubProvinValue, "SubArea1");
				
				verifyExists(Manage_PostcodeObj.ManageColt.SubArea1D_Text, "SubArea1-ID");
				sendKeys(Manage_PostcodeObj.ManageColt.SubArea1D_Text, SubAreaValue, "SubArea1-ID");
				
				verifyExists(Manage_PostcodeObj.ManageColt.SubArea2Community_Text, "SubArea2");
				sendKeys(Manage_PostcodeObj.ManageColt.SubArea2Community_Text, SubAreaComValue, "SubArea2");
				
				verifyExists( Manage_PostcodeObj.ManageColt.SubArea2ID_Text, "SubArea2-ID");
				sendKeys( Manage_PostcodeObj.ManageColt.SubArea2ID_Text, SubAreaIDValue, "SubArea2-ID");
				
				verifyExists(Manage_PostcodeObj.ManageColt.SubArea3B_Text, "SubArea3");
				sendKeys(Manage_PostcodeObj.ManageColt.SubArea3B_Text, SubAreaBValue, "SubArea3");
				
				verifyExists( Manage_PostcodeObj.ManageColt.SubAreaZipcode_Text, "SubArea3-ID");
				sendKeys( Manage_PostcodeObj.ManageColt.SubAreaZipcode_Text, SubAreaZIpValue, "SubArea3-ID");
				
				verifyExists(Manage_PostcodeObj.ManageColt.EmergencyAreaSub_Text, "Emergency Area");
				sendKeys(Manage_PostcodeObj.ManageColt.EmergencyAreaSub_Text, EmergencySUbValue, "Emergency Area");
				
				verifyExists( Manage_PostcodeObj.ManageColt.EmergencyNmbrKey_Text, "Emergency Number Key");
				sendKeys( Manage_PostcodeObj.ManageColt.EmergencyNmbrKey_Text, EmergencyKeyValue, "Emergency Number Key");
				
				verifyExists(Manage_PostcodeObj.ManageColt.ActualProvider_Text, "Actual Provider Mapping");
				sendKeys(Manage_PostcodeObj.ManageColt.ActualProvider_Text, ActualValue, "Actual Provider Mapping");
				
				verifyExists( Manage_PostcodeObj.ManageColt.DummyCode_Text, "Dummy Code");
				sendKeys( Manage_PostcodeObj.ManageColt.DummyCode_Text, DummyCodeValue, "Dummy Code");
				
				waitforPagetobeenable();
				javaScriptclick(Manage_PostcodeObj.ManageColt.forwardarrow);
				waitforPagetobeenable();

				javaScriptclick(Manage_PostcodeObj.ManageColt.OkbtnPostcode);
				
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clciked on OK  button");

				waitforPagetobeenable();
				
				//Verify Post code Success Message
				
				verifysuccessmessage("Postcode successfully created.");
				}
			else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, Countrylist + " country is not available");
			//assertTrue(false);
			
		}
		}
	
	public String countryCode(String countryName) {
 		
 		String code=null;
 		
 		if(countryName.equalsIgnoreCase("Belgium")){
 			code="32";
 		}
 		else if(countryName.equalsIgnoreCase("Denmark")){
 			code="45";
 		}
 		else if(countryName.equalsIgnoreCase("France")){
 			code="33";
 		}
		else if(countryName.equalsIgnoreCase("Germany")){
			code="49";
		}
		else if(countryName.equalsIgnoreCase("Portugal")){
			code="351";
		}
		else if(countryName.equalsIgnoreCase("Spain")){
			code="34";	
		}
		else if(countryName.equalsIgnoreCase("Sweden")){
			code="46";
		}
		else if(countryName.equalsIgnoreCase("Switzerland")){
			code="41";	
		}
 		
 		return code;
 	}
	
	public void verifyPostcodevalues(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws Exception 
	{
		String Countrylist =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Countrylist");
		
		String EmergencyAreaIDSub=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyAreaIDSub");
		
		String ImplementSwitchValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ImplementSwitchValue");
		
		String CityTranValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CityTranValue");
		
		String SubProvinValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubProvinValue");
		
		String SubAreaValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaValue");
		
		String SubAreaComValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaComValue");
		
		String SubAreaIDValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaIDValue");
		
		String SubAreaBValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaBValue");
		
		String SubAreaZIpValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SubAreaZIpValue");
		
		String EmergencySUbValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencySUbValue");
		
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying view Postcode");
		
		//select the postcode value in table and click on Action dropdown	
		selectAddedPostCodeInTable(Countrylist, EmergencyAreaIDSub);
		
		//Verifying the values in View Postcode
		
		javaScriptclick(Manage_PostcodeObj.ManageColt.PostcodeActionView);
		
		Report.LogInfo("INFO","Clicked on View","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on View in Action Dropdown" );
		
		if(Countrylist.equalsIgnoreCase("France"))
		{
		
		compareText_InViewPage("Emergency Area ID (INSEE Code)",EmergencyAreaIDSub);
		
		compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
		
		compareText_InViewPage("City Translator Number:",CityTranValue);
		
		compareText_InViewPage("SubArea1 (Department)",SubProvinValue);
		
		compareText_InViewPage("SubArea1-ID (Department ID)",SubAreaValue);
		
		compareText_InViewPage("SubArea2 (SubArea2)",SubAreaComValue);
		
		compareText_InViewPage("SubArea2-ID (SubArea2-ID)",SubAreaIDValue);
		
		compareText_InViewPage("SubArea3 (SubArea3)",SubAreaBValue);
		
		compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
		
		compareText_InViewPage("Emergency Area (City (Area))",EmergencySUbValue);
		}
		
		else if(Countrylist.equalsIgnoreCase("Belgium"))
		{
			compareText_InViewPage("Emergency Area ID(SUBCOM-ID)",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (Province)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (SubArea1-ID)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (Community)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (SubArea2-ID)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (b)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (ZIP code)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (SUBCOM)",EmergencySUbValue);

		}
		else if(Countrylist.equalsIgnoreCase("Denmark"))
		{
			compareText_InViewPage("Emergency Area ID (Kommunenummer)",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (SubArea1)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (SubArea1-ID)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (SubArea2)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (SubArea2-ID)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (SubArea3)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (Kommune)",EmergencySUbValue);

		}
		else if(Countrylist.equalsIgnoreCase("Germany"))
		{
			compareText_InViewPage("Emergency Area ID (Local Area ID (U_ONKZ))",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (SubArea1)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (SubArea1-ID)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (SubArea2)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (SubArea2-ID)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (SubArea3)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (Local Area Name (U_ON_NAME))",EmergencySUbValue);

		}
		else if(Countrylist.equalsIgnoreCase("Portugal"))
		{
			compareText_InViewPage("Emergency Area ID (CLISRVPF INDEX)",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (SubArea1)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (SubArea1-ID)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (SubArea2)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (SubArea2-ID)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (SubArea3)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (Province)",EmergencySUbValue);

		}
		else if(Countrylist.equalsIgnoreCase("Portugal"))
		{
			compareText_InViewPage("Emergency Area ID (CLISRVPF INDEX)",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (SubArea1)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (SubArea1-ID)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (SubArea2)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (SubArea2-ID)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (SubArea3)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (Province)",EmergencySUbValue);

		}
		
		else if(Countrylist.equalsIgnoreCase("Spain"))
		{
			compareText_InViewPage("Emergency Area ID (Municip.Code)",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (Province)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (Prov.Code)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (SubArea2)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (SubArea2-ID)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (SubArea3)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (Municipality)",EmergencySUbValue);

		}
		else if(Countrylist.equalsIgnoreCase("Sweden"))
		{
			compareText_InViewPage("Emergency Area ID (Kommun-ID/ALARM-ID)",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (Lan)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (SubArea1-ID)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (SubArea2)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (ZIP)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (SubArea3)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (Kommun)",EmergencySUbValue);

		}
		else if(Countrylist.equalsIgnoreCase("Switzerland"))
		{
			compareText_InViewPage("Emergency Area ID (Emergency Area ID)",EmergencyAreaIDSub);
			
			compareText_InViewPage("To Implement Switch:",ImplementSwitchValue);
			
			compareText_InViewPage("City Translator Number:",CityTranValue);
			
			compareText_InViewPage("SubArea1 (Canton)",SubProvinValue);
			
			compareText_InViewPage("SubArea1-ID (GDEKT)",SubAreaValue);
			
			compareText_InViewPage("SubArea2 (District)",SubAreaComValue);
			
			compareText_InViewPage("SubArea2-ID (GDEBZNR)",SubAreaIDValue);
			
			compareText_InViewPage("SubArea3 (Municipality)",SubAreaBValue);
			
			compareText_InViewPage("SubArea3-ID (SubArea3-ID)",SubAreaZIpValue);
			
			compareText_InViewPage("Emergency Area (Emergency Area)",EmergencySUbValue);

		}
	
		waitforPagetobeenable();
		verifyExists(Manage_PostcodeObj.ManageColt.PostcodeBackbtn,"Back button");
		click(Manage_PostcodeObj.ManageColt.PostcodeBackbtn,"Clicked on back button");
		//Log.info("Clicked on back button");
		Report.LogInfo("INFO","Clicked on back button","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on back button");

		
	    //Log.info("Navigated to Manage Post code page");
		Report.LogInfo("INFO","Navigated to Manage Post code page","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Navigated to Manage Post code page");

	}
	
public void selectAddedPostCodeInTable(String Countrylist, String EmergencyAreaIDSub) throws Exception {
		
		String countryCode=countryCode(Countrylist);
		String expectedNTserviceArea=countryCode+"-"+EmergencyAreaIDSub;
		
		verifyExists(Manage_PostcodeObj.ManageColt.Searchfield,"Value Entered in TextField");
		sendKeys(Manage_PostcodeObj.ManageColt.Searchfield,expectedNTserviceArea,"Value Entered in TextField");
		//Log.info("Value Entered in TextField");
		Report.LogInfo("INFO","Value Entered in Search TextField","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Value Entered in Search TextField" );

		verifyExists(Manage_PostcodeObj.ManageColt.Searchbtn,"Search button present");
		click(Manage_PostcodeObj.ManageColt.Searchbtn,"Search button clicked");
		//Log.info("Clicked on Search button");
		Report.LogInfo("INFO","Clicked on search button","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Clicked on search button" );
		
		waitforPagetobeenable();
		
		WebElement addPostCodeLink=findWebElement(Manage_PostcodeObj.ManageColt.Addpostcode);
		scrolltoview(addPostCodeLink);
		waitforPagetobeenable();
		
		 	//safeJavaScriptClick(findWebElement(Manage_PostcodeObj.ManageColt.PostcodeCheckbox)).replace("value", EmergencyAreaIDSub);
		verifyExists(Manage_PostcodeObj.ManageColt.PostcodeCheckbox1+EmergencyAreaIDSub+Manage_PostcodeObj.ManageColt.PostcodeCheckbox2,"Post code Checkbox");
		click(Manage_PostcodeObj.ManageColt.PostcodeCheckbox1+EmergencyAreaIDSub+Manage_PostcodeObj.ManageColt.PostcodeCheckbox2,"Post code Checkbox");
		 	
		Report.LogInfo("INFO","Clicked on checkbox","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "selected postcode record in Table");
			waitforPagetobeenable();
			
		    safeJavaScriptClick(findWebElement(Manage_PostcodeObj.ManageColt.PostcodeAction));
		    Report.LogInfo("INFO","Clicked on Action Dropdown","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Action Dropdown");
			waitforPagetobeenable();

	
	
		/*
		WebElement addPostCodeLink=findWebElement(Manage_PostcodeObj.ManageColt.Addpostcode);
		scrollDown(Manage_PostcodeObj.ManageColt.Addpostcode);
		waitforPagetobeenable();
		
		//WebElement element = webDriver.findElement(By.xpath("//div[text()='EmergencyAreaIDSub']/parent::div//span//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
		
		Manage_PostcodeObj.ManageColt.PostcodeCheckbox1+EmergencyAreaIDSub+Manage_PostcodeObj.ManageColt.PostcodeCheckbox2
		javaScriptclick(Manage_PostcodeObj.ManageColt.PostcodeCheckbox);
		
		//Log.info("Clicked on checkbox");
		Report.LogInfo("INFO","Clicked on checkbox","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "selected postcode record in Table");
		waitforPagetobeenable();
			
		javaScriptclick(Manage_PostcodeObj.ManageColt.PostcodeAction);
		Report.LogInfo("INFO","Clicked on Action Dropdown","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Action Dropdown");
		waitforPagetobeenable();
	*/
	}

public void editPostcode(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, Exception {
	
	String Countrylist=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Countrylist");
	
	String EmergencyAreaIDSub=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyAreaIDSub");
	
	String edit_ImplementSwitchValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_ImplementSwitchValue");
	
	String edit_CityTranValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_CityTranValue");
	
	String edit_SubArea1=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SubArea1");
	
	String edit_SubArea1_ID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SubArea1_ID");
	
	String edit_SubArea2 =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SubArea2");
	
	String edit_SubArea2_ID =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SubArea2_ID");
	
	String edit_SubArea3 =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SubArea3");
	
	String edit_SubArea3_ID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_SubArea3_ID");
	
	String edit_EmergencyArea=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_EmergencyArea");
	
	String replaceEmergencyArea=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"replaceEmergencyArea");
	
	String updateEmergencyArea=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"updateEmergencyArea");
	
	String edit_EmergencyKeyValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_EmergencyKeyValue");
	
	String edit_ActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_ActualValue");
	
	String edit_DummyCodeValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_DummyCodeValue");
	
	//select the postcode value in table and click on Action dropdown	
	selectAddedPostCodeInTable(Countrylist, EmergencyAreaIDSub);
   		    
	javaScriptclick(Manage_PostcodeObj.ManageColt.PostcodeActionEdit);
    
	Report.LogInfo("INFO","Clicked on Edit button","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Edit button");

	
	waitforPagetobeenable();
	
	waitforPagetobeenable();
	
	sendKeys(Manage_PostcodeObj.ManageColt.ToimplemetSwitch_Text, edit_ImplementSwitchValue,"To Implement Switch");
	
	sendKeys( Manage_PostcodeObj.ManageColt.CityTranslatorNmbr_Text, edit_CityTranValue, "City Translator Number");
	
	sendKeys( Manage_PostcodeObj.ManageColt.SubAreaProvince_1_Text, edit_SubArea1, "SubArea1");
	
	sendKeys( Manage_PostcodeObj.ManageColt.SubArea1D_Text, edit_SubArea1_ID, "SubArea1-ID");
	
	sendKeys(Manage_PostcodeObj.ManageColt.SubArea2Community_Text, edit_SubArea2, "SubArea2");
	
	sendKeys(Manage_PostcodeObj.ManageColt.SubArea2ID_Text, edit_SubArea2_ID,"SubArea2-ID");
	
	sendKeys(Manage_PostcodeObj.ManageColt.SubArea3B_Text, edit_SubArea3, "SubArea3");
	
	sendKeys(Manage_PostcodeObj.ManageColt.SubAreaZipcode_Text, edit_SubArea3_ID,"SubArea3-ID");
	
	sendKeys(Manage_PostcodeObj.ManageColt.EmergencyAreaSub_Text, edit_EmergencyArea, "Emergency Area");
	
	if(replaceEmergencyArea.equalsIgnoreCase("yes") && updateEmergencyArea.equalsIgnoreCase("No")) {
		
		//TODO::
		
	}
	else if(replaceEmergencyArea.equalsIgnoreCase("No") && updateEmergencyArea.equalsIgnoreCase("yes")) {
		
		sendKeys(Manage_PostcodeObj.ManageColt.EmergencyNmbrKey_Text, edit_EmergencyKeyValue, "Emergency Number Key");
		
		sendKeys(Manage_PostcodeObj.ManageColt.ActualProvider_Text, edit_ActualValue,"Actual Provider Mapping");
		
		sendKeys(Manage_PostcodeObj.ManageColt.DummyCode_Text, edit_DummyCodeValue, "Dummy Code");
		
		waitforPagetobeenable();
		
		verifyExists(Manage_PostcodeObj.ManageColt.forwardarrow,"Forward Arrow");
		click(Manage_PostcodeObj.ManageColt.forwardarrow,"Forward Arrow");
		waitforPagetobeenable();

		
	}
	
    javaScriptclick(Manage_PostcodeObj.ManageColt.OkbtnPostcode);
    waitforPagetobeenable();
    Report.LogInfo("INFO","Clicked on OK button","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on OK button");

	verifysuccessmessage("Postcode successfully updated.");
	
	}

public void createFileForUploadupdateFile(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	
	//String filepath =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"filePathForUploadUpdateFletestDataToUpload");
	
	String dummyCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"addUploadUpdateFiletestData_dumyCode");
	
	String providerMapping=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"addUploadUpdateFiletestData_providerMapping");
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/upload_update_file.csv";
	
	//sendKeys(Manage_PostcodeObj.ManageColt.upload_update_choosefilebutton, uploadFilePath,"Upload Uupdate File");
	//click(Manage_PostcodeObj.ManageColt.  "Submit");
	
	String tempFile=uploadFilePath;
	File newfile = new File(tempFile);
	String id=""; 
	
	String name="";
	boolean found=false;
	
	try {
		FileWriter fw= new FileWriter(tempFile, true);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter pw = new PrintWriter(bw); 
		
			pw.println(dummyCode  + "," + providerMapping);
			
		pw.flush();
		pw.close();
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Upload Update File' file is created");
		ExtentTestManager.getTest().log(LogStatus.PASS, "In 'Upload Update File' CSV file, Test data is added as: "+ dummyCode  + "," + providerMapping);
		Report.LogInfo("INFO","'Upload Update File' file is created","PASS");
		Report.LogInfo("INFO","In 'Upload Update File' CSV file, Test data is added as: "+ dummyCode  + "," + providerMapping,"PASS");
		
	}catch(Exception e) {
		e.printStackTrace();
	}
		
	
}

public void createFileForAddEmergencyNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	
	//String filepath = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "filePathForAddEmergencyNumberTestDataToUpload");
	String emergencyID  =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EmergencyAreaIDSub");
	String dummycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addEmergencyNumberTestdata_DummyCodeValue");
	String actualProviderMapping=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addEmergencyNumberTestata_ActualProviderValue");
	String emergencyNumberKey=	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addEmergencyNumberTestData_EmergencyKeyValue");
	
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/Add_Emergency_Numbers.csv";
	
	String tempFile=uploadFilePath;
	
	File newfile = new File(tempFile);
	String id=""; String name="";
	boolean found=false;
	
	try {
		FileWriter fw= new FileWriter(tempFile, true);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter pw = new PrintWriter(bw);
		
			pw.println(emergencyID + "," + dummycode + "," + actualProviderMapping + "," + emergencyNumberKey);
			
		pw.flush();
		pw.close();
		
		Report.LogInfo("INFO","'Add Emergency Number' file is created","PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Emergency Number' file is created");
		ExtentTestManager.getTest().log(LogStatus.PASS, "In 'Add Emergency Area' CSV file, test data is added as:  "+ emergencyID + "," + dummycode + "," + actualProviderMapping + "," + emergencyNumberKey);
		
	}catch(Exception e) {
		e.printStackTrace();
	}
		
	}
public void createFileForuploadNTServiceArea(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	
	//String filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"filePathForUploadNTServiceAreaTestDataToUpload");
	
	String EmergencyAreaIDSub=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyAreaIDSub");
	
	String Countrylist=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Countrylist");
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/Upload_NT_Service_Area.csv";
	
	String countryCode=countryCode(Countrylist);
	String expectedNTserviceArea=countryCode+"-"+EmergencyAreaIDSub;
	
 		String tempFile=uploadFilePath;
		File newfile = new File(tempFile);
		String id=""; String name="";
		boolean found=false;
		
		try {
			FileWriter fw= new FileWriter(tempFile, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw); 
			
				pw.println(EmergencyAreaIDSub  + "," + expectedNTserviceArea);
				
			pw.flush();
			pw.close();
			
			Report.LogInfo("INFO","'Upload NT Service Area' file is created","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Upload NT Service Area' file is created");
			ExtentTestManager.getTest().log(LogStatus.PASS, "In Upload NT Serice ARea CSV file, Test data is added as: "+ EmergencyAreaIDSub  + "," + expectedNTserviceArea);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
}
public void verifyDownloadNt(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws Exception
{
	String downloadpath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Downloadspath");
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Download NT Service Area");
	
	javaScriptclick(Manage_PostcodeObj.ManageColt.DownloadNt,"Download NT");
	
	Report.LogInfo("INFO","Download Nt Link is displayed","PASS");
	Report.LogInfo("INFO","Clicked on Download Nt Service Area link","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Download Nt Service Area link");
	waitforPagetobeenable();
	waitforPagetobeenable();
	//isFileDownloaded(downloadpath, "postcode_cld_68");


}
public boolean isFileDownloaded(String downloadPath, String fileName) {
	  File dir = new File(downloadPath);
	  File[] dirContents = dir.listFiles();

	  for (int i = 0; i < dirContents.length; i++) {
	      if (dirContents[i].getName().contains(fileName)) {
	          // File has been found, it can now be deleted:
	          //dirContents[i].delete();
	    	  
	    	  String downloadedFileName=dirContents[i].getName();
	    	  Report.LogInfo("INFO","Downloaded file name is displaying as: "+ downloadedFileName,"PASS");
	    	  ExtentTestManager.getTest().log(LogStatus.PASS, "Downloaded file name is displaying as: "+ dirContents[i]);
	    	  
	          return true;
	      }
	          }
	      return false;
	  }
public void deletePostcode(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
{
	String Countrylist =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Countrylist");
	String EmergencyAreaIDSub =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmergencyAreaIDSub");
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Delete' Functionality");
	waitforPagetobeenable();
	
	//webDriver.findElement(By.tagName("body")).sendKeys(Keys.HOME); 
	//webDriver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
	scrolltoend();
		
	//select the postcode value in table and click on Action dropdown	
	selectAddedPostCodeInTable(Countrylist, EmergencyAreaIDSub);
 
	verifyExists(Manage_PostcodeObj.ManageColt.PostcodeActionDelete,"Delete");
	click(Manage_PostcodeObj.ManageColt.PostcodeActionDelete,"Delete");
	
//Handling Delete Alert
	WebElement DeleteAlertPopup= findWebElement(Manage_PostcodeObj.ManageColt.delete_alertpopup);
    if(DeleteAlertPopup.isDisplayed())
    {
  	 String deletPopUpMessage= getTextFrom(Manage_PostcodeObj.ManageColt.deleteMessages_textMessage,"delete Messages_text Message");
  	 ExtentTestManager.getTest().log(LogStatus.PASS, "Delete Pop up message displays as: "+ deletPopUpMessage);
  	 
  	 verifyExists(Manage_PostcodeObj.ManageColt.deletebutton, "Delete");
     click(Manage_PostcodeObj.ManageColt.deletebutton, "Delete");
   //scrollToTop();
     scrollUp();
     waitforPagetobeenable();
       
       verifysuccessmessage("Postcode successfully deleted.");
    }
    else
    {
    	  Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
          ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete alert popup is not displayed");
    }    
		
	}

public void verifysuccessmessage(String expected) throws InterruptedException {
		
		
		waitforPagetobeenable();
		//scrollToTop();
		scrollUp();
		waitforPagetobeenable();
		try {	
			
			boolean successMsg=findWebElement(Manage_PostcodeObj.ManageColt.alertMsg_managePOstcode).isDisplayed();

			if(successMsg) {
				
				String alrtmsg=findWebElement(Manage_PostcodeObj.ManageColt.AlertForServiceCreationSuccessMessage_managePOstcode).getText();
				
				if(expected.contains(alrtmsg)) {
					
					ExtentTestManager.getTest().log(LogStatus.PASS,"Message is verified. It is displaying as: "+alrtmsg);
					Report.LogInfo("INFO","Message is verified. It is displaying as: "+alrtmsg,"PASS");
					
					//successScreenshot(application);
					
				}else if(expected.equals(alrtmsg)){
					
					ExtentTestManager.getTest().log(LogStatus.PASS,"Message is verified. It is displaying as: "+alrtmsg);
					Report.LogInfo("INFO","Message is verified. It is displaying as: "+alrtmsg,"PASS");
					//successScreenshot(application);
					
				}else {
					
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg +" .The Expected value is: "+ expected);
					Report.LogInfo("INFO","Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg,"FAIL");
					//failureScreenshot(application);
				}
				
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");
				Report.LogInfo("INFO"," Success Message is not displaying","FAIL");
				//failureScreenshot(application);
			}
			
		}catch(Exception e) {
			Report.LogInfo("INFO","failure in fetching success message  ","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, expected+ " Message is not displaying");
			Report.LogInfo("INFO",expected+ " message is not getting dislpayed","FAIL");
			//failureScreenshot(application);
		}
	}

public void deleteFile_NTServiceArea(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	//String filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "filePathForUploadNTServiceAreaTestDataToUpload");
	
	String fileName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Upload NT Service Area");
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/Upload_NT_Service_Area.csv";
	
		File newfile = new File(uploadFilePath);
		  
		  if(newfile.delete()) {
				Report.LogInfo("INFO",fileName + " file is deleted","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, fileName + " file is deleted");
			}else {
				Report.LogInfo("INFO",fileName + " file is not deleted","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, fileName + " file is not deleted");
			}
		  
	}
public void deleteFile_AddEmergencyNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	
	//String filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "filePathForAddEmergencyNumberTestDataToUpload");
	
	String fileName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Add Emergency Number");
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/Add_Emergency_Numbers.csv";
	
		File newfile = new File(uploadFilePath);
		  
		  if(newfile.delete()) {
				Report.LogInfo("INFO",fileName + " file is deleted","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, fileName + " file is deleted");
			}else {
				Report.LogInfo("INFO",fileName + " file is not deleted","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, fileName + " file is not deleted");
			}
		  
	}

public void deleteFile_UploadupdateFile(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	
	//String filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "filePathForUploadUpdateFletestDataToUpload");
	
	String fileName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Upload Update File");
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/upload_update_file.csv";
	
		File newfile = new File(uploadFilePath);
		  
		  if(newfile.delete()) {
				Report.LogInfo("INFO",fileName + " file is deleted","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, fileName + " file is deleted");
			}else {
				Report.LogInfo("INFO",fileName + " file is not deleted","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, fileName + " file is not deleted");
			}
		  
	}

public void verifyUploadupdatefile (String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws Exception 
	{
	//String filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"filePathForUploadUpdateFletestDataToUpload");
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/upload_update_file.csv";
	
	waitforPagetobeenable();
	
	waitforPagetobeenable();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Upload Update file");
	
	javaScriptclick(Manage_PostcodeObj.ManageColt.Uploadupdatefile);
	
	//Log.info("Clicked on Upload update file");
	Report.LogInfo("INFO","Clicked on Upload update file","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Upload update file");

	waitforPagetobeenable();
	
	compareText("Choose file", Manage_PostcodeObj.ManageColt.Chosefile, "Choose file");
	
	compareText("OK",Manage_PostcodeObj.ManageColt.OkbtnPostcode, "OK");
	
	compareText("Cancel", Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
	
	//verifyExists(Manage_PostcodeObj.ManageColt.Chosefile,"Choose File");
	//sendKeys(Manage_PostcodeObj.ManageColt.Chosefile, filepath,"Choose File");
	
	verifyExists(Manage_PostcodeObj.ManageColt.upload_update_choosefilebutton,"Upload Uupdate File");
	sendKeys(Manage_PostcodeObj.ManageColt.upload_update_choosefilebutton, uploadFilePath,"Upload Uupdate File");
	
	 
	ExtentTestManager.getTest().log(LogStatus.PASS, "File uploaded");
	
	javaScriptclick(Manage_PostcodeObj.ManageColt.OkbtnPostcode);
	
	Report.LogInfo("INFO","Clicked on OK button","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on OK button");
	 
	waitforPagetobeenable();
     
	verifysuccessmessage("Emergency Number successfully updated.PSX sync started successfully.");
		
	try {	
			
		boolean successmEssage_UploadUpdateFile= findWebElement(Manage_PostcodeObj.ManageColt.successmessageForUploadUpdateFile).isDisplayed();
	
		if(successmEssage_UploadUpdateFile) {
			
			Report.LogInfo("INFO","upload success message is displaying","PASS");
		}else {
			verifyExists(Manage_PostcodeObj.ManageColt.Cancelbtn,"cancel");
			click(Manage_PostcodeObj.ManageColt.Cancelbtn,"cancel");
		}
	}catch(Exception e) {
		e.printStackTrace();
		
		verifyExists(Manage_PostcodeObj.ManageColt.Cancelbtn,"cancel");
		click(Manage_PostcodeObj.ManageColt.Cancelbtn,"cancel");
	}
		}

public void verifyAddEmergencyNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws Exception 
{
	//String filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "filePathForAddEmergencyNumberTestDataToUpload");
	
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/Add_Emergency_Numbers.csv";
	
	waitforPagetobeenable();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Add Emergency number");

	verifyExists(Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Number");
	click(Manage_PostcodeObj.ManageColt.AddEmergencyNumber,"Add Emergency Number");
	
	Report.LogInfo("INFO","Clicked on Add Emergency Number","PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Add Emergency Number");

	waitforPagetobeenable();

	waitforPagetobeenable();
	
    compareText("Choose file", Manage_PostcodeObj.ManageColt.Chosefile, "Choose file");
	
	compareText("OK", Manage_PostcodeObj.ManageColt.OkbtnPostcode, "OK");
	
	compareText("Cancel", Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
	
	//verifyExists(Manage_PostcodeObj.ManageColt.Chosefile,"Choose file");
	//sendKeys(Manage_PostcodeObj.ManageColt.Chosefile, uploadFilePath,"Choose file");
	
	verifyExists(Manage_PostcodeObj.ManageColt.upload_update_choosefilebutton,"Add Emergency Numbers");
	sendKeys(Manage_PostcodeObj.ManageColt.upload_update_choosefilebutton, uploadFilePath,"Add Emergency Numbers");

	ExtentTestManager.getTest().log(LogStatus.PASS, "File uploaded");

	verifyExists(Manage_PostcodeObj.ManageColt.OkbtnPostcode,"OK");
	click(Manage_PostcodeObj.ManageColt.OkbtnPostcode,"OK");
	 
	verifysuccessmessage("Emergency Number successfully created.PSX sync started successfully. Please check the status of the Postcodes.");
	 
	 
	 try {
			boolean ActNtServicemsg = findWebElement(Manage_PostcodeObj.ManageColt.successMessageForEmergencyNumber).isDisplayed();

			if(ActNtServicemsg) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message mismatches might text chnage");
				Report.LogInfo("INFO","Success Message mismatches might text chnage","PASS");
				
			}else {
				verifyExists(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
				click(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
			}
			}catch(Exception e) {
				e.printStackTrace();
				verifyExists(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
				click(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
			}
	
}
public void verifyUploadNtserviceArea (String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws Exception 
		{
	//String filepath=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"filePathForUploadNTServiceAreaTestDataToUpload");
	
	String path1 = new File(".").getCanonicalPath();
	String uploadFilePath = path1+"/Upload_NT_Service_Area.csv";
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Upload NT Service Area");
	
	verifyExists(Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");
	click(Manage_PostcodeObj.ManageColt.UploadNt,"Upload NT Service Area");
	
	Report.LogInfo("INFO","Clicked on Upload NT Service Area","PASS");
	
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Upload NT Service Area");
	waitforPagetobeenable();
	
	
	waitforPagetobeenable();
	
	compareText("Choose file", Manage_PostcodeObj.ManageColt.Chosefile, "Choose file");
	
	compareText("OK", Manage_PostcodeObj.ManageColt.OkbtnPostcode, "OK");
	
	compareText("Cancel", Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
	
	//verifyExists(Manage_PostcodeObj.ManageColt.ChosefileTrans,"Choose File Trans");
	//sendKeys(Manage_PostcodeObj.ManageColt.ChosefileTrans, filepath, "Choose File Trans");
	
	verifyExists(Manage_PostcodeObj.ManageColt.upload_update_choosefilebutton,"Upload NT Service Area");
	sendKeys(Manage_PostcodeObj.ManageColt.upload_update_choosefilebutton, uploadFilePath,"Upload NT Service Area");
	
	ExtentTestManager.getTest().log(LogStatus.PASS, "File uploaded");
	
	 javaScriptclick(Manage_PostcodeObj.ManageColt.OkbtnPostcode,"Ok Button");
	 
	 Report.LogInfo("INFO","Clicked on OK button","PASS");
	 ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on OK button");
			
			//verifysuccessmessage("NT Service Area successfully updated.");
			
			try {
			boolean ActNtServicemsg = findWebElement(Manage_PostcodeObj.ManageColt.AlertUploadNt).isDisplayed();

			if(ActNtServicemsg) {
				Report.LogInfo("INFO","Success Message displays","PASS");
				
			}else {
				verifyExists(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
				click(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
			}
			}catch(Exception e) {
				e.printStackTrace();
				verifyExists(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
				click(Manage_PostcodeObj.ManageColt.Cancelbtn, "Cancel");
			}
	
		}

}
