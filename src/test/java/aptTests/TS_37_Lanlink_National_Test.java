
package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.relevantcodes.extentreports.LogStatus;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.Lanlink_National;
import testHarness.aptFunctions.Lanlink_OLO;

public class TS_37_Lanlink_National_Test extends SeleniumUtils {

	APT_Login Login = new APT_Login();
	Lanlink_National National = new Lanlink_National();
	
	
	Properties prop = new Properties();

	public String deviceName_Equip = null;
	public String devicename_IntEquipment = null;
	public String siteOrderValue = null;

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void LANLINK_OLO(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName) throws Exception {

		Report.createTestCaseReportHeader(ScenarioName);

		try (InputStream input = new FileInputStream(g.getRelativePath() + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			
			
			String CustomerName1=null;
			String devicename_intEquip = null;
			String manageAdres_intEquip = null;
			String ServiceID = null;
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");
			
			String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"serviceNumber");
			String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_serviceNumber");
			String devicename_IntEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"devicename_IntEquipment");
			String Interfacename_forEditInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "Interfacename_forEditInterface");
			String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RemoveInterface_Selection");
			String Siteordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Siteordernumber");
			String siteOrderNumber_10G_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "siteOrderNumber_10G_PointToPoint");
			String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"siteOrderNumber_PointToPoint");
			String siteOrderNumber_p2p_mspSelected = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "siteOrderNumber_p2p_mspSelected");
			String ServiceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");

			String EDIT_Intequip_cpe_deviecname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"EDIT_Intequip_cpe_deviecname");
			String device_intEquip_name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"device_intEquip_name");
			String intEquip_existingDeviceValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"intEquip_existingDeviceValue");

			String Modularmsp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");
		String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"AddInterface_Selection");
			String deviceCreation_IntermediateEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "AddInterface_Selection");
			String speed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
			String ModularMSp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");
			String existingDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"IntEquip_existingdeviceSelection");
			String newDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"IntEquip_newdeviceSelection");
			String vpnTopology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vpnTopology");
			String circuitType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CircuitType");
			String commandIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CommandIPv4_Routertool");
			String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Interfacespeed");
			String deviceCreation_Equipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"deviceCreation_Equipment");
			String Technologyname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"technology");
			
			String devicename = null;
			String sitePreferenceType = "Null";
			String siteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"siteOrder_sitePreferenceType");
			String editSiteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "editSiteOrder_sitePreferenceType");


			String Equip_existingDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "Equip_existingDevicename");
			String devicename_equip = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"devicename_equip");
			String EDIT_cpename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"EDIT_cpename");
					
			

				if(newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
						
						//CreateNewCustomer_Lanlink_National
						National.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
							
						//selectExistingCustomer_National"); 
						National.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);						
						
				}
				else if(newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
						//selectExistingCustomer_Lanlink_National"); 
						National.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
					
				}
				
			//verifyListofFieldsForOrderandServicetype_LANLINK_National");
					National.Verifyfields(testDataFile, dataSheet, scriptNo, dataSetNo);
				//	National.selectCustomertocreateOrderfromleftpane(CustomerName1);
					National.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
					National.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
					National.selectServiceType(ServiceType);
					
				
			//selectTheServiceType_LANLINK_National");
					National.selectsubtypeunderServiceTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);
					
				
			//verifyFieldsFortheSubServicetypeselelcted_LANLINK_National");
					National.VerifyFieldsForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);	
					

			//enterDatafortheServiceSubtypeSelected_LANLINK_National");
					National.selectOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
					National.selectServiceType(ServiceType);
					National.selectsubtypeunderServiceTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);
					National.dataToBeEnteredOncesubservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);
					 National.verifysuccessmessage("Service successfully created");
					 
				
//			//verifyUserDetailsInformation_LANLINK_National");
//				National.VerifyUsersPanel();
					
					 
			//verifyOrderDetailsInformation_LANLINK_National");
				National.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				National.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);		 
				
					 
			//verifydEnteredValuesforService_LANLINK_National");
					 National.VerifydatenteredForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);
					 	
					
			//editService_LANLINK_National");
					 National.EditTheservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);
					 
					 
			//successmessageforServiceUpdation_LANLINK_National");
					 National.verifysuccessmessage("Service successfully updated.");
					 
			
					 String proactiveMonitorvalue = National.fetchProActiveMonitoringValue();
					 
			//synchronizeService_LANLINK_National");
					National.syncservices();
								
						
			//ManageSubnets_LANLINK_National");
					National.manageSubnets();
					
		
			//Dump_LANLINK_National");
					National.dump_viewServicepage();		
					
				
			/**
			 * Site Order		
			 */
					
			//verifyAddSiteOrderFields_LANLINKNational");
					
					
			if((vpnTopology.equals("Point-to-Point")) &&  (circuitType.equals("Composite Circuit"))){
						
					Report.LogInfo("Info", "Site order' Page will not display, if we select 'VPN TOpology' as 'Point-to-Point' "
							+ "and 'Circuit Type' as 'Composite Circuit' ","Info");
					
						
			}
			else {
				
				Report.LogInfo("Info", "Verifying 'Add Site Order' fields","Info");
						National.Enteraddsiteorder();
						National.verifyAddsiteorderFields(testDataFile, dataSheet, scriptNo, dataSetNo);
						
					
						
					
					//EnterValueInAddSiteOrderFields_LANLINK");	
						Report.LogInfo("Info","create Site Order'","Info");	
						National.Enteraddsiteorder();
						National.addsiteorder(testDataFile, dataSheet, scriptNo, dataSetNo);
						
						
				//Successmessage_siteOrderCreation_LANLINK_National");
						National.verifysuccessmessage("Site order created successfully");
						
					 	
				//verifyEnteredValues_ForSiteOrder_LANLINK_National");
						National.VerifyDataEnteredForSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
						
				
				//editSiteOrder_LANLINK_National");
						National.returnbacktoviewsiteorderpage();						
						National.editSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.verifysuccessmessage("Site Order successfully updated.");
						
				}
					
					
		//For Device, Circuit creation					
		
			if(editSiteOrder_sitePreferenceType.equalsIgnoreCase("Null")) {
				sitePreferenceType = siteOrder_sitePreferenceType;
			}
			else {
				sitePreferenceType = editSiteOrder_sitePreferenceType;
			}
			
		if(((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit")))  ||  ((vpnTopology.equals("Point-to-Point")) &&  (circuitType.equals("Extended Circuit"))  &&  (sitePreferenceType.equalsIgnoreCase("Circuit")))){
				
				
				if(Edit_serviceNumber.equalsIgnoreCase("null")) {
					ServiceID=serviceNumber;
				}else {
					ServiceID=Edit_serviceNumber;
				}
				
				if(Interfacespeed.equals("1GigE")) {
					
					//Overture
					//CircuitCreationFor1G_Overture_LanlinkNational");
						National.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.verifysuccessmessage("Circuit successfully created");
						National.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
						National.deleteCircuit();
						
						
					//Accedian-1G
						//CircuitCreationFor1G_Accedian-1G_LanlinkNational");
						National.addAccedianCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.verifysuccessmessage("Circuit successfully created");
						National.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
						National.deleteCircuit();
						
						
				//Atrica
						//CircuitCreationFor1G_Atrica_LanlinkNational");
						National.addAtricaCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.verifysuccessmessage("Circuit successfully created");
						National.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
						National.deleteCircuit();
						
					}
					else if(Interfacespeed.equals("10GigE")) {
						
						//create Circuit for 10G");
						//Overture	
						National.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.verifysuccessmessage("Circuit successfully created");
						National.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
						National.deleteCircuit();
						
					}
			}
			else {		
			//Actelis_Equipment Configuration_LANLINK_National");

						
					if(Technologyname.equalsIgnoreCase("Actelis")) {
					boolean equipConfigurationPanel=National.EquipmentCOnfigurationPanel();
					if(equipConfigurationPanel) {
						Report.LogInfo("Info", "verifying 'Actelis CPE Device'","Info");
						National.equipConfiguration_Actelis_AddDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.verifysuccessmessage("Device successfully created");
						National.verifyDataEnteredFordeviceCreation_Actelis(testDataFile, dataSheet, scriptNo, dataSetNo);
						National.returnbacktoviewsiteorderpage();
						National.deleteDeviceFromService_EquipmentConfig_Actelis();
						National.verifysuccessmessage("Actelis CPE Device successfully deleted and removed from service");
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Equipment Configuration' panel is not displaying");
					}
				
		
				//Actelis Configuration panel	
					//Actelis Configuration Panel_Add DSLAM and HSL_LANLINK_National");
					National.verifyAddDSLAMandHSLlink();
					National.AddDSLAMandHSL(testDataFile, dataSheet, scriptNo, dataSetNo);
					National.showInterface_ActelisConfiguuration();
					National.deletInterface_ActelisConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					National.successMessage_deleteInterfaceFromDevice_ActelisConfiguration();
					
					}else {
						Report.LogInfo("Info", "Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page","Info");
						Reporter.log("Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page");
							
					}
				
					
				
			/**
			 * Deviec creation_Equipment		
			 */
			//AdddeviceforEquipment_LANLINK_National");
					//verify whether Equipment panel is available	
					boolean EquipmentPanel=National.findPanelHeader("Equipment");
					if(EquipmentPanel) {
						//Verify whether Equipment device to be created
						if(deviceCreation_Equipment.equalsIgnoreCase("yes")){
							Report.LogInfo("Info", " Device to be created for Eqiupment as per input provided","Info");	
							Report.LogInfo("Info", "Under Equipement, list of actions to be performed are: "
									+ "Verify fields for Add device"
									+ "Add device"
									+ "Verify entered values for device"
									+ "Edit device"
									+ "Select Interface"
									+ "Configure Interface -- Edit Inteface"
									+ "show/Hide Interface -- Edit Interface"
									+ "Select Interface -- Add Interface to service , Remove Interface from Service"
									+ "Delete device ","Info");
					
						
				if(ModularMSp.equalsIgnoreCase("Yes")) 
					{
						if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							Report.LogInfo("Info", "selectExistingDevice_MSPselected_Equipment","Info");
							National.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
									dataSheet, scriptNo, dataSetNo, speed);
							National.verifysuccessmessage("Device successfully created");
							National.verifyValuesforCPEexistingdevice_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
							
							
							//editExistingDevice_MSPselected_Equipment_LANLINK_National");
							National.eDITCPEdevicedetailsentered_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
							National.verifysuccessmessage("Device successfully updated");
							
						}
						else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
							Report.LogInfo("Info", "addNewDevice_MSPselected_Equipment_LANLINK_National","Info");
							National.verifyFieldsandAddCPEdevicefortheserviceselected_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
							National.verifysuccessmessage("Device successfully created");
							
							
							
							//verifyEnteredValues_MSPselected_Eqiupment_LANLINK_National");
							National.verifydetailsEnteredforCPEdevice_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
							
							
							//editDevice_MSPselected_Equipment_LANLINK_National");
							National.eDITCPEdevicedetailsentered_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
							National.verifysuccessmessage("Device successfully updated");
							
						}
					}
					else
					{
						if(speed.equals("1GigE")) {
							if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								Report.LogInfo("Info", "selectExistingDevice_1G_Equipment","Info");
								National.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
										dataSheet, scriptNo, dataSetNo, speed);
								National.verifysuccessmessage("Device successfully created");
								National.verifyValuesforCPEexistingdevice_1G_Equipment(testDataFile, dataSheet, scriptNo, dataSetNo);
								
								
								//editExistingDevice_1G_Equipment_LANLINK_National");
								National.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage("Device successfully updated");	
								
							}
							else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
								Report.LogInfo("Info", "addNewDevice_1G_Equipment","Info");
								National.verifyFieldsandAddCPEdevicefortheserviceselected_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage("Device successfully created");
								
								
								//verifyEnteredValues_1G_Equipment_LANLINK_National");
								National.verifydetailsEnteredforCPEdevice_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
								
								
								//editDevice_1G_Equipment_LANLINK_National");
								National.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage("Device successfully updated");
								
							}
						}
						if(speed.equals("10GigE")) {
							if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								Report.LogInfo("Info", "selectExistingDevice_10G_Equipment","Info");
								National.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
										dataSheet, scriptNo, dataSetNo, speed);
								National.verifysuccessmessage("Device successfully created");
								National.verifyValuesforCPEexistingdevice_10G_Equipment(testDataFile, dataSheet, scriptNo, dataSetNo);
								
								
								//editExistingDevice_10G_Equipment_LANLINK_National");
								National.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage("Device successfully updated");
								
							}
							else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
								Report.LogInfo("Info", "addNewDevice_10G_Equipment","Info");
								National.verifyFieldsandAddCPEdevicefortheserviceselected_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage("Device successfully created");
								
								
								
								//verifyEnteredValues_10G_Equipment_LANLINK_National");
								National.verifydetailsEnteredforCPEdevice_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
								
								
								//editDevice_10G_Equipment_LANLINK_National");
								National.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage("Device successfully updated");
								
							}
						}
					}	
					
							
							//get Device name 	
								if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
									devicename=Equip_existingDevicename;
								}
								else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
									if(EDIT_cpename.equalsIgnoreCase("null")) {
										devicename=devicename_equip;
									}
									else if(!EDIT_cpename.equalsIgnoreCase("null")) {
										devicename=EDIT_cpename;
									}	
								}
								
						//Navigate to view device page 	
							National.Equip_clickonviewButton(devicename);
							
						//devicename
							String devicename_EquipActual=null;
							devicename=National.fetchdevicename_InviewPage();
							if(devicename.contains("...")) {
								devicename_EquipActual = devicename.substring(0, 10);
							}else {
								devicename_EquipActual=devicename;
							}
							
							deviceName_Equip=devicename_EquipActual;
							
							String manageAdres=National.fetchManagementAddressValue_InviewPage();		//Management Address
							String vendorModel=National.fetchVendorModelValue();		//Vendor/Model
						
							
						//Perform fetch from Interface
						//fetchdeviceInterface_LANLINKNational");
							//National.testStatus();
							boolean link=National.fetchDeviceInterface_viewdevicepage();
						
						
						
						//routerTools_Equipment_LANLINKNational");
							National.routerPanel(commandIPv4,manageAdres);
							
						
						//configureEquipment_LANLINKNational");
							//Site Order Number	
							String siteOrderNumber = null;
							if (vpnTopology.equals("Point-to-Point")) {
								if (Modularmsp.equalsIgnoreCase("Yes")) {
									siteOrderNumber = siteOrderNumber_p2p_mspSelected;
								} else {
									if (Interfacespeed.equalsIgnoreCase("1GigE")) {
										siteOrderNumber = siteOrderNumber_PointToPoint;
									} else if (Interfacespeed.equalsIgnoreCase("10GigE")) {
										siteOrderNumber = siteOrderNumber_10G_PointToPoint;
									}
								}
							} else {
								siteOrderNumber =Siteordernumber;
							}
						
							siteOrderValue=siteOrderNumber;
							
							if(ModularMSp.equalsIgnoreCase("Yes")) {
								Report.LogInfo("Info", "Configure link do not display, if 'MSP' selected","Info");
								
							}else {
								National.clickOnBreadCrump(siteOrderNumber);
							National.selectconfigurelinkAndverifyEditInterfacefield__Equipment(deviceName_Equip);
							String interfaceavailability_configure = National.EnterdataForEditInterfaceforConfigurelinkunderIntermediateEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
							if(interfaceavailability_configure.equalsIgnoreCase("Yes")) {
								National.verifyeditedinterfacevalue(testDataFile, dataSheet, scriptNo, dataSetNo);
							}
							
							}
							
							
						//selectInterface_Equipment_LANLINKNational");
							National.clickOnBreadCrump(siteOrderValue);
							National.selectInterfacelinkforEqipment(deviceName_Equip);
							if(RemoveInterface_Selection.equalsIgnoreCase("yes")) {
								National.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
							}else {
								ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed");
							}
							if(AddInterface_Selection.equalsIgnoreCase("yes")) {
								National.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
							}else {
								ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed");
							}
							
							
							
						//showInterface_Equipment_LANLINKNational");
							National.clickOnBreadCrump(siteOrderValue);
							String interfaceAvailability = National.SelectShowInterfacelinkAndVerifyEditInterfacePage(testDataFile, dataSheet, scriptNo, dataSetNo, devicename_EquipActual);
							if(interfaceAvailability.equalsIgnoreCase("Yes")) {
								National.EnterdataForEditInterfaceforShowInterfacelinkunderEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
								
							}
							
							

							//AMNvalidator_Equipment_LANLINKNational");
							// if(proactiveMonitorvalue.equalsIgnoreCase("Yes")) {
								 if(("Yes").equalsIgnoreCase("Yes")) {
								 National.clickOnBreadCrump(siteOrderNumber);
									String csrName=National.fetchCSRsiteName();
									String cityName=National.fetchDeviceCityName();
									String countryName=National.fetchSiteOrderCountryName();
									National.clickOnAMNvalidatorLink();
									National.AMNvalidator(siteOrderNumber ,deviceName_Equip, csrName, cityName,countryName);
									
							 }else {
								 Report.LogInfo("Info", "'AMN Validator' link do not display, as 'proactive Monitoring' checkbox is not selected ","Info");
								 
							 }
							
						//deleteDevice_Equipment_LANLINKNational");
							National.clickOnBreadCrump(siteOrderValue);
							National.deleteDeviceFromServiceForequipment(deviceName_Equip);
							National.successMessage_deleteFromService();
							
							
						}else {
								ExtentTestManager.getTest().log(LogStatus.PASS, "Equipment device is not created as expected");
								System.out.println("Equipment device is not created as expected");
								
							}
						}	else {
							Report.LogInfo("Info", " 'Equipment' panel is not displaying under 'view site order' page","Info");
							System.out.println(" 'Equipment' panel is not displaying under 'view site order' page");
							
						}
					
					
					/**
					 * device creation_Intermediate Equipment
					 */
					//IntermediateEquipment_LANLINKNational");
					waitForAjax();
					
					boolean IntermediateEquipmentPanel=National.findPanelHeader("Intermediate Equipment");
					if(IntermediateEquipmentPanel) {
						if(deviceCreation_IntermediateEquipment.equalsIgnoreCase("yes")){
							Report.LogInfo("Info", " Device to be created for Intermediate Eqiupment as per input provided","Info");	
							Report.LogInfo("Info", "Under Intermediate Equipement, list of actions to be performed are: "
									+ "Verify fields for Add device"
									+ "Add device"
									+ "Verify entered values for device"
									+ "Edit device"
									+ "Select Interface"
									+ "show/Hide Interface -- Edit Interface"
									+ "Select Interface -- Add Interface to service , Remove Interface from Service"
									+ "Delete device ","Info");
						
							
						if(ModularMSp.equalsIgnoreCase("yes")) 
						{
							if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
								Report.LogInfo("Info", "selectExistingDevice_MSPselected_IntEquipment","Info");
								National.addDevice_IntEquipment();
								National.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(intEquip_existingDeviceValue);
								National.verifyValuesforCPEexistingdevice_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
								
								
								//editCPEdevice_MSPselected_IntermediateEquipment_LANLINKNational");
								National.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage("Device successfully updated");
								
								
							}else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
								Report.LogInfo("Info", "addNewDevice_MSPselected_IntEquipment","Info");
								National.addDevice_IntEquipment();
								National.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);	
								National.verifysuccessmessage("Device successfully created");
								
								
								//verifyEnteredValues_MSPselected_IntermediateEquipment_LANLINKNational");
								National.verifyCPEdevicedataenteredForIntermediateEquipment_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
								
								
								//editDevice_MSpselected_IntermediateEquipment_LANLINKNational");
								National.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
								National.verifysuccessmessage( "Device successfully updated");
								
							}
							}
						else
						{
							if(speed.equals("1GigE")) {
								if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
									Report.LogInfo("Info", "selectExistingDevice_1G_IntEquipment","Info");
									National.addDevice_IntEquipment();
									National.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									National.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(intEquip_existingDeviceValue);
									National.verifyValuesforCPEexistingdevice_1G_intEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
									
									
									//editCPEdevice_1G_IntermediateEquipment_LANLINKNational");
									National.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
									National.verifysuccessmessage("Device successfully updated");
									
									  
								  }else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
									  Report.LogInfo("Info", "addNewDevice_1G_IntEquipment","Info");
									  National.addDevice_IntEquipment();
									  National.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									  National.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(testDataFile, dataSheet, scriptNo, dataSetNo);	
									 National.verifysuccessmessage("Device successfully created");
										
										
										//verifyEnteredValues_1G_IntermediateEquipment_LanlinkNational");
										National.verifyCPEdevicedataenteredForIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
										
										
										//editCPEdevice_1G_IntermediateEquipment_LANLINKNational");
										National.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
										National.verifysuccessmessage("Device successfully updated");
										
								  }
								}
								
								else if(speed.equals("10GigE")) {
									if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
										Report.LogInfo("Info", "selectExistingDevice_10G_IntEquipment","Info");
										National.addDevice_IntEquipment();
										National.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
										National.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(intEquip_existingDeviceValue);
										National.verifyValuesforCPEexistingdevice_10G_intEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
										
										
										//editCPEdevice_10G_IntermediateEquipment_LANLINKNational");
										National.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
										National.verifysuccessmessage("Device successfully updated");
										
											
									}else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
										Report.LogInfo("Info", "addNewDevice_10G_IntEquipment","Info");
										National.addDevice_IntEquipment();
										National.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
										National.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(testDataFile, dataSheet, scriptNo, dataSetNo);	
										National.verifysuccessmessage("Device successfully created");
																				
										//verifyEnteredValues_10G_IntermediateEquipment_LANLINKNational");
										National.verifyCPEdevicedataenteredForIntermediateEquipment_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
										
										
										//editCPEDevice_10G_IntermediateEquipment_LANLINKNational");
										National.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
										National.verifysuccessmessage("Device successfully updated");
										
									}
								}
							}
							
							//Get Device Name	
								if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
									devicename_intEquip=intEquip_existingDeviceValue;
								}
								else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
									
									if(EDIT_Intequip_cpe_deviecname.equalsIgnoreCase("null")) {
										devicename_intEquip=device_intEquip_name;
									}
									else if(!EDIT_Intequip_cpe_deviecname.equalsIgnoreCase("null")) {
										devicename_intEquip=EDIT_Intequip_cpe_deviecname;
									}
								}
								
							//Navigate to view device page
							National.IntEquip_clickonviewButton(devicename_intEquip);
								
						
							//devicename
							String devicename_intEquipActual=null;
								devicename_intEquip=National.fetchdevicename_InviewPage();
								if(devicename_intEquip.contains("...")) {
									devicename_intEquipActual = devicename_intEquip.substring(0, 10);
								}else {
									devicename_intEquipActual=devicename_intEquip;
								}
								
								devicename_IntEquipment=devicename_intEquipActual;
								
							manageAdres_intEquip=National.fetchManagementAddressValue_InviewPage();		//Maagement Address
							String vendorModel_intEqiup=National.fetchVendorModelValue();	//Vendor/Model	
							String country_intEquip=National.fetchCountryValue_InviewPage();		//Country
						
						
							//Fetch device Interface
							//fetchDeviceInterface_IntEquipment_LanlinkNational");
							National.testStatus();
							boolean link=National.fetchDeviceInterface_viewdevicepage();
							waitForAjax();
							
						
							
							
					//routerTools_intermediateEquipment_LanlinkNational");
							National.routerPanel(commandIPv4,manageAdres_intEquip);
							
							
					//selectinterface_IntermediateEquipment_LanlinkNational");
							National.clickOnBreadCrump(siteOrderValue);
							National.selectInterfacelinkforIntermediateEqipment(devicename_IntEquipment);
							if(RemoveInterface_Selection.equalsIgnoreCase("yes")) {
								National.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
							}else {
								System.out.println("interfaces are not removed");
							}
							
							if(AddInterface_Selection.equalsIgnoreCase("yes")) {
								National.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
							}
							
							
					//showInterace_IntermediateEquipment_LanlinkNational");
							National.clickOnBreadCrump(siteOrderValue);
							National.SelectShowInterfacelink_IntermediateequipmentAndVerifyEditInterfacePage(
									Interfacename_forEditInterface, devicename_IntEquipment);
							National.EnterdataForEditInterfaceforShowInterfacelinkunderIntermediateEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
						
							
							
							
					//deletDeviceFromService_IntermediateEquipment_LanlinkNational");
							National.deleteDeviceFromServiceForIntermediateequipment(devicename_IntEquipment);
							National.successMessage_deleteFromService();
							
						}
						
							}else {
								ExtentTestManager.getTest().log(LogStatus.PASS, " 'Intermediate Equipment' panel is displaying under 'view site order' page");
								Reporter.log(" 'Intermediate Equipment' panel is displaying under 'view site order' page");
								
							}
					 
					
					//PAMTest_LanlinkNational");
							
							if(Edit_serviceNumber.equalsIgnoreCase("null")) {
								ServiceID=serviceNumber;
							}else {
								ServiceID=Edit_serviceNumber;
							}
							National.returnbacktoviewsiteorderpage();
							
							National.pamTest(ServiceID);
								
							
					//deleteSiteOrder_LanlinkNational");
							
							//National.deleteSiteOrder(siteOrderNumber_PointToPoint);
							
				}
			
			
				
					if(Edit_serviceNumber.equalsIgnoreCase("null")) {
						ServiceID=serviceNumber;
					}else {
						
						ServiceID=Edit_serviceNumber;
					}
				//deleteService_LanlinkNational");
					//	National.clickOnBreadCrump(ServiceID);
					//	National.deleteService();
						
		
			
		}

	catch (

			Exception e) {
				Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
				ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
				ExtentTestManager.getTest().log(LogStatus.INFO,
						ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

			}

		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();
	}
}