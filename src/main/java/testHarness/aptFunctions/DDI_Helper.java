package testHarness.aptFunctions;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import pageObjects.aptObjects.APT_DDIRangeObj;

public class DDI_Helper extends SeleniumUtils
{
	
	
	public void SearchTrunkName(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String TrunkValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkValue");
		
		verifyExists(APT_DDIRangeObj.DDI.ManageCustomerServiceLink,"ManageCustomerServiceLink");
        click(APT_DDIRangeObj.DDI.ManageCustomerServiceLink,"ManageCustomerServiceLink");
        
        ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Clicked on 'Manage Customer Service Link");
        Report.LogInfo("INFO", "Clicked on 'Manage Customer Service Link", "PASS");
        
		System.out.println("Clicked on 'Manage Customer Service Link");
		Reporter.log("=== MCS page navigated ===");

		
		verifyExists(APT_DDIRangeObj.DDI.SearchService,"SearchService");
		click(APT_DDIRangeObj.DDI.SearchService,"SearchService");
		
		waitForAjax();
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Clicked on 'Search Order/Service");
        Report.LogInfo("INFO", "Clicked on 'Search Order/Service", "PASS");

		
		System.out.println("Clicked on 'Search Order/Service");
		
		Reporter.log("=== Search Order/Service navigated ===");
		
		verifyExists(APT_DDIRangeObj.DDI.TrunkName,"Trunk Name");
		sendKeys(APT_DDIRangeObj.DDI.TrunkName, TrunkValue,"Trunk Name");
		
		scrollDown(APT_DDIRangeObj.DDI.Searchbtn);
		
		verifyExists(APT_DDIRangeObj.DDI.Searchbtn,"Search");
		click(APT_DDIRangeObj.DDI.Searchbtn,"Search");
		
		waitForAjax();
		scrollDown(APT_DDIRangeObj.DDI.DDICheckbox);
		
		verifyExists(APT_DDIRangeObj.DDI.DDICheckbox, "DDI checkbox");
		click(APT_DDIRangeObj.DDI.DDICheckbox, "DDI checkbox");
		
		verifyExists(APT_DDIRangeObj.DDI.searchDDi_actionDropdown,"Action");
		click(APT_DDIRangeObj.DDI.searchDDi_actionDropdown,"Action");
		
		verifyExists(APT_DDIRangeObj.DDI.searchDDI_viewLink, "View");
		click(APT_DDIRangeObj.DDI.searchDDI_viewLink, "View");
		
		waitForAjax();
			
	}
	
	
	public void selectTrunk(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String TrunkValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkValue");
		
		waitForAjax();
		//WebElement Trunknumber1=null;
		//Trunknumber1 = findWebElement((APT_DDIRangeObj.DDI.selectTrunkName).replace("value", TrunkValue));
		//boolean Trunk = Trunknumber1.isDisplayed();
		//if(Trunk)
		
		//	String Trunknumber= getAttributeFrom(APT_DDIRangeObj.DDI.selectTrunkName,"value");
		  //click(Trunknumber);

			//safeJavaScriptClick(getwebelement(xml.getlocator("//locators/" +application+ "/TrunkValue")));
		//	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Trunk number");
		//	System.out.println("Clicked on Trunk number");
			
		//	javaScriptclick(APT_DDIRangeObj.DDI.TrunkPanelAction);
			
		//	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Action Dropdown");
		//	javaScriptclick(APT_DDIRangeObj.DDI.TrunkPanelView);
		
		//tr//td[contains(.,'BELNASAAA73000000300')]//following-sibling::td//a[contains(.,'View')]
			
			waitForAjax();
			scrollDown(APT_DDIRangeObj.DDI.view);
			
			waitForAjax();
			
			webDriver.findElement(By.xpath("//tr//td[contains(.,'"+TrunkValue+"')]//following-sibling::td//a[contains(.,'View')]")).click();

	
	}
	
	public void showDDIRange() throws Exception
	{
		//WebElement ShowDDIRanges = findWebElement(APT_DDIRangeObj.DDI.ShowDDIRanges);
		scrollDown(APT_DDIRangeObj.DDI.ShowDDIRanges);
		
		waitForAjax();

		javaScriptclick(APT_DDIRangeObj.DDI.ShowDDIRanges);
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Show DDI Ranges");
        Report.LogInfo("INFO", "Clicked on Show DDI Ranges", "PASS");

		waitForAjax();
		
		verifyExists(APT_DDIRangeObj.DDI.CountrycodeLabel,"Country Code");
    	
		verifyExists(APT_DDIRangeObj.DDI.LacLabel,"LAC");

		verifyExists(APT_DDIRangeObj.DDI.MainNumberLabel,"Main Number/Range Start-End");

		verifyExists(APT_DDIRangeObj.DDI.ExtensionLabel,"Extension digits");

		verifyExists(APT_DDIRangeObj.DDI.EmergLabel,"Emerg Area");

		verifyExists(APT_DDIRangeObj.DDI.IncomingLabel,"Incoming Routing");

		verifyExists(APT_DDIRangeObj.DDI.InGeoLabel,"IN GEO");

	}
	
	
	public void UploadDDIRange(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String expected = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "expected");
	//	String uploadFilePath = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "filePath_forUploading");
	
		verifyExists(APT_DDIRangeObj.DDI.UploadDDIRanges,"UploadDDIRanges");
		javaScriptclick(APT_DDIRangeObj.DDI.UploadDDIRanges);
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Upload DDI Ranges");
        Report.LogInfo("INFO", "Clicked on Upload DDI Ranges", "PASS");

		waitForAjax();
		
		verifyExists(APT_DDIRangeObj.DDI.OKbutton,"OK");
		
    	ExtentTestManager.getTest().log(LogStatus.PASS,"Step : OK button is present");
        Report.LogInfo("INFO", "OK button is present", "PASS");


    	verifyExists(APT_DDIRangeObj.DDI.Cancelbtn,"Cancel");
    	
    	ExtentTestManager.getTest().log(LogStatus.PASS,"Step : Cancel button is present");
    	Report.LogInfo("INFO", "Cancel button is present", "PASS");
    	
    	try {
    	WebElement ChoseFile = findWebElement(APT_DDIRangeObj.DDI.ChosefileDDI);
		//sa.assertTrue(ChoseFile.isDisplayed(),"Chose File btn is not displayed");
		Reporter.log("ChoseFile is displayed");
		
		 System.out.println("ChoseFile is displayed");
		 
			ExtentTestManager.getTest().log(LogStatus.PASS, "Verified Chose File button is diplayed");
			Report.LogInfo("INFO", "Verified Chose File button is diplayed", "PASS");
			
			
			verifyExists(APT_DDIRangeObj.DDI.ChosefileDDI,"ChosefileDDI");
			String path1 = new File(".").getCanonicalPath();

			String uploadFilePath = path1+"/ddiRanges_Tgid_2298385545.csv";
			//try (InputStream input = new FileInputStream(path1+"/configuration.properties"))
			
			sendKeys(APT_DDIRangeObj.DDI.ChosefileDDI, uploadFilePath,"ChosefileDDI");
			 
			//	ExtentTestManager.getTest().log(LogStatus.PASS, "File uploaded");
				
				waitForAjax();
				
				javaScriptclick(APT_DDIRangeObj.DDI.OKbutton);
				
				waitForAjax();
				
				System.out.println("File Upload Message");
				
				WebElement ActFileuploadmsg = findWebElement(APT_DDIRangeObj.DDI.FileuploadSuccess);
				if (ActFileuploadmsg.getText().equals("File Upload successfull."))
				{
					Reporter.log("Wildcarde Message : " + ActFileuploadmsg.getText());
					ExtentTestManager.getTest().log(LogStatus.PASS, "File Upload message:" + ActFileuploadmsg.getText());
					Report.LogInfo("INFO", "File Upload message:", "PASS");
					System.out.println("Message Displayed as:"+ActFileuploadmsg.getText());
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step:File Upload message:" +ActFileuploadmsg.getText());

				}
				else
				{
					//sa.assertTrue(ActFileuploadmsg.isDisplayed(),"Message not displayed");
					Reporter.log("File upload message not displayed");
					System.out.println("File upload message not displayed");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step:File upload message not displayed" );
					Report.LogInfo("INFO", "File upload message not displayed", "PASS");

				}	
				
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
					
				waitForAjax();
				verifyExists(APT_DDIRangeObj.DDI.Cancelbtn,"Cancelbtn");
				javaScriptclick(APT_DDIRangeObj.DDI.Cancelbtn);
				waitForAjax();

	}
	
	public void downloadDDIRange()throws Exception ,InterruptedException, IOException
	{
		
		scrollDown(APT_DDIRangeObj.DDI.ShowDDIRanges);
		waitForAjax();
		
		//WebElement ShowDDIRanges = getwebelement(xml.getlocator("//locators/" + application + "/ShowDDIRanges"));

		scrollDown(APT_DDIRangeObj.DDI.ShowDDIRanges);
		waitForAjax();
		
		verifyExists(APT_DDIRangeObj.DDI.DownlaodDDIRanges,"DownlaodDDIRanges");
		click(APT_DDIRangeObj.DDI.DownlaodDDIRanges,"DownlaodDDIRanges");
		
		waitForAjax();
		
	//	String path1 = new File(".").getCanonicalPath();

		//String uploadFilePath = path1+"/DownloadedFile.csv";
		//try (InputStream input = new FileInputStream(path1+"/configuration.properties"))
		
	//	 sendKeys(APT_DDIRangeObj.DDI.ChosefileDDI, uploadFilePath,"ChosefileDDI");
		//isFileDownloaded_DDI("D:\\APT_Phase1_Automation_Project_Deliverables\\APTAutomationProject\\src", "ddiRanges_Tgid2");
		
		waitForAjax();
	}
	
	public boolean isFileDownloaded_DDI(String downloadPath, String fileName) {
		  File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();

		  for (int i = 0; i < dirContents.length; i++) {
		      if (dirContents[i].getName().contains(fileName)) {
		          // File has been found, it can now be deleted:
		          //dirContents[i].delete();
		    	  
		    	  String downloadedFileName=dirContents[i].getName();
		    	  System.out.println("Downloaded file name is displaying as: "+ downloadedFileName);
		    	  ExtentTestManager.getTest().log(LogStatus.PASS, "Downloaded file name is displaying as: "+ dirContents[i]);
		    	  
		          return true;
		      }
		          }
		      return false;
		  }
	
	
	public void duplicateDDIRange() throws InterruptedException, IOException
	{
		scrollDown(APT_DDIRangeObj.DDI.DDIRange_Header);
		waitForAjax();
		
		try {
		verifyExists(APT_DDIRangeObj.DDI.DuplicateDDIRange ,"Duplicate DDI Range");
		click( APT_DDIRangeObj.DDI.DuplicateDDIRange ,"Duplicate DDI Range");
		
		waitForAjax();
		
      WebElement NoDuplicateDDI = findWebElement(APT_DDIRangeObj.DDI.NoDuplicateRecord);
     // boolean NoDuplicateDDIMessage=NoDuplicateDDI.isDisplayed();
      
		if(NoDuplicateDDI.isDisplayed()) {
			System.out.println("No Duplicate DDI Range  message is displaying as:" +NoDuplicateDDI.getText());
			ExtentTestManager.getTest().log(LogStatus.INFO, "No Duplicate DDI Range  message is displaying as: " +NoDuplicateDDI.getText());
			Report.LogInfo("INFO", "No Duplicate DDI Range  message is displaying as", "PASS");
		}
		else{
		  WebElement DuplicateDDI = findWebElement(APT_DDIRangeObj.DDI.DuplicateRecord);
		 // boolean DuplicateDDIMessage=DuplicateDDI.isDisplayed();
		if(DuplicateDDI.isDisplayed())
     {
			System.out.println("DDI Range Duplicate message is displaying as:" +DuplicateDDI.getText());
			ExtentTestManager.getTest().log(LogStatus.INFO, "DDI Range Duplicate message is displaying as: " +DuplicateDDI.getText());
			Report.LogInfo("INFO", "DDI Range Duplicate message is displaying as:", "PASS");
	}
		}
		
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO, "DDI Range not displayed in Duplicate DDI Range screen");
			Report.LogInfo("INFO", "DDI Range not displayed in Duplicate DDI Range screen", "PASS");
			System.out.println( "DDI Range not displayed in Duplicate DDI Range screen");
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO, "DDI Range not displayed in Duplicate DDI Range screen");
			Report.LogInfo("INFO", "DDI Range not displayed in Duplicate DDI Range screen", "PASS");
			System.out.println( "DDI Range not displayed in Duplicate DDI Range screen");
		}
	}
	
	
	public void searchDDIRange() throws InterruptedException, IOException
    {
		
  	  System.out.println("select Manage Colt's Network");
	  ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on 'Manage colt's Network' link");
	  Report.LogInfo("INFO", "clicked on 'Manage colt's Network", "PASS");


		waitForAjax();
		
		try {
			verifyExists(APT_DDIRangeObj.DDI.ManageColtNetworkLink,"ManageColtNetworkLink");
			mouseMoveOn(APT_DDIRangeObj.DDI.ManageColtNetworkLink);
			
			System.out.println("Mouse hovered on Manage Colt Network ");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Mouse hovered on Manage Colt Network");
			Report.LogInfo("INFO", "Mouse hovered on Manage Colt Network", "PASS");
			
			boolean isDisplayed = findWebElement(APT_DDIRangeObj.DDI.SearchDDI).isDisplayed();
			//sa.assertTrue(isDisplayed,"Search DDI Range not displayed ");
			
			System.out.println("Search DDI Range not displayed ");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Search DDI Ranges displayed ");
			Report.LogInfo("INFO", "Step :Search DDI Ranges displayed", "PASS");
			
			verifyExists(APT_DDIRangeObj.DDI.SearchDDI,"SearchDDI");
			click(APT_DDIRangeObj.DDI.SearchDDI,"SearchDDI");
			
			waitForAjax();
			
			Reporter.log("=== Clicked on Search DDI Ranges ===");
			System.out.println("Clicked on Search DDI Ranges");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Clicked on Search DDI Ranges");
			Report.LogInfo("INFO", "Step :Clicked on Search DDI Ranges", "PASS");

			
 		    ExtentTestManager.getTest().log(LogStatus.PASS, "Step:Navigated to Search DDI Ranges");
			Report.LogInfo("INFO", "Step :Navigated to Search DDI Ranges", "PASS");


 		   Reporter.log("=== Navigated to Search DDI Ranges ===");
			
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO, "Field not displayed for Search DDI Range screen");
			System.out.println( "Field not displayed for Search DDI Range screen");
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO, "Field not displayed for Search DDI Range screen");
			System.out.println( "Field not displayed for Search DDI Range screen");
		}
    }
	
	
	public void fillfieldDDIRange(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String LACValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LACValue");
		String CountrycodeValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountrycodeValue");
		
		 try {
			 verifyExists(APT_DDIRangeObj.DDI.DDIWildcard,"You can use % as wildcard");
		      	
			 verifyExists(APT_DDIRangeObj.DDI.DDIISdcode,"ISD Code(Country Code)");
			 
			 verifyExists(APT_DDIRangeObj.DDI.DDISTdcode,"STD Code(LAC)");

			 verifyExists(APT_DDIRangeObj.DDI.DDIManageNmbr,"Main Number/Range Start-End");
		    	
			 verifyExists(APT_DDIRangeObj.DDI.DDIISdcode_Text,"ISD Code(Country Code)");
			 sendKeys(APT_DDIRangeObj.DDI.DDIISdcode_Text, CountrycodeValue,"ISD Code(Country Code)");
			
			 verifyExists(APT_DDIRangeObj.DDI.DDILAC_Text,"STD Code(LAC)");
			 sendKeys(APT_DDIRangeObj.DDI.DDILAC_Text, LACValue,"STD Code(LAC)");
				
			 verifyExists(APT_DDIRangeObj.DDI.Searchbtn,"Search");
			 click(APT_DDIRangeObj.DDI.Searchbtn,"Search");
			 
			 waitForAjax();
				
		    	  }catch(NoSuchElementException e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.INFO, "Field not displayed for Fill Field DDI Range screen");
						System.out.println( "Field not displayed for Fill Field DDI Range screen");
					}catch(Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.INFO, "Field not displayed for Fill Field DDI Range screen");
						System.out.println( "Field not displayed for Fill Field DDI Range screen");
					}
	
	}
	
	public void viewSearchDDIRange(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String LACValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LACValue");
		String CountrycodeValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountrycodeValue");
		String trunkValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkValue");
		
	  waitForAjax();
   	  scrollDown(APT_DDIRangeObj.DDI.selectLACvalue_searchFoeDDipage);
	  waitForAjax();

	  			WebElement LACvalue=findWebElement((APT_DDIRangeObj.DDI.selectLACvalue_searchFoeDDipage).replace("value", trunkValue));
	  			System.out.println();
	  			
	  			verifyExists(APT_DDIRangeObj.DDI.selectLACvalue_searchFoeDDipage,"selectLACvalue_searchFoeDDipage");
	  			click(APT_DDIRangeObj.DDI.selectLACvalue_searchFoeDDipage,"selectLACvalue_searchFoeDDipage");
	  			
	  		  waitForAjax();
	  			try {
	  				scrollDown(APT_DDIRangeObj.DDI.searchForDDI_actionDropdown);
	  	  		  waitForAjax();

	  	  		verifyExists(APT_DDIRangeObj.DDI.searchForDDI_actionDropdown,"Action");
				click(APT_DDIRangeObj.DDI.searchForDDI_actionDropdown,"Action");
				
		  		waitForAjax();
		  		  
		  		verifyExists(APT_DDIRangeObj.DDI.ssearchForDDI_viewLink,"view");
				click(APT_DDIRangeObj.DDI.ssearchForDDI_viewLink,"view");
				
		  		 waitForAjax();
		  		 
				if(isVisible(APT_DDIRangeObj.DDI.ViewTrunk_Header)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "'View Trunk' page navigated as expected");
					Report.LogInfo("INFO", "'View Trunk' page navigated as expected", "PASS");

					System.out.println("'View Trunk' page navigated as expected");
					
				}else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "'View Trunk' page not navigated");
					Report.LogInfo("INFO", "'View Trunk' page not navigated", "PASS");

					System.out.println("'View Trunk' page not navigated");
				}
				
	  			}catch(NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.INFO, "Field not displayed for View Search DDI Range screen");
					System.out.println( "Field not displayed for View Search DDI Range screen");
				}catch(Exception e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.INFO, "Field not displayed for View Search DDI Range screen");
					System.out.println( "Field not displayed for View Search DDI Range screen");
				}

	}
	
	
	public void deleteDDIRange(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String LACValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LACValue");
		
		
		try {
  		  
	  		 waitForAjax();
	    	 scrollDown(APT_DDIRangeObj.DDI.DDIRange_Header);
	    	 
	    	 waitForAjax();
	    	 
	    	verifyExists(APT_DDIRangeObj.DDI.ShowDDIRanges,"ShowDDIRanges");
	    	click(APT_DDIRangeObj.DDI.ShowDDIRanges,"ShowDDIRanges");
	    	
	  		waitForAjax();
	  		
	  		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Show DDI Ranges");
			Report.LogInfo("INFO", "Clicked on Show DDI Ranges", "PASS");
	  		
			scrollDown(APT_DDIRangeObj.DDI.DDIDelete);
			waitForAjax();
	  		verifyExists(APT_DDIRangeObj.DDI.DDIDelete,"DDIDelete");
	  		click(APT_DDIRangeObj.DDI.DDIDelete,"DDIDelete");
	  		
	  		ExtentTestManager.getTest().log(LogStatus.PASS, "Selected Delete from Action Dropdown");
			Report.LogInfo("INFO", "Selected Delete from Action Dropdown", "PASS");

	  		waitForAjax();
	  		
	  		verifyExists(APT_DDIRangeObj.DDI.Deletebtn,"Deletebtn");
	  		click(APT_DDIRangeObj.DDI.Deletebtn,"Deletebtn");
	  		
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Delete button");
			Report.LogInfo("INFO", "Clicked on Delete button", "PASS");

	  		waitForAjax();
	  		
	  		verifyExists(APT_DDIRangeObj.DDI.DeleteDDIRangeSuccessMessage, "DDIRange Delete successfully");

	    	  }catch(NoSuchElementException e) {
	  			e.printStackTrace();
	  			ExtentTestManager.getTest().log(LogStatus.INFO, "DDI Range not displayed in Duplicate DDI Range screen");
	  			System.out.println( "DDI Range not displayed in Duplicate DDI Range screen");
	  		}catch(Exception e) {
	  			e.printStackTrace();
	  			ExtentTestManager.getTest().log(LogStatus.INFO, "DDI Range not displayed in Duplicate DDI Range screen");
	  			System.out.println( "DDI Range not displayed in Duplicate DDI Range screen");
	  		}

		
		
	}
	
	
	
	
	
	
	

}
