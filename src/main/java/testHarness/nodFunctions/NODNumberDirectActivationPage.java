package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODNumberDirectActivationPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder=new NODMyOrderPage();
	private static HashMap<String, String> Number= new HashMap<String, String>();
	String OrderStatus;
	


	public void DirectActivation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String NumberType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_Type");
		String BlockSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_Size");
		String QuantitySize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity_Size");
	    String CustomerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Type");
		String FirstName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"First_Name");
		String LastName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Last_Name");
		String LanguageType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Language_Type");
		String LanValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Lan_Value");
		String DirectoryListingValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Directory_Listing_Value");
		String DepartmentValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Department_Value");
		String LocalAreaCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Local_AreaCode");
		String MuncipalityVal = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Muncipality");
		
		 
		
		verifyExists(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODNumberDirectActivationObj.NumDirActivation.QuickLinkDrpDwnArw,"Quick Link Drop down");
		
			
	   if(isElementPresent(By.xpath("//div[contains(@class,'disabled')]")))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Direct activation in Italy is not allowed � please reserve the number(s) first and then request these number(s) be activated");	
		}
	   else
		{
        click(NODNumberDirectActivationObj.NumDirActivation.ActivationLink,"Activation link click");
		waitForAjax();
		waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.ActiationPage,15);
		if(isElementPresent(By.xpath("//*[contains(text(),'Select Customer Type')]")))
		{
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer",CustomerType),"Select Country");
		    javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.customerTypeRadioBtn.replace("Customer",CustomerType),"wholesaleradiobtn");
		    
		     if(CustomerType.toString().equals("Residential"))
		     {
		    	sendKeys(NODNumberDirectActivationObj.NumDirActivation.FirstName, FirstName, "Enter First Name");
		    	sendKeys(NODNumberDirectActivationObj.NumDirActivation.LastName, LastName, "Enter Last Name");
		    	verifyExists(NODNumberDirectActivationObj.NumDirActivation.Language,"Language");
		    	selectByValue(NODNumberDirectActivationObj.NumDirActivation.Language,LanValue,"LanguageType");
		    	sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber, "Enter House Number");
		 		sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
		 		sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
		 		sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
		     }
		     else if(CustomerType.toString().equals("Business")){
		    	sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName, "Enter Customer Name");
		    	Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.Language,LanguageType);
		 		sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber, "Enter House Number");
		 		sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
		 		sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
		 		sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
		     }
		}
		else{
		Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.ServiceType,ServiceType);
		
	    sendKeys(NODNumberDirectActivationObj.NumDirActivation.CustomerName, CustomerName, "Enter Customer Name");
		sendKeys(NODNumberDirectActivationObj.NumDirActivation.BuildingNumber, HouseNumber, "Enter House Number");
		sendKeys(NODNumberDirectActivationObj.NumDirActivation.StreetName, StreetName, "Enter Street Name");
		if (isElementPresent(By.xpath("//label[contains(text(),'Department')]"))){
	        selectByValue(NODNumberDirectActivationObj.NumDirActivation.DepartmentType,DepartmentValue,"Department Type");
	        WaitForAjax();
	        waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.CityOrTownFR,30);
	        Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.CityOrTownFR,CityTown);
	        waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.PostalCodeFR,15);
	    	Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.PostalCodeFR,PostalCode);
	    	}
		else{
		sendKeys(NODNumberDirectActivationObj.NumDirActivation.CityOrTown, CityTown, "Enter City Town");
		sendKeys(NODNumberDirectActivationObj.NumDirActivation.PostalCode, PostalCode, "Enter Postal Code");
		}
		if(isElementPresent(By.xpath("//label[contains(text(),'Municipality (Gemeinde)')]")))
        {                              
			// for CH country
			int MuncipalityValType = Integer.parseInt(MuncipalityVal.trim());
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.Muncipality,"Muncipality");
		    selectByIndex(NODNumberDirectActivationObj.NumDirActivation.Muncipality,MuncipalityValType,"Muncipality");
			
        }
		}
		if(isElementPresent(By.xpath("//button[contains(text(),'Validate Address')]")))
        {                              
			javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ValidateAddressBtn,"Validate Address Button");
			waitForAjax();
        }
		
		
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.SelectTypeAndQuantity," Select Type And Quantity Btn");
        
		verifyExists(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType",NumberType),"Select Number Type radio btn");
	    javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberTypeRadioBtn.replace("NoType",NumberType),"Number Type radio btn");
	    if(isElementPresent(By.xpath("//label[contains(text(),'Your Local Area Code (LAC)')]")))
	    {
	    	verifyExists(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"LocalAreaCode");
		    javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCode,"LocalAreaCode");
		    javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.LocalAreaCodeOption.replace("option",LocalAreaCode),"LocalAreaCode");
	    }
		Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.BlockSize,BlockSize);
		Reusable.Select(NODNumberDirectActivationObj.NumDirActivation.QuantitySize,QuantitySize);
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.ProvideNumbersBtn," Provide Numbers Btn");
		waitForAjax();
		waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox,15);
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberRangeChkBox," Number Range CheckBox");
		waitForAjax();
		waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn,15);
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.NumberAddOnsBtn," Number AddOns Btn");
		
		//click(NODNumberDirectActivationObj.NumDirActivation.DirectoryServiceUpdateChkBox,"Directory Service Update CheckBox");
		waitForAjax();
		if(isElementPresent(By.xpath("//label[contains(text(),'Directory Listing Option')]")))
		{
			verifyExists(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,"DirectoryListingOption");
	    	selectByValue(NODNumberDirectActivationObj.NumDirActivation.DirectoryListingOption,DirectoryListingValue,"DirectoryListingOption");
	    	
		}
		WaitForAjax();
		waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.OrderReview,15);
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.OrderReview," Order Review");
		WaitForAjax();
		waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.OrderConfirmationBtn,15);
		javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.OrderConfirmationBtn," Order Confirmation Btn");
		//WaitForAjax();
		waitForElementToBeVisible(NODNumberDirectActivationObj.NumDirActivation.ActivationConfirmationMsg,15);
	    if(isElementPresent(By.xpath("//div[@id='activation-l-6']/div[@class='row ng-star-inserted']")))
	    {
	    	String SuccessMessage=getTextFrom(NODNumberDirectActivationObj.NumDirActivation.SuccessMessage, "SuccessMessage");
	    	//String emailNotification=getTextFrom(NODNumberDirectActivationObj.NumDirActivation.EmailNotification, "SuccessMessage");
	    	ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage);
	    }
	    else{
	    	ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Activation is Not successful");
	    }
	    String DirectActivationOrderId=getTextFrom(NODNumberDirectActivationObj.NumDirActivation.ActivationOrderId, "Order Id");
	    WaitForAjax();
	    javaScriptclick(NODNumberDirectActivationObj.NumDirActivation.CheckMyOrder,"Check MyOrder Btn");
	    WaitForAjax();
	    waitForElementToDisappear(NODNumberDirectActivationObj.NumDirActivation.TableRecordLoader,120);
	    click(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld, "Send Order ID");
	    sendKeys(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld,DirectActivationOrderId , "Send Order ID");
	    WaitForAjax();
		click(NODNumberDirectActivationObj.NumDirActivation.SearchIcon,"Search Icon");
		waitForElementToDisappear(NODNumberDirectActivationObj.NumDirActivation.TableRecordLoader,120);
		waitForAjax();
		
		//String OrderStatus;
		if(isElementPresent(By.xpath("//a[@class='transaction']"))){
		 OrderStatus = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.orderStatusVal,"Order Status");
		}else
		{  
		    for(int i=0;i<5;i++)
            {          
		    	WaitForAjax();
				sleep(60000);
				refreshPage();
				waitForElementToDisappear(NODNumberDirectActivationObj.NumDirActivation.TableRecordLoader,120);
			    click(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld, "Send Order ID");
			    sendKeys(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld,DirectActivationOrderId , "Send Order ID");
			    WaitForAjax();
				click(NODNumberDirectActivationObj.NumDirActivation.SearchIcon,"Search Icon");
				//waitForAjax();
				waitForElementToDisappear(NODNumberDirectActivationObj.NumDirActivation.TableRecordLoader,120);
                                                         
                 if (isElementPresent(By.xpath("//a[@class='transaction']")))
                 {
                	 OrderStatus = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.orderStatusVal,"Order Status");  
                     break;         	
                 }
                          
	        }
		    
		/*	WaitForAjax();
			sleep(120000);
			refreshPage();
			WaitForAjax();
		    click(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld, "Send Order ID");
		    sendKeys(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld,DirectActivationOrderId , "Send Order ID");
		    WaitForAjax();
			click(NODNumberDirectActivationObj.NumDirActivation.SearchIcon,"Search Icon");
			//waitForAjax();
			waitForElementToDisappear(NODNumberDirectActivationObj.NumDirActivation.TableRecordLoader,120);
			OrderStatus = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.orderStatusVal,"Order Status"); */
		}
		if(OrderStatus.equals("In Progress")) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Status Is: "+OrderStatus);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Status is not Present");	
		}
		
		}
        
	}












}



