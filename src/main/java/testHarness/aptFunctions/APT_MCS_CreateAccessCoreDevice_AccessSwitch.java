package testHarness.aptFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;
import testHarness.commonFunctions.ReusableFunctions;

public class APT_MCS_CreateAccessCoreDevice_AccessSwitch extends SeleniumUtils{
	
	ReusableFunctions Reusable = new ReusableFunctions();
	WebElement el;

	SoftAssert sa = new SoftAssert();
	
	public void navigatetomanagecoltnetwork() throws InterruptedException, IOException
	{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink, "Manage Colt Network Link");
		mouseMoveOn(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink);
		waitForAjax();
	}
	
	public void navigatetocreateaccesscoredevicepage() throws InterruptedException, IOException
	 {		
	verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink, "Create Access Core Device Link");
	click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink, "Create Access Core Device Link");
	waitForAjax();
     }
	
		

	 public void managelink() throws InterruptedException, IOException {
		 scrollUp() ;
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage, "Manage Link View Device page");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,"Manage Link View Device page");
		
		 waitForAjax();
		}
	 
	
	 public void routerPanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	 {
		 String Command_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv4"); 
		 String Command_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv6");
		 String vrf_Ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrf_Ipv4");
		 String vrf_Ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrf_Ipv6");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown, "Command IPV4_dropdown");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown,"Command IPV4_dropdown");
		 ClickonElementByString("//div[text()='"+ Command_ipv4 +"']", 30);
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField, "commandIPv4 Vrf Textfield");
		 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField,vrf_Ipv4,"commandIPv4 Vrf Textfield");
		 
		 //verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield, "commandIPv4 Hostname Textfield");
		 //sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,newPremiseCode,"commandIPv4 Hostname Textfield");

		 //verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4, "Execute Button_Ipv4");
		 //click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4,"Execute Button_Ipv4");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown, "Command IPV6_dropdown");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown,"Command IPV6_dropdown");
		 ClickonElementByString("//div[text()='"+ Command_ipv6 +"']", 30);
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField, "commandIPv6 Vrf Textfield");
		 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,vrf_Ipv6,"commandIPv6 Vrf Textfield");
		 
		 //verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield, "commandIPv6 Hostname Textfield");
		 //sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield,newPremiseCode,"commandIPv6 Hostname Textfield");
		 
		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6, "Execute Button_Ipv6");
		// click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6,"Execute Button_Ipv6");
		 
	 }
	 
	 public void verifyDefaultSelection_connectivityprotocol_ssh() throws InterruptedException, IOException
	 {
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "ssh Radio Button");
		 isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "ssh Radio Button");
		 
		 
	 }
	 
	 public void verifyDefaultSelection_connectivityprotocol_telnet() throws InterruptedException, IOException
	 {
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
		 isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
	 }
	 
	 public void verifEditedValue_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	 {   
		
		 String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editDevicename");
		 String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
		 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editVendorModel");
		 String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editRouterID");
		 //String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editModularMSP");
		 //String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editFullIQNET");
		 //String editIOSXR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editIOSXR");
		 String editTelnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editTelnet");
		 String editSSH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp2C");
		 String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmProNewValue");
		 String editSnmprwNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmprwNewValue");
		 String editSnmpv3UsernameNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3UsernameNewValue");
		 String editSnmpv3AuthpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3AuthpasswordNewValue");
		 
		 String editSnmpv3PrivpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3PrivpasswordNewValue");
		 
		 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editManagementAddress");
		 
		 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
		 
		 String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
		 String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
		 String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
		 String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
		 
		 String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
		 String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
		 //String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
		 //String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
		 
		 //String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
		 //String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
		 //String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
		 
		 //String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
		 //String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
		// String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
		 //String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");
			//Device name
		 
		 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab, editDevicename);
			
		//Device Type
		 
		 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType, DeviceType);
			//compareText_InViewPage(application, "Device Type", DeviceType, xml);	
			
		//Vendor/Model
		 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model, editVendorModel);
		
		 //connectivity
		 if(editTelnet.equals("yes")&& editSSH.equals("no"))
		 {
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "telnet");
		 }
		 else if(editTelnet.equals("no")&& editSSH.equals("yes"))
		 {
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "ssh");
		 }
		 //router Id
		 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId, editRouterID);
		
		 //Country
		 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country, editCountry);
			
		 //Management Address
		 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd, editManagementAddress);
		 
		 if(editSnmp2C.equals("yes")&&editSnmp3.equals("no"))
		 {
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro, editSnmProNewValue);
				
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw, editSnmprwNewValue);
				
		 }
		 else if(editSnmp2C.equals("no")&& editSnmp3.equals("yes"))
		 {
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username, editSnmpv3UsernameNewValue);
			 
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass, editSnmpv3AuthpasswordNewValue);
			 
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass, editSnmpv3PrivpasswordNewValue);
		 }
			
		 
		 if(editExistingCity.equalsIgnoreCase("yes"))
		 {
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, editExistingCityValue);
		 }
		 if(editExistingSite.equalsIgnoreCase("yes"))
		 {
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, editExistingSiteValue);
		 }
		 if(editExistingPremise.equalsIgnoreCase("yes"))
		 {
			 CompareText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, editExistingPremiseValue);
		 }
		 
	}
	 /*
	 public void fetchDeviceInterface_viewdevicepage() throws InterruptedException, IOException {
			
			 scrollUp() ;
			 waitForAjax();
			 
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
			 
			 waitForAjax();
			 
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage, "Fetch Device Interfaces");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage,"Fetch Device Interfaces");
			
			 waitForAjax();
			 
				String expectedValue="Fetch interfaces started successfully. Please check the sync status of this device";
				boolean successMessage=false;
				
				successMessage=isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage);
				//successMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).isDisplayed();
				
				String actualMessage = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert);
				//String actualMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).getText();
				if(successMessage) {
					
					if(actualMessage.contains(expectedValue)) {
					
					//click on the 'click here' link
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface, "Interfaces click here Link");
					 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
					 waitForAjax();
					
					}
					else if (actualMessage.equalsIgnoreCase(expectedValue)) 
					{
						//click on the 'click here' link
						verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface, "Interfaces click here Link");
						click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
						 waitForAjax();
					}
					else 
					{
					//Action Button Click 
						verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
						 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
						 waitForAjax();
					//click on 'Manage' link
						 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage, "manage Link_view Devicepage");
						 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,"manage Link view Devicepage");
						
						 waitForAjax();
						 
					}
				}
				
		
		}
		*/
	 
	 public void fetchDeviceInterface_viewdevicepage() throws IOException, InterruptedException {

			scrollUp();
			waitForAjax();
			//Action Button Click  
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
			 
			 waitForAjax();
			 	
			//click on 'fetch device interface
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage, "Fetch Device Interfaces");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage,"Fetch Device Interfaces");
			 waitForAjax();
				
				
			//verify success Message
				String expectedValue="Fetch interfaces started successfully. Please check the sync status of this device";
				boolean successMessage=false;
				successMessage=isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage);
				//successMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).isDisplayed();
				
				//String actualMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).getText();
				String actualMessage = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert,"success Message_Alert");
				if(successMessage) {
					
					if(actualMessage.isEmpty()) {
						
						Report.LogInfo("No messages displays", "Success message is not displaying", "FAIL");
					}
					if(actualMessage.contains(expectedValue)) {
						
						Report.LogInfo("After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected", "After clicking on 'Fetch Device Interface' link, success Message is displaiyng", "PASS");
					
						Report.LogInfo( "INFO", "Success Message displays as: "+actualMessage, "PASS");
					
					//click on the 'click here' link
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
					click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
					
					waitForAjax();
					
					}
					else if (actualMessage.equalsIgnoreCase(expectedValue)) {
						
						Report.LogInfo("INFO", "After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected", "PASS");
						
						Report.LogInfo("INFO", "Success Message displays as: "+actualMessage, "PASS");
						
						//click on the 'click here' link
						verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
						click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
						waitForAjax();
					}
					else {
						
						Report.LogInfo("INFO", "After clicking on 'Fetch Device Interface' link, message displays as "+actualMessage, "PASS");
						managelink();
					//Action Button Click 
					//click on 'Manage' link
						waitForAjax();
					}
				}
				else {
					Report.LogInfo("INFO", "After clicking on 'Fetch Device Interface' link, success Message is not displaying", "PASS");
				}
		}
	 
	 public void verifyisEmpty(String xpath,String fieldname) throws InterruptedException {
	    	
	    	//String ele = getwebelement(xpath).getAttribute("value");
	    	String ele = getAttributeFrom(xpath,"value");
	    	if (ele.isEmpty()) {
	    		
	    		Report.LogInfo("INFO", "selected field is empty " + fieldname, "PASS");
	    		
	    	}else {
	    		
	    		sa.fail();
	    		Report.LogInfo("INFO", "selected field is not empty " + fieldname, "FAIL");
	    	}
	    	
	    	sa.assertAll();
	    }
	 
	 public String compareText_InViewPage_ForNonEditedFields(String application, String labelname)throws IOException, InterruptedException  {
			
			String emptyele = null;
			//WebElement element = null;
		
			try {
				waitForAjax();
				//element = getwebelement("//div[div[label[contains(text(),'"+ labelname + "')]]]/div[2]");	
				//emptyele = element.getText().toString();	
				emptyele = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.compareText_InViewPage_ForNonEditedFields1
						+ labelname +APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.compareText_InViewPage_ForNonEditedFields2,"Compare test in view page");
				
				Report.LogInfo("INFO", labelname +" field is not edited. It is displaying as "+emptyele, "PASS");
						
			}catch (Exception e) {
				e.printStackTrace();
				
				Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");
			}
			return emptyele;
		
		}
	 public void compareText_InViewPage(String labelname, String expectedVal) throws IOException {
			WebElement element = null;
			String el1= "//div[div[label[contains(text(),'";
			String el2= "')]]]/div[2]";
			//element = findWebElement("//div[div[label[contains(text(),'"+labelname+"')]]]/div[2]");
			element = webDriver.findElement(By.xpath(el1+labelname+el2));
			String actualVal = element.getText().toString();

			if (actualVal.contains(expectedVal)) {
				Report.LogInfo("CompareText", "Text match on View page", "PASS");
			} else {
				Report.LogInfo("CompareText", "Text not match on View page", "Failed");
			}
		}
	 	 
	
	 
	 public void verifyenteredValue_forDeviceCreation(String testDataFile, String sheetName, String scriptNo,
				String dataSetNo) throws InterruptedException, IOException {
		 	
		 	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Name");
			String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
			String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RouterID"); 
			String IosXr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IOSXR");
			String Full_IQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Full_IQNET");
			String ModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modular_MSP");
			String telnet = "telnet";
			String ssh = "ssh";
			String Snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp3");
			String Snmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp2C");
			String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmProNewValue");
			String Snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmprwNewValue");
			String SnmpUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3UsernameNewValue");
			String SnmpAuthPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3AuthpasswordNewValue");
			String SnmpPrivPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3PrivpasswordNewValue");
			String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
			String Management_Address= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address");
			String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
			String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
			String ExSite =	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
			String ExSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSiteValue");
			String ExPremise= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
			String ExPremiseValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremiseValue");
			String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
			String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
			//String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
			
			String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
			String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
			///String newSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
			
			String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
			String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
			//String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab, "Name Tab");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab);
			compareText("Name",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab,Name);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType, "Device Type");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType);//String deviceType =
			compareText("Device Type",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType,deviceType);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model);
			compareText("Vendor/Model",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model,VendorModel);
			
			if(Full_IQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET Checkbox");
			compareText("Full IQNET",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "true");
			
			}
			/*
			if(ModularMSP.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Modular MSP Checkbox");
				compareText("Modular MSP",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Yes");
			}
			*/
			if(IosXr.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "IOSXR Checkbox");
			compareText("IOS-XR",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "Yes");
			}
			if(telnet.equalsIgnoreCase("Yes") && ssh.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,telnet);
			}
			
			else if(telnet.equalsIgnoreCase("No") && ssh.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,ssh);
			}
			
			if(Snmp2C.equalsIgnoreCase("Yes") && Snmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw, "snmpro Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmprw",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw,Snmprw);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro, "snmprw Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmpro",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro,Snmpro);
			
			}
			else if(Snmp2C.equalsIgnoreCase("No") && Snmp3.equalsIgnoreCase("Yes"))
			{
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username, "SNMP priv UserName");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmp v3 Username",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username,SnmpUser);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass, "SNMP priv Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmp v3 Priv password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass,SnmpPrivPasswd);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass, "SNMP Auth Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass);
				compareText("Snmp v3 Auth password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass,SnmpAuthPasswd);
				
			}
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId, "Router Id");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId);
			compareText("Router Id",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId,RouterId);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Country");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET);
			//compareText("Country",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET,country);
			compareText_InViewPage1("Country", country);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd);
			//compareText("Management Address",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd,Management_Address);
			compareText_InViewPage1("Management Address", Management_Address);
			
			if(ExCity.equalsIgnoreCase("Yes") && newCity.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,ExCityValue);
			}
			else if(ExCity.equalsIgnoreCase("No") && newCity.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,newCityName);
			}

			if(ExSite.equalsIgnoreCase("Yes") && newSite.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,ExSiteValue);
			}
			else if(ExSite.equalsIgnoreCase("No") && newSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,newSiteName);
			}
			if(ExPremise.equalsIgnoreCase("Yes") && newPremise.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,ExPremiseValue);
			}
			else if(ExPremise.equalsIgnoreCase("No") && newPremise.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,newPremiseName);
			}
		
	 }
	 public void verifyDeviceCreationMessage() throws InterruptedException, IOException
	 {	Reusable.waitForAjax();
		
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg, "Device created successfully");
	
		 String deviceCreation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg, "Device creation success msg");
		 String expected_deviceUpdation="Device created successfully";
		 if(deviceCreation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
		 }
		 else{
			 Report.LogInfo("INFO","Device not updated: "+deviceCreation_Msg , "FAIL");
		 }
		 
		 Reusable.WaitforCPQloader();
		 
	 }
	 
	 public void verifyDeviceUpdationSuccessMessage() throws InterruptedException, IOException
	 {
		 Reusable.waitForAjax();
			
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation, "Device updated successfully");
		 String deviceUpdation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation,"success Message_deviceUpdation");
		 String expected_deviceUpdation="Device updated successfully";
		 if(deviceUpdation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
		 }
		 else{
			 Report.LogInfo("INFO","Device not updated: "+deviceUpdation_Msg , "FAIL");
		 }
		 
	 }
	 public void verifydeviceDelete_AccessRouter() throws IOException, InterruptedException 
	 {			
		 scrollUp() ;
		 refreshPage();
		 waitForAjax();
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
		 
		 waitForAjax();
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete, "Delete Device");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete,"Delete Device");

		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.delete_alertpopup, "Delete Alert Popup");
		
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton, "Delete Button");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton,"Delete Button");
		 
		 waitForAjax();
		 
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg, "Device created successfully");
			
		 String deviceCreation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg,"delete Device ConfMsg");
		 String expected_deviceUpdation="Device successfully deleted.";
		 if(deviceCreation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
		 }
		 else{
			 Report.LogInfo("INFO","Device not deleted: "+deviceCreation_Msg , "FAIL");
		 }
		 
	    }

	 public void verifydevicecreation_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
			
			waitForAjax();
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.WarningMesassage_site, "Warning Mesassage Site");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_city, "Warning Mesassage City");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_Country, "Warning Mesassage Country");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_name, "Warning Mesassage Name");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_deviceType, "Warning Mesassage Device Type");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_vendor, "Warning Mesassage vendor");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_routerId, "Warning Mesassage Router Id");
			
			
			String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Name");
			String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
			String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RouterID"); 
			String IosXr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IOSXR");
			String Full_IQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Full_IQNET");
			String ModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modular_MSP");
			String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telnet");
			String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SSH");
			String Snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp3");
			String Snmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp2C");
			String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmProNewValue");
			String Snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmprwNewValue");
			String SnmpUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3UsernameNewValue");
			String SnmpAuthPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3AuthpasswordNewValue");
			String SnmpPrivPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3PrivpasswordNewValue");
			String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
			String Management_Address= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address");
			String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
			String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
			String ExSite =	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
			String ExSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSiteValue");
			String ExPremise= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
			String ExPremiseValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremiseValue");
			String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
			String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
			String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
			
			String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
			String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
			String newSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
			
			String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
			String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
			String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
			
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "Name Text Field");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,Name,"Name Text Field");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, "Device Type Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput,"Device Type Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput1 +deviceType+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput2, "Device Type Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, deviceType, "Device Type Input");
			scrollUp();
			scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput);
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput1 +VendorModel+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput2, "Vendor Model Input");
			//selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, VendorModel, "Vendor Model Input");
			addDropdownValues_commonMethod("Vendor/Model", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, VendorModel);
			if(Full_IQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader, "Full IQNET Alert Header");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
			}
			//else
			//{
			//	isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, null);
			//}
			
			if(ModularMSP.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
			}
			//else
			//{
				//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, null);
			//}
			if(IosXr.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			}
			//else
			//{
				//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox,  null);
			//}
			
			if(telnet.equalsIgnoreCase("Yes") && ssh.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
			}
			
			else if(telnet.equalsIgnoreCase("No") && ssh.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			}
			
			if(Snmp2C.equalsIgnoreCase("Yes") && Snmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, "snmpro Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,Snmpro);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield)==false)
				////{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, "snmprw Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield,Snmprw);
				//}
			}
			
			else if(Snmp2C.equalsIgnoreCase("No") && Snmp3.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username,SnmpUser);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword, "snmp AuthPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,SnmpAuthPasswd);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword, "snmp PrivPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,SnmpPrivPasswd);
				//}
			}
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, "Router Id Textfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, RouterId );
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput,"Country Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput1 +country+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput2,"Country Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, country, "Country Input");
			
			//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox)==true)
			//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox, "Management Address Textbox");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,Management_Address);
			//}
			
			if(ExCity.equalsIgnoreCase("Yes")&& newCity.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,"City dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput1 +ExCityValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput2,"City dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, ExCityValue, "City dropdown Input");
			
			}
			else if(ExCity.equalsIgnoreCase("No")&& newCity.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,newCityName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,newCityCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,newSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,newSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,newPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,newPremiseCode);
				
			}
			if(ExSite.equalsIgnoreCase("Yes")&& newSite.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,"Site Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput1 +ExSiteValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput2,"Site dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, ExSiteValue, "Site dropdown Input");
			
			}
			else if(ExSite.equalsIgnoreCase("No")&&newSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,newSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,newSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,newPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,newPremiseCode);
			}
			if(ExPremise.equalsIgnoreCase("Yes")&& newPremise.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,"Premise Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput1 +ExPremiseValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput2,"Premise dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, ExPremiseValue, "Premise Dropdown Input");
			
			}
			else if(ExPremise.equalsIgnoreCase("No")&& newPremise.equalsIgnoreCase("yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,newPremiseName, "Premise Name Inputfield");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,newPremiseCode,"Premise Code Inputfield");
			}
			scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
			Reusable.waitForAjax();
	    	
	    	
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
			Reusable.waitForAjax();
			}
	 public void addDropdownValues_commonMethod(String labelname, String xpath, String expectedValueToAdd)
			 throws InterruptedException{
			 boolean availability=false;
			 List<String> ls = new ArrayList<String>();

			 try {

			 //availability=getwebelementNoWait(xml.getlocator("//locators/" + application + "/"+ xpath +"")).isDisplayed();
			 if(isVisible(xpath)) {
			 Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
			 if(expectedValueToAdd.equalsIgnoreCase("null")) {
			 Report.LogInfo("INFO", " No values selected under "+ labelname + " dropdown", "PASS");
			 }else {
			 webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//div[text()='�']")).click();
			 //Clickon(getwebelementNoWait("//div[label[text()='"+ labelname +"']]//div[text()='�']"));

			 //verify list of values inside dropdown
			 List<WebElement> listofvalues = webDriver
			 .findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

			 for (WebElement valuetypes : listofvalues) {
			 //Log.info("List of values : " + valuetypes.getText());
			 ls.add(valuetypes.getText());
			 }

			 //ExtentTestManager.getTest().log(LogStatus.PASS, "list of values inside "+labelname+" dropdown is: "+ls);
			 //System.out.println("list of values inside "+labelname+" dropdown is: "+ls);

			 webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd);
			 //SendKeys(getwebelementNoWait("//div[label[text()='"+ labelname +"']]//input"), expectedValueToAdd);

			 webDriver.findElement(By.xpath("(//div[label[text()='"+ labelname +"']]//div[contains(text(),'"+ expectedValueToAdd +"')])[1]")).click();
			 //Clickon(getwebelementNoWait("(//div[label[text()='"+ labelname +"']]//div[contains(text(),'"+ expectedValueToAdd +"')])[1]"));
			 //Thread.sleep(1000);

			 /*String actualValue=getwebelementNoWait("//label[text()='"+ labelname +"']/following-sibling::div//span").getText();
			 ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
			 System.out.println( labelname + " dropdown value selected as: "+ actualValue);
			 */
			 }
			 }else {
			 Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
			 //ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			 //System.out.println(labelname + " is not displaying");
			 }
			 }catch(NoSuchElementException e) {
				 
			 //ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			 Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
			 System.out.println(labelname + " is not displaying");
			 }catch(Exception ee) {
			 ee.printStackTrace();
			 //ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under "+ labelname + " dropdown");
			 Report.LogInfo("INFO", " NOt able to perform selection under "+ labelname + " dropdown", "FAIL");
			 System.out.println(" NO value selected under "+ labelname + " dropdown");
			 }
			 }
	 public void verifydeviceEdit_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException {

		 String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editdeviceName");
		 //String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
		 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editVendorModel");
		 String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editRouterID");
		 String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editModularMSP");
		 String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editFullIQNET");
		 String editIOSXR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editIOSXR");
		 String editTelnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editTelnet");
		 String editSSH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSSH");
		 String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp2C");
		 String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmProNewValue");
		 String editSnmprwNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmprwNewValue");
		 String editSnmpv3UsernameNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3UsernameNewValue");
		 String editSnmpv3AuthpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3AuthpasswordNewValue");
		 
		 String editSnmpv3PrivpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3PrivpasswordNewValue");
		 
		 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editManagementAddress");
		 
		 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
		 
		 String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
		 String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
		 String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
		 String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
		 
		 String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
		 String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
		 String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
		 String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
		 
		 String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
		 String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
		 String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
		 
		 String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
		 String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
		 String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
		 String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");
			scrollUp();
			waitForAjax();

			// Action Button Click
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			waitForAjax();

			// Edit Button Click
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
			//String EditButtonClick = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit);
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "Edit Device Name TextField");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,editDevicename,"Edit Device Name TextField");
			
			
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, "Device Type Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput,"Device Type Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput1 +deviceType+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput2, "Device Type Input");
			//selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput,deviceType, "Device Type Input");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput1 +editVendorModel+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput2, "Vendor Model Input");
			//selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, editVendorModel, "Vendor Model Input");
			addDropdownValues_commonMethod("Vendor/Model", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, editVendorModel);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, "Edit RouterID TextField");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield,editRouterID,"Edit RouterID TextField");
			
			if(editFullIQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			waitForAjax();
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader, "Full IQNET Alert Header");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
			}
			//else
			//{
			//	isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, null);
			//}
			
			if(editModularMSP.equalsIgnoreCase("Yes"))
			{
				waitForAjax();
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				waitForAjax();
			}
			//else
			//{
				//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, null);
			//}
			if(editIOSXR.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			}
			//else
			//{
				//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox,  null);
			//}
			
			if(editTelnet.equalsIgnoreCase("Yes") && editSSH.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
			}
			
			else if(editTelnet.equalsIgnoreCase("No") && editSSH.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			}
			
			if(editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, "snmpro Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,editSnmProNewValue);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield)==false)
				////{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, "snmprw Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield,editSnmprwNewValue);
				//}
			}
			
			else if(editSnmp2C.equalsIgnoreCase("No") && editSnmp3.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username,editSnmpv3UsernameNewValue);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword, "snmp AuthPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,editSnmpv3AuthpasswordNewValue);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword, "snmp PrivPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,editSnmpv3PrivpasswordNewValue);
				//}
			//}
			}
			
		if(!editCountry.equalsIgnoreCase("null"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
				//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput,"Country Input");
				//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput1 +editCountry+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput2,"Country Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, editCountry, "Edit Country Input");
				
			if(editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,"City dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput1 +editExistingCityValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput2,"City dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, editExistingCityValue, "City dropdown Input");
			
			}
			else if(editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,editNewCityName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,editNewCityCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
				
			}
				
			if(editExistingSite.equalsIgnoreCase("Yes")&& editNewSite.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,"Site Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput1 +editExistingSiteValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput2,"Site dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, editExistingSiteValue, "Site dropdown Input");
			
			
			}
			else if (editExistingSite.equalsIgnoreCase("No")&& editNewSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
			}
			
			if(editExistingPremise.equalsIgnoreCase("Yes")&& editNewPremise.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,"Premise Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput1 +editExistingPremiseValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput2,"Premise dropdown Input");
			
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, editExistingPremiseValue, "Premise Dropdown Input");
			}
			else if(editExistingPremise.equalsIgnoreCase("No")&& editNewPremise.equalsIgnoreCase("yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,editNewPremiseName, "Premise Name Inputfield");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,editNewPremiseCode,"Premise Code Inputfield");
			}
			
			}
		else if(editCountry.equalsIgnoreCase("Null")){
			Report.LogInfo("INFO"," No changes made for 'Country' dropdown","PASS");
		}
		//Edit City
		if(editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,"City dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput1 +editExistingCityValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput2,"City dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, editExistingCityValue, "City Dropdown Input");
		
		}
		else if(editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,editNewCityName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,editNewCityCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
			
		}
		//Edit Site	
		if(editExistingSite.equalsIgnoreCase("Yes")&& editNewSite.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,"Site Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput1 +editExistingSiteValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput2,"Site dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, editExistingSiteValue, "Site dropdown Input");
		
		}
		else if (editExistingSite.equalsIgnoreCase("No")&& editNewSite.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
		}
		//Edit Premise
		if(editExistingPremise.equalsIgnoreCase("Yes")&& editNewPremise.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,"Premise Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput1 +editExistingPremiseValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput2,"Premise dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, editExistingPremiseValue, "Premise dropdown Input");
		
		}
		else if(editExistingPremise.equalsIgnoreCase("No")&& editNewPremise.equalsIgnoreCase("yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,editNewPremiseName, "Premise Name Inputfield");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,editNewPremiseCode,"Premise Code Inputfield");
		}
		
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		Reusable.waitForAjax();
	
	
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		waitForAjax();
		Reusable.WaitforCPQloader();
		
		}
}
