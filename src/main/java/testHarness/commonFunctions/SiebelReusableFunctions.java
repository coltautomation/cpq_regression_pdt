package testHarness.commonFunctions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

import org.apache.poi.util.DocumentFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;
import com.sun.mail.util.MailSSLSocketFactory;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelHubSiteObj;
import pageObjects.siebelObjects.SiebelModeObj;


public class SiebelReusableFunctions extends SeleniumUtils
{
	Random rnd = new Random();


	public void WaitforC4Cloader() throws IOException, InterruptedException {
		 
		int i = 1;
		Thread.sleep(1000);   
		
          try {
        	  while (webDriver.findElement(By.xpath("//*[contains(@title,'Please wait')]")).isDisplayed()) {   
        		  if (i > 60) { break; }
                      Thread.sleep(1000);            
                      i = i+1;
                } 
          } catch(Exception e) {
        	  Waittilljquesryupdated();
          
          }
	}
	
	public void WaitforCPQloader() throws IOException, InterruptedException {
		int i = 1;
		Thread.sleep(1000);   
		
          try {
        	  while (webDriver.findElement(By.xpath("//*[@id='loading-mask']")).isDisplayed()) {   
        		  if (i > 60) { break; }
                      Thread.sleep(1000);
                      System.out.println("Waiting CPQ Page to load");
                      i = i+1;
                } 
          } catch(Exception e) {
        	  Waittilljquesryupdated();
          }
	}
	
	public void Waittilljquesryupdated() throws InterruptedException, SocketTimeoutException {
		
		boolean Status = false;
		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) webDriver;
		for (int i=1; i<10; i++) {
			if (js == null) {
				Thread.sleep(250);
				js = (JavascriptExecutor) webDriver;
				continue;
			} else {
				try {
					while(!(js.executeScript("return document.readyState").equals("complete")))
					{
						Thread.sleep(500);
					}
					Status = true;
					if (Status = true) { Thread.sleep(250); break; }
				} catch (Exception e) {
					continue;
				}
			}
		}	
	}
	
	public void waitForpageloadmask() throws InterruptedException
	{    
		boolean Status = false;
		Thread.sleep(1500);
		for (int j=0; j < 10; j++) {
			try {
				try {
					  for(int i=0;i<=20;i++) {
						  while(webDriver.findElement(By.xpath("//div[@id='lockCreateScreen' and not(@style='display: none;')]")).isDisplayed()) {
								Thread.sleep(1000);
								Status = true;
	//							System.out.println("Load Mask Displayed");
						  }
					  }
				} catch(Exception e) {
					Thread.sleep(1000);
					Waittilljquesryupdated();
	//				System.out.println("Load Mask Not Displayed");
	//				System.out.println(e.getMessage().toString());
				}
			} catch (SocketTimeoutException e1) {
				continue;
			}
			if (Status = true) { break; }
		} 
				
		Thread.sleep(300);
	}
	
	public String WebTableCellAction(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue) throws IOException, InterruptedException 
	{
			
		int row_number = 0; String tXpath = null, Row_Val;
		
		if (fluentWaitForElementToBeVisible(webDriver.findElement(By.xpath(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable)), 35)) {
			scrollIntoView(webDriver.findElement(By.xpath(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable)));
			String sXpath = CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable.toString();
			tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
		} else {
			
			return "False";
		}
		
//		getting the row and column number
		List<WebElement> rows = webDriver.findElement(By.xpath(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable)).findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(0).findElements(By.tagName("th"));
		int tot_row = rows.size();
		int tot_col = columns.size();
//		System.out.println("Total Column size is "+tot_col);
		int iCol, iRow, rColumn_number = 0, aColumn_number = 0;
		
		//Reading the column headers of table and set the column number with use of reference
		for(iCol = 1; iCol <= tot_col-1; iCol++){
			String Col_Val = columns.get(iCol).getAttribute("title").trim();
			if (Col_Val.equals(refColumn)){ 
				rColumn_number = iCol+1; 
//				System.out.println("ref Column number is "+rColumn_number); 
				break; 
			}
		}
		
//		Returns the function of reference column number
		if (iCol >= tot_col) {
			return "False";
		}		
		
		if (!ActionType.equals("Click")) {
			//Reading the actual column number ff table and set the column number with use of reference
			for(iCol = 1; iCol <= tot_col-1; iCol++){
				String Col_Val = columns.get(iCol).getAttribute("title").trim();
				if (Col_Val.contains(actColumn)){ 
					aColumn_number = iCol+1; 
//					System.out.println("act Column number is "+aColumn_number); 
					break; 
				}
			}
		
//			Returns the function of column names are not matched
			if (iCol >= tot_col) {
				return "False";
			}
		}
		
		//Taking the row value
		for(iRow =1; iRow < tot_row-1; iRow++){
			WebElement rowValue = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span[contains(@data-bind,'text')]"));
			Row_Val = rowValue.getAttribute("title").trim();
			if (Row_Val.equalsIgnoreCase(rowRef)){ 
				row_number = iRow; 
//				System.out.println("Row number is "+row_number);
				break;
			}
		}
		
//		Returns the function if rows names are not matched
		if (iRow >= tot_row-1) {
			return "False";
		}
		
		waitForpageloadmask();
		
		WebElement Cell; String sOut = null;
		switch (ActionType) {
			case "Edit":
				Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				scrollIntoView(Cell);
				click(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]");
				//WebElement edit_Box = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input"));
//				WebElement edit_Box = driver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				sendKeys(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input", ActionValue);
				sOut = "True";
				break;
			case "Select":
				Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
				scrollIntoView(Cell);
				Cell.click();
				WebElement dropdown = webDriver.findElement(By.xpath("//descendant::ul[@role='listbox']"));
				List<WebElement> options = dropdown.findElements(By.tagName("li"));
				for (WebElement option : options)
				{
				    if (option.getText().equals(ActionValue))
				    {
				        option.click(); // click the desired option
				        break;
				    }
				}
				sOut = "True";
				break;
			case "Store":
				Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//child::span"));
				scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");
				break;
				
			case "Click":
				Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+rColumn_number+"]"));
				scrollIntoView(Cell);
				click(tXpath+"/tbody/tr["+row_number+"]/td["+rColumn_number+"]");
				sOut = "True";
				break;
		}
		
		return sOut;
	
	}
	
	public void UploadFile(String locator, String filepath) throws InterruptedException, IOException 
	{
	
		webDriver.findElement(By.xpath(locator)).sendKeys(filepath);
		
	}
	
	public void waitForSiebelLoader() throws InterruptedException, IOException
	{
		int i = 1;
		Thread.sleep(1000);
		WebElement el = webDriver.findElement(By.xpath("//html"));
		try {
		     while (el.getAttribute("class").contains("siebui-busy")){
		    	 if (i > 300) { break; }
                 Thread.sleep(2000);            
                 i = i+1;
		        }
		} catch(Exception e) {
	    	  Waittilljquesryupdated();
		      
	      }
		
	}
	
	/////////////////MD/////////////////////////
	
	public String CurrentDate()
	{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return (formatter.format(date));

	}
	
	
	public String FutureDate(int value)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Calendar c = Calendar.getInstance();
	    c.setTime(new Date());
	    c.add(Calendar.DATE, value); 
	    return (formatter.format(c.getTime()));
	}
	
	
	public void uploadafile(String locator, String FileName) throws InterruptedException, IOException 
	{
		String str = System.getProperty("user.dir") + "\\src\\test\\resources\\" + FileName;
		
		webDriver.findElement(By.xpath(locator)).sendKeys(str);
	
		
		if (locator.startsWith("id"))
		{

			String[] finalval = locator.split("=");
			WebElement el = webDriver.findElement(By.id(finalval[1]));
			el.sendKeys(str);
		} else if (locator.startsWith("name"))
		{

			String[] finalval = locator.split("=");
			WebElement el = webDriver.findElement(By.name(finalval[1]));
			el.sendKeys(str);

		} else if (locator.startsWith("//") || locator.startsWith("(//"))
		{

			// String[] finalval=locator.split("=");
			WebElement el = webDriver.findElement(By.xpath(locator));
			el.sendKeys(str);

		} else
		{
			System.out.println("Loactor Stretegy is not defined");
		}

	}
	
	public void savePage() throws InterruptedException, IOException
	{
		if (isElementPresent(By.xpath("(//a[contains(text(),'Click here to')])[1]"))){
			click(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickheretoSaveAccess,"Click on Save link");	
			waitForSiebelLoader();
		}else{
		Actions keyAction = new Actions(webDriver);
		keyAction.keyDown(Keys.CONTROL).sendKeys("s").keyUp(Keys.CONTROL).perform();
		}
		waitForSiebelSpinnerToDisappear();
		waitForSiebelLoader();
		
	}
	
	/*public void SendkeaboardKeys(String el, Keys k) throws InterruptedException
	{
		// Thread.sleep(3000);
		el.sendKeys(k);
		// Thread.sleep(3000);
	}*/
	
	public void SendkeaboardKeys(String locator, Keys k) throws InterruptedException
	{
		WebElement el= findWebElement(locator);
		el.sendKeys(k);		
	}
		
	public void alertPopUp() throws DocumentFormatException, InterruptedException, IOException
	{
		
			if (isVisible((SiebelAddProdcutObj.EnterInstallationChargeInFooter.AlertAccept))) {
				verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.AlertAccept,"AlertAccept");
				click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.AlertAccept,"AlertAccept");
				waitForSiebelLoader();
			}
			if (isVisible((SiebelAddProdcutObj.EnterInstallationChargeInFooter.AlertAccept1))) {
				verifyExists(SiebelAddProdcutObj.EnterInstallationChargeInFooter.AlertAccept1,"AlertAccept");
				click(SiebelAddProdcutObj.EnterInstallationChargeInFooter.AlertAccept1,"AlertAccept");
				waitForSiebelLoader();
			}

		
	}
	
	public void ClickContinue() throws DocumentFormatException, InterruptedException, IOException {
		if (isVisible(SiebelAddProdcutObj.TechnicalValidation.BlankPriceContinue)) {
			verifyExists(SiebelAddProdcutObj.TechnicalValidation.BlankPriceContinue,"BlankPriceContinue");
			click(SiebelAddProdcutObj.TechnicalValidation.BlankPriceContinue,"BlankPriceContinue");
		}
		waitForSiebelLoader();
	}
	
	public void ClickHereSave() throws InterruptedException, DocumentFormatException {
		try {		
			
			verifyExists(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickheretoSaveAccess,"Click here to Save Access");
			click(SiebelAddProdcutObj.CircuitReferenceGeneration.ClickheretoSaveAccess,"Click on SaveAccess");
						
			waitForSiebelLoader();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	public void Save() throws InterruptedException, DocumentFormatException {
		try
		{
			verifyExists(SiebelAddProdcutObj.Save.Save,"Save button");
			waitForSiebelLoader();
				
		if(isVisible(SiebelAddProdcutObj.Save.Save))
		{
		
		click(SiebelAddProdcutObj.Save.Save,"Save button");
		waitForSiebelLoader();
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	///////////////////////KK

	//***************************** KK *********************
	public void AEndDropdownSelection(String DropdownName, String DropdownValue) throws InterruptedException,IOException
	{

	String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value", DropdownName);
	waitForElementToAppear(eleLoct, 10);
	click(eleLoct);
	if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
	verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
	click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
	waitForSiebelLoader();
	}

	eleLoct= SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value", DropdownValue);
	waitForSiebelLoader();
	waitForElementToAppear(eleLoct, 10);
	click(eleLoct);
	if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
	verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
	click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
	waitForSiebelLoader();
	}
	
	waitForSiebelSpinnerToDisappear();
	waitForSiebelLoader();
	}

	public void AEndInputEnter(String TextBoxName, String InputText) throws InterruptedException, IOException
	{
		waitToPageLoad();
		waitForSiebelLoader();
	//	verifyExists(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput,"SiteA Input");
		String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", TextBoxName);
		click(eleLoct);
		sendKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", TextBoxName),InputText);
		SendkeaboardKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", TextBoxName),Keys.ENTER);	
	
	}
	
	public void BEndDropdownSelection(String DropdownName, String DropdownValue) throws InterruptedException, IOException
	{
	
		String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value", DropdownName);
		waitForElementToAppear(eleLoct, 10);
		click(eleLoct);
		if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
		verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
		click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
		waitForSiebelLoader();
		}

		eleLoct= SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value", DropdownValue);
		waitForSiebelLoader();
		waitForElementToAppear(eleLoct, 10);
		click(eleLoct);
		if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
		verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
		click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
		waitForSiebelLoader();
		}
		waitForSiebelSpinnerToDisappear();
		waitForSiebelLoader();
	}

	public void BEndInputEnter(String TextBoxName, String InputText) throws InterruptedException, IOException
	{
		waitToPageLoad();
		waitForSiebelLoader();
	//	verifyExists(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput,"SiteB Input");
		String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", TextBoxName);
		click(eleLoct);
		sendKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", TextBoxName),InputText);
		SendkeaboardKeys(SiebelAddProdcutObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", TextBoxName),Keys.ENTER);	
	
	}
	//////////////// k 10
	
	public void ClearSendKeys(String locator, String value) throws InterruptedException, IOException
	{
		WebElement el= findWebElement(locator);
		el.clear();
		el.sendKeys(value);
	
	}
	
	
	public void closePopUp() throws Exception 
	{
		verifyExists(SiebelModeObj.R5DataCoverage.PopClose,"Popup Close");
		click(SiebelModeObj.R5DataCoverage.PopClose,"Popup Close");
				
		waitForSiebelSpinnerToDisappear();
		waitForSiebelLoader();
		
	}

	public void Select1(String locator, String value) throws IOException, InterruptedException
	{
	WebElement el= findWebElement(locator);
	Select s1 = new Select(el);
	s1.selectByVisibleText(value);
	}
	
	public void Pagerefresh() throws InterruptedException
	{
		webDriver.navigate().refresh();

	}
	
	public void SelectTodaysDate(String locator, String ObjectName ) throws IOException, InterruptedException
	{
	//	verifyExists(locator,ObjectName);
		click(locator,ObjectName);
		waitForElementToAppear(SiebelModeObj.EnterDateInFooter.CalendarNowBtn, 30);
		click(SiebelModeObj.EnterDateInFooter.CalendarNowBtn,"Now button");
		waitForSiebelLoader();
		verifyExists(SiebelModeObj.EnterDateInFooter.TodayDate,"Todays Date");
		click(SiebelModeObj.EnterDateInFooter.TodayDate,"Todays Date");
		waitForSiebelLoader();
	}
	
	public void waitForSiebelSpinnerToDisappear() throws InterruptedException, IOException
	{
		waitForElementToDisappear(SiebelModeObj.VoiceFeatureTab.Spinner,60);		
	}
	
	public void waitforAttributeloader() throws InterruptedException
	{
		Thread.sleep(2000);

		while (isDisplayed(
				"//i[contains(@class,'colt-status-spinner') and contains(@class,'colt-attr-status-icon') and contains(@class,'fa-spinner')]"))

		{
			Thread.sleep(1000);
		}
	}
	public boolean isDisplayed(String locator)
	{
		try
		{
			webDriver.findElement(By.xpath(locator));
			//Log.info("Element Found: True");
			Report.LogInfo("isDisplay","Element Found: True", "INFO");	
			return webDriver.findElement(By.xpath(locator)).isDisplayed();
		} catch (NoSuchElementException e)
		{
		//	Log.info("Element Found: False");
			Report.LogInfo("isDisplay","Element Found: False", "INFO");	
			return false;
		}
	}
	
	public void SelectDropDownValue(String DropdownName, String DropdownValue) throws InterruptedException, IOException
	{
	
		String eleLoct=SiebelAddProdcutObj.ModTechModCommWaveAndLine.DropdownClick.replace("Value", DropdownName);
		waitForElementToAppear(eleLoct, 10);
		click(eleLoct);
		waitForSiebelSpinnerToDisappear();
		eleLoct= SiebelAddProdcutObj.ModTechModCommWaveAndLine.DropDownValue.replace("Value", DropdownValue);
		waitForElementToAppear(eleLoct, 10);
		click(eleLoct);
		waitForSiebelSpinnerToDisappear();
	}

}


	

