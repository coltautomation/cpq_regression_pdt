package pageObjects.aptObjects;

public class Lanlink_DirectFiberObj {

	public static class DirectFiber {

		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";
		
		public static final String listofA_endTechnologies = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String listofinterfacespeed = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String listofServicesubtypes = "//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofB_endTechnologies = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofnotificationmanagement = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String listofdeliverychannel = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String managementConnection = "@xpath=//div[label[contains(text(),'Management Connection')]]//input";
		public static final String listofmanagementOrder = "@xpath=//div[@class='sc-ifAKCX oLlzc']";
		public static final String fieldValidation2 = "')]";
		public static final String fieldValidation1 = "@xpath=//div[contains(text(),'";
		public static final String ENNI_checkbox = "@xpath=//div[label[text()='ENNI']]//input";
		public static final String viewPage1 = "@xpath=//div[div[label[contains(text(),'";
		public static final String viewPage2 = "')]]]/div[2]";
		public static final String testlist1 = "@xpath=(//tbody/tr[";
		public static final String testlist2 = "]/td)[1]";
		public static final String testlist3 = "]/td)[2]/div";
		public static final String interfacedetails1 = "@xpath=//div[div[text()='";
		public static final String interfacedetails2 = "']]/div";

		public static final String deviceview1 = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'";
		public static final String deviceview2 = "')]]]//span[text()='View']";

		public static final String obutton_spanTag = "@xpath=//span[contains(text(),'OK')]";

		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";

		public static final String managecoltnetworklink = "@xpath=(//div[@class='ant-menu-submenu-title'])[4]";

		public static final String createOrder_NextButton = "@xpath=//span[text()='Next']";

		public static final String createOrderService_warningMessage = "@xpath=(//span[text()='Order/Contract Number(Parent SID) is required'])[1]";

		public static final String serviceType_warningMessage = "@xpath=//span[text()='Service type is required']";

		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";

		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";

		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";

		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";

		public static final String selectorderswitch = "@xpath=(//div[@class='react-switch-bg'])[2]";

		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";

		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";

		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";

		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";

		public static final String newordertextfield = "@xpath=//input[@id='orderName']";

		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";

		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";

		// public static final String !-- create order/service page --="@xpath=

		public static final String createOrderORService = "@xpath=//div[text()='Create Order / Service']";

		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";

		public static final String choosecustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";

		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";

		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";

		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";

		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";

		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";

		public static final String country = "@xpath=//div[div[label[text()='Country']]]/div[2]";

		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";

		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";

		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";

		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";

		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";

		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";

		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";

		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";

		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";

		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String nametextfield = "@xpath=//input[@id='name']";

		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";

		// public static final String
		// country="@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String ocntextfield = "@xpath=//input[@id='ocn']";

		public static final String referencetextfield = "@xpath=//input[@id='reference']";

		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";

		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";

		public static final String emailtextfield = "@xpath=//input[@id='email']";

		public static final String phonetextfield = "@xpath=//input[@id='phone']";

		public static final String faxtextfield = "@xpath=//input[@id='fax']";

		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";

		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";

		// public static final String
		// okbutton="@xpath=//span[contains(text(),'OK')]";

		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String clearbutton = "@xpath=//span[text()='Reset']";

		public static final String resetButton = "@xpath=//span[text()='Reset']";

		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";

		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// public static final String !-- Verify warning messages --="@xpath=

		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";

		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";

		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";

		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";

		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";

		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";

		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";

		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";

		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";

		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";

		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";

		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String orderpanelheader = "@xpath=(//div[contains(text(),'Order')])[1]";

		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";

		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";

		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";

		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";

		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";

		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";

		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";

		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String successmsg = "@xpath=//div[@role='alert']//span";

		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";

		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";

		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";

		public static final String EmailColumn = "@xpath=//div[@col-id='email']";

		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";

		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";

		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";

		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		// public static final String !-- New User creation in view service page
		// --="@xpath=

		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";

		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";

		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";

		// public static final String !-- Warning messages --="@xpath=

		public static final String warningmsg_username = "@xpath=//label[text()='User Name']/parent::div//div";

		public static final String warningmsg_firstname = "@xpath=//label[text()='First Name']/parent::div//div";

		public static final String warningmsg_surname = "@xpath=//label[text()='Surname']/parent::div//div";

		public static final String warningmsg_postaladdress = "@xpath=//label[text()='Postal Address']/parent::div/following-sibling::div";

		public static final String warningmsg_useremail = "@xpath=//label[text()='Email']/parent::div//div";

		public static final String warningmsg_userphone = "@xpath=//label[text()='Phone']/parent::div//div";

		public static final String warningmsg_userpassword = "@xpath=//label[text()='Password']/parent::div//div";

		public static final String UserName = "@xpath=(//div[@class='position-relative form-group'])[1]//input[@id='userName']";

		public static final String FirstName = "@xpath=(//div[@class='position-relative form-group'])[2]//input[@id='firstName']";

		public static final String SurName = "@xpath=(//div[@class='position-relative form-group'])[3]//input[@id='surname']";

		public static final String PostalAddress = "@xpath=(//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";

		// public static final String
		// Email="@xpath=(//div[@class='position-relative
		// form-group'])[5]//input[@id='email']";

		public static final String Phone = "@xpath=(//div[@class='position-relative form-group'])[6]//input[@id='phone']";

		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";

		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";

		public static final String OkButton = "@xpath=//button[@type='submit']//span[text()='Ok']";

		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";

		// public static final String
		// delete_alertpopup="@xpath=//div[@class='modal-content']";

		// public static final String deletebutton="@xpath=//button[text()='
		// Delete']";

		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String userspanel_header = "@xpath=//div[text()='Users']";

		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";

		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";

		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";

		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";

		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";

		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";

		public static final String OK_button = "@xpath=//button[@type='submit']//span[text()='OK']";

		public static final String userdelete = "@xpath=//button[text()='Delete']";

		public static final String IPGuardianAccountGroup = "@xpath=//input[@id='ipGuardianAccountGrp']";

		public static final String IPGuardianAccountGroup_Text = "@xpath=//label[contains(text(),'IPGuardian Account Group')]";

		public static final String IPGuardianAccountGroup_viewpage = "@xpath=//div[div[label//b[contains(text(),'IPGuardian Account Group')]]]/div[2]";

		public static final String ColtOnlineUser = "@xpath=//input[@id='onlineUser']";

		public static final String ColtOnlineUser_Text = "@xpath=//label[contains(text(),'Colt Online User')]";

		public static final String coltonlineuser_viewpage = "@xpath=//div[div[label//b[contains(text(),'Colt Online User')]]]/div[2]";

		public static final String Password_Text = "@xpath=//label[contains(text(),'Password')]";

		public static final String Password_Textfield = "@xpath=//input[@id='password']";

		public static final String GeneratePasswordLink = "@xpath=//span[text()='Generate Password']";

		public static final String GeneratePasswordLink_Text = "@xpath=//span[@class='badge-success badge badge-secondary']";

		public static final String roleDropdown_available = "@xpath=//select[@id='availableRoles']//option";

		public static final String roleDropdown_addButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='>>']";

		public static final String roleDropdown_removeButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='value']";

		public static final String roleDropdown_selectedValues = "@xpath=//select[@id='selectedRoles']//option";

		public static final String hideRouterToolIPv4_Huawei_available = "@xpath=//select[@id='availableIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4__Huawei_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='>>']";

		public static final String hideRouterToolIPv4_Huawei_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='value']";

		public static final String hideRouterToolIpv4_Huawei_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4_Cisco_Available = "@xpath=//select[@id='availableIPV4CommandCisco']//option";

		public static final String hideRouterToolIPv4_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String hideRouterToolIPv4_Cisco_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='value']";

		public static final String hideRouterToolIpv4_Cisco_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandCisco']//option";

		public static final String HideService_Available = "@xpath=//select[@id='availableHideService']//option";

		public static final String HideService_addButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='>>']";

		public static final String HideService_removeButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='value']";

		public static final String HideServicesDropdown_selectedValues = "@xpath=//select[@id='selectedHideService']//option";

		public static final String HideSiteOrder_Available = "@xpath=//select[@id='availableHideSiteOrder']//option";

		public static final String hideSiteOrder_addbutton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='>>']";

		public static final String hideSiteOrder_removeButton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='value']";

		public static final String HideSiteOrderDropdown_selectedValues = "@xpath=//select[@id='selectedHideSiteOrder']//option";

		public static final String HideRouterToolIPv6_Cisco_Available = "@xpath=//select[@id='availableIPV6Command']//option";

		public static final String hideRouterToolIPv6_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String viewUser_HiddenRouterToolIPv4Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]]//following-sibling::div";

		public static final String viewUser_HiddenRouterToolCommandIPv4Huawei = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div";

		public static final String viewUser_HiddenRouterToolCommandIPv6Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]]//following-sibling::div";

		public static final String selectValueUnderUserPanel_ViewSericePage = "@xpath=//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String changeorder_cancelbutton = "@xpath=//button[span[text()='Cancel']]";

		public static final String changeOrder_orderDropdown = "@xpath=//div[label[text()='Order/Contract Number (Parent SID)']]//input";

		public static final String fetchorderNumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//following-sibling::div";

		public static final String checkExistingUserUnderUserPanel = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String userPanelFilterButton = "@xpath=(//span[@class='ag-icon ag-icon-menu'])[2]";

		public static final String userNamefilterTextField = "@xpath=//input[@id='filterText']";

		public static final String selectuncheckedCheckbox_UserPanel = "@xpath=(//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked'])[1]";

		// public static final String
		// obutton_spanTag="@xpath=//span[contains(text(),'OK')]";

		// public static final String
		// ManageCustomerServiceLink="@xpath=//span/b[contains(text(),"MANAGE
		// CUSTOMER'S SERVICE")]";

		// public static final String
		// nextbutton="@xpath=//span[contains(text(),'Next')]";

		// public static final String
		// order_contractnumberErrmsg="@xpath=(//span[text()='Order/Contract
		// Number(Parent SID) is required'])[2]";

		/// public static final String
		/// servicetypeerrmsg="@xpath=//span[contains(text(),'Service type is
		/// required')]";

		// public static final String
		// interfaceSpeedErrmsg="@xpath=//span[contains(text(),'Interface Speed
		// is required')]";

		// public static final String
		// servicesubtypeErrMsg="@xpath=//span[contains(text(),'Sub service type
		// is required')]";

		// public static final String
		// CreateOrderServiceLink="@xpath=//li[text()='Create Order/Service']";

		public static final String chooseCustomer_build3 = "@xpath=//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30']";

		// public static final String
		// entercustomernamefield="@xpath=//input[@id='customerName']";

		// public static final String
		// chooseCustomerdropdown="@xpath=//div[label[text()='Choose a
		// customer']]//input";

		public static final String ordererrormsg = "@xpath=(//span[text()='Order/Contract number is required'])[1]";

		public static final String Servicetypeerrmsg = "@xpath=//span[contains(text(),'Service type is required')]";

		public static final String CreateOrderService_Text = "@xpath=//li[text()='Create Order Service']";

		public static final String ChooseCustomer_Select = "@xpath=(//input[@placeholder='Select...'])[2]";

		// public static final String
		// Next_Button="@xpath=//button[contains(@class,'pull-right')]//span[contains(text(),'Next')]";

		// public static final String
		// OrderContractNumber_Select="@xpath=(//input[@placeholder='Select...'])[2]";

		public static final String ordercontractNumberdropdown = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']]//input)[1]";

		public static final String ordercontractNumberdropdown_secondTime = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']]//input)[1]";

		// public static final String
		// OrderContractNumber_Select_build3="@xpath=(//div[@class='react-dropdown-select-content
		// react-dropdown-select-type-single css-jznujr-ContentComponent
		// e1gn6jc30'])[1]";

		// public static final String
		// ServiceType_Select1_build3="@xpath=(//div[@class='react-dropdown-select-content
		// react-dropdown-select-type-single css-jznujr-ContentComponent
		// e1gn6jc30'])[2]";

		public static final String servicetypedropdowntoclick = "@xpath=//div[label[text()='Service Type']]//input";

		public static final String orderdropdown = "@xpath=(//div[label[text()='Order/Contract Number(Parent SID)']])[1]//input";

		public static final String createButton = "@xpath=//button[@class='btn btnSquar btnWitdh undefined btn-secondary ']";

		public static final String CreateOrderSuccessMessage = "@xpath=//span[contains(text(),'Order created successfully')]";


		public static final String intefacespeederrormsg = "@xpath=//span[contains(text(),'Interface Speed is required')]";

		public static final String servicesubtypeerrmsg = "@xpath=//span[contains(text(),'Sub service type is required')]";

		public static final String ServiceType_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'][1])[2]";

		public static final String listOfservicetypes = "@xpath=//div/span[@role='option']";

		// public static final String LANLINK="@xpath=

		public static final String ClassNameForDropdowns = "@xpath=//div[@class='sc-ifAKCX oLlzc']";

		public static final String selectValueUnderTechnologyDropdown = "@xpath=//div[text()='value']";

		// public static final String
		// obutton_spanTag="@xpath=//span[contains(text(),'OK')]";
		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String OrderContractNumber_Select_build3 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";

		public static final String ServiceType_Select1_build3 = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[2]";

		public static final String OrderContractNumbertoggleButton = "@xpath=(//div[div[label[text()='Select Order']]]//div[@class='react-switch-handle'])[1]";

		public static final String OrderName = "@xpath=//input[@id='orderName']";

		public static final String ServiceSubtype = "@xpath=//div[label[contains(text(),'Service Subtype')]]//input";

		public static final String InterfaceSpeed = "@xpath=//div[label[contains(text(),'Interface Speed')]]//input";

		public static final String Interfacespeedropdownvalue = "@xpath=//div[@role='list']//span[@role='option']";

		public static final String AvailableCircuits = "@xpath=//div[label[contains(text(),'Available Circuits')]]//input";

		public static final String Next = "@xpath=(//span[contains(text(),'Next')])[1]";

		public static final String A_Endtechnology = "@xpath=//div[label[contains(text(),'A-End Technology')]]//input";

		public static final String A_EndTechnology_xbuton = "@xpath=//div[label[text()='A-End Technology']]//div[text()='�']";

		public static final String B_Endtechnology = "@xpath=//div[label[contains(text(),'B-End Technology')]]//input";

		public static final String B_EndTechcnology_xbutton = "@xpath=//div[label[text()='B-End Technology']]//div[text()='�']";

		public static final String Autocreatebutton = "@xpath=//span[contains(text(),'AutoCreate Service')]";

		public static final String interfacespeedvaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[3]";

		public static final String interfacespeedlabel = "@xpath=//label[contains(text(),'Interface Speed')]";

		public static final String servicesubtypevaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[2]";

		public static final String servicetypevaluepopulated = "@xpath=(//div[@class='customLabelValue form-label'])[1]";

		public static final String modularmspcheckbox = "@xpath=//div[@class='form-group']//div[@class='form-group']//input[@id='modularMSP']";

		public static final String AutocreateServicecheckbox = "@xpath=//input[@id='autoCreateService']";

		public static final String notificationmanagement = "@xpath=//div[label[contains(text(),'Notification Management Team')]]//input";

		public static final String configurtoinptionpanel_webelementToScroll = "@xpath=//div[contains(text(),'Configuration Options')]";

		public static final String ServiceIdentification = "@xpath=//input[@id='serviceIdentification']";

		public static final String ServiceType = "@xpath=//div[contains(text(),'LANLink')]";

		public static final String EndpointCPE = "@xpath=//div[label[contains(text(),'Single Endpoint CPE')]]//input";

		public static final String Email = "@xpath=//div[label[contains(text(),'Email')]]//input";

		public static final String PhoneContact = "@xpath=//input[@id='phoneContact']";

		public static final String Remark = "@xpath=//textarea[@name='remark']";

		public static final String Performancereporting = "@xpath=//input[@id='proactiveMonitoring']";

		public static final String proActivenotification_TransmissionLink = "@xpath=//input[@id='proactiveNotification']";

		public static final String NotificationManagementTeam_TransmissionLink = "@xpath=//select[@id='notificationManagementTeam']";

		public static final String Next_Button = "@xpath=//button[contains(@class,'pull-right')]//span[contains(text(),'Next')]";

		public static final String performancemonitoring = "@xpath=//div[label[contains(text(),'Performance Monitoring')]]//input";

		public static final String performanceReportingcheckbox = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";

		public static final String perCoSperformncereport = "@xpath=//div[label[contains(text(),'Per CoS Performance Reporting')]]//input";

		public static final String ActelisBasedcheckbox = "@xpath=//input[@id='actelisBased']";

		public static final String standardCIRtextfield = "@xpath=//input[@id='standardCir']";

		public static final String standardEIRtextfield = "@xpath=//input[@id='standardEir']";

		public static final String premiumCIRtextfield = "@xpath=//input[@id='premiumCir']";

		public static final String premiumEIRtextfield = "@xpath=//input[@id='premiumEir']";

		public static final String proactiveMonitoring = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//input";

		public static final String DeliveryChannel = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//input";

		public static final String managementOrder = "@xpath=//div[label[contains(text(),'Management Order')]]//input";

		public static final String VPNtopology = "@xpath=//div[label[contains(text(),'VPN Topology')]]//input";

		public static final String VPNtopology_xbutton = "@xpath=//div[label[text()='VPN Topology']]//div[text()='�']";

		public static final String IntermediateTechnology = "@xpath=//div[label[contains(text(),'Intermediate Technologies')]]//input";

		public static final String circuitReference = "@xpath=//div[label[contains(text(),'Circuit Reference')]]//input";

		public static final String Aggregate_10Gig_1Gig_Traffic = "@xpath=//div[label[contains(text(),'Aggregate 10Gig-1Gig Traffic')]]//input";

		public static final String DeliverychannelForselectTag = "@xpath=//select[@id='deliveryChannel']";

		public static final String VpnTopologyForSelctTag = "@xpath=//select[@id='vpnTechnology']";

		public static final String ENNIcheckbox = "@xpath=//input[@id='enni']";

		public static final String xButton = "@xpath=//span[contains(text(),'�')]";

		public static final String deliverychannel_withclasskey = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//input";

		public static final String cicuitreferenceLabel_serviceCreation = "@xpath=//label[contains(text(),'Circuit Reference')]";

		public static final String circuitTyeplabel_servicecreation = "@xpath=//label[contains(text(),'Circuit Type')]";

		public static final String intermediatetechLabelname = "@xpath=//label[contains(text(),'Intermediate Technology')]";

		public static final String vpntopoloty_xbutton = "@xpath=(//div[@class='react-dropdown-select-clear css-w49n55-ClearComponent e11qlq5e0'])[3]";

		public static final String notigymanagement_xbutton = "@xpath=//div[label[contains(text(),'Notification Management Team')]]//div[text()='�']";

		public static final String deliverychannel_xbutton = "@xpath=//div[label[contains(text(),'Delivery Channel')]]//div[text()='�']";

		public static final String managementOrder_xButton = "@xpath=//div[label[contains(text(),'Management Order')]]//div[text()='�']";

		// public static final String
		// entercustomernamefield="@xpath=//input[@id='customerName']";

		public static final String managementConnectForselectTag = "@xpath=//select[@id='managementConnection']";

		public static final String listofcircuittypes = "@xpath=//div[@class='div-border div-margin container']//div[@class='row'][4]//input";

		public static final String cancelButton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String Editinterface_popupTitlename = "@xpath=//div[@class='modal-title h4']";

		public static final String EditInterfacepopup_xbutton = "@xpath=//span[text()='�']";

		public static final String AlertForServiceCreationSuccessMessage = "@xpath=//div[@role='alert']/span";

		public static final String serivceAlert = "@xpath=//div[@role='alert']";

		public static final String Actiondropdown_siteorder = "@xpath=//div[div[text()='Site Orders']]//button[text()='Action']";

		public static final String ActiondropdownforshowInterfaceunderEquipment = "@xpath=(//button[@id='dropdown-basic-button'])[2]";

		public static final String Viewcreatedservice_serviceIdentification = "@xpath=//div[div[label[contains(text(),'Service Identification')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_Servicetype = "@xpath=//div[div[label[contains(text(),'Service Type')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_servicesubtype = "@xpath=//div[div[label[contains(text(),'Service Subtype')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_interfacespeed = "@xpath=//div[div[label[contains(text(),'Interface Speed')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_SinplepointCPE = "@xpath=//div[div[label[contains(text(),'Single Endpoint CPE')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_email = "@xpath=//div[div[label[contains(text(),'Email')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_phone = "@xpath=//div[div[label[contains(text(),'Phone Contact')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_msp = "@xpath=//div[div[label[contains(text(),'Modular MSP')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_remark = "@xpath=//div[div[label[contains(text(),'Remark')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_performReport = "@xpath=//div[div[label[contains(text(),'Performance Reporting')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_proactivemonitor = "@xpath=//div[div[label[contains(text(),'Proactive Monitoring')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_deliverychanel = "@xpath=//div[div[label[contains(text(),'Delivery Channel')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_vpnTopology = "@xpath=//div[div[label[contains(text(),'VPN Topology')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_circuittype = "@xpath=//div[div[label[contains(text(),'Circuit Type')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_circuitreference = "@xpath=//div[div[label[contains(text(),'Circuit Reference')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_notificationManagement = "@xpath=//div[div[label[contains(text(),'Notification Management Team')]]]//div[@class='customLabelValue form-label']";

		public static final String Viewcreatedservice_managementorder = "@xpath=//div[div[label[contains(text(),'Management Order')]]]//div[@class='customLabelValue form-label']";

		public static final String viewcreatedservice_ManagementConnection = "@xpath=//div[div[label[contains(text(),'Management Connection')]]]//div[@class='customLabelValue form-label']";

		public static final String viewcreatedservice_ENNI = "@xpath=//div[div[label[contains(text(),'ENNI')]]]//div[@class='customLabelValue form-label']";

		// public static final String !-- Add site order Error warning messages
		// --="@xpath=

		public static final String Addsiteorder_countryerrmsg = "@xpath=//div[text()='Device Country']";

		public static final String Addsiteorder_devciexngCityErrmsg = "@xpath=//div[text()='Device Xng City']";

		public static final String Addsiteorder_xngcitynameerrmsg = "@xpath=//div[text()='Device Xng City Name']";

		public static final String Addsiteorder_xngcityCodeerrmsg = "@xpath=//div[text()='Device Xng City Code']";

		public static final String Addsiteorder_csrnameErrmsg = "@xpath=//div[text()='Physical Site: CSR Name']";

		public static final String Addsitieorder_technologyErrmsg = "@xpath=//div[text()='Technology']";

		public static final String Addsiteorder_physicalsiteErrmsg = "@xpath=//div[text()='Physical Site']";

		public static final String Addsiteorder_sitOrderNumberErrmsg = "@xpath=//div[text()='Site Order Number(Siebel Service ID)']";

		public static final String Addsiteorder_circuitReferenceErrmsg = "@xpath=//div[text()='Circuit Reference']";

		public static final String Addsiteorder_IVReferenceErrmsg = "@xpath=//div[text()='IV Reference']";

		// public static final String !-- Add Site Order fields --="@xpath=

		public static final String Addsiteorderlink = "@xpath=//a[contains(text(),'Add Site Order')]";

		public static final String EditsideOrderlink = "@xpath=//a[contains(text(),'Edit')]";

		public static final String deletesiteorderlink = "@xpath=//a[contains(text(),'Delete')]";

		public static final String viewsiteorderlink = "@xpath=//a[@class='dropdown-item'][contains(text(),'View')]";

		public static final String Addsiteorder_Country = "@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String Addsiteorder_City = "@xpath=//div[label[contains(text(),'Device Xng City')]]//input";

		public static final String Addsiteorder_Citytogglebutton = "@xpath=(//div[@class='react-switch']/div)[2]";

		public static final String Addsiteorder_disabledCitytogglebutton = "@xpath=(//div[@class='react-switch']/div)[3]";

		public static final String Addsiteorder_Sitetogglebutton = "@xpath=(//div[@class='react-switch']/div)[6]";

		public static final String AddsiteOrdr_disabledSitetogglebutton = "@xpath=(//div[div[label[text()='Select Site']]]//div[@class='react-switch-handle'])[1]";

		public static final String Addsiteorder_sitedropdown = "@xpath=//div[label[contains(text(),'Physical Site')]]//input";

		public static final String Addsiteorder_CSRname = "@xpath=//input[@id='csrName']";

		public static final String Addsiteorder_performancereporting = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";

		public static final String Addsiteorder_performancereporting_xbutton = "@xpath=//div[label[text()='Performance Reporting']]//div[text()='�']";

		public static final String Addsiteorder_performancereportingdefaultvalue = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//span[text()='Follow Service']";

		public static final String Addsiteorder_proactivemonitoring = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//input";

		public static final String Addsitorder_proactivemonitoring_xbutton = "@xpath=//div[label[text()='Proactive Monitoring']]//div[text()='�']";

		public static final String Addsiteorder_proactivemonitoringdefaultvalue = "@xpath=//div[label[contains(text(),'Proactive Monitoring')]]//span[text()='Follow Service']";

		public static final String Addsiteorder_smartmonitoring = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//input";

		public static final String Addsiteorder_smartmonitoring_xbutton = "@xpath=//div[label[text()='Smarts Monitoring']]//div[text()='�']";

		public static final String Addsiteorder_smartmonitoringdefaultvalue = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//span[text()='Follow Service']";

		public static final String Addsiteorder_Technology = "@xpath=//div[label[contains(text(),'Technology')]]//input";

		public static final String AddSiteOrder_sitePreferenceType_Dropodwn = "@xpath=//label[text()='Site Preference Type']/parent::div//input";

		public static final String Addsiteorder_sitealias = "@xpath=//div[label[text()='Site Alias']]//input";

		public static final String Addsiteorder_Vlanid = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String Addsiteorder_DCAenabledsitecheckbox = "@xpath=//input[@id='dcaEnabledSite']";

		public static final String Addsiteorder_next = "@xpath=//span[contains(text(),'Next')]";

		public static final String Addsiteorder_cancel = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String Addsiteorder_cloudserviceProvider = "@xpath=//div[label[contains(text(),'Cloud Service Provider')]]//input";

		public static final String Addsiteorder_remark = "@xpath=//div[label[text()='Remark']]//textarea";

		public static final String Addsiteorder_selectcitytoggle = "@xpath=(//div[div[label[text()='Select City']]]//div[@class='react-switch-bg'])[1]";

		public static final String Addsiteorder_xngcityname = "@xpath=//input[@id='cityName']";

		public static final String Addsiteorder_XNGcitycode = "@xpath=//input[@id='cityCode']";

		public static final String Addsiteorder_IVreferencedropdown = "@xpath=//div[label[contains(text(),'IV Reference')]]//input";

		public static final String Addsiteorder_circuitReferenceField = "@xpath=//div[label[contains(text(),'Circuit Reference')]]//input";

		public static final String Addsiteorder_offnetCheckbox = "@xpath=//div[label[contains(text(),'Offnet')]]//input";

		public static final String Addsiteorder_spokeId = "@xpath=//div[div[label[contains(text(),'Spoke Id')]]]//div[text()='0']";

		public static final String Addsiteorder_spokeIdField = "@xpath=//label[contains(text(),'Spoke Id')]";

		public static final String Addsiteorder_GCRoloTypeDropdown = "@xpath=//div[label[contains(text(),'GCR OLO Type')]]//input";

		public static final String Addsiteorder_GCRoloTypeDropdown_xbutton = "@xpath=//div[label[text()='GCR OLO Type']]//div[text()='�']";

		public static final String Addsiteorder_VLAN = "@xpath=//div[label[text()='VLAN']]//input";

		public static final String Addsiteorder_VLANEtherTypeDropdown = "@xpath=//div[label[text()='VLAN Ether Type']]//input";

		public static final String Addsiteorder_VLAnEtheryTypeDropdown_xbutton = "@xpath=//div[label[text()='VLAN Ether Type']]//div[text()='�']";

		public static final String Addsiteorder_PrimaryVLAN = "@xpath=//div[label[text()='Primary VLAN']]//input";

		public static final String Addsiteorder_primaryVLANEtherTypeDropdown = "@xpath=//div[label[contains(text(),'Primary VLAN Ether Type')]]//input";

		public static final String Addsiteorder_primaryVLANEtherTypeDropdown_xbutton = "@xpath=//div[label[text()='Primary VLAN Ether Type']]//div[text()='�']";

		public static final String Addsiteorder_EPNoffnetCheckbox = "@xpath=//div[label[contains(text(),'EPN Offnet')]]//input";

		public static final String Addsiteorder_siteordernumbertextfield = "@xpath=//div[label[contains(text(),'Site Order Number(Siebel Service ID)')]]//input";

		// public static final String
		// Addsiteorder_EPNoffnetCheckbox="@xpath=//div[label[contains(text(),'EPN
		// Offnet')]]//input";

		public static final String Addsiteorder_EPNEOSDHCheckbox = "@xpath=//div[label[contains(text(),'EPN EOSDH')]]//input";

		public static final String Addsiteorder_IVreference_Primary = "@xpath=//div[contains(text(),'Primary')]";

		public static final String Addsiteorder_IVreference_Access = "@xpath=//div[contains(text(),'Access')]";

		public static final String Addsiteorder_Devicenamefield = "@xpath=//div[label[text()='Device Name']]//input";

		public static final String Addsiteorder_nonterminationpoint = "@xpath=//div[label[text()='Non Termination Point']]//input";

		public static final String Addsiteorder_protected = "@xpath=//div[label[text()='Protected']]//input";

		public static final String Addsiteorder_mappingModedropdown = "@xpath=//div[label[text()='Mapping Mode']]//input";

		public static final String Addsiteorder_mappingModedropdown_xbutton = "@xpath=//div[label[text()='Mapping Mode']]//div[text()='�']";

		public static final String portname_textField = "@xpath=//div[label[text()='Port Name']]//input";

		public static final String vlanid_textfield = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String Backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String Editsiteorder_country = "@xpath=(//div[@class='customLabelValue form-label'])[1]";

		public static final String Editsiteorder_city = "@xpath=(//div[@class='customLabelValue form-label'])[2]";

		public static final String Editsiteorder_CSRname = "@xpath=(//div[@class='customLabelValue form-label'])[3]";

		public static final String Editsiteorder_performreport = "@xpath=//div[label[contains(text(),'Performance Reporting')]]//input";

		public static final String Editsiteorder_proactivemonitor = "@xpath=//div[label[contains(text(),'Procative Monitoring')]]//input";

		public static final String Editsiteorder_smartmonitor = "@xpath=//div[label[contains(text(),'Smarts Monitoring')]]//input";

		public static final String Editsiteorder_technology = "@xpath=(//div[@class='customLabelValue form-label'])[4]";

		public static final String Editsiteorder_sitealias = "@xpath=//div[label[contains(text(),'Site Alias')]]//input";

		public static final String Editsiteorder_VLANid = "@xpath=//div[label[contains(text(),'VLAN Id')]]//input";

		public static final String Editsiteorder_remark = "@xpath=//textarea[@placeholder='Enter Remarks']";

		public static final String Editsiteorder_DCAenabledsite = "@xpath=//div[label[contains(text(),'DCA Enabled Site')]]//input";

		public static final String Editsiteorder_cloudserviceprovider = "@xpath=//div[label[contains(text(),'Cloud Service Provider')]]//input";

		public static final String verifysiteOrder_Devicecountry = "@xpath=//div[div[label[contains(text(),'Device Country')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_DeviceCity = "@xpath=//div[div[label[contains(text(),'Device Xng City')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_CSRname = "@xpath=//div[div[label[contains(text(),'CSR Name')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_performReport = "@xpath=//div[div[label[contains(text(),'Performance Reporting')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_proactivemonitor = "@xpath=//div[div[label[contains(text(),'Procative Monitoring')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_smartmonitor = "@xpath=//div[div[label[contains(text(),'Smarts Monitoring')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_Technology = "@xpath=//div[div[label[contains(text(),'Technology')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_DCAenabledsite = "@xpath=//div[div[label[contains(text(),'DCA Enabled Site')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_VLAnid = "@xpath=//div[div[label[contains(text(),'VLAN Id')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_sitealias = "@xpath=//div[div[label[contains(text(),'Site Alias')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_remark = "@xpath=//div[div[label[contains(text(),'Remark')]]]//div[@class='customLabelValue form-label']";

		public static final String Deletesiteorderrbutton = "@xpath=//button[@class='btn btn-danger']";

		public static final String totalpage_intermeiateshowinterfacelink = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//span[@ref='lbTotal']";

		public static final String currentpage_intermeduateshowinterfacelink = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//span[@ref='lbCurrent']";

		public static final String TotalPagesforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbTotal']";

		public static final String Currentpageforsiteorderfunc = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbCurrent']";

		public static final String pagenavigationfornextpage_underviewserviceforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//button[text()='Next']";

		public static final String Equipmentconfig_Currentpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//span[@ref='lbCurrent']";

		public static final String Equipmentconfig_Totalpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//span[@ref='lbTotal']";

		public static final String Equipmentconfig_nextpage = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div[1]//button[text()='Next']";

		public static final String Equipment_configurationPanelName = "@xpath=//div[text()='Equipment']";

		public static final String IntermediateEquipment_configurationPanelName = "@xpath=//div[text()='Intermediate Equipment']";

		public static final String intermediateEquip_Totalpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//span[@ref='lbTotal']";

		public static final String intermediateequip_currentpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//span[@ref='lbCurrent']";

		public static final String intermediateequip_nextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[6]//button[@ref='btNext']";

		public static final String Pagenavigationfornextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[2]//button[text()='Next']";

		public static final String pagenavigateforshowinterface = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//button[text()='Next']";

		public static final String pagenavigateforIntermediate = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[8]//button[text()='Next']";

		// public static final String !-- Add CPE device for Provider Equipment
		// Error Message --="@xpath=

		public static final String AddCPEdevice_nameerrmsg = "@xpath=//span[contains(text(),' should be replaced by corresponding value on Device Name')]";

		public static final String AddCPEdevice_snmproerrmsg = "@xpath=//div[text()='Snmpro']";

		public static final String AddCPEdevice_managementAddresserrmsg = "@xpath=//div[text()='Management Address']";

		public static final String AddCPEdevice_powerAlarmerrmsg = "@xpath=//div[text()='Power Alarm']";

		public static final String AddCPEdevice_mediaselectionerrmsg = "@xpath=//div[text()='Media Selection']";

		public static final String AddCPEdevice_macAdressErrmsg = "@xpath=//div[text()='MAC Address']";

		public static final String AddCPEdevice_vendorErrmsg = "@xpath=//div[text()='Vendor/Model']";

		public static final String AddCPEdevice_serialNumberErrmsg = "@xpath=//div[text()='Serial Number']";

		public static final String AddCPEdevice_countryErrmsg = "@xpath=//div[text()='Country']";

		public static final String AddCPEdevice_hexaSerialNumnerErrmsg = "@xpath=//div[text()='Hexa Serial Number']";

		// public static final String !-- Add CPE device fields for Provider
		// Eqeuipment --="@xpath=

		public static final String CPEdevice_adddevicelink = "@xpath=//div[div[div[text()='Equipment']]]//a[text()='Add Device']";

		public static final String CPEdevice_showinterfaceslink = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Equipment']]]//a[text()='Show Interfaces']";

		public static final String CPEdevice_hideinterfaceslinkforequipment = "@xpath=//div[div[@class='div-margin row']//div[text()='Equipment']]//a[text()='Hide Interfaces']";

		public static final String AddCPEdevice_Name = "@xpath=//div[label[contains(text(),'Name')]]//input";

		public static final String AddCPEdevice_selectdevicetogglebutton = "@xpath=(//div[@class='react-switch'])[1]";

		public static final String AddCPEdevice_vender = "@xpath=//div[label[text()='Vendor/Model']]//input";

		public static final String AddCPEdevice_snmpro = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";

		public static final String AddCPEdevice_manageaddress = "@xpath=//input[@id='managementAddress']";

		public static final String AddCPEdevice_manageaddressdropdown = "@xpath=//div[label[contains(text(),'Management Address')]]//input";

		public static final String AddCPEdevice_getSubnetbutton = "@xpath=//span[text()='Get Subnets']";

		public static final String AddCPEdevice_selectSubnettogglebutton = "@xpath=//div[div[label[contains(text(),'Select SubNet')]]]//div[@class='react-switch-bg']";

		public static final String AddCPEdevice_mepid = "@xpath=//div[label[contains(text(),'MEP Id')]]//input";

		public static final String AddCPEdevice_poweralarm = "@xpath=//div[label[text()='Power Alarm']]//input";

		public static final String AddCPEdevice_mediaselection = "@xpath=//div[label[text()='Media Selection']]//input";

		public static final String AddCPEdevice_macaddress = "@xpath=//div[label[text()='MAC Address']]//input";

		public static final String AddCPEdevice_serialnumber = "@xpath=//div[label[text()='Serial Number']]//input";

		public static final String AddCPEdevice_hexaserialnumber = "@xpath=//input[@id='hexaSerialNumber']";

		public static final String AddCPEdevice_linklostforowarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//input[@type='checkbox']";

		public static final String AddCPEdevice_OKbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String AddCPEdevice_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String CPEdevice_snmpro_autoPopulatedValue = "@xpath=//input[@value='JdhquA5']";

		public static final String viewCPEdevicelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='View'])[3]";

		public static final String viewCPEdevice_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_mepid = "@xpath=//div[div[label[contains(text(),'MEP Id')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_poweralarm = "@xpath=//div[div[label[contains(text(),'Power Alarm')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_mediaselection = "@xpath=//div[div[label[contains(text(),'Media Selection')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_linklostforwarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_macaddress = "@xpath=//div[div[label[contains(text(),'MAC Address')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_serialnumber = "@xpath=//div[div[label[text()='Serial Number']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevice_hexaserialnumber = "@xpath=//div[div[label[contains(text(),'Hexa Serial Number')]]]//div[@class='customLabelValue form-label']";

		public static final String viewPCEdevice_Actiondropdown = "@xpath=(//div[div[text()='View CPE Device']]//button[text()='Action'])";

		public static final String EditCPEdevicelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Edit'])[3]";

		public static final String EditCPEdevicelinkunderviewpage = "@xpath=//a[text()='Edit']";

		public static final String EditCPEdevlielink_underEquipment = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Edit'])[2]";

		public static final String EditCPEdevice_vendoModel_xbutton = "@xpath=//div[label[text()='Vendor/Model']]//div[text()='�']";

		public static final String EditCPEdevice_powerAlarm_xbutton = "@xpath=//div[label[text()='Power Alarm']]//div[text()='�']";

		public static final String EditCPEdevice_mediaselection_xbutton = "@xpath=//div[label[text()='Media Selection']]//div[text()='�']";

		public static final String DeletfromserviceforcpeDevice_Equipment = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Delete from Service'])[3]";

		public static final String deleteMessage_equipment = "@xpath=//div[@class='modal-body']";

		public static final String deletebutton_deletefromsrviceforequipment = "@xpath=//button[@class='btn btn-danger']";

		public static final String xbuttonfordeletefromservicepopup_Equipment = "@xpath=//div[@class='modal-header']//span[contains(text(),'�')]";

		public static final String deleteDevice_successmEssage = "@xpath=//span[text()='Site device deleted successfully']";

		public static final String technologyPopup = "@xpath=//div[@class='modal-title h4']";

		public static final String tchnologyPopup_dropdownValues = "@xpath=//div[label[@class='form-label labelStyle']]//span";

		public static final String technologypopup_dropdown = "@xpath=//div[label[@class='form-label labelStyle']]//input";

		public static final String AddCPEforIntermediateEquip_adddevicelink = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Add Device']";

		public static final String AddCPEforIntermediateEquip_Name = "@xpath=//input[@id='deviceName']";

		public static final String AddCPEforIntermediateEquip_selectdevicetogglebutton = "@xpath=(//div[@class='react-switch'])[1]";

		public static final String AddCPEforIntermediateEquip_vender = "@xpath=//div[label[text()='Vender/Model']]//input";

		public static final String AddCPEforIntermediateEquip_snmpro = "@xpath=//input[@id='snmpro']";

		public static final String AddCPEforIntermediateEquip_manageaddress = "@xpath=//input[@id='managementAddress']";

		public static final String AddCPEforIntermediateEquip_selectSubnettogglebutton = "@xpath=(//div[@class='react-switch'])[2]";

		public static final String AddCPEforIntermediateEquip_mepid = "@xpath=//input[@id='mePId']";

		public static final String AddCPEforIntermediateEquip_poweralarm = "@xpath=//div[label[text()='Power Alarm']]//input";

		public static final String AddCPEforIntermediateEquip_mediaselection = "@xpath=//div[label[text()='Media Selection']]//input";

		public static final String AddCPEforIntermediateEquip_macaddress = "@xpath=//input[@id='macAddress']";

		public static final String AddCPEforIntermediateEquip_serialnumber = "@xpath=//input[@id='serialNumber']";

		public static final String AddCPEforIntermediateEquip_hexaserialnumber = "@xpath=//input[@id='hexaSerialNumber']";

		public static final String AddCPEforIntermediateEquip_linklostforowarding = "@xpath=//input[@id='linkLostForwarding']";

		public static final String AddCPEforIntermediateEquip_country = "@xpath=//select[@id='country']";

		public static final String AddCPEforIntermediateEquip_countrydiv = "@xpath=//div[label[text()='Country']]//input";

		public static final String countrylabelname_IntEquipment = "@xpath=//label[text()='Country']";

		public static final String AddCPEforIntermediateEquip__city = "@xpath=//select[@id='city']";

		public static final String AddCPEforIntermediateEquip__citydiv = "@xpath=//div[label[text()='City']]//input";

		public static final String AddCPEforIntermediateEquip_citytogglebutton = "@xpath=(//div[@class='react-switch'])[3]";

		public static final String AddCPEforIntermediateEquip_disabledCityToggleButton = "@xpath=(//div[@class='react-switch-bg'])[4]";

		public static final String AddCPEforIntermediateEquip_site = "@xpath=//select[@id='site']";

		public static final String AddCPEforIntermediateEquip_sitediv = "@xpath=//div[label[text()='Site']]//input";

		public static final String AddCPEforIntermediateEquip_sitetogglebutton = "@xpath=(//div[@class='react-switch'])[5]";

		public static final String AddCPEforIntermediateEquip_premise = "@xpath=//select[@id='premise']";

		public static final String AddCPEforIntermediateEquip_premisediv = "@xpath=//div[label[text()='Premise']]//input";

		public static final String AddCPEforIntermediateEquip_premisetogglebutton = "@xpath=(//div[@class='react-switch'])[7]";

		public static final String AddCPEforIntermediateEquip_citynamefield = "@xpath=//input[@id='cityName']";

		public static final String AddCPEforIntermediateEquip_citycodefield = "@xpath=//input[@id='cityCode']";

		public static final String AddCPEforIntermediateEquip_sitenamefield = "@xpath=(//div[label[text()='Site Name']]//input)[1]";

		public static final String AddCPEforIntermediateEquip_sitecodefield = "@xpath=(//div[label[text()='Site Code']]//input)[1]";

		public static final String AddCPEforIntermediateEquip_premisenamefield = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";

		public static final String AddCPEforIntermediateEquip_premisecodefield = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";

		public static final String AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton = "@xpath=(//div[label[text()='Site Name']]//input)[2]";

		public static final String AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton = "@xpath=(//div[label[text()='Site Code']]//input)[2]";

		public static final String AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";

		public static final String AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";

		public static final String AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		public static final String AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";

		public static final String deletefromservicelink_IntermediateEquipment = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='Delete from Service']";

		public static final String viewCPEforIntermediateEquiplink = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='View']";

		public static final String viewCPEforIntermediateEquip_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_mepid = "@xpath=//div[div[label[contains(text(),'MEP Id')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_poweralarm = "@xpath=//div[div[label[contains(text(),'Power Alarm')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_mediaselection = "@xpath=//div[div[label[contains(text(),'Media Selection')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_linklostforwarding = "@xpath=//div[div[label[contains(text(),'Link Lost Forwarding')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_macaddress = "@xpath=//div[div[label[contains(text(),'MAC Address')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_serialnumber = "@xpath=//div[div[label[text()='Serial Number']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_hexaserialnumber = "@xpath=//div[div[label[contains(text(),'Hexa Serial Number')]]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_country = "@xpath=//div[div[label[text()='Country']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_city = "@xpath=//div[div[label[text()='City']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_site = "@xpath=//div[div[label[text()='Site']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_premise = "@xpath=//div[div[label[text()='Premise']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEforIntermediateEquip_Actiondropdown = "@xpath=(//div[@class='heading-green row'][1]//button[@id='dropdown-basic-button'])[1]";

		public static final String EditCPEdeviceforIntermediateEquipmentlinkunderviewpage = "@xpath=//a[text()='Edit']";

		public static final String EditCPEdeviceforIntermediateEquipmentlink = "@xpath=(//div[div[div[text()='Intermediate Equipment']]]//div[@class='div-margin row'])[5]//span[text()='Edit']";

		public static final String showInterface_ForIntermediateEquipment = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Show Interfaces']";

		public static final String HideInterface_ForIntermediateEquipment = "@xpath=//div[div[div[text()='Intermediate Equipment']]]//a[text()='Hide Interfaces']";

		// public static final String !-- Fetch Dveice Interface --="@xpath=

		public static final String fetchDeviceinterfacelink_viewDevicePage = "@xpath=//a[text()='Fetch Device Interfaces']";

		public static final String succesMessageForfetchDeviceInterfcae = "@xpath=//div[contains(text(),'Fetch Interfaces started successfully. Please chec')]";

		public static final String clickherelink_fetchDeviceinterfcae = "@xpath=//a[@class='customLink heading-green-1']";

		public static final String ManageNetworkHeaderName = "@xpath=//div[div[contains(text(),'Manage Network')]]";

		// public static final String !-- Select interface for Intermediate
		// Equipment --="@xpath=

		public static final String CPEdevice_showinterfaceslink_intEquip = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Intermediate Equipment']]]//a[text()='Show Interfaces']";

		// public static final String !-- xpath for table --="@xpath=

		public static final String configure_totalPage = "@xpath=//span[@ref='lbTotal']";

		public static final String configure_currentpage = "@xpath=//span[@ref='lbCurrent']";

		public static final String Actelisconfig_addDSLAM = "@xpath=//div[div[div[text()='Actelis Configuration']]]//a[text()='Add DSLAM and HSL']";

		public static final String DSLM_Device_Select = "@xpath=//div[div[div[label[@class='form-label']]]]//input";

		public static final String List_HSL_Link = "@xpath=//span[contains(text(),'List HSL')]";

		public static final String ActelisConfigurationPanel = "@xpath=//div[div[contains(text(),'Actelis Configuration')]]";

		public static final String ActiondropdownforEditLink_IntermediateEquipmentforcpedeviec = "@xpath=(//div[div[text()='View CPE Device']]//button[text()='Action'])[2]";

		public static final String AddPElink_LANlinkoutband = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Add Device']";

		public static final String ProviderEquipment_showInterfacelink = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Show Interfaces']";

		public static final String SelectIMSlocation = "@xpath=(//select[@id='imsPOP'])[1]";

		public static final String selectdevice_ToggleButton = "@xpath=(//div[@class='div-margin row']//div[@class='react-switch-bg'])[1]";

		public static final String Name = "@xpath=//input[@id='name']";

		public static final String VenderModel = "@xpath=//select[@id='vender']";

		public static final String managementaddress = "@xpath=//input[@id='address']";

		public static final String Snmpro = "@xpath=//input[@id='snmpro']";

		// public static final String country="@xpath=//select[@id='country']";

		public static final String city = "@xpath=//select[@id='city']";

		public static final String site = "@xpath=//select[@id='site']";

		public static final String premise = "@xpath=//select[@id='premise']";

		public static final String existingDevice_SelectDeviceToggleButton = "@xpath=//div[div[label[text()='Select Device']]]//div[@class='react-switch']";

		public static final String chooseAdeviceDropdown = "@xpath=//div[label[text()='Choose a Device']]//input";

		public static final String IntermediateEquipment_OKbuttonforpopup = "@xpath=//button[@class='btn btn-secondary']";

		public static final String verifyPEname_lanlinkoutband = "@xpath=(//div[div[label[text()='Name']]])[2]//div[@class='customLabelValue form-label']";

		public static final String verifyPEvendor_lanlinkoutband = "@xpath=(//div[div[label[text()='Vendor/Model']]])[2]//div[@class='customLabelValue form-label']";

		public static final String verifyPEaddress_lanlinkoutband = "@xpath=(//div[div[label[text()='Management Address']]])[2]//div[@class='customLabelValue form-label']";

		public static final String verifyPEsnmpro_lanlinkoutband = "@xpath=(//div[div[label[text()='Snmpro']]])[2]//div[@class='customLabelValue form-label']";

		public static final String verifyPEcountry_lanlinkoutband = "@xpath=(//div[div[label[text()='Country']]])[1]//div[@class='customLabelValue form-label']";

		public static final String verifyPECity_lanlinkoutband = "@xpath=(//div[div[label[text()='City']]])[1]//div[@class='customLabelValue form-label']";

		public static final String verifyPESite_lanlinkoutband = "@xpath=(//div[div[label[text()='Site']]])[1]//div[@class='customLabelValue form-label']";

		public static final String verifyPEPremise_lanlinkoutband = "@xpath=(//div[div[label[text()='Premise']]])[1]//div[@class='customLabelValue form-label']";

		// public static final String verifyIMSlocation/="@xpath=

		public static final String Providerequipment_viewlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='View'])[3]";

		public static final String Providerequipment_Editlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Edit'])[2]";

		public static final String Providerequipment_selectinterface = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Select Interfaces'])[2]";

		public static final String Providerequipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Configure'])[2]";

		public static final String Providerequipment_deletefromservice = "@xpath=(//div[div[@class='div-margin row']//div[text()='Provider Equipment (PE)']]//span[text()='Delete from Service'])[3]";

		public static final String Providerequipment_ShowInterface = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Show Interfaces']";

		public static final String providerEquipment_actiondropdownfromviewpage = "@xpath=(//button[text()='Action'])[2]";

		public static final String providerEquipment_Editlinkunderviewdevicepage = "@xpath=//a[text()='Edit']";

		public static final String providerEquipment_showinterfaceTatoalpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//span[@ref='lbTotal']";

		public static final String providerEquipment_showinterfaceCurrentpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//span[@ref='lbCurrent']";

		public static final String providerEquipment_showinterfaceNextpage = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[1]//button[@ref='btNext']";

		public static final String ProviderEquipment_Generateconfigforstaticlink = "@xpath=//a[text()='Generate Configuration for Static Routes']";

		public static final String providerEquipment_saveconfigurationlink = "@xpath=//a[text()='Save Configuration']";

		public static final String providerEquipment_ExecuteconfigurationOndevicelink = "@xpath=//a[text()='Execute Configuration on Device']";

		public static final String providerEquipment_currentpageInterfaceToselect = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbCurrent'])[2]";

		public static final String providerEquipment_InterfaceToselectTotalpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbTotal'])[2]";

		public static final String providerEquipment_InterfaceToselectActiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";

		public static final String providerEquipment_InterfaceToselectAddbutton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";

		public static final String providerEquipment_InterfaceToselectnextpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//button[@ref='btNext'])[2]";

		public static final String providerEquipment_currentpageInterfaceInselect = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbCurrent'])[1]";

		public static final String providerEquipment_InterfaceInselectTotalpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//span[@ref='lbTotal'])[1]";

		public static final String providerEquipment_InterfaceInselectActiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";

		public static final String providerEquipment_InterfaceInselectRemovebutton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";

		public static final String providerEquipment_InterfaceInselectnextpage = "@xpath=(//div[@class='row']//span[@class='ag-paging-page-summary-panel']//button[@ref='btNext'])[1]";

		public static final String customerpremiseEquipment_viewlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='View'])[1]";

		public static final String customerpremiseequipment_Editlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Edit'])[1]";

		public static final String cusomerpremiseequipment_SelectInertafeceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Select Interfaces'])[1]";

		public static final String customerpremiseequipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Configure'])[1]";

		public static final String customerpremiseEquipment_FetchdeviceInterfacelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";

		public static final String customerpremiseEquipment_PPPconfigurationlink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";

		public static final String customerpremiseEquipment_deletefromservceilink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Customer Premise Equipment (CPE)']]//span[text()='Fetch device interfaces'])[1]";

		public static final String customerpremiseEquipment_Adddevicelink = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Add Device']";

		public static final String customerpremiseEQuipment_showinetrfacelink = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Show Interfaces']";

		public static final String AddCPElink_LANlinkoutband = "@xpath=//div[div[div[text()='Customer Premise Equipment (CPE)']]]//a[text()='Add Device']";

		public static final String Actionbutton_PE2CPE = "@xpath=(//div[@class='heading-green div-margin row']//button[text()='Action'])[1]";

		public static final String Addnewlink_Pe2CPE = "@xpath=//a[text()='Add New Link']";

		public static final String AddnewlinkPe2CPE_LinkorCircuitId = "@xpath=//input[@id='linkCircuitId']";

		public static final String AddnewlinkPe2CPE_SourceDevice = "@xpath=//select[@id='fromDeviceName']";

		public static final String AddnewlinkPe2CPE_SourceInterface = "@xpath=//select[@id='fromInterfaceName']";

		public static final String AddnewlinkPe2CPE_TargetDevice = "@xpath=//select[@id='toDeviceName']";

		public static final String AddnewlinkPe2CPE_TargetInterface = "@xpath=//select[@id='toInterfaceName']";

		public static final String AddnewlinkPe2CPE_Nextbutton = "@xpath=//span[contains(text(),'Next')]";

		public static final String Headername_PE2CPElink = "@xpath=//div[@class='modal-title h4']";

		public static final String Pe2CPElinktable_totalpage = "@xpath=//div[@class='ag-div-margin row']//span[@ref='lbTotal']";

		public static final String Pe2CPElinktable_currentpage = "@xpath=//div[@class='ag-div-margin row']//span[@ref='lbCurrent']";

		public static final String Pe2CPElinktable_nextpage = "@xpath=//div[@class='ag-div-margin row']//button[@ref='btNext']";

		// public static final String !-- Copy from here --="@xpath=

		public static final String intermediateTechErrorMessage = "@xpath=//button[@id='id1']//*[contains(@class,'svg-inline--fa fa-info-circle fa-w-16')]";

		public static final String IntermediaTechnologypopup = "@xpath=//div[@class='popover-body']";

		public static final String circuitreferenceIalert = "@xpath=//button[@id='id2']";

		public static final String circuitreferencealertmessage = "@xpath=//div[@class='modal-body']";

		public static final String circuitreferencealerterrmsg = "@xpath=//div[@class='popover-body']";

		public static final String managementconnection = "@xpath=//div[label[contains(text(),'Management Connection')]]//input";

		public static final String Equipment_selectInterfaceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Select Interfaces'])[2]";

		public static final String InterfaceToselect = "@xpath=(//div[@class='row'])[7]";

		public static final String InterfaceInselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";

		public static final String InterfaceInselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";

		public static final String InterfaceInselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";

		public static final String InterfaceInselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";

		public static final String InterfaceToselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";

		public static final String InterfaceToselect_addbuton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";

		public static final String InterfaceInslect = "@xpath=(//div[@class='row'])[6]";

		public static final String InterfaceToselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";

		public static final String InterfaceToselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";

		public static final String InterfaceToselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";

		public static final String InterfaceToselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";

		public static final String InterfaceInselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";

		public static final String InterfaceInselect_removebuton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";

		public static final String InterfaceToselect_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String findlistofInterfacesNames = "@xpath=//div[@class='ag-cell ag-cell-not-inline-editing ag-cell-with-height ag-cell-no-focus ag-cell-value'][1]";

		public static final String IntermediateEquipment_selectInterfaceslink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Intermediate Equipment']]//span[text()='Select Interfaces'])[2]";

		public static final String configure_alertPopup = "@xpath=//div[text()='Alert']";

		public static final String configure_alertMessage = "@xpath=//div[@class='modal-body']";

		public static final String configure_alertPopup_xbutton = "@xpath=//span[text()='�']";

		public static final String IntermediateEquipment_Configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Intermediate Equipment']]//span[text()='Configure'])[2]";

		public static final String Equipment_configurelink = "@xpath=(//div[div[@class='div-margin row']//div[text()='Equipment']]//span[text()='Configure'])[2]";

		public static final String Equipment_configureActiondropdown = "@xpath=//div[div[text()='Interfaces']]//button[text()='Action']";

		public static final String Equipment_configureEditlink = "@xpath=//a[text()='Edit']";

		public static final String Equipment_showintefaceedit = "@xpath=(//a[text()='Edit'])[1]";

		public static final String Equipment_configureEditInterfaceTitle = "@xpath=//div[@class='modal-title h4']";

		public static final String Equipment_configureXNGCircuitID = "@xpath=(//div[label[text()='XNG Circuit Id']])[1]//input";

		public static final String selectCircuit_togglebutton = "@xpath=//div[@class='react-switch-bg']";

		public static final String Equipment_configureBearerType = "@xpath=//div[label[text()='Bearer Type']]//input";

		public static final String Equipment_configureBearerSpeed = "@xpath=//div[label[text()='Bearer Speed']]//input";

		public static final String Equipment_configureTotalcircuitbandwidth = "@xpath=//div[label[text()='Total Circuit Bandwidth']]//input";

		public static final String Equipment_configureVLANid = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String Equipment_configureVlanType = "@xpath=//div[label[text()='VLAN Type']]//input";

		public static final String Equipment_showInterfaceActiondropdown = "@xpath=(//button[text()='Action'])[2]";

		public static final String Equipment_showInterfaceTotalPages = "@xpath=(//div[@class='ag-root-wrapper ag-layout-normal ag-ltr'])[3]//span[@ref='lbTotal']";

		public static final String Currentpageforsiteorder = "@xpath=(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//span[@ref='lbCurrent']";

		public static final String IntermediateEquipment_Actiondropdown = "@xpath=(//button[text()='Action'])[4]";

		public static final String IntermediateEquipment_Editlink = "@xpath=(//a[text()='Edit'])[3]";

		public static final String Editservice_actiondropdown = "@xpath=//div[div[text()='Service']]//button[text()='Action']";

		public static final String Editservice_actiondropdownalternate = "@xpath=(//button[@id='dropdown-basic-button'])[4]";

		public static final String Editservice_Editlink = "@xpath=//div[div[text()='Service']]//a[contains(text(),'Edit')]";

		public static final String Editservice_Deletelink = "@xpath=//a[text()='Delete']";

		public static final String Editservice_sysnchronizelink = "@xpath=//a[text()='Synchronize']";

		public static final String Editservice_infovistareport = "@xpath=//a[text()='Show New Infovista Report']";

		public static final String Editservice_managelink = "@xpath=//a[text()='Manage']";

		public static final String Editservice_managesubnets = "@xpath=//a[text()='Manage Subnets']";

		public static final String Editservice_AMNvalidator = "@xpath=//a[text()='AMN Validator']";

		public static final String Editservice_Dumplink = "@xpath=//a[text()='Dump']";

		public static final String viewServicepage_Servicepanel = "@xpath=//div[contains(text(),'Service')]";

		public static final String viewServicepage_ManagementOptionsPanel = "@xpath=//div[contains(text(),'Management Options')]";

		public static final String viewServicepage_ConfigurationoptionsPanel = "@xpath=//div[contains(text(),'Configuration Options')]";

		public static final String viewServicepage_SiteORderPanel = "@xpath=//div[contains(text(),'Site Orders')]";

		public static final String viewServicepage_OrderPanel = "@xpath=//div[text()='Order']";

		// public static final String !-- Outband Management --="@xpath=

		public static final String Addsiteorder_siteordernumberfield = "@xpath=//div[label[contains(text(),'Site Order Number')]]//input";

		public static final String Addsiteorder_primarysiteorder = "@xpath=//div[label[contains(text(),'Primary Site Order')]]//input";

		public static final String Addsiteorder_perCoSperformncereport = "@xpath=//div[label[contains(text(),'Per CoS Performance Reporting')]]//input";

		public static final String AddSiteorder_IVreferencedropdown = "@xpath=//div[label[contains(text(),'IV Reference')]]//input";

		public static final String Addsiteorder_RouterconfigviewIpv4 = "@xpath=//div[label[contains(text(),'Router Configuration View IPv4')]]//input";

		public static final String Addsiteorder_wholesaleProvider = "@xpath=//div[label[contains(text(),'Wholesale Provider')]]//input";

		public static final String Addsiteorder_managedsite = "@xpath=//div[label[contains(text(),'Managed Site')]]//input";

		public static final String Addsiteorder_priority = "@xpath=//div[label[contains(text(),'Priority')]]//input";

		public static final String Addsiteorder_actelisbasedcheckbox = "@xpath=//div[label[contains(text(),'Actelis Based')]]//input";

		public static final String Addsiteorder_voipcheckbox = "@xpath=//div[label[contains(text(),'VoIP')]]//input";

		public static final String verifysiteOrder_perCoSperformReporting = "@xpath=//div[div[label[contains(text(),'Per CoS Performance Reporting')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_primarysiteorder = "@xpath=//div[div[label[contains(text(),'Primary Site Order')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_IVreference = "@xpath=//div[div[label[contains(text(),'IV Reference')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_priority = "@xpath=//div[div[label[contains(text(),'Priority')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_managedSite = "@xpath=//div[div[label[contains(text(),'Managed Site')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_RouterConfigviewIPv4 = "@xpath=//div[div[label[contains(text(),'Router Configuration View IPv4')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_actelisBased = "@xpath=//div[div[label[contains(text(),'Actelis Based')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_wholesaleProvider = "@xpath=//div[div[label[contains(text(),'Wholesale Provider')]]]//div[@class='customLabelValue form-label']";

		public static final String verifysiteOrder_VoIP = "@xpath=//div[div[label[contains(text(),'VoIP')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_providerEquipment_addDevice = "@xpath=//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a[text()='Add Device']";

		public static final String outband_providerEquipment_addDevice_chooseAdevicedrodown = "@xpath=//div[label[@class='form-label labelStyle']]//input";

		public static final String outband_providerEquipment_addDevice_venderModel = "@xpath=//div[label[contains(text(),'Vendor/Model')]]//input";

		public static final String outband_providerEquipment_addDevice_managenmetaddress = "@xpath=//div[label[contains(text(),'Management Address')]]//input";

		public static final String outband_providerEquipment_addDevice_snmpro = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";

		public static final String outband_providerEquipment_addDevice_country = "@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String outband_providerEquipment_addDevice_city = "@xpath=//div[label[contains(text(),'City')]]//input";

		public static final String outband_providerEquipment_addDevice_site = "@xpath=//div[label[contains(text(),'Site')]]//input";

		public static final String outband_providerEquipment_addDevice_premise = "@xpath=//div[label[contains(text(),'Premise')]]//input";

		public static final String outband_customerpremiseEquipment_addDevicelink = "@xpath=//div[div[div[contains(text(),'Customer Premise Equipment (CPE)')]]]//a[text()='Add Device']";

		public static final String outband_customerpremiseEquipment_addDevice_routerIderrmsg = "@xpath=//div[text()='Router Id']";

		public static final String outband_customerpremiseEquipment_addDevice_nameErrmsg = "@xpath=//div[text()='Name']";

		public static final String outband_customerpremiseEquipment_addDevice_vendorErrmsg = "@xpath=//div[text()='Vendor/Model']";

		public static final String outband_customerpremiseEquipment_addDevice_managementAddressErrmsg = "@xpath=//div[text()='Management Address']";

		public static final String outband_customerpremiseEquipment_addDevice_snmproErrmsg = "@xpath=//div[text()='Snmpro']";

		public static final String outband_customerpremiseEquipment_addDevice_routerIdField = "@xpath=//div[label[contains(text(),'Router Id')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_deviceNameField = "@xpath=//div[label[contains(text(),'Device Name')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_vendorModelField = "@xpath=//div[label[contains(text(),'Vendor/Model')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_manageAddresField = "@xpath=//div[label[contains(text(),'Management Address')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_snmproField = "@xpath=//div[label[contains(text(),'Snmpro')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_snmprwField = "@xpath=//div[label[contains(text(),'Snmprw')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_snmpv3ContextnameField = "@xpath=//div[label[contains(text(),'Snmp V3 Context Name')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_snmpv3ContextEngineIdField = "@xpath=//div[label[contains(text(),'Snmp V3 Context Engine ID')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_snmpv3SecurityUsernameField = "@xpath=//div[label[contains(text(),'Snmp V3 Security Username')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_snmpv3authprotoDropdown = "@xpath=//div[label[contains(text(),'Snmp V3 Auth Proto')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_snmpv3authPasswordField = "@xpath=//div[label[contains(text(),'Snmp V3 Auth Password')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_premisedropdown = "@xpath=//div[label[text()='Premise']]//input";

		public static final String outband_customerpremiseEquipment_addDevice_selectpremiseToggleButton = "@xpath=(//div[div[label[text()='Select Premise']]]//div[@class='react-switch-handle'])[1]";

		public static final String outband_customerpremiseEquipment_addDevice_premisenameField = "@xpath=//div[label[contains(text(),'Premise Name')]]//input";

		public static final String outband_customerpremiseEquipment_addDevice_premiseCodeField = "@xpath=//div[label[contains(text(),'Premise Code')]]//input";

		// public static final String
		// outband_customerpremiseEquipment_addDevice_networkDropdown/="@xpath=

		public static final String outband_customerpremiseEquipment_viewDevice_name = "@xpath=//div[div[label[contains(text(),'Name')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_customerpremiseEquipment_viewDevice_vendor = "@xpath=//div[div[label[contains(text(),'Vendor/Model')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_customerpremiseEquipment_viewDevice_managementAddress = "@xpath=//div[div[label[contains(text(),'Management Address')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_customerpremiseEquipment_viewDevice_snmpro = "@xpath=//div[div[label[contains(text(),'Snmpro')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_customerpremiseEquipment_viewDevice_country = "@xpath=//div[div[label[contains(text(),'Country')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_customerpremiseEquipment_viewDevice_city = "@xpath=//div[div[label[contains(text(),'City')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_customerpremiseEquipment_viewDevice_site = "@xpath=//div[div[label[contains(text(),'Site')]]]//div[@class='customLabelValue form-label']";

		public static final String outband_customerpremiseEquipment_viewDevice_Premise = "@xpath=//div[div[label[contains(text(),'Premise')]]]//div[@class='customLabelValue form-label']";

		// public static final String !-- For Interface speed 10GigE --="@xpath=

		public static final String circuit_10GigeActiondropdown = "@xpath=//div[@class='dropdwon-actionBtn-right show dropdown']//button[@id='dropdown-basic-button']";

		public static final String Circuit_10GigEaddOverturelink = "@xpath=//a[contains(text(),'Add Overture')]";

		public static final String Circuit_10GigEconfigurelink = "@xpath=//a[contains(text(),'Configure')]";

		public static final String Circuit_10GigEPAMtest = "@xpath=//a[contains(text(),'PAM Test')]";

		public static final String Circuit_10GigEdeletelink = "@xpath=//a[contains(text(),'Delete')]";

		public static final String Overture_servicenameErrmsg = "@xpath=//div[text()='Service Name']";

		public static final String Overture_ServiceNameField = "@xpath=//label[text()='Service Name']";

		public static final String Overture_searchButton = "@xpath=//span[contains(text(),'Search')]";

		// public static final String !-- Router Tool panel --="@xpath=

		public static final String outband_customerpremiseEquipment_viewDevicePage_RouterToolPanel_commandIPv4 = "@xpath=//div[label[text()='Command IPvp4']]//input";

		public static final String outband_customerpremiseEquipment_viewDevicePage_RouterToolPanel_execute = "@xpath=//a[contains(text(),'Execute')]";

		// public static final String
		// nextbutton="@xpath=//span[contains(text(),'Next')]";

		public static final String order_contractnumberErrmsg = "@xpath=(//span[text()='Order/Contract Number(Parent SID) is required'])[2]";

		public static final String servicetypeerrmsg = "@xpath=//span[contains(text(),'Service type is required')]";

		public static final String interfaceSpeedErrmsg = "@xpath=//span[contains(text(),'Interface Speed is required')]";

		public static final String servicesubtypeErrMsg = "@xpath=//span[contains(text(),'Sub service type is required')]";

		public static final String serviceIdentificationerrmsg = "@xpath=//div[text()='Service Identification']";

		public static final String circuitreferenceErrmsg = "@xpath=//div[text()='Circuit Reference']";

		// public static final String !-- 10May --="@xpath=

		public static final String EquipementConfigurationPanel = "@xpath=//div[div[contains(text(),'Equipment Configuration')]]";

		public static final String equipConfig_addCPEdevice = "@xpath=//div[div[contains(text(),'Equipment Configuration')]]/following-sibling::div//a";

		public static final String addActelisCPEpage_headerName = "@xpath=//div[div[p[text()='Add Actelis CPE']]]";

		public static final String nameField_addCPE_Actelis = "@xpath=//div[label[text()='Name']]//input";

		public static final String vendorField_addCPE_Actelis = "@xpath=//div[label[text()='Vendor/Model']]//input";

		public static final String RouterIdField_addCPE_Actelis = "@xpath=//div[label[text()='Router Id']]//input";

		public static final String managementAddressField_addCPE_Actelis = "@xpath=//div[label[text()='Management Address']]//input";

		public static final String MEPidField_addCPE_Actelis = "@xpath=//div[label[text()='MEP Id']]//input";

		public static final String ETHportField_addCPE_Actelis = "@xpath=//div[label[text()='ETH Port']]//input";

		public static final String devicenameFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Device Name']";

		public static final String RouterIDFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Router Id']";

		public static final String manageAddressFieldErrMSg_addCPE_Actelis = "@xpath=//div[text()='Management Address']";

		public static final String fetchInterface_AddDSLAMdevice = "@xpath=//span[contains(text(),'Fetched interfaces successfully')]";

		public static final String InterfaceToSelect_actelis_totalpage = "@xpath=//span[@ref='lbTotal']";

		public static final String InterfaceToSelect_actelis_currentpage = "@xpath=//span[@ref='lbCurrent']";

		public static final String alertForSynchronize = "@xpath=//div[@role='alert']";

		public static final String alertMSG_synchronize = "@xpath=//div[@role='alert']/span";

		public static final String showInterface_ActelisCnfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//a[text()='Show Interfaces']";

		public static final String AcionButton_ActelisConfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//button[text()='Action']";

		public static final String removeButton_ActelisConfiguration = "@xpath=//a[text()='Remove']";

		public static final String popupMessage_forRemove_ActelisConfiguration = "@xpath=//div[@class='modal-body']";

		public static final String deleteInterface_successMessage_ActelisCOnfiguration = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

		public static final String successMessage_ActelisConfiguration_removeInterface = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

		// public static final String !-- 9 june --="@xpath=

		public static final String warningmEssage1_devicename = "@xpath=(//span/li)[1]";

		public static final String warningmEssage2_devicename = "@xpath=(//span/li)[2]";

		public static final String warningmEssage3_devicename = "@xpath=(//span/li)[3]";

		public static final String changeButton_managementAddress = "@xpath=//span[contains(text(),'Change')]";

		public static final String manageSubnetPage_header = "@xpath=(//div[div[text()='Manage Subnets']])[1]";

		public static final String manageSubnet_errMsg = "@xpath=//div[@class='msg-error']/span";

		public static final String dumpPage_header = "@xpath=//div[@class='modal-header']";

		public static final String dumpheaderName = "@xpath=//div[@class='modal-header']/div";

		public static final String dumpMessage_body = "@xpath=//div[@class='modal-body']/div";

		public static final String dump_xButton = "@xpath=//span[contains(text(),'�')]";

		public static final String dump_container = "@xpath=(//div[div[@class='container']]/div)[2]";

		// public static final String !-- Edit device --="@xpath=

		public static final String countryinput = "@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String citydropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'City')]]//input";

		public static final String sitedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//input";

		public static final String premisedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//input";

		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String citynameinputfield = "@xpath=//input[@id='cityName']";

		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode']";

		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1]";

		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1]";

		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";

		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";

		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2]";

		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2]";

		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";

		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";

		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";

		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		// public static final String !-- view page --="@xpath=

		public static final String standardHeaderName_viewPage = "@xpath=(//tr)[2]/th/label";

		public static final String standardCIRvalue_viewPage = "@xpath=(//label[text()='Standard']/parent::th/following-sibling::td/div)[1]";

		public static final String standardEIRvalue_viewPage = "@xpath=(//label[text()='Standard']/parent::th/following-sibling::td/div)[2]";

		public static final String PremisumHeaderName_viewPage = "@xpath=(//tr)[3]/th/label";

		public static final String premiumCIRvalue_viewPage = "@xpath=(//label[text()='Premium']/parent::th/following-sibling::td/div)[1]";

		public static final String premiumEIRvalue_viewPage = "@xpath=(//label[text()='Premium']/parent::th/following-sibling::td/div)[2]";

		public static final String labelName_searialnumber = "@xpath=//label[text()='Serial Number']";

		public static final String labelname_managementAddresss = "@xpath=//label[text()='Management Address']";

		// public static final String !-- Router Tool --="@xpath=

		public static final String commandIPV4_dropdown = "@xpath=//div[label[text()='Command IPV4']]//input";

		public static final String commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";

		public static final String executebutton_Ipv4 = "@xpath=(//span[text()='Execute'])[1]";

		public static final String result_textArea = "@xpath=//div[label[text()='Result']]//textarea";

		// public static final String !-- Fetch device Interface --="@xpath=

		public static final String ClickhereLink_fetchInterface = "@xpath=//a[text()='here']";

		public static final String successMessage_Alert = "@xpath=//div[@role='alert']//span";

		public static final String succesMsg_alertDisplay = "@xpath=//div[@role='alert']";

		public static final String CPEdevice_hideInterfaeLink_eqiupment = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Equipment']]]//a[text()='Hide Interfaces']";

		public static final String CPEdevoce_hideInterfacelink_intEquip = "@xpath=//div[@class='div-border cst-details container']//div[div[div[text()='Intermediate Equipment']]]//a[text()='Hide Interfaces']";

		public static final String breadcrump = "@xpath=//a[text()='value']";

		public static final String fetchVendorModelvalue = "@xpath=//div[div[label[text()='Vendor/Model']]]//div[@class='customLabelValue form-label']";

		public static final String fetchDeviceValue = "@xpath=//div[div[label[text()='Name']]]//div[@class='customLabelValue form-label']";

		public static final String fetchmanagementAddressvalue = "@xpath=//div[div[label[text()='Management Address']]]//div[@class='customLabelValue form-label']";

		public static final String fetchCountryValue = "@xpath=//div[div[label[text()='Country']]]//div[@class='customLabelValue form-label']";

		public static final String viewCPEdevicepage_devices = "@xpath=//div[div[text()='View CPE Device']] ";

		public static final String devicePanelHeaders_InViewSiteOrderPage = "@xpath=//div[div[text()='value']]";

		// public static final String !-- PAM Test --="@xpath=

		public static final String PAMtest_Link = "@xpath=//a[contains(text(),'PAM Test')]";

		public static final String PAMtest_popupPage = "@xpath=//div[div[div[text()='PAM Test']]]";

		public static final String PAMTest_TypeFieldValue = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Type']]//following-sibling::div";

		public static final String PAMTest_ServiceValue = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Service']]//following-sibling::div";

		public static final String PAMTest_ToolResponse = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Tool Response']]//following-sibling::div";

		// public static final String
		// PAMTest_siteValue="@xpath=//div[div[div[text()='PAM
		// Test']]]//div[label[text()='Site']]//following-sibling::div";

		// public static final String !-- Manage Service --="@xpath=

		public static final String viewServicePage_fetchServiceId = "@xpath=//div[div[label[contains(text(),'Service Identification')]]]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String viewServicePage_orderNumber = "@xpath=//div[div[label[contains(text(),'Order/Contract Number (Parent SID)')]]]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String ManageServicesPage = "@xpath=//div[div[contains(text(),'Manage Services')]]";

		public static final String status_columnNames = "@xpath=//div[div[text()='Status']]//following-sibling::div//label";

		public static final String statusPanel_serviceId = "@xpath=((//div[div[label[text()='Service']]])[1]//following-sibling::div//a)[1]";

		public static final String statusPanel_serviceType = "@xpath=((//div[div[label[text()='Service Type']]])[1]//following-sibling::div//div[@class='col-12 col-sm-12 col-md-2'])[2]";

		public static final String statusPanel_statusColumn = "@xpath=((//div[div[label[text()='Service Type']]])[1]//following-sibling::div//div[@class='col-12 col-sm-12 col-md-2'])[4]";

		public static final String statusPanel_detailColumn = "@xpath=((//div[div[label[text()='Service Type']]])[1]//following-sibling::div//div[@class='col-12 col-sm-12 col-md-2'])[3]";

		public static final String statusPanel_lastModificationColumn = "@xpath=((//div[div[text()='Status']]//following-sibling::div)[3]/div)[5]";

		// public static final String !-- AMN Validator --="@xpath=

		public static final String AMNvalidator_panelHeader = "@xpath=//div[div[text()='AMN Validator Results']]";

		public static final String AMNvalidator_status_columnHeader = "@xpath=//div[div[text()='Site Orders']]//following-sibling::div//span[@role='columnheader']";

		public static final String AMNvalidator_device_columnHeader = "@xpath=//div[div[text()='Devices For Service']]//following-sibling::div//span[@role='columnheader']";

		public static final String AMNvalidator_statusPanel_siteOrderColumnValue = "@xpath=//div[@col-id='groupName'][@role='gridcell']";

		public static final String AMNvalidator_statusPanel_CSRsiteNameColumnValue = "@xpath=//div[@col-id='siteName'][@role='gridcell']";

		public static final String AMNvalidator_statusPanel_deviceXngCityName = "@xpath=//div[@col-id='cityName'][@role='gridcell']";

		public static final String AMNvalidator_statusPanel_countryName = "@xpath=//div[@col-id='countryName'][@role='gridcell']";

		public static final String AMNvalidator_devicePanel_deviceName = "@xpath=//div[div[text()='Devices For Service']]//following-sibling::div//div[@class='ag-react-container']/a";

		public static final String physicalSiteCSRName_viewSiteOrder = "@xpath=//div[label[text()='Physical Site: CSR Name']]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String deviceCityName_viewSiteOrder = "@xpath=//div[label[text()='Device Xng City']]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String countryName_viewSiteOrder = "@xpath=//div[label[text()='Device Country']]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String siteOrder_ActionButton = "@xpath=//div[div[text()='Site Order']]//button[text()='Action']";

		public static final String AMNvalidatorlink = "@xpath=//a[text()='AMN Validator']";

		public static final String fetchDeviceInterace_SuccessMessage = "@xpath=(//div[div[@class='alert alert-success alert-Style col-12 col-sm-12 col-md-12']])[1]";

		public static final String fetchdeviceInterface_successtextMessage = "@xpath=(//div[@class='alert alert-success alert-Style col-12 col-sm-12 col-md-12'])[1]";

		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";

		public static final String deletebutton = "@xpath=//button[@class='btn btn-danger']";

		public static final String deleteMessage = "@xpath=//div[@class='modal-body']//div[@class='col-12 col-sm-12 col-md-12']";

		public static final String deleteMessages_textMessage = "@xpath=//div[@class='modal-body']";

		public static final String deleteLink_common = "@xpath=//a[text()='Delete']";

		public static final String remark_viewPage = "@xpath=//label[text()='Remark']/parent::div//div";

		// public static final String !-- select tag Dropdown --="@xpath=

		public static final String countryDropdown_selectTag = "@xpath=//select[@id='country']";

		public static final String cityDropdown_selectTag = "@xpath=//select[@id='cityId']";

		public static final String siteDropdown_selectTag = "@xpath=//select[@id='siteId']";

		public static final String premiseDropdown_selectTag = "@xpath=//select[@id='premiseId']";

		public static final String actelis_EquipConfig_viewLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='View']";

		public static final String actelis_EquipConfig_deleteLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='Delete']";

		public static final String addDSLAMandHSL_xButton = "@xpath=//div[div[label[text()='DSLAM Device:']]]//following-sibling::div//div[text()='�']";

		public static final String selectDSLAMdeviceValue = "@xpath=//div[text()='value']";

		// public static final String !-- Circuit Creation --="@xpath=

		public static final String addOvertureLink = "@xpath=(//span[text()='Add Overture'])[1]";

		public static final String addCircuit_AccedianLink = "@xpath=//span[text()='Add Accedian-1G']";

		public static final String addCircuit_AtricaLink = "@xpath=//span[contains(text(),'Add Circuit')]";

		public static final String addOverture_OKbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String addOverture_overturePanel = "@xpath=//div[contains(text(),'Select Overture Circuit')]";

		public static final String addCircuit_Accedian1Gpage_panel = "@xpath=//div[contains(text(),'Select Accedian-1G Circuit')]";

		public static final String addCircuit_Atricapage_Panel = "@xpath=//div[text()='Select Atrica Circuit']";

		public static final String addOverture_serviceNameTextField = "@xpath=//label[text()='Service Name']/parent::div//input";

		public static final String addOverture_searchButton = "@xpath=//span[text()='Search']";

		public static final String selectValueUnderAddOverturePage = "@xpath=(//div[text()='value'])[1]";

		public static final String addOverture_cancelButton = "@xpath=//span[text()='Cancel']";

		public static final String addOverture_interfaceInServicePanel = "@xpath=//div[label[text()='Interfaces In Service']]";

		public static final String interfaceFilterButton = "@xpath=//div[span[text()='Interface']]/preceding-sibling::span";

		public static final String interfacePage_filterText = "@xpath=//input[@id='filterText']";

		public static final String interfaceInService_selectValueUnderTable = "@xpath=(//div[text()='value']/parent::div//input)[1]";

		public static final String interfaceinService_selectEdgePointforInterface = "@xpath=(//div[text()='value']/parent::div//input)[2]";

		public static final String PAMtest_selectinterface = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//div[text()='value']";

		public static final String PAMtest_actionDropdown = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//button[text()='Action']";

		public static final String PAMTest_siteValue = "@xpath=//div[div[div[text()='PAM Test']]]//div[label[text()='Site']]//following-sibling::div";

		public static final String PAMTest_deleteButton = "@xpath=//div[div[@class='col-12 col-sm-12 col-md-5']]//following-sibling::div//span[text()='Delete']";

		public static final String deleteCircuit_deleteButton = "@xpath=//button[@class='btn btn-danger']";

		public static final String fetchProActiveMonitoringValue = "@xpath=//div[label[text()='Proactive Monitoring']]//following-sibling::div";
		public static final String addSiteOrderpanelHeader = "@xpath=//p[text()='Add Site Order']";
		// public static final String !-- Manage User Portal --="@xpath=

		public static final String portalAccess_ActionDropdown = "@xpath=//div[div[label[text()='Portal Access']]]//following-sibling::div//button[text()='Action']";

		public static final String portalAccess_addLink = "@xpath=//a[text()='Add']";

		public static final String portalAccess_addUserPopup = "@xpath=//div[div[div[text()='Add User']]]";

		public static final String portalAccess_addUser_userNameField = "@xpath=//div[div[div[text()='Add User']]]//div[label[text()='User Name']]//input";

		public static final String portalAccess_addUser_passwordField = "@xpath=//div[div[div[text()='Add User']]]//div[label[text()='Password']]//input";

		public static final String portalAccess_addUser_generatePasswordLink = "@xpath=//div[div[div[text()='Add User']]]//span[text()='Generate Password']";

	}

}
