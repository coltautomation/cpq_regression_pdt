
package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.Lanlink_OLO;

public class TS_29_Lanlink_OLO_Test extends SeleniumUtils {

	APT_Login Login = new APT_Login();
	Lanlink_OLO OLO = new Lanlink_OLO();
	APT_IPTransit IPTransit = new APT_IPTransit();
	Properties prop = new Properties();

	public String deviceName_Equip = null;
	public String devicename_IntEquipment = null;
	public String siteOrderValue = null;
	String devicename_intEquip = null;
	String manageAdres_intEquip = null;
	String ServiceID = null;
	String devicename = null;
	String sitePreferenceType = "Null";

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void LANLINK_OLO(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName) throws Exception {

		Report.createTestCaseReportHeader(ScenarioName);

		try (InputStream input = new FileInputStream(g.getRelativePath() + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"existingCustomerSelection");
			String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"AddInterface_Selection");
			String deviceCreation_IntermediateEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,dataSetNo, "deviceCreation_IntermediateEquipment");
			String speed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
			String ModularMSp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");
			String existingDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IntEquip_existingdeviceSelection");
			String newDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IntEquip_newdeviceSelection");
			String CustomerName1 = null;
			CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"existingCustomer");
			String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"serviceNumber");
			String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Edit_serviceNumber");
			String devicename_IntEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"devicename_IntEquipment");
			String Interfacename_forEditInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,dataSetNo, "Interfacename_forEditInterface");
			String Siteordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Siteordernumber");
			String siteOrderNumber_10G_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,dataSetNo, "siteOrderNumber_10G_PointToPoint");
			String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"siteOrderNumber_PointToPoint");
			String siteOrderNumber_p2p_mspSelected = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,dataSetNo, "siteOrderNumber_p2p_mspSelected");
			String ServiceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
			String Modularmsp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");
			String breadCrumpLink = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"breadCrumpLink");
			String EDIT_Intequip_cpe_deviecname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"EDIT_Intequip_cpe_deviecname");
			String device_intEquip_name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"device_intEquip_name");
			String intEquip_existingDeviceValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"intEquip_existingDeviceValue");

			String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"RemoveInterface_Selection");
			String siteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"siteOrder_sitePreferenceType");
			String editSiteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,dataSetNo, "editSiteOrder_sitePreferenceType");
			String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Interfacespeed");
			String Technologyname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"technology");
			String commandIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CommandIPv4_Routertool");
			
			String Equip_existingDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "Equip_existingDevicename");
			String devicename_equip = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"devicename_equip");
			String EDIT_cpename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"EDIT_cpename");
			// String manageAdres_intEquip=DataMiner.fngetcolvalue(testDataFile,
			// dataSheet, scriptNo, dataSetNo,"manageAdres_intEquip");
		
			
			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {

				// CreateCustomer - IP Transit
				OLO.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

				// selectExistingCustomer - IP Transit
				OLO.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {

				// selectExistingCustomer - IP Transit
				OLO.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

			}
			
			
			OLO.createorderservice(testDataFile, dataSheet, scriptNo,dataSetNo);
			OLO.selectCustomertocreateOrderfromleftpane(CustomerName1);
			OLO.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			OLO.selectServiceType(ServiceType);

			// selectTheServiceType_LanlinkOLO
		OLO.selectsubtypeunderServiceTypeSelected(testDataFile,
			 dataSheet,scriptNo,dataSetNo);

			// verifyFields_creatService_OLO
			OLO.VerifyFieldsForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// enterData_createSerice_OLO
			OLO.selectOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			OLO.selectServiceType(ServiceType);
			 OLO.selectsubtypeunderServiceTypeSelected(testDataFile,
					 dataSheet,scriptNo,dataSetNo);
			OLO.dataToBeEnteredOncesubservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);
			OLO.verifysuccessmessage("Service successfully created.");

			
			// verifyOrderDetailsInformation_LanlinkOLO
			OLO.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			OLO.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyDateEnteredForCreatingService_LanlinkOLO
			OLO.VerifydatenteredForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// editServiceSubtype_LanlinkOLO
			OLO.EditTheservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// successmessageforServiceUpdation_LanlinkOLO
			OLO.verifysuccessmessage("Service successfully updated.");

			String proactiveMonitorvalue = OLO.fetchProActiveMonitoringValue();

			// synchronize_LanlinkOLO
			OLO.syncservices();

			
			// ManageSubnets_LanlinkOLO
			OLO.manageSubnets();

			// Dump_LanlinkOLO
			OLO.dump_viewServicepage();

			/**
			 * Site Order
			 */
			// verifyFieldsForAddSiteOrder_LANLINKolo
			String vpnTopology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vpnTopology");
			String circuitType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CircuitType");

			if ((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit"))) {

				// "Site order' Page will not display, if we select 'VPN
				// TOpology' as 'Point-to-Point' "
			Report.LogInfo("Info",
						"Site order' Page will not display, if we select 'VPN TOpology' as 'Point-to-Point' "
								+ "and 'Circuit Type' as 'Composite Circuit' ","Info");

			} else {
				// Verifying 'Add Site Order' Fields;
				OLO.Enteraddsiteorder();
				OLO.verifyAddsiteorderFields(testDataFile, dataSheet, scriptNo, dataSetNo);
				

				// AddSiteOrder_LANLINKolo
				// create Site Order
		
		//		OLO.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				OLO.Enteraddsiteorder();
				OLO.addsiteorder(testDataFile, dataSheet, scriptNo, dataSetNo);

				// SuccessmessageforSiteOrderCreation_LanlinkOLO
				OLO.verifysuccessmessage("Site order created successfully");

				// verifyEnteredValuesForAddSiteOrder_LanlinkOLO
				OLO.VerifyDataEnteredForSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

				// EditSiteOrder_LanlinkOLO
				OLO.returnbacktoviewsiteorderpage();
				

				OLO.editSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				OLO.verifysuccessmessage("Site Order successfully updated.");
				
			
				
			// For Device, Circuit creation
			
			if ((editSiteOrder_sitePreferenceType).equalsIgnoreCase("Null")) {
				sitePreferenceType = siteOrder_sitePreferenceType;
			} else {
				sitePreferenceType = editSiteOrder_sitePreferenceType;
			}


			if (((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit")))
					|| ((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Extended Circuit"))
							&& (sitePreferenceType.equalsIgnoreCase("Circuit")))) {

				if ((Edit_serviceNumber).equalsIgnoreCase("null")) {
					ServiceID = serviceNumber;
				} else {
					ServiceID = Edit_serviceNumber;
				}

				if ((Interfacespeed).equals("1GigE")) {

					// Overture
					// CircuitCreationFor1G_Overture_LanlinkOLO
					OLO.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.verifysuccessmessage("Circuit successfully created");
					OLO.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					OLO.deleteCircuit();

					// Accedian-1G
					// CircuitCreationFor1G_Accedian-1G_LanlinkOLO
					OLO.addAccedianCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.verifysuccessmessage("Circuit successfully created");
					OLO.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					OLO.deleteCircuit();

					// Atrica
					// "CircuitCreationFor1G_Atrica_LanlinkOLO");
					OLO.addAtricaCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.verifysuccessmessage("Circuit successfully created");
					OLO.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					OLO.deleteCircuit();

				} else if ((Interfacespeed).equals("10GigE")) {
					// createCircuitFor10G_LanlinkOLO
					// Overture
					OLO.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.verifysuccessmessage("Circuit successfully created");
					OLO.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					OLO.deleteCircuit();

				}
			} else {
				// Actelis_EquipmentConfiguration_LanlinkOLO");
				
				if (Technologyname.equalsIgnoreCase("Actelis")) {

					boolean equipConfigurationPanel = OLO.EquipmentCOnfigurationPanel();
					if (equipConfigurationPanel) {
						//verify add CPE Device
						OLO.equipConfiguration_Actelis_AddDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
						OLO.verifysuccessmessage("Device successfully created");

						// verifyDataEnteredForActelis_LanlinkOLO");
						OLO.verifyDataEnteredFordeviceCreation_Actelis(testDataFile, dataSheet, scriptNo, dataSetNo);
						OLO.returnbacktoviewsiteorderpage();

						// deleteActelisEquipmentConfigurationDevice_LanlinkOLO
						OLO.deleteDeviceFromService_EquipmentConfig_Actelis();
						OLO.verifysuccessmessage("Actelis CPE Device successfully deleted and removed from service");

					} else {
						// Equipment Configuration' panel is not displaying

					}

					// Actelis Configuration panel
					// addDSLAMandHSL_LanlinkOLO
					OLO.verifyAddDSLAMandHSLlink();
					OLO.AddDSLAMandHSL(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.showInterface_ActelisConfiguuration();
					OLO.deletInterface_ActelisConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					OLO.successMessage_deleteInterfaceFromDevice_ActelisConfiguration();

				} else {
					Report.LogInfo("Info", "Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page","Info");

					Reporter.log(
							"Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page");

				}

				/**
				 * Device Creation Equipment
				 */
				String deviceCreation_Equipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"deviceCreation_Equipment");

				// AdddeviceforEquipment_LanlinkOLO
				// verify whether Equipment panel is available
				boolean EquipmentPanel = OLO.findPanelHeader("Equipment");
				if (EquipmentPanel) {
					// Verify whether Equipment device to be created
					if ((deviceCreation_Equipment).equalsIgnoreCase("yes")) {
						// " Device to be created for Eqiupment as per input
						// provided");
						// "Under Equipement, list of actions to be performed

						if (ModularMSp.equalsIgnoreCase("Yes")) {
							if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								// "selectExistingDevice_MSPselected_Equipment");
								OLO.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
										dataSheet, scriptNo, dataSetNo, speed);
								OLO.verifysuccessmessage("Device successfully created");
								OLO.verifyValuesforCPEexistingdevice_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								// editExistingDevice_MSPselected_Equipment_LanlinkOLO");
								OLO.eDITCPEdevicedetailsentered_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								OLO.verifysuccessmessage("Device successfully updated");

							} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
								// "addNewDevice_MSPselected_Equipment");
								OLO.verifyFieldsandAddCPEdevicefortheserviceselected_MSPselected(testDataFile,
										dataSheet, scriptNo, dataSetNo);
								OLO.verifysuccessmessage("Device successfully created");

								// verifyEnteredValues_MSPselected_Equipment_LanlinkOLO");
								OLO.verifydetailsEnteredforCPEdevice_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								// editNewDevice_MSPselected_Equipment_LanlinkOLO");
								OLO.eDITCPEdevicedetailsentered_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								OLO.verifysuccessmessage("Device successfully updated");

							}
						} else {
							if (speed.equals("1GigE")) {
								if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
									// "selectExistingDevice_1G_Equipment_LanlinkOLO");
									OLO.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
											dataSheet, scriptNo, dataSetNo, speed);
									OLO.verifysuccessmessage("Device successfully created");
									OLO.verifyValuesforCPEexistingdevice_1G_Equipment(testDataFile, dataSheet, scriptNo,
											dataSetNo);

									// editExistingDevice_1G_Equipment_LanlinkOLO");
									OLO.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
									
									// "addNewDevice_1G_Equipment_LanlinkOLO");
									OLO.verifyFieldsandAddCPEdevicefortheserviceselected_1G(testDataFile, dataSheet,
											scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_1G_Equipment_LanlinkOLO");
									
									OLO.verifydetailsEnteredforCPEdevice_1G(testDataFile, dataSheet, scriptNo,
											dataSetNo);

									// editDevice_1G_Equipment_LanlinkOLO");
									OLO.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								}
							}
							if (speed.equals("10GigE")) {

								if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
									// "selectExistingDevice_10G_Equipment_LanlinkOLO");
									OLO.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
											dataSheet, scriptNo, dataSetNo, speed);
									OLO.verifysuccessmessage("Device successfully created");
									OLO.verifyValuesforCPEexistingdevice_10G_Equipment(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editExistingDevice_10G_Eqiupment_LanlinkOLO");
									OLO.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
									// "addNewDevice_10G_Equipment");
									OLO.verifyFieldsandAddCPEdevicefortheserviceselected_10G(testDataFile, dataSheet,
											scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_10G_Equipment_LanlinkOLO");
									OLO.verifydetailsEnteredforCPEdevice_10G(testDataFile, dataSheet, scriptNo,
											dataSetNo);

									// editDevice_10G_Equipment_LanlinkOLO");
									OLO.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								}
							}
						}
						

						

						// get Device name
						if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							devicename = Equip_existingDevicename;
						} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
							if ((EDIT_cpename).equalsIgnoreCase("null")) {
								devicename = devicename_equip;
							} else if ((EDIT_cpename).equalsIgnoreCase("null")) {
								devicename = EDIT_cpename;
							}
						}

						// Navigate to view device page
						OLO.Equip_clickonviewButton(devicename);

						// devicename
						String devicename_EquipActual = null;
						devicename = OLO.fetchdevicename_InviewPage();
						if (devicename.contains("...")) {
							devicename_EquipActual = devicename.substring(0, 12);
						} else {
							devicename_EquipActual = devicename;
						}

						deviceName_Equip = devicename_EquipActual;

						String manageAdres = OLO.fetchManagementAddressValue_InviewPage(); // Management
																							// Address
						String vendorModel = OLO.fetchVendorModelValue(); // vendor/Model

						// Perform fetch from Interface
						// "fetchDeviceInterface_LanlinkOLO");
						//OLO.testStatus();
						boolean link = OLO.fetchDeviceInterface_viewdevicepage();
						waitForAjax();

						// routerTools_Equipment");
						OLO.routerPanel(commandIPv4,manageAdres);

						// configureInterface_Equipment_LanlinkOLO");
						// Site Order Number
						String siteOrderNumber = null;

						if ((vpnTopology).equals("Point-to-Point")) {
							if ((Modularmsp).equalsIgnoreCase("Yes")) {
								siteOrderNumber = siteOrderNumber_p2p_mspSelected;
							} else {
								if ((Interfacespeed).equalsIgnoreCase("1GigE")) {
									siteOrderNumber = siteOrderNumber_PointToPoint;
								} else if ((Interfacespeed).equalsIgnoreCase("10GigE")) {
									siteOrderNumber = siteOrderNumber_10G_PointToPoint;
								}
							}
						} else {
							siteOrderNumber = Siteordernumber;
						}

						siteOrderValue = siteOrderNumber;
						if (ModularMSp.equalsIgnoreCase("Yes")) {
							// "Configure link do not display, if 'MSP'
							// selected");

						} else {
							
							
							OLO.clickOnBreadCrump(siteOrderNumber);
							OLO.selectconfigurelinkAndverifyEditInterfacefield__Equipment(devicename_equip);
							String interfaceavailability_configure = OLO.EnterdataForEditInterfaceforConfigurelinkunderIntermediateEquipment(testDataFile,
											dataSheet, scriptNo, dataSetNo);
							if (interfaceavailability_configure.equalsIgnoreCase("Yes")) {
								OLO.verifyeditedinterfacevalue(testDataFile, dataSheet, scriptNo, dataSetNo);
							}

						}

						// selectInterface_Equipment_LanlinkOLO");
						OLO.clickOnBreadCrump(siteOrderValue);
						OLO.selectInterfacelinkforEqipment(devicename_equip);

						if ((RemoveInterface_Selection).equalsIgnoreCase("yes")) {
							OLO.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed", "PASS");
						}

						if ((AddInterface_Selection).equalsIgnoreCase("yes")) {
							OLO.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed", "PASS");
						}

						// showInterface_Equipment_LanlinkOLO");
						OLO.clickOnBreadCrump(siteOrderValue);
						String interfaceAvailability = OLO.SelectShowInterfacelinkAndVerifyEditInterfacePage(
								testDataFile, dataSheet, scriptNo, dataSetNo,devicename_EquipActual);
						if ((interfaceAvailability).equalsIgnoreCase("Yes")) {
							OLO.EnterdataForEditInterfaceforShowInterfacelinkunderEquipment(testDataFile, dataSheet,
									scriptNo, dataSetNo);
						//	OLO.hideInterfaceLink_Equipment();
						}

						// AMNvalidator_Equipment_LanlinkOLO");
						if (proactiveMonitorvalue.equalsIgnoreCase("Yes")) {          
							OLO.clickOnBreadCrump(breadCrumpLink);
							String csrName = OLO.fetchCSRsiteName();
							String cityName = OLO.fetchDeviceCityName();
							String countryName = OLO.fetchSiteOrderCountryName();
							OLO.clickOnAMNvalidatorLink();
							OLO.AMNvalidator(siteOrderNumber , deviceName_Equip, csrName, cityName, countryName);

						} else {
							Report.LogInfo("Info", "'AMN Validator' link do not display, as 'proactive Monitoring' checkbox is not selected ","Info");


						}
						

						// deleteDevice_Equipment_LanlinkOLO");
						OLO.clickOnBreadCrump(siteOrderValue);
						OLO.deleteDeviceFromServiceForequipment(devicename_equip);
						OLO.successMessage_deleteFromService();

					} else {
						Report.LogInfo("Info", "Equipment device is not created as expected","PASS");
						Reporter.log("Equipment device is not created as expected");

					}
				} else {
					Report.LogInfo("Info", " 'Equipment' panel is not displaying under 'view site order' page","Info");

					Reporter.log(" 'Equipment' panel is not displaying under 'view site order' page");

				}

				/**
				 * Device Creation_Intermediate Equipment
				 */
				// IntermediateEquipment_LanlinkOLO");
				boolean IntermediateEquipmentPanel = OLO.findPanelHeader("Intermediate Equipment");
				if (IntermediateEquipmentPanel) {
					if ((deviceCreation_IntermediateEquipment).equalsIgnoreCase("yes")) {
						Report.LogInfo("Info", " Device to be created for Intermediate Eqiupment as per input provided","Info");	


						if (ModularMSp.equalsIgnoreCase("yes")) {
							if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								// "selectExistingDevice_MSPselected_IntermediateEquipment_LanlinkOLO");
								OLO.addDevice_IntEquipment();
								OLO.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								OLO.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(
										intEquip_existingDeviceValue);
								OLO.verifyValuesforCPEexistingdevice_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								// editExistingDevice_MSPselected_IntermediateEquipment_LanlinkOLO");
								OLO.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet,
										scriptNo, dataSetNo);
								OLO.verifysuccessmessage("Device successfully updated");

							} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
								// "addNewDevice_MSPselected_IntermediateEquipment_LanlinkOLO");
								OLO.addDevice_IntEquipment();
								OLO.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								OLO.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_MSPselected(testDataFile,
										dataSheet, scriptNo, dataSetNo);
								OLO.verifysuccessmessage("Device successfully created");

								// verifyEnteredValues_MSPselected_IntermediateEquipment_LanlinkOLO");
								OLO.verifyCPEdevicedataenteredForIntermediateEquipment_MSPselected(testDataFile,
										dataSheet, scriptNo, dataSetNo);

								// editNewDevice_MSPselected_IntermediateEquipment_LanlinkOLO");
								OLO.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet,
										scriptNo, dataSetNo);
								OLO.verifysuccessmessage("Device successfully updated");

							}
						} else {
							if (speed.equals("1GigE")) {
								if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
									// "selectExistingDevice_1G_IntermediateEquipment_LanlinkOLO");
									OLO.addDevice_IntEquipment();
									OLO.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									OLO.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(
											intEquip_existingDeviceValue);
									OLO.verifyValuesforCPEexistingdevice_1G_intEquipment(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editExistingDevice_1G_IntermediateEquipment_LanlinkOLO");
									OLO.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
									// addNewDevice_1G_IntermediateEqiupment_LanlinkOLO
									
									OLO.addDevice_IntEquipment();
									OLO.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									
									OLO.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(testDataFile,
									 dataSheet, scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_1G_IntermediateEquipment_LanlinkOLO
									OLO.verifyCPEdevicedataenteredForIntermediateEquipment_1G(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editNewDevice_1G_IntermediateEquipment_LanlinkOLO
									OLO.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								}
							} else if (speed.equals("10GigE")) {
								if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
									// "selectExistingDevice_10G_IntermediateEquipment_LanlinkOLO"
									// );
									OLO.addDevice_IntEquipment();
									OLO.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									OLO.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(
											intEquip_existingDeviceValue);
									OLO.verifyValuesforCPEexistingdevice_10G_intEquipment(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editExistingDevice_10G_IntermediateEquipment_LanlinkOLO");
									OLO.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
									// "addNewDevice_10G_IntermediateEquipment_LanlinkOLO");
									
									OLO.addDevice_IntEquipment();
									OLO.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									OLO.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(testDataFile,
											dataSheet, scriptNo, dataSetNo);
									OLO.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_10G_IntermediateEquipment_LanlinkOLO");
									OLO.verifyCPEdevicedataenteredForIntermediateEquipment_10G(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editNewDevice_10G_IntermediateEquipment_LanlinkOLO
									OLO.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									OLO.verifysuccessmessage("Device successfully updated");

								}
							}
						}

						// Get Device Name
						if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							devicename_intEquip = intEquip_existingDeviceValue;
						} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
							if ((EDIT_Intequip_cpe_deviecname).equalsIgnoreCase("null")) {
								devicename_intEquip = device_intEquip_name;
							} else if (!(EDIT_Intequip_cpe_deviecname).equalsIgnoreCase("null")) {
								devicename_intEquip = EDIT_Intequip_cpe_deviecname;
							}
						}

						// Navigate to view device page
						
						 OLO.IntEquip_clickonviewButton(devicename_intEquip);

						// devicename
						String devicename_intEquipActual = null;
						devicename_intEquip = OLO.fetchdevicename_InviewPage();
						if (devicename_intEquip.contains("...")) {
							devicename_intEquipActual = devicename_intEquip.substring(0, 13);
						} else {
							devicename_intEquipActual = devicename_intEquip;
						}

						devicename_IntEquipment = devicename_intEquipActual;

						manageAdres_intEquip = OLO.fetchManagementAddressValue_InviewPage(); // Management
																								// Address
						String vendorModel_intEquip = OLO.fetchVendorModelValue(); // Vendor/model
						String country_intEquip = OLO.fetchCountryValue_InviewPage(); // Country

						// Fetch device Interface
						// "FetchDeviceInterface_LanlinkOLO");
						//OLO.testStatus();
						boolean link = OLO.fetchDeviceInterface_viewdevicepage();
						waitForAjax();

						// routerTools_intermediateEquipment_LanlinkOLO");
						OLO.routerPanel(commandIPv4,manageAdres_intEquip);

						// selectinterface_IntermediateEquipment_LanlinkOLO");
						// Site Order Number
						String siteOrderNumber = null;
						if ((vpnTopology).equals("Point-to-Point")) {
							if ((Modularmsp).equalsIgnoreCase("Yes")) {
								siteOrderNumber = siteOrderNumber_p2p_mspSelected;
							} else {
								if ((Interfacespeed).equalsIgnoreCase("1GigE")) {
									siteOrderNumber = siteOrderNumber_PointToPoint;
								} else if ((Interfacespeed).equalsIgnoreCase("10GigE")) {
									siteOrderNumber = siteOrderNumber_10G_PointToPoint;
								}
							}
						} else {
							siteOrderNumber = Siteordernumber;
						}

						OLO.clickOnBreadCrump(siteOrderNumber);
						OLO.selectInterfacelinkforIntermediateEqipment(devicename_IntEquipment);
						if ((RemoveInterface_Selection).equalsIgnoreCase("yes")) {
							OLO.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							Reporter.log("interfaces are not removed");
						}
						if ((AddInterface_Selection).equalsIgnoreCase("yes")) {
							OLO.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
						}

						// showInterface_IntermediateEquipment_LanlinkOLO");
						OLO.clickOnBreadCrump(siteOrderNumber);
						OLO.SelectShowInterfacelink_IntermediateequipmentAndVerifyEditInterfacePage(
								Interfacename_forEditInterface, devicename_IntEquipment);
						OLO.EnterdataForEditInterfaceforShowInterfacelinkunderIntermediateEquipment(testDataFile,
								dataSheet, scriptNo, dataSetNo);
					
					}
				} else {
					Reporter.log(" 'Intermediate Equipment' panel is not displaying under 'view site order' page");

				}

				// PAMTest_LanlinkOLO

				if ((Edit_serviceNumber).equalsIgnoreCase("null")) {
					ServiceID = serviceNumber;
				} else {
					ServiceID = Edit_serviceNumber;
				}
				OLO.returnbacktoviewsiteorderpage();
				
				OLO.pamTest(ServiceID);

				
			}

			if ((Edit_serviceNumber).equalsIgnoreCase("null")) {
				ServiceID = serviceNumber;
			} else {

				ServiceID = Edit_serviceNumber;
			}
			
		
			}
		}

		catch (

		Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

		}

		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}
}