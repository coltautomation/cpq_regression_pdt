package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.Lanlink_International;

public class TS_36_Lanlink_InternationalTest extends SeleniumUtils{
	
	Lanlink_International LanlinkInternational = new Lanlink_International();
	APT_Login Login = new APT_Login();
	
	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-01", priority = 1)
	public void LANLINK_International(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomerSelection");

			
				if(newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {  
						
			
					LanlinkInternational.CreateCustomer(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						
						String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
						
						LanlinkInternational.selectCustomertocreateOrder(CustomerName1);
						
						
				}
				else if(newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
						
					String CustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

					LanlinkInternational.selectCustomertocreateOrder(CustomerName);
					
						String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

				}
		

				
				LanlinkInternational.Verifyfields(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");
					LanlinkInternational.selectCustomertocreateOrderfromleftpane(CustomerName1);
					
					LanlinkInternational.createorderservice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					LanlinkInternational.selectServiceType(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
	 
					LanlinkInternational.selectsubtypeunderServiceTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					LanlinkInternational.VerifyFieldsForServiceSubTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
			
					LanlinkInternational.selectOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					LanlinkInternational.selectServiceType(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					LanlinkInternational.selectsubtypeunderServiceTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					LanlinkInternational.dataToBeEnteredOncesubservicesselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					LanlinkInternational.verifysuccessmessage( "Service successfully created");
			 
			
					LanlinkInternational.verifyorderpanel_editorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					LanlinkInternational.verifyorderpanel_changeorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					LanlinkInternational.VerifydatenteredForServiceSubTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					LanlinkInternational.EditTheservicesselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					LanlinkInternational.verifysuccessmessage("Service successfully updated.");
					
			     	LanlinkInternational.syncservices();	
					
					LanlinkInternational.manageSubnets();
				
					LanlinkInternational.dump_viewServicepage();
					
				
					
					
					// Site Order	
					
					
	
							
							String vpnTopology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vpnTopology");
							String circuitType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CircuitType");

						
					if((vpnTopology.equals("Point-to-Point")) &&  (circuitType.equals("Composite Circuit"))){
						
							ExtentTestManager.getTest().log(LogStatus.INFO, "Site order' Page will not display, if we select 'VPN TOpology' as 'Point-to-Point' "
									+ "and 'Circuit Type' as 'Composite Circuit' ");
							ExtentTestManager.endTest();
						
					}else {   
						ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying fields for 'Add Site Order' page");
						LanlinkInternational.Enteraddsiteorder();
						LanlinkInternational.verifyAddsiteorderFields(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						LanlinkInternational.Enteraddsiteorder();
						
						
						LanlinkInternational.addsiteorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						
						LanlinkInternational.verifysuccessmessage("Site order created successfully");
						
						LanlinkInternational.VerifyDataEnteredForSiteOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						
						LanlinkInternational.returnbacktoviewsiteorderpage();
						LanlinkInternational.editSiteOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						LanlinkInternational.verifysuccessmessage("Site Order successfully updated.");
						
					}
					
					
				//For device, Circuit creation
					String sitePreferenceType = "Null";
					String editSiteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editSiteOrder_sitePreferenceType");
					String siteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrder_sitePreferenceType");

					if((editSiteOrder_sitePreferenceType).equalsIgnoreCase("Null")) {
						sitePreferenceType = siteOrder_sitePreferenceType;
					}
					else {
						sitePreferenceType = editSiteOrder_sitePreferenceType;
					}
					
					if(((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit")))  ||  ((vpnTopology.equals("Point-to-Point")) &&  (circuitType.equals("Extended Circuit"))  &&  (sitePreferenceType.equalsIgnoreCase("Circuit")))){
						
						String ServiceID = null;
						String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_serviceNumber");
						String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "serviceNumber");

						if((Edit_serviceNumber).equalsIgnoreCase("null")) {
							ServiceID=serviceNumber;
						}else {
							ServiceID=Edit_serviceNumber;
						}
						String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");

						if((Interfacespeed).equals("1GigE")) {
							
							//Overture
							LanlinkInternational.addOvertureCircuit(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.selectInterfaceForCircuits(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.verifysuccessmessage("Circuit successfully created");
							LanlinkInternational.addOveture_PAMtest_selectRow(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.PAMtest_ForCircuitCreation(testDataFile,  dataSheet,  scriptNo,  dataSetNo,ServiceID);
							LanlinkInternational.deleteCircuit();
								
							//Accedian-1G
							LanlinkInternational.addAccedianCircuit(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.selectInterfaceForCircuits(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.verifysuccessmessage("Circuit successfully created");
							LanlinkInternational.addOveture_PAMtest_selectRow(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.PAMtest_ForCircuitCreation(testDataFile,  dataSheet,  scriptNo,  dataSetNo,ServiceID);
							LanlinkInternational.deleteCircuit();
								
							//Atrica
							LanlinkInternational.addAtricaCircuit(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.selectInterfaceForCircuits(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.verifysuccessmessage("Circuit successfully created");
							LanlinkInternational.addOveture_PAMtest_selectRow(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.PAMtest_ForCircuitCreation(testDataFile,  dataSheet,  scriptNo,  dataSetNo,ServiceID);
							LanlinkInternational.deleteCircuit();
							
							}
						

							else if((Interfacespeed).equals("10GigE")) {
								
								//Overture	
								LanlinkInternational.addOvertureCircuit(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								LanlinkInternational.selectInterfaceForCircuits(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								LanlinkInternational.verifysuccessmessage( "Circuit successfully created");
								LanlinkInternational.addOveture_PAMtest_selectRow(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								LanlinkInternational.PAMtest_ForCircuitCreation(testDataFile,  dataSheet,  scriptNo,  dataSetNo,ServiceID);
								LanlinkInternational.deleteCircuit();
							}
					}
					else {
						
						String technology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "technology");
						String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");

						String Technologyname=technology;
						if(Technologyname.equalsIgnoreCase("Actelis")) {
						boolean equipConfigurationPanel=LanlinkInternational.EquipmentCOnfigurationPanel();
							if(equipConfigurationPanel) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "verify 'Add CPE Device'");
								LanlinkInternational.equipConfiguration_Actelis_AddDevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								LanlinkInternational.verifysuccessmessage("Device successfully created");
								
								LanlinkInternational.verifyDataEnteredFordeviceCreation_Actelis(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								LanlinkInternational.returnbacktoviewsiteorderpage();
								
								LanlinkInternational.deleteDeviceFromService_EquipmentConfig_Actelis();
								LanlinkInternational.verifysuccessmessage("Actelis CPE Device successfully deleted and removed from service");
							}else {
								ExtentTestManager.getTest().log(LogStatus.FAIL, "'Equipment Configuration' panel is not displaying");
							}
						
					//Actelis Configuration panel
							LanlinkInternational.verifyAddDSLAMandHSLlink();
							LanlinkInternational.AddDSLAMandHSL();
							LanlinkInternational.showInterface_ActelisConfiguuration();
							LanlinkInternational.deletInterface_ActelisConfiguration(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							LanlinkInternational.successMessage_deleteInterfaceFromDevice_ActelisConfiguration();
						}else {
							ExtentTestManager.getTest().log(LogStatus.INFO, "Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page");
							Reporter.log("Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page");
						}
						
						String devicename=null;  String vendorModel=null;
						String manageAdres=null;  String country="";
						String siteOrderNumber=null;
						String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");
						String siteOrderNumber_10G_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrderNumber_10G_PointToPoint");

						if((vpnTopology).equals("Point-to-Point")) {
							if((Interfacespeed).equalsIgnoreCase("1GigE")) {
								siteOrderNumber=siteOrderNumber_PointToPoint;
							}
							else if((Interfacespeed).equalsIgnoreCase("10GigE")) {
								siteOrderNumber=siteOrderNumber_10G_PointToPoint;
							}
							
						}else {
							String Siteordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Siteordernumber");

							siteOrderNumber=Siteordernumber;
						}
						
						 /// Equipment device creation
						 
					//verify whether Equipment panel is available
						
					//	LanlinkInternational.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
						
					//	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
					//	waitForAjax();
					//	webDriver.findElement(By.xpath("//tr//*[contains(.,'"+siteOrderNumber_PointToPoint+"')]//following-sibling::td//a[text()='View']")).click();

						boolean EquipmentPanel=LanlinkInternational.findPanelHeader("Equipment");
						if(EquipmentPanel) 
						{    
							
					
							
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Equipment' panel is displaying under 'view site order' page as expected");
					//Verify whether Equipment device to be created
							String deviceCreation_Equipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "deviceCreation_Equipment");

							if((deviceCreation_Equipment).equalsIgnoreCase("yes")){
								
								ExtentTestManager.getTest().log(LogStatus.INFO, " Device to be created for Eqiupment as per input provided");	

						ExtentTestManager.getTest().log(LogStatus.INFO, "Under Equipement, list of actions to be performed are: "
								+ "Verify fields for Add device"
								+ "Add device"
								+ "Verify entered values for device"
								+ "Edit device"
								+ "Select Interface"
								+ "Configure Interface -- Edit Inteface"
								+ "show/Hide Interface -- Edit Interface"
								+ "Select Interface -- Add Interface to service , Remove Interface from Service"
								+ "Delete device ");
						
						System.out.println("------For Equipment ..........Entering add cpe device, Verify CPE device, Edit CPE device----------");
						//String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
						String Equip_ExistingDeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Equip_ExistingDeviceSelection");
						String Equip_NewDeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "deviceCreation_Equipment");
						String ModularMSp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");

						    String speed=Interfacespeed;
							String existingDevice=Equip_ExistingDeviceSelection;
							String newDevice=Equip_NewDeviceSelection;
										
						
							if(ModularMSp.equalsIgnoreCase("Yes")) 
							{
								if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
									ExtentTestManager.getTest().log(LogStatus.INFO, "AddExistingDevice_1G_Eqiupment");
									LanlinkInternational.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo,speed);
									LanlinkInternational.verifysuccessmessage("Device successfully created");
									LanlinkInternational.verifyValuesforCPEexistingdevice_MSPselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									
									LanlinkInternational.eDITCPEdevicedetailsentered_MSPselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage( "Device successfully updated");
									ExtentTestManager.endTest();
								}
								else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
									ExtentTestManager.getTest().log(LogStatus.INFO, "addNewDevice_MSPselected_Equipment_LanlinkInternational");
									LanlinkInternational.verifyFieldsandAddCPEdevicefortheserviceselected_MSPselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully created");
									
									
									LanlinkInternational.verifydetailsEnteredforCPEdevice_MSPselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									
									LanlinkInternational.eDITCPEdevicedetailsentered_MSPselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully updated");
								}
							}
							else
							{
								if(speed.equals("1GigE")) {
									if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
										ExtentTestManager.getTest().log(LogStatus.INFO, "selectExistingDevice_1G_Equipment");
										LanlinkInternational.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo, speed);
										LanlinkInternational.verifysuccessmessage("Device successfully created");
									
									LanlinkInternational.verifyValuesforCPEexistingdevice_1G_Equipment();
									
									LanlinkInternational.eDITCPEdevicedetailsentered_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully updated");
								}
								else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
									ExtentTestManager.getTest().log(LogStatus.INFO, "addNewDevice_1G_Equipment");
									LanlinkInternational.verifyFieldsandAddCPEdevicefortheserviceselected_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully created");
									
									LanlinkInternational.verifydetailsEnteredforCPEdevice_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									
									LanlinkInternational.eDITCPEdevicedetailsentered_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully updated");
								}
								
							} if(speed.equals("10GigE")) {
								
								if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
									ExtentTestManager.getTest().log(LogStatus.INFO, "addExistingDevice_10G_Eqiupment");
									LanlinkInternational.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo,speed);
									LanlinkInternational.verifysuccessmessage("Device successfully created");
									
									LanlinkInternational.verifyValuesforCPEexistingdevice_10G_Equipment(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									
									LanlinkInternational.eDITCPEdevicedetailsentered_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully updated");
								}
									
								else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
									ExtentTestManager.getTest().log(LogStatus.INFO, "addNewDevice_10G_Equipment");
									LanlinkInternational.verifyFieldsandAddCPEdevicefortheserviceselected_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully created");
									
									LanlinkInternational.verifydetailsEnteredforCPEdevice_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									
									LanlinkInternational.eDITCPEdevicedetailsentered_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
									LanlinkInternational.verifysuccessmessage("Device successfully updated");
								}
							}	
						}
						//get Device name 	
							String Equip_existingDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Equip_existingDevicename");
							String EDIT_cpename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EDIT_cpename");
							String devicename_equip = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "devicename_equip");

							if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								devicename=Equip_existingDevicename;
							}
							else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
								if((EDIT_cpename).equalsIgnoreCase("null")) {
									devicename=devicename_equip;
								}
								else if(!(EDIT_cpename).equalsIgnoreCase("null")) {
									devicename=EDIT_cpename;
								}	
							}
							
							LanlinkInternational.Equip_clickonviewButton(devicename); //navigate to view device page
							
							//devicename
							String devicename_EquipActual=null;
							
								devicename=LanlinkInternational.fetchdevicename_InviewPage();		//fetch Device Name
								if(devicename.contains("...")) {
									devicename_EquipActual = devicename.substring(0, 10);
								}else {
									devicename_EquipActual=devicename;
								}
								
							String	deviceName_Equip=devicename_EquipActual;
								
								manageAdres=LanlinkInternational.fetchManagementAddressValue_InviewPage();	//Fetch Management Address
								vendorModel=LanlinkInternational.fetchVendorModelValue();		//Fetch vendor/Model
					
						
								LanlinkInternational.testStatus();
							boolean link=LanlinkInternational.fetchDeviceInterface_viewdevicepage(devicename);
							
							String CommandIPv4_Routertool = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CommandIPv4_Routertool");

							LanlinkInternational.routerPanel(CommandIPv4_Routertool, manageAdres);
								
							String Modularmsp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");
							String siteOrderNumber_p2p_mspSelected = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrderNumber_p2p_mspSelected");
							
							if ((vpnTopology).equals("Point-to-Point")) {
								if ((Modularmsp).equalsIgnoreCase("Yes")) {
									siteOrderNumber = siteOrderNumber_p2p_mspSelected;
								} else {
									if ((Interfacespeed).equalsIgnoreCase("1GigE")) {
										siteOrderNumber = siteOrderNumber_PointToPoint;
									} else if ((Interfacespeed).equalsIgnoreCase("10GigE")) {
										siteOrderNumber = siteOrderNumber_10G_PointToPoint;
									}
								}
								
								}else {
									String Siteordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Siteordernumber");

									siteOrderNumber = Siteordernumber;
								}
						String	siteOrderValue=siteOrderNumber;
							if(ModularMSp.equalsIgnoreCase("Yes")) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "Configure link do not display, if 'MSP' selected");
								ExtentTestManager.endTest();
							}else {
							
								LanlinkInternational.clickOnBreadCrump(siteOrderNumber);
								LanlinkInternational.selectconfigurelinkAndverifyEditInterfacefield__Equipment(devicename_EquipActual);
							String interfaceavailability_configure = LanlinkInternational.EnterdataForEditInterfaceforConfigurelinkunderIntermediateEquipment(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							if(interfaceavailability_configure.equalsIgnoreCase("Yes")) {
								LanlinkInternational.verifyeditedinterfacevalue(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							}
						}
							
							LanlinkInternational.clickOnBreadCrump(siteOrderValue);
							LanlinkInternational.selectInterfacelinkforEqipment(deviceName_Equip);
							
							String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RemoveInterface_Selection");

									if((RemoveInterface_Selection).equalsIgnoreCase("yes")) {
										LanlinkInternational.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
									}else {
										System.out.println("interfaces are not removed");
										ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed");
									}
									String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddInterface_Selection");

									if((AddInterface_Selection).equalsIgnoreCase("yes")) {
										LanlinkInternational.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
									}else {
										System.out.println("Interfaces are not added");
										ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed");
									}
									
									
									LanlinkInternational.clickOnBreadCrump(siteOrderValue);
									String Interfacename_forEditInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacename_forEditInterface");

									String interfaceAvailability = LanlinkInternational.SelectShowInterfacelinkAndVerifyEditInterfacePage(Interfacename_forEditInterface, devicename_EquipActual);
									if(interfaceAvailability.equalsIgnoreCase("Yes")) {
										LanlinkInternational.EnterdataForEditInterfaceforShowInterfacelinkunderEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
										//LanlinkInternational.hideInterfaceLink_Equipment();
									}
									String proactiveMonitorvalue = LanlinkInternational.fetchProActiveMonitoringValue();
						 if(proactiveMonitorvalue.equalsIgnoreCase("Yes")) {
							LanlinkInternational.clickOnBreadCrump(siteOrderNumber);
								String csrName=LanlinkInternational.fetchCSRsiteName();
								String cityName=LanlinkInternational.fetchDeviceCityName();
								String countryName=LanlinkInternational.fetchSiteOrderCountryName();
								LanlinkInternational.clickOnAMNvalidatorLink();
								LanlinkInternational.AMNvalidator(siteOrderNumber , deviceName_Equip, csrName, cityName, countryName);
						 }else {
							 ExtentTestManager.getTest().log(LogStatus.INFO, "'AMN Validator' link do not display, as 'proactive Monitoring' checkbox is not selected ");
							 ExtentTestManager.endTest();
						 }
						 			
									
						 LanlinkInternational.clickOnBreadCrump(siteOrderValue);
						 LanlinkInternational.deleteDeviceFromServiceForequipment(deviceName_Equip);
						 LanlinkInternational.successMessage_deleteFromService();
							
						   }else {
								ExtentTestManager.getTest().log(LogStatus.PASS, "Equipment device is not created as expected");
								System.out.println("Equipment device is not created as expected");
									}
							}else {
								ExtentTestManager.getTest().log(LogStatus.INFO, " 'Equipment' panel is not displaying under 'view site order' page");
								System.out.println(" 'Equipment' panel is not displaying under 'view site order' page");
							}
						
					
								boolean IntermediateEquipmentPanel=LanlinkInternational.findPanelHeader("Intermediate Equipment");
								if(IntermediateEquipmentPanel) {   
									ExtentTestManager.getTest().log(LogStatus.PASS, " 'Intermediate Equipment' panel is displaying under 'view site order' page as expected");
									
									String deviceCreation_IntermediateEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "deviceCreation_IntermediateEquipment");

									if((deviceCreation_IntermediateEquipment).equalsIgnoreCase("yes")){
										ExtentTestManager.getTest().log(LogStatus.INFO, " Device to be created for Intermediate Eqiupment as per input provided");	
								ExtentTestManager.getTest().log(LogStatus.INFO, "Under Intermediate Equipement, list of actions to be performed are: "
										+ "Verify fields for Add device"
										+ "Add device"
										+ "Verify entered values for device"
										+ "Edit device"
										+ "Select Interface"
										+ "show/Hide Interface -- Edit Interface"
										+ "Select Interface -- Add Interface to service , Remove Interface from Service"
										+ "Delete device ");
								String IntEquip_existingdeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IntEquip_existingdeviceSelection");
								String IntEquip_newdeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IntEquip_newdeviceSelection");
								String Modularmsp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");

								String speed=Interfacespeed;
								String ModularMSp= Modularmsp;
								String existingDevice=IntEquip_existingdeviceSelection;
								String newDevice=IntEquip_newdeviceSelection;
								
								
								if(ModularMSp.equalsIgnoreCase("yes"))  {
									if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
										ExtentTestManager.getTest().log(LogStatus.INFO, "selectExistingDevice_1G_IntermediateEquipment");
										LanlinkInternational.addDevice_IntEquipment();
										LanlinkInternational.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
										LanlinkInternational.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
										LanlinkInternational.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
										LanlinkInternational.verifysuccessmessage("Device successfully updated");
										
									}else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
										ExtentTestManager.getTest().log(LogStatus.INFO, "addNewDevice_MSPselected_IntEquipment_LanlinkInternational");
										LanlinkInternational.addDevice_IntEquipment();
										LanlinkInternational.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
										LanlinkInternational.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);	
										LanlinkInternational.verifysuccessmessage("Device successfully created");
										
										
										LanlinkInternational.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet, scriptNo, dataSetNo);
										LanlinkInternational.verifysuccessmessage("Device successfully updated");
									}
									}
								else
								{
									if(speed.equals("1GigE")) {
										if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
											ExtentTestManager.getTest().log(LogStatus.INFO, "selectExistingDevice_1G_IntEquipment");
											LanlinkInternational.addDevice_IntEquipment();
											LanlinkInternational.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
											LanlinkInternational.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
											
										LanlinkInternational.verifyValuesforCPEexistingdevice_1G_intEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
										
										LanlinkInternational.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
										LanlinkInternational.verifysuccessmessage("Device successfully updated");
									}
									else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
									LanlinkInternational.addDevice_IntEquipment();
									LanlinkInternational.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									LanlinkInternational.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(testDataFile, dataSheet, scriptNo, dataSetNo);	
									LanlinkInternational.verifysuccessmessage("Device successfully created");
									   
									 LanlinkInternational.verifyCPEdevicedataenteredForIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
									   
									   LanlinkInternational.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
									   LanlinkInternational.verifysuccessmessage("Device successfully updated");
									}
								}
						    else if(speed.equals("10GigE")) {
						    	if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
						    		ExtentTestManager.getTest().log(LogStatus.INFO, "addExistingDevice_10G_IntermediateEquipment");
						    		LanlinkInternational.addDevice_IntEquipment();
						    		LanlinkInternational.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
						    		LanlinkInternational.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
						    		LanlinkInternational.verifyValuesforCPEexistingdevice_10G_intEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
						    		
						    		LanlinkInternational.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
						    		LanlinkInternational.verifysuccessmessage("Device successfully updated");
						    	}
						    	else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
						    		ExtentTestManager.getTest().log(LogStatus.INFO, "addNewDevice_10G_IntermediateEquipment");
						    		LanlinkInternational.addDevice_IntEquipment();
						    		LanlinkInternational.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
						    		LanlinkInternational.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(testDataFile, dataSheet, scriptNo, dataSetNo);	
						    		LanlinkInternational.verifysuccessmessage("Device successfully created");
								   
						    		LanlinkInternational.verifyCPEdevicedataenteredForIntermediateEquipment_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
								   
						    		LanlinkInternational.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
						    		LanlinkInternational.verifysuccessmessage("Device successfully updated");
						    		}
								}
								}
								
								String devicename_intEquip=null;
								String manageAdres_intEquip=null;
								
							//Get device name 	
								if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
									String intEquip_existingDeviceValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "intEquip_existingDeviceValue");

									devicename_intEquip=intEquip_existingDeviceValue;
								}
								else if(existingDevice.equalsIgnoreCase("no")  && newDevice.equalsIgnoreCase("Yes")) {
									String EDIT_Intequip_cpe_deviecname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");

									
									if((EDIT_Intequip_cpe_deviecname).equalsIgnoreCase("null")) {
										String device_intEquip_name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "device_intEquip_name");

										devicename_intEquip=device_intEquip_name;
									}
									else if((EDIT_Intequip_cpe_deviecname).equalsIgnoreCase("null")) {
										devicename_intEquip=EDIT_Intequip_cpe_deviecname;
									}
								}

								
						//navigate to view device page	
								LanlinkInternational.IntEquip_clickonviewButton(devicename_intEquip);
								

									String devicename_intEquipActual=null;
									devicename_intEquip=LanlinkInternational.fetchdevicename_InviewPage();
								
									if(devicename_intEquip.contains("...")) {
										devicename_intEquipActual = devicename_intEquip.substring(0, 10);
									}else {
										devicename_intEquipActual=devicename_intEquip;
									}
								String	devicename_IntEquipment=devicename_intEquipActual;
									
								manageAdres_intEquip=LanlinkInternational.fetchManagementAddressValue_InviewPage();		//Maagement Address
								String vendorModel_intEqiup=LanlinkInternational.fetchVendorModelValue();	//Vendor/Model	
								String country_intEquip=LanlinkInternational.fetchCountryValue_InviewPage();		//Country
							
								
							//Fetch device Interface
								LanlinkInternational.testStatus();
								boolean link=LanlinkInternational.fetchDeviceInterface_viewdevicepage(devicename_intEquip);
								String CommandIPv4_Routertool = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CommandIPv4_Routertool");

							LanlinkInternational.routerPanel(CommandIPv4_Routertool, manageAdres_intEquip);
							
							String siteOrderValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");

								LanlinkInternational.clickOnBreadCrump(siteOrderValue);
								LanlinkInternational.selectInterfacelinkforIntermediateEqipment(devicename_IntEquipment);
								String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RemoveInterface_Selection");

									if((RemoveInterface_Selection).equalsIgnoreCase("yes")) {
										LanlinkInternational.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
									}else {
										System.out.println("interfaces are not removed");
									}
									String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddInterface_Selection");

									if((AddInterface_Selection).equalsIgnoreCase("yes")) {
										LanlinkInternational.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
									}
									
									LanlinkInternational.clickOnBreadCrump(siteOrderValue);
									String Interfacename_forEditInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacename_forEditInterface");

									LanlinkInternational.SelectShowInterfacelink_IntermediateequipmentAndVerifyEditInterfacePage(Interfacename_forEditInterface,  devicename_IntEquipment );
									LanlinkInternational.EnterdataForEditInterfaceforShowInterfacelinkunderIntermediateEquipment(testDataFile, dataSheet, scriptNo, dataSetNo);
								

									LanlinkInternational.deleteDeviceFromServiceForIntermediateequipment(testDataFile, dataSheet, scriptNo, dataSetNo);
									LanlinkInternational.successMessage_deleteFromService();
									}
								}else {
									ExtentTestManager.getTest().log(LogStatus.PASS, " 'Intermediate Equipment' panel not displaying under 'view site order' page");
									Reporter.log(" 'Intermediate Equipment' panel not displaying under 'view site order' page");
								}
								
								String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_serviceNumber");
								String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "serviceNumber");

								String ServiceID = null;
								if((Edit_serviceNumber).equalsIgnoreCase("null")) {
									ServiceID=serviceNumber;
								}else {
									ServiceID=Edit_serviceNumber;
								}
								LanlinkInternational.returnbacktoviewsiteorderpage();
							
								LanlinkInternational.pamTest(ServiceID);
								LanlinkInternational.deleteSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
						}	
					String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_serviceNumber");
					String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "serviceNumber");

							String ServiceID = null;
							if((Edit_serviceNumber).equalsIgnoreCase("null")) {
								ServiceID=serviceNumber;
							}else {
								
								ServiceID=Edit_serviceNumber;
							}
							LanlinkInternational.clickOnBreadCrump(ServiceID);
							LanlinkInternational.deleteService();
					
					
		
	} catch (Exception e) {
		Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
		ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	}
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
}

}