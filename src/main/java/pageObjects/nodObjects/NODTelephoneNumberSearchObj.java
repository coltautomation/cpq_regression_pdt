package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODTelephoneNumberSearchObj {
	
	public static class MyNumber
	{
		public static final String myTelephoneNoOpt="@xpath=//label[contains(text(),'My Telephone Numbers')]"; //My Telephone Number option
		public static final String myTelephoneNoPage="@xpath=//ng-containter[contains(text(),'My Telepho')]"; //My Telephone Number page
		
		public static final String SelectDrpDwn="@xpath=//button[contains(text(),' Select')]"; //Select Drop down
		public static final String NumberOpt="@xpath=//*[@class='dropdown-menu show']//a[contains(text(),'Number')]"; //Number option
		public static final String OrderIDOpt="@xpath=//*[@class='dropdown-menu show']//a[contains(text(),'Order Id')]"; //Order ID option

		public static final String SearchTxtFld="@xpath=(//*[@class='input-group']//*[@placeholder='Search'])[2]"; 
		public static final String SearchIcon="@xpath=(//*[@class='input-group-append']//*[@type='button'])[1]"; 

		
		public static final String RangeStartColumn="@xpath=//div[contains(text(),'Range Start')]"; 
		public static final String RangeStartVal="@xpath=(//*[@tabulator-field='telephoneNumberStart'])[2]"; 

		public static final String RangeEndColumn="@xpath=//div[contains(text(),'Range End')]"; 
		public static final String RangeEndVal="@xpath=(//*[@tabulator-field='telephoneNumberEnd'])[2]"; 

		public static final String DateColumn="@xpath=//div[contains(text(),'Date (dd-mm-yy)')]"; 
		public static final String DateVal="@xpath=(//*[@tabulator-field='date'])[2]"; 

		public static final String OrderIDColumn="@xpath=//div[contains(text(),'Order Id')]";
		public static final String OrderIDVal="@xpath=(//*[@tabulator-field='transactionId'])[2]";
		
		public static final String UserColumn="@xpath=//div[contains(text(),'User')]";
		public static final String UserVal="@xpath=(//*[@tabulator-field='userName'])[2]";
		
		public static final String DownlaodExelIcon="@xpath=(//button[@class='ct-icon-box'])[4]"; 
				public static final String OrderDetailsSection="@xpath=//a[contains(text(),'Order Details')]"; 
				public static final String OrderDetailsPage="@xpath=//h5[contains(text(),'Order Details')]"; 

				public static final String NumberDetailsection="@xpath=//a[contains(text(),'Number Details')]"; 
				public static final String NumberDetailsPage="@xpath=//h4[contains(text(),'Telephone Numbers')]"; 

				public static final String HistoryConversationSection="@xpath=//a[contains(text(),'History / Conversation')]"; 
				public static final String HistoryConversationPage="@xpath=//div[@id='NewPortIncollapseStepper']"; 

				public static final String NumberHistorysSection="@xpath=//a[contains(text(),'Number History')]"; 
				public static final String NumberHistoryField="@xpath=//h6[contains(text(),'Search for a number')]"; 

				//Atul
				public static final String TableRecordLoader="@xpath=//*[@class='ph-item tblLoader']"; 
				public static final String NumberStatus="@xpath=//*[@id='numberStatus']"; 
				public static final String OrderID="@xpath=(//a[@class='transaction'])[1]"; 
		
				public static final String NumberStatusColumn="@xpath=//div[contains(text(),'Number Status')]";
				public static final String NumberVal="@xpath=(//*[@tabulator-field='numberStatus'])[2]";	
				
	}
	
	
	public static class TXDetailpage
	{
		public static final String ActionDropDown="@xpath=//option[text()='Actions']/parent::select"; 
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}