package testHarness.siebelFunctions;
import java.util.Map.Entry;

import org.apache.commons.math3.stat.descriptive.summary.Product;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
//import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import java.util.List;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
//import baseClasses.PropertyReader;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelNewOrderOnnetHelperObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;


public class SiebelNewOrderOnnetHelper extends SeleniumUtils {
	
	Random rnd = new Random();
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();
	WebElement el;
	public static ThreadLocal<String> Circuitreferencenumber = new ThreadLocal<>();
	public static ThreadLocal<String> OrderingCustomer = new ThreadLocal<>();
	public static ThreadLocal<String> ServiceOrder = new ThreadLocal<>();
	public static ThreadLocal<String> ModifiedServiceOrder = new ThreadLocal<>();
	
	
	SiebelLoginPage Login = new SiebelLoginPage();

	public void OpenServiceOrder(Object[] Inputdata) throws Exception {
		waitForAjax();

		do {
			Reusable.Pagerefresh();
			System.out.println("Page to be refresed");
			waitForAjax();
		}while (!isElementPresent(By.xpath("//a[text()='Orders']")));

		try {
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderTab);
		} catch (Exception e) {
			try {
				javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderTab);
			} catch (Exception e1) {

				e1.printStackTrace();
			}
		}
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputServiceOrder,10);
		Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputServiceOrder,"871684004/191025-0035" /*ServiceOrder.get().toString()*/);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderGo);
		waitForAjax();
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderClickOn);

	}
	
	/*
	 * added by-Vikram for Hub product for Spoke
	 */
	public void hubSiteCustomize(Object[] Inputdata) throws Exception {
			
		Reusable.waitForSiebelLoader();
		waitForAjax();
	
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Resilience Option"));
	waitForAjax();
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[27].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();
	

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceBandwidthDropdownAccess,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceBandwidthDropdownAccess);
	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceBandwidthSelectAccess.replace("value", Inputdata[32].toString()),10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceBandwidthSelectAccess.replace("value", Inputdata[32].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();
	
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Hard Modify Flag"));
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", "N"));
	waitForAjax();
	Reusable.waitForSiebelLoader();

	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "OSS Platform Flag"),
			Inputdata[38].toString());
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();
	waitForAjax();
	

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickheretoSaveAccess,10);
	
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickheretoSaveAccess);

	Reusable.waitForSiebelLoader();

	javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.searchAddressSiteA);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Search Address SiteA");
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.streetNamerfs, Inputdata[86].toString());
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Street Name");

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.country);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", Inputdata[87].toString()));
	ExtentTestManager.getTest().log(LogStatus.PASS," Step: Private Ehternet >> Service Bandwidth Select : " + Inputdata[32].toString());
	// SendKeys(getwebelement(xml3.getlocator("//locators/Country")), Inputdata[87].toString());
	// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Country");
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.city, Inputdata[88].toString());
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter City");
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.postalCode, Inputdata[89].toString());
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Postal Code");
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.premises, Inputdata[90].toString());
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Premises");
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.search);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Search");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.searchAddressRowSelection,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.searchAddressRowSelection);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Pick Address");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.pickAddress,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.pickAddress);

	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Pick");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.searchAddressRowSelection,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.searchAddressRowSelection);

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.pickBuilding,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.pickBuilding);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Row and Pick Building");

	Reusable.waitForSiebelLoader();
	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.searchAddressRowSelection,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.searchAddressRowSelection);

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.pickSite,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.pickSite);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Row and Pick Site");
	Reusable.waitForSiebelLoader();


//	WaitforElementtobeclickable(xml.getlocator("//locators/IpGurdianSave"));
//	Clickon(getwebelement(xml.getlocator("//locators/IpGurdianSave")));
//	 Thread.sleep(8000);
//
//	waitforPagetobeenable();

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.servicePartySearchAccess,10);
	mouseMoveOn(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.servicePartySearchAccess);
	javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.servicePartySearchAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Search Service Party");

	// System.out.println("EnterService");
	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.servicePartyDropdownAccess, 8);

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.servicePartyDropdownAccess,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.servicePartyDropdownAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Service Party Dropdown");

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.partyNameAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Party Name");

	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputPartyNameAccess, Inputdata[69].toString());
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Party Name");

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.partyNameSearchAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click On Search");

	waitForAjax();
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.partyNameSubmitAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click On Submit");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteContactSearchAccess,10);
	javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteContactSearchAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Search Site Contact");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteContactDropdownAccess, 8);
	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteContactDropdownAccess,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteContactDropdownAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Site Name Dropdown");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputSiteNameAccess,10);
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputSiteNameAccess, Inputdata[70].toString());
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Site Name");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.lastNameSiteSearchAccess,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.lastNameSiteSearchAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click On Search");
	Reusable.waitForSiebelLoader();
	waitForAjax();
	//waitforPagetobeenable();
	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.lastNameSiteSubmitAccess,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.lastNameSiteSubmitAccess);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click On Submit");

	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.ipGurdianSave,10);
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.ipGurdianSave);
	Reusable.waitForSiebelLoader();

	Reusable.waitForSiebelLoader();

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Access Type"));
	waitForAjax();
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[42].toString()));
	
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Access Technology"));
	waitForAjax();
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[43].toString()));
	
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Building Type"));
	waitForAjax();
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[45].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Customer Site Pop Status"));
	waitForAjax();
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[46].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	waitForAjax();
	Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "3rd Party Connection Reference"));
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "3rd Party Connection Reference"),
			"As12");
	Reusable.waitForSiebelLoader();

	Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "BCP Reference"));
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "BCP Reference"),
			"As12");
	Reusable.waitForSiebelLoader();
	
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Cabinet Type"));
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[48].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Cabinet ID"));
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Cabinet ID"), "12");
	Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Cabinet ID"),
			Keys.ENTER);
	waitForAjax();

	Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Shelf ID"));
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Shelf ID"), "12");
	Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Shelf ID"),
			Keys.ENTER);
	waitForAjax();

	Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Slot ID"));
	Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Slot ID"), "12");
	Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Slot ID"),
			Keys.ENTER);
	waitForAjax();

	Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Physical Port ID"));
		Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Physical Port ID"),
				"12");
		Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.textInput.replace("Value", "Physical Port ID"),
				Keys.ENTER);
		waitForAjax();

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Presentation Interface"));
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[53].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Connector Type"));
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[54].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Fibre Type"));
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[55].toString()));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();
	
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Port Role"));
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", "Physical Port"));
	Reusable.waitforAttributeloader();
	Reusable.waitForSiebelLoader();

	

	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickDropdown.replace("Value", "Install Time"));
	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.selectValueDropdown.replace("Value", Inputdata[56].toString()));
	Reusable.waitForSiebelLoader();
	
	
//	WaitforElementtobeclickable((xml.getlocator("//locators/CircuitReferenceAccess")));
//	Clickon(getwebelement(xml.getlocator("//locators/CircuitReferenceAccess")));
//	waitforAttributeloader();
//	waitforPagetobeenable();
//	Thread.sleep(30000);
//
//	Circuitreferencenumber.set(Getattribute(getwebelement2(xml.getlocator("//locators/CircuitReferenceValue")),"value"));
//	System.out.println(Circuitreferencenumber.get());
//	ExtentTestManager.getTest().log(LogStatus.PASS,
//			" Step: Generated circuit reference No: " + Circuitreferencenumber.get());
//
	Reusable.ClickHereSave();
	Reusable.waitForSiebelLoader();
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	waitForAjax();

		}
		
	public void middleApplet(Object Inputdata[]) throws InterruptedException, IOException {
		System.out.println("middle applet start");
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.supportStarDate,10);
		// Clear(getwebelement(xml2.getlocator("//locators/SupportStarDate")));
		// getwebelement(xml2.getlocator("//locators/SupportStarDate")).clear();
		// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clear Date");
		String TempDate= Reusable.CurrentDate();
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.supportStarDate);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter  SupportStarDate : "+ TempDate);
		Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.supportStarDate, TempDate);

		System.out.println("middle applet start1");
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.supprtEndDate,10);
		// Clear(getwebelement(xml2.getlocator("//locators/SupprtEndDate")));
		
		// getwebelement(xml2.getlocator("//locators/SupprtEndDate")).clear();
		// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clear Date");

		TempDate=Reusable.FutureDate(60);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.supprtEndDate);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter  SupprtEndDate");
		System.out.println("the End date value is" + TempDate);

		Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.supprtEndDate, TempDate);		

		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.saveButton);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Save Button");
		waitForAjax();
		Reusable.waitForSiebelLoader();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Middle Applet End");
		
	}

	public void LaunchingXNGApplication(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String username, String password) throws Exception {
		String Circuitreference_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitReference_NumberXNG");
		//Thread.sleep(50000);
		openBrowser("ie");
		Thread.sleep(50000);
		//openurl(Configuration.XNG_URL);
	   // webDriver.switchTo().alert();
	                
	     Reusable.waitForSiebelLoader();
			
		waitForAjax();
		try
		{
			//PropertyReader pr = new PropertyReader();
			//Alert alert = webDriver.switchTo().alert();		
		/*	waitForAjax();
			Robot rb = new Robot();
					
			Clipboard cl = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection stringSelection = new StringSelection(username);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter username in XNG");
			cl.setContents(stringSelection, null);
			rb.keyPress(KeyEvent.VK_CONTROL);
			rb.keyPress(KeyEvent.VK_V);
			rb.keyRelease(KeyEvent.VK_V);
			rb.keyRelease(KeyEvent.VK_CONTROL);

			waitForAjax();
			rb.keyPress(KeyEvent.VK_TAB);
			rb.keyRelease(KeyEvent.VK_TAB);
			waitForAjax();
			StringSelection stringSelection1 = new StringSelection(password);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter password in XNG");
			cl.setContents(stringSelection1, null);

			rb.keyPress(KeyEvent.VK_CONTROL);
			rb.keyPress(KeyEvent.VK_V);
			rb.keyRelease(KeyEvent.VK_V);
			rb.keyRelease(KeyEvent.VK_CONTROL);

			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			*/
			waitForAjax();
			// System.out.println("The title of page is:"+iedr.getTitle());

			verifyExists(SiebelNewOrderOnnetHelperObj.XNG.CircuitPathsLink,"circuit paths");
			click(SiebelNewOrderOnnetHelperObj.XNG.CircuitPathsLink,"circuit paths");
			waitForAjax();
		
			verifyExists(SiebelNewOrderOnnetHelperObj.XNG.CircuitId,"circuit ID");
			click(SiebelNewOrderOnnetHelperObj.XNG.CircuitId,"circuit ID");
			waitForAjax();
						
			sendKeys(SiebelNewOrderOnnetHelperObj.XNG.CircuitId,Circuitreference_Number,"Enter circuit ID");
			waitForAjax();
			
			verifyExists(SiebelNewOrderOnnetHelperObj.XNG.SeachButton,"Seach Button");
			click(SiebelNewOrderOnnetHelperObj.XNG.SeachButton,"Seach Button");
			waitForAjax();
						
			String xp = "//th[text()='ID']/../following-sibling::tr/td/a";
			
			//verifyExists(SiebelNewOrderOnnetHelperObj.XNG.CircuitReferenceNumber.replace("Value", xp),"Circuit Reference Number");
			//click(SiebelNewOrderOnnetHelperObj.XNG.CircuitReferenceNumber.replace("Value", xp),"Circuit Reference Number");
			//waitForAjax();
			
			webDriver.findElement(By.xpath(xp)).click();
			Report.LogInfo("click","<b><i>"+xp+"</i></b> Is Clicked Successfully", "PASS");
			//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on cicuit reference number");
			String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
			
			/*
			waitForAjax();
			String xp = "//th[text()='ID']/../following-sibling::tr/td/a";
			iedr.findElement(By.xpath(xp)).click();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on cicuit reference number");
			String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
			*/
			// Bandwidth verify//
			waitForAjax();
			String title3 = webDriver.getTitle();
			System.out.println("The title of third page is:" + title3);
			Report.LogInfo("Verify","<b><i>"+title3+"</i></b> Is Display Successfully", "PASS");
			waitForAjax();
			if (ProductName.toString().equals("Dark Fibre")) {
				WebElement bandwidth = webDriver.findElement(By.cssSelector("table tbody tr:nth-child(3) td:nth-child(4)"));
				int firstBandwidth;

				if (bandwidth.getText().toLowerCase().contains("k")) {
					int getNumberFromText = Integer.parseInt(bandwidth.getText().toLowerCase().split("k")[0]);
					firstBandwidth = getNumberFromText / 1024;

				} else if (bandwidth.getText().toLowerCase().contains("g")) {
					firstBandwidth = Integer.parseInt(bandwidth.getText().toLowerCase().split("g")[0]);
				} else {
					firstBandwidth = Integer.parseInt(bandwidth.getText().toLowerCase().split("m")[0]);
				}
				System.out.println("The first text is:" + firstBandwidth);

				if (ProductName.toString().equals("IP VPN Service")) {
					String Service_Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
					//String data = Inputdata[40].toString();
					int textinput = Integer.parseInt(Service_Bandwidth.toLowerCase().split("m")[0].trim());
					System.out.println(textinput);
					Assert.assertTrue(textinput == firstBandwidth, "Bandwith not match in XNG");
					Report.LogInfo("Verify","<b><i>"+firstBandwidth+"</i></b> Bandwidth verify has to be done", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Bandwidth verify has to be done");
				} else if (ProductName.toString().equals("Private Wave Service")
				||ProductName.toString().equals("Private Ethernet")
				||ProductName.toString().equals("DCA Ethernet")
				||ProductName.toString().equals("Ultra Low Latency")
				||ProductName.toString().equals("Ethernet VPN Access")
				||ProductName.toString().equals("Ethernet Hub")
				||ProductName.toString().equals("IP Access")){
					String Service_Bandwidth1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
					//String data = Inputdata[32].toString();
					
					if (ProductName.toString().equals("Private Ethernet")
							||(ProductName.toString().equals("IP Access"))) {
						int textinput = Integer.parseInt(Service_Bandwidth1.toLowerCase().split("m")[0].trim());
						System.out.println(textinput);
						Assert.assertTrue(textinput == firstBandwidth, "Bandwith not match in XNG");
						Report.LogInfo("Verify","<b><i>"+firstBandwidth+"</i></b> Bandwidth verify has to be done", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Bandwidth verify has to be done");
					} else {
						int textinput = Integer.parseInt(Service_Bandwidth1.toLowerCase().split("g")[0].trim());
						System.out.println(textinput);
						Assert.assertTrue(textinput == firstBandwidth, "Bandwith not match in XNG");
						Report.LogInfo("Verify","<b><i>"+firstBandwidth+"</i></b> Bandwidth verify has to be done", "FAILED");
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Bandwidth verify has to be done");
					}
				}

				else if (ProductName.toString().equals("Ethernet Spoke")
						||(ProductName.toString().equals("Ethernet Line")
						||(ProductName.toString().equals("Ethernet Wave")))) {
					String Service_Bandwidth2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
					//String data = Inputdata[78].toString();
					if (ProductName.toString().equals("Wave")) {
						int textinput = Integer.parseInt(Service_Bandwidth2.toLowerCase().split("g")[0].trim());
						System.out.println(textinput);
						Assert.assertTrue(textinput == firstBandwidth, "Bandwith not match in XNG");
						Report.LogInfo("Verify","<b><i>"+firstBandwidth+"</i></b> Bandwidth verify has to be done", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Bandwidth verify has to be done");

					} else {
						int textinput = Integer.parseInt(Service_Bandwidth2.toLowerCase().split("m")[0].trim());
						System.out.println(textinput);
						Assert.assertTrue(textinput == firstBandwidth, "Bandwith not match in XNG");
						Report.LogInfo("Verify","<b><i>"+firstBandwidth+"</i></b> Bandwidth verify has to be done", "FAILED");
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Bandwidth verify has to be done");
					}
				}
			} // end of if loop
				// for ordering customer

			String data1 = OrderingCustomer.get().trim();
			System.out.println(data1);
			String orderingCustomer = webDriver
					.findElement(By.xpath("//b[text()='Ordering Customer:']/parent::td/following-sibling::td[1]"))
					.getText();
			System.out.println("THe ordering customer is:" + orderingCustomer);
			Assert.assertTrue(data1.equalsIgnoreCase(orderingCustomer), "Ordering customer not match in XNG");
			Report.LogInfo("Verify","<b><i>"+orderingCustomer+"</i></b> Bandwidth verify has to be done", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Verify ordering customer");

			// for topology//

//					String topology = iedr.findElement(By.xpath("//b[text()='Topology:']/parent::td/following-sibling::td[1]")).getText();
//					System.out.println("Topolgy from browser:"+ topology);
//					String topologyin = Inputdata[27].toString();
//					String sub = topologyin.substring(0, 15).trim();
//					System.out.println("Topology in input sheet:"+sub );
//					Assert.assertTrue(sub.equalsIgnoreCase(topology), "Ordering customer not match in XNG");
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Topology verified");
//					
			// for closing the browser
			webDriver.close();

		} catch (Exception e) {
			//iedr.close();
		}
			
	}
	
	/*
	 * By Aman
	 */
	public void middleUltraLowLatency(Object[] Inputdata) throws Exception {
		middleAppletDarkFibre(Inputdata);
	}
	
	public void SiteBAccessPortPUD(Object[] InputData) throws InterruptedException, IOException {
		Random rand = new Random();
		String ProductName = InputData[8].toString();

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Dark Fibre"))
				|| (ProductName.equalsIgnoreCase("Private Wave Service"))
				|| (ProductName.equalsIgnoreCase("Ultra Low Latency"))) {
			// Connector Type
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Connector Type"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Connector Type"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[54].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS,
					" Step: Select Connector Type : " + InputData[54].toString());
			Reusable.waitForSiebelLoader();

			// Fibre Type
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Fibre Type"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Fibre Type"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[55].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Fibre Type : " + InputData[55].toString());
			Reusable.waitForSiebelLoader();

			// Physical Port ID
			int rand_int1 = rand.nextInt(1000);
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Physical Port ID"),10);
			Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Physical Port ID"));
			if (InputData[115].toString() == "") {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Physical Port ID"),
						Integer.toString(rand_int1));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Physical Port ID : " + Integer.toString(rand_int1));
			} else {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Physical Port ID"),
						InputData[101].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Physical Port ID : " + InputData[115].toString());
			}
			Reusable.waitForSiebelLoader();

			// Slot ID
			rand_int1 = rand.nextInt(1000);
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Slot ID"),10);
			Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Slot ID"));
			if (InputData[67].toString() == "") {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Slot ID"),
						Integer.toString(rand_int1));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Enter Slot ID : " + Integer.toString(rand_int1));
			} else {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBInput.replace("Value", "Slot ID"),
						InputData[67].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Slot ID : " + InputData[66].toString());
			}
			Reusable.waitForSiebelLoader();
		}

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Ultra Low Latency"))) {
			// Presentation Interface
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Presentation Interface"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Presentation Interface"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[116].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS,
					" Step: Select Presentation Interface: " + InputData[116].toString());
			Reusable.waitForSiebelLoader();

			if ((ProductName.equalsIgnoreCase("Private Ethernet"))) {
				// VLAN Tagging Mode
				Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "VLAN Tagging Mode"),10);
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "VLAN Tagging Mode"));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Select VLAN Tagging Mode : " + InputData[121].toString());
				Reusable.waitForSiebelLoader();
			}

		}
	}
	
	/* Added by Devesh for R4 Products */
	public void SiteBDiversityCircuitConfig(Object[] InputData)
			throws InterruptedException, IOException {

		String ProductName = InputData[8].toString();
		if (ProductName == "Private Ethernet") {
			// Entering Customer Dedicated Access Ring
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value",
					"Customer Dedicated Access Ring"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value",
					"Customer Dedicated Access Ring"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[154].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS,
					" Step: Select Customer Dedicated Access Ring : " + InputData[154].toString());
			Reusable.waitForSiebelLoader();

			// Diversity Type
			/*
			 * WaitforElementtobeclickable(
			 * xml.getlocator("//locators/R4/SiteBDropdownClick").replace("Value",
			 * "Diversity Type")); Clickon(getwebelement(
			 * xml.getlocator("//locators/R4/SiteBDropdownClick").replace("Value",
			 * "Diversity Type"))); Clickon(getwebelement(
			 * xml.getlocator("//locators/R4/SiteABSelection").replace("Value",
			 * InputData[155].toString()))); ExtentTestManager.getTest().log(LogStatus.PASS,
			 * " Step: Select Diversity Type : " + InputData[155].toString());
			 * waitforPagetobeenable();
			 */

			// Entering Customer Dedicated Access Ring
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Dual Customer Power Source"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteBDropdownClick.replace("Value", "Dual Customer Power Source"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[156].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS,
					" Step: Select Dual Customer Power Source : " + InputData[156].toString());
			Reusable.waitForSiebelLoader();
		}
	}
	
	/*
	 * Created by Aman
	 */
	public void middleAppletDarkFibre(Object[] Inputdata) throws Exception {
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		if (Inputdata[8].toString().contains("Ultra Low Latency")
				|| Inputdata[8].toString().contains("Private Wave Service")) {
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Service Bandwidth"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Service Bandwidth"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Service Bandwidth");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", Inputdata[32].toString()),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", Inputdata[32].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  4 Mbps");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Bandwidth Type"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Bandwidth Type"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Bandwidth Type");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Layer 1"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Layer 1"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Layer 1");

		}

		if ((Inputdata[8].toString().contains("Dark Fibre"))
				|| (Inputdata[8].toString().contains("Ultra Low Latency"))) {
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Coverage"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Coverage"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Coverage");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", Inputdata[138].toString()),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", Inputdata[138].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  "+Inputdata[138]);
		}

		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "A End Resilience Option"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "A End Resilience Option"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  A End Resilience Option");
		if (!(Inputdata[8].toString().contains("Ultra Low Latency"))
				&& !(Inputdata[8].toString().contains("Private Wave Service"))) {
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Single Pair"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Single Pair"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Single pair");
		}
		if (Inputdata[8].toString().contains("Ultra Low Latency")
				|| (Inputdata[8].toString().contains("Private Wave Service"))) {
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", Inputdata[75].toString()),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", Inputdata[75].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  "+Inputdata[75]);
		}

		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "B End Resilience Option"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "B End Resilience Option"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  B End Resilience Option");
		if (!(Inputdata[8].toString().contains("Ultra Low Latency"))
				&& !(Inputdata[8].toString().contains("Private Wave Service"))) {
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Single Pair"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Single Pair"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Single pair");
		}
		if ((Inputdata[8].toString().contains("Ultra Low Latency")
				|| (Inputdata[8].toString().contains("Private Wave Service")))) {
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Protected"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Protected"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Protected");
		}

		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "OSS Platform Flag"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "OSS Platform Flag"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on OSS Platform Flag ");

		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Legacy"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Legacy"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Metro");

		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Hard Modify Flag"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Hard Modify Flag"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Hard Modify Flag");

		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Y"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "Y"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  yes");

		if ((Inputdata[8].toString().contains("Private Wave Service"))) {
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Circuit Category"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Circuit Category"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Hard Modify Flag");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "LANLINK"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "LANLINK"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  LANLINK");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Service Type"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Service Type"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Service Type");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "100 Gbps Ethernet"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "100 Gbps Ethernet"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  100 Gbps Ethernet");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Circuit Prefix"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleDropDown.replace("Value", "Circuit Prefix"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Circuit Prefix");

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "LE-"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.middleLi.replace("Value", "LE-"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  LE-");
		}

//			WaitforElementtobeclickable(xml.getlocator("//locators/DarkFiber/ClicktoShowFulls"));
//			Clickon(getwebelement(xml.getlocator("//locators/DarkFiber/ClicktoShowFulls")));
//			
		//
//			WaitforElementtobeclickable(xml.getlocator("//locators/DarkFiber/AEndSiteInput").replace("Value", "Attachment Link"));
//			Clickon(getwebelement(xml.getlocator("//locators/DarkFiber/AEndSiteInput").replace("Value", "Attachment Link")));
//			SendKeys(getwebelement(xml.getlocator("//locators/DarkFiber/AEndSiteInput").replace("Value", "Attachment Link")),Inputdata[187].toString());
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: enter  on Attachment Link");
//			
		//
//			WaitforElementtobeclickable(xml.getlocator("//locators/DarkFiber/AEndSiteInput").replace("Value", "Diverse From Service Reference"));
//			Clickon(getwebelement(xml.getlocator("//locators/DarkFiber/AEndSiteInput").replace("Value", "Diverse From Service Reference")));
//			SendKeys(getwebelement(xml.getlocator("//locators/DarkFiber/AEndSiteInput").replace("Value", "Diverse From Service Reference")),Inputdata[187].toString());
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: enter  on Site Name Alias");
		//
////			WaitforElementtobeclickable(xml.getlocator("//locators/DarkFiber/MiddleDropDown").replace("Value", "Circuit Reference"));
////			Clickon(getwebelement(xml.getlocator("//locators/ManagedDedicatedFirewall/MiddleDropDown").replace("Value", "Circuit Reference")));
////			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Coverage");

		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.drakFiberSaveButton,10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.drakFiberSaveButton);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Save Button ");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

	}
	
	/*
	 * Created by Aman
	 */

	public void settingPrivateWaveNode(Object[] Inputdata) throws Exception {
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton,10);
    	waitForAjax();
    	
    		//Clickon(getwebelement(xml.getlocator("//locators/PrivateWaveService/Dipesh")));	
    	
    	
    	//clickUsingAction(getwebelement(xml.getlocator("//locators/PrivateWaveNode/SettingsButton")));
    	javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton);
		  // waitforPagetobeenable();
    	waitForAjax(); 
		   
		   Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor,10);
		   click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor);
    	sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor, "ADVA");
    	Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor,Keys.TAB);
    	
    	
    	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.techonolgyValuecell,10);
    	click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.techonolgyValuecell);
    	 Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputvaluesTechonolgy,10);
    	 sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputvaluesTechonolgy, "CWDM");
    	 Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputvaluesTechonolgy,Keys.TAB);

    	 waitForAjax();
    	 Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.oKButton,10);
    	 click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.oKButton);
	}
	
	/*
	 * Created by Aman
	 */
	public void SiteAAccessPortPUD(Object[] InputData) throws InterruptedException, IOException {
		Random rand = new Random();
		String ProductName = InputData[8].toString();
		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Ultra Low Latency"))) {
			int rand_int1 = rand.nextInt(1000);
			// Presentation Interface
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "Presentation Interface"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "Presentation Interface"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[99].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS,
					" Step: Select Presentation Interface: " + InputData[99].toString());
			Reusable.waitForSiebelLoader();
			if ((ProductName.equalsIgnoreCase("Private Ethernet"))) {
				// VLAN Tagging Mode
				Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "VLAN Tagging Mode"),10);
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "VLAN Tagging Mode"));
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[100].toString()));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Select VLAN Tagging Mode : " + InputData[100].toString());
				Reusable.waitForSiebelLoader();
			}
		}
		if ((ProductName.equalsIgnoreCase("Dark Fibre")) || (ProductName.equalsIgnoreCase("Private Ethernet"))
				|| (ProductName.equalsIgnoreCase("Ultra Low Latency"))|| (ProductName.equalsIgnoreCase("Private Wave Service"))) {
			int rand_int1 = rand.nextInt(1000);
			// Connector Type
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "Connector Type"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "Connector Type"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[54].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS,
					" Step: Select Connector Type : " + InputData[54].toString());
			Reusable.waitForSiebelLoader();

			// Fibre Type
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "Fibre Type"),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteADropdownClick.replace("Value", "Fibre Type"));
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteABSelection.replace("Value", InputData[55].toString()));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Fibre Type : " + InputData[55].toString());
			Reusable.waitForSiebelLoader();
			// Physical Port ID
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Physical Port ID"),10);
			Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Physical Port ID"));
			if (InputData[101].toString() == "") {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Physical Port ID"),
						Integer.toString(rand_int1));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Physical Port ID : " + Integer.toString(rand_int1));
			} else {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Physical Port ID"),
						InputData[101].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Physical Port ID : " + InputData[101].toString());
			}
			Reusable.waitForSiebelLoader();

			// Slot ID
			rand_int1 = rand.nextInt(1000);
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Slot ID"),10);
			Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Slot ID"));
			if (InputData[66].toString() == "") {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Slot ID"),
						Integer.toString(rand_int1));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Enter Slot ID : " + Integer.toString(rand_int1));
			} else {
				sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.siteAInput.replace("Value", "Slot ID"),
						InputData[66].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Slot ID : " + InputData[66].toString());
			}
			Reusable.waitForSiebelLoader();
		}

	}
	
	/*
	 * Created by: Aman Gupta Purpose: Specific for middle applet of Managed
	 * Dedicated Firewall
	 */
	public void middleAppletManagedDedicatedFirewall(Object[] Inputdata) throws Exception {
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		System.out.println("valu of 178=" + Inputdata[179].toString());
		System.out.println("valu of 178=" + Inputdata[177].toString());
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.cPECombinationIDGeneric
				.replace("Value", "CPE Combination ID"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.cPECombinationIDGeneric
				.replace("Value", "CPE Combination ID"));
		System.out.println("valu of 180=" + Inputdata[180].toString());
		Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.cPECombinationIDGeneric
				.replace("Value", "CPE Combination ID"), Inputdata[179].toString());
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click  CPE Combination ID");
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.highAvailabilityRequired
				.replace("Value", "High Availability Required"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.highAvailabilityRequired
				.replace("Value", "High Availability Required"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  High Avilabilty");
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.highAvailabilityRequiredli,10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.highAvailabilityRequiredli);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Yes");
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.cPECombinationIDGeneric
				.replace("Value", "Security Policy Attachment Link"),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.cPECombinationIDGeneric
				.replace("Value", "Security Policy Attachment Link"));
		Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.cPECombinationIDGeneric
				.replace("Value", "Security Policy Attachment Link"), Inputdata[180].toString());
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click  on  Security");
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.saveButton,10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.saveButton);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on  Yes");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
	}
	/*
	 * added by-Vikram for Hub product for Spoke
	 */
	public void installationCharges(Object Inputdata[]) throws Exception {

		if (Inputdata[8].toString().equalsIgnoreCase("Ethernet Hub")
				|| (Inputdata[8].toString().equalsIgnoreCase("Ethernet Spoke"))) {

			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.expandAllButton);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Expand All Button");
			waitForAjax();
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.installationChargeNRC);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Installation Charge NRC");
			findWebElement(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputinstallationChargeNRC).clear();
			Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputinstallationChargeNRC,
					Inputdata[23].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Installation Charge NRC");
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.recurringChargeMRC);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Recurring Charge MRC");
			findWebElement(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputRecurringChargeMRC).clear();
			Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputRecurringChargeMRC, Inputdata[24].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Recurring Charge MRC");
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNInstallationChargeNRC);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on BCN Installation Charge NRC");
			waitForAjax();
			javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNInstallationChargeNRCSearch);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on BCN Installation Charge NRC Search");
			Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNInstallationChargeNRCInput,
					Inputdata[25].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter BCN Installation Charge NRC Input");
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNNRCSubmit);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on BCN NRC Submit");
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNRecurringChargeMRC);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on BCN Recurring Charge MRC");
			waitForAjax();
			javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNRecurringChargeMRCSearch);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on BCN Recurring Charge MRC Search");
			Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNRecurringChargeMRCInput,
					Inputdata[26].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter BCN Recurring Charge MRC Input");
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.bCNNRCSubmit);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on BCN NRC Submit");
			waitForAjax();
		}
	}

	public void serviceGroupReference() throws Exception {

		if (isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupTab))) {
			System.out.println("go to if loop of service group tab");
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupTab,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupTab);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on service tab");
		} else {
			System.out.println("service group tab present in dropdown");
			select(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.installtionDropdown, "Service Group");
			ExtentTestManager.getTest().log(LogStatus.PASS,
					" Step: Click on Installation Dropdown button and Select Installation and Test");

			Reusable.waitForSiebelLoader();
			waitToPageLoad();
		}
		waitForAjax();
		if (isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderCheck))) {

			System.out.println("Element is present");
			Boolean t = isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderCheck));
			System.out.println(t);
			if (t == true) {
				System.out.println("Service Record already exist!");
			}
		} else {
			System.out.println("Service Record not exist!");
			waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickOnPlusButtonService, 5);
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickOnPlusButtonService,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickOnPlusButtonService);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on plus button");
			Reusable.waitForSiebelLoader();

			waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupReference, 5);
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupReference,10);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on service reference");
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupReference);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on plus button");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickOkButton,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickOkButton);
			Reusable.waitForSiebelLoader();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on ok button");
		}

	}
	
	public void EnterInstallationChargeInFooterPartial(Object Inputdata[]) throws Exception {
				select(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.installtionDropdown, "Installation and Test");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Step: Click on Installation Dropdown button and Select Installation and Test");
		        Reusable.waitForSiebelLoader();
		        waitForAjax();
				Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.primaryTestingMethod);
				Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.primaryTestingMethod, "Not Required");
				Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.primaryTestingMethod, Keys.TAB);
				
				Reusable.clearTextBox(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.secondaryTestingMethod);
				Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.secondaryTestingMethod, "Not Required");
				Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.secondaryTestingMethod, Keys.TAB);
				
				Reusable.savePage();
				Reusable.waitForSiebelLoader();
				waitForAjax();
					click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.saveOrderContinue);
					Reusable.waitForSiebelLoader();
					waitForAjax();
			}
	
	public HashMap<Integer, HashMap<String, String>> EROData()
			throws IOException, InterruptedException {
		OpenTab("ERO");
		HashMap<Integer, HashMap<String, String>> MainMap = new HashMap<Integer, HashMap<String, String>>();
		List<WebElement> ColumnHeader = findWebElements(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderGridHeader);
		Assert.assertTrue(ColumnHeader.size() > 0, "ERO table no found");
		int CpnIndex = -1;
		int CityIndex = -1;
		int i = 0;
		for (WebElement ele : ColumnHeader) {
			Reusable.javaScriptClickWE(ele,"");
			String Text = ele.getText();

			System.out.println("Column : " + Text);
			if (Text.equalsIgnoreCase("CPN Number")) {
				CpnIndex = i;
			} else if (Text.equalsIgnoreCase("City")) {
				CityIndex = i;
			}
			if (CpnIndex > -1 && CityIndex > -1)
				break;
			i++;
		}

		Assert.assertTrue(CpnIndex > -1 && CityIndex > -1, "Not able to locate 'CPN Number' and 'City' column");
		List<WebElement> GridRow = findWebElements(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.eroGirdRow);
		for (int k = 0; k < GridRow.size() - 1; k++) {
			Reusable.javaScriptClickWE(GridRow.get(k), "");

			String templocator = SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.eroGridData;
			System.out.println("Locator1 : " + templocator);
			String CpnLocator = templocator.replace("-row10", String.valueOf(k + 1)).replace("-td10",
					String.valueOf(CpnIndex + 1));
			System.out.println("Locator2 : " + CpnLocator);
			String CityLocator = templocator.replace("-row10", String.valueOf(k + 1)).replace("-td10",
					String.valueOf(CityIndex + 1));
			System.out.println("Locator3 : " + CityLocator);

			String CpnValue = getTextFrom(findWebElement(CpnLocator).toString());
			System.out.println("CPN Number : " + CpnValue);
			String CityValue = getTextFrom(findWebElement(CityLocator).toString());
			System.out.println("City  : " + CityValue);

			Assert.assertTrue(CpnValue != null || CpnValue != "", "Not able to locate CPN no for City : " + CityValue);
			HashMap<String, String> detail = new HashMap<String, String>();
			detail.put(CpnValue, CityValue);
			MainMap.put(k + 1, detail);
		}
		for (Entry<Integer, HashMap<String, String>> entry : MainMap.entrySet()) {
			Map<String, String> ChildMap = entry.getValue();
			for (Entry<String, String> entry2 : ChildMap.entrySet()) {
				String childKey = entry2.getKey();
				String childValue = entry2.getValue();
				System.out.println("Key  : " + childKey + ",Value : " + childValue);
			}
		}

		return MainMap;
	}
	
	/*
	 * Added by Ayush
	 */
	public void OperationalAttributes(Object[] Inputdata) throws Exception {
		
		waitToPageLoad();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton,10);
		waitForAjax();
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton);
		Reusable.waitForSiebelLoader();
		waitForAjax();
		int count = getXPathCount(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttribueClick);
		System.out.println(count);
		for (int i = 0; i < count; i++) {

			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttribueClick,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttribueClick
					.replace("index", String.valueOf(i + 1)));
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttributeDropdown,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttributeDropdown);
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.locatorSelectValueDropdown.replace("Value", Inputdata[7].toString()),10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.locatorSelectValueDropdown.replace("Value", Inputdata[7].toString()));
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.okButtonOperationalAttribute,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.okButtonOperationalAttribute);

		}
	}
	
	public void SelectServiceGroupTab(Object[] Inputdata) throws Exception {
		if (Inputdata[8].toString().equals("Voice Line V") || Inputdata[8].toString().equals("SIP Trunking"))  {
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.dropDown);
//	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Drop down");
//	Clickon(getwebelement(xml.getlocator("//locators/InstallationAndTestTab")));
			waitForAjax();
			select(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.dropDown, "Service Group");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Service Group Tab");
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupNew);
			waitForAjax();
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGrouplookup,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGrouplookup);
			// Thread.sleep(5000);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceGroupOk);
			waitForAjax();
			/*
			 * Clickon(getwebelement(xml.getlocator("//locators/ServiceGroupItemNew")));
			 * Thread.sleep(5000); WaitforElementtobeclickable(xml.getlocator(
			 * "//locators/ServiceGroupItemlookup"));
			 * Clickon(getwebelement(xml.getlocator("//locators/ServiceGroupItemlookup")));
			 * Thread.sleep(5000);
			 * //Clickon(getwebelement(xml.getlocator("//locators/selectitem")));
			 * //Thread.sleep(5000);
			 * Clickon(getwebelement(xml.getlocator("//locators/ServiceorderOk")));
			 * Thread.sleep(5000); savePage();
			 */
		}
	}
	
	/*
	 * Created by Aman
	 */
		
	public void settingUltraLowLatency(Object[] Inputdata) throws Exception
	    {
		  
	    	waitToPageLoad();
	    	Reusable.waitForSiebelLoader();
	    	Reusable.savePage();
	    	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton,10);
	    	waitForAjax();
	    	javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton);
	    	waitForAjax();
			   
	    	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor,10);
	    	 click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor);
	    	sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor, "ADVA");
	    	Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputValuesVendor,Keys.TAB);
	    	
	    	
	    	Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.techonolgyValuecell,10);
	    	 click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.techonolgyValuecell);
	    	 Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputvaluesTechonolgy,10);
	    	 sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputvaluesTechonolgy, "CWDM");
	    	 Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputvaluesTechonolgy,Keys.TAB);
	
	    	 waitForAjax();
	    	 Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.oKButton,10);
	    	 click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.oKButton);
		}
	
	/*
	 * Created by Aman
	 */
		
	public void OperationalAttributeUltra(Object[] Inputdata) throws Exception {
			 Reusable.savePage();
//			   waitForpageload();
//		    	waitforPagetobeenable();
		    	
			 Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton,10);
		    	waitForAjax();
		    	
		    	javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.settingsButton);
				  
		    	waitForAjax();
				Reusable.waitForSiebelLoader();
				waitForAjax();
				int count = getXPathCount(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttribueCount);
				System.out.println(count);
				for (int i = 0; i < count; i++) {

			    	
					////////////////////////////////////
					Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttribueClick.replace("index",
							String.valueOf(i + 1)),10);
					click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttribueClick.replace("index",
							String.valueOf(i + 1)));
					Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttributeText,10);
					click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttributeText);
//						WaitforElementtobeclickable(xml.getlocator("//locators/SelectValueDropdown").replace("Value", Inputdata[7].toString()));
//						Clickon(getwebelement(xml.getlocator("//locators/SelectValueDropdown").replace("Value", Inputdata[7].toString())));
					Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttributeText, "Test1");

				}
				waitForAjax();
				Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttributeOK,10);
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.operationalAttributeOK);
				Reusable.savePage();
				waitForAjax();
				waitToPageLoad();
				Reusable.waitForSiebelLoader();
			}
	// Added By Abhay 
	 
		public void PartialTab(Object[] Inputdata) throws Exception {
			Reusable.waitForSiebelLoader();
					waitToPageLoad();
					
					Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.partialCheckbox,10);
					javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.partialCheckbox);
					waitForAjax();
					Reusable.savePage();
					Reusable.waitForSiebelLoader();
					waitForAjax();
					Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickLink.replace("Value", "Partial Delivery"),10);
					click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickLink.replace("Value", "Partial Delivery"));
					Reusable.waitForSiebelLoader();
					waitForAjax();
					
					click("(//td[contains(@id,'Partial_Delivery')])[1]");
					waitForAjax();
					
					click("//input[@name='COLT_Partial_Delivery']");
					waitForAjax();
					
					click("//button[@aria-label='Partial Delivery:Submit']");
					waitForAjax();
					if (Inputdata[11].toString().equalsIgnoreCase("IP VPN Access")||Inputdata[11].toString().equalsIgnoreCase("IP VPN Plus")) {
						Reusable.alertPopUp();
						}
					
					Reusable.waitForSiebelLoader();
					waitForAjax();
					
					
				}
		
	
	public void PartialDeliveryAttachment(Object[] Inputdata) throws IOException, InterruptedException {
		Reusable.waitForSiebelLoader();
		select(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.tabDropdown, "Attachments");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Attachments Tab Selected");
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.attachmentTabSelection);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Attachments Tab Clicked");
		waitForAjax();
		
		Reusable.UploadFile(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.fileUpload, "test.txt");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Test File Uploaded");
		waitForAjax();
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.documnetTypeOther);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Other File Type Clicked");
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.downArrow);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.doucmentTypeSelection.replace("Filetype", "Partial Delivery"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Partial Delivery File Type Selected");

	}
	
	// --- Added By Abhay- 28Sep-2019
		// --- Added By Abhay- 28Sep-2019
		public void MovetoIPService() throws InterruptedException, IOException {
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.clickLink.replace("Value", "Network"));
			Reusable.waitForSiebelLoader();
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.networkService);
			waitForAjax();
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.gOIPService.replace("Value", ServiceOrder.get().toString()));
			Reusable.waitForSiebelLoader();
			waitForAjax();
		}
		
		private String RandomName()
		{
			String[] BuildNames= {"APIAutomation","Automation","Selenium","Chrome","Opera","Safari","FireFox","Ghost","Edge"};
			int index=rnd.nextInt(BuildNames.length);
			return BuildNames[index];
		}
		
		public void Save() throws InterruptedException {
			try
			{
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.save,10);
			if(isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.save))&&Reusable.isDisplayed(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.save))
			{
			
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.save);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click  on Save");
			Reusable.waitForSiebelLoader();
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		/* Purpose: To open assest
		 * Created By: Dipesh Jain
		 */
			 
public void openAsset() throws InterruptedException, IOException {
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.assetButton,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.assetButton);
			if (isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.alertAccept))) {
				Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.alertAccept,10);
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.alertAccept);
				waitForAjax();
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.assetButton);
			}
			waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Assest");
			String Order=ServiceOrder.get();
			String[] Order1=Order.split("/");
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderOM,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderOM);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on ServiceOrderOM");
			Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderOM, Order1[0]);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter ServiceOrderOM:"+Order1[0]);
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.goButton,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.goButton);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Go Button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.assetNumber,10);
			click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.assetNumber);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on AssetNumber");
		}

/* Created by: Dipesh
 * New method added to open Service order
 */
		 
public void openServiceOrder(Object[] Inputdata) throws Exception {
	waitForAjax();
			
				try {
					click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderTab);
					if (isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.alertAccept))) {
						Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.alertAccept,10);
						click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.alertAccept);
						waitForAjax();
						click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderTab);
					}
					waitForAjax();
				} catch (Exception e) {
					try {
						javaScriptclick(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderTab);
					} catch (Exception e1) {

						e1.printStackTrace();
					}
				}
				Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputServiceOrder,10);
				Reusable.sendKeys(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.inputServiceOrder, ServiceOrder.get());
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderGo);
				waitForAjax();
				click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.serviceOrderClickOn);
				waitForAjax();
			}
		 
private void OpenTab(String TabName) throws IOException, InterruptedException {
	try {
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		Reusable.waitForElementToAppear(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.tabs.replace("tabName", TabName),10);
		click(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.tabs.replace("tabName", TabName));
		System.out.println("Click on tab : " + TabName);
	} catch (Exception e) {
		select(SiebelNewOrderOnnetHelperObj.NewOrderOnnetHelper.installtionDropdown, TabName);

	}
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open Tab :" + TabName);
	Reusable.waitForSiebelLoader();
	waitToPageLoad();
}
public void createCustomerOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

	String Opportunity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
	String DeliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
	String InputPartyname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Input Party name");
	String InputPartyAddr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Input Party Addr");
	String Channel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Channel");
	String Firstname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"First Name");

	
	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OpportunityNo, Opportunity);
	sendKeys(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OpportunityNo, Opportunity);

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.RequestReceivedDate, Reusable.CurrentDate());
	sendKeys(SiebelNewOrderOnnetHelperObj.createCustomerOrder.RequestReceivedDate, Reusable.CurrentDate());

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.DeliveryChannel, DeliveryChannel);
	sendKeys(SiebelNewOrderOnnetHelperObj.createCustomerOrder.DeliveryChannel, DeliveryChannel);
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OrderingPartySearch);
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OrderingPartySearch);
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PopupCustomDropdown,"verify Popup Custom Drop down");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PopupCustomDropdown,"click Popup Custom Drop down");
	Reusable.waitForSiebelLoader();
	waitToPageLoad();
	
	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PartyName,"verify Party Name");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PartyName,"click Party Name");

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.InputPartyname, InputPartyname);
	sendKeys(SiebelNewOrderOnnetHelperObj.createCustomerOrder.InputPartyname, InputPartyname);

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PickAccntOk,"verify input Party name");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PickAccntOk,"click input Party name");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OrderingPartyAddrSearch,"verify Ordering Part Name");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OrderingPartyAddrSearch,"click Ordering Party Addr Search ");
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PopupCustomDropdown,"verify Popup Custom Drop down");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PopupCustomDropdown,"click Popup Custom Drop down");
	Reusable.waitForSiebelLoader();
	waitToPageLoad();
	
	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PartyAddr,"verify input Party address");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PartyAddr,"click input Party address");
	sendKeys(SiebelNewOrderOnnetHelperObj.createCustomerOrder.InputPartyAddr, InputPartyAddr);

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PickAddrSearch);
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PickAddrSearch);
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PickAddrSubmit1);
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PickAddrSubmit1);

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OrderingPartyContactSearch,"verify Ordering Party Contact Search");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.OrderingPartyContactSearch," click on Ordering Party Contact Search");

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PopupCustomDropdown,"verify on Popup Custom Drop down");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PopupCustomDropdown,"click on Popup Custom Drop down");
	Reusable.waitForSiebelLoader();
	waitToPageLoad();
	

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.LastName,"verify Last name");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.LastName, "click on Last name");

	sendKeys(SiebelNewOrderOnnetHelperObj.createCustomerOrder.InputFirstname, Firstname);
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.PickContactSearch,"click on Pick Contact Search");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.FirstnameSubmit,"Verify on First Name Submit");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.FirstnameSubmit,"Click on First Name Submit");
	sendKeys(SiebelNewOrderOnnetHelperObj.createCustomerOrder.SalesChannel, Channel);

	verifyExists(SiebelNewOrderOnnetHelperObj.createCustomerOrder.NewServiceOrder,"Verify on New Service Order");
	click(SiebelNewOrderOnnetHelperObj.createCustomerOrder.NewServiceOrder,"Click on New Service Order");
	waitToPageLoad();
	Reusable.waitForSiebelLoader();
}
public void ASiteCustomize(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
	String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
	String SelectCoverage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Select_Coverage");
	String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
	String AEndResilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd_Resilience");
	String BEndResilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BEnd_Resilience");
	String WorkerPath = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Workerpath");
	String ProtectedPath = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Protected Path");
	String OffNet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Off_Net");
	String ThirdPartyConnect = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Third_Party_Connect");
	String AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Technology");
	String SitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Pop_Status");
	String BuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Type");
	String CabinetType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_Type");
	String CabinetId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
	String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
	String PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Port_Role");
	String VLANTaggingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VLAN_Tagging_Mode");
	String VLANTagId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VLAN_Tag_Id");
	String InstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
	
	waitToPageLoad();
	Reusable.waitForSiebelLoader();
	if (ProductName.equalsIgnoreCase("Ethernet Line")
			|| ProductName.equalsIgnoreCase("Wave")) {
		verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CustomizeButton);
		click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CustomizeButton,"Click on Customize Button");
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Coverage, SelectCoverage);
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ServiceBandwidth, ServiceBandwidth);
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AEndResilienceOption, AEndResilience);
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.BEndResilienceOption, BEndResilience);
		Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.BEndResilienceOption, Keys.TAB);

		if (ProductName.equalsIgnoreCase("Wave")) {
			Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.subseaWorkerpath, WorkerPath);
			
			Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.subseaprotectedpath, ProtectedPath);
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.subseaprotectedpath, Keys.TAB);
			// if(isElementPresent((xml3.getlocator("//locators/proceedbotton"))))
			// {
			// WaitforElementtobeclickable((xml3.getlocator("//locators/proceedbotton")));
			// Clickon(getwebelement(xml3.getlocator("//locators/proceedbotton")));
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Proceed
			// button");
			// }
		}

		// savePage();
		verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Connectionlink);
		click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AEndSite);
		
		if (OffNet.equals("Offnet"))
		{
		
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessTechnologySearch);
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessTechnologySearch);
			
			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SelectAccessTechnology1);
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SelectAccessTechnology1);
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessTechnologySubmit);
			
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartyAccessProviderSearch);
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartyAccessProviderSelect);
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartyAccessProviderSubmit);

			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartyConnecRef);
			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartyConnecRef);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartyConnecRef,ThirdPartyConnect);
			
			
			
			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartySlaTier);
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartySlaTier);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartySlaTierValue);
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ThirdPartySlaTierValue);
			Reusable.waitForSiebelLoader();

			
		}
		else
		{
		

		verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessTechnologySearch,"AccessTechnologySearch");
		click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessTechnologySearch,"AccessTechnologySearch");

		ClickonElementByString("//table[@summary='Select Access Technology']//tr//td[text()='3rd Party Leased Line'"+ AccessTechnology + "']", 30); 
	    click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessTechnologySubmit,"AccessTechnologySearch");
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CustomerSitePopStatusselect, SitePopStatus);
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Buildingtyperfs, BuildingType);
		Reusable.waitForSiebelLoader();


		}
	

		if (ProductName.equalsIgnoreCase("Ethernet Line")) {
			

			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CPEInformationLink,"Click on CPE Information Link");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CPEInformationLink,"Click on CPE Information Link");
			Reusable.waitForSiebelLoader();


			Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CabinetType,CabinetType);
			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CabinetId);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CabinetId, CabinetId);
			
			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ShelfID);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ShelfID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ShelfID, Keys.TAB);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessPortLink,"Verify on Access Port Link");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessPortLink,"click on Access Port Link");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PresentationInterfaceSearch,"Verify on Presentation Interface Search");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PresentationInterfaceSearch,"click on Presentation Interface Search");
			Reusable.waitForSiebelLoader();

			ClickonElementByString( "//table[@summary='Select Presentation Interface-Connector Type']//tr//td[text()='"+ PresentationInterface + "']",30);
			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SubmitPresentationInterface,"Verify on SubmitPresentationInterface");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SubmitPresentationInterface,"Click on SubmitPresentationInterface ");
			Reusable.waitForSiebelLoader();

			Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PortRole, PortRole);

			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SlotID);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SlotID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SlotID, Keys.TAB);
			Reusable.waitForSiebelLoader();
			
			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PhysicalPortID);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PhysicalPortID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PhysicalPortID, Keys.TAB);
			Reusable.waitForSiebelLoader();

			if (PortRole.equalsIgnoreCase("Physical Port")) {
			} else {
				
				verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.VLANTaggingMode,"VLANTaggingMode");
				click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.VLANTaggingMode,"VLANTaggingMode");
				Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.VLANTaggingMode, VLANTaggingMode);
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Vlannew,"verify on Vlan New");
				click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Vlannew,"click on Vlan New");
				Reusable.waitForSiebelLoader();

				clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.VlanTagId);
				sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.VlanTagId, VLANTagId);
				Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.VlanTagId, Keys.ENTER);
				Reusable.savePage();
				Reusable.waitForSiebelLoader();
			}
		}
		String  ColName96 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ordering_Party_Address");
		String ColName97 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ordering_Party_Address");
		String ColName99 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ordering_Party_Address");


		if (ProductName.equalsIgnoreCase("Wave")) {
			
			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Terminationinformation,"Verify on Termination information");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Terminationinformation,"click on Termination information");
			Reusable.waitForSiebelLoader();

			Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CabinetType, ColName96);
			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CabinetId);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.CabinetId, ColName97);
			
			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ShelfID);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ShelfID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.ShelfID, Keys.TAB);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessPortLink,"Verify on Access Port Link");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AccessPortLink,"click on Access Port Link");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PresentationInterfaceSearch,"Verify on Presentation Interface Search");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PresentationInterfaceSearch,"click on Presentation Interface Search");

			ClickonElementByString("//table[@summary='Select Presentation Interface-Connector Type']//tr//td[text()='"+ ColName99+ "']",30);

			verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SubmitPresentationInterface,"Verify on SubmitPresentationInterface");
			click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SubmitPresentationInterface,"Click on SubmitPresentationInterface ");
			Reusable.waitForSiebelLoader();

			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SlotID);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.SlotID, "1234");
			
			clearTextBox(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PhysicalPortID);
			sendKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PhysicalPortID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.PhysicalPortID, Keys.TAB);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
		}

		verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AEndSite,"Verify on AEndSite");
		click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.AEndSite,"Click on AEndSite");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.InstallationTimeLink,"Verify on Installation Time Link");
		click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.InstallationTimeLink,"Click on Installation Time Link");
		Reusable.waitForSiebelLoader();

		Reusable.Select1(SiebelNewOrderOnnetHelperObj.ASiteCustomize.InstallTime,InstallTime);
		Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.ASiteCustomize.InstallTime, Keys.TAB);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Connectionlink,"Verify on Connection link");
		click(SiebelNewOrderOnnetHelperObj.ASiteCustomize.Connectionlink,"Click on Connection link");
		Reusable.waitForSiebelLoader();

	}
}


public void BSiteCustomize(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
	
	String ProductType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
	String OffNet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Off_Net");
	String productName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"product Name");
	String colName113 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet Type BEnd");
	String colName114 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet ID");
	String colName99 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation Interface Search");
	String colName116 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation Interface-Connector");
	String colName115 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical Port");
	String colName117 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VLAN Tagging Mode");
	String colName118 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VLAN Tagging Mode");
	String colName44 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyConnecRef");
	String colName120 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdPartyConnecRef");
	String colName111 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access Technology");
	String colName110 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building type BEnd");
	String colName112 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ordering_Party_Address");

	if (ProductType.equals("Ethernet Line") || ProductType.equals("Wave")) {
		verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.BEndSite,"Click on B End Site");
		click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.BEndSite,"Click on B End Site");

		if (OffNet.equals("Offnet"))
		{
		
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySearch, " verify Access Technology Search");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySearch," click on Access Technology Search");
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SelectAccessTechnology1, " Verify Select Access Technology value");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SelectAccessTechnology1," Click on Select Access Technology value");
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySubmit,"Verify on Access Technology Submit");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySubmit,"Click on Access Technology Submit");
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyAccessProviderSearch,"Verify on ThirdParty Access Provider Search");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyAccessProviderSearch,"click on ThirdParty Access Provider Search");

			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyAccessProviderSelect,"Verify on Third Party Access Provider Select");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyAccessProviderSelect,"Click on Third Party Access Provider Select");

			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyAccessProviderSubmit,"Verify on ThirdParty Access Provider Submit");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyAccessProviderSubmit,"click on ThirdParty Access Provider Submit");


			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyConnecRef,"Verify on ThirdParty ConnecRef");
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyConnecRef );
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartyConnecRef,colName44);
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartySlaTier,"Verify on Third Party SlA Tier");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartySlaTier,"click on Third Party SlA Tier");
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartySlaTierValue,"Verify on Third Party SlA Tier");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ThirdPartySlaTierValue," Click on Third Party SlA Tier");
			
			
		}
		else
		

		{
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySearch,"Verify on Access Technology Search");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySearch,"click on Access Technology Search");

			javaScriptclick("//table[@summary='Select Access Technology']//tr//td[text()='"+ colName120 + "']"); 
		
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySubmit,"verify on Access Technology Submit");
            click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessTechnologySubmit,"Click on Access Technology Submit");

		Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize.CustomerSitePopStatusBEnd, colName111);
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize.BuildingtypeBEnd, colName110);
		

		}

		if (ProductType.equalsIgnoreCase("Ethernet Line")) {
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.CPEInformationLink,"CPE Information Link");
			Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize.CabinetTypeBEnd, colName113);
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.CabinetIdBEnd,"Verify on Cabinet Type B End");
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize.CabinetIdBEnd);
			
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.CabinetIdBEnd,"Enter Cabinet Id BEnd",colName114);
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ShelfID);
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.ShelfID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize. ShelfID, Keys.TAB);

			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessPortLink,"Verify on Access Port Link");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessPortLink,"click on Access Port Link");

			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.PresentationInterfaceSearch,"Verify on Presentation Interface Search");
			ClickonElementByString(
					"//table[@summary='Select Presentation Interface-Connector Type']//tr//td[text()='"
							+ colName116 + "']", 30);
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SubmitPresentationInterface,"Click on Submit Presentation Interface");
			
			
			Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize. PortRoleBEnd, colName115);
			
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SlotID);
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SlotID, "1234");
			
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize.PhysicalPortID);
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.PhysicalPortID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.PhysicalPortID, Keys.TAB);

			

			if (colName115.equalsIgnoreCase("Physical Port")) {
			} else {
				verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VLANTaggingMode,"verify on VLAN Tagging Mode");
				click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VLANTaggingMode,"Click on VLAN Tagging Mode");
				Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VLANTaggingMode, colName117);
				
				verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VLAN,"verify on VLAN link");
				click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VLAN,"Click on VLAN link");
				clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VlanTagId);
				sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VlanTagId, colName118);
				Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize.VlanTagId, Keys.ENTER);
				Reusable.savePage();
				Reusable.waitForSiebelLoader();
			}
		}
		
		
		if (productName.equalsIgnoreCase("Wave")) {
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.Terminationinformation);
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.Terminationinformation);

			Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize. CabinetTypeBEnd, colName113);
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize. CabinetIdBEnd);
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize. CabinetIdBEnd);

			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize. CabinetIdBEnd);
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize. CabinetIdBEnd, colName114);
			
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Cabinet Id BEnd");
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize. ShelfID);
			
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize. ShelfID, "1234");
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize. ShelfID, Keys.TAB);
			
			Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize.AccessPortItem, "Access Port");

			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize. AccessPortLink,"Verify on Access Port Link");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize. AccessPortLink,"Click on Access Port Link");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.PresentationInterfaceSearch,"Verify on Presentation Interface Search");
			ClickonElementByString("//table[@summary='Select Presentation Interface-Connector Type']//tr//td[text()='"+ colName99 + "']", 30);
			
			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SubmitPresentationInterface,"Verify on Submit Presentation Interface");
			click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SubmitPresentationInterface,"Click on Submit Presentation Interface");
			
			Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize.PortRole, "Physical Port");

			verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.SlotID,"Verify Slot Id");
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize. SlotID);
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize. SlotID, " Enter Slot Id 1234");
			
			clearTextBox(SiebelNewOrderOnnetHelperObj.BSiteCustomize. PhysicalPortID);
			sendKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize. PhysicalPortID, "Enter Physical Port Id 1234");
			
			Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.BSiteCustomize. PhysicalPortID, Keys.TAB);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
		}

		verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.BEndSite,"B End Site");
		click(SiebelNewOrderOnnetHelperObj.BSiteCustomize. BEndSite,"Click on B End Site");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize.InstallationTimeLink,"InstallationTimeLink");
		click(SiebelNewOrderOnnetHelperObj.BSiteCustomize.InstallationTimeLink," Click on Installation Time Link");
		Reusable.Select1(SiebelNewOrderOnnetHelperObj.BSiteCustomize. InstallTimeBEnd, colName112);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize. InstallTimeBEnd);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Install Time BEnd");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.BSiteCustomize. DoneEthernetConnection,"Verify on Done Ethernet Connection");
		click(SiebelNewOrderOnnetHelperObj.BSiteCustomize. DoneEthernetConnection,"Click on Done Ethernet Connection");
		Reusable.waitForSiebelLoader();

	}
}
public void addEthernetSiteHub(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City");
String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PostalCode");
String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
String InputServicePartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InputService_PartyName");
String InputContactName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InputContact_Name");
String AddressTable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Address_Table");

if (ProductName.equalsIgnoreCase("Ethernet Hub")
		|| ProductName.equalsIgnoreCase("Ethernet Spoke")) {

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Searchbutton,"verify on Search button");
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Searchbutton,"click on Search button");

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.StreetName,StreetName );
	sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.StreetName,StreetName );
	
	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Country, Country);
	sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Country, Country);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.City,  City);
	sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.City,  City);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.PostalCode,PostalCode);
    sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.PostalCode,PostalCode);
	
    verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Premises,Premises);
	sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Premises,Premises);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Search);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Search);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.pickAddress);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.pickAddress);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Pick);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Pick);

	ClickonElementByString(
			"//table[@id='address_table']//tr//td[contains(text(),'" + AddressTable + "')]", 30);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Pick);
	ClickonElementByString(
			"//table[@id='address_table']//tr//td[contains(text(),'" + AddressTable + "')]", 30);
	
	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Pick);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.Pick);
	
	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.ServicePartySearch,"verify on Service Party Search");
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.ServicePartySearch,"click on Service Party Search");

	Reusable.Select1(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.SelectServiceParty, "Party Name");

	sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.InputServicePartyName, InputServicePartyName);
	sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.InputServicePartyName, InputServicePartyName);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.SearchServicePartyName);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.SearchServicePartyName);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.pickServiceParty);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.pickServiceParty);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.PickButton);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.PickButton);

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.SiteContactSearch);
	sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.InputContactName, InputContactName);
	
	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.SearchContactName,"verify site Contact search name");
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.SearchContactName,"click site Contact search name");

	verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.pickContactName);
	click(SiebelNewOrderOnnetHelperObj.addEthernetSiteHub.PickContact);
}
}

public void addEthernetSiteSpoke(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

String ColName103 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
String ColName104 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
String ColName105= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City");
String ColName106= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PostalCode");
String ColName107= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
String ColName108 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InputService_PartyName");
String ColName109= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InputContact_Name");
String ColName16 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Address_Table");
String ColName17 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Address_Table");

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SearchAddressSiteB);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SearchAddressSiteB);

sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.StreetName, ColName103);
sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Country, ColName104);
sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.City, ColName105);
sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.PostalCode, ColName106);
sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Premises, ColName107);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Search,"Search");
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Search);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.pickAddress);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.pickAddress);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Pick);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Pick);
ClickonElementByString(
		"//table[@id='address_table']//tr//td[contains(text(),'" + ColName108 + "')]",30);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Pick);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Pick);
ClickonElementByString(
		"//table[@id='address_table']//tr//td[contains(text(),'" + ColName109 + "')]",30);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Pick);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.Pick);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.ServicePartySearchSiteB);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.ServicePartySearchSiteB);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SelectServiceParty, "Party Name");
Reusable.Select1(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SelectServiceParty, "Party Name");
Reusable.waitForSiebelLoader();

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.InputServicePartyName, ColName16); // Domain
sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.InputServicePartyName, ColName16); // Domain
																									// Name
verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SearchServicePartyName);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SearchServicePartyName);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.pickServiceParty);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.pickServiceParty);

click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.PickButton);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.PickButton);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SiteContactSearchSiteB);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SiteContactSearchSiteB);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.InputContactName, ColName17); // DNS Type
sendKeys(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.InputContactName, ColName17); // DNS Type
																										// Input
verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SearchContactName);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.SearchContactName);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.pickContactName);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.pickContactName);

verifyExists(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.PickContact);
click(SiebelNewOrderOnnetHelperObj.addEthernetSiteSpoke.PickContact);
}
public void AEndInputEnter(String TextBoxName, String InputText) throws InterruptedException, IOException
{
waitToPageLoad();
Reusable.waitForSiebelLoader();		
verifyExists(SiebelNewOrderOnnetHelperObj.AEndInputEnter.SiteAInput.replace("Value", TextBoxName));
clearTextBox(SiebelNewOrderOnnetHelperObj.AEndInputEnter.SiteAInput.replace("Value", TextBoxName));
sendKeys(SiebelNewOrderOnnetHelperObj.AEndInputEnter.SiteAInput.replace("Value", TextBoxName),InputText);
Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.AEndInputEnter.SiteAInput.replace("Value", TextBoxName),Keys.ENTER);

}
public void BEndInputEnter(String TextBoxName, String InputText) throws InterruptedException,IOException
{
waitToPageLoad();
Reusable.waitForSiebelLoader();		
verifyExists(SiebelNewOrderOnnetHelperObj.BEndInputEnter.SiteBInput.replace("Value", TextBoxName));
clearTextBox(SiebelNewOrderOnnetHelperObj.BEndInputEnter.SiteBInput.replace("Value", TextBoxName));
sendKeys(SiebelNewOrderOnnetHelperObj.BEndInputEnter.SiteBInput.replace("Value", TextBoxName),InputText);
Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.BEndInputEnter.SiteBInput.replace("Value", TextBoxName),Keys.ENTER);

}
public void AlertAccept() throws  InterruptedException, IOException {
if (isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.AlertAccept.AlertAccept))) {
	verifyExists(SiebelNewOrderOnnetHelperObj.AlertAccept.AlertAccept,"Verify Alert Accept");
	click(SiebelNewOrderOnnetHelperObj.AlertAccept.AlertAccept,"click on Alert Accept");
}
}
public void alertPopUp() throws  InterruptedException, IOException
{
if (isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.AlertAccept.AlertAccept))) {
verifyExists(SiebelNewOrderOnnetHelperObj.AlertAccept.AlertAccept,"Verify Alert Accept");
click(SiebelNewOrderOnnetHelperObj.AlertAccept.AlertAccept,"click on Alert Accept");
}
}
public void AppendData(String Path, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
ThreadLocal<String> ServiceOrder2 = new ThreadLocal<>();
ThreadLocal<String> ServiceOrder = new ThreadLocal<>();

try {

	FileInputStream fis = new FileInputStream(new File(Path));
	XSSFWorkbook workbook = new XSSFWorkbook(fis);
	// XSSFSheet sheet = workbook.
	int i = workbook.getNumberOfSheets();
	XSSFSheet sheet = workbook.getSheetAt(0);
	int num = sheet.getLastRowNum();
	Row row = sheet.createRow(++num);
	String OrderNumber = ProductName.equalsIgnoreCase("IP VPN Service")? ServiceOrder2.get(): ServiceOrder.get();
	row.createCell(0).setCellValue(ProductName);
	row.createCell(1).setCellValue(OrderNumber);
	row.createCell(2).setCellValue(Reusable.CurrentDate());
	fis.close();
	FileOutputStream fos = new FileOutputStream(Path);
	workbook.write(fos);
	fos.close();
} catch (Exception e) {
}
}
public void BEndDropdownSelection(String DropdownName, String DropdownValue) throws InterruptedException, IOException
{
String eleLoct=SiebelNewOrderOnnetHelperObj.BEndDropdownSelection.SiteBDropdownClick.replace("Value", DropdownName);
verifyExists(eleLoct);
click(eleLoct);

eleLoct=SiebelNewOrderOnnetHelperObj.BEndDropdownSelection.SiteABSelection.replace("Value", DropdownValue);
waitToPageLoad();
Reusable.waitForSiebelLoader();
verifyExists(eleLoct);
click(eleLoct);
}
public void Carnor_getReferenceNo1(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception, Exception {

String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

	if (ProductName.equalsIgnoreCase("Ethernet VPN Access")
	|| ProductName.equalsIgnoreCase("Dark Fibre")
	|| ProductName.equalsIgnoreCase("Ultra Low Latency")
	|| ProductName.equalsIgnoreCase("Private Ethernet")
	|| ProductName.equalsIgnoreCase("DCA Ethernet")
	|| ProductName.equalsIgnoreCase("Private Wave Service")
	|| ProductName.equalsIgnoreCase("IP Access")
	|| ProductName.equalsIgnoreCase("IP VPN Service"))
	{
	verifyExists(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.CircuitReferenceAccess,"Verify Circuit Reference Access");
	click(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.CircuitReferenceAccess,"click on Circuit Reference Access");

	Reusable.Save();
	Reusable.waitForSiebelLoader();

	Circuitreferencenumber.set(getTextFrom(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.CircuitReferenceValue));
	}
	

	 //for enter value in circuit reference
	 else if(ProductName.contains("Ethernet Access"))
	 {
		 verifyExists(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.NetworkTopology,"verify on Network Topology");
		 click(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.NetworkTopology,"click on Network Topology");
		 Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.NetworkTopology, Keys.TAB);
	 sendKeys(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.CircuitReference,"2");
	 Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.Carnor_getReferenceNo1.CircuitReference, Keys.TAB);
	 Reusable.Save();
	 Reusable.waitForSiebelLoader();
	 }
	
}
public void CheckServiceTab(Object[] Inputdata) throws Exception {

Reusable.waitForSiebelLoader();

	
	try				// By   Aman Gupta
	{
		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.ModifyButtonClick,"Verify on Modify Button Click");
		click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.ModifyButtonClick,"Click on Modify Button Click");
	}
	catch(Exception e)
	{
		
		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.ModifyBtn,"Verify on Modify Btn");
		click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.ModifyBtn,"Click on Modify Btn");
		
	}
	
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.OpportunityNo, Inputdata[1].toString());
	sendKeys(SiebelNewOrderOnnetHelperObj.CheckServiceTab.OpportunityNo, Inputdata[1].toString());

	verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.RequestReceivedDate, Reusable.CurrentDate());
	sendKeys(SiebelNewOrderOnnetHelperObj.CheckServiceTab.RequestReceivedDate, Reusable.CurrentDate());

	ModifiedServiceOrder.set(getTextFrom(SiebelNewOrderOnnetHelperObj.CheckServiceTab.ModifyOrderNumber));// Added by
																										// Rekha
	verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.ServiceOrderClickOn," verify on Service Order Click On");
	click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.ServiceOrderClickOn," click on Service Order Click On");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.OrderSubTypeSearch);
	click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.OrderSubTypeSearch,"Click on Order Sub Type Search");

	verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.AddOrderSubType,"Verify on Add Order Sub Type");
	click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.AddOrderSubType,"Click on Add Order Sub Type");
	Reusable.waitForSiebelLoader();
	// ---> Condition need to be added for mod com and mod tech

	if (Inputdata[Inputdata.length - 1].toString().contains("Com")
			|| Inputdata[Inputdata.length - 1].toString().contains("Carnor")) // Added by Dipesh

	{
		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.InputOrderSubType, "BCN Change");// Specific for mod
		sendKeys(SiebelNewOrderOnnetHelperObj.CheckServiceTab.InputOrderSubType, "BCN Change");																						// com
	} else if (Inputdata[Inputdata.length - 1].toString().contains("Tech")) {
		
		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.InputOrderSubType, "Upgrade Bandwith"); // specific for
		sendKeys(SiebelNewOrderOnnetHelperObj.CheckServiceTab.InputOrderSubType, "Upgrade Bandwith");																								// mod tech
	}
	Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.CheckServiceTab.InputOrderSubType, Keys.ENTER);
	verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.SubmitSubOrderType,"verify on Submit Sub Order Type");
	click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.SubmitSubOrderType,"Click on Submit Sub Order Type");
	Reusable.waitForSiebelLoader();
	

	if (!Inputdata[1].toString().equalsIgnoreCase("Wave")
			&& !Inputdata[1].toString().equalsIgnoreCase("Ether Line")) // added shivananda
	{
		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.MaintenancePartySearch,"Verify on Maintenance PartySearch");
		click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.MaintenancePartySearch,"Click on Maintenance Party Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.MaintenancePartyPopupDropdown,"verify on Maintenance Party Popup Dropdown");
		click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.MaintenancePartyPopupDropdown,"click on Maintenance Party Popup Dropdown");
		
		click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.SelectValueDropdown.replace("Value", "Party Name"));
		sendKeys(SiebelNewOrderOnnetHelperObj.CheckServiceTab.InputAccountStatus, "Colt");
		
		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.AccountStatusSearch,"Verify on Account Status Search ");
		click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.AccountStatusSearch,"click on Account Status Search ");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelNewOrderOnnetHelperObj.CheckServiceTab.AccountStatusSubmit,"verify on Account Status Submit ");
		click(SiebelNewOrderOnnetHelperObj.CheckServiceTab.AccountStatusSubmit,"click on Account Status Submit ");
		Reusable.waitForSiebelLoader();

	}
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
}
public void Check1(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

String ServiceOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");

//ServiceOrder.get();
//String x = ServiceOrder.get();
// String string = "004-034556";
String[] parts = ServiceOrder.split("/");
String part1 = parts[0];
String part2 = parts[1];

verifyExists(SiebelNewOrderOnnetHelperObj.Check1.ClickLink.replace("Value", "Customer Orders"));
click(SiebelNewOrderOnnetHelperObj.Check1.ClickLink.replace("Value", "Customer Orders"));

Reusable.waitForSiebelLoader();
verifyExists(SiebelNewOrderOnnetHelperObj.Check1.CustomerOrderInput, part2);
sendKeys(SiebelNewOrderOnnetHelperObj.Check1.CustomerOrderInput, part2);
click(SiebelNewOrderOnnetHelperObj.Check1.CustomerOrderSearch,"Click on Customer Order Search");

}

/*
* Added as per Ayush
*/
public void Check(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

do {
	Reusable.Pagerefresh();
	
} while (!isElementPresent(By.xpath(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderTab)));

try {
	verifyExists(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderTab);
	click(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderTab);
} catch (Exception e) {
	try {
		verifyExists(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderTab);
		click(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderTab);
	} catch (Exception e1) {

		e1.printStackTrace();
	}
}
verifyExists(SiebelNewOrderOnnetHelperObj.Check.InputServiceOrder);
sendKeys(SiebelNewOrderOnnetHelperObj.Check.InputServiceOrder, "871519413/190805-0060");

verifyExists(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderGo);
click(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderGo);

verifyExists(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderClickOn,"Verify on service order");
click(SiebelNewOrderOnnetHelperObj.Check.ServiceOrderClickOn,"Click on service order");

}
public void ClickSection(String SectionName) throws InterruptedException, IOException
{
String temp=SiebelNewOrderOnnetHelperObj.ClickSection.SectionSelection.replace("Value",SectionName);
Reusable.waitForSiebelLoader();
verifyExists(SiebelNewOrderOnnetHelperObj.ClickSection.SectionSelection.replace("Value",SectionName));
click(SiebelNewOrderOnnetHelperObj.ClickSection.SectionSelection.replace("Value",SectionName));
}
public void closePopUp() throws Exception {

verifyExists(SiebelNewOrderOnnetHelperObj.closePopUp.PopClose);
click(SiebelNewOrderOnnetHelperObj.closePopUp.PopClose);
}
public void CommercialValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

clearTextBox(SiebelNewOrderOnnetHelperObj.CommercialValidation.OrderStatus);
sendKeys(SiebelNewOrderOnnetHelperObj.CommercialValidation.OrderStatus, "Commercial Validation");
Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.CommercialValidation.OrderStatus, Keys.ENTER);
Reusable.SendkeaboardKeys(SiebelNewOrderOnnetHelperObj.CommercialValidation.OrderStatus, Keys.TAB);
Reusable.waitForSiebelLoader();
Reusable.waitForSiebelLoader();

}
}