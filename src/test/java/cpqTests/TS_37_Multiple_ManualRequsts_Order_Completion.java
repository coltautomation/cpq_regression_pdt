package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_37_Multiple_ManualRequsts_Order_Completion extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	GlobalVariables g = new GlobalVariables();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testOffnetOrderCompletion(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			
			Accounts.NavigateToAccounts();
			System.out.println("scriptNo: "+scriptNo);
			System.out.println("dataSetNo: "+dataSetNo);System.out.println("dataSheet: "+dataSheet);
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);			
			
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String Flow_Type=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String sResult;
			if(ProductName.equalsIgnoreCase("Wave")) {
				
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Created");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method to add Legal and Technical Contact Info
				String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
				sResult = Approvals.SwitchCPQUser("SE_User", Quote_ID);

				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(ProductName);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//				calling the below method to configure the product
				sResult = AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
					
//				calling the below method to update and save the product in cpq
				sResult = AddProduct.updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
			}
//          Verify the Quote Stage
			if (Flow_Type.equalsIgnoreCase("ManualNearnet"))
			{
				AddProduct.verifyQuoteStage("Waiting for BCP");
			}
			else
			{
				AddProduct.verifyQuoteStage("Waiting for 3rd Party");
			}
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
			String CopyQuote = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CopyQuote");						
			
			if (Flow_Type.equalsIgnoreCase("ManualDiverse")||Flow_Type.equalsIgnoreCase("ManualDSL")||Flow_Type.equalsIgnoreCase("ManualNearnet")||Flow_Type.equalsIgnoreCase("ManualOffnet"))
			{																							
				if (CopyQuote.equalsIgnoreCase("Yes"))
				{
					AddProduct.copyQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
					DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID",Quote_ID);
					
					AddProduct.clickProductConfigurationBtn(ProductName);												 
					
						if(Flow_Type.equalsIgnoreCase("ManualOffnet"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.offnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								break;
								
							case "EthernetLine":
								AddProduct.offnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								AddProduct.offnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}														
						}
						else if(Flow_Type.equalsIgnoreCase("ManualNearnet"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.nearnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								break;
								
							case "EthernetLine":
								AddProduct.nearnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								AddProduct.nearnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}																	
						}
						else if(Flow_Type.equalsIgnoreCase("ManualDSL"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.dslEntries_MultiRequest(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");	
								break;
							case "EthernetLine":
								AddProduct.dslEntries_MultiRequest(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");	
								AddProduct.dslEntries_MultiRequest(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}														
						}
						else if(Flow_Type.equalsIgnoreCase("ManualDiverse"))
						{
							switch(ProductName) {
							
							case "EthernetSpoke": case "ColtIpAccess":
								AddProduct.ManualDiverseEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");
								break;
								
							case "EthernetLine":
								AddProduct.ManualDiverseEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "A_End");	
								AddProduct.ManualDiverseEntries(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End");	
								break;
							}															
						}
					
					if(ProductName.equalsIgnoreCase("Wave"))
					{	
						Approvals.SwitchCPQUser("SE_User", Quote_ID);
						AddProduct.clickProductConfigurationBtn(ProductName);
						AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);							
					}							
					AddProduct.updateSaveProductCPQ("Save");							
				}
					CPQLogin.CPQLogout();
							
				//	calling the below method to Reject the Request in Explorer
						close();
						openBrowser(g.getBrowser());	
						openurl(Configuration.EXPLORE_URL);
						if(Flow_Type.equalsIgnoreCase("ManualNearnet"))
						{
							ExpConfig.nearnetExploreRejectOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						}
						else { ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo); }	
														
				//		Calling the Below method to perform C4C Login and navigage to the Quote in CPQ	
						close();
						openBrowser(g.getBrowser());								
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
						CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);										
																				
				// 		calling the below method to verify the Status of Manual Requests in CPQ and Recreate the Manual Request
						switch(ProductName) {
						
						case "EthernetSpoke": case "ColtIpAccess":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							AddProduct.updateSaveProductCPQ("Save");
							break;
							
						case "EthernetLine": case "Wave":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							AddProduct.markPartialSave(ProductName);
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"B_End");
							AddProduct.updateSaveProductCPQ("Save");
							AddProduct.verifyQuoteStage("Waiting for 3rd Party");	
							break;
						}							
										
				//		calling the below method to configure the product
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						if(Flow_Type.equalsIgnoreCase("ManualNearnet"))
						{
							ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						}
						else { ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo); }					
																				
				//		Calling the Below method to perform C4C Login	
						close();
						openBrowser(g.getBrowser());			
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
						CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
											
														
				// 		calling the below method to verify the Status of Manual Requests in CPQ and Recreate the Manual Request
						switch(ProductName) {
						
						case "EthernetSpoke": case "ColtIpAccess":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							break;
							
						case "EthernetLine": case "Wave":
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"A_End");
							AddProduct.ViewManualRequest_Status(ProductName,testDataFile, dataSheet, scriptNo, dataSetNo,"B_End");
							break;
						}												
					}			
				
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
				String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
				System.out.println("quoteStage: "+quoteStage);
				if (quoteStage.equalsIgnoreCase("SE Required")) 
				{
					AddProduct.verifyQuoteStage("SE Required");
					AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo); 
					//Login with CPQ SE user
					close();
					openBrowser(g.getBrowser());		
					openurl(Configuration.CPQ_URL);
					CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
					CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
					Approvals.SwitchCPQUser("Sales_User", Quote_ID);
				}
		
				AddProduct.verifyQuoteStage("Priced");
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);				
				
				if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
				{
				String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
				Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
				} else {
				String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
				Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
				}			
				
				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
				Submit.SubmitOrder();
				AddProduct.verifyQuoteStage("Ordered");
				Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
		
	}	
	
}
