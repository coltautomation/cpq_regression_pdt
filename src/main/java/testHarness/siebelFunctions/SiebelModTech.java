package testHarness.siebelFunctions;

import java.io.IOException;
import java.util.Random;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelModTechObj;
import pageObjects.siebelObjects.SiebelModeObj;
import pageObjects.siebelObjects.SiebelSpokeSiteObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelModTech extends SeleniumUtils {
	SiebelMode siebelmod = new SiebelMode();
	Random rand = new Random();
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();
	Random random = new Random();
	public void ModTech(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		switch (ProductName) 
		{
	
		case "SIP Trunking": {
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modSipTrunkingMiddleapplet(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		
		case "Private Web Service":
		{

			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrivateWebServiceFibreMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrivateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			waitToPageLoad();
			Reusable.waitForSiebelLoader();	
			break;
		
		}
		
		
		case "IP VPN Service":
		{

			String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
			String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
			
			if(Samle.equalsIgnoreCase("IP VPN Wholesale"))
			{
				modTechPrizmNetMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
				Reusable.savePage();
			}
			if(Samle.equalsIgnoreCase("IP VPN Access"))
			{
				modTechPrizmNetMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
				Reusable.savePage();
			}
			if(Samle.equalsIgnoreCase("IP VPN Plus"))
			{
				modTechPrizmNetMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
				Reusable.savePage();
			}
			if(Samle.equalsIgnoreCase("SWIFTNet"))
			{
				modTechPrizmNetMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
				Reusable.savePage();
			}
			if(Samle.equalsIgnoreCase("PrizmNet"))
			{
				modTechPrizmNetMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
				Reusable.savePage();
			}
			break;
		}

		case "CPE Solutions Service":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechMiddleAppletCPESolutionService(testDataFile, sheetName, scriptNo,dataSetNo);
			
			break;
		
		}
	
		case "Ethernet Access":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechEthernetAccessMiddleApplet();
			modTechPrivateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			break;
		
		}
		
		case "Dark Fibre":
				{
					modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
					modTechDarkFibreMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);
					modTechDarkFibreA(testDataFile, sheetName, scriptNo,dataSetNo);
					modTechDarkFibreB(testDataFile, sheetName, scriptNo,dataSetNo);
					Reusable.waitForSiebelLoader();
					Reusable.savePage();
					waitToPageLoad();
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					break;
				
				}
		case "Private Wave Service":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrivateWebServiceFibreMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrivateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			break;
		
		}
	
		case "Private Wave Node":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrivateWaveNodeMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrivateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			Reusable.savePage();
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			break;
		
		}
		
		case "Private Ethernet": {
			ModTechR4(testDataFile, sheetName, scriptNo,dataSetNo);
			ModTechPrivateEthernet(testDataFile, sheetName, scriptNo,dataSetNo);
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			break;
		}
		
		case "Ultra Low Latency":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechultraLowLatencyMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrivateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			break;
		
		}
		//Added By  AMAN GUPTA
		
		case "Ethernet Line": 
		{
			
			String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
			String Aend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
			String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");

/*			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.CustomizeButton,"CustomizeButton");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.CustomizeButton,"CustomizeButton");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.CustomizeServiceBandwidth,"CustomizeServiceBandwidth");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.CustomizeServiceBandwidth,"CustomizeServiceBandwidth");
			Reusable.Select1(SiebelModTechObj.modTechDarkFibreMiddleApplet.CustomizeServiceBandwidth, Samle);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.Connectionlink,"Connectionlink");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.Connectionlink,"Connectionlink");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.AendSiteLink,"AendSiteLink");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.AendSiteLink,"AendSiteLink");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.InstallationTimeLink,"InstallationTimeLink");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.InstallationTimeLink,"InstallationTimeLink");
			sendKeys(SiebelModTechObj.modTechDarkFibreMiddleApplet.InstallTime, Aend_Install_Time,"InstallTime");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.Connectionlink,"Connectionlink");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.Connectionlink,"Connectionlink");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.BendSiteLink,"BendSiteLink");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.BendSiteLink,"BendSiteLink");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.InstallationTimeLink,"InstallationTimeLink");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.InstallationTimeLink,"InstallationTimeLink");
			sendKeys(SiebelModTechObj.modTechDarkFibreMiddleApplet.InstallTime, Bend_Install_Time,"Update Installation time");
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.DoneEthernetConnection,"DoneEthernetConnection");
			click(SiebelModTechObj.modTechDarkFibreMiddleApplet.DoneEthernetConnection,"DoneEthernetConnection");
			Reusable.waitForSiebelLoader();
		*/	break;
			}
		
		case "Wave": 
		{
			String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
			String AendInstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
			String BendInstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");
/*

			verifyExists(SiebelModTechObj.Wave.CustomizeButton,"CustomizeButton");
			click(SiebelModTechObj.Wave.CustomizeButton,"Clicked on Customize button");

			verifyExists(SiebelModTechObj.Wave.CustomizeServiceBandwidth,"CustomizeServiceBandwidth");
			click(SiebelModTechObj.Wave.CustomizeServiceBandwidth,"CustomizeServiceBandwidth");
			Reusable.Select1(SiebelModTechObj.Wave.CustomizeServiceBandwidth, Samle);
			Reusable.savePage();
			verifyExists(SiebelModTechObj.Wave.Connectionlink,"Connectionlink");
			click(SiebelModTechObj.Wave.Connectionlink,"Click on Ethernet Connection link");
			verifyExists(SiebelModTechObj.Wave.AendSiteLink,"AendSiteLink");
			click(SiebelModTechObj.Wave.AendSiteLink,"Click on Aend Site link");
			verifyExists(SiebelModTechObj.Wave.InstallationTimeLink,"InstallationTimeLink");
			click(SiebelModTechObj.Wave.InstallationTimeLink,"InstallationTimeLink");
			sendKeys(SiebelModTechObj.Wave.InstallTime, AendInstallTime,"Update Installation time");
			verifyExists(SiebelModTechObj.Wave.Connectionlink,"Connectionlink");
			click(SiebelModTechObj.Wave.Connectionlink,"Click on Ethernet Connection link");
			verifyExists(SiebelModTechObj.Wave.BendSiteLink,"BendSiteLink");
			click(SiebelModTechObj.Wave.BendSiteLink,"Click on Bend Site link");
			verifyExists(SiebelModTechObj.Wave.InstallationTimeLink,"InstallationTimeLink");
			click(SiebelModTechObj.Wave.InstallationTimeLink,"InstallationTimeLink");
			sendKeys(SiebelModTechObj.Wave.InstallTime, BendInstallTime,"Update Installation time");
			Reusable.savePage();
			verifyExists(SiebelModTechObj.Wave.DoneEthernetConnection,"DoneEthernetConnection");
			click(SiebelModTechObj.Wave.DoneEthernetConnection,"Click on Done Ethernet Connection");
		*/	break;
		}
		
		case "DCA Ethernet": {
			ModTechR4(testDataFile, sheetName, scriptNo,dataSetNo);
			ModTechPrivateEthernet(testDataFile, sheetName, scriptNo,dataSetNo);
			Reusable.savePage();
			break;
			
		}

		case "IP Guardian": {

			String OrderingPartyAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ordering_Party_Address");
			String OrderingPartyContract = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ordering_Party_Contract");
			String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
			String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModTechObj.IPGuardianObj.AlertingNotification,"Verify AlertingNotification");
			sendKeys(SiebelModTechObj.IPGuardianObj.AlertingNotification,OrderingPartyAddress,OrderingPartyAddress);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModTechObj.IPGuardianObj.Customerdnsresolve,"Verify Customerdnsresolve");
			sendKeys(SiebelModTechObj.IPGuardianObj.Customerdnsresolve,OrderingPartyContract,OrderingPartyContract);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModTechObj.IPGuardianObj.servicebandwidth,"Verify servicebandwidth");
			sendKeys(SiebelModTechObj.IPGuardianObj.servicebandwidth,ServiceBandwidth,"Servicebandwidth");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModTechObj.IPGuardianObj.Testwindowipaccess,"Verify TestWindow");
			sendKeys(SiebelModTechObj.IPGuardianObj.Testwindowipaccess,TestWindow,"Test Window");
			Reusable.waitForSiebelLoader();
			
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();


			break;
			}
		case "Voice Line V": {
			String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");

			Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelModTechObj.Mode.TotalNumberDDIs, 10);
			verifyExists(SiebelModTechObj.Mode.TotalNumberDDIs,"Total DDi");	
			click(SiebelModTechObj.Mode.TotalNumberDDIs,"Total DDi");	
			sendKeys(SiebelModTechObj.Mode.TotalNumberDDIs, "6", "Total DDi");
			Reusable.SendkeaboardKeys(SiebelModTechObj.Mode.TotalNumberDDIs, Keys.ENTER);
			Reusable.Save();
			Reusable.waitForSiebelLoader();
			break;
		}
		case "IP Access": {
			
			String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
			  String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
	    	  String Layer3Resilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Layer_3_Resilience");
	    	
			waitForElementToAppear(SiebelModTechObj.Mode.RouterTypeDropdownAccess, 10);
			verifyExists(SiebelModTechObj.Mode.RouterTypeDropdownAccess,"RouterTypeDropdownAccess");	
			click(SiebelModTechObj.Mode.RouterTypeDropdownAccess,"RouterTypeDropdownAccess");	
			
			verifyExists(SiebelModTechObj.Mode.searchInput.replace("Value",RouterType));
			click(SiebelModTechObj.Mode.searchInput.replace("Value",RouterType));
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			
		//	verifyExists(SiebelModTechObj.Mode.SelectRouterTypeDropDownAccess,"Router Type Drop Down");	
		//	click(SiebelModTechObj.Mode.SelectRouterTypeDropDownAccess,"Router Type Drop Down");	
			
			
			verifyExists(SiebelModTechObj.Mode.Layer3ResillanceDropdownAccess,"Layer 3 Resillance Dropdown");	
			click(SiebelModTechObj.Mode.Layer3ResillanceDropdownAccess,"Layer 3 Resillance Dropdown");	
			
			verifyExists(SiebelModTechObj.Mode.searchInput.replace("Value",Layer3Resilience),"Layer 3 Resillance Dropdown value");
			click(SiebelModTechObj.Mode.searchInput.replace("Value",Layer3Resilience),"Layer 3 Resillance Dropdown value");
			Reusable.waitForSiebelLoader();
			
		//	verifyExists(SiebelModTechObj.Mode.Layer3ResillanceSelectDropdownAccess,"Layer 3 Resillance Dropdown value");	
		//	click(SiebelModTechObj.Mode.Layer3ResillanceSelectDropdownAccess,"Layer 3 Resillance Dropdown value");	
		
		
			verifyExists(SiebelModTechObj.Mode.ServiceBandwidthDropdownAccess,"Service Bandwidth Dropdown");	
			click(SiebelModTechObj.Mode.ServiceBandwidthDropdownAccess,"Service Bandwidth Dropdown");
			
			verifyExists(SiebelModTechObj.Mode.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth),"ServiceBandwidth");
			click(SiebelModTechObj.Mode.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth),"ServiceBandwidth");
			Reusable.waitForSiebelLoader();	
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			break;
		}
		case "Interconnect": {
			
			String CallAdmissionControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallAdmissionControl");
			String NumberOfSignallingTrunks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfSignallingTrunks");


			System.out.println("go to else loop of call admission control");
	
			verifyExists(SiebelModTechObj.InterconnectObj.CallAdmissionControl,"Verify CallAdmissionControl");
			sendKeys(SiebelModTechObj.InterconnectObj.CallAdmissionControl,CallAdmissionControl,"CallAdmissionControl");
			Reusable.waitForSiebelLoader();
			
			Reusable.SendkeaboardKeys(SiebelModTechObj.InterconnectObj.CallAdmissionControl, Keys.TAB);
			verifyExists(SiebelModTechObj.InterconnectObj.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
			sendKeys(SiebelModTechObj.InterconnectObj.NumberOfSignallingTrunks,NumberOfSignallingTrunks,"NumberOfSignallingTrunks Value");
			Reusable.waitForSiebelLoader();
			
			Reusable.SendkeaboardKeys(SiebelModTechObj.InterconnectObj.NumberOfSignallingTrunks, Keys.TAB);
			
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			break;
			}
		
		case "Cloud Unified Communications":

		{
			ModTechforcommanproduct(testDataFile, sheetName, scriptNo,dataSetNo);

			break;
		}
		case "Professional Services":
		{
			ModTechforcommanproduct(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "IP Voice Solutions":
		{
			ModTechforcommanproduct(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "Ethernet Spoke": {

			String Alerting_Notification_Email_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String InstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelModTechObj.EthernetSpokeObj.ClickDropdown.replace("Value", "Resilience Option"));
			click(SiebelModTechObj.EthernetSpokeObj.ClickDropdown.replace("Value", "Resilience Option"));
			verifyExists(SiebelModTechObj.EthernetSpokeObj.SelectValueDropdown.replace("Value",Alerting_Notification_Email_Address));
			click(SiebelModTechObj.EthernetSpokeObj.SelectValueDropdown.replace("Value",Alerting_Notification_Email_Address));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModTechObj.EthernetSpokeObj.ClickDropdownB.replace("Value", "Install Time"));
			click(SiebelModTechObj.EthernetSpokeObj.ClickDropdownB.replace("Value", "Install Time"));
			verifyExists(SiebelModTechObj.EthernetSpokeObj.SelectValueDropdown.replace("Value",InstallTime));
			click(SiebelModTechObj.EthernetSpokeObj.SelectValueDropdown.replace("Value",InstallTime));
			Reusable.waitForSiebelLoader();
			if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']")))
			{
			verifyExists(SiebelModTechObj.EthernetSpokeObj.SaveOrderChanges,"Verify SaveOrderChanges");
			click(SiebelModTechObj.EthernetSpokeObj.SaveOrderChanges,"Click on SaveOrderChanges");
			System.out.println("page load succesfuuly now come to middle applet");
			}
			else
			{
			Reusable.savePage();
			}
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			break;
			}
		case "Ethernet Hub": {
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modEthernetHubMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);	
			break;
		}
		case "Number Hosting": {
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechNumberHosting(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "PrizmNet":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechPrizmNetMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);	
			break;
		}
		case "SWIFTNet":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechSwiftNetMiddleApplet();
			modTechDarkFibreA(testDataFile, sheetName, scriptNo,dataSetNo);		
			break;
		}
		
		case "Managed Dedicated Firewall":
		{
			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			modTechMangedDeicatedFirewall(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
			
		}
		case "Ethernet VPN Access": 
		{

			modHeader(testDataFile, sheetName, scriptNo,dataSetNo);
			Reusable.waitForSiebelSpinnerToDisappear();
			modTechPrivateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
		//	Reusable.ClickHereSave();
			Reusable.savePage();
			ethernetVpnAccessMiddleApplet(testDataFile, sheetName, scriptNo,dataSetNo);
			Reusable.savePage();
		//	Reusable.ClickHereSave();
			Reusable.waitForSiebelLoader();	
			Reusable.waitForSiebelSpinnerToDisappear();
			break;
		}
		

		default: {
			break;
		}
		}
}
	public void modHeader(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String ExistingCapacityLeadTimePrimary = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Capacity_ Lead_Time_Primary");

		
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelModTechObj.Mode.PrimaryLeadInput, 10);
		verifyExists(SiebelModTechObj.Mode.PrimaryLeadInput,"Primary Lead Input");	
		clearTextBox(SiebelModTechObj.Mode.PrimaryLeadInput);
		sendKeys(SiebelModTechObj.Mode.PrimaryLeadInput, ExistingCapacityLeadTimePrimary, "Primary Lead Input");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
		
		
	/*	waitForElementToAppear(SiebelModTechObj.Mode.PrimaryLeadInput, 10);
		verifyExists(SiebelModTechObj.Mode.PrimaryLeadInput,"Primary Lead Input");	
		clearTextBox(SiebelModTechObj.Mode.PrimaryLeadInput);
		sendKeys(SiebelModTechObj.Mode.PrimaryLeadInput, "No", "Primary Lead Input");
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	*/	
	}
	
	public void modSipTrunkingMiddleapplet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		
		String CallAdmissionControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallAdmissionControl");
		String TotalNumberDDIs = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TotalNumberDDIs");

		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelModTechObj.Mode.CallAdmissionControl, 10);
		verifyExists(SiebelModTechObj.Mode.CallAdmissionControl,"Call Admission Control");	
		click(SiebelModTechObj.Mode.CallAdmissionControl,"Call Admission Control");	
		clearTextBox(SiebelModTechObj.Mode.CallAdmissionControl);
		sendKeys(SiebelModTechObj.Mode.CallAdmissionControl, CallAdmissionControl, "Call Admission Control");
		verifyExists(SiebelModTechObj.Mode.TotalNumberDDIs,"Total Number DDIs");	
		click(SiebelModTechObj.Mode.TotalNumberDDIs,"Total Number DDIs");	
		clearTextBox(SiebelModTechObj.Mode.TotalNumberDDIs);
		sendKeys(SiebelModTechObj.Mode.TotalNumberDDIs, TotalNumberDDIs, "Total Number DDIs");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		
		
		/*
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelModTechObj.Mode.CallAdmissionControl, 10);
		verifyExists(SiebelModTechObj.Mode.CallAdmissionControl,"Call Admission Control");	
		click(SiebelModTechObj.Mode.CallAdmissionControl,"Call Admission Control");	
		clearTextBox(SiebelModTechObj.Mode.CallAdmissionControl);
		sendKeys(SiebelModTechObj.Mode.CallAdmissionControl, "12", "Call Admission Control");
		verifyExists(SiebelModTechObj.Mode.TotalNumberDDIs,"Total Number DDIs");	
		click(SiebelModTechObj.Mode.TotalNumberDDIs,"Total Number DDIs");	
		clearTextBox(SiebelModTechObj.Mode.TotalNumberDDIs);
		sendKeys(SiebelModTechObj.Mode.TotalNumberDDIs, "1", "Total Number DDIs");
		Reusable.waitForSiebelLoader();
		Reusable.Save();
		Reusable.waitForSiebelLoader();
	*/
	}

	public void modTechPrivateWebServiceFibreMiddleApplet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");

		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "A End Resilience Option"),"A End Resilience Option");	
		click(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "A End Resilience Option"),"A End Resilience Option");	
		verifyExists(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", AEndResilienceOption),AEndResilienceOption);	
		click(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", AEndResilienceOption),AEndResilienceOption);	
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "B End Resilience Option"),"B End Resilience Option");	
		click(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "B End Resilience Option"),"B End Resilience Option");	
		verifyExists(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", BEndResilienceOption),BEndResilienceOption);	
		click(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", BEndResilienceOption),BEndResilienceOption);	
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
		
	/*	
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "A End Resilience Option"),"Call Admission Control");	
		click(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "A End Resilience Option"),"Call Admission Control");	
		verifyExists(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", "Unprotected"),"Unprotected");	
		click(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", "Unprotected"),"Unprotected");	
		verifyExists(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "B End Resilience Option"),"B End Resilience Option");	
		click(SiebelModTechObj.DarkFiber.MiddleDropDown.replace("Value", "B End Resilience Option"),"B End Resilience Option");	
		verifyExists(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", "Unprotected"),"Unprotected");	
		click(SiebelModTechObj.DarkFiber.MiddleLi.replace("Value", "Unprotected"),"Unprotected");	
		Reusable.Save();
		Reusable.waitForSiebelLoader();
		*/
	}
	

	public void ModTechR4(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String CustomerDNSResolvers = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_DNS_Resolvers");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet"))
		{
		
			verifyExists(SiebelModTechObj.ModTechR4.MiddleDropDown.replace("Value", "Service Bandwidth"), "Service Bandwidth");
			click(SiebelModTechObj.ModTechR4.MiddleDropDown.replace("Value", "Service Bandwidth"),"Service Bandwidth");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechR4.MiddleLi.replace("Value", IPRange), IPRange);
			click(SiebelModTechObj.ModTechR4.MiddleLi.replace("Value", IPRange));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechR4.MiddleDropDown.replace("Value", "A End Resilience Option"), "A End Resilience Option");
			click(SiebelModTechObj.ModTechR4.MiddleDropDown.replace("Value", "A End Resilience Option"));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechR4.MiddleLi.replace("Value", AEndResilienceOption) , "Verify A End Resilience Option");
			click(SiebelModTechObj.ModTechR4.MiddleLi.replace("Value", AEndResilienceOption),"A End Resilience Option");
			Reusable.waitForSiebelSpinnerToDisappear();
			waitToPageLoad();
		
		} 
		if (ProductName.equalsIgnoreCase("DCA Ethernet"))
		{
			
			verifyExists(SiebelModTechObj.ModTechR4.MiddleDropDown.replace("Value", "Hard Modify Flag") , "Hard Modify Flag DropDown");
			click(SiebelModTechObj.ModTechR4.MiddleDropDown.replace("Value", "Hard Modify Flag"),"Hard Modify Flag DropDown");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechR4.MiddleLi.replace("Value", CustomerDNSResolvers) , CustomerDNSResolvers);
			click(SiebelModTechObj.ModTechR4.MiddleLi.replace("Value", CustomerDNSResolvers),CustomerDNSResolvers);
			Reusable.waitForSiebelSpinnerToDisappear();
		}
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		
	}

	public void ModTechPrivateEthernet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String AendInstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String BendInstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");
		String Contractterm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_term");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet")) {

			// Site A
			
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteADropdownClick.replace("Value", "Install Time") , "Install Time");
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteADropdownClick.replace("Value", "Install Time"),"Install Time");
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteABSelection.replace("Value", AendInstallTime) , AendInstallTime);
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteABSelection.replace("Value", AendInstallTime),AendInstallTime);
			Reusable.waitForSiebelSpinnerToDisappear();
			
			// Site B
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteBDropdownClick.replace("Value", "Install Time") ,"Install Time");
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteBDropdownClick.replace("Value", "Install Time"),"Install Time");
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteABSelection.replace("Value", BendInstallTime) , BendInstallTime);
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteABSelection.replace("Value", BendInstallTime),BendInstallTime);
			Reusable.waitForSiebelSpinnerToDisappear();
		}
		
		/*
		if (ProductName.equalsIgnoreCase("DCA Ethernet")) 
		{
			
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.AEndSiteDropDown.replace("Value", "Access Type"), "Access Type");
			click(SiebelModTechObj.ModTechPrivateEthernet.AEndSiteDropDown.replace("Value", "Access Type"));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.AList.replace("Value", Contractterm),Contractterm);
			click(SiebelModTechObj.ModTechPrivateEthernet.AList.replace("Value", Contractterm),Contractterm);
			Reusable.waitForSiebelSpinnerToDisappear();
			// Setting A
			
	//		Reusable.ClickHereSave();
			Reusable.savePage();
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteASetting , "Verify SiteASetting");
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteASetting,"Site A Setting Click");
			Reusable.waitForSiebelLoader();

			// Pop Up
			
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.PopPlus,"Verify PopPlus");
			click(SiebelModTechObj.ModTechPrivateEthernet.PopPlus,"Click Add Sign");
			Reusable.waitForSiebelLoader();
	
			// Attribute
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.PopAttributeDrodown,"Verify PopAttributeDrodown");
			click(SiebelModTechObj.ModTechPrivateEthernet.PopAttributeDrodown,"Click PopAttributeDrodown");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.MiddleLi.replace("Value", "CSP interconnect A-end") , "Verify MiddleLi");
			click(SiebelModTechObj.ModTechPrivateEthernet.MiddleLi.replace("Value", "CSP interconnect A-end"));
			Reusable.SendkeaboardKeys(SiebelModTechObj.ModTechPrivateEthernet.AttributeName, Keys.TAB);	
			Reusable.waitForSiebelSpinnerToDisappear();
			
			// Attribute
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.PopAttributeDrodown,"Verify PopAttributeDrodown");
			click(SiebelModTechObj.ModTechPrivateEthernet.PopAttributeDrodown,"Click PopAttributeDrodown");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.MiddleLi.replace("Value", "Dedicated Port"),"Verify MiddleLi");
			click(SiebelModTechObj.ModTechPrivateEthernet.MiddleLi.replace("Value", "Dedicated Port"));
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.AttributeValue,"Verify AttributeValue");
			Reusable.SendkeaboardKeys(SiebelModTechObj.ModTechPrivateEthernet.AttributeValue, Keys.TAB);	
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteASettingOK,"Verify SiteASettingOK");
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteASettingOK,"Click SiteASettingOK");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
		//	Reusable.ClickHereSave();
			
			// Setting B 
			Reusable.savePage();

			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteBSetting,"Verify SiteBSetting");
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteBSetting,"Click Site B Setting Click");
			Reusable.waitForSiebelLoader();
		
			// Attribute
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.PopAttributeDrodown,"Verify PopAttributeDrodown");
			click(SiebelModTechObj.ModTechPrivateEthernet.PopAttributeDrodown,"Click PopAttributeDrodown");
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.MiddleLi.replace("Value", "Dedicated Port"),"Verify MiddleLi");
			click(SiebelModTechObj.ModTechPrivateEthernet.MiddleLi.replace("Value", "Dedicated Port"));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.AttributeValue,"Verify AttributeValue");
			Reusable.SendkeaboardKeys(SiebelModTechObj.ModTechPrivateEthernet.AttributeValue, Keys.TAB);	
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModTechObj.ModTechPrivateEthernet.SiteASettingOK,"Verify SiteASettingOK");
			click(SiebelModTechObj.ModTechPrivateEthernet.SiteASettingOK,"OK Clicked");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			//waitToPageLoad();
			//Reusable.ClickHereSave();
			//Reusable.waitForSiebelLoader();
		//	waitToPageLoad();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
		}
		*/
		
	}	
	

	public void ModTechforcommanproduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		
		Reusable.waitForSiebelLoader();
		String TempStartDate= Reusable.CurrentDate();

		verifyExists(SiebelModTechObj.ModTechforcommanproduct.SupportStarDate,"SupportStarDate");
		click(SiebelModTechObj.ModTechforcommanproduct.SupportStarDate,"SupportStarDate");
		sendKeys(SiebelModTechObj.ModTechforcommanproduct.SupportStarDate,TempStartDate,"SupportStarDate");
		Reusable.waitForSiebelSpinnerToDisappear();

		String TempEndDate = Reusable.FutureDate(60);
		verifyExists(SiebelModTechObj.ModTechforcommanproduct.SupprtEndDate,"SupprtEndDate");
		click(SiebelModTechObj.ModTechforcommanproduct.SupprtEndDate,"SupprtEndDate");
		sendKeys(SiebelModTechObj.ModTechforcommanproduct.SupprtEndDate,TempEndDate,"SupportStarDate");
		Reusable.waitForSiebelSpinnerToDisappear();

		Reusable.savePage();
		
		Reusable.waitForSiebelSpinnerToDisappear();
		
	/*	String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		String ServiceParty = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party");
		
		System.out.println("middle applet start");
		
		verifyExists(SiebelModTechObj.ModTechforcommanproduct.SupportStarDate,"Verify SupportStarDate");
		click(SiebelModTechObj.ModTechforcommanproduct.SupportStarDate,"Click on SupportStarDate");
		sendKeys(SiebelModTechObj.ModTechforcommanproduct.SupportStarDate,ServiceBandwidth, "Enter  SupportStarDate");

		verifyExists(SiebelModTechObj.ModTechforcommanproduct.SupprtEndDate,"Verify SupprtEndDate");
		click(SiebelModTechObj.ModTechforcommanproduct.SupprtEndDate,"Click on SupprtEndDate");
		sendKeys(SiebelModTechObj.ModTechforcommanproduct.SupprtEndDate,ServiceParty, "Enter  SupprtEndDate");

		verifyExists(SiebelModTechObj.ModTechforcommanproduct.SaveButton,"Verify SaveButton");
		click(SiebelModTechObj.ModTechforcommanproduct.SaveButton,"Click on SaveButton");
		System.out.println("middle applet end");

*/	}

	public void modEthernetHubMiddleApplet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{

		String InstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
		String SlotID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
		String PhysicalPortID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modEthernetHubMiddleApplet.TextInput.replace("Value", "Slot ID"),"Verify TextInput Slot ID");
		sendKeys(SiebelModTechObj.modEthernetHubMiddleApplet.TextInput.replace("Value", "Slot ID"), SlotID,"Slot ID");
		Reusable.SendkeaboardKeys(SiebelModTechObj.modEthernetHubMiddleApplet.TextInput.replace("Value", "Slot ID"),Keys.ENTER);
	
		verifyExists(SiebelModTechObj.modEthernetHubMiddleApplet.TextInput.replace("Value", "Physical Port ID"),"Verify TextInput PhysicalPortID");
		sendKeys(SiebelModTechObj.modEthernetHubMiddleApplet.TextInput.replace("Value", "Physical Port ID"), PhysicalPortID,"PhysicalPortID");
		Reusable.SendkeaboardKeys(SiebelModTechObj.modEthernetHubMiddleApplet.TextInput.replace("Value", "Physical Port ID"),Keys.ENTER);
		
		verifyExists(SiebelModTechObj.modEthernetHubMiddleApplet.ClickDropdown.replace("Value", "Install Time"),"Verify Install Time Dropdown");
		click(SiebelModTechObj.modEthernetHubMiddleApplet.ClickDropdown.replace("Value", "Install Time"));
		verifyExists(SiebelModTechObj.modEthernetHubMiddleApplet.SelectValueDropdown.replace("Value", InstallTime),"Verify Select Install Time Dropdown Value");
		click(SiebelModTechObj.modEthernetHubMiddleApplet.SelectValueDropdown.replace("Value", InstallTime),"Install Time Dropdown Value");

		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
	}
	
	public void modTechNumberHosting(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		
		String NumberOfSignallingTrunks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfSignallingTrunks");
		String CallAdmissionControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallAdmissionControl");

		System.out.println("go to else loop of call admission control");
		verifyExists(SiebelModTechObj.modTechNumberHosting.CallAdmissionControl,"Verify CallAdmissionControl");
		sendKeys(SiebelModTechObj.modTechNumberHosting.CallAdmissionControl,CallAdmissionControl,"Enter call admission control");
		Reusable.SendkeaboardKeys(SiebelModTechObj.modTechNumberHosting.CallAdmissionControl,Keys.TAB);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelModTechObj.modTechNumberHosting.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
		sendKeys(SiebelModTechObj.modTechNumberHosting.NumberOfSignallingTrunks,NumberOfSignallingTrunks,"Enter number of signalling trunks");
		Reusable.SendkeaboardKeys(SiebelModTechObj.modTechNumberHosting.NumberOfSignallingTrunks,Keys.TAB);
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		waitToPageLoad();
		
		Reusable.waitForSiebelSpinnerToDisappear();
	}
	
	
	public void modTechPrizmNetMiddleApplet() throws Exception {

		{
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModTechObj.modTechPrizmNetMiddleApplet.ClickDropdown,"Verify ClickDropdown");
			click(SiebelModTechObj.modTechPrizmNetMiddleApplet.ClickDropdown.replace("Value", "IP Addressing Format"));
			verifyExists(SiebelModTechObj.modTechPrizmNetMiddleApplet.SelectValueDropdown,"Verify SelectValueDropdown");
			click(SiebelModTechObj.modTechPrizmNetMiddleApplet.SelectValueDropdown.replace("Value", "IPv4 and IPv6"));		
			verifyExists(SiebelModTechObj.modTechPrizmNetMiddleApplet.SaveButton,"Verify SaveButton");
			click(SiebelModTechObj.modTechPrizmNetMiddleApplet.SaveButton,"Click on SaveButton");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
		
		}
		}
		
	public void modTechSwiftNetMiddleApplet() throws Exception
	{

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleDropDown , "Verify MiddleDropDown");
		click(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleDropDown.replace("Value", "Service Bandwidth (Primary)"));
		verifyExists(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleLi , "Verify MiddleLi");
		click(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleLi.replace("Value", "4 Mbps"));
	
		/////////////Routing Sequence
		
		verifyExists(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleDropDown , "Verify MiddleDropDown");
		click(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleDropDown.replace("Value", "Routing Sequence"));
		verifyExists(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleLi , "Verify MiddleLi");
		click(SiebelModTechObj.modTechSwiftNetMiddleApplet.MiddleLi.replace("Value", "Round Robin"));
		
		verifyExists(SiebelModTechObj.modTechSwiftNetMiddleApplet.CPECombinationIDGeneric,"Verify NumberOfSignallingTrunks");
		sendKeys(SiebelModTechObj.modTechSwiftNetMiddleApplet.CPECombinationIDGeneric.replace("Value", "Capacity Check Reference"), "1");
		Reusable.SendkeaboardKeys(SiebelModTechObj.modTechSwiftNetMiddleApplet.CPECombinationIDGeneric.replace("Value", "Capacity Check Reference"), Keys.TAB);
		
		verifyExists(SiebelModTechObj.modTechSwiftNetMiddleApplet.SaveButton,"Verify SaveButton");
		click(SiebelModTechObj.modTechSwiftNetMiddleApplet.SaveButton,"Click on SaveButton");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		
	}
	

	public void modTechMangedDeicatedFirewall(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
	{
		String CPEc = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CPEc");
		String EstimatedDeliveryDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Estimated_Delivery_Date");
		String HighAvai = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"HighAvai");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		
		System.out.println("valu of 178=" + CPEc);
		System.out.println("valu of 178=" + EstimatedDeliveryDate);
		
		verifyExists(SiebelModTechObj.modTechMangedDeicatedFirewall.CPECombinationIDGeneric , "Verify CPECombinationIDGeneric");
		click(SiebelModTechObj.modTechMangedDeicatedFirewall.CPECombinationIDGeneric.replace("Value", "CPE Combination ID"));
		System.out.println("valu of 180=" + HighAvai);
		sendKeys(SiebelModTechObj.modTechMangedDeicatedFirewall.CPECombinationIDGeneric.replace("Value", "CPE Combination ID"), "50");
		
		verifyExists(SiebelModTechObj.modTechMangedDeicatedFirewall.HighAvailabilityRequired, "Verify HighAvailabilityRequired");
		click(SiebelModTechObj.modTechMangedDeicatedFirewall.HighAvailabilityRequired.replace("Value", "High Availability Required"));
		
		verifyExists(SiebelModTechObj.modTechMangedDeicatedFirewall.HighAvailabilityRequiredli, "Verify HighAvailabilityRequiredli");
		click(SiebelModTechObj.modTechMangedDeicatedFirewall.HighAvailabilityRequiredli, "HighHighAvailabilityRequiredli");
	
		verifyExists(SiebelModTechObj.modTechMangedDeicatedFirewall.SaveButton,"Verify SaveButton");
		click(SiebelModTechObj.modTechMangedDeicatedFirewall.SaveButton,"Click on SaveButton");
		
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
	}
	
	
	public void modTechPrivateWaveNodeMiddleApplet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
	{
		
		String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelModTechObj.modTechPrivateWaveNodeMiddleApplet.MiddleDropDown.replace("Value", "Network Topology") , "Verify Network Topology Dropdown");
		click(SiebelModTechObj.modTechPrivateWaveNodeMiddleApplet.MiddleDropDown.replace("Value", "Network Topology"),"Network Topology");
		verifyExists(SiebelModTechObj.modTechPrivateWaveNodeMiddleApplet.changeMiddleLi.replace("Value", Topology) , "Verify Network Topology Drop Down");
		click(SiebelModTechObj.modTechPrivateWaveNodeMiddleApplet.changeMiddleLi.replace("Value", Topology),"Verify Network Topology Drop Down");	
	
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}
	
	public void ethernetVpnAccessMiddleApplet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
	{
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
		click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Click on ServiceBandwidthDropdownAccess");
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
		click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
		Reusable.waitForSiebelSpinnerToDisappear();

	/*	verifyExists(SiebelModTechObj.ethernetVpnAccessMiddleApplet.ServiceBandwidthIPAccess,"Verify ServiceBandwidthIPAccess");
		click(SiebelModTechObj.ethernetVpnAccessMiddleApplet.ServiceBandwidthIPAccess,"Click on ServiceBandwidthIPAccess");
	
		sendKeys(SiebelModTechObj.ethernetVpnAccessMiddleApplet.ServiceBandwidthIPAccess,ServiceBandwidth,"ServiceBandwidthIPAccess");
		Reusable.SendkeaboardKeys(SiebelModTechObj.ethernetVpnAccessMiddleApplet.ServiceBandwidthIPAccess, Keys.TAB);
	//	Reusable.ClickHereSave();
	*/	Reusable.savePage();
		Reusable.Pagerefresh();
		 waitToPageLoad();
		 Reusable.waitForSiebelLoader();
	}
	
	
	public void modTechDarkFibreA(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Reusable.waitForSiebelLoader();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Aend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");

		int rand_int1 = random.nextInt(1000);
		int rand_int2 = random.nextInt(1000); 

		if(!(ProductName.equalsIgnoreCase("Private Wave Service"))) 
		{
			verifyExists(SiebelModTechObj.modTechDarkFibreA.SiteADropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreA.SiteADropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreA.SiteABSelection.replace("Value", Aend_Install_Time));	

			verifyExists(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Notification Period"),"Access Notification Period");	
			sendKeys(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Notification Period"),Integer.toString(rand_int1));	

			verifyExists(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Time Window"),"Access Time Window");	
			sendKeys(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Time Window"),Integer.toString(rand_int2));	
		}
		if (ProductName.equalsIgnoreCase("Ultra Low Latency")|| ProductName.equalsIgnoreCase("Private Wave Service") || ProductName.equalsIgnoreCase("Dark Fibre"))
		{
			verifyExists(SiebelModTechObj.modTechDarkFibreA.SiteADropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreA.SiteADropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreA.SiteABSelection.replace("Value", Bend_Install_Time));	

			verifyExists(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Notification Period"),"Access Notification Period");	
			sendKeys(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Notification Period"),Integer.toString(rand_int1));	

			verifyExists(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Time Window"),"Access Time Window");	
			sendKeys(SiebelModTechObj.modTechDarkFibreA.SiteAInput.replace("Value", "Access Time Window"),Integer.toString(rand_int2));	
			waitToPageLoad();
		}
	}

	public void modTechDarkFibreB(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
	{
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Aend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");

		int rand_int1 = rand.nextInt(1000); 
		int rand_int2 = rand.nextInt(1000);

		if(!(ProductName.equalsIgnoreCase("Private Wave Service"))) 
		{
			verifyExists(SiebelModTechObj.modTechDarkFibreB.SiteBDropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreB.SiteBDropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreB.SiteABSelection.replace("Value", Aend_Install_Time));	

			verifyExists(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Notification Period"),"Access Notification Period");	
			sendKeys(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Notification Period"),Integer.toString(rand_int1));	

			verifyExists(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Time Window"),"Access Time Window");	
			sendKeys(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Time Window"),Integer.toString(rand_int2));	
		}

		if (ProductName.equalsIgnoreCase("Ultra Low Latency")|| ProductName.equalsIgnoreCase("Private Wave Service"))
		{

			verifyExists(SiebelModTechObj.modTechDarkFibreB.SiteBDropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreB.SiteBDropdownClick.replace("Value", "Install Time"),"Install Time");	
			click(SiebelModTechObj.modTechDarkFibreB.SiteABSelection.replace("Value", Bend_Install_Time));	

			verifyExists(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Notification Period"),"Access Notification Period");	
			sendKeys(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Notification Period"),Integer.toString(rand_int1));	

			verifyExists(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Time Window"),"Access Time Window");	
			sendKeys(SiebelModTechObj.modTechDarkFibreB.SiteBInput.replace("Value", "Access Time Window"),Integer.toString(rand_int2));	
			waitToPageLoad();

		}

	}

	public void modTechPrizmNetMiddleApplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
	{
		String AutoMitigation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modTechPrizmNetMiddleApplet.ClickDropdown.replace("Value", "IP Addressing Format"),"IP Addressing Format Drop Down");	
		click(SiebelModTechObj.modTechPrizmNetMiddleApplet.ClickDropdown.replace("Value", "IP Addressing Format"),"IP Addressing Format Drop Down");	
	
		
		verifyExists(SiebelModTechObj.modTechPrizmNetMiddleApplet.SelectValueDropdown.replace("Value", AutoMitigation),AutoMitigation);
		click(SiebelModTechObj.modTechPrizmNetMiddleApplet.SelectValueDropdown.replace("Value", AutoMitigation),AutoMitigation);
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();

	/*	Reusable.ClickHereSave();
		click(SiebelModTechObj.modTechPrizmNetMiddleApplet.SelectValueDropdown.replace("Value", "IPv4 and IPv6"));

		verifyExists(SiebelModTechObj.modTechPrizmNetMiddleApplet.ClickheretoSaveAccess,"ClickheretoSaveAccess");	
		click(SiebelModTechObj.modTechPrizmNetMiddleApplet.ClickheretoSaveAccess,"ClickheretoSaveAccess");	
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		*/
		
		

	}

	public void modTechMiddleAppletCPESolutionService(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
	{
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");

		
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleDropDown.replace("Value", "A End Resilience Option"));	
		click(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleDropDown.replace("Value", "A End Resilience Option"));
		
		verifyExists(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleLi.replace("Value", AEndResilienceOption),AEndResilienceOption);	
		click(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleLi.replace("Value", AEndResilienceOption),AEndResilienceOption);	

		verifyExists(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleDropDown.replace("Value", "B End Resilience Option"));	
		click(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleDropDown.replace("Value", "B End Resilience Option"));	
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleLi.replace("Value", BEndResilienceOption),BEndResilienceOption);	
		click(SiebelModTechObj.modTechMiddleAppletCPESolutionService.MiddleLi.replace("Value", BEndResilienceOption),BEndResilienceOption);	
		Reusable.waitForSiebelLoader();

		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
	//	verifyExists(SiebelModTechObj.modTechMiddleAppletCPESolutionService.SaveButton,"SaveButton");	
	//	click(SiebelModTechObj.modTechMiddleAppletCPESolutionService.SaveButton,"SaveButton");	
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

	}

	public void modTechPrivateWaveNode(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
	{
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Aend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");

		verifyExists(SiebelModTechObj.modTechPrivateWaveNode.SiteADropdownClick.replace("Value","Install Time"));	
		click(SiebelModTechObj.modTechPrivateWaveNode.SiteADropdownClick.replace("Value","Install Time"));	

		verifyExists(SiebelModTechObj.modTechPrivateWaveNode.SiteABSelection.replace("Value",Aend_Install_Time));	
		click(SiebelModTechObj.modTechPrivateWaveNode.SiteABSelection.replace("Value",Aend_Install_Time));	

		int rand_int1 = rand.nextInt(1000);
		verifyExists(SiebelModTechObj.modTechPrivateWaveNode.SiteAInput.replace("Value","Access Notification Period"));	
		sendKeys(SiebelModTechObj.modTechPrivateWaveNode.SiteAInput.replace("Value", "Access Notification Period"),Integer.toString(rand_int1));	
		Reusable.waitForSiebelLoader();
		
		int rand_int2 = rand.nextInt(1000); 
		verifyExists(SiebelModTechObj.modTechPrivateWaveNode.SiteAInput.replace("Value","Access Time Window"));	
		sendKeys(SiebelModTechObj.modTechPrivateWaveNode.SiteAInput.replace("Value", "Access Time Window"),Integer.toString(rand_int2));	
		Reusable.waitForSiebelLoader();
		
		if (ProductName.equalsIgnoreCase("Ultra Low Latency")|| ProductName.equalsIgnoreCase("Private Wave Service")  )
		{
			verifyExists(SiebelModTechObj.modTechPrivateWaveNode.SiteBDropdownClick.replace("Value","Install Time"));	
			click(SiebelModTechObj.modTechPrivateWaveNode.SiteBDropdownClick.replace("Value","Install Time"));	
			click(SiebelModTechObj.modTechPrivateWaveNode.SiteABSelection.replace("Value",Bend_Install_Time));	

			verifyExists(SiebelModTechObj.modTechPrivateWaveNode.SiteBInput.replace("Value","Access Notification Period"));	
			sendKeys(SiebelModTechObj.modTechPrivateWaveNode.SiteBInput.replace("Value", "Access Notification Period"),Integer.toString(rand_int1));	

			verifyExists(SiebelModTechObj.modTechPrivateWaveNode.SiteBInput.replace("Value","Access Time Window"));	
			sendKeys(SiebelModTechObj.modTechPrivateWaveNode.SiteBInput.replace("Value", "Access Time Window"),Integer.toString(rand_int2));	

			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
						
		}
	
	}

	public void modTechDarkFibreMiddleApplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
	{
		
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");

		
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value","A End Resilience Option"),"A End Resilience Option");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value","A End Resilience Option"),"A End Resilience Option");	
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value",AEndResilienceOption),AEndResilienceOption);	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value",AEndResilienceOption),AEndResilienceOption);	
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value","B End Resilience Option"),"B End Resilience Option");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value","B End Resilience Option"),"B End Resilience Option");	
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value",BEndResilienceOption),BEndResilienceOption);	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value",BEndResilienceOption),BEndResilienceOption);	
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
	//	verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.SaveButton,"SaveButton");	
	//	click(SiebelModTechObj.modTechDarkFibreMiddleApplet.SaveButton,"SaveButton");	
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
	
	}

	public void modTechultraLowLatencyMiddleApplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
	{
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");

		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
	
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value", "Service Bandwidth"),"Service Bandwidth");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value", "Service Bandwidth"),"Service Bandwidth");
		
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value", ServiceBandwidth),"ServiceBandwidth");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value", ServiceBandwidth),"ServiceBandwidth");
		
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value", "A End Resilience Option"),"AEndResilienceOption");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value", "A End Resilience Option"),"AEndResilienceOption");
		
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value", AEndResilienceOption),"AEndResilienceOption");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value", AEndResilienceOption),"AEndResilienceOption");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value", "B End Resilience Option"),"BEndResilienceOption");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleDropDown.replace("Value", "B End Resilience Option"),"BEndResilienceOption");
		
		verifyExists(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value", BEndResilienceOption),"BEndResilienceOption");	
		click(SiebelModTechObj.modTechDarkFibreMiddleApplet.MiddleLi.replace("Value", BEndResilienceOption),"BEndResilienceOption");
		Reusable.waitForSiebelLoader();
		
		Reusable.savePage();	
		waitToPageLoad();
		Reusable.waitForSiebelSpinnerToDisappear();	
		}

	public void modTechEthernetAccessMiddleApplet() throws InterruptedException, IOException
	{
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.modTechEthernetAccessMiddleApplet.MiddleDropDown.replace("Value", "A End Resilience Option"),"MiddleDropDown");	
		click(SiebelModTechObj.modTechEthernetAccessMiddleApplet.MiddleDropDown.replace("Value", "A End Resilience Option"),"MiddleDropDown");	
		verifyExists(SiebelModTechObj.modTechEthernetAccessMiddleApplet.MiddleLi.replace("Value", "Single Pair"),"MiddleLi");	
		click(SiebelModTechObj.modTechEthernetAccessMiddleApplet.MiddleLi.replace("Value", "Single Pair"),"MiddleLi");	
		verifyExists(SiebelModTechObj.modTechEthernetAccessMiddleApplet.SaveButton,"SaveButton");	
		click(SiebelModTechObj.modTechEthernetAccessMiddleApplet.SaveButton,"SaveButton");	
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
	}
	
	
	public void Carnor_SelectServiceGroupTab(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
	{
String ProductType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		
		if (ProductType.equals("Voice Line V") || ProductType.equals("SIP Trunking")
				|| ProductType.equals("Interconnect") || ProductType.equals("Number Hosting"))
		{
			/*
		}
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModTechObj.CarnorSelectServiceGroupTab.DropDown,"Verify DropDown");
			click(SiebelModTechObj.CarnorSelectServiceGroupTab.DropDown,"Click on DropDown");
			
			Reusable.Select1(SiebelModTechObj.CarnorSelectServiceGroupTab.DropDown, "Service Group");
			
			verifyExists(SiebelModTechObj.CarnorSelectServiceGroupTab.ServiceGroupNew,"Verify ServiceGroupNew");
			click(SiebelModTechObj.CarnorSelectServiceGroupTab.ServiceGroupNew,"Click on ServiceGroupNew");
			
			verifyExists(SiebelModTechObj.CarnorSelectServiceGroupTab.serviceGrouplookup,"Verify serviceGrouplookup");
			click(SiebelModTechObj.CarnorSelectServiceGroupTab.serviceGrouplookup,"Click on serviceGrouplookup");
			
			verifyExists(SiebelModTechObj.CarnorSelectServiceGroupTab.ServiceGroupOk,"Verify ServiceGroupOk");
			click(SiebelModTechObj.CarnorSelectServiceGroupTab.ServiceGroupOk,"Click on ServiceGroupOk");	
			
			*/
			
			
			
			
			Reusable.waitForSiebelLoader();
			if (isElementPresent(By.xpath("//a[text()='Service Group']"))) {
			
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGroupTab,"Service Group");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGroupTab,"Service Group");
						
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				Reusable.waitForSiebelLoader();
			}
			else{
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown,"Drop down");
				
		//		select(SiebelAddProdcutObj.SelectServiceGroupTab.DropDown, "Service Group");
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupoption,"Service Group");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupoption,"Service Group");
		
				
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupNew, "Service Group New");
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.serviceGrouplookup, "service Group lookup");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				click(SiebelAddProdcutObj.SelectServiceGroupTab.ServiceGroupOk, "ServiceGroupOk");
				Reusable.waitForSiebelLoader();
			}
		}
	}
	
	
	
	
	
	
	public void Carnor_getReferenceNo(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
	{
		String ProductType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		
		if (ProductType.equalsIgnoreCase("Ethernet VPN Access")
		|| ProductType.equalsIgnoreCase("Dark Fibre")
		|| ProductType.equalsIgnoreCase("Ultra Low Latency")
		|| ProductType.equalsIgnoreCase("Private Ethernet")
		|| ProductType.equalsIgnoreCase("DCA Ethernet")
		|| ProductType.equalsIgnoreCase("Private Wave Service")
		|| ProductType.equalsIgnoreCase("IP Access")
		|| ProductType.equalsIgnoreCase("IP VPN Service"))
		{
			
		verifyExists(SiebelModTechObj.CarnorgetReferenceNo.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
		click(SiebelModTechObj.CarnorgetReferenceNo.CircuitReferenceAccess,"Click on CircuitReferenceAccess");	
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		
		String Circuitreferencenumber = getTextFrom(SiebelModTechObj.CarnorgetReferenceNo.CircuitReferenceValue);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Circuitreferencenumber", Circuitreferencenumber);	
	
		System.out.println(Circuitreferencenumber);
	
		}
	}
		
	public void OperationAttribute_Carnor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException
	{
SiebelAddProduct Product = new SiebelAddProduct();
		
		String ProductType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Subproduct = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
	
		if (ProductType.equals("Voice Line V"))
		{
			
			verifyExists(SiebelModTechObj.OperationAttributeCarno.Voiceconfigtab,"Verify Voiceconfigtab");
			click(SiebelModTechObj.OperationAttributeCarno.Voiceconfigtab,"Click on Voiceconfigtab");	
			verifyExists(SiebelModTechObj.OperationAttributeCarno.OtherTab,"Verify OtherTab");
			click(SiebelModTechObj.OperationAttributeCarno.OtherTab,"Click on OtherTab");	
			verifyExists(SiebelModTechObj.OperationAttributeCarno.OperationAttribute1,"Verify OperationAttribute1");
			click(SiebelModTechObj.OperationAttributeCarno.OperationAttribute1,"Click on OperationAttribute1");	
			verifyExists(SiebelModTechObj.OperationAttributeCarno.AttributeValue1,"Verify AttributeValue1");
			click(SiebelModTechObj.OperationAttributeCarno.AttributeValue1,"Click on AttributeValue1");	
			sendKeys(SiebelModTechObj.OperationAttributeCarno.AttributeValue1,"test","Enter attribute value");

			verifyExists(SiebelModTechObj.OperationAttributeCarno.OperationAttributeSubmit,"Verify OperationAttributeSubmit");
			click(SiebelModTechObj.OperationAttributeCarno.OperationAttributeSubmit,"Click on Submit");	
			verifyExists(SiebelModTechObj.OperationAttributeCarno.OperationAttribute2,"Verify OperationAttribute");
			click(SiebelModTechObj.OperationAttributeCarno.OperationAttribute2,"Click on OperationAttribute");	
			verifyExists(SiebelModTechObj.OperationAttributeCarno.OperationAttributeSubmit,"Verify OperationAttributeSubmit");
			click(SiebelModTechObj.OperationAttributeCarno.OperationAttributeSubmit,"Click on Submit");	
			verifyExists(SiebelModTechObj.OperationAttributeCarno.OperationAttribute2,"Verify OperationAttribute");
			click(SiebelModTechObj.OperationAttributeCarno.OperationAttribute2,"Click on OperationAttribute");	
			
			verifyExists(SiebelModTechObj.OperationAttributeCarno.AttributeValue1,"Verify AttributeValue1");
			click(SiebelModTechObj.OperationAttributeCarno.AttributeValue1,"Click on AttributeValue1");	
			sendKeys(SiebelModTechObj.OperationAttributeCarno.AttributeValue1,"test","Enter attribute value");
			verifyExists(SiebelModTechObj.OperationAttributeCarno.OperationAttributeSubmit,"Verify OperationAttributeSubmit");
			click(SiebelModTechObj.OperationAttributeCarno.OperationAttributeSubmit,"Click on Submit");	
	}
		if (Subproduct.equalsIgnoreCase("IP VPN Access") || Subproduct.equalsIgnoreCase("IP VPN Plus") 
				|| Subproduct.equalsIgnoreCase("Swiftnet") ||Subproduct.equalsIgnoreCase("Prizmnet"))
		{
			Reusable.waitForSiebelLoader();		
			verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink.replace("Value", "Sites"),"ClickLink");
			click(SiebelAddProdcutObj.CompletedValidation_offnet.ClickLink.replace("Value", "Sites"));
			Reusable.waitForSiebelLoader();		

			verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.RouterSpecificationSetting,"RouterSpecificationSetting");
			click(SiebelAddProdcutObj.CompletedValidation_offnet.RouterSpecificationSetting,"RouterSpecificationSetting");
			Reusable.waitForSiebelLoader();

			int RowCount = getXPathCount(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueCount);
			for (int i = 0; i < RowCount; i++) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"OperationalAttribueClick");
				click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"OperationalAttribueClick");

				verifyExists(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText,"OperationalAttributeText");
				click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText,"OperationalAttributeText");
				sendKeys(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeText, "Test1");
			}
			ScrollIntoViewByString(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeOK);
			click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeOK,"OperationalAttributeOK");
			Reusable.savePage();	
		}
		
		if(ProductType.contains("Private Wave Service"))
		{
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
			javaScriptclick(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
			Reusable.waitForSiebelLoader();	 
			System.out.println("enter if loop of private wave service");
		 
			verifyExists(SiebelModeObj.EthernetAccessNewFields.LicenseFlag,"LicenseFlag");
		 	click(SiebelModeObj.EthernetAccessNewFields.LicenseFlag,"LicenseFlag");
		 	sendKeys(SiebelModeObj.EthernetAccessNewFields.LicenseFlag,"Y","LicenseFlag");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.LicenseFlag, Keys.TAB);
			Reusable.waitForSiebelLoader();
	
			verifyExists(SiebelModeObj.EthernetAccessNewFields.LicenseText,"LicenseText");
			click(SiebelModeObj.EthernetAccessNewFields.LicenseText,"LicenseText");
		 	sendKeys(SiebelModeObj.EthernetAccessNewFields.LicenseText,"Test1","LicenseText");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.LicenseText, Keys.TAB);
		 	Reusable.waitForSiebelLoader();
		 	
			verifyExists(SiebelModeObj.EthernetAccessNewFields.VendorValuecell,"VendorValuecell");
			click(SiebelModeObj.EthernetAccessNewFields.VendorValuecell,"VendorValuecell");
		 	sendKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecell,"ADVA","VendorValuecell");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecell, Keys.TAB);
		 	Reusable.waitForSiebelLoader();
		 	
			verifyExists(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell,"TechonolgyValuecell");
			click(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell,"TechonolgyValuecell");
		 	sendKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell,"CWDM","TechonolgyValuecell");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell, Keys.TAB);
		 	Reusable.waitForSiebelLoader();
		 	
			verifyExists(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection,"InputValuesProtection");
		 	click(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection,"InputValuesProtection");
		 	sendKeys(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection,"No Protection","InputValuesProtection");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection, Keys.TAB);
			Reusable.waitForSiebelLoader();
			ScrollIntoViewByString(SiebelModeObj.EthernetAccessNewFields.OKButton);
			verifyExists(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");
			click(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");
	  
			if (isElementPresent(By.xpath("//div[@class='ui-dialog-buttonset']/button)[2]"))) 
			{
				System.out.println("enter if loop");
				boolean t = isElementPresent(By.xpath("//div[@class='ui-dialog-buttonset']/button)[2]"));
				System.out.println(t);
	   
				click(SiebelModeObj.EthernetAccessNewFields.Dialogbuttonset,"Dialogbuttonset");		 
				Reusable.waitForSiebelLoader();
			}
		}
		
		if(ProductType.contains("Private Wave Node"))
								 
														 
		{
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
			javaScriptclick(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
			Reusable.waitForSiebelLoader();	 
			System.out.println("enter if loop of private wave service");
		 
			verifyExists(SiebelModeObj.EthernetAccessNewFields.VendorValueField,"VendorPlatform");
		 	click(SiebelModeObj.EthernetAccessNewFields.VendorValueField,"VendorPlatform");
			verifyExists(SiebelModeObj.EthernetAccessNewFields.VendorValuecell,"VendorPlatform");
		 	sendKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecell,"ADVA","VendorPlatform");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecell, Keys.TAB);
		 	Reusable.waitForSiebelLoader();
		 	
			verifyExists(SiebelModeObj.EthernetAccessNewFields.TechonolgyValueField,"Technology");
		 	click(SiebelModeObj.EthernetAccessNewFields.TechonolgyValueField,"Technology");
			verifyExists(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell,"Technology");
		 	sendKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell,"CWDM","Technology");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell, Keys.TAB);
		 	Reusable.waitForSiebelLoader();
		 	ScrollIntoViewByString(SiebelModeObj.EthernetAccessNewFields.OKButton);
			verifyExists(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");
			click(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");
		}
		if(ProductType.contains("DCA Ethernet"))
		{
			verifyExists(SiebelModeObj.SiteBSettingClick.SiteBSetting,"Site B Setting");
			click(SiebelModeObj.SiteBSettingClick.SiteBSetting,"Site B Setting Click");
			Reusable.waitForSiebelLoader();
	
			verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
			click(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
			click(SiebelModeObj.CSPInterconnectSiteAEntry.MiddleLi.replace("Value", "Dedicated Port"));
			Reusable.waitForSiebelLoader();
			
		 	ScrollIntoViewByString(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK);		
			verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK,"OK Clicked");
			click(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK,"OK Clicked");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
		}

		if(ProductType.contains("Dark Fibre"))
		{	refreshPage();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
			click(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
			Reusable.waitForSiebelLoader();	 
			System.out.println("enter if loop of private wave service");
		 
			verifyExists(SiebelModeObj.middleAppletDarkFibre.PrimaryOrderField,"PrimaryOrder");
		 	click(SiebelModeObj.middleAppletDarkFibre.PrimaryOrderField,"PrimaryOrder");
			verifyExists(SiebelModeObj.middleAppletDarkFibre.PrimaryOrder,"PrimaryOrder");
		 	sendKeys(SiebelModeObj.middleAppletDarkFibre.PrimaryOrder,"Y","PrimaryOrder");
			Reusable.SendkeaboardKeys(SiebelModeObj.middleAppletDarkFibre.PrimaryOrder, Keys.TAB);
			Reusable.waitForSiebelLoader();
	
			verifyExists(SiebelModeObj.middleAppletDarkFibre.ContractTypeField,"ContractType");
		 	click(SiebelModeObj.middleAppletDarkFibre.ContractTypeField,"ContractType");
			verifyExists(SiebelModeObj.middleAppletDarkFibre.ContractType,"ContractType");
		 	sendKeys(SiebelModeObj.middleAppletDarkFibre.ContractType,"Lease","ContractType");
			Reusable.SendkeaboardKeys(SiebelModeObj.middleAppletDarkFibre.ContractType, Keys.TAB);
			Reusable.waitForSiebelLoader();
		 	ScrollIntoViewByString(SiebelModeObj.EthernetAccessNewFields.OKButton);
			verifyExists(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");
			click(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");
			Reusable.waitForSiebelLoader();
			siebelmod.GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			
		}

	}
	
	
	public void CarnorCompletedValidation(Object[] Inputdata) throws Exception {
		
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.CarnorCompletedValidation.OrderStatusDropdown,"Verify OrderStatusDropdown");
		click(SiebelModTechObj.CarnorCompletedValidation.OrderStatusDropdown,"Click on Order status drop down");	
		verifyExists(SiebelModTechObj.CarnorCompletedValidation.SelectCompleted,"Verify SelectCompleted");
		click(SiebelModTechObj.CarnorCompletedValidation.SelectCompleted,"Click Completed Status");				
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModTechObj.CarnorCompletedValidation.OrderComplete,"Verify OrderComplete");
		click(SiebelModTechObj.CarnorCompletedValidation.OrderComplete,"Click Order Complete");		
		Reusable.waitForSiebelLoader();
	
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		if (isElementPresent(By.xpath("//span[text()='Ok']"))) 
		{
			System.out.println("");
			System.out.println("Alert Present");
			verifyExists(SiebelModTechObj.CarnorCompletedValidation.AlertAccept,"Verify OrderComplete");
			click(SiebelModTechObj.CarnorCompletedValidation.AlertAccept,"Click AlertAccept");		
		}

		if (isElementPresent(By.xpath("//button[@class='colt-primary-btn']"))) 
		{
			System.out.println("");
			System.out.println("Alert Present");
			verifyExists(SiebelModTechObj.CarnorCompletedValidation.SubnetworkPopUP,"Verify SubnetworkPopUP");
			click(SiebelModTechObj.CarnorCompletedValidation.SubnetworkPopUP,"Click SubnetworkPopUP");

		}
			
	}
	
public void CarnorCompletedValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		
	String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
	Reusable.waitForSiebelLoader();
	if (ProdType.toString().equalsIgnoreCase("Wave")) 
	{
		if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
		{		
			Reusable.SelectDropDownValue("Reason Not IQNET","Offering Not applicable for IQNET");
			Reusable.waitForSiebelSpinnerToDisappear();	
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();	
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();	
		}
	}		
	else{
		if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
		{		
			Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
			Reusable.waitForSiebelSpinnerToDisappear();	
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();	
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();	
		}
	}
	click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
	click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"OrderCompleteYes");
	click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
	Reusable.waitForSiebelLoader();
	Reusable.waitToPageLoad();
	Reusable.waitForAjax();
			
		
		if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']"))) {
			System.out.println("");
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"SubnetworkPopUP");
			click(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
		}
 		Reusable.waitForSiebelLoader();
		String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus.contains("Progress")) {
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//button[text()='Yes']"))) {
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}
		System.out.println("Order complete");
		String CompValidation= null;
		CompValidation = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		
		if(CompValidation.equals("Completed")) 
		{
			Report.LogInfo("verify","<i></b> order status completed", "PASS");
		}
		else
		{
			Report.LogInfo("verify","<i></b> order status not competed", "Fail");
		}
		Reusable.waitForSiebelLoader();
		
		
		
		
		
		
		
		
		
		/*

		if(isElementPresent(By.xpath("//span[text()='Ok']"))) 
		{
			System.out.println("Alert Present");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
			click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Alert Accept");
		}

		Reusable.alertPopUp();
		Reusable.waitForSiebelLoader();
		
		String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
		if(Orderstatus.contains("Progress")) 
		{
			verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
			click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
			verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
			click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//button[text()='Yes']"))) 
			{
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
			}
		}	
*/
		Reusable.savePage();
		Reusable.waitForSiebelLoader();

	
	}
public void CarnorReasonIQNET(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws Exception
{
	String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
	Reusable.waitForSiebelLoader();
	Reusable.waitForSiebelSpinnerToDisappear();
	
	if (!ProdType.toString().equalsIgnoreCase("Wave")||(!ProdType.toString().equalsIgnoreCase("DCA Ethernet"))) 
	{
	   if (isElementPresent(By.xpath("//li[not(contains(@style,'display'))]//a[text()='Sites']")))
	  {
		verifyExists(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites Tab");
		click(SiebelAddProdcutObj.getReferenceNo.Sites,"Sites");
		Reusable.waitForSiebelSpinnerToDisappear();
	   }
	if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
	{		
		Reusable.SelectDropDownValue("Reason Not IQNET","NA");
		Reusable.waitForSiebelSpinnerToDisappear();	
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();	
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();	
	}
   }
}
	
	
}
