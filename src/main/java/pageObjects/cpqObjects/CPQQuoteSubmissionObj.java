package pageObjects.cpqObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CPQQuoteSubmissionObj {

	public static class QuoteSubmit
	{ 
		////Quote Submission to Siebel element
		public static final String CustomerSignature ="@xpath=//span[text()='Customer Signature']";//Customer Signature tab on the Quote 
		public static final String CustomerSignatureString ="@xpath=//span[text()='Customer Signature']";
		public static final String ProposalNoteTxBox ="@xpath=//textarea[contains(@id,'proposalNotes_t')]";//Proposal note text box under Generate Proposal section
		public static final String GenerateProposalBtn ="@xpath=//span[@class='oj-button-text' and text()='Generate Proposal']";//Proposal Button under Generate Proposal section
		public static final String ProposalGeneratedMsg ="@xpath=//font[contains(text(),'The proposal was last generated on')]";//Proposal Generated message 
		public static final String SendProposalSection ="@id=sendProposalLabel_t-label";//Send Proposal section
		public static final String SelectWorkflowDropdown ="@id=oj-select-choice-workflow_t";//Select Work flow Drop down
		public static final String EmailOption ="@xpath=//div[@class='oj-listbox-result-label' and contains(text(), 'Email')]";//Email option from Work flow Drop down
		public static final String ToRecipientTextbox ="@xpath=//input[contains(@id,'toRecepient_t')]";//To Recipient text box
		public static final String SendProposalBtn ="@xpath=//span[@class='oj-button-text' and contains(text(), 'Send Proposal')]";//Send Proposal button		


		public static final String ContactInformation ="@xpath=//span[text()='Contact Information']";//Contact information section
		public static final String ContactInformationString ="//span[text()='Contact Information']";
		public static final String ContactInformationChkBx ="@id=selectJET-1";//Check box into contact information record

		public static final String CompanyNameColName ="@xpath=//div[contains(text(),'Company Name A End')]";//Company Name column from contact information section
		//public static final String CompanyNameTxtField ="@xpath=//td[@class='oj-table-data-cell oj-form-control-inherit col-companyName type-textfield']";//Company Name text field from contact information section
		public static final String CompanyNameTxtField ="@id=additionalQuoteInformation_t-arraySet-table:_hdrCol0_49_7";//Company Name text field from contact information section

		public static final String CreateOrderBtn ="@xpath=//span[@class='oj-button-text' and contains(text(), 'Create Order')]"; //Create Order Button
		public static final String OrderCreatedMsg ="@xpath=//div[contains(text(),'#Your Order request is on-going. You will be updated shortly.')]";//Proposal Generated message
		public static final String DefaultDateElem ="@xpath=//a[contains(@class,'-selected')]";

		//Order Section
	
		public static final String OrderBtn="@xpath=//span[@class='oj-tabbar-item-label'][text()='Order']";
	
		public static final String QuoteActionField="@xpath=//label[contains(@id,'quoteActionAcceptOrReject')]";
		public static final String QuoteActionDropDown="@xpath=//div[contains(@id,'quoteActionAcceptOrReject')]//following-sibling::a";
		public static final String SelectQuoteAction="@xpath=//span[@class='oj-select-chosen' and contains(text(), 'Accept')]";
		
		public static final String StatusDropDownlable="@xpath=//label[contains(@id,'reasonForStatusWon')]";
		public static final String SelectStatusDropDown="@xpath=//div[@id='oj-select-choice-reasonForStatusWon_t']";
		public static final String StatusDropDown="@xpath=//ul[@id='oj-listbox-results-reasonForStatusWon_t']";
		public static final String SelectStaus="@xpath=//div[@class='oj-listbox-result-label' and contains(text(), 'Contract Terms')]";
		
		public static final String AcceptanceDocLable="@xpath=//label[@id='acceptanceDocument_t-label']";
		public static final String CustSignedDateLable="@xpath=//label[contains(@id,'customerSignedDate')]";
		public static final String CustomerSignedDateField="@xpath=//span[@title='Select Date.']";
		public static final String CurrentDate="@xpath=//a[@class='oj-enabled oj-selected']";
		
		public static final String RadioBtn="@xpath=//div[@id='pleaseConfirmThatTheCorrectSignedOrderFormHasBeenU-wrapper']//div[@class='oj-switch-thumb']";
		public static final String ConfirmQuoteBtn="@xpath=//span[contains(text(),'Confirm Quote')]";
		public static final String ConfirmQuoteBtnElem="//span[contains(text(),'Confirm Quote')]";
		public static final String SignedDate="@xpath=//span[@class='oj-inputdatetime-calendar-icon oj-clickable-icon-nocontext oj-component-icon oj-enabled oj-default oj-hover']";
		public static final String SelectCurrentDate="@xpath=//a[@class='oj-enabled oj-selected oj-hover']";
		public static final String MessageHeader="@xpath=//div[@class='message-header']";

		//Billing information 
		public static final String SelectProduct="@xpath=//*[@id=\'lig-table\']/table/tbody/tr[1]";
		public static final String billingInformationBtn="@xpath=//oj-button[@name='billing_information']";
		public static final String Selectcheckbox="@xpath=//*[@id=\'sELECT_A-2\']";
		public static final String Selectcheckbox1="@xpath=//input[contains(@id, 'sELECT_A')]";
		public static final String ClickOnApplyBtn="@xpath=//span[@class='oj-button-text'][text()='Apply']";
		public static final String bcnSearchTxb="@xpath=//input[@type='search']";
		public static final String pickDefaultBCNCbx="@xpath=//table[@id='bcnRecordsList']//..//i[1]";
		public static final String bcnSelectAllBtn="@xpath=//button[@id='lineItemsSelectionSelectAllBtn']";
		public static final String bcnUpdatedCloseBtn="@xpath=//button[@id='lineItemsSelectionUpdateAndCloseBtn']";
		public static final String bcnUpdatedCloseBtnString="//button[@id='lineItemsSelectionUpdateAndCloseBtn']";
		public static final String billingInfoCbx="@xpath=//input[@value='BCN' and @type='checkbox']//parent::span";
		
		public static final String uploadFileBtn="//input[contains(@id,'acceptanceDocument')]";
		public static final String additionalQuoteInfoTable="@xpath=//oj-table[contains(@id, 'additionalQuoteInformation')]";
		public static final String additionalQuoteInfoTableElm="//table[contains(@aria-describedby,'additionalQuoteInformation')]";
		
		//Service Order
		public static final String orderDetailsCbx="@xpath=//input[@value='Order' and @type='checkbox']//parent::span";
		
		public static final String forwardArrow="@xpath=//oj-conveyor-belt[@class='oj-conveyorbelt oj-component oj-component-initnode oj-complete']//div[@class='oj-enabled oj-conveyorbelt-overflow-indicator oj-end oj-default']/span[@class='oj-component-icon oj-conveyorbelt-overflow-icon oj-end']";
		public static final String backwardArrow="@xpath=//div[@class='oj-enabled oj-conveyorbelt-overflow-indicator oj-start oj-default']/span[@class='oj-component-icon oj-conveyorbelt-overflow-icon oj-start']";
		
		public static final String SelectoptionVal ="@xpath=//div[@id='oj-select-choice-selectOptionsTechApproval_t']";
        public static final String ProceedtoQuote ="@xpath=//*[contains(@name,'proceed_to_quote')]";
		
	}


}