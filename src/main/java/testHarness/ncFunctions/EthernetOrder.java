package testHarness.ncFunctions;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class EthernetOrder extends SeleniumUtils
{
    
	public static ThreadLocal<String> Orderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> ModifyOrderscreenURL = new ThreadLocal<>();
	public static ThreadLocal<Integer> workitemcounter=new ThreadLocal<>();


	GlobalVariables g = new GlobalVariables();
	ReusableFunctions Reusable = new ReusableFunctions();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();

	public void ExistingOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String Ordernumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_No");
		
		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Account Name Sorting");
		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Account Name Sorting");
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		select(EthernetOrderObj.CompositOrders.FilterSelectName,"Name");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
//		String Ordernumber = "Order #0000535455";
		//Ordernumber.set("Order #0000535455");
		//Log.info(Ordernumber.get());
		String[] arrOfStr = Ordernumber.split("#", 0);
		//Log.info(arrOfStr[1]);
		
//		ClickonElementByString(EthernetOrderObj.CompositOrders.arrorder+arrOfStr[1]+EthernetOrderObj.CompositOrders.arrorder2,60);
//		waitForAjax();
//		String Orderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
//		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL", Orderscreenurl);
//		
//		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Account bredcrumb");
//		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Account bredcrumb");
	}

	public void CreateCompositOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		verifyExists(EthernetOrderObj.CompositOrders.NewCompositeOrder,"CancelTask");
		click(EthernetOrderObj.CompositOrders.NewCompositeOrder,"CancelTask");
		
		//String	Ordernumber= getTextFrom(EthernetOrderObj.CompositOrders.OrderNumber,"Order Number");
		
		//Reporter.log(Ordernumber);
		
		//need to check
		//String[] arrOfStr = Ordernumber.get().split("#", 0); 
		//System.out.println(arrOfStr[1]);
		//String[] arrOfStr = Ordernumber.split("#", 0); 
		
		String Ordernumber = getTextFrom(EthernetOrderObj.CompositOrders.OrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No", Ordernumber);
		Reusable.waitForSiebelLoader();
		String[] arrOfStr = Ordernumber.split("#", 0);
		
		String orderDescription= "P2P Order Created using Automation script";
		verifyExists(EthernetOrderObj.CompositOrders.OrderDescription,"Order Description");  
		sendKeys(EthernetOrderObj.CompositOrders.VlanTagId,orderDescription,"Order Description");
		
		verifyExists(EthernetOrderObj.CompositOrders.Update,"Details are Updated");
		click(EthernetOrderObj.CompositOrders.Update,"Details are Updated");	
		
		String Orderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL", Orderscreenurl);
		System.out.println(Orderscreenurl);
		    
	}

	public void AddProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String ProductType = null;
		ProductType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
		
		verifyExists(EthernetOrderObj.CompositOrders.OrderTab,"OrderTab");
		click(EthernetOrderObj.CompositOrders.OrderTab,"OrderTab");
		
		verifyExists(EthernetOrderObj.CompositOrders.AddonOrderTab,"AddonOrderTab");
		click(EthernetOrderObj.CompositOrders.AddonOrderTab,"AddonOrderTab");
		
		if(ProductType.equalsIgnoreCase("Ethernet Connection Product"))
		{
			verifyExists(EthernetOrderObj.CompositOrders.EthernetProductCheckBox,"EthernetProductCheckBox");
			click(EthernetOrderObj.CompositOrders.EthernetProductCheckBox,"EthernetProductCheckBox");	
		}
		else
		{
		System.out.println("Not a valid option");
		}
		verifyExists(EthernetOrderObj.CompositOrders.Addbutton,"EthernetProductCheckBox");
		click(EthernetOrderObj.CompositOrders.Addbutton,"EthernetProductCheckBox");	
		
	}
	
	public void AddProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String Orderreferencenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Sys Ref ID");
		String OrderNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
		String CommericalProduct = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Commercial Product Name");
		String CarNorProductId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CarNor Product ID");
//		String RetainedNcsId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Retained NCS ID");
		String serviceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");

		verifyExists(EthernetOrderObj.CompositOrders.UnderlyningOrder,"UnderlyningOrder");
		click(EthernetOrderObj.CompositOrders.UnderlyningOrder,"UnderlyningOrder");	
		
		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit button");
		click(EthernetOrderObj.CompositOrders.Edit,"Edit button");	
		
		String Spark= "SPARK";
		verifyExists(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
		click(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.OrderSystemName,"SPARK","Order System Name");
		
		verifyExists(EthernetOrderObj.CompositOrders.Orderreferencenumber,"Orderreferencenumber");
		sendKeys(EthernetOrderObj.CompositOrders.Orderreferencenumber,Orderreferencenumber,"Orderreferencenumber");	
		
		String Topology = "Point to Point";
		verifyExists(EthernetOrderObj.CompositOrders.Topology,"Topology");
		click(EthernetOrderObj.CompositOrders.Topology,"Topology");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.Topology,Topology,"Topology");
		
		verifyExists(EthernetOrderObj.CompositOrders.Ordernumber,"Ordernumber");
		sendKeys(EthernetOrderObj.CompositOrders.Ordernumber,OrderNo,"Ordernumber");	
		
		verifyExists(EthernetOrderObj.CompositOrders.CommercialProductName,"CommercialProductName");
		sendKeys(EthernetOrderObj.CompositOrders.CommercialProductName,CommericalProduct,"CommercialProductName");	
		
		verifyExists(EthernetOrderObj.CompositOrders.CarnorProductId,"CarnorProductId");
		sendKeys(EthernetOrderObj.CompositOrders.CarnorProductId,CarNorProductId,"CarnorProductId");
		
//		verifyExists(EthernetOrderObj.CompositOrders.RetainedNCSerId,"Retained NCS ID");
//		sendKeys(EthernetOrderObj.CompositOrders.RetainedNCSerId,RetainedNcsId,"Retained NCS ID");

		String circuitCategory= "LE";
		verifyExists(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		click(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.CircuitCategory,circuitCategory,"Circuit Category");
		
		verifyExists(EthernetOrderObj.CompositOrders.ServiceBandwidth,"ServiceBandwidth");
		sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,serviceBandwidth,"ServiceBandwidth");
		Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth, Keys.ENTER);
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.Update,"Details are Updated");
		click(EthernetOrderObj.CompositOrders.Update,"Details are Updated");	
	}
	
	public void AddFeatureDetails() throws InterruptedException, IOException
	{
		String endSiteProduct = "End Site Product";
				
		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab," General Information Tab");
		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"General Information Tab");
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
		click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
		
//		verifyExists(EthernetOrderObj.CompositOrders.TypeofFeature,"Type of feature area");  
	    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,endSiteProduct,"Product Display");
	    Thread.sleep(30000);
	    Reusable.waitForAjax();
		click(EthernetOrderObj.CompositOrders.EndSite,"End Site Product Select");
	    
//		Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.TypeofFeature, Keys.ENTER);
	    
		verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
		click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
		
		waitForAjax();
		
	}
	
	public void DecomposeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		
		verifyExists(EthernetOrderObj.CompositOrders.OrderTab,"Navigate to Orders Tab");
		click(EthernetOrderObj.CompositOrders.OrderTab,"Navigate to Orders Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.Suborder,"Select Ethernet Connection Product Order");
		click(EthernetOrderObj.CompositOrders.Suborder,"Select Ethernet Connection Product Order");
		
		verifyExists(EthernetOrderObj.CompositOrders.Decompose,"Click on Decompose button");
		click(EthernetOrderObj.CompositOrders.Decompose,"Click on Decompose button");
		
		waitForAjax();
		
	}
	
	public void ProductDeviceDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String ResiliencOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Resilience Option");
		String accessTechnology= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access Technology");
		String accessType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access type");
		String aendSiteID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Site ID");
		String aCabinateID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Cabinet ID");
	    String aCabinateType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Cabinet Type");
		String aPortRole= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Port Role");
		String vlanTaggMode= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Vlan Tagging Mode");
	    String aendVlanTagId= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Vlan Tag Id");
		String bEndResiliencOption= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B end Resilience Option");
		String bEndaccessTechnology= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BEnd Access Technology");
		String bEndaccessType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BEnd Access type");
		String bEndSiteID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B end Site ID");
		String bPortRole= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend Port Role");
		String bvlanTaggMode= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend Vlan Tagging Mode");
		String bCabinateID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B end Cabinet ID");
	    String bCabinateType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B end Cabinet Type");
	    String bproductVlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend Port Role"); //need to change to columan name
	    String bendVlanTagId= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend Vlan Tag Id");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		verifyExists(EthernetOrderObj.CompositOrders.NewEndSiteProductAend,"NewEndSiteProductAend");
		click(EthernetOrderObj.CompositOrders.NewEndSiteProductAend,"NewEndSiteProductAend");
		
		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit");
		click(EthernetOrderObj.CompositOrders.Edit,"Edit");
		
		if(ResiliencOption.equalsIgnoreCase("Protected"))
		{
			String ResilienceOptions= "Protected";
			verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
			click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"Order System Name");
		}
		else
		{
			String ResilienceOptions= "Unprotected";
			verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
			click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"A End Resilience Option");
		}
		
		verifyExists(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Access Technology");
		click(EthernetOrderObj.CompositOrders.AccessTechnolgy,"A End Access Technology");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessTechnolgy,accessTechnology,"A End Access Technology");

		verifyExists(EthernetOrderObj.CompositOrders.AccessType,"Access Type");
		click(EthernetOrderObj.CompositOrders.AccessType,"A End Access Type");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessType,accessType,"A End Access Type");

		
		String siteEnd= "A END";
		verifyExists(EthernetOrderObj.CompositOrders.SiteEnd,"Site End");
		click(EthernetOrderObj.CompositOrders.SiteEnd,"A End Site End");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.SiteEnd,siteEnd,"A End Site End");
		
		verifyExists(EthernetOrderObj.CompositOrders.SiteID," SiteID");  
	    sendKeys(EthernetOrderObj.CompositOrders.SiteID,aendSiteID," SiteID");
	    

		verifyExists(EthernetOrderObj.CompositOrders.Update,"details are Updated");
		click(EthernetOrderObj.CompositOrders.Update,"details are Updated");
		
		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
		
		verifyExists(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
		click(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
		
		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
		click(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
		
		String presentConnectType= "LC/PC";
		verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
		click(EthernetOrderObj.CompositOrders.PresentConnectType,"A End Presentation Connector Type");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,presentConnectType,"A End Presentation Connector Type");
		
		
		verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
		click(EthernetOrderObj.CompositOrders.AccessportRole,"A End port Role");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,aPortRole,"A End Port Role");
		
		verifyExists(EthernetOrderObj.CompositOrders.VlanTaggingMode,"VLAN Tagging Mode");
		click(EthernetOrderObj.CompositOrders.VlanTaggingMode,"A End VLAN Tagging Mode");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.VlanTaggingMode,vlanTaggMode,"A End VLAN Tagging Mode");
		
//		String autoNegotiation= "Enable";
//		verifyExists(EthernetOrderObj.CompositOrders.AutoNegotiation,"Auto Negotiation");
//		click(EthernetOrderObj.CompositOrders.AutoNegotiation,"Auto Negotiation");
//		Reusable.waitForSiebelLoader();
//		selectByVisibleText(EthernetOrderObj.CompositOrders.AutoNegotiation,autoNegotiation,"Auto Negotiation");
		
		verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
		click(EthernetOrderObj.CompositOrders.Update,"Update");
		
		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
		
		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
		click(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
		
		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit");
		click(EthernetOrderObj.CompositOrders.Edit,"Edit");
	
		verifyExists(EthernetOrderObj.CompositOrders.CabinetID," Cabinet ID");  
	    sendKeys(EthernetOrderObj.CompositOrders.CabinetID,aCabinateID," Cabinet ID");
	    
		verifyExists(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
		click(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.CabinetType,aCabinateType,"Cabinet Type");
	    
	    waitForAjax();
	    
	    verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
		click(EthernetOrderObj.CompositOrders.Update,"Update");
	    
		 waitForAjax();
		 
		 String productVlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Port Role"); //need to change to columan name
		 

			if(productVlan.contains("VLAN"))
			{
				
				verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
				click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
				
				verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
				click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
				
				waitForAjax();

				String typeoffeatue="VLAN";
				verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
				click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
				
			    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,typeoffeatue,"Product Display");
			    Thread.sleep(30000);
			    Reusable.waitForAjax();
				click(EthernetOrderObj.CompositOrders.Vlan,"Select VLAN");
			    
				verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
				click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
				click(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
				
				verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit button");
				click(EthernetOrderObj.CompositOrders.Edit,"Edit button");
				
				String etherType= "VLAN (0x8100)";
				verifyExists(EthernetOrderObj.CompositOrders.Ethertype,"Ether type");
				String etherTypeclr= EthernetOrderObj.CompositOrders.Ethertype;
				clearTextBox(etherTypeclr);
			    sendKeys(EthernetOrderObj.CompositOrders.Ethertype,etherType,"Ether Type");
			    
			    waitForAjax();
			    
				verifyExists(EthernetOrderObj.CompositOrders.VlanTagId,"VlanTagId Details are Entered");  
			    sendKeys(EthernetOrderObj.CompositOrders.VlanTagId,aendVlanTagId,"VlanTagId Details are Entered");
			    
			    waitForAjax();
			    
			    verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
				click(EthernetOrderObj.CompositOrders.Update,"Update");
			    
				waitForAjax();
			
			}
			
			else
			{
				Reporter.log("A End Not VLAN");
			}
			Reporter.log("Required details are Updated");

			getUrl(Orderscreenurl);
			
		    verifyExists(EthernetOrderObj.CompositOrders.OrderTab,"Order Tab");
			click(EthernetOrderObj.CompositOrders.OrderTab,"Order Tab");
			
			verifyExists(EthernetOrderObj.CompositOrders.NewEndSiteProductBend,"New End SiteProduct Bend");
			click(EthernetOrderObj.CompositOrders.NewEndSiteProductBend,"New End Site Product Bend");
			
			verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit button");
			click(EthernetOrderObj.CompositOrders.Edit,"Edit button");
			
			if(bEndResiliencOption.equalsIgnoreCase("Protected"))
			{
				String ResilienceOptions= "Protected";
				verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
				click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"Order System Name");

			}
			else
			{
				String ResilienceOptions= "Unprotected";
				verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
				click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"A End Resilience Option");
			}
			
			verifyExists(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Access Technology");
			click(EthernetOrderObj.CompositOrders.AccessTechnolgy,"B End Access Technology");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessTechnolgy,bEndaccessTechnology,"B End Access Technology");

			verifyExists(EthernetOrderObj.CompositOrders.AccessType,"Access Type");
			click(EthernetOrderObj.CompositOrders.AccessType,"B End Access Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessType,bEndaccessType,"B End Access Type");
			
			String BsiteEnd= "B END";
			verifyExists(EthernetOrderObj.CompositOrders.SiteEnd,"Site End");
			click(EthernetOrderObj.CompositOrders.SiteEnd,"B End Site");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.SiteEnd,BsiteEnd,"B End Site");
			
			verifyExists(EthernetOrderObj.CompositOrders.SiteID," SiteID");  
		    sendKeys(EthernetOrderObj.CompositOrders.SiteID,bEndSiteID," SiteID");
		    

			verifyExists(EthernetOrderObj.CompositOrders.Update,"details are Updated");
			click(EthernetOrderObj.CompositOrders.Update,"details are Updated");
			
			verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
			click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
			
			verifyExists(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
			click(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
			
			verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
			click(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
			
			String bpresentConnectType= "LC/PC";
			verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
			click(EthernetOrderObj.CompositOrders.PresentConnectType,"B End Presentation Connector Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,bpresentConnectType,"B End Presentation Connector Type");
			
			verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
			click(EthernetOrderObj.CompositOrders.AccessportRole,"B End port Role");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,bPortRole,"B End Port Role");
			
			verifyExists(EthernetOrderObj.CompositOrders.VlanTaggingMode,"VLAN Tagging Mode");
			click(EthernetOrderObj.CompositOrders.VlanTaggingMode,"B End VLAN Tagging Mode");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.VlanTaggingMode,bvlanTaggMode,"B End VLAN Tagging Mode");
			
			verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
			click(EthernetOrderObj.CompositOrders.Update,"Update");
			
			verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
			click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
			
			verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
			click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
			
			verifyExists(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
			click(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
			
			verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit");
			click(EthernetOrderObj.CompositOrders.Edit,"Edit");
		
			verifyExists(EthernetOrderObj.CompositOrders.CabinetID," Cabinet ID");  
		    sendKeys(EthernetOrderObj.CompositOrders.CabinetID,bCabinateID," Cabinet ID");
		    
			verifyExists(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
			click(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.CabinetType,bCabinateType,"Cabinet Type");
		    
		    waitForAjax();
		    
		    verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
			click(EthernetOrderObj.CompositOrders.Update,"Update");
		    
			 waitForAjax();
			 
				if(bproductVlan.contains("VLAN"))
				{
					
					verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
					click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
					
					verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
					click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
					
					waitForAjax();

					String typeoffeatue="VLAN";
					verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
					click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
					
				    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,typeoffeatue,"Product Display");
				    Thread.sleep(30000);
				    Reusable.waitForAjax();
					click(EthernetOrderObj.CompositOrders.Vlan,"Select VLAN");
				    
					verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
					click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
					
					waitForAjax();
					
					verifyExists(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
					click(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
					
					verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit button");
					click(EthernetOrderObj.CompositOrders.Edit,"Edit button");
					
					String etherType= "VLAN (0x8100)";
					verifyExists(EthernetOrderObj.CompositOrders.Ethertype,"Ether type");
					String etherTypeclr= EthernetOrderObj.CompositOrders.Ethertype;
					clearTextBox(etherTypeclr);
				    sendKeys(EthernetOrderObj.CompositOrders.Ethertype,etherType,"Ether Type");
				    
				    waitForAjax();
				    
					verifyExists(EthernetOrderObj.CompositOrders.VlanTagId,"VlanTagId Details are Entered");  
				    sendKeys(EthernetOrderObj.CompositOrders.VlanTagId,bendVlanTagId,"VlanTagId Details are Entered");
				    
				    waitForAjax();
				    
				    verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
					click(EthernetOrderObj.CompositOrders.Update,"Update");
				    
					waitForAjax();
				
				}
				else
				{
					Reporter.log("B End Not VLAN");
				}
				Reporter.log("Required details are Updated");
				getUrl(Orderscreenurl);
			}

	public void ProcessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		
		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Navigate to Accounts Composite Orders Tab");
		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Navigate to Accounts Composite Orders Tab");
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Navigate AccountNameSorting");
		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Navigate AccountNameSorting");
		
		String Order_Number = null;
		Order_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_No");
		String arrOfStr = EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrOfStr2;
		verifyExists(arrOfStr,"Select the Composite Order");
		click(arrOfStr,"Select the Composite Order");

		verifyExists(EthernetOrderObj.CompositOrders.StartProccessing,"Click on Start Processing Link");
		click(EthernetOrderObj.CompositOrders.StartProccessing,"Click on Start Processing Link");
	}
	
	public void CompleteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Order_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_No");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		String WorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Work Items");

//		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
//		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
//		
//		String arrOfStr = EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrOfStr2;
//		verifyExists(arrOfStr,"Composite Order");
//		click(arrOfStr,"Composite Order");

		getUrl(Orderscreenurl);
		
		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		
		waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
		click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
		
		waitForAjax();
		
		for (int k=1; k<=Integer.parseInt(WorkItems);k++)
		{
			waitForElementToAppear(EthernetOrderObj.CompositOrders.TaskReadytoComplete,90,10000);
			click(EthernetOrderObj.CompositOrders.TaskReadytoComplete,"Workitem in Ready status");
			//need to verify
			CompleteInflightworkitem(GetText(NcOrderObj.taskDetails.TaskTitle),testDataFile, sheetName, scriptNo,dataSetNo);
		}
		
		getUrl(Orderscreenurl);
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Order_Number);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrorder1,180,20000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrorder1);
		if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+Order_Number +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Order_Number +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+Order_Number +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Order_Number +" Execution InProgress");
		}
	}
	
	public void GotoErrors(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String Order_Number = null;
		Order_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_No");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting the Orders");
		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting the Orders");
		
		//Errors.set(GetText(getwebelement("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::*[4]")));
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Get the value of In Error column for the order");
		//if(Errors.get().equalsIgnoreCase("Blocked by Errors"))
			
     String	arrOfStr= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrOfStr2);
		if(arrOfStr.equalsIgnoreCase("Blocked by Errors"))
		{
			click(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrOfStr2);
			
			verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
			click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
			
			verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			
			waitForAjax();
			
			verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
			click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
			
			waitForAjax();
			
		}
		else 
		{	
			//Log.info("Not required to Navigate to Errors tab");
			//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order did not have any errors to be Captured");
			System.out.println("Order did not have any errors to be Captured");
		}
	}

	public void CancelStubTasks() throws InterruptedException, IOException
	{
		Orderscreenurl.set(getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href"));
		openurl(Orderscreenurl.get());
		
		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
		click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
		
		waitForAjax();
		
		String	FailedTask= getTextFrom(EthernetOrderObj.CompositOrders.FailedTaskName,"FailedTaskName");
		Reporter.log(FailedTask);
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		
		waitForAjax();
		
		String failedTasks = EthernetOrderObj.CompositOrders.FailedTaskName1+ FailedTask + EthernetOrderObj.CompositOrders.FailedTaskName2;
		verifyExists(failedTasks,"checkbox for failed task");
		click(failedTasks,"checkbox for failed task");
		
		verifyExists(EthernetOrderObj.CompositOrders.CancelTask,"CancelTask");
		click(EthernetOrderObj.CompositOrders.CancelTask,"CancelTask");
		
		waitForAjax();
		
	}

	
	public void RetryErrors() throws Exception
	{
		Orderscreenurl.set(getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href"));
		openurl(Orderscreenurl.get());
		
		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
		click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
		waitForAjax();
		
		String	FailedTask= getTextFrom(EthernetOrderObj.CompositOrders.FailedTaskName,"FailedTaskName");
		Reporter.log(FailedTask);
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		
		waitForAjax();
		
		String failedTasks = EthernetOrderObj.CompositOrders.FailedTaskName1+ FailedTask + EthernetOrderObj.CompositOrders.FailedTaskName2;
		verifyExists(failedTasks,"checkbox for failed task");
		click(failedTasks,"checkbox for failed task");
		
		verifyExists(EthernetOrderObj.CompositOrders.RetryTask,"CancelTask");
		click(EthernetOrderObj.CompositOrders.RetryTask,"CancelTask");
		
		waitForAjax();
	}

	public void ProcessInflightOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");	
		
		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountNameSorting");
		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountNameSorting");	
		
		String Order_Number = null;
		Order_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
		String arrOfStrs = EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.Str3;
		verifyExists(arrOfStrs,"Modified Composite Order");
		click(arrOfStrs,"Modified Composite Order");
		
		verifyExists(EthernetOrderObj.CompositOrders.StartProccessing,"Start Processing button");
		click(EthernetOrderObj.CompositOrders.StartProccessing,"Start Processing button");
		
	}

	public void CompleteInflightOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Account Name Sorting");
		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Account Name Sorting");
		
		String Order_Number = null;
		Order_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
		String arrOfStrs = EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arr1;
		verifyExists(arrOfStrs,"Composite Order");
		click(arrOfStrs,"Composite Order");
		
		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
		click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
		
		waitForAjax();
		
		String bendPortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend Port Role");
		
		for (int k=1; k<=Integer.parseInt(bendPortRole);k++)
		{
			waitForElementToAppear(EthernetOrderObj.CompositOrders.TaskReadytoComplete,60);
			click(EthernetOrderObj.CompositOrders.TaskReadytoComplete,"Workitem in Ready status");
			//need to verify
			CompleteInflightworkitem(GetText(NcOrderObj.taskDetails.TaskTitle),testDataFile, sheetName, scriptNo,dataSetNo);
//			CompletInflightworkitem(GetText2(getwebelement(xml.getlocator("//locators/Tasks/TaskTitle"))),Inputdata);
		}
		
		Orderscreenurl.set(getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href"));
		openurl(Orderscreenurl.get());
		
		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
		
		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Accounts Composite Orders Tab");
		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Accounts Composite Orders Tab");
		
	}

	public void CompleteInflightworkitem(String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
					
	{
		String bEndSiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Site ID");
		String aEndSiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Site ID");
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Resilience Option");
//		String AccessNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access NW Element");
//		String aendAccessPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access port");
//		String CPENNiPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
//		String AendCPENN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Resilience Option");
		String BendAccessNWElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Access NW Element");
		String BendAccessport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Access port");
		String BendCPENNITrunkPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 1");
		String BendCPENNITrunkPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 2");
//		String AendResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Resilience Option");
//		String BendResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Resilience Option");
		String AendAccessNWElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access NW Element");
		String AendAccessport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access port");
		String AendCPENNITrunkPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
		String AendCPENNITrunkPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
		String AendPeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
		String AendPEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 1");
		String AendPEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 2");
		String AendVcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend VCX Control");
		String BendPeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend PE NW Element");
		String BendPEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend PE Port 1");
		String BendPEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend PE Port 2");
		String BendVcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend VCX Control");
		String Beacon = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Beacon");
		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");
		
		System.out.println("In Switch case with TaskName :" +taskname);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Start completion of Task: " +taskname[0]);
		switch(taskname[0].trim())
		{
			case "Reserve Access Resources":
				
			verifyExists(EthernetOrderObj.CompositOrders.OrderDifferenceTab,"Order Difference Tab");
			click(EthernetOrderObj.CompositOrders.OrderDifferenceTab,"Order Difference Tab");
			Reusable.waitForAjax();
			
			String RaSiteEnd = getTextFrom(EthernetOrderObj.CompositOrders.RaEndCheck);
			
			verifyExists(EthernetOrderObj.CompositOrders.ServiceInfoTab,"Service Information Tab");
			click(EthernetOrderObj.CompositOrders.ServiceInfoTab,"Service Information Tab");
			
			verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
			click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			
			Reusable.waitForAjax();
			
//			if(!aEndSiteID.equals(bEndSiteID))
//			{
				if(RaSiteEnd.equalsIgnoreCase("A End"))
				{
					if(AEndResilienceOption.equalsIgnoreCase("Protected"))
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AendAccessNWElement);
						
						waitForAjax();
						
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AendAccessport);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,AendCPENNITrunkPort1);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,AendCPENNITrunkPort2);
						waitForAjax();
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//						workitemcounter.set(workitemcounter.get()+1);
					}
					else
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						waitForAjax();

						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AendAccessNWElement);
						waitForAjax();
						
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AendAccessport);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,AendCPENNITrunkPort1);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//						workitemcounter.set(workitemcounter.get()+1);
					}
				}
				else 
				{
					if(BEndResilienceOption.equalsIgnoreCase("Protected"))
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,BendAccessNWElement);
						
						waitForAjax();
						
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,BendAccessport);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,BendCPENNITrunkPort1);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,BendCPENNITrunkPort2);
						waitForAjax();
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//						workitemcounter.set(workitemcounter.get()+1);
					}
					else
					{
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,BendAccessNWElement);
						
						waitForAjax();
						
						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,BendAccessport);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						waitForAjax();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,BendCPENNITrunkPort1);
						waitForAjax();

						Reusable.waitForSiebelLoader();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
						waitForAjax();
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						waitForAjax();
						Reusable.waitForAjax();
//						workitemcounter.set(workitemcounter.get()+1);
					}
				}
			break;

			case "Transport Circuit Design":
//			if(!aEndSiteID.equals(bEndSiteID))
//			{
				String SiteEnd = getTextFrom(EthernetOrderObj.CompositOrders.EndCheck);
				System.out.println("End Site Value is :" +SiteEnd);
				if(SiteEnd.equalsIgnoreCase("A End"))
				{
					if (AEndResilienceOption.contains("Protected"))
					{
						//Code for Protected
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
						clearTextBox(NcOrderObj.workItems.PENetworkElement);
						sendKeys(NcOrderObj.workItems.PENetworkElement,AendPeNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
						Reusable.waitForAjax();
						
						if (DeviceType.contains("Stub"))
						{
							if (AendAccessNWElement.contains("SC"))
							{
								verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//								clearTextBox(NcOrderObj.workItems.VCXController);
								sendKeys(NcOrderObj.workItems.VCXController,AendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
								Reusable.waitForAjax();
								Reusable.waitForpageloadmask();
								sendKeys(NcOrderObj.workItems.VCXController,AendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
							}			
//							if (Beacon.contains("Beacon"))
//							{
//								verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//								clearTextBox(NcOrderObj.workItems.Beacon);
//								sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//								Reusable.waitForAjax();
//								Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//								Reusable.waitForSiebelSpinnerToDisappear();
//							}
						}						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
						click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
						
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PePort,"PE Port");
						clearTextBox(NcOrderObj.workItems.PePort);
						Reusable.waitForAjax();
						
						sendKeys(NcOrderObj.workItems.PePort,AendPEPort1);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
		
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
						click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
						
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PePort,"PE Port");
						clearTextBox(NcOrderObj.workItems.PePort);
						Reusable.waitForAjax();
						
						sendKeys(NcOrderObj.workItems.PePort,AendPEPort2);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
		
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					else
					{
		            	//Code for Unprotected
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
						clearTextBox(NcOrderObj.workItems.PENetworkElement);
						sendKeys(NcOrderObj.workItems.PENetworkElement,AendPeNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(NcOrderObj.workItems.PePort,"PE Port");
						clearTextBox(NcOrderObj.workItems.PePort);
						Reusable.waitForAjax();
						sendKeys(NcOrderObj.workItems.PePort,AendPEPort1);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
						Reusable.waitForAjax();
		
						if (DeviceType.contains("Stub"))
						{
							if (AendAccessNWElement.contains("SC"))
							{
								verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//								clearTextBox(NcOrderObj.workItems.VCXController);
								sendKeys(NcOrderObj.workItems.VCXController,AendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
								Reusable.waitForAjax();
								Reusable.waitForpageloadmask();
								sendKeys(NcOrderObj.workItems.VCXController,AendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
							}
			
//							if (Beacon.contains("Beacon"))
//							{
//								verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//								clearTextBox(NcOrderObj.workItems.Beacon);
//								sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//								Reusable.waitForAjax();
//								Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//								Reusable.waitForSiebelSpinnerToDisappear();
//							}
						}						
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						Reusable.waitForAjax();
					}
				}
				else
				{
					if (BEndResilienceOption.contains("Protected"))
					{
						//Code for Protected
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
						clearTextBox(NcOrderObj.workItems.PENetworkElement);
						sendKeys(NcOrderObj.workItems.PENetworkElement,BendPeNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
						Reusable.waitForAjax();
						
						if (DeviceType.contains("Stub"))
						{
							if (BendAccessNWElement.contains("SC"))
							{
								verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//								clearTextBox(NcOrderObj.workItems.VCXController);
								sendKeys(NcOrderObj.workItems.VCXController,BendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
								Reusable.waitForAjax();
								Reusable.waitForpageloadmask();
								sendKeys(NcOrderObj.workItems.VCXController,BendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
							}			
//							if (Beacon.contains("Beacon"))
//							{
//								verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//								clearTextBox(NcOrderObj.workItems.Beacon);
//								sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//								Reusable.waitForAjax();
//								Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//								Reusable.waitForSiebelSpinnerToDisappear();
//							}
						}						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
						click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
						
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PePort,"PE Port");
						clearTextBox(NcOrderObj.workItems.PePort);
						sendKeys(NcOrderObj.workItems.PePort,BendPEPort1);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
		
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
						click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
						
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PePort,"PE Port");
						clearTextBox(NcOrderObj.workItems.PePort);
						sendKeys(NcOrderObj.workItems.PePort,BendPEPort2);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
		
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					else
					{
		            	//Code for Unprotected
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						Reusable.waitForAjax();
		
						verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
						clearTextBox(NcOrderObj.workItems.PENetworkElement);
						sendKeys(NcOrderObj.workItems.PENetworkElement,BendPeNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(NcOrderObj.workItems.PePort,"PE Port");
						clearTextBox(NcOrderObj.workItems.PePort);
						sendKeys(NcOrderObj.workItems.PePort,BendPEPort1);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
						Reusable.waitForAjax();
		
						if (DeviceType.contains("Stub"))
						{
							if (BendAccessNWElement.contains("SC"))
							{
								verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//								clearTextBox(NcOrderObj.workItems.VCXController);
								sendKeys(NcOrderObj.workItems.VCXController,BendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
								Reusable.waitForAjax();
								Reusable.waitForpageloadmask();
								sendKeys(NcOrderObj.workItems.VCXController,BendVcxControl);
								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
							}
//							if (Beacon.contains("Beacon"))
//							{
//								verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//								clearTextBox(NcOrderObj.workItems.Beacon);
//								sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//								Reusable.waitForAjax();
//								Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//								Reusable.waitForSiebelSpinnerToDisappear();
//							}
						}						
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		
						verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
						Reusable.waitForAjax();
					}
				}
			break;
			
	    	case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				if(DeviceType.contains("Stub"))
				{
					ErrorWorkItems.errorWorkitems("EIP: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
					ErrorWorkItems.checkipaddress("Check IP address in DNS",testDataFile, sheetName, scriptNo, dataSetNo);
				}
	    	}
	    	break;
	    	
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					Reusable.waitForAjax();
					verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
					Reusable.waitForAjax();
					
					for(int i=1;i<=3;i++)
    				{
						ErrorWorkItems.errorWorkitems("Beacon: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
    				}
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Activation Start Confirmation":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				if(DeviceType.contains("Stub"))
				{
					ErrorWorkItems.errorWorkitems("Check GX/LTS Smarts Status in NC OSS",testDataFile, sheetName, scriptNo, dataSetNo);
					for(int i=1;i<=2;i++)
					{
						ErrorWorkItems.errorWorkitems("Create Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
					}
				}
			}
			break;
		}
	}
	
	public void NavigatebacktoAccountScreen() throws Exception
	{
		//Geturl(OrderscreenURL.get());
        //Clickon(getwebelement(xml.getlocator("//locators/Accountbredcrumb")));	
		Orderscreenurl.set(getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href"));
		openurl(Orderscreenurl.get());
	}
}