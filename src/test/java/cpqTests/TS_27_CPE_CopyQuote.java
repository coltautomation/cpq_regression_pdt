package cpqTests;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;

public class TS_27_CPE_CopyQuote extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQQuoteCreation CreateQuote = new CPQQuoteCreation();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testCPECopyQuote(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
				
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			
			Accounts.NavigateToAccounts();
			String Product_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Product_Type");
			String Network_Reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Network_Reference");
			if (Product_Type.equalsIgnoreCase("Addon")) { 
				DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Related_Network_Reference", Network_Reference); 
			}
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");				
			AddProduct.AddProductToQuote("CPESolutionsSite");
			String Copy_Quote_At=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Copy_Quote_At");
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
			String sResult;											
			
			if(Copy_Quote_At.equalsIgnoreCase("Created")) 
			{
	               AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				   AddProduct.AddProductToQuote("CPESite");						
				   AddProduct.cpeIntialConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				   AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				   AddProduct.cpeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				   AddProduct.markPartialSave(ProductName);
								
//			       calling the below method to perform copyquote					
					Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
					System.out.println("Quote ID:  "+Quote_ID);	
				
			}
			else if(Copy_Quote_At.equalsIgnoreCase("To be Priced")) 
			{
				
				    AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.AddProductToQuote("CPESite");						
					AddProduct.cpeIntialConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.cpeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					AddProduct.markPartialSave(ProductName);
				
					verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
					String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
					System.out.println("quoteStage: "+quoteStage);
					if (quoteStage.equalsIgnoreCase("SE Required")) 
					{
						AddProduct.verifyQuoteStage("SE Required");
						AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				            
						//Login with CPQ SE user
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
						CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
						AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
						Approvals.SwitchCPQUser("Sales_User", Quote_ID);
					}
			}else {
				
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");				
				
			}
		
			String Discount_Percentage = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Discount_Percentage");
			String Discount_Applicable = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Discount_Applicable");
			if(Discount_Applicable.equalsIgnoreCase("Yes")) {
				
//				Calling the below method to enter the discounting process
				sResult = AddProduct.discountingProcess(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//				calling the below method to verify quote stage and status
				AddProduct.SubmitForApproval();
				
				if (Integer.parseInt(Discount_Percentage) > 5) {
//				calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Pending Governance Approval");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
			if (Integer.parseInt(Discount_Percentage) > 10) {
//				calling the below method to switch to VP sales user
				sResult = Approvals.SwitchCPQUser("VPSales1_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} else {
//				calling the below method to switch to VP sales user
				sResult = Approvals.SwitchCPQUser("VPSales2_User", Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}
			
//			calling the below method to switch to VP sales user
			sResult = AddProduct.quoteDiscountGovernanceApproval("Reject");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below method to verify quote stage and status
			sResult = AddProduct.verifyQuoteStage("Approval Denied");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below method to verify quote stage and status
			sResult = AddProduct.logoutCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
			
//			Calling the Below method to perform C4C Login
			close();
			openBrowser(g.getBrowser());
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
						
//			calling the below method to navigate to quotes from c4c main page
			AddProduct.navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below method to search the quote from c4c
			sResult = AddProduct.searchQuoteC4C(Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			} 				
			}

			Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
			
			Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
		String Copy_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Copy_Type");

		if(Copy_Type.equalsIgnoreCase("LineItem")) {
			Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");	}
		else
		{
			Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
		}

			//Calling below method to tear down if stage is to be priced
			if(!Copy_Quote_At.equalsIgnoreCase("To be Priced")) {
				
				

				//Calling the below method to Capture Hub Id
				if (ProductName.equalsIgnoreCase("EthernetHub")) 
				{
					ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
					String HubId = AddProduct.addHubrefernce(ProductName);
					if (HubId.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					if(HubId.equals("True"))DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Hub_Reference", HubId);
				}

				if(!Discount_Applicable.equalsIgnoreCase("Yes")) 
				{
					 Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
					//        calling the below method to move the quote status to Commercial Approval
					
						String BaseCost=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Base_Cost");
						ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
						verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
						String quoteStage1 = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
						System.out.println("quoteStage: "+quoteStage1);
						if (quoteStage1.equalsIgnoreCase("To be Priced")) 
						{
							AddProduct.editEthernetProductConfiguration(); 
							Approvals.SwitchCPQUser("AD_User", Quote_ID);
							AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
							AddProduct.verifyQuoteStage("Portfolio Pricing Review");
							AddProduct.clickProductConfigurationBtn(ProductName);
							AddProduct.UpdateBaseCostException(BaseCost);
						}
						
					AddProduct.SubmitForApproval();
		
					//		calling the below method to verify quote stage and status
					sResult = AddProduct.verifyQuoteStage("Approved");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}

				//calling the below method to perform copyquote when commerically approved
				Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
			//	if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				//calling the below method to add Legal and Technical Contact Info
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);

				
				//calling the below method to navigate to quotes from c4c main page
				AddProduct.navigateQuotesFromHomepage();
			//	if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				//calling the below method to search the quote from c4c
				sResult = AddProduct.searchQuoteC4C(Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				//calling the below method to verify quote stage and status
				sResult = AddProduct.verifyQuoteStage("Approved");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				//calling the below method to perform copyquote
				Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				//Entering BCN values for CPE Solution Service
				String sProduct = ProductName.replaceAll("(?!^)([A-Z])", " $1");
				String refColName = null; 

				String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
				if (Copy_Type.equalsIgnoreCase("Lineitem")) 
				{
					sResult =AddProduct.copyQuoteLineItemEntries(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				} else {
					Submit.addBillingInformation(ProductName, BCN, "BCN NRC");				
				}

				//Calling the below method to generate and send proposal
				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

				//calling the below method to make contact info entry
				if(!Product_Type.equalsIgnoreCase("Addon")) {
					if (Copy_Type.equalsIgnoreCase("Lineitem")) {
						sResult = AddProduct.navigateContactInfoTab();
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						sResult =AddProduct.copyQuoteLineItemEntries(testDataFile, dataSheet, scriptNo, dataSetNo,"Contact");
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					} 
					else {
						Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);		
					}
				}else {
					//	calling the below method to login to cpq as cst user
					sResult = AddProduct.contactInfoEntryAddons(testDataFile, scriptNo, dataSetNo, ProductName);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}

				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.verifyQuoteStage("Ordered");	

				Submit.SubmitOrder();
				AddProduct.verifyQuoteStage("Ordered");

				//calling the below method to perform copyquote
				Quote_ID = AddProduct.CopyQuoteOrderTypeAt(testDataFile, dataSheet, scriptNo, dataSetNo,Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

				//calling the below method to capture the service order
				if (Copy_Type.equalsIgnoreCase("Lineitem")) {
					sResult =AddProduct.copyQuoteLineItemEntries(testDataFile, dataSheet, scriptNo, dataSetNo,"ServiceOrder");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				} else {
					Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}

				//calling the below method to verify quote stage and status
				CPQLogin.CPQLogout();	
			}

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	
	
}
