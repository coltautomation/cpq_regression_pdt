package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.c4cObjects.C4CAccountsObj;
import testHarness.commonFunctions.ReusableFunctions;

public class APT_IPAccessNoCPEHelper extends SeleniumUtils

{

	private static final String String = null;
	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();

	public static String editedInterfaceName = null;

	public void createcustomer(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");

		// ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Customer
		// Creation Functionality");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink, " Manage Customer Service Link");
		mouseMoveOn(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.createcustomerlink, " create customer link");
		click(APTIPAccessNoCPE.APTNoCPE.createcustomerlink, "Create Customer Link");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.createcustomer_header, "create customer page header");

		// scrolltoend();
		verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");
		click(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");

		// Warning msg check
		verifyExists(APTIPAccessNoCPE.APTNoCPE.customernamewarngmsg, "Legal Customer Name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.countrywarngmsg, "Country");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.ocnwarngmsg, "OCN");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.typewarngmsg, "Type");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.emailwarngmsg, "Email");

		// Create customer by providing all info

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nametextfield, "name");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.nametextfield, name, "name");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.maindomaintextfield, "Main Domain");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.maindomaintextfield, maindomain, "Main Domain");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.country, "country");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.country, country, "country");
		addDropdownValues_commonMethod("Country", APTIPAccessNoCPE.APTNoCPE.country, country);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.ocntextfield, "ocn");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.ocntextfield, ocn, "ocn");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.referencetextfield, "reference");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.referencetextfield, reference, "reference");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.technicalcontactnametextfield, "technical contact name");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.technicalcontactnametextfield, technicalcontactname,
				"technical contact name");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.typedropdown, "technical contact name");
		addDropdownValues_commonMethod("Type", APTIPAccessNoCPE.APTNoCPE.type, type);

		// sendKeys(APTIPAccessNoCPE.APTNoCPE.typedropdown, type, "technical
		// contact name");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield, "email");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield, email, "email");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.phonetextfield, "phone text field");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.phonetextfield, phone, "phone text field");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.faxtextfield, "fax text field");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.faxtextfield, fax, "fax text field");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");
		click(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.customercreationsuccessmsg, "Customer successfully created.");

	}

	public void selectCustomertocreateOrder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

		mouseMoveOn(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.CreateOrderServiceLink, "Create Order Service Link");
		click(APTIPAccessNoCPE.APTNoCPE.CreateOrderServiceLink, "Create Order Service Link");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		// click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield, "name");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield, name, "name");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"name");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"*","name");

		waitForAjax();

		addDropdownValues_commonMethod("Choose a customer", APTIPAccessNoCPE.APTNoCPE.chooseCustomerdropdown, name);
		// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

	}

	public void createorderservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderService");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.order_contractnumber_warngmsg, "order contract number");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.servicetype_warngmsg, "service type");

		if (neworder.equalsIgnoreCase("NO")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.newordertextfield, "new order textfield");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.newrfireqtextfield, "new rfireq textfield");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.createorderbutton, "create order button");
			click(APTIPAccessNoCPE.APTNoCPE.createorderbutton, "create order button");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.OrderCreatedSuccessMsg, "Order Created Success Msg");

		} else if (existingorderservice.equalsIgnoreCase("NO")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.selectorderswitch, "select order switch");
			click(APTIPAccessNoCPE.APTNoCPE.selectorderswitch, "select order switch");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown, "existing order drop down");

			click(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown, "existing order dropdown");
			// sendKeys(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,existingordernumber,"existingordernumber");

			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
					APTIPAccessNoCPE.APTNoCPE.existingorderdropdown, existingordernumber);
			waitForAjax();

		} else {
			Reporter.log("Order not selected");
		}

	}

	public void verifyServicetypeAndNetworkConfigurationSelection(String testDataFile, String dataSheet,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		String NetworkConfiguration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NetworkConfiguration");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.ServiceTypeWarningMessage, "Service Type Warning Message");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.servicetypetextfield, "service type textfield");
		addDropdownValues_commonMethod("Service Type", APTIPAccessNoCPE.APTNoCPE.servicetypetextfield, servicetype);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.NetworkConfigurationWarningMessage, "Network Configuration");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.networkconfigurationinputfield, "network configuration input field");
		click(APTIPAccessNoCPE.APTNoCPE.networkconfigurationinputfield, "network configuration input field");

		String servicesubtype = APTIPAccessNoCPE.APTNoCPE.subtype1 + NetworkConfiguration
				+ APTIPAccessNoCPE.APTNoCPE.subtype2;
		verifyExists(servicesubtype, "service subtype");
		click(servicesubtype, "service subtype");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		String createOrder = "Create Order / Service";
		String orderService = getTextFrom(APTIPAccessNoCPE.APTNoCPE.createorderservice_header,"create order service_header");
		orderService.equalsIgnoreCase(createOrder);
		Reporter.log("Create Order");
	}

	public void verifyServiceCreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ServiceIdentification");
		String BillingType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "BillingType");
		String TerminationDate = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TerminationDate");
		String EmailService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EmailService");
		String PhoneService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneService");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");
		String ManageService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManageService");
		String RouterConfigurationViewIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv4");
		String RouterConfigurationViewIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv6");
		String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PerformanceReporting");
		String IPGuardian = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IPGuardian");
		String SNMPNotification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"SNMPNotification");
		String DeliveryChannel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DeliveryChannel");
		String RouterBasedFirewall = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterBasedFirewall");
		String TrapTargetAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TrapTargetAddress");
		String Qos = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Qos");
		String ExtendPErangetocustomerLAN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExtendPErangetocustomerLAN");

		// if(isElementPresent(By.xpath("//div[text()='Create Order /
		// Service'])")))
		// {
		verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
		click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.sidwarngmsg, "Service Identification");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_warngmsg, "Billing Type");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield, "new order textfield");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield, ServiceIdentification, "newordertextfield");

		Thread.sleep(15000);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, "Billing Type");
		selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, BillingType, "Billing Type");
		// click(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,"billingtype");

		// addDropdownValues_commonMethod("Billing
		// Type",APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,BillingType);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.terminationdate_field, "Terminate Date");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.terminationdate_field, TerminationDate, "Terminate Date");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, "Email Service");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, EmailService, "Email Service");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield, "new
		// order textfield");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield,
		// PhoneService, "newordertextfield");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.remarktextarea, "new order
		// textfield");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.remarktextarea, Remarks,
		// "newordertextfield");

		if (ManageService.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageServicecheckbox, "Manage Service checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.ManageServicecheckbox, "Manage Service checkbox");
		} else {
			Reporter.log("Not Manage Service checkbox");
		}
		if (RouterConfigurationViewIPv4.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv4checkbox,
					"Router Configuration View IPv4 checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv4checkbox, "Router Configuration View IPv4");
		} else {
			Reporter.log("Not Router Configuration View IPv4 checkbox ");
		}

		if (RouterConfigurationViewIPv6.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv6checkbox,
					"Router Configuration View IPv6 checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv6checkbox, "Router Configuration View IPv6");
		} else {
			Reporter.log("Not Router Configuration View IPv6 checkbox ");
		}
		if (PerformanceReporting.equalsIgnoreCase("Yes")) {
			// verifyExists(APTIPAccessNoCPE.APTNoCPE.performancereportingcheckbox,
			// "performance reporting checkbox");
			// click(APTIPAccessNoCPE.APTNoCPE.performancereportingcheckbox,
			// "performance reporting checkbox");
		} else {
			Reporter.log("Not performance reporting checkbox");
		}

		if (IPGuardian.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.IPGuardiancheckbox, "IP Guardian checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.IPGuardiancheckbox, "IP Guardian checkbox");
		} else {
			Reporter.log("Not IP Guardian checkbox");
		}
		if (SNMPNotification.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification checkbox");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, TrapTargetAddress);
		} else {
			Reporter.log("Not SNMP Notification checkbox");
		}

		verifyExists(APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown, "Delivery Channel");
		addDropdownValues_commonMethod("Delivery Channel", APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown,
				DeliveryChannel);

		if (RouterBasedFirewall.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterBasedFirewallcheckbox, "Router Based Firewall checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.RouterBasedFirewallcheckbox, "Router Based Firewall checkbox");
		} else {
			Reporter.log("Not Router Based Firewall checkbox");
		}
		if (Qos.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox, "Qos checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox, "Qos checkbox");
		} else {
			Reporter.log("Not Qos checkbox");
		}
		if (ExtendPErangetocustomerLAN.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.ExtendPErangetocustomerLANcheckbox,
					"Extend PE rangeto customer LAN checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.ExtendPErangetocustomerLANcheckbox,
					"Extend PE rangeto customer LAN checkbox");
		} else {
			Reporter.log("Not Extend PE rangeto customer LAN checkbox");
		}
		scrollDown(APTIPAccessNoCPE.APTNoCPE.Nextbutton_ServiceCreation);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.Nextbutton_ServiceCreation, "Nextbutton Service Creation");
		click(APTIPAccessNoCPE.APTNoCPE.Nextbutton_ServiceCreation, "Nextbutton Service Creation");

		String serviceCreated = "Service successfully created";
		String orderService = getTextFrom(APTIPAccessNoCPE.APTNoCPE.servicecreationmessage,"service creation message");
		serviceCreated.equalsIgnoreCase(orderService);
		Reporter.log("Service successfully created");

		// }
		/*
		 * else { Reporter.log("Servce Creation' page not navigated");
		 * 
		 * }
		 */
	}

	public void searchorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		waitforPagetobeenable();
		waitForAjax();
		String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink, " Manage Customer Service Link");
		mouseMoveOn(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.searchorderlink, "search order link");
		click(APTIPAccessNoCPE.APTNoCPE.searchorderlink, "search order link");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.servicefield, "service field");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.servicefield, sid);

		click(APTIPAccessNoCPE.APTNoCPE.searchbutton, "searchbutton");
		// click(searchbutton,"searchbutton");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceradiobutton, "service radio button");
		click(APTIPAccessNoCPE.APTNoCPE.serviceradiobutton, "service radio button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.searchorder_actiondropdown, "search order actiodropdown");
		click(APTIPAccessNoCPE.APTNoCPE.searchorder_actiondropdown, "search order linksearch order actiodropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.view, "view");
		click(APTIPAccessNoCPE.APTNoCPE.view, "view");

	}

	public void verifyCustomerDetailsInformation(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"newCustomerCreation");
		String existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomerSelection");
		String newCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
		String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MainDomain");

		if (newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Name_Value, "Name Value");
			String nameValue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Name_Value, "Name Value");
			nameValue.equalsIgnoreCase(newCustomer);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Country_Value, "Country");
			String countryValue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Country_Value, "Country");
			countryValue.equalsIgnoreCase(country);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.OCN_Value, "OCN");
			String OCN = getTextFrom(APTIPAccessNoCPE.APTNoCPE.OCN_Value, "OCN");
			OCN.equalsIgnoreCase(ocn);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Reference_Value, "Reference");
			String Reference = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Reference_Value, "Reference");
			Reference.equalsIgnoreCase(reference);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.TechnicalContactName_Value, "Technical Contact Name");
			String technicalContactName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.TechnicalContactName_Value, "Technical Contact Name");
			technicalContactName.equalsIgnoreCase(technicalcontactname);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Type_Value, "Type");
			String Type = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Type_Value, "Type");
			Type.equalsIgnoreCase(type);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Email_Value, "Email");
			String Email = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Email_Value, "Email");
			Email.equalsIgnoreCase(email);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Phone_Value, "Phone");
			String Phone = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Phone_Value, "Phone");
			Phone.equalsIgnoreCase(phone);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Fax_Value, "Fax");
			String Fax = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Fax_Value, "Fax");
			Fax.equalsIgnoreCase(fax);
		} else if (newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Name_Value, "Fax");
			String ExistingCustomer = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Name_Value, "Fax");
			ExistingCustomer.equalsIgnoreCase(existingCustomer);
		}
		// Main Domain
		if (maindomain.equalsIgnoreCase("Null")) {
			Reporter.log("A default displays for main domain field, if no provided while creating customer");

		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.MainDomain_Value, "Main Domain");
			String Maindomain = getTextFrom(APTIPAccessNoCPE.APTNoCPE.MainDomain_Value, "Main Domain");
			Maindomain.equalsIgnoreCase(maindomain);
		}
		Reporter.log("Customer Details panel fields Verified");
	}

	public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editOrderSelection");
		String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_OrderNumber");
		String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_VoicelineNumber");

		if (editOrderSelection.equalsIgnoreCase("no")) {
			Reporter.log("Edit Order is not performed");
		} else if (editOrderSelection.equalsIgnoreCase("Yes")) {
			Reporter.log("Performing Edit Order Functionality");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");
			click(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderheader, "Edit Order");

			String EditOrderNo = getTextFrom(APTIPAccessNoCPE.APTNoCPE.editorderno, "Edit Order no");
			click(APTIPAccessNoCPE.APTNoCPE.editorderno, "edit order no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editorderno);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderno, "Order Number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno, editorderno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, "edit voice line no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editvoicelineno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.cancelbutton, "cancel button");
			click(APTIPAccessNoCPE.APTNoCPE.cancelbutton, "cancel button");
			
			scrollDown(APTIPAccessNoCPE.APTNoCPE.orderactionbutton);
			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");
			click(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderheader, "Edit Order Header");
			String editOrderHeader = getTextFrom(APTIPAccessNoCPE.APTNoCPE.editorderheader, "Edit Order Header");
			String EditOrder = "Edit Order";
			editOrderHeader.equalsIgnoreCase(EditOrder);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderno, "edit order no");
			click(APTIPAccessNoCPE.APTNoCPE.editorderno, "edit order no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editorderno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno, editorderno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, "edit voice line no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editvoicelineno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorder_okbutton, "OK Button");
			click(APTIPAccessNoCPE.APTNoCPE.editorder_okbutton, "OK Button");

			if (editorderno.equalsIgnoreCase("Null")) {
				Reporter.log("Order/Contract Number (Parent SID) field is not edited");
			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ordernumbervalue, "order number value");
				String editOrderno = getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordernumbervalue, "order number value");
				editOrderno.equalsIgnoreCase(editorderno);
			}

			if (editvoicelineno.equalsIgnoreCase("Null")) {
				Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ordervoicelinenumbervalue, "order voice line number value");
				String editVoicelineno = getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordervoicelinenumbervalue, "order voice line number value");
				editVoicelineno.equalsIgnoreCase(editvoicelineno);
			}
			Reporter.log("Edit Order is successful");

		}

	}

	public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{

		String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"changeOrderSelection_newOrder");
		String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "changeOrderSelection_existingOrder");
		String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_newOrderNumber");
		String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_VoicelineNumber");
		String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_existingOrderNumber");

		if ((changeOrderSelection_newOrder.equalsIgnoreCase("No"))
				&& (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
			Reporter.log("Change Order is not performed");

		} else if (changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");
			click(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderheader, "change order header");
			String changeOrderHeader = getTextFrom(APTIPAccessNoCPE.APTNoCPE.changeorderheader, "change order header");
			String changeOrder = "Change Order";
			changeOrderHeader.equalsIgnoreCase(changeOrder);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_selectorderswitch, "select order switch");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_selectorderswitch, "select order switch");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeordernumber, "change order number");
			click(APTIPAccessNoCPE.APTNoCPE.changeordernumber, "change order number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno, ChangeOrder_newOrderNumber);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber, "change order voiceline number");
			click(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber, "change order voiceline number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber, changevoicelineno);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.createorder_button, "create order button");
			click(APTIPAccessNoCPE.APTNoCPE.createorder_button, "create order button");
			
			waitForAjax();
			waitforPagetobeenable();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");
			

			Reporter.log("Change Order is successful");
		} else if (changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) {
			Reporter.log("Performing Change Order functionality");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "order action button");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "order action button");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");
			click(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderheader, "change order");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_chooseorderdropdown,
					"Order/Contract Number (Parent SID)");
			select(APTIPAccessNoCPE.APTNoCPE.changeorder_chooseorderdropdown, ChangeOrder_existingOrderNumber);
			
			//waitForAjax();
			waitforPagetobeenable();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");

			// String
			// orderNumberValue=getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordernumbervalue);
			// orderNumberValue.equalsIgnoreCase(ChangeOrder_existingOrderNumber);

			// String
			// orderVoicelineNumberValue=getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordervoicelinenumbervalue);
			// orderVoicelineNumberValue.equalsIgnoreCase(changevoicelineno);

			Reporter.log("Change Order is successful");

		}

	}

	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String service = "Service";
		verifyExists(APTIPAccessNoCPE.APTNoCPE.servicepanel_header, "service panel");
		String servicePanelHeader = getTextFrom(APTIPAccessNoCPE.APTNoCPE.servicepanel_header, "service panel");
		servicePanelHeader.equalsIgnoreCase(service);

	}

	public void verifyServicePanelActions_NoCPE(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String BillingTypeEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BillingTypeEdit");
		String EmailServiceEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EmailServiceEdit");
		String PhoneServiceEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PhoneServiceEdit");
		String RemarksEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RemarksEdit");
		String ManageServiceEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ManageServiceEdit");
		String RouterConfigurationViewIPv4Edit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv4Edit");
		String RouterConfigurationViewIPv6Edit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigurationViewIPv6Edit");
		String PerformanceReportingEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PerformanceReportingEdit");
		String IPGuardianEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IPGuardianEdit");
		String SNMPNotificationEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"SNMPNotificationEdit");
		String DeliveryChannelEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DeliveryChannelEdit");
		String RouterBasedFirewallEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterBasedFirewallEdit");
		String TrapTargetAddressEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TrapTargetAddressEdit");
		String QosEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "QosEdit");
		String ExtendPErangetocustomerLANEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExtendPErangetocustomerLANEdit");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "Action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "Action dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.edit, "edit");
		click(APTIPAccessNoCPE.APTNoCPE.edit, "edit");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.cancelbutton, "cancel button");
		click(APTIPAccessNoCPE.APTNoCPE.cancelbutton, "cancel button");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.servicepanel_header)) {
			Reporter.log("Navigated to view service page");

		} else {
			Reporter.log(" Didn't navigate to view service page");
		}

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "Action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "Action dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");
		click(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.EditService_Header)) {
			Reporter.log("Edit Servce' page navigated as expected");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, "billin type dropdown");
			addDropdownValues_commonMethod("Billing Type", APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,
					BillingTypeEdit);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, "email text field Service");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, EmailServiceEdit, "email text field Service");

			

			if (SNMPNotificationEdit.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification checkbox");
				addCheckbox_commonMethod(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification",
						SNMPNotificationEdit);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.TrapTargetAddresstextfield, "Trap Target Address text field");
				clearTextBox(APTIPAccessNoCPE.APTNoCPE.TrapTargetAddresstextfield);
				sendKeys(APTIPAccessNoCPE.APTNoCPE.TrapTargetAddresstextfield, TrapTargetAddressEdit);
			} else {
				Reporter.log("SNMP Notification checkbox is not checked");

			}

			verifyExists(APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown, "Delivery Channel Dropdown");
			addDropdownValues_commonMethod("Delivery Channel", APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown,
					DeliveryChannelEdit);
			/*
			 * if(RouterBasedFirewallEdit.equalsIgnoreCase("Yes")) {
			 * verifyExists(APTIPAccessNoCPE.APTNoCPE.
			 * RouterBasedFirewallcheckbox,"Router Based Firewall checkbox");
			 * addCheckbox_commonMethod(APTIPAccessNoCPE.APTNoCPE.
			 * RouterBasedFirewallcheckbox,"Router Based Firewall"
			 * ,RouterBasedFirewallEdit); } else {
			 * Reporter.log("Router Based Firewall checkbox is not checked");
			 * 
			 * } if(QosEdit.equalsIgnoreCase("Yes")) {
			 * verifyExists(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox,"Qos checkbox"
			 * );
			 * addCheckbox_commonMethod(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox,
			 * "Qos",QosEdit); } else {
			 * Reporter.log("Qos checkbox is not checked");
			 * 
			 * } if(ExtendPErangetocustomerLANEdit.equalsIgnoreCase("Yes")) {
			 * verifyExists(APTIPAccessNoCPE.APTNoCPE.
			 * ExtendPErangetocustomerLANcheckbox,"Extend PE rangeto customer LAN checkbox"
			 * ); addCheckbox_commonMethod(APTIPAccessNoCPE.APTNoCPE.
			 * ExtendPErangetocustomerLANcheckbox,"Extend PE range to customer LAN"
			 * ,ExtendPErangetocustomerLANEdit); } else { Reporter.
			 * log("Extend PE range to customer LAN checkbox is not checked");
			 * 
			 * }
			 */
			verifyExists(APTIPAccessNoCPE.APTNoCPE.editservice_okbutton, "Ok Button");
			click(APTIPAccessNoCPE.APTNoCPE.editservice_okbutton, "OK Butoon");

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.customerdetailsheader)) {
				Reporter.log("Navigated to view service page");

			} else {
				Reporter.log("Service not updated");
			}
		} else {
			Reporter.log("Edit Service' page not navigated");
		}
		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.managesubnets_link, "manage subnet link");
		click(APTIPAccessNoCPE.APTNoCPE.managesubnets_link, "manage subnet link");

		waitForAjax();

		String spaceName = "Service";
		verifyExists(APTIPAccessNoCPE.APTNoCPE.spacename_column, "space name");
		String SpaceName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.spacename_column, "space name");
		SpaceName.equalsIgnoreCase(spaceName);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.spacename_column, "spacename");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.blockname_column, "block name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.subnetname_column, "subnet name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.startaddress_column, "start address");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.size_column, "size");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "close");
		click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "close");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.managesubnetsipv6_link, "manage subnets ipv6 link");
		click(APTIPAccessNoCPE.APTNoCPE.managesubnetsipv6_link, "manage subnets ipv6 link");

		waitForAjax();

		verifyExists(APTIPAccessNoCPE.APTNoCPE.spacename_column, "spacename");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.blockname_column, "block name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.subnetname_column, "subnet name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.startaddress_column, "start address");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.size_column, "size");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "close");
		click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "close");

		// dump

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.dump_link, "dump");
		click(APTIPAccessNoCPE.APTNoCPE.dump_link, "dump");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.service_header, "service");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "close");
		click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "close");
	}

	public void verifyManageService(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String Sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
		String serviceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		String servicestatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatus");
		String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ServiceStatusChangeRequired");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "service action dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.manageLink, "manage");
		click(APTIPAccessNoCPE.APTNoCPE.manageLink, "manage");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.manageservice_header, "manage service");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.status_servicename, "space name");
		String sid = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_servicename, "space name");
		sid.equalsIgnoreCase(Sid);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.status_servicetype, "space type");
		String servicetype = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_servicetype, "space Type");
		servicetype.equalsIgnoreCase(serviceType);

		String ServiceDetails_value = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_servicedetails,"status_service details");

		if (isEmptyOrNull(ServiceDetails_value)) {
			Reporter.log("Service Details column value is empty as expected");

		} else {
			Reporter.log("Service Details column value should be empty");
		}

		String serviceStatus = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_currentstatus,"status_current status");
		serviceStatus.equalsIgnoreCase(servicestatus);

		String LastModificationTime_value = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_modificationtime,"status_modification time");

		if (LastModificationTime_value.contains("BST")) {
			Reporter.log("Service status is displayed as");

		}

		else {
			Reporter.log("Incorrect modification time format");

		}
		verifyExists(APTIPAccessNoCPE.APTNoCPE.statuslink, "Status");
		click(APTIPAccessNoCPE.APTNoCPE.statuslink, "Status");

		if (servicestatuschangerequired == "Yes") {

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.Servicestatus_popup)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdown, "change status");
				click(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdown, "change status");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdownvalue, "change status");
				click(APTIPAccessNoCPE.APTNoCPE.changestatus_dropdownvalue, "change status");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");
				click(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.servicestatushistory)) {
					Reporter.log("Service status change request logged");
				} else {
					Reporter.log(" Service status change request is not logged");
				}
			} else {
				Reporter.log(" Status link is not working");

			}
		} else {
			Reporter.log("Service status change not reqired");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.servicestatus_popupclose, "service status");
			click(APTIPAccessNoCPE.APTNoCPE.servicestatus_popupclose, "service status");
		}

		String syncServicename = getTextFrom(APTIPAccessNoCPE.APTNoCPE.sync_servicename,"sync_service name");
		syncServicename.equalsIgnoreCase(sid);

		String syncServiceType = getTextFrom(APTIPAccessNoCPE.APTNoCPE.sync_servicetype,"sync_service type");
		syncServiceType.equalsIgnoreCase(serviceType);

		if (isEmptyOrNull(APTIPAccessNoCPE.APTNoCPE.sync_servicedetails)) {
			Reporter.log("Service Details column value is empty as expected");

		} else {
			Reporter.log("Service Details column value should be empty");

		}

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.managepage_backbutton,"manage
		// page back button");
		// click(APTIPAccessNoCPE.APTNoCPE.managepage_backbutton,"manage page
		// back button");
	}

	public void addExistingPEDevice_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingDeviceName");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.providerequipment_header, "provide requipment header");
		String providerequipment = getTextFrom(APTIPAccessNoCPE.APTNoCPE.providerequipment_header, "provide requipment header");
		String provideRequipment = "Provider Equipment (PE)";
		providerequipment.equalsIgnoreCase(provideRequipment);

		waitForAjax();
		verifyExists(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "addpe device");
		click(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "addpe device");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.addpedevice_header)) {
			Reporter.log("Add PE Device' page navigated as expected");
			waitToPageLoad();
			waitForAjax();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown, "typepe name dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown, "typepe name dropdown");
			// click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown,"typepename");
			waitToPageLoad();

			sendKeys(APTIPAccessNoCPE.APTNoCPE.typepename_dropdowninputfield, existingdevicename,
					"typepe name dropdown");
			webDriver.findElement(By.xpath("(//div[label[text()='Type PE name to filter']]//span[contains(text(),'"
					+ existingdevicename + "')])[1]")).click();
			addDropdownValues_commonMethod("Type PE name to filter", APTIPAccessNoCPE.APTNoCPE.typepename_dropdown1,
					existingdevicename);
			// click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown1, "typepe
			// name dropdown");
			webDriver.findElement(By.xpath("(//div[label[text()='Type PE name to filter']]//span[contains(text(),'"
					+ existingdevicename + "')])[1]")).click();

			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_vendormodelvalue1,
					"existing device vendor model value");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_managementaddressvalue1,
					"existing device management addressvalue");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_connectivityprotocol1,
					"existing device connectivi typrotocol");

			String SNMPVersionValue = APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpversion1;

			if (SNMPVersionValue.equalsIgnoreCase("2c")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpro1, "existing device snmpro");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmprw1, "existing device snmprw");
			}

			else if (SNMPVersionValue.equalsIgnoreCase("3")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3username1,
						"existing device snmp v3 username");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3authpassword1,
						"existing device snmp v3 auth password");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpv3privpassword1,
						"existingdevice snmp v3 priv password");

			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_snmpversion1, "existing device snmp version");

			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_country1, "Country");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_city1, "City");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_site1, "Site");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.existingdevice_premise1, "Premise");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next");
			click(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next");

			waitForAjax();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.successmsg, "Add Device success msg");
			String successMsg = getTextFrom(APTIPAccessNoCPE.APTNoCPE.successmsg, "Add Device success msg");
			String SuccessMsg = "Device is added to Service.";

			successMsg.equalsIgnoreCase(SuccessMsg);

		} else {
			Reporter.log("Add PE Device' page not navigated");

		}

	}

	public void verifyExistingDevice_ViewDevicedetails_PE(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		Reporter.log("Verifying Existing PE Device Information");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
			Reporter.log("View PE Device' page navigated as expected");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename, "Name");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel, "Vendor/Model");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Connectiviy Protocol");

			String SNMPVersionValue = APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion;

			if (SNMPVersionValue.equalsIgnoreCase("2c")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro, "view page snmpro");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw, "view page snmprw");

			}

			else if (SNMPVersionValue.equalsIgnoreCase("3")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3username, "Snmp v3 Username");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3authpassword, "Snmp v3 Auth password");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3privpassword, "Snmp v3 Priv password");

			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_city, "City");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");

			// verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,"Back");
			// click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,"Back");
		} else {
			Reporter.log("View PE Device' page not navigated");
		}
	}

	public void testStatus_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		waitforPagetobeenable();
		waitForAjax();
		waitForAjax();
		waitForAjax();
		String element = null;
		String status = null;
		int listSize = getXPathCount(APTIPAccessNoCPE.APTNoCPE.tableRow);

		if (listSize >= 1) {

			for (int i = 1; i <= listSize; i++) {
				element = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[1]")).getText();

				if (element.isEmpty()) {

				} else {
					Report.LogInfo("TestStatus", "Test Name is displaying as: " + element, "PASS");
					status = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[2]/div")).getAttribute("class");
					if (status.contains("red")) {
						Report.LogInfo("TestStatus", "status colour dipslays as: red", "PASS");

					} else if (status.contains("green")) {
						Report.LogInfo("TestStatus", "status colour dipslays as: green", "PASS");

					}
				}
			}
		} else {
			Report.LogInfo("TestStatus",
					"Command/Status table is not displaying in view device page. Instead Message displays as: ",
					"PASS");
			Report.LogInfo("TestStatus", "* Device reachability tests require the device to have an IP address.",
					"PASS");

		}

	}

	public void deleteDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		waitForAjax();
		if (isVisible(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			String DeviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"ExistingDeviceName");
			List<WebElement> webElements = findWebElements(APTIPAccessNoCPE.APTNoCPE.addeddevicesList);
			// String
			// addeddevicesList=findWebElements(APTIPAccessNoCPE.APTNoCPE.addeddevicesList);
			int addeddevicesLists = getXPathCount(APTIPAccessNoCPE.APTNoCPE.addeddevicesList1);

			for (int i = 0; i < addeddevicesLists; i++) {

				String AddedDeviceNameText = webElements.get(i).getText();
				String AddedDevice_SNo = AddedDeviceNameText.substring(0, 1);
				if (AddedDeviceNameText.contains(DeviceName)) {
					String AddedDevice_DeletefromserviceLink = APTIPAccessNoCPE.APTNoCPE.deleteDevice1 + AddedDevice_SNo
							+ APTIPAccessNoCPE.APTNoCPE.deleteDevice2;
					click(AddedDevice_DeletefromserviceLink,"delete device");

					Reporter.log("Device successfully removed from service.");
				} else {
					Reporter.log("Invalid device name");
				}
			}
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	public void navigateToAddNewDevicepage_PE() throws InterruptedException, IOException

	{
		String ProviderEquipment = "Provider Equipment (PE)";
		scrollDown(APTIPAccessNoCPE.APTNoCPE.providerequipment_header);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.providerequipment_header, "provide requipment");
		String PE = getTextFrom(APTIPAccessNoCPE.APTNoCPE.providerequipment_header, "provide requipment");
		PE.equalsIgnoreCase(ProviderEquipment);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "Add PE Device");
		click(APTIPAccessNoCPE.APTNoCPE.addpedevice_link, "Add PE Device");

		String AddPEDevice = "Add PE Device";
		verifyExists(APTIPAccessNoCPE.APTNoCPE.addpedevice_header, "provide requipment");
		String addpeDevice = getTextFrom(APTIPAccessNoCPE.APTNoCPE.addpedevice_header, "provide requipment");
		addpeDevice.equalsIgnoreCase(AddPEDevice);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.addnewdevice_togglebutton, "Add New Device toggle");
		click(APTIPAccessNoCPE.APTNoCPE.addnewdevice_togglebutton, "Add New Device toggle");

	}

	public void verifyadddevicefields_PE() throws InterruptedException, IOException {
		verifyExists(APTIPAccessNoCPE.APTNoCPE.nametextfield, "Name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.vendormodelinput, "Vendor Model");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.managementaddresstextbox, "Management Address");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.snmpv3username, "Snmp v3 Username");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.snmpv3authpassword, "Snmp v3 Auth password");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.snmpv3privpassword, "Snmp v3 Priv password");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.countryinput, "Country");
		scrollDown(APTIPAccessNoCPE.APTNoCPE.citydropdowninput);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.citydropdowninput, "City");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.sitedropdowninput, "Site");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.premisedropdowninput, "Premise");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.addcityswitch, "Add City");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.addsiteswitch, "Add Site");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.addpremiseswitch, "Add Premise");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.cancelbutton, "Cancel");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next");
	}

	public void addNewPEDevice_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ManagementAddress");
		String telnet = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SSH");
		String snmp2c = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Snmp2C");
		String snmp3 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Snmp3");
		String snmpro2cvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SnmProNewValue");
		String Snmpv3Usernamevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Snmpv3UsernameNewValue");
		String Snmpv3Authpasswordvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Snmpv3AuthpasswordNewValue");
		String Snmpv3Privpasswordvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Snmpv3PrivpasswordNewValue");
		String Country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Country");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityName");
		String Citycode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityCode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteName");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteCode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseName");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseCode");
		String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Existing SiteValue");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Existing PremiseValue");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Nex Button");
		click(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next Button");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.warningMessage_name, "Device Name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.warningMessage_vendor, "Vendor/Model");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nametextfield, "Name");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.nametextfield, name, "Name");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.vendormodelinput, "Vendor/Model");
		selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.vendormodelinput, vendormodel, "Vendor/Model");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.managementaddresstextbox, "Management Address");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.managementaddresstextbox, managementaddress, "Management Address");

		if ((telnet.equalsIgnoreCase("No")) && (ssh.equalsIgnoreCase("No"))) {
			// verifyDefaultSelection_connectivityprotocol_ssh(application);

			// verifyDefaultSelection_connectivityprotocol_telnet(application);
		} else {
			// verifyDefaultSelection_connectivityprotocol_ssh(application);

			// verifyDefaultSelection_connectivityprotocol_telnet(application);

			if ((telnet.equalsIgnoreCase("Yes")) && (ssh.equalsIgnoreCase("No"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.telnetradiobutton, "Telnet");
				click(APTIPAccessNoCPE.APTNoCPE.telnetradiobutton, telnet);
			} else if ((telnet.equalsIgnoreCase("No")) && (ssh.equalsIgnoreCase("Yes"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.sshradiobutton, "SSH");
				click(APTIPAccessNoCPE.APTNoCPE.sshradiobutton, ssh);
			}
		}

		if ((snmp2c.equalsIgnoreCase("Yes")) && (snmp3.equalsIgnoreCase("NO"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.c2cradiobutton, "SNMP Version-2c");
			click(APTIPAccessNoCPE.APTNoCPE.c2cradiobutton, snmp2c);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.snmprotextfield, "Snmpro");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.snmprotextfield, snmpro2cvalue);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.snmprwtextfield, "Snmprw");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.snmprwtextfield, snmpro2cvalue);

		} else if ((snmp2c.equalsIgnoreCase("no")) && (snmp3.equalsIgnoreCase("yes"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.c3radiobutton, "SNMP Version-3");
			click(APTIPAccessNoCPE.APTNoCPE.c3radiobutton, snmp3);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.snmpv3username, "Snmpro");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.snmpv3username, Snmpv3Usernamevalue);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.snmpv3authpassword, "Snmp v3 Auth password");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.snmpv3authpassword, Snmpv3Authpasswordvalue);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.snmpv3privpassword, "Snmp v3 Auth password");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.snmpv3privpassword, Snmpv3Privpasswordvalue);
		}
		verifyExists(APTIPAccessNoCPE.APTNoCPE.SnmprwLabelAddPEDevicePage, "Snprw Label");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.countryinput, "Snmp v3 Auth password");
		selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.countryinput, Country, "Country");

		if (existingcity.equalsIgnoreCase("no") & newcity.equalsIgnoreCase("yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.addcityswitch, "city switch");
			click(APTIPAccessNoCPE.APTNoCPE.addcityswitch, "city switch");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.citynameinputfield, "City Name");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.citynameinputfield, cityname, "City Name");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.citycodeinputfield, "City Code");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.citycodeinputfield, Citycode, "City Code");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addCityToggleSelected, "Site Name");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addCityToggleSelected, sitename, "Site Name");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addCityToggleSelected, "Site Code");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addCityToggleSelected, sitecode, "Site Code");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addCityToggleSelected, "Premise Name");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addCityToggleSelected, premisename,
					"Premise Name");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addCityToggleSelected, "Premise Code");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addCityToggleSelected, premisecode,
					"Premise Code");
		} else if (existingcity.equalsIgnoreCase("yes") & newcity.equalsIgnoreCase("no")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.citydropdowninput, "City");
			selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.citydropdowninput, existingcityvalue, "City");

			if (existingsite.equalsIgnoreCase("yes") & newsite.equalsIgnoreCase("no")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.sitedropdowninput, "City");
				selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.sitedropdowninput, existingsitevalue, "Site");

				if (existingpremise.equalsIgnoreCase("yes") & NewPremise.equalsIgnoreCase("no")) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.premisedropdowninput, "Premise");
					selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.premisedropdowninput, existingpremisevalue,
							"Premise");
				}

				else if (existingpremise.equalsIgnoreCase("no") & NewPremise.equalsIgnoreCase("yes")) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.addpremiseswitch, "premise switch");
					click(APTIPAccessNoCPE.APTNoCPE.addpremiseswitch, "premise switch");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addPremiseToggleSelected,
							"Premise name");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addPremiseToggleSelected, premisename,
							"Premise name");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addPremiseToggleSelected,
							"Premise Code");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addPremiseToggleSelected, premisecode,
							"Premise Code");
				}
			}

			else if (existingsite.equalsIgnoreCase("no") & newsite.equalsIgnoreCase("yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.addsiteswitch, "site switch");
				click(APTIPAccessNoCPE.APTNoCPE.addsiteswitch, "site switch");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addSiteToggleSelected, "Premise name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addSiteToggleSelected, sitename, "Site Name");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addSiteToggleSelected, "Site Code");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addSiteToggleSelected, sitecode, "Site Code");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addSiteToggleSelected, "Premise name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addSiteToggleSelected, premisename,
						"Premise name");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addSiteToggleSelected, "Premise Code");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addSiteToggleSelected, premisecode,
						"Premise Code");
			}
		}
		verifyExists(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next Button");
		click(APTIPAccessNoCPE.APTNoCPE.Next_Button, "Next Button");

		waitForAjax();
		waitForAjax();
		waitToPageLoad();
		scrollDown(APTIPAccessNoCPE.APTNoCPE.PE_AddPEDeviceSuccessfulMessage);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_AddPEDeviceSuccessfulMessage, "Add Device success msg");
		click(APTIPAccessNoCPE.APTNoCPE.PE_AddPEDeviceSuccessfulMessage, "Add Device success msg");

		waitForAjax();

	}

	public void verifyViewpage_Devicedetails_PE(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		// waitForAjax();
		waitforPagetobeenable();
		waitForAjax();

		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ManagementAddress");
		String telnet = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SSH");
		String snmp2c = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Snmp2C");
		String snmp3 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Snmp3");
		String Country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Country");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityName");
		String sitename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteName");
		String premisename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseName");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Existing SiteValue");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Existing PremiseValue");

		waitForAjax();

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
			Reporter.log("View PE Device' page navigated as expected");

			String Name = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename,"viewpage_devicename");
			Name.equalsIgnoreCase(name);

			String vendorModel = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel,"viewpage_vendormodel");
			vendorModel.equalsIgnoreCase(vendormodel);

			if ((telnet.equalsIgnoreCase("Yes")) && (ssh.equalsIgnoreCase("No"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Telnet");
				String Telnet = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol,"viewpage_connectivity protocol");
				String talent = "telnet";
				Telnet.equalsIgnoreCase(talent);
			} else if ((telnet.equalsIgnoreCase("no")) && (ssh.equalsIgnoreCase("Yes"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "ssh");
				String sshh = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol,"viewpage_connectivity protocol");
				String SSH = "ssh";
				sshh.equalsIgnoreCase(SSH);
			}
			if ((snmp2c.equalsIgnoreCase("Yes")) && (snmp3.equalsIgnoreCase("No"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");
				String SNMPVersion = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion,"viewpage_snmp version");
				String SNMPversion = "2c";
				SNMPVersion.equalsIgnoreCase(SNMPversion);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro, "Snmpro");
				String Incc = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro,"viewpage_snmpro");
				String incc = "incc";
				Incc.equalsIgnoreCase(incc);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw, "Snmprw");
				String Snmprw = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw,"viewpage_snmprw");
				String snmprw = "ip4corp3";
				Snmprw.equalsIgnoreCase(snmprw);
			}

			else if ((snmp2c.equalsIgnoreCase("No")) && (snmp3.equalsIgnoreCase("Yes"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3username, "Snmp v3 Username");
				String coltnms = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3username,"viewpage_snmpv3username");
				String coltNms = "colt-nms";
				coltnms.equalsIgnoreCase(coltNms);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3authpassword, "Snmp V3 Auth Password");
				String V3Password = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3authpassword,"viewpage_snmpv3authpassword");
				String v3Password = "OrHzjWmRvr4piJZb";
				V3Password.equalsIgnoreCase(v3Password);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3privpassword, "Snmp V3 Priv Password");
				String V3PrivPassword = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3privpassword,"Snmp V3 Priv Password");
				String v3PrivPassword = "3k0hw8thNxHucQkE";
				V3PrivPassword.equalsIgnoreCase(v3PrivPassword);
			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			String managementAddress = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress,"Management Address");
			managementAddress.equalsIgnoreCase(managementaddress);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			String country = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			country.equalsIgnoreCase(Country);

			if ((existingcity.equalsIgnoreCase("yes")) && (newcity.equalsIgnoreCase("NO"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_city, "City");

			} else if ((existingcity.equalsIgnoreCase("NO")) && (newcity.equalsIgnoreCase("Yes"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_city, "City");
				String cityName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_city, "City");
				cityName.equalsIgnoreCase(cityname);
			}

			if ((existingsite.equalsIgnoreCase("yes")) && (newsite.equalsIgnoreCase("NO"))) {

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
				String existingSitevalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
				existingSitevalue.equalsIgnoreCase(existingsitevalue);
			} else if ((existingsite.equalsIgnoreCase("No")) && (newsite.equalsIgnoreCase("Yes"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
				String siteName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
				siteName.equalsIgnoreCase(sitename);
			}

			if ((existingpremise.equalsIgnoreCase("yes")) && (NewPremise.equalsIgnoreCase("NO"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
				String existingPremisevalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
				existingPremisevalue.equalsIgnoreCase(existingpremisevalue);
			} else if ((existingpremise.equalsIgnoreCase("No")) && (NewPremise.equalsIgnoreCase("Yes"))) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
				String premiseName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
				premiseName.equalsIgnoreCase(premisename);
			}
		} else {
			Reporter.log("View PE Device' page not navigated");
		}
	}

	public void verifyEditDevice_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");

		String editDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editVendorModel");
		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editManagementAddress");

		String editCountry = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCountry");
		String editExistingCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingCity");
		String editNewCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewCity");
		String editNewCityName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewCityName");
		String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewCityCode");
		String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewSiteName");
		String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewSiteCode");
		String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewPremiseName");
		String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewPremiseCode");
		String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingCityValue");
		String editExistingSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingSite");
		String editNewSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewSite");
		String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingSiteValue");
		String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingPremise");
		String editNewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewPremise");
		String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingPremiseValue");

		Reporter.log("Verifying Edit PE Device Functionality");

		scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			waitForAjax();
			// verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_editdevicelink,
			// "Edit");
			// click(APTIPAccessNoCPE.APTNoCPE.viewservicepage_editdevicelink,"Edit");
			webDriver.findElement(By.xpath("//td[contains(.,'" + name + "')]//span//a[text()='Edit']")).click();
			// td[contains(.,'"+name+"')]//span//a[text()='Edit']
			waitForAjax();

			/*verifyExists(APTIPAccessNoCPE.APTNoCPE.editdeviceheader, "City");
			String editPEDevice = getTextFrom(APTIPAccessNoCPE.APTNoCPE.editdeviceheader);
			String EditPEDevice = "Edit PE Device";
			editPEDevice.equalsIgnoreCase(EditPEDevice);
			*/
			
			verifyExists(APTIPAccessNoCPE.APTNoCPE.nametextfield, "Name");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.nametextfield, editDevicename, "Name");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.vendormodelinput, "Vendor/Model");
			select(APTIPAccessNoCPE.APTNoCPE.vendormodelinput, editVendorModel);

			// verifyExists(APTIPAccessNoCPE.APTNoCPE.managementaddresstextbox,"Name");
			// sendKeys(APTIPAccessNoCPE.APTNoCPE.managementaddresstextbox,editManagementAddress,"Name");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.countryinput, "Vendor/Model");
			select(APTIPAccessNoCPE.APTNoCPE.countryinput, editCountry);

			if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.addcityswitch, "add city switch");
				click(APTIPAccessNoCPE.APTNoCPE.addcityswitch, "add city switch");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.citynameinputfield, "Name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.citynameinputfield, editNewCityName, "Name");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.citycodeinputfield, "CityCode");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.citycodeinputfield, editNewCityCode, "CityCode");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addCityToggleSelected, "Site Name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addCityToggleSelected, editNewSiteName,
						"Site Name");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addCityToggleSelected, "SiteCode");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addCityToggleSelected, editNewSiteCode,
						"SiteCode");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addCityToggleSelected, "Premise Name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addCityToggleSelected, editNewPremiseName,
						"Premise Name");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addCityToggleSelected, "Premise Name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode,
						"Premise Code");
			}

			else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.citydropdowninput, "City");
				select(APTIPAccessNoCPE.APTNoCPE.citydropdowninput, editExistingCityValue);

				if (editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.sitedropdowninput, "Site");
					select(APTIPAccessNoCPE.APTNoCPE.sitedropdowninput, editExistingSiteValue);

					if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.premisedropdowninput, "City");
						select(APTIPAccessNoCPE.APTNoCPE.premisedropdowninput, editExistingPremiseValue);
					}

					else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.addpremiseswitch, "add premise switch");
						click(APTIPAccessNoCPE.APTNoCPE.addpremiseswitch, "add premise switch");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addPremiseToggleSelected,
								"Premise Name");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addPremiseToggleSelected,
								editNewPremiseName, "Premise Name");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addPremiseToggleSelected,
								"Premise Code");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addPremiseToggleSelected,
								editNewPremiseCode, "Premise Code");
					}
				}

				else if (editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.addsiteswitch, "site switch");
					click(APTIPAccessNoCPE.APTNoCPE.addsiteswitch, "site switch");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addCityToggleSelected, "Site Name");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.sitenameinputfield_addCityToggleSelected, editNewSiteName,
							"Site Name");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addCityToggleSelected, "SiteCode");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.sitecodeinputfield_addCityToggleSelected, editNewSiteCode,
							"SiteCode");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addCityToggleSelected, "Premise Name");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.premisenameinputfield_addCityToggleSelected, editNewPremiseName,
							"Premise Name");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addCityToggleSelected, "Premise Name");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode,
							"Premise Code");
				}

			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "Ok Button");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "Ok Button");

		}

		else if (editCountry.equalsIgnoreCase("Null")) {
			Reporter.log("No changes made for 'Country' dropdown");

			CreateFirewall.editCity(editExistingCity, editNewCity, "citydropdowninput", "selectcityswitch",
					"addcityswitch", editExistingCityValue, editNewCityName, editNewCityCode, "City");

			// Site
			CreateFirewall.editSite(editExistingSite, editNewSite, "sitedropdowninput", "selectsiteswitch",
					"addsiteswitch", editExistingSiteValue, editNewSiteName, editNewSiteCode, "Site");

			// Premise
			CreateFirewall.editPremise(editExistingPremise, editNewPremise, "premisedropdowninput",
					"selectpremiseswitch", "addpremiseswitch", editExistingPremiseValue, editNewPremiseName,
					editNewPremiseCode, "Premise");
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	public void navigateToViewDevicePage(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String editDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");

		if (isVisible(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2)) {
			scrollDown(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2);
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2, "View Link");
			webDriver.findElement(By.xpath("//td[contains(.,'" + editDevicename + "')]//span//a[text()='View']"))
					.click();

			// click(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2,"View
			// Link");

			waitforPagetobeenable();
			waitForAjax();
			// waitForAjax();

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
				Report.LogInfo("INFO", "View PE Device' page navigated as expected", "PASS");
				Reporter.log("View PE Device' page navigated as expected");
			} else {
				Report.LogInfo("INFO", "View PE Device' page not navigated", "PASS");
				Reporter.log("View PE Device' page not navigated");
			}
		} else {
			Report.LogInfo("INFO", "No Device added in grid", "PASS");
			Reporter.log("No Device added in grid");
		}

	}

	public void verifyViewpage_UpdatedDevicedetails_PE(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ManagementAddress");
		String telnet = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SSH");
		String snmp2c = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Snmp2C");
		String snmp3 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Snmp3");
		String Country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Country");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityName");
		String sitename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteName");
		String premisename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseName");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Existing SiteValue");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Existing PremiseValue");
		String editDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editVendorModel");
		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editManagementAddress");

		String editCountry = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCountry");

		String editExistingSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingSite");
		String editNewSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewSite");
		String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingSiteValue");
		String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingPremise");
		String editNewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewPremise");
		String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editExistingPremiseValue");
		String editSSH = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editSSH");
		String editTelnet = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editTelnet");
		String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editSnmp2C");
		String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editSnmp3");
		String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewPremiseName");
		String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editNewSiteName");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
			Reporter.log("View PE Device' page navigated as expected");

		}

		if (editDevicename.equalsIgnoreCase("Null")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename, "Name");
			String Name = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename, "Name");
			Name.equalsIgnoreCase(name);
		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename, "Name");
			String Name = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_devicename, "Name");
			Name.equalsIgnoreCase(editDevicename);
		}

		if (editVendorModel.equalsIgnoreCase("Null")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel, "Vendor model");
			String Vendormodel = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel, "Vendor model");
			Vendormodel.equalsIgnoreCase(vendormodel);
		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel, "Vendor model");
			String EditVendorModel = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_vendormodel, "Vendor model");
			EditVendorModel.equalsIgnoreCase(editVendorModel);
		}
		if ((editSSH.equalsIgnoreCase("null")) || (editSSH.equalsIgnoreCase("no"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Connectivity Protocol");

		}

		else if ((editTelnet.equalsIgnoreCase("null")) || (editTelnet.equalsIgnoreCase("no"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Connectivity Protocol");

		} else if (editSSH.equalsIgnoreCase("yes") && editTelnet.equalsIgnoreCase("no")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Connectivity Protocol");

		} else if (editSSH.equalsIgnoreCase("no") && editTelnet.equalsIgnoreCase("yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_connectivityprotocol, "Connectivity Protocol");

		}
		if (editManagementAddress.equalsIgnoreCase("Null")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			String managementAddress = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			managementAddress.equalsIgnoreCase(managementaddress);
		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			String EditManagementAddress = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_managementaddress, "Management Address");
			EditManagementAddress.equalsIgnoreCase(editManagementAddress);
		}

		if (editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("No")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro, "Management Address");
			String Incc = "incc";
			String INcc = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro,"viewpage_snmpro");
			INcc.equalsIgnoreCase(Incc);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw, "Snmprw");
			String ip4corp3 = "ip4corp3";
			String Ip4corp3 = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw,"viewpage_snmprw");
			Ip4corp3.equalsIgnoreCase(ip4corp3);

		} else if (editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("null")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro, "Management Address");
			String Incc = "incc";
			String INcc = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpro,"viewpage_snmpro");
			INcc.equalsIgnoreCase(Incc);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw, "Snmprw");
			String ip4corp3 = "ip4corp3";
			String Ip4corp3 = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw,"viewpage_snmprw");
			Ip4corp3.equalsIgnoreCase(ip4corp3);
		}

		else if (editSnmp2C.equalsIgnoreCase("no") && editSnmp3.equalsIgnoreCase("yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");
		}

		else if (editSnmp2C.equalsIgnoreCase("null") && editSnmp3.equalsIgnoreCase("yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3username, "Snmp v3 Username");
			String coltnms = "colt-nms";
			String coltNms = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmprw, "Snmp v3 Username");
			coltNms.equalsIgnoreCase(coltnms);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3authpassword, "Snmp v3 Auth password");
			String snmpv3Authpassword = "OrHzjWmRvr4piJZb";
			String Snmpv3Authpassword = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3authpassword, "Snmp v3 Auth password");
			Snmpv3Authpassword.equalsIgnoreCase(snmpv3Authpassword);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3privpassword, "Snmp v3 Priv password");
			String Snmpv3password = "3k0hw8thNxHucQkE";
			String snmpv3password = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpv3privpassword, "Snmp v3 Priv password");
			snmpv3password.equalsIgnoreCase(Snmpv3password);
		} else if ((editSnmp2C.equalsIgnoreCase("null")) || (editSnmp2C.equalsIgnoreCase("no"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

		} else if ((editSnmp3.equalsIgnoreCase("null")) || (editSnmp3.equalsIgnoreCase("no"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_snmpversion, "SNMP Version");

		}
		if (editCountry.equalsIgnoreCase("Null")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			String country = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			country.equalsIgnoreCase(Country);
		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			String EditCountry = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_country, "Country");
			EditCountry.equalsIgnoreCase(editCountry);
		}
		if ((editExistingSite.equalsIgnoreCase("Null") && editNewSite.equalsIgnoreCase("Null"))
				|| (editExistingSite.equalsIgnoreCase("no") && editNewSite.equalsIgnoreCase("no"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			String Newsite = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			Newsite.equalsIgnoreCase(newsite);
		} else if (editExistingSite.equalsIgnoreCase("yes") && editNewSite.equalsIgnoreCase("no")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			String EditExistingSiteValue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			EditExistingSiteValue.equalsIgnoreCase(editExistingSiteValue);
		} else if (editExistingSite.equalsIgnoreCase("no") && editNewSite.equalsIgnoreCase("yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			String EditNewSiteName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_site, "Site");
			EditNewSiteName.equalsIgnoreCase(editNewSiteName);
		}

		if ((editExistingPremise.equalsIgnoreCase("Null") && editNewPremise.equalsIgnoreCase("Null"))
				|| (editExistingPremise.equalsIgnoreCase("no") && editNewPremise.equalsIgnoreCase("no"))) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
			String Newpremise = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
			Newpremise.equalsIgnoreCase(NewPremise);
		} else if (editExistingPremise.equalsIgnoreCase("yes") && editNewPremise.equalsIgnoreCase("no")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
			String EditExistingPremiseValue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
			EditExistingPremiseValue.equalsIgnoreCase(editExistingPremiseValue);
		} else if (editExistingPremise.equalsIgnoreCase("no") && editNewPremise.equalsIgnoreCase("yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
			String EditNewPremiseName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.viewpage_premise, "Premise");
			EditNewPremiseName.equalsIgnoreCase(editNewPremiseName);
		} else {
			Reporter.log("View PE Device' page not navigated");

		}
	}

	public void routerPanel_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String commandIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_CommandIPV4");
		String commandIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_CommandIPV6");
		scrollDown(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4Command_Executebutton);

		waitForAjax();
		waitForAjax();
		Reporter.log("Verifying Router Tool Functionality");

		String vendor = getTextFrom(APTIPAccessNoCPE.APTNoCPE.PE_View_VendorModelValue,"PE_View_Vendor Model Value");
		String ipAddress = getTextFrom(APTIPAccessNoCPE.APTNoCPE.PE_View_ManagementAddressValue,"PE_View_Management Address Value");

		if (vendor.startsWith("Cisco")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4CommandsDropdown, "Site Name");
			addDropdownValues_commonMethod("Command IPV4", APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4CommandsDropdown,
					commandIPv4);

			hostnametextField_IPV4_PE(testDataFile, dataSheet, scriptNo, dataSetNo, ipAddress);

			vrfNametextField_IPV4_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			executeCommandAndFetchTheValue_PE();

			verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV6CommandsDropdown, "Command IPV6");
			addDropdownValues_commonMethod("Command IPV6", APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV6CommandsDropdown,
					commandIPv6);

			hostnametextField_IPV6_PE(testDataFile, dataSheet, scriptNo, dataSetNo, ipAddress);

			vrfNametextField_IPV6_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			executeCommandAndFetchTheValue_PE6();
		}

		else if (vendor.startsWith("Juniper ")) {
			// ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4Command_Executebutton);
			scrollDown(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4Command_Executebutton);
			verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4Command_Executebutton, "Command IPV4");
			addDropdownValues_commonMethod("Command IPV4", APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4CommandsDropdown,
					commandIPv4);

			hostnametextField_IPV4_PE(testDataFile, dataSheet, scriptNo, dataSetNo, ipAddress);

			vrfNametextField_IPV4_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			executeCommandAndFetchTheValue_PE();
		}

		else {
			Reporter.log("Router Panel will not display for the selected vendorModel");
		}

	}

	public void addRouteFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String PE_RouteCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String PE_RouteSubnetSize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PE_RouteSubnetSize");
		String PE_Gateway = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_Gateway");
		String PE_NetworkAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PE_NetworkAddress");
		String PE_NetworkMAS = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_NetworkMAS");
		String PE_Metrics = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_Metrics");

		Reporter.log("Verifying Add Route Functionality");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.Routes_Header)) {
			scrollDown(APTIPAccessNoCPE.APTNoCPE.Routes_Actionbutton);
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Actionbutton, "Action");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_Actionbutton, "Action");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_AddLink, "Add Link");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_AddLink, "Add Link");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_AddRoutes_Header, "Routes_Add Routes_Header");
			String RoutesAddRoutesHeader = "Add Route";
			String RoutesAddRoutesheader = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Routes_AddRoutes_Header, "Routes_Add Routes_Header");
			RoutesAddRoutesheader.equalsIgnoreCase(RoutesAddRoutesHeader);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocationLink, "Routes EIP Allocation Link");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocationLink, "Routes EIP Allocation Link");

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.Routes_EIPSubnetAllocationforService_Header)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.CPE_Routes_EIPAllocation_SubnetTypeValue, "Subnet Type");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_AllocateSubnetButon, "Allocate Subnet Button");
				click(APTIPAccessNoCPE.APTNoCPE.Routes_AllocateSubnetButon, "Allocate Subnet Button");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CityWarningMessage,
						"City  warning message");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeWarningMessage,
						"Subnet Size  warning message");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_AvilablePoolWarningMessage,
						"Avilable Pool  warning message");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CityDropdown, "City");
				click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CityDropdown, "City");

				// sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CityDropdown,PE_RouteCity,"City");
				// webDriver.findElement(By.xpath("(//div[label[text()='Type PE
				// name to
				// filter']]//span[contains(text(),'"+PE_RouteCity+"')])[1]")).click();
				addDropdownValues_commonMethod("City", APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CityDropdown,
						PE_RouteCity);
				click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CityDropdown, "typepe name dropdown");
				webDriver
						.findElement(By.xpath(
								"(//div[label[text()='City']]//span[contains(text(),'" + PE_RouteCity + "')])[1]"))
						.click();

				// addDropdownValues_commonMethod("City",APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CityDropdown,PE_RouteCity);
				// click(APTIPAccessNoCPE.APTNoCPE.typepename_dropdown,"typepe
				// name dropdown");
				// webDriver.findElement(By.xpath("(//div[label[text()='Type PE
				// name to
				// filter']]//span[contains(text(),'"+PE_RouteCity+"')])[1]")).click();

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, "Sub Net Sizel");
				click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, "Sub Net Size");

				// sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown,PE_RouteSubnetSize,"Sub
				// Net Size");
				webDriver.findElement(By.xpath("(//div[label[text()='Sub Net Size']]//span[contains(text(),'"+ PE_RouteSubnetSize +"')])[1]")).click();
				addDropdownValues_commonMethod("Sub Net Size",
						APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, PE_RouteSubnetSize);
				click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, "Sub Net Size");
				webDriver.findElement(By.xpath("(//div[label[text()='Sub Net Size']]//span[contains(text(),'"+ PE_RouteSubnetSize +"')])[1]")).click();

				// addDropdownValues_commonMethod("Sub Net
				// Size",APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown,PE_RouteSubnetSize);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_AvailablePoolsMessage,
						"Available Pool Message");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CloseButton, "Close EIP Allocation window");
				click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_CloseButton, "Close EIP Allocation window");
			} else {
				Reporter.log("EIP Subnet Allocation for Service popup is not displaying");

			}
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "OK Button");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "OK Button");

			waitForAjax();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Gatway_WarningMessage, "Getway");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkAddress_WarningMessage, "Network Address");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkMASK_WarningMessage, "Network MAS");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Gatewaytextfield, "Getway");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_Gatewaytextfield, PE_Gateway, "Getway");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkAddresstextfield, "Network Address");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkAddresstextfield, PE_NetworkAddress, "Network Address");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkMAStextfield, "Network MAS");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkMAStextfield, PE_NetworkMAS, "Network MAS");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Metricstextfield, "Metrics");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_Metricstextfield, PE_Metrics, "Metrics");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "OK Button");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "OK Button");

		} else {
			Reporter.log("Routes panel header is not displaying");
		}
	}

	public void editRouteFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String PE_RouteSubnetSizeEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PE_RouteSubnetSizeEdit");
		String PE_GatewayEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_GatewayEdit");
		String PE_NetworkAddressEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PE_NetworkAddressEdit");
		String PE_NetworkMASEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PE_NetworkMASEdit");
		String PE_MetricsEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_MetricsEdit");

		Reporter.log("Verifying Edit Route Functionality");
		waitToPageLoad();
		waitForAjax();
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.Routes_Header)) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_SelectRouteCheckbox, "Select Route Checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_SelectRouteCheckbox, "Select Route Checkbox");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Actionbutton, "Action");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_Actionbutton, "Action");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EditLink, "Edit Link");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_EditLink, "Edit Link");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EditRoutes_Header, "Edit Route header");
			String EditRoute = "Edit Route";
			String editRoute = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Routes_EditRoutes_Header,"Routes_Edit Routes_Header");
			editRoute.equalsIgnoreCase(EditRoute);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocationLink, "Routes EIP Allocation Link");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocationLink, "Routes EIP Allocation Link");

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.Routes_EIPSubnetAllocationforService_Header)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.CPE_Routes_EIPAllocation_SubnetTypeValue, "Subnet Type");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, "Sub Net Size");
				click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, "Sub Net Size");

				// sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown,PE_RouteSubnetSizeEdit,"Sub
				// Net Size");
				webDriver.findElement(By.xpath("(//div[label[text()='Sub Net Size']]//span[contains(text(),'"
						+ PE_RouteSubnetSizeEdit + "')])[1]")).click();
				addDropdownValues_commonMethod("Sub Net Size",
						APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, PE_RouteSubnetSizeEdit);
				click(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown, "Sub Net Size");
				webDriver.findElement(By.xpath("(//div[label[text()='Sub Net Size']]//span[contains(text(),'"
						+ PE_RouteSubnetSizeEdit + "')])[1]")).click();
				// addDropdownValues_commonMethod("Sub Net
				// Size",APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_SubnetSizeDropdown,PE_RouteSubnetSizeEdit);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_EIPAllocation_AvailablePoolsMessage,
						"Available Pool Message");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_AllocateSubnetButon, "Edit Route header");
				String AllocateSubnet = "Allocate Subnet";
				String Allocatesubnet = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Routes_AllocateSubnetButon,"Routes_Allocate Sub net Buton");
				Allocatesubnet.equalsIgnoreCase(AllocateSubnet);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.EditRoutes_EIPAllocation_CloseButton,
						"Close EIP Allocation window");
				click(APTIPAccessNoCPE.APTNoCPE.EditRoutes_EIPAllocation_CloseButton, "Close EIP Allocation window");
			} else {
				Reporter.log("EIP Subnet Allocation for Service popup is not displaying");
			}

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Gatewaytextfield, "Getway");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.Routes_Gatewaytextfield);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_Gatewaytextfield, PE_GatewayEdit, "Getway");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkAddresstextfield, "Network Address");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkAddresstextfield);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkAddresstextfield, PE_NetworkAddressEdit,
					"Network Address");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkMAStextfield, "Network MAS");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkMAStextfield);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_NetworkMAStextfield, PE_NetworkMASEdit, "Network MAS");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Metricstextfield, "Metrics");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.Routes_Metricstextfield);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.Routes_Metricstextfield, PE_MetricsEdit, "Metrics");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "OK Button");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_OKButton, "OK Button");
			waitToPageLoad();
		} else {
			Reporter.log("Routes panel header is not displaying");
		}
	}

	public void deleteRouteFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException, AWTException {

		Reporter.log("Verifying Delete Route Functionality");
		waitForAjax();
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.Routes_Header)) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_SelectRouteCheckbox, "Select Route Checkbox");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_SelectRouteCheckbox, "Select Route Checkbox");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_Actionbutton, "Action");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_Actionbutton, "Action");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Routes_DeleteLink, "Delete Link");
			click(APTIPAccessNoCPE.APTNoCPE.Routes_DeleteLink, "Delete Link");

			// SendkeyusingAction(Keys.ENTER);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);

			// Alert alert = webDriver.switchTo().alert();
			// Actions builder = new Actions(webDriver);

			// builder.build().perform();
			// builder.keyDown(Keys.ENTER).perform();

			Reporter.log("Static Route successfully deleted.");
		}

		else {
			Reporter.log("Routes panel header is not displaying");
		}
	}

	public void verifyInterfaceConfigHistory(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
		//String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");

		waitToPageLoad();
		if (vendormodel.contains("Juniper")) {
			searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceconfighistory_header, "Interface Configuration History");
			String interfacecon = "Interface Configuration History";
			String Interfacecon = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interfaceconfighistory_header, "Interface Configuration History");
			Interfacecon.equalsIgnoreCase(interfacecon);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.date_column, "Interface Configuration History");
			String datecolumn = "Date";
			String Datecolumn = getTextFrom(APTIPAccessNoCPE.APTNoCPE.date_column,"date_column");
			Datecolumn.equalsIgnoreCase(datecolumn);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.filename_column, "File Name column");
			String FileName = "File Name";
			String Filname = getTextFrom(APTIPAccessNoCPE.APTNoCPE.filename_column, "File Name column");
			Filname.equalsIgnoreCase(FileName);
		} else {
			Reporter.log("verifyInterfaceConfigHistory Done");
			Report.LogInfo("INFO", "verify Interface Config History Done", "PASS");

		}
	}

	public void verifyFetchDeviceInterface_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ServiceIdentification");
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String devicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String Inservice_status = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InServiceStatus");
		String Inmaintenance_status = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InMaintenanceStatus");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ManagementAddress");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SnmProNewValue");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Country");

		scrollDown(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header1);

		Reporter.log("Verifying Fetch Device Functionality");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header1)) {
			Reporter.log("View PE Device' page navigated as expected");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewdevice_Actiondropdown, "Action");
			click(APTIPAccessNoCPE.APTNoCPE.viewdevice_Actiondropdown, "Action");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewdevice_fetchinterfacelink, "Fetch Interface");
			click(APTIPAccessNoCPE.APTNoCPE.viewdevice_fetchinterfacelink, "Fetch Interface");

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.PE_FetchDeviceInterfacesSuccessMessage)) {
				Reporter.log(
						"Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.herelink_fetchinterface, "herelink fetch interface");
				click(APTIPAccessNoCPE.APTNoCPE.herelink_fetchinterface, "herelink fetch interface");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.managenetwork_header, "Manage Network header");
				String managenetwork = "Manage COLT's Network - Manage Network";
				String manageNetwork = getTextFrom(APTIPAccessNoCPE.APTNoCPE.managenetwork_header, "Manage Network header");
				manageNetwork.equalsIgnoreCase(managenetwork);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_header, "Status Header");
				String status = "Status";
				String Status = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_header, "Status Header");
				Status.equalsIgnoreCase(status);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_header, "Synchronization header");
				String synchronization = "Synchronization";
				String Synchronization = getTextFrom(APTIPAccessNoCPE.APTNoCPE.synchronization_header, "Synchronization header");
				Synchronization.equalsIgnoreCase(synchronization);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_devicecolumn, "Device column header");
				String statusdevice = "Device";
				String statusDevice = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_devicecolumn, "Device column header");
				statusDevice.equalsIgnoreCase(statusdevice);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_statuscloumn, "Status column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_lastmodificationcolumn,
						"Last Modification column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_Action, "Action column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_devicecolumn, "Device column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_syncstatuscolumn, "Sync Status column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_smartscolumn, "Smarts column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_FetchInterfacescolumn,
						"Fetch Interfaces column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_vistamartdevice,
						"VistaMart Device column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_actioncolumn, "Action column header");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_FetchInterfacescolumn,
						"Fetch Interfaces column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_FetchInterfacescolumn,
						"Fetch Interfaces column header");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_devicevalue, "Device column header");
				String statusdevicevalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_devicevalue, "Device column header");
				statusdevicevalue.equalsIgnoreCase(devicename);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_statusvalue, "status");
				String ServiceStatus = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_statusvalue, "status");
				if (ServiceStatus.equalsIgnoreCase(Inservice_status)) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.status_statusvalue, "Status");
					String statusstatusvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_statusvalue, "status");
					statusstatusvalue.equalsIgnoreCase(Inservice_status);
				} else {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.status_statusvalue, "Status");
					String InmaintenanceStatus = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_statusvalue, "status");
					InmaintenanceStatus.equalsIgnoreCase(Inmaintenance_status);
				}
				String GMTValue;
				String LastModificationvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.status_lastmodificationvalue,"status_last modification value");
				if (LastModificationvalue.length() > 3) {
					GMTValue = LastModificationvalue.substring(LastModificationvalue.length() - 3);

				} else {
					GMTValue = LastModificationvalue;
				}
				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_statuslink, "Status");
				click(APTIPAccessNoCPE.APTNoCPE.status_statuslink, "Status");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Statuspage_header, "Status page header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_nameheader, "Name field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_vendormodelheader, "Vendor/Model field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_managementaddressheader,
						"Management Address field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_countryheader, "Country field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_cityheader, "City field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_siteheader, "Site field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_premiseheader, "Premise field header");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_namevalue, "statuspage_namevalue");
				String deviceName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.statuspage_namevalue, "statuspage_namevalue");
				deviceName.equalsIgnoreCase(devicename);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_vendormodelvalue, "statuspage_vendormodelvalue");
				String vendorModel = getTextFrom(APTIPAccessNoCPE.APTNoCPE.statuspage_vendormodelvalue, "statuspage_vendormodelvalue");
				vendorModel.equalsIgnoreCase(vendormodel);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_managementaddressvalue, "Management Address");
				String managementAddress = getTextFrom(APTIPAccessNoCPE.APTNoCPE.statuspage_managementaddressvalue, "Management Address");
				managementAddress.equalsIgnoreCase(managementaddress);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_snmprovalue, "statuspage_snmprovalue");
				String Snmpro = getTextFrom(APTIPAccessNoCPE.APTNoCPE.statuspage_snmprovalue, "statuspage_snmprovalue");
				Snmpro.equalsIgnoreCase(snmpro);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_countryvalue, "statuspage_countryvalue");
				String Country = getTextFrom(APTIPAccessNoCPE.APTNoCPE.statuspage_countryvalue, "statuspage_countryvalue");
				Country.equalsIgnoreCase(country);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_cityvalue, "City");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_sitevalue, "Site");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_premisevalue, "Premise");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Statuspage_statusheader, "Status header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_currentstatusfieldheader,
						"Current Status field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusfieldheader, "New Status field header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_currentstatusvalue, "Current Status");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusdropdown, "New Status Dropdown");
				click(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusdropdown, "New Status Dropdown");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusdropdownvalue,
						"status page newstatus dropdown");
				click(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusdropdownvalue, "status page newstatus dropdown");

				String NewStatusvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusdropdownvalue, "status page newstatus dropdown");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_okbutton, "OK");
				click(APTIPAccessNoCPE.APTNoCPE.statuspage_okbutton, "OK");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_statuscolumnheader, "Status column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_changedon_columnheader, "Changed On column header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_changedby_columnheader, "Changed By column header");
				

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusvalue, "statuspage_newstatusvalue");
				String newStatusvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.statuspage_newstatusvalue, "statuspage_newstatusvalue");
				newStatusvalue.equalsIgnoreCase(NewStatusvalue);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.statuspage_closebutton, "Close");
				click(APTIPAccessNoCPE.APTNoCPE.statuspage_closebutton, "Close");

				waitForAjax();

				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_viewinterfaceslink, "View Interfaces");
				click(APTIPAccessNoCPE.APTNoCPE.status_viewinterfaceslink, "View Interfaces");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewinterfacepage_header, "Interface page header");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.status_viewinterfaceslink, "Interface page subheader");

				String AddedInterface = getAttributeFrom(APTIPAccessNoCPE.APTNoCPE.addedInterface, "style");

				if (!AddedInterface.contains("height: 1px")) {
					Reporter.log("Interface added in table");

				} else {
					Reporter.log("NO Interface added in table");

				}
				verifyExists(APTIPAccessNoCPE.APTNoCPE.viewinterface_closebutton, "Close");
				click(APTIPAccessNoCPE.APTNoCPE.viewinterface_closebutton, "Close");

				waitForAjax();

				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_devicevalue, "Device");
				String DeviceName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.synchronization_devicevalue, "Device");
				DeviceName.equalsIgnoreCase(devicename);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_syncstatusvalue, "Sync Status");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_smartsvalue, "Smarts");

				String Smartsvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.smarts_datetimevalue,"smarts_datetimevalue");
				String SmartsDateTimevalue = "";
				if (Smartsvalue.length() > 20) {
					SmartsDateTimevalue = Smartsvalue.substring(Smartsvalue.length() - 20);
					Reporter.log("last 20 characters: " + SmartsDateTimevalue);

				} else {
					SmartsDateTimevalue = Smartsvalue;
				}
				Reporter.log("Smarts date value is displayed as: " + SmartsDateTimevalue);

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd mm:ss");
				if (SmartsDateTimevalue.length() > 3) {
					GMTValue = SmartsDateTimevalue.substring(SmartsDateTimevalue.length() - 3);
				} else {
					GMTValue = SmartsDateTimevalue;
				}

				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_fetchinterfacesvalue, "Fetch Interfaces");

				String GMTValue1;
				String FetchInterfacesvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.fetchinterfaces_datetime,"fetchinterfaces_datetime");
				String FetchInterfaces_DateTimevalue = "";
				if (FetchInterfacesvalue.length() > 20) {
					FetchInterfaces_DateTimevalue = FetchInterfacesvalue.substring(FetchInterfacesvalue.length() - 20);
					System.out.println("last 20 characters:" + FetchInterfaces_DateTimevalue);
				} else {
					FetchInterfaces_DateTimevalue = FetchInterfacesvalue;
				}

				Reporter.log("Fetch Interfaces date value is displayed as: " + FetchInterfaces_DateTimevalue);
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-mm-dd mm:ss");
				if (FetchInterfaces_DateTimevalue.length() > 3) {
					GMTValue1 = FetchInterfaces_DateTimevalue.substring(FetchInterfaces_DateTimevalue.length() - 3);
				} else {
					GMTValue1 = FetchInterfaces_DateTimevalue;
				}

				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_fetchinterfacesvalue, "Fetch Interfaces");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_vistamartdevicevalue, "VistaMart Device");
				verifyExists(APTIPAccessNoCPE.APTNoCPE.synchronization_synchronizelink, "Synchronize");
				click(APTIPAccessNoCPE.APTNoCPE.synchronization_synchronizelink, "Synchronize");
				waitForAjax();

				verifyExists(APTIPAccessNoCPE.APTNoCPE.Sync_successmsg, "Name");
				String successmsg = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Sync_successmsg,"Sync_success msg");
				String Successmsg = "Sync started successfully. Please check the sync status of this device.";
				successmsg.equalsIgnoreCase(Successmsg);

			} else {
				Reporter.log(
						"Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed");

			}
		} else {
			Reporter.log("View PE Device' page not navigated");

		}
	}

	public void selectInterfacelinkforDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");

		Reporter.log("check 'Select Interface' link");
		waitforPagetobeenable();

		waitForAjax();
		if (isVisible(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);
			waitForAjax();
			webDriver.findElement(By.xpath(
					"//tr//*[contains(.,'" + name + "')]//following-sibling::span//a[contains(.,'Select Interfaces')]"))
					.click();
			waitForAjax();
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	public void deleteDevice_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException, AWTException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");
		scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			webDriver.findElement(By.xpath("//tr//*[contains(.,'" + name
					+ "')]//following-sibling::span//a[contains(.,'Delete from Service')]")).click(); // Alert
																										// alert
																										// =
																										// webDriver.switchTo().alert();

			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);

			waitForAjax();

			verifyExists(APTIPAccessNoCPE.APTNoCPE.PEDevicedeletionsuccessmessage,"Success message");

			waitForAjax();
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	public void deleteSevice1(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException, AWTException {

		waitForAjax();
		scrollDown(APTIPAccessNoCPE.APTNoCPE.orderpanelheader);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.orderpanelheader, "order panel header");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "Action dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.serviceactiondropdown, "Action dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.delete, "Delete");
		click(APTIPAccessNoCPE.APTNoCPE.delete, "Delete");

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.deletesuccessmsg, "Device");
		String deletesuccessmsg = getTextFrom(APTIPAccessNoCPE.APTNoCPE.deletesuccessmsg,"delete success msg");
		String deletesuccessmsgs = "Service successfully deleted.";
		deletesuccessmsg.equalsIgnoreCase(deletesuccessmsgs);
	}

	public void VerifyEditInterface_JuniperVendor(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");

		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String edit_eipallocation1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"edit_eipallocation1");
		String edit_eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String edit_eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EIPAllocation_Subnetsize");
		String edit_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_GetAddress");
		String edit_interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceAddressRange_value");
		String edit_eipallocation2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EIPAllocation2");
		String edit_eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_EIPAllocation_IPv6_Subnetsize");
		String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IPv6_GetAddress");
		String edit_interfaceaddressrangeIPv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_InterfaceAddressRangeIPv6_value");
		String edit_linkvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_LinkValue");
		// String edit_network = DataMiner.fngetcolvalue(testDataFile,
		// dataSheet, scriptNo, dataSetNo, "Edit_Network");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_value");
		String edit_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Bandwidth_value");
		String edit_cardtype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_CardType_DropdownValue");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EncapsulationValue");
		String edit_framingtype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_FramingTypeValue");
		String edit_clocksource_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ClockSourceValue");
		String edit_STM1Number_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_STM1NumberValue");
		String edit_bearerNo_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerNumber_Value");
		String edit_unitid_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_UnitIDValue");
		String edit_slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_SlotValue");
		String edit_pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_PicValue");
		String edit_port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_PortValue");
		String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLANIDvalue");
		String edit_juniper_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_Juniper_ConfigInterface_checkbox");

		Reporter.log("Verifying Edit Interface Functionality - Juniper");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
				

				scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);
				waitForAjax();
				webDriver.findElement(By.xpath(
						"//tr//*[contains(text(),'" + InterfaceName + "')]//following-sibling::td//a[text()='Edit']"))
						.click();
				// verifyExists(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");
				// click(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.editinterface_header)) {
					Reporter.log("Edit Interface' page navigated as expected");

					scrollDown(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox);
					waitForAjax();
					verifyExists(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
							"Configure Interface on Device");
					click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue, "Network");
					// selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue,edit_network,"Network");
					// verifyExists(APTIPAccessNoCPE.APTNoCPE.Interfacefield,
					// "Interface search");
					// sendKeys(APTIPAccessNoCPE.APTNoCPE.Interfacefield,
					// InterfaceName, "Interface send");

					if (edit_eipallocation1.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
						click(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header, "EIP Subnet Allocation");
							verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_value, "Subnet Type");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, "City");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown,
									edit_eipallocation_city, "City");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, "Sub Net Size0");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize,
									edit_eipallocation_subnetsize, "Sub Net Size0");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_availablepools_value,
									"Available Pools");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet button");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
							click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						} else {
							Reporter.log("EIP Allocation button is not working");

						}
					}
					if (edit_getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
									"Interface Address Range");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
									edit_interfaceaddressrange_value, "Address dropdown");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									"Interface Address Range");
							sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									edit_interfaceaddressrange_value, "Interface Address Range");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						}
					}

					// verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
					// "Interface Address Range Arrow");
					// click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
					// "Interface Address Range Arrow");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.networkipv6_fieldvalue, "Network");

					// verify EIP Allocation
					if (edit_eipallocation2.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
						click(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocationIPv6_header,
								"EIP Address Allocation header");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_value, "Subnet Type");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_spacename, "Space Name");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, "Sub Net Size");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize,
								edit_eipallocation_ipv6_subnetsize, "ipv6 sub net size");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown, "Available Blocks");
							verifyExists(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
							click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

							// verify getaddress ipv6
							if (edit_ipv6_getaddress.equalsIgnoreCase("yes")) {
								verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");
								click(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown,
										"Sub Net Size");
								selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown,
										edit_interfaceaddressrangeIPv6_value, "IP6 value");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
								click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
							} else {
								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
										"Interface Address Range");
								sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
										edit_interfaceaddressrangeIPv6_value, "Interface Address Range IPv6");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
								click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
							}
						} else {
							Reporter.log("Available Blocks dropdown values are not displaying");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_popupclose, "Close");
							click(APTIPAccessNoCPE.APTNoCPE.eipallocation_popupclose, "Close");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									"Interface Address Range IPv6");
							sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									edit_interfaceaddressrangeIPv6_value, "Interface Address Range IPv6");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");

						}
					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "Link");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, edit_linkvalue, "Link");
					
					if(isVisible(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit)){
					verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, "Bearer Type");
					selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, edit_bearertype_value,
							"Bearer Type");
					}
					if (edit_bearertype_value.equalsIgnoreCase("10Gigabit Ethernet")
							|| edit_bearertype_value.equalsIgnoreCase("Gigabit Ethernet")
							|| edit_bearertype_value.equalsIgnoreCase("Gigabit Ethernet")) {
						if(isVisible(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown)){
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
								"Bandwidth");
						}
						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");
															
						
						/*verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, edit_framingtype_value,
								"Framing Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, "Clock Source");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, edit_clocksource_value,
								"Clock Source");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
						*/	
					} else if (edit_bearertype_value.equalsIgnoreCase("E1")) {
						if(isVisible(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown)){
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
									"Bandwidth");
							}

						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, edit_framingtype_value,
								"Framing Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, "Clock Source");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, edit_clocksource_value,
								"Clock Source");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
					} else if (edit_bearertype_value.equalsIgnoreCase("E3")
							|| edit_bearertype_value.equalsIgnoreCase("T3")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
								"Bandwidth");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
					} else if (edit_bearertype_value.equalsIgnoreCase("STM-1")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
								"Bandwidth");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
								"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
								edit_encapsulation_value, "Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");
					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, "Unit ID");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, edit_unitid_value, "Unit ID");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.slot_textfield, "Slot");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.slot_textfield, edit_slot_value, "Slot");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.pic_textfield, "Pic");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.pic_textfield, edit_pic_value, "Pic");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.port_textfield, "Port");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.port_textfield, edit_port_value, "Port");

					// String edit_Juniper_InterfaceName = getAttributeFrom(
					// APTIPAccessNoCPE.APTNoCPE.interfacename_textfield,
					// "value");
					if (edit_encapsulation_value.equalsIgnoreCase("802.1q")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, "VLAN Id");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, edit_vlanID_value, "VLAN Id");
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");
					click(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");
					
					if(isVisible(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox)){
					verifyExists(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");
					click(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");
					}
					// verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox,
					// "BGP");
					// click(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");

					// configuration panel in edit interface page
					if (edit_juniper_configureinterface_checkbox.equalsIgnoreCase("Yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_header, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_textarea, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

						Alert alert = webDriver.switchTo().alert();

						// Capturing alert message.
						String alertMessage = webDriver.switchTo().alert().getText();
						if (alertMessage.isEmpty()) {
							Reporter.log("No message displays");

						} else {
							Reporter.log("Alert message displays as: " + alertMessage);

						}
						alert.accept();
					} else {
						editedInterfaceName = getAttributeFrom(APTIPAccessNoCPE.APTNoCPE.Interfacefield, "value");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						
					}
					Reporter.log("Interface successfully updated.");
				} else {
					Reporter.log("Edit Interface' page not navigated");

				}
			} else {
				Reporter.log("Interface is not added");
			}
		} else {
			Reporter.log("No Device added in grid");
		}
	}

	public void verify_CiscoVendor_EditInterface(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String edit_eipallocation1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"edit_eipallocation1");
		String edit_eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String edit_eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EIPAllocation_Subnetsize");
		String edit_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_GetAddress");
		String edit_interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceAddressRange_value");
		String edit_eipallocation2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EIPAllocation2");
		String edit_eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_EIPAllocation_IPv6_Subnetsize");
		String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IPv6_GetAddress");
		String edit_interfaceaddressrangeIPv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Edit_InterfaceAddressRangeIPv6_value");
		String edit_linkvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_LinkValue");
		String edit_network = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_Network");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_value");

		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_EncapsulationValue");
		String edit_framingtype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_FramingTypeValue");

		String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLANIDvalue");
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceName");
		String getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "GetAddress_Button");
		String edit_ciscovendor_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_CiscoVendor_Bandwidth_value");
		String edit_bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BGPCheckbox");
		String edit_bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BGPTemplate_Dropdownvalue");
		String edit_cpewan_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_CPEWAN_Value");
		String edit_cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_CPEWANIPv6_Value");
		String edit_descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_DescriptionValue");
		String edit_ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_AsCustomerValue");
		String edit_bgppassword_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BGPPasswordValue");
		String ipsubnetipv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IPSubnetIPv6Value");
		String ipsubnetipv4_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IPSubnetIPv4Value");
		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");

		Reporter.log("Verifying Edit Interface Functionality - Cisco");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.portalaccess_header, "portal access header");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			if (isClickable(APTIPAccessNoCPE.APTNoCPE.selectinterface)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.selectinterface, "select interface");
				click(APTIPAccessNoCPE.APTNoCPE.selectinterface, "select interface");

				Reporter.log("Clicked on existing Interface radio button");

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.addeddevice_interface_actiondropdown)) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.addeddevice_interface_actiondropdown,
							"addeddevice interface actiondropdown");
					click(APTIPAccessNoCPE.APTNoCPE.addeddevice_interface_actiondropdown,
							"addeddevice interface actiondropdown");
				} else if (isClickable(APTIPAccessNoCPE.APTNoCPE.addeddevice_interface_actiondropdown)) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.addeddevice_interface_actiondropdown,
							"addeddevice interface actiondropdown");
					click(APTIPAccessNoCPE.APTNoCPE.addeddevice_interface_actiondropdown,
							"addeddevice interface actiondropdown");
				}

				verifyExists(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");
				click(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.editinterface_header)) {
					Reporter.log("Edit Interface' page navigated as expected");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
							"Configure Interface on Device");
					click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");

					if (!edit_interfacename.equalsIgnoreCase("null")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, "Interface");
						// clearTextBox(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield);
						// sendKeys(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield,
						// edit_interfacename, "Interface");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, "Interface");
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue, "Network");
					select(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue, edit_network);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");

					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header, "EIP Subnet Allocation");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_value, "Subnet Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, "City");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, edit_eipallocation_city);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, "Sub Net Size0");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, edit_eipallocation_subnetsize);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_availablepools_value, "Available Pools");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
					} else {
						Reporter.log("EIP Allocation button is not working");

					}

					// verify getaddress
					if (getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown, "Sub Net Size0");
						select(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
								edit_interfaceaddressrange_value);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
								"Interface Address Range Arrow");
						click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
								"Interface Address Range");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
								edit_interfaceaddressrange_value, "Interface Address Range");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
								"Interface Address Range Arrow");
						click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");

					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.networkipv6_fieldvalue, "Network");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue, "Network");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocationIPv6_header, "EIP Address Allocation header");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_valueMultilink, "Subnet Type");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_spacename, "Space Name");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, "Sub Net Size");
					select(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, edit_eipallocation_ipv6_subnetsize);

					if (isClickable(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown, "Available Blocks");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

						// verify getaddress ipv6
						if (edit_ipv6_getaddress.equalsIgnoreCase("yes")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");
							click(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown, "Sub Net Size");
							select(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown,
									edit_interfaceaddressrangeIPv6_value);

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									"Interface Address Range");
							sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									edit_interfaceaddressrangeIPv6_value, "Interface Address Range IPv6");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
						}
					} else {
						Reporter.log("Available Blocks dropdown values are not displaying");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_popupclose, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.eipallocation_popupclose, "Close");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
								"Interface Address Range IPv6");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
								edit_interfaceaddressrangeIPv6_value, "Interface Address Range IPv6");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
								"Interface Address Range IPv6 Arrow");
						click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
								"Interface Address Range IPv6 Arrow");
					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "Link");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, edit_linkvalue, "Link");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, "Bearer Type");
					select(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, edit_bearertype_value);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.multilink_bandwidth_dropdown, "Bearer Type");
					select(APTIPAccessNoCPE.APTNoCPE.multilink_bandwidth_dropdown, edit_ciscovendor_bandwidth_value);

					if (edit_bearertype_value.equalsIgnoreCase("E1")) {

						verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
						select(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, edit_framingtype_value);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						select(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, edit_encapsulation_value);

					} else if (edit_bearertype_value.equalsIgnoreCase("Ethernet")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, "VLAN Id");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, edit_vlanID_value, "VLAN Id");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
						select(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, edit_encapsulation_value);
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "lin text field");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");
					click(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");

					if (edit_bgp_checkbox.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bgptemplate_dropdown, "BGP Templates Generate For");
						select(APTIPAccessNoCPE.APTNoCPE.bgptemplate_dropdown, edit_bgptemplate_dropdownvalue);

						if (!edit_cpewan_value.equalsIgnoreCase("null")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.cpewan_textfield, "CPE WAN");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.cpewan_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewan_textfield, edit_cpewan_value, "CPE WAN");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.cpewan_textfield, "CPE WAN");

						}
						if (!edit_cpewanipv6_value.equalsIgnoreCase("null")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.cpewanipv6_textfield, "CPE WAN IPv6 Address");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.cpewanipv6_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewanipv6_textfield, edit_cpewanipv6_value,
									"CPE WAN IPv6 Address");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.cpewanipv6_textfield, "CPE WAN IPv6 Address");

						}
						if (!edit_descriptionfield_value.equalsIgnoreCase("null")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_descriptionfield, "Description");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.bgp_descriptionfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_descriptionfield, edit_descriptionfield_value,
									"Description");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_descriptionfield, "Description");

						}
						if (!edit_ascustomerfield_value.equalsIgnoreCase("null")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_ascustomerfield, "AS CUSTOMER");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.bgp_ascustomerfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_ascustomerfield, edit_ascustomerfield_value,
									"AS CUSTOMER");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_ascustomerfield, "CPE WAN IPv6 Address");

						}

						if (!edit_bgppassword_value.equalsIgnoreCase("null")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bgppassword_field, "BGP PASSWORD");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.bgppassword_field);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.bgppassword_field, edit_bgppassword_value,
									"BGP PASSWORD");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bgppassword_field, "BGP PASSWORD");

						}

						// Add IP Subnet IPv6
						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_addbutton, "IP Subnet IPv6 Add");
						click(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_addbutton, "IP Subnet IPv6 Add");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_textfield, "IP Subnet IPv6");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_textfield, ipsubnetipv6_value,
								"IP Subnet IPv6");

						// Remove IP Subnet IPv6
						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_removebutton, "IP Subnet IPv6 Remove");
						click(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_removebutton, "IP Subnet IPv6 Remove");

						// Add IP Subnet IPv6
						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_addbutton, "IP Subnet IPv6 Add");
						click(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_addbutton, "IP Subnet IPv6 Add");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_textfield, "IP Subnet IPv6 Add");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_textfield, ipsubnetipv6_value,
								"IP Subnet IPv6 ");

						// Add IP Subnet IPv4
						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_addbutton, "IP Subnet IPv6 Add");
						click(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv6_addbutton, "IP Subnet IPv6 Add");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_addbutton, "IP Subnet IPv4 Add");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_addbutton, ipsubnetipv4_value,
								"IP Subnet IPv4");

						// Remove IP Subnet IPv4
						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_removebutton, "IP Subnet IPv4 Remove");
						click(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_removebutton, "IP Subnet IPv4 Remove");

						// Add IP Subnet IPv4
						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_addbutton, "IP Subnet IPv4 Add");
						click(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_addbutton, "IP Subnet IPv4 ");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_textfield, "IP Subnet IPv4 Add");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.ipsubnetipv4_textfield, ipsubnetipv4_value,
								"IP Subnet IPv4");

					}

					if (edit_configureinterface_checkbox.equalsIgnoreCase("Yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_header, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_textarea, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.saveconfiguration_button, "Save Configuration to File");
						click(APTIPAccessNoCPE.APTNoCPE.saveconfiguration_button, "Save Configuration to File");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceconfighistory_header,
								"Interface Configuration History header");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.date_column, "Date column");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.filename_column, "File Name column");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

						waitForAjax();

						verifyExists(APTIPAccessNoCPE.APTNoCPE.UpdateInterfaceSucessMessage,
								"Interface update success message");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					}
				} else {
					Reporter.log("Interface is not added");

				}
			} else {
				Reporter.log("Edit Interface' page not navigated");

			}
		}
	}

	public void hostnametextField_IPV4_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo,
			String ipAddress) throws InterruptedException, IOException {
		String command_ipv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv4_hostnameTextfield)) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv4_hostnameTextfield, "IP Address or Hostname");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv4_hostnameTextfield, ipAddress,
					"IP Address or Hostname");
		} else {
			Reporter.log("Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4);

		}
	}

	public void vrfNametextField_IPV4_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String command_ipv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_CommandIPV4");
		String vrfname_ipv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_vrf_Ipv4");
		if (command_ipv4.contains("vrf")) {
			if (isClickable(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv4_vrfnameTextField)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv4_vrfnameTextField, "Router Vrf Name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv4_vrfnameTextField, vrfname_ipv4,
						"Router Vrf Name");
			}

			else {
				Reporter.log("VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4);

			}
		} else {
			Reporter.log("VRF Name IPv4' text field is not displaying for " + command_ipv4);

		}
	}

	public void executeCommandAndFetchTheValue_PE() throws InterruptedException, IOException {
		click(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV4Command_Executebutton, "Execute");
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.PE_CPE_result_textArea))

		{
			Reporter.log("Result' text field is displaying");

			String remarkvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.PE_CPE_result_textArea,"PE_CPE_result_textArea");
			Reporter.log("value under 'Result' field displaying as " + remarkvalue);

		} else {
			Reporter.log("Result' text field is not displaying");

		}
	}

	public void executeCommandAndFetchTheValue_PE6() throws InterruptedException, IOException {
		click(APTIPAccessNoCPE.APTNoCPE.PE_CPE_Router_IPV6Command_Executebutton, "Execute");
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.PE_CPE_result_textArea))

		{
			Reporter.log("Result' text field is displaying");

			String remarkvalue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.PE_CPE_result_textArea,"PE_CPE_result_textArea");
			Reporter.log("value under 'Result' field displaying as " + remarkvalue);

		} else {
			Reporter.log("Result' text field is not displaying");

		}
	}

	public void hostnametextField_IPV6_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo,
			String ipv6Address) throws InterruptedException, IOException {
		String commandIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_CommandIPV6");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv6_hostnameTextfield)) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv6_hostnameTextfield, "Router Vrf Name");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv6_hostnameTextfield, ipv6Address, "Router Vrf Name");
		} else {
			Reporter.log("Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6);

		}
	}

	public void vrfNametextField_IPV6_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String commandIPV6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_CommandIPV6");
		String vrfname_IPV6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_vrf_Ipv4");
		if (commandIPV6.contains("vrf")) {
			if (isClickable(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv6_vrfnameTextField)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv6_vrfnameTextField, "Router Vrf Name");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.PE_CPE_commandIPv6_vrfnameTextField, vrfname_IPV6,
						"Router Vrf Name");
			}

			else {
				Reporter.log("VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6);

			}
		} else {
			Reporter.log("VRF Name IPv6' text field is not displaying for " + commandIPV6);

		}
	}

	public void SelectInterfacetoaddwithservcie(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Edit_InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		// String InterfaceName = DataMiner.fngetcolvalue(testDataFile,
		// dataSheet, scriptNo, dataSetNo, "InterfaceName");
		scrollDown(APTIPAccessNoCPE.APTNoCPE.InteraceColumn_Filter);
		waitForAjax();

		if (Edit_InterfaceName.equalsIgnoreCase("null")) {
			webDriver.findElement(By.xpath("(//div[@col-id='interfaceName'][text()='" + editedInterfaceName + "'])[1]"))
					.click();
			waitForAjax();

		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.interface_gridselect1+Edit_InterfaceName+APTIPAccessNoCPE.APTNoCPE.interface_gridselect2, "Interface check box");
			click(APTIPAccessNoCPE.APTNoCPE.interface_gridselect1+Edit_InterfaceName+APTIPAccessNoCPE.APTNoCPE.interface_gridselect2, "Interface check box");

			//sendKeys(APTIPAccessNoCPE.APTNoCPE.InterfacefilterTxt, Edit_InterfaceName, "Interface search");

			//String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interface_gridselect).replace("value",
					//Edit_InterfaceName);
			//click(CheckToAddInterface);

		}
		waitForAjax();
		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_Actiondropdown, "Action Dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_Actiondropdown, "Action Dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_addbuton, "Add");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_addbuton, "Add");

		scrollDown(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");
		click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");
	}

	public void SelectInterfacetoremovefromservice(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Edit_InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		// String InterfaceName = DataMiner.fngetcolvalue(testDataFile,
		// dataSheet, scriptNo, dataSetNo, "InterfaceName");
		waitforPagetobeenable();
		waitForAjax();
		

		scrollDown(APTIPAccessNoCPE.APTNoCPE.interfacesinservice_filter);
		waitForAjax();

		if (Edit_InterfaceName.equalsIgnoreCase("null")) {
			webDriver.findElement(By.xpath("(//div[@col-id='interfaceName'][text()='" + editedInterfaceName + "'])[1]"))
					.click();
			waitForAjax();
		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_gridselect1+Edit_InterfaceName+APTIPAccessNoCPE.APTNoCPE.interfaceinservice_gridselect2,"Interface in Service search");
			//sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_fitertext, Edit_InterfaceName,
					//"Interface in Service search");

			click(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_gridselect1+Edit_InterfaceName+APTIPAccessNoCPE.APTNoCPE.interfaceinservice_gridselect2, "Interface in Service search");

			/*String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_gridselect)
					.replace("value", Edit_InterfaceName);
			click(CheckToAddInterface);
			*/
		}
		waitForAjax();
		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_Actiondropdown, "Action Dropdown");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_Actiondropdown, "Action Dropdown");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_removebuton, "Remove");
		click(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_removebuton, "Remove");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.RemoveSelectInterfaceSuccessMessage, "Remove Interface Success Message");
	}

	public void verify_JuniperVendor_AddMultilink(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Multilink_ConfigInterface_checkbox");
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_SubnetSize");
		String interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		String eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LinkValue");
		String Multilink_Encapsulation_Value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_Encapsulation_Value");
		String getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "GetAddress_Button");
		String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IPV6_GetAddressButton");
		String unitid_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "UnitIDValue");
		String slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SlotValue");
		String pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PicValue");
		String port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PortValue");
		String bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPTemplate_Value");
		String cpewan_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEWAN_Value");
		String cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CPEWANIPv6_Value");
		String descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Description_Value");
		String ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"AsCustomer_Value");
		String bgppassword_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPPassword_Value");
		String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CheckToAddInterface_Checkbox");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");
		String multilink_bgpcheckbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_BGPCheckbox");
		String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_InterfaceName");
		String editDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			scrollDown(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2);
			waitForAjax();
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2, "View Link");
			webDriver.findElement(By.xpath("//td[contains(.,'" + editDevicename + "')]//span//a[text()='View']"))
					.click();

			waitForAjax();

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header1)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaces_header, "Interfaces header");

				scrollDown(APTIPAccessNoCPE.APTNoCPE.interfacepanel_actiondropdown);
				verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacepanel_actiondropdown, "Action dropdown");
				click(APTIPAccessNoCPE.APTNoCPE.interfacepanel_actiondropdown, "Action dropdown");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.addmultilink_link, "Add Multilink");
				click(APTIPAccessNoCPE.APTNoCPE.addmultilink_link, "Add Multilink");

				waitForAjax();

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.addmultilink_header)) {
					scrollDown(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox);
					waitForAjax();
					verifyExists(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
							"Configure Interface on Device");
					click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");

					if (multilink_configureinterface_checkbox.equalsIgnoreCase("no")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.interface_warngmsg, "interface");
					if (isVisible(APTIPAccessNoCPE.APTNoCPE.encapsulation_warngmsg)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_warngmsg, "encapsulation");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								"encapsulation Warning message is not displayed");
						Report.LogInfo("INFO", "encapsulation Warning message is not displayed", "FAIL");
					}
					// verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_warngmsg,
					// "encapsulation");
					if (isVisible(APTIPAccessNoCPE.APTNoCPE.slot_warngmsg)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.slot_warngmsg, "slot");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Slot Warning message is not displayed");
						Report.LogInfo("INFO", "Slot Warning message is not displayed", "FAIL");
					}

					if (isVisible(APTIPAccessNoCPE.APTNoCPE.pic_warngmsg)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.pic_warngmsg, "pic");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Pic Warning message is not displayed");
						Report.LogInfo("INFO", "Pic Warning message is not displayed", "FAIL");
					}

					if (isVisible(APTIPAccessNoCPE.APTNoCPE.port_warngmsg)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.port_warngmsg, "port");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Port Warning message is not displayed");
						Report.LogInfo("INFO", "Port Warning message is not displayed", "FAIL");
					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, "Interface");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, multilink_interfacename, "Interface");

					scrollDown(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button);
					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
					waitForAjax();
					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header, "EIP Subnet Allocation");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_valueMultilink, "Subnet Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, "City");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, eipallocation_city,
								"City");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, "Sub Net Size");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize,
								eipallocation_subnetsize, "Sub Net Size");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

					} else {
						Reporter.log("EIP Allocation button is not working");

					}
					if (getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
									"Interface Address Range");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
									interfaceaddressrange_value, "Interface Address Range");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									"Interface Address Range");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									interfaceaddressrange_value, "Interface Address Range");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						}

					}
					scrollDown(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button);
					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
					waitForAjax();
					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipallocationIPv6_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, "Sub Net Size");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize,
								eipallocation_ipv6_subnetsize, "Sub Net Size");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");
						click(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
					} else {
						Reporter.log("EIP Allocation button is not working");

					}

					if (edit_ipv6_getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");

						/*
						 * if (isEnable(APTIPAccessNoCPE.APTNoCPE.
						 * interfacerange_AddressIpv6_dropdownMultilink)) {
						 * verifyExists(APTIPAccessNoCPE.APTNoCPE.
						 * interfacerange_AddressIpv6_dropdown,
						 * "Interface Address Range IPv6");
						 * selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.
						 * interfacerange_AddressIpv6_dropdown,
						 * interfaceaddressrange_value,"Interface Address Range"
						 * ); } else { verifyExists(APTIPAccessNoCPE.APTNoCPE.
						 * interfaceaddressrangeIPv6_textfield,
						 * "Interface Address Range IPv6");
						 * sendKeys(APTIPAccessNoCPE.APTNoCPE.
						 * interfaceaddressrangeIPv6_textfield,
						 * interfaceaddressrange_value);
						 * 
						 * verifyExists(APTIPAccessNoCPE.APTNoCPE.
						 * interfaceaddressIPv6_Addarrow,
						 * "Interface Address Range IPv6 Arrow");
						 * click(APTIPAccessNoCPE.APTNoCPE.
						 * interfaceaddressIPv6_Addarrow,
						 * "Interface Address Range IPv6 Arrow"); }
						 */

					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "Link");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, link_value);

					scrollDown(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown);
					verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
					selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, Multilink_Encapsulation_Value,
							"Encapsulation");

					sendKeys(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, unitid_value);
					sendKeys(APTIPAccessNoCPE.APTNoCPE.slot_textfield, slot_value);
					sendKeys(APTIPAccessNoCPE.APTNoCPE.pic_textfield, pic_value);
					sendKeys(APTIPAccessNoCPE.APTNoCPE.port_textfield, port_value);

					// click(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");

					if (multilink_bgpcheckbox.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.multilink_bgptemplate_dropdown, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.multilink_bgptemplate_dropdown,
								bgptemplate_dropdownvalue, "Encapsulation");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewan_textfield, cpewan_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewanipv6_textfield, cpewanipv6_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_descriptionfield, descriptionfield_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_ascustomerfield, ascustomerfield_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgppassword_field, bgppassword_value);

					}
					String MultilinkBearer_ExistingInterface = getAttributeFrom(
							APTIPAccessNoCPE.APTNoCPE.multilinkbearer_tabledata, "style");

					if (!MultilinkBearer_ExistingInterface.contains("height: 1px")) {
						if (checktoaddinterface_checkbox.equalsIgnoreCase("yes")) {
							if (name.contains("Cisco")) {
								String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.checktoaddinterface,"checktoaddinterface")
										.replace("value", interfacename);
								click(CheckToAddInterface,"Add Interface");
							} else {
								click(APTIPAccessNoCPE.APTNoCPE.checktoaddinterface,"check to add interface");

							}

						}
					} else {
						Reporter.log("No existing interfaces to display");

					}

					if (multilink_configureinterface_checkbox.equalsIgnoreCase("yes")
							|| multilink_configureinterface_checkbox.equalsIgnoreCase("null")) {
						click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.saveconfiguration_button, "Save Configuration to File");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

					} else {
						scrollDown(APTIPAccessNoCPE.APTNoCPE.okbutton);
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					}

					waitForAjax();
					waitforPagetobeenable();
					scrollUp();
					if (isVisible(APTIPAccessNoCPE.APTNoCPE.AddMultilinkSucessMessage)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.AddMultilinkSucessMessage, "Mulilink Success Message");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Mulilink Success Message is not displayed");
						Report.LogInfo("INFO", "Mulilink Success Message is not displayed", "FAIL");
					}

					// verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,
					// "Back");
					// click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,
					// "Back");
				} else {
					Reporter.log("Add Multilink' page not navigated");

				}

			} else {
				Reporter.log("View PE Device' page not navigated");

			}
		} else {
			Reporter.log("No Device added in grid");

		}

	}

	public void verify_CiscoVendor_AddMultilink(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Multilink_ConfigInterface_checkbox");
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_SubnetSize");
		String interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		String eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LinkValue");
		String Multilink_Encapsulation_Value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_Encapsulation_Value");
		String getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "GetAddress_Button");
		String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IPV6_GetAddressButton");
		String unitid_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "UnitIDValue");
		String slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SlotValue");
		String pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PicValue");
		String port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PortValue");
		String bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPTemplate_Value");
		String cpewan_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEWAN_Value");
		String cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CPEWANIPv6_Value");
		String descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Description_Value");
		String ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"AsCustomer_Value");
		String bgppassword_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPPassword_Value");
		String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CheckToAddInterface_Checkbox");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");
		String multilink_bgpcheckbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_BGPCheckbox");
		String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_InterfaceName");
		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink1, "View");
			click(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink1, "View");

			waitForAjax();

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header1)) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaces_header, "Interfaces header");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacepanel_actiondropdown, "Action dropdown");
				click(APTIPAccessNoCPE.APTNoCPE.interfacepanel_actiondropdown, "Action dropdown");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.addmultilink_link, "Add Multilink");
				click(APTIPAccessNoCPE.APTNoCPE.addmultilink_link, "Add Multilink");

				waitForAjax();

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.addmultilink_header)) {
					waitForAjax();
					verifyExists(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
							"Configure Interface on Device");
					click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");

					if (multilink_configureinterface_checkbox.equalsIgnoreCase("no")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.interface_warngmsg, "interface");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_warngmsg, "encapsulation");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.slot_warngmsg, "slot");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.pic_warngmsg, "pic");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.port_warngmsg, "port");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, "Interface");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, multilink_interfacename, "Interface");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");

					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header, "EIP Subnet Allocation");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_valueMultilink, "Subnet Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, "City");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, eipallocation_city);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, "Sub Net Size");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, eipallocation_subnetsize);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

					} else {
						Reporter.log("EIP Allocation button is not working");

					}
					if (getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
									"Interface Address Range");
							select(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
									interfaceaddressrange_value);

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									"Interface Address Range");
							select(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									interfaceaddressrange_value);

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						}

					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");

					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipallocationIPv6_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, "Sub Net Size");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, eipallocation_ipv6_subnetsize);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");
						click(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
					} else {
						Reporter.log("EIP Allocation button is not working");

					}

					if (edit_ipv6_getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdownMultilink)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown,
									"Interface Address Range IPv6");
							select(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown,
									interfaceaddressrange_value);
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									"Interface Address Range IPv6");
							sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									interfaceaddressrange_value);

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
						}

					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "Link");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, link_value);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
					select(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, Multilink_Encapsulation_Value);

					sendKeys(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, unitid_value);
					sendKeys(APTIPAccessNoCPE.APTNoCPE.slot_textfield, slot_value);
					sendKeys(APTIPAccessNoCPE.APTNoCPE.pic_textfield, pic_value);
					sendKeys(APTIPAccessNoCPE.APTNoCPE.port_textfield, port_value);

					click(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");

					if (multilink_bgpcheckbox.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.multilink_bgptemplate_dropdown, "Encapsulation");
						select(APTIPAccessNoCPE.APTNoCPE.multilink_bgptemplate_dropdown, bgptemplate_dropdownvalue);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewan_textfield, cpewan_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewanipv6_textfield, cpewanipv6_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_descriptionfield, descriptionfield_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_ascustomerfield, ascustomerfield_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgppassword_field, bgppassword_value);

					}
					String MultilinkBearer_ExistingInterface = getAttributeFrom(
							APTIPAccessNoCPE.APTNoCPE.multilinkbearer_tabledata, "style");

					if (!MultilinkBearer_ExistingInterface.contains("height: 1px")) {
						if (checktoaddinterface_checkbox.equalsIgnoreCase("yes")) {
							if (name.contains("Cisco")) {
								String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.checktoaddinterface,"check to add interface")
										.replace("value", interfacename);
								click(CheckToAddInterface,"Add Interface");
							} else {
								click(APTIPAccessNoCPE.APTNoCPE.checktoaddinterface,"checktoaddinterface");

							}

						}
					} else {
						Reporter.log("No existing interfaces to display");

					}

					if (multilink_configureinterface_checkbox.equalsIgnoreCase("yes")
							|| multilink_configureinterface_checkbox.equalsIgnoreCase("null")) {
						click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.saveconfiguration_button, "Save Configuration to File");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					}

					waitForAjax();

					verifyExists(APTIPAccessNoCPE.APTNoCPE.AddMultilinkSucessMessage,
							"Multilink Added success message");

					// verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,
					// "Back");
					// click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton,
					// "Back");
				} else {
					Reporter.log("Add Multilink' page not navigated");

				}

			} else {
				Reporter.log("View PE Device' page not navigated");

			}
		} else {
			Reporter.log("No Device added in grid");

		}

	}

	public void verify_CiscoVendor_AddInterface(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Multilink_ConfigInterface_checkbox");
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_SubnetSize");
		String interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		String eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LinkValue");
		String Multilink_Encapsulation_Value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_Encapsulation_Value");
		String getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "GetAddress_Button");
		String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IPV6_GetAddressButton");
		String unitid_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "UnitIDValue");
		String slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SlotValue");
		String pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PicValue");
		String port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PortValue");
		String bgptemplate_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPTemplate_Value");
		String cpewan_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEWAN_Value");
		String cpewanipv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CPEWANIPv6_Value");
		String descriptionfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Description_Value");
		String ascustomerfield_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"AsCustomer_Value");
		String bgppassword_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPPassword_Value");
		String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CheckToAddInterface_Checkbox");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");
		String multilink_bgpcheckbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_BGPCheckbox");
		String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_InterfaceName");
		String eipallocation_availableblocksvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "EIPAllocation_AvailableBlocksValue");
		String ipv6_getaddress_button = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IPV6_GetAddressButton");
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerType_Value");
		String ciscovendor_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CiscoVendor_Bandwidth_Value");
		String framingtype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"FramingType_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VLANID_Value");
		String bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "BGP_Checkbox");
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink1, "View");
			click(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink1, "View");

			waitForAjax();

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
				click(APTIPAccessNoCPE.APTNoCPE.interfacepanel_actiondropdown, "Action dropdown");
				click(APTIPAccessNoCPE.APTNoCPE.addinterface_link, "Add Interface/Link");

				if (isClickable(APTIPAccessNoCPE.APTNoCPE.addinterface_header)) {
					verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.interface_warngmsg, "Interface");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_warngmsg, "Bearer Type");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_warngmsg, "Encapsulation");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
							"Configure Interface on Device");
					click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, "Interface");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield, interfacename);

					// verify EIP Allocation
					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");

					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header, "EIP Subnet Allocation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, "City");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, eipallocation_city);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, "Sub Net Size");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, eipallocation_subnetsize);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

					} else {
						Reporter.log("EIP Allocation button is not working");

					}
					// verify getaddress
					if (getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown, "Sub Net Size");
							select(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
									interfaceaddressrange_value);

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									"Interface Address Range IPv6");
							sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
									interfaceaddressrange_value);

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
									"Interface Address Range Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
						}

					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.networkipv6_fieldvalue, "Network");

					// verify EIP Allocation for IPV6

					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");

					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipallocationIPv6_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_value, "Subnet Type");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_spacename, "Space Name");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, "Sub Net Size");
						select(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, eipallocation_ipv6_subnetsize);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown, "Available Blocks");
						select(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown, eipallocation_availableblocksvalue);

						click(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.successmsg, "Subnet aalocation success message");
					} else {
						Reporter.log("EIP Allocation button is not working");

					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue, "network field value");

					// verify getaddress ipv6
					if (ipv6_getaddress_button.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");

						if (isClickable(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown)) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									"Interface Address Range IPv6");
							sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
									interfaceaddressrange_value);

							verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
							click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
									"Interface Address Range IPv6 Arrow");
						}
					} else {
						click(APTIPAccessNoCPE.APTNoCPE.eipallocation_popupclose, "Close");

						sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
								interfaceaddressrange_value);

						click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
								"Interface Address Range IPv6 Arrow");
					}

					sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, link_value);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdown, "Bearer Type");
					select(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdown, bearertype_value);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
					select(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, ciscovendor_bandwidth_value);

					if (bearertype_value.equalsIgnoreCase("E1")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
						select(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, framingtype_value);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
						select(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, encapsulation_value);
					} else if (bearertype_value.equalsIgnoreCase("Ethernet")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, "VLAN Id");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, vlanID_value);

					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
						select(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, encapsulation_value);
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");
					click(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");

					if (bgp_checkbox.equalsIgnoreCase("yes")) {
						select(APTIPAccessNoCPE.APTNoCPE.multilink_bgptemplate_dropdown, bgptemplate_dropdownvalue);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewan_textfield, cpewan_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.cpewanipv6_textfield, cpewanipv6_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_descriptionfield, descriptionfield_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgp_ascustomerfield, ascustomerfield_value);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bgppassword_field, bgppassword_value);
					}

					// configuration panel in add interface page
					if (configureinterface_checkbox.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_header, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");

						click(APTIPAccessNoCPE.APTNoCPE.saveconfiguration_button, "Save Configuration to File");

						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.AddInterfaceSucessMessage,
							"Interface Added success message");
				} else {
					Reporter.log("Add Interface' page not navigated");

				}
			} else {
				Reporter.log("View PE Device' page not navigated");

			}
		} else {
			Reporter.log("No Device added in grid");

		}
	}

	public void VerifyAddInterface_JuniperVendor(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String editDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editdeviceName");

		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingCityValue");
		String eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_SubnetSize");
		String interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		String eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LinkValue");
		// String Multilink_Encapsulation_Value =
		// DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
		// "Multilink_Encapsulation_Value");
		String getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "GetAddress_Button");
		// String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile,
		// dataSheet, scriptNo, dataSetNo,
		// "IPV6_GetAddressButton");
		String unitid_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "UnitIDValue");
		String slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SlotValue");
		String pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PicValue");
		String port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PortValue");
		String ipv6_getaddress_button = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IPV6_GetAddressButton");
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerType_Value");
		// String ciscovendor_bandwidth_value =
		// DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
		// "CiscoVendor_Bandwidth_Value");
		String framingtype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"FramingType_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VLANID_Value");
		// String bgp_checkbox = DataMiner.fngetcolvalue(testDataFile,
		// dataSheet, scriptNo, dataSetNo, "BGP_Checkbox");
		String InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");
		String juniper_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "Juniper_ConfigInterface_checkbox");
		String bandwidth_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Bandwidth_Value");
		String cardtype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CardType_DropdownValue");
		String clocksource_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ClockSourceValue");
		String STM1Number_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"STM1NumberValue");
		String bearerNo_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerNumber_Value");

		if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2)) {
			scrollDown(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2);
			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2, "View Link");
			webDriver.findElement(By.xpath("//td[contains(.,'" + editDevicename + "')]//span//a[text()='View']"))
					.click();

			waitForAjax();
			waitforPagetobeenable();

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header1)) {
				click(APTIPAccessNoCPE.APTNoCPE.interfacepanel_actiondropdown, "Action dropdown");
				click(APTIPAccessNoCPE.APTNoCPE.addinterface_link, "Add Interface/Link");

				waitForAjax();
				if (isClickable(APTIPAccessNoCPE.APTNoCPE.addinterface_header)) {

					ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.executeandsave_button);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
					click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

					ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.interface_warngmsg);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.interface_warngmsg, "Interface");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_warngmsg, "Bearer Type");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_warngmsg, "Encapsulation");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.slot_warngmsg, "Slot");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.pic_warngmsg, "Pic");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.port_warngmsg, "Port");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.stm1number_warngmsg, "STM1 Number");
					verifyExists(APTIPAccessNoCPE.APTNoCPE.interface_warngmsg, "Interface");

					// ScrollIntoViewByString(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox);
					// waitForAjax();
					// click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
					// "Configure Interface on Device");
					// waitForAjax();

					// verify EIP Allocation
					waitForAjax();
					scrollDown(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button);
					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
					waitForAjax();
					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header, "EIP Subnet Allocation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, "City");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, eipallocation_city,
								"City");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, "Sub Net Size");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize,
								eipallocation_subnetsize, "Sub Net Size");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

					} else {
						Reporter.log("EIP Allocation button is not working");

					}
					// verify getaddress
					if (getaddress.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");
						click(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown, "Sub Net Size");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
								interfaceaddressrange_value, "Interface Address Range");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
								"Interface Address Range Arrow");
						click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
								"Interface Address Range IPv6");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
								interfaceaddressrange_value);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
								"Interface Address Range Arrow");
						click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.networkipv6_fieldvalue, "Network");

					// verify EIP Allocation for IPV6
					waitForAjax();
					scrollDown(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button);
					verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
					click(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
					waitForAjax();
					if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipallocationIPv6_header)) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_value, "Subnet Type");
						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_spacename, "Space Name");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, "Sub Net Size");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize,
								eipallocation_ipv6_subnetsize, "Sub Net Size");

						click(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");

						click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

					} else {
						Reporter.log("EIP Allocation button is not working");

					}

					// verify getaddress ipv6
					if (ipv6_getaddress_button.equalsIgnoreCase("yes")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
								"Interface Address Range IPv6");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
								interfaceaddressrange_value);

						verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
								"Interface Address Range IPv6 Arrow");
						click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
								"Interface Address Range IPv6 Arrow");
					}

					scrollDown(APTIPAccessNoCPE.APTNoCPE.link_textfield);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "Link");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, link_value);

					verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdown, "Bearer Type");
					selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdown, bearertype_value, "Bearer Type");

					if (bearertype_value.equalsIgnoreCase("10Gigabit Ethernet")
							|| bearertype_value.equalsIgnoreCase("Gigabit Ethernet")
							|| bearertype_value.equalsIgnoreCase("Gigabit Ethernet")) {
						if(isVisible(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown)){
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, bandwidth_value, "Bandwidth");
						}
						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, cardtype_dropdownvalue,
								"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, encapsulation_value,
								"Encapsulation");

					} else if (bearertype_value.equalsIgnoreCase("E1")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, bandwidth_value, "Bandwidth");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, cardtype_dropdownvalue,
								"Card Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, encapsulation_value,
								"Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, framingtype_value,
								"Framing Type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, "Clock Source");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, clocksource_value,
								"Clock Source");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, bearerNo_value, "Bearer No");
					} else if (bearertype_value.equalsIgnoreCase("E3") || bearertype_value.equalsIgnoreCase("T3")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, bandwidth_value, "Bandwidth");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, encapsulation_value,
								"Encapculation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, cardtype_dropdownvalue,
								"Card type");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, STM1Number_value, "STM1 Number");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, bearerNo_value, "Bearer No");
					} else if (bearertype_value.equalsIgnoreCase("STM-1")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, bandwidth_value, "Bnadwidth");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, cardtype_dropdownvalue,
								"CardType");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, encapsulation_value,
								"Encapsulation");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, STM1Number_value, "STM1 Number");
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, "Encapsulation");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdown, encapsulation_value,
								"Encapsultion");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, STM1Number_value, "STM1 Number");
					}

					verifyExists(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, "Unit ID");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, unitid_value, "Unit ID");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.slot_textfield, "Slot");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.slot_textfield, slot_value, "Slot");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.pic_textfield, "Pic");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.pic_textfield, pic_value, "Pic");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.port_textfield, "Port");
					sendKeys(APTIPAccessNoCPE.APTNoCPE.port_textfield, port_value, "Port");

					String Juniper_InterfaceName = getAttributeFrom(APTIPAccessNoCPE.APTNoCPE.interfacename_textfield,
							"value");
					if (Juniper_InterfaceName.equalsIgnoreCase("802.1q")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, "VLAN Id");
						clearTextBox(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield);
						sendKeys(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, vlanID_value, "VLAN Id");
					}else{
						if(isVisible(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield)){
							verifyExists(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, "VLAN Id");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, vlanID_value, "VLAN Id");
						
						}
					}
					verifyExists(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");
					click(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");
					
					if(isVisible(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox)){
					verifyExists(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");
					click(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");
					}
					// configuration panel in add interface page
					if (juniper_configureinterface_checkbox.equalsIgnoreCase("yes")
							|| juniper_configureinterface_checkbox.equalsIgnoreCase("null")) {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_header, "Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");
						click(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_textarea, "Save Configuration to File");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.Interfacefield, "Interface search");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.Interfacefield, InterfaceName, "Interface send");

						click(APTIPAccessNoCPE.APTNoCPE.saveconfiguration_button, "Execute and Save");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
						click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

						Alert alert = webDriver.switchTo().alert();

						// Capturing alert message.
						String alertMessage = webDriver.switchTo().alert().getText();
						if (alertMessage.isEmpty()) {
							Reporter.log("No message displays");

						} else {
							Reporter.log("Alert message displays as: " + alertMessage);

						}
						alert.accept();
					} else {
						verifyExists(APTIPAccessNoCPE.APTNoCPE.Interfacefield, "Interface search");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.Interfacefield, InterfaceName, "Interface send");

						waitForAjax();
						scrollDown(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox);
						click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");
						waitForAjax();

						scrollDown(APTIPAccessNoCPE.APTNoCPE.okbutton);
						verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
						click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
					}
					waitForAjax();
					scrollDown(APTIPAccessNoCPE.APTNoCPE.AddInterfaceSucessMessage);
					verifyExists(APTIPAccessNoCPE.APTNoCPE.AddInterfaceSucessMessage,
							"Interface Added success message");

					verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");
					click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");

					waitForAjax();

				} else {
					Reporter.log("Add Interface' page not navigated");

				}
			} else {
				Reporter.log("View PE Device' page not navigated");
			}
		} else {
			Reporter.log("No Device added in grid");
		}

	}

	public void addDropdownValues_commonMethod(String labelname, String xpath, String expectedValueToAdd)
			throws InterruptedException {
		boolean availability = false;
		List<String> ls = new ArrayList<String>();

		try {
			if (isVisible(xpath)) {
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
				if (expectedValueToAdd.equalsIgnoreCase("null")) {
					Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {
					webDriver.findElement(By.xpath("//div[label[text()='" + labelname + "']]//div[text()='�']"))
							.click();

					// verify list of values inside dropdown
					List<WebElement> listofvalues = webDriver
							.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

					for (WebElement valuetypes : listofvalues) {
						// Log.info("List of values : " + valuetypes.getText());
						ls.add(valuetypes.getText());
					}

					webDriver.findElement(By.xpath("//div[label[text()='" + labelname + "']]//input"))
							.sendKeys(expectedValueToAdd);
					// SendKeys(getwebelementNoWait("//div[label[text()='"+
					// labelname +"']]//input"), expectedValueToAdd);

					webDriver.findElement(By.xpath("(//div[label[text()='" + labelname + "']]//div[contains(text(),'"
							+ expectedValueToAdd + "')])[1]")).click();

				}
			} else {
				Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
				// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + "
				// is not displaying");
				// System.out.println(labelname + " is not displaying");
			}
		} catch (NoSuchElementException e) {
			// ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is
			// not displaying");
			System.out.println(labelname + " is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					" NOt able to perform selection under " + labelname + " dropdown");
			System.out.println(" NO value selected under " + labelname + " dropdown");
		}
	}

}
