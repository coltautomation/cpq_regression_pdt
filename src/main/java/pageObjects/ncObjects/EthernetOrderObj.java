package pageObjects.ncObjects;

public class EthernetOrderObj {
	
	public static class CompositOrders
	{
		
	// Existing order
		public static final String NameFiltering="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*";
		public static final String FilterSelectName="@xpath=//td/select[@class='inputs']";
		public static final String FilterInputValue="@xpath=//div/input[@class='inputs']";
		public static final String ApplyButton="@xpath=//*[@class='FilterButtons'] //*[text()='Apply']";
		public static final String arrorder = "//a/span[contains(text(),'";
		public static final String arrorder1 = "')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]";
		public static final String arrorder2 = "')]/parent::*";
		public static final String arrorder3 = "')]/parent::*/parent::*/parent::*/td//input";

	// add feature details 
		public static final String GeneralInformationTab="@xpath=//a[text()='General Information']";     // GeneralInformationTab
		public static final String AddFeaturelink="@xpath=//a[text()='Add Feature']";     // TypeofFeature
		public static final String TypeofFeature="@xpath=//a[text()='Add Feature']/following-sibling::*//textarea";
		public static final String EndSite="@xpath=//*[@id='nc_refsel_list_row_2']/div[1]";
				//div[@aria-label='End Site Product']
		public static final String Vlan="@xpath=//div[@aria-label='VLAN']";
				//*[@id='nc_refsel_list_row_5']/div[1]";
		public static final String SelectFeature="@xpath=//input[@value='Select']";  
		
	// Decompse order

		public static final String OrderTab="@xpath=//a[text()='Orders']";     
		public static final String Suborder="@xpath=//a/span[contains(text(),'New Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";     
		public static final String Decompose="@xpath=//a[text()='Decompose']";    
	
	//ProcessOrder
	
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";     
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";     
		public static final String StartProccessing="@xpath=//a[text()='Start Processing']";    
		
		public static final String arrOfStr1="@xpath=//a/span[contains(text(), '";
		public static final String arrOfStr2="')]"; 
		public static final String Str3="')]/parent::*/parent::*/parent::*/td//input"; 
		public static final String arr1="')]/parent::*"; 

		
	//CompleteOrder
		
		public static final String LinkforOrder="@xpath=//a[contains(text(),'Order #')]";
		public static final String TaskTab="@xpath=//a[text()='Tasks']";
		public static final String ExecutionFlowlink="@xpath=//a[text()='Execution Flow']/parent::*/following-sibling::*//a[2]";
		public static final String Workitems="@xpath=//a[text()='Work Items']";    
		public static final String TaskReadytoComplete="@xpath=//td[text()='Ready']/parent::*/td//a";    
		
		public static final String Errors="@xpath=//a[text()='Errors']";    
		
	//AddProduct
		
		public static final String AddonOrderTab="@xpath=//a[text()='Add']";   
		public static final String EthernetProductCheckBox="@xpath=//a[text()='Ethernet Connection Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String Addbutton="@xpath=//button[text()='Add']";

   //ProcessInflightOrder
   //addFeatureDetails 
		public static final String UnderlyningOrder="@xpath=//span[contains(text(),'Ethernet Connection')]/parent::a";  
		public static final String Edit="@xpath=//a[text()='Edit']"; 
		public static final String OrderSystemName="@xpath=//td/a[text()='Order System']/parent::*/following-sibling::*//select";  
		public static final String Orderreferencenumber="@xpath=//td/a[text()='Order System Service ID']/parent::*/following-sibling::*//input";
		public static final String Topology="@xpath=//td/a[text()='Topology']/parent::*/following-sibling::*//select"; 
		public static final String Ordernumber="@xpath=//td/a[text()='Order Number']/parent::*/following-sibling::*//input"; 
		public static final String CommercialProductName="@xpath=//td/a[text()='Commercial Product Name']/parent::*/following-sibling::*//input";
		public static final String CarnorProductId="@xpath=//td/a[text()='CARNOR Product ID']/parent::*/following-sibling::*//input";
		public static final String RetainedNCSerId="@xpath=//td/a[text()='Retained NC Service ID']/parent::*/following-sibling::*//input[@type='text']";
        public static final String CircuitCategory="@xpath=//td/a[text()='Circuit Category']/parent::*/following-sibling::*//select";
        public static final String ServiceBandwidth="@xpath=//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//input[@type='text']";
   //CancelStubTasks
        public static final String FailedTaskName="@xpath=(//td[text()='Resolved']/following-sibling::*)[6]";
        public static final String FailedTaskName1="@xpath=//a/span[contains(text(),'";
        public static final String FailedTaskName2="')]/parent::*/parent::*/parent::*/td//input";
        public static final String CancelTask="@xpath=//a[text()='Cancel']";


	// ProductDeviceDetails
		
        public static final String NewEndSiteProductAend="@xpath=//td[text()='Entering']/parent::*/td/a";
        public static final String ResilienceOption="@xpath=//td/a[text()='Resilience Option']/parent::*/following-sibling::*//select";
        public static final String AccessTechnolgy="@xpath=//td/a[text()='Access Technology']/parent::*/following-sibling::*//select";
        public static final String AccessType="@xpath=//td/a[text()='Access Type']/parent::*/following-sibling::*//select";
        public static final String SiteEnd="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*//select";
        public static final String SiteID="@xpath=//td/a[text()='Site ID']/parent::*/following-sibling::*//input";
        public static final String Update="@xpath=//a[text()='Update']";
        public static final String AccessPortLink="@xpath=//span[text()='Access Port']/parent::*";
        public static final String PresentConnectType="@xpath=//td/a[text()='Presentation Connector Type']/parent::*/following-sibling::*//select";
        public static final String AccessportRole="@xpath=//td/a[text()='Port Role']/parent::*/following-sibling::*//select";
        public static final String VlanTaggingMode="@xpath=//td/a[text()='VLAN Tagging Mode']/parent::*/following-sibling::*//select";
        public static final String AutoNegotiation="@xpath=//  ";
        public static final String CPElink="@xpath=//span[text()='CPE Information']/parent::*";
        public static final String CabinetID="@xpath=//td/a[text()='Cabinet ID']/parent::*/following-sibling::*//input";
        public static final String CabinetType="@xpath=//td/a[text()='Cabinet Type']/parent::*/following-sibling::*//select";
        public static final String VLANLink="@xpath=//span[text()='VLAN']/parent::*";
        public static final String Ethertype="@xpath=(//*[text()[contains(.,'Ethertype')]]/parent::*/following-sibling::*//input)[3]";
        public static final String VlanTagId="@xpath=//td/a[text()='VLAN Tag ID']/parent::*/following-sibling::*//input";
        public static final String NewEndSiteProductBend="@xpath=(//a/span[contains(text(),'New End Site Product')])[2]/parent::*";
        public static final String RetryTask="@xpath=//a[text()='Retry']";
		
        public static final String NewCompositeOrder="@xpath=//a[text()='New Composite Order']";
        public static final String OrderNumber="@xpath=//td[@id='vv_-1']/span";
        public static final String OrderDescription="@xpath=//*[@name='common_descr']";
        public static final String AccessPort="@xpath=//td/a[text()='Access Port']/parent::*/following-sibling::*//input[@type='text']";
	
        public static final String AccessNetworkElement="@xpath=//td/a[text()='Access Network Element']/parent::*/following-sibling::*//input[@type='text']";
        public static final String CPENNIPort="@xpath=//*[text()='CPE NNI Port 1']/parent::*/following-sibling::*//input[@type='text']";
        public static final String CPENNIPort2="@xpath=//*[text()='CPE NNI Port 2']/parent::*/following-sibling::*//input[@type='text']";
        public static final String AntSerialNo="@xpath=//td/a[text()='ANT Serial Number']/parent::*/following-sibling::*//input[@type='text']";
        public static final String GxLtsSerialNo="@xpath=//td/a[text()='GX/LTS Serial Number']/parent::*/following-sibling::*//input[@type='text']";
        
        public static final String Complete="@xpath=//a[@tooltip='Complete']";
        public static final String EndCheck="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*[1]";
        public static final String RaEndCheck="@xpath=//td[text()='Site End']/following-sibling::*/following-sibling::*";
		public static final String OrderDifferenceTab="@xpath=//a[text()='Order Difference']";
		public static final String ServiceInfoTab="@xpath=//a[text()='Service Information']";
	}
	


}
