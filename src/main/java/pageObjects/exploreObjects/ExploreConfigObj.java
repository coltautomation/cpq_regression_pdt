package pageObjects.exploreObjects;

import java.io.File;
import java.io.IOException;

import baseClasses.DataMiner;

public class ExploreConfigObj 
{
	
	public static class ExploreConfiguration
	 { 
		public static final String FibrePlanningQueueBtn="@xpath=//span[text()='Fibre Planning Work Queue']";
		public static final String OnqtWorkQueueBtn="@xpath=//span[text()='ONQT Work Queue']";
		public static final String ActionsBtn="@xpath=descendant::span[text()='Actions'][3]";
		public static final String AssignRequestToMeLnk="@xpath=descendant::tr[@aria-label='Assign Request to Me '][2]";
		public static final String AssignToMeLnk="@xpath=descendant::td[text()='Assign To Me'][2]";
		public static final String AssignSuccess="@xpath=//div[@class='dijitToasterContent']";
		public static final String CreateCostLnk="@xpath=//tr[@aria-label='Create Cost ']";
		public static final String RejectLnk="@xpath=descendant::tr[@aria-label='Reject '][2]";
		public static final String CommentExplore="@xpath=descendant::textarea[@name='comment'][4]";
		public static final String SubmitExplorePopUpBtn="@xpath=descendant::span[text()='Submit'][5]";
		//public static final String DigRejectCheckBox="@xpath=(//input[@value='REJECTED'])[1]";
		//public static final String DigRejectCheckBox="@xpath=//input[@name='primaryWidget' and @type='radio' and @value='REJECTED' and @aria-checked='false']";
		public static final String DigRejectCheckBox="@xpath=//*[@id='dijit_form_RadioButton_1']";
		public static final String NearNetcommentExplore="@xpath=descendant::textarea[@name='comment'][1]";

		//Explore Offnet Cost popup
		public static final String EditBtn="@xpath=//span[text()='Edit']";
		public static final String CarrierTxb="@xpath=//span[text()='Carrier']//preceding-sibling::input";
		public static final String NodeTxb="@xpath=//span[text()='Node']//preceding-sibling::input";
		public static final String MrcTxb="@xpath=descendant::span[text()='MRC']//preceding-sibling::input[1]";
		public static final String NrcTxb="@xpath=descendant::span[text()='NRC']//preceding-sibling::input[1]";
		public static final String TermsTxb="@xpath=//span[text()='Term (Years)']//preceding-sibling::input";
		public static final String CurrenctTxb="@xpath=//span[text()='Currency']//preceding-sibling::input";
		public static final String PriceTypeTxb="@xpath=//span[text()='Price Type']//preceding-sibling::input";
		public static final String ConnectorTxb="@xpath=//span[text()='Connector/Interface']//preceding-sibling::input";
		public static final String DualEntryFromOLOTxb="@xpath=//span[text()='Dual Entry from OLO']//preceding-sibling::input";
		public static final String CreateCostBtn="@xpath=//span[text()='Create Cost']";
		public static final String ApproveQuote="@xpath=//span[text()='Approve Quote']";
		public static final String SaveCostBtn="@xpath=//span[text()='Save Cost']";

		//Nearnet Cost popup
		public static final String DistanceFromColtTxb="@xpath=//input[@name='primaryWidgetdistFromNetwork']";
		public static final String QuoteValidityUpToMonthTxb="@xpath=(//label[text()='Quote Valid Up to (months)']/following::div/input)[2]";
		public static final String NearnetCurrencyTxb="@xpath=descendant::span[text()='currency']//preceding-sibling::input[1]";
		public static final String AddCostBtn="@xpath=descendant::span[text()='Add Cost'][1]";
		public static final String NearnetCostHeaderTable="@xpath=//table[@id='dgrid_6-header']";
		public static final String NearnetCostTable="@xpath=//div[contains(@id,'dgrid_6-row')]//child::table[@role='presentation']";
		public static final String ExploreSendToSalesBtn="@xpath=descendant::span[text()='Send to Sales'][1]";
		public static final String ExploreSaveBtn="@xpath=//span[@id='dijit_form_Button_30_label']";
		public static final String NearnetStatusElem="@xpath=//label[text()='Status']//following-sibling::span[@data-dojo-attach-point='statusNode']";

		//Nearnet Secondary Entries
		public static final String DistanceFromColtSecondaryTxb="@xpath=descendant::input[@name='secondaryWidgetdistFromNetwork']";
		public static final String QuoteValidityUpToMonthSecondaryTxb="@xpath=descendant::input[@name='secondaryWidgetopEx'][1]";
		public static final String NearnetCurrencySecondaryTxb="@xpath=descendant::span[text()='currency'][2]//preceding-sibling::input";
		public static final String DualEntryDigCostTab="@xpath=//span[text()='Dual Entry Dig Cost']";
		public static final String AddSecondaryCostBtn="@xpath=descendant::span[text()='Add Cost'][2]";
		public static final String NearnetCostHeaderSecondaryTable="@xpath=//table[@id='dgrid_7-header']";
		public static final String NearnetCostSecondaryTable="@xpath=//div[contains(@id,'dgrid_7-row')]//child::table[@role='presentation']";
		public static final String AddHubSecondaryCostBtn="@xpath=descendant::span[text()='Add Cost']";

		//Explore Logout
		public static final String MenuLnk="@xpath=//div[@class='inTopBar']//child::span[contains(@class,'menuButton')]";
		public static final String ExploreLogoutLnk="@xpath=//td[text()='Log Out']";
	 }

}
