package pageObjects.cpqObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadingAndWritingTextFile;

public class CPQQuoteApprovalsObj {
	
	public static class QuoteApprovals
	 { 
		//SE Approvals
		public static final String TechnicalApprovalLink="@xpath=//span[@class='oj-tabbar-item-label'][text()='Technical Approval']";
		public static final String TechnicalApprovalLinkString="//span[@class='oj-tabbar-item-label'][text()='Technical Approval']";
		public static final String SubmittoCSTAppvlBtn="@xpath=//span[@class='oj-button-text'][text()='Submit to CST Approval']";
	
		//CST Approvals
		public static final String ApproveBtn="@xpath=//span[@class='oj-button-text'][text()='Approve']";
		public static final String ApproveBtnElem="@xpath=//span[@class='oj-button-text'][text()='Approve']";
		
		//PS Approvals
		public static final String ApprovalLnk="@xpath=//a[text()='Approval']";
		public static final String SubmitToApprovalBtn="@xpath=//button[@name='submit_to_approval']";
		public static final String CPQSummaryErrorMsgElem="@xpath=//div[contains(@class,'message-summary message-error')]";
		public static final String CPQErrorMsgElem="@xpath=//div[contains(@class,'message-detail')]";
		
		public static final String Logoutbtn="@xpath=//img[@title='Log out']";
		public static final String proxyLogoutBtn="@xpath=//img[@title='Proxy Logout']";
		public static final String proxyLogoutRFSBtn="@xpath=//img[@alt='Proxy Logout']";
		public static final String cpqLogoutRFSBtn="@xpath=//img[@title='Log out']";
		
		public static final String lineItemGridTable="@xpath=//div[@aria-label='Line Item Table']";
		public static final String lineItemGridTableElm="@xpath=//table[@aria-label='Line Item Grid']";
		public static final String billingInfoPopup="@xpath=//div[@class='panel-col oj-flex-item oj-form popup_on_viewbcn oj-sm-labels-inline']";
		public static final String selectProductChbx="@id=sELECT_A-1";
		public static final String applyBtn="@name=apply";
		public static final String closeBtn="@xpath=//div[@class='panel-col oj-flex-item oj-form popup_on_viewbcn oj-sm-labels-inline']/descendant::button[@name='close']";
		
		public static final String seRevLnk="@xpath=//span[text()='SE Rev']";
		public static final String CSTRevLnk="@xpath=//span[text()='CST Rev']";
		public static final String defaultDateElem="@xpath=//a[contains(@class,'-selected')]";
		
	 }

}
