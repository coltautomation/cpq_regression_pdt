package pageObjects.siebelObjects;

public class SiebelAddProdcutObj {
	

	
	public static class product
	 { 
		public static final String ProductSelectionArrow="@xpath=//i[@class='fa fa-arrow-circle-o-right']";
		public static final String ProductName="@xpath=//div[@class='product-name']";
		public static String addProductPlusIcon="@xpath=(//div[@class='product-name'])[-10]/parent::li/button";
		
	
	 }
	
	public static class serviceOrder
	 { 
		public static final String ServiceOrderReferenceNo="@xpath=//a[contains(@name,'Order Num')]";
	
		
	
	 }
	
	//////////////////////MD/////////////////
	
	public static class SelectAttachmentTab
	 { 
		public static final String tabDropdown="@xpath=//select[@id='j_s_vctrl_div_tabScreen']";
		public static final String Attachmentsoption="@xpath=//select[@id='j_s_vctrl_div_tabScreen']//option[contains(text(),'Attachments')]";
		
		public static final String AttachmentTabSelection="@xpath=//div[@id='s_vctrl_div_tabScreen']//ul//li//a[text()='Attachments']";
		
	
	 }
	public static class UploadSOWTypeDocument
	 { 
	//	public static final String FileUpload="@xpath=//input[@name='s_SweFileName' and @title='Attachments:New File']";
	//	public static final String FileUpload="@xpath=//*[@class='siebui-label-btn siebui-file-label appletButton']//input";
		public static final String FileUpload="//*[@class='siebui-label-btn siebui-file-label appletButton']//input";
		
		public static final String DocumnetTypeOther="@xpath=//table[@summary='Attachments']//td[@title='Other']";
		public static final String DownArrow="@xpath=//span[@class='siebui-icon-dropdown']";
		//public static final String DocumentTypeCallBaringForm="@xpath=//ul[contains(@style,'block')]//li[text()='Call Barring Form']";
		public static final String DocumentTypeCallBaringForm="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Call Barring Form']";
		//public static final String DocumentTypeDisaster="@xpath=//ul[contains(@style,'block')]//li[text()='Disaster Recovery Documents']";
		public static final String DocumentTypeDisaster="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Disaster Recovery Documents']";
		//public static final String DoucmentTypeSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Filetype']";
		public static final String DoucmentTypeSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Filetype']";
	
	 } 
	 public static class SelectServiceGroupTab
	 { 
		    public static final String DropDown="@xpath=//*[@aria-label='Third Level View Bar']";
			public static final String ServiceGroupNew="@xpath=//button[@aria-label='Service Group:New']";
			public static final String serviceGrouplookup="@xpath=//span[@class='siebui-icon-pick']";
			public static final String ServiceGroupOk="@xpath=//button[contains(@title,'Pick Service Group:OK')]";
			public static final String ServiceGroupoption="@xpath=//select[@id='j_s_vctrl_div_tabScreen']//option[contains(text(),'Service Group')]";
			public static final String serviceGroupTab="@xpath=//a[text()='Service Group']";
		
	 }
	 
	 public static class EnterInstallationChargeInFooter
	 { 
		public static final String InstalltionDropdown="@xpath=//select[@aria-label='Third Level View Bar']";
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
		public static final String SaveOrderContinue="@xpath=//a[@class='colt-noedit-config-save']";
		public static final String PrimaryTestingMethod="@xpath=//input[@aria-label='Primary Testing Method']";
		public static final String Installationoption="@xpath=//select[@id='j_s_vctrl_div_tabScreen']//option[contains(text(),'Installation and Test')]";
		public static final String AlertAccept1="@xpath=//button[@id='colt-formalerts-ok-btn']";
		public static final String InstallationAndTestTab="@xpath=(//a[contains(text(),'Installation and Test')])[1]";
		public static final String InstallationTab="@xpath=a[text()='Installation and Test']";
	 }
	 
	 public static class CommercialValidation
	 { 
		public static final String OrderStatus="@xpath=//input[@aria-label='Order Status']";
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
		public static final String SaveOrderContinue="@xpath=//a[@class='colt-noedit-config-save']";
		public static final String PrimaryTestingMethod="@xpath=//input[@aria-label='Primary Testing Method']";	
		public static final String ServiceOrderSourceRefNumber="@xpath=//input[@aria-labelledby='COLT_Service_Order_Source_Ref_Label']";
	 } 
	 
	 public static class TechnicalValidation
	 { 
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
		public static final String SelectTechnicalValidation="@xpath=//div[text()='Technical Validation']";
		public static final String BlankPriceContinue="@xpath=//button[text()='Continue']";
		public static final String TriggerTRButton="@xpath=//button[text()='Trigger TR']";
		public static final String BDWREVW_CheckBox="@xpath=//div[@id='colt-workflows-selection-popup-content']//input";
		public static final String TriggerTRButtonCheckbox="@xpath=//input[@data-queue='BDWREVW']";
		
	 }
	 public static class CircuitReferenceGeneration
	 { 
		public static final String ClickLink="@xpath=//a[text()='Value']";
		public static final String CircuitReferenceAccess="@xpath=//button[@id='colt-circuitref-button']";
		public static final String CircuitReferenceValue="@xpath=//input[@class='colt-attribute-input siebui-ctrl-input colt-popup-link-ctrl']";
		public static final String TriggerTRButton="@xpath=//button[text()='Trigger TR']";
		public static final String ClickheretoSaveAccess="@xpath=(//a[contains(text(),'Click here to')])[1]";
		
		public static final String OtherCircuitReferenceValue="@xpath=//span[text()='Other Circuit Reference']/following-sibling::input";
		
		
	 } 
	 public static class DeliveryValidation
	 { 
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
		//public static final String SelectDeliveryValidation="@xpath=//li[text()='Delivery']" ;
		public static final String SelectDeliveryValidation="@xpath=//li/*[text()='Delivery']" ;

	 }
	 
	 public static class LaunchingCEOSApplication
	{ 
			//Login page elements
			public static final String userNameTxb="@id=username-id"; //Username
			public static final String passWordTxb="@id=pwd-id"; //Password
			public static final String authenticationTxb="@id=auth-id"; //Authentication
			public static final String loginBtn="@xpath=//input[@name='login']"; //Login Button	
			public static final String logoutBtn="@xpath=//div[@id='TBlogout']"; // Logout Button
			
			public static final String TBsearchsavechanges="@id=TBsearchsavechanges"; 
			public static final String CeosOrder="@xpath=//*[contains(@class,'sr')]"; 
			public static final String SupplierCircuitID="@xpath=//label[text()='Supplier Circuit ID']/following-sibling::textarea";
			public static final String SupplierProduct="@xpath=//label[text()='Supplier Product']/following-sibling::textarea";
			public static final String SLA_Tier_values="@xpath=//td[text()='value']/parent::*";
			public static final String Bandwidth="@xpath=//label[text()='Bandwidth']/following-sibling::a";
			public static final String Bandwidth1="@xpath=//td[contains(@class,'MenuEntryNameHover')]//td[@name='1 Gbps']/parent::*";
			public static final String Interface="@xpath=//label[text()='Interface']/following-sibling::a";
			public static final String Interface1="@xpath=//td[text()='OPTICAL']/parent::*";
			public static final String Connector="@xpath=//label[text()='Connector']/following-sibling::textarea";
			public static final String Connector1="@xpath=//td[text()='1310mn']/parent::*";
			public static final String OffnetAccessTechnology="@xpath=//label[text()='Offnet Access Technology']/following-sibling::a";
			public static final String OffnetAccessTechnology1="@xpath=//td[text()='ETHERNET over NGN']/parent::*";
			public static final String SupplierOrderingReference="@xpath=//label[text()='Supplier Ordering Reference']/following-sibling::textarea";
			public static final String OLOAdmin="@xpath=//label[text()='OLO Admin']/following-sibling::textarea";
			public static final String OrderingDate="@xpath=//label[text()='Ordering Date']/following-sibling::input";
			public static final String RequestedDate="@xpath=//label[text()='Requested Date']/following-sibling::input";
			public static final String SupplierConfirmationDate="@xpath=//label[text()='Supplier Confirmation Date']/following-sibling::input";
			public static final String ConfirmedDeliveryDate="@xpath=//label[text()='Confirmed Delivery Date']/following-sibling::input";
			public static final String Expecteddeliverydate="@xpath=//label[text()='Expected delivery date']/following-sibling::input";
			public static final String SupplierHandoverDate="@xpath=//label[text()='Supplier HandOver Date']/following-sibling::input";
			public static final String Tariff="@xpath=//label[text()='Tariff']/following-sibling::textarea";	
			public static final String Tariff1="@xpath=//td[text()='flat']/parent::*";	
			public static final String OLOUse="@xpath=//label[text()='OLO Use']/following-sibling::div/input";	
			public static final String OLOUse1="@xpath=//td[text()='Customer access ']/parent::*";	
			public static final String InstallCost="@xpath=//label[text()='Install Cost']/following-sibling::input";	
			public static final String RecurringCost="@xpath=//label[text()='Recurring Cost']/following-sibling::input";
			public static final String ContractPeriod="@xpath=//label[text()='Contract Period (Months)']/following-sibling::div/input";
			public static final String ContractEnds="@xpath=//label[text()='Contract Ends']/following-sibling::input";
			public static final String SupplierInvoiceStartDate="@xpath=//label[text()='Supplier Invoice Start Date']/following-sibling::input";
			public static final String ClickSave="@xpath=//div[text()='Save'][2]";
			public static final String InterconnectDetails="@xpath=//a[text()='Interconnect Details']";
			public static final String ICNotRequiredcheckbox="@xpath=//input[@type='checkbox'][3]";
			public static final String ICNotRequiredcheckbox1="@xpath=//label[text()='I/C Not Required']/following-sibling::*";
			public static final String ICNotReqConf="@xpath=//span[text()='Yes']";	
			
			
			//Newly added given by gokul
	        public static final String ActualDeliveryRfsDate="@xpath=//label[text()='Actual Delivery (RFS) Date']/following-sibling::input";
            public static final String OrderManagment="@xpath=//a[text()='Order Managment']";
            public static final String FailureParty="@xpath=//label[text()='Failure Party']/following-sibling::a";
            public static final String FailureReason="@xpath=//label[text()='Failure Reason']/following-sibling::a";
			public static final String OrderManagement="@xpath=//a[text()='Order Managment']";
			public static final String TBnewsearch="@xpath=//div[@id='TBnewsearch']";
			public static final String AcceptUser="@xpath=//button[text()='OK']"; 
			
			public static final String AcceptUser2="@xpath=//a[text()='Cancel']";
			public static final String InterfaceFinal="@xpath=//label[text()='Interface']/following-sibling::textarea";
			public static final String Currency="@xpath=//label[text()='Currency']/following-sibling::a";
			public static final String RecurringCostBillCycle="@xpath=//label[text()='Recurring Cost Bill Cycle']/following-sibling::div/input";
	}
	
	 public static class getReferenceNo
	 { 
		public static final String Sites="@xpath=//a[text()='Sites']";
			
	 }
	
	
	 public static class GetReference
	 { 
		 
		public static final String GetReference="@xpath=//button[text()='Get Reference']";
		
	
	 }
	 
	 public static class Save
	 { 
		 
		public static final String Save="@xpath=//div[@id='colt-s-order-connection-header']//a[text()='Save']";
			
	 }
	 
	 public static class ServiceOrderReferenceNo2
	 { 
		 
		public static final String ServiceOrderReferenceNo2="@xpath=(//a[contains(@name,'Order Num')])[2]";
			
	 }

	 public static class NetworkReferenceFill
	 { 		
		public static final String NetworkReferenceInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String NetworkReferenceSearchin="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Network Reference:Go']";
		public static final String NetworkReferenceGo="@xpath=//button[@aria-label='Pick Network Reference:OK']";
		public static final String NetworkReferenceSearch="@xpath=//input[@aria-labelledby='COLT_Network_Reference_Label']/..//span[@aria-label='Selection Field']";

					
	 } 
	 
	 public static class ServiceChargeforIPVPNSite
	 { 		
		public static final String ExpandAllButton="@xpath=//button[@title='Service Charges:Expand All']";
		public static final String BillingLastRow="@xpath=//table[@summary='Service Charges']/parent::*//tr[last()]";
		public static final String BillingRow="@xpath=//table[@summary='Service Charges']/parent::*//tr";
		public static final String BillingRowAmount="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5])[Value]";
		public static final String BillingRowAmountInput="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5]//input";
		public static final String BillingRowBCN="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[6])[Value]";
		public static final String BCNSearchClick="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[6]/span";
		public static final String BCNInstallationChargeNRCInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String BCNNRCSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Billing Profile:Go']";
		public static final String FirstLineitem="@xpath=//table[@summary='Service Charges']/parent::*//tr)[2]";
		public static final String ClickNextPage="@xpath=//span[@title='Next record set'][1]";
		
	 } 
	 
	 public static class OperationalAttributesforIPVPN
	 { 		
		public static final String ClickLink="@xpath=//a[text()='Value']";
		public static final String RouterSpecificationSetting="@xpath=//span[contains(@class,'colt-oa-btn colt-oa-std-btn colt-oa-Primary-Child')][2]";
		public static final String OperationalAttribueCount="@xpath=//table[@summary='Operational Attributes']//tr[contains(@class,'mandatory')]//td[3]";
		public static final String BillingRowAmount="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5])[Value]";
		public static final String BillingRowAmountInput="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5]//input";
		public static final String BillingRowBCN="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[6])[Value]";
		public static final String BCNSearchClick="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[6]/span";
		public static final String BCNInstallationChargeNRCInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String BCNNRCSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Billing Profile:Go']";
		public static final String FirstLineitem="@xpath=//table[@summary='Service Charges']/parent::*//tr)[2]";
		public static final String ClickNextPage="@xpath=//span[@title='Next record set'][1]";
		
	 }
	 
	 ///////////////////////////KK
	 public static class OperationAttribute
	 { 
		public static final String Voiceconfigtab="@xpath=//a[text()='Voice Config']";
		public static final String OperationAttribute="@xpath=//span[@class='colt-oa-btn colt-oa-std-btn']";
		public static final String AttributeNew="@xpath=//button[@aria-label ='Operational Attributes:New']";
		public static final String AttributeNameDropDown="@xpath=//span[@class='siebui-icon-dropdown']";
		//public static final String AttributeName="@xpath=//li[text()='Scenario']";
		public static final String AttributeName="@xpath=//li/*[text()='Scenario']";
		public static final String OperationAttributeSubmit="@xpath=//button[@title='Operational Attributes:OK']";
		public static final String OtherTab="@xpath=//a[text()='Other']";
		public static final String OperationAttribute1="@xpath=(//span[@class='colt-oa-btn colt-oa-std-btn'])[2]";
		public static final String AttributeValue1="@xpath=//input[@name='COLT_Attribute_Value']";
		public static final String OperationAttribute2="@xpath=(//span[@class='colt-oa-btn colt-oa-std-btn'])[3]";
		public static final String AttributeValueDropDown="@xpath=//span[@class='siebui-icon-dropdown']";
		//public static final String AttributeValue="@xpath=//li[text()='Scenario 1']";
		public static final String AttributeValue="@xpath=//li/*[text()='Scenario 1']";

	 }
	
	public static class OrderCompleteEthernetHubSpoke
	 {
		public static final String DropDown="@xpath=//*[@aria-label='Third Level View Bar']";
		public static final String PrimaryTestingMethod="@xpath=//input[@aria-label='Primary Testing Method']";
		public static final String ManualValidation="@xpath=(//button[@aria-label='Service Orders:Manual Validation'])[1]";
		public static final String OrderStatus="@xpath=//input[@aria-label='Order Status']";
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
		//public static final String SelectCompleted="@xpath=//li[text()='Completed']";
		public static final String SelectCompleted="@xpath=//li/*[text()='Completed']";
		public static final String OrderComplete="@xpath=//button[text()='Yes']";	
		
	 }
	
	public static class validateSlaMatrix
	 {
		public static final String InstalltionDropdown="@xpath=//select[@aria-label='Third Level View Bar']";	
		public static final String EntitlementsTab="@xpath=//a[text()='Entitlements']";	
		public static final String SlaTag="@xpath=(//a[@name='Entitlement Name'])[1]";	
			
		//public static final String AssetButton="@xpath=//span[@title='Assets']";
		public static final String AssetButton="@xpath=//a[text()='Assets']";
		
		public static final String AlertAccept="@xpath=//span[text()='OK']";	
		public static final String ServiceOrderOM="@xpath=//input[@aria-label='Service ID OM']";	
		public static final String GoButton="@xpath=//button[@title='Search:Go']";	
		public static final String AssetNumber="@xpath=//a[@name='Asset Number']";	

		public static final String GoToAssetBtn="@xpath=//button[@data-display='Go To Asset']";	

		public static final String Entitlementsoption="@xpath=//select[@id='j_s_vctrl_div_tabScreen']//option[contains(text(),'Entitlements')]";

		
	 }
	
	public static class Carnor
	 {
		
		public static final String CarReason="@xpath=//input[@aria-labelledby='COLT_Customer_Order_For_Code_Label']";	
		//	public static final String CarNorRunFlag="@xpath=(//span[@class='siebui-icon-dropdown applet-form-combo applet-list-combo'])[7]";	
			public static final String CarNorRunFlag="@xpath=//input[@aria-labelledby='COLT_Parallel_Run_Flag_Label']";	
			
			
			//public static final String CarNorRunFlagValue="@xpath=//li[text()='Yes']";	
			public static final String CarNorRunFlagValue="@xpath=//li/*[text()='Yes']";	

			public static final String ExistingCapacityLeadTimePrimary="@xpath=//input[@aria-labelledby='COLT_Existing_Capacity_Lead_Time_Code_Label']";	
			public static final String CarNorButton="@xpath=//span[text()='CAR-NOR']";	
			public static final String carnorOrderReferencevalue="@xpath=//input[@aria-labelledby='COLTT_Service_Order_Label']";	
			public static final String Carnororder="@xpath=(//span[@class='colt-s-order-infobar-ctrl-text'])[1]";	
	
	 }
	
	/*public static class ServiceChargeforIPVPNSite
	 {
		public static final String ExpandAllButton="@xpath=//button[@title='Service Charges:Expand All']";	
		//public static final String BillingLastRow="@xpath=//table[@summary='Service Charges']/parent:://tr[last()]";	
		//public static final String BillingRow="@xpath=//table[@summary='Service Charges']/parent:://tr";	
		//public static final String BillingRowAmount="@xpath=(//table[@summary='Service Charges']/parent:://tr[@tabindex='-1']//td[5])[Value]";	
		//public static final String BillingRowAmountInput="@xpath=//table[@summary='Service Charges']/parent:://tr[@tabindex='-1']//td[5]//input";	
		//public static final String BillingRowBCN="@xpath=(//table[@summary='Service Charges']/parent:://tr[@tabindex='-1']//td[6])[Value]";	
		//public static final String BCNSearchClick="@xpath=//table[@summary='Service Charges']/parent:://tr[@tabindex='-1']//td[6]/span";	
		//public static final String BCNInstallationChargeNRCInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";	
		//public static final String BCNNRCSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Billing Profile:Go']";	
	//	public static final String FirstLineitem="@xpath=(//table[@summary='Service Charges']/parent:://tr)[2]";	
	//	public static final String ClickNextPage="@xpath=(//span[@title='Next record set'])[1]";	
	//}*/
	
	public static class ColtPromissDate
	 {
		public static final String ColtPromissday="@xpath=//input[@aria-labelledby='Colt_Promised_Date_Label']";	
		
	 }
	
	public static class ModTechModCommWaveAndLine
	 {
		public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";	
		public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";	
		//public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String SiteABSelection="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
		public static final String SiteAInput="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";	
		public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";	
		public static final String SiteBInput="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";	
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";	
		//public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";
		public static final String DropdownClick="@xpath=//span[text()='Value']/following-sibling::span";	
		//public static final String DropDownValue="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";	
		public static final String DropDownValue="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";
	 }
	
	public static class CompletedValidation_offnet
	 {
		public static final String ClickLink="@xpath=//a[text()='Value']";	
		public static final String CustOrder="@xpath=//input[@aria-labelledby='Order_Number_Label']";	
		public static final String CustOrderGo="@xpath=//button[contains(@class,'gotoview')]";	
		public static final String ClickSeibelOrder="@xpath=//a[@name='Order Number']";	
		public static final String NewServiceOrder="@xpath=//*[(@aria-label='Line Items:New Service Order')]";	
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";	
		//public static final String SelectCompleted="@xpath=//li[text()='Completed']";	
		public static final String SelectCompleted="@xpath=//li/*[text()='Completed']";	
		public static final String AlertAccept="@xpath=//span[text()='Ok']";	
		public static final String OrderComplete="@xpath=//button[text()='Yes']";	
		public static final String SubnetworkPopUP="@xpath=//button[@class='colt-primary-btn']";	
		public static final String OrderStatusInput="@xpath=//input[@aria-label='Order Status']";	
		public static final String RouterSpecificationSetting="@xpath=(//span[contains(@class,'colt-oa-btn colt-oa-std-btn colt-oa-Primary-Child')])[2]";	
		public static final String OperationalAttribueCount="@xpath=(//table[@summary='Operational Attributes']//tr[contains(@class,'mandatory')]//td[3])";	
		public static final String OperationalAttribueClick="@xpath=(//table[@summary='Operational Attributes']//tr[contains(@class,'mandatory')]//td[3])[index]";	
		public static final String OperationalAttributeText="@xpath=//table[@summary='Operational Attributes']//tr[contains(@class,'mandatory')]//td[3]/input";	
		public static final String OperationalAttributeOK="@xpath=//button[@aria-label='Operational Attributes:OK']";		
		
	 }
	//////////////////////////0306
	
	 public static class CompletedValidation
	 { 
		 
		public static final String CustOrder="@xpath=//input[@aria-labelledby='Order_Number_Label']";
		public static final String ClickLink="@xpath=//a[text()='Value']";
		public static final String CustOrderGo="@xpath=//button[contains(@class,'gotoview')]";
		public static final String ClickSeibelOrder="@xpath=//a[@name='Order Number']";
		public static final String NewServiceOrder="@xpath=//*[(@aria-label='Line Items:New Service Order')]";
		public static final String SubnetworkPopUP="@xpath=//button[@class='colt-primary-btn']";
		public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
		//public static final String SelectCompleted="@xpath=//li[text()='Completed']"; 
		public static final String SelectCompleted="@xpath=//li/*[text()='Completed']"; 
		public static final String OrderComplete="@xpath=//button[text()='Yes']";
		public static final String AlertAccept="@xpath=//span[text()='Ok']";
		public static final String OrderStatusInput="@xpath=//input[@aria-label='Order Status']";	
		public static final String AlertAccept2="@xpath=//button[text()='Ok']";
		
	 }
	 
	 public static class MovetoIPService
	 {
		 public static final String ClickLink="@xpath=//a[text()='Value']";
		 public static final String NetworkService="@xpath=//td[@title='IP VPN Service']";
		 public static final String GOIPService="@xpath=//a[text()='Value']";


	 }
	 public static class BillingStatus
	 { 
		 
		public static final String ServiceOrderTable ="@xpath=(//table[@class='ui-jqgrid-btable'])[1]";
		public static final String BillingStatus ="@xpath=(//table[@summary='Service Order List'])[1]/tbody/tr[2]/td[9]";
		
		
		
	 }
	
}
