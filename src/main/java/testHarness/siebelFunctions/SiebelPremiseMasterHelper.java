package testHarness.siebelFunctions;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import com.relevantcodes.extentreports.LogStatus;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelHubSiteObj;
import pageObjects.siebelObjects.SiebelModeObj;
import pageObjects.siebelObjects.SiebelPremiseMasterHelperObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

import java.util.Random;

public class SiebelPremiseMasterHelper extends SeleniumUtils
{
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();
	Random rnd =new Random();
	WebElement el;
	String temp;
	String BuildingID=null;
	
	private char RandomChar()
	{
		String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		char block = abc.charAt(rnd.nextInt(abc.length()));
		return block;
	}
	
	private String RandomName()
	{
		String[] BuildNames= {"TestBuilding","Kenwood","RiverLine","CrossRoad","WalkStreet","TechWorld","WindBall"};
		int index=rnd.nextInt(BuildNames.length);
		return BuildNames[index];
	}
	
	private String  HouseNoGeneration ()
	{
		int HNo=rnd.nextInt(1000); 
		String temp=  String.valueOf(RandomChar())+"-"+String.valueOf(HNo);
		System.out.println("Flat Number Genrated : "+ temp);
		return temp;
	}
	
	private String  BuildingGeneration ()
	{
		int flatNo=rnd.nextInt(1000); 
		temp=  RandomName()+"-"+String.valueOf(RandomChar())+"-"+String.valueOf(flatNo);
		System.out.println("Building Name Genrated : "+ temp);
		return temp;
	}
	
	public void ClickIcon() throws Exception
	{
		Reusable.waitForElementToAppear(SiebelPremiseMasterHelperObj.PremiseMasterHelper.image,10);
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.image);
		
	}
	
	public void ClickTopMeu(String menuItem ) throws Exception
	{
		
	}

	public void LaunchingPremiseMasterApplication(String testDataFile, String sheetName, String scriptNo, String dataSetNo,String username, String password) throws Exception 
	{

////Open PremiseMaster URL and perform login action

//		openBrowser("firefox");
		openurl(Configuration.Premise_URL);
		Reusable.waitForSiebelLoader();
		
		waitForElementToAppear(SiebelPremiseMasterHelperObj.PremiseMasterHelper.username, 5);
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.username,"User Name textbox");
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.username, username,"Enter User Name");
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.password,"Password textbox");
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.password, password,"Password textbox field");
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.login,"Login button");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.login,"Login button");
		Reusable.waitForSiebelLoader();
		
//		verifyExists(SiebelAddProdcutObj.LaunchingCEOSApplication.loginBtn,"Login button");
//		click(SiebelAddProdcutObj.LaunchingCEOSApplication.loginBtn,"Login button");
//		Reusable.waitForSiebelLoader();
	}
	
	public void SearchSite(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premises");
		String BuildingID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Building_ID");
		
		// Streetname selection
		verifyExists(SiebelModeObj.addSiteADetailsObj.StreetNamerfs,"Enter Street Name");
		sendKeys(SiebelModeObj.addSiteADetailsObj.StreetNamerfs,StreetName, "Enter Street Name");	

		//Country selection 
		verifyExists(SiebelHubSiteObj.Customize.Country,"County");
		click(SiebelHubSiteObj.Customize.Country,"County");
		verifyExists(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Country),"Country Value");
		click(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Country),"Country Value");

		//Enter City
		verifyExists(SiebelHubSiteObj.Customize.City,"City");
		sendKeys(SiebelHubSiteObj.Customize.City, City, "City");
		Reusable.waitForSiebelLoader();

		//Postal Code
		verifyExists(SiebelHubSiteObj.Customize.PostalCode,"Postal Code");
		click(SiebelHubSiteObj.Customize.PostalCode," Click PostalCode");
		sendKeys(SiebelHubSiteObj.Customize.PostalCode, PostalCode, "PostalCode Value");
		Reusable.waitForSiebelLoader();

		//House Number
		verifyExists(SiebelHubSiteObj.Customize.Premises,"Premises");
		sendKeys(SiebelHubSiteObj.Customize.Premises, Premises, "Premises");
		Reusable.waitForSiebelLoader();
		
		if(Premises.toString().equalsIgnoreCase(null)||Premises.toString().equalsIgnoreCase(""))
		{
			temp=HouseNoGeneration();
			Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.houseNo, temp);
		}
		else
		{
			temp=Premises.toString()+"-"+ String.valueOf(rnd.nextInt(1000));
			Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.houseNo, temp);
		}
		Reusable.waitForSiebelLoader();
		
		//Building  Name
		if(BuildingID.toString().equalsIgnoreCase(null)||BuildingID.toString().equalsIgnoreCase(""))
		{
			temp=HouseNoGeneration();
			Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchlocationBuildingName, temp);
		}
		else
		{
			temp=BuildingID.toString()+"-"+ String.valueOf(rnd.nextInt(1000));
			Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchlocationBuildingName, temp);
		}
		Reusable.waitForSiebelLoader();	
		
		//Street Entry
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchlocationStreetName, StreetName);
		Reusable.waitForSiebelLoader();
		
		//Postal Entry	
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.postalCode, PostalCode);
		Reusable.waitForSiebelLoader();
		
		//State Entry	
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.state, BuildingID);
		Reusable.waitForSiebelLoader();
	}
	
	public HashMap<String, String> AddSiteAndBuilding(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
	{
		
		HashMap<String, String> SiteValues=new HashMap<>();
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
		String BuildingID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_ID");
		String BuildingFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing Address");

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"),StreetName);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Country"));
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropdown.replace("Value", "Country"));
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteABSelection.replace("Value", Country),"Country");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteABSelection.replace("Value", Country),"Country");
//		verifyExists(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Aend_CountryValue),"Country Value");
//		click(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Aend_CountryValue),"Country Value");

		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"),City);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"),PostalCode);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"),Premises);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchButton, "Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchButton, "Search");
		Reusable.waitForSiebelLoader();

		if(isElementPresent(By.xpath("//i[@class='fa fa-search']/parent::button")))
		{
			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addressNotFound, "Address Not Found? Search Address Doctor");
			click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addressNotFound, "Address Not Found? Search Address Doctor");
			Reusable.waitForSiebelLoader();
		}
		waitToPageLoad();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addAddress, "Address Not Found? Create a new Address");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addAddress, " Address Not Found? Create a new Address");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress, "Save Address");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress, "Save Address");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		
//		if(isVisible(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addBuilding))
////		if(BuildingFlag.toString().equalsIgnoreCase("Yes"))
//		{
//			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addBuilding, "Building Not Found? Create a new Building");
//			click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addBuilding, "Building Not Found? Create a new Building");
//			Reusable.waitForSiebelLoader();
//		}

		if(waitForElementToBeVisible("@xpath=//button[text()='Building Not Found? Create a new Building']",60))
		{
			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addBuilding, "Building Not Found? Create a new Building");
			click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addBuilding, "Building Not Found? Create a new Building");
			Reusable.waitForSiebelLoader();
		}

		//Fill Building Details
		
		//Building  Name
		if(BuildingID.equals(null)||BuildingID.equals("")) 
		{
			temp=BuildingGeneration();
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Building Name Generated >> "+temp);
		}
		else
		{
			temp=BuildingID+"-"+ String.valueOf(rnd.nextInt(1000));
		}
		SiteValues.put("BuildingName", temp);
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingName, temp);
		Reusable.waitForSiebelLoader();
		String TempBuilding = getAttributeFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingName, "value");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Building_ID", TempBuilding);	

		//House Number
		if(Premises.equals(null)||Premises.equals("")) 
		{
			temp=HouseNoGeneration();
		}
		else
		{
			temp=Premises+"-"+ String.valueOf(rnd.nextInt(1000));
		}
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.roomName, temp);
		Reusable.waitForSiebelLoader();
		SiteValues.put("Room", temp);
		
		//Floor 
		int floor =rnd.nextInt((20 - 1) + 1) + 1;
		System.out.println(String.valueOf(floor));
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.floor, "Floor");
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.floor, String.valueOf(floor));
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select Floor >> "+String.valueOf(floor));
		Reusable.waitForSiebelLoader();
		SiteValues.put("Floor", temp);
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress, "Click on Save New Building & Site");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress, "Click on Save New Building & Site");
		waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		waitToPageLoad();
		
		if (isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) 
		{
			verifyExists(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"verify Save order changes");
			click(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"Click on Save order Changes");
			Reusable.waitForSiebelLoader();
			System.out.println("page load succesfuuly now come to middle applet");
			Reusable.waitForSiebelLoader();
		}

		return SiteValues;
		
	}
	
	//NewWith Hub And Spoke
	public HashMap<String, String> AddSiteAndBuildingNew(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		HashMap<String, String> SiteValues=new HashMap<>();
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premises");
		String BuildingID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Building_ID");

		if(ProdType.equals("IP Voice Solutions")|| ProdType.equals("IP Voice Solutions"))
		{
			return SiteValues;
		}
		else
		{
			Reusable.waitForElementToAppear(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"),10);
			if(StreetName.equals(null)||StreetName.equals(""))
		{
			
		}
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"),StreetName);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Country"));
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropdown.replace("Value", "Country"));
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteABSelection.replace("Value", Country));
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"));
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"),City);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"));
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"),PostalCode);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"));
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"),Premises);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchButton, "Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchButton);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addressNotFound, "Click on Address Not Found? Search Address Doctor");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addressNotFound);
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addAddress, "Click on Address Not Found? Create a new Address");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addAddress);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress, "Save Address");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addAddress, "Click on Building Not Found? Create a new Building");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.addAddress);
		Reusable.waitForSiebelLoader();
		
		
		//Fill Building Details
		
		//Building  Name
				if(BuildingID.equals(null)||BuildingID.equals("")) 
				{
					temp=BuildingGeneration();
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Building Name Generated >> "+temp);
				}
				else
				{
					temp=BuildingID+"-"+ String.valueOf(rnd.nextInt(1000));
				}
				SiteValues.put("BuildingName", temp);
				Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingName, temp);
				Reusable.waitForSiebelLoader();
		
				//House Number
				if(Premises.equals(null)||Premises.equals("")) 
				{
					temp=HouseNoGeneration();
				}
				else
				{
					temp=Premises+"-"+ String.valueOf(rnd.nextInt(1000));
				}
				Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.roomName, temp);
				Reusable.waitForSiebelLoader();
				SiteValues.put("Room", temp);
				
				//Floor 
				int floor =rnd.nextInt((20 - 1) + 1) + 1;
				System.out.println(String.valueOf(floor));
				verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.floor, "Floor");
				Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.floor, String.valueOf(floor));
				Reusable.waitForSiebelLoader();
				SiteValues.put("Floor", temp);
				
				verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress, "Save New Building & Site");
				click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.saveAddress);
				Reusable.waitForSiebelLoader();
				waitToPageLoad();
				waitForAjax();
				return SiteValues;
		}	
	}

	public void SearchSiteHubSpoke(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		if(ProdType.equalsIgnoreCase("Ethernet Spoke"))
		{
			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.hubSiteB, "Search Site B");
			click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.hubSiteB, "Search Site B");
		}
//		else if (ProdType.equalsIgnoreCase("Ethernet Hub"))
//		{
//			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.hubSiteA, "Search Site A");
//			click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.hubSiteA, "Search Site A");
//		}
		else
		{
			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.hubSiteA, "Search Site A");
			click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.hubSiteA, "Search Site A");
		}

		Reusable.waitForSiebelLoader();
	}
	
	
	public void ClickSearch() throws InterruptedException, IOException
	{
		String attribute=null;
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchLocationSearchButton, "Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchLocationSearchButton, "Search");
		int loop=1;
		do
		{
			waitForAjax();
			Reusable.mouseMoveOn(SiebelPremiseMasterHelperObj.PremiseMasterHelper.state);
			waitToPageLoad();
			loop++;
			if(getAttributeFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchAgain, "value").equalsIgnoreCase("Submit"))
				break;
			else
			{
				click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchAgain);
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on Search Again");
			}
			attribute=getAttributeFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchAgain, "value");
		}while(!attribute.equalsIgnoreCase("Submit") && loop<5);
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Attribute Verified : "+attribute);
		
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchAgain);
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on Submit button ");
	}
	public void CreateSite() throws InterruptedException, IOException
	{
		waitToPageLoad();
		Reusable.waitForElementToAppear(SiebelPremiseMasterHelperObj.PremiseMasterHelper.createSite, 10);
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.createSite);
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on Create Site Link ");
	}
	public void EnterSiteDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		HashMap<String, String> SiteValues=new HashMap<>();
		String BuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BuildingType");
		String BuildingStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Status");
		String BuildingAccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Access_Type");
		String BuildingCategory = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Category");
		String Schema = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CNMT Schema");
		String ExistingStreet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing Street");
		String SettlementName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CNMT Settlemet name");
		String SiteType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Type");
		String CityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Main_City_Code");
		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OCN");
		String Status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Status");
		
		// Building Type
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingType, "Building Type");
		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingType, BuildingType);
		Reusable.waitForSiebelLoader();
		
		// Status 
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingStatus, "Building Status");
		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingStatus, BuildingStatus);
		Reusable.waitForSiebelLoader();
		
		//Access Type
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.accessType, "Building Access Type");
		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.accessType, BuildingAccessType);
		Reusable.waitForSiebelLoader();
		
		//Building Category
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingCategory, "Building Cateogory");
		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingCategory, BuildingCategory);
		Reusable.waitForSiebelLoader();
		
		if(BuildingType.equalsIgnoreCase("On Net"))
		{   
			//Schema
			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.schema, "CNMT Schema");
			select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.schema, Schema);
			Reusable.waitForSiebelLoader();
			
			//Existing Street
			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.streetName, "CNMT Street Name");
			select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.streetName, ExistingStreet);
			Reusable.waitForSiebelLoader();
			
			//CNMT Settlement
			Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.settlementName, SettlementName);
			Reusable.waitForSiebelLoader();
		}
		
		//Site Type
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteType, "Site Type ID");
		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteType, SiteType);
		Reusable.waitForSiebelLoader();
		
		//Floor 
		int floor =rnd.nextInt((20 - 1) + 1) + 1;
		System.out.println(String.valueOf(floor));
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.floorCreateSite, "Floor");
		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.floorCreateSite, String.valueOf(floor));
		Reusable.waitForSiebelLoader();
		
		if(BuildingType.equalsIgnoreCase("On Net"))
		{
		//Man City Code
			verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.cityId, "City Code");
			select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.cityId, CityCode);
			Reusable.waitForSiebelLoader();
		
			//OCN
			Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteOCN, OCN);
			Reusable.waitForSiebelLoader();
			
			//AccessID
			/*System.out.println(InputData[32].toString());
			WaitforElementtobeclickable(xml.getlocator("//locators/CreateSite/AccessID"));		
			Select(getwebelement(xml.getlocator("//locators/CreateSite/AccessID")), InputData[34].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select CityID >> "+InputData[34].toString());
			waitforPagetobeenable();*/
		}
		//Status 
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Status");
		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, Status);
		Reusable.waitForSiebelLoader();
	//	}
		int Room =rnd.nextInt((50 - 1) + 1) + 1;
		System.out.println(String.valueOf(Room));
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.roomId, "Room ID");
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.roomId, String.valueOf(Room));
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.submit, "Submit");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.submit);
		
		waitToPageLoad();
		BuildingID= getTextFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingId);
	}
	
	public void SearchSiteAndVerified(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_ID");

		waitToPageLoad();
		ClickTopMeu("Search");
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		Reusable.waitForSiebelLoader();

		waitToPageLoad();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Select Search Type >> Site");
//		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Dropdown Search type");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteValue, "Site");
//		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropDown.replace("Value", "Site"));
		Reusable.waitForSiebelLoader();
		
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchValue, SiteID);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Click On Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Click On Search");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		
		String SiteId=getTextFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteResultId);
//		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise_Site_ID", SiteId);
		assertTrue(SiteID.equalsIgnoreCase(SiteId), "Search ID : "+BuildingID+"!="+SiteId);
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteResultId, "Site ID");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteResultId, "Site ID");
		Reusable.waitForSiebelLoader();
		
		waitToPageLoad();

//		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteBuildingId, "Building ID");
//		String BuildId=getAttributeFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteBuildingId,"value");

		String BuildingID = getAttributeFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteBuildingId, "value");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise_Building_ID", BuildingID);	

//		return BuildId;
	}
	
	public void SearchBuildingAndEdit(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String BuildingID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_ID");
		String BuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BuildingType");
		String BuildingStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Status");
		String BuildingAccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Access_Type");
		String BuildingCategory = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Category");
		
		waitToPageLoad();
		ClickTopMeu("Search");
		
		waitToPageLoad();
				
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		Reusable.waitForSiebelLoader();

		waitToPageLoad();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Select Search Type >> Building");
//		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Dropdown Search type");
//		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteValue, "Building");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropDown.replace("Value", "Building"));
		Reusable.waitForSiebelLoader();
		
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchValue, BuildingID);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Search");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		
		//Edit Building
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editBuilding, "Edit Link");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editBuilding, "Edit Link");
		Reusable.waitForSiebelLoader();

		waitToPageLoad();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		
//		//Updating Building Type
//		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingType, "Building Type");
//		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingType, BuildingType);
//		Reusable.waitForSiebelLoader();
//		
//		// Status 
//		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingStatus, "Building Status");
//		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingStatus, BuildingStatus);
//		Reusable.waitForSiebelLoader();
//		
//		//Access Type
//		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.accessType, "Building Access Type");
//		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.accessType, BuildingAccessType);
//		Reusable.waitForSiebelLoader();
		
		//Building Category
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingCategory, "Building Cateogory");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropDown.replace("Value", BuildingCategory));
//		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingCategory, BuildingCategory);
		Reusable.waitForSiebelLoader();

		String SiteId = getTextFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.updateSiteId).trim();
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise_Site_ID", SiteId);	

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.submit, "Submit");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.submit);
		Reusable.waitForSiebelLoader();
		
		waitToPageLoad();
		
		String Message = getTextFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.buildingUpdateMessage);
		Message=Message.toUpperCase();
		System.out.println("Building Updated Message : "+Message);
//		Assert.assertTrue(Message.contains("BUILDING UPDATED SUCCESSFULLY WITH ID")&& Message.contains(BulidingID), "Error in Building Updation");
	
	}
	
	public void SearchSiteAndEdit(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		
		String SiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premise_Site_ID");
		String SiteType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Type");
		String CityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Main_City_Code");
		String Status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Status");

		waitToPageLoad();
		ClickTopMeu("Search");
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		Reusable.waitForSiebelLoader();

		waitToPageLoad();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Select Search Type >> Site");
//		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Dropdown Search type");
//		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteValue, "Site");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropDown.replace("Value", "Site"));
		Reusable.waitForSiebelLoader();
		
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchValue, SiteID);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Click On Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Click On Search");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editBuilding, "Edit Link");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editBuilding, "Edit Link");
		Reusable.waitForSiebelLoader();

		waitToPageLoad();

		//Site Type
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteType, "Site Type");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropDown.replace("Value", SiteType));
//		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteType, SiteType);
		Reusable.waitForSiebelLoader();

		//Status 
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.status, "Status");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropDown.replace("Value", Status));
//		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, Status);
		Reusable.waitForSiebelLoader();

		//Man City Code
//		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.cityId, "City Code");
//		select(SiebelPremiseMasterHelperObj.PremiseMasterHelper.cityId, CityCode);
//		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.updateSite, "Update site info");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.updateSite, "Update site info");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
	}

	public String GetUpdateSiteId(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
	{
		String BuildingID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_ID");

		waitToPageLoad();
		ClickTopMeu("Search");
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.search, "Click On Top Menu Search");
		Reusable.waitForSiebelLoader();

		waitToPageLoad();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Select Search Type >> Building");
//		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteStatus, "Dropdown Search type");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropDown.replace("Value", "Building"));
		Reusable.waitForSiebelLoader();
		
		Reusable.ClearSendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchValue, BuildingID);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Click On Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchSiteSubmit, "Click On Search");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editBuilding, "Edit Link");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editBuilding, "Edit Link");
		Reusable.waitForSiebelLoader();

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		waitForAjax();
		Reusable.waitForSiebelSpinnerToDisappear();
//		waitToPageLoad();

		String Id = getTextFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.updateSiteId).trim();

//		String Id = getTextFrom(SiebelPremiseMasterHelperObj.PremiseMasterHelper.updateSiteId).trim();
		return Id;
		
	} 

	public void EditSite() throws InterruptedException, IOException
	{
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchResultId, "Click on Site ID");
		
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		
		Reusable.waitForElementToAppear(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editLink, 10);
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.editLink);
		
		waitToPageLoad();
		
	}

	public void BuildingVerificationInSiebel(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
		String BuildingID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_ID");

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Street Name"),StreetName);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Country"));
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchDropdown.replace("Value", "Country"));
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteABSelection.replace("Value", Country),"Country");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.siteABSelection.replace("Value", Country),"Country");
		
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "City / Town"),City);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Postal Code"),PostalCode);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Premises"),Premises);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Building Name"));
		clearTextBox(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Building Name"));
		sendKeys(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchInput.replace("Value", "Building Name"),BuildingID);

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchButton, "Search");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.searchButton, "Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.SearchAddressRowSelection,"SearchAddressRowSelection");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.SearchAddressRowSelection,"Search Address RowSelection");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.PickAddress,"Pick Address");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.PickAddress," Click on Row and Pick Address");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.SearchAddressRowSelection,"Search AddressRowSelection");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.SearchAddressRowSelection,"Search Address RowSelection");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.PickBuilding,"Pick Building");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.PickBuilding,"Click on Row and Pick Building");
	
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.SearchAddressRowSelection,"SearchAddressRowSelection");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.SearchAddressRowSelection,"Click on Search Address RowSelection");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.PickSite,"Site");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.PickSite,"Site");

		Reusable.waitForSiebelLoader();
		waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		waitToPageLoad();
		
		if (isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) 
		{
			verifyExists(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"verify Save order changes");
			click(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"Click on Save order Changes");
			Reusable.waitForSiebelLoader();
			System.out.println("page load succesfuuly now come to middle applet");
			Reusable.waitForSiebelLoader();
		}
	}

	public void PremiseLogout() throws Exception
	{
//		openBrowser("firefox");
		verifyExists(SiebelPremiseMasterHelperObj.PremiseMasterHelper.logout,"Logout");
		click(SiebelPremiseMasterHelperObj.PremiseMasterHelper.logout,"Logout");
		Reusable.waitForSiebelLoader();
	}

}