package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.ReadExcelFile;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;

public class TS_13_Copy_Quote_Using_Types extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testCopyQuoteUsingTypes(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName)
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");		
		record.startVideoRecording(botId, ScenarioName);
				
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
			AddProduct.AddProductToQuote(ProductName);
			}
			String Product_Type=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
	
			
			if (Product_Type.equalsIgnoreCase("Addon")) {
//				calling the below product to configure ip addons product
				AddProduct.ipAddonProductConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				}
			else if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
//				Calling the below method to add the product in CPQ
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
				
			}else {
					
//				calling the below method to enter the site address
				AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
									
//				calling the below method to configure the product
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				}			
			
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
	            
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);	
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			
			
			String NRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NRC");
			String MRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MRC");
			String BaseCost=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Base_Cost");
			String Pricing_City=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Pricing_City");
            verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
            quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
            System.out.println("quoteStage: "+quoteStage);
            if (quoteStage.equalsIgnoreCase("To be Priced"))
            {
                AddProduct.editEthernetProductConfiguration();
                Approvals.SwitchCPQUser("AD_User", Quote_ID);
                AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
                AddProduct.verifyQuoteStage("Portfolio Pricing Review");
                AddProduct.clickProductConfigurationBtn(ProductName);
                
                if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
                AddProduct.UpdateBasePriceException(NRC, MRC);
                } else {
              
                AddProduct.UpdatepricingCityException(ProductName,Pricing_City,NRC,MRC);
                }
                Approvals.SwitchCPQUser("Sales_User", Quote_ID);
            }
			
			
			
			AddProduct.verifyQuoteStage("Priced");
			String HubId = AddProduct.addHubrefernce(ProductName);
			if (HubId.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			if(HubId.equals("True"))DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Hub_Reference", HubId);

			String Copy_Type=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Copy_Type");

				AddProduct.copyQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.clickUpdateConfigBtn();
		
//				Picking up the rows if the copy quote type is line item
				String sProduct = ProductName.replaceAll("(?!^)([A-Z])", " $1");
				String bProduct_RowNumber = null; String aProduct_RowNumber = null;
				if (Copy_Type.equalsIgnoreCase("Lineitem")) {
					bProduct_RowNumber = Reusable.MultiLineWebTableCellAction("Product", sProduct, "","GetRow", null,1);
					aProduct_RowNumber = Reusable.MultiLineWebTableCellAction("Product", sProduct, "","GetRow", null, Integer.parseInt(bProduct_RowNumber)+1);
				} else {
					Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Copy_Quote_ID");
					//AddProduct.verifyQuoteStage("Created");
					//AddProduct.clickUpdateConfigBtn();
					
					quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
					System.out.println("quoteStage: "+quoteStage);
					if (quoteStage.equalsIgnoreCase("SE Required")) 
					{
						AddProduct.verifyQuoteStage("SE Required");
								            
						//Login with CPQ SE user
						Approvals.SwitchCPQUser("SE_User", Quote_ID);
						AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);	
						Approvals.SwitchCPQUser("Sales_User", Quote_ID);
					}
					
					 NRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NRC");
					 MRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MRC");
					 BaseCost=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Base_Cost");
					 Pricing_City=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Pricing_City");
		            verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
		            quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
		            System.out.println("quoteStage: "+quoteStage);
		            if (quoteStage.equalsIgnoreCase("To be Priced"))
		            {
		                AddProduct.editEthernetProductConfiguration();
		                Approvals.SwitchCPQUser("AD_User", Quote_ID);
		                AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
		                AddProduct.verifyQuoteStage("Portfolio Pricing Review");
		                AddProduct.clickProductConfigurationBtn(ProductName);
		                
		                if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
		                AddProduct.UpdateBasePriceException(NRC, MRC);
		                } else {
		              
		                AddProduct.UpdatepricingCityException(ProductName,Pricing_City,NRC,MRC);
		                }
		                Approvals.SwitchCPQUser("Sales_User", Quote_ID);
		            }
					
					AddProduct.verifyQuoteStage("Priced");
				}
			
				AddProduct.SubmitForApproval();
				AddProduct.verifyQuoteStage("Approved");
				AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				
//				Entering BCN values for CPE Solution Service
				String refColName = null;
				String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
				if (Copy_Type.equalsIgnoreCase("Lineitem")) 
				{
					Submit.addMultiLineBillingInformation(sProduct, BCN, Integer.parseInt(bProduct_RowNumber));
					Submit.addMultiLineBillingInformation(sProduct, BCN, Integer.parseInt(aProduct_RowNumber));
					
				} else {
					if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
					{
					String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
					Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
					} else {
					BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
					Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
					}
								
				}
				
				
				Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				
			
//			Calling the below method to fetch the quote ID
			String pQuoteLineItem = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Quote Line Item Id","GetValue", null, Integer.parseInt(bProduct_RowNumber));
			System.out.println("pQuoteLineItem "+ pQuoteLineItem);
			if (pQuoteLineItem.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			String aQuoteLineItem = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Quote Line Item Id","GetValue", null, Integer.parseInt(aProduct_RowNumber));
			System.out.println("aQuoteLineItem "+ aQuoteLineItem);
			if (aQuoteLineItem.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			
				if(!Product_Type.equalsIgnoreCase("Addon")) {
				if (Copy_Type.equalsIgnoreCase("Lineitem")) {
					AddProduct.navigateContactInfoTab();
										
//					Calling the below function to enter A company name
					String Company_Name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Company_Name");
					System.out.println("Company_Name "+ Company_Name);
					
//					A-End Entries
					Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, pQuoteLineItem, "Company Name A End", Company_Name);
					Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, aQuoteLineItem, "Company Name A End", Company_Name);					
					
					//					B-End Entries
					if(!ProductName.equalsIgnoreCase("ColtIpAccess")&&!ProductName.equalsIgnoreCase("EthernetHub") ) {
						Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, pQuoteLineItem, "Company Name B End", Company_Name);
						Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, aQuoteLineItem, "Company Name B End", Company_Name);															
					} 
				}
				//else {
				//	AddProduct.contactInfoEntry(testDataFile, dataSheet, scriptNo, dataSetNo, ProductName);
										
				//	AddProduct.saveCPQ("Main");
				//	}
				}
				
				Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				Submit.SubmitOrder();
				AddProduct.verifyQuoteStage("Ordered");
				
				
				if (Copy_Type.equalsIgnoreCase("Lineitem")) {
					String Service_Order = AddProduct.retriveServiceOrder(ProductName, Integer.parseInt(bProduct_RowNumber));
					if (Service_Order.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Service_Order", Service_Order);
					
			//		calling the below method to capture the copy service order
					String Copy_Service_Order = AddProduct.retriveServiceOrder(ProductName, Integer.parseInt(aProduct_RowNumber));
					if (Copy_Service_Order.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Copy_Service_Order", Copy_Service_Order);
				} else {
					Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				
				CPQLogin.CPQLogout();
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	

	
}
