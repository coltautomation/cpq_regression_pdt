package testHarness.c4cFunctions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClasses.Configuration;
import baseClasses.GlobalVariables;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4CLoginObj;
import testHarness.commonFunctions.ReusableFunctions;

public class C4CLoginPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void C4CLogin() throws InterruptedException, IOException 
	{
		Reusable.Waittilljquesryupdated();
		Reusable.WaitforC4Cloader();
		isPresent(C4CLoginObj.Login.userNameTxb,90);
		sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.SalesUser1_Username,"Username textbox");
	
		verifyExists(C4CLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.SalesUser1_Password,"Password textbox");
		
		Reusable.Waittilljquesryupdated();
		verifyExists(C4CLoginObj.Login.loginBtn,"Login button");
		click(C4CLoginObj.Login.loginBtn,"Login button");
		Reusable.Waittilljquesryupdated();
		
		waitForElementToAppear(C4CLoginObj.Login.homeMenu, 30);
		verifyExists(C4CLoginObj.Login.homeMenu,"Home Tab on Landing page");
		Reusable.WaitforC4Cloader();
	}

	public String QT_C4CLogin() throws InterruptedException, IOException 
	{
		verifyExists(C4CLoginObj.Login.userNameTxb,"Username textbox");
		sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.QT_Username,"Username textbox");
	
		verifyExists(C4CLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.QT_Password,"Password textbox");
		
		waitForAjax();
		Reusable.WaitforC4Cloader();
		verifyExists(C4CLoginObj.Login.loginBtn,"Login button");
		click(C4CLoginObj.Login.loginBtn,"Login button");
		
		waitForAjax();
		Reusable.WaitforC4Cloader();
		waitForElementToAppear(C4CLoginObj.Login.homeMenu,120);
		verifyExists(C4CLoginObj.Login.homeMenu,"Home Tab on Landing Page");
		
		return "True";

	}

}
