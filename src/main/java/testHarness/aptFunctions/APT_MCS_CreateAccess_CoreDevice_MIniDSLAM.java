package testHarness.aptFunctions;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;
import testHarness.commonFunctions.ReusableFunctions;

public class APT_MCS_CreateAccess_CoreDevice_MIniDSLAM extends SeleniumUtils{
		
	ReusableFunctions Reusable = new ReusableFunctions();
	//public static String newPremiseName= null;
	//public static String newSiteName= null;
	//public static String newCityName= null;
	
	public void navigatetomanagecoltnetwork() throws InterruptedException, IOException
	{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink, "Manage Colt Network Link");
		mouseMoveOn(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink);
		waitForAjax();
	}
	
	public void navigatetocreateaccesscoredevicepage() throws InterruptedException, IOException
	 {		
	verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink, "Create Access Core Device Link");
	click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink, "Create Access Core Device Link");
	waitForAjax();
     }
	
public void verifydevicecreation_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	    
    	
    	try {
    		
    		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
    		
    //Verify Mandatory fields
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
    		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
    		
    		waitForAjax();
    		
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.WarningMesassage_site, "Warning Mesassage Site");
    		
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_city, "Warning Mesassage City");
    		
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_Country, "Warning Mesassage Country");
    		
    		scrollUp();
    		
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_name, "Warning Mesassage Name");
    		
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_deviceType, "Warning Mesassage Device Type");
    		
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_vendor, "Warning Mesassage vendor");
    		
    		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_routerId, "Warning Mesassage Router Id");
    		
    		
    		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Name");
    		String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
    		String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
    		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RouterID"); 
    		String IosXr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IOSXR");
    		String Full_IQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Full_IQNET");
    		String ModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modular_MSP");
    		String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telnet");
    		String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SSH");
    		String Snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp3");
    		String Snmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp2C");
    		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmProNewValue");
    		String Snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmprwNewValue");
    		String SnmpUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3UsernameNewValue");
    		String SnmpAuthPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3AuthpasswordNewValue");
    		String SnmpPrivPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3PrivpasswordNewValue");
    		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
    		String Management_Address= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address");
    		String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
    		String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
    		String ExSite =	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
    		String ExSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSiteValue");
    		String ExPremise= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
    		String ExPremiseValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremiseValue");
    		String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
    		String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
    		String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
    		
    		String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
    		String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
    		String newSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
    		
    		String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
    		String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
    		String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
    	
    		
    		//name
			//addtextFields_commonMethod(application, "Name", "nametextfield", name, xml);
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "Name Text Field");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,Name,"Name Text Field");
		
			//devicetype
			//selectValueInsideDropdown(application, "devicetypeinput", "Device Type", devicetype, xml);
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, "Device Type Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, deviceType, "Device Type Input");
		
			//vendormodel
			//addDropdownValues_commonMethod(application, "Vendor/Model", "vendormodelinput", vendormodel, xml);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
			addDropdownValues_commonMethod("Vendor/Model", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, VendorModel);
		
			waitForAjax();
			
			//modular msp selection
			if(ModularMSP.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
			}
			//full iqnet selection
			if(Full_IQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader, "Full IQNET Alert Header");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
			}
			/*
			if(IosXr.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IosXr Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IosXr Checkbox");
			}
			*/
	//Connectivity Protocol		
			if(telnet.equalsIgnoreCase("Yes") && ssh.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
			}
			
			else if(telnet.equalsIgnoreCase("No") && ssh.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			}
			
		
	//SNMP version		
			if(Snmp2C.equalsIgnoreCase("Yes") && Snmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, "snmpro Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,Snmpro);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, "snmprw Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield,Snmprw);
				
			}
			
			else if(Snmp2C.equalsIgnoreCase("No") && Snmp3.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username,SnmpUser);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword, "snmp AuthPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,SnmpAuthPasswd);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword, "snmp PrivPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,SnmpPrivPasswd);
				
			}
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput,"Country Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput1 +country+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput2,"Country Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, country, "Country Input");
			
		//Router id
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, "Router Id Textfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, RouterId );
				
		//Management address
			
			if(!Management_Address.equalsIgnoreCase("Null"))
			{
				//Management_Address = genRandomUptoHundred()+"."+Management_Address;
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox, "Management Address Textbox");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,Management_Address);		
			//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManagementAddress_Mg_Network",Management_Address);
			//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address",Management_Address);
			
			}
			//select country
				scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput);
				waitForAjax();
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, country, "Country Input");
				
				if(ExCity.equalsIgnoreCase("Yes")&& newCity.equalsIgnoreCase("No"))
				{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, ExCityValue, "City dropdown Input");
				
				}
				else if(ExCity.equalsIgnoreCase("No")&& newCity.equalsIgnoreCase("Yes"))
				{
					/*newCityName="CityB"+genRandomUptoThousand();
					newPremiseName="PremiseB"+genRandomUptoThousand();
					newSiteName="SiteB"+genRandomUptoThousand();
					newCityCode="C"+genRandomUptoHundred();
					newPremiseCode="P"+genRandomUptoHundred();
					newSiteCode="S"+genRandomUptoHundred();
					*/
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
					click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,newCityName);
					//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City_Mg_Network",newCityName);
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, newCityCode);
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,newSiteName);
					//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Mg_Network",newSiteName);
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, newSiteCode);
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,newPremiseName);
					//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premise_Mg_Network",newPremiseName);
					
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, newPremiseCode);
					
				}
				if(ExSite.equalsIgnoreCase("Yes")&& newSite.equalsIgnoreCase("No"))
				{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, ExSiteValue, "Site dropdown Input");
				
				}
				else if(ExSite.equalsIgnoreCase("No")&&newSite.equalsIgnoreCase("Yes")&& newPremise.equalsIgnoreCase("yes")&& newCity.equalsIgnoreCase("No"))
				{
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");
					click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch,"Add Site Switch");
					
					//newSiteName="SiteB"+genRandomUptoThousand();
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected, "Site Name Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,newSiteName);
					//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Mg_Network",newSiteName);
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected, "Site Code Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,newSiteCode+genRandomUptoThousand());
					
					//newPremiseName = "PremiseA"+genRandomUptoThousand();
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected, "Premise Name Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,newPremiseName);
					//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premise_Mg_Network",newPremiseName);
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected, "Premise Code Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,newPremiseCode+genRandomUptoThousand());
				}
				if(ExPremise.equalsIgnoreCase("Yes")&& newPremise.equalsIgnoreCase("No"))
				{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, ExPremiseValue, "Premise Dropdown Input");
				
				}
				else if(ExPremise.equalsIgnoreCase("No")&& newPremise.equalsIgnoreCase("yes")&&newSite.equalsIgnoreCase("No")&& newCity.equalsIgnoreCase("No"))
				{
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "Add Premise Switch");
					click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "Add Premise Switch");
					
					//newPremiseName="PremiseB"+genRandomUptoThousand();
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,newPremiseName, "Premise Name Inputfield");
					//DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premise_Mg_Network",newPremiseName);
					
					verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
					sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,newPremiseCode,"Premise Code Inputfield");
				}
    	} catch (StaleElementReferenceException e) {
			
    		e.printStackTrace();
		}
    	
    	scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		Reusable.waitForAjax();
	   
	    verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
	    click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
	    webDriver.findElement(By.tagName("body")).sendKeys(Keys.HOME);
	    Reusable.WaitforCPQloader();	
    }
	
	public void verifyDeviceCreationMessage() throws InterruptedException, IOException
	 {	Reusable.waitForAjax();
	 	 webDriver.findElement(By.tagName("body")).sendKeys(Keys.HOME);
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg, "Device created successfully");
	
		 String deviceCreation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg,"device creation success msg");
		 String expected_deviceUpdation="Device created successfully";
		 if(deviceCreation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS, expected_deviceUpdation);
		 }
		 else{
			 Report.LogInfo("INFO","Device not created: "+deviceCreation_Msg , "FAIL");
			 ExtentTestManager.getTest().log(LogStatus.FAIL, "Device not created: "+deviceCreation_Msg);
		 }
		 
		 Reusable.WaitforCPQloader();
		 
	 }
	
	 
	 public void verifyDeviceUpdationSuccessMessage() throws InterruptedException, IOException
	 {
		 Reusable.waitForAjax();
		scrollIntoTop();
		if(isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation)){
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation, "Device updated successfully");
		 String deviceUpdation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation,"success Message_device Updation");
		 String expected_deviceUpdation="Device updated successfully";
		 if(deviceUpdation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS, expected_deviceUpdation);
		 }
		 else{
			 Report.LogInfo("INFO","Device not updated: "+deviceUpdation_Msg , "FAIL");
			 ExtentTestManager.getTest().log(LogStatus.FAIL, "Device not updated: "+deviceUpdation_Msg);
			 
		 }
		}else{
			Report.LogInfo("INFO","Device not updated: ", "FAIL");
			 ExtentTestManager.getTest().log(LogStatus.FAIL, "Device not updated: ");	 
			
		}
		 
	 }
	 public void verifydeviceDelete_AccessRouter() throws IOException, InterruptedException 
	 {			
		 scrollUp() ;
		 refreshPage();
		 waitForAjax();
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
		 
		 waitForAjax();
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete, "Delete Device");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete,"Delete Device");

		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.delete_alertpopup, "Delete Alert Popup");
		
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton, "Delete Button");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton,"Delete Button");
		 
		 waitForAjax();
		 
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg, "Device Deleted successfully");
		 
		 String deviceDelete_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg,"delete Device Conf Msg");
		 String expected_deviceDelete="Device successfully deleted.";
		 if(deviceDelete_Msg.equals(expected_deviceDelete))
		 {
			 Report.LogInfo("INFO",expected_deviceDelete , "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS, expected_deviceDelete);
		 }
		 else{
			 Report.LogInfo("INFO","Device not deleted: "+deviceDelete_Msg , "FAIL");
			 ExtentTestManager.getTest().log(LogStatus.FAIL, "Device could not be deleted: "+deviceDelete_Msg);
			 
		 }
		 
	    }
	 
	 public void verifEditedValue_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	 {   
		 String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editdeviceName");
		 String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
		 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editVendorModel");
		 String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editRouterID");
		 String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editModularMSP");
		 String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editFullIQNET");
		 String editIOSXR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editIOSXR");
		 String editTelnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editTelnet");
		 String editSSH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp2C");
		 String Telnet ="telnet";
		 String SSH ="ssh";
		 String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmProNewValue");
		 String editSnmprwNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmprwNewValue");
		 String editSnmpv3UsernameNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3UsernameNewValue");
		 String editSnmpv3AuthpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3AuthpasswordNewValue");
		 
		 String editSnmpv3PrivpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3PrivpasswordNewValue");
		 
		 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editManagementAddress");
		 
		 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
		 
		 String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
		 String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
		 String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
		 String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
		 
		 String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
		 String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
		 String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
		 String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
		 
		 String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
		 String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
		 String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
		 
		 String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
		 String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
		 String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
		 String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");
			
		 //Device name
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab, "Name Tab");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab);
			compareText("Name",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab,editDevicename);
			
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType, "Device Type");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType);//String deviceType =
			//compareText("Device Type",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType,deviceType);
			if(!editVendorModel.equalsIgnoreCase("null")){
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model);
			compareText("Vendor/Model",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model,editVendorModel);
			}
			if(editFullIQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET Checkbox");
			compareText("Full IQNET",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "true");
			
			}
			
			//if(editModularMSP.equalsIgnoreCase("Yes"))
			//{
				//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Modular MSP Checkbox");
				//compareText("Modular MSP",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Yes");
			//}
		
			if(editIOSXR.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "IOSXR Checkbox");
			compareText("IOS-XR",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "Yes");
			}
			
			if(editTelnet.equalsIgnoreCase("Yes") && editSSH.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,Telnet);
				
			}
			
			else if(editTelnet.equalsIgnoreCase("No") && editTelnet.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,SSH);
			}
			
			if(editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw, "snmpro Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmprw",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw,editSnmprwNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro, "snmprw Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmpro",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro,editSnmProNewValue);
			
			}
			else if(editSnmp2C.equalsIgnoreCase("No") && editSnmp3.equalsIgnoreCase("Yes"))
			{
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username, "SNMP priv UserName");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmp v3 Username",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username,editSnmpv3UsernameNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass, "SNMP priv Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmp v3 Priv password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass,editSnmpv3PrivpasswordNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass, "SNMP Auth Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass);
				compareText("Snmp v3 Auth password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass,editSnmpv3AuthpasswordNewValue);
				
			}
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId, "Router Id");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId);
			compareText("Router Id",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId,editRouterID);
			if(!editCountry.equalsIgnoreCase("Null"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country, "Country");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country);
			compareText("Country",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country,editCountry);
			compareText_InViewPage1("Country", editCountry);
			}
			{
				Report.LogInfo("Country", "Country value not been updated", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Country value not been updated");
			}
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd);
			//compareText("Management Address",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd,editManagementAddress);
			//compareText_InViewPage("Management Address", editManagementAddress);
			
			if(editExistingCity.equalsIgnoreCase("Yes") && editNewCity.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,editNewCityName);
			}
			else if(editExistingCity.equalsIgnoreCase("No") && editNewCity.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,editNewCityName);
			}

			if(editExistingSite.equalsIgnoreCase("Yes") && editNewSite.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,editNewSiteName);
			}
			else if(editExistingSite.equalsIgnoreCase("No") && editNewSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,editNewSiteName);
			}
			if(editExistingPremise.equalsIgnoreCase("Yes") && editNewPremise.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,editNewPremiseName);
			}
			else if(editExistingPremise.equalsIgnoreCase("No") && editNewPremise.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,editNewPremiseName);
			}
		 
	}
	 
	 public void fetchDeviceInterface_viewdevicepage() throws InterruptedException, IOException {
			waitForAjax();
			webDriver.findElement(By.tagName("body")).sendKeys(Keys.HOME); 
			//scrollUp() ;
			waitForAjax();
			//scrollUp() ;
			 
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
			 
			 waitForAjax();
			 
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage, "Fetch Device Interfaces");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage,"Fetch Device Interfaces");
			
			 waitForAjax();
			 
				String expectedValue="Fetch interfaces started successfully. Please check the sync status of this device";
				boolean successMessage=false;
				
				successMessage=isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert);
				//successMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).isDisplayed();
				
				String actualMessage = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert,"success Message_Alert");
				//String actualMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).getText();
				
				
				if(successMessage) {
					
					if(actualMessage.isEmpty()) {
						Report.LogInfo("INFO", "Success message is not displaying", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Success message is not displaying");
						
					}
					if(actualMessage.contains(expectedValue)) {
						
						Report.LogInfo("INFO", " Success Message displays as: "+actualMessage, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message displays as: "+actualMessage);
						
						verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface, "Interfaces click here Link");
						click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
						 waitForAjax();
					
					}
					else if (actualMessage.equalsIgnoreCase(expectedValue)) {
						Report.LogInfo("INFO", " Success Message displays as: "+actualMessage, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message displays as: "+actualMessage);
						//click on the 'click here' link
						verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface, "Interfaces click here Link");
						click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
						waitForAjax();
					}
					else {
						Report.LogInfo("INFO", "Error in fetching interface: "+actualMessage, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Error in fetching interface: "+actualMessage);
						
						//Action Button Click 
							verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
							 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
							 waitForAjax();
						//click on 'Manage' link
							 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage, "manage Link_view Devicepage");
							 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,"manage Link view Devicepage");
							
							 waitForAjax();
					}
					
				}else {
					
					Report.LogInfo("INFO", " After clicking on 'Fetch Device Interface' link, success Message is not displaying", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, " After clicking on 'Fetch Device Interface' link, success Message is not displaying");
					
				}
		}
	 
	 public void verifyenteredValue_forDeviceCreation(String testDataFile, String sheetName, String scriptNo,
				String dataSetNo) throws InterruptedException, IOException {
		 	
		 	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Name");
			String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
			String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RouterID"); 
			String IosXr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IOSXR");
			String Full_IQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Full_IQNET");
			String ModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modular_MSP");
			String telnet = "telnet";
			String ssh = "ssh";
			String Snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp3");
			String Snmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp2C");
			String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmProNewValue");
			String Snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmprwNewValue");
			String SnmpUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3UsernameNewValue");
			String SnmpAuthPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3AuthpasswordNewValue");
			String SnmpPrivPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3PrivpasswordNewValue");
			String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
			String Management_Address= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address");
			String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
			String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
			String ExSite =	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
			String ExSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSiteValue");
			String ExPremise= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
			String ExPremiseValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremiseValue");
			String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
			String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
			String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
			
			String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
			String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
			String newSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
			
			String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
			String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
			String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab, "Name Tab");
					
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab);
			compareText("Name",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab,Name);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType, "Device Type");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType);//String deviceType =
			compareText("Device Type",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType,deviceType);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model);
			compareText("Vendor/Model",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model,VendorModel);
			
			if(Full_IQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET Checkbox");
			compareText("Full IQNET",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "true");
			
			}
			/*
			if(ModularMSP.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Modular MSP Checkbox");
				compareText("Modular MSP",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Yes");
			}
			
			if(IosXr.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "IOSXR Checkbox");
			compareText("IOS-XR",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "Yes");
			}
			*/
			if(telnet.equalsIgnoreCase("Yes") && ssh.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,telnet);
			}
			
			else if(telnet.equalsIgnoreCase("No") && ssh.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,ssh);
			}
			
			if(Snmp2C.equalsIgnoreCase("Yes") && Snmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw, "snmpro Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmprw",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw,Snmprw);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro, "snmprw Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmpro",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro,Snmpro);
			
			}
			else if(Snmp2C.equalsIgnoreCase("No") && Snmp3.equalsIgnoreCase("Yes"))
			{
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username, "SNMP priv UserName");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmp v3 Username",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username,SnmpUser);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass, "SNMP priv Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmp v3 Priv password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass,SnmpPrivPasswd);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass, "SNMP Auth Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass);
				compareText("Snmp v3 Auth password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass,SnmpAuthPasswd);
				
			}
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId, "Router Id");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId);
			compareText("Router Id",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId,RouterId);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Country");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET);
			//compareText("Country",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET,country);
			compareText_InViewPage1("Country", country);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd);
			//compareText("Management Address",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd,Management_Address);
			compareText_InViewPage1("Management Address", Management_Address);
			
			if(ExCity.equalsIgnoreCase("Yes") && newCity.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,ExCityValue);
			}
			else if(ExCity.equalsIgnoreCase("No") && newCity.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,newCityName);
			}

			if(ExSite.equalsIgnoreCase("Yes") && newSite.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,ExSiteValue);
			}
			else if(ExSite.equalsIgnoreCase("No") && newSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,newSiteName);
			}
			if(ExPremise.equalsIgnoreCase("Yes") && newPremise.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,ExPremiseValue);
			}
			else if(ExPremise.equalsIgnoreCase("No") && newPremise.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,newPremiseName);
			}
		
	 }	
		
	 public void verifydeviceEdit_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException {

		 String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editdeviceName");
		 //String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
		 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editVendorModel");
		 String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editRouterID");
		 String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editModularMSP");
		 String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editFullIQNET");
		 String editIOSXR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editIOSXR");
		 String editTelnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editTelnet");
		 String editSSH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSSH");
		 String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp2C");
		 String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmProNewValue");
		 String editSnmprwNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmprwNewValue");
		 String editSnmpv3UsernameNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3UsernameNewValue");
		 String editSnmpv3AuthpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3AuthpasswordNewValue");
		 
		 String editSnmpv3PrivpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3PrivpasswordNewValue");
		 
		 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editManagementAddress");
		 
		 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
		 
		 String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
		 String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
		 String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
		 String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
		 
		 String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
		 String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
		 String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
		 String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
		 
		 String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
		 String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
		 String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
		 
		 String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
		 String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
		 String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
		 String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");
			scrollUp();
			waitForAjax();

			// Action Button Click
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			waitForAjax();

			// Edit Button Click
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
			//String EditButtonClick = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit);
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
			
			waitforPagetobeenable();
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "Edit Device Name TextField");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,editDevicename,"Edit Device Name TextField");
			
			
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, "Device Type Input");
			//selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput,deviceType, "Device Type Input");
			if(!editVendorModel.equalsIgnoreCase("null")){
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
			//selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, editVendorModel, "Vendor Model Input");
			addDropdownValues_commonMethod("Vendor/Model", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, editVendorModel);
			}
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, "Edit RouterID TextField");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield,editRouterID,"Edit RouterID TextField");
			
			if(editFullIQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			waitForAjax();
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader, "Full IQNET Alert Header");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
			}
			
			if(editModularMSP.equalsIgnoreCase("Yes"))
			{
				waitForAjax();
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				waitForAjax();
			}
			
			if(editIOSXR.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			}
			
			if(editTelnet.equalsIgnoreCase("Yes") && editSSH.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
			}			
			else if(editTelnet.equalsIgnoreCase("No") && editSSH.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			}
			
			if(editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, "snmpro Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,editSnmProNewValue);
			
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, "snmprw Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield,editSnmprwNewValue);
				
			}
			
			else if(editSnmp2C.equalsIgnoreCase("No") && editSnmp3.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username,editSnmpv3UsernameNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword, "snmp AuthPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,editSnmpv3AuthpasswordNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword, "snmp PrivPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,editSnmpv3PrivpasswordNewValue);
				//}
			//}
			}
			
		if(!editCountry.equalsIgnoreCase("null"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, editCountry, "Edit Country Input");
				
			if(editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, editExistingCityValue, "City dropdown Input");
			
			}
			else if(editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,editNewCityName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,editNewCityCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
				
			}
				
			if(editExistingSite.equalsIgnoreCase("Yes")&& editNewSite.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, editExistingSiteValue, "Site dropdown Input");
			
			
			}
			else if (editExistingSite.equalsIgnoreCase("No")&& editNewSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch,"Add Site Switch");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
			}
			
			if(editExistingPremise.equalsIgnoreCase("Yes")&& editNewPremise.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, editExistingPremiseValue, "Premise Dropdown Input");
			}
			else if(editExistingPremise.equalsIgnoreCase("No")&& editNewPremise.equalsIgnoreCase("yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "Add Premise Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch,"Add Premise Switch");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,editNewPremiseName, "Premise Name Inputfield");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,editNewPremiseCode,"Premise Code Inputfield");
			}
			
			}
		else if(editCountry.equalsIgnoreCase("Null")){
			Report.LogInfo("INFO"," No changes made for 'Country' dropdown","PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, " No changes made for 'Country' dropdown");
			
		}
		//Edit City
		if(editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, editExistingCityValue, "City Dropdown Input");
		
		}
		else if(editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,editNewCityName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,editNewCityCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
			
		}
		//Edit Site	
		if(editExistingSite.equalsIgnoreCase("Yes")&& editNewSite.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, editExistingSiteValue, "Site dropdown Input");
		
		}
		else if (editExistingSite.equalsIgnoreCase("No")&& editNewSite.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "Add Site Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch,"Add Site Switch");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
		}
		//Edit Premise
		if(editExistingPremise.equalsIgnoreCase("Yes")&& editNewPremise.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, editExistingPremiseValue, "Premise dropdown Input");
		
		}
		else if(editExistingPremise.equalsIgnoreCase("No")&& editNewPremise.equalsIgnoreCase("yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "Add Premise Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch,"Add Premise Switch");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,editNewPremiseName, "Premise Name Inputfield");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,editNewPremiseCode,"Premise Code Inputfield");
		}
		
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		Reusable.waitForAjax();
	
	
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		waitForAjax();
		Reusable.WaitforCPQloader();
		
		}
	 public void testStatus() throws InterruptedException, IOException {
		 Reusable.waitForAjax();
		 Reusable.WaitforCPQloader();
			String element = null;
			String status = null;
			int listSize = getXPathCount(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.tableRow);

			if (listSize >= 1) {

				for (int i = 1; i <= listSize; i++) {
					//element = findWebElement("(//tbody/tr[" + i + "]/td)[1]").getText();
					element = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[1]") ).getText();
				
					if (element.isEmpty()) {

					} else {
						Report.LogInfo("TestStatus", "Test Name is displaying as: " + element, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Test Name is displaying as: " + element);
						// ExtentTestManager.getTest().log(LogStatus.PASS, "Test
						// Name is displaying as: " + element);
						// Log.info("Test Name is displaying as: " + element);
						status = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[2]/div") ).getAttribute("class");
						//status = findWebElement("(//tbody/tr[" + i + "]/td)[2]/div").getAttribute("class");
						// Log.info("status displays as: " + status);
						if (status.contains("red")) {
							Report.LogInfo("TestStatus", "status colour dipslays as: red", "PASS");		
							ExtentTestManager.getTest().log(LogStatus.PASS, "status colour dipslays as: red");
						} else if (status.contains("green")) {
							Report.LogInfo("TestStatus", "status colour dipslays as: green", "PASS");	
							ExtentTestManager.getTest().log(LogStatus.PASS, "status colour dipslays as: green");
						}
					}
				}
			} else {
				Report.LogInfo("TestStatus",
						"Command/Status table is not displaying in view device page. Instead Message displays as: ",
						"FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Command/Status table is not displaying in view device page. Instead Message displays as: ");
				Report.LogInfo("TestStatus", "* Device reachability tests require the device to have an IP address.",
						"FAIL");

			}

		}
	 public void compareText_InViewPage(String labelname, String expectedVal) throws IOException {
			WebElement element = null;
			String el1= "//div[div[label[contains(text(),'";
			String el2= "')]]]/div[2]";
			//element = findWebElement("//div[div[label[contains(text(),'"+labelname+"')]]]/div[2]");
			element = webDriver.findElement(By.xpath(el1+labelname+el2));
			String actualVal = element.getText().toString();

			if (actualVal.contains(expectedVal)) {
				Report.LogInfo("CompareText", "Text match on View page", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Text match on View page");
			} else {
				Report.LogInfo("CompareText", "Text not match on View page", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Text not match on View page");
			}
		}
	 public void compareText_InViewPage_ForNonEditedFields(String labelname)
				throws InterruptedException {

			//String text = null;
			WebElement element = null;

			try {
				//Thread.sleep(1000);
				//element = getwebelement("//div[div[label[contains(text(),'" + labelname + "')]]]/div[2]");
				waitForAjax();
				element = webDriver.findElement(By.xpath("//div[div[label[contains(text(),'" + labelname + "')]]]/div[2]"));
				String emptyele = element.getText().toString();
				
				Report.LogInfo("INFO", labelname + " field is not edited. It is displaying as '" + emptyele + "'", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " field is not edited. It is displaying as '" + emptyele + "'");
				/*ExtentTestManager.getTest().log(LogStatus.PASS,
						labelname + " field is not edited. It is displaying as '" + emptyele + "'");
				Log.info(labelname + " field is not edited. It is displaying as '" + emptyele + "'");*/
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
				/*ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
				Log.info(labelname + " field is not displaying");*/
			}

		}
	 public void routerPanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo) 
			 throws InterruptedException, IOException {

			 String editedVendor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editVendorModel");
			 String addedVendor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
			 String managementAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editManagementAddress");
			 String managementAddressCreated = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "Management_Address");

			 String command_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv4");
			 String command_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv6");
			 String vrf_Ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");
			 String vrf_Ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv6");

			 String vendorModel = null;
			 // String editedVendor = map.get("editVendorModel");
			 // String addedVendor = map.get("VendorModel");
			 if (editedVendor.equalsIgnoreCase("null")) {
			 vendorModel = addedVendor;
			 } else {
			 vendorModel = editedVendor;
			 }

			 // String managementAddressEdit = map.get("editManagementAddress");
			 // String managementAddressCreated = map.get("Management Address");

			 if (vendorModel.startsWith("Cisco")) {
			 if (managementAddressEdit.equalsIgnoreCase("null")) {

			 routerPanel(command_ipv4, command_ipv6, managementAddressCreated, vrf_Ipv4, vrf_Ipv6);
			 } else {
			 routerPanel(command_ipv4, command_ipv6, managementAddressEdit, vrf_Ipv4, vrf_Ipv6);
			 }
			 } else {
			 Report.LogInfo("INFO", "Router Panel will not display for the selected vendorModel: " + vendorModel,
			 "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS,"Router Panel will not display for the selected vendorModel: " + vendorModel);
			 }

			 // logger = ExtentTestManager.startTest("fetchDeviceInterface");
			 String DeviceName = null;
			 String VendorModel = null;
			 String managementAddress = null;
			 String country = null;

			 String editdeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editdeviceName");
			 String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
			 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editVendorModel");
			 String VendorModell = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
			 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editManagementAddress");
			 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");
			 String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
			 String SnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
			 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editSnmProNewValue");

			 // Device name
			 if (editdeviceName.equalsIgnoreCase("null")) {
			 DeviceName = Name;
			 } else {
			 DeviceName = editdeviceName;
			 }

			 // Vendor/Model
			 if (editVendorModel.equalsIgnoreCase("null")) {
			 VendorModel = VendorModell;
			 } else {
			 VendorModel = editVendorModel;
			 }

			 // Management Address
			 if (editManagementAddress.equalsIgnoreCase("null")) {
			 managementAddress = managementAddressCreated;
			 } else {
			 managementAddress = editManagementAddress;
			 }

			 // Country
			 if (editCountry.equalsIgnoreCase("null")) {
			 country = Country;
			 } else {
			 country = editCountry;
			 }

			 // Snmpro
			 String Snmpro = null;
			 String snmproCreated = SnmProNewValue;
			 String snmproEdited = editSnmProNewValue;
			 String snmproDefaultValue = "JdhquA5";

			 if (snmproEdited.equalsIgnoreCase("null")) {
			 if (snmproCreated.equalsIgnoreCase("null")) {
			 Snmpro = snmproDefaultValue;
			 } else {
			 Snmpro = snmproCreated;
			 }
			 } else {
			 Snmpro = snmproEdited;
			 }
			 }

			 public void routerPanel(String commandIPv4, String commandIPv6, String ipAddress, String vrfname_ipv4,
			 String vrfname_ipv6) throws InterruptedException, IOException {

			 // scrollDown();
			 // Thread.sleep(1000);

			 // Command IPV4
			 //selectByValue(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.commandIPV4_dropdown, commandIPv4,
			 //"Command IPV4");
			 addDropdownValues_commonMethod("Command IPV4",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown, commandIPv4);

			 // addDropdownValues_commonMethod("Command IPV4",
			 // "commandIPV4_dropdown", commandIPv4);

			 hostnametextField_IPV4(commandIPv4, ipAddress);

			 vrfNametextField_IPV4(commandIPv4, vrfname_ipv4);

			 executeCommandAndFetchTheValue(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4);

			 // Commmand IPV6
			 addDropdownValues_commonMethod("Command IPV6", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown, commandIPv6);

			 hostnametextField_IPV6(commandIPv6, ipAddress);

			 vrfNametextField_IPV6(commandIPv6, vrfname_ipv6);

			 executeCommandAndFetchTheValue(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6);

			 }

			 public void hostnametextField_IPV4(String command_ipv4, String ipAddress) throws InterruptedException, IOException {
			 boolean IPV4availability = false;
			 IPV4availability = isVisible(
			 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield);

			 if (IPV4availability) {
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,"commandIPv4_hostnameTextfield");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield, ipAddress);
			 } else {
			 Report.LogInfo("LogStatus",
			 "'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4, "INFO");
			ExtentTestManager.getTest().log(LogStatus.FAIL,"'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4);
				
			 //Report.LogInfo("LogStatus",
			// "'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4, "INFO");
			 }
			 }

			 public void vrfNametextField_IPV4(String command_ipv4, String vrfname_ipv4) throws InterruptedException, IOException {
			 boolean IPV4availability = false;

			 if (command_ipv4.contains("vrf")) {
			 IPV4availability = isVisible(
					 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField);

			 if (IPV4availability) {
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField,"commandIPv4_vrfnameTextField");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,
			 vrfname_ipv4);
			 } else {
			 Report.LogInfo("LogStatus", "'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4,
			 "INFO");
			 ExtentTestManager.getTest().log(LogStatus.FAIL,"'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4);
			 }
			 } else {
			 Report.LogInfo("LogStatus", "'VRF Name IPv4' text field is not displaying for " + command_ipv4, "INFO");
			 ExtentTestManager.getTest().log(LogStatus.FAIL,"'VRF Name IPv4' text field is not displaying for " + command_ipv4);
			 }

			 }

			 public void executeCommandAndFetchTheValue(String executeButton) throws InterruptedException, IOException {

			 click(executeButton,"Execute");
			 // click_commonMethod(application, "Execute", executeButton, xml);

			 boolean resultField = false;
			 resultField = isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea);
			 if (resultField) {
			 Report.LogInfo("LogStatus", "''Result' text field is displaying", "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS,"'Result' text field is displaying");
			 

			 String remarkvalue = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea,"result_text Area");
			 Report.LogInfo("LogStatus", "value under 'Result' field displaying as " + remarkvalue, "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS,"value under 'Result' field displaying as " + remarkvalue);

			 } else {
			 Report.LogInfo("LogStatus", "'Result' text field is not displaying", "FAIL");
			 ExtentTestManager.getTest().log(LogStatus.FAIL,"'Result' text field is not displaying");
			 }
			 }

			 public void hostnametextField_IPV6(String commandIPv6, String ipv6Address)
			 throws InterruptedException, IOException {
			 boolean IPV4availability = false;
			 IPV4availability = isVisible(
					 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield);
			 if (IPV4availability) {

			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield,
			 "IP Address or Hostname");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield, ipv6Address);

			 } else {
			 Report.LogInfo("INFO", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6,
			 "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS,"'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6);
			 }
			 }

			 public void vrfNametextField_IPV6(String commandIPV6, String vrfname_IPV6)
			 throws InterruptedException, IOException {
			 boolean IPV6availability = false;

			 if (commandIPV6.equalsIgnoreCase("vrf")) {

			 IPV6availability = isVisible(
					 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField);
			 if (IPV6availability) {
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,
			 "Router Vrf Name");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,
			 vrfname_IPV6);

			 } else {
			
				 Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6, "PASS");
				 ExtentTestManager.getTest().log(LogStatus.PASS,"'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6);
				 
			 }
			 } else {
			 Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6, "PASS");
			 ExtentTestManager.getTest().log(LogStatus.PASS,"'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6);
			 }

			 }

}
