package baseClasses;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class Configuration {

	public static String URL_INFO = null;
	// public static String PASSWORD= properties.getProperty("password");
	private static GlobalVariables glb = new GlobalVariables();

	public static String C4C_URL, SalesUser1_Username, SalesUser1_Password, SalesUser2_Username, SalesUser2_Password, CPQ_URL, SEUser_Username, SEUser_Password, CSTUser_Username, CSTUser_Password, ContactDetails_URL,EXPLORE_URL,EXPLORE_Offnet_User,EXPLORE_Nearnet_User,
	EXPLORE_Password,PSUser_Username,PSUser_Password,ADUser_Username,DPUser_Username,DPUser_Password,VP_SalesUser1_Username,VP_SalesUser1_Password,VP_SalesUser2_Username,VP_SalesUser2_Password,QT_Username,QT_Password,
	SiebelUser_Username,SiebelUser_Password,Siebel_URL,CEOS_URL,CEOS_Username,CEOS_Password,OMP_URL, OMP_Username, OMP_Password,NH_B2B_URL,NMTS_URL,NMTS_Username,NMTS_Password,APT_URL, APT_Username, APT_Password,Premise_URL, Premise_Username, Premise_Password,
	XNG_URL,XNG_Username,XNG_Password,NC_URL, NC_Username, NC_Password,NOD_URL,NOD_Username,NOD_Password;
	
	public static void setUrlInfoDataSet(HashMap<String, Object> basicURLinfo) {

		C4C_URL = (String) basicURLinfo.get("C4C_URL");
		SalesUser1_Username = (String) basicURLinfo.get("SalesUser1_Username");
		SalesUser1_Password = (String) basicURLinfo.get("SalesUser1_Password");
		SalesUser2_Username = (String) basicURLinfo.get("SalesUser2_Username");
		SalesUser2_Password = (String) basicURLinfo.get("SalesUser2_Password");
		QT_Username = (String) basicURLinfo.get("QT_Username");
		QT_Password = (String) basicURLinfo.get("QT_Password");

		CPQ_URL = (String) basicURLinfo.get("CPQ_URL");
		SEUser_Username = (String) basicURLinfo.get("SEUser_Username");
		SEUser_Password = (String) basicURLinfo.get("SEUser_Password");
		CSTUser_Username = (String) basicURLinfo.get("CSTUser_Username");
		CSTUser_Password = (String) basicURLinfo.get("CSTUser_Password");
		PSUser_Username = (String) basicURLinfo.get("PSUser_Username");
		PSUser_Password = (String) basicURLinfo.get("PSUser_Password");
		DPUser_Username = (String) basicURLinfo.get("DPUser_Username");
		DPUser_Password = (String) basicURLinfo.get("DPUser_Password");
		VP_SalesUser1_Username = (String) basicURLinfo.get("VPSales1_Username");
		VP_SalesUser1_Password = (String) basicURLinfo.get("VPSales1_Password");
		VP_SalesUser2_Username = (String) basicURLinfo.get("VPSales2_Username");
		VP_SalesUser2_Password = (String) basicURLinfo.get("VPSales2_Password");
		
		EXPLORE_URL = (String) basicURLinfo.get("EXPLORE_URL");
		EXPLORE_Offnet_User = (String) basicURLinfo.get("Explore_Offnet_User");
		EXPLORE_Nearnet_User = (String) basicURLinfo.get("Explore_Nearnet_User");
		EXPLORE_Password = (String) basicURLinfo.get("Explore_Password");		

		ADUser_Username = (String) basicURLinfo.get("ADUser_Username");

		ContactDetails_URL = (String) basicURLinfo.get("ContactDetails_URL");
		
		Siebel_URL = (String) basicURLinfo.get("Siebel_URL");
		SiebelUser_Username = (String) basicURLinfo.get("SiebelUser_Username");
		SiebelUser_Password = (String) basicURLinfo.get("SiebelUser_Password");
		
		NH_B2B_URL = (String) basicURLinfo.get("NH_B2B_URL");
		
		CEOS_URL= (String) basicURLinfo.get("CEOS_URL");
		CEOS_Username= (String) basicURLinfo.get("CEOS_Username"); 
		CEOS_Password= (String) basicURLinfo.get("CEOS_Password");
		
		OMP_URL= (String) basicURLinfo.get("OMP_URL");
		OMP_Username= (String) basicURLinfo.get("OMP_Username"); 
		OMP_Password= (String) basicURLinfo.get("OMP_Password");
		
		NMTS_URL = (String) basicURLinfo.get("NMTS_URL");
		NMTS_Username = (String) basicURLinfo.get("NMTS_Username");
		NMTS_Password = (String) basicURLinfo.get("NMTS_Password");
		
		APT_URL = (String) basicURLinfo.get("APT_URL");
		APT_Username = (String) basicURLinfo.get("APT_Username");
		APT_Password = (String) basicURLinfo.get("APT_Password");
		
		Premise_URL= (String) basicURLinfo.get("Premise_URL");
		Premise_Username= (String) basicURLinfo.get("Premise_Username"); 
		Premise_Password= (String) basicURLinfo.get("Premise_Password"); 
		
		XNG_URL= (String) basicURLinfo.get("XNG_URL");
		XNG_Username= (String) basicURLinfo.get("XNG_Username"); 
		XNG_Password= (String) basicURLinfo.get("XNG_Password");
		
		NC_URL= (String) basicURLinfo.get("NC_URL");
		NC_Username= (String) basicURLinfo.get("NC_Username"); 
		NC_Password= (String) basicURLinfo.get("NC_Password");
		
		NOD_URL = (String) basicURLinfo.get("NOD_URL");
		NOD_Username = (String) basicURLinfo.get("NOD_Username");
		NOD_Password = (String) basicURLinfo.get("NOD_Password");
	}

	// ====================================================================================================
	// FunctionName : getURL
	// Description : Function to get all the URL information
	// Input Parameter : None
	// Return Value :
	// ====================================================================================================
	public static String getURL()
	{
		Properties prop = new Properties();
		
		try (InputStream input = new FileInputStream(glb.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
	    	    
		//String Environment = util.getValue("Environment", "RFS").trim();
	    String Environment = prop.getProperty("Environment");
	    //System.out.println("Environment is: "+Environment);
		glb.setEnvironment(Environment);
		if(Environment.equalsIgnoreCase("RFS"))
		{
			URL_INFO = "RFS";

		}else if(Environment.equalsIgnoreCase("Test1"))
		{
			URL_INFO = "Test1";

		}else if(Environment.equalsIgnoreCase("UAT"))
		{
			URL_INFO = "UAT";

		} else if(Environment.equalsIgnoreCase("RFS (navmctmnms300)"))
		{
			URL_INFO = "RFS (navmctmnms300)";
		}
		else if(Environment.equalsIgnoreCase("NC SIT"))
		{
			URL_INFO = "SIT";
		}
		else if(Environment.equalsIgnoreCase("NC RFS"))
		{
			URL_INFO = "RFS";
		}
		return URL_INFO;
	}
}