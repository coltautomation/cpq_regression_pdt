package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDirectActivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODPortInRequestPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder=new NODMyOrderPage();
	private static HashMap<String, String> Number= new HashMap<String, String>();
	String OrderStatus;

	public void PortInActivation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
		String CustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Name");
		String HouseNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"House_Number");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City_Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String NewHouseNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"New_House_Number");
		String NewStreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"New_Street_Name");
		String NewCityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"New_City_Town");
		String NewPostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"New_Postal_Code");
		String NumbersToPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Numbers_To_Port");
		String SingleOrMultiLineNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Single_Multi_LineNo");
		String MainBillingNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Main_BillingNo");
		String currentOperator = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Current_Operator");
		String PortInWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PortIn_Window");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String CustomerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Type");
		
		
		verifyExists(NODPortInRequestObj.portIn.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODPortInRequestObj.portIn.QuickLinkDrpDwnArw,"Quick Link Drop down");

		click(NODPortInRequestObj.portIn.portInLink,"Port In link click");
		waitForAjax();
		waitForElementToBeVisible(NODPortInRequestObj.portIn.portInPage,15);
		Reusable.Select(NODPortInRequestObj.portIn.ServiceType,ServiceType);
		
	    sendKeys(NODPortInRequestObj.portIn.CustomerName, CustomerName, "Enter Customer Name");
		sendKeys(NODPortInRequestObj.portIn.BuildingNumber, HouseNumber, "Enter House Number");
		sendKeys(NODPortInRequestObj.portIn.StreetName, StreetName, "Enter Street Name");
		sendKeys(NODPortInRequestObj.portIn.CityOrTown, CityTown, "Enter City Town");
		sendKeys(NODPortInRequestObj.portIn.PostalCode, PostalCode, "Enter Postal Code");
		
		
		javaScriptclick(NODPortInRequestObj.portIn.ValidateAddressBtn,"Validate Address Button");
		waitForAjax();
        if(isElementPresent(By.xpath("//h5[contains(text(),'Provided address has validated successfully')]")))
        {                              
        	javaScriptclick(NODPortInRequestObj.portIn.ProvideNumbersBtn,"Provide Numbers Btn");
        }
        else{
        	Report.LogInfo("Log Report",":","Please check that if all entered Addressed fileds are correct");
        }
        waitForElementToBeVisible(NODPortInRequestObj.portIn.NumberForPortIn,15);
        sendKeys(NODPortInRequestObj.portIn.NumberForPortIn, NumbersToPort, "Number To Port");
        javaScriptclick(NODPortInRequestObj.portIn.ValidateNumbers,"Validate Numbers");
        WaitForAjax();
        javaScriptclick(NODPortInRequestObj.portIn.NumberEnrichmentBtn,"Number Enrichment Btn");
        
        waitForElementToBeVisible(NODPortInRequestObj.portIn.MainBillingNumberChkBox,15);
        javaScriptclick(NODPortInRequestObj.portIn.MainBillingNumberChkBox,"Main Billing Number ChkBox");
        sendKeys(NODPortInRequestObj.portIn.MainBillingNo, MainBillingNo, "Enter New Postal Code");
        Reusable.Select(NODPortInRequestObj.portIn.SingleAndMultiLine,SingleOrMultiLineNo);
        WaitForAjax();
        javaScriptclick(NODPortInRequestObj.portIn.ProvidePortingDocBtn,"Provide Porting Documents Btn");
		
        String path1 = new File(".").getCanonicalPath();
		String uploadFilePath = path1+"/TestData/Test.docx";
		
		sendKeys(NODPortInRequestObj.portIn.ChooseFileBtn, uploadFilePath,"Choose file for Letter Of Authorisation");
		
	    LocalDate today = LocalDate.now();
		String PortInDate=(addDaysSkippingWeekends(today, 20)).toString();
	    System.out.println(PortInDate);  
	    sendKeys(NODPortInRequestObj.portIn.PortInDate, PortInDate, "Port In Date");
	    verifyExists(NODPortInRequestObj.portIn.currentOperator,"OcurrentOperator");
	    javaScriptclick(NODPortInRequestObj.portIn.currentOperator,"OcurrentOperator");
	    javaScriptclick(NODPortInRequestObj.portIn.currentOperatorOption.replace("operator",currentOperator),"OcurrentOperator");
	   // Reusable.Select(NODPortInRequestObj.portIn.currentOperator,currentOperator);
	   
	    sendKeys(NODPortInRequestObj.portIn.PortInWindow, PortInWindow, "Port In Window");
	    WaitForAjax();
	    javaScriptclick(NODPortInRequestObj.portIn.ProvideContactDetailsBtn,"Provide Contact Details Btn");
	    WaitForAjax();
	    javaScriptclick(NODPortInRequestObj.portIn.ReviewYourOrderBtn,"Review Your Order Btn");
	    WaitForAjax();
	    javaScriptclick(NODPortInRequestObj.portIn.ConfirmYourOrderBtn,"Confirm Your Order Btn");
	    
	    waitForElementToBeVisible(NODPortInRequestObj.portIn.PortInConfirmationMsg,15);
	    if(isElementPresent(By.xpath("//div[@id='portIn-l-7']//div[@class='row ng-star-inserted'][1]")))
	    {
	    	String SuccessMessage=getTextFrom(NODPortInRequestObj.portIn.SuccessMessage, "SuccessMessage");
	    	String emailNotification=getTextFrom(NODPortInRequestObj.portIn.EmailNotification, "EmailNotification");
	    	ExtentTestManager.getTest().log(LogStatus.PASS, "Success Message is :"+SuccessMessage+"And "+emailNotification);
	    }
	    else{
	    	ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Port in is Not successful");
	    }
	    String portInOrderId=getTextFrom(NODPortInRequestObj.portIn.portInOrderId, "SuccessMessage");
	    WaitForAjax();
	    javaScriptclick(NODPortInRequestObj.portIn.CheckMyOrder,"Check MyOrder Btn");
	    //WaitForAjax();
	    waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
	    click(NODPortInRequestObj.portIn.SearchTxtFld, "Send Order ID");
	    sendKeys(NODPortInRequestObj.portIn.SearchTxtFld,portInOrderId , "Send Order ID");
	    WaitForAjax();
		click(NODPortInRequestObj.portIn.SearchIcon,"Search Icon");
		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
		//waitForAjax();
		
		//String OrderStatus;
		if(isElementPresent(By.xpath("//a[@class='transaction']"))){
		 OrderStatus = getTextFrom(NODPortInRequestObj.portIn.orderStatusVal,"Order Status");
		}else
		{
			 for(int i=0;i<5;i++)
	            {          
			    	WaitForAjax();
					sleep(60000);
					refreshPage();
					waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);;
				    click(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld, "Send Order ID");
				    sendKeys(NODNumberDirectActivationObj.NumDirActivation.SearchTxtFld,portInOrderId , "Send Order ID");
				    WaitForAjax();
					click(NODNumberDirectActivationObj.NumDirActivation.SearchIcon,"Search Icon");
					//waitForAjax();
					waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
	                                                         
	                 if (isElementPresent(By.xpath("//a[@class='transaction']")))
	                 {
	                	 OrderStatus = getTextFrom(NODNumberDirectActivationObj.NumDirActivation.orderStatusVal,"Order Status");  
	                     break;         	
	                 }
	                          
		        }
		
		}
		if(OrderStatus.equals("Validation In Progress")) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Status Is: "+OrderStatus);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Status is not Present");	
		}
		
	
	}

	public void SendQuickNote(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Comment = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quick_Note");

		verifyExists(NODPortInRequestObj.QuickNote.OrderRow,"Searched Record");
		
		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]")))
        {                              
	        javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
	        javaScriptclick(NODPortInRequestObj.QuickNote.QuickNoteAction,"Quick Note Action");
			verifyExists(NODPortInRequestObj.QuickNote.AddNotePopUp,"Add Note Popup");

			verifyExists(NODPortInRequestObj.QuickNote.AddNoteTxtBox,"Add Note Text Box");
	        sendKeys(NODPortInRequestObj.QuickNote.AddNoteTxtBox, Comment, "Quick Note");

	        ScrollIntoViewByString(NODPortInRequestObj.QuickNote.AddNoteBtn);
		//	verifyExists(NODPortInRequestObj.QuickNote.AddNoteBtn,"Add Note Button");
			click(NODPortInRequestObj.QuickNote.AddNoteBtn,"Add Note Button");

			verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");

        
        }
        else{
      //  	Report.LogInfo("Log Report",":","Order is not found");
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
		
	}
	
	
	
	
	public static LocalDate addDaysSkippingWeekends(LocalDate date, int days) {
	    LocalDate result = date;
	    int addedDays = 0;
	    while (addedDays < days) {
	        result = result.plusDays(1);
	        if (!(result.getDayOfWeek() == DayOfWeek.SATURDAY || result.getDayOfWeek() == DayOfWeek.SUNDAY)) {
	            ++addedDays;
	        }
	    }
	    return result;
	}












}



