package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_IPTransitObj;
import testHarness.aptFunctions.APT_HSS;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
//import testHarness.aptFunctions.APT_SANManagement;
//import testHarness.aptFunctions.Lanlink_Outbandmanagementhelper;
import testHarness.aptFunctions.searchForDevice;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_22_APT_HSS_Test extends SeleniumUtils {
	public String CustomerName = null;
	public static String DeviceNameValue = null;
	public static String VendorModelValue = null;

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	searchForDevice searchDevice = new searchForDevice();
	APT_Login Login = new APT_Login();
	APT_HSS HSS = new APT_HSS();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void APT_IPTransit_01(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException {
		Report.createTestCaseReportHeader(ScenarioName);

		try (InputStream input = new FileInputStream(g.getRelativePath() + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerCreation");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");

			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {

				// CreateCustomer - HSS
				// ---- HSS.createcustomer(testDataFile, dataSheet, scriptNo,
				// dataSetNo);

				// selectExistingCustomer - HSS
				HSS.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			} else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {

				// selectExistingCustomer - HSS
				HSS.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			// verifycreateorder - HSS
			HSS.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyservicetypeselection - HSS
			HSS.verifyselectservicetype(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyservicecreation - HSS
			HSS.verifyServiceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyCustomerDetailsInformation - HSS
			HSS.verifyCustomerDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyservicepanelinviewservicepage - HSS
			HSS.verifyservicepanelInformationinviewservicepage(testDataFile, dataSheet, scriptNo, dataSetNo);

			// VerifyManagementOptionsPanel - HSS
			HSS.VerifyManagementPanel(testDataFile, dataSheet, scriptNo, dataSetNo);

			// VerifyOrderPanel - HSS
			HSS.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			HSS.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyServicePanelLinks - HSS
			HSS.verifyEditservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			HSS.verifyDump();

			// AddNewPEDevice - HSS
			HSS.navigateToAddNewDevicepage();
			//HSS.addNewPEDevice(testDataFile, dataSheet, scriptNo,
			//dataSetNo);

			// --- HSS.verifyViewpage_Devicedetails(testDataFile, dataSheet,
			// scriptNo, dataSetNo);

			// --- HSS.verifyEditDevice_PE(testDataFile, dataSheet, scriptNo,
			// dataSetNo);

			// AddExistingDevice - HSS
			HSS.AddExistingDevice(testDataFile, dataSheet, scriptNo, dataSetNo);

			// SelectInterface - HSS
			HSS.selectInterfacelinkforDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			HSS.SelectInterface();

			// DeleteNewDevice - HSS
			HSS.deleteDevice(testDataFile, dataSheet, scriptNo, dataSetNo);

			// DeleteService - HSS
			HSS.deleteService();

		}

		catch (

		Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

		}

		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}
}
