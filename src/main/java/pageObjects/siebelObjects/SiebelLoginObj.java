package pageObjects.siebelObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class SiebelLoginObj {
	
	public static class Login
	{ 
		//Login page elements
				public static final String userNameTxb="@xpath=//*[@id='s_swepi_1']"; //Username
				public static final String passWordTxb="@name=SWEPassword"; //Password
				public static final String loginBtn="@xpath=//*[@id='s_swepi_22']"; //Login Button		

				//Landing page elements
				public static final String SiebelUserinfo="@xpath=//*[@id='welcome' and contains(text(), 'Ankit Chambial ')]"; //Landing Page
				
				public static final String AccountSettingButton ="@xpath=//*[@id='gen_menu_6']"; //landing page 

				////Logout button
			//	public static final String Logout ="@xpath=//button[text()='Logout']"; //Logout button
				
				
				public static final String Settings ="@xpath=//li[@title='Settings']";
				public static final String Logout ="@xpath=//button[text()='Logout']"; //Logout button
				
				public static final String Advanced="@xpath=//*[@id='details-button']"; // added for IP2020
				public static final String Navigate="@xpath=//*[@id='proceed-link']"; // added for IP2020
				
				public static final String SiebelUserinfo1="@xpath=//span[text()='My Homepage']"; //Landing Page
		}

}