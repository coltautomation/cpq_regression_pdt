package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_CreateAccessCoreDevice_ManageNetwork;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_MCS_CreateAccessCoreDevice_TrafficAggregator;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_26_APT_MCS_CreateAccessCoreDevice_TrafficAggregator extends SeleniumUtils{
		
		ReusableFunctions Reusable = new ReusableFunctions();
		Properties prop = new Properties();
		GlobalVariables g = new GlobalVariables();
		APT_Login Login=new APT_Login();
		APT_MCS_CreateAccessCoreDevice_TrafficAggregator TrafficAggregator = new APT_MCS_CreateAccessCoreDevice_TrafficAggregator();
		APT_CreateAccessCoreDevice_ManageNetwork MCS_ManageNetwork = new APT_CreateAccessCoreDevice_ManageNetwork();
		
		@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
		@Test
		public void CreateAccessCoreDevice_TrafficAggregator(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)throws IOException
		{	
			Report.createTestCaseReportHeader(ScenarioName);
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
		{

	        // load a properties file
	        prop.load(input);               

	    }
		catch (IOException ex) 
		{
	        ex.printStackTrace();
	    }
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		
		try
		{
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			TrafficAggregator.navigatetomanagecoltnetwork();
			TrafficAggregator.navigatetocreateaccesscoredevicepage();
			TrafficAggregator.verifydevicecreation_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
			TrafficAggregator.verifyDeviceCreationMessage();
			TrafficAggregator.verifyenteredValue_forDeviceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);
			TrafficAggregator.verifydeviceEdit_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
			TrafficAggregator.verifyDeviceUpdationSuccessMessage();
			TrafficAggregator.verifEditedValue_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
			TrafficAggregator.testStatus();
			TrafficAggregator.routerPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
			TrafficAggregator.fetchDeviceInterface_viewdevicepage();
			MCS_ManageNetwork.verifyFetchInterface(testDataFile, dataSheet, scriptNo, dataSetNo); 
			TrafficAggregator.verifydeviceDelete_TrafficAggregator();
		}
		catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();
		
		}
		}
