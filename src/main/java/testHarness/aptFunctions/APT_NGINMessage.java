package testHarness.aptFunctions;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import baseClasses.ExtentTestManager;
import baseClasses.Report;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_NGINMessageObj;

public class APT_NGINMessage extends SeleniumUtils {

	public void verifyNGINMessage(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String nginmessage_sannumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NGINMessage_SANNumber");
		String startnetwork_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"StartNetwork_Checkbox");

		verifyExists(APT_NGINMessageObj.NGINMessage.managecoltnetworklink,"managecoltnetworklink");
		mouseMoveOn(APT_NGINMessageObj.NGINMessage.managecoltnetworklink);

		waitForAjax();

		verifyExists(APT_NGINMessageObj.NGINMessage.nginmessageslink, "Ngin Messages");
		click(APT_NGINMessageObj.NGINMessage.nginmessageslink, "NGIN Messages");

		waitForAjax();
		waitforPagetobeenable();

		compareText("Manage Messages Header", APT_NGINMessageObj.NGINMessage.nginmessageheader, "Manage Messages - Messages Search");

		isVisible(APT_NGINMessageObj.NGINMessage.nginmsg_sannumber);
		isVisible(APT_NGINMessageObj.NGINMessage.nginmsg_customername_textfield);

		waitForAjax();

		verifyExists(APT_NGINMessageObj.NGINMessage.nginmsg_sannumber, "SAN Number");
		sendKeys(APT_NGINMessageObj.NGINMessage.nginmsg_sannumber, nginmessage_sannumber, "SAN Number");

		verifyExists(APT_NGINMessageObj.NGINMessage.san_searchbutton, "Search");
		click(APT_NGINMessageObj.NGINMessage.san_searchbutton, "Search");

		waitForAjax();
		waitforPagetobeenable();

		if (isVisible(APT_NGINMessageObj.NGINMessage.ebodyNoSearch)) {
			Report.LogInfo("INFO", "Data is not available for respective search", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"Data is not available for respective search");
		
		} else {
			WebElement SelectExistingSAN = webDriver.findElement(By.xpath("//div[text()='" + nginmessage_sannumber
					+ "']/preceding-sibling::div//span[contains(@class,'unchecked')]"));
			if (SelectExistingSAN.isDisplayed()) {
				// Verify existing SAN column headers
				compareText("FRC Number", APT_NGINMessageObj.NGINMessage.nginmsg_frcnumbercolumn, "FRC Number");
				compareText("Name", APT_NGINMessageObj.NGINMessage.nginmsg_namecolumn, "Name");
				compareText("Customer Name", APT_NGINMessageObj.NGINMessage.customernamecolumn, "Customer Name");

				waitForAjax();

				webDriver.findElement(By.xpath("//div[text()='" + nginmessage_sannumber
						+ "']/preceding-sibling::div//span[contains(@class,'unchecked')]")).click();

				waitForAjax();

				verifyExists(APT_NGINMessageObj.NGINMessage.searchsan_actiondropdown, "Action dropdown");
				click(APT_NGINMessageObj.NGINMessage.searchsan_actiondropdown, "Action dropdown");

				// View link displayed verify
				if (isVisible(APT_NGINMessageObj.NGINMessage.edit)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Step : Edit link is displaying in Manage Messages page");
					Report.LogInfo("INFO", "Edit link is displaying in Manage Messages page", "PASS");

					verifyExists(APT_NGINMessageObj.NGINMessage.edit, "Edit");
					click(APT_NGINMessageObj.NGINMessage.edit, "Edit");

					waitForAjax();
					waitforPagetobeenable();

					compareText("Edit Message Header", "editmessageheader", "Edit Message");

					scrollDown(APT_NGINMessageObj.NGINMessage.cancelbutton);

					verifyExists(APT_NGINMessageObj.NGINMessage.cancelbutton, "Cancel");
					click(APT_NGINMessageObj.NGINMessage.cancelbutton, "Cancel");

					waitForAjax();
					waitforPagetobeenable();
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Step : Edit link is not displaying in Manage Messages page");
					Report.LogInfo("INFO", "Edit link is not displaying in Manage Messages page", "PASS");

				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : No existing NGIN Messages to display");
				Report.LogInfo("INFO", "No existing NGIN Messages to display", "PASS");
			}

			WebElement SelectExistingSAN1 = webDriver.findElement(By.xpath("//div[text()='" + nginmessage_sannumber
					+ "']/preceding-sibling::div//span[contains(@class,'unchecked')]"));
			if (SelectExistingSAN1.isDisplayed()) {
				webDriver.findElement(By.xpath("//div[text()='" + nginmessage_sannumber
						+ "']/preceding-sibling::div//span[contains(@class,'unchecked')]")).click();

				waitForAjax();

				verifyExists(APT_NGINMessageObj.NGINMessage.searchsan_actiondropdown, "Action dropdown");
				click(APT_NGINMessageObj.NGINMessage.searchsan_actiondropdown, "Action dropdown");

				if (isVisible(APT_NGINMessageObj.NGINMessage.edit)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Step : Edit link is displaying in Manage Messages page");
					Report.LogInfo("INFO", "Edit link is displaying in Manage Messages page", "PASS");

					verifyExists(APT_NGINMessageObj.NGINMessage.edit, "Edit");
					click(APT_NGINMessageObj.NGINMessage.edit, "Edit");

					waitForAjax();
					waitforPagetobeenable();

					compareText("Edit Message Header", "editmessageheader", "Edit Message");
					// verify edit message page fields
					// verify customer name field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_customername, "readonly") != null)

					{
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Customer Name field is disabled as expected");
						Report.LogInfo("INFO", " Customer Name field is disabled as expected", "PASS");

						String Customernamevalue = getAttributeFrom(
								APT_NGINMessageObj.NGINMessage.editmessage_customername, "value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Customer Name field value is displayed as:" + Customernamevalue);
						Reporter.log(" Customer Name field value is displayed as:" + Customernamevalue);

					}

					else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Customer Name field is enabled");
					}

					// verify country code field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_countrycode, "readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Country code field is disabled as expected");
						String CountryCodevalue = getAttributeFrom(
								APT_NGINMessageObj.NGINMessage.editmessage_countrycode, "value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Country code field value is displayed as:" + CountryCodevalue);
						Reporter.log("Step : Country code field value is displayed as:" + CountryCodevalue);
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Country code field is enabled");
					}

					// verify san reference field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_sanreference, "readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : SAN Reference field is disabled as expected");
						String SANReferencevalue = getAttributeFrom(
								APT_NGINMessageObj.NGINMessage.editmessage_sanreference, "value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : SAN Reference field value is displayed as:" + SANReferencevalue);
						Reporter.log("Step : SAN Reference field value is displayed as:" + SANReferencevalue);
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : SAN Reference field is enabled");
					}

					// verify name field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_name, "readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Name field is disabled as expected");
						String Namevalue = getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_name, "value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Name field value is displayed as:" + Namevalue);
						Reporter.log("Step : Name field value is displayed as:" + Namevalue);
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Name field is enabled");
					}

					// verify description field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_description, "readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Description field is disabled as expected");
						String Descriptionvalue = getAttributeFrom(
								APT_NGINMessageObj.NGINMessage.editmessage_description, "value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Description field value is displayed as:" + Descriptionvalue);
						Reporter.log("Step : Description field value is displayed as:" + Descriptionvalue);

					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Description field is enabled");
					}

					// verify body field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_bodyfield, "readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Body field is disabled as expected");
						String Bodyvalue = getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_bodyfield,
								"value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Body field value is displayed as:" + Bodyvalue);
						Reporter.log("Step : Body field value is displayed as:" + Bodyvalue);
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Body field is enabled");
					}

					// verify prefix field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_prefixfield, "readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Prefix field is disabled as expected");
						String Prefixvalue = getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_prefixfield,
								"value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Prefix field value is displayed as:" + Prefixvalue);
						Reporter.log("Step : Prefix field value is displayed as:" + Prefixvalue);
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Prefix field is enabled");
					}

					// verify repetitions field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_repetitionsfield,
							"readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Repetitions field is disabled as expected");
						String Repetitionsvalue = getAttributeFrom(
								APT_NGINMessageObj.NGINMessage.editmessage_repetitionsfield, "value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Repetitions field value is displayed as:" + Repetitionsvalue);
						Reporter.log("Step : Repetitions field value is displayed as:" + Repetitionsvalue);
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Repetitions field is enabled");
					}

					// verify duration field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_durationfield,
							"readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Duration field is disabled as expected");
						String Durationvalue = getAttributeFrom(
								APT_NGINMessageObj.NGINMessage.editmessage_durationfield, "value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Duration field value is displayed as:" + Durationvalue);
						Reporter.log("Step : Duration field value is displayed as:" + Durationvalue);

					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Duration field is enabled");
					}

					// verify suffix field
					if (getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_suffixfield, "readonly") != null) {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Suffix field is disabled as expected");
						String Suffixvalue = getAttributeFrom(APT_NGINMessageObj.NGINMessage.editmessage_suffixfield,
								"value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Suffix field value is displayed as:" + Suffixvalue);
						Reporter.log("Step : Suffix field value is displayed as:" + Suffixvalue);

					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Suffix field is enabled");
					}

					// verify start network charge checkbox field
					editcheckbox_commonMethod(startnetwork_checkbox,
							APT_NGINMessageObj.NGINMessage.editmessage_startnetworkcharge, "Start Network Charge");

					scrollDown(APT_NGINMessageObj.NGINMessage.editmessage_okbutton);
					waitForAjax();

					verifyExists(APT_NGINMessageObj.NGINMessage.editmessage_okbutton, "OK");
					click(APT_NGINMessageObj.NGINMessage.editmessage_okbutton, "OK");

					waitForAjax();
					waitforPagetobeenable();

					if (isVisible(APT_NGINMessageObj.NGINMessage.nginmessageheader)) {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Navigated to Manage Messages page");
						Report.LogInfo("INFO", "Navigated to Manage Messages page", "PASS");
						// verifysuccessmessage("Message successfully
						// updated.");

					} else {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Not navigated to Manage Messages page");
						Report.LogInfo("INFO", "Not navigated to Manage Messages page", "PASS");
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Step : Edit link is not displaying in Manage Messages page");
					Report.LogInfo("INFO", "Edit link is not displaying in Manage Messages page", "PASS");
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : No existing NGIN Messages to display");
				Report.LogInfo("INFO", "No existing NGIN Messages to display", "PASS");

			}

		}
	}

}
