package pageObjects.siebelObjects;

import java.io.IOException;

import baseClasses.SeleniumUtils;

public class SiebelModeObj  {


	public static class WaveLineMidleApplet
	{
		public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]"; //Customers Menu
	//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		public static final String MiddleLi="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";//updated 2020;
		public static final String BMiddleLi="@xpath=(//li/*[text()='Value'])[2]";
		
		public static final String SaveWaveLineMidApplet="@xpath=//div[@id='colt-s-order-connection-header']//a[text()='Save']";
	
		public static final String ShowFullInfoB="@xpath=//div[@id='colt-site-right']//*//a[text()='Show full info']";
		public static final String ShowFullInfoA="@xpath=//div[@id='colt-site-left']//*//a[text()='Show full info']";

		public static final String TechnicalContact="@xpath=//span[text()='Technical Contact']/..//span[2]";

		public static final String OkButton ="@xpath=//button[@aria-label='Pick Contact:OK']";
		//public static final String CrossButton="@xpath=//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";
		public static final String CrossButton="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";
		public static final String ProccedButton="@xpath=//*[@id='colt-err-modal-proceed-btn']";
		//public static final String CrossButton="@xpath=//*[contains(@class,'colt-order-full-info')]//button[@title='Close']";


	
	}
	
	public static class addSiteADetailsObj
	{
		public static final String SearchAddressSiteA ="@xpath=//div[@id='colt-site-left']//span[@title='Select a site']";
		public static final String StreetNamerfs ="@xpath=//input[@name='streetName']";
		public static final String Country ="@xpath=//span[text()='Country']/following-sibling::span";
	//	public static final String SiteABSelection ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
	//	public static final String SiteABSelection ="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020;

		public static final String City ="@xpath=//input[@name='cityTown']";
		public static final String PostalCode ="@xpath=//input[@name='postalZipCode']";
		public static final String Premises ="@xpath=//input[@name='premisesNumber']";
		public static final String Search ="@xpath=//div[@id='colt-site-selection-btn-bar']//button[text()='Search']";
		public static final String SearchAddressRowSelection ="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][1]";
	//	public static final String PickAddress ="@xpath=//table[@id='address_table']/tbody/tr[2]";
	//	public static final String PickBuilding ="@xpath=//table[@id='address_table']/tbody/tr";
	//	public static final String PickSite ="@xpath=//div[@id='PopupDiv']//table[@id='address_table']/tbody/tr[1]";
		public static final String ServicePartySearchAccess ="@xpath=//span[text()='Service Party']/..//span[2]";
		public static final String ServicePartyDropdownAccess ="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
		public static final String SiteContactDropdownAccess ="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/parent::*/span";
		public static final String InputPartyNameAccess ="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String PartyNameSearchAccess ="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String PartyNameSubmitAccess ="@xpath=//button[@aria-label='Pick Account:OK']";
		public static final String SaveOrderChanges ="@xpath=//a[text()='Click here to save your order changes.']";
		public static final String SiteContactSearchAccess ="@xpath=//span[text()='Site Contact']/..//span[2]";
		public static final String InputSiteNameAccess ="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		public static final String LastNameSiteSearchAccess ="@xpath=//td[@class='siebui-popup-filter']//button";
		public static final String LastNameSiteSubmitAccess ="@xpath=//button[@aria-label='Pick Contact:OK']";
		public static final String IpGurdianSave ="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
		public static final String SearchAddressRowSelection1 ="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][2]";
		public static final String PickAddress1 ="@xpath=//button[text()='Pick Address']";
		public static final String PickBuilding1 ="@xpath=//button[text()='Pick Building']";
		public static final String PickSite1 ="@xpath=//button[text()='Pick Site']";
		
		public static final String PickAddress="@xpath=//button[text()='Pick Address']";
		public static final String PickBuilding="@xpath=//button[text()='Pick Building']";
		public static final String PickSite="@xpath=//button[text()='Pick Site']";
		
		public static final String SiteABSelection ="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
	}
	
	public static class MandatoryFieldsInHeader
	{ 
		public static final String OrderSubTypeSearch="@xpath=//div[@class='mceGridField siebui-value mceField']/span[@aria-label='Multiple Selection Field']";
		
		public static final String AddOrderSubType="@xpath=//button[@title='Service Order Sub Type:New']";

		public static final String InputOrderSubType="@xpath=//input[@name='COLT_Order_Item_Sub_Type']";
		
		public static final String SubmitSubOrderType="@xpath=//button[@title='Service Order Sub Type:OK']";
		
	//	public static final String ContractSearch="@xpath=//*[@id='s_5_1_52_0_icon']";

		public static final String ContractSearch="@xpath=(//*[@aria-labelledby='COLT_Contract_Label']//following-sibling::*[@class='siebui-icon-pick applet-form-pick applet-list-pick'])[1]";

		public static final String InputContractId="@xpath=//tr//input[@aria-label='Starting with']";
		
		public static final String ContractIdSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Contract:Go']";

		public static final String NetworkReferenceSearch="@xpath=//*[@aria-labelledby='COLT_Network_Reference_Label']/following-sibling::span";

		public static final String AddNetworkReferenceSearch="@xpath=//button[@title='Pick Network Reference:New']";
		
		public static final String SubmitNetworkReference="@xpath=//button[@title='Pick Network Reference:OK']";
		
		public static final String SearchNetworkReference="@xpath=//td[@class='siebui-popup-filter']//button[@title='Pick Network Reference:Go']";

		public static final String NetworkPlusSign="@xpath=//button[@title='Pick Network Reference:New']";

		public static final String SelectNetworkReference="@xpath=//table[@summary='Pick Network Reference']/tbody/tr[2]";
		
		public static final String ExistingCapacityLeadTimePrimary="@xpath=//input[@aria-labelledby='COLT_Existing_Capacity_Lead_Time_Code_Label']";

		public static final String MaintenancePartySearch="@xpath=//input[@aria-label='Maintenance Party']/..//span";

		public static final String MaintenancePartyPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";

	//	public static final String AccountStatus="@xpath=//li[text()='Account Status']";
		public static final String AccountStatus="@xpath=//li/*[text()='Account Status']";//updated2020
		
		public static final String InputAccountStatus="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		
		public static final String AccountStatusSearch="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']/../following-sibling::span/*[@title='Pick Account:Go']";
		
		public static final String AccountStatusSubmit="@xpath=//button[@aria-label='Pick Account:OK']";

		public static final String MaintenancePartyContact="@xpath=//input[@aria-label='Maintenance Party Contact']/..//span"; 

		public static final String MaintenancePartyContactPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";

	//	public static final String MaintenancePartyPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";

	//	public static final String MaintenanceLastName="@xpath=//li[text()='Last Name']";
		public static final String MaintenanceLastName="@xpath=//li/*[text()='Last Name']";  //updateed 2020
		public static final String InputMaintenanceLastName="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		  
		public static final String InputMaintenanceLastNameSearch="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']/../following-sibling::span/*[@title='Pick Contact:Go']";
		  
		public static final String MaintenancePartyContactSubmit="@xpath=//button[@aria-label='Pick Contact:OK']";
		
		public static final String MaintenancePartyAddress="@xpath=//input[@aria-label='Maintenance Party Address']/..//span";

		public static final String MaintenancePartyAddresPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";

	//	public static final String PartyAddresStreetName="@xpath=//li[text()='Street Name']"; 
		public static final String PartyAddresStreetName="@xpath=//li/*[text()='Street Name']";  //UPdated 2020

		public static final String InputPartyAddresStreetName="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		
		public static final String InputPartyAddresStreetNameSearch="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']/../following-sibling::span/*[@title='Pick Address:Go']";
		
		public static final String MaintenancePartyAddressSubmit="@xpath=//button[@aria-label='Pick Address:OK']";

		 public static final String SaveOrderChanges="@xpath=//a[text()='Click here to save your order changes.']";
		 public static final String InputNetworkReference="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
		 public static final String TextNetworkReference="@xpath=//table[@summary='Pick Network Reference']//tr[2]/td[2]";
	
		 public static final String ProactiveContactSearch="@xpath=//input[@aria-label='Proactive Contact']/..//span";
		 public static final String ProactiveContact="@xpath=(//tr[@class='ui-widget-content jqgrow ui-row-ltr ui-state-highlight selected-row ui-state-hover'])[1]";
		 public static final String ProactiveContactSubmit="@xpath=//button[@aria-label='Pick Contact:OK']";
		 
		 public static final String BespokeReferenceCheckbox="@xpath=(//input[@aria-label='Bespoke/Reference'])[1]";
		 public static final String BespokeReferenceValue="@xpath=(//input[@aria-label='Bespoke/Reference'])[2]";
		 
		 public static final String NetworkReferenceCancelBtn="@xpath=//button[@title='Pick Network Reference:Cancel'][not (contains(@disabled,'disabled'))]";
	}
	
	/*public static class VoiceConfigTab
	{
		public static final String TrunkName="@xpath=//span[text()='Trunk Name']/..//input";
		
		public static final String AccessLineTypeDropDown="@xpath=(//span[text()='Access Line Type']/..//span)[3]";
		
		public static final String AccessLineType="@xpath=//span[text()='Access Line Type']/following-sibling::span";	
		
		public static final String AccessServiceIdlookup="@xpath=(//span[@class='siebui-icon-pick'])[2]";
				
		public static final String PickServiceSubmit="@xpath=//button[@title='Pick Service:OK']";

		public static final String showfullinfo="@xpath=//a[text()='Show full info']";
		
		public static final String TrunkConfigurationplus="@xpath=(//span[@class='colt-site-component-collapse fa fa-plus-square-o'])[2]";
						
		public static final String outsidebuissnesshourinstallation="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[2]";
		
		public static final String CrossButton="@xpath=(//button[@title='Close'])[4]";
										
		public static final String SaveButtonClick="@xpath=(//a[text()='Save'])[2]";
										  
		public static final String TrunckConfigplus="@xpath=(//span[@class='colt-site-component-collapse fa fa-plus-square-o'])[1]";

		public static final String OtherTab="@xpath=//a[text()='Other']";
		
		public static final String SelectSiteSearchAccess="@xpath=//span[text()='Select a site:']//span";
			
		public static final String StreetNameAccess="@xpath=//span[text()='Street Name']/..//input";
						
		public static final String CountryAccess="@xpath=//span[text()='Country']/..//input";
						
		public static final String CityTownAccess="@xpath=//span[text()='City / Town']/..//input";
										
		public static final String PostalCodeAccess="@xpath=//span[text()='Postal Code']/..//input";
												
		public static final String PremisesAccess="@xpath=//span[text()='Premises']/..//input";
														
		public static final String SearchButtonAccess="@xpath=//button[text()='Search']"; 
		
		public static final String SelectPickAddressAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
		
		public static final String PickAddressButtonAccess="@xpath=//button[text()='Pick Address']";
		
		public static final String SelectPickBuildingAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
		
		public static final String PickBuildingButtonAccess="@xpath=//button[text()='Pick Building']";
		
				public static final String SelectPickSiteAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
				
				public static final String PickSiteButtonAccess="@xpath=//button[text()='Pick Site']";


	}*/
	
	 public static class EnterDateInFooter
		{
			public static final String AlertAccept="@xpath=//span[text()='Ok']";
		
			public static final String OrderDates="@xpath=//a[text()='Order Dates']";
			public static final String OrderSignedDate="@xpath=//input[contains(@aria-label,'Order Signed Date')]";
			public static final String CustomerRequestedDate="@xpath=//input[@aria-labelledby='Customer_Requested_Date1_Label']";
			public static final String ColtActualDate="@xpath=//input[@aria-labelledby='COLT_Actual_RFS_Date_Label']";
			public static final String OrderReceivedDate="@xpath=//input[@aria-labelledby='COLT_Order_Received_Date_Label']";
			public static final String ColtPromissday="@xpath=//input[@aria-labelledby='Colt_Promised_Date_Label']";

			public static final String OrderSignedDateIcon="@xpath=//input[contains(@aria-label,'Order Signed Date')]/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String CustomerRequestedDateIcon="@xpath=//input[@aria-labelledby='Customer_Requested_Date1_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String ColtActualDateIcon="@xpath=//input[@aria-labelledby='COLT_Actual_RFS_Date_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String OrderReceivedDateIcon="@xpath=//input[@aria-labelledby='COLT_Order_Received_Date_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String ColtPromissdayIcon="@xpath=//input[@aria-labelledby='Colt_Promised_Date_Label']/following-sibling::*[contains(@class,'siebui-icon-date')]";

			public static final String TodayDate="@xpath=//*[contains(@class,'today')]";
			
			public static final String CalendarNowBtn="@xpath=//button[text()='Now']";
			
			
		}
	 
	 public static class EnterBillingDateInFooter
		{
			public static final String Billing="@xpath=//a[text()='Billing']";
			public static final String ContractRenewalFlag="@xpath=//input[@aria-label='Contract Renewal Flag']";
			public static final String ContractTerm="@xpath=//input[contains(@aria-label,'Contract Term')]"; 
		//	public static final String SelectContractTerm="@xpath=//li[text()='12']";
			public static final String SelectContractTerm="@xpath=//li/*[text()='12']"; //Updated2020

			public static final String BillingStartDateAccess="@xpath=//input[@aria-label='Billing Start Date']";
			public static final String POStartDateAccess="@xpath=//input[@aria-label='PO Start Date']"; 
			public static final String POEndDateAccess="@xpath=//input[@aria-label='PO End Date']";

			public static final String BillingStartDateAccessIcon="@xpath=//input[@aria-label='Billing Start Date']/following-sibling::*[contains(@class,'siebui-icon-date')]";
			public static final String POStartDateAccessIcon="@xpath=//input[@aria-label='PO Start Date']/following-sibling::*[contains(@class,'siebui-icon-date')]"; 
			public static final String POEndDateAccessIcon="@xpath=//input[@aria-label='PO End Date']/following-sibling::*[contains(@class,'siebui-icon-date')]";

			public static final String POStartDateInput="@xpath=//input[@aria-label='PO Start Date']"; 
			

		}

	 public static class ProductSpecificCompleted
		{
			public static final String OrderStatusDropdown="@xpath=//input[@aria-label='Order Status']/..//span";
		//	public static final String SelectCompleted="@xpath=//li[text()='Completed']";
			public static final String SelectCompleted="@xpath=//li/*[text()='Completed']"; //UPdated2020

			public static final String OrderComplete="@xpath=//button[text()='Yes']";
			public static final String AlertAccept="@xpath=//span[text()='Ok']";

		}

		public static class LeadCapacity
		{
			public static final String ExistingCapacityLeadTimePrimary="@xpath=//input[@aria-labelledby='COLT_Existing_Capacity_Lead_Time_Code_Label']";


		}
		
		///////////// AA & BK
		
		public static class VoiceConfigTab
		{
			public static final String TrunkName="@xpath=//span[text()='Trunk Name']/..//input";
			
			public static final String AccessLineTypeDropDown="@xpath=(//span[text()='Access Line Type']/..//span)[3]";
			
			public static final String AccessLineType="@xpath=//span[text()='Access Line Type']/following-sibling::span";	
			
			public static final String AccessServiceIdlookup="@xpath=//*[text()='Access Service ID']/following-sibling::span[@class='siebui-icon-pick']";
					
			public static final String PickServiceSubmit="@xpath=//button[@title='Pick Service:OK']";

			public static final String showfullinfo="@xpath=//a[text()='Show full info']";
			
			public static final String TrunkConfigurationplus="@xpath=(//span[@class='colt-site-component-collapse fa fa-plus-square-o'])[2]";
							
			public static final String outsidebuissnesshourinstallation="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[2]";
			
			//public static final String CrossButton="@xpath=//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";
			public static final String CrossButton="@xpath=//*[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";				
			public static final String SaveButtonClick="@xpath=(//a[text()='Save'])[2]";
											  
			public static final String TrunckConfigplus="@xpath=(//span[@class='colt-site-component-collapse fa fa-plus-square-o'])[1]";

			public static final String OtherTab="@xpath=//a[text()='Other']";
			
			public static final String SelectSiteSearchAccess="@xpath=//span[text()='Select a site:']//span";
				
			public static final String StreetNameAccess="@xpath=//span[text()='Street Name']/..//input";
							
			public static final String CountryAccess="@xpath=//span[text()='Country']/..//input";
							
			public static final String CityTownAccess="@xpath=//span[text()='City / Town']/..//input";
											
			public static final String PostalCodeAccess="@xpath=//span[text()='Postal Code']/..//input";
													
			public static final String PremisesAccess="@xpath=//span[text()='Premises']/..//input";
															
			public static final String SearchButtonAccess="@xpath=//div[@id='colt-site-selection-btn-bar']/button"; 
			
			public static final String SelectPickAddressAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
			
			public static final String PickAddressButtonAccess="@xpath=//button[text()='Pick Address']";
			
			public static final String SelectPickBuildingAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
			
			public static final String PickBuildingButtonAccess="@xpath=//button[text()='Pick Building']";
			
					public static final String SelectPickSiteAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
					
					public static final String PickSiteButtonAccess="@xpath=//button[text()='Pick Site']";
					
					public static final String PortIDDropDown="@xpath=(//span[text()='Port ID']/..//span)[3]";
					
			//		public static final String PortId="@xpath=//li[text()='Port 1']";
					public static final String PortId="@xpath=//li/*[text()='Port 1']"; //UPdated2020

					
					public static final String TrafficDirectionDropDown="@xpath=(//*[contains(text(),'Traffic Direction')]/following-sibling::*[@class='siebui-icon-dropdown applet-form-combo applet-list-combo'])[2]";
			//		public static final String TrafficDirection="@xpath=//li[text()='Incoming calls to the customer']";
					public static final String TrafficDirection="@xpath=//li/*[text()='Incoming calls to the customer']"; //Updated2020

					public static final String CabinetTypeDropDown="@xpath=(//span[text()='Cabinet Type']/..//span)[3]";
			//		public static final String Cabinet_Type="@xpath=//li[text()='Existing Colt Cabinet']";
					public static final String Cabinet_Type="@xpath=//li/*[text()='Existing Colt Cabinet']"; //UPdated 2020

					public static final String Cabinet_Id="@xpath=//*[contains(text(),'Cabinet_ID')]//following-sibling::input";
					public static final String Shelf_ID="@xpath=//*[contains(text(),'Shelf_ID')]//following-sibling::input";
					public static final String SIPShowfullinfo="@xpath=(//a[text()='Show full info'])[2]";
					public static final String Isdnport="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
					public static final String PortIDDropDownAccess="@xpath=(//span[text()='Port ID']/..//span)[9]";																														
				//	public static final String PortIDDropDownAccessValue="@xpath=(//li[text()='Port 3'])[2]";
					public static final String PortIDDropDownAccessValue="@xpath=(//li/*[text()='Port 3'])[2]"; //UPDated2020

					public static final String PortIDDropDownAccess2="@xpath=(//span[text()='Port ID']/..//span)[12]";
				//	public static final String PortIDDropDownAccessValue2="@xpath=(//li[text()='Port 2'])[3]";
					public static final String PortIDDropDownAccessValue2="@xpath=(//li/*[text()='Port 2'])[3]"; //UPdated2020

					public static final String TrafficDirectionDropDown2="@xpath=(//span[text()='Traffic Direction']/..//span)[12]";
				//	public static final String TrafficDirection1="@xpath=(//li[text()='Incoming calls to the customer'])[2]";
					public static final String TrafficDirection1="@xpath=(//li/*[text()='Incoming calls to the customer'])[2]";

					public static final String SipTDMGateway="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[2]";
					public static final String PortIDDropDownAccess3="@xpath=(//span[text()='Port ID']/..//span)[15]";
				//	public static final String PortIDDropDownAccessValue3="@xpath=(//li[text()='Port 2'])[4]";
					public static final String PortIDDropDownAccessValue3="@xpath=(//li/*[text()='Port 2'])[4]"; //UPdated2020

					public static final String CabinetTypeDropDown2="@xpath=(//span[text()='Cabinet Type']/..//span)[9]";
				//	public static final String CabinetType1="@xpath=(//li[text()='Existing Colt Cabinet'])[2]";
					public static final String CabinetType1="@xpath=(//li/*[text()='Existing Colt Cabinet'])[2]"; //Updated2020

					public static final String Cabinet_Id1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[17]";
					public static final String ShelfID1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[18]";
					//public static final String CrossButtonVLV="@xpath=//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";
					public static final String CrossButtonVLV="@xpath=//*[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";
					public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";
					public static final String TypeoflineDropDown="@xpath=(//span[text()='Type of Lines']/..//span)[3]";
				//	public static final String TypeofLinevalue="@xpath=//li[text()='PRI']";
				//	public static final String NumberOfLines="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[2]";
							
					public static final String CRC4DropDown="@xpath=(//span[text()='CRC4']/..//span)[3]";
				//	public static final String CRC4Value="@xpath=//li[contains(text(),'MF')]";
					public static final String CRC4Value="@xpath=//li/*[contains(text(),'MF')]"; //updated2020

					public static final String MaxChannelDropDown="@xpath=(//span[text()='Max Channels (PRI)']/..//span)[3]";
				//	public static final String MaxChannelvalue="@xpath=//li[text()='30']";
					public static final String VoiceOptionshowinfo="@xpath=(//a[text()='Show full info'])[1]";
					public static final String ColtDDIRanges="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
					//public static final String POrtInDDIRanges="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
					//public static final String ResellerConfiguration="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])";
					public static final String UnratedCDRs="@xpath=//span[@class='colt-component-check-available fa fa-circle-o']";
							
					
				//	<Site_Contact>//button[@title="Pick Contact:OK"]</Site_Contact>
					public static final String NOofsingleColtDDIs="@xpath=//span[text()='Number of single Colt DDIs']/..//input";
					public static final String Noof10ColtDDIs="@xpath=//span[text()='Number of 10 Colt DDI Ranges']/..//input";
					public static final String Noof100coltDDIs="@xpath=//span[text()='Number of 100 Colt DDI Ranges']/..//input";
					public static final String Noof1000coldDDIs="@xpath=//span[text()='Number of 1000 Colt DDI Ranges']/..//input";
					public static final String NoOfsinglePOrtedDDIs="@xpath=//span[text()='Number of single Ported In DDIs']/..//input";
					public static final String Noof10PortedDDIs="@xpath=//span[text()='Number of 10 Ported In DDI Ranges']/..//input";
					public static final String NOof100PortedDDIs="@xpath=//span[text()='Number of 100 Ported In DDI Ranges']/..//input";
					public static final String Noof1000PortedDDIs="@xpath=//span[text()='Number of 1000 Ported In DDI Ranges']/..//input";
		
					public static final String TrunkConfigurationplus1="@xpath=//div[starts-with(@id,'interactive_item')]//span[@class='colt-site-component-collapse fa fa-plus-square-o'][1]";
					public static final String outsidebuissnesshourinstallation1="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
					public static final String ClickheretoSaveAccess1="@xpath=//a[text()='Click here to ']";
					public static final String ExternalAccessServiceReference="@xpath=//span[text()='External Access Service Reference']/following-sibling::input";
		
					public static final String SIPGatewaySetting="@xpath=(//span[text()='SIP TDM Gateway']/..//span)[5]";
			//		public static final String AttributeValueCPEName="@xpath=//td[text()='CPE Name']/..//td/following-sibling::td/following-sibling::td/inputs";
					public static final String AttributeValueCPEName="@xpath=//td[text()='CPE Name']/..//td/following-sibling::td/following-sibling::td/input";
					
					public static final String SIPGatewaySetting1="@xpath=(//span[text()='SIP TDM Gateway']/..//span)[10]";
			
					
					
				//	public static final String TypeofLinevalue="@xpath=//*[contains(@style,'block')]//li[text()='PRI']";
					//public static final String TypeofLinevalue="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='PRI']";//updated2020
					public static final String TypeofLinevalue="@xpath=//li/*[text()='PRI']";
					public static final String NumberOfLines="@xpath=//span[text()='Number of Lines']/following-sibling::input";
				//	public static final String MaxChannelvalue="@xpath=//*[contains(@style,'block')]//li[text()='30']";
					//public static final String MaxChannelvalue="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='30']";//updated2020
					public static final String MaxChannelvalue="@xpath=//li/*[text()='30']";
					
					public static final String POrtInDDIRanges="@xpath=//span[text()='Port In DDI Ranges']/..//span[1]";
					public static final String ResellerConfiguration="@xpath=//span[text()='Reseller Configuration']/..//span[1]";
				
					public static final String Cabinet_Id1new="@xpath=(//*[contains(text(),'Cabinet_ID')]//following-sibling::input)[2]";
					public static final String Shelf_Id1new="@xpath=(//*[contains(text(),'Shelf_ID')]//following-sibling::input)[2]";
		}
		
		public static class VoiceFeatureTab
		{
			
			
			public static final String VoicefeaturesTab="@xpath=//a[text()='Voice Features']";
			public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";
			public static final String showfullinfo="@xpath=//a[text()='Show full info']";
			public static final String ByPassNumber="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
			public static final String CallBarring="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
			public static final String CallBarringDropDown="@xpath=(//span[text()='Call Barring Option']/..//span)[3]";
		//	public static final String Callbarringvalue="@xpath=//li[text()='Profile 13']";
			public static final String Callbarringvalue="@xpath=//li/*[text()='Profile 13']"; //updated2020

			public static final String Connectivity="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
			public static final String carrierHotel="@xpath=//span[text()='Carrier Hotel Cross Connect']/parent::div/span[1]";
			public static final String FastTrack="@xpath=//span[text()='Fast Track']/parent::div/span[1]";
			public static final String InternalCabling="@xpath=//span[text()='Internal Cabling']/parent::div/span[1]";
			public static final String LOngLining="@xpath=//span[text()='Long Lining']/parent::div/span[1]";
			public static final String DisasterRecovery="@xpath=//span[text()='Disaster Recovery']/parent::div/span[1]";
			public static final String NumberOfDrPlans="@xpath=(//span[text()='Number of DR Plans']/..//span)[3]";
		//	public static final String NumberofDRplansValue="@xpath=//li[text()='3DR']";
			public static final String NumberofDRplansValue="@xpath=//li/*[text()='3DR']";//updated2020

			public static final String InboundCallRerouting="@xpath=//span[text()='Inbound Call Rerouting']/parent::div/span[1]";
			public static final String DestinationNumberforRerouting="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[8]";
			public static final String MinimumSpendCommitment="@xpath=//span[text()='Minimum Spend Commitment']/parent::div/span[1]";
			public static final String PartialNumberReplacement="@xpath=//span[text()='Partial Number Replacement']/parent::div/span[1]";
	
			//public static final String CrossButtonVLV="@xpath=(//button[@title='Close'])[3]";

			public static final String CrossButtonVLV="@xpath=//*[@class='ui-button-icon ui-icon ui-icon-closethick']//button[@title='Close']";	
			
			
			/*		
			public static final String internationalCode="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[5]";
			public static final String LOcalAreaCode="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[6]";
			public static final String NUmberRangeStart="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[7]";
			public static final String NumberRangeEnd="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[8]";
			public static final String ReplacementLOcalAreaCode="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[9]";
			public static final String ReplacementMainNumber="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[10]";
			public static final String ReplacementNumberRangeStart="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[11]";
			public static final String ReplacementNumberRangeEnd="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[12]";
			public static final String PartialNumberConfiguration="@xpath=(//span[text()='Partial Number Replacement Configuration']/parent::div/span[1])[2]";
			public static final String internationalCode1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[13]";
			public static final String LOcalAreaCode1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[14]";
			public static final String NUmberRangeStart1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[15]";
			public static final String NumberRangeEnd1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[16]";
		*/
															
															
			//public static final String ReplacementLOcalAreaCode1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[17]";
			//public static final String ReplacementMainNumber1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[18]";
			//public static final String ReplacementNumberRangeStart1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[19]";
			//public static final String ReplacementNumberRangeEnd1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[20]";
			public static final String ServiceActivationSupport="@xpath=//span[text()='Service Activation Support']/parent::div/span[1]";
			public static final String LanguageDropDown="@xpath=(//span[text()='Language']/..//span)[3]";
		//	public static final String LanguageValue="@xpath=//li[text()='English']";
			public static final String LanguageValue="@xpath=//li/*[text()='English']"; //updated2020

			public static final String TelephoneDirectoryServices="@xpath=//span[text()='Telephone Directory Services']/parent::div/span[1]";
			public static final String DisasterRecoveryContact="@xpath=//a[text()='Disaster Recovery Contacts']";
			public static final String DisasterRecoveryAdd="@xpath=(//button[@title='Add DR Contact:New'])";
			public static final String DisasterAdd2="@xpath=//table[@summary='Add DR Contact']//tr[@id='2']";
			public static final String DisasterRecoveryOk="@xpath=(//button[@title='Disaster Recovery Contact:OK'])[1]";
	//		public static final String CrossButtonVLV="@xpath=(//button[@title='Close'])[4]";
			public static final String ClickSaveConfig="@xpath=(//span[@class='colt-infopanel-moredetails-msg']//a[text()='Click here to save your product configuration.'])";
		//	public static final String SIPVoiceFeatureInput="@xpath=//span[text()='value']//following-sibling::input";
			
			public static final String SIPCheckBoxCount="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])";
			public static final String SIPCheckBox="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[value]";
			//public static final String CrossButton1="@xpath=//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";
			public static final String CrossButton1="@xpath=//*[contains(@class,'colt-save-level')]//button[@title='Close']";
		//	public static final String SIPVoiceFeatureInput="@xpath=//span[text()='value']//following-sibling::input";

			public static final String DisasterRecoveryContactAdd="@xpath=//span//button[@title='Disaster Recovery Contact:Add >']";
			public static final String DisasterAdd3="@xpath=//table[@summary='Add DR Contact']//tr[@id='3']";


			public static final String Spinner="@xpath=(//i[contains(@class,'spinner')])[1]";


			public static final String internationalCode="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[9]";
			public static final String LOcalAreaCode="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[10]";
			public static final String NUmberRangeStart="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[11]";
			public static final String NumberRangeEnd="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[12]";
			public static final String ReplacementLOcalAreaCode="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[13]";
			public static final String ReplacementMainNumber="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[14]";
			public static final String ReplacementNumberRangeStart="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[15]";
			public static final String ReplacementNumberRangeEnd="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[16]";
			public static final String PartialNumberConfiguration="@xpath=(//span[text()='Partial Number Replacement Configuration']/parent::div/span[1])[2]";
			public static final String internationalCode1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[17]";
			public static final String LOcalAreaCode1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[18]";
			public static final String NUmberRangeStart1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[19]";
			public static final String NumberRangeEnd1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[20]";
		
			public static final String SIPVoiceFeatureInput="@xpath=//span[text()='Value']//following-sibling::input";
			//public static final String CrossButton1="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//following-sibling::button[@title='Close']";
		
			public static final String ReplacementLOcalAreaCode1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[21]";
			public static final String ReplacementMainNumber1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[22]";
			public static final String ReplacementNumberRangeStart1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[23]";
			public static final String ReplacementNumberRangeEnd1="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input'])[24]";
		}
		
		
		
		public static class AlertPopUp
		{
			public static final String AlertAccept="@xpath=//span[text()='Ok']";
		
		
		
		}
		
	/*	public static class EnterDateInFooter
		{
			public static final String AlertAccept="@xpath=//span[text()='Ok']";
		
			public static final String OrderDates="@xpath=//a[text()='Order Dates']";
			
			public static final String OrderSignedDate="@xpath=//input[@aria-label='Order Signed Date']";
			public static final String CustomerRequestedDate="@xpath=//input[@aria-labelledby='Customer_Requested_Date1_Label']";
			public static final String ColtActualDate="@xpath=//input[@aria-labelledby='COLT_Actual_RFS_Date_Label']";
			public static final String OrderReceivedDate="@xpath=//input[@aria-labelledby='COLT_Order_Received_Date_Label']";

			public static final String ColtPromissday="@xpath=//input[@aria-labelledby='Colt_Promised_Date_Label']";

		}*/
		
		/*public static class EnterBillingDateInFooter
		{
			public static final String Billing="@xpath=//a[text()='Billing']";
			public static final String ContractRenewalFlag="@xpath=//input[@aria-label='Contract Renewal Flag']";
			public static final String ContractTerm="@xpath=//input[@aria-label='Contract Term']"; 
			public static final String SelectContractTerm="@xpath=//li[text()='12']";
			public static final String BillingStartDateAccess="@xpath=//input[@aria-label='Billing Start Date']";
			public static final String POStartDateAccess="@xpath=//input[@aria-label='PO Start Date']"; 
			public static final String POEndDateAccess="@xpath=//input[@aria-label='PO End Date']";



		}*/
		
		public static class ClickHereSave
		{
			public static final String ClickheretoSaveAccess="@xpath=(//a[contains(text(),'Click here to')])[1]";

		}
		

		public static class EnterServiceChargeInFooter
		{
			
			public static final String ExpandAllButton="@xpath=//button[@title='Service Charges:Expand All']";
			public static final String BillingLastRow="@xpath=//table[@summary='Service Charges']/parent::*//tr[last()]";
			public static final String BillingRow="@xpath=//table[@summary='Service Charges']/parent::*//tr";
			//public static final String BillingRowAmount="@xpath=(//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5])[Value]";
			//public static final String BillingRowAmountInput="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5]/input";
			public static final String BillingRowAmount="@xpath=(//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5])[Value]";
			public static final String BillingRowAmountInput="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[5]//input";
			//public static final String BillingRowAmountInput="@xpath=//td[contains(@id,'COLT_Amount')][Value]";
			public static final String BillingRowBCNInput="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[6]//input";
			public static final String BillingRowBCN="@xpath=(//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[6])[Value]";
			public static final String BCNNRCSearch="@xpath=//td[@class='siebui-popup-filter']//button[@title='Billing Profile:Go']"; 
			public static final String BCNInstallationChargeNRCInput="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			public static final String FirstLineitem="@xpath=(//table[@summary='Service Charges']/parent::*//tr)[2]";
			public static final String ClickNextPage="@xpath=(//span[@title='Next record set'])[1]";
			public static final String ClickNextPage1="@xpath=//td[contains(@style,'default')]//span[@title='Next record set']";
			public static final String BCNSearchClick="@xpath=//table[@summary='Service Charges']/parent::*//tr[@tabindex='-1']//td[6]/span";
			public static final String ServiceTable="(//table[@class='ui-pg-table'])[4]";
		}

		
		public static class ServiceTab
		{
			
			public static final String ServiceOrderTab="@xpath=//a[text()='Service Order']";
			public static final String InputServiceOrder="@xpath=//input[@aria-label='Starting with']";
			public static final String ServiceOrderGo="@xpath=(//button[@aria-label='Service Order List:Go'])[2]";
			public static final String ModifyButtonClick="@xpath=//span[text()='Modify']";

			public static final String ModifyBtn="@xpath=//button[@aria-label='Service Orders:Modify]";
			public static final String OpportunityNo="@xpath=//*[(@aria-labelledby='Opportunity_Number_Label')]";
			public static final String RequestReceivedDate="@xpath=//*[(@aria-label='Request Received Date')]";
			public static final String ModifyOrderNumber="@xpath=//a[@name='Order Number']";
			public static final String ServiceOrderClickOn="@xpath=//a[contains(@name,'Order Num')]";
			public static final String OrderSubTypeSearch="@xpath=//div[@class='mceGridField siebui-value mceField']/span[@aria-label='Multiple Selection Field']"; 
			public static final String InputOrderSubType="@xpath=//input[@name='COLT_Order_Item_Sub_Type']";
			public static final String SubmitSubOrderType="@xpath=//button[@title='Service Order Sub Type:OK']";
			public static final String AddOrderSubType="@xpath=//button[@title='Service Order Sub Type:New']";
		//	public static final String InstallTimeSelectAccess="@xpath=//li[text()='Business Hours']";
			public static final String InstallTimeSelectAccess="@xpath=//*[contains(@style,'block')]//li[text()='Business Hours']";
			public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";
			public static final String InstallTimeDropdownAccess="@xpath=(//span[text()='Install Time']/..//span)[3]";
			public static final String ExistingCapacityLeadTimePrimary="@xpath=//input[@aria-labelledby='COLT_Existing_Capacity_Lead_Time_Code_Label']";
			public static final String InstalltimeBside="@xpath=(//span[text()='Install Time']/..//span)[6]";

			public static final String MaintenancePartySearch="@xpath=//input[@aria-label='Maintenance Party']/..//span";
			

		//	public static final String AccountStatus="@xpath=//li[text()='Account Status']";
			public static final String AccountStatus="@xpath=//li/*[text()='Account Status']";//updated2020

			public static final String AccountStatusSubmit="@xpath=//button[@aria-label='Pick Account:OK']";

			public static final String InputAccountStatus="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			
			public static final String AccountStatusSearch="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']/../following-sibling::span/*[@title='Pick Account:Go']";

			
			public static final String MaintenancePartyPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
			
			public static final String MaintenancePartyContactPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";

		//	public static final String MaintenanceLastName="@xpath=//li[text()='Last Name']";
			public static final String MaintenanceLastName="@xpath=//li/*[text()='Last Name']";//updated2020

			public static final String InputMaintenanceLastName="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			  
			public static final String InputMaintenanceLastNameSearch="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']/../following-sibling::span/*[@title='Pick Contact:Go']";
			  
			public static final String MaintenancePartyContactSubmit="@xpath=//button[@aria-label='Pick Contact:OK']";
			
			public static final String InputPartyAddresStreetName="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			
			public static final String InputPartyAddresStreetNameSearch="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']/../following-sibling::span/*[@title='Pick Address:Go']";
			public static final String MaintenancePartyAddressSubmit="@xpath=//button[@aria-label='Pick Address:OK']";
			public static final String MaintenancePartyAddress="@xpath=//input[@aria-label='Maintenance Party Address']/..//span";
			public static final String MaintenancePartyAddresPopupDropdown="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";


		//	public static final String PartyAddresStreetName="@xpath=//li[text()='Street Name']"; 
			public static final String PartyAddresStreetName="@xpath=//li/*[text()='Street Name']";  //updated2020

			public static final String COLTProContactFullNameLabel="@xpath=//input[@aria-labelledby='COLT_ProContact_FullName_Label']/following-sibling::span";
		
			public static final String OkButton="@xpath=//button[@aria-label='Pick Contact:OK']";
			public static final String coltformalertsokbtn="@xpath=//button[@id='colt-formalerts-ok-btn1']";
			
			public static final String SiteABSelection ="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
		}
			
		public static class installationTimeUpdate
		{
			public static final String ClickDropdownB="@xpath=(//span[text()='Value']/parent::*/span)[3]";
		//	public static final String SelectValueDropdown="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			public static final String SelectValueDropdown="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020;

			public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*/span)[2]";
			public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
		//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		//	public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020;
			public static final String SiteABSelection="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
			public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";




			
		}
		

		public static class ModTechModCommWaveAndLine
		 {
			public static final String ClickheretoSaveAccess="@xpath=//a[text()='Click here to save your product configuration.']";	
			public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";	
		//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			//public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated2020

			public static final String SiteAInput="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";	
			public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";	
			public static final String SiteBInput="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";	
			public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";	
		//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";	
			//public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";	//updated2020
			public static final String MiddleLi="@xpath=//li/*[text()='Value']";
			public static final String SiteADropdownClick1="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
			public static final String SiteABSelection="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
		 }
		
		
	public static class enterMandatoryDetailsInMiddleApple
			{
				public static final String SiteADropdownClick ="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
				public static final String SiteAInput ="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";
						
			}
	/*	public static class WaveLineMidleApplet
		{
			public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::/span[2]"; //Customers Menu
			public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			public static final String SaveWaveLineMidApplet="@xpath=//div[@id='colt-s-order-connection-header']//a[text()='Save']";
		}*/
		
		//public static class addSiteADetailsObj
		//{
			//public static final String SearchAddressSiteA ="@xpath=//div[@id='colt-site-left']//span[@title='Select a site']";
			//public static final String StreetNamerfs ="@xpath=//input[@name='streetName']";
			//public static final String Country ="@xpath=//div[contains(@style,'block')]//span[text()='Country']/following-sibling::span";
			//public static final String SiteABSelection ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			//public static final String City ="@xpath=//input[@name='cityTown']";
			//public static final String PostalCode ="@xpath=//input[@name='postalZipCode']";
			//public static final String Premises ="@xpath=//input[@name='premisesNumber']";
			//public static final String Search ="@xpath=//div[@id='colt-site-selection-btn-bar']//button[text()='Search']";
			//public static final String SearchAddressRowSelection ="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][1]";
			//public static final String PickAddress ="@xpath=//table[@id='address_table']/tbody/tr[2]";
			//public static final String PickBuilding ="@xpath=//table[@id='address_table']/tbody/tr";
			//public static final String PickSite ="@xpath=//div[@id='PopupDiv']//table[@id='address_table']/tbody/tr[1]";
			//public static final String ServicePartySearchAccess ="@xpath=//span[text()='Service Party']/..//span[2]";
			//public static final String ServicePartyDropdownAccess ="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
			//public static final String SiteContactDropdownAccess ="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/parent::*/span";
			//public static final String InputPartyNameAccess ="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			//public static final String PartyNameSearchAccess ="@xpath=//td[@class='siebui-popup-filter']//button";
			//public static final String PartyNameSubmitAccess ="@xpath=//button[@aria-label='Pick Account:OK']";
			//public static final String SaveOrderChanges ="@xpath=//a[text()='Click here to save your order changes.']";
			//public static final String SiteContactSearchAccess ="@xpath=//span[text()='Site Contact']/..//span[2]";
			//public static final String InputSiteNameAccess ="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			//public static final String LastNameSiteSearchAccess ="@xpath=//td[@class='siebui-popup-filter']//button";
			//public static final String LastNameSiteSubmitAccess ="@xpath=//button[@aria-label='Pick Contact:OK']";
			//public static final String IpGurdianSave ="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
		//}
		public static class addSiteBDetailsObj
		{
			public static final String SearchAddressSiteB ="@xpath=//div[@id='colt-site-right']//span[@title='Select a site']";
			public static final String ServicePartySearchSiteB ="@xpath=//div[@id='colt-site-right']//span[text()='Service Party']/..//span[2]";
	        public static final String SiteContactSearchSiteB="@xpath=//div[@id='colt-site-right']//span[text()='Site Contact']/..//span[2]"; 
		}
		public static class PrivateEthernetMiddleApletObj
		{
			public static final String MiddleDropDown ="@xpath=//span[text()='Value']/parent::*/span[2]";
		//	public static final String MiddleLi ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			//public static final String MiddleLi ="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020;
			public static final String MiddleLi="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
			public static final String BMiddleLi="@xpath=(//li/*[text()='Value'])[2]";
			public static final String ShowFullinfo="@xpath=//a[text()='Click to Show Full info']";

		}
		public static class PrivateEthernetEntryObj
		{
		//	public static final String PopDropdownClick ="@xpath=//div[contains(@style,'block')]//span[text()='Value']/following-sibling::span";
		//	public static final String PopDropdownClick ="@xpath=//*[not(contains(@style,'display'))]//span[text()='Value']/following-sibling::span";//updated2020
			public static final String PopDropdownClick ="@xpath=//*[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//span[text()='Value']/following-sibling::span";
		//	public static final String InsideDropdownValues ="@xpath=//li[text()='Value']";
			public static final String InsideDropdownValues ="@xpath=//li/*[text()='Value']";//updated2020

			public static final String MiddleDropDown ="@xpath=//span[text()='Value']/parent::*/span[2]";
			
		}
		public static class DiversityCircuitEntryObj
		{
			//public static final String PopInput ="@xpath=//div[contains(@style,'block')]//span[text()='Value']/parent::div/input";
		//	public static final String PopInput ="@xpath=//*[not(contains(@style,'display'))]//div//span[text()='Value']/parent::div/input";

		//	public static final String PopClose ="@xpath=//div[contains(@style,'block')]//button[@title='Close']";
			public static final String PopClose ="@xpath=//*[not(contains(@style,'display'))]//button[@title='Close']";//updated2020
			public static final String PopInput ="@xpath=//*[(@class='colt-attribute colt-attr-req')]//span[text()='Value']/parent::div/input";
			
		}
		public static class  GetReferenceObj
		{
			public static final String GetReference ="@xpath=//button[text()='Get Reference']";
			public static final String CircuitReferenceValue ="@xpath=//input[@class='colt-attribute-input siebui-ctrl-input colt-popup-link-ctrl']";

		}
		public static class SiteAServicePartyObj
		{
			public static final String AlertAccept ="@xpath=//span[text()='Ok']";
			public static final String SiteADropdownClick ="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";

		}
		public static class PickServicePartyObj
		{
		/*	//public static final String PickAccountDropdown ="@xpath=//div[contains(@style,'block')]//input[@aria-labelledby='PopupQueryCombobox_Label']/following-sibling::span";
			public static final String PickAccountDropdown ="@xpath=//*[not(contains(@style,'display'))]//input[@aria-labelledby='PopupQueryCombobox_Label']/following-sibling::span";
			//public static final String AList ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']"; 
			public static final String AList="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; //Update IP2020
			public static final String PickAccountValue ="@xpath=//*[not(contains(@style,'display'))]//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			public static final String PickAccountGo ="@xpath=(//*[not(contains(@style,'display'))]//button[@title='Pick Account:Go'])[2]";
			public static final String PickAccountOk ="@xpath=//*[not(contains(@style,'display'))]//button[@title='Pick Account:OK']";
			*/
			
			public static final String PickAccountDropdown ="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
			public static final String AList="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; //Update IP2020
			public static final String PickAccountValue ="@xpath=//div[(@class='ui-dialog-content ui-widget-content')]//input[@aria-labelledby='PopupQuerySrchspec_Label']";
			public static final String PickAccountGo ="@xpath=(//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Account:Go'])[2]";
			public static final String PickAccountOk ="@xpath=//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Account:OK']";
		
		}
		public static class SiteBServicePartyObj
		{
			public static final String SiteBDropdownClick ="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";

		}
		
		public static class PickSiteContactPartyObj
		{
			//public static final String PickContactGo ="@xpath=(//div[contains(@style,'block')]//button[@title='Pick Contact:Go'])[2]";
			public static final String PickContactGo ="@xpath=(//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Contact:Go'])[2]";
		//	public static final String PickContactOk ="@xpath=//div[contains(@style,'block')]//button[@title='Pick Contact:OK']";
			public static final String PickContactOk ="@xpath=//div[(@class='ui-dialog-content ui-widget-content')]//button[@title='Pick Contact:OK']";
		
		}
		public static class SearchSiteAObj
		{
			public static final String LeftSiteSearch="@xpath=//div[@id='colt-site-left']//span[@title='Select a site']";

		}
		public static class SearchSiteBObj
		{
			public static final String RightSiteSearch="@xpath=//div[@id='colt-site-right']//span[@title='Select a site']";

		}
		public static class SearchSiteAEntryObj
		{
			public static final String SearchInput="@xpath=//span[text()='Value']/parent::div/input";
			public static final String SearchDropdown="@xpath=//span[text()='Value']/following-sibling::span";
			public static final String SearchButton="@xpath=//button[@class='colt-siteselection-button colt-primary-btn']";
			
		}
		public static class AEndSiteObj
		{
			public static final String AccessTypeDropdownAccess="@xpath=(//span[text()='Access Type']/..//span)[3]";
			//public static final String AccesstypeOffnet="@xpath=//li[text()='AccessTypeValue']";
		//	public static final String AccesstypeOffnet="@xpath=//li/*[text()='AccessTypeValue']";//Update IP2020
			public static final String AccesstypeOffnet="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//Update IP2020
			public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
			public static final String AEndSiteDropDown="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
			//public static final String AList="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			public static final String AList="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//Update IP2020
			public static final String ThirdpartyaccessproviderDropDown="@xpath=(//span[text()='Third Party Access Provider']/..//span)[3]";
			//public static final String ThirdpartyaccessProvidervalue="@xpath=//li[text()='Value']";
			//public static final String ThirdpartyaccessProvidervalue="@xpath=//li/*[text()='Value']";//Update IP2020
			public static final String ThirdpartyaccessProvidervalue="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']"; //Update IP2020
			public static final String Thirdpartyconectionreference="@xpath=//span[text()='Value']/..//input";
			public static final String Thirdpartyconectionreference1="@xpath=//span[text()='3rd Party Connection Reference']/..//input";
			//public static final String MiddleLi ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//Update IP2020
			public static final String ThirdpartyDropDown ="@xpath=(//span[text()='Third Party SLA Tier']/..//span)[3]";
			public static final String SiteTypeDropDown ="@xpath=(//span[text()='Site Type']/..//span)[3]";
			//public static final String SiteTypeDropDownvalue="@xpath=//li[text()='Customer']";
			public static final String SiteTypeDropDownvalue="@xpath=//li/*[text()='Customer']";//Update IP2020
			public static final String AEndSiteInput="@xpath=(//span[text()='Value']/parent::div/input)[1]";
		
	
		}
		public static class BEndSiteObj
		{
//			public static final String BEndSiteDropDown="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
			public static final String BEndSiteDropDown="@xpath=(//span[text()='Value']/parent::*//span)[6]";
			//public static final String BList="@xpath=//li[text()='Value']";
			//public static final String BList="@xpath=//li/*[text()='Value']";//Update IP2020
//			/ul[contains(@style,'block')]//li[text()='Value']";
			public static final String BEndSiteInput="@xpath=(//span[text()='Value']/parent::div/input)[2]";
			public static final String BList="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
			public static final String ThirdpartyconectionreferenceB="@xpath=(//span[text()='3rd Party Connection Reference']/..//input)[2]";
		}

		public static class VoiceLineVObj
		{
			public static final String VoiceServiceCountryDropdownAccess="@xpath=(//span[text()='Voice Service Country']/..//span)[3]";
			//public static final String VoiceServiceInputValue="@xpath=//li[text()='Austria']";
			public static final String VoiceServiceInputValue="@xpath=//li/*[text()='Austria']";//Update IP2020
			public static final String EgressNumberFormatDropDownAccess="@xpath=(//span[text()='Egress Number Format']/..//span)[3]";
			//public static final String EgreeNUmberFormat="@xpath=//li[text()='E.164 All International with +']";
			public static final String EgreeNUmberFormat="@xpath=//li/*[text()='E.164 All International with +']";//Update IP2020
			public static final String TopologyDropDown="@xpath=(//span[text()='Topology']/..//span)[3]";
			//public static final String Topology="@xpath=//li[text()='Multi-Site']";
			public static final String Topology="@xpath=//li/*[text()='Multi-Site']";//Update IP2020
			public static final String TotalDDi="@xpath=//*[contains(text(),'Total Number of DDI')]/following-sibling::input[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate']";
			public static final String CustomerdefaultNumber="@xpath=//span[text()='Customer Default Number']/..//input";
			public static final String ClickheretoSaveAccess="@xpath=(//a[contains(text(),'Click here to')])[1]";
			public static final String Clicktoshowfullinfo="@xpath=//a[@id='colt-s-order-enabledfeatures-showfullinfo']";
			public static final String SiteContactlookup="@xpath=//*[@id='colt-site-datafields-sitecontact-input']//span[@class='siebui-icon-pick applet-form-pick applet-list-pick']";
			public static final String Site_Contact="@xpath=//button[@title='Pick Contact:OK']";
			public static final String ServicePartsearch="@xpath=//*[@id='colt-site-datafields-serviceparty-input']//span[@class='siebui-icon-pick applet-form-pick applet-list-pick']";
			public static final String PickAccntOk="@xpath=//button[@title='Pick Account:OK']";
			public static final String DeliveryTeamDropDown="@xpath=(//span[text()='Delivery Team Country']/..//span)[3]";
	//		public static final String DeliveryTeamCountry="@xpath=(//input[@class='colt-attribute-input siebui-ctrl-input ui-autocomplete-input'])[7]";
			public static final String DeliveryTeamCountry="@xpath=//*[@id='colt-site-datafields-deliveryteamcountry-input']//input";
			public static final String DeliveryTeamCity="@xpath=//*[@id='colt-site-datafields-deliveryteamcity-input']//input";
			public static final String PerformanceReporting="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[1]";
		//	public static final String WLECInvoicing="@xpath=(//span[@class='colt-component-check-available fa fa-circle-o'])[5]";
	//		public static final String Crossbutton="@xpath=(//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close'])[3]";
			public static final String Crossbutton="@xpath=//span[text()='Voice Line V']/following-sibling::button";
			public static final String ClickheretoSaveAccess1="@xpath=//a[text()='Click here to ']";
			public static final String DeliveryTeamCityDropDown="@xpath=(//span[text()='Delivery Team City']/..//span)[3]";
			public static final String WLECInvoicing="@xpath=//span[text()='WLEC Invoicing']/..//span[1]";
			
		}
		public static class IPAccessObj
		{
			public static final String capacitycheckreference="@xpath=//div[@id='colt-s-order-connection-attributes']/div[1]/input[1]";
			public static final String Inputossplatformflag="@xpath=//span[text()='OSS Platform Flag']/..//input";
		//	public static final String RouterTypeDropdownAccess="@xpath=(//span[text()='Router Type']/..//span)[3]";
			//public static final String SelectRouterTypeDropDownAccess="@xpath=//li[text()='No Colt Router']";
			public static final String SelectRouterTypeDropDownAccess="@xpath=//li/*[text()='No Colt Router']";//Update IP2020
			public static final String Layer3ResillanceDropdownAccess="@xpath=(//span[text()='Layer 3 Resilience']/..//span)[3]";
			//public static final String Layer3ResillancePartialSelectDropdownAccess="@xpath=//li[text()='Dual Access Unmanaged']";
			public static final String Layer3ResillancePartialSelectDropdownAccess="@xpath=//li/*[text()='Dual Access Unmanaged']";//Update IP2020
			public static final String ServiceBandwidthDropdownAccess="@xpath=(//span[text()='Service Bandwidth']/..//span)[3]";
			//public static final String ServiceBandwidthSelectAccess="@xpath=//li[text()='Value']";
			public static final String ServiceBandwidthSelectAccess="@xpath=//li/*[text()='Value']";//Update IP2020
			public static final String SelectSiteSearchAccess="@xpath=//span[text()='Select a site:']//span";
			public static final String StreetNameAccess="@xpath=//span[text()='Street Name']/..//input";
			public static final String CountryAccess="@xpath=//span[text()='Country']/..//input";
			public static final String CityTownAccess="@xpath=//span[text()='City / Town']/..//input";
			public static final String PostalCodeAccess="@xpath=//span[text()='Postal Code']/..//input";
			public static final String PremisesAccess="@xpath=//span[text()='Premises']/..//input";
			public static final String SearchButtonAccess="@xpath=//div[@id='colt-site-selection-btn-bar']/button";
			public static final String SearchButtonAccess1="@xpath=//button[@class='colt-siteselection-button colt-primary-btn']";
			public static final String SelectPickAddressAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
			public static final String PickAddressButtonAccess="@xpath=//button[text()='Pick Address']";
			public static final String PickBuildingButtonAccess="@xpath=//button[text()='Pick Building']";
			public static final String SelectPickSiteAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
			public static final String PickSiteButtonAccess="@xpath=//button[text()='Pick Site']";
			public static final String ServicePartySearchAccess="@xpath=//span[text()='Service Party']/..//span[2]";
			public static final String SelectPickBuildingAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
			//public static final String PartyNameAccess="@xpath=//li[text()='Party Name']";
			public static final String PartyNameAccess="@xpath=//li/*[text()='Party Name']";//Update IP2020
			//public static final String AccessTypeSelectAccess="@xpath=//li[text()='Colt Fibre']";
			public static final String AccessTypeSelectAccess="@xpath=//li/*[text()='Colt Fibre']";//Update IP2020
			public static final String AccesstechnologyDropdownAccess="@xpath=(//span[text()='Access Technology']/..//span)[3]";
			//public static final String AccesstechnologySelectAccess="@xpath=//li[text()='Ethernet over MSP']";
			public static final String AccesstechnologySelectAccess="@xpath=//li/*[text()='Ethernet over MSP']";//Update IP2020
			public static final String BuildingTypeDropdownAccess="@xpath=(//span[text()='Building Type']/..//span)[3]";
		//	public static final String BuildingTypeSelectAccess="@xpath=//li[text()='Existing Building']";
			public static final String BuildingTypeSelectAccess="@xpath=//li/*[text()='Existing Building']"; //updated IP2020
			public static final String CustomerSitePopStatusDropdownAccess="@xpath=(//span[text()='Customer Site Pop Status']/..//span)[3]";
			//public static final String CustomerSitePopStatusSelectAccess="@xpath=//li[text()='Existing']";
			public static final String CustomerSitePopStatusSelectAccess="@xpath=//li/*[text()='Existing']";//updated IP2020
			public static final String CabinetTypeDropdownAccess="@xpath=(//span[text()='Cabinet Type']/..//span)[3]";
			//public static final String CabinetTypeSelectAccess="@xpath=//li[text()='Existing Colt Cabinet']";
			public static final String CabinetTypeSelectAccess="@xpath=//li/*[text()='Existing Colt Cabinet']"; //updated IP2020
			public static final String CabinetID="@xpath=//span[text()='Cabinet ID']/..//input";
			public static final String shelfid="@xpath=//span[text()='Shelf ID']/..//input";
			public static final String Slotid="@xpath=//span[text()='Slot ID']/..//input";
			public static final String Physicalportid="@xpath=//span[text()='Physical Port ID']/..//input";
			
			public static final String PresentationInterfaceDropdownAccess="@xpath=(//span[text()='Presentation Interface']/..//span)[3]";
			//public static final String PresentationInterfaceSelectAccess="@xpath=//li[text()='value']";
			public static final String PresentationInterfaceSelectAccess="@xpath=//li/*[text()='Value']";//Update IP2020
			public static final String ConnectorTypeDropdownAccess="@xpath=(//span[text()='Connector Type']/..//span)[3]";
			//public static final String ConnectorTypeSelectAccess="@xpath=//li[text()='LC']";
			public static final String ConnectorTypeSelectAccess="@xpath=//li/*[text()='LC']";//Update IP2020
			public static final String FibreTypeDropdownAccess="@xpath=(//span[text()='Fibre Type']/..//span)[3]";
			//public static final String FibreTypeSelectAccess="@xpath=//li[text()='value']";
		//	public static final String FibreTypeSelectAccess="@xpath=//li/*[text()='value']";//Update IP2020
			public static final String PortRoleDropDown="@xpath=(//span[text()='Port Role']/parent::*//span)[3]";
			//public static final String PortValue="@xpath=//li[text()='Physical Port']";
			public static final String PortValue="@xpath=//li/*[text()='Physical Port']";//Update IP2020
			public static final String InstallTimeDropdownAccess="@xpath=(//span[text()='Install Time']/..//span)[3]";
			//public static final String InstallTimeSelectAccess="@xpath=//li[text()='Business Hours']";
			public static final String InstallTimeSelectAccess="@xpath=//li/*[text()='Business Hours']";//Update IP2020
			public static final String Accesstimewindow="@xpath=//span[text()='Access Time Window']/..//input";
			public static final String RouterCountryAccess="@xpath=(//span[text()='Router Country']/..//input)[1]";
			//public static final String RouterCountrySelect="@xpath=//li[text()='value']";
			//public static final String RouterCountrySelect="@xpath=//li/*[text()='Value']";//Update IP2020
			public static final String RouterCountrySelect="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";
			public static final String routermodel="@xpath=//span[text()='Router Model']/..//input";
			public static final String RouterSiteNameDropdownAccess="@xpath=(//span[text()='Site Name']/..//span)[3]";
		//	public static final String RouterSiteNameSelectAccess="@xpath=//li[text()='IE_D03-DA06_EAST WALL ROAD_UNIT 15-16_1_NODE']";
			public static final String RouterSiteNameSelectAccess="@xpath=//li/*[text()='IE_D03-DA06_EAST WALL ROAD_UNIT 15-16_1_NODE']";//Update IP2020
		//	public static final String RouterSiteNameSelectAccess1="@xpath=//li[text()='UK_E14-9RL_PRESTONS ROAD_UNIT C10-C12 10_G_PBP2']";
			public static final String RouterSiteNameSelectAccess1="@xpath=//li/*[text()='UK_E14-9RL_PRESTONS ROAD_UNIT C10-C12 10_G_PBP2']";//Update IP2020
			public static final String ClickShowFullInfoAccess="@xpath=//a[@id='colt-s-order-enabledfeatures-showfullinfo']";
			public static final String IPAdressingFormatDropdownAccess="@xpath=(//span[text()='IP Addressing Format']/..//span)[3]";
			//public static final String IPAdressingFormatSelectAccess="@xpath=//li[text()='IPv4 format only']";
			public static final String IPAdressingFormatSelectAccess="@xpath=//li/*[text()='IPv4 format only']";//Update IP2020
			public static final String IPV4AdressingTypeDropdownAccess="@xpath=(//span[contains(text(),'Addressing Type')]/..//span)[3]";
		//	public static final String IPV4AdressingTypeSelectAccess="@xpath=//li[text()='Provider Aggregated IP (PA)']";
			public static final String IPV4AdressingTypeSelectAccess="@xpath=//li/*[text()='Provider Aggregated IP (PA)']";//Update IP2020
			public static final String Crossbuttonforipaccess="@xpath=//span[text()='IP Access']/..//button";
			public static final String Secondarybtn="@xpath=//a[text()='Secondary']";
			public static final String BackupbandwidthDropDownAccess="@xpath=(//span[text()='Backup Bandwidth']/..//span)[3]";
		//	public static final String BackupBandwidthSelectValue="@xpath=//li[text()='3 Mbps']";
			public static final String BackupBandwidthSelectValue="@xpath=//li/*[text()='3 Mbps']";//Update IP2020
			public static final String ProceedButton="@xpath=//button[text()='Proceed']";
	 		public static final String CircuitReferenceAccess="@xpath=//button[@id='colt-circuitref-button']";
	 		public static final String CircuitReferenceValue="@xpath=//input[@class='colt-attribute-input siebui-ctrl-input colt-popup-link-ctrl']";
	 		public static final String Layer3ResillanceSelectDropdownAccess="@xpath=//li[text()='No Resilience']";
	 	//	public static final String Customersitepopupstatusoffnet="@xpath=//li[text()='NA']";
	 		public static final String Customersitepopupstatusoffnet="@xpath=//li/*[text()='NA']";//Update IP2020
	 	//	public static final String Customersitepopupstatusoffnet1="@xpath=//li[text()='Existing']//following-sibling::li[text()='NA']";
	 		public static final String Customersitepopupstatusoffnet1="@xpath=//li/*[text()='Existing']//following-sibling::li[text()='NA']";//Update IP2020
	 	//	public static final String Customersitepopupstatusoffnet2="@xpath=//li[text()='New']";
	 		public static final String Customersitepopupstatusoffnet2="@xpath=//li/*[text()='New']";//Update IP2020
	 		
	 		//public static final String ThirdpartySLATiervalue="@xpath=//li[text()='SLAValue']";
	 		public static final String ThirdpartySLATiervalue="@xpath=//li/*[text()='SLAValue']";//Update IP2020
	 		public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*/span)[2]";
	 	//	public static final String SelectValueDropdown="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
	 		public static final String SelectValueDropdown="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//Update IP2020

	 		public static final String RouterCountryAccess1="@xpath=(//span[text()='Router Country']/..//input)[1]//following-sibling::span[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']";
	 		public static final String routermodel1="@xpath=//span[text()='Router Model']/..//input//following-sibling::span[@class='siebui-icon-dropdown applet-form-combo applet-list-combo']";
	 	//	public static final String routermodelSelect="@xpath=//li[text()='value']";
	 		public static final String routermodelSelect="@xpath=//li/*[text()='Value']";//Update IP2020
	 		
	 		public static final String PresentationInterfaceinput="@xpath=//span[text()='Presentation Interface']/..//input";
			
	 		//public static final String AccesstechnologyEhternetValue="@xpath=//li[text()='Ethernet']";
	 		public static final String AccesstechnologyEhternetValue="@xpath=//li/*[text()='Ethernet']";//Update IP2020

	 	//	public static final String BuildingTypeOFFNetSelectAccess="@xpath=//li[text()='Off-net over OLO']";
	 		public static final String BuildingTypeOFFNetSelectAccess="@xpath=//li/*[text()='Off-net over OLO']";//Update IP2020
	 	//	public static final String CustomerSitePopStatusOFFnetSelectAccess="@xpath=//*[contains(@style,'block')]//li[text()='NA']";
	 	//	public static final String CustomerSitePopStatusOFFnetSelectAccess="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='NA']";//Update IP2020
	 		public static final String CustomerSitePopStatusOFFnetSelectAccess="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";//Update IP2020

	 		
	 		public static final String FibreTypeSelectAccess="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";//Update IP2020
	 		public static final String searchInput="@xpath=//li/div[text()='Value']";
	 		public static final String LastNameAccess="@xpath=//li/div[text()='Last Name']";
	 		public static final String NextHopIp="@xpath=//span[text()='Next Hop IP']/..//input";
	 		public static final String RouterTypeDropdownAccess="@xpath=(//span[text()='Value']/..//span)[3]";
		}
		
		/////////// 0306
		 public static class IPVPNSITEMiddleApplet
			{
				    public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[3]";
				  //  public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])";
				    public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])";  //Update IP2020
				    public static final String TextInput="@xpath=//span[text()='Value']/parent::*//input";
				    public static final String SelectSiteSearchAccess="@xpath=//span[text()='Select a site:']//span"; 
				    public static final String StreetNameAccess="@xpath=//span[text()='Street Name']/..//input";
					
					public static final String CountryAccess="@xpath=//span[text()='Country']/..//input";
									
					public static final String CityTownAccess="@xpath=//span[text()='City / Town']/..//input";
													
					public static final String PostalCodeAccess="@xpath=//span[text()='Postal Code']/..//input";
															
					public static final String PremisesAccess="@xpath=//span[text()='Premises']/..//input";
																	
					public static final String SearchButtonAccess="@xpath=//div[@id='colt-site-selection-btn-bar']//button[text()='Search']"; 
					
					public static final String SelectPickAddressAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
					
					public static final String PickAddressButtonAccess="@xpath=//button[text()='Pick Address']";
					
					public static final String SelectPickBuildingAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
					
					public static final String PickBuildingButtonAccess="@xpath=//button[text()='Pick Building']";
					
					public static final String SelectPickSiteAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
							
					public static final String PickSiteButtonAccess="@xpath=//button[text()='Pick Site']";

					public static final String ServicePartySearchAccess="@xpath=//span[text()='Service Party']/..//span[2]";
					public static final String ServicePartyDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/../span";
					public static final String SiteContactDropdownAccess="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']/parent::*/span";
					public static final String InputPartyNameAccess="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
					public static final String PartyNameSearchAccess="@xpath=//td[@class='siebui-popup-filter']//button";
					public static final String PartyNameSubmitAccess="@xpath=//button[@aria-label='Pick Account:OK']";
					public static final String SaveOrderChanges ="@xpath=//a[text()='Click here to save your order changes.']";
					public static final String SiteContactSearchAccess ="@xpath=//span[text()='Site Contact']/..//span[2]";
					public static final String InputSiteNameAccess ="@xpath=//input[@aria-labelledby='PopupQuerySrchspec_Label']";
					public static final String LastNameSiteSearchAccess ="@xpath=//td[@class='siebui-popup-filter']//button";
					public static final String LastNameSiteSubmitAccess ="@xpath=//button[@aria-label='Pick Contact:OK']";	
				//	public static final String PartyNameAccess ="@xpath=//li[text()='Party Name']";
					public static final String PartyNameAccess ="@xpath=//li/*[text()='Party Name']"; //Updated IP2020

					public static final String AccessTypeDropdownAccess="@xpath=(//span[text()='Access Type']/..//span)[3]";
				   // public static final String AccesstypeOffnet="@xpath=//li[text()='AccessTypeValue']"; 
				    public static final String AccesstypeOffnet="@xpath=//li/*[text()='AccessTypeValue']";//Updated IP2020
					public static final String ThirdPartyAccessProvDropDown="@xpath=(//span[text()='Third Party Access Provider']/..//span)[3]";
				   // public static final String ThirdpartyAccessOffnet="@xpath=//li[text()='AccessProviderValue']";
				    public static final String ThirdpartyAccessOffnet="@xpath=//li/*[text()='AccessProviderValue']";//Updated IP2020
				    public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
				    public static final String Thirdpartyconectionreference="@xpath=//span[text()='3rd Party Connection Reference']/..//input";
				    public static final String Bcpreference="@xpath=//span[text()='BCP Reference']/..//input";
				    public static final String SiteNameAlias="@xpath=//span[text()='Site Name Alias']/..//input";
					public static final String ThirdPartySlaTierDropDown="@xpath=(//span[text()='Third Party SLA Tier']/..//span)[3]";
				  //  public static final String ThirdPartySlaTierOffnet="@xpath=//li[text()='SlaTierValue']";
				    public static final String ThirdPartySlaTierOffnet="@xpath=//li/*[text()='SlaTierValue']";//Updated IP2020
				    public static final String CircuitReferenceAccess="@xpath=//button[@id='colt-circuitref-button']";
					public static final String ClickLink="@xpath=//a[text()='Value']";
					public static final String IPDetailsPlus="@xpath=//span[contains(@class,'fa-plus-square-o')]";
					public static final String SearchInput="@xpath=(//span[text()='Value']/parent::*//span)[3]";		  
					public static final String AccessPortList="@xpath=//tr[@class='colt-siteselection-datarow']";			  
				   // public static final String SubmitSubVPNList="@xpath=//button[text()='OK']";
				    public static final String SelectSubVPNList="@xpath=//td[@id='Sub VPN ID']";
					public static final String SubVPNID="@xpath=//span[@id='Sub VPN ID']";
					
					public static final String SubmitSubVPNList="@xpath=//*[@id='colt-site-selection-btn-bar']//button[text()='OK']";
				
			}
		 
		 public static class SIPTrunkingObj
			{
				public static final String VoiceServiceCountry="@xpath=//span[text()='Voice Service Country']/following-sibling::span";
			//	public static final String NewVoiceServiceCountry="@xpath=//li[text()='United Kingdom']";
				public static final String NewVoiceServiceCountry="@xpath=//li/*[text()='United Kingdom']";//Updated IP2020
				public static final String CallAdmissionControl="@xpath=//*[contains(text(),'Call Admission Control (Number of Channels')]//following-sibling::input";
				public static final String NumberOfSignallingTrunks="@xpath=//*[contains(text(),'Number of Signalling Trunks')]//following-sibling::input";
				public static final String EgressNumberFormatDropDownAccess="@xpath=(//span[text()='Egress Number Format']/..//span)[3]";
				//public static final String EgreeNUmberFormat="@xpath=//li[text()='E.164 All International with +']";
				public static final String EgreeNUmberFormat="@xpath=//li/*[text()='E.164 All International with +']";//Updated IP2020
				public static final String InvlidCLITreatmet="@xpath=(//*[@value='Allow'])";
				public static final String TotalNumberDDIs="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[3])";
				public static final String IncomingDDIs="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input ui-autocomplete-input'])[4])";
				public static final String RoutingSequence="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input ui-autocomplete-input'])[5])";
				public static final String SIPSiteContact="@xpath=//*[@id='colt-site-datafields-sitecontact-input']//span[@class='siebui-icon-pick applet-form-pick applet-list-pick']";
				public static final String ClickOK="@xpath=(//button[@title='Pick Contact:OK'])";
				public static final String ServiceParty="@xpath=//*[@id='colt-site-datafields-serviceparty-input']//span[@class='siebui-icon-pick applet-form-pick applet-list-pick']";
				public static final String OKSelection="@xpath=(//button[@title='Pick Account:OK'])";
				public static final String SIPDeliveryTeamNewCountry="@xpath=//*[@id='colt-site-datafields-deliveryteamcountry-input']//input";
				public static final String SIPDeliveryTeamNewCity="@xpath=//*[@id='colt-site-datafields-deliveryteamcity-input']//input";
				public static final String CloseSymbol="@xpath=//*[contains(text(),'SIP Trunking')]/parent::div//button[@title='Close']";
				public static final String Save="@xpath=//*[@id='colt-s-order-connection-header-btns']/a";
				public static final String SIPVoiceConfigExpandSymbol="@xpath=(//*[@class='colt-site-component-collapse fa fa-plus-square-o'])[1]";
				public static final String SIPTrunkSequence="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[4])";
				public static final String SIPTrunkName="@xpath=//*[contains(text(),'Trunk Name')]//following-sibling::input";
				public static final String AccessLineTypeDropDown="@xpath=(//span[text()='Access Line Type']/..//span)[3]";
				//public static final String AccessLineType="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				public static final String AccessLineType="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
				public static final String AccessServiceIdlookup="@xpath=//*[text()='Access Service ID']/following-sibling::span[@class='siebui-icon-pick']";
				public static final String SearchServiceIDbox="@xpath=//input[@aria-labelledby='PopupQueryCombobox_Label']";
				public static final String SearchText="@xpath=//td[@class='siebui-popup-filter']/span[4]/input";
				public static final String SearchButtonProcess="@xpath=//td[@class='siebui-popup-filter']/span[5]/button";
				public static final String SearchServiceIdOKButton="@xpath=//button[@title='Pick Service:OK']";
				public static final String SIPCustomerOriginatingIPv4="@xpath=//*[contains(text(),'Customer Originating IPv4 Address')]//following-sibling::input";
				public static final String SIPCustomerOriginatingIPv6="@xpath=//*[contains(text(),'Customer Originating IPv6 Address')]//following-sibling::input";
				public static final String SIPCustomerTerminatingIPv4="@xpath=//*[contains(text(),'Customer Terminating IPv4 Address')]//following-sibling::input";
				public static final String SIPCustomerTerminatingIPv6="@xpath=//*[contains(text(),'Customer Terminating IPv6 Address')]//following-sibling::input";
				public static final String SIPSignalingTransportProtocol="@xpath=//*[contains(text(),'Signalling Transport Protocol')]/following-sibling::span[@data-allowdblclick='true']";
				//public static final String SIPSignalingTransportProtocolDropdown="@xpath=(//li[text()='TCP'])";
				public static final String SIPSignalingTransportProtocolDropdown="@xpath=(//li/*[text()='TCP'])";//updated 2020
				public static final String SIPvoIPProtocol="@xpath=//*[contains(text(),'VoIP Protocol')]/following-sibling::span[@data-allowdblclick='true']";
			//	public static final String SIPvoIPProtocolDropDown="@xpath=(//li[text()='SIP'])";
				public static final String SIPvoIPProtocolDropDown="@xpath=(//li/*[text()='SIP'])";//updated 2020
				public static final String SIPEncryptionType="@xpath=//*[contains(text(),'Encryption Type')]//following-sibling::input";
				public static final String SIPProportionPercent="@xpath=//*[contains(text(),'Proportion')]//following-sibling::input";
				public static final String SIPTrunkSiteNameAlias="@xpath=//*[contains(text(),'Trunk Site Name Alias')]//following-sibling::input";
				public static final String ClickLink="@xpath=//a[text()='Value']";
				public static final String ClickToShowFullInfo="@xpath=//a[@id='colt-s-order-enabledfeatures-showfullinfo']";
				public static final String SIPNATRequired="@xpath=//*[contains(text(),'NAT Required')]//following-sibling::input";
				public static final String SIPNNIWANIPv4Address="@xpath=//*[contains(text(),'NNI WAN IPv4 Address')]//following-sibling::input";
				public static final String SIPNNIWANIPv6Address="@xpath=//*[contains(text(),'NNI WAN IPv6 Address')]//following-sibling::input";
				public static final String SIPIPv4AddressRangeNAT="@xpath=//*[contains(text(),'Public IPv4 Address Range for NAT Side')]//following-sibling::input";
				public static final String SIPVLANStartEndRange="@xpath=//*[contains(text(),'VLAN Start')]//following-sibling::input";
				public static final String SIPVLANID="@xpath=//*[contains(text(),'VLAN ID')]//following-sibling::input";
				public static final String SIPASNumber="@xpath=//*[contains(text(),'AS Number')]//following-sibling::input";
				public static final String SIPColtHostEquipmentIPv4Address="@xpath=//*[contains(text(),'Colt Host Equipment IPv4')]//following-sibling::input";
				public static final String SIPColtHostEquipmentIPv6Address="@xpath=//*[contains(text(),'Colt Host Equipment IPv6')]//following-sibling::input";
				public static final String SIPTrunkMaxNumberofSimoultaniousCalls="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[8])";
				public static final String SIPTrunkInboundOutboundSplit="@xpath=//*[contains(text(),'Inbound Outbound Split')]//following-sibling::input";
				public static final String SIPTrunkMaxInboundCalls="@xpath=//*[contains(text(),'Maximum Inbound Calls')]//following-sibling::input";
				public static final String SIPTrunkOverCallLimitForTrunk="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[10])";
				public static final String SIPTrunkIncallLimitForTrunk="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[11])";
				public static final String SIPTrunkOutCallLimitForTrunk="@xpath=((//*[@class='colt-attribute-input siebui-ctrl-input colt-attribute-validate'])[12])";
				public static final String SIPTrunkTypeofCAC="@xpath=//*[contains(text(),'Type of CAC')]//following-sibling::input";
				public static final String SIPCustomerIPPbxIdTab="@xpath=(//a[text()='Customer IP PBX'])";
				public static final String SIPAddrecordSymbolIPPBX="@xpath=(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[2]/preceding::button[1]";
				public static final String SIPIPPBXAliasName="@xpath=(//*[@id='1_s_2_l_COLT_Alias_Name'])";
				public static final String SIPIPPBXAliasNameInputField="@xpath=//input[@id='1_COLT_Alias_Name']";
				public static final String SIPIPPBXModel="@xpath=(//*[@id='1_s_2_l_COLT_PBX_MODEL'])";
				public static final String SIPIPPBXModelInput="@xpath=(//*[@id='1_s_2_l_COLT_PBX_MODEL'])/input";
				public static final String SIPIPPBXVendor="@xpath=(//*[@id='1_s_2_l_COLT_PBX_Vendor'])";
				public static final String SIPIPPBXVendorInput="@xpath=(//*[@id='1_COLT_PBX_Vendor'])";
				public static final String SIPSOftwareVersionField="@xpath=(//*[@id='1_s_2_l_COLT_Software_Version'])";
				public static final String SIPSoftwareVersionInput="@xpath=(//*[@id='1_COLT_Software_Version'])";
				public static final String SIPIPPBXAddressUID="@xpath=(//*[@id='1_s_2_l_COLT_Address_UID'])";
				public static final String SIPIPPBXLookupoptionClick="@xpath=(//*[@class='siebui-icon-pick'])";
				public static final String SIPIPPBXAddressUIDStreetName="@xpath=(//*[@name='streetName'])";
				public static final String SIPIPPBXAddressUIDCountryName="@xpath=(//input[@name='country'])";
				public static final String SIPIPPBXAddressUIDCityName="@xpath=(//input[@name='cityTown'])";
				public static final String SIPIPPBXAddressUIDPostalCode="@xpath=(//input[@name='postalZipCode'])";
				public static final String SIPIPPBXAddressUIDPremiseCode="@xpath=(//input[@name='premisesNumber'])";
				public static final String SIPAddressUIDSearchButton="@xpath=//div[@id='colt-site-selection-btn-bar']/button";
			//	public static final String SIPSelectAddressRecord="@xpath=//table[@id='colt-siteselection-result-table']/tbody/tr[2]/td[2]";
				public static final String SIPPickAddressButtonForUID="@xpath=//div[@id='colt-site-selection-btn-bar']/button[3]";
				public static final String SIPPBXTechnicalContact="@xpath=(//*[@id='1_s_2_l_COLT_Tech_Contact'])";
				public static final String SIPPBXTecnicalContactLookup="@xpath=//*[@class='siebui-icon-pick']";
			//	public static final String SIPPBXTechnicalContactLookupOKButton="@xpath=(//*[@class='siebui-ctrl-btn appletButton siebui-icon-pickrecord s_5_1_33_0'])";
				public static final String SIPVoiceConfigTab="@xpath=//a[contains(text(),'Voice Config')]";
				public static final String SIPVoiceConfigExpandSymbolIPPBXSelewction="@xpath=(//*[@class='colt-site-component-collapse fa fa-plus-square-o'])";
				public static final String SIPReClickToIPPBXLookUp="@xpath=//*[contains(text(),'IP PBX ID')]/following-sibling::span[@class='siebui-icon-pick']";
				public static final String SIPOKSelectionButton="@xpath=//button[@title='COLT IP PBX Pick Applet:OK']";
				public static final String SIPOtherTab="@xpath=//a[contains(text(),'Other')]";
				public static final String SIPVoiceConfigOthersTabShowFullInfo="@xpath=(//a[contains(text(),'Show full info')])[1]";
				public static final String SIPVoiceConfigOthersTabColtDDIRanges="@xpath=(//*[normalize-space(text()) and normalize-space(.)='Colt DDI Ranges'])[1]/preceding::span[1]";
				public static final String SIPNumberOfSingleColtDDIs="@xpath=(.//*[normalize-space(text()) and normalize-space(.)='Number of 100 Colt DDI Ranges'])[1]/preceding::input[2]";
				public static final String SIPNumberOf10ColtDDIRanges="@xpath=(.//*[normalize-space(text()) and normalize-space(.)='Number of 1000 Colt DDI Ranges'])[1]/preceding::input[2]";
				public static final String SIPNumberOf100ColtDDIRanges="@xpath=//*[contains(text(),'Number of 100 Colt DDI Ranges')]/following-sibling::input";
				public static final String SIPNumberOf1000ColtDDIRanges="@xpath=//*[contains(text(),'Number of 1000 Colt DDI Ranges')]/following-sibling::input";
			//	public static final String SIPVoiceConfigOthersTabColtDDIRangesCloseWindow="@xpath=//*[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//button[@title='Close']";

				public static final String SIPSelectAddressRecord="@xpath=//table[@id='colt-siteselection-result-table']/tr[2]";
				public static final String SIPPBXTechnicalContactLookupOKButton="@xpath=//button[@aria-label='Pick Contact:OK']";
				public static final String SIPVoiceConfigOthersTabColtDDIRangesCloseWindow="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//following-sibling::button[@title='Close']";

			}
		 
////////////////////kk 10 dec
		 
		 public static class SiteBInstallationTimePUD
			{
				//public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				//public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
				public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";

				public static final String SiteABSelection="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
			}
			
			public static class CSPInterconnectSiteAEntry
			{
				public static final String PopPlus="@xpath=//div[@title='Operational Attributes List Applet']//button[@title='Operational Attributes:New']";
				public static final String PopAttributeDrodown="@xpath=//div[@title='Operational Attributes List Applet']//span[@class='siebui-icon-dropdown']";
				//public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020

				public static final String AttributeValue="@xpath=//div[@title='Operational Attributes List Applet']//input[@name='COLT_Attribute_Value']";
				public static final String SiteASettingOK="@xpath=//div[@title='Operational Attributes List Applet']//button[@title='Operational Attributes:OK']";
				
			}
			public static class R5DataCoverage
			{
				public static final String EnableFeature="@xpath=//div[@id='colt-s-order-enabledfeatures-header']";
				public static final String ClickShowFullInfoAccess="@xpath=//a[@id='colt-s-order-enabledfeatures-showfullinfo']";
				public static final String SectionSelection="@xpath=//span[text()='Value']/preceding-sibling::span[@class='colt-component-check-available fa fa-circle-o']";
				public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*/span)[2]";
			//	public static final String SelectValueDropdown="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
				public static final String InputBox="@xpath=(//span[text()='Value']/following-sibling::input)[-1]";
				//public static final String InputBox1="@xpath=(//span[text()='Value']/following-sibling::input)[2]";
				//public static final String PopClose="@xpath=//div[contains(@style,'block')]//button[@title='Close']";			
			//	public static final String PopClose="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//following-sibling::button[@title='Close']";
				
				public static final String SelectValueDropdown="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
				public static final String PopClose="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//following-sibling::button[@title='Close']";
			}
			
			
		//////////////////// B 10 Dec
			public static class middleAppletPrivateWaveNode
			{
				
				public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
			//	public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
			//	public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
				public static final String SaveButton="@xpath=//div/a[text()='Save']";	
				public static final String MiddleLi="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
			}
			public static class OperationalAttributeUltra
			{
				public static final String SettingsButton="@xpath=//div[@id='colt-s-order-connection-header-btns']//span";
				public static final String OperationalAttribueCount="@xpath=(//table[@summary='Operational Attributes']//tr[contains(@class,'mandatory')]//td[3])";
				public static final String OperationalAttribueClick="@xpath=(//table[@summary='Operational Attributes']//tr[contains(@class,'mandatory')]//td[3])[index]";
				public static final String OperationalAttributeText="@xpath=//table[@summary='Operational Attributes']//tr[contains(@class,'mandatory')]//td[3]/input";
				public static final String OperationalAttributeOK="@xpath=//button[@aria-label='Operational Attributes:OK']";

	       }
			
			public static class SiteAInstallationTimePUD
			{
				public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span"; 
			//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
			}


			public static class SiteATerminationTimePUD
			{
				public static final String SiteAInput="@xpath=//div[@id='colt-site-left']//span[text()='Value']/parent::div/input";
				public static final String SiteADropdownClick="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span"; 

			}


			public static class AEndSitePUD
			{
				public static final String AccessTypeDropdownAccess="@xpath=(//span[text()='Access Type']/..//span)[3]";
			//	public static final String AccesstypeOffnet="@xpath=//li[text()='AccessTypeValue']";
				public static final String AccesstypeOffnet="@xpath=//li/*[text()='AccessTypeValue']";//updated 2020
				public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
				public static final String AEndSiteDropDown="@xpath=//div[@id='colt-site-left']//span[text()='Value']/following-sibling::span";
				//public static final String AList="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				//public static final String AList="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020

				public static final String ThirdpartyaccessproviderDropDown="@xpath=(//span[text()='Third Party Access Provider']/..//span)[3]";	
				public static final String Thirdpartyconectionreference="@xpath=//span[text()='3rd Party Connection Reference']/..//input";
				public static final String ThirdpartyDropDown="@xpath=(//span[text()='Third Party SLA Tier']/..//span)[3]";
				//public static final String ThirdpartySLATiervalue="@xpath=//li[text()='SLAValue']";
				public static final String ThirdpartySLATiervalue="@xpath=//li/*[text()='SLAValue']";//updated 2020
			//	public static final String ThirdpartyaccessProvidervalue="@xpath=//li[text()='Value']";
				public static final String ThirdpartyaccessProvidervalue="@xpath=//li/*[text()='Value']";//updated 2020
				public static final String AEndSiteInput="@xpath=(//span[text()='Value']/parent::div/input)[1]";
				public static final String AListBuildingType="@xpath=(//*[not(contains(@style,'display'))]//li/*[text()='Value'])[2]";//updated 2020

				public static final String AList="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
				public static final String AListB="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
				
			}

			public static class EthernetAccessNewFields
			{
				public static final String Topology="@xpath=//span[text()='Topology']/..//input";
				public static final String NetworkTopology="@xpath=//span[text()='Network Topology']/..//input/following-sibling::span";
				public static final String CircuitReference="@xpath=//*[text()='Circuit Reference']/..//input";
				public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[3]";
				//public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])";
				public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])";//updated 2020
				public static final String SettingsButton="@xpath=//div[@id='colt-s-order-connection-header-btns']//span";
				public static final String AttributeValueEthernetAccess="@xpath=//td[text()='NC Technical Service Id']/..//td/following-sibling::td/following-sibling::td/input";
				public static final String Dialogbuttonset="@xpath=(//div[@class='ui-dialog-buttonset']/button)[2]";
				public static final String OkButtonOperationalAttribute="@xpath=//button[@aria-label='Operational Attributes:OK']";
				//public static final String InputValuesProtection="@xpath=(//td[text()='Protection Type']//following::input)[1]";
				//public static final String VendorValuecell="@xpath=(//td[text()='Vendor Platform']//following::td)[1]";
				//public static final String TechonolgyValuecell="@xpath=(//td[text()='Technology']//following::td)[1]";
				public static final String OKButton="@xpath=//button[@aria-label='Operational Attributes:OK']";
				
				public static final String VendorValuecellInput="@xpath=(//td[text()='Vendor Platform']//following::input)[1]";
				public static final String TechonolgyValuecellinput="@xpath=(//td[text()='Technology']//following::input)[1]";

				
				public static final String LicenseFlagField="@xpath=(//td[text()='License Flag']//following::td)[1]";
				public static final String LicenseFlag="@xpath=//td[text()='License Flag']//following-sibling::td/input";
				public static final String LicenseTextField="@xpath=(//td[text()='License Text']//following::input)[1]";
				public static final String LicenseText="@xpath=//td[text()='License Text']//following-sibling::td/input";
				public static final String ProtectionTypeField="@xpath=(//td[text()='Protection Type']//following::input)[1]";
				public static final String InputValuesProtection="@xpath=//td[text()='Protection Type']//following-sibling::td/input";
				public static final String VendorValueField="@xpath=(//td[text()='Vendor Platform']//following::td)[1]";
				public static final String VendorValuecell="@xpath=//td[text()='Vendor Platform']//following-sibling::td/input";
				public static final String TechonolgyValueField="@xpath=(//td[text()='Technology']//following::td)[1]";
				public static final String TechonolgyValuecell="@xpath=//td[text()='Technology']//following-sibling::td/input";
				
			}
			
			
			public static class middleAppletDarkFibre
			{
				//public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
				//public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				//public static final String SaveButton="@xpath=//div/a[text()='Save']";
				
				public static final String MiddleDropDown="@xpath=//span[text()='Value']/parent::*/span[2]";
				//public static final String MiddleLi="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				//public static final String MiddleLi="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
				public static final String MiddleLi="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
				public static final String MiddleLiB="@xpath=(//*[not(contains(@style,'display'))]//li/*[text()='Value'])[2]";
				public static final String SaveButton="@xpath=//div/a[text()='Save']";
				public static final String PrimaryOrderField="@xpath=(//td[text()='Primary Order']//following::td)[1]";
				public static final String PrimaryOrder="@xpath=//td[text()='Primary Order']//following-sibling::td/input";
				public static final String ContractTypeField="@xpath=(//td[text()='Contract Type']//following::td)[1]";
				public static final String ContractType="@xpath=//td[text()='Contract Type']//following-sibling::td/input";
			}
			
			
			public static class middleApplet
			{
				public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
				public static final String SupportStarDate="@xpath=//span[text()='Support Start Date']/parent::*//input";
			    public static final String SupprtEndDate="@xpath=//span[text()='Support End Date']/parent::*//input";
			}
			
			public static class DiversityCircuitEntry
			{
				/*public static final String PopInput ="@xpath=//div[contains(@style,'block')]//span[text()='Value']/parent::div/input";
				public static final String PopClose ="@xpath=//div[contains(@style,'block')]//button[@title='Close']";
				*/
				public static final String PopInput ="@xpath=//*[not(contains(@style,'display'))]//span[text()='Value']/parent::div/input";
				public static final String PopClose ="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//following-sibling::button[@title='Close']";
			}
			
			public static class middleAppletManagedDedicatedFirewall
			{
				public static final String CPECombinationIDGeneric="@xpath=//span[text()='Value']/parent::div//input";
				public static final String HighAvailabilityRequired="@xpath=(//span[text()='Value']/parent::*//span)[3]";
			//	public static final String HighAvailabilityRequiredli="@xpath=//li[text()='Yes']";
				public static final String HighAvailabilityRequiredli="@xpath=//li/*[text()='Yes']";//updated 2020

				public static final String SaveButton="@xpath=//div/a[text()='Save']";		
			}
				
			public static class SiteAVLan
			{
				public static final String AEndSiteInput="@xpath=(//span[text()='Value']/parent::div/input)[1]";

			}
			
			public static class SiteBVLan
			{
				public static final String BEndSiteInput="@xpath=(//span[text()='Value']/parent::div/input)[2]";

			}
			
			public static class SiteBDedicatedCloudAccess
			{
				public static final String SiteBInput="@xpath=//div[@id='colt-site-right']//span[text()='Value']/parent::div/input";	
				public static final String SiteBDropdownClick="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
				//public static final String SiteABSelection ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
				public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
			}
			
			public static class IPVPNServicePlusAccess
			{
				public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[3]";
				//public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])";
				public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])";//updated 2020
				public static final String ClickheretoSaveAccess="@xpath=(//a[contains(text(),'Click here to')])[1]";
				public static final String TextInput="@xpath=//span[text()='Value']/parent::*//input";
				
			}
			public static class IPVPNServicePlusAccess1
				{
				
			//	public static final String SelectValueDropdown1="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']"; //updated 2020
				public static final String SelectValueDropdown1="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']"; //updated 2020
				 public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[3]";
				//public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])";
				public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])";//updated 2020
				public static final String ClickheretoSaveAccess="@xpath=(//a[contains(text(),'Click here to')])[1]";
				//public static final String SelectValueDropdown1="@xpath=//*[contains(@style,'block')]//li[text()='Value']";
		//		public static final String SelectValueDropdown1="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020

				public static final String Bus1Dscp="@xpath=//span[text()='Business 1 DSCP']/..//input";
				public static final String Bus2Dscp="@xpath=//span[text()='Business 2 DSCP']/..//input";
				public static final String Bus3Dscp="@xpath=//span[text()='Business 3 DSCP']/..//input";
				public static final String PreDscp="@xpath=//span[text()='Premium DSCP']/..//input";
				public static final String StandDscp="@xpath=//span[text()='Standard DSCP']/..//input";
				
				}
				
				public static class NetworkReferenceFillService
				{
				 public static final String NetworkPlusSign="@xpath=//button[@title='Pick Network Reference:New']";
				 public static final String SubmitNetworkReference="@xpath=//button[@title='Pick Network Reference:OK']";
				 public static final String NetworkReferenceSearch="@xpath=//*[@aria-labelledby='COLT_Network_Reference_Label']/following-sibling::span";

				}
				public static class IPVPNServiceNetworkReference
				{
				 public static final String NetworkReference="@xpath=//input[@aria-labelledby='COLT_Network_Reference_Label']";

				}
				
				////////////////////////// 11 dec//////////////////////////
				
				public static class ManagedVirtualFirewallObj
				{
				//	public static final String MVFBillingTab ="@xpath=((//*[@class='ui-tabs-anchor'])[25])";
					public static final String MVFBillingTab ="@xpath=//a[text()='Billing']";
					
					public static final String MVFShowfullInfo ="@xpath=(//*[@id='colt-s-order-enabledfeatures-showfullinfo'])";
					public static final String MVFSecurityPolicyAttachmentlink ="@xpath=(//span[text()='Security Policy Attachment Link']/parent::div//input)[2]";
					public static final String MVFAddbutton ="@xpath=(//*[@class='addcls'])";
				//	public static final String MVFClosebutton ="@xpath=((//*[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close'])[3])";
					public static final String MVFClosebutton ="@xpath=//*[contains(text(),'Managed')]/parent::div//button[@title='Close']";
					
					public static final String MVFSavebutton ="@xpath=(//*[@class='colt-primary-btn colt-s-order-save-btn'])";

				}

			public static class IPCPESolutionSite{
								
						/*		 public static final String ScrollDownAend="@xpath=//span[text()='A End Resilience Option']";
								 public static final String AendResillenceOption="@xpath=//span[text()='A End Resilience Option']/..//input/following-sibling::span";
								 public static final String ValuesInsideDropdownWithIndex="@xpath=(//li[text()='Data'])[value]";
								 public static final String BendResillenceOption="@xpath=//span[text()='B End Resilience Option']/..//input/following-sibling::span";
								 public static final String NetworkTopology="@xpath=//span[text()='Network Topology']/..//input/following-sibling::span";
								 public static final String ValueInsideDropdown="@xpath=//li[text()='Data']";
								 public static final String ServiceBandwidthCpe="@xpath=//span[text()='Service Bandwidth']/..//input/following-sibling::span";
								 public static final String ServiceTypecpe="@xpath=//span[text()='Service Type']/..//input/following-sibling::span";
								 public static final String SaveButtonClick="@xpath=(//a[text()='Save'])[2]";
								 public static final String ServiceOrderNumber="@xpath=//span[text()='Service Order: ']/following-sibling::span";
							*/
				
								 public static final String ScrollDownAend="@xpath=//span[text()='A End Resilience Option']";
								 public static final String AendResillenceOption="@xpath=//span[text()='A End Resilience Option']/..//input/following-sibling::span";
								 public static final String ValuesInsideDropdownWithIndex="@xpath=(//li[text()='Data'])['Value']";
								 public static final String BendResillenceOption="@xpath=//span[text()='B End Resilience Option']/..//input/following-sibling::span";
								 public static final String NetworkTopology="@xpath=//span[text()='Network Topology']/..//input/following-sibling::span";
								// public static final String ValueInsideDropdown="@xpath=//li[text()='Data']";
								 public static final String ValueInsideDropdown="@xpath=//li[text()='Data']";//updated 2020
								 public static final String ServiceBandwidthCpe="@xpath=//span[text()='Service Bandwidth']/..//input/following-sibling::span";
								 public static final String ServiceTypecpe="@xpath=//span[text()='Service Type']/..//input/following-sibling::span";
								 public static final String SaveButtonClick="@xpath=(//a[text()='Save'])[2]";
								 public static final String ServiceOrderNumber="@xpath=//span[text()='Service Order: ']/following-sibling::span";
								 
								 
								}
			public static class SiteASettingClick{
							
							 public static final String SiteASetting="@xpath=//div[@id='colt-site-left']//span[@class='colt-oa-btn colt-oa-std-btn colt-oa-Primary-Child']";

							}
							public static class SiteBSettingClick{
								
								 public static final String SiteBSetting="@xpath=//div[@id='colt-site-right']//span[@class='colt-oa-btn colt-oa-std-btn colt-oa-Primary-Child']";

								}
			public static class EthernetVPNAccessObj
				{
					public static final String LinkTypeDropDown ="@xpath=(//span[text()='Link Type']/parent::*//span)[3]";
					//public static final String LinkTYpeValue ="@xpath=//li[text()='E-VPN Primary']";
					public static final String LinkTYpeValue ="@xpath=//li/*[text()='E-VPN Primary']";//updated 2020
					public static final String ServiceBandwidthDropdownAccess ="@xpath=(//span[text()='Service Bandwidth']/..//span)[3]";
				//	public static final String ServiceBandwidthSelect ="@xpath=//li[text()='value']";
				//	public static final String ServiceBandwidthSelect ="@xpath=//li/*[text()='value']";//updated 2020
					public static final String ResilenceOptionDropDown ="@xpath=(//span[text()='Resilience Option']/parent::*//span)[3]";
					//public static final String ResilienceValue ="@xpath=//li[text()='Unprotected']";
				//	public static final String ResilienceValue ="@xpath=//li/*/[text()='Unprotected']";//updated 2020
					public static final String OSSPlatformDropDown ="@xpath=(//span[text()='OSS Platform Flag']/parent::*//span)[3]";
					//public static final String OSSplatformValue ="@xpath=//li[text()='Legacy']";
					public static final String OSSplatformValue ="@xpath=//li/*[text()='Legacy']";//updated 2020
					public static final String SelectSiteSearchAccess ="@xpath=//span[text()='Select a site:']//span";
					public static final String BCPReference ="@xpath=//span[text()='BCP Reference']/..//input";
					//public static final String ConnectorTypeSelect ="@xpath=//li[text()='FC/PC']";
					public static final String ConnectorTypeSelect ="@xpath=//li/*[text()='FC/PC']";//updated 2020
					//public static final String sitenamevaluevpn ="@xpath=//ul[contains(@style,'block')]/li";
					//public static final String sitenamevaluevpn="@xpath=//*[not(contains(@style,'display'))]//li";//updated 2020
					public static final String SiteIDInput ="@xpath=//input[@name='siteId']";
					public static final String SiteIDSearchBtn ="@xpath=//*[@class='colt-siteid-button colt-primary-btn']";
					
					public static final String ServiceBandwidthSelect="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";//updated 2020
					public static final String ResilienceValue ="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Unprotected']";//updated 2020
					public static final String sitenamevaluevpn="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='AT_1010_KARNTNER RING_12_EG_NODE']";//updated 2020
				
				}
			public static class IPDomain
							{
								public static final String Domainname="@xpath=(//div[@class='colt-child-wrapper']//div[2]//following::div//span//following::input)[1]";
								public static final String Clicktoshowfullinfomiddle="@xpath=//div[@id='colt-s-order-enabledfeatures-content']";
								public static final String SaveButtonClick="@xpath=(//a[text()='Save'])[2]";
							//	public static final String Crossbutton="@xpath=(//button[@title='Close'])[4]";
								public static final String DNStype="@xpath=//span[text()='DNS Type']/following-sibling::span";
							//	public static final String DNSTypeInput="@xpath=//li[contains(text(),'Colt Primary')]";
								public static final String DNSTypeInput="@xpath=//li/*[contains(text(),'Colt Primary')]";//updated 2020
								//public static final String DomainordertypeData="@xpath=//li[text()='Update']";
								public static final String DomainordertypeData="@xpath=//li/*[text()='Update']";//updated 2020
								public static final String Crossbutton="@xpath=//span[text()='IP Domain']/..//button";
							}
							
							public static class Interconnect
							{
								public static final String VoiceServiceCountry="@xpath=//span[text()='Voice Service Country']/following-sibling::span";
								//public static final String VoiceServiceCountryValue="@xpath=//li[text()='Value']";
								public static final String VoiceServiceCountryValue="@xpath=//li/*[text()='Value']";//updated 2020
								public static final String CallAdmissionControl="@xpath=//span[contains(text(),'Call Admission Control ')]/..//input";
								public static final String NumberOfSignallingTrunks="@xpath=//span[text()='Number of Signalling Trunks']/..//input";
								public static final String VoiceConfigurationReference="@xpath=//span[text()='Voice Configuration Reference']/..//input";
								public static final String TrafficDirection="@xpath=//span[text()='Traffic Direction']/following-sibling::span";

								public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[3]";
								//public static final String SiteABSelection ="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
								public static final String SiteABSelection="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";//updated 2020
								
								//public static final String SelectValueDropdown="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
								public static final String SelectValueDropdown="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
								public static final String ClickLink="@xpath=//a[text()='Value']";
								public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
								public static final String ShowMore="@xpath=//a[text()='Show More']";
								public static final String ClickToShowFullInfo="@xpath=//a[@id='colt-s-order-enabledfeatures-showfullinfo']";
								
								public static final String SiteContact="@xpath=//span[text()='Site Contact']/following-sibling::span";
													
								
								public static final String OkButton="@xpath=//button[@aria-label='Pick Contact:OK']";
								public static final String ServiceParty="@xpath=//span[text()='Service Party']/following-sibling::span";
								public static final String OkButtonService="@xpath=//button[@aria-label='Pick Account:OK']";
								public static final String DelieveryTeamCountry="@xpath=//span[text()='Delivery Team Country']/following-sibling::span";

								public static final String DeliveryTeamCity="@xpath=//span[text()='Delivery Team City']/..//input";
										public static final String CrossButton="@xpath=(//span[text()='Interconnect']/..//button)[2]";
					//			public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
							public static final String TrunkSequence="@xpath=//span[text()='Trunk Sequence']/..//input";
							public static final String TrunkConfiguration="@xpath=//span[text()='Trunk Configuration']]";
							public static final String TrunkName="@xpath=//span[text()='Trunk Name']/..//input";
							public static final String AccessLineType="@xpath=//span[text()='Access Line Type']/following-sibling::span";
							public static final String AccessServiceID="@xpath=//span[text()='Access Service ID']/following-sibling::span";
							public static final String OkButtonAccess="@xpath=//button[@aria-label='Pick Service:OK']";
							//public static final String CrossButtonTrunk="@xpath=//*[contains(@class,'colt-save-level')][contains(@style,'block')]//button[@title='Close']";
							public static final String CrossButtonTrunk="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//following-sibling::button[@title='Close']";
							public static final String ClickOnPlusButtonService="@xpath=//button[@aria-label='Service Group:New']";
							public static final String ServiceGroupReference="@xpath=//input[@name='COLT_Service_Group_Reference']/following-sibling::span";
							public static final String ClickOkButton="@xpath=//button[@aria-label='Add/Pick Service Group:OK']";

							public static final String ShowFullInfoTrunk="@xpath=//a[text()='Show full info']";
							public static final String AddButtonTrunk="@xpath=(//div[@class='subvpn-content']//following::span)[1]";	
							public static final String CustomerOriginatingIP4Address="@xpath=(//span[text()='Customer Originating IPv4 Address Range'])[2]/..//input";
							public static final String CustomerOriginatingIP6Address="@xpath=(//span[text()='Customer Originating IPv6 Address Range'])[2]/..//input";
							public static final String CustomerTerminatingIP4Address="@xpath=(//span[text()='Customer Terminating IPv4 Address'])[2]/..//input";
							public static final String CustomerTerminatingIP6Address="@xpath=(//span[text()='Customer Terminating IPv6 Address'])[2]/..//input";

							public static final String SignallingTransportProtocol="@xpath=(//span[text()='Signalling Transport Protocol'])[2]/following-sibling::span";
							public static final String VoipProtocol="@xpath=(//span[text()='VoIP Protocol'])[2]/following-sibling::span";
							public static final String EncryptionProtocol="@xpath=(//span[text()='Encryption Type'])[2]/following-sibling::span";
							public static final String InstalltionDropdown="@xpath=//select[@aria-label='Third Level View Bar']";
							public static final String ServiceGroupTab="@xpath=//a[text()='Service Group']";
							public static final String NATRequired="@xpath=(//span[text()='NAT Required?'])[2]/..//input";
							public static final String InboundOutboundSplit="@xpath=(//span[text()='Inbound Outbound Split'])[2]/..//input";		
							public static final String TypeOfCAC="@xpath=(//span[text()='Type of CAC'])[2]/following-sibling::span";
							public static final String VoiceTab="@xpath=//a[text()='Voice Features']";
							public static final String Resillence="@xpath=//span[text()='Resilience']/following-sibling::span";
							public static final String Language="@xpath=//span[text()='Language']/..//input";

							}
							public static class IPGuardian
							{
								
								public static final String DNSTypeInput="@xpath=//div[@id='colt-s-order-connection-attributes']//div[1]//input";
								public static final String Automigration="@xpath=//div[@id='colt-s-order-connection-attributes']//div[2]/input";
								public static final String Customerdnsresolve="@xpath=//div[@id='colt-s-order-connection-attributes']//div[3]//input";
								public static final String IpguardianVariant="@xpath=//div[@id='colt-s-order-connection-attributes']//div[4]//input";
								public static final String Colttechnicaltestcalls="@xpath=//div[@id='colt-s-order-connection-attributes']//div[5]//input";
								public static final String servicebandwidth="@xpath=//div[@id='colt-s-order-connection-attributes']//div[6]//input";					
								public static final String Testwindowipaccess="@xpath=//span[text()='Test Window']/..//input";
								public static final String CircuitipaddressClick="@xpath=//a[text()='Click to Show Full info']";
								public static final String CircuitIPAddressInput="@xpath=//span[text()='Circuit IP Address']/..//input"; 
								public static final String CrossButtonGurdian="@xpath=//span[text()='IP Guardian']/..//button";
								public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
								public static final String AlertingNotification="@xpath=//div[@id='colt-s-order-connection-attributes']//div[1]//input";
								public static final String IpRange="@xpath=//span[text()='IP Range']/..//input";

							}
							public static class	BEndSitePUD
							{

								public static final String AccesstypedropdownB="@xpath=(//span[text()='Access Type']/..//span)[6]";
								//public static final String AccesstypeOffnet="@xpath=//li[text()='AccessTypeValue']";
								public static final String AccesstypeOffnet="@xpath=//li/*[text()='AccessTypeValue']";//Updated IP2020
								public static final String IpGurdianSave="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";


								public static final String BEndSiteDropDown="@xpath=//div[@id='colt-site-right']//span[text()='Value']/following-sibling::span";
								public static final String BEndSiteInput="@xpath=(//span[text()='Value']/parent::div/input)[2]";
								public static final String ServicePartyB="@xpath=(//span[text()='Value'])[2]/parent::div/span[2]";
								//public static final String BList="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
							//	public static final String BList="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
								public static final String ThirdpartyaccessproviderDropDownB="@xpath=(//span[text()='Third Party Access Provider']/..//span)[6]";
								//	public static final String ThirdpartyaccessProvidervalue="@xpath=//li[text()='Value']";
								public static final String ThirdpartyaccessProvidervalue="@xpath=//li/*[text()='Value']";//updated 2020
								public static final String ThirdpartyconectionreferenceB="@xpath=(//span[text()='3rd Party Connection Reference']/..//input)[2]";
								public static final String ThirdpartyDropDownB="@xpath=(//span[text()='Third Party SLA Tier']/..//span)[6]";

								//public static final String ThirdpartySLATiervalue="@xpath=//li[text()='SLAValue']";
								public static final String ThirdpartySLATiervalue="@xpath=//li/*[text()='SLAValue']";//updated 2020

								public static final String BList="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";


							}

				public static class	privateWaveServiceEntry
							{
							
								public static final String NetworkTopologyPrivateWave="@xpath=(//span[text()='Network Topology']/..//input/following-sibling::span)[2]";
							//	public static final String InsideDropdownValues="@xpath=//li[text()='Data']";
								public static final String InsideDropdownValues="@xpath=//li/*[text()='Data']";//updated 2020
							//	public static final String BendResillenceOption="@xpath=//span[text()='B End Resilience Option']/..//input/following-sibling::span";

								//public static final String ValuesInsideDropdownWithIndex="@xpath=(//li[text()='Data'])[value]";
								public static final String ValuesInsideDropdownWithIndex="@xpath=(//li/*[text()='Data'])[value]";//updated 2020
							//	public static final String ValueInsideDropdown="@xpath=//li[text()='Data']";
								public static final String ValueInsideDropdown="@xpath=//li/*[text()='Data']";//updated 2020
								public static final String ServiceTypePrivate="@xpath=(//span[text()='Service Type']/..//input/following-sibling::span)[2]";

							//	public static final String PopDropdownClick="@xpath=//div[contains(@style,'block')]//span[text()='Value']/following-sibling::span";
								
								public static final String BendResillenceOption="@xpath=//div[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//span[text()='B End Resilience Option']/..//input/following-sibling::span";
								public static final String PopDropdownClick="@xpath=//div[@class='colt-fullinfo-wrapper ui-dialog-content ui-widget-content colt-save-level']//span[text()='Value']/following-sibling::span";
			}
							
			public static class SearchSiteEntery
			{

			public static final String SearchInput="@xpath=//span[text()='Value']/parent::div/input";
			public static final String SearchDropdown="@xpath=//span[text()='Value']/following-sibling::span";
			public static final String SearchInputEthernetAccess="@xpath=//span[text()='Value']/parent::div/input";
			public static final String SearchAddressRowSelection="@xpath=//div[@id='colt-site-selection-popup-content']//tr[@class='colt-siteselection-datarow'][1]";
			public static final String PickSite="@xpath=//button[text()='Pick Site']";
		//	public static final String SiteABSelection="@xpath=//ul[contains(@style,'display')]//li[text()='Value']";
			public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
			public static final String Searchbutton="@xpath=//div[@id='colt-site-selection-btn-bar']/button";
			public static final String PickAddress="@xpath=//button[text()='Pick Address']";
			public static final String PickBuilding="@xpath=//button[text()='Pick Building']";

			}

			public static class SaveAndCloseMask
			{

			public static final String SaveEthernetAccess="@xpath=//div[@id='colt-s-order-connection-header']//a[text()='Save']";

			}	

			public static class NumberHostingObj
			{
			public static final String VoiceServiceCountry="@xpath=//span[text()='Voice Service Country']/following-sibling::span";
		//	public static final String VoiceServiceCountryValue="@xpath=//li[text()='value']";
		//	public static final String VoiceServiceCountryValue="@xpath=//li/*[text()='value']";//updated 2020
	
			public static final String CallAdmissionControl="@xpath=//span[contains(text(),'Call Admission Control ')]/..//input";
			public static final String NumberOfSignallingTrunks="@xpath=//span[text()='Number of Signalling Trunks']/..//input";
			public static final String VoiceConfigurationReference="@xpath=//span[text()='Voice Configuration Reference']/..//input";
			public static final String TrafficDirection="@xpath=//span[text()='Traffic Direction']/following-sibling::span";
	//		public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[2]";
			//public static final String SelectValueDropdown="@xpath=(//li[text()='Value'])";
			public static final String SelectValueDropdown="@xpath=(//li/*[text()='Value'])";//updated 2020
			//public static final String SiteABSelection="@xpath=//ul[contains(@style,'block')]//li[text()='Value']";
		//	public static final String SiteABSelection="@xpath=//*[not(contains(@style,'display'))]//li/*[text()='Value']";//updated 2020
			public static final String ClickLink="@xpath=//a[text()='Value']";
			public static final String SaveButton="@xpath=//a[@class='colt-primary-btn colt-s-order-save-btn']";
			public static final String ShowMore="@xpath=//a[text()='Show More']";
			public static final String ClickToShowFullInfo="@xpath=//a[@id='colt-s-order-enabledfeatures-showfullinfo']";
	//		public static final String SiteContact="@xpath=//button[@title='Pick Contact:OK']";
			public static final String OkButton="@xpath=//button[@aria-label='Pick Contact:OK']";
			public static final String ServiceParty="@xpath=//span[text()='Service Party']/following-sibling::span";
			public static final String OkButtonService="@xpath=//button[@aria-label='Pick Account:OK']";
			public static final String DelieveryTeamCountry="@xpath=//span[text()='Delivery Team Country']/following-sibling::span";
			public static final String DeliveryTeamCity="@xpath=//span[text()='Delivery Team City']/..//input";
	//		public static final String CrossButtonTrunk="@xpath=(//span[text()='Close']/ancestor::*/button)[4]";
			public static final String TrunkConfiguration="@xpath=//span[text()='Trunk Configuration']";
			public static final String TrunkName="@xpath=//span[text()='Trunk Name']/..//input";
			public static final String TrunkSequence="@xpath=//span[text()='Trunk Sequence']/..//input";
			public static final String AccessLineType="@xpath=//span[text()='Access Line Type']/following-sibling::span";
			public static final String AccessServiceID="@xpath=//span[text()='Access Service ID']/following-sibling::span";
			public static final String OkButtonAccess="@xpath=//button[@aria-label='Pick Service:OK']";
			public static final String ShowFullInfoTrunk="@xpath=//a[text()='Show full info']";
			public static final String AddButtonTrunk="@xpath=(//div[@class='subvpn-content']//following::span)[1]";
			public static final String CustomerOriginatingIP4Address="@xpath=(//span[text()='Customer Originating IPv4 Address Range'])[2]/..//input";
			public static final String CustomerOriginatingIP6Address="@xpath=(//span[text()='Customer Originating IPv6 Address Range'])[2]/..//input";
			public static final String CustomerTerminatingIP4Address="@xpath=(//span[text()='Customer Terminating IPv4 Address'])[2]/..//input";
			public static final String CustomerTerminatingIP6Address="@xpath=(//span[text()='Customer Terminating IPv6 Address'])[2]/..//input";
			public static final String SignallingTransportProtocol="@xpath=(//span[text()='Signalling Transport Protocol'])[2]/following-sibling::span";
			public static final String VoipProtocol="@xpath=(//span[text()='VoIP Protocol'])[2]/following-sibling::span";
			public static final String EncryptionProtocol="@xpath=(//span[text()='Encryption Type'])[2]/following-sibling::span";
			public static final String NATRequired="@xpath=(//span[text()='NAT Required?'])[2]/..//input";
			public static final String InboundOutboundSplit="@xpath=(//span[text()='Inbound Outbound Split'])[2]/..//input";
			public static final String TypeOfCAC="@xpath=(//span[text()='Type of CAC'])[2]/following-sibling::span";
		//	public static final String OtherTabClick="@xpath=(//li[@aria-controls='s_vctrl_div_tabView_noop'])[2]";
		//	public static final String OtherTabClick="@xpath=(//li/*[@aria-controls='s_vctrl_div_tabView_noop'])[2]";//Updated IP2020
		
			public static final String TextInput="@xpath=//span[text()='Value']/parent::*//input";
			public static final String VoiceTab="@xpath=//a[text()='Voice Features']";
			public static final String Language="@xpath=//span[text()='Language']/..//input";
			public static final String Resillence="@xpath=//span[text()='Resilience']/following-sibling::span";
			public static final String ServiceGroupTab="@xpath=//a[text()='Service Group']";
			public static final String InstalltionDropdown="@xpath=//select[@aria-label='Third Level View Bar']";
			public static final String ClickOnPlusButtonService="@xpath=//button[@aria-label='Service Group:New']";
			public static final String ClickOkButton="@xpath=//button[@aria-label='Add/Pick Service Group:OK']";
	
			//////new addedby A
			public static final String SiteContact="@xpath=//span[text()='Site Contact']/following-sibling::span";
			public static final String ClickDropdown="@xpath=(//span[text()='Value']/parent::*//span)[3]";
		//	public static final String CrossButtonTrunk="@xpath=(//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close'])[3]";
			public static final String CrossButtonTrunConfig="@xpath=(//span[text()='Close']/ancestor::*/button)[4]";
			public static final String ServiceGroupReference="@xpath=//input[@name='COLT_Service_Group_Reference']/following-sibling::span";
			
		//	public static final String PickAccountGo="@xpath=(//button[@aria-label='Pick Service:Go'])[2]";
			
			
			public static final String SiteABSelection="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";//updated 2020
			public static final String VoiceServiceCountryValue="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Value']";
			public static final String CrossButtonTrunk="@xpath=//div[@class='ui-dialog colt-order-full-info-popup-dialog ui-widget ui-widget-content ui-front colt-order-full-info ui-draggable ui-resizable colt-save-level']//following-sibling::button[@title='Close']";
			public static final String OtherTabClick="@xpath=(//li[@aria-controls='s_vctrl_div_tabView_noop'])[2]";//Updated IP2020
			public static final String PickAccountGo="@xpath=(//button[@aria-label='Pick Service:Go'])[2]";

			}	
			
			
			public static class Network
			{
				public static final String NetworkTab="@xpath=//a[text()='Network']";
				public static final String SubNetworkSubTab="@xpath=//a[text()='Sub Network']";
				public static final String SubNetworkNewPlusIcon="@xpath=//button[@aria-label='Sub Network:New']";
				public static final String TechnologyInput="@xpath=//input[@name='COLT_Technology']";
				//public static final String SwitchingNodeSiteFld="@xpath=//*[contains(@aria-labelledby,'Switching_Node_Site')]";
				public static final String SwitchingNodeSiteSearchIcon="@xpath=//*[@class='siebui-icon-pick']";
				public static final String SwitchingSiteGoArrow="@xpath=//*[@aria-labelledby='PopupQuerySrchspec_Label']/../following-sibling::span/*[@title='Switching Site:Go']";
				public static final String SwitchingSiteOkBtn="@xpath=//*[@aria-label='Switching Site:OK']";
				
				public static final String SwitchingNodeSubTab="@xpath=//a[text()='Switching Node']";
				public static final String SwitchingNodeNewPlusIcon="@xpath=//*[@aria-label='Switching Node:New']";
			//	public static final String SubNetworkFld="@xpath=//*[contains(@aria-labelledby,'Sub_Network_Id')]";
				public static final String SubNetworkSearchIcon="@xpath=//*[@class='siebui-icon-pick']";
				public static final String SubNetworkIDOkBtn="@xpath=//*[@aria-label='Sub Network Id:OK']";
				
				public static final String SubNetworkFld="@xpath=//td[@id='1_s_1_l_COLT_Sub_Network_Id']";

				public static final String SwitchingNodeSiteFld="@xpath=//td[@id='1_s_2_l_COLT_Switching_Node_Site']";
				public static final String SwitchingNodeSiteFld1="@xpath=//td[@id='1_s_1_l_COLT_Switching_Node_Site']";
			}	
			public static class Site
			{
				public static final String SiteTab="@xpath=//a[text()='Sites']";
			}
			
			public static class Workflow
			{
				public static final String WorkflowTab="@xpath=//a[text()='Workflows']";
				public static final String listRootWiNumber="@xpath=//td[contains(@id,'EXTERNAL_URL')]";
				//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td/following-sibling::td[contains(@id,'EXTERNAL_URL')]";
				public static final String status="@xpath=//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td[contains(@id,'Workflow_Status')]";
				public static final String rowCount="@xpath=//table[@summary='Workflow']/tbody/tr/following-sibling::tr";
			}
			
			public static class addSiteAXNGDetails
			{
			public static final String SiteID="@xpath=//input[@name='siteId']";
			public static final String SearchAddressSiteA ="@xpath=//div[@id='colt-site-left']//span[@title='Select a site']";
			public static final String SearchAddressSiteB ="@xpath=//div[@id='colt-site-right']//span[@title='Select a site']";

			public static final String SearchButton="@xpath=//button[@class='colt-siteid-button colt-primary-btn']";
			}
}
