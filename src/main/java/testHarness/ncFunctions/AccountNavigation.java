package testHarness.ncFunctions;
import org.openqa.selenium.By;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;


import pageObjects.ncObjects.AccountNavigationObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;


public class AccountNavigation extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void GotoDocument() throws Exception
	{
		waitForAjax();
//		verifyExists(AccountNavigationObj.Account.Topnavegation,"Top Navigation");
		mouseMoveOn(AccountNavigationObj.Account.Topnavegation);
	
		verifyExists(AccountNavigationObj.Account.Submenu,"Sub Menu");
		click(AccountNavigationObj.Account.Submenu,"Sub Menu");
		
		waitForAjax();
	}
	
	public void GotoAccount() throws Exception
	{	
		verifyExists(AccountNavigationObj.Account.Customertype,"Customer Type");
		click(AccountNavigationObj.Account.Customertype,"Customer Type");
		
		waitForAjax();

		verifyExists(AccountNavigationObj.Account.Customername,"Customer Name");
		click(AccountNavigationObj.Account.Customername,"Customer Name");		
	}

	public void GotoAccount1(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{	
		String AccountNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer Account No");
		
		verifyExists(AccountNavigationObj.Account.Customertype,"Customer Type");
		click(AccountNavigationObj.Account.Customertype,"Customer Type");
		
		waitForAjax();

		verifyExists(AccountNavigationObj.Account.Fastsearch,"Fastsearch");
		click(AccountNavigationObj.Account.Fastsearch,"Fastsearch");
		
		verifyExists(AccountNavigationObj.Account.ServiceInventorySearch,"ServiceInventorySearch");
		mouseMoveOn(AccountNavigationObj.Account.ServiceInventorySearch);
		
		verifyExists(AccountNavigationObj.Account.AccountSearch,"Account Search");
		click(AccountNavigationObj.Account.AccountSearch,"Account Search");
		Reusable.waitForSiebelLoader();
		
		verifyExists(AccountNavigationObj.Account.Ocnsearchcriterion,"OCN Search criterion");
		click(AccountNavigationObj.Account.Ocnsearchcriterion,"OCN Search criterion");
		sendKeys(AccountNavigationObj.Account.Ocnsearchcriterion,AccountNo,"OCN Search criterion");
		
		verifyExists(AccountNavigationObj.Account.searchButton,"searchButton");
		click(AccountNavigationObj.Account.searchButton,"searchButton");
		Reusable.waitForSiebelLoader();
		
		verifyExists(AccountNavigationObj.Account.AccountNamelink,"Account Name link");
		click(AccountNavigationObj.Account.AccountNamelink,"Account Name link");
		Reusable.waitForAjax();

//		verifyExists(AccountNavigationObj.Account.Customername,"Customer Name");
//		click(AccountNavigationObj.Account.Customername,"Customer Name");		
	}
}
