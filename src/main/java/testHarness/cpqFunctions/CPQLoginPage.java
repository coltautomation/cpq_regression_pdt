package testHarness.cpqFunctions;

import java.awt.AWTException;
import java.io.IOException;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CLoginObj;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;
import testHarness.commonFunctions.ReusableFunctions;


public class CPQLoginPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public String CPQLogin(String username, String password) throws InterruptedException, AWTException, IOException 
	{
		waitForElementToAppear(CPQLoginObj.Login.userNameTxb, 5);
		verifyExists(CPQLoginObj.Login.userNameTxb,"Username textbox");
		sendKeys(CPQLoginObj.Login.userNameTxb, username,"Username textbox field");
	
		verifyExists(CPQLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(CPQLoginObj.Login.passWordTxb, password,"Password textbox field");
		verifyExists(CPQLoginObj.Login.loginBtn,"Login button");
		click(CPQLoginObj.Login.loginBtn,"Login button");
		
		waitForAjax();
		//waitForElementToAppear(CPQLoginObj.Login.ManImg, 10);
		//verifyExists(CPQLoginObj.Login.ManImg,"Man Image on Landing Page");
		
		return "True";
	}
	
	public void SelectQuote(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		verifyExists(CPQLoginObj.Login.GearIcon,"Gear Icon on Header of Landing Page");
		click(CPQLoginObj.Login.GearIcon,"Gear Icon on Header of Landing Page");
		/*if(verifyExists(CPQLoginObj.Login.MyProfilelink))
		{
			waitForElementToAppear(CPQLoginObj.Login.MyProfilelink, 10);
			verifyExists(CPQLoginObj.Login.MyProfilelink,"Header Oracle Quote link on Landing Page");
		
			//CPQ SE Approval Process
			click(CPQLoginObj.Login.MyProfilelink,"Header Oracle Quote link");
			Reusable.waitForpageloadmask();
		}
		
		waitForElementToAppear(CPQLoginObj.Login.headerOracleQuotelink, 10);
		verifyExists(CPQLoginObj.Login.headerOracleQuotelink,"Header Oracle Quote link on Landing Page");
		
		//CPQ SE Approval Process
		click(CPQLoginObj.Login.headerOracleQuotelink,"Header Oracle Quote link");*/
		Reusable.waitToPageLoad();
		//Reusable.waitForpageloadmask();
		//scrollDown(CPQLoginObj.Login.TransactionIcon);
		verifyExists(CPQLoginObj.Login.TransactionIcon,"TransactionIcon");
		click(CPQLoginObj.Login.TransactionIcon,"TransactionIcon");
		Reusable.waitForpageloadmask();
		Reusable.waitToPageLoad();
		
		//Open Quote details
		String Quote_ID = DataMiner.fngetcolvalue(testDataFile,sheetName, scriptNo, dataSetNo,"Quote_ID");
		String quoteIdObj = CPQLoginObj.Login.QuoteID1+ Quote_ID +CPQLoginObj.Login.QuoteID2;
		waitForElementToAppear(quoteIdObj, 10);
		verifyExists(quoteIdObj,"Quote ID on Transaction list Page");
		click(quoteIdObj,"Quote ID");
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
	} 
	
	public void SelectStdQuote(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String Quote_Type) throws InterruptedException, AWTException, IOException 
	{
		if(Quote_Type.equalsIgnoreCase("Std_Quote")) {		
			waitForElementToAppear(CPQLoginObj.Login.headerOracleQuotelink, 10);
				
			//CPQ CST Approval Process		
			verifyExists(CPQLoginObj.Login.headerOracleQuotelink,"Header Oracle Quote link");
			click(CPQLoginObj.Login.headerOracleQuotelink,"Header Oracle Quote link");
				
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile,sheetName, scriptNo, dataSetNo,"Quote_ID");
			String quoteIdObj = CPQLoginObj.Login.QuoteID1+ Quote_ID +CPQLoginObj.Login.QuoteID2;
			
			waitForElementToAppear(quoteIdObj, 10);
			verifyExists(quoteIdObj,"Quote ID");
			click(quoteIdObj,"Quote ID");
			Reusable.WaitforCPQloader();
			waitForAjax();
			Reusable.waitForpageloadmask();
			
		}
		
	} 
	
	public void CPQLogout() throws InterruptedException, AWTException, IOException 
	{
		waitForElementToAppear(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn, 20);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn);
		click(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn,"Log out button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		//waitForElementToAppear(CPQLoginObj.Login.userNameTxb, 10);
		//verifyExists(CPQLoginObj.Login.userNameTxb,"Username textbox");
	}

	public String QT_C4CLogin() throws InterruptedException, IOException 
	{
		verifyExists(C4CLoginObj.Login.userNameTxb,"Username textbox");
		sendKeys(C4CLoginObj.Login.userNameTxb,Configuration.QT_Username,"Username textbox");
	
		verifyExists(C4CLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(C4CLoginObj.Login.passWordTxb,Configuration.QT_Password,"Password textbox");
		
		waitForAjax();
		verifyExists(C4CLoginObj.Login.loginBtn,"Login button");
		click(C4CLoginObj.Login.loginBtn,"Login button");
		
		waitForAjax();
		waitForElementToAppear(C4CLoginObj.Login.homeMenu,120);
		verifyExists(C4CLoginObj.Login.homeMenu,"Home Tab on Landing Page");
		
		return "True";

	}
	
}
