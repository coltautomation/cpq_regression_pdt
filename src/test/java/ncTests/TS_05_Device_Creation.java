package ncTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.ncFunctions.DeviceCreation;
import testHarness.ncFunctions.NcLogin;


public class TS_05_Device_Creation extends SeleniumUtils{
	
//	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	
	Properties prop = new Properties();
	
	NcLogin Login = new NcLogin();
	DeviceCreation CreateDevice = new DeviceCreation();
		
	GlobalVariables g = new GlobalVariables();
	
	@Parameters({"scriptNo","dataSetNo","dataSheet","ScenarioName"})
	@Test
	public void DeviceCreation(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws Exception
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) 
		{

            // load a properties file
            prop.load(input);
		} 
		catch (IOException ex) 
		{
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        
        try
        {
        	Login.Login(Configuration.NC_Username, Configuration.NC_Password);
			Login.VerifySuccessLogin();
			CreateDevice.NcCreateDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
        }
	catch(Exception e)
	{
        Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
        ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
        ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	} 
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
	}
	
}
