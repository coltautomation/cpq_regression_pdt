package testHarness.aptFunctions;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;
import pageObjects.aptObjects.APT_MCN_Create_CustomerObj;

public class APT_MCS_CreateFirewallDevice extends SeleniumUtils {

	public void navigatetomanagecoltnetwork() {
		mouseMoveOn(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink);
		waitForAjax();
	}

	public void navigatetocreateaccesscoredevicepage() throws InterruptedException, IOException {

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink,
				"create access core device link");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink,
				"create access core device link");
		waitForAjax();
	}

	public void alertMessage_IQNet() throws IOException {
		if (isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader)) {
			String alertMsg = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertBody,"IQNet_alert Body");
			if (alertMsg.isEmpty()) {
				clickOnBankPage();
				waitForAjax();
			} else {
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "IONet xButton");
			}
		}
	}

	public void edittextFields_SNMPversion(String locator, String Fieldname, String Value) throws IOException {
		if (isVisible(locator)) {
			String actualvalueInsidetextfield = getAttributeFrom(locator, "value");
			if (actualvalueInsidetextfield.isEmpty()) {
				Log.info("No values displaying under" + Fieldname + "text field");
				clearTextBox(locator);
				waitForAjax();
				sendKeys(locator, Value);
			} else {
				clearTextBox(locator);
				waitForAjax();
				sendKeys(locator, Value);
			}
		} else {
			Log.info(Fieldname + " text field is not displaying");
		}

	}

	public void verifydevicecreation_AccessRouter(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		String vendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");

		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String devicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceType");
		//String vendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String modularmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modular_MSP");
		String fulliqnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Full_IQNET");
		String snmpro2cvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
		String routerid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterID");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management_Address");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCity");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityName");
		String Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityCode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteCode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseName");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseCode");
		String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingCityValue");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing_SiteValue");
		String newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSite");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing_PremiseValue");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremise");

		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "ok button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "ok button");
		waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_Country,
				"warning Message Country");
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_city, "warning Message City");
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.WarningMesassage_site,
				"warning Message Site");
		scrollUp();
		waitForAjax();

		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_name, "warning Message Name");
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_deviceType,
				"warning Message Device Type");
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_vendor,
				"warning Message Vendor/Model");
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_routerId,
				"warning Message Router ID");

		// name
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "name text field");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, name);

		// devicetype
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput);
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, "Device Type");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, devicetype, "Device Type");
		//addDropdownValues_commonMethod("Device Input",APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.devicetypeinput,devicetype);

		// vendormodel
		scrollUp();
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput);
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor/Model");
		//select(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.vendormodelinput, vendorModel);
		addDropdownValues_commonMethod("Vendor/Model",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, vendorModel);

		
		// modular msp selection
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP CheckBox");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, modularmsp);

		// full iqnet selection
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET");
		//check(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.fulliqnetcheckbox, fulliqnet);
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox,"Full iqnet");
		
		alertMessage_IQNet();
		/*if (fulliqnet.equalsIgnoreCase("yes")) {
			alertMessage_IQNet();
		}*/

		// SNMP version
		// snmpro
		edittextFields_SNMPversion(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,
				"snmpro text field", snmpro2cvalue);

		// Router id
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, "Router Id");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, routerid);

		// Management address
		//javascriptexecutor(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.managementaddresstextbox);
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,
				//"Management address");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox, managementaddress);

		// select country
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryDropdown_selectTag);
		waitForAjax();
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryDropdown_selectTag, Country,"Country");
		//addDropdownValues_commonMethod("Country",APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.countryDropdown_selectTag,Country);

		// New City
		if (existingcity.equalsIgnoreCase("no") & newcity.equalsIgnoreCase("yes")) {
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "add city switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "add city switch");
			// City name
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, cityname);
			// City Code
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, Citycode);
			// Site name
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
					//"Site Name");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
					sitename);
			// Site Code
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
					//"Site Code");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
					sitecode);
			// Premise name
			//verifyExists(
					//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
					//"Premise name");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
					premisename);
			// Premise Code
			//verifyExists(
					//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
					//"Premise code");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
					premisecode);

		} // Existing City
		else if (existingcity.equalsIgnoreCase("yes") & newcity.equalsIgnoreCase("no")) {
			scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.cityDropdown_selectTag);
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.cityDropdown_selectTag, "City Dropdown");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.cityDropdown_selectTag, existingcityvalue,"City Dropdown");
			//addDropdownValues_commonMethod("existing city value",APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.cityDropdown_selectTag,existingcityvalue);

			// Existing Site
			if (existingsite.equalsIgnoreCase("yes") & newsite.equalsIgnoreCase("no")) {
				scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.siteDropdown_selectTag);
				//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.siteDropdown_selectTag,
						//"Site Dropdown");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.siteDropdown_selectTag, existingsitevalue,"existing Site value");
				//addDropdownValues_commonMethod("existing city value",APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.siteDropdown_selectTag,existingcityvalue);

				// Existing Premise
				if (existingpremise.equalsIgnoreCase("yes") & NewPremise.equalsIgnoreCase("no")) {
					selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premiseDropdown_selectTag,
							existingpremisevalue,"existing primes value");
					//addDropdownValues_commonMethod("existing city value",APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.premiseDropdown_selectTag,existingpremisevalue);

				} // New Premise
				else if (existingpremise.equalsIgnoreCase("no") & NewPremise.equalsIgnoreCase("yes")) {

					click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch, "add premise switch");
					// Premise name
					//verifyExists(
							//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
							//"Premise name");
					sendKeys(
							APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
							premisename);

					// Premise Code
					//verifyExists(
							//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
							//"Premise code");
					sendKeys(
							APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
							premisecode);
				}
			} else if (existingsite.equalsIgnoreCase("no") & newsite.equalsIgnoreCase("yes")) {
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "add site switch");
				// Site name
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,
						//"Site Name");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,
						sitename);
				// Site Code
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,
						//"Site Code");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,
						sitecode);
				// Premise name
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,
						//"Premise name");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,
						premisename);
				// Premise Code
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,
						//"Premise code");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,
						premisecode);

			}
		}
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		waitForAjax();
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		waitForAjax();
	}

	public void verifysuccessmessage() throws IOException, InterruptedException {
		waitForAjax();
		scrollUp();
		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.succesMsg_alertDisplay,
		// "Success message");

	}

	public void verifyenteredValue_forDeviceCreation(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String devicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceType");
		String vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String routerid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterID");
		String snmprocvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
		String managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management_Address");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingCity");
		String newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCity");
		String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingCityValue");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCityName");
		String existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingSite");
		String newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSite");
		String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing_SiteValue");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
		String existingpremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPremise");
		String NewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremise");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Existing_PremiseValue");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewPremiseName");

		// Device Name
		compareText_InViewPage("Name", name);

		// Device type
		compareText_InViewPage("Device Type", devicetype);

		// Vendor/model
		compareText_InViewPage("Vendor/Model", vendormodel);

		// Router Id
		compareText_InViewPage("Router Id", routerid);

		// Snmpro
		if (snmprocvalue.equalsIgnoreCase("null")) {
			compareText_InViewPage("Snmpro", "JdhquA5");
		} else {
			compareText_InViewPage("Snmpro", snmprocvalue);
		}

		// Management Address
		compareText_InViewPage("Management Address", managementaddress);

		// Country
		compareText_InViewPage1("Country", Country);

		// City
		if ((existingcity.equalsIgnoreCase("yes")) && (newcity.equalsIgnoreCase("NO"))) {
			// Existing City
			compareText_InViewPage("City", existingcityvalue);

		} else if ((existingcity.equalsIgnoreCase("NO")) && (newcity.equalsIgnoreCase("Yes"))) {
			// new City
			compareText_InViewPage("City", cityname);
		}

		// Site
		if ((existingsite.equalsIgnoreCase("yes")) && (newsite.equalsIgnoreCase("NO"))) {
			// Existing Site
			compareText_InViewPage("Site", existingsitevalue);
		} else if ((existingsite.equalsIgnoreCase("No")) && (newsite.equalsIgnoreCase("Yes"))) {
			// New Site
			compareText_InViewPage("Site", sitename);
		}

		// Premise
		if ((existingpremise.equalsIgnoreCase("yes")) && (NewPremise.equalsIgnoreCase("NO"))) {
			// Existing premise
			compareText_InViewPage("Premise", existingpremisevalue);

		} else if ((existingpremise.equalsIgnoreCase("No")) && (NewPremise.equalsIgnoreCase("Yes"))) {
			// new premise
			compareText_InViewPage("Premise", premisename);
		}

	}

	public void verifydeviceEdit_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editdeviceName");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editVendorModel");
		String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editRouterID");
		String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editModularMSP");
		String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");
		String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editModularMSP");
		String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCity");
		String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewCity");
		String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSite");
		String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewCityName");
		String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editFullIQNET");
		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editManagementAddress");
		String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewCityCode");
		String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewSiteName");
		String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewSiteCode");
		String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewPremiseName");
		String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewPremiseCode");
		String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCityValue");
		String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremise");
		String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewSite");
		String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewPremise");
		String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSiteValue");
		String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremiseValue");

		scrollUp();
		waitForAjax();
		 
		// Action Button Click
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		waitForAjax();

		// Edit Button Click
		//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
		String EditButtonClick = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit,"Edit");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");

		// Device Name
		edittextFields_commonMethod("Name", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,
				editDevicename);

		// Device Type

		//selectByIndex(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.devicetypeinput, 1, "");
		/*String Expacted = getTextFrom(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.devicetypeinput);
		if (Expacted.equalsIgnoreCase("Firewall")) {
			Report.LogInfo("DeviceVerification", "Device type is displaying as expected", "PASS");
		} else {
			Report.LogInfo("DeviceVerification", "Device type not is displaying as expected", "FAIL");
		}*/

		// Vendor Model
		addDropdownValues_commonMethod("Vendor Model", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, editVendorModel);

		// Router Id
		edittextFields_commonMethod("Router Id", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, editRouterID);

		// snmpro
		edittextFields_SNMPversion(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,"Snmpro", editSnmProNewValue);

		// Modular msp
		editcheckbox_commonMethod(editModularMSP, APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP");

		// full IQNET
		editcheckbox_commonMethod(editFullIQNET, APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET");
		// alertMessage_editIQNet(application);
		alertMessage_IQNet();
		// scrollDown();
		waitForAjax();
		// Thread.sleep(2000);

		// management Address
		edittextFields_commonMethod("Management Address", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox, editManagementAddress);

		// Country
		if (!editCountry.equalsIgnoreCase("Null")) {

			selectValueInsideDropdown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryDropdown_selectTag, "Country", editCountry);

			// New City
			if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {

				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "add city switch");
				// City name
				//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, editNewCityName);
				// addtextFields_commonMethod(application, "City Name",
				// "citynameinputfield", editNewCityName, xml);
				// City Code
				//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, editNewCityCode);
				// addtextFields_commonMethod(application, "City Code",
				// "citycodeinputfield", editNewCityCode, xml);
				// Site name
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,
						//"Site Name");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, editNewSiteName);
				// addtextFields_commonMethod(application, "Site Name",
				// "sitenameinputfield_addCityToggleSelected",editNewSiteName,
				// xml);
				// Site Code
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						//"Site Code");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,
						editNewSiteCode);
				// addtextFields_commonMethod(application, "Site Code",
				// "sitecodeinputfield_addCityToggleSelected",editNewSiteCode,
				// xml);
				// Premise name
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						//"Premise Name");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,
						editNewPremiseName);
				// addtextFields_commonMethod(application, "Premise Name",
				// "premisecodeinputfield_addCityToggleSelected",editNewPremiseName,
				// xml);
				// Premise Code
				//verifyExists(
						//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						//"Premise Code");
				sendKeys(
						APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,
						editNewPremiseCode);
				// addtextFields_commonMethod(application, "Premise Code",
				// "premisenameinputfield_addCityToggleSelected",editNewPremiseCode,
				// xml);

			} // Existing City
			else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {

				selectValueInsideDropdown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.cityDropdown_selectTag,
						"City", editExistingCityValue);

				// Existing Site
				if (editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
					selectValueInsideDropdown(
							APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.siteDropdown_selectTag, "Site",
							editExistingSiteValue);

					// Existing Premise
					if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
						selectValueInsideDropdown(
								APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premiseDropdown_selectTag,
								"Premise", editExistingPremiseValue);
					}
					// New Premise
					else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {
						click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addpremiseswitch,
								"add premise switch");
						// Clickon_addToggleButton(application,
						// "addpremiseswitch");

						// Premise name
						//verifyExists(
								//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
								//"premise code input field add Premise Toggle Selected");
						sendKeys(
								APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,
								editNewPremiseName);
						// addtextFields_commonMethod(application, "Premise
						// Name","premisecodeinputfield_addPremiseToggleSelected",
						// editNewPremiseName, xml);
						// Premise Code
						//verifyExists(
								//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
								//"premise name input field add Premise Toggle Selected");
						sendKeys(
								APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,
								editNewPremiseCode);

						// addtextFields_commonMethod(application, "Premise
						// Code","premisenameinputfield_addPremiseToggleSelected",
						// editNewPremiseCode, xml);
					}
				}

				else if (editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {
					click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addsiteswitch, "add site switch");
					// Clickon_addToggleButton(application, "addsiteswitch");
					// Site name
					//verifyExists(
							//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,
							//"Site Name input field add Premise Toggle Selected");
					sendKeys(
							APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,
							editNewSiteName);
					// addtextFields_commonMethod(application, "Site Name",
					// "sitenameinputfield_addSiteToggleSelected",editNewSiteName,
					// xml);

					// Site Code
					//verifyExists(
							//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,
							//"Site Code input field add Premise Toggle Selected");
					sendKeys(
							APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,
							editNewSiteCode);
					// addtextFields_commonMethod(application, "Site Code",
					// "sitecodeinputfield_addSiteToggleSelected",editNewSiteCode,
					// xml);

					// Premise name
					//verifyExists(
							//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,
							//"premise Code input field add Premise Toggle Selected");
					sendKeys(
							APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,
							editNewPremiseName);
					// addtextFields_commonMethod(application, "Premise
					// Name","premisecodeinputfield_addSiteToggleSelected",
					// editNewPremiseName, xml);

					// Premise Code
					//verifyExists(
							//APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,
							//"premise name input field add Premise Toggle Selected");
					sendKeys(
							APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,
							editNewPremiseCode);
					// addtextFields_commonMethod(application, "Premise
					// Code","premisenameinputfield_addSiteToggleSelected",
					// editNewPremiseCode, xml);
				}
			}

		} else if (editCountry.equalsIgnoreCase("Null")) {

			// ExtentTestManager.getTest().log(LogStatus.PASS, " No changes made
			// for 'Country' dropdown");

			// City
			editCity(editExistingCity, editNewCity, APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "selectcityswitch", "addcityswitch",
					editExistingCityValue, editNewCityName, editNewCityCode, "City");

			// Site
			editSite(editExistingSite, editNewSite, APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "selectsiteswitch", "addsiteswitch",
					editExistingSiteValue, editNewSiteName, editNewSiteCode, "Site");

			// Premise
			editPremise(editExistingPremise, editNewPremise, APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "selectpremiseswitch",
					"addpremiseswitch", editExistingPremiseValue, editNewPremiseName, editNewPremiseCode, "Premise");

		}

		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		waitForAjax();
		// click_commonMethod(application, "OK", "okbutton", xml);
		// Thread.sleep(3000);
		// ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Device is
		// edited successfully");

	}

	public void editCity(String editExistingCity, String editNewCity, String dropdown_xpath,
			String selectCityToggleButton, String addCityToggleButton, String dropdownValue, String editNewCityName,
			String editNewCityCode, String labelname) throws InterruptedException, IOException {
		if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
			existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
		} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
			existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
		} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
			newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
		} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
			newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
		}

		else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
			Report.LogInfo("EditCity", "No chnges made under 'City' field", "PASS");
		}

	}

	public void existingCity(String dropdown_xpath, String dropdownValue, String selectCityToggleButton,
			String labelname) throws InterruptedException, IOException {

		if (isVisible(dropdown_xpath)) {

			selectByValue(dropdown_xpath, dropdownValue, labelname);
			// addDropdownValues_commonMethod(application, labelname,
			// dropdown_xpath, dropdownValue, xml);

		} else {

			click(selectCityToggleButton,"Country to be select");
			waitForAjax();
			// click_commonMethod(application, "Select City toggle button",
			// selectCityToggleButton, xml);
			// Thread.sleep(1000);

			selectByValue(dropdown_xpath, dropdownValue, labelname);
			// addDropdownValues_commonMethod(application, labelname,
			// dropdown_xpath, dropdownValue, xml);

		}
	}

	public void newCity(String dropdown_xpath, String addCitytoggleButton, String editNewCityName,
			String editNewCityCode) throws InterruptedException, IOException {

		if (isVisible(dropdown_xpath)) {

			click(addCitytoggleButton,"Country ti be selected");
			waitForAjax();
			// click_commonMethod(application, "Select City toggle button",
			// addCitytoggleButton, xml);
			// Thread.sleep(1000);
			// City name
			edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
			// City Code
			edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
		} else {
			// City name
			edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
			// City Code
			edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
		}
	}

	public void editSite(String editExistingCity, String editNewCity, String dropdown_xpath,
			String selectSiteToggleButton, String addSitetoggleButton, String dropdownValue, String editNewSiteName,
			String editNewSiteCode, String labelname) throws InterruptedException, IOException {

		if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
			existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
		} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
			existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
		} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
			newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
		} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
			newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
		} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
			Report.LogInfo("EditSite", "No changes made under 'Site' field", "PASS");
		}

	}

	public void existingSite(String dropdown_xpath, String dropdownValue, String selectSiteToggleButton,
			String labelname) throws InterruptedException, IOException {

		if (isVisible(dropdown_xpath)) {

			selectByValue(dropdown_xpath, dropdownValue, labelname);
		} else {
			click(selectSiteToggleButton,"Site to be selected");
			waitForAjax();
			// Thread.sleep(1000);
			selectByValue(dropdown_xpath, dropdownValue, labelname);

		}
	}

	public void newSite(String dropdown_xpath, String addSitetoggleButton, String editNewSiteName,
			String editNewSiteCode, String labelname) throws InterruptedException, IOException {

		if (isVisible(dropdown_xpath)) {
			click(addSitetoggleButton,"Site to be selected");
			waitForAjax();
			// click_commonMethod(application, "Select City toggle button",
			// addSitetoggleButton, xml);
			// Thread.sleep(1000);
			// Site name
			edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
			// Site Code
			edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
		} else {
			// Site name
			edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
			// Site Code
			edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
		}
	}

	public void editPremise(String editExistingPremise, String editNewPremise, String dropdown_xpath,
			String selectPremiseToggleButton, String addPremisetoggleButton, String dropdownValue,
			String editNewPremiseName, String editNewPremiseCode, String labelname)
			throws InterruptedException, IOException {

		if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
			existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
		} else if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("null")) {
			existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
		} else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("Yes")) {
			newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
		} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("Yes")) {
			newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
		} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("null")) {
			Report.LogInfo("EditPremise", "No changes made under 'Premise' field", "PASS");

		}

	}

	public void existingPremise(String dropdown_xpath, String dropdownValue, String selectPremiseToggleButton,
			String labelname) throws InterruptedException, IOException {

		if (isVisible(dropdown_xpath)) {
			selectByValue(dropdown_xpath, dropdownValue, labelname);
			// addDropdownValues_commonMethod(application, labelname,
			// dropdown_xpath, dropdownValue, xml);
		} else {
			click(selectPremiseToggleButton,"Premise to be selected");
			waitForAjax();
			// click_commonMethod(application, "Select Premise toggle button",
			// selectPremiseToggleButton, xml);
			// Thread.sleep(1000);
			selectByValue(dropdown_xpath, dropdownValue, labelname);
			// addDropdownValues_commonMethod(application, labelname,
			// dropdown_xpath, dropdownValue, xml);
		}
	}

	public void newPremise(String dropdown_xpath, String addPremisetoggleButton, String editNewPremiseName,
			String editNewPremiseCode) throws InterruptedException, IOException {

		if (isVisible(dropdown_xpath)) {
			click(addPremisetoggleButton,"Premise to be selected");
			waitForAjax();
			// click_commonMethod(application, "Select Premise toggle button",
			// addPremisetoggleButton, xml);
			// Thread.sleep(1000);
			// Premise name
			edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
					editNewPremiseName);

			// Premise Code
			edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
					editNewPremiseCode);
		} else {
			// Premise name
			edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
					editNewPremiseName);
			// Premise Code
			edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
					editNewPremiseCode);
		}
	}

	public void testStatus() throws InterruptedException {
		String element = null;
		String status = null;
		int listSize = getXPathCount(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.tableRow);

		if (listSize >= 1) {

			for (int i = 1; i <= listSize; i++) {
				// element = findWebElement("(//tbody/tr[" + i +
				// "]/td)[1]").getText();
				element = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[1]")).getText();

				if (element.isEmpty()) {

				} else {
					Report.LogInfo("TestStatus", "Test Name is displaying as: " + element, "PASS");
					// ExtentTestManager.getTest().log(LogStatus.PASS, "Test
					// Name is displaying as: " + element);
					// Log.info("Test Name is displaying as: " + element);
					status = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[2]/div")).getAttribute("class");
					// status = findWebElement("(//tbody/tr[" + i +
					// "]/td)[2]/div").getAttribute("class");
					// Log.info("status displays as: " + status);
					if (status.contains("red")) {
						Report.LogInfo("TestStatus", "status colour dipslays as: red", "PASS");
					} else if (status.contains("green")) {
						Report.LogInfo("TestStatus", "status colour dipslays as: green", "PASS");
					}
				}
			}
		} else {
			Report.LogInfo("TestStatus",
					"Command/Status table is not displaying in view device page. Instead Message displays as: ",
					"PASS");
			Report.LogInfo("TestStatus", "* Device reachability tests require the device to have an IP address.",
					"PASS");

		}

	}

	public void verifEditedValue_Firewall(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceType");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editVendorModel");
		String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editRouterID");
		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editManagementAddress");
		String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editFullIQNET");
		String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");
		String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCity");
		String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewCity");
		String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingCityValue");
		String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewCityName");
		String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSite");
		String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewSite");
		String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingSiteValue");
		String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewSiteName");
		String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremise");
		String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewPremise");
		String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editExistingPremiseValue");
		String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editNewPremiseName");

		// Device name
		if (editDevicename.equalsIgnoreCase("Null")) {
			compareText_InViewPage_ForNonEditedFields("Name");
		} else {
			compareText_InViewPage("Name", editDevicename);
		}

		// Device Type
		compareText_InViewPage("Device Type", DeviceType);

		// Vendor/Model
		if (editVendorModel.equalsIgnoreCase("Null")) {
			compareText_InViewPage_ForNonEditedFields("Vendor/Model");
		} else {
			compareText_InViewPage("Vendor/Model", editVendorModel);
		}

		// Router Id
		if (editRouterID.equalsIgnoreCase("Null")) {
			compareText_InViewPage_ForNonEditedFields("Router Id");
		} else {
			compareText_InViewPage("Router Id", editRouterID);
		}

		// Management Address
		if (editManagementAddress.equalsIgnoreCase("Null")) {
			compareText_InViewPage_ForNonEditedFields("Management Address");
		} else {
			compareText_InViewPage("Management Address", editManagementAddress);
		}

		// Full IQNET
		if (editFullIQNET.equalsIgnoreCase("Null")) {
			// compareText_InViewPage_ForNonEditedFields(application, "Full
			// IQNET", xml);
			Log.info("No changes made");
		} else if (editFullIQNET.equalsIgnoreCase("yes")) {

			compareText_InViewPage("Full IQNET", "Null");

		} else if (editFullIQNET.equalsIgnoreCase("no")) {

			compareText_InViewPage("Full IQNET", "Null");

		}

		// Country
		if (editCountry.equalsIgnoreCase("Null")) {
			compareText_InViewPage_ForNonEditedFields("Country");
		} else {
			compareText_InViewPage("Country", editCountry);
		}

		// City
		if ((editExistingCity.equalsIgnoreCase("Null") && editNewCity.equalsIgnoreCase("Null"))
				|| (editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("no"))) {

			compareText_InViewPage_ForNonEditedFields("City");
		} else if (editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no")) {

			compareText_InViewPage("City", editExistingCityValue);

		} else if (editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes")) {

			compareText_InViewPage("City", editNewCityName);
		}

		// Site
		if ((editExistingSite.equalsIgnoreCase("Null") && editNewSite.equalsIgnoreCase("Null"))
				|| (editExistingSite.equalsIgnoreCase("no") && editNewSite.equalsIgnoreCase("no"))) {

			compareText_InViewPage_ForNonEditedFields("Site");
		} else if (editExistingSite.equalsIgnoreCase("yes") && editNewSite.equalsIgnoreCase("no")) {

			compareText_InViewPage("Site", editExistingSiteValue);

		} else if (editExistingSite.equalsIgnoreCase("no") && editNewSite.equalsIgnoreCase("yes")) {

			compareText_InViewPage("Site", editNewSiteName);
		}

		// Premise
		if ((editExistingPremise.equalsIgnoreCase("Null") && editNewPremise.equalsIgnoreCase("Null"))
				|| (editExistingPremise.equalsIgnoreCase("no") && editNewPremise.equalsIgnoreCase("no"))) {

			compareText_InViewPage_ForNonEditedFields("Premise");
		} else if (editExistingPremise.equalsIgnoreCase("yes") && editNewPremise.equalsIgnoreCase("no")) {

			compareText_InViewPage("Premise", editExistingPremiseValue);

		} else if (editExistingPremise.equalsIgnoreCase("no") && editNewPremise.equalsIgnoreCase("yes")) {

			compareText_InViewPage("Premise", editNewPremiseName);
		}

	}

	public void routerPanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String editedVendor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editVendorModel");
		String addedVendor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String managementAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editManagementAddress");
		String managementAddressCreated = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management_Address");

		String command_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv4");
		String command_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv6");
		String vrf_Ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");
		String vrf_Ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv6");

		String vendorModel = null;
		// String editedVendor = map.get("editVendorModel");
		// String addedVendor = map.get("VendorModel");
		if (editedVendor.equalsIgnoreCase("null")) {
			vendorModel = addedVendor;
		} else {
			vendorModel = editedVendor;
		}

		// String managementAddressEdit = map.get("editManagementAddress");
		// String managementAddressCreated = map.get("Management Address");

		if (vendorModel.startsWith("Cisco")) {
			if (managementAddressEdit.equalsIgnoreCase("null")) {

				routerPanel(command_ipv4, command_ipv6, managementAddressCreated, vrf_Ipv4, vrf_Ipv6);
			} else {
				routerPanel(command_ipv4, command_ipv6, managementAddressEdit, vrf_Ipv4, vrf_Ipv6);
			}
		} else {
			Report.LogInfo("INFO", "Router Panel will not display for the selected vendorModel: " + vendorModel,
					"PASS");
		}

		// logger = ExtentTestManager.startTest("fetchDeviceInterface");
		String DeviceName = null;
		String VendorModel = null;
		String managementAddress = null;
		String country = null;

		String editdeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editdeviceName");
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editVendorModel");
		String VendorModell = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
		String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editManagementAddress");
		String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String SnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
		String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editSnmProNewValue");

		// Device name
		if (editdeviceName.equalsIgnoreCase("null")) {
			DeviceName = Name;
		} else {
			DeviceName = editdeviceName;
		}

		// Vendor/Model
		if (editVendorModel.equalsIgnoreCase("null")) {
			VendorModel = VendorModell;
		} else {
			VendorModel = editVendorModel;
		}

		// Management Address
		if (editManagementAddress.equalsIgnoreCase("null")) {
			managementAddress = managementAddressCreated;
		} else {
			managementAddress = editManagementAddress;
		}

		// Country
		if (editCountry.equalsIgnoreCase("null")) {
			country = Country;
		} else {
			country = editCountry;
		}

		// Snmpro
		String Snmpro = null;
		String snmproCreated = SnmProNewValue;
		String snmproEdited = editSnmProNewValue;
		String snmproDefaultValue = "JdhquA5";

		if (snmproEdited.equalsIgnoreCase("null")) {
			if (snmproCreated.equalsIgnoreCase("null")) {
				Snmpro = snmproDefaultValue;
			} else {
				Snmpro = snmproCreated;
			}
		} else {
			Snmpro = snmproEdited;
		}
	}

	public void routerPanel(String commandIPv4, String commandIPv6, String ipAddress, String vrfname_ipv4,
			String vrfname_ipv6) throws InterruptedException, IOException {

		// scrollDown();
		// Thread.sleep(1000);

		// Command IPV4
		// selectByValue(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.commandIPV4_dropdown,
		// commandIPv4,
		// "Command IPV4");
		addDropdownValues_commonMethod("Command IPV4",
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown, commandIPv4);

		// addDropdownValues_commonMethod("Command IPV4",
		// "commandIPV4_dropdown", commandIPv4);

		hostnametextField_IPV4(commandIPv4, ipAddress);

		vrfNametextField_IPV4(commandIPv4, vrfname_ipv4);

		executeCommandAndFetchTheValue(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4);

		// Commmand IPV6
		addDropdownValues_commonMethod("Command IPV6",
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown, commandIPv6);

		hostnametextField_IPV6(commandIPv6, ipAddress);

		vrfNametextField_IPV6(commandIPv6, vrfname_ipv6);

		executeCommandAndFetchTheValue(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6);

	}

	public void hostnametextField_IPV4(String command_ipv4, String ipAddress) {
		boolean IPV4availability = false;
		IPV4availability = isVisible(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield);

		if (IPV4availability) {
			// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield, ipAddress);
		} else {
			Report.LogInfo("LogStatus",
					"'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4, "INFO");
			Report.LogInfo("LogStatus",
					"'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4, "INFO");
		}
	}

	public void vrfNametextField_IPV4(String command_ipv4, String vrfname_ipv4) {
		boolean IPV4availability = false;

		if (command_ipv4.contains("vrf")) {
			IPV4availability = isVisible(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField);

			if (IPV4availability) {
				// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,
						vrfname_ipv4);
			} else {
				Report.LogInfo("LogStatus", "'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4,
						"INFO");
			}
		} else {
			Report.LogInfo("LogStatus", "'VRF Name IPv4' text field is not displaying for " + command_ipv4, "INFO");
		}

	}

	public void executeCommandAndFetchTheValue(String executeButton) throws InterruptedException, IOException {

		click(executeButton,"Execute");
		// click_commonMethod(application, "Execute", executeButton, xml);

		boolean resultField = false;
		resultField = isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea);
		if (resultField) {
			Report.LogInfo("LogStatus", "''Result' text field is displaying", "PASS");

			String remarkvalue = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea,"Result text Area");
			Report.LogInfo("LogStatus", "value under 'Result' field displaying as " + remarkvalue, "PASS");

		} else {
			Report.LogInfo("LogStatus", "'Result' text field is not displaying", "FAIL");
		}
	}

	public void hostnametextField_IPV6(String commandIPv6, String ipv6Address)
			throws InterruptedException, IOException {
		boolean IPV4availability = false;
		IPV4availability = isVisible(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield);
		if (IPV4availability) {

			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield,
					//"IP Address or Hostname");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield, ipv6Address);

		} else {
			Report.LogInfo("INFO", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6,
					"PASS");
		}
	}

	public void vrfNametextField_IPV6(String commandIPV6, String vrfname_IPV6)
			throws InterruptedException, IOException {
		boolean IPV6availability = false;

		if (commandIPV6.equalsIgnoreCase("vrf")) {

			IPV6availability = isVisible(
					APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField);
			if (IPV6availability) {
				//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,
						//"Router Vrf Name");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,
						vrfname_IPV6);

			} else {
				Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6, "PASS");
			}
		} else {
			Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6, "PASS");
		}

	}

	public void fetchDeviceInterface_viewdevicepage() throws InterruptedException, IOException {

		scrollUp();
		waitForAjax();
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action);
		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,
		// "Action");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		waitForAjax();

		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage,
				"fetch Device interface link view Device Page");
		waitForAjax();
		// verify success Message
		String expectedValue = "Fetch interfaces started successfully. Please check the sync status of this device";
		boolean successMessage = false;

		successMessage = isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert);
		String actualMessage = getTextFrom(
				APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert,"success Message_Alert");

		if (successMessage) {
			if (actualMessage.isEmpty()) {
				Report.LogInfo("INFO", "Success message is not displaying", "PASS");
			}
			if (actualMessage.contains(expectedValue)) {
				Report.LogInfo("INFO", "After clicking on 'Fetch Device Interface' link, success Message is displaiyng",
						"PASS");
				Report.LogInfo("INFO", " Success Message displays as: " + actualMessage, "PASS");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,
						"Click here Link_fetch Interface");
				waitForAjax();
			} else if (actualMessage.equalsIgnoreCase(expectedValue)) {
				Report.LogInfo("INFO",
						"After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected",
						"PASS");
				Report.LogInfo("INFO", " Success Message displays as: " + actualMessage, "PASS");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,
						"Click here Link_fetch Interface");
				waitForAjax();
			} else {
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,
						"manage Link_view Device page");
				waitForAjax();
			}

		} else {
			Report.LogInfo("INFO", "After clicking on 'Fetch Device Interface' link, success Message is not displaying",
					"PASS");
		}
	}

	public void verifydeviceDelete_AccessRouter() throws IOException, InterruptedException {
		if (isVisible(APT_MCN_Create_CustomerObj.CreateCustomer.breadCrumbNav)) {
			click(APT_MCN_Create_CustomerObj.CreateCustomer.breadCrumbNav, "BreadCrumb");
			waitForAjax();
		}
		scrollUp();

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,
		// "Action");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");

		waitForAjax();

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete,
		// "Delete Device");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete, "Delete Device");

		waitForAjax();

		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.delete_alertpopup,
		// "Delete Alert Popup");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deletebutton, "Delete Button");

		//// verifyExists(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.deleteDeviceConfMsg,
		//// "Delete Device Confirmation Message");
		// getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg);

	}

}
