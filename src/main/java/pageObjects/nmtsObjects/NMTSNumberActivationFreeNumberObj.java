package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSNumberActivationFreeNumberObj {
	
	public static class NumberActivationFreeNumber
	{
		
		public static final String userNameTxb="@xpath=//*[@id='s_swepi_1']"; //Username

	//	public static final String CancelReserveCheckbox="@xpath=((//table[@id='ucxRangeList_grdView']//tr)[2]/td)[8]/input";
		public static final String SelectReservedRangeCheckbox="@xpath=(//table[@id='ucxRangeList_grdView']//input[contains(@id,'ucxRangeList_grdView')])[1]";
		
		public static final String ReservedRangeSelectMarkedLink="@xpath=(//a[contains(text(),'Select Marked')])[1]";
	//	public static final String SelectedRangeCheckrange="@xpath=(//table[@id='ucxSelectRange_grdView']//tr[2]/td)[8]";
		public static final String SelectedRangeCheckbox="@xpath=(//table[@id='ucxSelectRange_grdView']//input[contains(@id,'ucxSelectRange_grdView')])[1]";
		
		public static final String selectDecision="@xpath=//select[@id='cboProcessingStatus']";
		public static final String ContactNo="@xpath=//input[@id='txtCancelReason']";
		public static final String ContractSource="@xpath=//select[@id='cboContractSource']";
		public static final String SubmitDecision="@xpath=//input[@id='btnSubmitDecision']";
		public static final String MRN="@xpath=//input[@id='txtMRN']";
		public static final String VATnumber="@xpath=//input[@id='txtVatNumber']";
		public static final String EndCustName="@xpath=//input[@id='txtCustName']";

	//	public static final String BuildingName="@xpath=//form[@id='frmReservationDetail_New']//input[@id='txtBuild']";
	//	public static final String BuildingNumber="@xpath=//form[@id='frmReservationDetail_New']//input[@id='txtBuildNumber']";
	//	public static final String PostCodeNMTS="@xpath=//form[@id='frmReservationDetail_New']//input[@name='txtPostCode']";

		public static final String BuildingName="@xpath=//input[@id='txtBuild']";
		public static final String BuildingNumber="@xpath=//input[@id='txtBuildNumber']";
	
		public static final String StreetName="@xpath=//input[@id='txtStreet']";
		public static final String Town="@xpath=//input[@id='txtTown']";
		public static final String PostCodeNMTS="@xpath=//input[@name='txtPostCode']";

		public static final String ResidentialRadiobtn="@xpath=//table[@id='radCustomerType']//tr/td[2]/input";

		public static final String flatNumber="@xpath=//input[@id='txtFlatNumber']";
		public static final String Extention="@xpath=//input[@id='txtExtension']";
		public static final String buildingnumberletter="@xpath=//select[@id='ddlBuildNoLetter']";
		public static final String AllovationFirstName="@xpath=//input[@id='txtFirstName']";
		public static final String AllovationlastName="@xpath=//input[@id='txtLastName']";
		public static final String CustomerTitle="@xpath=//input[@id='txtCustomerTitle']";
		public static final String AllocationLanguage="@xpath=//input[@id='txtLanguage']";
		public static final String RegisteredName="@xpath=//input[@id='txtRegisteredName']";
		public static final String TelephoneNumber="@xpath=//input[@id='txtTelephone']";
		public static final String CIFvalue="@xpath=//input[@id='txtCIF']";
		public static final String AllocationMessage="@xpath=//*[@id='lblSuccessfulMsg']";

		public static final String ActivateNumberTab="@xpath=//font[contains(@onclick,'M=AO')]";
		public static final String SelectReservationColt="@xpath=//select[@id='ucxOrderHistory_cboHostedNumberFilter']";
		public static final String madeBy="@xpath=//select[@id='ucxOrderHistory_cboSiteUsers']";
		public static final String ActivateAvailability="@xpath=//select[@id='ucxOrderHistory_cboAvailability']";
		public static final String FilterBy="@xpath=//select[@id='ucxOrderHistory_cboFilterOption']";
		public static final String matching="@xpath=//select[@id='ucxOrderHistory_cboFilterMethod']";
		public static final String FilterText="@xpath=//input[@id='ucxOrderHistory_txtFilterText']";
		public static final String SubmitSearchbtn="@xpath=//input[@id='ucxOrderHistory_btnSubmitSearch']";
		public static final String OrderId="@xpath=//table[@id='ucxOrderHistory_grdView']//tr[2]/td[1]";
		public static final String SelectAllocatedRangeCheckbox="@xpath=(//table[@id='ucxRangeList_grdView']//input[contains(@id,'ucxRangeList_grdView')])[1]";
		public static final String AllocatedRangeSelectMarkedLink="@xpath=(//a[contains(text(),'Select Marked')])[1]";
		public static final String AllocatedSelectedRangeCheckbox="@xpath=(//table[@id='ucxSelectRange_grdView']//input[contains(@id,'ucxSelectRange_grdView')])[1]";
		public static final String ActivateNumnerbtn="@xpath=//input[@id='btnActivateNumbers']";
		public static final String ActivateNumberMessage="@xpath=//*[@id='lblSuccessMsg']";
		public static final String ProcessingMessage="@xpath=//*[@id='lblProcessing']/font";
		public static final String PopupYesbtn="@xpath=//img[contains(@src,'Images/General/Yes.JPG')]/..";
		
		
		

	}

}