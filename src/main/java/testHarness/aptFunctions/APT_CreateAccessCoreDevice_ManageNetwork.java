package testHarness.aptFunctions;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;


import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.ManageNetwork_createAccessCOreDeviceObj;
import pageObjects.aptObjects.ManageNetwork_createAccessCOreDeviceObj.ManageNetwork;
import testHarness.commonFunctions.ReusableFunctions;

public class APT_CreateAccessCoreDevice_ManageNetwork extends SeleniumUtils{
	
	ReusableFunctions Reusable = new ReusableFunctions();

	
	public static String InterfaceAddress;
	public void verifyFetchInterface(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
		{
			String Inservice_status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InServiceStatus");
			String Inmaintenance_status =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InMaintenanceStatus");
			//String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmProNewValue");
			String country =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
			String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterfaceName_Mg_Network");
			//String FMC_column =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FMC_column");
			//String SMARTS_column= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SMARTS_column");
			//String DCS_column =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DCS_column");
			//String FetchInterfaces_column= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FetchInterfaces_column");
			//String Vistamart_column=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Vistamart_column");
			//String PGW_column=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PGW_column");
			String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Name");
			//String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
			String managementaddress= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address");
			String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
			String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
			String ExSite =	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
			String ExSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSiteValue");
			String ExPremise= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
			String ExPremiseValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremiseValue");
			String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
			String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
			String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
			String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
			String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
			String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
			String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editdeviceName");
			String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editVendorModel");
			//String editsnmppro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmProNewValue");
			
			Report.LogInfo("INFO", "VerifyFetchInterFace", "PASS");
			try {	
				
				
				
				compareText("Manage Network header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.managenetwork_header,"Managenetwork Header");
				
				compareText("Status header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_header, "Status");
				compareText("Synchronization header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_header, "Synchronization");
				
				//verify column headers
				compareText("Device column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicecolumn, "Device");
				compareText("Status column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_statuscloumn, "Status");
				compareText("Last Modification column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_lastmodificationcolumn, "Last Modification");
				compareText("Action column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_Action, "Action");
				compareText("Device column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_devicecolumn, "Device");
				compareText("Sync Status column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_syncstatuscolumn, "Sync Status");
				compareText("Smarts column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_smartscolumn, "Smarts");
				if(managementaddress.equalsIgnoreCase("null")) {
					
					ExtentTestManager.getTest().log (LogStatus.FAIL, "'Fetch Interface' column do not display, if no values passed for 'Management Address'");				
					Report.LogInfo("INFO", "'Fetch Interface' column do not display, if no values passed for 'Management Address'", "FAIL");
					
				}else {
					compareText( "Fetch Interfaces column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_FetchInterfacescolumn, "Fetch Interfaces");
				}
				
				if(isVisible(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_vistamartdevice)){
					compareText("VistaMart Device column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_vistamartdevice, "VistaMart Device");
				}
				
				compareText("Action column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_actioncolumn, "Action");
				
				//verify Status panel column values
				if(!editDevicename.equalsIgnoreCase("Null"))
					compareText("Device",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue, editDevicename);
				else
				{
					compareText("Device",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue, Name);
					
				}
				//String ServiceStatus= GetText(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_statusvalue);
				String ServiceStatus= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_statusvalue,"Status Value");
				
				if(ServiceStatus.equalsIgnoreCase(Inservice_status))
				{
					compareText("Status",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_statusvalue, Inservice_status);
				}
				else
				{
					compareText("Status",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_statusvalue, Inmaintenance_status);
				}
				
				//verify last modification
				try {
				String GMTValue;
				String LastModificationvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_lastmodificationvalue, "Status Modification value");
				//System.out.println("Last modified date: " +LastModificationvalue);
				SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
				
				if (LastModificationvalue.length() > 3) 
				{
				    GMTValue = LastModificationvalue.substring(LastModificationvalue.length() - 3);
				} 
				else
				{
					GMTValue = LastModificationvalue;
				}
				//sa.assertEquals(GMTValue, "GMT");
						
				}catch(Exception e)
				{
					e.printStackTrace();
					//ExtentTestManager.getTest().log (LogStatus.FAIL, "Step : Last Modification column value field is not displaying");
				
					Report.LogInfo("INFO","'Fetch Interface' column do not display, if no values passed for 'Management Address'","FAIL");
				}
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_statuslink,"Status Link");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_statuslink,"Status Link");
				
				waitForAjax();
					compareText("Status page header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.Statuspage_header, "Device");
					
					//verify field headers in status page
					compareText("Name field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_nameheader, "Name");
					
					compareText("Vendor/Model field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_vendormodelheader, "Vendor/Model");
					compareText("Management Address field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_managementaddressheader, "Management Address");
					compareText("Snmpro field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_snmproheader, "Snmpro");
					compareText("Country field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_countryheader, "Country");
					compareText("City field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_cityheader, "City");
					compareText("Site field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_siteheader, "Site");
					compareText("Premise field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_premiseheader, "Premise");
					
					//verify field values in status page
					//compareText("Name",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_namevalue, devicename);
					
					if(editDevicename.equalsIgnoreCase("Null"))
						compareText("Device",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue, Name );
					else
					{
						compareText("Device",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue, editDevicename);
						
					}
					//compareText("Vendor/Model",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_vendormodelvalue, vendormodel);
					
					if(!editVendorModel.equalsIgnoreCase("Null") && !editVendorModel.contains("Traffic Aggregator"))
						
						compareText("Vendor/Model",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_vendormodelvalue,editVendorModel);
						
						else if(!VendorModel.contains("Traffic Aggregator"))
					{
						compareText("Vendor/Model",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_vendormodelvalue,VendorModel);	
					}
					
					if(!editVendorModel.equalsIgnoreCase("Null") && editVendorModel.contains("Traffic Aggregator"))
						
						compareText("Vendor/Model",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_vendormodelvalue,"Traffic Aggregator Cisco");
						
						else if(VendorModel.contains("Traffic Aggregator"))
					{
						compareText("Vendor/Model",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_vendormodelvalue,"Traffic Aggregator Cisco");	
					}
					compareText("Management Address",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_managementaddressvalue, managementaddress);
					
					compareText("Country", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_countryvalue, country);
					
					if(newCity.equalsIgnoreCase("Yes")&& ExCity.equalsIgnoreCase("NO") ){
						compareText("City", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_cityvalue,newCityName);
					}else if(newCity.equalsIgnoreCase("No")&& ExCity.equalsIgnoreCase("yes") ){
						compareText("City", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_cityvalue,ExCityValue);
					}
					if(newSite.equalsIgnoreCase("Yes")&& ExSite.equalsIgnoreCase("NO")){
						compareText("Site", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_sitevalue,newSiteName);
					}else if(newSite.equalsIgnoreCase("No")&& ExSite.equalsIgnoreCase("Yes")){
						compareText("Site", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_sitevalue,ExSiteValue);
					}
					if(newPremise.equalsIgnoreCase("Yes")&& ExPremise.equalsIgnoreCase("NO")){
						compareText("Premise", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_premisevalue,newPremiseName);
					}else if(newPremise.equalsIgnoreCase("No")&& ExPremise.equalsIgnoreCase("yes")){
						compareText("Premise", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_premisevalue,ExPremiseValue);
					}
				
				compareText("Status header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.Statuspage_statusheader, "Status");
				compareText("Current Status field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_currentstatusfieldheader, "Current Status");
				compareText("New Status field header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusfieldheader, "New Status");
				compareText("Current Status",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_currentstatusvalue, Inservice_status);
				
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusdropdown,"New Status Dropdown");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusdropdown,"New Status Dropdown");
				
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusdropdownvalue,"New Status Dropdown Value");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusdropdownvalue,"New Status Dropdown Value");
				
				String NewStatusvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusdropdownvalue,"New Status Dropdown Value");
				
				Report.LogInfo("INFO", "New Status Value is: "+NewStatusvalue, "PASS");
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_okbutton,"Status Page Ok Button");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_okbutton,"Status Page Ok Button");
				
				waitForAjax();
				scrollUp();
				
				compareText( "Device status update success message",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.Sync_successmsg, "Device Status history successfully changed");
				
				waitForAjax();
				//scrolltoend();
				scrollDown(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_statuscolumnheader);
				//verify 'new status' table column headers
				compareText("Status column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_statuscolumnheader, "Status");
				compareText("Changed On column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_changedon_columnheader, "Changed On");
				compareText("Changed By column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_changedby_columnheader, "Changed By");
				
				//verify 'new status' table column values
				//Device status history table
				int TotalPages;
				String TotalPagesText = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbTotal']")).getText();
				TotalPages = Integer.parseInt(TotalPagesText);
				//System.out.println("Total number of pages in table is: " + TotalPages);

				if (TotalPages != 0) {

					outerloop:
					for (int k = 1; k <= TotalPages; k++) {

						String AddedInterface= webDriver.findElement(By.xpath("//div[@role='grid']//div[@ref='eBodyViewport']/div")).getAttribute("style");
						if(!AddedInterface.contains("height: 1px"))
						{
						List<WebElement> results = webDriver.findElements(By.xpath("//div[@class='modal-content']//div[@class='ag-div-margin row']//div[@ref='eBodyContainer']//div[@role='row']"));
						int numofrows = results.size();
						

						if ((numofrows == 0)) {

						
							Report.LogInfo("INFO", "Device Status History is empty", "PASS");
						}
						else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent']")).getText();
						int Current_page = Integer.parseInt(CurrentPage);
						
							for (int i = 0; i < numofrows; i++) {
								try {

									String Devicehistorydata = results.get(i).getText();
									
									if (Devicehistorydata.contains(NewStatusvalue)) 
									{
										//ExtentTestManager.getTest().log (LogStatus.PASS, "Device status history table has data");
										Report.LogInfo("INFO", "Device status history table has data", "PASS");
										waitForAjax();
										compareText("Status", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusvalue, NewStatusvalue);
										
										try {
											String GMTValue;
											//String ChangedOnvalue= GetText(application, "Changed On", "statuspage_changedonvalue");
											String ChangedOnvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_newstatusvalue,"New status value");
											//System.out.println("Changed On value: " +ChangedOnvalue);
											SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
											
											if (ChangedOnvalue.length() > 3) 
											{
											    GMTValue = ChangedOnvalue.substring(ChangedOnvalue.length() - 3);
											} 
											else
											{
												GMTValue = ChangedOnvalue;
											}
											//sa.assertEquals(GMTValue, "GMT");
											//assertEquals( "GMT",GMTValue,"GMT Value verification");
													
											}catch(Exception e)
											{
												e.printStackTrace();
												Report.LogInfo("INFO", "Step : Changed on column value field is not displaying", "FAIL");
											}
										
									compareText("Changed By", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_changedbyvalue, "colttest@colt.net");
										waitForAjax();
										
										verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_closebutton,"Status Close Button");
										click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_closebutton,"Status Close Button");
										break outerloop;
									}

								} catch (StaleElementReferenceException e) {
									
									e.printStackTrace();
								}
							}
						}
						}
						else
						{
							ExtentTestManager.getTest().log (LogStatus.PASS, "No interfaces added");
							Report.LogInfo("INFO", "No interfaces added", "PASS");
						}
					}
					
				}else {
					ExtentTestManager.getTest().log (LogStatus.PASS, "No data available in table");
					Report.LogInfo("INFO", "No data available in table", "PASS");
				}
				
				//verify view interfaces page
				waitForAjax();
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_viewinterfaceslink,"Click on Interface link");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_viewinterfaceslink,"Click on Interface link");
				waitForAjax();
				compareText("Interface page header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterfacepage_header,"Interfaces");
				String AddedInterface= webDriver.findElement(By.xpath("//div[@role='grid']//div[@ref='eBodyViewport']/div")).getAttribute("style");
				if(!AddedInterface.contains("height: 1px"))
				{
				compareText("Device Name column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_devicenamecolumnheader, "Device Name");
				compareText("Interface Name column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacename_columnheader, "Interface Name");
				compareText("Interface Address column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfaceaddress_columnheader, "Interface Address");
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceAddressRowValue,"InterfaceAddressRowValue");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceAddressRowValue,"InterfaceAddressRowValue");
				//sendKeys(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceAddressRowValue.Keys.TAB);
				SendkeyusingAction(Keys.TAB);
				
				compareText("Interface Type column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_columnheader, "Interface Type");
				
				//WebElement InterfaceTypeRowValue= findWebElement(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue);
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
				//sendKeys(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue, Keys.TAB);
				SendkeyusingAction(Keys.TAB);
				
				
				compareText("Status column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_columnheader, "Status");
				//WebElement StatusRowValue= findWebElement(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_rowvalue);
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
				//sendKeys(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_rowvalue,Keys.TAB);
				SendkeyusingAction(Keys.TAB);
				
				compareText("Last Modification column header",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_lastmod_columnheader, "Last Modification");
				
				waitForAjax();
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_closebutton,"Status Close Button");
				waitForAjax();
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_viewinterfaceslink,"Status Interface Link");
				waitForAjax();
				
				int TotalPages1;
				String TotalPagesText1 = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbTotal']")).getText();
				TotalPages1 = Integer.parseInt(TotalPagesText1);
			//	System.out.println("Total number of pages in table is: " + TotalPages1);

				if (TotalPages1 != 0) {

					outerloop:
					for (int k = 1; k <= TotalPages1; k++) {

						List<WebElement> results = webDriver.findElements(By.xpath("//div[@role='row']//div[@role='gridcell'][@col-id='name']"));

						int numofrows = results.size();
						

						if ((numofrows == 0)) {

							
							Report.LogInfo("INFO", "Interface table is empty", "PASS");
						}
						else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent']")).getText();
						int Current_page = Integer.parseInt(CurrentPage);
						

						
							for (int i = 0; i < numofrows; i++) {
								try {

									String AddedInterfacedata = results.get(i).getText();
									
									if (AddedInterfacedata.equalsIgnoreCase(interfacename)) {

										String InterfaceNameRowID= getAttributeFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceNameRowID1 +interfacename+ ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceNameRowID2,"row-id");
									
										
										//verify interface values in table
										String DeviceNamevalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.DeviceNamevalue1+InterfaceNameRowID+ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.DeviceNamevalue2,"Device name value");
										//ExtentTestManager.getTest().log (LogStatus.PASS, "Step: Interface Device Name value is displayed as : "+DeviceNamevalue);
										Report.LogInfo("INFO", "Step: Interface Device Name value is displayed as : "+DeviceNamevalue, "PASS");
										
										String InterfaceNamevalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceNamevalue1+InterfaceNameRowID+ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceNamevalue2,"Interface name value");
										Report.LogInfo("INFO", "Step: Interface Name value is displayed as : : "+InterfaceNamevalue, "PASS");
										
										String InterfaceAddressvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceAddressvalue1+InterfaceNameRowID+ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceAddressvalue2,"Interface name value");
										Report.LogInfo("INFO", "Step: Interface Address value is displayed as : "+InterfaceAddressvalue, "PASS");
										//WebElement InterfaceAddressRowValue1= driver.findElement(By.xpath("(//div[@role='gridcell'][@col-id='address'])[1]"));
										
										verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceAddressRowValue1,"InterfaceAddressRowValue1");
										click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.InterfaceAddressRowValue1,"InterfaceAddressRowValue1");
										//sendKeys(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.Keys.TAB);
										SendkeyusingAction(Keys.TAB);
										
										String InterfaceTypevalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='type.desc']")).getText();
										
										Report.LogInfo("INFO", "Step: Interface Type value is displayed as : "+InterfaceTypevalue, "PASS");
										//WebElement InterfaceTypeRowValue1= findWebElement(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue);
										
										verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
										click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue,"interfacetype_rowvalue");
										//sendKeys(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interfacetype_rowvalue.Keys.TAB);
										SendkeyusingAction(Keys.TAB);
										
										String Statusvalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='currentStatus.desc']")).getText();
										
										Report.LogInfo("INFO", "Step: Status value is displayed as : "+Statusvalue, "PASS");
										//WebElement StatusRowValue1= getwebelement(xml.getlocator("//locators/" + application + "/viewinterface_status_rowvalue"));
										verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
										click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_rowvalue,"viewinterface_status_rowvalue");
										//sendKeys(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_status_rowvalue.Keys.TAB);
										SendkeyusingAction(Keys.TAB);
										
										String LastModificationvalue= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='m_time']")).getText();
										
										Report.LogInfo("INFO", "Step: Last Modification value is displayed as : "+LastModificationvalue, "PASS");
										//WebElement LastModificationRowValue= getwebelement(xml.getlocator("//locators/" + application + "/viewinterface_lastmod_rowvalue"));
										
										verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_lastmod_rowvalue,"viewinterface_lastmod_rowvalue");
										click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_lastmod_rowvalue,"viewinterface_lastmod_rowvalue");
										//sendKeys(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_lastmod_rowvalue.Keys.TAB);
										SendkeyusingAction(Keys.TAB);
										
										WebElement StatusLink= webDriver.findElement(By.xpath("//div[@role='gridcell']/parent::div[@row-id="+InterfaceNameRowID+"]//div[@col-id='Status']/div/a"));
										verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.StatusLink1+ InterfaceNameRowID +ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.StatusLink2,"Status link");
										click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.StatusLink1+ InterfaceNameRowID +ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.StatusLink2,"StatusLink2");
										
										Report.LogInfo("INFO", "Step: Clicked on Status link in interface table", "PASS");
										InterfaceAddress= InterfaceAddressvalue;
										break outerloop;
									}
									
								} catch (StaleElementReferenceException e) {
									
									e.printStackTrace();

								}
							}
							verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.nextButton, "Next Button");
							click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.nextButton, "Next Button");
							waitForAjax();
						}
					}
					
				//verify status page headers & field names
				compareText("Interface header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.statuspage_interfaceheader, "Interface");
				compareText("Name field header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_namefield, "Name");
				compareText("Interface Address field header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_interfaceaddressfield, "Interface Address");
				compareText("Status header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_statusheader, "Status");
				compareText("Current Status field header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_currentstatusfield, "Current Status");
				compareText("New Status field header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusfield, "New Status");
				
				//verify status page field values
				compareText("Name", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_namevalue, interfacename);
				compareText("Interface Address", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_interfaceaddressvalue, InterfaceAddress);
				compareText("Current Status", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_currentstatusvalue, Inservice_status);
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusdropdown,"Interface Status page New Status Dropdown");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusdropdown,"Interface Status page New Status Dropdown");
				//WebElement selectNewStatusvalue1= findWebElement(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusdropdownvalue);
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusdropdownvalue,"interface_statuspage_newstatusdropdownvalue");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusdropdownvalue,"interface_statuspage_newstatusdropdownvalue");
				String NewStatusvalue1= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusdropdownvalue,"interface status dropdown value");
				//ExtentTestManager.getTest().log (LogStatus.PASS, "New Status Value is: "+NewStatusvalue1);
				Report.LogInfo("INFO", "New Status Value is: "+NewStatusvalue1, "PASS");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_okbutton,"interface_statuspage_okbutton");
				waitForAjax();
				scrollUp();
				compareText("Interface status update success message", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.Sync_successmsg, "Interface Status History successfully changed.");
				waitForAjax();
				//scrolltoend();
				scrollDown(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_statuscolumnheader);
				waitForAjax();
				//verify 'new status' table column headers
				compareText("Status column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_statuscolumnheader, "Status");
				compareText("Changed On column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_changedon_columnheader, "Changed On");
				compareText("Changed By column header", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_changedby_columnheader, "Changed By");
				
				//verify 'new status' table column values
				//verify interface status history table
				int TotalPages2=0;
				String TotalPagesText2 = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbTotal'])[1]")).getText();
				TotalPages2 = Integer.parseInt(TotalPagesText2);
				

				if (TotalPages2 != 0) {

					outerloop:
					for (int k = 1; k <= TotalPages2; k++) {

						List<WebElement> results = webDriver.findElements(By.xpath("(//div[@ref='eBodyContainer'])[2]//div[@role='row']"));

						int numofrows = results.size();
					

						if ((numofrows == 0)) {

							//ExtentTestManager.getTest().log (LogStatus.PASS, "Interface Status History is empty");
							Report.LogInfo("INFO", "Interface Status History is empty", "PASS");
						}
						else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("(//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent'])[1]")).getText();
						int Current_page = Integer.parseInt(CurrentPage);
						

						
							for (int i = 0; i < numofrows; i++) {
								try {
									String Interfacehistorydata = results.get(i).getText();
								
									if (Interfacehistorydata.contains(NewStatusvalue1)) 
									{
										
										Report.LogInfo("INFO", "Interface status history table has data", "PASS");
										waitForAjax();
										compareText("New Status", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_newstatusvalue, NewStatusvalue1);
										try {
											String GMTValue;
											String ChangedOnvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_changedonvalue,"interface status page change value");
											
											SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
											
											if (ChangedOnvalue.length() > 3)
											{
											    GMTValue = ChangedOnvalue.substring(ChangedOnvalue.length() - 3);
											} 
											else
											{
												GMTValue = ChangedOnvalue;
											}
											
													
											}catch(Exception e)
											{
												e.printStackTrace();
												
												Report.LogInfo("INFO", "Step : Changed On column value field is not displaying", "FAIL");
											}
										
									    compareText("Changed By", "interface_statuspage_changedbyvalue", "colttest@colt.net");
										waitForAjax();
										verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_closebutton,"Status Page Close Button");
										click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.interface_statuspage_closebutton,"Status Page Close Button");
										break outerloop;
									}

								} catch (StaleElementReferenceException e) {
							
									e.printStackTrace();

								}
							}
						}
					}
					
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No data available in status history table");
					Report.LogInfo("INFO", "No data available in status history table", "FAIL");
				}
				}else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "No Interface added in table");
					Report.LogInfo("INFO", "No data available in Interface table", "PASS");
					
				}
				}else
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "No Interface added in table");
					Report.LogInfo("INFO", "No Interface added in table", "PASS");
					
				}
				
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_closebutton,"Close Interface Popup");
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.viewinterface_closebutton,"Close Interface Popup");
				waitForAjax();
				
				//verify synchronization panel column values
				waitForAjax();
				//scrolltoend();
				scrollDown(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_devicevalue);
				
				//compareText("Device", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_devicevalue, devicename);
				if(!editDevicename.equalsIgnoreCase("Null"))
					compareText("Device",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue, editDevicename);
				else
				{
					compareText("Device",ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue, Name);	
				}
				String syncstatusvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_syncstatusvalue,"synchronization syn cstatus value");
				Report.LogInfo("INFO", "Sync Status value is: "+syncstatusvalue, "INFO");
				
				//verify smarts value
				
				String smartsvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_smartsvalue,"synchronization smarts value");
				Report.LogInfo("INFO", "Sync Status value is: "+smartsvalue, "INFO");
				//verify smarts date time 
				try {
					String GMTValue;
					String Smartsvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.smarts_datetimevalue,"smarts date time value");
					String SmartsDateTimevalue= "";
					if (Smartsvalue.length() > 20) 
					{
						SmartsDateTimevalue = Smartsvalue.substring(Smartsvalue.length() - 20);
						
					} 
					else 
					{
						SmartsDateTimevalue = Smartsvalue;
					}

					
					Report.LogInfo("INFO", "Smarts date value is displayed as: "+SmartsDateTimevalue, "PASS");
					SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
					if (SmartsDateTimevalue.length() > 3) 
					{
					    GMTValue = SmartsDateTimevalue.substring(SmartsDateTimevalue.length() - 3);
					} 
					else
					{
						GMTValue = SmartsDateTimevalue;
					}
				
					
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				
				//verify fetch interfaces value
				if(managementaddress.equalsIgnoreCase("null")) {
					
					//Log.info("fetch Interface column will not display, if management address value is null");
					Report.LogInfo("INFO", "fetch Interface column will not display, if management address value is null", "INFO");
					
				//verify vistamart device value
					String vistamart=getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_vistamartValue_noManageAddress,"synchronization vista mart Value no Manage Address");
					
					Report.LogInfo("INFO", "fetch Interface column will not display, if management address value is null"+vistamart, "INFO");
					//verify vistamart device date time
					try {
						String GMTValue;
						String VistaMartDevicevalue= (ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.vistamartDEvice_dateTime_noManageAddress);
						String VistaMartDevice_DateTimevalue= "";
						if (VistaMartDevicevalue.length() > 20) 
						{
							VistaMartDevice_DateTimevalue = VistaMartDevicevalue.substring(VistaMartDevicevalue.length() - 20);
						
						} 
						else 
						{
							VistaMartDevice_DateTimevalue = VistaMartDevicevalue;
						}
						
						
						Report.LogInfo("INFO", "Vistamart Device date value is displayed as: "+VistaMartDevice_DateTimevalue, "PASS");
						SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
						if (VistaMartDevice_DateTimevalue.length() > 3) 
						{
						    GMTValue = VistaMartDevice_DateTimevalue.substring(VistaMartDevice_DateTimevalue.length() - 3);
						} 
						else
						{
							GMTValue = VistaMartDevice_DateTimevalue;
						}
						
						
						}catch(Exception e)
						{
							e.printStackTrace();
						}
					
					//verify synchronize link
					waitForAjax();
					click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_synchronizelink,"synchronization_synchronizelink");
					waitForAjax();
					scrollUp();
					waitForAjax();
					//isElementPresent("//locators/" + application + "/Sync_successmsg");
					compareText("Synchronize Success Msg", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.Sync_successmsg, "Sync started successfully. Please check the sync status of this device.");
					waitForAjax();
					
					
				}
				else {
					String fetchinterfacesvalue=getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_fetchinterfacesvalue,"synchronization fetch interfaces value");
					Report.LogInfo("INFO", "Value for Fetch Interfaces value is: "+fetchinterfacesvalue, "INFO");
					//verify fetch interfaces date time
					try {
						String GMTValue;
						String FetchInterfacesvalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.fetchinterfaces_datetime,"fetch interfaces date time");
						String FetchInterfaces_DateTimevalue= "";
						if (FetchInterfacesvalue.length() > 20) 
						{
							FetchInterfaces_DateTimevalue = FetchInterfacesvalue.substring(FetchInterfacesvalue.length() - 20);
							
						} 
						else 
						{
							FetchInterfaces_DateTimevalue = FetchInterfacesvalue;
						}

						
						Report.LogInfo("INFO", "Fetch Interfaces date value is displayed as: "+FetchInterfaces_DateTimevalue, "PASS");
						SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
						if (FetchInterfaces_DateTimevalue.length() > 3) 
						{
						    GMTValue = FetchInterfaces_DateTimevalue.substring(FetchInterfaces_DateTimevalue.length() - 3);
						} 
						else
						{
							GMTValue = FetchInterfaces_DateTimevalue;
						}
						
						
						}catch(Exception e)
						{
							e.printStackTrace();
							
							Report.LogInfo("INFO", "Step : Fetch Interfaces date value field is not displaying", "FAIL");
						}
					
					
				//verify vistamart device value
					String vistamart_device_value = getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_vistamartdevicevalue,"synchronization vista mart device value");
					Report.LogInfo("INFO", "Vistamart Device Value is: "+vistamart_device_value, "PASS");
					//verify vistamart device date time
					try {
						String GMTValue;
						String VistaMartDevicevalue= getTextFrom(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.vistamartdevice_datetime,"vista mart device date time");
						String VistaMartDevice_DateTimevalue= "";
						if (VistaMartDevicevalue.length() > 20) 
						{
							VistaMartDevice_DateTimevalue = VistaMartDevicevalue.substring(VistaMartDevicevalue.length() - 20);
							
						} 
						else 
						{
							VistaMartDevice_DateTimevalue = VistaMartDevicevalue;
						}
						
						Report.LogInfo("INFO", "Vistamart Device date value is displayed as: "+VistaMartDevice_DateTimevalue, "PASS");
						SimpleDateFormat sdf= new SimpleDateFormat("yyyy-mm-dd mm:ss");
						if (VistaMartDevice_DateTimevalue.length() > 3) 
						{
						    GMTValue = VistaMartDevice_DateTimevalue.substring(VistaMartDevice_DateTimevalue.length() - 3);
						} 
						else
						{
							GMTValue = VistaMartDevice_DateTimevalue;
						}
						
						
						}catch(Exception e)
						{
							e.printStackTrace();
						}
					
					//verify synchronize link
					waitForAjax();
					click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.synchronization_synchronizelink,"Synchronize Link");
					waitForAjax();
					scrollUp();
					waitForAjax();
					//isElementPresent("//locators/" + application + "/Sync_successmsg");
					compareText("Synchronize Success Msg", ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.Sync_successmsg, "Sync started successfully. Please check the sync status of this device.");
					waitForAjax();
				}
				verifyExists(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue,"Device for Status");	
				click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue,"Click on Device for Status");
				waitForAjax();
				
			}catch(Exception e) {
				e.printStackTrace();
				
				//verify device name link in status panel
				if(isVisible(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue)){
					click(ManageNetwork_createAccessCOreDeviceObj.ManageNetwork.status_devicevalue,"Click Exception");
				}
				waitForAjax();
			}
			}

			
			
	

}
