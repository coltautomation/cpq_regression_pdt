package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_VoiceLine;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_47_APT_MCS_VoiceLineTest extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	APT_Login Login=new APT_Login();
	
	public String CustomerName=null;
	public String newCustomerName=null;
	public String existingCustomer=null;
	public String Gateway;
	public String sid;
	public String removeInterface_Selection;
	public String addInterface_Selection;
	
	APT_VoiceLine VoiceLine = new APT_VoiceLine();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void CreateAccessCoreDevice_AccessRouter(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)throws IOException
	{	
		Report.createTestCaseReportHeader(ScenarioName);
	
	try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
	{

        // load a properties file
        prop.load(input);               

    }
	catch (IOException ex) 
	{
        ex.printStackTrace();
    }
	String dataFileName = prop.getProperty("TestDataSheet");
	File path = new File("./TestData/"+dataFileName);
	String testDataFile = path.toString();
	
	try
	{	
		openurl(Configuration.APT_URL);
		Login.APT_VoiceService();
		newCustomerName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"newCustomerCreation");
        existingCustomer=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"existingCustomerSelection");
        sid=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"ServiceIdentification");
     
        if(newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
             VoiceLine.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
             
             CustomerName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"newCustomer");
             VoiceLine.selectCustomertocreateOrderNewCust(testDataFile, dataSheet, scriptNo, dataSetNo);
              
        }
        else if(newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
            
        	VoiceLine.selectCustomertocreateOrderExCust(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CustomerName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"existingCustomer"); 
        }
       
        VoiceLine.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyselectservicetype(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyservicecreation(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyCustomerDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyUserDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyservicepanelInformationinviewservicepage(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyEditService(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyManageSubnetsIPv6();
		VoiceLine.verifyDump();
		VoiceLine.verifyManageService(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyManagementOptionspanel(testDataFile, dataSheet, scriptNo, dataSetNo);
        VoiceLine.verifyAddASRDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyEditASRDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyViewASRDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifyViewDevicepage_Links(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verify_Cisco_RouterTools(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verify_CiscoVendor_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
		
 
		VoiceLine.selectInterfacelinkforDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
	
		VoiceLine.navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		boolean FetchInterfaceSuccessMsg= VoiceLine.fetchDeviceInterface_viewdevicepage();
		
		if(FetchInterfaceSuccessMsg) {
		VoiceLine.verifyFetchInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
		}
		else {
			VoiceLine.navigateToViewServicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
		}
		
		VoiceLine.addTrunkSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.editSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
	
        newCustomerName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"newCustomerCreation");
        existingCustomer=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"existingCustomerSelection");
        
        VoiceLine.verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.addTrunk(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.viewTrunk_Primary(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.editTrunk(testDataFile, dataSheet, scriptNo, dataSetNo);
	
		
		VoiceLine.verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.addResilienttrunk(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.verifySynchronize();
		
		VoiceLine.verifyAddVoiceCPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.ViewVoiceCPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.EditVoiceCPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		
		String siteOrderNumber=null;
		String edit_TrunkGroupOrderNumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_TrunkGroupOrderNumber");
		
		
		if(edit_TrunkGroupOrderNumber.equalsIgnoreCase("null")) {
			
			siteOrderNumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"TrunkGroupOrderNumber");
					
					
		}else{
			siteOrderNumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_TrunkGroupOrderNumber");
					
		}
		
		VoiceLine.clickOnCPEdeviceLink(siteOrderNumber);
		VoiceLine.addCPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifysuccessmessage("CPE Device added successfully");
		
		VoiceLine.CPEdevice_clickOnViewLink();
		VoiceLine.viewCPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
	
		VoiceLine.CPEdevice_clickOnEditLink();
		VoiceLine.editCPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		
		String DRusingTDMValue= null;
		String edit_DRusingTDM_checkbox=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_DRusingTDM_checkbox");
		
		if(edit_DRusingTDM_checkbox.equalsIgnoreCase("null")) {
			
			DRusingTDMValue=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"DRusingTDM_checkbox");
				
		}
		else {
			DRusingTDMValue=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_DRusingTDM_checkbox");
		
		}
				
		if(DRusingTDMValue.equalsIgnoreCase("Yes")) {
		
			String siteOrderNumber1=null;
			String edit_TrunkGroupOrderNumber1=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_TrunkGroupOrderNumber");
			
			if(edit_TrunkGroupOrderNumber1.equalsIgnoreCase("null")) {
				
				siteOrderNumber1=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"TrunkGroupOrderNumber");
						
						
			}else{
				siteOrderNumber1=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_TrunkGroupOrderNumber");
						
			}
		VoiceLine.clickOnViewTrunkLink(siteOrderNumber1); 
		VoiceLine.verifyAddDRPlans(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.verifyDRPlansBulkInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.verifydownloadDRplans(testDataFile, dataSheet, scriptNo, dataSetNo);
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : DR Links are not displaying as DRusingTDM checkbox is not checked");
			Report.LogInfo("INFO", "Step : DR Links are not displaying as DRusingTDM checkbox is not checked", "INFO");
		}
		
		VoiceLine.VerifyDisasterRecoveryStatus();
		
		waitforPagetobeenable();
		VoiceLine.AddPortGroup(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.ViewPortGroup(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.EditPortGroup(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.OverflowPortGroup(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		
		VoiceLine.AddDDIRange(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.viewDDIRange(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.editDDIRange(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.VerifyVoiceResiliency(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.VerifyPSXcommandExecution(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.VerifyGSXcommandExecution(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		VoiceLine.verifyConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
	
		VoiceLine.deleteDDIRange();
		VoiceLine.deletePortGroup(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.deleteVoiceCPEDevice();
		VoiceLine.deleteCPEDevice();
		
		String siteOrderNumber2=null;
		
		if(edit_TrunkGroupOrderNumber.equalsIgnoreCase("null")) {
			
			siteOrderNumber2=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"TrunkGroupOrderNumber");
			 //map.get("TrunkGroupOrderNumber");
					
		}else{
			siteOrderNumber2=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_TrunkGroupOrderNumber");
			
					
					//map.get("edit_TrunkGroupOrderNumber");
		}
		VoiceLine.deleteResilientTrunk();
		VoiceLine.deleteTrunkSiteOrder(siteOrderNumber2);
		VoiceLine.deleteASRDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		VoiceLine.deleteService();
	
	}
		
	
	catch(Exception e)
	{		Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
		ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		
	}
	
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
	ExtentTestManager.endTest();
	ExtentManager.getReporter().flush();
	WEB_DRIVER_THREAD_LOCAL.get().close();
	
	}
	}
