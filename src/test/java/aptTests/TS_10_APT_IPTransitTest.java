package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_IPTransitObj;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
//import testHarness.aptFunctions.Lanlink_Outbandmanagementhelper;
import testHarness.aptFunctions.searchForDevice;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_10_APT_IPTransitTest extends SeleniumUtils {
	public String CustomerName = null;
	public static String DeviceNameValue = null;
	public static String VendorModelValue = null;

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	searchForDevice searchDevice = new searchForDevice();
	APT_Login Login = new APT_Login();
	APT_IPTransit IPTransit = new APT_IPTransit();
	APT_IPAccessNoCPEHelper NoCPE = new APT_IPAccessNoCPEHelper();
	// Lanlink_Outbandmanagementhelper Outband = new
	// Lanlink_Outbandmanagementhelper();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void APT_IPTransit_01(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException {
		Report.createTestCaseReportHeader(ScenarioName);

		try (InputStream input = new FileInputStream(g.getRelativePath() + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerCreation");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");
			// String DeviceName = DataMiner.fngetcolvalue(testDataFile,
			// dataSheet, scriptNo, dataSetNo, "DeviceName");

			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {

				// CreateCustomer - IP Transit				
				IPTransit.createcustomer(testDataFile, dataSheet, scriptNo,
				  dataSetNo);
				  
				  // selectExistingCustomer - IP Transit
				  NoCPE.selectCustomertocreateOrder(testDataFile, dataSheet,
				  scriptNo, dataSetNo); } else if
				  (newCustomerName.equalsIgnoreCase("no") &&
				  existingCustomer.equalsIgnoreCase("Yes")) {
				  
				  // selectExistingCustomer - IP Transit
				  NoCPE.selectCustomertocreateOrder(testDataFile, dataSheet,
				  scriptNo, dataSetNo); CustomerName = existingCustomer;
				  
				  }
				  
				  // verifyCreateorder - IP Transit"
				IPTransit.createorderservice(testDataFile, dataSheet, scriptNo,dataSetNo);
				  
				  // verifyservicetypeselection - IP Transit
				  IPTransit.verifyselectservicetype(testDataFile, dataSheet,
				  scriptNo, dataSetNo);
				  
				  // verifyservicecreation - IP Transit");
				  IPTransit.verifyServiceCreation(testDataFile, dataSheet,scriptNo, dataSetNo);
				  
				  // verifyCustomerDetailsInformation - IP Transit
				  NoCPE.verifyCustomerDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
				  IPTransit.verifyUserDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
				  
				  // //verifyUserDetailsInformation - IP Transit
				  //IPTransit.VerifyUsersPanel(testDataFile, dataSheet,scriptNo,dataSetNo); //
				  
				  // verifyOrderDetailsInformation - IP Transit");
				  IPTransit.verifyorderpanel_editorder(testDataFile, dataSheet,scriptNo, dataSetNo);
				  IPTransit.verifyorderpanel_changeorder(testDataFile, dataSheet,scriptNo, dataSetNo);
				  
				  // verifyServicepanelinviewservicepage - IP Transit");
				  IPTransit.verifyservicepanelInformationinviewservicepage(
				  testDataFile, dataSheet, scriptNo, dataSetNo);
				  
				  // verifyManagementOptionspanel - IP Transit");
				  IPTransit.verifyManagementOptionspanel(testDataFile,
				  dataSheet, scriptNo, dataSetNo);
				  
				  // verifyServicepanelLinks - IP Transit");
				  IPTransit.verifyEditservice(testDataFile, dataSheet,scriptNo, dataSetNo); // need to check
				  //IPTransit.verifyManageSubnets();
				  IPTransit.verifyManageService(testDataFile, dataSheet, scriptNo,dataSetNo); 
				  IPTransit.verifyDump(); //
				  IPTransit.verifyShowNewInfovistaReport();
				  
				  // addExistingPEDevice - IP Transit
				
				 Report.LogInfo("INFO", " ============Existing ADD PE Device =============== ", "PASS");
				IPTransit.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo); 

				IPTransit.addExistingPEDevice(testDataFile, dataSheet,scriptNo,dataSetNo);
				IPTransit.verifyExistingDevice_ViewDevicedetails();
				IPTransit.deleteExistingDevice(testDataFile, dataSheet,scriptNo,dataSetNo);
				
				 Report.LogInfo("INFO", " ============New ADD PE Device =============== ", "PASS");
				// addNewPEDevice - IP Transit
				IPTransit.navigateToAddNewDevicepage(); // Done
				IPTransit.verifyadddevicefields(); // Done
				IPTransit.addNewPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
				IPTransit.verifyViewpage_Devicedetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				IPTransit.verifyViewDevicepage_Links(); //// Last action and
														//// click on delete
				NoCPE.verifyEditDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				 Report.LogInfo("INFO", " ============Router Toool =============== ", "PASS");
				// verifyRouterTools - IP Transit");
				IPTransit.navigatetoViewDevicepage();
				String DeviceName = IPTransit.DeviceName();
				String VendorModel = IPTransit.VendorModel();
				String managementAddressEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"editManagementAddress");
				String managementAddressCreated = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"ManagementAddress");
				if (VendorModel.contains("Cisco")) {
					if (managementAddressEdit.equalsIgnoreCase("null")) {

						//IPTransit.verify_Cisco_RouterTools(testDataFile,dataSheet,scriptNo,dataSetNo);
					} else {
						// IPTransit.verify_Cisco_RouterTools(testDataFile,
						// dataSheet, scriptNo,
						// dataSetNo,managementAddressEdit);
					}
				} else {
					if (managementAddressEdit.equalsIgnoreCase("null")) {

						IPTransit.verify_Juniper_RouterTools(testDataFile, dataSheet, scriptNo, dataSetNo);
					} else {
						IPTransit.verify_Juniper_RouterTools(testDataFile, dataSheet, scriptNo, dataSetNo);
					}
				}				

				 Report.LogInfo("INFO", " ============ADD Edit Interface =============== ", "PASS");

				// verifyRoutesPanel - IP Transit");
				//IPTransit.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (VendorModel.contains("Cisco")) {
					IPTransit.verify_CiscoVendor_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					NoCPE.verify_CiscoVendor_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPTransit.verify_CiscoVendor_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo, DeviceName);
				} else {
					
					IPTransit.verify_JuniperVendor_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
					IPTransit.verify_JuniperVendor_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
				}

				if (VendorModel.contains("Juniper")) {
					// verifyInterfaceConfigHistory - IP Transit");
					IPTransit.verifyInterfaceConfigHistory(testDataFile, dataSheet, scriptNo, dataSetNo);
				}

				// VerifyManageService - IP Transit
				//NoCPE.verifyManageService(testDataFile, dataSheet, scriptNo, dataSetNo);

				IPTransit.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				// Delete Device - IP Transit");
				IPTransit.deleteDevice(testDataFile, dataSheet, scriptNo, dataSetNo);

				// Delete Service - IP Transit");
				IPTransit.deleteService();

		}

	catch(

	Exception e)
	{
		Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
		ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
		ExtentTestManager.getTest().log(LogStatus.INFO,
				ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

	}

	Report.createTestCaseReportFooter();Report.SummaryReportlog(ScenarioName);ExtentTestManager.endTest();ExtentManager.getReporter().flush();WEB_DRIVER_THREAD_LOCAL.get().close();

}}
