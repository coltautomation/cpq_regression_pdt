package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_wholeSale_Obj;
import pageObjects.aptObjects.Lanlink_DirectFiberObj;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import baseClasses.SeleniumUtils;

public class APT_wholeSale extends SeleniumUtils {
	
	public String primarytrunkGroupname=null;
	
public void serviceSelection(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
	String serviceToBeSelected= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"serviceToBeSelected");
		Report.LogInfo("Info", "select Service type","Info");
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitForAjax();
		addDropdownValues_commonMethod("Service Type", APT_wholeSale_Obj.APT_wholeSale.servicetypeDropdown, serviceToBeSelected);
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitForAjax();
		
		click(APT_wholeSale_Obj.APT_wholeSale.Nextbutton, "Next");
		waitForAjax();
		
	}
	
public void serviceCreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String serviceidentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ServiceIdentification");
	String ImproperEmailID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ImproperEmailID");
	String properMailId= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"properMailId");
	String Phone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Phone");
	String remark= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"remark");
	String PerformanceReporting= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PerformanceReporting");
	String serviceType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"serviceToBeSelected");
	
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	//verify mandatory warning messages
	click(APT_wholeSale_Obj.APT_wholeSale.OKbutton, "OK");
	waitForAjax();
	
	scrollIntoTop();
	waitForAjax();

//Service Identification warning message	
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.serviceIdentificationWarningmsg, "Service Identification");
	
	
//Enter value in all fields
	//Service Identificaton
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.secivceIdentification_textField, serviceidentification,"Service Identification");
	
	//Service Type
		compareText_InViewPage("Service Type", serviceType);
	
	//Remark
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.remark_textField, remark,"Remark");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.Email_addButton);
	
	
	//Email
	waitForAjax();
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.Email_textfield, ImproperEmailID,"Email");
	
	
//checking error message for email field	
	Email_improperFormat(APT_wholeSale_Obj.APT_wholeSale.ImproperEmail_errValidationmsg, "Email");
	
	//Email
	clearTextBox(APT_wholeSale_Obj.APT_wholeSale.Email_textfield);
	waitForAjax();
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.Email_textfield, properMailId,"Email");
	
	click(APT_wholeSale_Obj.APT_wholeSale.Email_addButton, ">>");
	waitForAjax();
	
	//phone contact
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.phoneContact_TextField, Phone,"Phone Contact");
	
	click(APT_wholeSale_Obj.APT_wholeSale.phoneContact_addButton,"Phone");
	waitForAjax();
	
	
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	//Performance Reporting
	addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.performanceReporting_checkbox, "Performance Reporting", PerformanceReporting);
	
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	//Click on "OK" button
	click(APT_wholeSale_Obj.APT_wholeSale.OKbutton, "OK");
}


public void Email_improperFormat(String xpath, String fieldlabelName) throws InterruptedException, IOException {
 	boolean message=false;
 	//Field Error Message
 			try {
 				message = isElementPresent(xpath);
 				waitForAjax();
 		//	sa.assertTrue(message, fieldlabelName + " field error message is not displayed ");
 			if(message) {
 			String ErrMsg =  getTextFrom(xpath,fieldlabelName);
 			
 			Reporter.log( fieldlabelName + " field warning  message displayed as : " + ErrMsg + " when we enter value in wrong format");
 			Report.LogInfo("Info", "  validation message for"+ fieldlabelName +"  field displayed as : " + ErrMsg + " when we enter value in wrong format","PASS");
 			Reporter.log(fieldlabelName + " field warning  message displayed as : " + ErrMsg+ " when we enter value in wrong format");
 			}else{
 				Report.LogInfo("Info", "validation message for"+ fieldlabelName +"  field is not displaying","FAIL");
 				Reporter.log("validation message for"+ fieldlabelName +"  field is not displaying");
 			}
 			}catch(NoSuchElementException e) {
 				e.printStackTrace();
 				Reporter.log( "No warning message displayed for "+ fieldlabelName +" when we enter value in wrong format");
 				Report.LogInfo("Info", "No warning message displayed for "+ fieldlabelName + " when we enter value in wrong format","FAIL");
 			}catch(Exception ed) {
 				ed.printStackTrace();
 				Reporter.log( "No warning message displayed for "+ fieldlabelName);
 				Report.LogInfo("Info", "No warning message displayed for "+ fieldlabelName,"FAIL");
 			}
 }


public void verifyEnteredValuesForServiceCreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,  IOException { 
	
	String serviceidentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ServiceIdentification");
	String ImproperEmailID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ImproperEmailID");
	String properMailId= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"properMailId");
	String Phone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Phone");
	String remark= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"remark");
	String PerformanceReporting= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PerformanceReporting");
	String serviceType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"serviceToBeSelected");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.service);
	waitForAjax();
	
//Service Identification
	compareText_InViewPage("Service Identification", serviceidentification);

//Service Type	
	//Service Type
	compareText_InViewPage("Service Type", serviceType);
	
//Email	
	compareText_InViewPage("Email",  properMailId);
	
//Phone Contact	
	compareText_InViewPage("Phone Contact", Phone);
	
//Remark	
	compareText("Remark", APT_wholeSale_Obj.APT_wholeSale.viewPage_RemarkField, remark);
	
//under Management Options panel
//performance reporting	
	compareText_InViewPage("Performance Reporting", PerformanceReporting);

}

public void navigateToEditPage() throws InterruptedException, IOException {
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.OrderPanel);
	waitForAjax();
	
//click on Action dropdown	
	click(APT_wholeSale_Obj.APT_wholeSale.ActionDropdown_InViewPage, "Action");
	waitForAjax();
	
//click on edit link
	click(APT_wholeSale_Obj.APT_wholeSale.editService_Link, "edit");
	waitForAjax();
	
}

public void editService(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
	
	String ServiceId= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_serviceId");
	String Email= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_email");
	String properMailId= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"properMailId");
	String phone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_phone");
	String Remark= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_remark");
	String PerformanceReport= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_performanceReport");
	String serviceType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"serviceToBeSelected");
	
	Report.LogInfo("Info", "Verifying 'Edit Service' Functionality","Info");
	boolean editServicePage=false;
	
	scrollIntoTop();
	waitForAjax();
	
	editServicePage=isElementPresent(APT_wholeSale_Obj.APT_wholeSale.editSerivcePage_pnaleHeader);
	if(editServicePage) {
		
		
		
	//service identification
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.secivceIdentification_textField, ServiceId,"Service Identification");
	
		
	//Remark	
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.remark_textField, Remark,"Remark");
		
	
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
waitForAjax();

	//Performance Reporting
		editcheckbox_commonMethod("Performance Reporting",APT_wholeSale_Obj.APT_wholeSale.performanceReporting_checkbox,PerformanceReport);
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	//Click on "OK" button
		click(APT_wholeSale_Obj.APT_wholeSale.OKbutton, "OK");	
		
		
	}else {
		Report.LogInfo("Info", " 'Edit Service' page is not displaying","FAIL");
		Reporter.log(" 'Edit Service' page is not displaying");
	}
	
}


public void verifyEnteredValuesForServiceUpdation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException { 
	
	String serviceidentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ServiceIdentification");
	String ImproperEmailID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ImproperEmailID");
	String properMailId= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"properMailId");
	String Phone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Phone");
	String remark= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"remark");
	String PerformanceReporting= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PerformanceReporting");
	String editedServiceId= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_serviceId");
	String editedRemark= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_remark");
	String editedEmail= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_email");
	String editedphone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_phone");
	String editedPerformanceReport= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_performanceReport");
	String serviceType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"serviceToBeSelected");
	
	
	
	Report.LogInfo("Info", "Verifying Updated values","Info");

	scrollDown(APT_wholeSale_Obj.APT_wholeSale.service);
	waitForAjax();
	
//Service Identification
if(editedServiceId.equalsIgnoreCase("null")) {
	
	compareText_InViewPage("Service Identification", serviceidentification);
}else {
	compareText_InViewPage("Service Identification", editedServiceId);
}
	

//Service Type	
	compareText_InViewPage("Service Type",  "Wholesale SIP Trunking");
	
//Email	
	compareText_InViewPage("Email",  properMailId);
	
//Phone Contact	
	compareText_InViewPage("Phone Contact", Phone);
	
//Remark	
if(editedRemark.equalsIgnoreCase("Null")) {
	//compareText("Remark", APT_wholeSale_Obj.APT_wholeSale.viewPage_RemarkField, remark);
}else {
	//compareText("Remark", APT_wholeSale_Obj.APT_wholeSale.viewPage_RemarkField, editedRemark);
}
	
	
//under Management Options panel
//performance reporting	
if(editedPerformanceReport.equalsIgnoreCase("Null")) {
	compareText_InViewPage("Performance Reporting", PerformanceReporting);
}else {
	compareText_InViewPage("Performance Reporting", editedPerformanceReport);
}
	

}


public String fetchOrderNumber() throws InterruptedException,  IOException {
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.userspanel_header);
	
	String orderNumber=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.fetchorderNumber,"Fetch order Number");
	
	return orderNumber;
	
}

public void manageSubnet_viewServicepage() throws InterruptedException, IOException {
	
	Report.LogInfo("Info", "Verifying 'Manage Subnet'","Info");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.OrderPanel);
		waitForAjax();
	
		click(APT_wholeSale_Obj.APT_wholeSale.Editservice_actiondropdown,  "Action");
		waitForAjax();
	   click(APT_wholeSale_Obj.APT_wholeSale.manageSubnet_IPv6, "Manage Subnet IPv6");
	   waitForAjax();
	   
	   boolean DumpPage=false;
	   DumpPage=isElementPresent(APT_wholeSale_Obj.APT_wholeSale.dumpPage_header);
	   if(DumpPage) {
		   Report.LogInfo("Info", "Manage Subnet IPv6 page is displaying","PASS");
		   Reporter.log("manage subnet ipv6 page is displaying");
		  
		  //Header 
		   String headerName=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.dumpheaderName,"Dump header Name");
		   if(headerName.isEmpty()) {
			   Report.LogInfo("Info", "Header name is not displaying","FAIL");
			   Reporter.log("Header name is not displaying");
			 
		   }else {
			   Report.LogInfo("Info", "manage Subnet IPv6 header name is displaying as "+ headerName,"PASS");  
			   Reporter.log("manage subnet ipv6 header name is displaying as "+ headerName);
		   }
		   
		   //body
		   String bodyContent=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.manageSubnet_Body,"ManageSubnet_Body");
		   if(bodyContent.isEmpty()) {
			   Report.LogInfo("Info", "manage subnet value is not displaying","FAIL");
			   Reporter.log("manage subnet value is not displaying");
		   }else {
			   Report.LogInfo("Info", "manage subnet message is displaying as "+bodyContent,"PASS");
			   Reporter.log("manage subnet message is displaying as "+bodyContent);
		   }
		   
		   click(APT_wholeSale_Obj.APT_wholeSale.dump_xButton,"X button");
		   waitForAjax();
		   
		   
	   }else {
		   Report.LogInfo("Info", "manage subnet ipv6 page is not displaying","FAIL");
		   Reporter.log("manage subnet ipv6 page is not displaying");

	   }
}

public void verifyAddMASswitch(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException { 
	
	String MAS_IMSPOPLocation= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"MAS_IMSPOPLocation");

	waitToPageLoad();
	 waitForAjax();
	
	 Report.LogInfo("Info", "Verifying 'Add MAS Switch' Functionality","Info");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
		waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.addMASswitch_link, "Add MAS switch link");  //click on MAS switch link
	
	compareText("Add MAS Switch header", APT_wholeSale_Obj.APT_wholeSale.MAS_AddMASSwitch_header, "Add MAS Switch");  //compare MAS switch Header
	

	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.MASswitch_warningmessage, "IMS POP Location");
	
	addDropdownValues_commonMethod("IMS POP Location",APT_wholeSale_Obj.APT_wholeSale.AddmasSWitch_IMSppswitch_dropdown, MAS_IMSPOPLocation);
	
	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
	 waitForAjax();
	
	
	Reporter.log("------ MAS Switch added successfully ------");
}

public void MASswitch_clickOnViewPage(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String existingdevicename= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"MAS_deviceName");
	
	
	Report.LogInfo("Info", "Verifying 'view MAS Switch'","Info");
	
	
	waitToPageLoad();
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	waitForAjax();
	
	if(isElementPresent(APT_wholeSale_Obj.APT_wholeSale.existingdevicegrid_MAS))
    {
		
		webDriver.findElement(By.xpath("//tr//td//b[contains(.,'"+ existingdevicename +"')]//parent::td//following-sibling::span//a[text()='View']")).click();
	//	webDriver.findElement(By.xpath("//tr//td//b[contains(text(),'')]//parent::td//following-sibling::span//a[text()='View']")).click();

    }
    else
    {
          Report.LogInfo("Info", "No Device added in grid","FAIL");
    }		
}

public void verifyAddedMASswitchInformation_View() throws InterruptedException,IOException {
	
	
	
	
	scrollIntoTop();
	waitForAjax();

	// Verify All Device Information under Device panel for MAS Switch
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_DeviceNameValue,"Device Name");
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_VendorModelValue,"Vendor/Model");
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_ManagementAddressValue,"Management Address");
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_SnmproValue,"Snmpro");
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_CountryValue,"Country");
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_CityValue,"City");
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_SiteValue,"Site");
	getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_PremiseValue,"Premise");
	
}

public void clickOnEditInterfaceLink() throws InterruptedException, IOException {
	
	Report.LogInfo("Info", "Verifying 'Edit Interface' Functionality","Info");
	
	
	
	click(APT_wholeSale_Obj.APT_wholeSale.editLink_InViewPage , "Edit_Link");
}


public String fetchTrunkName() throws InterruptedException,  IOException {
	
	scrollIntoTop();
	waitForAjax();
    String trunkNames = getTextFrom(APT_wholeSale_Obj.APT_wholeSale.fetchTrunkGroupName,"fetch Trunk GroupName");
	
	return trunkNames;
	
	
	
}

public String fetchgatewayValue() throws InterruptedException,  IOException {
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.HomeBreadcrump);
	waitForAjax();
	
	
	String gateway = getTextFrom(APT_wholeSale_Obj.APT_wholeSale.fetchGateway,"fetch Gateway");
	
	return gateway;
}

public void deleteSBC_manualExecutionConfig(String application) throws InterruptedException, IOException {
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	Report.LogInfo("Info", "verifying 'Delete SBC Manual Configuration' Functionality","Info");
	
	click(APT_wholeSale_Obj.APT_wholeSale.SBC_selectCreatedValue, "SBCcreated");
	
	click(APT_wholeSale_Obj.APT_wholeSale.SBCManualConfig_actionDropdown, "Action");  //click acton dropdown
	click(APT_wholeSale_Obj.APT_wholeSale.SBC_deleteLink, "Delete link");   //click on edit link
	
	 boolean DeleteAlertPopup= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
     if(DeleteAlertPopup)
     {
   	 String deletPopUpMessage= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.deleteMessage,"delete Message");
   	Report.LogInfo("Info", "Delete Pop up message displays as: "+ deletPopUpMessage,"PASS");
   	 
        click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
        
        scrollIntoTop();
        waitForAjax();
        
        verifysuccessmessage("Deleted SBC manual config successfully.");
     }
     else
     {
           Reporter.log("Delete alert popup is not displayed");
           Report.LogInfo("Info", " Delete alert popup is not displayed","FAIL");
     }
	
}

public void deletePSX_manualExecutionConfig() throws InterruptedException, IOException {
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	 Report.LogInfo("Info", "verifying 'Delete PSX Manual Configuration' Functionality","Info");
	
	click(APT_wholeSale_Obj.APT_wholeSale.PSX_selectCreatedValue, "PSXcreated");
	
	click(APT_wholeSale_Obj.APT_wholeSale.PSXManualConfig_actionDropdown, "Action");  //click action dropdown
	click(APT_wholeSale_Obj.APT_wholeSale.PSX_deleteLink, "Delete link");   //click on edit link
	
	 boolean DeleteAlertPopup= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
     if(DeleteAlertPopup)
     {
   	 String deletPopUpMessage= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.deleteMessage,"delete Message");
   	Report.LogInfo("Info", "Delete Pop up message displays as: "+ deletPopUpMessage,"PASS");
   	 
        click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
        
        scrollIntoTop();
        waitForAjax();
        
        verifysuccessmessage("Deleted PSX manual config successfully.");
     }
     else
     {
           Reporter.log("Delete alert popup is not displayed");
           Report.LogInfo("Info", " Delete alert popup is not displayed","FAIL");
     }
}

public void editCPEdeviceLink() throws InterruptedException, IOException {
	
	scrollIntoTop();
	
	
	Report.LogInfo("Info", "Verifying 'Edit CPE Device' Functionality","Info");
	
	//click(APT_wholeSale_Obj.APT_wholeSale.viewdevicePage_actionDropdown , "Action");
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.editLink_InViewPage , "Edit_device Link");
	
}


//////////////////////////////////
//////////////////////////////////
//////////////////////////////////

public void deleteTrunk(String trunkGroupName, String siteOrderName) throws Exception {
	
	
	   waitToPageLoad();
	   ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	Report.LogInfo("Info", "Verifying 'Delete Trunk' Funtionality","Info");

	
	//click on delete link
		click(APT_wholeSale_Obj.APT_wholeSale.Turnk_DeleteLink, "Delete");
		
		 boolean DeleteAlertPopup= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
      if(DeleteAlertPopup)
      {
    	 String deletPopUpMessage= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.deleteMessages_textMessage,"delete Messages_textMessage");
    	 Report.LogInfo("Info", "Delete Pop up message displays as: "+ deletPopUpMessage,"PASS");
    	 
         click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
         
         scrollIntoTop();
         waitForAjax();
         
         verifysuccessmessage("Trunk deleted successfully");
      }
      else
      {
            Reporter.log("Delete alert popup is not displayed");
            Report.LogInfo("Info", "Delete alert popup is not displayed","FAIL");
      }
			
}


public void SelectInterfacetoremovefromservice(String interfacename)
		throws IOException, InterruptedException{

	
	
	waitToPageLoad();
	
	 Report.LogInfo("Info", "'Select Interface_Remove Interace from Service","Info");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.MAS_View_ManagementAddressValue);
	
	waitForAjax();
	
	webDriver.findElement(By.xpath("//div[div[text()='"+ interfacename +"']]//span[@class='ag-icon ag-icon-checkbox-unchecked']")).click();
	
	click(APT_wholeSale_Obj.APT_wholeSale.InterfaceInselect_Actiondropdown, "Action");
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.InterfaceInselect_removebuton, "Remove");
	//selectRowforInterfaceInService(interfacename); //not needed

}


public void SelectInterfacetoaddwithservcie(String interfacenumber)
		throws InterruptedException, IOException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	Report.LogInfo("Info",  "'Select interface_Add Interface to Service'","Info");
	
	waitToPageLoad();
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.InterfaceInService_panelHeader);
	
	waitForAjax();
	
//	click(Application, "Interface Column filter", "InteraceColumn_Filter", xml);
//	waitForAjax();
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_searchbox, interfacenumber, "Interface Name/Address"); 
	click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_searchButton, "Search");
	waitForAjax();
	
	webDriver.findElement(By.xpath("(//div[div[text()='"+ interfacenumber +"']]//span[@class='ag-icon ag-icon-checkbox-unchecked'])[3]")).click();

	click(APT_wholeSale_Obj.APT_wholeSale.InterfaceToselect_Actiondropdown, "Action Dropdown");
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.InterfaceToselect_addbuton, "Add");
	//selectrowforInterfaceToselecttable(interfacenumber);

}

public void verifyinterfaceTableColumnNames(String interfaceName) throws InterruptedException,IOException {
	
	//String interfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Interface");


	Report.LogInfo("Info", "Verifying 'Added Interface' value in table","Info");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.routerTool_HeaderPanel);
	
	waitForAjax();
	
	String[] InterfaceFieldNames= {"Interface", "Link/Circuit Id", "Interface Address Range", "Interface Address", "Bearer Type","Bandwidth", "VLAN Id" , "IfInOctets", "IV Management"};
	List<String> ls = new ArrayList<String>();
	
	String expectedvalues="[Interface, Link/Circuit Id, Interface Address Range, Interface Address, Bearer Type, Bandwidth, VLAN Id, IfInOctets, IV Management]";
	Report.LogInfo("Info", "Expected column names are: "+ expectedvalues,"PASS");
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_searchbox, interfaceName, "Search box");
	click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_searchButton, "Search");

	/*
//Fetches list of common label names	
	List<WebElement> interfaceLabelnames = findWebElements(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_labelnames);
	for (WebElement interfaceLabelname : interfaceLabelnames) {

		boolean match = false;
		for (int i = 0; i < InterfaceFieldNames.length; i++) {
			if (interfaceLabelname.getText().equals(InterfaceFieldNames[i])) {
				match = true;
				ls.add(interfaceLabelname.getText());
			}
		}
	}
	*/
	Report.LogInfo("Info", "Actual column names displaying are: "+ ls,"PASS");  //printing list of column names displaying
	Reporter.log("Actual column names displaying are: "+ ls);
	
}


public String MASswitch_getDeviceSerialNumber(String existingdevicename) throws InterruptedException, IOException {
	
	String expectedDeviceSerialNumber=null;
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	
	waitForAjax();
	
	if(isElementPresent(APT_wholeSale_Obj.APT_wholeSale.existingdevicegrid_MAS))
    {
          List<WebElement> addeddevicesList= findWebElements(APT_wholeSale_Obj.APT_wholeSale.MAS_fetchAlldevice_InviewPage);
//          Reporter.log(addeddevicesList);
          int AddedDevicesCount= addeddevicesList.size();
          for(int i=0;i<AddedDevicesCount;i++) {
                String AddedDeviceNameText= addeddevicesList.get(i).getText();
                String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
                Reporter.log("number "+ AddedDevice_SNo);
                if(AddedDeviceNameText.contains(existingdevicename))
                {
                	expectedDeviceSerialNumber=AddedDevice_SNo;
                	 break;
                	
                }
                else
                {
                      Report.LogInfo("Info", "Invalid device name","FAIL");
                }
               
          }
    }
    else
    {
    	Report.LogInfo("Info", "No Device added in grid","FAIL");
    }	
	
	return expectedDeviceSerialNumber; 
	
}

public String PEdevice_getDeviceSerialNumber(String existingdevicename) throws InterruptedException, IOException {
	
	String expectedDeviceSerialNumber=null;
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.MASswitch_panelHeader);
	waitForAjax();
	
	if(isElementPresent(APT_wholeSale_Obj.APT_wholeSale.existingdevicegrid))
    {
          List<WebElement> addeddevicesList= webDriver.findElements(By.xpath(APT_wholeSale_Obj.APT_wholeSale.PE_fetchAlldevice_InviewPage));
//          Reporter.log(addeddevicesList);
          int AddedDevicesCount= addeddevicesList.size();
          for(int i=0;i<AddedDevicesCount;i++) {
                String AddedDeviceNameText= addeddevicesList.get(i).getText();
                String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
//                Reporter.log("number "+ AddedDevice_SNo);
                if(AddedDeviceNameText.contains(existingdevicename))
                {
                	expectedDeviceSerialNumber=AddedDevice_SNo;
                	 break;
                }
                else
                {
                	Report.LogInfo("Info", "Invalid device name","FAIL");
                }
               
          }
    }
    else
    {
    	Report.LogInfo("Info", "No Device added in grid","FAIL");
    }
	
	return expectedDeviceSerialNumber;
}


public void viewInterface_PEdevice(String deviceName,String Interface,String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PE_Link");
	
	String interfaceAddressrange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PE_InterfaceAddressRange");
	
	String vlanID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PE_VLANID");
	
	
	scrollUp();
	waitForAjax();
	
	
	compareText_InViewPage( "Device Name", deviceName);
	
	compareText_InViewPage( "Interface", Interface);
	
	compareText_InViewPage( "Link/Circuit Id", link);
	
	compareText_InViewPage( "Interface Address Range", interfaceAddressrange);
	
	compareText_InViewPage( "VLAN Id", vlanID);
	
}

public void selectInterfaceAndClickonConfigureLInk_PEdevice(String deviceName, String interfaceName) throws InterruptedException, IOException {
	
	Report.LogInfo("Info", "verifying 'Configure Interface_PE device' ","Info");
	
	//String interfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Interface");

	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Configure Interface_MAS Swicth' Functionality");
	
	//WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	waitForAjax();
	
	
	webDriver.findElement(By.xpath("(//tr//td[contains(.,'"+ interfaceName +"')]//following-sibling::td//a[text()='Configure'])[1]")).click();
	
	
	}


public void deleteSBC_manualExecutionConfig() throws InterruptedException, IOException {
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitForAjax();
	
	Report.LogInfo("Info", "verifying 'Delete SBC Manual Configuration' Functionality","PASS");
	
	click(APT_wholeSale_Obj.APT_wholeSale.SBC_selectCreatedValue, "SBCcreated");
	
	click(APT_wholeSale_Obj.APT_wholeSale.SBCManualConfig_actionDropdown, "Action");  //click acton dropdown
	click(APT_wholeSale_Obj.APT_wholeSale.SBC_deleteLink, "Delete link");   //click on edit link
	
	 boolean DeleteAlertPopup= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
     if(DeleteAlertPopup)
     {
   	 String deletPopUpMessage= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.deleteMessage,"delete Message");
   	 Report.LogInfo("Info", "Delete Pop up message displays as: "+ deletPopUpMessage,"PASS");
   	 
        click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
        
        scrollIntoTop();
        waitForAjax();
        
        verifysuccessmessage("Deleted SBC manual config successfully.");
     }
     else
     {
          Reporter.log("Delete alert popup is not displayed");
           Report.LogInfo("Info", " Delete alert popup is not displayed","FAIL");
     }
	
}

public void deleteGSX_manualExecutionConfig() throws InterruptedException,  IOException {
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	 Report.LogInfo("Info", "Verifying 'Delete GSX Manual Configuration' Functionality","Info");
	
	click(APT_wholeSale_Obj.APT_wholeSale.GSX_selectCreatedValue, "PSXcreated");
	
	click(APT_wholeSale_Obj.APT_wholeSale.GSXManualConfig_actionDropdown, "Action");  //click action dropdown
	click(APT_wholeSale_Obj.APT_wholeSale.GSX_deleteLink, "Delete link");   //click on edit link
	
	 boolean DeleteAlertPopup= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
     if(DeleteAlertPopup)
     {
   	 String deletPopUpMessage= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.deleteMessage,"delete Message");
   	Report.LogInfo("Info", "Delete Pop up message displays as: "+ deletPopUpMessage,"PASS");
   	 
        click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
        
        scrollIntoTop();
        waitForAjax();
        
        verifysuccessmessage("Deleted GSX manual config successfully.");
     }
     else
     {
           Reporter.log("Delete alert popup is not displayed");
          Report.LogInfo("Info", " Delete alert popup is not displayed","FAIL");
     }
}

public void verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(String siteOrderName) throws InterruptedException, IOException {
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
		
	Report.LogInfo("Info", "Verifying 'Added Site Order'","Info");
	
		Report.LogInfo("Info", siteOrderName + " 'Site Order' is displaying under 'Trunk' panel","PASS");
		Reporter.log(siteOrderName + " 'Site Order' is displaying under 'Trunk' panel");
		
		
		Report.LogInfo("Info", "Verifying 'Add Trunk' Functionality","Info");
		
	//Click on Add trunk link	
		webDriver.findElement(By.xpath("//div[div[span['"+ siteOrderName +"']]]/following-sibling::div//a[text()='Add Trunk']")).click();
		//click_commonMethod_PassingWebelementDirectly(addTunklinkXpath, "Add Trunk");
		waitForAjax();
		
	
	
}

public void PEdevice_clickOnViewPage(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String PE_deviceName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_deviceName");

	
	
	Report.LogInfo("Info", "Verifying 'view MAS Switch'","Info");
	
	
	waitToPageLoad();
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	waitForAjax();
	
	if(isElementPresent(APT_wholeSale_Obj.APT_wholeSale.existingdevicegrid_MAS))
    {
		
		webDriver.findElement(By.xpath("//tr//td//b[contains(.,'"+ PE_deviceName +"')]//parent::td//following-sibling::span//a[text()='View']")).click();
	//	webDriver.findElement(By.xpath("//tr//td//b[contains(text(),'')]//parent::td//following-sibling::span//a[text()='View']")).click();

    }
    else
    {
          Report.LogInfo("Info", "No Device added in grid","FAIL");
    }		
	
	
	/*
	Report.LogInfo("Info", "Verifying 'View PE Device'","Info");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.MASswitch_panelHeader);
	waitForAjax();
	
	if(isElementPresent(APT_wholeSale_Obj.APT_wholeSale.existingdevicegrid))
    {
          List<WebElement> addeddevicesList= webDriver.findElements(By.xpath(APT_wholeSale_Obj.APT_wholeSale.PE_fetchAlldevice_InviewPage));
          int AddedDevicesCount= addeddevicesList.size();
          for(int i=0;i<AddedDevicesCount;i++) {
                String AddedDeviceNameText= addeddevicesList.get(i).getText();
                String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
                Reporter.log("number "+ AddedDevice_SNo);
                if(AddedDeviceNameText.contains(PE_deviceName))
                {
                	findWebElement((APT_wholeSale_Obj.APT_wholeSale.PE_viewLink_InViewPage).replace("value", AddedDevice_SNo)).click(); 
                     
                      waitForAjax();
                      break;
                }
                else
                {
                      Report.LogInfo("Info", "Invalid device name","FAIL");
                }
               
          }
    }
    else
    {
          Report.LogInfo("Info", "No Device added in grid","FAIL");
    }		
    */
}

public void PEdevice_clickOnselectInterface(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String existingdevicename=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_deviceName");

	WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	waitForAjax();
	
	if(findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader).isDisplayed())
    {
		
		webDriver.findElement(By.xpath("//tr//td//b[contains(.,'"+ existingdevicename +"')]//parent::td//following-sibling::span//a[text()='Select Interfaces']")).click();
		
    }
    else
    {
          ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
    }	
}

public String verifyDevicesUnderPEpanel() throws InterruptedException, IOException {
	
	
	waitforPagetobeenable();
	
	String deviceAvailability = null;
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.MASswitch_panelHeader);
	waitForAjax();

try {	
    List<WebElement> addeddevicesList= webDriver.findElements(By.xpath(APT_wholeSale_Obj.APT_wholeSale.MASswitch_panelHeader));
    int AddedDevicesCount= addeddevicesList.size();
    
    if(AddedDevicesCount>=1) {
    	deviceAvailability = "Yes";
    	
    	Report.LogInfo("Info", "List of device names displaying under 'Provider Equipment' panel are: ","Info");
    	Reporter.log("List of device names displaying under 'Provider Equipment' panel are: ");
    	
    	for(int i=0;i<AddedDevicesCount;i++) {
            String AddedDeviceNameText= addeddevicesList.get(i).getText();
            
            Report.LogInfo("PASS", AddedDeviceNameText,"PASS");
            Reporter.log(AddedDeviceNameText);
    	} 
    }
    else
    {
    	deviceAvailability = "No";
    }
    
}catch(Exception e) {
	e.printStackTrace();
	deviceAvailability = "No";
}
    
	return deviceAvailability;
	
}

public void addTrunkSiteOrder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException { 
	
	String trunkGroupOrder= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"TrunkGroupOrder");
	String trunkgroupOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"TrunkGroupOrderNumber");
	
	boolean trunkPanel=false;
	boolean siteOrderpage=false;
	boolean trunkgrupOrderErrMsg= false;
	
	Report.LogInfo("Info", "Verifying 'Add Trunk Site Order' Functionality","Info");
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	trunkPanel= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.trunkPanel);
	if(trunkPanel) {
		Report.LogInfo("Info", "'Trunk Group/Site Orders' panel is displaying as expected in 'view Service' page","PASS");
		Reporter.log("'Trunk Group/Site Orders' panel is displaying as expected in 'view Service' page");
		
		
	//Perform Add Site order
		click(APT_wholeSale_Obj.APT_wholeSale.addTrunkSiteOrderlink, "Add TrunkGroup/SiteOrder link");
		
		siteOrderpage= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.addtrunkSiteorderPage_panelheader);
		if(siteOrderpage) {
			
			Report.LogInfo("Info", "'Add Trunk Group/Site Order' page is displaying as expected","PASS");
			Reporter.log("'Add Trunk Group/Site Order' page is displaying as expected");
			
		//verify mandatory Warning Message
			click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.trunkGrouporder_warningmsg, "Trunk Group Order");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.trunkGroupOrderName_warningmsg, "Trunk group Order Number");
			
		//Add trunkgroup Order checkbox
			if(trunkGroupOrder.equalsIgnoreCase("yes")) {
				addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.trunkGroupOrder_checkbox, "Trunk Group Order", trunkGroupOrder);
			}else {
				Report.LogInfo("Info", " ' Trunk Group order' checkbox is a mandatory field. No values passed","FAIL");
				Reporter.log(" ' Trunk Group order' checkbox is a mandatory field. No values passed");

			}
			
			
		//Add Trunk Group order Number text field
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.trunkGroupOrderName_textField, trunkgroupOrderNumber, "Trunk Group Order Number");
			
			
		//Click on OK button
			click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
			waitForAjax();
			
			try {
			trunkgrupOrderErrMsg=isElementPresent(APT_wholeSale_Obj.APT_wholeSale.alertMSg_siteorderCreation);
			String actualMsg=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.alertMSg_siteorderCreation,"AlertMSg_site orderCreation");
			if(trunkgrupOrderErrMsg) {
				if(actualMsg.contains("1.trunkgroup number already exists")) {
					
					Report.LogInfo("Info", " Error message we are getting as: "+ actualMsg,"FAIL");
					Reporter.log(" Error message we are getting as: "+ actualMsg);
				}
				else if(actualMsg.equalsIgnoreCase("Trunk Group created successfully")) {
					
					Report.LogInfo("Info", " Success Message displays as 'Trunk Group created successfully'","PASS");
					Reporter.log(" Success Message displays as 'Trunk Group created successfully'");
				}
				
			}
			}catch(Exception e) {
				e.printStackTrace();
				
			}
			
			
		}else {
			Report.LogInfo("Info", "'Add Trunk Group/Site Order' page is not displaying","PASS");
			Reporter.log("'Add Trunk Group/Site Order' page is not displaying");
		}
		
	}else {
		Report.LogInfo("Info", "'Trunk Group/Site Orders' panel is not displaying in 'view Service' page","PASS");
		Reporter.log("'Trunk Group/Site Orders' panel is not displaying in 'view Service' page");
	}
}


public void editSiteOrder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,IOException {
	
	String siteOrderName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"TrunkGroupOrderNumber");
	String trunkGroupOrder= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_TrunkGroupOrder");
	String trunkGrouporderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"edit_TrunkGroupOrderNumber");
	
	boolean siteOrderpage=false;
	boolean trunkgrupOrderErrMsg=false;
	
	
	waitForAjax();
	
	Report.LogInfo("Info", "Verifying 'edit trunk Site Order' Functionality","Info");
	
	WebElement editlink=findWebElement((APT_wholeSale_Obj.APT_wholeSale.editSiteOrderLink).replace("value", siteOrderName));
			
	editlink.click();
	
	siteOrderpage= isElementPresent(APT_wholeSale_Obj.APT_wholeSale.addtrunkSiteorderPage_panelheader);
	if(siteOrderpage) {
		Report.LogInfo("Info", "'Edit Trunk Group/Site Order' page is displaying as expected","PASS");
		Reporter.log("'Edit Trunk Group/Site Order' page is displaying as expected");
		
	//Trunk group Order
		if(trunkGroupOrder.equalsIgnoreCase("no")) {
			Report.LogInfo("Info", "'Trunk Group Order' is a mandatory field. It cannot be unselected","FAIL");
			Reporter.log("'Trunk Group Order' is a mandatory field. It cannot be unselected");
		}else {
		//	addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.trunkGroupOrder_checkbox, "Trunk Group Order", trunkGroupOrder);
					
		}
		
		
	//Trunk Group Order Number
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.trunkGroupOrderName_textField, trunkGrouporderNumber, "Trunk Group Order Number");
		
		waitForAjax();
	//Click on OK button
		click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
		waitForAjax();
		
		try {
		trunkgrupOrderErrMsg=isElementPresent(APT_wholeSale_Obj.APT_wholeSale.alertMSg_siteorderCreation);
		String actualMsg=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.alertMSg_siteorderCreation,"AlertMSg_siteorderCreation");
		if(trunkgrupOrderErrMsg) {
			if(actualMsg.contains("1.trunkgroup number already exists")) {
				
				Report.LogInfo("Info", " Error message we are getting as: "+ actualMsg,"FAIL");
				Reporter.log(" Error message we are getting as: "+ actualMsg);
			}
			else if(actualMsg.contains("Trunk Group successfully updated")) {
				
				Report.LogInfo("Info", " Success Message displays as 'Trunk Group successfully updated.'","PASS");
				Reporter.log(" Success Message displays as 'Trunk Group successfully updated.'");
			}
			
		}
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		
		
	}else {
		Report.LogInfo("Info", "'Edit Trunk Group/Site Order' page is not displaying","PASS");
		Reporter.log("'Edit Trunk Group/Site Order' page is displaying");
	}
}

public void addSBC_manualExecutionConfig(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String manualConfigurationValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"SBCmanualConfigvalue");
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitForAjax();
	
	Report.LogInfo("Info", "verifying 'Add SBC Manual Configuration' Functionality","Info");
	
try {	
	boolean SBCHeder=isElementPresent(APT_wholeSale_Obj.APT_wholeSale.SBCmanualConfig_PanelHeader);
	if(SBCHeder) {
		Report.LogInfo("Info", "'SBC Manully Executed Configuration' panel is displaying","PASS");
		Reporter.log("'SBC Manully Executed Configuration' panel is displaying");
		
		
		click(APT_wholeSale_Obj.APT_wholeSale.SBC_addLink, "Add link");
		
		//manual configuration on SBC page
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.manualConfiguration_textArea, manualConfigurationValue, "SBC Manual Configuration");
		click(APT_wholeSale_Obj.APT_wholeSale.saveButton_manualConfiguration, "Save");
		
		scrollIntoTop();
		waitForAjax();
		
		//verifysuccessmessage("Manual Configuration added Successfully");   //verify success message
		
	}else {
		Report.LogInfo("Info", "'SBC Manully Executed Configuration' panel is not displaying","FAIL");
		Reporter.log("'SBC Manully Executed Configuration' panel is not displaying");
	}
}catch(Exception e) {
	e.printStackTrace();
	Report.LogInfo("Info", "'SBC Manully Executed Configuration' panel is not displaying","FAIL");
	Reporter.log("'SBC Manully Executed Configuration' panel is not displaying");
}
}

public void click_commonMethod_PassingWebelementDirectly(String labelname, String webelement) throws InterruptedException, IOException {
    WebElement element= null;

 

    try {
        Thread.sleep(1000);
        element = findWebElement(webelement);
        if(element==null)
        {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "Step:  '"+labelname+"' not found");
        }
        else {
            element.click();    
            ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Clicked on '"+labelname+"' button");
        }

 

    } catch (Exception e) {
        ExtentTestManager.getTest().log(LogStatus.FAIL,"Step: Clicking on '"+labelname+"' button is unsuccessful");
        e.printStackTrace();
    }
}

public void safeJavaScriptClick(WebElement element) throws Exception {
    try {
        if (element.isEnabled() && element.isDisplayed()) {
            Reporter.log("Clicking on element with using java script click");

 

            ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", element);
        } else {
            Reporter.log("Unable to click on element");
        }
    } catch (StaleElementReferenceException e) {
        Reporter.log("Element is not attached to the page document "+ e.getStackTrace());
    } catch (NoSuchElementException e) {
        Reporter.log("Element was not found in DOM "+ e.getStackTrace());
    } catch (Exception e) {
        Reporter.log("Unable to click on element "+ e.getStackTrace());
    }
}
	
public void verifysuccessmessage(String expected) throws IOException, InterruptedException {
	waitForAjax();
	scrollUp();
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.AlertForServiceCreationSuccessMessage, "Success message"+expected);

}

public void addPSX_manualExecutionConfig(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String PSXmanualConfiguratio= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PSXmanualConfigValue");
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Add Verifying 'PSX Manual Configuration' Functionality");
	
	boolean PSXHeader=findWebElement(APT_wholeSale_Obj.APT_wholeSale.PSXmanualConfig_PanelHeader).isDisplayed();
	if(PSXHeader) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'PSX Manully Executed Configuration' panel is displaying");
		Reporter.log("'PSX Manully Executed Configuration' panel is displaying");
		
		
		waitForAjax();
		click(APT_wholeSale_Obj.APT_wholeSale.PSX_addLink,  "Add link");
		
		//TODO compare Header name
		
	//Text Area	
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.manualConfiguration_textArea, PSXmanualConfiguratio, "Manul Configuration");
		
		//scrolltoend();
				waitForAjax();
		
		click(APT_wholeSale_Obj.APT_wholeSale.saveButton_manualConfiguration, "Save");   //click on Save buttton
		waitForAjax();
		
		//verifysuccessmessage("Manual Configuration added Successfully");  //verify success Message
		
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'PSX Manully Executed Configuration' panel is not displaying");
		Report.LogInfo("INFO","'PSX Manully Executed Configuration' panel is not displaying","FAIL");
	}
}


public void verifyPSXfileAdded() throws InterruptedException, IOException {
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying 'Added value under PSX Manual Configuration' Functionality");
	
	String PSXfilenameCreated=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.PSX_selectCreatedValue,"PSX_selectCreatedValue");
	
	
	if(PSXfilenameCreated.isEmpty()) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "PSX Manually Executed configuration file name is not displaying");
		Reporter.log("PSX Manually Executed configuration file name is not displaying");
	}else {
		
		int i=0; int k=0; int j=0;
		
	//Fetching Column Names	
		List<WebElement> columns=findWebElements(APT_wholeSale_Obj.APT_wholeSale.PSX_columnNames);
		int listOfColumns=columns.size();
		
		String columnName[]=new String[listOfColumns];
		for (WebElement column : columns) {
			
			columnName[k]=column.getText();
			k++;
		}
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "column names display as: "+ columnName[1] + "  " + columnName[2]);  //printing column Names
		
		
	//Fetching value under File Column	
		List<WebElement> files=findWebElements(APT_wholeSale_Obj.APT_wholeSale.PSX_fileName);
		int listOfFiles=files.size();
		
		String[] fileNameValues=new String[listOfFiles];
		
		for(WebElement filenametransfer : files) {
			
			fileNameValues[i] = filenametransfer.getText();
			i++;
		}
		
	
	//Fetching value under Date Column
		List<WebElement> dates=findWebElements(APT_wholeSale_Obj.APT_wholeSale.PSX_dates);
		int listOfDates=dates.size();
		
		String[] dateValues=new String[listOfDates];
		
		for(WebElement dateList : dates) {
			
			dateValues[j] = dateList.getText();
			j++;
		}
	
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Values under 'PSX Manually Executed Configuration' value displays as: ");
	
	for(int y=0; y<fileNameValues.length; y++) {
	ExtentTestManager.getTest().log(LogStatus.PASS, fileNameValues[y] +"      "+ dateValues[y]);
	}
		
	}
}
	
	
public void editPSX_manualExecutionConfig(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String editPSXmanualConfigvalur = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editPSXmanualConfigValue");

	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying 'Edit PSX Manual Configuration' Functionality");
	
	click( APT_wholeSale_Obj.APT_wholeSale.PSX_selectCreatedValue, "PSXcreated");
	
	click(APT_wholeSale_Obj.APT_wholeSale.PSXManualConfig_actionDropdown, "Action");  //click acton dropdown
	click(APT_wholeSale_Obj.APT_wholeSale.PSX_editLink, "Edit link");   //click on edit link
	
//	Log.info("date displaying as : "+getCurrentDate());
//	Log.info("username displays as: "+Getkeyvalue("APT_login_1_Username"));
	
	String mainWindow=null;
	
	waitForAjax();
	
	mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
    Iterator<String> itr = allwindows.iterator();
    while(itr.hasNext())
    {
          String childWindow = itr.next();
          if(!mainWindow.equals(childWindow)){
        	  webDriver.switchTo().window(childWindow);
               Reporter.log(webDriver.switchTo().window(childWindow).getTitle());
                  
//                driver.manage().window().maximize();
                waitForAjax();
                
//               Write here  whatever you want to do and perform
                Report.LogInfo("INFO","came inside child window","PASS");
                
                sendKeys(APT_wholeSale_Obj.APT_wholeSale.GSX_editPage_teaxtArea, editPSXmanualConfigvalur, "PSX manual Configuration");
                
               // scrolltoend();
                waitForAjax();
                
                click(APT_wholeSale_Obj.APT_wholeSale.GSX_editPage_SaveButton, "Save");
                
          }
    }
    
    webDriver.switchTo().window(mainWindow);
    waitForAjax();
    verifysuccessmessage("Manual Configuration updated Successfully");

    
}
	
public void addGSX_manualExecutionConfig(String testDataFile,String dataSheet,String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String GSXmanualConfiguratio = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "GSXmanualConfigValue");

	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	 waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Add Verifying 'GSX Manual Configuration' Functionality");
	
	boolean PSXHeader=findWebElement(APT_wholeSale_Obj.APT_wholeSale.GSXmanualConfig_PanelHeader).isDisplayed();
	if(PSXHeader) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'GSX Manully Executed Configuration' panel is displaying");
		 Reporter.log("'GSX Manully Executed Configuration' panel is displaying");
		
		
		 click(APT_wholeSale_Obj.APT_wholeSale.GSXManualConfig_actionDropdown, "Action");
		 click(APT_wholeSale_Obj.APT_wholeSale.GSX_addLink, "Add link");
		
		//TODO compare Header name
		
	//Text Area	
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.manualConfiguration_textArea, GSXmanualConfiguratio, "Manul Configuration");
		
		//scrolltoend();
		 waitForAjax();
		
		click(APT_wholeSale_Obj.APT_wholeSale.saveButton_manualConfiguration,  "Save");   //click on Save buttton
		 waitForAjax();
		
		//verifysuccessmessage( "Manual Configuration added Successfully");  //verify success Message
		
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'GSX Manully Executed Configuration' panel is not displaying");
		 Reporter.log("'GSX Manully Executed Configuration' panel is not displaying");
	}
}
	
public void verifyGSXfileAdded() throws InterruptedException, IOException {
	
	//scrolltoend();
	Thread.sleep(1000);
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying 'Added value under GSX Manual Configuration' Functionality");
	
	String GSXfilenameCreated=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.GSX_selectCreatedValue,"GSX_select Created Value");
	
	
	if(GSXfilenameCreated.isEmpty()) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "GSX Manually Executed configuration file name is not displaying");
		Reporter.log("GSX Manually Executed configuration file name is not displaying");
	}else {
		
		int i=0; int k=0; int j=0;
		
	//Fetching Column Names	
		List<WebElement> columns=findWebElements(APT_wholeSale_Obj.APT_wholeSale.GSX_columnNames);
		int listOfColumns=columns.size();
		
		String columnName[]=new String[listOfColumns];
		for (WebElement column : columns) {
			
			columnName[k]=column.getText();
			k++;
		}
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "column names display as: ");  //printing column Names
		ExtentTestManager.getTest().log(LogStatus.PASS,  columnName[1] + "     	     " + columnName[2]);
		
	//Fetching value under File Column	
		List<WebElement> files=findWebElements(APT_wholeSale_Obj.APT_wholeSale.GSX_fileName);
		int listOfFiles=files.size();
		
		String[] fileNameValues=new String[listOfFiles];
		
		for(WebElement filenametransfer : files) {
			
			fileNameValues[i] = filenametransfer.getText();
			i++;
		}
		
	
	//Fetching value under Date Column
		List<WebElement> dates=findWebElements(APT_wholeSale_Obj.APT_wholeSale.GSX_dates);
		int listOfDates=dates.size();
		
		String[] dateValues=new String[listOfDates];
		
		for(WebElement dateList : dates) {
			
			dateValues[j] = dateList.getText();
			j++;
		}
	
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Values under 'GSX Manually Executed Configuration' value displays as: ");
	
	for(int y=0; y<fileNameValues.length; y++) {
	ExtentTestManager.getTest().log(LogStatus.PASS, fileNameValues[y] +"      "+ dateValues[y]);
	}
	
	}
}
public void editGSX_manualExecutionConfig(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String editGSXmanualConfigvalur = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editGSXmanualConfigValue");

	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying 'Edit GSX Manual Configuration' Functionality");
	
	click(APT_wholeSale_Obj.APT_wholeSale.GSX_selectCreatedValue, "GSXcreated");
	
	click(APT_wholeSale_Obj.APT_wholeSale.GSXManualConfig_actionDropdown, "Action");  //click acton dropdown
	click(APT_wholeSale_Obj.APT_wholeSale.GSX_editLink, "Edit link");   //click on edit link
	
//	Log.info("date displaying as : "+getCurrentDate());
//	Log.info("username displays as: "+Getkeyvalue("APT_login_1_Username"));
	
	 waitForAjax();
	
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
    Iterator<String> itr = allwindows.iterator();
    while(itr.hasNext())
    {
          String childWindow = itr.next();
          if(!mainWindow.equals(childWindow)){
        	  webDriver.switchTo().window(childWindow);
                Reporter.log(webDriver.switchTo().window(childWindow).getTitle());
                  
//                driver.manage().window().maximize();
               waitForAjax();
                
//               Write here  whatever you want to do and perform
                Reporter.log("came inside child window");
                
                sendKeys(APT_wholeSale_Obj.APT_wholeSale.GSX_editPage_teaxtArea, editGSXmanualConfigvalur, "GSX manual Configuration");
               
               // scrolltoend();
                waitForAjax();
                
                click(APT_wholeSale_Obj.APT_wholeSale.GSX_editPage_SaveButton, "Save");
                
          }
    }
    
    webDriver.switchTo().window(mainWindow);
   waitForAjax();
    verifysuccessmessage( "Manual Configuration updated Successfully");

    
}

public void verifyManageService(String testDataFile,String dataSheet,String scriptNo,String dataSetNo,String serviceIdentification,String orderNumber) throws InterruptedException, IOException {

	String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "serviceToBeSelected");
	String servicestatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatus");
	String syncstatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "syncstatus");
	String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatusChangeRequired");
	String changeStatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeStatus");

	scrollDown(APT_wholeSale_Obj.APT_wholeSale.orderpanelheader);
	waitForAjax();
	click(APT_wholeSale_Obj.APT_wholeSale.serviceactiondropdown, "Action dropdown");
	click(APT_wholeSale_Obj.APT_wholeSale.manageLink, "Manage");
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.manageservice_header,"Manage service header");
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.order_syncPanel,"Order");
	compareText("Service Identification", APT_wholeSale_Obj.APT_wholeSale.status_servicename, serviceIdentification );
	compareText("Service type", APT_wholeSale_Obj.APT_wholeSale.status_servicetype, servicetype);
	String ServiceDetails_value = getTextFrom(APT_wholeSale_Obj.APT_wholeSale.status_servicedetails,"status_servicedetails");
	if(ServiceDetails_value.isEmpty())
	{
		Reporter.log("Service Details column value is empty as expected");
		System.out.println("Service Details column value is empty as expected");
	}
	else
	{
		Reporter.log("Service Details column value should be empty");
		System.out.println("Service Details column value should be empty");
	}
	compareText("Service Status", APT_wholeSale_Obj.APT_wholeSale.status_currentstatus, servicestatus);

	String LastModificationTime_value = getTextFrom(APT_wholeSale_Obj.APT_wholeSale.status_modificationtime,"status_modificationtime");
	if(LastModificationTime_value.contains("BST"))
	{
		Reporter.log("Service status is displayed as : " + LastModificationTime_value);
		System.out.println("Service status is :"+ LastModificationTime_value);
	}
	else
	{
		Reporter.log("Incorrect modification time format");
		System.out.println("Incorrect modification time format");
	}
	click( APT_wholeSale_Obj.APT_wholeSale.statuslink,  "Status");

	if(servicestatuschangerequired.equalsIgnoreCase("Yes"))
	{
		WebElement ServiceStatusPage= findWebElement(APT_wholeSale_Obj.APT_wholeSale.Servicestatus_popup);
		if(ServiceStatusPage.isDisplayed())
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Service Status' popup displays");
			Reporter.log("'Service Status' popup displays");
			
			compareText( "Service Identification", APT_wholeSale_Obj.APT_wholeSale.serviceStatusPopup_sericeIdentificationValue, serviceIdentification );
			compareText("Service type", APT_wholeSale_Obj.APT_wholeSale.serviceStatusPopup_serviceTypeValue, servicetype);
			
			addDropdownValues_commonMethod("Change Status", APT_wholeSale_Obj.APT_wholeSale.serviceStatusPopup_changeStatusDropdown, changeStatus);
			click(APT_wholeSale_Obj.APT_wholeSale.okbutton, "OK");
			
			verifysuccessmessage("Service Status Changed successfully..");
			
			
			waitforPagetobeenable();
			waitForAjax();
			
			WebElement ServiceStatusHistory= findWebElement(APT_wholeSale_Obj.APT_wholeSale.servicestatushistory);
			try
			{
				if(ServiceStatusHistory.isDisplayed())
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, " Service status change request logged");
					Reporter.log("Service status change request logged");
					
					compareText("status column value", APT_wholeSale_Obj.APT_wholeSale.serviceStatusPopup_statusTable_statusColumn, changeStatus);
					
					click(APT_wholeSale_Obj.APT_wholeSale.servicestatus_popupclose, "Close");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Service status change request is not logged");
					Reporter.log("Service status change request is not logged");
					
					click(APT_wholeSale_Obj.APT_wholeSale.servicestatus_popupclose, "Close");
				}
			}
			catch(StaleElementReferenceException e)
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No service history displays");
				Reporter.log("No service history displays");
				
				click(APT_wholeSale_Obj.APT_wholeSale.servicestatus_popupclose,"Close");
			}
			
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Status link is not working");
			Reporter.log("No service history to display");
		}
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, " Service status change not reqired");
		Reporter.log("Service status change not reqired");
		click(APT_wholeSale_Obj.APT_wholeSale.servicestatus_popupclose, "Close");
	}

//synchronize panel in manage service page
	compareText( "Order", APT_wholeSale_Obj.APT_wholeSale.order_syncPanel, orderNumber);
	compareText( "Service Identification", APT_wholeSale_Obj.APT_wholeSale.sync_servicename, serviceIdentification );
	compareText( "Service type", APT_wholeSale_Obj.APT_wholeSale.sync_servicetype, servicetype);

	String ServiceDetails_value1 = getTextFrom(APT_wholeSale_Obj.APT_wholeSale.sync_servicedetails,"sync_servicedetails");
	if(ServiceDetails_value1.isEmpty())
	{
		Reporter.log("Service Details column value is empty as expected");
		System.out.println("Service Details column value is empty as expected");
	}
	else
	{
		Reporter.log("Service Details column value should be empty");
		System.out.println("Service Details column value should be empty");
	}

	//scrolltoend();
	compareText( "Sync Status", APT_wholeSale_Obj.APT_wholeSale.sync_status, syncstatus);
	click(APT_wholeSale_Obj.APT_wholeSale.synchronizelink,"Synchronize");
	verifysuccessmessage( "Sync started successfully. Please check the sync status of this service.");
	
}

public void editMASdevice(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) 
				throws InterruptedException, IOException {
	
	String editDeviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editName");
	String editVendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editVendor");
	String editmanageAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_manageAddress");
	String editSnmpro = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Snmpro");
	String editCountry = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editCountry");
	String editExistingCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_ExistingCitySelection");
	String editNewCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_newCitySelection");
	String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editExistingCity");
	String editNewCityName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editNewCityname");
	String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editNewCityCode");
	String editExistingSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_ExistingSiteSelection");
	String editNewSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_newSiteSelection");
	String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editExistingSite");
	String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editNewSitename");
	String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editNewSiteCode");
	String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_ExistingPremiseSelection");
	String editNewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_newPremiseSelection");
	String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editExistingPremise");
	String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editNewPremiseName");
	String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editNewPremiseCode");
	
	scrollUp();
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Edit MAS Switch' Functionality");
	
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_View_Action_EditLink, "Edit link");

	click(APT_wholeSale_Obj.APT_wholeSale.MAS_View_Action_EditLink, "Edit link");
	
	
	
	waitforPagetobeenable();
	waitForAjax();
	/*
//Device name	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_deviceName, editDeviceName, "Device name");
	
//vendor/model	
	addDropdownValues_commonMethod_ForSpantag("Vendor/Model",APT_wholeSale_Obj.APT_wholeSale.MAS_vendorModel, editVendorModel);
	
//Management Address	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_managementAddress, editmanageAddress, "Management Address");
	
//Snmrpo	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_snmpro, editSnmpro, "Snmpro");

//	scrolltoend();
	waitForAjax();
	
	//Country
			if(!editCountry.equalsIgnoreCase("Null")) {
				
//				addDropdownValues_commonMethod(application, "Country", "countryinput", editCountry, xml);
				addDropdownValues_commonMethod_ForSpantag("Country", APT_wholeSale_Obj.APT_wholeSale.countryinput, editCountry);
				
				//New City		
				if(editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {
					click( APT_wholeSale_Obj.APT_wholeSale.addcityswitch,"addcityswitch");
				   //City name
					sendKeys(APT_wholeSale_Obj.APT_wholeSale.citynameinputfield, editNewCityName, "City Name");
				   //City Code	
					sendKeys(APT_wholeSale_Obj.APT_wholeSale.citycodeinputfield, editNewCityCode, "City Code");
				   //Site name
					sendKeys( APT_wholeSale_Obj.APT_wholeSale.sitenameinputfield_addCityToggleSelected, editNewSiteName, "Site Name");
				   //Site Code
					sendKeys( APT_wholeSale_Obj.APT_wholeSale.sitecodeinputfield_addCityToggleSelected, editNewSiteCode, "Site Code");
				   //Premise name	
					sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisenameinputfield_addCityToggleSelected, editNewPremiseName, "Premise Name");
				   //Premise Code	
					sendKeys( APT_wholeSale_Obj.APT_wholeSale.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode, "Premise Code");
						
				}	
			
			//Existing City	
				else if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
					
//				   addDropdownValues_commonMethod(application, "City", "citydropdowninput", editExistingCityValue, xml);
				   selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.citydropdowninput,"City", editExistingCityValue);
					
					
				 //Existing Site
					  if(editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
//						  addDropdownValues_commonMethod(application, "Site", "sitedropdowninput", editExistingSiteValue, xml);
						  selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale. sitedropdowninput,"Site", editExistingSiteValue);
						  
					 //Existing Premise
						  if(editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
//							  addDropdownValues_commonMethod(application, "Premise", "premisedropdowninput", editExistingPremiseValue, xml);
							  selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.premisedropdowninput,"Premise", editExistingPremiseValue);
				          	 }
						  
						//New Premise  
						  else if(editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {
							  
							  click( APT_wholeSale_Obj.APT_wholeSale.addpremiseswitch,"addpremiseswitch");
							  //Premise name	
							  sendKeys( APT_wholeSale_Obj.APT_wholeSale.premisenameinputfield_addPremiseToggleSelected, editNewPremiseName, "Premise Name");
							   //Premise Code	
								sendKeys( APT_wholeSale_Obj.APT_wholeSale.premisecodeinputfield_addPremiseToggleSelected, editNewPremiseCode, "Premise Code");
						  } 
			         	}
		  		
				  else if(editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {
					  	
					  click(APT_wholeSale_Obj.APT_wholeSale.addsiteswitch,"addsiteswitch");
					  //Site name
						sendKeys(APT_wholeSale_Obj.APT_wholeSale.sitenameinputfield_addSiteToggleSelected, editNewSiteName, "Site Name");
					   //Site Code
						sendKeys( APT_wholeSale_Obj.APT_wholeSale.sitecodeinputfield_addSiteToggleSelected, editNewSiteCode, "Site Code");
					   //Premise name	
						sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisenameinputfield_addSiteToggleSelected, editNewPremiseName, "Premise Name");
					   //Premise Code	
						sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisecodeinputfield_addSiteToggleSelected, editNewPremiseCode, "Premise Code");
					  }
				}
				
			}
			else if(editCountry.equalsIgnoreCase("Null")) {
				
				ExtentTestManager.getTest().log(LogStatus.PASS, " No changes made for 'Country' dropdown");
			
			//City	
				editCity( editExistingCity, editNewCity, APT_wholeSale_Obj.APT_wholeSale.citydropdowninput, "selectcityswitch", "addcityswitch",
						editExistingCityValue, editNewCityName, editNewCityCode, "City");
				
				
			//Site	
				editSite(editExistingSite, editNewSite, APT_wholeSale_Obj.APT_wholeSale.sitedropdowninput, "selectsiteswitch",
						"addsiteswitch", editExistingSiteValue , editNewSiteName, editNewSiteCode, "Site");
				
			//Premise
				editPremise(editExistingPremise, editNewPremise, APT_wholeSale_Obj.APT_wholeSale.premisedropdowninput, "selectpremiseswitch",
						"addpremiseswitch", editExistingPremiseValue, editNewPremiseName, editNewPremiseCode, "Premise");
				
			}

*/
	waitForAjax();
	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
	
}


public void selectInterfaceAndClickonConfigureLInk_MASswitch(String deviceName,String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String interfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Interface");

	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Configure Interface_MAS Swicth' Functionality");
	
	//WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	waitForAjax();
	
	
	webDriver.findElement(By.xpath("(//tr//td[contains(.,'"+ interfaceName +"')]//following-sibling::td//a[text()='Configure'])[1]")).click();
	
	/*
		webDriver.findElement(By.xpath("//div[div[b[text()='"+ deviceName+"']]]//following-sibling::div//div[@role='gridcell'][text()='"+ interfaceName +"']")).click();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on "+ interfaceName + "Interface");
		waitForAjax();
		
		//click((APT_wholeSale_Obj.APT_wholeSale.addeddevice_interface_actiondropdown).replace("value", deviceName));
		click(APT_wholeSale_Obj.APT_wholeSale.configureLink, "Configure");
   */
	
	}

public void MSAdevice_clickOnselectInterface(String testDataFile,String dataSheet,String scriptNo,String dataSetNo ) throws InterruptedException, IOException {
	
	String existingdevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_deviceName");

	WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	waitForAjax();
	
	if(findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader).isDisplayed())
    {
		
		webDriver.findElement(By.xpath("//tr//td//b[contains(.,'"+ existingdevicename +"')]//parent::td//following-sibling::span//a[text()='Select Interfaces']")).click();
		
    }
    else
    {
          ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
    }		
}

public void selectInterface_AndDelete_MASswitch(String testDataFile,String dataSheet,String scriptNo,String dataSetNo, String interfaceName) throws InterruptedException, IOException {
	
	String deviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_deviceName");

	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Delete Interface' Functionality");
		
		WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
		scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
		waitForAjax();
		
		
//		VerifyText(text);
		if(findWebElement(APT_wholeSale_Obj.APT_wholeSale.MASswitch_showInterfaceLink).isDisplayed())
		{
		click( APT_wholeSale_Obj.APT_wholeSale.MASswitch_showInterfaceLink, "Show Interfaces");
		
		waitForAjax();
		WebElement SelectInterface= webDriver.findElement(By.xpath("//div[div[b[text()='"+ deviceName+"']]]//following-sibling::div//div[@role='gridcell'][text()='"+ interfaceName +"']"));
		if(SelectInterface.isDisplayed())
		{
		webDriver.findElement(By.xpath("//div[div[b[text()='"+ deviceName+"']]]//following-sibling::div//div[@role='gridcell'][text()='"+ interfaceName +"']")).click();

			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on "+ interfaceName + "Interface");
			waitForAjax();
			
			click((APT_wholeSale_Obj.APT_wholeSale.addeddevice_interface_actiondropdown).replace("value", deviceName));
			
		click(APT_wholeSale_Obj.APT_wholeSale.MAS_View_Action_DeleteLink, "delete");
		waitForAjax();
             WebElement DeleteAlertPopup= findWebElement(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
             if(DeleteAlertPopup.isDisplayed())
             {
                   click( APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
             }
             else
             {
                  Reporter.log("Delete alert popup is not displayed");
                   ExtentTestManager.getTest().log(LogStatus.FAIL, " Delete alert popup is not displayed");
             }

             verifysuccessmessage( "Interface deleted successfully");
             
             
		  }else {
			  ExtentTestManager.getTest().log(LogStatus.FAIL, interfaceName + " is not displaying under 'MAS Switch' for the device "+ deviceName);
		  }
		}else {
//			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Show Interface' link is not available under 'MAS switch'");
			
			Thread.sleep(1000);
			WebElement SelectInterface= webDriver.findElement(By.xpath("//div[div[b[text()='"+ deviceName+"']]]//following-sibling::div//div[@role='gridcell'][text()='"+ interfaceName +"']"));
			if(SelectInterface.isDisplayed())
			{
	webDriver.findElement(By.xpath("//div[div[b[text()='"+ deviceName+"']]]//following-sibling::div//div[@role='gridcell'][text()='"+ interfaceName +"']")).click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on "+ interfaceName + "Interface");
				waitForAjax();
				
				click((APT_wholeSale_Obj.APT_wholeSale.addeddevice_interface_actiondropdown).replace("value", deviceName));

				 click(APT_wholeSale_Obj.APT_wholeSale.MAS_View_Action_DeleteLink, "delete");
				 waitForAjax();
	             WebElement DeleteAlertPopup= findWebElement( APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
	             if(DeleteAlertPopup.isDisplayed())
	             {
	            	 click( APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
	             }
	             else
	             {
	                   Reporter.log("Delete alert popup is not displayed");
	                   ExtentTestManager.getTest().log(LogStatus.FAIL, " Delete alert popup is not displayed");
	             }

	             verifysuccessmessage("Interface deleted successfully"); 

			  }else {
				  ExtentTestManager.getTest().log(LogStatus.FAIL, interfaceName + " is not displaying under 'MAS Switch' for the device "+ deviceName);
			  }
		}
		
		}

public void editPremise(String editExistingPremise, String editNewPremise, String dropdown_xpath,
		String selectPremiseToggleButton, String addPremisetoggleButton, String dropdownValue,
		String editNewPremiseName, String editNewPremiseCode, String labelname)
		throws InterruptedException, IOException {

	if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
		existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
	} else if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("null")) {
		existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
	} else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("Yes")) {
		newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
	} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("Yes")) {
		newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
	} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("null")) {
		Report.LogInfo("EditPremise", "No changes made under 'Premise' field", "PASS");

	}

}

public void existingPremise(String dropdown_xpath, String dropdownValue, String selectPremiseToggleButton,
		String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {
		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);
	} else {
		click(selectPremiseToggleButton,"selectPremiseToggleButton");
		waitForAjax();
		// click_commonMethod(application, "Select Premise toggle button",
		// selectPremiseToggleButton, xml);
		// Thread.sleep(1000);
		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);
	}
}

public void newPremise(String dropdown_xpath, String addPremisetoggleButton, String editNewPremiseName,
		String editNewPremiseCode) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {
		click(addPremisetoggleButton,"addPremisetoggleButton");
		waitForAjax();
		// click_commonMethod(application, "Select Premise toggle button",
		// addPremisetoggleButton, xml);
		// Thread.sleep(1000);
		// Premise name
		edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
				editNewPremiseName);

		// Premise Code
		edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
				editNewPremiseCode);
	} else {
		// Premise name
		edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
				editNewPremiseName);
		// Premise Code
		edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
				editNewPremiseCode);
	}
}

public void existingSite(String dropdown_xpath, String dropdownValue, String selectSiteToggleButton,
		String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {

		selectByValue(dropdown_xpath, dropdownValue, labelname);
	} else {
		click(selectSiteToggleButton,"selectSiteToggleButton");
		waitForAjax();
		// Thread.sleep(1000);
		selectByValue(dropdown_xpath, dropdownValue, labelname);

	}
}


public void newSite(String dropdown_xpath, String addSitetoggleButton, String editNewSiteName,
		String editNewSiteCode, String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {
		click(addSitetoggleButton,"addSitetoggleButton");
		waitForAjax();
		// click_commonMethod(application, "Select City toggle button",
		// addSitetoggleButton, xml);
		// Thread.sleep(1000);
		// Site name
		edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
		// Site Code
		edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
	} else {
		// Site name
		edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
		// Site Code
		edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
	}
}

public void existingCity(String dropdown_xpath, String dropdownValue, String selectCityToggleButton,
		String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {

		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);

	} else {

		click(selectCityToggleButton,"selectCityToggleButton");
		waitForAjax();
		// click_commonMethod(application, "Select City toggle button",
		// selectCityToggleButton, xml);
		// Thread.sleep(1000);

		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);

	}
}

public void newCity(String dropdown_xpath, String addCitytoggleButton, String editNewCityName,
		String editNewCityCode) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {

		click(addCitytoggleButton,"addCitytoggleButton");
		waitForAjax();
		// click_commonMethod(application, "Select City toggle button",
		// addCitytoggleButton, xml);
		// Thread.sleep(1000);
		// City name
		edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
		// City Code
		edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
	} else {
		// City name
		edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
		// City Code
		edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
	}
}

public void editSite(String editExistingCity, String editNewCity, String dropdown_xpath,
		String selectSiteToggleButton, String addSitetoggleButton, String dropdownValue, String editNewSiteName,
		String editNewSiteCode, String labelname) throws InterruptedException, IOException {

	if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
		existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
		existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
		newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
	} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
		newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
	} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
		Report.LogInfo("EditSite", "No changes made under 'Site' field", "PASS");
	}

}
public void editCity(String editExistingCity, String editNewCity, String dropdown_xpath,
		String selectCityToggleButton, String addCityToggleButton, String dropdownValue, String editNewCityName,
		String editNewCityCode, String labelname) throws InterruptedException, IOException {
	if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
		existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
		existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
		newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
	} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
		newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
	}

	else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
		Report.LogInfo("EditCity", "No chnges made under 'City' field", "PASS");
	}

}	

public void deleteService() throws InterruptedException, IOException {
	
	WebElement orderPanel= findWebElement(APT_wholeSale_Obj.APT_wholeSale.OrderPanel);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.OrderPanel);
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.ActionDropdown_InViewPage, "Action");
	Reporter.log("Action dropdown is working");
	waitForAjax();
	
		//click on delete link
			click(APT_wholeSale_Obj.APT_wholeSale.deleteServiceLink, "Delete");
			
			 WebElement DeleteAlertPopup= findWebElement(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
	         if(DeleteAlertPopup.isDisplayed())
	         {
	       	 String deletPopUpMessage= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.deleteMessages_textMessage,"deleteMessages_textMessage");
	       	 ExtentTestManager.getTest().log(LogStatus.PASS, "Delete Pop up message displays as: "+ deletPopUpMessage);
	       	 
	            click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
	            
	            scrollUp();
	           waitForAjax();
	            verifysuccessmessage("Service successfully deleted..");
	           
	         }
	         else
	         {
	              Reporter.log("Delete alert popup is not displayed");
	               ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete alert popup is not displayed");
	         }
}

public void deleteSiteOrder(String siteOrder) throws InterruptedException, IOException {

	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	webDriver.findElement(By.xpath("//tr//*[contains(.,'"+siteOrder+"')]//following-sibling::td//a[text()='Delete']")).click();

	
	WebElement DeleteAlertPopup= findWebElement(Lanlink_DirectFiberObj.DirectFiber.delete_alertpopup);
    if(DeleteAlertPopup.isDisplayed())
    {
  	 String deletPopUpMessage= getTextFrom(Lanlink_DirectFiberObj.DirectFiber.deleteMessages_textMessage,"deleteMessages_textMessage");
  	 ExtentTestManager.getTest().log(LogStatus.PASS, "Delete Pop up message displays as: "+ deletPopUpMessage);
  	 
       click(Lanlink_DirectFiberObj.DirectFiber.deletebutton, "Delete");
       
       scrollUp();
      waitForAjax();
       
       verifysuccessmessage("Site Order successfully deleted");
    }
    else
    {
         Reporter.log("Delete alert popup is not displayed");
          ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete alert popup is not displayed");
    }
}

public void clickOnBreadCrump(String breadCrumpLink) throws IOException, InterruptedException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitForAjax();
	String breadcrumb = null;

	try {
		breadcrumb = (APT_wholeSale_Obj.APT_wholeSale.breadcrump).replace("value", breadCrumpLink);

		if (isElementPresent(breadcrumb)) {
			click(breadcrumb,"breadcrumb");
			waitForAjax();
			waitToPageLoad();
		} else {
			Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
		}
	} catch (Exception e) {
		e.printStackTrace();
		Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
	}
}

public void CPEdevice_clickOnDeleteLink(String existingdevicename) throws InterruptedException, IOException {
	
	 ((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitForAjax();
	

waitforPagetobeenable();

//scrolltoend();
waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Delete CPE Device' Functionality");
	
	
		webDriver.findElement(By.xpath("//div[contains(.,'"+existingdevicename+"')]//span//a[text()='Delete']")).click();

                      
                      WebElement DeleteAlertPopup= findWebElement(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
                      if(DeleteAlertPopup.isDisplayed())
                      {
                            click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
                            
                      }
                      else
                      {
                            Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
                            ExtentTestManager.getTest().log(LogStatus.FAIL, " Delete alert popup is not displayed");
                      }
                
                
         
    
    		
}

public void routerPanel(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String commandIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_command_ipv4");
	String commandIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_command_ipv6");
	String vrfname_ipv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_vrf_Ipv4");
	String vrfname_ipv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_vrf_Ipv6");

	
	waitforPagetobeenable();
	
	scrollUp();
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Router Tool' panel");
	
	WebElement vendorModel=findWebElement(APT_wholeSale_Obj.APT_wholeSale.MAS_View_VendorModelValue);
	String vendor=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_VendorModelValue,"MAS_View_VendorModelValue");
	WebElement manageAddress=findWebElement(APT_wholeSale_Obj.APT_wholeSale.MAS_View_ManagementAddressValue);
	String ipAddress=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.MAS_View_ManagementAddressValue,"MAS_View_ManagementAddressValue");
	
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.MAS_View_VendorModelValue);
	waitForAjax();
	
	if(vendor.startsWith("Cisco")) {
		
	//Command IPV4	
		addDropdownValues_commonMethod("Command IPV4", APT_wholeSale_Obj.APT_wholeSale.commandIPV4_dropdown, commandIPv4);
		
		hostnametextField_IPV4( commandIPv4, ipAddress);
		
		vrfNametextField_IPV4( commandIPv4, vrfname_ipv4);
		
		click(APT_wholeSale_Obj.APT_wholeSale.executebutton_Ipv4,"executebutton_Ipv4");
		
		
	//Commmand IPV6
		addDropdownValues_commonMethod("Command IPV6", APT_wholeSale_Obj.APT_wholeSale.commandIPV6_dropdown, commandIPv6);
		
		hostnametextField_IPV6(commandIPv6, ipAddress);
		
		vrfNametextField_IPV6( commandIPv6, vrfname_ipv6);
		
		click(APT_wholeSale_Obj.APT_wholeSale.executebutton_IPv6,"executebutton_IPv6");
		
	}
	else if((vendor.startsWith("Juniper "))  && (!vendor.equals("Juniper MX204"))){
		
		//Command IPV4	
				addDropdownValues_commonMethod( "Command IPV4", APT_wholeSale_Obj.APT_wholeSale.commandIPV4_dropdown, commandIPv4);
				
				hostnametextField_IPV4( commandIPv4, ipAddress);
				
				vrfNametextField_IPV4( commandIPv4, vrfname_ipv4);
				
				click( APT_wholeSale_Obj.APT_wholeSale.executebutton_Ipv4,"executebutton_Ipv4");
				
		
	}
	else if(vendor.startsWith("Huawei ")){
		
		//Command IPV4	
				addDropdownValues_commonMethod( "Command IPV4", APT_wholeSale_Obj.APT_wholeSale.commandIPV4_dropdown , commandIPv4);
				
				hostnametextField_IPV4( commandIPv4, ipAddress);
				
				vrfNametextField_IPV4( commandIPv4, vrfname_ipv4);
				
				click( APT_wholeSale_Obj.APT_wholeSale.executebutton_Ipv4,"executebutton_Ipv4");
				
		
	}
	else {
		ExtentTestManager.getTest().log(LogStatus.INFO, "Router Panel will not display for the selected vendorModel: "+vendorModel);
		Report.LogInfo("INFO","Router Panel will not display for the selected vendorModel: "+vendorModel,"PASS");
	}

}

public void hostnametextField_IPV4(String command_ipv4, String ipAddress) {
	boolean IPV4availability=false;
	try {  
		IPV4availability=findWebElement(APT_wholeSale_Obj.APT_wholeSale.commandIPv4_hostnameTextfield).isDisplayed();
	  
	  if(IPV4availability) {
		  
		  sendKeys(APT_wholeSale_Obj.APT_wholeSale.commandIPv4_hostnameTextfield, ipAddress, "IP Address or Hostname");
		  
	  }else {
		  	ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4);
			Report.LogInfo("INFO","Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4,"PASS");
	  }
	}catch(Exception e) {
		e.printStackTrace();
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4);
		Report.LogInfo("INFO","Hostname or IpAddress' for 'Ipv4' text field is not displaying for "+ command_ipv4,"PASS");
	}
}
public void vrfNametextField_IPV4(String command_ipv4, String vrfname_ipv4) {
	boolean IPV4availability=false;
	  
		
	if(command_ipv4.contains("vrf")) {
		try {
			IPV4availability=findWebElement(APT_wholeSale_Obj.APT_wholeSale.commandIPv4_vrfnameTextField).isDisplayed();
			
			if(IPV4availability) {
				sendKeys(APT_wholeSale_Obj.APT_wholeSale.commandIPv4_vrfnameTextField, vrfname_ipv4,"Router Vrf Name");
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
				Report.LogInfo("INFO","'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"PASS");
			}
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
			Report.LogInfo("INFO","'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"PASS");
		}
		
	}else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'VRF Name IPv4' text field is not displaying for "+ command_ipv4);
		Report.LogInfo("INFO","'VRF Name IPv4' text field is not displaying for "+ command_ipv4 +" command","PASS");
	}
}	
public void hostnametextField_IPV6(String commandIPv6, String ipv6Address) {
	boolean IPV4availability=false;
	try {  
		IPV4availability=findWebElement(APT_wholeSale_Obj.APT_wholeSale.commandIPv6_hostnameTextfield).isDisplayed();
	  
	  if(IPV4availability) {
		  
		  sendKeys(APT_wholeSale_Obj.APT_wholeSale.commandIPv6_hostnameTextfield, ipv6Address, "IP Address or Hostname");
		  
	  }else {
		  	ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
			Report.LogInfo("INFO","'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"PASS");
	  }
	}catch(Exception e) {
		e.printStackTrace();
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
		Report.LogInfo("INFO","'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"PASS");
	}
}
public void vrfNametextField_IPV6(String commandIPV6, String vrfname_IPV6) {
	boolean IPV6availability=false;
	
	if(commandIPV6.equalsIgnoreCase("vrf")) {
		try {  
			IPV6availability=findWebElement(APT_wholeSale_Obj.APT_wholeSale.commandIPv6_vrfnameTextField).isDisplayed();
			
			if(IPV6availability) {
				sendKeys(APT_wholeSale_Obj.APT_wholeSale.commandIPv6_vrfnameTextField, vrfname_IPV6, "Router Vrf Name");
			}else {
				ExtentTestManager.getTest().log(LogStatus.INFO, "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
				Report.LogInfo("INFO","'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"PASS");
			}
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO, "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
			Report.LogInfo("INFO","'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"PASS");
		}
	}
	else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'VRF Name IPv6' text field is not displaying for "+ commandIPV6);
		Report.LogInfo("INFO","'VRF Name IPv6' text field is not displaying for "+ commandIPV6 +" command","PASS");
	}
}	


public void editCPEdevice(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String editRouterId = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_routerID");
	String editVendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_vendorModel");
	String editManagamentAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_managementAddress");
	String editSnmpro = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_Snmpro");
	String editSnmprw = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_Snmprw");
	String editSNMPv3ContaxtName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_SNMPv3Contextname");
	String editSNMPv3ContextEngineId = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_SNMPv3ContextEngineId");
	String editSNMPv3securityUsername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_SNMPv3securityUsername");
	String editSNMPv3AutoProto = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_SNMPv3AuthProto");
	String editSNMPv3AuthPasswrd = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_SNMPv3AuthPasswrd");
	String editCountry = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCountry");
	String editExistingCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editExistingCity");
	String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editExistingCityValue");
	String editExistingSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editExistingSite");
	String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editExistingSiteValue");
	String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editExistingPremise");
	String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editExistingPremiseValue");
	String editNewCity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewCity");
	String editNewSite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewSite");
	String editNewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewPremise");
	String editNewCityName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewCityName");
	String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewCityCode");
	String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewSiteName");
	String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewSiteCode");
	String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewPremiseName");
	String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editNewPremiseCode");
	


	
	//sendKeys(APT_wholeSale_Obj.APT_wholeSale.routerId , editRouterId, "Router ID");   //Router ID
	
	addDropdownValues_commonMethod_ForSpantag("Vendor/Model" , APT_wholeSale_Obj.APT_wholeSale.CPE_vendorModel, editVendorModel);   //Vendor Model
	
	//sendKeys( APT_wholeSale_Obj.APT_wholeSale.CPE_manageAddress, editManagamentAddress, "Management Address");   //Management Address
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpro, editSnmpro , "Snmpro");   //Snmpro
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmprw, editSnmprw, "Snmprw");   //Snmprw
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpV3ContextName, editSNMPv3ContaxtName, "Snmp V3 Context Name");   //Snmp V3 Context Name
	
	sendKeys( APT_wholeSale_Obj.APT_wholeSale.CPE_snmpV3ContextEngineID, editSNMPv3ContextEngineId, "Snmp V3 Context Engine ID");   //Snmp V3 Context Engine ID
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpv3SecurityUserName, editSNMPv3securityUsername, "Snmp V3 Security Username");   //Snmp V3 Security Username
	
	//addDropdownValues_commonMethod( "Snmp V3 Auth Proto" , APT_wholeSale_Obj.APT_wholeSale.CPE_snmpv3AuthProto, editSNMPv3AutoProto);   ////Snmp V3 Auth Proto
	
	//sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpv3AuthPasswrd, editSNMPv3AuthPasswrd, "Snmp V3 Auth Password");   //Snmp V3 Auth Password
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	clickOnBankPage();
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton,  "OK");
	waitForAjax();
	
}

public void addDropdownValues_commonMethod_ForSpantag(String labelname, String xpath, String expectedValueToAdd) throws InterruptedException {
	  boolean availability=false;
	  List<String> ls = new ArrayList<String>();
	  
	try {  
	//  availability=getwebelement(xml.getlocator("//locators/" + application + "/"+ xpath +"")).isDisplayed();
		if(isVisible(xpath)) {
		  ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
		  Report.LogInfo("INFO",labelname + " dropdown is displaying","PASS");
		  
		  if(expectedValueToAdd.equalsIgnoreCase("null")) {
			  
			  ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under "+ labelname + " dropdown");
			  Report.LogInfo("INFO"," No values selected under "+ labelname + " dropdown","PASS");
		  }else {
			  webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//div[text()='�']")).click();
			 // Clickon(getwebelement("//div[label[text()='"+ labelname +"']]//div[text()='�']"));
			 waitForAjax();
			  
			  //verify list of values inside dropdown
			  List<WebElement> listofvalues = webDriver
						.findElements(By.xpath("//span[@role='option']"));
			  
			  ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside "+ labelname + " dropdown is:  ");
			  Report.LogInfo( "INFO"," List of values inside "+ labelname + "dropdown is:  ","PASS");
			  
				for (WebElement valuetypes : listofvalues) {
					Report.LogInfo("INFO","service sub types : " + valuetypes.getText(),"PASS");
							 ls.add(valuetypes.getText());
				}
				
				    ExtentTestManager.getTest().log(LogStatus.PASS, "list of values inside "+labelname+" dropdown is: "+ls);
				    Report.LogInfo("INFO","list of values inside "+labelname+" dropdown is: "+ls,"PASS");
				
				waitForAjax();
				webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd);
				 waitForAjax();
				
			webDriver.findElement(By.xpath("(//span[contains(text(),'"+ expectedValueToAdd +"')])[1]")).click();
			 waitForAjax();
			  
			/*  String actualValue=getwebelement("//label[text()='"+ labelname +"']/following-sibling::div//span").getText();
			  ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
			  Report.LogInfo( labelname + " dropdown value selected as: "+ actualValue);
			  */
		  }
	  }else {
		  ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
		  Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
	  }
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
		 Report.LogInfo("INFO",labelname + " is not displaying","FAIL");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under "+ labelname + " dropdown");
		 Report.LogInfo("INFO"," NO value selected under "+ labelname + " dropdown","FAIL");
	}
	
}

public void viewCPEdevice(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String RouterId = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_routerID");
	String VendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_vendorModel");
	String ManagamentAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_managementAddress");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_Snmpro");
	String Snmprw = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_Snmprw");
	String SNMPv3ContextName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3Contextname");
	String SNMPv3ContextEngineId = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3ContextEngineId");
	String SNMPv3securityUsername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3securityUsername");
	String SNMPv3AutoProto = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3AutoProto");
	String SNMPv3AuthPasswrd = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3AuthPasswrd");
	String Country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Country");
	String existingcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingCity");
	String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingCityValue");
	String existingsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingSite");
	String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingSiteValue");

	String existingpremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingPremise");
	String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Existing PremiseValue");
	String newcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCity");
	String cityname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityName");
	String Citycode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityCode");
	String sitename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteName");
	String sitecode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteCode");
	String premisename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseName");
	String premisecode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseCode");
	String newsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSite");
	String NewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremise");
	

	
	waitforPagetobeenable();
	scrollUp();
	waitForAjax();
	
	
	
	compareText_InViewPage( "Vendor/Model" , VendorModel);   //vendor/model
	
	compareText_InViewPage( "Management Address", ManagamentAddress);   //Management Address
	
	compareText_InViewPage( "Snmpro", Snmpro);   //Snmpro
	
	
	
	
//City
	if((existingcity.equalsIgnoreCase("yes")) && (newcity.equalsIgnoreCase("NO"))) {
		
		//Existing City
		  //	compareText_InViewPage( "City", existingcityvalue);
		
	}
	else if((existingcity.equalsIgnoreCase("NO")) && (newcity.equalsIgnoreCase("Yes"))) {
		
		//new City
			compareText_InViewPage( "City", cityname);
		
	}
	
//Site
	if((existingsite.equalsIgnoreCase("yes")) && (newsite.equalsIgnoreCase("NO"))) {
		
		//Existing Site
			//compareText_InViewPage( "Site" , existingsitevalue);
	}
	else if((existingsite.equalsIgnoreCase("No")) && (newsite.equalsIgnoreCase("Yes"))) {
		
		//New Site
			compareText_InViewPage( "Site" , sitename);
		
	}
	
//Premise
	if((existingpremise.equalsIgnoreCase("yes")) && (NewPremise.equalsIgnoreCase("NO"))) {
		
		//Existing premise
			 compareText_InViewPage( "Premise", existingpremisevalue);
		
	}
	else if((existingpremise.equalsIgnoreCase("No")) && (NewPremise.equalsIgnoreCase("Yes"))) {
		
		//new premise
			compareText_InViewPage("Premise", premisename);
	}

}


public void CPEdevice_clickOnViewLink(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String existingdevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_routerID");

	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'View CPE Device' Functionality");
	
	
		webDriver.findElement(By.xpath("//div[contains(.,'"+existingdevicename+"')]//span//a[text()='View']")).click();
    
   		
}
public void addCPEdevice(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String RouterId = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_routerID");
	String VendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_vendorModel");
	String ManagamentAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_managementAddress");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_Snmpro");
	String Snmprw = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_Snmprw");
	String SNMPv3ContaxtName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3Contextname");
	String SNMPv3ContextEngineId = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3ContextEngineId");
	String SNMPv3securityUsername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3securityUsername");
	String SNMPv3AutoProto = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3AutoProto");
	String SNMPv3AuthPasswrd = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_SNMPv3AuthPasswrd");
	String Country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Country");
	String existingcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingCity");
	String existingcityvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingCityValue");
	String existingsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingSite");
	String existingsitevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingSiteValue");

	String existingpremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingPremise");
	String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingPremiseValue");
	String newcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCity");
	String cityname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityName");
	String Citycode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewCityCode");
	String sitename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteName");
	String sitecode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSiteCode");
	String premisename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseName");
	String premisecode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremiseCode");
	String newsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewSite");
	String NewPremise = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewPremise");
	


	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.routerId, RouterId, "Router ID");   //Router ID
	
	addDropdownValues_commonMethod_ForSpantag("Vendor/Model" , APT_wholeSale_Obj.APT_wholeSale.CPE_vendorModel, VendorModel);   //Vendor Model
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_manageAddress, ManagamentAddress, "Management Address");   //Management Address
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpro, Snmpro , "Snmpro");   //Snmpro
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmprw, Snmprw, "Snmprw");   //Snmprw
	
	sendKeys( APT_wholeSale_Obj.APT_wholeSale.CPE_snmpV3ContextName, SNMPv3ContaxtName, "Snmp V3 Context Name");   //Snmp V3 Context Name
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpV3ContextEngineID, SNMPv3ContextEngineId, "Snmp V3 Context Engine ID");   //Snmp V3 Context Engine ID
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpv3SecurityUserName, SNMPv3securityUsername, "Snmp V3 Security Username");   //Snmp V3 Security Username
	
	addDropdownValues_commonMethod( "Snmp V3 Auth Proto" , APT_wholeSale_Obj.APT_wholeSale.CPE_snmpv3AuthProto, SNMPv3AutoProto);   ////Snmp V3 Auth Proto
	
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.CPE_snmpv3AuthPasswrd, SNMPv3AuthPasswrd, "Snmp V3 Auth Password");   //Snmp V3 Auth Password
	
	
//select country
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
//	addDropdownValues_commonMethod(application, "Country", "countryinput", Country, xml);
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.countryinput,"countryinput", Country);
	
//New City		
	if(existingcity.equalsIgnoreCase("no") & newcity.equalsIgnoreCase("yes")) {
		click(APT_wholeSale_Obj.APT_wholeSale.addcityswitch,"addcityswitch");
	   //City name
		sendKeys( APT_wholeSale_Obj.APT_wholeSale.citynameinputfield, cityname, "City Name");
	   //City Code	
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.citycodeinputfield, Citycode, "City Code");
	   //Site name
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.sitenameinputfield_addCityToggleSelected, sitename, "Site Name");
	   //Site Code
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.sitecodeinputfield_addCityToggleSelected, sitecode, "Site Code");
	   //Premise name	
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisenameinputfield_addCityToggleSelected, premisename, "Premise Name");
	   //Premise Code	
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisecodeinputfield_addCityToggleSelected, premisecode, "Premise Code");
			
	}	

//Existing City	
	else if(existingcity.equalsIgnoreCase("yes") & newcity.equalsIgnoreCase("no")) {
		
//	   addDropdownValues_commonMethod(application, "City", "citydropdowninput", existingcityvalue, xml);
	   selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.citydropdowninput, "City", existingcityvalue);
		
		
	 //Existing Site
		  if(existingsite.equalsIgnoreCase("yes") & newsite.equalsIgnoreCase("no")) {
//			  addDropdownValues_commonMethod(application, "Site", "sitedropdowninput", existingsitevalue, xml);
			  selectValueInsideDropdown( APT_wholeSale_Obj.APT_wholeSale.sitedropdowninput, "Site", existingsitevalue);
			  
		 //Existing Premise
			  if(existingpremise.equalsIgnoreCase("yes") & NewPremise.equalsIgnoreCase("no")) {
//				  addDropdownValues_commonMethod(application, "Premise", "premisedropdowninput", existingpremisevalue, xml);
				  selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.premisedropdowninput, "Premise", existingpremisevalue);
	          	 }
			  
			//New Premise  
			  else if(existingpremise.equalsIgnoreCase("no") & NewPremise.equalsIgnoreCase("yes")) {
				  
				  click(APT_wholeSale_Obj.APT_wholeSale.addpremiseswitch,"addpremiseswitch");
				  //Premise name	
					sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisenameinputfield_addPremiseToggleSelected, premisename, "Premise Name");
				   //Premise Code	
					sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisecodeinputfield_addPremiseToggleSelected, premisecode, "Premise Code");
			  } 
         	}
		  
	  else if(existingsite.equalsIgnoreCase("no") & newsite.equalsIgnoreCase("yes")) {
		  	
		  click(APT_wholeSale_Obj.APT_wholeSale.addsiteswitch,"addsiteswitch");
		  //Site name
		  sendKeys(APT_wholeSale_Obj.APT_wholeSale.sitenameinputfield_addSiteToggleSelected, sitename, "Site Name");
		   //Site Code
		  sendKeys(APT_wholeSale_Obj.APT_wholeSale.sitecodeinputfield_addSiteToggleSelected, sitecode, "Site Code");
		   //Premise name	
		  sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisenameinputfield_addSiteToggleSelected, premisename, "Premise Name");
		   //Premise Code	
		  sendKeys(APT_wholeSale_Obj.APT_wholeSale.premisecodeinputfield_addSiteToggleSelected, premisecode, "Premise Code");
		  }
	}

	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");	Thread.sleep(1000);
	
	clickOnBankPage();
	waitForAjax();
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");	Thread.sleep(1000);
	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton , "OK");
	waitForAjax();
	
}


public void MASswitch__DeleteFromServiceFunctionality(String existingdevicename) throws InterruptedException, IOException { 
	
			
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Delete MAS Switch' Functionality");
	
	WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	
	
	if(findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader).isDisplayed())
    {
		
		webDriver.findElement(By.xpath("//tr//td//b[contains(.,'"+ existingdevicename +"')]//parent::td//following-sibling::span//a[text()='Delete from Service']")).click();
		
		
        WebElement DeleteAlertPopup= findWebElement(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);

                      if(DeleteAlertPopup.isDisplayed())
                      {
                            click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
                            //compareText(APT_wholeSale_Obj.APT_wholeSale.MAS_deleteSuccessMessage, "MAS switch deleted successfully",  "Device delete success message");
                           
                      
                      }
                      else
                      {
                    	  Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
                            ExtentTestManager.getTest().log(LogStatus.FAIL, " Delete alert popup is not displayed");
                      }
                      
                }
                
               
         
   
    else
    {
          ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
          Report.LogInfo("INFO","No Device added in grid","FAIL");
    }		
	}


public void verifyAddInterfaceFunction_PE(String testDataFile, String dataSheet, String scriptNo,String dataSetNo ) 
		throws InterruptedException,  IOException { 


String MAS_AccessMedia= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_AccessMedia");
String MAS_Network= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_Network");
String MAS_HSRPBGP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPBGP");
String MAS_Interface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_Interface");
String MAS_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressRange");
String MAS_InterfaceAddressMask= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressMask");
String MAS_HSRPIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPIP");
String MAS_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressRangeIPV6");
String MAS_HSRPIPv6Address= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPIPv6Address");
String MAS_PrimaryIPv6onMas1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_PrimaryIPv6onMAS1");
String MAS_SecondaryIPv6onMas2= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_SecondaryIPv6onMAS2");
String MAS_GroupNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_GroupNumber");
String MAS_Link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_Link");
String MAS_VLANID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VLANID");
String MAS_IVManagement= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_IVManagement");
String MAS_generateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_GenerateConfiguration");
String MAS_HSRPTrackInterface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPTrackInterface");
String MAS_HSRPAuthentication= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPAuthentication");


ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Add Interface' Functionality");

WebElement routerpanel= findWebElement(APT_wholeSale_Obj.APT_wholeSale.routerTool_HeaderPanel);
scrollIntoView(routerpanel);
waitForAjax();

  //clicking on action dropdown under Interface panel
click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_addInterfaceLink,"Add Interface/Link");

scrollDown(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton);
click( APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");

verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_configurationWarningMessage, "Configuration");

scrollUp();
waitForAjax();

verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_interfaceWarningMessage, "Interface");

if(MAS_AccessMedia.equalsIgnoreCase("VPN")) {
addDropdownValues_commonMethod( "Access Media", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);  //Access Media Dropdown
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
addDropdownValues_commonMethod("Network",  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange, "Interface Address Range");   //interface address range text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1,  "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field

scrollDown(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
Thread.sleep(1000);

sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber, "Group Number");   //Group Number text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox,"IV Management",  MAS_IVManagement);   //IV Management checkbox

}else if(MAS_AccessMedia.equalsIgnoreCase("EPN")) {


addDropdownValues_commonMethod("Access Media", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);  //Access Media Dropdown
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
addDropdownValues_commonMethod( "HSRP/BGP", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRDorBPdropdown, MAS_HSRPBGP);   //HSRP/BGP dropdown
addDropdownValues_commonMethod( "Network", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange, "Interface Address Range");   //interface address range text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1, "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field

scrollDown(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
waitForAjax();

sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber, "Group Number");   //Group Number text field
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox,  MAS_IVManagement,"IV Management");   //IV Management checkbox
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPtrackInterfacetextField, MAS_HSRPTrackInterface,  "HSRP Track Interface");   //HSRP Track Interface text Field
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPauthenticationtextField, MAS_HSRPAuthentication, "HSRP Authentication");   //HSRP Authentication text Field

}

scrollUp();
waitForAjax();

//perform Generate configuration
boolean configurationpanel=false;
try {	
configurationpanel=findWebElement(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_confiugrationPanelheader).isDisplayed();
if(configurationpanel) {
ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
//Log.info("'Configuration' panel is displaying");
Report.LogInfo("INFO","Configuration' panel is displaying","PASS");
addDropdownValues_commonMethod( "Generate Configuration ", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationDropdown, MAS_generateConfiguration);
waitForAjax();

click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationLink, "Generate Configuration");
waitForAjax();

scrollUp();
Thread.sleep(1000);

String configurationvalues=findWebElement(APT_wholeSale_Obj.APT_wholeSale.configuration_textArea).getText();
if(configurationvalues.isEmpty()) {
	ExtentTestManager.getTest().log(LogStatus.FAIL, "After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
	//Log.info("After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
	Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box","FAIL");


}else {
	ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate Configuration' link, "
			+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
	//Log.info("After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: "+configurationvalues);

	Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: +configurationvalues","PASS");

}

}else {
ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
//Log.info("'Configuration' panel is not displaying");
Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");

}
}catch(Exception e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
	//Log.info("'Configuration' panel is not displaying");
	Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");
}
((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
waitForAjax();
click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
}

/////////////////////////////////////////////////////

/////////////////////////

public void verifyInterfaceValues_PE(String PE_Interface,String testDataFile,String dataSheet,String scriptNo, String dataSetNo) throws InterruptedException, IOException {

String PE_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressRange");

String PE_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressRangeIPV6");



String interfaceAddressrangeValue=null;
	

if(PE_InterfaceAddressRangeIPV6.equalsIgnoreCase("null")) {
interfaceAddressrangeValue=PE_InterfaceAddressRange;
}else {
interfaceAddressrangeValue=PE_InterfaceAddressRange+","+PE_InterfaceAddressRangeIPV6;
}

verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_nameValue , "Interface" );
verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_linkValue, "Link/Circuit id");
verifyExists(  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_InteraceAddressRangeValue, "Interface Address Range");
verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_InteraceAddressValue, "Interface Address");
verifyExists(  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_bearertypeValue,"Bearer Type");
verifyExists(  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_bandwidthvalue,"Bandwidth" );
verifyExists(  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_vlanidvalue , "Vlan Id");
verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_interfacePanel_IfInOctetsValue,"IfInOctets");
verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_interfacePanel_IVmanagementvalue, "IV Management");

}

public void editInterface_PE(String testDataFile, String dataSheet, String scriptNo,String dataSetNo,String PE_interfaceName ) 
		throws InterruptedException, IOException { 



String MAS_AccessMedia= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_AccessMedia");
String MAS_Network= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_Network");
String MAS_HSRPBGP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPBGP");
String MAS_Interface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_Interface");
String MAS_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressRange");
String MAS_InterfaceAddressMask= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressMask");
String MAS_HSRPIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPIP");
String MAS_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_InterfaceAddressRangeIPV6");
String MAS_HSRPIPv6Address= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPIPv6Address");
String MAS_PrimaryIPv6onMas1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_PrimaryIPv6onMAS1");
String MAS_SecondaryIPv6onMas2= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_SecondaryIPv6onMAS2");
String MAS_GroupNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_GroupNumber");
String MAS_Link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_Link");
String MAS_VLANID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VLANID");
String MAS_IVManagement= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_IVManagement");
String MAS_generateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_GenerateConfiguration");
String MAS_HSRPTrackInterface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPTrackInterface");
String MAS_HSRPAuthentication= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_VRRPAuthentication");
	

waitforPagetobeenable();

addDropdownValues_commonMethod("Access Media", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);  //Access Media Dropdown
WebElement accessMediaElement = findWebElement(APT_wholeSale_Obj.APT_wholeSale.fetchMAS_AccessMediaDropdown); 
String accessMediaActualValue=accessMediaElement.getText();

if(accessMediaActualValue.equalsIgnoreCase("VPN")) {
	
	sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
	addDropdownValues_commonMethod( "Network", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange, "Interface Address Range");   //interface address range text field
	  //sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
	  //sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1, "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field
	
scrollDown( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
waitForAjax();
	
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber,"Group Number");   //Group Number text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
editcheckbox_commonMethod("IV Management",APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox ,MAS_IVManagement);   //IV Management checkbox	

}else if(accessMediaActualValue.equalsIgnoreCase("EPN")) {
	
	sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
	addDropdownValues_commonMethod("HSRP/BGP",  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRDorBPdropdown, MAS_HSRPBGP);   //HSRP/BGP dropdown
	addDropdownValues_commonMethod("Network",  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange,  "Interface Address Range");   //interface address range text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1, "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field
	
	scrollDown( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
	waitForAjax();
	
	sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber, "Group Number");   //Group Number text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
	editcheckbox_commonMethod( MAS_IVManagement, "MAS_PE_IVManagementCheckbox" , "IV Management");    //IV Management checkbox
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPtrackInterfacetextField, MAS_HSRPTrackInterface, "HSRP Track Interface");   //HSRP Track Interface text Field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPauthenticationtextField, MAS_HSRPAuthentication, "HSRP Authentication");   //HSRP Authentication text Field
	
}
	
scrollDown( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox);	
waitForAjax();

//perform Generate configuration
boolean configurationpanel=false;
try {	
configurationpanel=findWebElement(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox).isDisplayed();
if(configurationpanel) {
	ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
	//Log.info("'Configuration' panel is displaying");
	Report.LogInfo("INFO","Configuration' panel is displaying","PASS");
	addDropdownValues_commonMethod( "Generate Configuration ", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationDropdown, MAS_generateConfiguration);
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationLink, "Generate Configuration");
	waitForAjax();
	
	scrollDown( APT_wholeSale_Obj.APT_wholeSale.configuration_textArea);	
	waitForAjax();
	
	String configurationvalues=findWebElement(APT_wholeSale_Obj.APT_wholeSale.configuration_textArea).getText();
	if(configurationvalues.isEmpty()) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
		//Log.info("After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
		Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box","FAIL");
	
	}else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate Configuration' link, "
				+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
		//Log.info("After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
	
		Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: +configurationvalues","PASS");
	}

}else {
	ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
	//Log.info("'Configuration' panel is not displaying");
	Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");
}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
		//Log.info("'Configuration' panel is not displaying");
		Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");
	
	}
scrollDown( APT_wholeSale_Obj.APT_wholeSale.trunk_okButton);	
waitForAjax();
	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
}

public void clickOnCPEdeviceLink(String trunkGroupName, String siteOrderName) throws Exception {
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Add CPE Device' Functionality");
	
	webDriver.findElement(By.xpath("(//tr//td[contains(.,'"+trunkGroupName+"')]//following-sibling::td//a[text()='Add CPE Device'])[1]")).click();

	
}

public void editSBC_manualExecutionConfig(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String editSBCmanualConfigvalur= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editSBCmanualConfigvalue");

	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying 'Edit SBC Manual Configuration' Functionality");
	Report.LogInfo("INFO", "verifying 'Edit SBC Manual Configuration' Functionality", "PASS");
	
	click( APT_wholeSale_Obj.APT_wholeSale.SBC_selectCreatedValue, "PSXcreated");
	
	//click(APT_wholeSale_Obj.APT_wholeSale.SBCManualConfig_actionDropdown, "Action");  //click acton dropdown
	click(APT_wholeSale_Obj.APT_wholeSale.SBC_editLink, "Edit link");   //click on edit link
	
	//Reporter.log("date displaying as : "+getCurrentDate());
	//Reporter.log("username displays as: "+Getkeyvalue("APT_VoiceService"));
	
	waitForAjax();
	
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
    Iterator<String> itr = allwindows.iterator();
    while(itr.hasNext())
    {
          String childWindow = itr.next();
          if(!mainWindow.equals(childWindow)){
        	  webDriver.switchTo().window(childWindow);
                Reporter.log(webDriver.switchTo().window(childWindow).getTitle());
                  
//                driver.manage().window().maximize();
                waitForAjax();
                
                sendKeys(APT_wholeSale_Obj.APT_wholeSale.GSX_editPage_teaxtArea, editSBCmanualConfigvalur, "GSX manual Configuration");
               
                ((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
                waitForAjax();
                click(APT_wholeSale_Obj.APT_wholeSale.GSX_editPage_SaveButton, "Save");
                
          }
    }
    
    webDriver.switchTo().window(mainWindow);
    waitForAjax();
    verifysuccessmessage("Manual Configuration updated Successfully");

    
}


public void verifySBCfileAdded() throws InterruptedException, IOException {
	
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying Added Values under 'Manual SBC Configuration' panel");
	
	WebElement filename=findWebElement(APT_wholeSale_Obj.APT_wholeSale.SBC_selectCreatedValue);
	String SBCfilenameCreated=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.SBC_selectCreatedValue,"SBC_selectCreatedValue");
	
	if(SBCfilenameCreated.isEmpty()) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "SBC Manually Executed configuration file name is not displaying");
		Report.LogInfo("INFO","SBC Manually Executed configuration file name is not displaying","PASS");
	}else {
		
		int i=0; int k=0; int j=0;
		
	//Fetching Column Names	
		List<WebElement> columns=findWebElements(APT_wholeSale_Obj.APT_wholeSale.SBC_columnNames);
		int listOfColumns=columns.size();
		
		String columnName[]=new String[listOfColumns];
		for (WebElement column : columns) {
			
			columnName[k]=column.getText();
			k++;
		}
		
		ExtentTestManager.getTest().log(LogStatus.INFO, "column names display as: "+ columnName[1] + "  " + columnName[2]);  //printing column Names
		
		
	//Fetching value under File Column	
		List<WebElement> files=findWebElements(APT_wholeSale_Obj.APT_wholeSale.SBC_filenames);
		int listOfFiles=files.size();
		
		String[] fileNameValues=new String[listOfFiles];
		
		for(WebElement filenametransfer : files) {
			
			fileNameValues[i] = filenametransfer.getText();
			i++;
		}
		
	
	//Fetching value under Date Column
		List<WebElement> dates=findWebElements(APT_wholeSale_Obj.APT_wholeSale.SBC_dates);
		int listOfDates=dates.size();
		
		String[] dateValues=new String[listOfDates];
		
		for(WebElement dateList : dates) {
			
			dateValues[j] = dateList.getText();
			j++;
		}
	
	
		ExtentTestManager.getTest().log(LogStatus.INFO, "Values under 'SBC Manually Executed Configuration' value displays as: ");
		
	for(int y=0; y<fileNameValues.length; y++) {
	ExtentTestManager.getTest().log(LogStatus.PASS,  fileNameValues[y] +"      "+ dateValues[y]);
	}
	
	}
}

public void viewTrunk_GSX_executeConfiguration(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String expectedConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"viewtrunk_GSXconfiguration");
	String gsxTextvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"gsxConfigurationValue");

	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'GSX_Execute Configuration' Functionality");
	Report.LogInfo("INFO", "Verifying 'GSX_Execute Configuration' Functionality","PASS");
	
	WebElement localRingBack=findWebElement(APT_wholeSale_Obj.APT_wholeSale.labelName_localRingBackTone);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.labelName_localRingBackTone);
	waitForAjax();
	
	addDropdownValues_commonMethod("GSX Configuration", APT_wholeSale_Obj.APT_wholeSale.GSXconfigurationDropdown_viewtrunk, expectedConfiguration);
	
	click(APT_wholeSale_Obj.APT_wholeSale.viewTrunk_GSX_generateConfigurationButton, "Generate Configuration");
	
	if((expectedConfiguration.equalsIgnoreCase("Create Trunk Group")) ||  (expectedConfiguration.equalsIgnoreCase("Update trunk group"))) {
		
		String mainWindow=webDriver.getWindowHandle();
		Set<String> allwindows = webDriver.getWindowHandles();
        Iterator<String> itr = allwindows.iterator();
        while(itr.hasNext())
        {
              String childWindow = itr.next();
              if(!mainWindow.equals(childWindow)){
            	  webDriver.switchTo().window(childWindow);
                    Reporter.log(webDriver.switchTo().window(childWindow).getTitle());
                      
                    waitForAjax();
                  String gsxConfiguredValue = getTextFrom(APT_wholeSale_Obj.APT_wholeSale.GSXcongig_textArea,"GSXcongig_textArea");
                  if(gsxConfiguredValue.isEmpty()) {
                	  ExtentTestManager.getTest().log(LogStatus.PASS, "No values displaying under GSX Configuration");
                	  Report.LogInfo("INFO", "No values displaying under GSX Configuration","PASS");

                  }else {
                	  ExtentTestManager.getTest().log(LogStatus.PASS, "'Under GSX Configuration' value is displaying as: "+ gsxConfiguredValue);
                	  Report.LogInfo("INFO", "'Under GSX Configuration' value is displaying as: "+ gsxConfiguredValue,"PASS");

                  }
                  
                  sendKeys(APT_wholeSale_Obj.APT_wholeSale.GSXconfig_textArea, gsxTextvalue , "GSX Configuration");
                 Report.LogInfo("INFO","came inside child window","PASS");
                    
                 ((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
                    waitForAjax();
                    click(APT_wholeSale_Obj.APT_wholeSale.GSX_config_executeButton, "Execute");
                    waitForAjax();
                    
                        waitforPagetobeenable();
                    
                    click( APT_wholeSale_Obj.APT_wholeSale.GSXconfig_closeButton, "Close");
              }
        }
        
        waitForAjax();
        webDriver.switchTo().window(mainWindow);
        waitForAjax();
	}
	else if(expectedConfiguration.equalsIgnoreCase("Remove Trunk Group")) {
		
		 Alert alert = webDriver.switchTo().alert();	
		 alert.accept();
		 waitForAjax();
		
	}
	
    scrollUp();
   String gsxSuccessMesage= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.GSXconfig_sucessMessage,"GSXconfig_sucessMessage");
   if(gsxSuccessMesage.isEmpty()) {
	   ExtentTestManager.getTest().log(LogStatus.FAIL, "NO message displays after clicking on 'Execute' button");
	  Report.LogInfo("INFO", "NO message displays after clicking on 'Execute' button","PASS");

	   
   }
   else {
	   ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Execute' button, success Message displays as: "+gsxSuccessMesage);
	   Report.LogInfo("INFO", "After clicking on 'Execute' button, success Message displays as: "+gsxSuccessMesage,"PASS");

   }
 
}

public void viewTrunk_SBC_executeConfiguration(String testDataFile,String dataSheet,String scriptNo, String dataSetNo) throws InterruptedException, IOException, AWTException {
	
	String expectedConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"viewtrunk_SBCconfiguration");

	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'SBC_Execute Configuration' Functionality");
	Report.LogInfo("INFO", "Verifying 'SBC_Execute Configuration' Functionality","PASS");

	
	WebElement localRingBack=findWebElement(APT_wholeSale_Obj.APT_wholeSale.labelName_localRingBackTone);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.labelName_localRingBackTone);
	waitForAjax();
	
	addDropdownValues_commonMethod( "SBC Configuration", APT_wholeSale_Obj.APT_wholeSale.SBCconfigurationDropdown_viewtrunk, expectedConfiguration);
	/*
	click(APT_wholeSale_Obj.APT_wholeSale.viewTrunk_SBC_executeButton, "Execute");
	
	Robot r = new Robot();
	Thread.sleep(3000);
	r.keyPress(KeyEvent.VK_ENTER);
	Thread.sleep(1000);
	r.keyRelease(KeyEvent.VK_ENTER);
	Thread.sleep(1000);

     
     //fetch success Message
     if(expectedConfiguration.equalsIgnoreCase("Cease Trunk Group")) {
    	 verifysuccessmessage("SBC sync started successfully. Please check the sync status of this Trunk Group.");
     }
     else if(expectedConfiguration.equalsIgnoreCase("Synchronize All")) {
    	 verifysuccessmessage("SBC sync has not started because of the manual change in the SBC. Uncheck the 'Manual Configuration' flag "+"to overwrite the changes.");
     }
	*/
}


public void viewTrunk_PSX_executeConfiguration(String testDataFile,String dataSheet,String scriptNo,String dataSetNo,String trunkName) throws InterruptedException, IOException {
	
	
	String expectedConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"viewtrunk_PSXconfiguration");
	String carrierIPoriginating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carierIPOrient_resiltrunk");
	String carrierIPterminating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carierIPterminat_resiltrunk");
	

	waitforPagetobeenable();
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'PSX_Execute Configuration' Functionality");
	Report.LogInfo("INFO", "Verifying 'PSX_Execute Configuration' Functionality","PASS");

	
	WebElement localRingBack=findWebElement(APT_wholeSale_Obj.APT_wholeSale.labelName_localRingBackTone);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.labelName_localRingBackTone);
	waitForAjax();
	
	addDropdownValues_commonMethod( "PSX Configuration", APT_wholeSale_Obj.APT_wholeSale.PSXconfigurationDropdown_viewtrunk, expectedConfiguration);
	
	/*
	click(APT_wholeSale_Obj.APT_wholeSale.viewTrunk_PSX_executeButton, "Execute");
	
	
	 Alert alert = webDriver.switchTo().alert();		
 		
     // Capturing alert message.    
       String alertMessage= webDriver.switchTo().alert().getText();
       if(alertMessage.isEmpty()) {
    	   ExtentTestManager.getTest().log(LogStatus.FAIL, "No mEssage displays");
	       Report.LogInfo("INFO","No Message displays","PASS"); 
       }else {
    	   ExtentTestManager.getTest().log(LogStatus.PASS, "Alert message displays as: "+alertMessage);
    	 
       }
     
     try {  
       alert.accept();
       waitForAjax();
       
       Reporter.log("accept 2");
       alert.accept();
     }catch(Exception e) {
    	 e.printStackTrace();
     }
     
     if(expectedConfiguration.equalsIgnoreCase("Delete Trunk Group")) {
    	 verifysuccessmessage("PSX sync started successfully.");
     }
     else if(expectedConfiguration.equalsIgnoreCase("Add Destination IP Address")) {
    	 String alertMessage1=alert.getText();
    	 if(alertMessage1.isEmpty()) {
	    	   ExtentTestManager.getTest().log(LogStatus.FAIL, "After clicking on OK, No mEssage displays");
	    	   Report.LogInfo("INFO","No Message displays","FAIL"); 
	       }else {
	    	   ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on OK button in alert box, another Alert message displays as: "+alertMessage);
	    	   Report.LogInfo("INFO","After clicking on OK button in alert box, another Alert message displays as: "+alertMessage,"PASS");
		       
//		       alert.dismiss();
		       alert.accept();
		          waitforPagetobeenable();
		     try { 
		      boolean trunkPanel = findWebElement(APT_wholeSale_Obj.APT_wholeSale.addTrunkGroupPanel).isDisplayed(); 
		      if(trunkPanel) {
		    	  
		    	  waitForAjax();
		    	  ExtentTestManager.getTest().log(LogStatus.PASS, "Navigated to 'Add Trunk' Page as expected");
		    	  Report.LogInfo("INFO","Navigated to 'Add Trunk' Page as expected","PASS");
		    	  
		    	  scrollUp();
		    	  selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.primaryTrunkGroup_Dropdown, "Primary Trunk Group", trunkName);
			       
		    	  WebElement traficDirection=findWebElement(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown);
					scrollDown(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown);
					waitForAjax();
					
				     //Carrier IP originating
						sendKeys(APT_wholeSale_Obj.APT_wholeSale.carrierIPoriginating_textField, carrierIPoriginating,"Carrier IP Originating (Address/Mask)");
						click(APT_wholeSale_Obj.APT_wholeSale.carrierIPoriginating_addButtton, ">>");
						GetTheValuesInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.carrierIPOriginating_addedValue_selectDropdownField,"Carrier IP Originating (Address/Mask)");
						
						
					//Carrier IP Terminating
						sendKeys(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_textField, carrierIPterminating, "Carrier IP Terminating(Address)");
						click(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_addButton, ">>");
						GetTheValuesInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_addedValue_selectDropdownField, "Carrier IP Terminating (Address)");
						
						
						((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
						waitForAjax();
						click( APT_wholeSale_Obj.APT_wholeSale.OKbutton,  "OK");
						  waitforPagetobeenable();
						
						verifysuccessmessage("Trunk created successfully");
						
		      }else {
		    	  ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Trunk' Page is not displaying, after selcting 'add Destination IP Address'");
		    	  Report.LogInfo("INFO","'Add Trunk' Page is not displaying, after selcting 'add Destination IP Address'","PASS");
		    	  
		      }
		     } catch(Exception e) {
		    	 e.printStackTrace();
		    	 ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Trunk' Page is not displaying, after selcting 'add Destination IP Address'");
		    	 Report.LogInfo("INFO","'Add Trunk' Page is not displaying, after selcting 'add Destination IP Address'","PASS");
		     }
	       }
    	 
     }
     */
}
    	 
    	 public void GetTheValuesInsideDropdown(String locator, String labelname) throws IOException, InterruptedException
    		{ //Thread.sleep(3000);
    		 WebElement el = findWebElement(locator);
    			List<String> ls = new ArrayList<String>();
    			Select sel=new Select(el);
    			 List<WebElement> we = sel.getOptions();
    			   
    			    for(WebElement a : we)
    			    {
    			        if(!a.getText().equals("select")){
    			            ls.add(a.getText());
    			            
    			        }
    			    }
    			
    			Report.LogInfo("INFO","Value displaying under "+labelname+" is: "+ ls,"PASS");
    			ExtentTestManager.getTest().log(LogStatus.PASS, "Value displaying under "+labelname+" is: "+ ls);
    		
    		}

    	 
    	public void editTrunk(String testDataFile,String dataSheet,String scriptNo,String dataSetNo,String customerName, String servicename ) throws InterruptedException, IOException {        
    			
    			
    			
    			String trunktype= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"trunkType");
    			String edit_TrunkType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_TrunkType");
    			String edit_VOIPprotocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_VOIPprotocol");
    			String edit_BillingCountry= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_BillingCountry");
    			String edit_CDRdelivery= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_CDRdelivery");
    			String editPrefix= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editPrefix");
    			String editGateway= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editGateway");
    			String editQuality= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editQuality");
    			String editTrafficDirection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editTrafficDirection");
    			String edit_IpAddressType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_IpAddressType");
    			String editCarrierIPoriginating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCarrierIPoriginating");
    			String editCarrierIPterminating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCarrierIPterminating");

    			String	editSIPsignallingPort= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editSIPsignallingPort");
    			String	editSignallingTransport	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editSignallingTransport");
    			String	edit_TLSproflile= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_TLSproflile");
    			String  edit_SRTP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_SRTP"); 
    			String  edit_signallingZone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_signallingZone");
    			String	edit_coltSignalIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_coltSignalIP");
    			String	edit_mediaIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_mediaIP");
    			String	edit_reuseNIFgroup= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_reuseNIFgroup"); 
    			String	edit_reuseSigZonePart= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_reuseSigZonePart");
    			String	edit_callAdmissionControl= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_callAdmissionControl");
    			String	edit_callLimit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_callLimit");
    			String	edit_limitNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_limitNumber"); 
    			String	edit_callrateLimit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_callrateLimit");
    			String	edit_callrateLimitvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_callrateLimitvalue");
    			String	edit_sourceAddressFiltering= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_sourceAddressFiltering");
    			String	edit_relSupport= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_relSupport");
    			String	edit_sipSessionkeepAliveTimer= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_sipSessionkeepAliveTimer");
    			String	edit_internetBasedCustomer	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_internetBasedCustomer");
    			String	edit_vlantag= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_vlantag");
    			String	edit_subInterfaceSlot= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_subInterfaceSlot");
    			String	edit_retryInvite= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_retryInvite");
    			String	edit_addressReachability= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_addressReachability");
    			String	edit_routePriority= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_routePriority");
    			String	editglobalProfile_ExistingSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editglobalProfile_ExistingSelection");
    			String	editglobalProfile_newSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editglobalProfile_newSelection");
    			String	editGlobalProfile_ExistingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editGlobalProfile_ExistingValue");
    			String	editGlobalProfile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editGlobalProfile_newValue");
    			String	editLocalProfile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalProfile_existingFieldSelection");
    			String	editLocalProfile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalProfile_newFieldSelection");
    			String	editLocalProfile_existingvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalProfile_existingvalue"); 
    			String	editLocalProfile_newvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalProfile_newvalue");
    			String	editCOSprofile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCOSprofile_existingFieldSelection");
    			String	editCOSprofile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCOSprofile_newFieldSelection");
    			String	editCOSprofile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCOSprofile_existingValue");
    			String	editCOSprofile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCOSprofile_newValue");
    			String	editPSPGname_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editPSPGname_existingFieldSelection");
    			String	editPSPGname_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editPSPGname_newFieldSelection");
    			String	editpspgName_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editpspgName_existingValue");
    			String	editpspgName_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editpspgName_newValue");
    			String	editPrefferedPSP_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editPrefferedPSP_existingFieldSelection");
    			String	editPrefferedPSP_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editPrefferedPSP_newFieldSelection");
    			String	editPreferredPSP_exitingvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editPreferredPSP_exitingvalue");
    			String	editPreferredPSP_newvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editPreferredPSP_newvalue");
    			String	editCarrier_existingFieldSelection	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCarrier_existingFieldSelection");
    			String	editCarrier_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCarrier_newFieldSelection");
    			String	editCarrier_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCarrier_existingValue");
    			String	editCarrier_newValue	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCarrier_newValue");
    			String	editIPsignalProfile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editIPsignalProfile_existingFieldSelection");
    			String	editIPsignalProfile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editIPsignalProfile_newFieldSelection"); 
    			String	editIPsignalProfile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editIPsignalProfile_existingValue");
    			String	editIPsignalProfile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editIPsignalProfile_newValue");
    			String	editEgressIpsignal_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editEgressIpsignal_existingFieldSelection");
    			String	editEgressIpsignal_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editEgressIpsignal_newFieldSelection");
    			String	editEgressIPsignal_existingValue	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editEgressIPsignal_existingValue"); 
    			String	editEgressIPsignal_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editEgressIPsignal_newValue");
    			String	editInDMPMrule_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editInDMPMrule_existingFieldSelection");
    			String	editInDMPMrule_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editInDMPMrule_newFieldSelection");
    			String	editInDMPMrule_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editInDMPMrule_existingValue");
    			String	editInDMPMrule_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editInDMPMrule_newValue");
    			String	editOutDMPMrule_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editOutDMPMrule_existingFieldSelection"); 
    			String	editOutDMPMrule_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editOutDMPMrule_newFieldSelection");
    			String	editOutDMPMrule_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editOutDMPMrule_existingValue");
    			String	editOutDMPMrule_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editOutDMPMrule_newValue");
    			String	editFeatureControlprofile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editFeatureControlprofile_existingFieldSelection");
    			String	editFeatureControlprofile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editFeatureControlprofile_newFieldSelection");
    			String	editFeatureControlprofile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editFeatureControlprofile_existingValue");
    			String	editFeatureControlprofile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editFeatureControlprofile_newValue");
    			String	editLocalRingBackTone_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalRingBackTone_existingFieldSelection"); 
    			String	editLocalRingBackTone_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalRingBackTone_newFieldSelection"); 
    			String	editLocalRingBackTone_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalRingBackTone_existingValue"); 
    			String	editLocalRingBackTone_newValue	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editLocalRingBackTone_newValue");
    			String	editCreateLowerCaseRoutervalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"editCreateLowerCaseRoutervalue");
    			String	edit_PSXmanualConfigvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_PSXmanualConfigvalue");
    			String	edit_GSXmanualConfigvalue	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_GSXmanualConfigvalue");
    			String	edit_SBCmanualconfigValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"edit_SBCmanualconfigValue");
    			String	billingCoutry= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BillingCountry");

    			ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Edit Trunk' Functionality");
    			Report.LogInfo("INFO", "Verifying 'Edit Trunk' Functionality", "PASS");
    			
    			String subInterfacename_starting="SIF-1-";
    			String subInterfacename_middle="-2-";
    			String NIFgroupDEfaultValue_starting="SIF-1-";
    			String NIFgroupDEfaultValue_middle="-2-";
    			String NIGgroupdefaultValue_last="-OUTSIDE";
    			String ipInterfaceDEfaultValue="EXTERNAL_IPIF_";
    			String addressContextDefaultValue="EXTERNAL_AC_";
    			String ipInterfaceGroupDefaultvalue="EXTERNAL_IPIG_";
    			String signallingZoneDefaultValue="OUT-UNTRUSTED";
    			
    			String trunktype_code=null;
    			String trunkNameToBePassed=null;
    			String prefix_code=null;
    			String gatewayCode=null;
    			String primarytrunk="0";
    			
    			
    			
    			scrollUp();
    			
    			
    		//Action button	
    			//click(APT_wholeSale_Obj.APT_wholeSale.viewPage_ActionButton, "Action");
    			
    		//click on Edit link
    			click(APT_wholeSale_Obj.APT_wholeSale.editLink, "Edit");

    			scrollUp();
    			
    			
    			//Trunk Type	
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.trunkType_Dropdown, "Trunk Type", edit_TrunkType);
    					
    					if(edit_TrunkType.equalsIgnoreCase("null")) {
    						trunktype_code=trunkType_code(trunktype);
    						trunkNameToBePassed=trunktype;
    					}else {
    						trunktype_code=trunkType_code(edit_TrunkType);
    						trunkNameToBePassed=edit_TrunkType;
    					}
    				
    					
    			//Trunk Group Description
    					String expectedValue1=customerName+"_"+servicename+"_"+trunkNameToBePassed;
    					verifyExists(APT_wholeSale_Obj.APT_wholeSale.trunkGroupDEscription_textField,"Trunk Group Description");

    					
    				//Billing COuntry
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.billingCoutry_Dropdown, "Billing Country", edit_BillingCountry);
    					
    					String country_code=findWebElement(APT_wholeSale_Obj.APT_wholeSale.billingCoutry_Dropdown).getAttribute("value");
    					Reporter.log("country dropdon value is: "+country_code);
    					
    				//VOIP Protocol
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.voipProtocol_Dropdown, "VOIP Protocol", edit_VOIPprotocol);
    					waitForAjax();
    					String voipProtocol_actualvalue=GetTheSelectedValueInsideDropdown_trunk(APT_wholeSale_Obj.APT_wholeSale.voipProtocol_Dropdown, "VOIP Protocol");
    					waitForAjax();
    					Reporter.log("The voip protocol displaying is "+ voipProtocol_actualvalue);
    					String voip_code=voipProtocol_code(voipProtocol_actualvalue);
    					
    				//CDR Delivery
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.CDRdelivery_Dropdown, "CDR Delivery", edit_CDRdelivery);
    					waitForAjax();
    					String cdrDelivery_actualvalue=GetTheSelectedValueInsideDropdown_trunk(APT_wholeSale_Obj.APT_wholeSale.CDRdelivery_Dropdown, "CDR Delivery");
    					waitForAjax();
    					String CDR_code=CDRdelivery_code(cdrDelivery_actualvalue);
    				
    				//Prefix
    						 edittextFields_commonMethod(APT_wholeSale_Obj.APT_wholeSale.prefix_textField, editPrefix, "Prefix");
    						 Thread.sleep(1000);

    				//valdate the prefix value for adding into Trunk group Name field	 
    					 String preifxValueInsidetextField=findWebElement(APT_wholeSale_Obj.APT_wholeSale.prefix_textField).getAttribute("value");
    					
    					 int prefixSize=preifxValueInsidetextField.length(); 
    					 
    					 String country=null;
    					 if(edit_BillingCountry.equalsIgnoreCase("null")) {
    						 country=billingCoutry;
    					 }
    					 else {
    						 country=edit_BillingCountry;
    					 }
    					 
    					 if(prefixSize==4) {
    							prefix_code=preifxValueInsidetextField.substring(1);
    						}else if(prefixSize<4) {
    							Report.LogInfo("INFO","Prefix value cannot be less than 4","PASS");
    							ExtentTestManager.getTest().log(LogStatus.FAIL, "Prefix value cannot be less than 4. Value is displaying as "+preifxValueInsidetextField);
    						}else if(prefixSize>4) {
    							prefix_code=preifxValueInsidetextField.substring(1);
    							Report.LogInfo("INFO","Prefix value cannot be greater than 4","PASS");
    							ExtentTestManager.getTest().log(LogStatus.FAIL, "Prefix value cannot be greater than 4. Value is displaying as "+preifxValueInsidetextField);
    						}
    						 
    						 if((country.equals("NL (Netherlands)"))) {
    							 prefix_code = "B" + prefix_code.substring(1);
    						 }else {
    							 Report.LogInfo("INFO","No changes made in prefix code","PASS"); 
    						 }
    					 
    				//Gateway
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.gateway_Dropdown, "Gateway", editGateway);
    					String gateway_actualValue=GetTheSelectedValueInsideDropdown_trunk(APT_wholeSale_Obj.APT_wholeSale.gateway_Dropdown, "Gateway");
    					gatewayCode=gateway_code(gateway_actualValue);
    					
    					WebElement trunk_type=findWebElement(APT_wholeSale_Obj.APT_wholeSale.trunkType_Dropdown);
    					scrollDown(APT_wholeSale_Obj.APT_wholeSale.trunkType_Dropdown);
    					waitForAjax();
    					
    				//Quality
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.quality_Dropdown, "Quality", editQuality);
    					String quality_code=findWebElement(APT_wholeSale_Obj.APT_wholeSale.quality_Dropdown).getAttribute("value");
    					
    				//Trunk Group Name
    					String trunGroup=country_code+gatewayCode+voip_code+trunktype_code+CDR_code+prefix_code+primarytrunk+quality_code;
    					
    					int totalLen=trunGroup.length();
    					Reporter.log("Total lenth of 'Trunk Group' field is "+ totalLen);
    					Reporter.log("----------------Trunk group name is "+ trunGroup);
    					if(totalLen==13) {
    						verifyExists(APT_wholeSale_Obj.APT_wholeSale.trunkGroupName_TextField,"trunkGroupName_TextField");
    						primarytrunkGroupname=trunGroup;
    					}else {
    						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Trunk Group NAme' length is: "+totalLen);
    						verifyExists(APT_wholeSale_Obj.APT_wholeSale.trunkGroupName_TextField, "Trunk Group Name");
    					}
    					
    				//Traffic Directions 
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown, "Traffic Directions", editTrafficDirection);
    					
    				//IP Address Type
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.IPaddresstype_Dropdown, "IP Address Type", edit_IpAddressType);
    				
    					WebElement traficDirection=findWebElement(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown);
    					scrollDown(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown);
    					waitForAjax();
    					
    				//Carrier IP originating
    					sendKeys(APT_wholeSale_Obj.APT_wholeSale.carrierIPoriginating_textField, editCarrierIPoriginating,"Carrier IP Originating (Address/Mask)");
//    					click_commonMethod(application, ">>", "carrierIPoriginating_addButtton", xml);
    					click(APT_wholeSale_Obj.APT_wholeSale.carrierIPoriginating_addButtton,"carrierIPoriginating_addButtton");
    					GetTheValuesInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.carrierIPOriginating_addedValue_selectDropdownField, "Carrier IP Originating (Address/Mask)");
    					
    					
    				//Carrier IP Terminating
    					sendKeys(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_textField, editCarrierIPterminating, "Carrier IP Terminating(Address)");
//    					click_commonMethod(application, ">>", "carrierIPterminating_addButton", xml);
    					click(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_addButton,"carrierIPterminating_addButton");
    					GetTheValuesInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_addedValue_selectDropdownField, "Carrier IP Terminating (Address)");
    				
    				//SIP Signalling Port
    				  if(edit_VOIPprotocol.equalsIgnoreCase("SIP")) {
    					  sendKeys( APT_wholeSale_Obj.APT_wholeSale.SIPsignallingport_textField, editSIPsignallingPort, "SIP Signaling Port");
    							
    				  }else {
    					  ExtentTestManager.getTest().log(LogStatus.PASS, "'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-1'");
    					  Report.LogInfo("INFO","'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-I'","PASS");
    				  }
    				  
    				
    				//Internet Based Customer
    				  editcheckbox_commonMethod(edit_internetBasedCustomer, APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox, "Internet Based Customer");
    				  
    			    
    				//VLAN Tag
    				  sendKeys(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField, edit_vlantag, "VLAN Tag");
    				  String vlan_actualvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField).getAttribute("value");
    				  
    				  
    				//Sub Interface Slot
    				  selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.subInterfaceSlot_Dropdown, "Sub Interface Slot", edit_subInterfaceSlot);
    				  waitForAjax();
    				  String subIntercaeSlot_actualValue=GetTheSelectedValueInsideDropdown_trunk(APT_wholeSale_Obj.APT_wholeSale.subInterfaceSlot_Dropdown, "Sub Interface Slot");
    				  
    					 
    				if(!gateway_actualValue.contains("SBC")) {
    					
    					if(subIntercaeSlot_actualValue.equalsIgnoreCase("null")) {
    						
    						//Sub Interface Name
    						String SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlan_actualvalue;
    						verifyExists( APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField, "Sub Interface Name");
    						
    						//NIF Group  
    						String NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlan_actualvalue+NIGgroupdefaultValue_last;
    						verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField, "NIF Group");
    					}
    					else if(!subIntercaeSlot_actualValue.equalsIgnoreCase("null")) {
    						
    						//Sub Interface Name
    						String SubInterfaceName=subInterfacename_starting+subIntercaeSlot_actualValue+subInterfacename_middle+vlan_actualvalue;
    						verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField, "Sub Interface Name");
    						
    						//NIF Group
    						String NIFgroup=NIFgroupDEfaultValue_starting+subIntercaeSlot_actualValue+NIFgroupDEfaultValue_middle+vlan_actualvalue+NIGgroupdefaultValue_last;
    						Report.LogInfo("INFO","NIF Group value is: "+NIFgroup,"PASS");
    						verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField, "NIF Group");
    					}
    				}
    				else if(gateway_actualValue.contains("SBC")) {
    					

    					//IP Interface
    					  String ipInterface = ipInterfaceDEfaultValue +vlan_actualvalue;
    					  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterface_textField,"IP Interface");  
    					  
    				//Address Context
    					  String addressContext=addressContextDefaultValue+vlan_actualvalue;
    					  verifyExists(APT_wholeSale_Obj.APT_wholeSale.AddressContext_textField,"Address Context");  //verify default values
    					
    					  
    				//IP Interface Group
    					  String ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlan_actualvalue;
    					  verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterfaceGroup_textField,"IP Interface Group");    //verify default values
    			 
    				}

    				
    				//Signalling Zone
    				  sendKeys(APT_wholeSale_Obj.APT_wholeSale.signallingZone_textField, edit_signallingZone, "Signalling Zone");
    				  
    				  
    				//Signalling Transport Protocol
    				  	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.signallingTransportProtocol_Dropdown, "Signalling Transport Protocol", editSignallingTransport);
    					
    					WebElement internetBasedCusotmerView=findWebElement(APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox);
    					scrollDown(APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox);
    					waitForAjax();
    					
    					String signaltransport_actualvalue=GetTheSelectedValueInsideDropdown_trunk(APT_wholeSale_Obj.APT_wholeSale.signallingTransportProtocol_Dropdown, "Signalling Transport Protocol");
    					if(signaltransport_actualvalue.equalsIgnoreCase("sip-tls-tcp")) {
    						
    						//TLS Profile
    						sendKeys( APT_wholeSale_Obj.APT_wholeSale.TLS_textField, edit_TLSproflile,"TLS Profile");
    						
    						//SRTP
    						editcheckbox_commonMethod(edit_SRTP, APT_wholeSale_Obj.APT_wholeSale.srtp_checkbox, "SRTP");
    					}
    					
    				//Colt Signalling IP
    					sendKeys( APT_wholeSale_Obj.APT_wholeSale.coltSignalingIP_textField, edit_coltSignalIP, "Colt Signaling IP");
    					
    				//Media IP
    					sendKeys(APT_wholeSale_Obj.APT_wholeSale.mediaIP_textField, edit_mediaIP,  "Media IP");
    					
    				//Reuse NIF Group
    					if(gateway_actualValue.contains("SBC")) {
    						Report.LogInfo("INFO","Reuse NIF Group checkbox will not display, if gateway contains 'SBC'","PASS");
    					}
    					else{
    						editcheckbox_commonMethod(edit_reuseNIFgroup, APT_wholeSale_Obj.APT_wholeSale.reuseNIFgroup_checkbox, "Reuse NIF Group");
    					}
    					
    					
    				//Reuse Sig Zone/Part
    					editcheckbox_commonMethod(edit_reuseSigZonePart, APT_wholeSale_Obj.APT_wholeSale.reuseSigZonePart_checkbox, "Reuse Sig Zone/Part");
    					
    					
    					//Call Admission Control
    					editcheckbox_commonMethod(edit_callAdmissionControl, APT_wholeSale_Obj.APT_wholeSale.callAdmissionControl_checkbox, "Call Admission Control");
    					
    					boolean calladmisssion_actualvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.callAdmissionControl_checkbox).isSelected();
    					
    					if(calladmisssion_actualvalue) {
    						
    						//Call Limit
    						selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.callLimit_Dropdown, "Call Limit", edit_callLimit);
    						String callimit_actualvalue=GetTheSelectedValueInsideDropdown_trunk(APT_wholeSale_Obj.APT_wholeSale.callLimit_Dropdown, "CAll Limit");
    						
    						if(callimit_actualvalue.equalsIgnoreCase("Defined")) {
    							sendKeys(APT_wholeSale_Obj.APT_wholeSale.limitNumber_textField, edit_limitNumber, "Limit Number");
    						}
    					}
    					
    				//Call Rate Limit
    					editcheckbox_commonMethod(edit_callrateLimit, APT_wholeSale_Obj.APT_wholeSale.callrateLimit_checkbox, "Call Rate Limit");
    					boolean callratelimit_actualvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.callrateLimit_checkbox).isSelected();
    					
    					if(callratelimit_actualvalue) {
    						
    						String callratelimitactualvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.callRateLimitt_textField).getAttribute("value");
    						Report.LogInfo("INFO"," 'Call rate Limit' value is displaying as "+callratelimitactualvalue,"PASS");
    						ExtentTestManager.getTest().log(LogStatus.PASS, " 'Call rate Limit' value is displaying as "+callratelimitactualvalue);
    					
    						if(!edit_callrateLimitvalue.equalsIgnoreCase("null")) {
    							int i=Integer.parseInt(edit_callrateLimitvalue);
    								
    							if(i>100) {
    								ExtentTestManager.getTest().log(LogStatus.FAIL, "The CallRateLimit should be less 100 for all Trunks");
    							}
    							else if(i<=100){
    								edittextFields_commonMethod(APT_wholeSale_Obj.APT_wholeSale.callRateLimitt_textField, edit_callrateLimitvalue, "Call rate Limit");
    							}
    						}else {
    							ExtentTestManager.getTest().log(LogStatus.PASS, "'Call rate Limit' value is not edited");
    							Report.LogInfo("INFO","'Call rate Limit' value is not edited","PASS");
    						}
    					}
    					
    					
    				//Source Address Filtering
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.sourceAddressFiltering_Dropdown, "Source Address Filtering", edit_sourceAddressFiltering);
    					
    				//100rel Support
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.relsupport_Dropdown, "100rel Support", edit_relSupport);
    					
    				//SIP Session Keepalive Timer(Sec)
    					sendKeys(APT_wholeSale_Obj.APT_wholeSale.sipSessionKeepAliverTimer_textField, edit_sipSessionkeepAliveTimer, "SIP Session Keepalive Timer(Sec)");
    				
    				//Retry Invite
    					sendKeys(APT_wholeSale_Obj.APT_wholeSale.retryinvite_textField, edit_retryInvite, "Retry Invite");
    					
    				//Text message under "Retry Invite" field
    					//methodToFindMessagesUnderTextField(application, APT_wholeSale_Obj.APT_wholeSale.defaultTextMessageUnderretryInvite, "Retry Invite", "Default Retry Invite :2");
    					
    				//Route Priority
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.routepriority_Dropdown, "Route Priority", edit_routePriority);
    					
    				//Address Reachability
    					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.addressReachability_Dropdown, "Address Reachability", edit_addressReachability);
    					
    					WebElement retryinviteView=findWebElement(APT_wholeSale_Obj.APT_wholeSale.retryinvite_textField);
    					scrollDown(APT_wholeSale_Obj.APT_wholeSale.retryinvite_textField);
    					waitForAjax();
    				
    				//E164GlobalProfile
    					editTrunk_existingNewDropdownField(editglobalProfile_ExistingSelection, editglobalProfile_newSelection, editGlobalProfile_ExistingValue, 
    							editGlobalProfile_newValue, APT_wholeSale_Obj.APT_wholeSale.globalProfile_Dropdown , APT_wholeSale_Obj.APT_wholeSale.globalProfile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.globalProfile_textField, "E164GlobalProfile");
    					
    				//E164LocalProfile	
    					editTrunk_existingNewDropdownField( editLocalProfile_existingFieldSelection, editLocalProfile_newFieldSelection, editLocalProfile_existingvalue,
    							editLocalProfile_newvalue, APT_wholeSale_Obj.APT_wholeSale.localProfile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.localProfile_checkbox, APT_wholeSale_Obj.APT_wholeSale.localProfile_TextField, "E164LocalProfile");
    					
    					 
    				//COS profile
    					editTrunk_existingNewDropdownField( editCOSprofile_existingFieldSelection, editCOSprofile_newFieldSelection, editCOSprofile_existingValue,
    							editCOSprofile_newValue, APT_wholeSale_Obj.APT_wholeSale.COSprofile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.COSprofile_checkbox, APT_wholeSale_Obj.APT_wholeSale.COSprofile_TextField, "COS Profile");
    					
    								
    				//PSPG Name
    					editTrunk_existingNewDropdownField(editPSPGname_existingFieldSelection, editPSPGname_newFieldSelection, editpspgName_existingValue,
    							editpspgName_newValue, APT_wholeSale_Obj.APT_wholeSale.PSPSGname_Dropdown, APT_wholeSale_Obj.APT_wholeSale.PSPGname_Checkbox, APT_wholeSale_Obj.APT_wholeSale.PSPGname_TextField, "PSPG Name");
    					
    					
    				//Preferred  PSP
    					editTrunk_existingNewDropdownField(editPrefferedPSP_existingFieldSelection, editPrefferedPSP_newFieldSelection, editPreferredPSP_exitingvalue,
    							editPreferredPSP_newvalue, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_Dropdown, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_checkbox, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_TextField, "Preferred PSP");
    					
    					
    					
    					WebElement globalProfileView=findWebElement(APT_wholeSale_Obj.APT_wholeSale.globalProfile_textField);
    					scrollDown(APT_wholeSale_Obj.APT_wholeSale.globalProfile_textField);
    					waitForAjax();
    					
    				//Carrier
    					editTrunk_existingNewDropdownField( editCarrier_existingFieldSelection, editCarrier_newFieldSelection, editCarrier_existingValue,
    							editCarrier_newValue, APT_wholeSale_Obj.APT_wholeSale.carriers_Dropdown, APT_wholeSale_Obj.APT_wholeSale.carriers_checkbox, APT_wholeSale_Obj.APT_wholeSale.carriers_TextField, "Carrier");
    					
    				
    				//IP Signalling Profile
    					editTrunk_existingNewDropdownField(editIPsignalProfile_existingFieldSelection, editIPsignalProfile_newFieldSelection, editIPsignalProfile_existingValue,
    							editIPsignalProfile_newValue, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_textField, "IP Signaling Profile");
    					
    		
    				//Egress IP Signaling Profile
    					editTrunk_existingNewDropdownField(editEgressIpsignal_existingFieldSelection, editEgressIpsignal_newFieldSelection, editEgressIPsignal_existingValue,
    							editEgressIPsignal_newValue, APT_wholeSale_Obj.APT_wholeSale.EgressIpsignal_Dropdown, APT_wholeSale_Obj.APT_wholeSale.EgressIPsignal_checkbox, APT_wholeSale_Obj.APT_wholeSale.EgressipSignal_TextField, "Egress IP Signaling Profile");
    					
    					((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
    					waitForAjax();
    					
    				
    				//In DM/PM rule
    					editTrunk_existingNewDropdownField(editInDMPMrule_existingFieldSelection, editInDMPMrule_newFieldSelection, editInDMPMrule_existingValue,
    							editInDMPMrule_newValue,  APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_Dropdown, APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_checkbox, APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_TextField, "In DM/PM rule");
    					
    					
    				//Out DM/PM rule
    					editTrunk_existingNewDropdownField( editOutDMPMrule_existingFieldSelection, editOutDMPMrule_newFieldSelection, editOutDMPMrule_existingValue,
    							editOutDMPMrule_newValue, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_Dropdown, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_checkbox, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_TextField, "Out DM/PM rule");
    					
    			
    				//Feature Control Profile	
    					editTrunk_existingNewDropdownField(editFeatureControlprofile_existingFieldSelection, editFeatureControlprofile_newFieldSelection, editFeatureControlprofile_existingValue,
    							editFeatureControlprofile_newValue, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_TextField, "Feature Control Profile");
    					
    					
    				//Local Ring Back Tone
    					editTrunk_existingNewDropdownField(editLocalRingBackTone_existingFieldSelection, editLocalRingBackTone_newFieldSelection, editLocalRingBackTone_existingValue,
    							editLocalRingBackTone_newValue, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_Dropdown, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_checkbox, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_TextField, "Local Ring Back Tone");
    				
    					
    				//Create Lower Case Routes
    					editcheckbox_commonMethod(editCreateLowerCaseRoutervalue, APT_wholeSale_Obj.APT_wholeSale.createLowerCaseRoute_checkbox, "Create Lower Case Routes");
    					
    					
    				//PSX Manual Configuration	
    					editcheckbox_commonMethod(edit_PSXmanualConfigvalue, APT_wholeSale_Obj.APT_wholeSale.PSXmanualConfig_checkbox, "PSX Manual Configuration");
    					
    				//Manual Configuration On GSX
    					
    			
    					if(gateway_actualValue.contains("SBC")) {
    						//Manual Configuration on SBC
    						editcheckbox_commonMethod(edit_SBCmanualconfigValue, APT_wholeSale_Obj.APT_wholeSale.SBCmanualconfig_checkbox, "Manual Configuration On SBC");
    					}else {
    						//Manual Configuration On GSX
    						editcheckbox_commonMethod(edit_GSXmanualConfigvalue, APT_wholeSale_Obj.APT_wholeSale.GSXmanualConfig_checkbox, "Manual Configuration On GSX");
    					}
    					
    					
    					((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
    					waitForAjax();
    					
    					click_commonMethod(APT_wholeSale_Obj.APT_wholeSale.OKbutton, "OK");
    					
    		//			
    						
    					
    		}
    	 
public String trunkType_code(String trunkType) {
			
			String code=null;
			
			if(trunkType.equalsIgnoreCase("Number Hosting")) {
				code="N";
			}
			
			else if(trunkType.equalsIgnoreCase("Carrier VoIP")) {
				code="I";
			}
			
			else if(trunkType.equalsIgnoreCase("IN over Carrier VoIP")) {
				code="D";
			}
			
			else if(trunkType.equalsIgnoreCase("National Carrier VoIP")) {
				code="National Carrier VoIP";
			}
			
			return code;
		}

public String GetTheSelectedValueInsideDropdown_trunk(String locator, String labelname) throws IOException, InterruptedException
{ //Thread.sleep(3000);
	WebElement el = findWebElement(locator);
	String selectedValue=null;
	
try {	
	//WebElement el=getwebelement(xml.getlocator("//locators/" + application + "/"+ xpath +""));
	
	Select s1=new Select(el);
	WebElement option = s1.getFirstSelectedOption();
//	Log.info(option);
	selectedValue = option.getText();	
	
	
}catch(Exception e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
	Report.LogInfo("INFO",labelname + " field is not displaying","PASS");
}
	
	return selectedValue;

}

public String voipProtocol_code( String voipProtocol) {
	
	String code1=null;
	
	if(voipProtocol.equalsIgnoreCase("SIP")) {
		code1="S";
	}
	
	else if(voipProtocol.equalsIgnoreCase("SIP-I")) {
		code1="S";
	}
	
	return code1;
}

public String CDRdelivery_code(String cdrDElivery) {
	
	String code_cdr=null;
	
	if(cdrDElivery.equalsIgnoreCase("Delivery to COCOM")) {
		code_cdr="C";
	}
	
	else if(cdrDElivery.equalsIgnoreCase("No delivery to COCOM")) {
		code_cdr="0";
	}
	
	else if(cdrDElivery.equalsIgnoreCase("IN Tremination")) {
		code_cdr="I";
	}
	
	return code_cdr;
}
   
public String gateway_code(String gateway) {
	
	String code=null;
	
	if(gateway.equals("DEVGSX1")) {
		code="DEV";
	}
	
	else if(gateway.equals("DEVSBC1")) {
		code="DEA";
	}
	
	else if(gateway.equals("DEVSBC2")) {
		code="DEB";		
	}
			
	else if(gateway.equals("OPSGSX1")) {
		code="OPS";
	}
			
	else if(gateway.equals("MILGSX1")) {
		code="MIA";
	}
			
	else if(gateway.equals("ZRHNBS1")) {
		code="ZHA";
	}
			
	else if(gateway.equals("PARNBS1")) {
		code="PSA";
	}
			
	else if(gateway.equals("LONNBS1")) {
		code="LNA";
	}
			
	else if(gateway.equals("FRANBS1")) {
		code="FTA";
	}
			
	else if(gateway.equals("MADGSX1")) {
		code="MDA";
	}
			
	else if(gateway.equals("BHXNBS1")) {
		code="BHA";
	}
			
	else if(gateway.equals("PARGSX2")) {
		code="PSB";
	}
			
	else if(gateway.equals("FRAGSX2")) {
		code="FTB";
	}
			
	else if(gateway.equals("PARGSX3")) {
		code="PSC";
	}
			
	else if(gateway.equals("FRASBC1")) {
		code="FTC";
	}
			
	else if(gateway.equals("PARSBC1")) {
		code="PSD";
	}
			
	else if(gateway.equals("ZRHGSX2")) {
		code="ZRB";
	}
			
	else if(gateway.equals("FRASBC2")) {
		code="FTD";
	}
			
	else if(gateway.equals("PARSBC2")) {
		code="PSE";
	}
			
	else if(gateway.equals("FRASBC3")) {
		code="FTF";
	}
			
	else if(gateway.equals("PARSBC3")) {
		code="PSF";
	}
	
	return code;
}

public void editTrunk_existingNewDropdownField(String existingFieldSelection, String newFieldSelection, String existinFieldvalue,
		String newFieldvalue,String xpath_dropdown,String xpath_checkbox, String xpath_textfield, String labelname ) throws IOException, InterruptedException {
	
	if((existingFieldSelection.equalsIgnoreCase("yes")) && (newFieldSelection.equalsIgnoreCase("no"))) {
		
		editTrunk_selectExistingOrNewDropdown(xpath_dropdown, xpath_checkbox, existinFieldvalue, labelname);
		
	}
	else if((existingFieldSelection.equalsIgnoreCase("no")) && (newFieldSelection.equalsIgnoreCase("yes"))) {
		
		editTrunk_selectExistingOrNewTextField(xpath_textfield, xpath_checkbox, labelname, newFieldvalue);
		
	}
	
	else if((existingFieldSelection.equalsIgnoreCase("null")) && (newFieldSelection.equalsIgnoreCase("null"))) {
		
		ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " field is not edited");
	}
	
}

public void editTrunk_selectExistingOrNewDropdown(String xpath_dropdown, String xpath_checkbox, String existinFieldvalue, String labelname) throws InterruptedException, IOException {
	
	boolean fieldEnabled=false;
	
try {	
//	fieldEnabled=getwebelement(xml.getlocator("//locators/" + application + "/"+ xpath_dropdown +"")).isEnabled();
	
	if(isVisible(xpath_dropdown)) {
		
		selectValueInsideDropdown(xpath_dropdown, labelname, existinFieldvalue);
		
	}else {
		
		click_commonMethod(labelname, xpath_checkbox);
		Thread.sleep(1000);
		
		selectValueInsideDropdown( xpath_dropdown, labelname, existinFieldvalue);
		
	}
}catch(Exception e) {
	e.printStackTrace();
	
	ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
	Report.LogInfo("INFO",labelname + " field is not displaying","PASS");
}
}
public void click_commonMethod(String labelname,String locator) throws InterruptedException {
	
	
	try {
		Thread.sleep(1000);
		WebElement element = findWebElement(locator);
		System.out.println(element);
		if(element==null)
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step:  '"+labelname+"' not found");
		}
		else {
			element.click();	
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Clicked on '"+labelname+"' button");
		}

	} catch (Exception e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Step: Clicking on '"+labelname+"' button is unsuccessful");
		e.printStackTrace();
	}
}
public void editTrunk_selectExistingOrNewTextField(String xpath_textfield, String xpath_checkbox, String labelname, String textFieldValue) {
	
	boolean fieldEnabled=false;
	
	try {	
		//fieldEnabled=getwebelement(xml.getlocator("//locators/" + application + "/"+ xpath_textfield +"")).isEnabled();
		
		if(isVisible(xpath_textfield)) {
			
			sendKeys( xpath_textfield, textFieldValue);
			
		}else {
			
			click_commonMethod(labelname, xpath_checkbox);
			waitForAjax();
			
			sendKeys(xpath_textfield, textFieldValue);
			
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
		Report.LogInfo("INFO",labelname + " field is not displaying","PASS");
		
	}
	
}


public void testStatus() throws IOException , InterruptedException{

	
	waitForAjax();
	String element = null;
	String status = null;

	List<WebElement> testlist=webDriver.findElements(By.xpath("//tbody/tr"));
	int listSize = testlist.size();

	for (int i = 1; i <= listSize; i++) {
		try {
			element=webDriver.findElement(By.xpath("(//tbody/tr["+ i +"]/td)[1]")).getText();

			if (element.isEmpty()) {

			} else {
				Reporter.log("Test Name is displaying as: " + element);

				status=webDriver.findElement(By.xpath("(//tbody/tr["+ i +"]/td)[2]/div")).getAttribute("class");
				Reporter.log("status displays as: " + status);

				if (status.contains("red")) {
					Reporter.log(element + " status colour dipslays as: red");
				} else if (status.contains("green")) {
					Reporter.log(element + " status colour dipslays as: green");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

public void verifyPEdevice(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {

	String	PE_IMSPOPLocation= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_IMSPOPLocation");

	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Add PE Device' Functionality");
	Report.LogInfo("INFO","Verifying 'Add PE Device' Functionality","PASS");
	
	WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
	
	
	click(APT_wholeSale_Obj.APT_wholeSale.addPEdevice_link, "Add PE device link");  //click on PE device link
	
	verifyExists( APT_wholeSale_Obj.APT_wholeSale.PE_addPEdeviceHeaderName, "Add PE device");    //compare PE device Header name

	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton,  "OK");
	
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.PEdevice_IMSlocation_warningmessage, "IMS POP Location");
	
	addDropdownValues_commonMethod("IMS POP Location", APT_wholeSale_Obj.APT_wholeSale.AddmasSWitch_IMSppswitch_dropdown, PE_IMSPOPLocation);
	
	click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
	waitForAjax();
	

	Report.LogInfo("INFO","------ MAS Switch added successfully ------","PASS");
}





public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
{
	String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
	String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
	String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
	String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
	String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
	String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalContactName");
	String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
	String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
	String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

	
		//ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Customer Creation Functionality");

	
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.ManageCustomerServiceLink," Manage Customer Service Link");
			click(APT_wholeSale_Obj.APT_wholeSale.ManageCustomerServiceLink,"ManageCustomerServiceLink");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.createcustomerlink," create customer link");
			click(APT_wholeSale_Obj.APT_wholeSale.createcustomerlink,"Create Customer Link");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.createcustomer_header,"create customer page header");
			
			//scrolltoend();
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.okbutton,"ok");
			click(APT_wholeSale_Obj.APT_wholeSale.okbutton,"ok");
			
			//Warning msg check
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.customernamewarngmsg,"Legal Customer Name");
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.countrywarngmsg,"Country");
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.ocnwarngmsg,"OCN");
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.typewarngmsg,"Type");
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.emailwarngmsg,"Email");
		
			//Create customer by providing all info
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.nametextfield,"name");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.nametextfield,name,"name");
	
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.maindomaintextfield,"Main Domain");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.maindomaintextfield,maindomain,"Main Domain");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.country,"country");
		//	sendKeys(APTIPAccessNoCPE.APTNoCPE.country,country,"country");
			addDropdownValues_commonMethod("Country",APT_wholeSale_Obj.APT_wholeSale.country,country);
		
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.ocntextfield,"ocn");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.ocntextfield,ocn,"ocn");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.referencetextfield,"reference");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.referencetextfield,reference,"reference");
		
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.technicalcontactnametextfield,"technical contact name");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.technicalcontactnametextfield,technicalcontactname,"technical contact name");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.typedropdown,"technical contact name");
			//sendKeys(APTIPAccessNoCPE.APTNoCPE.typedropdown,type,"technical contact name");
			addDropdownValues_commonMethod("Type",APT_wholeSale_Obj.APT_wholeSale.type,type);
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.emailtextfield,"email");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.emailtextfield,email,"email");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.phonetextfield,"phone text field");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.phonetextfield,phone,"phone text field");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.faxtextfield,"fax text field");
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.faxtextfield,fax,"fax text field");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.okbutton,"ok");
			click(APT_wholeSale_Obj.APT_wholeSale.okbutton,"ok");
			
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.customercreationsuccessmsg,"Customer successfully created.");
			
		}
		
		
public void selectCustomertocreateOrder(String ChooseCustomerToBeSelected)
		throws InterruptedException, IOException {
	

	mouseMoveOn(Lanlink_DirectFiberObj.DirectFiber.ManageCustomerServiceLink);

	verifyExists(Lanlink_DirectFiberObj.DirectFiber.CreateOrderServiceLink, "Create Order Service Link");
	click(Lanlink_DirectFiberObj.DirectFiber.CreateOrderServiceLink, "Create Order Service Link");

	// verifyExists(APT_wholeSale_Obj.APT_wholeSale.nextbutton,"Next button");
	// click(APT_wholeSale_Obj.APT_wholeSale.nextbutton,"Next button");

	verifyExists(Lanlink_DirectFiberObj.DirectFiber.entercustomernamefield, "name");
	sendKeys(Lanlink_DirectFiberObj.DirectFiber.entercustomernamefield, ChooseCustomerToBeSelected, "name");

	// verifyExists(APT_wholeSale_Obj.APT_wholeSale.entercustomernamefield,"name");
	// sendKeys(APT_wholeSale_Obj.APT_wholeSale.entercustomernamefield,"*","name");

	waitForAjax();

	addDropdownValues_commonMethod("Choose a customer",Lanlink_DirectFiberObj.DirectFiber.chooseCustomerdropdown, ChooseCustomerToBeSelected);
	// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

	verifyExists(Lanlink_DirectFiberObj.DirectFiber.nextbutton, "Next button");
	click(Lanlink_DirectFiberObj.DirectFiber.nextbutton, "Next button");

}	

public void createorderservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderService");
	String newordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
	String newrfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
	String existingorderservice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingOrderService");
	String existingordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingOrderNumber");

	verifyExists(APT_wholeSale_Obj.APT_wholeSale.nextbutton, "Next button");
	click(APT_wholeSale_Obj.APT_wholeSale.nextbutton, "Next button");

	//verifyExists(APT_wholeSale_Obj.APT_wholeSale.order_contractnumber_warngmsg, "order contract number");
	//verifyExists(APT_wholeSale_Obj.APT_wholeSale.servicetype_warngmsg, "service type");

	if (neworder.equalsIgnoreCase("YES")) {
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.newordertextfield, "new order textfield");
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.newordertextfield, newordernumber, "newordertextfield");

		verifyExists(APT_wholeSale_Obj.APT_wholeSale.newrfireqtextfield, "new rfireq textfield");
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.newrfireqtextfield, newrfireqno, "new rfireq textfield");

		verifyExists(APT_wholeSale_Obj.APT_wholeSale.createorderbutton, "create order button");
		click(APT_wholeSale_Obj.APT_wholeSale.createorderbutton, "create order button");

		verifyExists(APT_wholeSale_Obj.APT_wholeSale.OrderCreatedSuccessMsg, "Order Created Success Msg");
		
		waitForAjax();

	} else if (existingorderservice.equalsIgnoreCase("YES")) {
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.selectorderswitch, "select order switch");
		click(APT_wholeSale_Obj.APT_wholeSale.selectorderswitch, "select order switch");

		verifyExists(APT_wholeSale_Obj.APT_wholeSale.existingorderdropdown, "existing order drop down");

		click(APT_wholeSale_Obj.APT_wholeSale.existingorderdropdown, "existing order dropdown");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,existingordernumber,"existingordernumber");

		addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
				APT_wholeSale_Obj.APT_wholeSale.existingorderdropdown, existingordernumber);
		waitForAjax();

	} else {
		Reporter.log("Order not selected");
	}

}

public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

{
	String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editOrderSelection");
	String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_OrderNumber");
	String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_VoicelineNumber");

	scrollDown(APT_wholeSale_Obj.APT_wholeSale.userspanel_header);
	waitForAjax();
	if(editOrderSelection.equalsIgnoreCase("no"))
	{
		Reporter.log("Edit Order is not performed");
    }
	else if(editOrderSelection.equalsIgnoreCase("Yes"))
	{
		Reporter.log("Performing Edit Order Functionality");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"Action dropdown");
		click(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"Action dropdown");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editorderlink,"Edit Order");
		click(APT_wholeSale_Obj.APT_wholeSale.editorderlink,"Edit Order");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editorderheader,"Edit Order");

		String EditOrderNo=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.editorderno,"editorderno");
		click(APT_wholeSale_Obj.APT_wholeSale.editorderno,"edit order no");
		clearTextBox(APT_wholeSale_Obj.APT_wholeSale.editorderno);
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editorderno,"Order Number");
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.editorderno,editorderno,"Order Number");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editvoicelineno,"edit voice line no");
		clearTextBox(APT_wholeSale_Obj.APT_wholeSale.editvoicelineno);
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.editvoicelineno,editvoicelineno,"Order Number");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.cancelbutton,"cancel button");
		click(APT_wholeSale_Obj.APT_wholeSale.cancelbutton,"cancel button");
	
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"Action dropdown");
		click(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"Action dropdown");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editorderlink,"Edit Order");
		click(APT_wholeSale_Obj.APT_wholeSale.editorderlink,"Edit Order");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editorderheader,"Edit Order Header");
		String editOrderHeader=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.editorderheader,"Edit Order Header");
		String EditOrder= "Edit Order";
		editOrderHeader.equalsIgnoreCase(EditOrder);
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editorderno,"edit order no");
		click(APT_wholeSale_Obj.APT_wholeSale.editorderno,"edit order no");
		clearTextBox(APT_wholeSale_Obj.APT_wholeSale.editorderno);
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.editorderno,editorderno,"Order Number");

		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editvoicelineno,"edit voice line no");
		clearTextBox(APT_wholeSale_Obj.APT_wholeSale.editvoicelineno);
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.editvoicelineno,editvoicelineno,"Order Number");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.editorder_okbutton,"OK Button");
		click(APT_wholeSale_Obj.APT_wholeSale.editorder_okbutton,"OK Button");
		
		if(editorderno.equalsIgnoreCase("Null"))
		{
			Reporter.log("Order/Contract Number (Parent SID) field is not edited");
		}
		else 
		{
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.ordernumbervalue,"order number value");
			String editOrderno=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.ordernumbervalue,"order number value");
			editOrderno.equalsIgnoreCase(editorderno);
		}
		
		if(editvoicelineno.equalsIgnoreCase("Null"))
		{
			Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
		}
		else
		{
			verifyExists(APT_wholeSale_Obj.APT_wholeSale.ordervoicelinenumbervalue,"order voice line number value");
			String editVoicelineno=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.ordervoicelinenumbervalue,"order voice line number value");
			editVoicelineno.equalsIgnoreCase(editvoicelineno);
		}
		Reporter.log("Edit Order is successful");	

	}

  }

public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

{

	String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_existingOrder");
	String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_newOrderNumber");
	String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_VoicelineNumber");
	String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_existingOrderNumber");

	if((changeOrderSelection_newOrder.equalsIgnoreCase("No")) && (changeOrderSelection_existingOrder.equalsIgnoreCase("No")))
	{
		Report.LogInfo("INFO","Change Order is not performed","PASS");	

	}
	else if(changeOrderSelection_newOrder.equalsIgnoreCase("Yes"))
	{
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"Action dropdown");
		click(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"Action dropdown");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorderlink,"change order link");
		click(APT_wholeSale_Obj.APT_wholeSale.changeorderlink,"change order link");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorderheader,"change order header");
		String changeOrderHeader=getTextFrom(APT_wholeSale_Obj.APT_wholeSale.changeorderheader,"change order header");
		String changeOrder = "Change Order";
		changeOrderHeader.equalsIgnoreCase(changeOrder);
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorder_selectorderswitch,"select order switch");
		click(APT_wholeSale_Obj.APT_wholeSale.changeorder_selectorderswitch,"select order switch");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeordernumber,"change order number");
		click(APT_wholeSale_Obj.APT_wholeSale.changeordernumber,"change order number");
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.editorderno,ChangeOrder_newOrderNumber);
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeordervoicelinenumber,"change order voiceline number");
		click(APT_wholeSale_Obj.APT_wholeSale.changeordervoicelinenumber,"change order voiceline number");
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.changeordervoicelinenumber,changevoicelineno);
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.createorder_button,"create order button");
		click(APT_wholeSale_Obj.APT_wholeSale.createorder_button,"create order button");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorder_okbutton,"change order ok button");
		click(APT_wholeSale_Obj.APT_wholeSale.changeorder_okbutton,"change order ok button");
				
		
		Reporter.log("Change Order is successful");	
	}
	else if(changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) 
	{
		Reporter.log("Performing Change Order functionality");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"order action button");
		click(APT_wholeSale_Obj.APT_wholeSale.orderactionbutton,"order action button");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorderlink,"change order link");
		click(APT_wholeSale_Obj.APT_wholeSale.changeorderlink,"change order link");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorderheader,"change order");
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorder_chooseorderdropdown,"Order/Contract Number (Parent SID)");
		addDropdownValues_commonMethod("Order/Contract Number (Parent SID)",APT_wholeSale_Obj.APT_wholeSale.changeorder_chooseorderdropdown,ChangeOrder_existingOrderNumber);
		
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.changeorder_okbutton,"change order ok button");
		click(APT_wholeSale_Obj.APT_wholeSale.changeorder_okbutton,"change order ok button");
		

		Reporter.log("Change Order is successful");

	}
	
}

public void dump_viewServicepage() throws IOException 
{

//String orderPanel= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.viewServicepage_OrderPanel);
scrollDown(APT_wholeSale_Obj.APT_wholeSale.OrderPanel);
waitForAjax();

click(APT_wholeSale_Obj.APT_wholeSale.Editservice_actiondropdown,"Action");
waitForAjax();
click(APT_wholeSale_Obj.APT_wholeSale.Editservice_Dumplink, "Dump");
waitForAjax();	   

waitToPageLoad();

String dumpelement= getTextFrom(APT_wholeSale_Obj.APT_wholeSale.dumpPage_header,"dumpPage_header");

String dumpvalue=dumpelement;

if(dumpvalue.isEmpty()) {

Reporter.log("NO values dipslaying under 'Dump' page");

}else{
Reporter.log("Dump value is displaying as:   "+ dumpvalue);
}
click(APT_wholeSale_Obj.APT_wholeSale.dump_xButton,"xButton");


//webDriver.navigate().back();
waitForAjax();

}

public void signallingPort( String gateway) throws InterruptedException, IOException {
	
	
	String signallingPort_expectedVaue=signalingport( gateway);
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.signallingPort_textField, "Signaling Port");
}

public String signalingport(String gateway) {
	
	String code=null;
	
	if(gateway.equals("DEVGSX1")) {
		code="543";
	}
	
	else if(gateway.equals("DEVSBC1")) {
		code="1017";
	}
	
	else if(gateway.equals("DEVSBC2")) {
		code="1013";		
	}
			
	else if(gateway.equals("OPSGSX1")) {
		code="509";
	}
			
	else if(gateway.equals("MILGSX1")) {
		code="690";
	}
			
	else if(gateway.equals("ZRHNBS1")) {
		code="1003";
	}
			
	else if(gateway.equals("PARNBS1")) {
		code="889";
	}
			
	else if(gateway.equals("LONNBS1")) {
		code="755";
	}
			
	else if(gateway.equals("FRANBS1")) {
		code="1120";
	}
			
	else if(gateway.equals("MADGSX1")) {
		code="725";
	}
			
	else if(gateway.equals("BHXNBS1")) {
		code="1013";
	}
			
	else if(gateway.equals("PARGSX2")) {
		code="773";
	}
			
	else if(gateway.equals("FRAGSX2")) {
		code="629";
	}
			
	else if(gateway.equals("PARGSX3")) {
		code="556";
	}
			
	else if(gateway.equals("FRASBC1")) {
		code="1023";
	}
			
	else if(gateway.equals("PARSBC1")) {
		code="1005";
	}
			
	else if(gateway.equals("ZRHGSX2")) {
		code="506";
	}
			
	else if(gateway.equals("FRASBC2")) {
		code="1013";
	}
			
	else if(gateway.equals("PARSBC2")) {
		code="1007";
	}
			
	else if(gateway.equals("FRASBC3")) {
		code="1001";
	}
			
	else if(gateway.equals("PARSBC3")) {
		code="1003";
	}
	
	return code;
}


public void addTrunk_existingNewDropdownField(String existingFieldSelection, String newFieldSelection, String existinFieldvalue,
		String newFieldvalue,String xpath_dropdown,String xpath_checkbox, String xpath_textfield, String labelname ) throws IOException, InterruptedException {
	
	if((existingFieldSelection.equalsIgnoreCase("yes")) && (newFieldSelection.equalsIgnoreCase("no"))) {
		
		selectValueInsideDropdown(xpath_dropdown, labelname, existinFieldvalue);
		
	}
	else if((existingFieldSelection.equalsIgnoreCase("no")) && (newFieldSelection.equalsIgnoreCase("yes"))) {
		
		click_commonMethod( labelname, xpath_checkbox);
		waitForAjax();
		
		sendKeys(labelname, xpath_textfield, newFieldvalue);
	}	
}

public void history(String revisionType,String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	
	String gateway= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"gateway"); 
	String voipProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"voipProtocol"); 
	String signallingTransportProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"signallingtransportProtocol"); 

	String voipValue = null;
	
	//Action button	
			//click(APT_wholeSale_Obj.APT_wholeSale.viewPage_ActionButton, "Action");
			
	//click on History link
	scrollUp();
			click( APT_wholeSale_Obj.APT_wholeSale.viewTrunkPage_historyLink, "History");
			
			
			waitforPagetobeenable();
			waitForAjax();
	
	//Revision Type
		compareText("Revision Type" , APT_wholeSale_Obj.APT_wholeSale.historyPanel_revisionTypeColumn, revisionType);
		
	//Last Modified By
		compareText("Last Modified By", APT_wholeSale_Obj.APT_wholeSale.historyPanel_lastModifiedColumn, "colttest@colt.net");
		
	//Trunk Group Name
		//verifyExists("Trunk uGrup Name", APT_wholeSale_Obj.APT_wholeSale.historyPanel_trunkGroupNameColumn);
		
	//Gateway
		compareText("Gateway", APT_wholeSale_Obj.APT_wholeSale.historyPanel_gatewayColumn, gateway);
		
	//VOIP Protocol
		if(voipProtocol.equals("SIP-I")) {
			voipValue = "SI";
		}
		else if(voipProtocol.equals("SIP")) {
			voipValue = "S";
		}
		compareText("VOIP Protocol", APT_wholeSale_Obj.APT_wholeSale.historyPanel_voipProtocolColumn, voipValue);
		
	//Signalling Transport Protocol
		compareText("Signalling Transport Protocol", APT_wholeSale_Obj.APT_wholeSale.historyPanel_signalingTransportprotocol, signallingTransportProtocol);
		
	//Action Column
		click(APT_wholeSale_Obj.APT_wholeSale.historyPanel_viewLink, "View");
		
		waitforPagetobeenable();
		Thread.sleep(1000);
		
	//click on back button
		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitForAjax();
		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		
		click(APT_wholeSale_Obj.APT_wholeSale.trunk_backbutton, "Back");
		
		waitforPagetobeenable();
		waitForAjax();
		
		click(APT_wholeSale_Obj.APT_wholeSale.trunk_backbutton, "Back");
}

public void viewTrunk_Primary(String customerName, String servicename, String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws IOException, InterruptedException {   
	
	
	String trunkType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "trunkType");
	String voipProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "voipProtocol"); 
	String country= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "BillingCountry"); 
	String CDRdelivery= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CDRdelivery");
	String gateway= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "gateway"); 
	String quality= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "quality");
	String trafficDirection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "trafficDirection"); 
	String ipAddresstype= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ipAddresstype");
	String SIPsignallingPort= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SIPsignallingPort"); 
	String internetBasedCustomer= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "internetBasedCustomer"); 
	String vlanTag= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vlanTag"); 
	String subInterfaceSlot= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "subInterfaceSlot");
	String signallngZone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "signallngZone");
	String signallingtransportProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "signallingtransportProtocol"); 
	String coltSignalingIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "coltSignalingIP");
	String mediaIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "mediaIP");
	String reuseNIFgroup= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "reuseNIFgroup");
	String reuseSigZonePart= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "reuseSigZonePart");
	String callAdmissionControl= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "callAdmissionControl");
	String callrateLimitSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "callrateLimitselection"); 
	String sourceAddressFiltering= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "sourceAddressFiltering");
	String relSupport= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String sipSessionkeepAliveTimer= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String retryInvite= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String routePriority= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String addressReachability= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String carrierIPoriginating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String carrierIPterminating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String TLSfield= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String srtp= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String prefix= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String globalProfile_ExistingSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String globalProfile_newSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String globalProfile_ExistingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String globalProfile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String localProfile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String localProfile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String localProfile_existingvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String localProfile_newvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String COSprofile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String COSprofile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String COSprofile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String COSprofile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String PSPGname_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String PSPGname_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String pspgName_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String pspgName_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String prefferedPSP_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String prefferedPSP_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String preferredPSP_exitingvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String preferredPSP_newvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String carrier_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String carrier_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String carrier_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String carrier_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String IPsignallingProfile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String IPsignallingProfile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String IPsignallingProfile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String IPsignallingProfile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String EgressIpsignal_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String EgressIpsignal_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String EgressIPsignal_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String EgressIPsignal_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String InDMPMrule_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String InDMPMrule_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String InDMPMrule_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String InDMPMrule_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String OutDMPMrule_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String OutDMPMrule_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String OutDMPMrule_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String OutDMPMrule_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String featureControlprofile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String featureControlprofile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String featureControlprofile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String featureControlprofile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String localRingBackTone_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String localRingBackTone_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String localRingBackTone_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String localRingBackTone_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String createLowerCaseRoutervalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String PSXmanualConfigvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String GSXmanualConfigvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String callLimit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String limitNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder"); 
	String callrateLimiteValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	String SBCmanualconfigValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	
	scrollUp();
	waitForAjax();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying Added Trunk value");
	String AddressContext="EXTERNAL_AC_";
	String IPINTERFACEGROUP ="EXTERNAL_IPIG_";
	String IPINTERFACE=	"EXTERNAL_IPIF_";
	
	String subInterfacename_starting="SIF-1-";
	String subInterfacename_middle="-2-";
	String NIFgroupDEfaultValue_starting="SIF-1-";
	String NIFgroupDEfaultValue_middle="-2-";
	String NIGgroupdefaultValue_last="-OUTSIDE";
	
	
	
	//Trunk Group Description
	String expectedValue1=customerName+"_"+servicename+"_"+trunkType;
	compareText_InViewPage("Trunk Group Description", expectedValue1);
	
	//Trunk Type
		compareText_InViewPage( "Trunk Type", trunkType);
		
	//VOIP Protocol
		compareText_InViewPage( "VOIP Protocol", voipProtocol);
		
	//Billing Country
		compareText_InViewPage("Billing Country", country);
		
	//CDR Delivery
		compareText_InViewPage( "CDR Delivery", CDRdelivery);
		
	//Prefix
//		compareText_InViewPage(application, "Prefix", prefix, xml);

	//Gateway
		compareText_InViewPage("Gateway", gateway);
		
	//Quality
		compareText_InViewPage("Quality", quality);
		
	//Trunk Group Name
		compareText_InViewPage("Trunk Group Name", primarytrunkGroupname);
		
	//Traffic Direction
		compareText_InViewPage("Traffic Directions", trafficDirection);
		
	//IP Address Type
		compareText_InViewPage("IP Address Type", "IPv4");
		
	//Carrier IP Originating
		compareText_InViewPage("Carrier IP Originating", carrierIPoriginating);
		
	//Carrier IP Terminating
		compareText_InViewPage("Carrier IP Terminating", carrierIPterminating);
		
	//SIP Signaling Port
		  if(voipProtocol.equalsIgnoreCase("SIP")) {
			  compareText_InViewPage("SIP Signalling Port", SIPsignallingPort);
		  }else {
			  Report.LogInfo("INFO"," 'SIP Signalling port' will not display, if voip protocol selected as 'SIP-1'","PASS");
		  }
		
	
		 
//VLAN Tag 
		  if(!gateway.contains("SBC")) {
			  
			//Sub Interface Slot
			  String subinterfaceSlot_viewPage=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'Sub Interface Slot')]]]/div[2]")).getText();
				
			  if(internetBasedCustomer.equalsIgnoreCase("no")) {
				  
				  //VLAN Tag
//				  compareText_InViewPage(application, "VLAN Tag", vlanTag, xml);
				  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
				  
				  //Sub Interface Name
				  	String SubInterfaceName=subInterfacename_starting+subinterfaceSlot_viewPage+subInterfacename_middle+vlan_actualValue;
					compareText_InViewPage("Sub Interface Name", SubInterfaceName);
					
					//NIF Group
					String NIFgroup=NIFgroupDEfaultValue_starting+subinterfaceSlot_viewPage+NIFgroupDEfaultValue_middle+vlan_actualValue+NIGgroupdefaultValue_last;
					Report.LogInfo("INFO","NIF Group value is: "+NIFgroup,"PASS");
					compareText_InViewPage("NIF Group", NIFgroup);
				  
				  //Signalling Zone
					if(signallngZone.equalsIgnoreCase("null")) {
				  compareText_InViewPage("Signalling Zone", "OUT-UNTRUSTED");
					}
					else if(!signallngZone.equalsIgnoreCase("null")) {
						  compareText_InViewPage("Signalling Zone", signallngZone);
							}
				  
				  
			  }
			  else if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
				 
				  //VLAN Tag
//				  compareText_InViewPage(application, "VLAN Tag", vlanTag, xml);
				  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
				  
				  //Sub Interface Name
				  	String SubInterfaceName=subInterfacename_starting+subinterfaceSlot_viewPage+subInterfacename_middle+vlan_actualValue;
					compareText_InViewPage("Sub Interface Name", SubInterfaceName);
					
					//NIF Group
					String NIFgroup=NIFgroupDEfaultValue_starting+subinterfaceSlot_viewPage+NIFgroupDEfaultValue_middle+vlan_actualValue+NIGgroupdefaultValue_last;
					Report.LogInfo("INFO","NIF Group value is: "+NIFgroup,"PASS");
					compareText_InViewPage("NIF Group", NIFgroup);
				  
					//Signalling Zone
					if(signallngZone.equalsIgnoreCase("null")) {
				  compareText_InViewPage("Signalling Zone", "OUT-UNTRUSTED");
					}
					else if(!signallngZone.equalsIgnoreCase("null")) {
						  compareText_InViewPage("Signalling Zone", signallngZone);
							}
				  
			  }
		  }
		  else if(gateway.contains("SBC")) {
			  if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
				  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
				  
				  //Address Content
				  compareText_InViewPage("Address Context", AddressContext+vlan_actualValue);
				  
				  //IP Interface Group
				 // compareText_InViewPage( "IP Interface Group", IPINTERFACEGROUP+vlan_actualValue);
				  
				  //IP Interface
				 // compareText( "IP Interface", "viewTrunkPage_IPinterface", IPINTERFACE+vlan_actualValue);
				  
				//Signalling Zone
					if(signallngZone.equalsIgnoreCase("null")) {
				  compareText_InViewPage("Signalling Zone", "OUT-UNTRUSTED");
					}
					else if(!signallngZone.equalsIgnoreCase("null")) {
						  compareText_InViewPage("Signalling Zone", signallngZone);
							}
				  
				  
			  }
			  else if(internetBasedCustomer.equalsIgnoreCase("No")) {
				  
				  //VLAN Tag
				  compareText_InViewPage( "VLAN Tag", vlanTag);
				  
				  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
				  
				  //Address Content
				  compareText_InViewPage( "Address Context", AddressContext+vlan_actualValue);
				  
				  //IP Interface Group
				  //compareText_InViewPage( "IP Interface Group", IPINTERFACEGROUP+vlan_actualValue);
				  
				  //IP Interface
				 // compareText( "IP Interface", APT_wholeSale_Obj.APT_wholeSale.viewTrunkPage_IPinterface, IPINTERFACE+vlan_actualValue);
			  }
		  }
			  
	//Signalling Transport Protocol
		compareText_InViewPage( "Signalling Transport Protocol", signallingtransportProtocol);
		
	//Signalling Port
	if(internetBasedCustomer.equalsIgnoreCase("No")) {	
//		signallingPort_viewPage(application, gateway);
	}
		
	//Colt Signalling IP
		compareText_InViewPage( "Colt Signalling IP", coltSignalingIP);

	//Media IP
		compareText_InViewPage( "Media IP", mediaIP);
		
	//Reuse NIF Group
		if(gateway.contains("SBC")) {
			Report.LogInfo("INFO","Reuse NIF field will not display for Gateway 'SBC'","PASS");
		}
		else {
			compareText_InViewPage( "Reuse NIF Group", reuseNIFgroup);
		}
		
		
	//Reuse Sig Zone/Port
		compareText_InViewPage( "Reuse Sig Zone/Port", reuseSigZonePart);
	
	//Call Admission Control
		compareText_InViewPage( "Call Admission Control", callAdmissionControl);
		
		if(callAdmissionControl.equalsIgnoreCase("yes")) {
		  //call limit	
			
			if(limitNumber.equalsIgnoreCase("null")) {
				Report.LogInfo("INFO","Limit Number value is not edited","PASS");
			}else {
				compareText_InViewPage( "Call Limit",limitNumber);
			}
			
		}
	
	//Call Rate Limit
		if(callrateLimitSelection.equalsIgnoreCase("Yes")) {
			Thread.sleep(2000);
			//call rate limit value
			if(callrateLimiteValue.equalsIgnoreCase("null")) {
				Report.LogInfo("INFO","Call rate Limit value is not edited","PASS");
			}else {
				compareText_InViewPage("Call Rate Limit", callrateLimiteValue);
			}
			
		}
		
		
	//Source Address Filtering
		compareText_InViewPage( "Source Address Filtering", sourceAddressFiltering);
		
	//100rel Support	
		compareText_InViewPage( "100rel Support", relSupport);
		
	//SIP session Keepalive timer
		compareText_InViewPage( "SIP session Keepalive timer", sipSessionkeepAliveTimer);
		
	//Retry Invite
		compareText_InViewPage( "Retry Invite", retryInvite);
		
	//Route Priority
		compareText_InViewPage("Route Priority", routePriority);
	
	//Address Reachability	
		compareText_InViewPage( "Address Reachability", addressReachability);
		
	//Create Lower Case Routes
		compareText_InViewPage( "Create Lower Case Routes", createLowerCaseRoutervalue);
		
	//PSX Manual Configuration	
		compareText_InViewPage( "PSX Manual Configuration", PSXmanualConfigvalue);
		
	//GSX or SBX configuration	
		if(gateway.contains("SBC")) {
			//Manual Configuration on SBC
			compareText_InViewPage( "Manual Configuration on SBC", SBCmanualconfigValue);
		}else {
			//Manual Configuration On GSX
			compareText_InViewPage( "Manual Configuration on GSX", GSXmanualConfigvalue);
		}

	try {	
		WebElement PSXconfigurationLabelName=findWebElement(APT_wholeSale_Obj.APT_wholeSale.psxConfiguration_LabelName_viewTrunkPage);
		scrollDown(APT_wholeSale_Obj.APT_wholeSale.psxConfiguration_LabelName_viewTrunkPage);
		waitForAjax();
	}catch(Exception e) {
		Reporter.log("PSX configuration label is not displaying");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "PSX configuration label is not displaying");
	}
		
	//E164Global Profile	
		verifyaddTrunk_existingNewDropdownField(globalProfile_ExistingSelection, globalProfile_newSelection, globalProfile_ExistingValue, 
				globalProfile_newValue, APT_wholeSale_Obj.APT_wholeSale.globalProfile_Dropdown , APT_wholeSale_Obj.APT_wholeSale.globalProfile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.globalProfile_textField, "E164Global Profile");
		
		
	//E164LocalProfile	
		verifyaddTrunk_existingNewDropdownField(localProfile_existingFieldSelection, localProfile_newFieldSelection, localProfile_existingvalue,
				localProfile_newvalue, APT_wholeSale_Obj.APT_wholeSale.localProfile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.localProfile_checkbox, APT_wholeSale_Obj.APT_wholeSale.localProfile_TextField, "E164 Local Profile");
		
		
	//COS profile
		verifyaddTrunk_existingNewDropdownField(COSprofile_existingFieldSelection, COSprofile_newFieldSelection, COSprofile_existingValue,
				COSprofile_newValue, APT_wholeSale_Obj.APT_wholeSale.COSprofile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.COSprofile_checkbox, APT_wholeSale_Obj.APT_wholeSale.COSprofile_TextField, "COS Profile");
		
		
	//PSPG Name
		verifyaddTrunk_existingNewDropdownField(PSPGname_existingFieldSelection, PSPGname_newFieldSelection, pspgName_existingValue,
				pspgName_newValue, APT_wholeSale_Obj.APT_wholeSale.PSPSGname_Dropdown, APT_wholeSale_Obj.APT_wholeSale.PSPGname_Checkbox, APT_wholeSale_Obj.APT_wholeSale.PSPGname_TextField, "PSPG Name");
		
		
	//Preferred  PSP
		verifyaddTrunk_existingNewDropdownField(prefferedPSP_existingFieldSelection, prefferedPSP_newFieldSelection, preferredPSP_exitingvalue,
				preferredPSP_newvalue, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_Dropdown, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_checkbox, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_TextField, "Preferred PSP");
		

	//Carrier
		verifyaddTrunk_existingNewDropdownField(carrier_existingFieldSelection, carrier_newFieldSelection, carrier_existingValue,
				carrier_newValue, APT_wholeSale_Obj.APT_wholeSale.carriers_Dropdown, APT_wholeSale_Obj.APT_wholeSale.carriers_checkbox, APT_wholeSale_Obj.APT_wholeSale.carriers_TextField, "Carrier");
		
		
		
	//IP Signalling Profile
		verifyaddTrunk_existingNewDropdownField(IPsignallingProfile_existingFieldSelection, IPsignallingProfile_newFieldSelection, IPsignallingProfile_existingValue,
				IPsignallingProfile_newValue, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_textField, "IP Signaling Profile");
		
		
	//Egress IP Signaling Profile
		verifyaddTrunk_existingNewDropdownField(EgressIpsignal_existingFieldSelection, EgressIpsignal_newFieldSelection, EgressIPsignal_existingValue,
				EgressIPsignal_newValue, APT_wholeSale_Obj.APT_wholeSale.EgressIpsignal_Dropdown, APT_wholeSale_Obj.APT_wholeSale.EgressIPsignal_checkbox, APT_wholeSale_Obj.APT_wholeSale.EgressipSignal_TextField, "Egress IP Signalling Profile");
		
	
	//In DM/PM rule
		verifyaddTrunk_existingNewDropdownField(InDMPMrule_existingFieldSelection, InDMPMrule_newFieldSelection, InDMPMrule_existingValue,
				InDMPMrule_newValue,  APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_Dropdown, APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_checkbox, APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_TextField, "In DM/PM rule");
		
		
	//Out DM/PM rule
		verifyaddTrunk_existingNewDropdownField(OutDMPMrule_existingFieldSelection, OutDMPMrule_newFieldSelection, OutDMPMrule_existingValue,
				OutDMPMrule_newValue, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_Dropdown, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_checkbox, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_TextField, "Out DM/PM rule");
		
		
	//Feature Control Profile	
		verifyaddTrunk_existingNewDropdownField(featureControlprofile_existingFieldSelection, featureControlprofile_newFieldSelection, featureControlprofile_existingValue,
				featureControlprofile_newValue, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_Checkbox,APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_TextField, "Feature Control Profile");
		
		
	//Local Ring Back Tone
		verifyaddTrunk_existingNewDropdownField(localRingBackTone_existingFieldSelection, localRingBackTone_newFieldSelection, localRingBackTone_existingValue,
				localRingBackTone_newValue, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_Dropdown, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_checkbox, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_TextField, "Local Ring Back Tone");

	
}


public void verifyInterfaceValues_MAS( String testDataFile, String dataSheet, String scriptNo,String dataSetNo ) throws InterruptedException, IOException {
	
	
	String MAS_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressRange");

String MAS_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressRangeIPV6");




String interfaceAddressrangeValue=null;
		
waitforPagetobeenable();
//scrolltoend();
if(MAS_InterfaceAddressRangeIPV6.equalsIgnoreCase("null")) {
	interfaceAddressrangeValue=MAS_InterfaceAddressRange;
}else {
	interfaceAddressrangeValue=MAS_InterfaceAddressRange+","+MAS_InterfaceAddressRangeIPV6;
}

verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_nameValue,  "Interface");
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_linkValue, "Link/Circuit id" );
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_InteraceAddressRangeValue, "Interface Address Range"  );
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_InteraceAddressValue, "Interface Address" );
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_bearertypeValue ,"Bearer Type" );
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_bandwidthvalue ,"Bandwidth" );
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfacePanel_vlanidvalue , "Vlan Id" );
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_interfacePanel_IfInOctetsValue,"IfInOctets" );
verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_interfacePanel_IVmanagementvalue, "IV Management" );

}

public void editPEdevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) 
		throws InterruptedException, IOException {


String editDeviceName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editName");
String editVendorModel= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editVendor");
String editmanageAddress= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_manageAddress");
String editSnmpro= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_Snmpro");
String editCountry= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editCountry");
String editExistingCity= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_ExistingCitySelection");
String editNewCity= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_newCitySelection");
String editExistingCityValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editExistingCity");
String editNewCityName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editNewCityname");
String editNewCityCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editNewCityCode");
String editExistingSite= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_ExistingSiteSelection");
String editNewSite= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_newSiteSelection");	 
String editExistingSiteValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editExistingSite");
String editNewSiteName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editNewSitename");
String editNewSiteCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editNewSiteCode");
String editExistingPremise= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_ExistingPremiseSelection");
String editNewPremise= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_newPremiseSelection");
String editExistingPremiseValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editExistingPremise");

String editNewPremiseName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editNewPremiseName");
String editNewPremiseCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_editNewPremiseCode");
scrollUp();
waitForAjax();

ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Edit PE Device' Functoinality");



scrollUp();
waitForAjax();

ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Edit MAS Switch' Functionality");

verifyExists(APT_wholeSale_Obj.APT_wholeSale.MAS_View_Action_EditLink, "Edit link");

click(APT_wholeSale_Obj.APT_wholeSale.MAS_View_Action_EditLink, "Edit link");



waitforPagetobeenable();
waitForAjax();
scrollDown(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton);
waitForAjax();
click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void selectInterface_AndDelete_PEdevice(String testDataFile, String dataSheet,String scriptNo, String dataSetNo,String interfaceName) throws InterruptedException, IOException {

String deviceName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_deviceName");

//String interfaceName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"interfacename2");

ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Delete Interface_PE device");

WebElement MASswitchPanel_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.MASswitch_panelHeader);
scrollDown(APT_wholeSale_Obj.APT_wholeSale.MASswitch_panelHeader);
waitForAjax();


WebElement SelectInterface= webDriver.findElement(By.xpath("//div[div[b[text()='"+ deviceName+"']]]//following-sibling::div//div[@role='gridcell'][text()='"+ interfaceName +"']"));
if(SelectInterface.isDisplayed())
{
SelectInterface.click();
ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on "+ interfaceName + "Interface");
waitForAjax();

WebElement addeddevice_interface_actiondropdown1=(findWebElement(APT_wholeSale_Obj.APT_wholeSale.addeddevice_interface_actiondropdown.replace("value", deviceName)));
addeddevice_interface_actiondropdown1.click();

 click( APT_wholeSale_Obj.APT_wholeSale.MAS_View_Action_DeleteLink, "delete");
 waitForAjax();
 WebElement DeleteAlertPopup= findWebElement(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);
 if(DeleteAlertPopup.isDisplayed())
 {
       click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
 }
 else
 {
     //  Log.info("Delete alert popup is not displayed");
       ExtentTestManager.getTest().log(LogStatus.FAIL, " Delete alert popup is not displayed");
       Report.LogInfo("INFO", "Delete alert popup is not displayed","FAIL");
       
 }
 
 verifysuccessmessage( "Interface deleted successfully");
 
 
}else {
  ExtentTestManager.getTest().log(LogStatus.FAIL, interfaceName + " is not displaying under 'PE Device' for the device "+ deviceName);
}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


public void PEdevice__DeleteFromServiceFunctionality(String existingdevicename) throws InterruptedException, IOException { 


ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Delete PE device' Functionality");

WebElement managementOptions_header= findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);
scrollDown(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader);


if(findWebElement(APT_wholeSale_Obj.APT_wholeSale.managementOptionsPanelheader).isDisplayed())
{
	
	webDriver.findElement(By.xpath("//tr//td//b[contains(.,'"+ existingdevicename +"')]//parent::td//following-sibling::span//a[text()='Delete from Service']")).click();
	
	
    WebElement DeleteAlertPopup= findWebElement(APT_wholeSale_Obj.APT_wholeSale.delete_alertpopup);

                  if(DeleteAlertPopup.isDisplayed())
                  {
                        click(APT_wholeSale_Obj.APT_wholeSale.deletebutton, "Delete");
                        //compareText(APT_wholeSale_Obj.APT_wholeSale.MAS_deleteSuccessMessage, "MAS switch deleted successfully",  "Device delete success message");
                       
                  
                  }
                  else
                  {
                	  Report.LogInfo("INFO","Delete alert popup is not displayed","FAIL");
                        ExtentTestManager.getTest().log(LogStatus.FAIL, " Delete alert popup is not displayed");
                  }
                  
            }
            
           
     

else
{
      ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
      Report.LogInfo("INFO","No Device added in grid","FAIL");
}		
}

public void verifyAddInterfaceFunction_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) 
		throws InterruptedException, IOException {  


String MAS_AccessMedia= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_AccessMedia");
String MAS_Network= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Network");
String MAS_HSRPBGP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPBGP");
String MAS_Interface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Interface");
String MAS_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressRange");
String MAS_InterfaceAddressMask= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressMask");
String MAS_HSRPIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPIP");
String MAS_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressRangeIPV6");
String MAS_HSRPIPv6Address= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPIPv6Address");
String MAS_PrimaryIPv6onMas1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_PrimaryIPv6onMas1");
String MAS_SecondaryIPv6onMas2= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_SecondaryIPv6onMas2");
String MAS_GroupNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_GroupNumber");
String MAS_Link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Link");
String MAS_VLANID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_VLANID");
String MAS_IVManagement= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_IVManagement");
String MAS_generateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_GenerateConfiguration");
String MAS_HSRPTrackInterface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPTrackInterface");
String MAS_HSRPAuthentication= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPAuthentication");




ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Add Interface' Functionality");

WebElement routerpanel= findWebElement(APT_wholeSale_Obj.APT_wholeSale.routerTool_HeaderPanel);
scrollIntoView(routerpanel);
waitForAjax();

  //clicking on action dropdown under Interface panel
click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_addInterfaceLink,"Add Interface/Link");

scrollDown(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton);
click( APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");

verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_configurationWarningMessage, "Configuration");

scrollUp();
waitForAjax();

verifyExists( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_interfaceWarningMessage, "Interface");

if(MAS_AccessMedia.equalsIgnoreCase("VPN")) {
addDropdownValues_commonMethod( "Access Media", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);  //Access Media Dropdown
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
addDropdownValues_commonMethod("Network",  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange, "Interface Address Range");   //interface address range text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
//sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1,  "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field

scrollDown(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
Thread.sleep(1000);

sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber, "Group Number");   //Group Number text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox,"IV Management",  MAS_IVManagement);   //IV Management checkbox

}else if(MAS_AccessMedia.equalsIgnoreCase("EPN")) {


addDropdownValues_commonMethod("Access Media", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);  //Access Media Dropdown
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
addDropdownValues_commonMethod( "HSRP/BGP", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRDorBPdropdown, MAS_HSRPBGP);   //HSRP/BGP dropdown
addDropdownValues_commonMethod( "Network", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange, "Interface Address Range");   //interface address range text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1, "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field

scrollDown(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
waitForAjax();

sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber, "Group Number");   //Group Number text field
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox,  MAS_IVManagement,"IV Management");   //IV Management checkbox
sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPtrackInterfacetextField, MAS_HSRPTrackInterface,  "HSRP Track Interface");   //HSRP Track Interface text Field
sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPauthenticationtextField, MAS_HSRPAuthentication, "HSRP Authentication");   //HSRP Authentication text Field

}

scrollUp();
waitForAjax();

//perform Generate configuration
boolean configurationpanel=false;
try {	
configurationpanel=findWebElement(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_confiugrationPanelheader).isDisplayed();
if(configurationpanel) {
ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
//Log.info("'Configuration' panel is displaying");
Report.LogInfo("INFO","Configuration' panel is displaying","PASS");
addDropdownValues_commonMethod( "Generate Configuration ", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationDropdown, MAS_generateConfiguration);
waitForAjax();

click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationLink, "Generate Configuration");
waitForAjax();

scrollUp();
Thread.sleep(1000);

String configurationvalues=findWebElement(APT_wholeSale_Obj.APT_wholeSale.configuration_textArea).getText();
if(configurationvalues.isEmpty()) {
	ExtentTestManager.getTest().log(LogStatus.FAIL, "After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
	//Log.info("After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
	Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box","FAIL");


}else {
	ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate Configuration' link, "
			+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
	//Log.info("After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: "+configurationvalues);

	Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: +configurationvalues","PASS");

}

}else {
ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
//Log.info("'Configuration' panel is not displaying");
Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");

}
}catch(Exception e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
	//Log.info("'Configuration' panel is not displaying");
	Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");
}
((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
waitForAjax();
click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
}

public void verifyaddTrunk_existingNewDropdownField(String existingFieldSelection, String newFieldSelection, String existinFieldvalue,
		String newFieldvalue,String xpath_dropdown,String xpath_checkbox, String xpath_textfield, String labelname ) throws IOException, InterruptedException {

	
	if((existingFieldSelection.equalsIgnoreCase("yes")) && (newFieldSelection.equalsIgnoreCase("no"))) {
		
		viewTrunktable_ForExistingalues(labelname, existinFieldvalue);
		
	}
	else if((existingFieldSelection.equalsIgnoreCase("no")) && (newFieldSelection.equalsIgnoreCase("yes"))) {
		
		viewTrunktable_ForNewValues(labelname, newFieldvalue);
	}
	
}

public void viewTrunktable_ForExistingalues(String labelName, String expectedValue) throws InterruptedException, IOException {
	
	try {	
		WebElement value=findWebElement((APT_wholeSale_Obj.APT_wholeSale.viewTrunk_Table_ForExistingValue).replace("value", labelName));
		String actualValue=getTextFrom(((APT_wholeSale_Obj.APT_wholeSale.viewTrunk_Table_ForExistingValue).replace("value", labelName)));
		
	if(expectedValue.equalsIgnoreCase("null")) {
		expectedValue="None";
		if(expectedValue.equalsIgnoreCase(actualValue)) {
			ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelName +"' field '"+expectedValue+"' is same as the Acutal value '"+actualValue+"'");
			Report.LogInfo("INFO","The Expected value for '"+ labelName +"' field '"+expectedValue+"' is same as the Acutal value '"+expectedValue+"'","PASS");
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelName +"' field '"+expectedValue+"' is not same as the Acutal value '"+actualValue+"'");
			Report.LogInfo("INFO","The Expected value for '"+ labelName +"' field '"+expectedValue+"' is not same as the Acutal value '"+expectedValue+"'","PASS");
		}
	}
	else {
		if(expectedValue.equalsIgnoreCase(actualValue)) {
			ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelName +"' field '"+expectedValue+"' is same as the Acutal value '"+actualValue+"'");
			Report.LogInfo("INFO","The Expected value for '"+ labelName +"' field '"+expectedValue+"' is same as the Acutal value '"+expectedValue+"'","PASS");
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelName +"' field '"+expectedValue+"' is not same as the Acutal value '"+actualValue+"'");
			Report.LogInfo("INFO","The Expected value for '"+ labelName +"' field '"+expectedValue+"' is not same as the Acutal value '"+expectedValue+"'","PASS");
		}
	}
		
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelName + " field is not displaying");
		Report.LogInfo("INFO",labelName + " field is not displaying","PASS");
	}
		
	}

public void viewTrunktable_ForNewValues( String labelName, String expectedValue) throws InterruptedException, IOException {
	
	try {	
		WebElement value=findWebElement((APT_wholeSale_Obj.APT_wholeSale.viewTrunk_Table_ForNewValues).replace("value", labelName));
		String actualValue= getTextFrom((APT_wholeSale_Obj.APT_wholeSale.viewTrunk_Table_ForNewValues).replace("value", labelName));
		
		if(expectedValue.equalsIgnoreCase(actualValue)) {
			ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelName +"' field '"+expectedValue+"' is same as the Acutal value '"+actualValue+"'");
			Report.LogInfo("INFO","The Expected value for '"+ labelName +"' field '"+expectedValue+"' is same as the Acutal value '"+expectedValue+"'","PASS");
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelName +"' field '"+expectedValue+"' is not same as the Acutal value '"+actualValue+"'");
			Report.LogInfo("INFO","The Expected value for '"+ labelName +"' field '"+expectedValue+"' is not same as the Acutal value '"+expectedValue+"'","PASS");
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelName + " field is not displaying");
		Report.LogInfo("INFO",labelName + " field is not displaying","PASS");
	}
		
	}

public void editInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	
	String MAS_AccessMedia= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_AccessMedia");
	String MAS_Network= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Network");
	String MAS_HSRPBGP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPBGP");
	String MAS_Interface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Interface");
	String MAS_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressRange");
	String MAS_InterfaceAddressMask= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressMask");
	String MAS_HSRPIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPIP");
	String MAS_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressRangeIPV6");
	String MAS_HSRPIPv6Address= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPIPv6Address");
	String MAS_PrimaryIPv6onMas1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_PrimaryIPv6onMas1");
	String MAS_SecondaryIPv6onMas2= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_SecondaryIPv6onMas2");
	String MAS_GroupNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_GroupNumber");
	String MAS_Link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Link");
	String MAS_VLANID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_VLANID");
	String MAS_IVManagement= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_IVManagement");
	String MAS_generateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_GenerateConfiguration");
	String MAS_HSRPTrackInterface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPTrackInterface");
	String MAS_HSRPAuthentication= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_HSRPAuthentication");
		
	
	waitforPagetobeenable();
	
	addDropdownValues_commonMethod("Access Media", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);  //Access Media Dropdown
	WebElement accessMediaElement = findWebElement(APT_wholeSale_Obj.APT_wholeSale.fetchMAS_AccessMediaDropdown); 
	String accessMediaActualValue=accessMediaElement.getText();
	
	if(accessMediaActualValue.equalsIgnoreCase("VPN")) {
		
		sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
		addDropdownValues_commonMethod( "Network", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange, "Interface Address Range");   //interface address range text field
		  //sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
		  //sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1, "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field
		
	scrollDown( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
	waitForAjax();
		
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber,"Group Number");   //Group Number text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
	editcheckbox_commonMethod("IV Management",APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox ,MAS_IVManagement);   //IV Management checkbox	
	
	}else if(accessMediaActualValue.equalsIgnoreCase("EPN")) {
		
		sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield, MAS_Interface, "Interface");   //interface Text Field
		addDropdownValues_commonMethod("HSRP/BGP",  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRDorBPdropdown, MAS_HSRPBGP);   //HSRP/BGP dropdown
		addDropdownValues_commonMethod("Network",  APT_wholeSale_Obj.APT_wholeSale.MAS_PE_networkDropdown, MAS_Network);   //network Dropdown
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeTextfield, MAS_InterfaceAddressRange,  "Interface Address Range");   //interface address range text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMask, "Interface Address/Mask");   //Interface Address/Mask textfield
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPTextfield, MAS_HSRPIP, "HSRP IP");   //HSRP IP text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceAddressRangeIPV6Textfield, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6");   //Interface Address Range IPV6 text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPIPv6AddressTextfield, MAS_HSRPIPv6Address, "HSRP IPv6 Address");   //HSRP IPv6 Address text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_PrimaryIPv6onMas1Textfield, MAS_PrimaryIPv6onMas1, "Primary IPv6 on Mas-1");   //Primary IPv6 on Mas-1 textfield
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas-2");   //Secondary IPv6 on Mas-2 text field
		
		scrollDown( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_InterfaceTextfield);	
		waitForAjax();
		
		sendKeys( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_GroupNumberTextfield, MAS_GroupNumber, "Group Number");   //Group Number text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_LinkTextfield, MAS_Link, "Link");   //Link text field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_VLANIDTextfield, MAS_VLANID, "VLAN Id");   //VLAN Id text field
		editcheckbox_commonMethod( MAS_IVManagement, "MAS_PE_IVManagementCheckbox" , "IV Management");    //IV Management checkbox
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPtrackInterfacetextField, MAS_HSRPTrackInterface, "HSRP Track Interface");   //HSRP Track Interface text Field
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_HSRPauthenticationtextField, MAS_HSRPAuthentication, "HSRP Authentication");   //HSRP Authentication text Field
		
	}
		
	scrollDown( APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox);	
	waitForAjax();
	
//perform Generate configuration
	boolean configurationpanel=false;
try {	
	configurationpanel=findWebElement(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_IVManagementCheckbox).isDisplayed();
	if(configurationpanel) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
		//Log.info("'Configuration' panel is displaying");
		Report.LogInfo("INFO","Configuration' panel is displaying","PASS");
		addDropdownValues_commonMethod( "Generate Configuration ", APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationDropdown, MAS_generateConfiguration);
		waitForAjax();
		
		click(APT_wholeSale_Obj.APT_wholeSale.MAS_PE_generateConfigurationLink, "Generate Configuration");
		waitForAjax();
		
		scrollDown( APT_wholeSale_Obj.APT_wholeSale.configuration_textArea);	
		waitForAjax();
		
		String configurationvalues=findWebElement(APT_wholeSale_Obj.APT_wholeSale.configuration_textArea).getText();
		if(configurationvalues.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
			//Log.info("After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box");
			Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, no values displaying under 'Configuration' text box","FAIL");
		
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate Configuration' link, "
					+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
			//Log.info("After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
		
			Report.LogInfo("INFO","After clicking on 'Generate Configuration' link, "+ "under 'Configuration' textbox values displaying as: +configurationvalues","PASS");
		}
	
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
		//Log.info("'Configuration' panel is not displaying");
		Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");
	}
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
			//Log.info("'Configuration' panel is not displaying");
			Report.LogInfo("INFO","Configuration' panel is not displaying","FAIL");
		
		}
scrollDown( APT_wholeSale_Obj.APT_wholeSale.trunk_okButton);	
waitForAjax();
		click(APT_wholeSale_Obj.APT_wholeSale.trunk_okButton, "OK");
}

public void selectrowforInterfaceToselecttable(String interfacenumber)
		throws IOException, InterruptedException {

	click(APT_wholeSale_Obj.APT_wholeSale.InterfaceToselect_Actiondropdown, "Action Dropdown");
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.InterfaceToselect_addbuton, "Add");
	/*
	int TotalPages;
	
			List<WebElement> results = webDriver.findElements(By.xpath("//form[div[div[text()='Interfaces to Select']]]//following-sibling::div//div[text()='"+ interfacenumber +"']"));
			int numofrows = results.size();
			Report.LogInfo("INFO","no of results: " + numofrows,"PASS");
			boolean resultflag;

				for (int i = 0; i < numofrows; i++) {
					try {
						resultflag = results.get(i).isDisplayed();
						Report.LogInfo("INFO","status of result: " + resultflag,"PASS");
						if (resultflag) {
							Report.LogInfo("INFO",results.get(i).getText(),"PASS");
							results.get(i).click();
							ExtentTestManager.getTest().log(LogStatus.PASS, interfacenumber + " is selected under 'Interface to select' table");
							Report.LogInfo("INFO",interfacenumber + " is selected under 'Interface to select' table","PASS");

							waitForAjax();
							click(APT_wholeSale_Obj.APT_wholeSale.InterfaceToselect_Actiondropdown, "Action Dropdown");
							waitForAjax();
							
							click(APT_wholeSale_Obj.APT_wholeSale.InterfaceToselect_addbuton, "Add");
							break;
						}

					} catch (StaleElementReferenceException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						results = webDriver.findElements(By.xpath("(//div[@class='row'])[8]//div[div[contains(text(),'"+ interfacenumber + "')]]//input"));
						numofrows = results.size();
						// results.get(i).click();
						Report.LogInfo("INFO","selected row is : " + i,"PASS");
						ExtentTestManager.getTest().log(LogStatus.FAIL, " Failure on selecting an Interface to ad with service ");
					}
				}
				*/
}

public void viewInterface_MASswitch(String deviceName, String testDataFile,String dataSheet, String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	String Interface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Interface");
	String link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_Link");
	String interfaceAddressrange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_InterfaceAddressRange");
	String vlanID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"MAS_VLANID");

	scrollUp();
	waitForAjax();
	
	
	compareText_InViewPage( "Device Name", deviceName);
	
	compareText_InViewPage( "Interface", Interface);
	
	compareText_InViewPage( "Link/Circuit Id", link);
	
	compareText_InViewPage( "Interface Address Range", interfaceAddressrange);
	
	compareText_InViewPage( "VLAN Id", vlanID);
	
}

public void searchorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException

{
	waitforPagetobeenable();
	waitForAjax();
	String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "edit_serviceId");

	verifyExists(APT_wholeSale_Obj.APT_wholeSale.ManageCustomerServiceLink, " Manage Customer Service Link");
	mouseMoveOn(APT_wholeSale_Obj.APT_wholeSale.ManageCustomerServiceLink);

	verifyExists(APT_wholeSale_Obj.APT_wholeSale.searchorderlink, "search order link");
	click(APT_wholeSale_Obj.APT_wholeSale.searchorderlink, "search order link");

	verifyExists(APT_wholeSale_Obj.APT_wholeSale.servicefield, "service field");
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.servicefield, sid);

	click(APT_wholeSale_Obj.APT_wholeSale.searchbutton, "searchbutton");
	// click(searchbutton,"searchbutton");

	verifyExists(APT_wholeSale_Obj.APT_wholeSale.serviceradiobutton, "service radio button");
	click(APT_wholeSale_Obj.APT_wholeSale.serviceradiobutton, "service radio button");

	verifyExists(APT_wholeSale_Obj.APT_wholeSale.searchorder_actiondropdown, "search order actiodropdown");
	click(APT_wholeSale_Obj.APT_wholeSale.searchorder_actiondropdown, "search order linksearch order actiodropdown");

	verifyExists(APT_wholeSale_Obj.APT_wholeSale.view, "view");
	click(APT_wholeSale_Obj.APT_wholeSale.view, "view");

}

public void addTrunk(String customerName, String servicename,String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws IOException, InterruptedException {       
	
	   
    String trunkType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"trunkType"); 
    String voipProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"voipProtocol"); 
    String country= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BillingCountry");
    String CDRdelivery= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CDRdelivery"); 
    String gateway= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"gateway"); 
    String quality= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"quality");
	String trafficDirection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"trafficDirection"); 
	String ipAddresstype= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"ipAddresstype"); 
	String SIPsignallingPort= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"SIPsignallingPort");
	String internetBasedCustomer= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"internetBasedCustomer"); 
	String vlanTag= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"vlanTag"); 
	String subInterfaceSlot= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"subInterfaceSlot");
	String signallngZone= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"signallngZone");
	String signallingtransportProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"signallingtransportProtocol");
	String coltSignalingIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"coltSignalingIP"); 
	String mediaIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"mediaIP"); 
	String reuseNIFgroup= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"reuseNIFgroup"); 
	String reuseSigZonePart= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"reuseSigZonePart");
	String callAdmissionControl= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"callAdmissionControl");
	String callrateLimit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"callrateLimitselection"); 
	String sourceAddressFiltering= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"sourceAddressFiltering"); 
	String relSupport= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"relSupport"); 
	String sipSessionkeepAliveTimer= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"sipSessionkeepAliveTimer");
	String retryInvite= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"retryInvite"); 
	String routePriority= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"routePriority"); 
	String addressReachability= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"addressReachability"); 
	String carrierIPoriginating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carrierIPoriginating"); 
	String carrierIPterminating= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carrierIPterminating");
	String TLSfield= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"TLSfield");
	String srtp= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"srtp");
	String prefix= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"prefix"); 
	String globalProfile_ExistingSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"globalProfile_ExistingSelection");
	String globalProfile_newSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"globalProfile_newSelection"); 
	String globalProfile_ExistingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"globalProfile_ExistingValue");
	String globalProfile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"globalProfile_newValue");
	String localProfile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localProfile_existingFieldSelection"); 
	String localProfile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localProfile_newFieldSelection");
	String localProfile_existingvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localProfile_existingvalue"); 
	String localProfile_newvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localProfile_newvalue");
	String COSprofile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"COSprofile_existingFieldSelection");
	String COSprofile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"COSprofile_newFieldSelection"); 
	String COSprofile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"COSprofile_existingValue"); 
	String COSprofile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"COSprofile_newValue");
	String PSPGname_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PSPGname_existingFieldSelection"); 
	String PSPGname_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PSPGname_newFieldSelection");
	String pspgName_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"pspgName_existingValue"); 
	String pspgName_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"pspgName_newValue");
	String prefferedPSP_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"prefferedPSP_existingFieldSelection");
	String prefferedPSP_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"prefferedPSP_newFieldSelection");
	String preferredPSP_exitingvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"preferredPSP_exitingvalue"); 
	String preferredPSP_newvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"preferredPSP_newvalue");
	String carrier_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carrier_existingFieldSelection");
	String carrier_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carrier_newFieldSelection");
	String carrier_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carrier_existingValue");
	String carrier_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"carrier_newValue");
	String IPsignallingProfile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IPsignallingProfile_existingFieldSelection");
	String IPsignallingProfile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IPsignallingProfile_newFieldSelection");
	String IPsignallingProfile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IPsignallingProfile_existingValue"); 
	String IPsignallingProfile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IPsignallingProfile_newValue");
	String EgressIpsignal_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"EgressIpsignal_existingFieldSelection");
	String EgressIpsignal_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"EgressIpsignal_newFieldSelection");
	String EgressIPsignal_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"EgressIPsignal_existingValue"); 
	String EgressIPsignal_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"EgressIPsignal_newValue");
	String InDMPMrule_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"InDMPMrule_existingFieldSelection");
	String InDMPMrule_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"InDMPMrule_newFieldSelection"); 
	String InDMPMrule_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"InDMPMrule_existingValue");
	String InDMPMrule_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"InDMPMrule_newValue");
	String OutDMPMrule_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"OutDMPMrule_existingFieldSelection"); 
	String OutDMPMrule_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"OutDMPMrule_newFieldSelection"); 
	String OutDMPMrule_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"OutDMPMrule_existingValue"); 
	String OutDMPMrule_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"OutDMPMrule_newValue");
	String featureControlprofile_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"featureControlprofile_existingFieldSelection");
	String featureControlprofile_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"featureControlprofile_newFieldSelection");
	String featureControlprofile_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"featureControlprofile_existingValue");
	String featureControlprofile_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"featureControlprofile_newValue");
	String localRingBackTone_existingFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localRingBackTone_existingFieldSelection");
	String localRingBackTone_newFieldSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localRingBackTone_newFieldSelection");
	String localRingBackTone_existingValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localRingBackTone_existingValue");
	String localRingBackTone_newValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"localRingBackTone_newValue");
	String createLowerCaseRoutervalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"createLowerCaseRoutervalue");
	String PSXmanualConfigvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PSXmanualConfigvalue"); 
	String GSXmanualConfigvalue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"GSXmanualConfigvalue");
	String callLimit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"callLimit"); 
	String limitNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"limitNumber"); 
	String callrateLimiteValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"callrateLimiteValue"); 
	String SBCmanualconfigValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"SBCmanualconfigValue");
	
	String gatewayCode=null;
	String subInterfacename_starting="SIF-1-";
	String subInterfacename_middle="-2-";
	String NIFgroupDEfaultValue_starting="SIF-1-";
	String NIFgroupDEfaultValue_middle="-2-";
	String NIGgroupdefaultValue_last="-OUTSIDE";
	String ipInterfaceDEfaultValue="EXTERNAL_IPIF_";
	String addressContextDefaultValue="EXTERNAL_AC_";
	String ipInterfaceGroupDefaultvalue="EXTERNAL_IPIG_";
	String signallingZoneDefaultValue="OUT-UNTRUSTED";
	
	
	String SubInterfaceName=null;
	String NIFgroup=null;
	String ipInterface=null;
	String addressContext=null;
	String ipInterfaceGroup=null;
	String prefix_code=null;
	
	
	waitforPagetobeenable();
	
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	click(APT_wholeSale_Obj.APT_wholeSale.OKbutton, "OK");
	
	scrollUp();
	waitForAjax();
	
	
//Trunk Type	
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.trunkType_Dropdown, "Trunk Type", trunkType);
	String trunktype_code=trunkType_code(trunkType);
	
	
//Trunk Group Description
	String expectedValue1=customerName+"_"+servicename+"_"+trunkType;
	verifyExists( APT_wholeSale_Obj.APT_wholeSale.trunkGroupDEscription_textField, "Trunk Group Description");

	
//VOIP Protocol
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.voipProtocol_Dropdown, "VOIP Protocol", voipProtocol);
	String voip_code=voipProtocol_code(voipProtocol);
	
//Billing COuntry
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.country_warningMessage, "Billing Country");   //validate warning message
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.billingCoutry_Dropdown, "Billing Country", country);
	
	String country_code=findWebElement(APT_wholeSale_Obj.APT_wholeSale.billingCoutry_Dropdown).getAttribute("value");
	Reporter.log("country dropdon value is: "+country_code);
	
//CDR Delivery
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.CDRdelivery_Dropdown, "CDR Delivery", CDRdelivery);
	String CDR_code=CDRdelivery_code(CDRdelivery);
	
//Prefix
	verifyExists(APT_wholeSale_Obj.APT_wholeSale.prefix_warningMessage, "Prefix");  //validate warning messsage
	
	//click on "Allocate Prefix" button
	 click(APT_wholeSale_Obj.APT_wholeSale.allocatePrefix_button, "Allocate Prefix");
	 
	 String prefix_actualvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.prefix_textField).getAttribute("value");
	 if(prefix_actualvalue.isEmpty()) {
		 
		 sendKeys(APT_wholeSale_Obj.APT_wholeSale.prefix_textField, prefix, "Prefix");
		 
	 }else {
		 ExtentTestManager.getTest().log(LogStatus.PASS, "When we click on 'Allocate Prefix' button, Under 'Prefix' value is displaying as: "+prefix_actualvalue);
		 Reporter.log("When we click on 'Allocate Prefix' button, Under 'Prefix' value is displaying as: "+prefix_actualvalue);
	 }

//valdate the prefix value for adding into Trunk group Name field	 
	 String preifxValueInsidetextField=findWebElement(APT_wholeSale_Obj.APT_wholeSale.prefix_textField).getAttribute("value");
	 
	 int prefixSize=preifxValueInsidetextField.length(); 
	if(prefixSize==4) {
		prefix_code=preifxValueInsidetextField.substring(1);
	}else if(prefixSize<4) {
		 Reporter.log("Prefix value cannot be less than 4");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Prefix value cannot be less than 4. Value is displaying as "+preifxValueInsidetextField);
	}else if(prefixSize>4) {
		prefix_code=preifxValueInsidetextField.substring(1);
		 Reporter.log("Prefix value cannot be greater than 4");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Prefix value cannot be greater than 4. Value is displaying as "+preifxValueInsidetextField);
	}
	 
	 if((country.equals("NL (Netherlands)"))) {
		 prefix_code = "B" + prefix_code.substring(1);
	 }else {
		 Reporter.log("No changes made in refix code"); 
	 }

	 
//Gateway
	 waitForAjax();
	 click(APT_wholeSale_Obj.APT_wholeSale.gateway_Dropdown,"gateway_Dropdown");
	 selectByVisibleText(APT_wholeSale_Obj.APT_wholeSale.gateway_Dropdown,gateway,"Gateway");
	 waitForAjax();
	//selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.gateway_Dropdown, "Gateway", gateway);
	gatewayCode=gateway_code(gateway);
	
	WebElement trunktype=findWebElement(APT_wholeSale_Obj.APT_wholeSale.trunkType_Dropdown);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.trunkType_Dropdown);
	waitForAjax();
	
//Quality
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.quality_Dropdown, "Quality", quality);
	String quality_code=findWebElement(APT_wholeSale_Obj.APT_wholeSale.quality_Dropdown).getAttribute("value");
	
//Trunk Group Name
	String trunGroup=country_code+gatewayCode+voip_code+trunktype_code+CDR_code+prefix_code+"0"+quality_code;
	
	int totalLen=trunGroup.length();
	Reporter.log("Total lenth of 'Trunk Group' field is "+ totalLen);
	Reporter.log("----------------Trunk group name is "+ trunGroup);
	if(totalLen==13) {
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.trunkGroupName_TextField, "Trunk Group Name");
		primarytrunkGroupname=trunGroup;
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Trunk Group NAme' length is: "+totalLen);
		verifyExists(APT_wholeSale_Obj.APT_wholeSale.trunkGroupName_TextField, "trunkGroupName_TextField");
	}
	
//traffic Direction
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown, "Traffic Directions", trafficDirection);
	
//IP Address Type
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.IPaddresstype_Dropdown, "IP Address Type", ipAddresstype);

	WebElement traficDirection=findWebElement(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.trafficDirection_Dropdown);
	waitForAjax();
	
	
//Carrier IP originating
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.carrierIPoriginating_textField, carrierIPoriginating, "Carrier IP Originating (Address/Mask)");
	click(APT_wholeSale_Obj.APT_wholeSale.carrierIPoriginating_addButtton, ">>");
	GetTheValuesInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.carrierIPOriginating_addedValue_selectDropdownField, "Carrier IP Originating (Address/Mask)");
	
	
//Carrier IP Terminating
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_textField, carrierIPterminating, "Carrier IP Terminating(Address)");
	click(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_addButton, ">>");
	GetTheValuesInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.carrierIPterminating_addedValue_selectDropdownField, "Carrier IP Terminating (Address)");
	
	
//SIP Signalling Port
  if(voipProtocol.equalsIgnoreCase("SIP")) {
	  sendKeys(APT_wholeSale_Obj.APT_wholeSale.SIPsignallingport_textField, SIPsignallingPort, "SIP Signaling Port");
	  
	  //message displaying under "SIP Signalling Port" text field	
		//methodToFindMessagesUnderTextField(APT_wholeSale_Obj.APT_wholeSale.SIPsignallingPOrt_defaultValue_textvalue, "SIP Signalling Port", "Default Port:5060");
		
  }else {
	  ExtentTestManager.getTest().log(LogStatus.PASS, "'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-1'");
	  Reporter.log("'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-I'");
  }
	

//Splitting the Gateway functionality into 2  
  if(!gateway.contains("SBC")) {
	
	  if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
		  
		  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
		  
		//Internet Based Customer
			addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
			
		  
		  String vlanDefaultvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField).getAttribute("value");
		  if(vlanDefaultvalue.isEmpty()) {
			  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
		  }else {
			 
			  ExtentTestManager.getTest().log(LogStatus.PASS, "When 'Internet Based Customer' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue);
			  if(vlanTag.equalsIgnoreCase("null")) {
					
					//Sub Interface slot
					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
					
					if(subInterfaceSlot.equalsIgnoreCase("null")) {
						
						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue;
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField,"Sub Interface Name");
						
						//NIF Group  
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField, "NIF Group");
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
						
						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue;
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField, "Sub Interface Name");
						
						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						Reporter.log("NIF Group value is: "+NIFgroup);
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField,"NIFgrouopp_textField" );
					}
					
				}else {
					
					//VLAN Tag
					String vlanDefaultvalue_afterVlanediting=findWebElement(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField).getAttribute("value");

				//Sub Interface slot
					selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
					
					if(subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue_afterVlanediting;
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField,"Sub Interface Name");
						
						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue_afterVlanediting+NIGgroupdefaultValue_last;
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField,"NIF Group");
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
						
						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue_afterVlanediting;
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField, "Sub Interface Name");
						
						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue_afterVlanediting+NIGgroupdefaultValue_last;
						Reporter.log("NIF Group value is: "+NIFgroup);
						verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField,"NIF Group");
					}
				}
		  }
	  }
	 //Internet Based Customer checkbox not selected 
	  else {
		if(vlanTag.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' field is  a mandatory field and no values are passed as an input");
			
			//Sub Interface slot
			selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
			
			if(subInterfaceSlot.equalsIgnoreCase("null")) {
				
				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfacename_middle;
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField,"Sub Interface Name");
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+NIGgroupdefaultValue_last;
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField, "NIF Group");
			}
			else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
				
				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle;
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField, "Sub Interface Name");
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+NIGgroupdefaultValue_last;
				Reporter.log("NIF Group value is: "+NIFgroup);
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField, "NIF Group");
			}
			
		}else {
			
		//VLAN Tag
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField, vlanTag, "VLAN Tag");

		//Sub Interface slot
			selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
			
			if(subInterfaceSlot.equalsIgnoreCase("null")) {

				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTag;
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField,"Sub Interface Name");
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField, "NIF Group");
			}
			else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
				
				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanTag;
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.subInterfaceName_textField, "Sub Interface Name");
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
				Reporter.log("NIF Group value is: "+NIFgroup);
				verifyExists(APT_wholeSale_Obj.APT_wholeSale.NIFgrouopp_textField, "NIF Group");
			}
		}
		
	//Signalling port
		signallingPort( gateway);
	  }
  }
 
  else if(gateway.contains("SBC")) {
	  if(gateway.startsWith("FRA")) {
		  if(!internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  if(vlanTag.equalsIgnoreCase("null")) {
				 
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is a mandatory field. No values has been passed as an input");
			 
				//IP Interface
				 // verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterface_textField, "IP Interface");  //verify default values
				  
			//Address Context
				  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.AddressContext_textField, "Address Context");  //verify default values
				
				  
			//IP Interface Group
				//verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterfaceGroup_textField,"IP Interface Group");    //verify default values
				
				  
			//Signalling port
					signallingPort(gateway);	  
		
			  }
			  else if(!vlanTag.equalsIgnoreCase("null")) {
				 
				//VLAN Tag
					sendKeys(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField, vlanTag, "VLAN Tag");

					//IP Interface
					  ipInterface = ipInterfaceDEfaultValue +vlanTag;
					 // verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterface_textField,"IP Interface");  
					  
				//Address Context
					  addressContext=addressContextDefaultValue+vlanTag;
					  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.AddressContext_textField, "Address Context");  //verify default values
					
					  
				//IP Interface Group
					  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanTag;
					  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterfaceGroup_textField,"IP Interface Group");    //verify default values
			 
			  }
			  
		  }
		  else if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
			  Report.LogInfo("INFO", " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected", "PASS");
			  
				//Internet Based Customer
					addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
					
			  String vlanDefaultvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField).getAttribute("value");
			  if(vlanDefaultvalue.isEmpty()) {
					  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
			  }else {
				//VLAN tag  
//				  boolean VlanEnability=getwebelement(xml.getlocator("//locators/" + application + "/vlanTag_textField")).isEnabled();
//				  if(VlanEnability) {
//					  Log.info("VLAN Tag is enabled");
//					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
//				  }else {
//					  Log.info("VLAN Tag is disabled");
//					  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
//				  } 
			  }
			  
				//IP Interface
			  ipInterface = ipInterfaceDEfaultValue +vlanDefaultvalue;
			//  verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterface_textField, "IP Interface");  
			  
		//Address Context
			  addressContext=addressContextDefaultValue+vlanDefaultvalue;
			  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.AddressContext_textField,"Address Context");  //verify default values
			
			  
		//IP Interface Group
			  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanDefaultvalue;
			  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterfaceGroup_textField,"IP Interface Group");    //verify default values
		  }
	  }
	 else {
		 if(!internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  
			//VLAN tag  
				  boolean VlanEnability=findWebElement(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField).isEnabled();
				  if(VlanEnability) {
					  Reporter.log("VLAN Tag is enabled");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
				  }else {
					  Reporter.log("VLAN Tag is disabled");
					  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
				  }
				  
			//IP Interface
				  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterface_textField, "IP Interface");  //verify default values
				  
			//Address Context
				  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.AddressContext_textField,  "Address Context");  //verify default values
				
				  
			//IP Interface Group
				  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.ipInterfaceGroup_textField,  "IP Interface Group");    //verify default values
				
				  
			//Signalling port
					signallingPort(gateway);	  
					
			}
		 
		 if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  
			  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
			  
			//Internet Based Customer
				addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
				
		  String vlanDefaultvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField).getAttribute("value");
		  if(vlanDefaultvalue.isEmpty()) {
				  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
		  }else {
			//VLAN tag  
			  boolean VlanEnability=findWebElement(APT_wholeSale_Obj.APT_wholeSale.vlanTag_textField).isEnabled();
			  if(VlanEnability) {
				  Reporter.log("VLAN Tag is enabled");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
			  }else {
				  Reporter.log("VLAN Tag is disabled");
				  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
			  } 
		  }
		  
			//IP Interface
		  ipInterface = ipInterfaceDEfaultValue +vlanDefaultvalue;
		 // verifyExists( APT_wholeSale_Obj.APT_wholeSale.ipInterface_textField,"IP Interface");  
		  
	//Address Context
		  addressContext=addressContextDefaultValue+vlanDefaultvalue;
		  //verifyExists(APT_wholeSale_Obj.APT_wholeSale.AddressContext_textField,"Address Context");  //verify default values
		
		  
	//IP Interface Group
		  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanDefaultvalue;
		  //verifyExists( APT_wholeSale_Obj.APT_wholeSale.ipInterfaceGroup_textField,"IP Interface Group");    //verify default values
		 }
	 }
  }
  
//Signalling Zone
  if(internetBasedCustomer.equalsIgnoreCase("yes")) {
	  
	  verifyExists( APT_wholeSale_Obj.APT_wholeSale.signallingZone_textField, "Signaling Zone");
	  
	  sendKeys( APT_wholeSale_Obj.APT_wholeSale.signallingZone_textField,signallngZone, "Signaling Zone");
	  
  }else {
	  sendKeys(APT_wholeSale_Obj.APT_wholeSale.signallingZone_textField,signallngZone, "Signaling Zone");
  }
	


//Signalling Transport Protocol
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.signallingTransportProtocol_Dropdown, "Signalling Transport Protocol", signallingtransportProtocol);

	WebElement internetBasedCusotmerView=findWebElement(APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.internetBasedCustomer_checkbox);
	waitForAjax();
	
	
	if(signallingtransportProtocol.equalsIgnoreCase("sip-tls-tcp")) {
		
		//TLS Profile
		sendKeys(APT_wholeSale_Obj.APT_wholeSale.TLS_textField, TLSfield, "TLS Profile");
		
		//SRTP
		addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.srtp_checkbox, "SRTP", srtp);
	}
	
//Colt Signalling IP
	sendKeys( APT_wholeSale_Obj.APT_wholeSale.coltSignalingIP_textField, coltSignalingIP, "Colt Signaling IP");
	
//Media IP
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.mediaIP_textField, mediaIP,  "Media IP");
	
//Reuse NIF Group
	if(gateway.contains("SBC")) {
		Reporter.log("Reuse NIF Group checkbox will not dipslay, if 'gateway' selected as 'SBC'");
	}
	else {
		addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.reuseNIFgroup_checkbox, "Reuse NIF Group", reuseNIFgroup);
	}
	
	
//Reuse Sig Zone/Part
	addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.reuseSigZonePart_checkbox, "Reuse Sig Zone/Part", reuseSigZonePart);
	
//Call Admission Control
	addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.callAdmissionControl_checkbox, "Call Admission Control", callAdmissionControl);
	
	if(callAdmissionControl.equalsIgnoreCase("yes")) {
		
		//Call Limit
		selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.callLimit_Dropdown, "Call Limit", callLimit);
		
		if(callLimit.equalsIgnoreCase("Defined")) {
			sendKeys(APT_wholeSale_Obj.APT_wholeSale.limitNumber_textField, limitNumber, "Limit Number");
		}
	}
	
	WebElement signalingTransportprotocolLabelname=findWebElement(APT_wholeSale_Obj.APT_wholeSale.signalingTransportProtocol_labeName);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.signalingTransportProtocol_labeName);
	waitForAjax();
	
//Call Rate Limit
	addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.callrateLimit_checkbox, "Call Rate Limit", callrateLimit);
	if(callrateLimit.equalsIgnoreCase("yes")) {
		
		String callratelimitactualvalue=findWebElement(APT_wholeSale_Obj.APT_wholeSale.callRateLimitt_textField).getAttribute("value");
		Reporter.log(" 'Call rate Limit' value is displaying as "+callratelimitactualvalue);
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'Call rate Limit' value is displaying as "+callratelimitactualvalue);
	
		if(!callrateLimiteValue.equalsIgnoreCase("null")) {
			int i=Integer.parseInt(callrateLimiteValue);
				
			if(i>100) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "The CallRateLimit should be less 100 for all Trunks");
			}
			else if(i<=100){
				
				waitforPagetobeenable();
				Thread.sleep(1000);
				
				sendKeys(APT_wholeSale_Obj.APT_wholeSale.callRateLimitt_textField, callrateLimiteValue, "Call rate Limit");
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Call rate Limit' value is not edited");
			Reporter.log("'Call rate Limit' value is not edited");
		}
	}
	
	
//Source Address Filtering
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.sourceAddressFiltering_Dropdown, "Source Address Filtering", sourceAddressFiltering);
	
//100rel Support
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.relsupport_Dropdown, "100rel Support", relSupport);
	
//SIP Session Keepalive Timer(Sec)
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.sipSessionKeepAliverTimer_textField, sipSessionkeepAliveTimer, "SIP Session Keepalive Timer(Sec)");

	//Text message under "SIP Session Keepalive timer"
	//	methodToFindMessagesUnderTextField(APT_wholeSale_Obj.APT_wholeSale.defaultTextMessageUnderSIPsessionTimer, "SIP Session Keepalive Timer(Sec)", "Default SIP Session Keepalive Timer (sec): 1800");
	
		
		WebElement reusenifgroup=findWebElement(APT_wholeSale_Obj.APT_wholeSale.reuseNIFgroup_checkbox);
		scrollDown(APT_wholeSale_Obj.APT_wholeSale.reuseNIFgroup_checkbox);
		waitForAjax();
		
//Retry Invite
	sendKeys(APT_wholeSale_Obj.APT_wholeSale.retryinvite_textField, retryInvite, "Retry Invite");
	
	//Text message under "Retry Invite" field
	//methodToFindMessagesUnderTextField(application, "defaultTextMessageUnderretryInvite", "Retry Invite", "Default Retry Invite :2");
	
//Route Priority
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.routepriority_Dropdown, "Route Priority", routePriority);
	
//Address Reachability
	selectValueInsideDropdown(APT_wholeSale_Obj.APT_wholeSale.addressReachability_Dropdown, "Address Reachability", addressReachability);
	
	WebElement retryinviteView=findWebElement(APT_wholeSale_Obj.APT_wholeSale.retryinvite_textField);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.retryinvite_textField);
	waitForAjax();

//E164GlobalProfile
	addTrunk_existingNewDropdownField(globalProfile_ExistingSelection, globalProfile_newSelection, globalProfile_ExistingValue, 
			globalProfile_newValue, APT_wholeSale_Obj.APT_wholeSale.globalProfile_Dropdown , APT_wholeSale_Obj.APT_wholeSale.globalProfile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.globalProfile_textField, "E164GlobalProfile");
	
	
//E164LocalProfile	
	//addTrunk_existingNewDropdownField(localProfile_existingFieldSelection, localProfile_newFieldSelection, localProfile_existingvalue,
			//localProfile_newvalue, APT_wholeSale_Obj.APT_wholeSale.localProfile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.localProfile_checkbox, APT_wholeSale_Obj.APT_wholeSale.localProfile_TextField, "E164LocalProfile");
	click(APT_wholeSale_Obj.APT_wholeSale.localProfile_checkbox,"localProfile_checkbox");
	
//COS profile
	//addTrunk_existingNewDropdownField(COSprofile_existingFieldSelection, COSprofile_newFieldSelection, COSprofile_existingValue,
			//COSprofile_newValue, APT_wholeSale_Obj.APT_wholeSale.COSprofile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.COSprofile_checkbox, APT_wholeSale_Obj.APT_wholeSale.COSprofile_TextField, "COS Profile");
	
	
//PSPG Name
	addTrunk_existingNewDropdownField(PSPGname_existingFieldSelection, PSPGname_newFieldSelection, pspgName_existingValue,
			pspgName_newValue, APT_wholeSale_Obj.APT_wholeSale.PSPSGname_Dropdown, APT_wholeSale_Obj.APT_wholeSale.PSPGname_Checkbox, APT_wholeSale_Obj.APT_wholeSale.PSPGname_TextField, "PSPG Name");
	
	
//Preferred  PSP
	//addTrunk_existingNewDropdownField(prefferedPSP_existingFieldSelection, prefferedPSP_newFieldSelection, preferredPSP_exitingvalue,
			//preferredPSP_newvalue, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_Dropdown, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_checkbox, APT_wholeSale_Obj.APT_wholeSale.preferredPSP_TextField, "Preferred PSP");
	click(APT_wholeSale_Obj.APT_wholeSale.preferredPSP_checkbox,"preferredPSP_checkbox");

	WebElement globalProfileView=findWebElement(APT_wholeSale_Obj.APT_wholeSale.globalProfile_textField);
	scrollDown(APT_wholeSale_Obj.APT_wholeSale.globalProfile_textField);
	waitForAjax();
	
	
//Carrier
	//addTrunk_existingNewDropdownField(carrier_existingFieldSelection, carrier_newFieldSelection, carrier_existingValue,
			//carrier_newValue, APT_wholeSale_Obj.APT_wholeSale.carriers_Dropdown, APT_wholeSale_Obj.APT_wholeSale.carriers_checkbox, APT_wholeSale_Obj.APT_wholeSale.carriers_TextField, "Carrier");
	click(APT_wholeSale_Obj.APT_wholeSale.carriers_checkbox,"carriers_checkbox");
	
//IP Signalling Profile
	//addTrunk_existingNewDropdownField(IPsignallingProfile_existingFieldSelection, IPsignallingProfile_newFieldSelection, IPsignallingProfile_existingValue,
			//IPsignallingProfile_newValue, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.IPsignallingProfile_textField, "IP Signaling Profile");
	
	
//Egress IP Signaling Profile
	addTrunk_existingNewDropdownField(EgressIpsignal_existingFieldSelection, EgressIpsignal_newFieldSelection, EgressIPsignal_existingValue,
			EgressIPsignal_newValue, APT_wholeSale_Obj.APT_wholeSale.EgressIpsignal_Dropdown, APT_wholeSale_Obj.APT_wholeSale.EgressIPsignal_checkbox, APT_wholeSale_Obj.APT_wholeSale.EgressipSignal_TextField, "Egress IP Signaling Profile");
	
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	

//In DM/PM rule
	addTrunk_existingNewDropdownField(InDMPMrule_existingFieldSelection, InDMPMrule_newFieldSelection, InDMPMrule_existingValue,
			InDMPMrule_newValue,  APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_Dropdown, APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_checkbox, APT_wholeSale_Obj.APT_wholeSale.InDMPMrule_TextField, "In DM/PM rule");
	
	
//Out DM/PM rule
	addTrunk_existingNewDropdownField(OutDMPMrule_existingFieldSelection, OutDMPMrule_newFieldSelection, OutDMPMrule_existingValue,
			OutDMPMrule_newValue, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_Dropdown, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_checkbox, APT_wholeSale_Obj.APT_wholeSale.OutDMPMrule_TextField, "Out DM/PM rule");
	
	
//Feature Control Profile	
	addTrunk_existingNewDropdownField(featureControlprofile_existingFieldSelection, featureControlprofile_newFieldSelection, featureControlprofile_existingValue,
			featureControlprofile_newValue, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_Dropdown, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_Checkbox, APT_wholeSale_Obj.APT_wholeSale.featureControlprofile_TextField, "Feature Control Profile");
	
	
//Local Ring Back Tone
	//addTrunk_existingNewDropdownField(localRingBackTone_existingFieldSelection, localRingBackTone_newFieldSelection, localRingBackTone_existingValue,
			//localRingBackTone_newValue, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_Dropdown, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_checkbox, APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_TextField, "Local Ring Back Tone");
click(APT_wholeSale_Obj.APT_wholeSale.localRingBackTone_checkbox,"localRingBackTone_checkbox");
	
//Create Lower Case Routes
	if((country.equals("DK (Denmark)")) || (country.equals("IT (Italy)")) || (country.equals("IE (Ireland)")) || (country.equals("PT (Portugal)")) ||
			(country.equals("ES (Spain)")) || (country.equals("SE (Sweden)")) || (country.equals("CH (Switzerland)")) || (country.equals("UK (UK)")))
	{
		addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.createLowerCaseRoute_checkbox, "Create Lower Case Routes", createLowerCaseRoutervalue);
	}
	else
	{
		//addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.createLowerCaseRoute_checkbox, "Create Lower Case Routes", createLowerCaseRoutervalue);
	}
	
	
	
//PSX Manual Configuration	
	addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.PSXmanualConfig_checkbox, "PSX Manual Configuration", PSXmanualConfigvalue);
	
	
	if(gateway.contains("SBC")) {
		//Manual Configuration on SBC
		addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.SBCmanualconfig_checkbox, "Manual Configuration On SBC", SBCmanualconfigValue);
	}else {
		//Manual Configuration On GSX
		addCheckbox_commonMethod(APT_wholeSale_Obj.APT_wholeSale.GSXmanualConfig_checkbox, "Manual Configuration On GSX", GSXmanualConfigvalue);
	}
	
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitForAjax();
	
	click( APT_wholeSale_Obj.APT_wholeSale.OKbutton, "OK");
	
}
}
