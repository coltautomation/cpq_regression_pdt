package pageObjects.ncObjects;

public class ModifyOrderObj {
	public static class ModifyOrder{
		public static final String OrderNumber="@xpath=//td[@id='vv_-1']/span";
		public static final String ProductInstTab="@xpath=//a[text()='Product Instances']";
		public static final String ProductOrdrTab="@xpath=//a[text()='Product Orders']";
		public static final String IpProductInstNumber="@xpath=//span[contains(text(),'IP Access Product Instance')]";
		public static final String ProductInstNumber="@xpath=//span[contains(text(),'Ethernet Connection Product Instance')]";
		public static final String EthernetConnProdOrderNumber="@xpath=//span[contains(text(),'Ethernet Connection Product Order #')]";
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";
		public static final String NameFiltering="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*";
		public static final String FilterSelectName="@xpath=//*[@class='inputs']";
		public static final String FilterInputValue="@xpath=//*[@name='inp']";
		public static final String ApplyButton="@xpath=//*[@class='FilterButtons'] //*[text()='Apply']";
		public static final String ModifyCompositOrder="@xpath=//a[text()='Create Modify Product Order']";
		public static final String InflightSoftProdOrder="@xpath=//a[text()='Soft InFlight Product Order']";
		public static final String LinkforOrder="@xpath=//a[contains(text(),'Order #')]";
		public static final String ModifiedNumber="@xpath=//a[contains(text(),'Order #')]";
		public static final String UnderlyingModifiedIpOrder="@xpath=//span[contains(text(),'Modify IP Access Product')]/parent::a";
		public static final String UndelyingModifiedOrder="@xpath=//span[contains(text(),'Modify Ethernet Connection')]/parent::a";
		public static final String UndelyingInfligtOrder="@xpath=//span[contains(text(),'In-flight New Ethernet Connection')]/parent::a";
		public static final String EditDesc="@xpath=//*[(@aria-label='Control+e')]";
		public static final String OrderDescription="@xpath=//*[@name='common_descr']";
		public static final String Edit="@xpath=//a[text()='Edit']";
		public static final String ServiceBandwidth="@xpath=//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//input[@type='text']";
		public static final String IpServiceBandwidth="@xpath=//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//select";
		public static final String Update="@xpath=//a[text()='Update']";
		public static final String OrderTab="@xpath=//a[text()='Orders']";
		public static final String ModifySubIpOrder="@xpath=//a/span[contains(text(),'Modify IP Access Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String ModifySuborder="@xpath=//a/span[contains(text(),'Modify Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String InflightSuborder="@xpath=//a/span[contains(text(),'In-flight New Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Decompose="@xpath=//a[text()='Decompose']";
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";
		public static final String ModifyEndSiteProductAend="@xpath=(//a/span[contains(text(),'Modify End Site Product')])[1]/parent::*";
		public static final String ModifyEndSiteProductBend="@xpath=(//a/span[contains(text(),'Modify End Site Product')])[2]/parent::*";
		public static final String EndCheck="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*[1]";
		public static final String GeneralInformationTab="@xpath=//a[text()='General Information']";
		public static final String AddFeaturelink="@xpath=//a[text()='Add Feature']";
		public static final String TypeofFeature="@xpath=//a[text()='Add Feature']/following-sibling::*//textarea";
		public static final String SelectFeature="@xpath=//input[@value='Select']";
		public static final String AccessPortLink="@xpath=//span[text()='Access Port']/parent::*";
		public static final String AccessportRole="@xpath=//td/a[text()='Port Role']/parent::*/following-sibling::*//select";
		public static final String VLANLink="@xpath=//span[text()='VLAN']/parent::*";
		public static final String VlanTagId="@xpath=//td/a[text()='VLAN Tag ID']/parent::*/following-sibling::*//input";
		public static final String Ethertype="@xpath=(//*[text()[contains(.,'Ethertype')]]/parent::*/following-sibling::*//input)[3]";
		public static final String VlanTaggingMode="@xpath=//td/a[text()='VLAN Tagging Mode']/parent::*/following-sibling::*//select";
		public static final String IpAddressingLink="@xpath=//span[text()='IP Addressing']/parent::*";
		public static final String IpAddressingFormat="@xpath=//td/a[text()='IP Addressing Format']/parent::*/following-sibling::*//select";
		public static final String Ipv4AddressingType="@xpath=//td/a[text()='IPv4 Addressing Type']/parent::*/following-sibling::*//select";
		public static final String Ipv6AddressingType="@xpath=//td/a[text()='IPv6 Addressing Type']/parent::*/following-sibling::*//select";
		public static final String DeleteFeature="@xpath=//a[text()='Delete Feature']";
		public static final String Bgp4FeedLink="@xpath=//span[text()='BGP4 Feed']/parent::*";
		public static final String TypeofAS="@xpath=//td/a[text()='Type of AS']/parent::*/following-sibling::*//select";
		public static final String ASNumber="@xpath=//td/a[text()='AS Number']/parent::*/following-sibling::*//input";
		public static final String ProviderAggIpv4Link="@xpath=(//span[contains(text(),'Provider Aggregated')]/parent::*)";
		public static final String ProviderAggIpv6Link="@xpath=(//span[contains(text(),'Provider Aggregated')]/parent::*)";
		public static final String SiebelCompId="@xpath=//td/a[text()='Siebel Component ID']/parent::*/following-sibling::*//input";

																//span[contains(text(),'Provider Aggregated IPv4')]/parent::*
		public static final String StartProccessing="@xpath=//a[text()='Start Processing']";
			
		//-------------Task tab-----------
			


		public static final String TaskTab="@xpath=//a[text()='Tasks']";
		public static final String ExecutionFlowlink="@xpath=//a[text()='Execution Flow']/parent::*/following-sibling::*//a[2]";
		public static final String TaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[New]')]";
		public static final String ModTaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[Modify]')]";
		public static final String InflightTaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[In-flight New]')]";
		public static final String SerialTaskName="@xpath=//td/a[text()='Name']/parent::*/following-sibling::*[1]";
		public static final String UpdateAntSerialNumber="@xpath=//a[text()='Update ANT Serial Number']";
		public static final String UpdateSerialNumber="@xpath=//a[text()='Update Serial Number']";
		public static final String Workitems="@xpath=//a[text()='Work Items']";
		public static final String Scheduler="@xpath=//a[text()='Scheduler']";
		public static final String Errors="@xpath=//a[text()='Errors']";
		public static final String TaskReadytoComplete="@xpath=//td[text()='Ready']/parent::*/td//a";
		public static final String Complete="@xpath=//a[@tooltip='Complete']";
		public static final String TransComplete="@xpath=//a[text()='Complete']";
		public static final String AccessNetworkElement="@xpath=//td/a[text()='Access Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AccessPort="@xpath=//td/a[text()='Access Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PENetworkElement="@xpath=//*[text()='PE Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PePort="@xpath=//*[text()='PE Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String VCXController="@xpath=//*[text()='VCX Controller']/parent::*/following-sibling::*//input[@type='text']";
		public static final String OAMProfile="@xpath=//*[text()='OAM Profile']/parent::*/following-sibling::*//input[@type='text']";
		//public static final String UpdateAntSerialNumber="@xpath=//a[text()='Update ANT Serial Number']";
			
		public static final String arrStr1 = "@xpath =//a/span[contains(text(),'";
		public static final String arrStr2 = "')]/parent::*/parent::*/parent::*/td//input))";
		
		public static final String Order1 = "@xpath =//a/span[contains(text(),'";
		public static final String Order2 = "')]/parent::*))";

			
	}
}
