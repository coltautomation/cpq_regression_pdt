package testHarness.aptFunctions;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;

import testHarness.commonFunctions.ReusableFunctions;

public class APT_MCS_CreateAccessCoreDevice_AccessRouter extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	//public static WebDriver webDriver=null;
	
public void webelementpresencelogger(WebElement ele, String fieldname) {
		
		try {	
			boolean flag = ele.isDisplayed();
			//Log.info("element presence state : " + flag);
			Report.LogInfo("INFO", "element presence state : " + flag, "NA");
			
			if (flag) {

				Report.LogInfo("INFO", "Step: expected field is displayed " +fieldname, "PASS");
			} 
			else {

				Report.LogInfo("INFO", "Step: expected field is not displayed " +fieldname, "FAIL");
			}
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			
			Report.LogInfo("INFO", "Step: expected field is not displayed " +fieldname, "FAIL");
		}

		}
	
	public void navigatetomanagecoltnetwork() throws InterruptedException, IOException
	{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink, "Manage Colt Network Link");
		mouseMoveOn(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.mcnlink);
		waitForAjax();
	}
	
	public void navigatetocreateaccesscoredevicepage() throws InterruptedException, IOException
	 {		
	verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink, "Create Access Core Device Link");
	click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.createaccesscoredevicelink, "Create Access Core Device Link");
	waitForAjax();
     }
	
	public void verifydevicecreation_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		
		waitForAjax();
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.WarningMesassage_site, "Warning Mesassage Site");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_city, "Warning Mesassage City");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_Country, "Warning Mesassage Country");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_name, "Warning Mesassage Name");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_deviceType, "Warning Mesassage Device Type");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_vendor, "Warning Mesassage vendor");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.warningMessage_routerId, "Warning Mesassage Router Id");
		
		
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Name");
		String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
		String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RouterID"); 
		String IosXr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IOSXR");
		String Full_IQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Full_IQNET");
		String ModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modular_MSP");
		String telnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telnet");
		String ssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SSH");
		String Snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp3");
		String Snmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp2C");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmProNewValue");
		String Snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmprwNewValue");
		String SnmpUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3UsernameNewValue");
		String SnmpAuthPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3AuthpasswordNewValue");
		String SnmpPrivPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3PrivpasswordNewValue");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String Management_Address= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address");
		String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
		String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
		String ExSite =	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
		String ExSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSiteValue");
		String ExPremise= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
		String ExPremiseValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremiseValue");
		String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
		String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
		String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
		
		String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
		String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
		String newSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
		
		String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
		String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
		String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
		
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "Name Text Field");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,Name,"Name Text Field");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, "Device Type Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput,"Device Type Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput1 +deviceType+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput2, "Device Type Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicetypeinput, deviceType, "Device Type Input");
		
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput);
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput1 +VendorModel+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput2, "Vendor Model Input");
		//selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, VendorModel, "Vendor Model Input");
		addDropdownValues_commonMethod("Vendor/Model", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, VendorModel);
		if(Full_IQNET.equalsIgnoreCase("Yes"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader, "Full IQNET Alert Header");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
		}
		//else
		//{
		//	isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, null);
		//}
		
		if(ModularMSP.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
		}
		//else
		//{
			//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, null);
		//}
		if(IosXr.equalsIgnoreCase("Yes"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
		}
		//else
		//{
			//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox,  null);
		//}
		
		if(telnet.equalsIgnoreCase("Yes") && ssh.equalsIgnoreCase("No"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
		}
		
		else if(telnet.equalsIgnoreCase("No") && ssh.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
		}
		
		if(Snmp2C.equalsIgnoreCase("Yes") && Snmp3.equalsIgnoreCase("No"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
			//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield)==false)
			//{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, "snmpro Textfield");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,Snmpro);
			//}
			//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield)==false)
			////{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, "snmprw Textfield");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield,Snmprw);
			//}
		}
		
		else if(Snmp2C.equalsIgnoreCase("No") && Snmp3.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
			
			//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username)==false)
			//{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username,SnmpUser);
			//}
			//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword)==false)
			//{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword, "snmp AuthPasswd");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,SnmpAuthPasswd);
			//}
			//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword)==false)
			//{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword, "snmp PrivPasswd");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,SnmpPrivPasswd);
			//}
		}
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, "Router Id Textfield");
		sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, RouterId );
		
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput,"Country Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput1 +country+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput2,"Country Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, country, "Country Input");
		
		//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox)==true)
		//{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox, "Management Address Textbox");
			clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox);
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.managementaddresstextbox,Management_Address);
		//}
		
		if(ExCity.equalsIgnoreCase("Yes")&& newCity.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,"City dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput1 +ExCityValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput2,"City dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, ExCityValue, "City dropdown Input");
		
		}
		else if(ExCity.equalsIgnoreCase("No")&& newCity.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,newCityName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,newCityCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,newSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,newSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,newPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,newPremiseCode);
			
		}
		if(ExSite.equalsIgnoreCase("Yes")&& newSite.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,"Site Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput1 +ExSiteValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput2,"Site dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, ExSiteValue, "Site dropdown Input");
		
		}
		else if(ExSite.equalsIgnoreCase("No")&&newSite.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addSiteToggleSelected,newSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addSiteToggleSelected,newSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addSiteToggleSelected,newPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addSiteToggleSelected,newPremiseCode);
		}
		if(ExPremise.equalsIgnoreCase("Yes")&& newPremise.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,"Premise Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput1 +ExPremiseValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput2,"Premise dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, ExPremiseValue, "Premise Dropdown Input");
		
		}
		else if(ExPremise.equalsIgnoreCase("No")&& newPremise.equalsIgnoreCase("yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,newPremiseName, "Premise Name Inputfield");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,newPremiseCode,"Premise Code Inputfield");
		}
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		Reusable.waitForAjax();
    	
    	
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		Reusable.waitForAjax();
		}
	
	
	 public void verifyDeviceCreationMessage() throws InterruptedException, IOException
	 {	Reusable.waitForAjax();
		
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg, "Device created successfully");
	
		 String deviceCreation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.devicecreationsuccessmsg,"device creation successmsg");
		 String expected_deviceUpdation="Device created successfully";
		 if(deviceCreation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
		 }
		 else{
			 Report.LogInfo("INFO","Device not updated: "+deviceCreation_Msg , "FAIL");
		 }
		 
		 Reusable.WaitforCPQloader();
		 
	 }
	 
	 public void verifyDeviceUpdationSuccessMessage() throws InterruptedException, IOException
	 {
		 Reusable.waitForAjax();
			
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation, "Device updated successfully");
		 String deviceUpdation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_deviceUpdation,"success Message_device Updation");
		 String expected_deviceUpdation="Device updated successfully";
		 if(deviceUpdation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
		 }
		 else{
			 Report.LogInfo("INFO","Device not updated: "+deviceUpdation_Msg , "FAIL");
		 }
		 
	 }
	 /*
	 public void routerPanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	 {
		 String Command_ipv4 = DataMiner.fngDevice updated successfullyetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv4"); 
		 String Command_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"command_ipv6");
		 String vrf_Ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrf_Ipv4");
		 String vrf_Ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vrf_Ipv6");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown, "Command IPV4_dropdown");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown,"Command IPV4_dropdown");
		 ClickonElementByString("//div[text()='"+ Command_ipv4 +"']", 30);
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField, "commandIPv4 Vrf Textfield");
		 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField,vrf_Ipv4,"commandIPv4 Vrf Textfield");
		 
		 //verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield, "commandIPv4 Hostname Textfield");
		 //sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,newPremiseCode,"commandIPv4 Hostname Textfield");

		 //verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4, "Execute Button_Ipv4");
		 //click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4,"Execute Button_Ipv4");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown, "Command IPV6_dropdown");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown,"Command IPV6_dropdown");
		 ClickonElementByString("//div[text()='"+ Command_ipv6 +"']", 30);
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField, "commandIPv6 Vrf Textfield");
		 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,vrf_Ipv6,"commandIPv6 Vrf Textfield");
		 
		 //verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield, "commandIPv6 Hostname Textfield");
		 //sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield,newPremiseCode,"commandIPv6 Hostname Textfield");
		 
		// verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6, "Execute Button_Ipv6");
		// click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6,"Execute Button_Ipv6");
		 
	 }
	 */
	 public void executeCommandAndFetchTheValue() throws InterruptedException, IOException
	 {
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6, "Execute Button_Ipv6");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6,"Execute Button_Ipv6");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");
		 String result_textArea_IPv6=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");
		 Report.LogInfo("INFO", "Result TextArea in IPv6 shows: "+result_textArea_IPv6, "INFO");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4, "Execute Button_Ipv4");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4,"Execute Button_Ipv4");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");
		 String result_textArea_IPv4=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea, "Result TextArea");
		 
		 Report.LogInfo("INFO", "Result TextArea in IPv4 shows: "+result_textArea_IPv4, "INFO");
	 }
	 
	 public void managelink() throws InterruptedException, IOException {
		 scrollUp() ;
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage, "Manage Link View Device page");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,"Manage Link View Device page");
		
		 waitForAjax();
		}
	 
	 public void verifydeviceDelete_AccessRouter() throws IOException, InterruptedException 
	 {			
		 scrollUp() ;
		 refreshPage();
		 waitForAjax();
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
		 
		 waitForAjax();
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete, "Delete Device");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Delete,"Delete Device");

		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.delete_alertpopup, "Delete Alert Popup");
		
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton, "Delete Button");
		 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.DeleteConfirmButton,"Delete Button");
		 
		 waitForAjax();
		 
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg, "Device created successfully");
			
		 String deviceCreation_Msg=getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.deleteDeviceConfMsg,"delete Device Conf Msg");
		 String expected_deviceUpdation="Device successfully deleted.";
		 if(deviceCreation_Msg.equals(expected_deviceUpdation))
		 {
			 Report.LogInfo("INFO",expected_deviceUpdation , "PASS");
		 }
		 else{
			 Report.LogInfo("INFO","Device not deleted: "+deviceCreation_Msg , "FAIL");
		 }
		 
	    }
	 
	 public void verifyDefaultSelection_connectivityprotocol_ssh() throws InterruptedException, IOException
	 {
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "ssh Radio Button");
		 isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "ssh Radio Button");
		 
		 
	 }
	 
	 public void verifyDefaultSelection_connectivityprotocol_telnet() throws InterruptedException, IOException
	 {
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
		 isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
	 }
	 
	
	 public void verifEditedValue_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	 {   
		 String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editdeviceName");
		 String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
		 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editVendorModel");
		 String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editRouterID");
		 String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editModularMSP");
		 String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editFullIQNET");
		 String editIOSXR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editIOSXR");
		 String editTelnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editTelnet");
		 String editSSH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp2C");
		 String Telnet ="telnet";
		 String SSH ="ssh";
		 String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmProNewValue");
		 String editSnmprwNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmprwNewValue");
		 String editSnmpv3UsernameNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3UsernameNewValue");
		 String editSnmpv3AuthpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3AuthpasswordNewValue");
		 
		 String editSnmpv3PrivpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3PrivpasswordNewValue");
		 
		 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editManagementAddress");
		 
		 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
		 
		 String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
		 String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
		 String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
		 String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
		 
		 String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
		 String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
		 String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
		 String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
		 
		 String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
		 String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
		 String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
		 
		 String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
		 String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
		 String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
		 String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");
			
		 //Device name
		 
		 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab, "Name Tab");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab);
			compareText("Name",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab,editDevicename);
			
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType, "Device Type");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType);//String deviceType =
			//compareText("Device Type",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType,deviceType);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model);
			compareText("Vendor/Model",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model,editVendorModel);
			
			if(editFullIQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET Checkbox");
			compareText("Full IQNET",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "true");
			
			}
			
			//if(editModularMSP.equalsIgnoreCase("Yes"))
			//{
				//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Modular MSP Checkbox");
				//compareText("Modular MSP",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Yes");
			//}
		
			if(editIOSXR.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "IOSXR Checkbox");
			compareText("IOS-XR",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "Yes");
			}
			
			if(editTelnet.equalsIgnoreCase("Yes") && editSSH.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,Telnet);
				
			}
			
			else if(editTelnet.equalsIgnoreCase("No") && editTelnet.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,SSH);
			}
			
			if(editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw, "snmpro Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmprw",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw,editSnmprwNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro, "snmprw Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmpro",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro,editSnmProNewValue);
			
			}
			else if(editSnmp2C.equalsIgnoreCase("No") && editSnmp3.equalsIgnoreCase("Yes"))
			{
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username, "SNMP priv UserName");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmp v3 Username",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username,editSnmpv3UsernameNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass, "SNMP priv Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmp v3 Priv password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass,editSnmpv3PrivpasswordNewValue);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass, "SNMP Auth Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass);
				compareText("Snmp v3 Auth password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass,editSnmpv3AuthpasswordNewValue);
				
			}
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId, "Router Id");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId);
			compareText("Router Id",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId,editRouterID);
			if(!editCountry.equalsIgnoreCase("Null"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country, "Country");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country);
			compareText("Country",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country,editCountry);
			compareText_InViewPage1("Country", editCountry);
			}
			{
				Report.LogInfo("Country", "Country value not been updated", "PASS");
			}
			//verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd);
			//compareText("Management Address",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd,editManagementAddress);
			//compareText_InViewPage("Management Address", editManagementAddress);
			
			if(editExistingCity.equalsIgnoreCase("Yes") && editNewCity.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,editNewCityName);
			}
			else if(editExistingCity.equalsIgnoreCase("No") && editNewCity.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,editNewCityName);
			}

			if(editExistingSite.equalsIgnoreCase("Yes") && editNewSite.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,editNewSiteName);
			}
			else if(editExistingSite.equalsIgnoreCase("No") && editNewSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,editNewSiteName);
			}
			if(editExistingPremise.equalsIgnoreCase("Yes") && editNewPremise.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,editNewPremiseName);
			}
			else if(editExistingPremise.equalsIgnoreCase("No") && editNewPremise.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,editNewPremiseName);
			}
		 
	}
	
	 public void fetchDeviceInterface_viewdevicepage() throws InterruptedException, IOException {
			waitForAjax();
			webDriver.findElement(By.tagName("body")).sendKeys(Keys.HOME); 
			//scrollUp() ;
			waitForAjax();
			//scrollUp() ;
			 
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
			 
			 waitForAjax();
			 
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage, "Fetch Device Interfaces");
			 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchDeviceinterfacelink_viewDevicePage,"Fetch Device Interfaces");
			
			 waitForAjax();
			 
				String expectedValue="Fetch interfaces started successfully. Please check the sync status of this device";
				boolean successMessage=false;
				
				successMessage=isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert);
				//successMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).isDisplayed();
				
				String actualMessage = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.successMessage_Alert,"success Message_Alert");
				//String actualMessage=getwebelement(xml.getlocator("//locators/" + application + "/successMessage_Alert")).getText();
				
				
				if(successMessage) {
					
					if(actualMessage.isEmpty()) {
						Report.LogInfo("INFO", "Success message is not displaying", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Success message is not displaying");
					}
					if(actualMessage.contains(expectedValue)) {
						
						verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface, "Interfaces click here Link");
						click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
						 waitForAjax();
					
					}
					else if (actualMessage.equalsIgnoreCase(expectedValue)) {
						
						//click on the 'click here' link
						verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface, "Interfaces click here Link");
						click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.ClickhereLink_fetchInterface,"Interfaces click here Link");
						 waitForAjax();
					}
					else {
						//Report.LogInfo("INFO", "Error in fetching interface: "+actualMessage, "FAIL");
						//ExtentTestManager.getTest().log(LogStatus.FAIL, "Success message is not displaying"+actualMessage);

						//Action Button Click 
							verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
							 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action,"Action");
							 waitForAjax();
						//click on 'Manage' link
							 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage, "manage Link_view Devicepage");
							 click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.manageLink_viewDevicepage,"manage Link view Devicepage");
							
							 waitForAjax();
					}
						
					
				}else {
					ExtentTestManager.getTest().log(LogStatus.PASS, " After clicking on 'Fetch Device Interface' link, success Message is not displaying");
					Report.LogInfo("INFO", " After clicking on 'Fetch Device Interface' link, success Message is not displaying", "PASS");
				}
		}
	 
	 public void verifyenteredValue_forDeviceCreation(String testDataFile, String sheetName, String scriptNo,
				String dataSetNo) throws InterruptedException, IOException {
		 	
		 	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Name");
			String deviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VendorModel");
			String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RouterID"); 
			String IosXr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IOSXR");
			String Full_IQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Full_IQNET");
			String ModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modular_MSP");
			String telnet = "telnet";
			String ssh = "ssh";
			String Snmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp3");
			String Snmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmp2C");
			String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmProNewValue");
			String Snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SnmprwNewValue");
			String SnmpUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3UsernameNewValue");
			String SnmpAuthPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3AuthpasswordNewValue");
			String SnmpPrivPasswd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Snmpv3PrivpasswordNewValue");
			String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
			String Management_Address= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management_Address");
			String ExCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCity");
			String ExCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingCityValue");
			String ExSite =	DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSite");
			String ExSiteValue =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingSiteValue");
			String ExPremise= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremise");
			String ExPremiseValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingPremiseValue");
			String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCity");
			String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityName");
			//String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewCityCode");
			
			String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSite");
			String newSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteName");
			///String newSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewSiteCode");
			
			String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremise");
			String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseName");
			//String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NewPremiseCode");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab, "Name Tab");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab);
			compareText("Name",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab,Name);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType, "Device Type");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType);//String deviceType =
			compareText("Device Type",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType,deviceType);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model);
			compareText("Vendor/Model",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model,VendorModel);
			
			if(Full_IQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET Checkbox");
			compareText("Full IQNET",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "true");
			
			}
			/*
			if(ModularMSP.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Modular MSP Checkbox");
				compareText("Modular MSP",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ModularMSP, "Yes");
			}
			*/
			if(IosXr.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "IOSXR Checkbox");
			compareText("IOS-XR",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_IOS_XR, "Yes");
			}
			if(telnet.equalsIgnoreCase("Yes") && ssh.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,telnet);
			}
			
			else if(telnet.equalsIgnoreCase("No") && ssh.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
				compareText("Connectivity Protocol",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity,ssh);
			}
			
			if(Snmp2C.equalsIgnoreCase("Yes") && Snmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw, "snmpro Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmprw",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmprw,Snmprw);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro, "snmprw Textfield");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmpro",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_snmpro,Snmpro);
			
			}
			else if(Snmp2C.equalsIgnoreCase("No") && Snmp3.equalsIgnoreCase("Yes"))
			{
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username, "SNMP priv UserName");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
				compareText("Snmp v3 Username",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username,SnmpUser);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass, "SNMP priv Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
				compareText("Snmp v3 Priv password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass,SnmpPrivPasswd);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass, "SNMP Auth Password");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass);
				compareText("Snmp v3 Auth password",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass,SnmpAuthPasswd);
				
			}
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId, "Router Id");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId);
			compareText("Router Id",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId,RouterId);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Country");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET);
			//compareText("Country",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET,country);
			compareText_InViewPage1("Country", country);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
			//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd);
			//compareText("Management Address",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd,Management_Address);
			compareText_InViewPage1("Management Address", Management_Address);
			
			if(ExCity.equalsIgnoreCase("Yes") && newCity.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,ExCityValue);
			}
			else if(ExCity.equalsIgnoreCase("No") && newCity.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("City",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City,newCityName);
			}

			if(ExSite.equalsIgnoreCase("Yes") && newSite.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,ExSiteValue);
			}
			else if(ExSite.equalsIgnoreCase("No") && newSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Site",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site,newSiteName);
			}
			if(ExPremise.equalsIgnoreCase("Yes") && newPremise.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,ExPremiseValue);
			}
			else if(ExPremise.equalsIgnoreCase("No") && newPremise.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
				//getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
				compareText("Premise",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise,newPremiseName);
			}
		
	 }
	/* 
	 public void GetValues() throws InterruptedException, IOException
		{		
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab, "Name Tab");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab);
			//compareText("Name",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_NameTab);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType, "Device Type");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_DeviceType);//String deviceType =
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Vendor_Model);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity, "Connectivity Protocol");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Connectivity);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username, "SNMP priv UserName");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_V3_Username);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass, "SNMP priv Password");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_priv_pass);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass, "SNMP Auth Password");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP_Auth_pass);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP, "SNMP");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_SNMP);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId, "Router Id");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_RouterId);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_FullIQNET);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_ManagementAdd);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country, "Country");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Country);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City, "City");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_City);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site, "Site");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Site);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise, "Premise");
			getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fetchValueFromViewPage_Premise);
		}
	 */
	 public void verifydeviceEdit_AccessRouter(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException {

		 String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editdeviceName");
		 //String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeviceType");
		 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editVendorModel");
		 String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editRouterID");
		 String editModularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editModularMSP");
		 String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editFullIQNET");
		 String editIOSXR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editIOSXR");
		 String editTelnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editTelnet");
		 String editSSH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSSH");
		 String editSnmp2C = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp2C");
		 String editSnmp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmp3");
		 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmProNewValue");
		 String editSnmprwNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmprwNewValue");
		 String editSnmpv3UsernameNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3UsernameNewValue");
		 String editSnmpv3AuthpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3AuthpasswordNewValue");
		 
		 String editSnmpv3PrivpasswordNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editSnmpv3PrivpasswordNewValue");
		 
		 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editManagementAddress");
		 
		 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editCountry");
		 
		 String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCity");
		 String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingCityValue");
		 String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSite");
		 String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingSiteValue");
		 
		 String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremise");
		 String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editExistingPremiseValue");
		 String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCity");
		 String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSite");
		 
		 String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremise");
		 String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityName");
		 String editNewCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewCityCode");
		 
		 String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteName");
		 String editNewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewSiteCode");
		 String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseName");
		 String editNewPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"editNewPremiseCode");
			scrollUp();
			waitForAjax();

			// Action Button Click
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Action, "Action");
			waitForAjax();

			// Edit Button Click
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
			//String EditButtonClick = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit);
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.Edit, "Edit");
			
			waitforPagetobeenable();
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield, "Edit Device Name TextField");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.nametextfield,editDevicename,"Edit Device Name TextField");
	
		
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, "Vendor Model Input");
			addDropdownValues_commonMethod("Vendor/Model", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.vendormodelinput, editVendorModel);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield, "Edit RouterID TextField");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.routeridtextfield,editRouterID,"Edit RouterID TextField");
			
			if(editFullIQNET.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.fulliqnetcheckbox, "Full IQNET Checkbox");
			waitForAjax();
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IQNet_alertHeader, "Full IQNET Alert Header");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.IONet_xButton, "Full IQNET Alert Close");
			}
			
			if(editModularMSP.equalsIgnoreCase("Yes"))
			{
				waitForAjax();
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, "Modular MSP Checkbox");
				waitForAjax();
			}
			//else
			//{
				//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.modularmspcheckbox, null);
			//}
			if(editIOSXR.equalsIgnoreCase("Yes"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox, "IOSXR Checkbox");
			}
			//else
			//{
				//isChecked(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.iosxrcheckbox,  null);
			//}
			
			if(editTelnet.equalsIgnoreCase("Yes") && editSSH.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.telnetradiobutton, "Telnet Radio Button");
			}
			
			else if(editTelnet.equalsIgnoreCase("No") && editSSH.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sshradiobutton, "SSH Radio Button");
			}
			
			if(editSnmp2C.equalsIgnoreCase("Yes") && editSnmp3.equalsIgnoreCase("No"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c2cradiobutton, "Snmp2C");
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield, "snmpro Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprotextfield,editSnmProNewValue);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield)==false)
				////{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield, "snmprw Textfield");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmprwtextfield,editSnmprwNewValue);
				//}
			}
			
			else if(editSnmp2C.equalsIgnoreCase("No") && editSnmp3.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.c3radiobutton, "Snmp3");
				
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username, "snmp UserName");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3username,editSnmpv3UsernameNewValue);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword, "snmp AuthPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3authpassword,editSnmpv3AuthpasswordNewValue);
				//}
				//if(isTextfieldEmpty(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword)==false)
				//{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword, "snmp PrivPasswd");
				clearTextBox(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword);
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.snmpv3privpassword,editSnmpv3PrivpasswordNewValue);
				//}
			//}
			}
			
		if(!editCountry.equalsIgnoreCase("null"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, "Country Input");
				//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput,"Country Input");
				//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput1 +editCountry+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput2,"Country Input");
				selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.countryinput, editCountry, "Edit Country Input");
				
			if(editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,"City dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput1 +editExistingCityValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput2,"City dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, editExistingCityValue, "City dropdown Input");
			
			}
			else if(editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
				click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,editNewCityName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,editNewCityCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
				
			}
				
			if(editExistingSite.equalsIgnoreCase("Yes")&& editNewSite.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,"Site Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput1 +editExistingSiteValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput2,"Site dropdown Input");
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, editExistingSiteValue, "Site dropdown Input");
			
			
			}
			else if (editExistingSite.equalsIgnoreCase("No")&& editNewSite.equalsIgnoreCase("Yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
			}
			
			if(editExistingPremise.equalsIgnoreCase("Yes")&& editNewPremise.equalsIgnoreCase("No"))
			{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,"Premise Dropdown Input");
			//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput1 +editExistingPremiseValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput2,"Premise dropdown Input");
			
			selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, editExistingPremiseValue, "Premise Dropdown Input");
			}
			else if(editExistingPremise.equalsIgnoreCase("No")&& editNewPremise.equalsIgnoreCase("yes"))
			{
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,editNewPremiseName, "Premise Name Inputfield");
				
				verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
				sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,editNewPremiseCode,"Premise Code Inputfield");
			}
			
			}
		else if(editCountry.equalsIgnoreCase("Null")){
			Report.LogInfo("INFO"," No changes made for 'Country' dropdown","PASS");
		}
		//Edit City
		if(editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, "City dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput,"City dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput1 +editExistingCityValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput2,"City dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citydropdowninput, editExistingCityValue, "City Dropdown Input");
		
		}
		else if(editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch, "Add City Switch");
			click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.addcityswitch,"Add City Switch");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield, "City Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citynameinputfield,editNewCityName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield, "City Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.citycodeinputfield,editNewCityCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
			
		}
		//Edit Site	
		if(editExistingSite.equalsIgnoreCase("Yes")&& editNewSite.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, "Site Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput,"Site Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput1 +editExistingSiteValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput2,"Site dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitedropdowninput, editExistingSiteValue, "Site dropdown Input");
		
		}
		else if (editExistingSite.equalsIgnoreCase("No")&& editNewSite.equalsIgnoreCase("Yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected, "Site Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitenameinputfield_addCityToggleSelected,editNewSiteName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected, "Site Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.sitecodeinputfield_addCityToggleSelected,editNewSiteCode);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addCityToggleSelected,editNewPremiseName);
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addCityToggleSelected,editNewPremiseCode);
		}
		//Edit Premise
		if(editExistingPremise.equalsIgnoreCase("Yes")&& editNewPremise.equalsIgnoreCase("No"))
		{
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, "Premise Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput,"Premise Dropdown Input");
		//click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput1 +editExistingPremiseValue+ APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput2,"Premise dropdown Input");
		selectByVisibleText(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisedropdowninput, editExistingPremiseValue, "Premise dropdown Input");
		
		}
		else if(editExistingPremise.equalsIgnoreCase("No")&& editNewPremise.equalsIgnoreCase("yes"))
		{
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected, "Premise Name Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisenameinputfield_addPremiseToggleSelected,editNewPremiseName, "Premise Name Inputfield");
			
			verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected, "Premise Code Inputfield");
			sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.premisecodeinputfield_addPremiseToggleSelected,editNewPremiseCode,"Premise Code Inputfield");
		}
		
		scrollDown(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton);
		Reusable.waitForAjax();
 	
 	
		verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		click(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.okbutton, "Ok Button");
		waitForAjax();
		Reusable.WaitforCPQloader();
		
		}
	 
	 public void testStatus() throws InterruptedException, IOException {
		 Reusable.waitForAjax();
		 Reusable.WaitforCPQloader();
			String element = null;
			String status = null;
			int listSize = getXPathCount(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.tableRow);

			if (listSize >= 1) {

				for (int i = 1; i <= listSize; i++) {
					//element = findWebElement("(//tbody/tr[" + i + "]/td)[1]").getText();
					element = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[1]") ).getText();
				
					if (element.isEmpty()) {

					} else {
						Report.LogInfo("TestStatus", "Test Name is displaying as: " + element, "PASS");
						// ExtentTestManager.getTest().log(LogStatus.PASS, "Test
						// Name is displaying as: " + element);
						// Log.info("Test Name is displaying as: " + element);
						status = webDriver.findElement(By.xpath("(//tbody/tr[" + i + "]/td)[2]/div") ).getAttribute("class");
						//status = findWebElement("(//tbody/tr[" + i + "]/td)[2]/div").getAttribute("class");
						// Log.info("status displays as: " + status);
						if (status.contains("red")) {
							Report.LogInfo("TestStatus", "status colour dipslays as: red", "PASS");						
						} else if (status.contains("green")) {
							Report.LogInfo("TestStatus", "status colour dipslays as: green", "PASS");						
						}
					}
				}
			} else {
				Report.LogInfo("TestStatus",
						"Command/Status table is not displaying in view device page. Instead Message displays as: ",
						"FAIL");
				Report.LogInfo("TestStatus", "* Device reachability tests require the device to have an IP address.",
						"FAIL");

			}

		}
	 
	 public void verifEditedValue_Firewall(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		 String editDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		 String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceType");
		 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editVendorModel");
		 String editRouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editRouterID");
		 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editManagementAddress");
		 String editFullIQNET = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editFullIQNET");
		 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");
		 String editExistingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editExistingCity");
		 String editNewCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewCity");
		 String editExistingCityValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editExistingCityValue");
		 String editNewCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewCityName");
		 String editExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editExistingSite");
		 String editNewSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewSite");
		 String editExistingSiteValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editExistingSiteValue");
		 String editNewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewSiteName");
		 String editExistingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editExistingPremise");
		 String editNewPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewPremise");
		 String editExistingPremiseValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editExistingPremiseValue");
		 String editNewPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editNewPremiseName");


		 // Device name
		 if (editDevicename.equalsIgnoreCase("Null")) {
		 compareText_InViewPage_ForNonEditedFields("Name");
		 } else {
		 compareText_InViewPage("Name", editDevicename);
		 }

		 // Device Type
		 compareText_InViewPage("Device Type", DeviceType);

		 // Vendor/Model
		 if (editVendorModel.equalsIgnoreCase("Null")) {
		 compareText_InViewPage_ForNonEditedFields("Vendor/Model");
		 } else {
		 compareText_InViewPage("Vendor/Model", editVendorModel);
		 }

		 // Router Id
		 if (editRouterID.equalsIgnoreCase("Null")) {
		 compareText_InViewPage_ForNonEditedFields("Router Id");
		 } else {
		 compareText_InViewPage("Router Id", editRouterID);
		 }

		 // Management Address
		 if (editManagementAddress.equalsIgnoreCase("Null")) {
		 compareText_InViewPage_ForNonEditedFields("Management Address");
		 } else {
		 compareText_InViewPage("Management Address", editManagementAddress);
		 }

		 // Full IQNET
		 if (editFullIQNET.equalsIgnoreCase("Null")) {
		 // compareText_InViewPage_ForNonEditedFields(application, "Full
		 // IQNET", xml);
		  //Log.info("No changes made");
			 Report.LogInfo("INFO", "No changes made", "PASS");
		 } else if (editFullIQNET.equalsIgnoreCase("yes")) {

		 compareText_InViewPage("Full IQNET", "Null");

		 } else if (editFullIQNET.equalsIgnoreCase("no")) {

		 compareText_InViewPage("Full IQNET", "Null");

		 }

		 // Country
		 if (editCountry.equalsIgnoreCase("Null")) {
		 compareText_InViewPage_ForNonEditedFields("Country");
		 } else {
		 compareText_InViewPage("Country", editCountry);
		 }

		 // City
		 if ((editExistingCity.equalsIgnoreCase("Null") && editNewCity.equalsIgnoreCase("Null"))
		 || (editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("no"))) {

		 compareText_InViewPage_ForNonEditedFields("City");
		 } else if (editExistingCity.equalsIgnoreCase("yes") && editNewCity.equalsIgnoreCase("no")) {

		 compareText_InViewPage("City", editExistingCityValue);

		 } else if (editExistingCity.equalsIgnoreCase("no") && editNewCity.equalsIgnoreCase("yes")) {

		 compareText_InViewPage("City", editNewCityName);
		 }

		 // Site
		 if ((editExistingSite.equalsIgnoreCase("Null") && editNewSite.equalsIgnoreCase("Null"))
		 || (editExistingSite.equalsIgnoreCase("no") && editNewSite.equalsIgnoreCase("no"))) {

		 compareText_InViewPage_ForNonEditedFields("Site");
		 } else if (editExistingSite.equalsIgnoreCase("yes") && editNewSite.equalsIgnoreCase("no")) {

		 compareText_InViewPage("Site", editExistingSiteValue);

		 } else if (editExistingSite.equalsIgnoreCase("no") && editNewSite.equalsIgnoreCase("yes")) {

		 compareText_InViewPage("Site", editNewSiteName);
		 }

		 // Premise
		 if ((editExistingPremise.equalsIgnoreCase("Null") && editNewPremise.equalsIgnoreCase("Null"))
		 || (editExistingPremise.equalsIgnoreCase("no") && editNewPremise.equalsIgnoreCase("no"))) {

		 compareText_InViewPage_ForNonEditedFields("Premise");
		 } else if (editExistingPremise.equalsIgnoreCase("yes") && editNewPremise.equalsIgnoreCase("no")) {

		 compareText_InViewPage("Premise", editExistingPremiseValue);

		 } else if (editExistingPremise.equalsIgnoreCase("no") && editNewPremise.equalsIgnoreCase("yes")) {

		 compareText_InViewPage("Premise", editNewPremiseName);
		 }

		 }
	
	 
	 public void compareText_InViewPage(String labelname, String expectedVal) throws IOException {
			WebElement element = null;
			String el1= "//div[div[label[contains(text(),'";
			String el2= "')]]]/div[2]";
			//element = findWebElement("//div[div[label[contains(text(),'"+labelname+"')]]]/div[2]");
			element = webDriver.findElement(By.xpath(el1+labelname+el2));
			String actualVal = element.getText().toString();

			if (actualVal.contains(expectedVal)) {
				Report.LogInfo("CompareText", "Text match on View page", "PASS");
			} else {
				Report.LogInfo("CompareText", "Text not match on View page", "Fail");
			}
		}
	 public void compareText_InViewPage_ForNonEditedFields(String labelname)
				throws InterruptedException {

			//String text = null;
			WebElement element = null;

			try {
				//Thread.sleep(1000);
				//element = getwebelement("//div[div[label[contains(text(),'" + labelname + "')]]]/div[2]");
				waitForAjax();
				element = webDriver.findElement(By.xpath("//div[div[label[contains(text(),'" + labelname + "')]]]/div[2]"));
				String emptyele = element.getText().toString();
				
				Report.LogInfo("INFO", labelname + " field is not edited. It is displaying as '" + emptyele + "'", "PASS");
				/*ExtentTestManager.getTest().log(LogStatus.PASS,
						labelname + " field is not edited. It is displaying as '" + emptyele + "'");
				Log.info(labelname + " field is not edited. It is displaying as '" + emptyele + "'");*/
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");

				/*ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
				Log.info(labelname + " field is not displaying");*/
			}

		}
	 
	 public void routerPanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo) 
			 throws InterruptedException, IOException {

			 String editedVendor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editVendorModel");
			 String addedVendor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
			 String managementAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editManagementAddress");
			 String managementAddressCreated = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "Management_Address");

			 String command_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv4");
			 String command_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "command_ipv6");
			 String vrf_Ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");
			 String vrf_Ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv6");

			 String vendorModel = null;
			 // String editedVendor = map.get("editVendorModel");
			 // String addedVendor = map.get("VendorModel");
			 if (editedVendor.equalsIgnoreCase("null")) {
			 vendorModel = addedVendor;
			 } else {
			 vendorModel = editedVendor;
			 }

			 // String managementAddressEdit = map.get("editManagementAddress");
			 // String managementAddressCreated = map.get("Management Address");

			 if (vendorModel.startsWith("Cisco")) {
			 if (managementAddressEdit.equalsIgnoreCase("null")) {

			 routerPanel(command_ipv4, command_ipv6, managementAddressCreated, vrf_Ipv4, vrf_Ipv6);
			 } else {
			 routerPanel(command_ipv4, command_ipv6, managementAddressEdit, vrf_Ipv4, vrf_Ipv6);
			 }
			 } else {
			 Report.LogInfo("INFO", "Router Panel will not display for the selected vendorModel: " + vendorModel,
			 "PASS");
			 }

			 // logger = ExtentTestManager.startTest("fetchDeviceInterface");
			 String DeviceName = null;
			 String VendorModel = null;
			 String managementAddress = null;
			 String country = null;

			 String editdeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editdeviceName");
			 String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
			 String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editVendorModel");
			 String VendorModell = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VendorModel");
			 String editManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editManagementAddress");
			 String editCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editCountry");
			 String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
			 String SnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SnmProNewValue");
			 String editSnmProNewValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			 "editSnmProNewValue");

			 // Device name
			 if (editdeviceName.equalsIgnoreCase("null")) {
			 DeviceName = Name;
			 } else {
			 DeviceName = editdeviceName;
			 }

			 // Vendor/Model
			 if (editVendorModel.equalsIgnoreCase("null")) {
			 VendorModel = VendorModell;
			 } else {
			 VendorModel = editVendorModel;
			 }

			 // Management Address
			 if (editManagementAddress.equalsIgnoreCase("null")) {
			 managementAddress = managementAddressCreated;
			 } else {
			 managementAddress = editManagementAddress;
			 }

			 // Country
			 if (editCountry.equalsIgnoreCase("null")) {
			 country = Country;
			 } else {
			 country = editCountry;
			 }

			 // Snmpro
			 String Snmpro = null;
			 String snmproCreated = SnmProNewValue;
			 String snmproEdited = editSnmProNewValue;
			 String snmproDefaultValue = "JdhquA5";

			 if (snmproEdited.equalsIgnoreCase("null")) {
			 if (snmproCreated.equalsIgnoreCase("null")) {
			 Snmpro = snmproDefaultValue;
			 } else {
			 Snmpro = snmproCreated;
			 }
			 } else {
			 Snmpro = snmproEdited;
			 }
			 }

			 public void routerPanel(String commandIPv4, String commandIPv6, String ipAddress, String vrfname_ipv4,
			 String vrfname_ipv6) throws InterruptedException, IOException {

			 // scrollDown();
			 // Thread.sleep(1000);

			 // Command IPV4
			 //selectByValue(APT_MCN_CreateAccessCoreDevice.CreateAccessCoreDevice.commandIPV4_dropdown, commandIPv4,
			 //"Command IPV4");
			 addDropdownValues_commonMethod("Command IPV4",APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV4_dropdown, commandIPv4);

			 // addDropdownValues_commonMethod("Command IPV4",
			 // "commandIPV4_dropdown", commandIPv4);

			 hostnametextField_IPV4(commandIPv4, ipAddress);

			 vrfNametextField_IPV4(commandIPv4, vrfname_ipv4);

			 executeCommandAndFetchTheValue(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_Ipv4);

			 // Commmand IPV6
			 addDropdownValues_commonMethod("Command IPV6", APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPV6_dropdown, commandIPv6);

			 hostnametextField_IPV6(commandIPv6, ipAddress);

			 vrfNametextField_IPV6(commandIPv6, vrfname_ipv6);

			 executeCommandAndFetchTheValue(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.executebutton_IPv6);

			 }

			 public void hostnametextField_IPV4(String command_ipv4, String ipAddress) throws InterruptedException, IOException {
			 boolean IPV4availability = false;
			 IPV4availability = isVisible(
			 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield);

			 if (IPV4availability) {
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,"commandIPv4_hostnameTextfield");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield, ipAddress);
			 } else {
			 Report.LogInfo("LogStatus",
			 "'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4, "INFO");
			 Report.LogInfo("LogStatus",
			 "'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4, "INFO");
			 }
			 }

			 public void vrfNametextField_IPV4(String command_ipv4, String vrfname_ipv4) throws InterruptedException, IOException {
			 boolean IPV4availability = false;

			 if (command_ipv4.contains("vrf")) {
			 IPV4availability = isVisible(
					 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField);

			 if (IPV4availability) {
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_vrfnameTextField,"commandIPv4_vrfnameTextField");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv4_hostnameTextfield,
			 vrfname_ipv4);
			 } else {
			 Report.LogInfo("LogStatus", "'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4,
			 "INFO");
			 }
			 } else {
			 Report.LogInfo("LogStatus", "'VRF Name IPv4' text field is not displaying for " + command_ipv4, "INFO");
			 }

			 }

			 public void executeCommandAndFetchTheValue(String executeButton) throws InterruptedException, IOException {

			 click(executeButton,"Execute");
			 // click_commonMethod(application, "Execute", executeButton, xml);

			 boolean resultField = false;
			 resultField = isVisible(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea);
			 if (resultField) {
			 Report.LogInfo("LogStatus", "''Result' text field is displaying", "PASS");

			 String remarkvalue = getTextFrom(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.result_textArea,"result_text Area");
			 Report.LogInfo("LogStatus", "value under 'Result' field displaying as " + remarkvalue, "PASS");

			 } else {
			 Report.LogInfo("LogStatus", "'Result' text field is not displaying", "FAIL");
			 }
			 }

			 public void hostnametextField_IPV6(String commandIPv6, String ipv6Address)
			 throws InterruptedException, IOException {
			 boolean IPV4availability = false;
			 IPV4availability = isVisible(
					 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield);
			 if (IPV4availability) {

			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield,
			 "IP Address or Hostname");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_hostnameTextfield, ipv6Address);

			 } else {
			 Report.LogInfo("INFO", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6,
			 "PASS");
			 }
			 }

			 public void vrfNametextField_IPV6(String commandIPV6, String vrfname_IPV6)
			 throws InterruptedException, IOException {
			 boolean IPV6availability = false;

			 if (commandIPV6.equalsIgnoreCase("vrf")) {

			 IPV6availability = isVisible(
					 APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField);
			 if (IPV6availability) {
			 verifyExists(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,
			 "Router Vrf Name");
			 sendKeys(APT_MCN_CreateAccessCoreDeviceObj.CreateAccessCoreDevice.commandIPv6_vrfnameTextField,
			 vrfname_IPV6);

			 } else {
			 Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6, "PASS");
			 }
			 } else {
			 Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6, "PASS");
			 }

			 }
			 public void addDropdownValues_commonMethod(String labelname, String xpath, String expectedValueToAdd)
					 throws InterruptedException{
					 boolean availability=false;
					 List<String> ls = new ArrayList<String>();

					 try {

					 //availability=getwebelementNoWait(xml.getlocator("//locators/" + application + "/"+ xpath +"")).isDisplayed();
					 if(isVisible(xpath)) {
					 Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
					 if(expectedValueToAdd.equalsIgnoreCase("null")) {
					 Report.LogInfo("INFO", " No values selected under "+ labelname + " dropdown", "PASS");
					 }else {
					 webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//div[text()='�']")).click();
					 //Clickon(getwebelementNoWait("//div[label[text()='"+ labelname +"']]//div[text()='�']"));

					 //verify list of values inside dropdown
					 List<WebElement> listofvalues = webDriver
					 .findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

					 for (WebElement valuetypes : listofvalues) {
					 //Log.info("List of values : " + valuetypes.getText());
					 ls.add(valuetypes.getText());
					 }

					 //ExtentTestManager.getTest().log(LogStatus.PASS, "list of values inside "+labelname+" dropdown is: "+ls);
					 //System.out.println("list of values inside "+labelname+" dropdown is: "+ls);

					 webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd);
					 //SendKeys(getwebelementNoWait("//div[label[text()='"+ labelname +"']]//input"), expectedValueToAdd);

					 webDriver.findElement(By.xpath("(//div[label[text()='"+ labelname +"']]//div[contains(text(),'"+ expectedValueToAdd +"')])[1]")).click();
					 //Clickon(getwebelementNoWait("(//div[label[text()='"+ labelname +"']]//div[contains(text(),'"+ expectedValueToAdd +"')])[1]"));
					 //Thread.sleep(1000);

					 /*String actualValue=getwebelementNoWait("//label[text()='"+ labelname +"']/following-sibling::div//span").getText();
					 ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
					 System.out.println( labelname + " dropdown value selected as: "+ actualValue);
					 */
					 }
					 }else {
					 Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
					 //ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
					 //System.out.println(labelname + " is not displaying");
					 }
					 }catch(NoSuchElementException e) {
						 
					 //ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
					 Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
					 System.out.println(labelname + " is not displaying");
					 }catch(Exception ee) {
					 ee.printStackTrace();
					 //ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under "+ labelname + " dropdown");
					 Report.LogInfo("INFO", " NOt able to perform selection under "+ labelname + " dropdown", "FAIL");
					 System.out.println(" NO value selected under "+ labelname + " dropdown");
					 }
					 }
}
	
	
	
