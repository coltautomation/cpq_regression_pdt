package testHarness.exploreFunctions;

import java.io.IOException;
import baseClasses.SeleniumUtils;
import pageObjects.exploreObjects.ExploreLoginObj;

public class ExploreLoginPage extends SeleniumUtils {
	
	public String ExploreLogin(String UserName, String Password) throws InterruptedException, IOException 
	{
		
		verifyExists(ExploreLoginObj.Login.userNameTxb,"Username textbox");
		sendKeys(ExploreLoginObj.Login.userNameTxb,UserName,"Username textbox");
	
		verifyExists(ExploreLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(ExploreLoginObj.Login.passWordTxb,Password,"Password textbox");
		
		verifyExists(ExploreLoginObj.Login.loginBtn,"Login button");
		click(ExploreLoginObj.Login.loginBtn,"Login button");
		
		waitForAjax();
		verifyExists(ExploreLoginObj.Login.mapViewBtn,"Home Tab on Landing Page");				
		return "True";
	}

}
