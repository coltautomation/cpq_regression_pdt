package pageObjects.aptObjects;

public class APT_MCS_CreateOrder_IPVPNSwiftNetObj {
	public static class CreateCustomer {

		public static final String ipvpntypetextfield = "@xpath=//div[div[label[text()='IP VPN Type']]]//input";

		public static final String sidwarngmsg = "@xpath=//label[text()='Service Identification']/parent::*//div";

		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";

		public static final String DeliveryChannelField = "@xpath=//div[div[label[text()='Delivery Channel']]]//input";

		public static final String PackageField = "@xpath=//div[div[label[text()='Package']]]//input";

		public static final String OrdernumberDropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']//parent::*//div//input";

		public static final String serviceidentificationtextfield = "@xpath=//label[text()='Service Identification']//parent::*//input";

		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";

		public static final String Phone_Value = "@xpath=//label[text()='Phone']//parent::div//following-sibling::div";

		// public static final String WarningMessageCommon
		// ="@xpath=//label[text()='Value']/parent::div//div";
		public static final String WarningMessageCommon1 = "@xpath=//label[text()='";
		public static final String WarningMessageCommon2 = "']/parent::div//div";

		public static final String UserNameWarnign = "@xpath=//input[@id='userName']//following-sibling::div[contains(.,'is required')]";
		public static final String passwordWarnign = "@xpath=//input[@id='password']//following-sibling::div[contains(.,'is required')]";

		public static final String WarningMessageddnCommon = "@xpath=(//label[text()='Value']//parent::div//following-sibling::div)[4]";
		public static final String WarningMessageddnCommon1 = "@xpath=(//label[text()='";
		public static final String WarningMessageddnCommon2 = "']//parent::div//following-sibling::div)[4]";

		// public static final String TextValueCommon
		// ="@xpath=//label[text()='Value']//following-sibling::input";
		public static final String TextValueCommon1 = "@xpath=//label[text()='";
		public static final String TextValueCommon2 = "']//following-sibling::input";

		// public static final String SelectValueDropdown
		// ="@xpath=//div[div[label[text()='Value']]]//input";
		public static final String physicalSite = "@xpath=(//div[div[label[text()='Physical Site']]]//input)[1]";
		public static final String SelectValueDropdown1 = "@xpath=//div[div[label[text()='";
		public static final String SelectValueDropdown2 = "']]]//input";
		public static final String SelectValueDropdown3 = "']]]//select";

		// public static final String CheckboxCommon
		// ="@xpath=//label[text()='Value']//parent::div//div//input";
		public static final String CheckboxCommon1 = "@xpath=//label[text()='";
		public static final String CheckboxCommon2 = "']//parent::div//div//input";

		public static final String SuccessMessageCom = "@xpath=(//div[@role='alert'])[1]";

		// public static final String CompareTextCommon
		// ="@xpath=//label[text()='Value']//parent::div//following-sibling::div";
		public static final String CompareTextCommon1 = "@xpath=//label[text()='";
		public static final String CompareTextCommon2 = "']//parent::div//following-sibling::div";

		public static final String DeleteCommon = "@xpath=//div[div[text()='Delete']]//following::div//button[@class='btn btn-danger']";

		public static final String ManageCustomerServiceLink = "@xpath=//b[contains(text(),'MANAGE CUSTOMER'S SERVICE')]";

		public static final String ManageColtNetworkLink = "@xpath=//b[contains(text(),'MANAGE COLT'S NETWORK')]";

		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";

		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";

		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";

		public static final String mcslink = "@xpath=//a[contains(text(),'Manage Customers Service')]";

		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";

		public static final String createorderlink = "@xpath=//li[text()='Create Order/Service']";

		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String nametextfield = "@xpath=//input[@id='name']";

		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";

		public static final String country = "@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String ocntextfield = "@xpath=//input[@id='ocn']";

		public static final String referencetextfield = "@xpath=//input[@id='reference']";

		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";

		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";

		// public static final String emailtextfield
		// ="@xpath=//input[@id='email']";

		public static final String phonetextfield = "@xpath=//input[@id='phone']";

		public static final String faxtextfield = "@xpath=//input[@id='fax']";

		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";

		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";

		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String clearbutton = "@xpath=//span[text()='Clear']";

		public static final String Resetbutton = "@xpath=//span[text()='Reset']";

		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='×']";

		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='×']";

		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";

		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";

		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";

		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";

		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";

		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Customer']";

		public static final String Name_Text = "@xpath=(//div//label[@for='name'])[1]";

		public static final String Name_Value = "@xpath=//label[text()='Legal Customer Name']/parent::div/parent::div//div[2]";

		public static final String MainDomain_Text = "@xpath=(//div//label[@for='name'])[2]";

		public static final String MainDomain_Value = "@xpath=//label[text()='Main Domain']/parent::div/parent::div//div[2]";

		public static final String Country_Text = "@xpath=(//div//label[@for='name'])[3]";

		public static final String Country_Value = "@xpath=//label[text()='Country']/parent::div/parent::div//div[2]";

		public static final String OCN_Text = "@xpath=(//div//label[@for='name'])[4]";

		public static final String OCN_Value = "@xpath=//label[text()='OCN']/parent::div/parent::div//div[2]";

		public static final String Reference_Text = "@xpath=(//div//label[@for='name'])[5]";

		public static final String Reference_Value = "@xpath=//label[text()='Reference']/parent::div/parent::div//div[2]";

		public static final String Type_Text = "@xpath=(//div//label[@for='name'])[6]";

		public static final String Type_Value = "@xpath=//label[text()='Type']/parent::div/parent::div//div[2]";

		public static final String TechnicalContactName_Text = "@xpath=(//div//label[@for='name'])[7]";

		public static final String TechnicalContactName_Value = "@xpath=//label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";

		public static final String Email_Text = "@xpath=(//div//label[@for='name'])[8]";

		public static final String Email_Value = "@xpath=//label[text()='Email']/parent::div/parent::div//div[2]";

		public static final String Phone_Text = "@xpath=(//div//label[@for='name'])[9]";

		public static final String Fax_Text = "@xpath=(//div//label[@for='name'])[10]";

		public static final String Fax_Value = "@xpath=//label[text()='Fax']/parent::div/parent::div//div[2]";

		// public static final String nametextfield
		// ="@xpath=//input[@id='customerSearch']";

		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";

		public static final String choosecustomerdropdown = "@xpath=//div[label[text()='Customer']]//input";

		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";

		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";

		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";

		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";

		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";

		// public static final String country
		// ="@xpath=//div[div[label[text()='Country']]]/div[2]";

		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";

		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";

		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";

		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";

		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";

		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";

		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";

		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";

		public static final String useractionbutton = "@xpath=//button[@id='dropdown-basic-button']";

		public static final String adduserbutton = "@xpath=//a[contains(text(),'Add')]";

		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";

		public static final String selectorderswitch = "@xpath=(//div[@class='react-switch-bg'])[2]";

		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";

		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";

		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";

		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";

		public static final String newordertextfield = "@xpath=//input[@id='orderName']";

		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String servicetypetextfield = "@xpath=//div[div[label[text()='Service Type']]]//input";

		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";

		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";

		public static final String changeorder_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// Service Creation Form in Service creation page

		public static final String createorderservice_header = "@xpath=//div[text()='Create Order / Service']";

		public static final String IPVPNType = "@xpath=//label[text()='IP VPN Type']//following::div";

		public static final String ManagementVpnField = "@xpath=//div[div[label[text()='Management Vpn']]]//input";

		public static final String VPNTopologyField = "@xpath=//div[div[label[text()='VPN Topology']]]//input";

		public static final String WholesaleVPNField = "@xpath=//div[div[label[text()='Wholesale VPN/Global MPLS']]]//input";

		public static final String MulticastCheckbox = "@xpath=//label[text()='Multicast']//following::input";

		public static final String MultiVPNCheckbox = "@xpath=//label[text()='Multi VPN']//following::input";

		public static final String DisableVoipCheckbox = "@xpath=//label[text()='Disable VoipAccess IPSLA Probes']//following::input";

		public static final String MultiGroupCheckbox = "@xpath=//label[text()='Multicast Group Pool']//following::input";

		public static final String MulticastGroupAddressvalue = "@xpath=//div[label[text()='Multicast Group Address']]//input";

		public static final String MulticastThresholdvalue = "@xpath=//div[label[text()='Multicast Threshold (in kbps)']]//input";

		public static final String MulticastGroupPoolvalue = "@xpath=//div[label[text()='Multicast Group Pool']]//input";

		public static final String servicetypevalue = "@xpath=//div[div[label[text()='Service Type']]]//div[2]";

		public static final String servicetype_value = "@xpath=//div[label[text()='Service Type']]/following-sibling::div";

		public static final String ResellerCodetextfield = "@xpath=//input[@id='resellerCode']";

		public static final String remarktextarea = "@xpath=//textarea[contains(@name,'remark')]";

		public static final String remarktextareavalue = "@xpath=//div[label[text()='Remark']]//textarea";

		public static final String Email_header = "@xpath=//div//label[text()='Email']";

		public static final String emailtextfield = "@xpath=//input[@id='email']";

		public static final String emailtextfieldvalue = "@xpath=//div[label[text()='Email']]//input";

		public static final String emailarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";

		public static final String SelectedEmail = "@xpath=(//div[@class='position-relative form-group']//select//option)[1]";

		public static final String emailarrowB = "@xpath=(//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span)[2]";

		public static final String phonecontacttextfield = "@xpath=//input[@id='phoneContact']";

		public static final String phonecontacttextfieldvalue = "@xpath=//div[label[text()='Phone Contact']]//input";

		public static final String phonearrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";

		public static final String phonearrowB = "@xpath=(//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span)[2]";

		public static final String SelectedPhone = "@xpath=(//div[@class='position-relative form-group']//select//option)[2]";

		// Management Options in Service creation page
		public static final String PackageDropdownDisabled = "@xpath=(//input[@placeholder='Select ...'])[1]";

		public static final String ManageServicecheckbox = "@xpath=//input[@id='managedService']";

		public static final String SyslogEventViewcheckbox = "@xpath=//input[@id='syslogEventView']";

		public static final String ServiceStatusViewcheckbox = "@xpath=//input[@id='serviceStatusView']";

		public static final String RouterConfigurationViewcheckbox = "@xpath=//input[@id='routerConfigView']";

		public static final String performancereportingcheckbox = "@xpath=//input[@id='performanceReporting']";

		public static final String ProactiveNotificationcheckbox = "@xpath=//input[@id='proactiveMonitoring']";

		public static final String NotificationManagementTeamDropdown = "@xpath=(//input[@placeholder='Select ...'])[2]";

		public static final String DialUserAdministrationcheckbox = "@xpath=//input[@id='dialUserAdministration']";

		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String OKbutton_ServiceCreation = "@xpath=//span[contains(text(),'Next')]";

		public static final String servicecreationmessage = "@xpath=(//div[@role='alert']//span)[1]";

		// public static final public static final String cancelbutton
		// ="@xpath=//button[@type='submit']//span[text()='Cancel']";

		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";

		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";

		public static final String searchbutton = "@xpath=//span[text()='Search']";

		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";

		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";

		public static final String viewLink_SearchOrderPage = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		public static final String searchdevicelink = "@xpath=//li[text()='Search for Device']";

		public static final String NetworkDeviceVendor = "@xpath=(//label[text()='Vendor/Model']//following-sibling::div//input)[1]";

		public static final String SelectDeviceVendor = "@xpath=(//label[text()='Vendor/Model']//following-sibling::div//input)[2]";

		public static final String NetworkDeviceType = "@xpath=(//label[text()='Device Type']//following-sibling::div//input)[1]";

		public static final String SelectDeviceType = "@xpath=(//label[text()='Device Type']//following-sibling::div//input)[2]";

		public static final String DeviceSelectioradio = "@xpath=(//div[@unselectable='on'])[1]//input";

		public static final String DeviceDetailHeader = "@xpath=//div[text()='Device Details']";

		public static final String NetworkDeviceName = "@xpath=//label[text()='Name']//parent::div//following-sibling::div";

		public static final String NetworkDeviceTypeValue = "@xpath=//label[text()='Device Type']//parent::div//following-sibling::div";

		public static final String NetworkVendor = "@xpath=//label[text()='Vendor/Model']//parent::div//following-sibling::div";

		public static final String NetworkRouterID = "@xpath=//label[text()='Router Id']//parent::div//following-sibling::div";

		public static final String NetworkManagementAdd = "@xpath=//label[text()='Management Address']//parent::div//following-sibling::div";

		public static final String NetworkCountry = "@xpath=//label[text()='Country']//parent::div//following-sibling::div";

		public static final String customerheader = "@xpath=//div[text()='Customer']";

		public static final String customerpanel_customername = "@xpath=(//label[text()='Customer Name']/parent::div/div//input)[1]";

		public static final String managecustomerheader = "@xpath=//div[@class='heading-green-row row']//span[text()='Manage Customer In OSP']";

		public static final String customer_ocn = "@xpath=//div[@class='position-relative form-group']//label[text()='OCN']/following-sibling::input";

		public static final String customer_country = "@xpath=(//label[text()='Country']/parent::div//div//input)[1]";

		public static final String customername = "@xpath=(//label[text()='Customer Name']/parent::div//div//input)[1]";

		public static final String defaultcheckbox = "@xpath=//label[@class='form-label capitalize']//input[@value='defcustprof']";

		public static final String configurecheckbox = "@xpath=//label[@class='form-label capitalize']//input[@value='configcustprof']";

		public static final String CustomerpanelActionDropdown = "@xpath=//div[contains(text(),'Customer')]/following-sibling::div/div//button[text()='Action']";

		public static final String addcustomer_successmsg = "@xpath=//div[@role='alert']//span";

		public static final String customername_columnvalue = "@xpath=//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@col-id='trailCustomerName']";

		public static final String editcustomer_successmsg = "@xpath=(//div[@role='alert']//span)[1]";

		public static final String viewpage_editcustomer = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit Customer']";

		public static final String viewpage_deletecustomer = "@xpath=//div[@class='dropdown-menu show']//a[text()='Delete Customer']";

		public static final String deletealertclose = "@xpath=//div[@class='modal-content']//button//span[text()='×']";

		public static final String customerpanel_customernamevalue = "@xpath=//label[text()='Customer Name']/parent::div/div//span";

		public static final String customerpanel_customernamecolumntext = "@xpath=//div[text()='Customer']/parent::div/following-sibling::div//span[text()='Customer Name']";

		public static final String customercountry_disabled = "@xpath=(//label[text()='Country']/parent::div//div//div)[1]";

		public static final String customername_disabled = "@xpath=(//label[text()='Customer Name']/parent::div//div//div)[1]";

		public static final String customer_countryvalue = "@xpath=//label[text()='Country']/parent::div/following-sibling::div";

		public static final String customer_customernamevalue = "@xpath=//label[text()='Customer Name']/parent::div/following-sibling::div";

		public static final String customername_selectedtext = "@xpath=(//label[text()='Customer Name']/parent::div//div//span)[1]";

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";

		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";

		public static final String EmailColumn = "@xpath=//div[@col-id='email']";

		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";

		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";

		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";

		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String UserUnchecked = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";

		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		public static final String UserRoles = "@xpath=//select[@name='availableRoles']//option[@label='Select']";

		public static final String UserRolestransferbutton = "@xpath=(//select[@name='availableRoles']//parent::div//parent::div//following-sibling::div//button)[1]";

		public static final String UserHideService = "@xpath=//select[@name='selectedHideService']//option[@label='Select']";

		public static final String UserHideServicetransferbutton = "@xpath=(//select[@name='availableHideService']//parent::div//parent::div//following-sibling::div//button)[2]";

		public static final String UserIPV4CommandCis = "@xpath=//select[@name='availableIPV4CommandCisco']//option[@label='Select']";

		public static final String IPV4Cistransferbutton = "@xpath=(//select[@name='availableIPV4CommandCisco']//parent::div//parent::div//following-sibling::div//button)[1]";

		public static final String UserIPV4CommandHua = "@xpath=//select[@name='availableIPV4CommandHuawei']//option[@label='Select']";

		public static final String IPV4Huatransferbutton = "@xpath=(//select[@name='availableIPV4CommandHuawei']//parent::div//parent::div//following-sibling::div//button)[1]";

		public static final String UserIPV6CommandCis = "@xpath=//select[@name='selectedIPV6Command']//option[@label='Select']";

		public static final String IPV6transferbutton = "@xpath=(//select[@name='availableIPV6Command']//parent::div//parent::div//following-sibling::div//button)[2]";

		public static final String EditUserRoles = "@xpath=//select[@name='selectedRoles']//option[@label='Select']";

		public static final String EditUserRolestransferbutton = "@xpath=(//select[@name='availableRoles']//parent::div//parent::div//following-sibling::div//button)[2]";

		public static final String EditUserHideService = "@xpath=//select[@name='availableHideService']//option[@label='Select']";

		public static final String EditUserHideServicetransferbutton = "@xpath=(//select[@name='availableHideService']//parent::div//parent::div//following-sibling::div//button)[1]";

		public static final String EditUserIPV4CommandCis = "@xpath=//select[@name='selectedIPV4CommandCisco']//option[@label='Select']";

		public static final String EditIPV4Cistransferbutton = "@xpath=(//select[@name='availableIPV4CommandCisco']//parent::div//parent::div//following-sibling::div//button)[2]";

		public static final String EditUserIPV4CommandHua = "@xpath=//select[@name='selectedIPV4CommandHuawei']//option[@label='Select']";

		public static final String EditIPV4Huatransferbutton = "@xpath=(//select[@name='availableIPV4CommandHuawei']//parent::div//parent::div//following-sibling::div//button)[2]";

		public static final String EditUserIPV6CommandCis = "@xpath=//select[@name='availableIPV6Command']//option[@label='Select']";

		public static final String EditIPV6transferbutton = "@xpath=(//select[@name='availableIPV6Command']//parent::div//parent::div//following-sibling::div//button)[1]";

		// New User creation page from view service page -- ="@xpath=

		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";

		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";

		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String UserName = "@xpath=(//div[@class='position-relative form-group'])[1]//input[@id='userName']";

		public static final String FirstName = "@xpath=(//div[@class='position-relative form-group'])[2]//input[@id='firstName']";

		public static final String SurName = "@xpath=(//div[@class='position-relative form-group'])[3]//input[@id='surname']";

		public static final String PostalAddress = "@xpath=(//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";

		public static final String Email = "@xpath=(//div[@class='position-relative form-group'])[5]//input[@id='email']";

		public static final String Phone = "@xpath=(//div[@class='position-relative form-group'])[6]//input[@id='phone']";

		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";

		public static final String UserIPGurdian = "@xpath=//label[text()='IPGuardian Account Group']//following-sibling::input";

		public static final String UserColtnline = "@xpath=//label[text()='Colt Online User']//following-sibling::input";

		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";

		public static final String OkButton = "@xpath=//button[@type='submit']";

		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";

		public static final String deletebutton = "@xpath=//button[text()='Delete']";

		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String userspanel_header = "@xpath=//div[text()='Users']";

		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";

		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";

		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";

		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";

		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";

		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";

		public static final String DedicatedPortalCustomer = "@xpath=//label[text()='Dedicated Portal']";

		public static final String UserView = "@xpath=//a[text()='View']";

		public static final String Userdelete = "@xpath=//a[text()='Delete']";

		public static final String UserUpdateMessage = "@xpath=//span[text()='User successfully updated']";

		public static final String UserCreatedMessage = "@xpath=//span[text()='User successfully created']";

		public static final String UserDeleteMessage = "@xpath=//span[text()='User successfully deleted']";

		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";

		public static final String DetailPageHeader = "@xpath=//div[contains(text(),'Customer Details')]";

		// !-- order panel - view service page -- ="@xpath=

		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";

		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";

		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";

		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";

		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";

		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";

		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String orderpanelheader = "@xpath=//div[text()='Order']";

		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";

		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";

		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";

		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";

		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";

		public static final String changeorder_dropdownlist = "@xpath=//div[contains(@class,'sc-ifAKCX oLlzc')]";

		public static final String OrderChangeMessage = "@xpath=//span[contains(text(),'Order successfully')]";

		public static final String OrderCreatedMessage = "@xpath=//span[text()='Order successfully created.']";

		public static final String OrderUpdatedMessage = "@xpath=//span[text()='Order successfully updated']";

		public static final String servicepanel_serviceidentificationvalue = "@xpath=//div[div[label[contains(text(),'Service Identification')]]]//div[2]";

		public static final String servicepanel_servicetypevalue = "@xpath=//div[div[label[contains(text(),'Service Type')]]]//div[2]";

		public static final String servicepanel_ResselerCodevalue = "@xpath=//div[div[label[contains(text(),'IP VPN Type')]]]//div[2]";

		public static final String servicepanel_Emailvalue = "@xpath=(//div[div[label[contains(text(),'Email')]]]//div)[4]";

		public static final String servicepanel_PhoneContactvalue = "@xpath=(//div[div[label[contains(text(),'Phone Contact')]]]//div)[2]";

		public static final String servicepanel_remarksvalue = "@xpath=//div[div[label[contains(text(),'Remark')]]]//div[2]";

		public static final String servicepanel_header = "@xpath=//div[text()='Service']";

		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";

		public static final String serviceupdate_successmsg = "@xpath=//span[contains(text(),'Service updated successfully')]";

		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";

		public static final String EditLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";

		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		public static final String SynchronizeServiceLink = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Synchronize')]";

		public static final String DeleteLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Delete']";

		public static final String ShowInfovistaReportLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show Infovista Report']";

		public static final String ShowNewInfovistaReportLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show New Infovista Report']";

		public static final String ManageSubnetsIpv6Link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets Ipv6']";

		public static final String ManageSubnetsIpv6_header = "@xpath=//div[@class='modal-title h4']";

		public static final String ManageSubnetsIpv6_Confirmationmessage = "@xpath=//div/span[text()='There are no subnets to be managed for this service.']";

		public static final String ManageSubnetsIpv6_action = "@xpath=(//div[div[button[@id='dropdown-basic-button' and @aria-haspopup='true']]]//div)[14]";

		public static final String ManageSubnetsIpv6_ManageIPs = "@xpath=//div[a[text()='Delete']]//a[text()='Manage IPs']";

		public static final String ManageSubnetsIpv6__Delete = "@xpath=//div[a[text()='Manage IPs']]//a[text()='Delete']";

		public static final String ManageSubnetsIpv6_popupcloseX = "@xpath=//button[span[text()='Close']]";

		public static final String DumpLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Dump']";

		public static final String DumpDetails = "@xpath=//div[@class='modal-body']";

		public static final String DumpClose = "@xpath=//button[@class='close']//span[text()='�']";

		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";

		// Status Panel -- ="@xpath=

		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";

		public static final String status_ordername = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";

		public static final String status_servicename = "@xpath=(//a[@title='Service']//parent::*)[1]";

		public static final String status_servicetype = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";

		public static final String status_servicedetails = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]";

		public static final String status_currentstatus = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";

		public static final String status_modificationtime = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]";

		public static final String statuslink = "@xpath=//a[@title='Status']";

		// !-- Synchronization Panel -- ="@xpath=

		public static final String sync_ordername = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";

		public static final String sync_servicename = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[1]";

		public static final String sync_servicetype = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";

		public static final String sync_servicedetails = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]";

		public static final String sync_status = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";

		public static final String synchronizelink = "@xpath=//a[text()='Synchronize']";

		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";

		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";

		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";

		public static final String changestatus_dropdown = "@xpath=//label[text()='New Status']//following-sibling::*";

		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";

		public static final String servicestatushistory = "@xpath=//div[@role='alert']";

		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";

		public static final String managementoptions_header = "@xpath=//div[text()='Management Options']";

		public static final String DeliveryChannel_text = "@xpath=//label[text()='Delivery Channel']";

		public static final String DeliveryChannel_value = "@xpath=//div[label[text()='Delivery Channel']]/following-sibling::div";

		public static final String ManageService_text = "@xpath=//label[text()='Managed Service']";

		public static final String ManageService_value = "@xpath=//div[label[text()='Managed Service']]/following-sibling::div";

		public static final String SyslogEventView_text = "@xpath=//label[text()='Syslog Event View']";

		public static final String SyslogEventView_value = "@xpath=//div[label[text()='Syslog Event View']]/following-sibling::div";

		public static final String ServiceStatusView_text = "@xpath=//label[text()='Service Status View']";

		public static final String ServiceStatusView_value = "@xpath=//div[label[text()='Service Status View']]/following-sibling::div";

		public static final String RouterConfigurationViewIPV6_text = "@xpath=//label[text()='Router Configuration View IPV6']";

		public static final String RouterConfigurationViewIPV6_Value = "@xpath=//div[label[text()='Router Configuration View IPV6']]/following-sibling::div";

		public static final String PerformanceReporting_text = "@xpath=//label[text()='Performance Reporting']";

		public static final String PerformanceReporting_value = "@xpath=//div[label[text()='Performance Reporting']]/following-sibling::div";

		public static final String ManagementOption_value = "@xpath=//div[label[text()='Management VPN']]/following-sibling::div";

		public static final String ManagementOption_text = "@xpath=//label[text()='Management VPN']";

		public static final String AllowSMCEmail_text = "@xpath=//label[text()='Allow SMC Email']";

		public static final String AllowSMCEmail_value = "@xpath=//div[label[text()='Allow SMC Email']]/following-sibling::div";

		public static final String ProactiveNotification_text = "@xpath=//label[text()='Pro-active Notification']";

		public static final String ProactiveNotification_Value = "@xpath=//div[label[text()='Pro-active Notification']]/following-sibling::div";

		public static final String NotificationManagementTeam_text = "@xpath=//label[text()='Notification Management Team']";

		public static final String NotificationManagementTeam_Value = "@xpath=//div[label[text()='Notification Management Team']]/following-sibling::div";

		public static final String DialUserAdministration_text = "@xpath=//label[text()='Dial User Administration']";

		public static final String DialUserAdministration_Value = "@xpath=//div[label[text()='Dial User Administration']]/following-sibling::div";

		// -- Add VPN Site Order -- ="@xpath=

		public static final String VPNSiteOrderHeader = "@xpath=(//div[contains(text(),'VPN Site Orders')])[1]";

		public static final String SiteOrderRadioButton = "@xpath=(//div[@role='gridcell']//input[contains(@class,'radio')])[1]";

		public static final String AddVPNSitelink = "@xpath=//a[text()='Add VPN Site Order']";

		public static final String AddVPNSitelinkSpoke = "@xpath=(//a[text()='Add VPN Site Order'])[2]";

		public static final String AddVPNSitePage = "@xpath=//div[contains(text(),'VPN Site Order')]";

		public static final String VPNNext = "@xpath=//span[text()='Next']";

		public static final String VPNCancel = "@xpath=//span[text()='Cancel']";

		public static final String VPNDeviceCountry = "@xpath=//div[div[label[text()='Device Country']]]//input";

		public static final String VPNDeviceCity = "@xpath=//div[div[label[text()='Device Xng City']]]//input";
		public static final String VPNDeviceCity1 = "@xpath=(//div[div[label[text()='Device Xng City']]]//input)[1]";

		public static final String VPNVoipService = "@xpath=//div[div[label[text()='VoIP Class of Service']]]//input";

		public static final String VPNVendoeModel = "@xpath=//div[div[label[text()='Vendor/Model']]]//input";

		public static final String VPNSiteOrderNum = "@xpath=//label[text()='Site Order Number']//following-sibling::input";

		public static final String VPNSiteAlis = "@xpath=//label[text()='Site Alias']//following-sibling::input";

		public static final String VPNRouterId = "@xpath=//label[text()='Router Id']//following-sibling::input";

		public static final String VPNPhysicalSite = "@xpath=//label[text()='Physical Site']//following-sibling::input";

		public static final String VPNManagementAdd = "@xpath=//label[text()='Management Address']//following-sibling::input";

		public static final String VPNSampName = "@xpath=//label[text()='Snmp V3 User Name']//following-sibling::input";

		public static final String VPNDeviceName = "@xpath=//label[text()='Device Name']//following-sibling::input";

		public static final String VPNGeneratePass1 = "@xpath=(//span[text()='Generate Password'])[1]";

		public static final String VPNGeneratePass2 = "@xpath=(//span[text()='Generate Password'])[2]";

		public static final String VPNVoIP = "@xpath=//input[@id='voipClassOfService']";

		public static final String VPNSiteactionbutton = "@xpath=//div[div[text()='VPN Site Orders']]//following-sibling::div//a";

		public static final String AddVPNSiteOrderLink = "@xpath=//div[div[text()='VPN Site Orders']]//div//a[text()='Add']";

		public static final String AddVPNSiteCommonLink = "@xpath=//div[div[text()='VPN Site Orders']]//div//a[text()='Value']";
		public static final String AddVPNOrderViewLink = "@xpath=(//div[div[text()='VPN Site Orders']]//following-sibling::table//td//a[text()='View'])[1]";
		public static final String AddVPNOrderDeleteLink = "@xpath=(//div[div[text()='VPN Site Orders']]//following-sibling::table//td//a[text()='Delete'])[1]";
		public static final String AddVPNOrderHubDeleteLink = "@xpath=(//div[div[div[text()='Hub']]]//span//a[text()='Delete'])[1]";
		public static final String RateFlagRadio = "@xpath=//input[@name='rateFlag']";

		public static final String Togglebutton = "@xpath=(//div[@class='react-switch-bg'])[1]";

		public static final String AddPrimierToggle = "@xpath=(//div[@class='react-switch-bg'])[3]";
		public static final String AddPrimierToggleSwiftNet = "@xpath=(//div[@class='react-switch-bg'])[2]";

		public static final String SelectsiteToogle = "@xpath=(//div[@class='react-switch-bg'])[3]";

		public static final String SelectsiteToogleL2 = "@xpath=(//div[@class='react-switch-bg'])[2]";

		public static final String SelectDeviceToogle = "@xpath=(//div[@class='react-switch-bg'])[3]";

		public static final String SelectCityToogle = "@xpath=(//div[@class='react-switch-bg'])[1]";

		public static final String VPNRemark = "@xpath=//label[text()='Remark']//following-sibling::textarea";

		public static final String ManageOptionHeader = "@xpath=//div[text()='Management Options']";
		public static final String ManageUserAction = "@xpath=(//a[text()='Manage Users']//parent::div//parent::div//following-sibling::div)[2]//button";

		public static final String ManageUserActionDdn = "@xpath=(//a[text()='Manage Users']//parent::div//parent::div//following-sibling::div)[2]//button//following-sibling::div//a[text()='Value']";
		public static final String ManageUserActionDdn1 = "@xpath=(//a[text()='Manage Users']//parent::div//parent::div//following-sibling::div)[2]//button//following-sibling::div//a[text()='";
		public static final String ManageUserActionDdn2 = "']";

		public static final String AddUserGeneretePass = "@xpath=//span[text()='Generate Password']";

		public static final String SelectHiddenUser = "@xpath=(//select[@id='selectedHideService']//option)[1]";

		public static final String AvailableHiddenUser = "@xpath=(//select[@id='availableHideService']//option)[1]";

		public static final String ForwardArrow = "@xpath=(//button[contains(@class,'btn btnSquar')]//span)[1]";

		public static final String BackwardArrow = "@xpath=(//button[contains(@class,'btn btnSquar')]//span)[2]";

		public static final String UserOk = "@xpath=(//button[contains(@class,'btn btnSquar')]//span)[3]";

		public static final String UserCheckbox = "@xpath=(((//div[text()='Abh']//parent::div)[1]//div)[1]//span)[4]";

		// Wholesale Interconnect -- ="@xpath=

		public static final String Wholesaleactionbutton = "@xpath=//div[div[text()='Wholesale Interconnect']]//button";

		public static final String SelectInterconnectink = "@xpath=//div[div[text()='Wholesale Interconnect']]//div//a[text()='Select Interconnect']";

		public static final String WholesaleEditLink = "@xpath=//div[div[text()='Wholesale Interconnect']]//div//a[text()='Edit']";

		public static final String WholesaleDeleteLink = "@xpath=//div[div[text()='Wholesale Interconnect']]//div//a[text()='Delete']";

		public static final String WholesaleDevice = "@xpath=//input[contains(@placeholder,'Select ...')]";

		public static final String WholesaleSearch = "@xpath=//span[contains(text(),'Search')]";

		public static final String WholesaleSelectPlus = "@xpath=//div[div[text()='Select Interconnect']]//following-sibling::div//div[text()='1280']";

		public static final String WholesaleSelectIPSec = "@xpath=//div[div[text()='Select Interconnect']]//following-sibling::div//div[text()='1280']";

		public static final String WholesaleSelectAccess = "@xpath=//div[div[text()='Select Interconnect']]//following-sibling::div//div[text()='1280']";

		public static final String WholesaleSelectConnect = "@xpath=//div[div[text()='Select Interconnect']]//following-sibling::div//div[text()='1280']";

		public static final String WholesaleNext = "@xpath=//button[contains(@class,'pull-right')]";

		public static final String ConfigurationOption = "@xpath=//div[text()='Configuration Options']";

		public static final String SelectWholesaleDevice = "@xpath=//div[@col-id='deviceName'][@role='gridcell']";

		public static final String IV_Management = "@xpath=//input[@id='ivmanagementFlag']";

		public static final String GetDevice = "@xpath=(//div[div[text()=' Edit VPN Name/Alias']]//following-sibling::div//div[@class='customLabelValue form-label'])[1]";

		public static final String GetInterface = "@xpath=(//div[div[text()=' Edit VPN Name/Alias']]//following-sibling::div//div[@class='customLabelValue form-label'])[2]";

		public static final String GetAddress = "@xpath=(//div[div[text()=' Edit VPN Name/Alias']]//following-sibling::div//div[@class='customLabelValue form-label'])[3]";

		public static final String OKButtonWholesale = "@xpath=//div[div[text()=' Edit VPN Name/Alias']]//following-sibling::div//button";

		// !-- .......VPN Alias................ -- ="@xpath=

		public static final String VPNAlisactionbutton = "@xpath=//div[div[text()='VPN Name/Alias']]//button";

		public static final String VPNAlisActionCommonLink = "@xpath=//div[div[text()='VPN Name/Alias']]//div//a[text()='Value']";

		// MAS Switch panel in view service page -- ="@xpath=

		public static final String MASswitch_header = "@xpath=//div[div[text()='MAS Switch']]";

		public static final String VPN_AddNewLink = "@xpath=//a[text()='Add New Link']";

		public static final String MAS_ShowInterfaceLink = "@xpath=//div[a[text()='Add MAS Switch']]//div[a[text()='Show Interfaces']]";

		public static final String MAS_HideInterfaceLink = "@xpath=//div[a[text()='Add MAS Switch']]//div[a[text()='Hide Interfaces']]";

		public static final String MAS_AddMASSwitch_header = "@xpath=//div[p[text()='Add MAS Switch']]";

		public static final String MAS_IMSPOPLocationDropdown = "@xpath=//div[input[@placeholder='Select ...']]";

		public static final String MAS_Cancelbutton = "@xpath=//button/span[text()='Cancel']";

		public static final String VPN_OKbutton = "@xpath=//button/span[text()='OK']";

		public static final String VPN_AddDeviceSuccessfulMessage = "@xpath=//div[span[text()='Site device created successfully']]";

		public static final String MAS_AddSwitchWarningMessage = "@xpath=//div/div/div[@style='color: red;']";

		public static final String VPN_EditSiteSuccessfulMessage = "@xpath=//div[span[text()='Site Order successfully updated.']]";

		public static final String VPN_UpdateSwitchSuccessfulMessage = "@xpath=//div[span[text()='Device successfully updated']]";

		public static final String MAS_DeleteSwitchSuccessfulMessage = "@xpath=//div[span[text()='MAS switch deleted successfully']]";

		public static final String MAS_ViewService_DeleteMASDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete?']";

		public static final String MAS_ViewService_DeleteButton = "@xpath=//div/button[text()='Delete']";

		public static final String MAS_editdevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Edit'])[1]";

		public static final String MAS_editdevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Edit'])[2]";

		public static final String MAS_SelectInterfacesdevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Select Interfaces'])[1]";

		public static final String MAS_SelectInterfacesdevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Select Interfaces'])[2]";

		public static final String MAS_deletefromservicedevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Delete from Service'])[1]";

		public static final String MAS_deletefromservicedevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Delete from Service'])[2]";

		public static final String VPN_viewdevice1 = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span[text()='View'])[1]";
		public static final String VPN_viewdevice2_View = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span//a[text()='View'])[1]";

		public static final String Hub_viewdevice1 = "@xpath=(//div[div[div[text()='Hub']]]//span//a[text()='View'])[1]";

		public static final String Spoke_viewdevice1 = "@xpath=(//div[div[div[text()='Spoke']]]//span//a[text()='View'])[1]";

		public static final String VPN_EditSite = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span[text()='Edit'])[1]";

		public static final String VPN_DeleteSite = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span[text()='Delete'])[1]";

		public static final String VPN_EditSiteHub = "@xpath=(//div[div[div[text()='Hub']]]//span[text()='Edit'])[1]";

		public static final String VPN_EditDeviceHub = "@xpath=(//div[div[div[text()='Hub']]]//span[text()='Edit'])[2]";

		public static final String VPN_EditSiteSpoke = "@xpath=(//div[div[div[text()='Spoke']]]//span[text()='Edit'])[1]";

		public static final String VPN_EditDeviceSpoke = "@xpath=(//div[div[div[text()='Spoke']]]//span[text()='Edit'])[2]";

		public static final String VPN_DeleteSiteHub = "@xpath=(//div[div[div[text()='Hub']]]//span[text()='Delete'])[1]";

		public static final String VPN_DeleteDeviceHub = "@xpath=(//div[div[div[text()='Hub']]]//span[text()='Delete'])[2]";

		public static final String VPN_DeleteSiteSpoke = "@xpath=(//div[div[div[text()='Spoke']]]//span[text()='Delete'])[1]";

		public static final String VPN_DeleteDeviceSpoke = "@xpath=(//div[div[div[text()='Spoke']]]//span//a[text()='Delete'])[1]";
		public static final String VPN_DeleteDeviceSiteOrder = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span//a[text()='Delete'])[1]";

		public static final String VPN_AddNewDevice = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span[text()='Add New Device'])[1]";
		public static final String VPN_AddNewDevice1 = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span//a[text()='Add New Device'])[1]";

		public static final String HUB_AddNewDevice = "@xpath=(//div[div[div[text()='Hub']]]//span//a[text()='Add New Device'])[1]";

		public static final String VPN_editdevice1 = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span[text()='Edit'])[2]";

		public static final String VPN_Deletedevice1 = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//span[text()='Delete'])[2]";
		public static final String VPN_Deletedevice2 = "@xpath=(//div[div[div[text()='VPN Site Orders']]]//a[text()='Delete'])[1]";

		public static final String MAS_viewdevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='View'])[2]";

		public static final String VPN_ViewDevice_Header = "@xpath=//div[text()='Device']";

		// Below are the fields values information under view MAS Switch page --
		// ="@xpath=

		public static final String VPN_View_DeviceNameValue = "@xpath=//div[label[text()='Device Name']]/following-sibling::div";

		public static final String VPN_View_VendorModelValue = "@xpath=//div[label[text()='Vendor/Model']]/following-sibling::div";

		public static final String VPN_View_ManagementAddressValue = "@xpath=//div[label[text()='Management Address']]/following-sibling::div";

		public static final String VPN_View_SnmproValue = "@xpath=//div[label[text()='Snmpro']]/following-sibling::div";

		public static final String VPN_View_CountryValue = "@xpath=//div[label[text()='Country']]/following-sibling::div";

		public static final String VPN_View_CityValue = "@xpath=//div[label[text()='City']]/following-sibling::div";

		public static final String VPN_View_SiteValue = "@xpath=//div[label[text()='Site']]/following-sibling::div";

		public static final String VPN_View_PremiseValue = "@xpath=//div[label[text()='Premise']]/following-sibling::div";

		public static final String VPN_View_ActionLink = "@xpath=(//button[@id='dropdown-basic-button'])[1]";

		public static final String VPN_View_TestColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Test']";

		public static final String VPN_View_StatusColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Status']";

		public static final String VPN_View_LastRefresh = "@xpath=//div/div[text()='Last Refresh:']";

		public static final String VPN_View_Action_EditLink = "@xpath=//div//a[text()='Edit']";

		public static final String VPN_Edit_PremiseLevel = "@xpath=//div//label[text()='Premise']";

		public static final String VPN_Backbutton = "@xpath=//span[text()='Back']";

		// -- Router Tool For MAS -- ="@xpath=

		public static final String RouterTool_header = "@xpath=//div/div[text()='Router Tools']";

		public static final String MAS_Router_IPV4CommandsDropdown = "@xpath=(//input[@placeholder='Select ...'])[1]";

		public static final String MAS_Router_IPV4CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";

		public static final String MAS_Router_IPV4Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[1]";

		public static final String MAS_Router_IPV6CommandsDropdown = "@xpath=(//input[@placeholder='Select ...'])[2]";

		public static final String MAS_Router_IPV6CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";

		public static final String MAS_Router_IPV6Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[2]";

		// -- Provider Equipment (CPE) panel in view service page -- ="@xpath=

		public static final String CPE_AddPEDeviceLink = "@xpath=//a[contains(text(),'Add CPE Device')]";

		public static final String CPEShowInterfaces = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div//div//a[contains(text(),'Show Interfaces')]";

		public static final String GetAddressCPE = "@xpath=//span[text()='Get Address']";

		public static final String ChangeAddressCPE = "@xpath=//span[text()='Change']";

		public static final String FreeIPCPE = "@xpath=//span[text()='Get Free IP']";

		public static final String AllocateSubnetButton = "@xpath=//span[text()='Allocate Subnet']";

		public static final String SelectDeviceCPEToggle = "@xpath=(//div[@class='react-switch-bg'])[1]";

		public static final String AddPrimiseToggle = "@xpath=(//div[@class='react-switch-bg'])[2]";

		public static final String SNMPCpe2c = "@xpath=//input[@name='snmpVersion' and @value='2c']";

		public static final String SNMPCpe3 = "@xpath=//input[@name='snmpVersion' and @value='3']";

		public static final String ConnectivityProtocolCPEtelnet = "@xpath=//input[@name='connectivityProtocol' and @value='telnet']";

		public static final String ConnectivityProtocolCPEssh = "@xpath=//input[@name='connectivityProtocol' and @value='ssh']";

		public static final String ViewCPEDevice = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']";

		public static final String EditCPEDevice = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='Edit']";

		public static final String SelectInterface = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='Select Interfaces']";

		public static final String DeleteCPE = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='Delete']";
		public static final String ConfigureCPE = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='Configure']";

		public static final String DeleteAny = "@xpath=//button[contains(@class,'btn-danger')]";

		public static final String CancelCPEDevice = "@xpath=//span[text()='Cancel']";

		public static final String InterfaceActionButton = "@xpath=//div[div[text()='Interfaces']]//button";

		public static final String RoutesActionButton = "@xpath=//div[div[text()='Routes']]//button";

		public static final String InterfaceActionCommonLink = "@xpath=//div[div[text()='Interfaces']]//div//a[contains(text(),'Value')]";
		public static final String InterfaceActionCommonLink1 = "@xpath=//div[div[text()='Interfaces']]//div//a[contains(text(),'";
		public static final String InterfaceActionCommonLink2 = "')]";

		public static final String RoutesActionCommonLink = "@xpath=//div[div[text()='Routes']]//div//a[contains(text(),'Value')]";
		public static final String RoutesActionCommonLink1 = "@xpath=//div[div[text()='Routes']]//div//a[contains(text(),'";
		public static final String RoutesActionCommonLink2 = "')]";

		public static final String SaveInterface = "@xpath=//span[text()='Save']";

		public static final String AddInterfacebutton = "@xpath=//span[text()='Add']";

		public static final String AddInterfacebutton2 = "@xpath=//button[@type='button']//span[text()='Add']";

		public static final String multilink_okButton = "@xpath=//span[text()='Ok']";

		public static final String Routes_okButton = "@xpath=//span[text()='OK']";

		public static final String InterfaceActionButtonCPE = "@xpath=(//button[@id='dropdown-basic-button'])[2]";

		public static final String InterfaceEditLink = "@xpath=(//button[@id='dropdown-basic-button'])[2]//following-sibling::div//a[text()='Edit']";

		public static final String InterfaceDeleteLink = "@xpath=(//button[@id='dropdown-basic-button'])[2]//following-sibling::div//a[text()='Delete ']";

		public static final String VPNSiteOrderRadio = "@xpath=//div[@col-id='siteOrder' and @unselectable='on']";

		public static final String RouterToolIPV4 = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";

		public static final String RouterToolIPV6 = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";

		public static final String RouterToolExecuteA = "@xpath=(//span[text()='Execute'])[1]";

		public static final String RouterToolExecuteB = "@xpath=(//span[text()='Execute'])[2]";

		public static final String RouterToolResult = "@xpath=//textarea[@name='responseData']";

		public static final String PE_showInterfacesLink = "@xpath=//div[a[text()='Add PE Device']]//div[a[text()='Show Interfaces']]";

		public static final String PE_HideInterfaceLink = "@xpath=//div[a[text()='Add PE Device']]//div[a[text()='Hide Interfaces']]";

		public static final String PE_ACTION = "@xpath=(//button[@id='dropdown-basic-button'])[4]";

		public static final String PE_AddPEDevice_header = "@xpath=//div//p[text()='Add PE Device']";

		// Router Tool For PE Device -- ="@xpath=

		// public static final String RouterTool_header
		// ="@xpath=//div/div[text()='Router Tools']";

		public static final String PE_Router_IPV4CommandsDropdown = "@xpath=(//input[@placeholder='Select ...'])[1]";

		public static final String PE_Router_IPV4CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";

		public static final String PE_Router_IPV4Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[1]";

		// -- Shiva Code-- ="@xpath=

		public static final String succeMessageFor_EIpAllocation = "@xpath=//div[div[text()='EIP Allocation']]//following-sibling::div//div[@role='alert']";

		public static final String successTextMessage_EIPAllocation = "@xpath=//div[div[text()='EIP Allocation']]//following-sibling::div//div[@role='alert']/span";

		public static final String PE_addInterface_networklabelName = "@xpath=(//label[text()='Network'])[1]";

		// -- PPP configuration -- ="@xpath=

		public static final String CPE_existingDeviceGrid = "@xpath=(//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row'])[2]";

		public static final String CPE_fetchAlldevice_InviewPage = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b";

		public static final String CPE_pppConfigurationLink = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//span[b[contains(text(),'value')]]/parent::div/following-sibling::div//span[text()='PPP Configuration']";
		public static final String CPE_pppConfigurationLink1 = "@xpath=//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//span[b[contains(text(),'";
		public static final String CPE_pppConfigurationLink2 = "')]]/parent::div/following-sibling::div//span[text()='PPP Configuration']";

		public static final String pppConfigurationPanelheader = "@xpath=//div[text()='Add']";

		public static final String pppConfiguration_CPEdevice_clickHereLink = "@xpath=//a/label[text()='Click here to add.']";

		public static final String addPPPconfigurationPanel_CPedevice = "@xpath=//div[text()='Add']";

		public static final String addPPPconfiguration_deviceName = "@xpath=(//div[label[text()='Device:']]/parent::div//label)[2]";

		public static final String addPPPconfiguration_generatePassowrd = "@xpath=//span[text()='Generate Password']";

		public static final String addPPPconfiguration_passwordField = "@xpath=//div[label[text()='PPP Password']]//input";

		public static final String addPPPcofig_famedWANipAddress = "@xpath=//div[label[text()='Framed/WAN IP Address']]//input";

		public static final String addPPPconfig_framedProtocol = "@xpath=//div[label[text()='Framed Protocol']]/parent::div//following-sibling::label";

		public static final String addPPPconfig_framedWANIpAddress = "@xpath=//div[label[text()='Framed/WAN IP Subnet Mask']]/parent::div//following-sibling::label";

		public static final String uniIngressPolicy_textField = "@xpath=//label[text()='Uni Ingress Policy']/parent::div//input";

		public static final String addPPPconfig_framedRouter0 = "@xpath=//div[label[contains(text(),'Framed Route0')]]//input";

		public static final String addPPPconfig_framedRouter1 = "@xpath=//div[label[contains(text(),'Framed Route1')]]//input";

		public static final String addPPPconfig_framedroute2 = "@xpath=//div[label[contains(text(),'Framed Route2')]]//input";

		public static final String addPPPconfig_framedRoute3 = "@xpath=//div[label[contains(text(),'Framed Route3')]]//input";

		public static final String addPPconfig_framedRoute4 = "@xpath=//div[label[contains(text(),'Framed Route4')]]//input";

		public static final String addPPPconfig_framedRoute5 = "@xpath=//div[label[contains(text(),'Framed Route5')]]//input";

		public static final String addPPPconfig_framedRoute6 = "@xpath=//div[label[contains(text(),'Framed Route6')]]//input";

		public static final String addPPPconfig_framedRoute7 = "@xpath=//div[label[contains(text(),'Framed Route7')]]//input";

		public static final String addPPPconfig_uniQOSprofileName = "@xpath=//label[text()=' Uni Qos Profile Name']/parent::div//input";

		public static final String addPPPconfig_uniVirtualRouterName = "@xpath=//div[label[contains(text(),'Uni Virtual Route Name')]]//input";

		public static final String addPPPconfig_uniLocalLoopbackInterface = "@xpath=//div[label[contains(text(),'Uni Local Loopback Interface')]]//input";

		public static final String addPPPconfig_QosParameters = "@xpath=//label[text()='Qos Parameters']/parent::div//input";

		public static final String addPPPconfig_QosParamertsTextField = "@xpath=//input[@id='qosParametersText']";

		public static final String addPPPconfig_SAvalidation = "@xpath=//label[text()='SA Validation']/parent::div//input";

		public static final String addPPPconfig_uniSphereIGMPenable = "@xpath=//label[text()=' Unisphere -IGMP-enable']/parent::div//input";

		public static final String addPPPconfig_uniSphereIGMPversion = "@xpath=//label[text()=' Unisphere -IGMP-version']/parent::div//input";

		public static final String addPPPconfig_rightArrowButtonForSelection = "@xpath=//span[text()='>>']";

		public static final String addPPPconfig_selectedQosParameters = "@xpath=//select[@id='selectedQosParameters']";

		public static final String addPPPconfig_IPv6Paramert = "@xpath=//label[text()='IPv6 parameters']/parent::div//input";

		public static final String addConfig_framedIPv6Prefix = "@xpath=//div[div[label[contains(text(),'Framed-IPv6-Prefix ')]]]//input";

		public static final String addPPPconfig_framedIPV6address = "@xpath=//div[div[label[contains(text(),'Framed-IPv6-Address')]]]//input";

		public static final String addPPPconfig_delegateIPv6Prefix = "@xpath=//div[div[label[contains(text(),'Delegate-IPv6-Prefix:')]]]//input";

		public static final String addPPPconfig_framedIPv6route0 = "@xpath=//div[div[label[text()='Framed-IPv6-Route0']]]//input";

		public static final String addPPPconfig_framedIPv6route1 = "@xpath=//div[div[label[text()='Framed-IPv6-Route1']]]//input";

		public static final String addPPPconfig_framedIPv6route2 = "@xpath=//div[div[label[text()='Framed-IPv6-Route2']]]//input";

		public static final String addPPPconfig_framedIPv6Route3 = "@xpath=//div[div[label[text()='Framed-IPv6-Route3']]]//input";

		public static final String addPPPconfig_framedIPv6route4 = "@xpath=//div[div[label[text()='Framed-IPv6-Route4']]]//input";

		public static final String addPPPconfig_framedIPv6Route5 = "@xpath=//div[div[label[text()='Framed-IPv6-Route5']]]//input";

		public static final String addPPPconfig_framedIPv6Route6 = "@xpath=//div[div[label[text()='Framed-IPv6-Route6']]]//input";

		public static final String addPPPconfig_framedIPv6Route7 = "@xpath=//div[div[label[text()='Framed-IPv6-Route7']]]//input";

		public static final String addPPPconfig_uniIPv6LocalInterface = "@xpath=//label[contains(text(),'Uni-IPv6-Local-Interface')]/parent::div//input";

		public static final String addPPPconfig_uniIPv6Virtualrouter = "@xpath=//label[contains(text(),'Uni-IPv6-Virtual-Router')]/parent::div//input";

		public static final String addPPPconfig_uniIngressStatistics = "@xpath=//label[contains(text(),'Uni-Ingress-Statistics')]/parent::div//input";

		public static final String addPPPconfig_viewPage_actionDrpodown = "@xpath=//button[text()='Action']";

		public static final String pppConfigurationViewPage_editLink = "@xpath=//a[text()='Edit']";

		public static final String pppConfigurationViewPage_deleteLink = "@xpath=//a[text()='Delete']";

		public static final String addPPPconfig_OKbutton = "@xpath=//span[text()='Ok']";

		public static final String viewPPPconfig_ActionDropdown = "@xpath=//div[text()='View']/parent::div//button[text()='Action']";

		public static final String pppConfig_editLink = "@xpath=//a[text()='Edit']";

		public static final String editPPPconfiguration_panelHeader = "@xpath=//div[text()='Edit']";

		public static final String deletePPPconfigurationLink = "@xpath=//a[text()='Delete']";

		public static final String pppConfiguration_deletepopup = "@xpath=//div[@class='modal-content']";

		public static final String pppConfiguration_deletePopup_TextMessage = "@xpath=//div[@class='col-12 col-sm-12 col-md-12']";

		public static final String deletePPPconfigurationPopup_deleteButton = "@xpath=//span[text()='Delete']";

		public static final String viewServicepage_OrderPanel = "@xpath=//div[text()='Order']";

		public static final String Editservice_actiondropdown = "@xpath=//div[div[text()='Service']]//button[text()='Action']";

		public static final String Editservice_sysnchronizelink = "@xpath=//a[text()='Synchronize']";

		public static final String alertForSynchronize = "@xpath=//div[@role='alert']";

		public static final String alertMSG_synchronize = "@xpath=//div[@role='alert']/span";

		// public static final String viewServicepage_OrderPanel
		// ="@xpath=//div[text()='Order']";

		// public static final String Editservice_actiondropdown
		// ="@xpath=//div[div[text()='Service']]//button[text()='Action']";

		public static final String Editservice_infovistareport = "@xpath=//a[text()='Show New Infovista Report']";

		public static final String devicePanelHeaders_InViewSiteOrderPage = "@xpath=//div[div[text()='value']]";
		public static final String devicePanelHeaders_InViewSiteOrderPage1 = "@xpath=//div[div[text()='";
		public static final String devicePanelHeaders_InViewSiteOrderPage2 = "']]";

		public static final String PEdevice_adddevicelink = "@xpath=//div[div[div[text()='Provider Equipment (PE)']]]//a[text()='Add PE Device']";

		public static final String chooseAdeviceDropdown = "@xpath=//div[label[text()='Choose a device']]//input";

		public static final String viewPEdevicePage_fetchDeviceDetails = "@xpath=//div[div[label[contains(text(),'value')]]]/div[2]";
		public static final String viewPEdevicePage_fetchDeviceDetails1 = "@xpath=//div[div[label[contains(text(),'";
		public static final String viewPEdevicePage_fetchDeviceDetails2 = "')]]]/div[2]";

		public static final String nextButton = "@xpath=//span[text()='Next']";

		// Add Interface PE device -- ="@xpath=

		public static final String routerToolPanelheader = "@xpath=//div[div[text()='Router Tools']]";

		public static final String InterfacePanel_actionDropdown = "@xpath=//div[div[text()='Interfaces']]//following-sibling::div//button[text()='Action']";

		public static final String addInterfaceOrLink = "@xpath=//a[text()='Add Interface']";

		public static final String addMultilinkLink = "@xpath=//a[text()=' Add Multlink']";

		public static final String addLoopBackLink = "@xpath=//a[contains(text(),'Add Loopback')]";

		public static final String configureInterfaceOnDevice_checkbox = "@xpath=//input[@id='configInterfaceOnDevice']";

		public static final String networkLabelname_IPv6 = "@xpath=(//label[text()='Network'])[2]";

		public static final String configureInterfaceOnDevice_LabelName = "@xpath=//label[contains(text(),'Configure Interface on Device')]";

		public static final String getAddress_IPv4 = "@xpath=(//span[text()='Get Address'])[1]";

		public static final String EIPallocation_addInterfaceIPV4 = "@xpath=(//span[contains(text(),'EIP Allocation')])[1]";

		public static final String EIPallocation_addInterfaceIpv6 = "@xpath=(//span[contains(text(),'EIP Allocation')])[2]";

		public static final String subnetSize_dropdown = "@xpath=//div[label[text()='Subnet Size']]//input";

		public static final String availableBlocks_dropdown = "@xpath=//div[label[text()='Available Blocks']]//input";

		public static final String allocateSubnetButton = "@xpath=//span[contains(text(),'Allocate Subnet')]";

		public static final String EIPallocation_xButton = "@xpath=//span[contains(text(),'�')]";

		public static final String FetchInterfaceAddressRangeDropdown_addInterface = "@xpath=(//div[label[text()='Interface Address Range']]//span)[2]";

		public static final String fetchInterfaceAddressRangeDropdown_addInterfaceIPv6 = "@xpath=(//div[label[text()='Interface Address Range']]//span)[4]";
		public static final String fetchInterfaceAddressRangeDropdown_addInterfaceIPv61 = "@xpath=(//div[label[text()='Interface Address Range']]//span)[3]";
		public static final String FetchInterfaceAddressRangeDropdown_addMultilink = "@xpath=(//div[label[text()='Interface Address Range']]//span)[1]";

		public static final String fetchInterfaceAddressRangeDropdown_addMultilinkIPv6 = "@xpath=(//div[label[text()='Interface Address Range']]//span)[2]";
		public static final String fetchInterfaceAddressRangeDropdown_addMultilinkIPv61 = "@xpath=(//div[label[text()='Interface Address Range']]//span)[3]";

		public static final String FetchInterfaceDropdown_addInterface = "@xpath=(//div[label[text()='Address']])[1]//input";

		public static final String FetchInterfaceDropdown_addInterfaceIPv6 = "@xpath=(//div[label[text()='Address']])[2]//input";

		public static final String interfaceAddressRange_textField = "@xpath=(//div[label[text()='Interface Address Range']]//input)[1]";

		public static final String interfaceAddressRangeIPv6_textField = "@xpath=(//div[label[text()='Interface Address Range']]//input)[2]";

		public static final String interfaceAddress_textField = "@xpath=(//input[@id='interfaceIPV4Address'])[1]";

		public static final String interfaceAddressIPv6_textField = "@xpath=(//input[@id='interfaceIPV6Address'])[1]";

		public static final String rightArrowButton_interfaceAddressRange = "@xpath=(//i[@class='fa fa-chevron-circle-right'])[1]";

		public static final String rightArrowButton_interfaceAddressRangeIPv6 = "@xpath=(//i[@class='fa fa-chevron-circle-right'])[2]";

		public static final String fetchValuefromAddressDropdown = "@xpath=(//div[label[text()='Address']]//span)[2]";

		public static final String addInterface_changeButton = "@xpath=(//span[contains(text(),'Change')])[1]";

		public static final String addInterface_changeButtonIPv6 = "@xpath=(//span[contains(text(),'Change')])[2]";

		public static final String Link_textField = "@xpath=//input[@id='linkName']";

		public static final String bearerType_Dropdown = "@xpath=//div[label[text()='Bearer Type']]//input";

		public static final String fetchBearerTypedropdownvalue = "@xpath=//div[label[text()='Bearer Type']]//div//span";

		public static final String bearerNumber_addtextField = "@xpath=//input[@id='bearerNumber']";

		public static final String encapsulation_Dropdown = "@xpath=//div[label[text()='Encapsulation']]//input";

		public static final String slot_textField = "@xpath=//div[label[text()='Slot']]//input";

		public static final String port_textField = "@xpath=//div[label[text()='Port']]//input";

		public static final String vlanId_textField = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String unitId_textField = "@xpath=//div[label[text()='Unit ID']]//input";

		public static final String pic_textField = "@xpath=//div[label[text()='Pic']]//input";

		public static final String stm1Number_textField = "@xpath=//div[label[text()='STM1 Number']]//input";

		public static final String bandwidth_Dropdown = "@xpath=//div[label[text()='Bandwidth']]//input";

		public static final String cardType_Dropdown = "@xpath=//div[label[text()='Card Type']]//input";

		public static final String framingType_Dropdown = "@xpath=//div[label[text()='Framing Type']]//input";

		public static final String clockSource_Dropdown = "@xpath=//div[label[text()='Clock Source']]//input";

		public static final String timeSlot_textField = "@xpath=//div[label[text()='Timeslot']]//input";

		public static final String Interface_textField = "@xpath=//div[label[text()='Interface']]//input";

		public static final String PPPencapsulation_Dropdown = "@xpath=(//div[label[text()='PPP Encapsulation']]//input)[1]";

		public static final String DSLdownstreamSpead_Dropdown = "@xpath=//div[label[text()='DSL Downstream Speed']]//input";

		public static final String DSLupstreamSpeed_Dropdown = "@xpath=//div[label[text()='DSL Upstream Speed']]//input";

		public static final String addMultilink_DSLdownStreamSpeeedPCR = "@xpath=(//div[label[text()='DSL Downstream Speed (PCR)']]//input)[1]";

		public static final String addMultilink_DSLupstreamSpeedSCR = "@xpath=(//div[label[text()='DSL Upstream Speed (SCR)']]//input)[1]";

		public static final String vpi_textField = "@xpath=//input[@id='vpi']";

		public static final String vci_textField = "@xpath=//input[@id='vci']";

		public static final String sipBat_textField = "@xpath=//input[@id='SIPBay']";

		public static final String mbsDropdown = "@xpath=//label[text()='MBS']/parent::div//input";

		public static final String interfaceAddress_TextField = "@xpath=//label[text()='Interface Address']/parent::div//input";

		public static final String addLoopback_interfaceTextField = "@xpath=//label[text()='Interface']/parent::div//input";

		public static final String configureOnBackupBRX_checkbox = "@xpath=//label[text()='Configure on backup BRX']/parent::div//input";

		public static final String IVmanagement_checkbox = "@xpath=//div[label[text()='IV management']]//input";

		public static final String addMultilink_networkLabelName = "@xpath=(//label[text()='Network'])[1]";

		public static final String configuraInterfaceCheckboxSelection = "@xpath=//label[text()='Configure Interface on Device']/parent::div//input";

		public static final String addInterface_okButton = "@xpath=//span[text()='OK']";

		public static final String addMultilink_okButton = "@xpath=//span[text()='Ok']";

		public static final String fetchDeviceName_PEdevice = "@xpath=//label[text()='Name']/parent::div//following-sibling::div[@class='customLabelValue form-label']";

		public static final String selectFirstValueUnderAddressDropdown = "@xpath=(//div[@class='sc-bxivhb kqVrwh'])[1]/div";

		public static final String addMultilink_multilinkTableHeader = "@xpath=//div[text()='Multilinked Bearers']";

		public static final String addMultilink_checkToAddColumnName = "@xpath=//div[text()='Multilinked Bearers']/parent::div//following-sibling::div//span[text()='Check to add to Multilink']";

		public static final String addMultilinkTable_interfaceColumnName = "@xpath=//div[text()='Multilinked Bearers']/parent::div//following-sibling::div//span[text()='Interface']";

		public static final String addMultilinkTable_linkOrcircuitColumnName = "@xpath=//div[text()='Multilinked Bearers']/parent::div//following-sibling::div//span[text()='Link / Circuit Id']";

		public static final String addMultilinkTable_bearerTypeColumnName = "@xpath=//div[text()='Multilinked Bearers']/parent::div//following-sibling::div//span[text()='Bearer Type']";

		public static final String addMultilinkTable_vlanIDcolumnName = "@xpath=//div[text()='Multilinked Bearers']/parent::div//following-sibling::div//span[text()='VLAN Id']";

		public static final String PEdevice_backButton = "@xpath=//span[text()='Back']";

		public static final String viewPEDevice_deviceName = "@xpath=//div[label[text()='Name']]//following-sibling::div";

		public static final String viewPEdveice_fetchVendorName = "@xpath=//div[label[text()='Vendor/Model']]//following-sibling::div";

		public static final String viewPEdevice_fetchManagementAddress = "@xpath=//div[label[text()='Management Address']]//following-sibling::div";

		public static final String addInterface_confiugrationPanelheader = "@xpath=//div[div[text()='Configuration']]";

		public static final String addInterface_generateLink = "@xpath=//span[text()='Generate']";

		public static final String addInterface_configurationtextArea = "@xpath=//textarea[@name='configurationDetails']";

		public static final String addInterface_executeAndSaveButton = "@xpath=//span[text()='Execute And Save']";

		public static final String interface_warningMessage = "@xpath=//li[text()='Interface is required.']";

		public static final String BearerType_warningMessage = "@xpath=//li[text()='Bearer Type is required.']";

		public static final String encapsulation_warningMessage = "@xpath=//li[text()='Encapsulation is required.']";

		public static final String slot_warningMessage = "@xpath=//li[text()='Slot is required.']";

		public static final String port_warningMessage = "@xpath=//li[text()='Port is required.']";

		public static final String vpi_warningMessage = "@xpath=//li[text()='VPI is required.']";

		public static final String vci_warningMessage = "@xpath=//li[text()='VCI is required.']";

		public static final String configuration_warningMessage = "@xpath=//li[text()='Configuration is required.']";

		public static final String interfaceAddress_warningMessage = "@xpath=//div[text()='Interface Address']";

		public static final String PEdevice_showInterfaceLink = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div//following-sibling::div//a[text()='Show Interfaces']";

		public static final String selectInterfaceUnderPEdevice = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div//following-sibling::div//div[div[contains(text(),'value')]]//input";
		public static final String selectInterfaceUnderPEdevice1 = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div//following-sibling::div//div[div[contains(text(),'";
		public static final String selectInterfaceUnderPEdevice2 = "')]]//input";

		public static final String actionDropdown_UnderProviderEquipmentPanel = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div//following-sibling::div//button[text()='Action']";

		public static final String editLink_underProviderEquipment = "@xpath=//a[text()='Edit']";

		// !-- Router Panel -- ="@xpath=

		public static final String commandIPV4_dropdown = "@xpath=//div[label[text()='Command IPV4']]//input";

		public static final String commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";

		public static final String commandIPv4_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[1]";

		public static final String commandIPV6_dropdown = "@xpath=//div[label[text()='Command IPV6']]//input";

		public static final String commandIPv6_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";

		public static final String commandIPv6_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])";

		public static final String executebutton_Ipv4 = "@xpath=(//span[text()='Execute'])[1]";

		public static final String executebutton_IPv6 = "@xpath=(//span[text()='Execute'])[2]";

		public static final String result_textArea = "@xpath=//div[label[text()='Result']]//textarea";

		// !-- Fetch Device INterface -- ="@xpath=

		public static final String viewDevicePage_ActionDropdown = "@xpath=//div[text()='Device']/parent::div//button[text()='Action']";

		public static final String viewDevicePage_fetchDeviceInterfaceLink = "@xpath=//a[text()='Fetch Device Interfaces']";

		public static final String fetchDeviceInterace_SuccessMessage = "@xpath=//div[contains(text(),'Fetch interfaces started successfully. Please check the sync status of this device.')]";

		public static final String fetchdeviceInterface_successtextMessage = "@xpath=(//div[contains(@class,'alert alert-success alert-Style')])[1]";

		public static final String fetchDeviceInterface_hereLink = "@xpath=//a[text()='here']";

		// !-- For Success Message -- ="@xpath=

		public static final String AlertForServiceCreationSuccessMessage = "@xpath=//div[@role='alert']/span";

		public static final String serivceAlert = "@xpath=//div[@role='alert']";

		// String !-- breadcrump common code -- ="@xpath=

		public static final String breadcrump = "@xpath=//a[text()='value']";
		public static final String breadcrump1 = "@xpath=//a[text()='";
		public static final String breadcrump2 = "']";

		// !-- Select Interface -- ="@xpath=

		public static final String existingdevicegrid_PEdevice = "@xpath=(//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row'])//b";

		public static final String existingdevicegrid = "@xpath=(//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row'])[2]";

		public static final String PE_fetchAlldevice_InviewPage = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b";

		public static final String PE_selectInterface_InViewPage = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";
		public static final String PE_selectInterface_InViewPage1 = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'";
		public static final String PE_selectInterface_InViewPage2 = "')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";

		public static final String interfacesInServices_searchTextBox = "@xpath=//div[text()='Interfaces in Service']/parent::div//following-sibling::div//input[@id='filter-text-box']";
		public static final String interfacescheckBoxes = "@xpath=(//div[text()='Interfaces in Service']//parent::div)[1]//following-sibling::div//input[@type='radio']";

		public static final String interfacesToSelect_searchtextBOx = "@xpath=//div[text()='Interfaces to Select']/parent::div//following-sibling::div//input[@id='filter-text-box']";

		public static final String selectinterfaceUnderInterfacesInService = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='value']]//input";
		public static final String selectinterfaceUnderInterfacesInService1 = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='";
		public static final String selectinterfaceUnderInterfacesInService2 = "']]//input";

		public static final String InterfaceInselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";

		public static final String InterfaceInselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";

		public static final String InterfaceInselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";

		public static final String InterfaceInselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";

		public static final String InterfaceToselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";

		public static final String InterfaceToselect_addbuton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";

		public static final String InterfaceToselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";

		public static final String InterfaceToselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";

		public static final String InterfaceToselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";

		public static final String InterfaceToselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";

		public static final String InterfaceInselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";

		public static final String InterfaceInselect_removebuton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";

		public static final String InterfaceToselect_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String InteraceColumn_Filter = "@xpath=(//div[div[text()='Interfaces to Select']]//following-sibling::div//span[@class='ag-icon ag-icon-menu'])[2]";

		public static final String filterTxt = "@xpath=//input[@id='filterText']";

		public static final String InterfaceInService_panelHeader = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]";

		public static final String InterfaceToSelect_interfaceColumnHeader = "@xpath=//div[div[text()='Interfaces to Select']]//following-sibling::div//span[text()='Interface']";

		public static final String InterfacetoSelect_listOFinterfaceValuesDisplaying = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String PEdevice_deleteLink = "@xpath=//a[text()='Delete']";

		// AutodiscoverVPN
		public static final String clickOnAutoDiscoverVPNunderProviderEquipmentPanel = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div//following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Autodiscover VPNs']";
		public static final String clickOnAutoDiscoverVPNunderProviderEquipmentPanel1 = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div//following-sibling::div[@class='div-margin row']//b[contains(text(),'";
		public static final String clickOnAutoDiscoverVPNunderProviderEquipmentPanel2 = "')]/parent::div/following-sibling::div//span[text()='Autodiscover VPNs']";

		public static final String providerEquipment_panelHeader = "@xpath=//div[text()='Provider Equipment (PE)']";

		// !-- CPE TO CPE Link -- ="@xpath=

		public static final String CPEtoCPEpanelHeader = "@xpath=//div[text()='CPE to CPE Links']";

		public static final String CPEtoCPELink_AddNewLink = "@xpath=//a[text()='Add New Link']";

		public static final String CPEtoCPELinkPage_Header = "@xpath=//div[text()='Add New Link']";

		public static final String CPEtoCPElink_SourceDeviceWarningMessage = "@xpath=//div[text()='Source Device']";

		public static final String CPEtoCPElink_SourceInterfaceWarningMessage = "@xpath=//div[text()='Source Interface']";

		public static final String CPEtoCPElink_TargetDeviceWarningMessage = "@xpath=//div[text()='Target Device']";

		public static final String CPEtoCPElink_TargetInterfaceWarningMessage = "@xpath=//div[text()='Target Interface']";

		public static final String CPEtoCPELink_sourceDeviceDropdown = "@xpath=//label[text()='Source Device']/parent::div//input";

		public static final String CPEtoCPELink_sourceInterfaceDropdown = "@xpath=//label[text()='Source Interface']/parent::div//input";

		public static final String CPEtoCPELink_targetDeviceDropdown = "@xpath=//label[text()='Target Device']/parent::div//input";

		public static final String CPEtoCPELink_targetInterfaceDropdown = "@xpath=//label[text()='Target Interface']/parent::div//input";

		public static final String CPEtoCPELink_nextButton = "@xpath=//span[text()='Next']";

		public static final String selectCreatedLink_UnderCPEtoCPElinkPanel = "@xpath=//div[div[text()='CPE to CPE Links']]//following-sibling::div//div[div[contains(text(),'value')]]//input";

		public static final String CPEtoCPElink_ActionDropdown = "@xpath=//div[text()='CPE to CPE Links']/parent::div//following-sibling::div//button[text()='Action']";

		public static final String CPEtoCPELink_deleteLink = "@xpath=//a[text()='Delete ']";

		public static final String deleteMessages_textMessage = "@xpath=//div[@class='modal-body']";

		// public static final String delete_alertpopup
		// ="@xpath=//div[@class='modal-content']";

		// public static final String deletebutton ="@xpath=//button[@class='btn
		// btn-danger']";

		// !-- For Actelis -- ="@xpath=

		public static final String Actelisconfig_addDSLAM = "@xpath=//div[div[div[text()='Actelis Configuration']]]//a[text()='Add DSLAM and HSL']";

		public static final String DSLM_Device_Select = "@xpath=//div[div[div[label[@class='form-label']]]]//input";

		public static final String List_HSL_Link = "@xpath=//span[contains(text(),'List HSL')]";

		public static final String ActelisConfigurationPanel = "@xpath=//div[div[contains(text(),'Actelis Configuration')]]";

		public static final String actelis_EquipConfig_viewLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='View']";

		public static final String actelis_EquipConfig_deleteLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='Delete']";

		public static final String addDSLAMandHSL_xButton = "@xpath=//div[div[label[text()='DSLAM Device:']]]//following-sibling::div//div[text()='�']";

		public static final String selectDSLAMdeviceValue = "@xpath=//div[text()='value']";
		public static final String selectDSLAMdeviceValue1 = "@xpath=//div[text()='";
		public static final String selectDSLAMdeviceValue2 = "']";

		public static final String fetchInterface_AddDSLAMdevice = "@xpath=//span[contains(text(),'Fetched interfaces successfully')]";

		public static final String InterfaceToSelect_actelis_totalpage = "@xpath=//span[@ref='lbTotal']";

		public static final String InterfaceToSelect_actelis_currentpage = "@xpath=//span[@ref='lbCurrent']";

		public static final String showInterface_ActelisCnfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//a[text()='Show Interfaces']";

		public static final String AcionButton_ActelisConfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//button[text()='Action']";

		public static final String removeButton_ActelisConfiguration = "@xpath=//a[text()='Remove']";

		public static final String popupMessage_forRemove_ActelisConfiguration = "@xpath=//div[@class='modal-body']";

		public static final String deleteInterface_successMessage_ActelisCOnfiguration = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

		public static final String successMessage_ActelisConfiguration_removeInterface = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

		// !-- Delete PE device -- ="@xpath=

		public static final String PE_deleteFromService = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete from Service']";
		public static final String PE_deleteFromService1 = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'";
		public static final String PE_deleteFromService2 = "')]/parent::div/following-sibling::div//span[text()='Delete from Service']";

		//////////////////////////////////////////// Newly Added//////
		public static final String managementoptions_performancereporting = "@xpath=//label[contains(text(),'Performance Reporting')]/parent::div/following-sibling::div ";
		public static final String managementoptions_ipguardian = "@xpath=//label[contains(text(),'IP Guardian')]/parent::div/following-sibling::div ";
		public static final String providerequipment_header = "@xpath=//div[text()='Provider Equipment (PE)']";
		public static final String addpedevice_link = "@xpath=a[text()='Add PE Device']";
		public static final String addpedevice_header = "@xpath=//div[text()='Add Ethernet PE Device']";
		public static final String addnewdevice_togglebutton = "@xpath=(//div[@class='react-switch-handle'])[1]";
		public static final String VPNVendorModel1 = "@xpath=(//div[contains(text(),'+ VPNVendorModel +')])[1]";
		public static final String VPNVendorModel2 = "@xpath=(//div[div[label[text()='Vendor/Model']]]//input)[1]";
		public static final String VPNVendorModel3 = "')])[1]";

		public static final String deleteInterafce1 = "@xpath=//div[text()='";
		public static final String deleteInterafce2 = "']";
		public static final String deleteInterfaceX = "@xpath=//button[@class='btn btn-danger']";

	}
}
