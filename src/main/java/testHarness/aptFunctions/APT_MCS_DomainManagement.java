package testHarness.aptFunctions;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_DoimainManagementObj;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;

public class APT_MCS_DomainManagement extends SeleniumUtils {

	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();

	public void selectCustomertocreateOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String ChooseCustomerToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newCustomer");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.ManageCustomerServiceLink,
				"Manage Customer Service Link");
		click(APT_DoimainManagementObj.Domainmanagement.ManageCustomerServiceLink, "Manage Customer Service Link");
		waitForAjax();

		verifyExists(APT_DoimainManagementObj.Domainmanagement.CreateOrderServiceLink, "Create Order Service Link");
		click(APT_DoimainManagementObj.Domainmanagement.CreateOrderServiceLink, "Create Order Service Link");

		// click_commonMethod(application, "Create Order/Service",
		// "CreateOrderServiceLink", xml);
		// Log.info("=== Create Order/Service navigated ===");

		// click on Next button to check mandatory messages
		//verifyExists(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");
		//click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");

		// Customer Error message
		//verifyExists(APT_DoimainManagementObj.Domainmanagement.customer_createorderpage_warngmsg, "Choose a customer");

		// Entering Customer name
		verifyExists(APT_DoimainManagementObj.Domainmanagement.entercustomernamefield, "Customer Name");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.entercustomernamefield, ChooseCustomerToBeSelected);

		waitForAjax();
		//verifyExists(APT_DoimainManagementObj.Domainmanagement.entercustomernamefield, "Customer Name");
		//sendKeys(APT_DoimainManagementObj.Domainmanagement.entercustomernamefield, "*");

		// Select Customer from dropdown
		addDropdownValues_commonMethod("Choose a customer",
				APT_DoimainManagementObj.Domainmanagement.chooseCustomerdropdown, ChooseCustomerToBeSelected);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.nextbutton, "Next");
		click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next");

		waitforPagetobeenable();
	}

	public static String newordernumber, newVoiceLineNumber, SelectOrderNumber;

	public void createorderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String neworder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderService");
		String neworderno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingOrderService");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		scrollDown(APT_DoimainManagementObj.Domainmanagement.nextbutton);
		waitForAjax();
		// ScrolltoElement(application, "nextbutton", xml);
		// Thread.sleep(1000);

		//verifyExists(APT_DoimainManagementObj.Domainmanagement.nextbutton, "Next");
		//click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next");
		// click_commonMethod(application, "Next", "nextbutton", xml);
		waitforPagetobeenable();

		// Warning messages verify
		//verifyExists(APT_DoimainManagementObj.Domainmanagement.servicetype_warngmsg, "Service Type");
		// warningMessage_commonMethod(application, "servicetype_warngmsg",
		// "Service Type", xml);

		if (neworder.equalsIgnoreCase("YES")) {
			waitForAjax();
			// Thread.sleep(2000);
			verifyExists(APT_DoimainManagementObj.Domainmanagement.newordertextfield, "Order/Contract Number");
			sendKeys(APT_DoimainManagementObj.Domainmanagement.newordertextfield, neworderno);
			// addtextFields_commonMethod(application, "Order/Contract Number",
			// "newordertextfield", neworderno, xml);

			verifyExists(APT_DoimainManagementObj.Domainmanagement.newrfireqtextfield, "RFI Voice line Number");
			sendKeys(APT_DoimainManagementObj.Domainmanagement.newrfireqtextfield, newrfireqno);
			// addtextFields_commonMethod(application, "RFI Voice line Number",
			// "newrfireqtextfield", newrfireqno, xml);

			verifyExists(APT_DoimainManagementObj.Domainmanagement.createorderbutton, "create order");
			click(APT_DoimainManagementObj.Domainmanagement.createorderbutton, "create order");
			waitforPagetobeenable();

			// click_commonMethod(application, "create order",
			// "createorderbutton", xml);
			waitforPagetobeenable();
			CreateFirewall.verifysuccessmessage();
			scrollDown(APT_DoimainManagementObj.Domainmanagement.CreateOrderHeader);
			// verifysuccessmessage(application, "Order created successfully");
			// ScrolltoElement(application, "CreateOrderHeader", xml);

			newordernumber = neworderno;
			newVoiceLineNumber = newrfireqno;
		}
		else if (existingorderservice.equalsIgnoreCase("YES")) {
			verifyExists(APT_DoimainManagementObj.Domainmanagement.selectorderswitch, "select order switch");
			click(APT_DoimainManagementObj.Domainmanagement.selectorderswitch, "select order switch");

			// click_commonMethod(application, "select order switch",
			// "selectorderswitch", xml);

			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
					APT_DoimainManagementObj.Domainmanagement.existingorderdropdown, existingordernumber);
			waitForAjax();
			// Log.info("=== Order Contract Number selected===");
			// Thread.sleep(2000);

			SelectOrderNumber = existingordernumber;
		} else {
			Report.LogInfo("INFO", "Order not selected", "PASS");
		}
	}

	public void verifyselectservicetype(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");

		// select service type
		scrollDown(APT_DoimainManagementObj.Domainmanagement.servicetypetextfield);
		
		boolean availability = false;
		try {

			if (isVisible(APT_DoimainManagementObj.Domainmanagement.servicetypetextfield)) {
				Report.LogInfo("INFO", "Service Type dropdown is displaying", "PASS");
				
				if (servicetype.equalsIgnoreCase("null")) {
					Report.LogInfo("INFO", " No values selected under Service Type dropdown", "PASS");

				} else {
					addDropdownValues_commonMethod("Service Type", APT_DoimainManagementObj.Domainmanagement.servicetypetextfield,
							servicetype);
					/*webDriver.findElement(By.xpath("//div[label[text()='Service Type']]//div[text()='�']"));
					waitForAjax();
					List<WebElement> listofvalues = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));

					Report.LogInfo("INFO", "List of values inside Service Type dropdown is:  ", "PASS");

					for (WebElement valuetypes : listofvalues) {
						Report.LogInfo("INFO", "service sub types : " + valuetypes.getText(), "PASS");
					}

					waitForAjax();
					webDriver.findElement(By.xpath("//div[label[text()='Service Type']]//input")).sendKeys(servicetype);
					waitForAjax();
					scrollDown(APT_DoimainManagementObj.Domainmanagement.nextbutton);
					waitForAjax();
					webDriver.findElement(By.xpath("(//div[text()='" + servicetype + "'])[1]")).click();
					*/
					String actualValue = webDriver
							.findElement(By.xpath("//label[text()='Service Type']/following-sibling::span"))
							.getText();
					Report.LogInfo("INFO", "Service Type dropdown value selected as: " + actualValue, "PASS");

				}
			} else {
				Report.LogInfo("INFO", "Service Type is not displaying", "FAIL");
			}
		} catch (NoSuchElementException e) {
			Report.LogInfo("INFO", "Service Type is not displaying", "FAIL");

		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("INFO", "Not able to perform selection under Service Type dropdown", "FAIL");
		}
		// click on next button
		verifyExists(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");
		click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");
		// click_commonMethod(application, "Next", "nextbutton", xml);
		waitforPagetobeenable();
		compareText("Create Order / Service Header",
				APT_DoimainManagementObj.Domainmanagement.createorderservice_header, "Create Order / Service");
	}

	public void verifyservicecreation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String orderno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");
		String phonecontact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServicePhone");
		String user = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceUser");
		String defaultemail = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceDefaultEmail");
		String servicefirstname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceFirstName");
		String servicelastname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceLastName");
		String organizationname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"OrganizationName");
		String serviceaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceAddress");
		String servicepostalcode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServicePostalCode");
		String servicecity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceCity");
		String servicestate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceState");
		String servicecountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceCountry");
		String servicephone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServicePhone");
		String servicefax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceFax");
		String email1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceEmail");

		// Cancel service creation
		compareText("Create Order / Service Header",
				APT_DoimainManagementObj.Domainmanagement.createorderservice_header, "Create Order / Service");
		scrollDown(APT_DoimainManagementObj.Domainmanagement.createorderservice_header);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.cancelbutton, "cancel button");
		click(APT_DoimainManagementObj.Domainmanagement.cancelbutton, "cancel button");
		waitforPagetobeenable();

		if (isVisible(APT_DoimainManagementObj.Domainmanagement.customerdetailsheader)) {
			Report.LogInfo("INFO", "Navigated to create order page", "PASS");

		}

		// Create service
		scrollDown(APT_DoimainManagementObj.Domainmanagement.createorderservice_header);
		// ScrolltoElement(application, "createorderservice_header", xml);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.selectorderswitch, "select order switch");
		click(APT_DoimainManagementObj.Domainmanagement.selectorderswitch, "select order switch");
		waitForAjax();

		addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
				APT_DoimainManagementObj.Domainmanagement.existingorderdropdown, orderno);
		waitForAjax();
		addDropdownValues_commonMethod("Service Type", APT_DoimainManagementObj.Domainmanagement.servicetypetextfield,
				servicetype);

		// click on next button
		scrollDown(APT_DoimainManagementObj.Domainmanagement.nextbutton);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");
		click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");

		waitforPagetobeenable();
		compareText("Create Order / Service Header",
				APT_DoimainManagementObj.Domainmanagement.createorderservice_header, "Create Order / Service");

		// verify warning messages
		scrollDown(APT_DoimainManagementObj.Domainmanagement.nextbutton);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");
		click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next button");
		waitForAjax();

		verifyExists(APT_DoimainManagementObj.Domainmanagement.sidwarngmsg, "Warning Message Service Identification");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.userfieldwarngmsg, "Warning Message User");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.passwordfieldwarngmsg, "Warning Message Password");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.defaultemailwarngmsg, "Warning Message Default Email");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.firstnamefieldwarngmsg, "Warning Message First Name");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.lastnamefieldwarngmsg, "Warning Message Last Name");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.organizationnamewarngmsg,
				"Warning Message Organization Name");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.addressfieldwarngmsg, "Warning Message Address");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.postalcodewarngmsg, "Warning Message Postal Code");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.cityfieldwarngmsg, "Warning Message City");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.phonefieldwarngmsg, "Warning Message Phone");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.faxfieldwarngmsg, "Warning Message Fax");
		verifyExists(APT_DoimainManagementObj.Domainmanagement.emailfieldwarngmsg, "Warning Message Email");

		// service creation
		scrollUp();
		waitForAjax();
		// Thread.sleep(1000);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.serviceidentificationtextfield,
				"Service Identification");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.serviceidentificationtextfield, sid);

		// addtextFields_commonMethod(application, "Service Identification",
		// "serviceidentificationtextfield", sid, xml);
		compareText("Service Type", APT_DoimainManagementObj.Domainmanagement.servicetype_value, servicetype);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.remarktextarea, "Remark");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.remarktextarea, Remarks);
		// addtextFields_commonMethod(application, "Remarks", "remarktextarea",
		// Remarks, xml);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.emailtextfieldvalue, "email");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.emailtextfieldvalue, email1);
		// addtextFields_commonMethod(application, "Email",
		// "emailtextfieldvalue", email, xml);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.emailaddarrow, "email add arrow");
		click(APT_DoimainManagementObj.Domainmanagement.emailaddarrow, "email add arrow");
		// click_commonMethod(application, "Email adding Arrow",
		// "emailaddarrow", xml);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.phonecontacttextfieldvalue, "Phone Contact");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.phonecontacttextfieldvalue, phonecontact);
		// addtextFields_commonMethod(application, "Phone Contact",
		// "phonecontacttextfieldvalue", phonecontact, xml);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.phoneaddarrow, "phone contact adding Arrow");
		click(APT_DoimainManagementObj.Domainmanagement.phoneaddarrow, "phone contact adding Arrow");
		// click_commonMethod(application, "phone contact adding Arrow",
		// "phoneaddarrow", xml);

		scrollDown(APT_DoimainManagementObj.Domainmanagement.phonecontacttextfieldvalue);
		// ScrolltoElement(application, "phonecontacttextfieldvalue", xml);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.userfield, "User");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.userfield, user);

		// EnterTextValue(application, passwordvalue, "Password", "Password");
		click(APT_DoimainManagementObj.Domainmanagement.GeneratePassword, "Generate Password");
		// click_commonMethod(application, "Generate Password",
		// "GeneratePassword", xml);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.defaultemail, "default email");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.defaultemail, defaultemail);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.servicefirstname, "service first name");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.servicefirstname, servicefirstname);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.servicelastname, "service last name");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.servicelastname, servicelastname);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.organizationname, "Organization Name");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.organizationname, organizationname);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.serviceaddress, "service address");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.serviceaddress, serviceaddress);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.servicepostalcode, "service postal code");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.servicepostalcode, servicepostalcode);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.servicecity, "service city");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.servicecity, servicecity);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.servicestate, "service State");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.servicestate, servicestate);

		scrollDown(APT_DoimainManagementObj.Domainmanagement.nextbutton);

		// select country from dropdown
		boolean availability1 = false;
		try {
			// availability1=getwebelement(xml.getlocator("//locators/" +
			// application + "/service_country")).isDisplayed();
			if (isVisible(APT_DoimainManagementObj.Domainmanagement.service_country)) {
				Report.LogInfo("INFO", "Country dropdown is displaying", "PASS");

				if (servicecountry.equalsIgnoreCase("null")) {
					Report.LogInfo("INFO", " No values selected under Country dropdown", "PASS");

				} else {
					
					addDropdownValues_commonMethod("Country",
							APT_DoimainManagementObj.Domainmanagement.Country, servicecountry);
					/*webDriver.findElement(By.xpath("//div[label[text()='Country']]//div[text()='�']")).click();
					waitForAjax();
					// Clickon(getwebelement("//div[label[text()='Country']]//div[text()='�']"));
					// Thread.sleep(3000);

					// verify list of values inside dropdown
					List<WebElement> listofvalues = webDriver.findElements(By.xpath("(//div[@role='list']//div)[3]"));
					Report.LogInfo("INFO", " List of values inside Country dropdown is:  ", "PASS");

					for (WebElement valuetypes : listofvalues) {
						Report.LogInfo("INFO", "service sub types : " + valuetypes.getText(), "PASS");

					}

					waitForAjax();
					webDriver.findElement(By.xpath("(//div[label[text()='Country']]//input)[1]"))
							.sendKeys(servicecountry);
					waitForAjax();
					// SendKeys(getwebelement("(//div[label[text()='Country']]//input)[1]"),
					// servicecountry);
					// Thread.sleep(2000);

					webDriver.findElement(By.xpath("(//div[contains(text(),'" + servicecountry + "')])[1]")).click();
					waitForAjax();
					// Clickon(getwebelement("(//div[contains(text(),'"+
					// servicecountry +"')])[1]"));
					// Thread.sleep(3000);

					// String
					// actualValue=getwebelement("//label[text()='Country']/following-sibling::div//div/span").getText();
					String actualValue = webDriver
							.findElement(By.xpath("(//div[contains(text(),'" + servicecountry + "')])[1]")).getText();
					Report.LogInfo("INFO", "Country dropdown value selected as: " + actualValue, "PASS");
						*/
				}
			} else {
				Report.LogInfo("INFO", "Country is not displaying", "FAIL");
			}
		} catch (NoSuchElementException e) {
			Report.LogInfo("INFO", "Country is not displaying", "FAIL");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("INFO", " NOt able to perform selection under Country dropdown", "FAIL");

		}
		verifyExists(APT_DoimainManagementObj.Domainmanagement.servicephone, "Phone");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.servicephone, servicephone);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.servicefax, "Fax");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.servicefax, servicefax);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.serviceemail, "Email");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.serviceemail, email1);

		scrollDown(APT_DoimainManagementObj.Domainmanagement.nextbutton);
		waitForAjax();

		click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "Next Button");
		waitforPagetobeenable();
		CreateFirewall.verifysuccessmessage();
	}

	public void verifyCustomerDetailsInformation(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newCustomerCreation");
		String existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingCustomerSelection");
		String newCustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		//String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "reference");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		//String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		//String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");
		String existingCustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		//String tcn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalContactName");

		Report.LogInfo("INFO", "'Verifying Customer informations", "PASS");

		// Customer Name
		if (newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) {
			scrollDown(APT_DoimainManagementObj.Domainmanagement.Name_Value);
			scrollUp();
			compareText("Customer Name", APT_DoimainManagementObj.Domainmanagement.Name_Value, newCustomer);
			compareText("Country", APT_DoimainManagementObj.Domainmanagement.Country_Value, country);
			compareText("OCN", APT_DoimainManagementObj.Domainmanagement.OCN_Value, ocn);
			//compareText("Reference", APT_DoimainManagementObj.Domainmanagement.Reference_Value, reference);
			//compareText("Technical Contact Name", APT_DoimainManagementObj.Domainmanagement.TechnicalContactName_Value,
					//tcn);
			compareText("Type", APT_DoimainManagementObj.Domainmanagement.Type_Value, type);
			compareText("Email", APT_DoimainManagementObj.Domainmanagement.Email_Value, email);
			//compareText("Phone", APT_DoimainManagementObj.Domainmanagement.Phone_Value, phone);
			//compareText("Fax", APT_DoimainManagementObj.Domainmanagement.Fax_Value, fax);

		} else if (newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes")) {

			compareText("Customer Name", APT_DoimainManagementObj.Domainmanagement.Name_Value, existingCustomer);
		}

		// Main Domain
		if (maindomain.equalsIgnoreCase("Null")) {
			Report.LogInfo("INFO", "A default displays for main domain field, if no provided while creating customer",
					"PASS");
		} else {
			compareText("Main Domain", APT_DoimainManagementObj.Domainmanagement.MainDomain_Value, maindomain);
		}

		Report.LogInfo("INFO", "=== Customer Details panel fields Verified ===", "PASS");
	}

	public void verifyorderpanel_editorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editOrderSelection");
		String editorderno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditOrder_OrderNumber");
		String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditOrder_VoicelineNumber");

		Report.LogInfo("INFO", "Verifying 'Edit Order' Functionality", "PASS");

		scrollDown(APT_DoimainManagementObj.Domainmanagement.userspanel_header);

		if (editOrderSelection.equalsIgnoreCase("No")) {
			Report.LogInfo("INFO", "Edit Order is not performed", "PASS");

		} else if (editOrderSelection.equalsIgnoreCase("Yes")) {
			Report.LogInfo("INFO", "Performing Edit Order Functionality", "PASS");

			// Cancel Edit order in Order panel
			click(APT_DoimainManagementObj.Domainmanagement.orderactionbutton, "Action dropdown");
			click(APT_DoimainManagementObj.Domainmanagement.editorderlink, "Edit Order");
			waitForAjax();
			// compareText(application, "Edit Order", "editorderheader", "Edit
			// Order", xml);
			// Thread.sleep(1000);

			// WebElement EditOrderNo=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/editorderno"));
			click(APT_DoimainManagementObj.Domainmanagement.editorderno, "Order Number");
			waitForAjax();
			// click_commonMethod(application, "Order Number", "editorderno",
			// xml);
			// Thread.sleep(1000);

			edittextFields_commonMethod("Order Number", APT_DoimainManagementObj.Domainmanagement.editorderno,
					editorderno);

			// WebElement EditVoiceLineNo=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/editvoicelineno"));
			click(APT_DoimainManagementObj.Domainmanagement.editvoicelineno, "RFI Voice Line Number");
			waitForAjax();
			// click_commonMethod(application, "RFI Voice Line Number",
			// "editvoicelineno", xml);
			// Thread.sleep(1000);

			edittextFields_commonMethod("RFI Voiceline Number",
					APT_DoimainManagementObj.Domainmanagement.editvoicelineno, editvoicelineno);
			click(APT_DoimainManagementObj.Domainmanagement.cancelbutton, "cancel button");
			waitforPagetobeenable();

			// Edit Order
			scrollDown(APT_DoimainManagementObj.Domainmanagement.userspanel_header);
			// Thread.sleep(1000);

			verifyExists(APT_DoimainManagementObj.Domainmanagement.orderactionbutton, "order action button");
			click(APT_DoimainManagementObj.Domainmanagement.orderactionbutton, "order action button");

			verifyExists(APT_DoimainManagementObj.Domainmanagement.editorderlink, "Edit Order");
			click(APT_DoimainManagementObj.Domainmanagement.editorderlink, "Edit Order");

			waitforPagetobeenable();
			compareText("Edit Order Header", APT_DoimainManagementObj.Domainmanagement.editorderheader, "Edit Order");
			verifyExists(APT_DoimainManagementObj.Domainmanagement.editorderno, "Order Number");
			click(APT_DoimainManagementObj.Domainmanagement.editorderno, "Order Number");
			waitForAjax();

			edittextFields_commonMethod("Order Number", APT_DoimainManagementObj.Domainmanagement.editorderno,
					editorderno);
			verifyExists(APT_DoimainManagementObj.Domainmanagement.editvoicelineno, "RFI Voice Line Number");
			click(APT_DoimainManagementObj.Domainmanagement.editvoicelineno, "RFI Voice Line Number");
			waitForAjax();

			edittextFields_commonMethod("RFI Voice Line Number",
					APT_DoimainManagementObj.Domainmanagement.editvoicelineno, editvoicelineno);
			verifyExists(APT_DoimainManagementObj.Domainmanagement.editorder_okbutton, "edit order ok button");
			click(APT_DoimainManagementObj.Domainmanagement.editorder_okbutton, "edit order ok button");

			waitforPagetobeenable();
			scrollDown(APT_DoimainManagementObj.Domainmanagement.userspanel_header);

			if (editorderno.equalsIgnoreCase("Null")) {
				Report.LogInfo("INFO", "'Order/Contract Number (Parent SID)' field is not edited", "PASS");
			} else {
				verifyExists(APT_DoimainManagementObj.Domainmanagement.ordernumbervalue,"ordernumbervalue");
				//compareText("Order Number", APT_DoimainManagementObj.Domainmanagement.ordernumbervalue, editorderno);
			}

			if (editvoicelineno.equalsIgnoreCase("Null")) {
				Report.LogInfo("INFO", "'RFI/RFQ/IP Voice Line Number' field is not edited", "PASS");

			} else {
				verifyExists(APT_DoimainManagementObj.Domainmanagement.ordervoicelinenumbervalue,"ordervoicelinenumbervalue");
				//compareText("RFI Voice Line Number",
						//APT_DoimainManagementObj.Domainmanagement.ordervoicelinenumbervalue, editvoicelineno);
			}
			Report.LogInfo("INFO", "------ Edit Order is successful ------", "PASS");
		}

	}

	public void verifyorderpanel_changeorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"changeOrderSelection_newOrder");
		String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "changeOrderSelection_existingOrder");
		String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ChangeOrder_OrderNumber");
		String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ChangeOrder_VoicelineNumber");
		String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ChangeOrder_existingOrderNumber");

		scrollDown(APT_DoimainManagementObj.Domainmanagement.userspanel_header);
		// ScrolltoElement(application, "userspanel_header", xml);
		Report.LogInfo("INFO", "Verifying 'Change Order' Functionality", "PASS");

		if ((changeOrderSelection_newOrder.equalsIgnoreCase("No"))
				&& (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
			Report.LogInfo("INFO", "Change Order is not performed", "PASS");

		} else if (changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {

			// Change Order
			verifyExists(APT_DoimainManagementObj.Domainmanagement.orderactionbutton, "Action dropdown");
			click(APT_DoimainManagementObj.Domainmanagement.orderactionbutton, "Action dropdown");

			verifyExists(APT_DoimainManagementObj.Domainmanagement.changeorderlink, "Change Order");
			click(APT_DoimainManagementObj.Domainmanagement.changeorderlink, "Change Order");
			waitforPagetobeenable();

			compareText("Change Order header", APT_DoimainManagementObj.Domainmanagement.changeorderheader,
					"Change Order");
			// Thread.sleep(1000);
			verifyExists(APT_DoimainManagementObj.Domainmanagement.changeorder_selectorderswitch,
					"Select order switch");
			click(APT_DoimainManagementObj.Domainmanagement.changeorder_selectorderswitch, "Select order switch");

			verifyExists(APT_DoimainManagementObj.Domainmanagement.changeordernumber, "Order Number");
			click(APT_DoimainManagementObj.Domainmanagement.changeordernumber, "Order Number");
			waitForAjax();

			verifyExists(APT_DoimainManagementObj.Domainmanagement.changeordernumber, "Order Number");
			sendKeys(APT_DoimainManagementObj.Domainmanagement.changeordernumber, ChangeOrder_newOrderNumber);

			verifyExists(APT_DoimainManagementObj.Domainmanagement.changeordervoicelinenumber, "RFI Voice Line Number");
			click(APT_DoimainManagementObj.Domainmanagement.changeordervoicelinenumber, "RFI Voice Line Number");
			waitForAjax();

			sendKeys(APT_DoimainManagementObj.Domainmanagement.changeordervoicelinenumber, changevoicelineno);

			verifyExists(APT_DoimainManagementObj.Domainmanagement.createorder_button, "Create Order");
			click(APT_DoimainManagementObj.Domainmanagement.createorder_button, "Create Order");

			waitforPagetobeenable();
			scrollDown(APT_DoimainManagementObj.Domainmanagement.userspanel_header);
			// ScrolltoElement(application, "userspanel_header", xml);
			Thread.sleep(1000);

			compareText("Order Number", APT_DoimainManagementObj.Domainmanagement.ordernumbervalue,
					ChangeOrder_newOrderNumber);
			compareText("RFI Voice Line Number", APT_DoimainManagementObj.Domainmanagement.ordervoicelinenumbervalue,
					changevoicelineno);
			Report.LogInfo("INFO", "------ Change Order is successful ------", "PASS");
		} else if (changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) {
			Report.LogInfo("INFO", "Performing Change Order functionality", "PASS");
			// ExtentTestManager.getTest().log(LogStatus.PASS, "Performing
			// Change Order functionality");

			scrollDown(APT_DoimainManagementObj.Domainmanagement.userspanel_header);
			// ScrolltoElement(application, "userspanel_header", xml);
			// Thread.sleep(1000);
			verifyExists(APT_DoimainManagementObj.Domainmanagement.orderactionbutton, "Action dropdown");
			click(APT_DoimainManagementObj.Domainmanagement.orderactionbutton, "Action dropdown");

			verifyExists(APT_DoimainManagementObj.Domainmanagement.changeorderlink, "Change Order");
			click(APT_DoimainManagementObj.Domainmanagement.changeorderlink, "Change Order");

			waitforPagetobeenable();

			compareText("Change Order header", APT_DoimainManagementObj.Domainmanagement.changeorderheader,
					"Change Order");
			// Thread.sleep(1000);

			addDropdownValues_commonMethod("Order/Contract Number (Parent SID)",
					APT_DoimainManagementObj.Domainmanagement.changeorder_chooseorderdropdown,
					ChangeOrder_existingOrderNumber);

			verifyExists(APT_DoimainManagementObj.Domainmanagement.changeorder_okbutton, "OK");
			click(APT_DoimainManagementObj.Domainmanagement.changeorder_okbutton, "OK");

			waitforPagetobeenable();
			scrollDown(APT_DoimainManagementObj.Domainmanagement.userspanel_header);

			compareText("Order Number", APT_DoimainManagementObj.Domainmanagement.ordernumbervalue,
					ChangeOrder_existingOrderNumber);
			compareText("RFI Voice Line Number", APT_DoimainManagementObj.Domainmanagement.ordervoicelinenumbervalue,
					changevoicelineno);
			Report.LogInfo("INFO", "------ Change Order is successful ------", "PASS");

		}
	}

	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String phonecontact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServicePhoneContact");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");
		String user = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceUser");
		String defaultemail = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceDefaultEmail");
		String servicefirstname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceFirstName");
		String servicelastname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceLastName");
		String organizationname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"OrganizationName");
		String serviceaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceAddress");
		String servicecomplement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceComplement");
		String servicepostalcode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServicePostalCode");
		String servicecity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceCity");
		String servicestate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceState");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceCountry");
		String servicephone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServicePhone");
		String servicefax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceFax");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceEmail");

		scrollDown(APT_DoimainManagementObj.Domainmanagement.orderpanelheader);

		compareText("Service panel header", APT_DoimainManagementObj.Domainmanagement.servicepanel_header, "Service");
		scrollDown(APT_DoimainManagementObj.Domainmanagement.servicecityvalue);

		compareText("Service Identification", APT_DoimainManagementObj.Domainmanagement.sidvalue, sid);
		compareText("Service Type", APT_DoimainManagementObj.Domainmanagement.servicepanel_servicetypevalue,
				servicetype);
		// GetText(application, "Email", "emailvalue");
		//compareText("Phone Contact", APT_DoimainManagementObj.Domainmanagement.phonecontactvalue, phonecontact);
		compareText("Remarks", APT_DoimainManagementObj.Domainmanagement.remarksvalue, Remarks);
		compareText("User", APT_DoimainManagementObj.Domainmanagement.userfieldvalue, user);
		compareText("Default Email", APT_DoimainManagementObj.Domainmanagement.defaultemailvalue, defaultemail);
		compareText("First Name", APT_DoimainManagementObj.Domainmanagement.servicefirstnamevalue, servicefirstname);
		compareText("Last Name", APT_DoimainManagementObj.Domainmanagement.servicelastnamevalue, servicelastname);
		compareText("Organisation Name", APT_DoimainManagementObj.Domainmanagement.organizationnamevalue,
				organizationname);
		compareText("Address", APT_DoimainManagementObj.Domainmanagement.serviceaddressvalue, serviceaddress);
		compareText("Complement", APT_DoimainManagementObj.Domainmanagement.servicecomplementvalue, servicecomplement);
		compareText("Postal Code", APT_DoimainManagementObj.Domainmanagement.servicepostalcodevalue, servicepostalcode);
		compareText("City", APT_DoimainManagementObj.Domainmanagement.servicecityvalue, servicecity);
		compareText("State", APT_DoimainManagementObj.Domainmanagement.servicestatevalue, servicestate);
		compareText("Country", APT_DoimainManagementObj.Domainmanagement.servicecountryvalue, country);
		compareText("Phone", APT_DoimainManagementObj.Domainmanagement.servicephonevalue, servicephone);
		compareText("Fax", APT_DoimainManagementObj.Domainmanagement.servicefaxvalue, servicefax);
		compareText("Email", APT_DoimainManagementObj.Domainmanagement.serviceemailvalue, email);

	}

	public void verifyEditService(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String EditRemarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_Remarks");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String editemail = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_ServiceEmail");
		String editphonecontact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServicePhoneContact");
		String edituser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_ServiceUser");
		String editdefaultemail = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServiceDefaultEmail");
		String editservicefirstname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServiceFirstName");
		String editservicelastname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServiceLastName");
		String editorganizationname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_OrganizationName");
		String editserviceaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServiceAddress");
		String editservicecomplement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServiceComplement");
		String editservicepostalcode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServicePostalCode");
		String editservicecity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServiceCity");
		String editservicestate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServiceState");
		String editcountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_ServiceCountry");
		String editservicephone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ServicePhone");
		String editservicefax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_ServiceFax");

		// Cancel edit service
		scrollDown(APT_DoimainManagementObj.Domainmanagement.orderpanelheader);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "Action dropdown");
		click(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.edit, "Edit");
		click(APT_DoimainManagementObj.Domainmanagement.edit, "Edit");

		waitforPagetobeenable();
		scrollDown(APT_DoimainManagementObj.Domainmanagement.editservice);
		String ServiceIDValue = getAttributeFrom(
				APT_DoimainManagementObj.Domainmanagement.serviceidentificationtextfield, "value");
		Report.LogInfo("INFO", "Step : Service Identification value is displayed as: " + ServiceIDValue, "PASS");

		// ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service
		// Identification value is displayed as: " +ServiceIDValue);
		edittextFields_commonMethod("Remarks", APT_DoimainManagementObj.Domainmanagement.remarktextarea, EditRemarks);
		scrollDown(APT_DoimainManagementObj.Domainmanagement.cancelbutton);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.cancelbutton, "Cancel");
		click(APT_DoimainManagementObj.Domainmanagement.cancelbutton, "Cancel");

		waitforPagetobeenable();

		if (isVisible(APT_DoimainManagementObj.Domainmanagement.servicepanel_header)) {
			scrollDown(APT_DoimainManagementObj.Domainmanagement.orderpanelheader);
			compareText("Service Identification", APT_DoimainManagementObj.Domainmanagement.sidvalue, sid);
			compareText("Remarks", APT_DoimainManagementObj.Domainmanagement.remarksvalue, Remarks);
		} else {
			Report.LogInfo("INFO", "Step : Didn't navigate to view service page", "FAIL");
		}

		// Edit service
		scrollDown(APT_DoimainManagementObj.Domainmanagement.orderpanelheader);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "service action dropdown");
		click(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "service action dropdown");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.edit, "Edit");
		click(APT_DoimainManagementObj.Domainmanagement.edit, "Edit");

		waitforPagetobeenable();
		scrollDown(APT_DoimainManagementObj.Domainmanagement.editservice);
		String ServiceIDValue1 = getAttributeFrom(
				APT_DoimainManagementObj.Domainmanagement.serviceidentificationtextfield, "value");
		Report.LogInfo("INFO", "Step : Service Identification value is displayed as: " + ServiceIDValue1, "PASS");

		compareText("Service Type", APT_DoimainManagementObj.Domainmanagement.servicetype_value, servicetype);
		edittextFields_commonMethod("Remarks", APT_DoimainManagementObj.Domainmanagement.remarktextarea, EditRemarks);

		// Edit email
		verifyExists(APT_DoimainManagementObj.Domainmanagement.selectedemail, "selected email");
		click(APT_DoimainManagementObj.Domainmanagement.selectedemail, "selected email");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.emailremovearrow, "email remove arrow");
		click(APT_DoimainManagementObj.Domainmanagement.emailremovearrow, "email remove arrow");

		edittextFields_commonMethod("Email", APT_DoimainManagementObj.Domainmanagement.emailtextfieldvalue, editemail);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.emailaddarrow, "Email adding arrow");
		click(APT_DoimainManagementObj.Domainmanagement.emailaddarrow, "Email adding arrow");

		// Edit phone contact
		verifyExists(APT_DoimainManagementObj.Domainmanagement.selectedphone, "Selected phone contact");
		click(APT_DoimainManagementObj.Domainmanagement.selectedphone, "Selected phone contact");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.phoneremovearrow, "Phonecontact remove arrow");
		click(APT_DoimainManagementObj.Domainmanagement.phoneremovearrow, "Phonecontact remove arrow");

		edittextFields_commonMethod("Phone Contact",
				APT_DoimainManagementObj.Domainmanagement.phonecontacttextfieldvalue, editphonecontact);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.phoneaddarrow, "Phonecontact adding arrow");
		click(APT_DoimainManagementObj.Domainmanagement.phoneaddarrow, "Phonecontact adding arrow");

		scrollDown(APT_DoimainManagementObj.Domainmanagement.phonecontacttextfieldvalue);
		edittextFields_commonMethod("User", APT_DoimainManagementObj.Domainmanagement.userfield, edituser);
		clearTextBox(APT_DoimainManagementObj.Domainmanagement.Password);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.GeneratePassword, "Generate Password");
		click(APT_DoimainManagementObj.Domainmanagement.GeneratePassword, "Generate Password");

		edittextFields_commonMethod("Default Email", APT_DoimainManagementObj.Domainmanagement.defaultemail,
				editdefaultemail);
		edittextFields_commonMethod("First Name", APT_DoimainManagementObj.Domainmanagement.servicefirstname,
				editservicefirstname);
		edittextFields_commonMethod("Last Name", APT_DoimainManagementObj.Domainmanagement.servicelastname,
				editservicelastname);
		edittextFields_commonMethod("Organization Name", APT_DoimainManagementObj.Domainmanagement.organizationname,
				editorganizationname);
		edittextFields_commonMethod("Address", APT_DoimainManagementObj.Domainmanagement.serviceaddress,
				editserviceaddress);
		edittextFields_commonMethod("Complement", APT_DoimainManagementObj.Domainmanagement.servicecomplement,
				editservicecomplement);
		edittextFields_commonMethod("Postal Code", APT_DoimainManagementObj.Domainmanagement.servicepostalcode,
				editservicepostalcode);
		edittextFields_commonMethod("City", APT_DoimainManagementObj.Domainmanagement.servicecity, editservicecity);
		edittextFields_commonMethod("State", APT_DoimainManagementObj.Domainmanagement.servicestate, editservicestate);
		edittextFields_commonMethod("Country", APT_DoimainManagementObj.Domainmanagement.country, editcountry);
		edittextFields_commonMethod("Phone", APT_DoimainManagementObj.Domainmanagement.servicephone, editservicephone);
		edittextFields_commonMethod("Fax", APT_DoimainManagementObj.Domainmanagement.servicefax, editservicefax);
		edittextFields_commonMethod("Email", APT_DoimainManagementObj.Domainmanagement.serviceemail, editemail);
		// Thread.sleep(2000);
		waitForAjax();
		scrollDown(APT_DoimainManagementObj.Domainmanagement.editservice_next);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.editservice_next, "edit service next");
		click(APT_DoimainManagementObj.Domainmanagement.editservice_next, "edit service next");

		waitforPagetobeenable();

		if (isVisible(APT_DoimainManagementObj.Domainmanagement.customerdetailsheader)) {
			Report.LogInfo("INFO", "Navigated to view service page", "PASS");
			CreateFirewall.verifysuccessmessage();
		} else {
			Report.LogInfo("INFO", "Service not updated", "PASS");

		}

	}

	public void verifySynchronize() throws InterruptedException, IOException {

		scrollDown(APT_DoimainManagementObj.Domainmanagement.orderpanelheader);
		// Thread.sleep(1000);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "Action dropdown");
		click(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.synchronizelink_servicepanel, "Synchronize");
		click(APT_DoimainManagementObj.Domainmanagement.synchronizelink_servicepanel, "Synchronize");
		waitForAjax();

		waitforPagetobeenable();
		CreateFirewall.verifysuccessmessage();
		// verifysuccessmessage(application, "Sync started successfully. Please
		// check the sync status of this service.");
	}

	public void verifyDeleteService() throws InterruptedException, IOException {

		scrollDown(APT_DoimainManagementObj.Domainmanagement.orderpanelheader);
		verifyExists(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "Action dropdown");
		click(APT_DoimainManagementObj.Domainmanagement.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.delete, "Delete");
		click(APT_DoimainManagementObj.Domainmanagement.delete, "Delete");
		waitForAjax();
		if (isVisible(APT_DoimainManagementObj.Domainmanagement.delete_alertpopup)) {
			verifyExists(APT_DoimainManagementObj.Domainmanagement.deletebutton, "delete button");
			click(APT_DoimainManagementObj.Domainmanagement.deletebutton, "delete button");

			waitforPagetobeenable();
			CreateFirewall.verifysuccessmessage();
			// verifysuccessmessage(application, "Service deleted
			// successfully");
		} else {
			Report.LogInfo("INFO", "Step : Delete alert popup is not displayed", "FAIL");
		}

	}

	public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws Exception {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.ManageCustomerServiceLink,
				"Manage Customer Service Link");
		click(APT_DoimainManagementObj.Domainmanagement.ManageCustomerServiceLink, "Manage Customer Service Link");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.createcustomerlink, "create customer link");
		click(APT_DoimainManagementObj.Domainmanagement.createcustomerlink, "create customer link");

		waitforPagetobeenable();
		compareText("create customer page header", APT_DoimainManagementObj.Domainmanagement.createcustomer_header,
				"Create Customer");
		// scrolltoend();
		// click_commonMethod(application, "Ok", "okbutton", xml);

		// Warning msg check
		/*
		 * warningMessage_commonMethod(application, "customernamewarngmsg",
		 * "Legal Customer Name", xml); warningMessage_commonMethod(application,
		 * "countrywarngmsg", "Country", xml);
		 * warningMessage_commonMethod(application, "ocnwarngmsg", "OCN", xml);
		 * warningMessage_commonMethod(application, "typewarngmsg", "Type",
		 * xml); warningMessage_commonMethod(application, "emailwarngmsg",
		 * "Email", xml);
		 */
		// Clear customer info
		/*
		 * addtextFields_commonMethod(application, "Customer Name",
		 * "nametextfield", name, xml); addtextFields_commonMethod(application,
		 * "Main Domain", "maindomaintextfield", maindomain, xml);
		 * Thread.sleep(1000); ScrolltoElement(application, "resetbutton", xml);
		 * click_commonMethod(application, "Reset", "resetbutton", xml);
		 * ExtentTestManager.getTest().log(LogStatus.PASS,
		 * "All text field values are cleared");
		 * Log.info("All text field values are cleared");
		 */

		// Name
		verifyExists(APT_DoimainManagementObj.Domainmanagement.nametextfield, "Legal Customer Name");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.nametextfield, name);

		// Main Domain
		verifyExists(APT_DoimainManagementObj.Domainmanagement.maindomaintextfield, "Main Domain");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.maindomaintextfield, maindomain);

		// Country dropdown
		addDropdownValues_commonMethod("Country", APT_DoimainManagementObj.Domainmanagement.country, country);

		// OCN
		verifyExists(APT_DoimainManagementObj.Domainmanagement.ocntextfield, "OCN");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.ocntextfield, ocn);

		// Reference
		// verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Referance_Build4,
		// "reference");
		// sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Referance_Build4,
		// Reference);
		// addtextFields_commonMethod(application, "reference",
		// "Referance_Build4", Reference, xml);

		// technical Contact name
		// verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.TechinicalContactName_Build4,
		// "Technical Contact Name");
		// sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.TechinicalContactName_Build4,
		// TechnicalContactName);
		// addtextFields_commonMethod(application, "Technical Contact Name",
		// "TechinicalContactName_Build4", TechnicalContactName, xml);

		// Type dropdown
		addDropdownValues_commonMethod("Type", APT_DoimainManagementObj.Domainmanagement.typedropdown, type);

		// Email
		verifyExists(APT_DoimainManagementObj.Domainmanagement.emailtextfield, "Email");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.emailtextfield, email);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.okbutton, "Ok Button");
		click(APT_DoimainManagementObj.Domainmanagement.okbutton, "Ok Button");
		waitforPagetobeenable();

		// Create customer by providing all info
		/*
		 * addtextFields_commonMethod(application, "Customer Name",
		 * "nametextfield", name, xml); addtextFields_commonMethod(application,
		 * "Main Domain", "maindomaintextfield", maindomain, xml);
		 * addDropdownValues_commonMethod(application, "Country", "country",
		 * country, xml); addtextFields_commonMethod(application, "OCN",
		 * "ocntextfield", ocn, xml); addtextFields_commonMethod(application,
		 * "Reference", "referencetextfield", reference, xml);
		 * addtextFields_commonMethod(application, "Technical Contact Name",
		 * "technicalcontactnametextfield", tcn, xml);
		 * addDropdownValues_commonMethod(application, "Type", "typedropdown",
		 * type, xml); addtextFields_commonMethod(application, "Email",
		 * "emailtextfield", email, xml);
		 * addtextFields_commonMethod(application, "Phone", "phonetextfield",
		 * phone, xml); addtextFields_commonMethod(application, "Fax",
		 * "faxtextfield", fax, xml); ScrolltoElement(application, "okbutton",
		 * xml); Thread.sleep(1000); click_commonMethod(application, "Ok",
		 * "okbutton", xml); waitforPagetobeenable();
		 * verifysuccessmessage(application, "Customer successfully created.");
		 */
	}

}
