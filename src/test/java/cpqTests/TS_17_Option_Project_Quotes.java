package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_17_Option_Project_Quotes extends SeleniumUtils{
	
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testOptionProjectQuotes(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");

			
			String sResult;
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
				
				AddProduct.AddProductToQuote(ProductName);
//				calling the below method to enter the site address
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below method to configure the product
				sResult = AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);	
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
			} else {
//				Calling the below method to add the product in CPQ
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
			}
			
						
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				            
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			AddProduct.verifyQuoteStage("Priced");
			
//	      Calling the below method to Capture Hub Id
			if (ProductName.equalsIgnoreCase("EthernetHub")) {
				String HubId = AddProduct.addHubrefernce(ProductName);
				if(HubId.equals("True"))DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Hub_Reference", HubId);
			}
			
			AddProduct.copyQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.optionQuoteEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.optionQuoteLineItemEntries(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.discountingProcess(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.SubmitForApproval();
		
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {				
//				calling the below method to verify quote stage and status
				AddProduct.verifyQuoteStage("Pending Governance Approval");
				}
			
			Approvals.SwitchCPQUser("VPSales2_User", Quote_ID);
			
			AddProduct.quoteDiscountGovernanceApprove("Approve");
			
			Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			AddProduct.verifyQuoteStage("Approved");			
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String Primary_Option = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Primary_Option");
			
//			calling the below method to select the quote for further processing among options
			if(!Primary_Option.contains("ProjectQuote")) {
			AddProduct.optionQuoteTechnicalApproval(testDataFile, dataSheet, scriptNo, dataSetNo);
			}else {
//				calling the below method to procees fastsign process
				AddProduct.projectQuoteProcess(testDataFile, dataSheet, scriptNo, dataSetNo);				
			}
			
			//Approvals.SwitchCPQUser("Sales_User", Quote_ID+"1");					
			
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.logoutCPQ("Main");
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	


}
