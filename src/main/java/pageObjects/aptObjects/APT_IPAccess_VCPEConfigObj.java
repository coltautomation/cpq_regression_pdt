package pageObjects.aptObjects;

public class APT_IPAccess_VCPEConfigObj {
	public static class ipaVCPE {

		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";
		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";
		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";

		public static final String mcslink = "@xpath=//a[contains(text(),'Manage Customer')]";
		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";
		public static final String createorderlink = "@xpath=//li[text()='Create Order/Service']";

		// !-- Create Customer Page -- = "@xpath=
		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String nametextfield = "@xpath=//input[@id='name']";
		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";
		public static final String country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String ocntextfield = "@xpath=//input[@id='ocn']";
		public static final String referencetextfield = "@xpath=//input[@id='reference']";
		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";
		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";
		public static final String emailtextfield = "@xpath=//input[@id='email']";
		public static final String phonetextfield = "@xpath=//input[@id='phone']";
		public static final String faxtextfield = "@xpath=//input[@id='fax']";
		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";
		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String resetbutton = "@xpath=//span[text()='Reset']";
		public static final String clearbutton = "@xpath=//span[text()='Clear']";
		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";
		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// !-- Verify warning messages -- = "@xpath=
		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";
		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";
		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";
		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";
		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";
		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";
		public static final String sidwarngmsg = "@xpath=//label[text()='Service Identification']/parent::div/div";
		public static final String billingtype_warngmsg = "@xpath=//label[text()='Billing Type']/parent::div//div";
		public static final String L2Technology_warngmsg = "@xpath=//label[text()='L2 Technology']/parent::div//div";

		// !-- Verify Customer details panel -- = "@xpath=
		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";
		public static final String Name_Text = "@xpath=(//div//label[@for='name'])[1]";
		public static final String Name_Value = "@xpath=//label[text()='Legal Customer Name']/parent::div/parent::div//div[2]";
		public static final String MainDomain_Text = "@xpath=(//div//label[@for='name'])[2]";
		public static final String MainDomain_Value = "@xpath=//label[text()='Main Domain']/parent::div/parent::div//div[2]";
		public static final String Country_Text = "@xpath=(//div//label[@for='name'])[3]";
		public static final String Country_Value = "@xpath=//label[text()='Country']/parent::div/parent::div//div[2]";
		public static final String OCN_Text = "@xpath=(//div//label[@for='name'])[4]";
		public static final String OCN_Value = "@xpath=//label[text()='OCN']/parent::div/parent::div//div[2]";
		public static final String Reference_Text = "@xpath=(//div//label[@for='name'])[5]";
		public static final String Reference_Value = "@xpath=//label[text()='Reference']/parent::div/parent::div//div[2]";
		public static final String Type_Text = "@xpath=(//div//label[@for='name'])[6]";
		public static final String Type_Value = "@xpath=//label[text()='Type']/parent::div/parent::div//div[2]";
		public static final String TechnicalContactName_Text = "@xpath=(//div//label[@for='name'])[7]";
		public static final String TechnicalContactName_Value = "@xpath=//label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";
		public static final String Email_Text = "@xpath=(//div//label[@for='name'])[8]";
		public static final String Email_Value = "@xpath=//label[text()='Email']/parent::div/parent::div//div[2]";
		public static final String Phone_Text = "@xpath=(//div//label[@for='name'])[9]";
		public static final String Phone_Value = "@xpath=//label[text()='Phone']/parent::div/parent::div//div[2]";
		public static final String Fax_Text = "@xpath=(//div//label[@for='name'])[10]";
		public static final String Fax_Value = "@xpath=//label[text()='Fax']/parent::div/parent::div//div[2]";

		// !-- create order/service page -- = "@xpath=
		// !-- public static final String nametextfield =
		// "@xpath=//input[@id='customerSearch']";

		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";
		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";
		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";
		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";
		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";
		// public static final String country =
		// "@xpath=//div[div[label[text()='Country']]]/div[2]";
		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";
		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";
		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";
		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";
		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";
		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";
		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";
		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";
		public static final String useractionbutton = "@xpath=//button[@id='dropdown-basic-button']";
		public static final String adduserbutton = "@xpath=//a[contains(text(),'Add')]";
		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";

		// !-- Create Order/service panel in view customer page -- = "@xpath=
		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";
		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";
		public static final String selectorderswitch = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/following-sibling::input/ancestor::div[@class='form-group']/parent::div/following-sibling::div//div[@class='react-switch-bg']";
		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";
		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";
		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";
		public static final String newordertextfield = "@xpath=//input[@id='orderName']";
		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";
		public static final String servicetype_warngmsg = "@xpath=//div[label[contains(text(),'Service Type')]]/following-sibling::span";
		public static final String servicetypetextfield = "@xpath=//div[div[label[text()='Service Type']]]//input";
		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";
		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String changeorder_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// !-- Service Creation Form -- = "@xpath=
		public static final String createorderservice_header = "@xpath=//div//div[text()='Create Order / Service']";
		public static final String serviceidentificationtextfield = "@xpath=//input[@id='serviceName']";
		public static final String servicetypevalue = "@xpath=//div[label[text()='Service Type']]/div";
		public static final String networkconfig_value = "@xpath=//div[label[text()='Network Configuration']]/div";
		public static final String cpewantechnology_value = "@xpath=//div[label[text()='CPE WAN Technology  ']]/div";
		public static final String pewantechnologytype_value = "@xpath=//div[label[text()='PE WAN Technology Type  ']]/div";
		public static final String noofcircuits_value = "@xpath=//div[label[text()='No. of circuits']]/div";
		// public static final String emailtextfield =
		// "@xpath=//input[@id='email']";
		public static final String emailtextfieldvalue = "@xpath=//div[label[text()='Email']]//input";
		public static final String phonecontacttextfield = "@xpath=//input[@id='phoneContact']";
		public static final String phonecontacttextfieldvalue = "@xpath=//div[label[text()='Phone Contact']]//input";
		public static final String remarktextarea = "@xpath=//textarea[contains(@name,'remark')]";
		public static final String remarktextareavalue = "@xpath=//div[label[text()='Remark']]//textarea";
		public static final String performancereporting_checkbox = "@xpath=//div[label[text()='Performance Reporting']]//input";
		public static final String ipguardian_checkbox = "@xpath=//div[label[text()='IP Guardian']]//input";
		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String servicecreationmessage = "@xpath=//div[@role='alert']//span";
		public static final String terminationdate_field = "@xpath=//label[text()='Termination Date']/parent::div//input";
		public static final String billingtype_dropdown = "@xpath=//label[text()='Billing Type']/parent::div//select";
		public static final String serviceBW_dropdown = "@xpath=//label[text()='Service BW']/parent::div//select";
		public static final String servicetype_value = "@xpath=//label[text()='Service Type']/following-sibling::div";
		public static final String networkconfig_dropdown = "@xpath=//label[text()='Network Configuration']/parent::div//input";
		public static final String package_dropdown = "@xpath=//label[text()='Package']/parent::div//select";
		public static final String managedservice_checkbox = "@xpath=//div[label[text()='Managed Service']]//input";
		public static final String snmpnotification_checkbox = "@xpath=//div[label[text()='SNMP Notification']]//input";
		public static final String routerconfigview_ipv4_checkbox = "@xpath=//div[label[text()='Router Configuration View IPv4']]//input";
		public static final String routerconfigview_ipv6_checkbox = "@xpath=//div[label[text()='Router Configuration View IPv6']]//input";
		public static final String syslogeventview_checkbox = "@xpath=//div[label[text()='Syslog Event View']]//input";
		public static final String deliverychannel_dropdown = "@xpath=//label[text()='Delivery Channel']/parent::div//select";
		public static final String configurationoptions_panelheader = "@xpath=//div[text()='Configuration Options']";
		public static final String routerbasedfirewall_checkbox = "@xpath=//div[label[text()='Router Based Firewall']]//input";
		public static final String withlogicaltunnel_checkbox = "@xpath=//div[label[text()='With Logical Tunnel']]//input";
		public static final String qos_checkbox = "@xpath=//div[label[text()='Qos']]//input";
		public static final String modularmsp_checkbox = "@xpath=//div[label[text()='Modular MSP']]//input";
		public static final String L2technology_dropdown = "@xpath=//label[text()='L2 Technology']/parent::div//select";
		public static final String routingprotocol_dropdown = "@xpath=//label[text()='Routing Protocol']/parent::div//select";
		public static final String BGPasnumber_textfield = "@xpath=//div[label[text()='BGP AS Number']]//input";
		public static final String customerdescription_textfield = "@xpath=//div[label[text()='Customer Description']]//input";

		// !-- public static final String cancelbutton =
		// "@xpath=//button[@type='submit']//span[text()='Cancel']";

		// !-- Search order -- = "@xpath=
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		// !-- order panel - view service page -- = "@xpath=
		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";
		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String orderpanelheader = "@xpath=//div[text()='Order']";
		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";
		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";
		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";
		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";
		public static final String changeorder_dropdownvalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false'][1]";

		// !-- service panel -- = "@xpath=
		public static final String servicepanel_serviceidentificationvalue = "@xpath=//label[contains(text(),'Service Identification')]/following-sibling::div";
		public static final String servicepanel_servicetypevalue = "@xpath=//label[contains(text(),'Service Type')]/following-sibling::div";
		public static final String servicepanel_networkconfigvalue = "@xpath=//label[contains(text(),'Network Configuration')]/following-sibling::div";
		public static final String servicepanel_remarksvalue = "@xpath=//label[contains(text(),'Remark')]/following-sibling::div";
		public static final String servicepanel_terminationdate = "@xpath=//label[contains(text(),'Termination Date')]/following-sibling::div";
		public static final String servicepanel_email = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Email')]/following-sibling::div";
		public static final String servicepanel_phone = "@xpath=//div[text()='Service']/parent::div/following-sibling::div//label[contains(text(),'Phone Contact')]/following-sibling::div";
		public static final String servicepanel_billingtype = "@xpath=//label[contains(text(),'Billing Type')]/following-sibling::div";
		public static final String servicepanel_cpewantechnology = "@xpath=//label[contains(text(),'CPE WAN Technology')]/following-sibling::div";
		public static final String servicepanel_pewantechnology = "@xpath=//label[contains(text(),'PE WAN Technology Type')]/following-sibling::div";
		public static final String servicepanel_Noofcircuits = "@xpath=//label[contains(text(),'No. of circuits')]/following-sibling::div";
		public static final String servicepanel_serviceBW = "@xpath=//label[contains(text(),'Service BW')]/following-sibling::div";

		public static final String servicepanel_header = "@xpath=//div[text()='Service']";
		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";
		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String synchronizelink_servicepanel = "@xpath=//a[text()='Synchronize']";
		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";

		public static final String status_serviceheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service']";
		// public static final String status_servicetypeheader =
		// "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin
		// row'])[1]//label[text()='Service Type']";
		public static final String status_detailsheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Details']";
		public static final String status_statusheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Status']";
		public static final String status_modificationheader = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Last Modification']";
		public static final String sync_serviceheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service']";
		public static final String status_servicetypeheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Service Type']";
		public static final String sync_detailsheader = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Details']";
		public static final String sync_syncstatus = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[1]//label[text()='Sync Status']";
		public static final String devicesforservice_deviceheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Device']";
		public static final String devicesforservice_syncstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Sync Status']";
		public static final String devicesforservice_smartsheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Smarts']";
		public static final String devicesforservice_fetchinterfacesheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='Fetch Interfaces']";
		public static final String devicesforservice_vistamartdeviceheader = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='ag-div-margin heading-green row'])[1]//label[text()='VistaMart Device']";

		public static final String status_ordername = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[2]/div/label";
		public static final String status_servicename = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[1]/a";
		public static final String status_servicetype = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";
		public static final String status_servicedetails = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]//a";
		public static final String status_currentstatus = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";
		public static final String status_modificationtime = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]";
		public static final String statuslink = "@xpath=(//div[text()='Status']/parent::div/parent::div//div[@class='div-margin row'])[3]/div/a[text()='Status']";

		public static final String sync_ordername = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[2]/div/label";
		public static final String sync_servicename = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[1]/a";
		public static final String sync_servicetype = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[2]";
		public static final String sync_servicedetails = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[3]//a";
		public static final String sync_status = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[4]";

		public static final String vistamart_status = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]/span";
		public static final String vistamart_datetime = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]//p";
		public static final String synchronization_serviceerror = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div[5]/span";
		public static final String synchronizelink = "@xpath=(//div[text()='Synchronisation']/parent::div/parent::div//div[@class='div-margin row'])[3]/div//a[text()='Synchronize']";
		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";
		public static final String changestatus_dropdown = "@xpath=//label[text()='Change Status']/parent::div//input";
		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";

		public static final String statuspanel_header = "@xpath=//div[text()='Status']";
		public static final String servicestatus_header = "@xpath=//div[@class='modal-header']/div[text()='Service Status']";
		public static final String statuspage_serviceidentification = "@xpath=//label[text()='Service Identification']/following-sibling::div";
		public static final String statuspage_servicetype = "@xpath=//label[text()='Service Type']/following-sibling::div";
		public static final String servicestatushistory_header = "@xpath=//div[text()='Service Status History']";
		public static final String statuspage_currentstatus = "@xpath=//label[text()='Current Status']/following-sibling::div";
		public static final String statuspage_newstatusdropdown = "@xpath=//label[text()='New Status']/following-sibling::select";
		public static final String statuspage_newstatusdropdownvalue = "@xpath=//label[text()='New Status']/following-sibling::select/option";

		public static final String servicestatushistory = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";

		public static final String devicesforservice_header = "@xpath=//div[text()='Devices for service']";
		public static final String deviceforservicepanel_devicename = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[1]";
		public static final String deviceforservicepanel_syncstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[2]";
		public static final String deviceforservicepanel_smartsstatus = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/span";
		public static final String deviceforservicepanel_smartsdatetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[3]/p";
		public static final String deviceforservicepanel_fetchinterfaces_status = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[4]/span";
		public static final String deviceforservicepanel_fetchinterfaces_datetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[4]/p";
		public static final String deviceforservicepanel_vistamart_status = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[5]/span";
		public static final String deviceforservicepanel_vistamart_datetime = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div[5]/p";
		public static final String deviceforservicepanel_managelink = "@xpath=(//div[text()='Devices for service']/parent::div/parent::div//div[@class='div-margin row'])[1]//div/a[text()='Manage']";

		public static final String successmsg = "@xpath=//div[@role='alert']//span";
		public static final String editservice_okbutton = "@xpath=//span[contains(text(),'Ok')]";
		public static final String managesubnets_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets']";
		public static final String managesubnet_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Manage')]";
		public static final String managesubnet_successmsg = "@xpath=//div[@class='modal-content']//div[@role='alert']//span";
		public static final String spacename_column = "@xpath=//span[text()='Space Name']";
		public static final String blockname_column = "@xpath=//span[text()='Block Name']";
		public static final String subnetname_column = "@xpath=//span[text()='Subnet Name']";
		public static final String startaddress_column = "@xpath=//span[text()='Start Address']";
		public static final String size_column = "@xpath=//span[text()='Size']";
		public static final String closesymbol = "@xpath=//button[@type='button']//span[text()='�']";
		public static final String dumppage_header = "@xpath=//div[@class='modal-header']//div[contains(text(),'Service')]";
		public static final String serviceretrieved_text = "@xpath=//div[@class='div-margin row'][contains(text(),'Service retrieved')]";
		public static final String service_header = "@xpath=//label[text()='Service']";
		public static final String dumppage_text = "@xpath=//label[text()='Service']/following-sibling::textarea";

		public static final String managesubnetsipv6_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets Ipv6']";
		public static final String shownewinfovistareport_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show New Infovista Report']";
		public static final String dump_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='dump']";
		public static final String selectedemail = "@xpath=//select[@name='selectedEmail']//option";
		public static final String selectedphone = "@xpath=//select[@name='selectedPhoneContact']//option";

		// !-- Management Options -- = "@xpath=
		public static final String managementoptions_performancereporting = "@xpath=//label[contains(text(),'Performance Reporting')]/following-sibling::div";
		public static final String managementoptions_ipguardian = "@xpath=//label[contains(text(),'IP Guardian')]/following-sibling::div";
		public static final String managementoptions_managedservice = "@xpath=//label[contains(text(),'Managed Service')]/following-sibling::div";
		public static final String managementoptions_snmpnotification = "@xpath=//label[contains(text(),'SNMP Notification')]/following-sibling::div";
		public static final String managementoptions_routerconfigIPv6 = "@xpath=//label[contains(text(),'Router Configuration View IPv6')]/following-sibling::div";
		public static final String managementoptions_routerconfigIPv4 = "@xpath=//label[contains(text(),'Router Configuration View IPv4')]/following-sibling::div";
		public static final String managementoptions_syslogevent = "@xpath=//label[contains(text(),'Syslog Event View')]/following-sibling::div";
		public static final String managementoptions_deliverychannel = "@xpath=//label[contains(text(),'Delivery Channel')]/following-sibling::div";

		// !-- configuration options panel -- = "@xpath=
		public static final String configoptions_routerbasedfirewall = "@xpath=//label[contains(text(),'Router Based Firewall')]/following-sibling::div";
		public static final String configoptions_withlogicaltunnel = "@xpath=//label[contains(text(),'With Logical Tunnel')]/following-sibling::div";
		public static final String configoptions_qos = "@xpath=//label[contains(text(),'QoS')]/following-sibling::div";
		public static final String configoptions_modularmsp = "@xpath=//label[contains(text(),'Modular MSP')]/following-sibling::div";
		public static final String configoptions_l2technology = "@xpath=//label[contains(text(),'L2 Technology')]/following-sibling::div";
		public static final String configoptions_bgpasnumber = "@xpath=//label[contains(text(),'BGP AS Number')]/following-sibling::div";
		public static final String configoptions_customerdescription = "@xpath=//label[contains(text(),'Customer Description')]/following-sibling::div";
		public static final String cofigoptions_routingprotocol = "@xpath=//label[contains(text(),'Routing Protocol')]/following-sibling::div";

		// !-- L2 Technology -- = "@xpath=
		public static final String L2technology_panelheader = "@xpath=//div[text()='L2 Technology']";
		public static final String L2technology_actiondropdown = "@xpath=//div[div[text()='L2 Technology']]//button[text()='Action']";
		public static final String addL2circuitwizardlink = "@xpath=//div[div[text()='L2 Technology']]//a[text()='L2 Circuit Wizard']";
		public static final String L2technology_modalheader = "@xpath=//div[@class='modal-header']/div[text()='L2 Circuit']";
		public static final String L2technology_viewlink = "@xpath=//div[div[text()='L2 Technology']]//a[text()='View']";
		public static final String L2technology_editlink = "@xpath=//div[div[text()='L2 Technology']]//a[text()='Edit']";
		public static final String addL2circuit_modal = "@xpath=//div[@class='modal-content']";
		public static final String addL2circuit_remarktextarea = "@xpath=//label[contains(text(),'Remark')]/following-sibling::textarea";
		public static final String addL2circuit_circuitreference_textfield = "@xpath=//label[text()='Circuit Reference']/following-sibling::input";
		public static final String addedL2circuit_remarkcolumnheader = "@xpath=//span[@role='columnheader'][text()='Remark']";
		public static final String addedL2circuit_remarkvalue = "@xpath=//div[@role='gridcell'][@col-id='remark']";
		public static final String addedL2circuit_interfacespeedcolumn = "@xpath=//span[@role='columnheader'][text()='Interface Speed']";
		public static final String addedL2circuit_interfacespeedvalue = "@xpath=//div[@role='gridcell'][@col-id='interfaceSpeed']";
		public static final String addedL2circuit_circuittypecolumn = "@xpath=//span[@role='columnheader'][text()='Circuit Type']";
		public static final String addedL2circuit_circuittypevalue = "@xpath=//div[@role='gridcell'][@col-id='circuitTypeDesc']";
		public static final String addedL2circuit_checkbox = "@xpath=//div[@role='gridcell'][@col-id='remark']/parent::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String viewcircuits_header = "@xpath=//div[contains(text(),'Circuits')]";
		public static final String viewL2circuit_circuitreferencevalue = "@xpath=//label[text()='Circuit Reference']/following-sibling::div";
		public static final String viewL2circuit_remarkvalue = "@xpath=//label[text()='Remarks']/following-sibling::div";
		public static final String viewL2circuit_interfacespeedvalue = "@xpath=//label[text()='Interface Speed']/following-sibling::div";
		public static final String viewL2circuit_circuittypevalue = "@xpath=//label[text()='Circuit Type']/following-sibling::div";
		public static final String viewL2circuit_managementordervalue = "@xpath=//label[text()='Management Order']/following-sibling::div";
		public static final String viewL2circuit_ENNIValue = "@xpath=//div[label[text()='ENNI']]//div";
		public static final String viewcircuits_actiondropdown = "@xpath=//div[div[contains(text(),'Circuits')]]//button[text()='Action']";
		public static final String viewcircuits_editlink = "@xpath=//div[div[contains(text(),'Circuits')]]//a[text()='Edit']";
		public static final String viewcircuits_deletelink = "@xpath=//div[div[contains(text(),'Circuits')]]//a[text()='Delete']";
		public static final String editcircuits_modalheader = "@xpath=//div[text()='L2 Circuit']";
		public static final String popup_close = "@xpath=//button/span[text()='�']";
		public static final String viewL2circuit_siteAheader = "@xpath=//div[text()='SITE A']";
		public static final String siteA_actiondropdown = "@xpath=//div[div[text()='SITE A']]//button[text()='Action']";
		public static final String siteA_addlink = "@xpath=//div[div[text()='SITE A']]//a[text()='Add']";
		public static final String siteA_editlink = "@xpath=//div[div[text()='SITE A']]//a[text()='Edit']";
		public static final String siteA_viewlink = "@xpath=//div[div[text()='SITE A']]//a[text()='View']";
		public static final String circuitsite_selectcity = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String circuitsite_citylabel = "@xpath=//label[text()=' Device Xng City']";
		public static final String circuitsite_selectsite = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']//div[@class='react-switch-handle']";
		public static final String physicalsite_dropdown = "@xpath=//div[label[contains(text(),'Physical Site')]]//select";
		public static final String csrname_field = "@xpath=//div[label[text()='CSR Name']]//input[@id='csrName']";
		public static final String vlanid_field = "@xpath=//div[label[text()='VLAN Id']]//input";
		public static final String remarks_textarea = "@xpath=//div[label[text()='Remarks']]//textarea";
		public static final String circuitsite_devicename = "@xpath=//div[label[text()='Device Name']]//input[@id='deviceName']";
		public static final String circuitsite_warngmsg_devicecountry = "@xpath=//label[text()='Device Country']/parent::div//div";
		public static final String circuitsite_warngmsg_devicexngcity = "@xpath=//label[text()='Device Xng City']/parent::div//div";
		public static final String circuitsite_warngmsg_csrname = "@xpath=//label[text()='CSR Name']/parent::div//div";
		public static final String circuitsite_warngmsg_devicename = "@xpath=//label[text()='Device Name']/parent::div//div";
		public static final String warngmsg_circuittype = "@xpath=//label[text()='Circuit Type']/parent::div//div";
		public static final String warngmsg_interfacespeed = "@xpath=//label[text()='Interface Speed']/parent::div//div";
		public static final String warngmsg_overture_circuittype = "@xpath=//label[text()='Circuit Type']/parent::div/following-sibling::div";
		public static final String warngmsg_overture_interfacespeed = "@xpath=//label[text()='Interface Speed']/parent::div/following-sibling::div";
		public static final String viewsiteA_header = "@xpath=//div[text()='Site A']";
		public static final String viewsite_remarkvalue = "@xpath=//label[text()='Remark']/parent::div/following-sibling::div";
		public static final String viewsite_devicecountry = "@xpath=//label[text()='Device Country']/parent::div/following-sibling::div";
		public static final String viewsite_devicecity = "@xpath=//label[text()='Device Xng City']/parent::div/following-sibling::div";
		public static final String viewsite_L2technologyvalue = "@xpath=//label[text()='L2 Technology']/parent::div/following-sibling::div";
		public static final String viewsite_protectedvalue = "@xpath=//label[text()='Protected']/parent::div/following-sibling::div";
		public static final String viewsite_devicename = "@xpath=//label[text()='Device Name']/parent::div/following-sibling::div";
		public static final String siteA_namecolumn = "@xpath=//div[div[text()='SITE A']]/following-sibling::div//th[text()='Name']";
		public static final String siteA_devicecountry = "@xpath=//div[div[text()='SITE A']]/following-sibling::div//th[text()='Device Country']";
		public static final String siteA_devicecity = "@xpath=//div[div[text()='SITE A']]/following-sibling::div//th[text()='Device Xing City']";
		public static final String siteA_technology = "@xpath=//div[div[text()='SITE A']]/following-sibling::div//th[text()='Technology']";
		public static final String siteA_namevalue = "@xpath=(//div[div[text()='SITE A']]/following-sibling::div//td)[1]";
		public static final String siteA_devicecountryvalue = "@xpath=(//div[div[text()='SITE A']]/following-sibling::div//td)[2]";
		public static final String siteA_devicecityvalue = "@xpath=(//div[div[text()='SITE A']]/following-sibling::div//td)[3]";
		public static final String siteA_technologyvalue = "@xpath=(//div[div[text()='SITE A']]/following-sibling::div//td)[4]";
		public static final String editsite_devicecountryvalue = "@xpath=//label[text()='Device Country']/parent::div//div";
		public static final String editsite_devicecityvalue = "@xpath=//label[text()='Device Xng City']/parent::div//div";
		public static final String editsite_physicalsitevalue = "@xpath=//label[text()='Physical Site']/parent::div//div";
		public static final String protected_checkbox = "@xpath=//div[label[text()='Protected']]//div/input";

		// !-- Site B -- = "@xpath=
		public static final String viewL2circuit_siteBheader = "@xpath=//div[text()='SITE B']";
		public static final String siteB_actiondropdown = "@xpath=//div[div[text()='SITE B']]//button[text()='Action']";
		public static final String siteB_addlink = "@xpath=//div[div[text()='SITE B']]//a[text()='Add']";
		public static final String siteB_editlink = "@xpath=//div[div[text()='SITE B']]//a[text()='Edit']";
		public static final String siteB_viewlink = "@xpath=//div[div[text()='SITE B']]//a[text()='View']";
		public static final String viewsiteB_header = "@xpath=//div[text()='Site B']";
		public static final String siteB_namecolumn = "@xpath=//div[div[text()='SITE B']]/following-sibling::div//th[text()='Name']";
		public static final String siteB_devicecountry = "@xpath=//div[div[text()='SITE B']]/following-sibling::div//th[text()='Device Country']";
		public static final String siteB_devicecity = "@xpath=//div[div[text()='SITE B']]/following-sibling::div//th[text()='Device Xing City']";
		public static final String siteB_technology = "@xpath=//div[div[text()='SITE B']]/following-sibling::div//th[text()='Technology']";
		public static final String siteB_namevalue = "@xpath=(//div[div[text()='SITE B']]/following-sibling::div//td)[1]";
		public static final String siteB_devicecountryvalue = "@xpath=(//div[div[text()='SITE B']]/following-sibling::div//td)[2]";
		public static final String siteB_devicecityvalue = "@xpath=(//div[div[text()='SITE B']]/following-sibling::div//td)[3]";
		public static final String siteB_technologyvalue = "@xpath=(//div[div[text()='SITE B']]/following-sibling::div//td)[4]";

		public static final String Acteliscircuit_warngmsg_devicecountry = "@xpath=//label[text()='Device Country']/parent::div/following-sibling::div";
		public static final String Acteliscircuit_warngmsg_circuitreference = "@xpath=//label[text()='Circuit Reference']/parent::div//div";
		public static final String Acteliscircuit_warngmsg_devicecity = "@xpath=//label[text()='Device Xng City']/parent::div/following-sibling::div";
		public static final String Acteliscircuit_warngmsg_physicalsite = "@xpath=//label[text()='Physical Site']/parent::div/following-sibling::div";
		public static final String Acteliscircuit_Countrydropdown = "@xpath=//div[label[contains(text(),'Device Country')]]//input";
		public static final String Acteliscircuite_sitename = "@xpath=//div[label[contains(text(),'Site Name')]]//input";
		public static final String Acteliscircuit_sitecode = "@xpath=//div[label[contains(text(),'Site Code')]]//input";
		public static final String Actelis_addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String Actelis_addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String Acteliscircuit_Citydropdown = "@xpath=//div[label[contains(text(),'Device Xng City')]]//input";
		public static final String Acteliscircuit_Sitedropdown = "@xpath=//div[label[contains(text(),'Physical Site')]]//input";
		public static final String viewActelis_remarkvalue = "@xpath=//label[text()='Remark']/following-sibling::div";
		public static final String viewActelis_devicecountry = "@xpath=//label[text()='Device Country']/parent::div//div";
		public static final String viewActelis_devicecity = "@xpath=//label[text()='Device Xng City']/parent::div//div";
		public static final String viewActelis_physicalsite = "@xpath=//label[text()='Physical Site']/parent::div//div";
		public static final String viewActelis_vlanid = "@xpath=//label[text()='VLAN Id']/parent::div//div";

		public static final String managementorder_dropdown = "@xpath=//div[label[text()='Management Order']]//div/input";
		public static final String selectorder_toggle = "@xpath=//div[@class='react-switch-bg']";
		public static final String managementorder_textfield = "@xpath=//div[label[text()='Management Order']]//input";
		public static final String circuittype_dropdown = "@xpath=//div[label[text()='Circuit Type']]//div/input";
		public static final String interfacespeed_dropdown = "@xpath=//div[label[text()='Interface Speed']]//div/input";
		public static final String ENNI_checkbox = "@xpath=//div[label[text()='ENNI']]//input";

		// !-- SiteA Equipment -- = "@xpath=
		public static final String equipment_header = "@xpath=//div[text()='Equipment']";
		public static final String equipment_adddevicelink = "@xpath=//a[text()='Add Device']";
		public static final String equipment_cpedevice_header = "@xpath=//div/p[text()='Add CPE Device']";
		public static final String equipment_warngmsg_vendormodel = "@xpath=//div[label[contains(text(),'Vendor/Model')]]/div";
		public static final String equipment_warngmsg_managementaddress = "@xpath=(//div[label[contains(text(),'Management Address')]])[2]/following-sibling::div";
		public static final String equiment_warngmsg_MACaddress = "@xpath=//div[label[contains(text(),'MAC Address')]]/div";
		public static final String equipment_warngmsg_poweralarm = "@xpath=//div[label[contains(text(),'Power Alarm')]]/div";
		public static final String equipment_warngmsg_mediaselection = "@xpath=//div[label[contains(text(),'Media Selection')]]/div";
		public static final String equipment_cpeName = "@xpath=//div[label[contains(text(),'Name')]]//input";
		public static final String equipment_networkvalue = "@xpath=//div[label[text()='Network']]";
		public static final String equipment_managementaddress_field = "@xpath=//div[label[text()='Management Address']]//input";
		public static final String equipment_managementaddress_dropdown = "@xpath=//div[label[text()='Management Address']]//select";
		public static final String equipment_MACaddress_field = "@xpath=//div[label[text()='MAC Address']]//input";
		public static final String equipment_MEPID = "@xpath=//div[label[text()='MEP ID']]//input";
		public static final String equipment_poweralarm_dropdown = "@xpath=//div[label[text()='Power Alarm']]//select";
		public static final String equipment_mediaselection_dropdown = "@xpath=//div[label[text()='Media Selection']]//select";
		public static final String equipment_linklostforwarding_checkbox = "@xpath=//div[label[text()='Link Lost Forwarding']]//input";
		public static final String existingCPE_toggle = "@xpath=(//div[@class='react-switch-bg'])[1]";
		public static final String equipment_changeaddresstoggle = "@xpath=(//div[@class='react-switch-bg'])[2]";
		public static final String equipment_choosedevice_dropdown = "@xpath=//div[label[text()='Choose a Device']]//input";

		// !-- Users panel in view service page -- = "@xpath=
		public static final String usergridcheck = "@xpath=(//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]";
		public static final String existinguser_row = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String addeduser_checkbox = "@xpath=//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";
		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";
		public static final String EmailColumn = "@xpath=//div[@col-id='email']";
		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";
		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";
		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";
		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";
		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		// public static final String view = "@xpath=//div[@class='dropdown-menu
		// show']//a[text()='View']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		// !-- New User creation in view service page -- = "@xpath=

		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";
		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";
		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String UserName = "@xpath=//input[@id='userName']";
		public static final String FirstName = "@xpath=//input[@id='firstName']";
		public static final String SurName = "@xpath=//input[@id='surname']";
		public static final String PostalAddress = "@xpath=//textarea[@name='postalAddress']";
		public static final String adduser_Email = "@xpath=//input[@id='email']";
		public static final String adduser_Phone = "@xpath=//input[@id='phone']";
		public static final String ipguardianaccountgroup = "@xpath=//input[@id='ipGuardianAccountGrp']";
		public static final String coltonlineuser = "@xpath=//input[@id='onlineUser']";
		public static final String rolesaddarrow = "@xpath=//label[text()='Roles']/parent::div/parent::div//button//span[text()='>>']";

		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";
		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";
		public static final String user_okbutton = "@xpath=//button[@type='submit']//span[text()='OK']";
		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()='Delete']";
		public static final String deletesuccessmsg = "@xpath=(//div[@role='alert']//span)[1]";
		public static final String userspanel_header = "@xpath=//div[text()='Users']";
		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";
		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";
		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";
		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";
		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";
		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";
		public static final String OK_button = "@xpath=//button[@type='submit']//span[text()='OK']";
		public static final String userdelete = "@xpath=//button[text()='Delete']";

		// !-- Warning messages -- = "@xpath=
		public static final String warningmsg_username = "@xpath=//label[text()='User Name']/parent::div//div";
		public static final String warningmsg_firstname = "@xpath=//label[text()='First Name']/parent::div//div";
		public static final String warningmsg_surname = "@xpath=//label[text()='Surname']/parent::div//div";
		public static final String warningmsg_postaladdress = "@xpath=//label[text()='Postal Address']/parent::div/following-sibling::div";
		public static final String warningmsg_useremail = "@xpath=//label[text()='Email']/parent::div//div";
		public static final String warningmsg_userphone = "@xpath=//label[text()='Phone']/parent::div//div";
		public static final String warningmsg_userpassword = "@xpath=//label[text()='Password']/parent::div//div";

		// !-- Select Dropdown -- = "@xpath=
		public static final String legalcustomerName_labelName = "@xpath=//label[text()='Legal Customer Name']";

		public static final String IPGuardianAccountGroup = "@xpath=//input[@id='ipGuardianAccountGrp']";
		public static final String IPGuardianAccountGroup_Text = "@xpath=//label[contains(text(),'IPGuardian Account Group')]";
		public static final String IPGuardianAccountGroup_viewpage = "@xpath=//div[div[label//b[contains(text(),'IPGuardian Account Group')]]]/div[2]";
		public static final String ColtOnlineUser = "@xpath=//input[@id='onlineUser']";
		public static final String ColtOnlineUser_Text = "@xpath=//label[contains(text(),'Colt Online User')]";
		public static final String coltonlineuser_viewpage = "@xpath=//div[div[label//b[contains(text(),'Colt Online User')]]]/div[2]";
		public static final String Password_Text = "@xpath=//label[contains(text(),'Password')]";
		public static final String Password_Textfield = "@xpath=//input[@id='password']";
		public static final String GeneratePasswordLink = "@xpath=//span[text()='Generate Password']";
		public static final String GeneratePasswordLink_Text = "@xpath=//span[@class='badge-success badge badge-secondary']";

		public static final String roleDropdown_available = "@xpath=//select[@id='availableRoles']//option";
		public static final String roleDropdown_addButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='>>']";
		public static final String roleDropdown_removeButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='value']";
		public static final String roleDropdown_selectedValues = "@xpath=//select[@id='selectedRoles']//option";

		public static final String hideRouterToolIPv4_Huawei_available = "@xpath=//select[@id='availableIPV4CommandHuawei']//option";
		public static final String hideRouterToolIPv4__Huawei_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='>>']";
		public static final String hideRouterToolIPv4_Huawei_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='value']";
		public static final String hideRouterToolIpv4_Huawei_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4_Cisco_Available = "@xpath=//select[@id='availableIPV4CommandCisco']//option";
		public static final String hideRouterToolIPv4_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";
		public static final String hideRouterToolIPv4_Cisco_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='value']";
		public static final String hideRouterToolIpv4_Cisco_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandCisco']//option";

		public static final String HideService_Available = "@xpath=//select[@id='availableHideService']//option";
		public static final String HideService_addButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='>>']";
		public static final String HideService_removeButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='value']";
		public static final String HideServicesDropdown_selectedValues = "@xpath=//select[@id='selectedHideService']//option";

		public static final String HideSiteOrder_Available = "@xpath=//select[@id='availableHideSiteOrder']//option";
		public static final String hideSiteOrder_addbutton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='>>']";
		public static final String hideSiteOrder_removeButton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='value']";
		public static final String HideSiteOrderDropdown_selectedValues = "@xpath=//select[@id='selectedHideSiteOrder']//option";

		public static final String HideRouterToolIPv6_Cisco_Available = "@xpath=//select[@id='availableIPV6Command']//option";
		public static final String hideRouterToolIPv6_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String viewUser_HiddenRouterToolIPv4Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv4Huawei = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv6Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]]//following-sibling::div";

		public static final String selectValueUnderUserPanel_ViewSericePage = "@xpath=//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		// !-- Management Options panel in view service page -- = "@xpath=

		public static final String managementoptions_header = "@xpath=//div[text()='Management Options']";
		public static final String portalaccess_header = "@xpath=//label[text()='Portal Access']";

		// !-- Device Panel -- = "@xpath=
		public static final String addpedevice_link = "@xpath=//a[text()='Add PE Device']";
		public static final String addpedevice_header = "@xpath=//div[text()='Add Ethernet PE Device']";
		public static final String addnewdevice_togglebutton = "@xpath=(//div[@class='react-switch-handle'])[1]";
		public static final String providerequipment_header = "@xpath=//div[text()='Provider Equipment (PE)']";
		public static final String addeddevices_list = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div//div[@class='div-margin row']//b";
		public static final String addeddevice_viewlink = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='View']";
		public static final String addeddevice_viewlink1 = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'";
		public static final String addeddevice_viewlink2 = "')]/parent::div/following-sibling::div//span[text()='View']";

		public static final String addeddevice_selectinterfaceslink = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div/div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";
		public static final String addeddevice_deletefromservicelink = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div//div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete from Service']";

		// !-- Create Device Page -- = "@xpath=
		public static final String addeddevicename = "@xpath=(//div[text()='ASR Device']/parent::div/following-sibling::div[@class='div-margin row'])//b";
		public static final String vendormodelinput = "@xpath=//div[label[text()='Vendor/Model']]//select";
		public static final String snmproinputfield = "@xpath=//input[@id='snmpro']";
		public static final String snmprotextfield = "@xpath=//input[@id='snmpro']";
		public static final String countryinput = "@xpath=//div[label[contains(text(),'Country')]]//select";
		public static final String managementaddresstextbox = "@xpath=//label[text()='Management Address']/following-sibling::input";
		public static final String citydropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'City')]]//select";
		public static final String sitedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//select";
		public static final String premisedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//input";
		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String successMessage_Alert = "@xpath=//div[@role='alert']//span";
		public static final String addeddevice_interface_actiondropdown = "@xpath=//div[div[text()='Provider Equipment (PE)']]/following-sibling::div//div//button[text()='Action']";
		public static final String cloudprioritization_checkbox = "@xpath=//label[text()='Cloud Prioritization']/following-sibling::div//input";

		// !-- Equipment in L2 technology -- = "@xpath=
		public static final String addedequipmentdevicename = "@xpath=(//div[text()='Equipment']/parent::div/following-sibling::div[@class='div-margin row'])[2]";
		public static final String existingequipmentdevicegrid = "@xpath=(//div[text()='Equipment']/parent::div/following-sibling::div[@class='div-margin row'])[2]";
		public static final String addedequipmentdevice_viewlink = "@xpath=//div[text()='Equipment']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='View']";
		public static final String addedequipmentdevice_viewlink1 = "@xpath=//div[text()='Equipment']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'";
		public static final String addedequipmentdevice_viewlink2 = "')]/parent::div/following-sibling::div//span[text()='View']";
	
		
		public static final String addedequipmentdevice_editlink = "@xpath=//div[text()='Equipment']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Edit']";
		public static final String addedequipmentdevice_editlink1 = "@xpath=//div[text()='Equipment']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'";
		public static final String addedequipmentdevice_editlink2 = "')]/parent::div/following-sibling::div//span[text()='Edit']";

		
		public static final String viewequipment_routerid = "@xpath=//div[label[text()='Router Id']]/following-sibling::div";
		public static final String viewequipment_name = "@xpath=//div[label[text()='Name']]/following-sibling::div";
		public static final String viewequipment_vendormodel = "@xpath=//div[label[text()='Vendor/Model']]/following-sibling::div";
		public static final String viewequipment_snmpro = "@xpath=//div[label[text()='Snmpro']]/following-sibling::div";
		public static final String viewequipment_managementaddress = "@xpath=//div[label[text()='Management Address']]/following-sibling::div";
		public static final String viewequipment_mepid = "@xpath=//div[label[text()='MEP Id']]/following-sibling::div";
		public static final String viewequipment_poweralarm = "@xpath=//div[label[text()='Power Alarm']]/following-sibling::div";
		public static final String viewequipment_linklostforwarding = "@xpath=//div[label[text()='Link Lost Forwarding']]/following-sibling::div";
		public static final String viewequipment_hexaserialnumber = "@xpath=//div[label[text()='Hexa Serial Number']]/following-sibling::div";
		public static final String viewequipment_serialnumber = "@xpath=//div[label[text()='Serial Number']]/following-sibling::div";

		// !-- Warning Message -- = "@xpath=
		public static final String warningMessage_Country = "@xpath=//div[text()='Country']";
		public static final String warningMessage_name = "@xpath=//div[label[text()='Name']]//div";
		public static final String warningMessage_vendor = "@xpath=//div[label[text()='Vendor/Model']]//div";

		// !-- fetch device Interface -- = "@xpath=
		public static final String fetchDeviceinterfacelink_viewDevicePage = "@xpath=//a[text()='Fetch device interfaces']";
		public static final String ClickhereLink_fetchInterface = "@xpath=//a[text()='here']";

		public static final String addCitytogglebutton = "@xpath=//div[div[label[text()='Add City']]]//div[@class='react-switch-bg']";
		public static final String citynameinputfield = "@xpath=//input[@id='cityName']";
		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode']";
		public static final String sitenameinputfield = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String sitecodeinputfield = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";
		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";

		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2]";
		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2]";
		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";
		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";

		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";
		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		
		// !-- view device page -- = "@xpath=
		public static final String viewpage_devicename = "@xpath=//label[text()='Name']/parent::div//div";
		public static final String viewpage_vendormodel = "@xpath=//label[text()='Vendor/Model']/parent::div//div";
		public static final String viewpage_managementaddress = "@xpath=//label[text()='Management Address']/parent::div//div";
		public static final String viewpage_country = "@xpath=//label[text()='Country']/parent::div//div";
		public static final String viewpage_city = "@xpath=//label[text()='City']/parent::div//div";
		public static final String viewpage_site = "@xpath=//label[text()='Site']/parent::div//div";
		public static final String viewpage_premise = "@xpath=//label[text()='Premise']/parent::div//div";
		public static final String viewpage_connectivityprotocol = "@xpath=//label[text()='Connectiviy Protocol']/parent::div//div";
		public static final String viewpage_snmpversion = "@xpath=//label[text()='SNMP Version']/parent::div//div";
		public static final String viewpage_snmpro = "@xpath=//label[text()='Snmpro']/parent::div//div";
		public static final String viewpage_snmprw = "@xpath=//label[text()='Snmprw']/parent::div//div";
		public static final String viewpage_snmpv3username = "@xpath=//label[text()='Snmp V3 User Name']/following-sibling::div";
		public static final String viewpage_snmpv3privpassword = "@xpath=//label[text()='Snmp V3 Priv Password']/following-sibling::div";
		public static final String viewpage_snmpv3authpassword = "@xpath=//label[text()='Snmp V3 Auth Password']/following-sibling::div";

		// !-- Edit Device -- = "@xpath=
		public static final String viewdevice_Actiondropdown = "@xpath=//div[text()='Device']/following-sibling::div//button[text()='Action']";
		public static final String viewdevice_Edit = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Edit']";
		public static final String editdeviceheader = "@xpath=//div[@class='heading-green-row row']//div[text()='Edit Ethernet PE Device']";

		public static final String existingdevicegrid = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div//div[@class='div-margin row']";
		public static final String viewservicepage_viewdevicelink = "@xpath=//span//a[text()='View']";
		public static final String viewdevicepage_header = "@xpath=(//div[contains(text(),'Device')])[1]";
		public static final String viewservicepage_editdevicelink = "@xpath=//a/span[text()='Edit']";

		// !-- Delete Device -- = "@xpath=
		public static final String viewdevice_delete = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Delete']";
		public static final String deletealertclose = "@xpath=//div[@class='modal-content']//button//span[text()='�']";

		public static final String managementaddressbutton = "@xpath=//button//span[text()='Management Address']";

		// !-- Fetch Interface -- = "@xpath=
		public static final String viewdevice_fetchinterfacelink = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Fetch Device Interfaces']";
		public static final String fetchsuccessmsg = "@xpath=(//div[@role='alert']//span)[1]";
		public static final String herelink_fetchinterface = "@xpath=(//div[@role='alert']//span)[1]/a";
		public static final String fetchDeviceInterace_SuccessMessage = "@xpath=(//div[contains(@class,'alert-success')]/span)[1]";
		public static final String fetchdeviceInterface_successtextMessage = "@xpath=(//div[@role='alert']/span)[1]s";

		// !-- Manage Network -- = "@xpath=
		public static final String managenetwork_header = "@xpath=(//div[@class='heading-green-row row'])[1]//div";
		public static final String status_header = "@xpath=(//div[@class='heading-green-row row'])[2]//div";
		public static final String synchronization_header = "@xpath=(//div[@class='heading-green-row row'])[3]//div";
		public static final String status_devicecolumn = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String status_statuscloumn = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Status']";
		public static final String status_lastmodificationcolumn = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Last Modification']";
		public static final String status_Action = "@xpath=//div[text()='Status']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String synchronization_devicecolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Device']";
		public static final String synchronization_syncstatuscolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Sync Status']";
		public static final String synchronization_smartscolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Smarts']";
		public static final String synchronization_FetchInterfacescolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Fetch Interfaces']";
		public static final String synchronization_vistamartdevice = "@xpath=(//label[@class='form-label'])[9]";
		public static final String synchronization_actioncolumn = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div//label[text()='Action']";
		public static final String status_devicevalue = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[1]";
		public static final String status_statusvalue = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[2]";
		public static final String status_lastmodificationvalue = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']/div[3]";
		public static final String status_statuslink = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[2]";
		public static final String status_viewinterfaceslink = "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//a)[3]";
		public static final String synchronization_devicevalue = "@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div//a)[1]";
		public static final String synchronization_syncstatusvalue = "@xpath=//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']/div[2]/span";
		public static final String synchronization_smartsvalue = "@xpath=(//label[text()='Smarts']/parent::div/parent::div/following-sibling::div//div)[3]/span";
		public static final String synchronization_fetchinterfacesvalue = "@xpath=(//label[text()='Fetch Interfaces']/parent::div/parent::div/following-sibling::div//div)[4]/span";
		public static final String synchronization_vistamartdevicevalue = "@xpath=(//div[@class='div-margin row']//div)[10]//span";
		public static final String synchronization_synchronizelink = "@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div//a)[2]";
		public static final String managenetwork_backbutton = "@xpath=(//div[@class='div-margin row'])[4]//button";
		public static final String smarts_datetimevalue = "@xpath=(//div[text()='Synchronization']/parent::div/following-sibling::div[@class='div-margin row']//div)[4]//span/parent::div";
		public static final String fetchinterfaces_datetime = "@xpath=(//label[text()='Fetch Interfaces']/parent::div/parent::div/following-sibling::div//div)[4]";
		public static final String vistamartdevice_datetime = "@xpath=(//label[text()='VistaMart Device']/parent::div/parent::div/following-sibling::div//div)[5]";
		// public static final String Sync_successmsg =
		// "@xpath=//div[@role='alert']//span";
		public static final String staus_statuspopup = "@xpath=//div[@class='modal-content']";
		public static final String Statuspage_header = "@xpath=//div[@class='modal-header']//div";
		public static final String statuspage_nameheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']";
		public static final String statuspage_vendormodelheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']";
		public static final String statuspage_managementaddressheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']";
		public static final String statuspage_snmproheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']";
		public static final String statuspage_countryheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']";
		public static final String statuspage_cityheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']";
		public static final String statuspage_siteheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']";
		public static final String statuspage_premiseheader = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']";
		public static final String statuspage_namevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Name']/following-sibling::div";
		public static final String statuspage_vendormodelvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Vendor/Model']/following-sibling::div";
		public static final String statuspage_managementaddressvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Management Address']/following-sibling::div";
		public static final String statuspage_snmprovalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[1]//label[text()='Snmpro']/following-sibling::div";
		public static final String statuspage_countryvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Country']/following-sibling::div";
		public static final String statuspage_cityvalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='City']/following-sibling::div";
		public static final String statuspage_sitevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Site']/following-sibling::div";
		public static final String statuspage_premisevalue = "@xpath=(//div[@class='modal-body']//div[@class='row'])[2]//label[text()='Premise']/following-sibling::div";
		public static final String Statuspage_statusheader = "@xpath=//div[@class='modal-content']//div[@class='heading-green-row row']//div";
		public static final String statuspage_currentstatusfieldheader = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']";
		public static final String statuspage_newstatusfieldheader = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='New Status']";
		public static final String statuspage_currentstatusvalue = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//label[text()='Current Status']/following-sibling::div";
		// public static final String statuspage_newstatusdropdown =
		// "@xpath=(//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin
		// row']//label)[2]/following-sibling::select";
		// public static final String statuspage_newstatusdropdownvalue =
		// "@xpath=((//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin
		// row']//label)[2]/following-sibling::select//option)[1]";
		public static final String statuspage_okbutton = "@xpath=(//div[@class='modal-content']//button[@type='button']//span)[3]";
		public static final String statuspage_statuscolumnheader = "@xpath=//span[@role='columnheader'][text()='Status']";
		public static final String statuspage_changedon_columnheader = "@xpath=//span[@role='columnheader'][text()='Changed On']";
		public static final String statuspage_changedby_columnheader = "@xpath=//span[@role='columnheader'][text()='Changed By']";
		public static final String statuspage_newstatusvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[1]";
		public static final String statuspage_changedonvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[2]";
		public static final String statuspage_changedbyvalue = "@xpath=(//span[@role='columnheader'][text()='Status']/ancestor::div[@role='row']/following-sibling::div//div[@role='gridcell'])[3]";
		public static final String statuspage_closebutton = "@xpath=(//div[@class='modal-content']//button[@type='button'])[1]";
		public static final String viewinterfacepage_header = "@xpath=//div[@class='modal-header']//div";
		public static final String viewinterfacepage_interfacesubheader = "@xpath=//div[@class='modal-body']//div[@class='heading-green-row row']/div";
		public static final String viewinterface_devicenamecolumnheader = "@xpath=(//div[@col-id='deviceName']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfacename_columnheader = "@xpath=(//div[@col-id='name']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfaceaddress_columnheader = "@xpath=(//div[@col-id='address']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfacetype_columnheader = "@xpath=(//div[@col-id='type.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String interfaceaddress_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='address'])[1]";
		public static final String interfacetype_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='type.desc'])[1]";
		public static final String viewinterface_status_columnheader = "@xpath=(//div[@col-id='currentStatus.desc']//div[@ref='eLabel'])[1]/span[1]";
		public static final String viewinterface_status_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='currentStatus.desc'])[1]";
		public static final String viewinterface_lastmod_columnheader = "@xpath=(//div[@col-id='m_time']//div[@ref='eLabel'])[1]/span[1]";
		public static final String viewinterface_lastmod_rowvalue = "@xpath=(//div[@role='row']//div[@role='gridcell'][@col-id='m_time'])[1]";
		public static final String viewinterface_closebutton = "@xpath=(//div[@class='modal-header']//button[@type='button']/span)[1]";
		public static final String statuspage_interfaceheader = "@xpath=(//div[@class='modal-header']//div)[2]";
		public static final String interface_statuspage_namefield = "@xpath=//label[text()='Name']";
		public static final String interface_statuspage_interfaceaddressfield = "@xpath=//label[text()='Interface Address']";
		public static final String interface_statuspage_currentstatusfield = "@xpath=//label[text()='Current Status']";
		public static final String interface_statuspage_newstatusfield = "@xpath=//label[text()='New Status']";
		public static final String interface_statuspage_namevalue = "@xpath=//label[text()='Name']/following-sibling::div";
		public static final String interface_statuspage_interfaceaddressvalue = "@xpath=//label[text()='Interface Address']/following-sibling::div";
		public static final String interface_statuspage_currentstatusvalue = "@xpath=//label[text()='Current Status']/following-sibling::div";
		public static final String interface_statuspage_newstatusdropdown = "@xpath=//label[text()='New Status']/parent::div//select";
		public static final String interface_statuspage_newstatusdropdownvalue = "@xpath=//label[text()='New Status']/parent::div//select/option";
		public static final String interface_statuspage_okbutton = "@xpath=//div[text()='Status']/parent::div/following-sibling::div[@class='div-margin row']//button[@type='button']//span";
		public static final String interface_statuspage_statuscolumnheader = "@xpath=(//div[@ref='eLabel']//span[text()='Status'])[2]";
		public static final String interface_statuspage_changedon_columnheader = "@xpath=//div[@ref='eLabel']//span[text()='Changed On']";
		public static final String interface_statuspage_changedby_columnheader = "@xpath=//div[@ref='eLabel']//span[text()='Changed By']";
		public static final String interface_statuspage_newstatusvalue = "@xpath=(//div[@role='gridcell'][@col-id='status.desc'])[1]";
		public static final String interface_statuspage_changedonvalue = "@xpath=(//div[@role='gridcell'][@col-id='changeDate'])[1]";
		public static final String interface_statuspage_changedbyvalue = "@xpath=(//div[@role='gridcell'][@col-id='user'])[1]";
		public static final String interface_statuspage_closebutton = "@xpath=(//div[@class='modal-header']//button[@type='button'])[2]";
		public static final String interface_statuspage_statusheader = "@xpath=(//div[@class='modal-body']//div[@class='heading-green-row row']//div)[2]";
		public static final String searchdevice_header = "@xpath=(//div[@class='heading-green-row row'])[1]//div";

		// !-- Existing device -- = "@xpath=
		public static final String typepename_dropdown = "@xpath=//label[text()='Type PE name to filter']/parent::div//div[contains(@class,'react-dropdown-select-content')]";
		public static final String existingdevice_vendormodelvalue = "@xpath=(//label[text()='Vendor/Model']/parent::div/div)[1]";
		public static final String existingdevice_managementaddressvalue = "@xpath=(//label[text()='Management Address']/parent::div/div)[1]";
		public static final String existingdevice_connectivityprotocol = "@xpath=(//label[text()='Connectiviy Protocol']/parent::div/div)[1]";
		public static final String existingdevice_snmpversion = "@xpath=(//label[text()='SNMP Version']/parent::div/div)[1]";
		public static final String existingdevice_country = "@xpath=(//label[text()='Country']/parent::div/div)[1]";
		public static final String existingdevice_city = "@xpath=(//label[text()='City']/parent::div/div)[1]";
		public static final String existingdevice_site = "@xpath=(//label[text()='Site']/parent::div/div)[1]";
		public static final String existingdevice_premise = "@xpath=(//label[text()='Premise']/parent::div/div)[1]";
		public static final String existingdevice_snmpro = "@xpath=(//label[text()='Snmpro']/parent::div/div)[1]";
		public static final String existingdevice_snmprw = "@xpath=(//label[text()='Snmprw']/parent::div/div)[1]";
		public static final String existingdevice_snmpv3username = "@xpath=(//label[text()='Snmp v3 Username']/parent::div/div)[1]";
		public static final String existingdevice_snmpv3authpassword = "@xpath=(//label[text()='Snmp v3 Auth password']/parent::div/div)[1]";
		public static final String existingdevice_snmpv3privpassword = "@xpath=(//label[text()='Snmp v3 Priv password']/parent::div/div)[1]";

		// !-- Router Tools Panel -- = "@xpath=
		public static final String commandIPV4_dropdown = "@xpath=//div[label[text()='Command IPV4']]//input";
		public static final String commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.ipAddress']";
		public static final String commandIPv4_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[1]";

		public static final String commandIPV6_dropdown = "@xpath=//div[label[text()='Command IPV6']]//input";
		public static final String commandIPv6_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String commandIPv6_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[2]";

		public static final String executebutton_Ipv4 = "@xpath=(//span[text()='Execute'])[1]";
		public static final String executebutton_IPv6 = "@xpath=(//span[text()='Execute'])[2]";

		public static final String result_textArea = "@xpath=//div[label[text()='Result']]//textarea";
		public static final String routertools_header = "@xpath=//div[text()='Router Tools']";

		// !-- Add Interface -- = "@xpath=
		public static final String interfaces_header = "@xpath=//div[text()='Interfaces']";
		public static final String interfacepanel_actiondropdown = "@xpath=//div[div[text()='Interfaces']]/following-sibling::div//button[@id='dropdown-basic-button']";
		public static final String addinterface_link = "@xpath=//a[text()='Add Interface/Link']";
		public static final String editinterface_link = "@xpath=//a[text()='Edit']";
		public static final String addinterface_header = "@xpath=//div[text()='Add']";
		public static final String addmultilink_header = "@xpath=//div[text()='Add Multilink']";
		public static final String configureinterface_checkbox = "@xpath=//input[@id='configInterfaceOnDevice']";
		public static final String interface_warngmsg = "@xpath=//input[@id='interfaceName']/parent::div//div";
		public static final String bearertype_warngmsg = "@xpath=//div[label[contains(text(),'Bearer Type')]]/div";
		public static final String bandwidth_warngmsg = "@xpath=//div[label[contains(text(),'Bandwidth')]]/following-sibling::div";
		public static final String encapsulation_warngmsg = "@xpath=//div[label[contains(text(),'Encapsulation')]]/div";
		public static final String slot_warngmsg = "@xpath=//div[label[contains(text(),'Slot')]]/div";
		public static final String pic_warngmsg = "@xpath=//div[label[contains(text(),'Pic')]]/div";
		public static final String port_warngmsg = "@xpath=//div[label[contains(text(),'Port')]]/div";
		public static final String stm1number_warngmsg = "@xpath=//div[label[contains(text(),'STM1 Number')]]/div";
		// public static final String interface_warngmsg =
		// "@xpath=//input[@id='interfaceName']/following-sibling::div";
		public static final String interfacename_textfield = "@xpath=//div[label[text()='Interface']]//input";
		public static final String logicaltunnelinterface_field = "@xpath=//div[label[text()='Logical Tunel Interface [VCPE]']]//input";
		public static final String network_fieldvalue = "@xpath=//label[text()='Network']/parent::div//select";
		public static final String networkipv4_dropdown1 = "@xpath=(//label[text()='Network']/parent::div//select)[1]";
		public static final String networkipv4_dropdown2 = "@xpath=(//label[text()='Network']/parent::div//select)[2]";
		public static final String getaddress_button = "@xpath=(//button/span[text()='Get Address'])[1]";
		public static final String getaddressIPv6_button = "@xpath=(//button/span[text()='Get Address'])[2]";
		public static final String WANgetaddress_button = "@xpath=(//button/span[text()='Get Address'])[3]";
		public static final String WANgetaddressIPv6_button = "@xpath=(//button/span[text()='Get Address'])[4]";
		public static final String interfacerange_Address_dropdown = "@xpath=(//label[text()='Interface Address Range']/parent::div//select)[1]";
		public static final String interfacerange_AddressIpv6_dropdown = "@xpath=(//label[text()='Interface Address Range IPV6']/parent::div//input)[1]";
		public static final String WANinterfacerange_address_dropdown = "@xpath=//label[text()='Wan Interface Address Range']/parent::div//select";
		public static final String WANinterfacerange_addressIPv6_dropdown = "@xpath=//label[text()='Wan Interface Address Range IPv6']/parent::div//select";
		public static final String interfaceaddress_Addarrow = "@xpath=(//button[@type='button']/span[text()='>>'])[1]";
		public static final String interfaceaddressIPv6_Addarrow = "@xpath=(//button[@type='button']/span[text()='>>'])[4]";
		public static final String WANinterfaceaddress_Addarrow = "@xpath=(//button[@type='button']/span[text()='>>'])[3]";
		public static final String WANinterfaceaddressIPv6_Addarrow = "@xpath=(//button[@type='button']/span[text()='>>'])[4]";
		public static final String eipallocation_button = "@xpath=(//button[@type='button']/span[text()='EIP Allocation'])[1]";
		public static final String eipallocationIPv6_button = "@xpath=(//button[@type='button']/span[text()='EIP Allocation'])[2]";
		public static final String WANeipallocation_button = "@xpath=(//button[@type='button']/span[text()='EIP Allocation'])[3]";
		public static final String WANeipallocationIPv6_button = "@xpath=(//button[@type='button']/span[text()='EIP Allocation'])[4]";
		public static final String interfaceaddressrange_textfield = "@xpath=(//div[label[text()='Interface Address Range']]//input)[1]";
		public static final String address_textfield = "@xpath=(//div[label[text()='Address']]//input)[1]";
		public static final String interfaceaddressrangeIPv6_textfield = "@xpath=(//label[text()='Interface Address Range IPV6']/following-sibling::input)[1]";
		public static final String addressIPv6_textfield = "@xpath=(//div[label[text()='Address IPv6']]//input)[1]";
		public static final String WANinterfaceaddressrange_textfield = "@xpath=//div[label[text()='Wan Interface Address Range']]//input";
		public static final String WANinterfaceaddressrangeIPv6_textfield = "@xpath=//div[label[text()='Wan Interface Address Range IPv6']]//input";
		public static final String addressVCPE_textfield = "@xpath=//div[label[text()='Address[VCPE]']]//input";
		public static final String addressPE_textfield = "@xpath=//div[label[text()='Address[PE]']]//input";
		public static final String addressIPv6VCPE_textfield = "@xpath=//div[label[text()='AddressIpv6[VCPE]']]//input";
		public static final String addressipv6PE_textfield = "@xpath=//div[label[text()='AddressIpv6[PE]']]//input";

		public static final String networkipv6_fieldvalue = "@xpath=//input[@id='networkIPV6']";
		public static final String link_textfield = "@xpath=//input[@id='link']";
		public static final String bearertype_dropdown = "@xpath=//label[text()='Bearer Type']/parent::div//select";
		public static final String bandwidth_dropdown = "@xpath=//label[text()='Bandwidth']/parent::div//select";
		public static final String encapsulation_dropdown = "@xpath=//label[text()='Encapsulation']/parent::div//select";
		public static final String cardtype_dropdown = "@xpath=//label[text()='Card Type']/parent::div//select";
		public static final String ciscovendor_interfaceaddressrange_textfield = "@xpath=//input[@name='interfaceAddressRange']";
		// public static final String interfaceaddressrange_textfield =
		// "@xpath=//input[@id='interfaceAddressRange']";
		// public static final String interfaceaddressrangeIPv6_textfield =
		// "@xpath=//label[text()='Interface Address Range
		// IPV6']/following-sibling::input";
		public static final String eipsubnetallocation_header = "@xpath=//div[text()='EIP Subnet Allocation']";
		public static final String subnettype_value = "@xpath=//label[text()='Subnet Type ']/parent::div/following-sibling::div";
		public static final String eipallocation_citydropdown = "@xpath=//label[text()='City']/parent::div//input";
		public static final String eipallocation_subnetsize = "@xpath=//label[text()='Sub Net Size']/parent::div//input";
		public static final String eipallocation_availablepools_value = "@xpath=//label[text()='Available Pools ']/parent::div/following-sibling::div";
		public static final String allocatesubnet_button = "@xpath=//span[text()='Allocate Subnet']";
		public static final String eipallocationIPv6_header = "@xpath=//div[contains(text(),'EIP Address Allocation For Interface  ')]";
		public static final String eipallocation_spacename = "@xpath=//label[text()=' Space Name ']/parent::div/following-sibling::div";
		public static final String eipallocation_ipv6_subnetsize = "@xpath=//label[text()='Sub Net Size']/parent::div//select";
		public static final String availableblocks_dropdown = "@xpath=//label[text()='Available Blocks']/parent::div//select";
		public static final String bgp_checkbox = "@xpath=//input[@id='bgp']";
		public static final String configuration_header = "@xpath=//div[text()='Configuration']";
		public static final String generateconfiguration_button = "@xpath=//button/span[text()='Generate Configuration']";
		public static final String configAlert_okbutton = "@xpath=//div[@class='modal-content']//button[text()='OK']";
		public static final String configuration_textarea = "@xpath=//label[text()='Configuration']/following-sibling::textarea";
		public static final String saveconfiguration_button = "@xpath=//span[text()='Save Configuration to File']";
		public static final String executeandsave_button = "@xpath=//span[text()='Execute and Save']";
		public static final String framingtype_dropdown = "@xpath=//label[text()='Framing Type']/parent::div//select";
		public static final String vlanid_textfield = "@xpath=//input[@id='VLANId']";
		public static final String unitid_textfield = "@xpath=//label[text()='Unit ID']/parent::div//input";
		public static final String slot_textfield = "@xpath=//label[text()='Slot']/parent::div//input";
		public static final String pic_textfield = "@xpath=//label[text()='Pic']/parent::div//input";
		public static final String port_textfield = "@xpath=//label[text()='Port']/parent::div//input";
		public static final String clocksource_dropdown = "@xpath=//label[text()='Clock Source']/parent::div//select";
		public static final String interface_bandwidthvalue = "@xpath=//label[text()='Bandwidth']/following-sibling::div";
		public static final String bearerno_textfield = "@xpath=//label[text()='Bearer No']/parent::div//input";
		public static final String STM1Number_textfield = "@xpath=//label[text()='STM1 Number']/parent::div//input";
		public static final String ivmanagement_checkbox = "@xpath=//label[text()='IV Management']/parent::div//input";
		public static final String mdnameformat_field = "@xpath=//label[text()='MD Name Format']/parent::div//input";
		public static final String domainlevel_dropdown = "@xpath=//label[text()='Domain Level']/parent::div/select";
		public static final String standardethernetOAM_label = "@xpath=//label[text()='Standard EthernetOAM']";
		public static final String atricaconnected_checkbox = "@xpath=//input[@id='atricaConnected']";
		public static final String bgptemplate_dropdown = "@xpath=//label[text()='BGP Templates Generate For']/parent::div//select";
		public static final String cpewan_textfield = "@xpath=//input[@id='cpeWAN']";
		public static final String cpewanipv6_textfield = "@xpath=//input[@id='cpeWANIpv6Address']";
		// public static final String bgp-descriptionfield =
		// "@xpath=//input[@id='description']";
		public static final String bgp_ascustomerfield = "@xpath=//input[@id='asCustomer']";
		public static final String bgppassword_field = "@xpath=//input[@id='bgpPassword']";
		public static final String interface_column = "@xpath=//span[text()='Interface']";
		public static final String link_column = "@xpath=//span[text()='Link/Circuit Id']";
		public static final String interfaceaddressrange_column = "@xpath=//span[text()='Interface Address Range']";
		public static final String interfaceaddress_column = "@xpath=//span[text()='Interface Address']";
		public static final String bearertype_column = "@xpath=//span[text()='Bearer Type']";
		public static final String bandwidth_column = "@xpath=//span[text()='Bandwidth']";
		public static final String vlanid_column = "@xpath=//span[text()='VLAN Id']";
		public static final String ifinoctets_column = "@xpath=//span[text()='IfInOctets']";
		public static final String interfacename_value = "@xpath=//div[@role='gridcell'][@col-id='interfaceName']";
		public static final String link_value = "@xpath=//div[@role='gridcell'][@col-id='linkId']";
		public static final String interfaceaddressrange_value = "@xpath=//div[@role='gridcell'][@col-id='interfaceAddressRange']";
		public static final String interfaceaddress_value = "@xpath=//div[@role='gridcell'][@col-id='interfaceAddress']";
		public static final String bearertype_value = "@xpath=//div[@role='gridcell'][@col-id='bearerType']";
		public static final String bandwidth_value = "@xpath=//div[@role='gridcell'][@col-id='bandWidth']";
		public static final String vlanid_value = "@xpath=//div[@role='gridcell'][@col-id='vlanId']";
		public static final String ifinoctets_value = "@xpath=//div[@role='gridcell'][@col-id='ifInOctets']";
		// public static final String interfaces_header =
		// "@xpath=//div[text()='Interfaces']";
		public static final String showinterfaces_link = "@xpath=//a[text()='Show Interfaces']";
		public static final String editinterface_header = "@xpath=//div[@class='heading-green-row row']//div[text()='Edit']";
		public static final String ipsubnetipv6_addbutton = "@xpath=//label[text()='IP Subnet IPv6']/parent::div/parent::div/following-sibling::div//span[text()='Add']";
		public static final String ipsubnetipv4_addbutton = "@xpath=//label[text()='IP Subnet IPv4']/parent::div/parent::div/following-sibling::div//span[text()='Add']";
		public static final String ipsubnetipv4_textfield = "@xpath=//label[text()='IP Subnet IPv4']/following-sibling::input";
		public static final String ipsubnetipv6_textfield = "@xpath=//label[text()='IP Subnet IPv6']/following-sibling::input";
		public static final String ipsubnetipv6_removebutton = "@xpath=//label[text()='IP Subnet IPv6']/parent::div/parent::div/following-sibling::div//span[text()='Remove']";
		public static final String ipsubnetipv4_removebutton = "@xpath=//label[text()='IP Subnet IPv4']/parent::div/parent::div/following-sibling::div//span[text()='Remove']";
		public static final String multilink_text = "@xpath=//div[text()='Multilink']";
		public static final String multilinkedbearers_header = "@xpath=//div[text()='Multilinked Bearers']";
		public static final String checktoaddinterface_column = "@xpath=//span[@role='columnheader'][text()='Check to add Interface']";
		public static final String multilink_interface_column = "@xpath=//span[@role='columnheader'][text()='Interface']";
		public static final String multilink_link_column = "@xpath=//span[@role='columnheader'][text()='Link/Circuit']";
		public static final String multilink_BearerType_column = "@xpath=//span[@role='columnheader'][text()='Bearer Type']";
		public static final String multilink_bandwidth_column = "@xpath=//span[@role='columnheader'][text()='Bandwidth']";
		public static final String multilink_vlanid_column = "@xpath=//span[@role='columnheader'][text()='VLAN Id']";
		public static final String multilink_ifinoctets_column = "@xpath=//span[@role='columnheader'][text()='lflnOctets']";
		public static final String addedinterfaces = "@xpath=//div[text()='Interfaces']/parent::div/following-sibling::div//div[@ref='eBodyContainer']";
		public static final String interface_rowid = "@xpath=//div[@col-id='interfaceName'][text()='value']/parent::div";
		public static final String interfacename_tablevalue = "@xpath=//div[@row-id='value']/div[@col-id='interfaceName']";
		public static final String link_tablevalue = "@xpath=//div[@row-id='value']//div[@col-id='linkCircuit']";
		public static final String interfaceaddressrange_tablevalue = "@xpath=//div[@row-id='value']//div[@col-id='addressRange']";
		public static final String interfaceaddress_tablevalue = "@xpath=//div[@row-id='value']//div[@col-id='address']";
		public static final String bearertype_tablevalue = "@xpath=//div[@row-id='value']//div[@col-id='bearerType']";
		public static final String bandwidth_tablevalue = "@xpath=//div[@row-id='value']//div[@col-id='bandwidthDesc']";
		public static final String vlanid_tablevalue = "@xpath=//div[@row-id='value']//div[@col-id='vlanId']";
		public static final String ifinoctets_tablevalue = "@xpath=//div[@row-id='value']//div[@col-id='ifInOctets']";
		public static final String selectinterface = "@xpath=//div[@role='gridcell'][@col-id='interfaceName'][text()='value']/preceding-sibling::div//span[contains(@class,'unchecked')]";
		public static final String multilink_bandwidth_dropdown = "@xpath=//label[text()='Bandwidth']/parent::div//select";
		public static final String multilink_bgptemplate_dropdown = "@xpath=//label[text()='BGP Templates Generate For']/parent::div//input";
		public static final String eipallocation_popupclose = "@xpath=//div[text()='EIP Address Allocation For Interface  ']/following-sibling::button/span[text()='�']";

		// !-- Select Interfaces -- = "@xpath=
		public static final String labelname_managementAddresss = "@xpath=//label[text()='Management Address']";
		public static final String InterfaceInselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceInselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceInselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceInselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceInselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";
		public static final String InterfaceInselect_removebuton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";
		public static final String InterfaceToselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";
		public static final String InterfaceToselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";
		public static final String InterfaceToselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";
		public static final String InterfaceToselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//button[text()='Action']";
		public static final String InterfaceToselect_addbuton = "@xpath=//div[div[text()='Interfaces to Select']]//a[contains(text(),'Add')]";
		public static final String InterfaceToselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";
		public static final String InterfaceToselect_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String selectinterface_link = "@xpath=//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'value')]]]//span[text()='Select Interfaces']";
		public static final String interfacesinservice_list = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String interfacesToSelect_header = "@xpath=//div[text()='Interfaces to Select']";
		public static final String InteraceColumn_Filter = "@xpath=(//div[div[text()='Interfaces to Select']]//following-sibling::div//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String InterfacefilterTxt = "@xpath=//div[text()='Interfaces to Select']/parent::div/parent::div//div[@class='ag-menu']//div[@class='ag-filter-body']//input[@id='filterText']";
		public static final String interface_gridselect = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='value']";
		public static final String InterfaceToSelect_interfaceColumnHeader = "@xpath=//div[div[text()='Interfaces to Select']]//following-sibling::div//span[text()='Interface']";
		public static final String InterfacetoSelect_listOFinterfaceValuesDisplaying = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String interfacesinservice_filter = "@xpath=(//div[div[text()='Interfaces in Service']]//following-sibling::div//span[@class='ag-icon ag-icon-menu'])[2]";
		public static final String interfaceinservice_fitertext = "@xpath=//div[text()='Interfaces in Service']/parent::div/parent::div//div[@class='ag-menu']//div[@class='ag-filter-body']//input[@id='filterText']";
		public static final String interfaceinservice_gridselect = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[@role='gridcell']]//div[text()='value']";
		public static final String InterfaceInService_panelHeader = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]";

		// !-- Interface Config history panel -- = "@xpath=
		public static final String interfaceconfighistory_header = "@xpath=//div[text()='Interface Configuration History']";
		public static final String date_column = "@xpath=//span[@role='columnheader'][text()='Date']";
		public static final String filename_column = "@xpath=//span[@role='columnheader'][text()='File Name']";

		// !-- Multilink -- = "@xpath=
		public static final String multilink_rowid = "@xpath=//div[@col-id='interfaceName'][text()='value']/parent::div";
		public static final String multilinkbearer_tabledata = "@xpath=//div[text()='Multilinked Bearers']/parent::div/following-sibling::div//div[@ref='eBodyContainer']";
		public static final String checktoaddinterface = "@xpath=//div[@col-id='interfaceName'][text()='value']";

		public static final String breadcrumb = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'value')]";
		public static final String breadcrumb1 = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'";
		public static final String breadcrumb2 = "')]";

		public static final String errormessage = "@xpath=//div[contains(@class,'alert-danger')]";

		// !-- Actelis Configuration -- = "@xpath=
		public static final String configure_totalPage = "@xpath=//span[@ref='lbTotal']";
		public static final String configure_currentpage = "@xpath=//span[@ref='lbCurrent']";
		public static final String Actelisconfig_addDSLAM = "@xpath=//div[div[div[text()='Actelis Configuration']]]//a[text()='Add DSLAM and HSL']";
		public static final String AddDSLM_header = "@xpath=//div[text()='Add DSLAM and HSL']";
		public static final String DSLM_Device_Select = "@xpath=//div[div[div[label[@class='form-label']]]]//input";
		public static final String List_HSL_Link = "@xpath=//span[contains(text(),'List HSL')]";
		public static final String ActelisConfigurationPanel = "@xpath=//div[div[contains(text(),'Actelis Configuration')]]";
		public static final String actelis_EquipConfig_viewLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='View']";
		public static final String actelis_EquipConfig_deleteLink = "@xpath=//div[div[text()='Equipment Configuration']]//following-sibling::div//span[text()='Delete']";
		public static final String addDSLAMandHSL_xButton = "@xpath=//div[div[label[text()='DSLAM Device:']]]//following-sibling::div//div[text()='�']";
		public static final String selectDSLAMdeviceValue = "@xpath=//div[text()='value']";
		public static final String InterfaceToSelect_actelis_totalpage = "@xpath=//span[@ref='lbTotal']";
		public static final String InterfaceToSelect_actelis_currentpage = "@xpath=//span[@ref='lbCurrent']";
		public static final String showInterface_ActelisCnfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//a[text()='Show Interfaces']";
		public static final String AcionButton_ActelisConfiguration = "@xpath=//div[div[text()='Actelis Configuration']]/following-sibling::div//button[text()='Action']";
		public static final String removeButton_ActelisConfiguration = "@xpath=//a[text()='Remove']";
		public static final String popupMessage_forRemove_ActelisConfiguration = "@xpath=//div[@class='modal-body']";
		public static final String deleteInterface_successMessage_ActelisCOnfiguration = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";
		public static final String successMessage_ActelisConfiguration_removeInterface = "@xpath=//span[contains(text(),'HSL Interface successfully removed from service')]";

		// !-- success Message -- = "@xpath=
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

	}
}
