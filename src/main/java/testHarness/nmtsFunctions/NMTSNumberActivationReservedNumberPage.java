package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.GridHelper;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSMergeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationReservedNumberObj;
import pageObjects.nmtsObjects.NMTSNumberDeactivationObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSNumberActivationReservedNumberPage extends SeleniumUtils {
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSNumberActivationFreeNumberPage actToFree= new NMTSNumberActivationFreeNumberPage();
	NMTSNumberReservationPage NumberReservation = new NMTSNumberReservationPage();
	GridHelper Helper = new GridHelper();
	
	public void ReserveToActivateScenario(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String MadeByName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MADE_BY_NAME");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String FilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_By");
		String MatchingWithFilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Matching_With_FilterBy");
	//	String FilterTex = NumberReservation.getvalue("NMTS_ID");
		String ReservedNumber_OR_NMTSID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ReservedNumber/NMTS_ID");

		String FilterText = NumberReservation.getvalue("NMTS_ID");

		
		
		NumberReservation.UserGroupforPhnNoMgn(testDataFile, sheetName, scriptNo, dataSetNo);
		Reusable.switchtodefault();
		waitForAjax();
		Reusable.switchtoframe("fmMenu");		
		waitForAjax();
		waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.ProcessReservationtab,15);
		verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.ProcessReservationtab,"ProcessReservationtab");
		javaScriptclick(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.ProcessReservationtab,"ProcessReservationtab");
		WaitForAjax();
		Reusable.switchtodefault();
	    WaitForAjax();
		Reusable.switchtoframe("fmMain");
		WaitForAjax();
		if(Country.toString().equals("IT") || Country.toString().equals("FR"))
		{
			verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SelectReservationColt,"Colt");
			Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SelectReservationColt,"Colt");
			
		}
		waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.madeByNMTS,15);
		verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.madeByNMTS,"made By");
		Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.madeByNMTS,MadeByName);
		waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.Availablity,10);
		verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.Availablity,"Availablity");
		Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.Availablity,Availablity);
		verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterBy,"Filter By");
		
		Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterBy,FilterBy);
		waitForAjax(); 
		verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.matching,"Matching With Filter By");
		Reusable.Select(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.matching,MatchingWithFilterBy);
		waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterText,15);
		verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterText,"Filter input");
		sendKeys(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.FilterText,ReservedNumber_OR_NMTSID,"Filter input data");
		
		waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SubmitSearchbtn,15);
		verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SubmitSearchbtn,"Submit Deactivate Button");
		javaScriptclick(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.SubmitSearchbtn,"Submit Deactivate Button");
		waitForAjax();
		
		if(isElementPresent(By.xpath("//table[@id='ucxOrderHistory_grdView']//tr/td[contains(text(),'No Record Found.')]")))
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No record found please enter the valid data number");	
	
		} 
		else {
			
			if(isElementPresent(By.xpath("(//table[@id='ucxOrderHistory_grdView']//tr)[2]//td[text()='Reserved']")))
			{
				waitForElementToBeVisible(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.OrderId,15);
				verifyExists(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.OrderId,"OrderId");
				javaScriptDoubleclick(NMTSNumberActivationReservedNumberObj.NumberActivationReserveNumber.OrderId,"OCN double click");
				
	//it will perform Allocate first then Activate
				actToFree.AllocationPerform(testDataFile,sheetName,scriptNo,dataSetNo);
				waitForAjax();
				actToFree.ActivatePerform_ReservedToActivate(testDataFile,sheetName,scriptNo,dataSetNo);
				
				}
			else {
				
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Please currect the Number from the Excel sheet . it seems like number is not in Reserve status");
				
			}
		
        }
	
    }
}
	
	
	
	
	
	
	
	
	
	
	
	
	
 

	
	
	
	

