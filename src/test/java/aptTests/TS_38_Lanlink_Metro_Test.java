package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.Lanlink_Metro;


public class TS_38_Lanlink_Metro_Test extends SeleniumUtils {

	APT_Login Login = new APT_Login();
	Lanlink_Metro Metro = new Lanlink_Metro();

	Properties prop = new Properties();

	public String deviceName_Equip = null;
	public String devicename_IntEquipment = null;
	public String siteOrderValue = null;

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void LANLINK_OLO(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName) throws Exception {

		Report.createTestCaseReportHeader(ScenarioName);

		try (InputStream input = new FileInputStream(g.getRelativePath() + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();

		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			String CustomerName1 = null;
			String devicename_intEquip = null;
			String manageAdres_intEquip = null;
			String ServiceID = null;
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");

			String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"serviceNumber");
			String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_serviceNumber");
			String devicename_IntEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"devicename_IntEquipment");
			String Interfacename_forEditInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "Interfacename_forEditInterface");
			String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RemoveInterface_Selection");
			String Siteordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Siteordernumber");
			String siteOrderNumber_10G_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "siteOrderNumber_10G_PointToPoint");
			String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"siteOrderNumber_PointToPoint");
			String siteOrderNumber_p2p_mspSelected = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "siteOrderNumber_p2p_mspSelected");
			String ServiceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");

			String CommandIPv4_Routertool = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"CommandIPv4_Routertool");
			// String manageAdres_intEquip=DataMiner.fngetcolvalue(testDataFile,
			// dataSheet, scriptNo, dataSetNo,"manageAdres_intEquip");
			String EDIT_Intequip_cpe_deviecname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"EDIT_Intequip_cpe_deviecname");
			String device_intEquip_name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"device_intEquip_name");
			String intEquip_existingDeviceValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"intEquip_existingDeviceValue");

			String Modularmsp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");
			String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"AddInterface_Selection");
			String deviceCreation_IntermediateEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "AddInterface_Selection");
			String speed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
			String ModularMSp = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Modularmsp");
			String existingDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"IntEquip_existingdeviceSelection");
			String newDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"IntEquip_newdeviceSelection");
			String existingDeviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"intEquip_existingDeviceValue");
			String vpnTopology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vpnTopology");
			String circuitType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CircuitType");
			String CommandIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"CommandIPv4_Routertool");
			String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Interfacespeed");
			String deviceCreation_Equipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"deviceCreation_Equipment");
			String Technologyname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "technology");
			String manageAdres = null;
			String vendorModel = null;
			String devicename = null;
			String sitePreferenceType = "Null";
			String siteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"siteOrder_sitePreferenceType");
			String editSiteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "editSiteOrder_sitePreferenceType");

			String Equip_existingDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Equip_existingDevicename");
			String devicename_equip = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"devicename_equip");
			String EDIT_cpename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EDIT_cpename");

			String existingDevice_intEquip = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"IntEquip_existingdeviceSelection");
			String newDevice_intEquip = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"IntEquip_newdeviceSelection");

			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {

				// CreateNewCustomer_Lanlink_Metro");
				Metro.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

				// selectExistingCustomer_Metro");
				Metro.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {

				// selectExistingCustomer_Lanlink_Metro");
				Metro.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

			}

			//verifyListofFieldsForOrderandServicetype_LANLINK_National");
			Metro.Verifyfields(testDataFile, dataSheet, scriptNo, dataSetNo);
			//	Metro.selectCustomertocreateOrderfromleftpane(CustomerName1);
			Metro.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Metro.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			Metro.selectServiceType(ServiceType);
				
			// selectTheServiceType_LanlinkMetro");
			Metro.selectsubtypeunderServiceTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifyFieldsFortheSubServicetypeselelcted_LANLINKmetro");
			Metro.VerifyFieldsForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// enterdataforServiceCreation_LANLINKmetro");
			Metro.selectOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Metro.selectServiceType(ServiceType);
			Metro.selectsubtypeunderServiceTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);
			Metro.dataToBeEnteredOncesubservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// successmessageforServicecreation_LANLINKmetro");
			Metro.verifysuccessmessage("Service successfully created");

			
			// verifyOrderDetailsInformation_LANLINKmetro");
			Metro.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Metro.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			// verifydataentered_LANLINKmetro");
			Metro.VerifydatenteredForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// editServiceSubtypes_LANLINKmetro");
			Metro.EditTheservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);

			// successmessageforServiceUpdation_LANLINKmetro");
			Metro.verifysuccessmessage("Service successfully updated.");

			String proactiveMonitorvalue = Metro.fetchProActiveMonitoringValue();

			// Synchronize_LANLINKmetro");
			Metro.syncservices();

			// ManageSubnets_LANLINKmetro");
			Metro.manageSubnets();

			// Dump_LANLINKmetro");
			Metro.dump_viewServicepage();

			/**
			 * Site Order
			 */
			// AddSiteOrder_LANLINKmetro");

			if ((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit"))) {

				Report.LogInfo("Info",
						"Site order' Page will not display, if we select 'VPN TOpology' as 'Point-to-Point' "
								+ "and 'Circuit Type' as 'Composite Circuit' ",
						"Info");
				ExtentTestManager.getTest();
			} else {
				Report.LogInfo("Info", "Verifying 'Add Site Order' fields", "Info");
				Metro.Enteraddsiteorder();
				Metro.verifyAddsiteorderFields(testDataFile, dataSheet, scriptNo, dataSetNo);

				Report.LogInfo("Info", "create site Order", "Info");
				Metro.Enteraddsiteorder();
				Metro.addsiteorder(testDataFile, dataSheet, scriptNo, dataSetNo);

				// Successmessage_CreationofSiteOrder_LANLINKmetro");
				Metro.verifysuccessmessage("Site order created successfully");

				// verifytheDataEnteredForSiteOrder_LANLINKmetro");
				Metro.VerifyDataEnteredForSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

				// EditSiteOrder_LANLINKmetro");
				Metro.returnbacktoviewsiteorderpage();
				// Metro.selectRowForsiteorder();
				Metro.editSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				Metro.verifysuccessmessage("Site Order successfully updated.");

			}

			// For Device, Circuit creation

			if (editSiteOrder_sitePreferenceType.equalsIgnoreCase("Null")) {
				sitePreferenceType = siteOrder_sitePreferenceType;
			} else {
				sitePreferenceType = editSiteOrder_sitePreferenceType;
			}

			if (((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit")))
					|| ((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Extended Circuit"))
							&& (sitePreferenceType.equalsIgnoreCase("Circuit")))) {

				if (Edit_serviceNumber.equalsIgnoreCase("null")) {
					ServiceID = serviceNumber;
				} else {
					ServiceID = Edit_serviceNumber;
				}

				if (Interfacespeed.equals("1GigE")) {

					// Overture
					// createCircuitFor1G_Overture_LanlinkMetro");
					Metro.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.verifysuccessmessage("Circuit successfully created");
					Metro.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Metro.deleteCircuit();

					// Accedian-1G
					// createCircuitFor1G_Accedian1GLanlinkMetro");
					Metro.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.verifysuccessmessage("Circuit successfully created");
					Metro.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Metro.deleteCircuit();

					// Atrica
					// createCircuitFor1G_Atricas_LanlinkMetro");
					Metro.addAtricaCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.verifysuccessmessage("Circuit successfully created");
					Metro.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Metro.deleteCircuit();

				}

				else if (Interfacespeed.equals("10GigE")) {
					// createCircuitFor10G_LanlinkMetro");
					// Overture
					Metro.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.verifysuccessmessage("Circuit successfully created");
					Metro.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Metro.deleteCircuit();

				}
			} else {
				// Actelis_EqiupmentConfiguration_LANLINKmetro");

				if (Technologyname.equalsIgnoreCase("Actelis")) {
					boolean equipConfigurationPanel = Metro.EquipmentCOnfigurationPanel();
					if (equipConfigurationPanel) {
						Report.LogInfo("Info", "Add Actelis CPE device", "Info");
						Metro.equipConfiguration_Actelis_AddDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
						Metro.verifysuccessmessage("Device successfully created");

						// verifyEnteredValues_Actelis_LanlinkMetro");
						Metro.verifyDataEnteredFordeviceCreation_Actelis(testDataFile, dataSheet, scriptNo, dataSetNo);
						Metro.returnbacktoviewsiteorderpage();

						// deleteDevice_Actelis_LanlinMetro");
						Metro.deleteDeviceFromService_EquipmentConfig_Actelis();
						Metro.verifysuccessmessage("Actelis CPE Device successfully deleted and removed from service");

					} else {
						Report.LogInfo("Info", "'Equipment Configuration' panel is not displaying", "FAIL");

					}
					// Actelis Configuration panel
					// Actelis_AddDSLAMandHSL_LANLINKmetro");
					Metro.verifyAddDSLAMandHSLlink();
					Metro.AddDSLAMandHSL(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.showInterface_ActelisConfiguuration();
					Metro.deletInterface_ActelisConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					Metro.successMessage_deleteInterfaceFromDevice_ActelisConfiguration();

				} else {
					Report.LogInfo("Info",
							"Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page",
							"Info");
					Reporter.log(
							"Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page");

				}

				/**
				 * Device creation _ Equipment
				 */
				// AdddeviceforEquipment_LANLINKmetro");

				// verify whether Equipment panel is available
				boolean EquipmentPanel = Metro.findPanelHeader("Equipment");
				if (EquipmentPanel) {
					// Verify whether Equipment device to be created
					if (deviceCreation_Equipment.equalsIgnoreCase("yes")) {
						Report.LogInfo("Info", " Device to be created for Eqiupment as per input provided", "Info");
						Report.LogInfo("Info", "Under Equipement, list of actions to be performed are: "
								+ "Verify fields for Add device" + "Add device" + "Verify entered values for device"
								+ "Edit device" + "Select Interface" + "Configure Interface -- Edit Inteface"
								+ "show/Hide Interface -- Edit Interface"
								+ "Select Interface -- Add Interface to service , Remove Interface from Service"
								+ "Delete device ", "Info");

						Reporter.log(
								"------For Equipment ..........Entering add cpe device, Verify CPE device, Edit CPE device----------");

						if (ModularMSp.equalsIgnoreCase("Yes")) {
							if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {

								Report.LogInfo("Info", "AddExistingDevice_MSPselected_Equipment", "Info");
								Metro.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
										dataSheet, scriptNo, dataSetNo, speed);
								Metro.verifysuccessmessage("Device successfully created");
								Metro.verifyValuesforCPEexistingdevice_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								// editExistingDevice_MSPselected_Equipment_LanlinkMetro");
								Metro.eDITCPEdevicedetailsentered_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								Metro.verifysuccessmessage("Device successfully updated");

							} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {

								Report.LogInfo("Info", "AddnewDevice_MSPselected_Equipment", "Info");
								Metro.verifyFieldsandAddCPEdevicefortheserviceselected_MSPselected(testDataFile,
										dataSheet, scriptNo, dataSetNo);
								Metro.verifysuccessmessage("Device successfully created");

								// verifyEnteredValue_MSPselected_Equipment_LanlinkMetro");
								Metro.verifydetailsEnteredforCPEdevice_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								// editNewDevice_MSPselected_Equipment_LanlinkMetro");
								Metro.eDITCPEdevicedetailsentered_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								Metro.verifysuccessmessage("Device successfully updated");

							}
						} else {
							if (speed.equals("1GigE")) {
								if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {

									Report.LogInfo("Info", "addExistingDevice_1G_Eqiupment", "Info");
									Metro.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(
											testDataFile, dataSheet, scriptNo, dataSetNo, speed);
									Metro.verifysuccessmessage("Device successfully created");
									Metro.verifyValuesforCPEexistingdevice_1G_Equipment(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editExistingDevice_1G_Eqiupment_LanlinkMetro");
									Metro.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								}

								else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {

									Report.LogInfo("Info", "addNewDevice_1G_Eqiupment", "Info");
									Metro.verifyFieldsandAddCPEdevicefortheserviceselected_1G(testDataFile, dataSheet,
											scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_1G_Equipment_LanlinkMetro");
									Metro.verifydetailsEnteredforCPEdevice_1G(testDataFile, dataSheet, scriptNo,
											dataSetNo);

									// editNewDevice_1G_Equipment_LanlinkMetro");
									Metro.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								}
							} else if (speed.equals("10GigE")) {
								if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {

									Report.LogInfo("Info", "addExistingDevice_10G_Equipment", "Info");
									Metro.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(
											testDataFile, dataSheet, scriptNo, dataSetNo, speed);
									Metro.verifysuccessmessage("Device successfully created");
									Metro.verifyValuesforCPEexistingdevice_10G_Equipment(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editExistingDevice_10G_Equipment_LanlinkMetro");
									Metro.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {

									Report.LogInfo("Info", "addNewDevice_10G_Equipment", "Info");
									Metro.verifyFieldsandAddCPEdevicefortheserviceselected_10G(testDataFile, dataSheet,
											scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_10G_Equipment_LanlinkMetro");
									Metro.verifydetailsEnteredforCPEdevice_10G(testDataFile, dataSheet, scriptNo,
											dataSetNo);

									// editNewDevice_10G_Equipment_LanlinkMetro");
									Metro.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								}
							}
						}

						// get Device name
						if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							if (EDIT_cpename.equalsIgnoreCase("null")) {
								devicename = Equip_existingDevicename;
							} else if (!EDIT_cpename.equalsIgnoreCase("null")) {
								devicename = EDIT_cpename;
							}
						} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
							if (EDIT_cpename.equalsIgnoreCase("null")) {
								devicename = devicename_equip;
							} else if (!EDIT_cpename.equalsIgnoreCase("null")) {
								devicename = EDIT_cpename;
							}
						}

						// Navigate to view device page
						Metro.Equip_clickonviewButton(devicename);
						devicename = Metro.fetchdevicename_InviewPage(); // Device
																			// Name
						manageAdres = Metro.fetchManagementAddressValue_InviewPage(); // Management
																						// Address
						vendorModel = Metro.fetchVendorModelValue(); // Vendor/Model

						// Perform fetch from Interface
						// FetchDeviceInterface_Equip_LanlinkMetro");
						//Metro.testStatus();
						boolean link = Metro.fetchDeviceInterface_viewdevicepage();
						waitForAjax();

						// routerTools_Equipment_LanlinkMetro");
						Metro.routerPanel(CommandIPv4, manageAdres);

						// configureEquipment_LanlinkMetro");
						String devicename_EquipActual = null;

						devicename = Metro.fetchdevicename_InviewPage();
						if (devicename.contains("...")) {
							devicename_EquipActual = devicename.substring(0, 10);
						} else {
							devicename_EquipActual = devicename;
						}

						String siteOrderNumber = null;
						if (vpnTopology.equals("Point-to-Point")) {
							if (Modularmsp.equalsIgnoreCase("Yes")) {
								siteOrderNumber = siteOrderNumber_p2p_mspSelected;
							} else {
								if (Interfacespeed.equalsIgnoreCase("1GigE")) {
									siteOrderNumber = siteOrderNumber_PointToPoint;
								} else if (Interfacespeed.equalsIgnoreCase("10GigE")) {
									siteOrderNumber = siteOrderNumber_10G_PointToPoint;
								}
							}
						} else {
							siteOrderNumber = Siteordernumber;
						}

						if (ModularMSp.equalsIgnoreCase("Yes")) {
							Report.LogInfo("Info", "Configure link do not display, if 'MSP' selected", "Info");

						} else {
							Metro.clickOnBreadCrump(siteOrderNumber);
							Metro.selectconfigurelinkAndverifyEditInterfacefield__Equipment(devicename_EquipActual);
							String interfaceavailability_configure = Metro.EnterdataForEditInterfaceforConfigurelinkunderIntermediateEquipment(testDataFile,dataSheet, scriptNo, dataSetNo);
							if (interfaceavailability_configure.equalsIgnoreCase("Yes")) {
								Metro.verifyeditedinterfacevalue(testDataFile, dataSheet, scriptNo, dataSetNo);
							}

						}

						// selectInterface_Equipment_LanlinkMetro");
						Metro.clickOnBreadCrump(siteOrderNumber);
						Metro.selectInterfacelinkforEqipment(devicename_EquipActual);
						if (RemoveInterface_Selection.equalsIgnoreCase("yes")) {
							Metro.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							System.out.println("Interfaces are not removed");
							Report.LogInfo("Info", "No Interfaces are removed", "PASS");
						}
						if (AddInterface_Selection.equalsIgnoreCase("yes")) {
							Metro.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							System.out.println("Interfaces are not added");
							Report.LogInfo("Info", "No Interfaces are added", "PASS");
						}

						// showInterface_Equipment_LanlinkMetro");
						deviceName_Equip = devicename_EquipActual;
						Metro.clickOnBreadCrump(siteOrderNumber);
						String interfaceAvailability = Metro.SelectShowInterfacelinkAndVerifyEditInterfacePage(
								testDataFile, dataSheet, scriptNo, dataSetNo, devicename_EquipActual);
						if (interfaceAvailability.equalsIgnoreCase("Yes")) {
							Metro.EnterdataForEditInterfaceforShowInterfacelinkunderEquipment(testDataFile, dataSheet,
									scriptNo, dataSetNo);
						}

						// AMNvalidator_Equipment_LanlinkMetro");
						if (proactiveMonitorvalue.equalsIgnoreCase("Yes")) {
							String csrName = Metro.fetchCSRsiteName();
							String cityName = Metro.fetchDeviceCityName();
							String countryName = Metro.fetchSiteOrderCountryName();
							Metro.clickOnAMNvalidatorLink();
							Metro.AMNvalidator(siteOrderNumber, deviceName_Equip, csrName, cityName, countryName);

						} else {
							Report.LogInfo("Info",
									"'AMN Validator' link do not display, as 'proactive Monitoring' checkbox is not selected ",
									"Info");

						}

						// deleteDevice_Equipment_LanlinkMetro");
						Metro.clickOnBreadCrump(siteOrderNumber);
						Metro.deleteDeviceFromServiceForequipment(devicename_equip);
						Metro.successMessage_deleteFromService();

					} else {
						Report.LogInfo("Info", "Equipment device is not created as expected", "PASS");
						System.out.println("Equipment device is not created as expected");

					}
				} else {
					Report.LogInfo("Info", " 'Equipment' panel is not displaying under 'view site order' page", "Info");
					System.out.println(" 'Equipment' panel is not displaying under 'view site order' page");

				}

				// IntermediateEquipment_addDevice_LanlinMetro");
				String country_intEquip = "";
				String vendorModel_intEquip = null;

				boolean IntermediateEquipmentPanel = Metro.findPanelHeader("Intermediate Equipment");
				if (IntermediateEquipmentPanel) {
					if (deviceCreation_IntermediateEquipment.equalsIgnoreCase("yes")) {
						Report.LogInfo("Info", " Device to be created for Intermediate Eqiupment as per input provided",
								"Info");
						Report.LogInfo("Info", "Under Intermediate Equipement, list of actions to be performed are: "
								+ "Verify fields for Add device" + "Add device" + "Verify entered values for device"
								+ "Edit device" + "Select Interface" + "show/Hide Interface -- Edit Interface"
								+ "Select Interface -- Add Interface to service , Remove Interface from Service"
								+ "Delete device ", "Info");

						if (ModularMSp.equalsIgnoreCase("yes")) {
							if (existingDevice_intEquip.equalsIgnoreCase("Yes")
									&& newDevice_intEquip.equalsIgnoreCase("No")) {
								Report.LogInfo("Info", "AddExistingDevice_MSPselected_IntEquipment", "Info");
								Metro.addDevice_IntEquipment();
								Metro.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								Metro.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(existingDeviceName);
								Metro.verifyValuesforCPEexistingdevice_MSPselected(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								// editExistingDevice_MSPselected_IntermediateEquipment_LanlinkMetro");
								Metro.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet,
										scriptNo, dataSetNo);
								Metro.verifysuccessmessage("Device successfully updated");

							} else if (existingDevice_intEquip.equalsIgnoreCase("No")
									&& newDevice_intEquip.equalsIgnoreCase("Yes")) {
								Report.LogInfo("Info", "AddNewDevice_MSPselected_IntermediateEquipment", "Info");
								Metro.addDevice_IntEquipment();
								Metro.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								Metro.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_MSPselected(
										testDataFile, dataSheet, scriptNo, dataSetNo);
								Metro.verifysuccessmessage("Device successfully created");

								// verifyEnteredValues_MSPselected_IntermediateEquipment_LanlinkMetro");
								Metro.verifyCPEdevicedataenteredForIntermediateEquipment_MSPselected(testDataFile,
										dataSheet, scriptNo, dataSetNo);

								// editNewDevice_MSPselected_IntermediateEquipment_LanlinkMetro");
								Metro.EDITCPEdevicedforIntermediateEquipment_MSPselected(testDataFile, dataSheet,
										scriptNo, dataSetNo);
								Metro.verifysuccessmessage("Device successfully updated");

							}
						} else {
							if (speed.equals("1GigE")) {
								if (existingDevice_intEquip.equalsIgnoreCase("Yes")
										&& newDevice_intEquip.equalsIgnoreCase("No")) {
									Report.LogInfo("Info", "addExistingDevice_1G_IntermediateEqiupment", "Info");
									Metro.addDevice_IntEquipment();
									Metro.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(
											intEquip_existingDeviceValue);
									Metro.verifyValuesforCPEexistingdevice_1G_intEquipment(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editNewDevice_1G_IntermediateEquipment_LanlinkMetro");
									Metro.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								} else if (existingDevice_intEquip.equalsIgnoreCase("No")
										&& newDevice_intEquip.equalsIgnoreCase("Yes")) {

									Report.LogInfo("Info", "addNewDevice_1G_IntermediateEquipment", "Info");
									Metro.addDevice_IntEquipment();
									Metro.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(testDataFile,
											dataSheet, scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_1G_IntermediateEquipment_LanlinkMetro");
									Metro.verifyCPEdevicedataenteredForIntermediateEquipment_1G(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editNewDevice_1G_IntermediateEquipment_LanlinkMetro");
									Metro.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								}
							} else if (speed.equals("10GigE")) {
								if (existingDevice_intEquip.equalsIgnoreCase("Yes")
										&& newDevice_intEquip.equalsIgnoreCase("No")) {

									Report.LogInfo("Info", "addExistingDevice_10G_IntermediateEquipment", "Info");
									Metro.addDevice_IntEquipment();
									Metro.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(
											intEquip_existingDeviceValue);
									Metro.verifyValuesforCPEexistingdevice_10G_intEquipment(testDataFile, dataSheet,
											scriptNo, dataSetNo);

									// editExistingDevice_10G_IntermediateEquipment_LanlinkMetro");
									Metro.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								} else if (existingDevice_intEquip.equalsIgnoreCase("No")
										&& newDevice_intEquip.equalsIgnoreCase("Yes")) {

									Report.LogInfo("Info", "addNewDevice_10G_IntermediateEqiupment", "Info");
									Metro.addDevice_IntEquipment();
									Metro.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
									Metro.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(testDataFile,
											dataSheet, scriptNo, dataSetNo);
									Metro.verifysuccessmessage("Device successfully created");

									// verifyEnteredValues_10G_IntermediateEquipment_LanlinkMetro");
									Metro.verifyCPEdevicedataenteredForIntermediateEquipment_10G(testDataFile,
											dataSheet, scriptNo, dataSetNo);

									// editNewDevice_10G_IntermediateEquipment_LanlinkMetro");
									Metro.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo,
											dataSetNo);
									Metro.verifysuccessmessage("Device successfully updated");

								}
							}
						}

						// Get device name
						if (existingDevice_intEquip.equalsIgnoreCase("Yes")
								&& newDevice_intEquip.equalsIgnoreCase("No")) {
							if (EDIT_Intequip_cpe_deviecname.equalsIgnoreCase("null")) {
								devicename_intEquip = intEquip_existingDeviceValue;
							} else if (!EDIT_Intequip_cpe_deviecname.equalsIgnoreCase("null")) {
								devicename_intEquip = EDIT_Intequip_cpe_deviecname;
							}
						} else if (existingDevice_intEquip.equalsIgnoreCase("No")
								&& newDevice_intEquip.equalsIgnoreCase("Yes")) {

							if (EDIT_Intequip_cpe_deviecname.equalsIgnoreCase("null")) {
								devicename_intEquip = device_intEquip_name;
							} else if (!EDIT_Intequip_cpe_deviecname.equalsIgnoreCase("null")) {
								devicename_intEquip = EDIT_Intequip_cpe_deviecname;
							}
						}

						// Navigate to view device page
						Metro.IntEquip_clickonviewButton(devicename_intEquip);
						waitForAjax();

						devicename_intEquip = Metro.fetchdevicename_InviewPage(); // Device
																					// Name
						manageAdres_intEquip = Metro.fetchManagementAddressValue_InviewPage(); // Management
																								// Address
						vendorModel_intEquip = Metro.fetchVendorModelValue(); // Vendor/model
						country_intEquip = Metro.fetchCountryValue_InviewPage(); // Country

						// Perform fetch from Interface
						// fetchDeviceInterface_LanlinkMetro");
						//Metro.testStatus();
						boolean link = Metro.fetchDeviceInterface_viewdevicepage();
						waitForAjax();

						// routerTools_intermediateEquipment_LanlinkMetro");
						Metro.routerPanel(CommandIPv4_Routertool, manageAdres_intEquip);

						// selectinterface_IntermediateEquipment_LanlinkMetro");
						String devicename_intEquipActual = null;
						devicename_intEquip = Metro.fetchdevicename_InviewPage();

						if (devicename_intEquip.contains("...")) {
							devicename_intEquipActual = devicename_intEquip.substring(0, 14);
						} else {
							devicename_intEquipActual = devicename_intEquip;
						}

						devicename_IntEquipment = devicename_intEquipActual;
						// get site Order name
						String siteOrderNumber = null;
						if (vpnTopology.equals("Point-to-Point")) {
							if (Modularmsp.equalsIgnoreCase("Yes")) {
								siteOrderNumber = siteOrderNumber_p2p_mspSelected;
							} else {
								if (Interfacespeed.equalsIgnoreCase("1GigE")) {
									siteOrderNumber = siteOrderNumber_PointToPoint;
								} else if (Interfacespeed.equalsIgnoreCase("10GigE")) {
									siteOrderNumber = siteOrderNumber_10G_PointToPoint;
								}
							}
						} else {
							siteOrderNumber = Siteordernumber;
						}

						Metro.clickOnBreadCrump(siteOrderNumber);
						Metro.selectInterfacelinkforIntermediateEqipment(devicename_IntEquipment);
						if (RemoveInterface_Selection.equalsIgnoreCase("yes")) {
							Metro.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							System.out.println("interfaces are not removed");
						}
						if (AddInterface_Selection.equalsIgnoreCase("yes")) {
							Metro.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
						}

						// showInterace_IntermediateEquipment_LanlinkMetro");
						Metro.clickOnBreadCrump(siteOrderNumber);
						Metro.SelectShowInterfacelink_IntermediateequipmentAndVerifyEditInterfacePage(
								Interfacename_forEditInterface, devicename_IntEquipment);
						Metro.EnterdataForEditInterfaceforShowInterfacelinkunderIntermediateEquipment(testDataFile,
								dataSheet, scriptNo, dataSetNo);

						// deletDeviceFromService_IntermediateEquipment_LanlinkMetro");
						Metro.deleteDeviceFromServiceForIntermediateequipment(devicename_IntEquipment);
						Metro.successMessage_deleteFromService();

					}
				} else {
					Report.LogInfo("Info", " 'Intermediate Equipment' panel is displaying under 'view site order' page",
							"PASS");
					Reporter.log(" 'Intermediate Equipment' panel is displaying under 'view site order' page");

				}

				// PAMTest_LanlinkMetro");

				if (Edit_serviceNumber.equalsIgnoreCase("null")) {
					ServiceID = serviceNumber;
				} else {
					ServiceID = Edit_serviceNumber;
				}
				Metro.returnbacktoviewsiteorderpage();
				Metro.pamTest(ServiceID);
				
			}

			if (Edit_serviceNumber.equalsIgnoreCase("null")) {
				ServiceID = serviceNumber;
			} else {

				ServiceID = Edit_serviceNumber;
			}
			

		}

		catch (

		Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

		}

		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}
}