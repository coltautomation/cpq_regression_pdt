package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODLoginObj 
{

	public static class Login
	{ 
		public static final String userNameTxb="@xpath=//*[@id='userId']"; //Username
		public static final String passWordTxb="@xpath=//*[@id='password']"; //Password
		public static final String loginBtn="@xpath=//button[@type='submit' and contains(text(), 'Login')]"; //Login button
		public static final String welcomeTxt="@xpath=//descendant::h4[contains(text(), 'Welcome to Numbers on Demand,')]"; //Welcome Text


	}

}