package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.exploreFunctions.ExploreLoginPage;

public class TS_31_Multi_Quote_Sales_NewFeatures extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testMultiQuoteSalesWithFeatures(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);

			//Create Multi Quote
			Opportunity.NavigateToQuotes(testDataFile,dataSheet , scriptNo, dataSetNo);
			AddProduct.addQuoteInC4CForMulti(testDataFile,dataSheet , scriptNo, dataSetNo);
						
			
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			
			String sResult;
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
//				calling the below method to enter the site address
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below method to configure the product
				sResult = AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);	
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
			} else {
//				Calling the below method to add the product in CPQ
				sResult = AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");}
			}
			
			AddProduct.multiQuoteProcess(testDataFile, dataSheet, scriptNo, dataSetNo,ProductName);
			CPQLogin.CPQLogout();
			String Multi_Quote_ID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Multi_Quote_ID");			
			
//			Creating Opportunity									
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			
//			Link Quote with Opportunity			
			String Opportunity_ID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Opportunity_ID");			
			AddProduct.LinkQuoteWithOpportunity(Opportunity_ID, Multi_Quote_ID);	
					
			//AddProduct.saveCPQ("main");
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem,"Quote Stage");
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo); 
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem,"Quote Stage");
			quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("To be Priced")) 
			{
				String Pricing_City=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Pricing_City");
				String NRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NRC");
				String MRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MRC");
				
				String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
				AddProduct.editEthernetProductConfiguration();
				AddProduct.verifyQuoteStage("Portfolio Pricing Review");
				Approvals.SwitchCPQUser("AD_User", Quote_ID);
				AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.clickProductConfigurationBtn(ProductName);
				
				AddProduct.UpdatepricingCityException(ProductName,Pricing_City,NRC,MRC);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			AddProduct.verifyQuoteStage("Priced");
			AddProduct.addHubrefernce(ProductName);
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);			
			
			if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
			{
			String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
			Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
			} else {
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
			}
			
			//String CPE_Site_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Site_BCN");
			//Submit.addBillingInformation("CPE Solutions Site", CPE_Site_BCN);
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	

}
