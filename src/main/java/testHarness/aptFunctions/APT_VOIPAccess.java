package testHarness.aptFunctions;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_VOIPAccess_Obj;
import pageObjects.aptObjects.Lanlink_Metro_Obj;

public class APT_VOIPAccess extends SeleniumUtils
{
	public void createcustomer(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");

		// Report.LogInfo("Info", "'Verifying Customer Creation
		// Functionality");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ManageCustomerServiceLink, " Manage Customer Service Link");
		click(APT_VOIPAccess_Obj.VOIPAccess.ManageCustomerServiceLink,"ManageCustomerServiceLink");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.createcustomerlink, " create customer link");
		click(APT_VOIPAccess_Obj.VOIPAccess.createcustomerlink, "Create Customer Link");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.createcustomer_header, "create customer page header");

		////// ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.okbutton, "ok");
		click(APT_VOIPAccess_Obj.VOIPAccess.okbutton, "ok");

		// Warning msg check
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.customernamewarngmsg, "Legal Customer Name");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.countrywarngmsg, "Country");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ocnwarngmsg, "OCN");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.typewarngmsg, "Type");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.emailwarngmsg, "Email");

		// Create customer by providing all info

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.nametextfield, "name");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.nametextfield, name, "name");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.maindomaintextfield, "Main Domain");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.maindomaintextfield, maindomain, "Main Domain");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.country, "country");
		// sendKeys(APT_VOIPAccess_Obj.VOIPAccess.country,country,"country");
		addDropdownValues_commonMethod("Country", APT_VOIPAccess_Obj.VOIPAccess.country, country);

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ocntextfield, "ocn");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.ocntextfield, ocn, "ocn");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.referencetextfield, "reference");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.referencetextfield, reference, "reference");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.technicalcontactnametextfield, "technical contact name");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.technicalcontactnametextfield, technicalcontactname,
				"technical contact name");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.typedropdown, "technical contact name");
		// sendKeys(APT_VOIPAccess_Obj.VOIPAccess.typedropdown,type,"technical
		// contact name");
		addDropdownValues_commonMethod("Type", APT_VOIPAccess_Obj.VOIPAccess.type, type);

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.emailtextfield, "email");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.emailtextfield, email, "email");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.phonetextfield, "phone text field");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.phonetextfield, phone, "phone text field");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.faxtextfield, "fax text field");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.faxtextfield, fax, "fax text field");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.okbutton, "ok");
		click(APT_VOIPAccess_Obj.VOIPAccess.okbutton, "ok");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.customercreationsuccessmsg, "Customer successfully created.");

	}

	public void selectCustomertocreateOrder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

		mouseMoveOn(APT_VOIPAccess_Obj.VOIPAccess.ManageCustomerServiceLink);

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.CreateOrderServiceLink, "Create Order Service Link");
		click(APT_VOIPAccess_Obj.VOIPAccess.CreateOrderServiceLink, "Create Order Service Link");

		// verifyExists(APT_VOIPAccess_Obj.VOIPAccess.nextbutton,"Next button");
		// click(APT_VOIPAccess_Obj.VOIPAccess.nextbutton,"Next button");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.entercustomernamefield, "name");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.entercustomernamefield, name, "name");

		// verifyExists(APT_VOIPAccess_Obj.VOIPAccess.entercustomernamefield,"name");
		// sendKeys(APT_VOIPAccess_Obj.VOIPAccess.entercustomernamefield,"*","name");

		waitforPagetobeenable();

		addDropdownValues_commonMethod("Choose a customer", APT_VOIPAccess_Obj.VOIPAccess.chooseCustomerdropdown, name);
		// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.nextbutton, "Next button");
		click(APT_VOIPAccess_Obj.VOIPAccess.nextbutton, "Next button");

	}

	public void createorderservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderselection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"ExistingOrderSelection");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.nextbutton);

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.nextbutton, "Next button");
		click(APT_VOIPAccess_Obj.VOIPAccess.nextbutton, "Next button");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.order_contractnumber_warngmsg, "order contract number");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.servicetype_warngmsg, "service type");

		if (neworder.equalsIgnoreCase("YES")) {
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.newordertextfield, "new order textfield");
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.newrfireqtextfield, "new rfireq textfield");
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.createorderbutton, "create order button");
			click(APT_VOIPAccess_Obj.VOIPAccess.createorderbutton, "create order button");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.OrderCreatedSuccessMsg, "Order Created Success Msg");

		} else if (existingorderselection.equalsIgnoreCase("YES")) {
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.selectorderswitch, "select order switch");
			click(APT_VOIPAccess_Obj.VOIPAccess.selectorderswitch, "select order switch");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.existingorderdropdown, "existing order drop down");
			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",APT_VOIPAccess_Obj.VOIPAccess.existingorderdropdown, existingordernumber);
		} else {
			Reporter.log("Order not selected");
		}

	}
	
	public void verifyselectservicetype(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException

	{
		String CreateOrderServiceHeader = "Create Order / Service";
		String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		// String
		// actualValue=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.service);

		// select service type
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.servicetypetextfield);

		// verifyExists(APT_VOIPAccess_Obj.VOIPAccess.nextbutton,"Next button");
		// click(APT_VOIPAccess_Obj.VOIPAccess.nextbutton,"Next button");

		// verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ServiceTypeWarningMessage,"Service
		// Type Warning Message");

		waitforPagetobeenable();
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.servicetypetextfield, "service type textfield");
		addDropdownValues_commonMethod("Service Type", APT_VOIPAccess_Obj.VOIPAccess.servicetypetextfield, servicetype);

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.nextbutton, "Next button");
		click(APT_VOIPAccess_Obj.VOIPAccess.nextbutton, "Next button");

		(APT_VOIPAccess_Obj.VOIPAccess.createorderservice_header).equals(CreateOrderServiceHeader);
	}
	
	
	
	public void searchorder(String sid)
			throws InterruptedException, IOException

	{
		waitforPagetobeenable();
		waitforPagetobeenable();
		

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ManageCustomerServiceLink, " Manage Customer Service Link");
		mouseMoveOn(APT_VOIPAccess_Obj.VOIPAccess.ManageCustomerServiceLink);

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.searchorderlink, "search order link");
		click(APT_VOIPAccess_Obj.VOIPAccess.searchorderlink, "search order link");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.servicefield, "service field");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.servicefield, sid);

		click(APT_VOIPAccess_Obj.VOIPAccess.searchbutton, "searchbutton");
		// click(searchbutton,"searchbutton");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.serviceradiobutton, "service radio button");
		click(APT_VOIPAccess_Obj.VOIPAccess.serviceradiobutton, "service radio button");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.searchorder_actiondropdown, "search order actiodropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.searchorder_actiondropdown, "search order linksearch order actiodropdown");

		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.view, "view");
		click(APT_VOIPAccess_Obj.VOIPAccess.view, "view");

	}
	
	public void verifyCustomerDetailsInformation(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"newCustomerCreation");
		String existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomerSelection");
		String newCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
		String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MainDomain");

		if (newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) {
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Name_Value, "Name Value");
			String nameValue = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Name_Value, "Name Value");
			nameValue.equalsIgnoreCase(newCustomer);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Country_Value, "Country");
			String countryValue = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Country_Value, "Country");
			countryValue.equalsIgnoreCase(country);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.OCN_Value, "OCN");
			String OCN = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.OCN_Value, "OCN");
			OCN.equalsIgnoreCase(ocn);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Reference_Value, "Reference");
			String Reference = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Reference_Value, "Reference");
			Reference.equalsIgnoreCase(reference);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.TechnicalContactName_Value, "Technical Contact Name");
			String technicalContactName = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.TechnicalContactName_Value, "Technical Contact Name");
			technicalContactName.equalsIgnoreCase(technicalcontactname);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Type_Value, "Type");
			String Type = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Type_Value, "Type");
			Type.equalsIgnoreCase(type);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Email_Value, "Email");
			String Email = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Email_Value, "Email");
			Email.equalsIgnoreCase(email);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Phone_Value, "Phone");
			String Phone = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Phone_Value, "Phone");
			Phone.equalsIgnoreCase(phone);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Fax_Value, "Fax");
			String Fax = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Fax_Value, "Fax");
			Fax.equalsIgnoreCase(fax);
		} else if (newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes")) {
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Name_Value, "Fax");
			String ExistingCustomer = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.Name_Value, "Fax");
			ExistingCustomer.equalsIgnoreCase(existingCustomer);
		}
		// Main Domain
		if (maindomain.equalsIgnoreCase("Null")) {
			Reporter.log("A default displays for main domain field, if no provided while creating customer");

		} else {
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.MainDomain_Value, "Main Domain");
			String Maindomain = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MainDomain_Value, "Main Domain");
			Maindomain.equalsIgnoreCase(maindomain);
		}
		Reporter.log("Customer Details panel fields Verified");
	}

	public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editOrderSelection");
		String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_OrderNumber");
		String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_VoicelineNumber");

		if (editOrderSelection.equalsIgnoreCase("no")) {
			Reporter.log("Edit Order is not performed");
		} else if (editOrderSelection.equalsIgnoreCase("Yes")) {
			Reporter.log("Performing Edit Order Functionality");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "Action dropdown");
			click(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "Action dropdown");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editorderlink, "Edit Order");
			click(APT_VOIPAccess_Obj.VOIPAccess.editorderlink, "Edit Order");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editorderheader, "Edit Order");

			String EditOrderNo = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.editorderno, "edit order no");
			click(APT_VOIPAccess_Obj.VOIPAccess.editorderno, "edit order no");
			clearTextBox(APT_VOIPAccess_Obj.VOIPAccess.editorderno);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editorderno, "Order Number");
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.editorderno, editorderno, "Order Number");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editvoicelineno, "edit voice line no");
			clearTextBox(APT_VOIPAccess_Obj.VOIPAccess.editvoicelineno);
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.cancelbutton, "cancel button");
			click(APT_VOIPAccess_Obj.VOIPAccess.cancelbutton, "cancel button");

			waitforPagetobeenable();
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "Action dropdown");
			click(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "Action dropdown");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editorderlink, "Edit Order");
			click(APT_VOIPAccess_Obj.VOIPAccess.editorderlink, "Edit Order");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editorderheader, "Edit Order Header");
			String editOrderHeader = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.editorderheader,"editorderheader");
			String EditOrder = "Edit Order";
			editOrderHeader.equalsIgnoreCase(EditOrder);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editorderno, "edit order no");
			click(APT_VOIPAccess_Obj.VOIPAccess.editorderno, "edit order no");
			clearTextBox(APT_VOIPAccess_Obj.VOIPAccess.editorderno);
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.editorderno, editorderno, "Order Number");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editvoicelineno, "edit voice line no");
			clearTextBox(APT_VOIPAccess_Obj.VOIPAccess.editvoicelineno);
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.editorder_okbutton, "OK Button");
			click(APT_VOIPAccess_Obj.VOIPAccess.editorder_okbutton, "OK Button");

			if (editorderno.equalsIgnoreCase("Null")) {
				Reporter.log("Order/Contract Number (Parent SID) field is not edited");
			} else {
				verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ordernumbervalue, "order number value");
				String editOrderno = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.ordernumbervalue, "order number value");
				editOrderno.equalsIgnoreCase(editorderno);
			}

			if (editvoicelineno.equalsIgnoreCase("Null")) {
				Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
			} else {
				verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ordervoicelinenumbervalue, "order voice line number value");
				String editVoicelineno = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.ordervoicelinenumbervalue, "order voice line number value");
				editVoicelineno.equalsIgnoreCase(editvoicelineno);
			}
			Reporter.log("Edit Order is successful");

		}

	}

	public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{

		String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"changeOrderSelection_newOrder");
		String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "changeOrderSelection_existingOrder");
		String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_newOrderNumber");
		String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_VoicelineNumber");
		String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_existingOrderNumber");

		if ((changeOrderSelection_newOrder.equalsIgnoreCase("No"))
				&& (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
			Reporter.log("Change Order is not performed");

		} else if (changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "Action dropdown");
			click(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "Action dropdown");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorderlink, "change order link");
			click(APT_VOIPAccess_Obj.VOIPAccess.changeorderlink, "change order link");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorderheader, "change order header");
			String changeOrderHeader = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.changeorderheader, "change order header");
			String changeOrder = "Change Order";
			changeOrderHeader.equalsIgnoreCase(changeOrder);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorder_selectorderswitch, "select order switch");
			click(APT_VOIPAccess_Obj.VOIPAccess.changeorder_selectorderswitch, "select order switch");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeordernumber, "change order number");
			click(APT_VOIPAccess_Obj.VOIPAccess.changeordernumber, "change order number");
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.editorderno, ChangeOrder_newOrderNumber);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeordervoicelinenumber, "change order voiceline number");
			click(APT_VOIPAccess_Obj.VOIPAccess.changeordervoicelinenumber, "change order voiceline number");
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.changeordervoicelinenumber, changevoicelineno);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.createorder_button, "create order button");
			click(APT_VOIPAccess_Obj.VOIPAccess.createorder_button, "create order button");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorder_okbutton, "change order ok button");
			click(APT_VOIPAccess_Obj.VOIPAccess.changeorder_okbutton, "change order ok button");

			// String
			// orderNumberValue=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.ordernumbervalue);
			// String orderNumberValue=
			// getAttributeFrom(APT_VOIPAccess_Obj.VOIPAccess.ordernumbervalues,"value");
			// if(orderNumberValue.equalsIgnoreCase(ChangeOrder_newOrderNumber))
			// {
			Reporter.log("orderNumberValue compare");
			// }
			// else
			// {
			// Reporter.log("orderNumberValue not same");
			// }

			// String
			// orderVoicelineNumberValue=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.ordervoicelinenumbervalue);
			// String orderVoicelineNumberValue=
			// getAttributeFrom(APT_VOIPAccess_Obj.VOIPAccess.ordervoicelinenumbervalues,"value");
			// if(orderVoicelineNumberValue.equalsIgnoreCase(changevoicelineno))
			// {
			// Reporter.log("orderVoicelineNumberValue compare");

			// }
			// else
			// {
			Reporter.log("orderVoicelineNumberValue not same");
			// }

			Reporter.log("Change Order is successful");
		} else if (changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) {
			Reporter.log("Performing Change Order functionality");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "order action button");
			click(APT_VOIPAccess_Obj.VOIPAccess.orderactionbutton, "order action button");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorderlink, "change order link");
			click(APT_VOIPAccess_Obj.VOIPAccess.changeorderlink, "change order link");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorderheader, "change order");

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorder_chooseorderdropdown,
					"Order/Contract Number (Parent SID)");
			select(APT_VOIPAccess_Obj.VOIPAccess.changeorder_chooseorderdropdown, ChangeOrder_existingOrderNumber);

			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changeorder_okbutton, "change order ok button");
			click(APT_VOIPAccess_Obj.VOIPAccess.changeorder_okbutton, "change order ok button");

			// String
			// orderNumberValue=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.ordernumbervalue);
			// orderNumberValue.equalsIgnoreCase(ChangeOrder_existingOrderNumber);

			// String
			// orderVoicelineNumberValue=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.ordervoicelinenumbervalue);
			// orderVoicelineNumberValue.equalsIgnoreCase(changevoicelineno);

			Reporter.log("Change Order is successful");

		}

	}	
	
	
	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException

	{
		String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

		String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");

		String ResellerCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerCode");

		String Remarks = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");

		String EmailService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EmailService");

		String PhoneService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneService");

		
		waitForAjax();
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
		
		try {
			
		compareText("Service panel Header", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_header, "Service");
		compareText("Service Identification", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_serviceidentificationvalue, sid);
		compareText("Service Type", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_servicetypevalue, servicetype);
		compareText("Reseller Code", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_ResselerCodevalue, ResellerCode);
		compareText("Remarks", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_remarksvalue, Remarks);
		compareText("Email", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_Emailvalue, EmailService);
		compareText("Phone Contact", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_PhoneContactvalue, PhoneService);
		
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed in View Service page","FAIL");
			Reporter.log(  e+ " : Field is not displayed in View Service page");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed in View Service page ","FAIL");
			Reporter.log(  e+" : Field is not displayed in View Service page");
		}

	}

	
	
	public void verifyManagementOptionspanel(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String ManageService	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManageService");

		String SyslogEventView	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SyslogEventView");
		String ServiceStatusView	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatusView");
		String RouterConfigurationView	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RouterConfigurationView");
		String PerformanceReporting	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PerformanceReporting");
		String ProactiveNotification	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ProactiveNotification");
		String	NotificationManagementTeam	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NotificationManagementTeam");
		String	DialUserAdministration	= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DialUserAdministration");
		
		
			waitForAjax();
				
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.servicepanel_header);		
		try {
		Boolean ManagementOptions_Header = isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.managementoptions_header);
		Reporter.log("Management options header text is displayed as : " + ManagementOptions_Header);
		Reporter.log("Management options header text:"+ ManagementOptions_Header);
	
		
		// verify Managed Service information
		compareText_InViewPage("Managed Service", ManageService);
		compareText_InViewPage("Syslog Event View", SyslogEventView);
		compareText_InViewPage("Service Status View", ServiceStatusView);
		compareText_InViewPage("Router Configuration View", RouterConfigurationView);
		compareText_InViewPage("Performance Reporting", PerformanceReporting);
		compareText_InViewPage("Pro-active Notification", ProactiveNotification);
		compareText_InViewPage("Dial User Administration", DialUserAdministration);
		if(ProactiveNotification.equalsIgnoreCase("Yes")) {
			compareText_InViewPage("Notification Management Team", NotificationManagementTeam);
		}
	
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed in View Service page","FAIL");
			Reporter.log(  e+ " : Field is not displayed in View Service page");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed in View Service page ","FAIL");
			Reporter.log(  e+" : Field is not displayed in View Service page");
		}
		
	}

	

	public void manageSubnet_viewServicepage() throws InterruptedException, IOException {
		
		waitforPagetobeenable();
		try {
		
		waitforPagetobeenable();
		waitforPagetobeenable();
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
		click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown, "Action");
		waitforPagetobeenable();
		   click(APT_VOIPAccess_Obj.VOIPAccess.ManageSubnetsIpv6Link, "Manage Subnet IPv6");
		   
		   waitforPagetobeenable();
		   boolean DumpPage=false;
		   DumpPage=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.dumpPage_header);
		   if(DumpPage) {
			  Report.LogInfo("Info", "Manage Subnet IPv6 page is displaying","PASS");
			   Reporter.log("manage subnet ipv6 page is displaying");
			  
			   try {
			  //Header 
			   String headerName=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.dumpheaderName,"dump header Name");
			   if(headerName.isEmpty()) {
				  Report.LogInfo("Info", "Header name is not displaying","PASS");
				   Reporter.log("Header name is not displaying");
				 
			   }else {
				  Report.LogInfo("Info", "manage Subnet IPv6 header name is displaying as "+ headerName,"PASS");  
				   Reporter.log("manage subnet ipv6 header name is displaying as "+ headerName);
			   }
			   
			   //body
			   String bodyContent=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.manageSubnet_successMSG,"manage Subnet_successMSG");
			   if(bodyContent.isEmpty()) {
				  Report.LogInfo("Info", "manage subnet value is not displaying","PASS");
				   Reporter.log("manage subnet value is not displaying");
			   }else {
				  Report.LogInfo("Info", "manage subnet message is displaying as "+bodyContent,"PASS");
				   Reporter.log("manage subnet message is displaying as "+bodyContent);
			   }
			   
			   click(APT_VOIPAccess_Obj.VOIPAccess.dump_xButton,"X button");
			   
			   
		   }catch(NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", e+ " : Field is not displayed in manage subnet ipv6","FAIL");
				Reporter.log(  e+ " : Field is not displayed in manage subnet ipv6");
			}catch(Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info",  e+" : Field is not displayed in manage subnet ipv6 ","FAIL");
				Reporter.log(  e+" : Field is not displayed in manage subnet ipv6");
			}
			
		   }else {
			  Report.LogInfo("Info", "manage subnet ipv6 page is not displaying","PASS");
			   Reporter.log("manage subnet ipv6 page is not displaying");

		   }
		   
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
			Reporter.log(  e+ " : Field is not displayed");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
			Reporter.log(  e+" : Field is not displayed");
		}
	
		   
	}

	public void manageServiceFunctionTest(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String Sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
		String serviceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		String servicestatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatus");
		String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatusChangeRequired");
		String syncstatus= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "syncstatus");
		try {
			waitforPagetobeenable();
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown,"service action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown,"service action dropdown");
		
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.manageLink,"manage");
		click(APT_VOIPAccess_Obj.VOIPAccess.manageLink,"manage");
		
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.manageservice_header,"manage service");
		
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.status_servicename,"space name");
		String sid=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.status_servicename,"space name");
		sid.equalsIgnoreCase(Sid);
		
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.status_servicetype,"space name");
		String servicetype=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.status_servicetype,"status_service type");
		servicetype.equalsIgnoreCase(serviceType);

		String ServiceDetails_value=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.status_servicedetails,"status_service details");
		
		if(isValueEmpty(ServiceDetails_value))
		{
			Reporter.log("Service Details column value is empty as expected");

		}
		else
		{
			Reporter.log("Service Details column value should be empty");
		}
		
		String serviceStatus=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.status_currentstatus,"status_current status");
		serviceStatus.equalsIgnoreCase(servicestatus);

		String LastModificationTime_value=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.status_modificationtime,"status_modification time");
		
		if(LastModificationTime_value.contains("BST"))
		{
			Reporter.log("Service status is displayed as");

		}
		
		else
		{
			Reporter.log("Incorrect modification time format");

		}
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.statuslink,"Status");
		click(APT_VOIPAccess_Obj.VOIPAccess.statuslink,"Status");
		
		if(servicestatuschangerequired=="Yes")
		{

			if(isClickable(APT_VOIPAccess_Obj.VOIPAccess.Servicestatus_popup))
			{
				verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changestatus_dropdown,"change status");
				click(APT_VOIPAccess_Obj.VOIPAccess.changestatus_dropdown,"change status");
				
				verifyExists(APT_VOIPAccess_Obj.VOIPAccess.changestatus_dropdownvalue,"change status");
				click(APT_VOIPAccess_Obj.VOIPAccess.changestatus_dropdownvalue,"change status");
				
				verifyExists(APT_VOIPAccess_Obj.VOIPAccess.okbutton,"ok");
				click(APT_VOIPAccess_Obj.VOIPAccess.okbutton,"ok");
				
				if(isClickable(APT_VOIPAccess_Obj.VOIPAccess.servicestatushistory))
				{
					Reporter.log("Service status change request logged");
				}
				else
				{
					Reporter.log(" Service status change request is not logged");
				}	
			}
			else
			{
				Reporter.log(" Status link is not working");
	
			}
		}
		else
		{
			Reporter.log("Service status change not reqired");
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.servicestatus_popupclose,"service status");
			click(APT_VOIPAccess_Obj.VOIPAccess.servicestatus_popupclose,"service status");
		}
		
		String syncServicename=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.sync_servicename,"sync_servicename");
		syncServicename.equalsIgnoreCase(sid);
		
		String syncServiceType=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.sync_servicetype,"sync_servicetype");
		syncServiceType.equalsIgnoreCase(serviceType);
		
		if(isEmptyOrNull(APT_VOIPAccess_Obj.VOIPAccess.sync_servicedetails))
		{
			Reporter.log("Service Details column value is empty as expected");	
		}
		else
		{
			Reporter.log("Service Details column value should be empty");
		}		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		compareText("Sync Status", APT_VOIPAccess_Obj.VOIPAccess.sync_status, syncstatus);
		click(APT_VOIPAccess_Obj.VOIPAccess.synchronizelink,"Synchronize");
		compareText("Synchronize Success Msg", APT_VOIPAccess_Obj.VOIPAccess.Sync_successmsg, "Sync started successfully. Please check the sync status of this service.");
		//((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0, document.body.scrollHeight)");

	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Manage Service page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in Manage Service page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in Manage Service page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in Manage Service page");
	}
	
}



	
public void verifyDumpFunctionInviewServicepage() throws IOException {

	try {
		waitForAjax();
		
			scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
		
			click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown, "Action");
		   
		   click(APT_VOIPAccess_Obj.VOIPAccess.DumpLink, "Dump");
		   
		   waitforPagetobeenable();
		   boolean DumpPage=false;
		   DumpPage=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.dumpPage_header);
		   if(DumpPage) {
			   Report.LogInfo("Info", "Service Dump Detail page is displaying","PASS");
			   Reporter.log("Service Dump Detail page is displaying");
			  
			  //Header 
			   String headerName=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.dumpheaderName,"dumpheaderName");
			   if(headerName.isEmpty()) {
				   Report.LogInfo("Info", "Header name is not displaying","FAIL");
				   Reporter.log("Header name is not displaying");
				 
			   }else {
				   Report.LogInfo("Info", "Dump header name is displaying as "+ headerName,"PASS");  
				   Reporter.log("Dump header name is displaying as "+ headerName);
			   }
			   
			   //body
			   String bodyContent=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.dumpMessage_body,"dumpMessage_body");
			   if(bodyContent.isEmpty()) {
				   Report.LogInfo("Info", "Dump value is not displaying","FAIL");
				   Reporter.log("Dump value is not displaying");
			   }else {
				   Report.LogInfo("Info", "Dump value is displaying as "+bodyContent,"PASS");
				   Reporter.log("Dump value is displaying as "+bodyContent);
			   }
			   
			   click(APT_VOIPAccess_Obj.VOIPAccess.dump_xButton,"X button");
			  
			   waitForAjax();
			   
			   scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
				click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown,"Action dropdown");
				compareText("Edit Link", APT_VOIPAccess_Obj.VOIPAccess.EditLink, "Edit");
				compareText("Delete Link", APT_VOIPAccess_Obj.VOIPAccess.DeleteLink, "Delete");
				compareText("Manage Subnets Ipv6 Link", APT_VOIPAccess_Obj.VOIPAccess.ManageSubnetsIpv6Link, "Manage Subnets Ipv6");
				compareText("Dump Link", APT_VOIPAccess_Obj.VOIPAccess.DumpLink, "Dump");
		   }else {
			   Report.LogInfo("Info", "Service Dump Detail page is not displaying","FAIL");
			   Reporter.log("Service Dump Detail page is not displaying");

		   }
		   
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
			Reporter.log(  e+ " : Field is not displayed");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
			Reporter.log(  e+" : Field is not displayed");
		}
		
	}

	
public void navigatetoViewDevicepage_MAS() throws InterruptedException, IOException {

	//String DeviceDetails = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.viewdevicepage_header, "View device header");
	//String DeviceDetailsValue = "Device Details";

	// click(APT_VOIPAccess_Obj.VOIPAccess.back, "back");
	// ScrollIntoViewByString(APT_VOIPAccess_Obj.VOIPAccess.viewservicepage_viewdevicelink1);
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice);
	// ((JavascriptExecutor)
	// webDriver).executeScript("window.scrollBy(0,-100)");
	if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice)) {
		
		webDriver.findElement(By.xpath("//tr//td//b[contains(.,'lo0-mas2.BHX.router.colt.net')]//parent::td//following-sibling::span//a[text()='View']")).click();
	//	click(APT_VOIPAccess_Obj.VOIPAccess.devicefirst, "View");
		waitforPagetobeenable();
		

		// DeviceDetails.equals(DeviceDetailsValue);
	} else {
		Reporter.log("No Device added in grid");
	}

}	

public void navigateToViewTrunkPage(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String TrunkGroupSiteOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");
	String ExistingTrunkName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingTrunkName");
	//scrollDown(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrderPanel_header")));
	
	boolean TrunkNameToAddCPEDeviceText=false;
	boolean trunkgrupOrderErrMsg= false;
	boolean TrunkGroupSiteOrderNumberText=false;
	waitforPagetobeenable();
	
	try {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	Reporter.log("Scroll To Trunk Panel");
	
	TrunkNameToAddCPEDeviceText= webDriver.findElement(By.xpath("//tr//td[contains(.,'')]")).isDisplayed();
	Reporter.log("Scroll To Trunk Panel2");
	if(TrunkNameToAddCPEDeviceText) {
		Report.LogInfo("Info", "Expected 'Trunk Name' is displaying as expected under Trunk Panel in 'view Service' page","PASS");
		Reporter.log("Expected 'Trunk Name'' is displaying as expected under Trunk Panel in 'view Service' page");
	
		//((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	//click(application, "Trunk Name in Trunk panel", "//div[text()='"+ExistingTrunkName+"']");//Select Checkbox for expected trunk Not working
/*	WebElement CheckboxForExpectedTrunk = webDriver.findElement(By.xpath("//div[text()='"+ExistingTrunkName+"']"));
	String TrunkName = CheckboxForExpectedTrunk.getText().toString();
	CheckboxForExpectedTrunk.click();
	Report.LogInfo("Info","Step : Checkbox Selection is done for Expected trunk, Selected Trunk is " + TrunkName,"PASS");
	Reporter.log("Selected Trunk is  : " + TrunkName); */
	
	
	 // String TunkActionLinkXpathInViewServicePage="//div[div[span[contains(text(),'"+TrunkGroupSiteOrderNumber+"')]]]/following-sibling::div//button[text()='Action']";
	  
	//**click_PassingWebelementDirectly(application, "Trunk Action", TunkActionLinkXpathInViewServicePage, xml);
//	click(APT_VOIPAccess_Obj.VOIPAccess.ViewService_Trunk_ActionLink1,"ACTION LINK in Trunk panel");
												
	
//	String ViewTrunkLinkXpathInViewServicePage="//div[div[span[contains(text(),'"+TrunkGroupSiteOrderNumber+"')]]]/following-sibling::div//a[text()='View']";
	//**click_PassingWebelementDirectly(application, "View Link", ViewTrunkLinkXpathInViewServicePage, xml);
		webDriver.findElement(By.xpath("(//tr//td[contains(.,'')]//following-sibling::td//a[text()='View'])[1]")).click();
	
	}else {
			Report.LogInfo("Info", "Expected 'Trunk Name' is not displaying as expected under Trunk Panel in 'view Service' page","FAIL");
			Reporter.log("Expected 'Trunk Name'' is not displaying as expected under Trunk Panel in 'view Service' page");
		  }
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}


}
	
public void testStatus_MAS() throws InterruptedException, IOException {
	
	Report.LogInfo("Info", "Verifying 'Test Status' table","Info");
	
	String element=null;
	String status=null;
	
//	List<WebElement> testlist=webDriver.findElements(By.xpath("//tbody/tr"));
	List<String> testlist = new ArrayList<>(Arrays.asList(getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.testlist,"test list")));
	int listSize=testlist.size();
	
	
	for(int i=1; i<=listSize; i++) {
	 /* try {	
		element=webDriver.findElement(By.xpath("(//tbody/tr["+ i +"]/td)[1]")).getText();
		
		if(element.isEmpty()) {
			
		}else {
			Report.LogInfo("Info", "Test Name is displaying as: "+element,"PASS");
			Reporter.log("Test Name is displaying as: "+element);
			
			
			status=webDriver.findElement(By.xpath("(//tbody/tr["+ i +"]/td)[2]/div")).getAttribute("text-left");
			Reporter.log("status displays as: "+status);
			
			if(status.contains("red")) {
				Report.LogInfo("Info", element + " status colour dipslays as: red","PASS");
				Reporter.log(element + " status colour dipslays as: red");
			}
			else if(status.contains("green")) {
				Report.LogInfo("Info", element + " status colour dipslays as: green","PASS");
				Reporter.log(element + " status colour dipslays as: green");
			}
		}
	  }*/
		
		try {
			element = getTextFrom(
					APT_VOIPAccess_Obj.VOIPAccess.testlist1 + i + APT_VOIPAccess_Obj.VOIPAccess.testlist2,"test list");

			if (element.isEmpty()) {

			} else {
				Reporter.log("Test Name is displaying as: " + element);

				status = getAttributeFrom(
						APT_VOIPAccess_Obj.VOIPAccess.testlist1 + i + APT_VOIPAccess_Obj.VOIPAccess.testlist3, "class");
				Reporter.log("status displays as: " + status);

				if (status.contains("red")) {
					Reporter.log(element + " status colour dipslays as: red");
				} else if (status.contains("green")) {
					Reporter.log(element + " status colour dipslays as: green");
				}
			}
		} 
	  catch(Exception e) {
		  e.printStackTrace();
	  }
	}
}

public void testStatus_PE() throws IOException, InterruptedException {

Report.LogInfo("Info", "Verifying 'Test Status' table","Info");
	waitforPagetobeenable();
waitForAjax();
waitForAjax();
String element=null;
String status=null;

//List<WebElement> testlist=webDriver.findElements(By.xpath("//tbody/tr"));
List<String> testlist = new ArrayList<>(Arrays.asList(getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.testlist,"test list")));
int listSize=testlist.size();


for(int i=1; i<=listSize; i++) {
 /* try {	
	element=webDriver.findElement(By.xpath("(//tbody/tr["+ i +"]/td)[1]")).getText();
	
	if(element.isEmpty()) {
		
	}else {
		Report.LogInfo("Info", "Test Name is displaying as: "+element,"PASS");
		Reporter.log("Test Name is displaying as: "+element);
		
		
		status=webDriver.findElement(By.xpath("(//tbody/tr["+ i +"]/td)[2]/div")).getAttribute("text-left");
		Reporter.log("status displays as: "+status);
		
		if(status.contains("red")) {
			Report.LogInfo("Info", element + " status colour dipslays as: red","PASS");
			Reporter.log(element + " status colour dipslays as: red");
		}
		else if(status.contains("green")) {
			Report.LogInfo("Info", element + " status colour dipslays as: green","PASS");
			Reporter.log(element + " status colour dipslays as: green");
		}
	}
  }*/
	
	try {
		element = getTextFrom(
				APT_VOIPAccess_Obj.VOIPAccess.testlist1 + i + APT_VOIPAccess_Obj.VOIPAccess.testlist2,"test list2");

		if (element.isEmpty()) {

		} else {
			Reporter.log("Test Name is displaying as: " + element);

			status = getAttributeFrom(
					APT_VOIPAccess_Obj.VOIPAccess.testlist1 + i + APT_VOIPAccess_Obj.VOIPAccess.testlist3, "class");
			Reporter.log("status displays as: " + status);

			if (status.contains("red")) {
				Reporter.log(element + " status colour dipslays as: red");
			} else if (status.contains("green")) {
				Reporter.log(element + " status colour dipslays as: green");
			}
		}
	} catch(Exception e) {
		  e.printStackTrace();
	  }
	}
}

public void verifysuccessmessage(String expected) throws InterruptedException, IOException {

	waitToPageLoad();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	try {

		boolean successMsg = isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.serivceAlert);

		if (successMsg) {

			String alrtmsg = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.AlertForServiceCreationSuccessMessage,"Alert For Service Creation Success Message");

			if (expected.contains(alrtmsg)) {

				Reporter.log("Message is verified. It is displaying as: " + alrtmsg);
				// successScreenshot();

			} else {

				Reporter.log("Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg);
				// failureScreenshot();
			}

		} else {
			Reporter.log(" Success Message is not displaying");
			// failureScreenshot();
		}

		waitforPagetobeenable();

	} catch (Exception e) {
		Reporter.log("failure in fetching success message - 'Service created Successfully'  ");
		Reporter.log(expected + " message is not getting dislpayed");
		// failureScreenshot();
	}
}

	
public void veriyFetchDeviceInterfacesFunction_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String MAS_ServiceStatusChangeRequired= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_ServiceStatusChangeRequired");
	
	scrollIntoTop();
	if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View MAS Device' page navigated as expected","PASS");
		Reporter.log("'View MAS Device' page navigated as expected");
	
	//click(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_ActionLink,"ACTION link");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_Action_FetchDeviceInterfacesLink,"Fetch Device Interfaces Link");
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_Action_FetchDeviceInterfacesLink,"Fetch Device Interfaces Link");
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_FetchDeviceInterfacesSuccessMessage)){
		Report.LogInfo("Info", "Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ","PASS");
		Reporter.log("Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ");
	}else {
		Report.LogInfo("Info", "Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ","FAIL");
		Reporter.log("Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ");
	}
	
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_hereLink_UnderFetchDeviceInterfacesSuccessMessage,"Click here Link for MAS");
	
	
	//Manage COLT's Network - Manage Network
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_ManageCOLTsNetworkManageNetwork_header)) {
	Report.LogInfo("Info", "'Manage COLT's Network - Manage Network' page navigated as expected","PASS");
	Reporter.log("'Manage COLT's Network - Manage Network' page navigated as expected");
				
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_SynchronizeLink);
			
	try {
	//Manage COLT's Network - Manage Network
	
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_DeviceValue,"Device Name in Manage Colt page under Status Panel");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_StatusValue,"Status in Manage Colt page");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_LastModificationValue,"Last Modification in Manage Colt page");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_StatusLink,"Status Link in Manage Colt page");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_ViewInterfacesLink,"View Interface Link in Manage Colt page");
	
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_DeviceValue,"Device Name in Manage Colt page under Synchronization Panel");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_SyncStatusValue,"Sync Status in Manage Colt page");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_SmartsValue,"Smarts in Manage Colt page");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_FetchInterfacesValue,"Fetch Interfaces in Manage Colt page");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_VistaMartDeviceValue,"VistaMart Device in Manage Colt page");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_SynchronizeLink,"Synchronize Link in Manage Colt page");

	
	scrollIntoTop();
	String MAS_Manage_Status_LastModificationValue= getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_LastModificationValue,"MAS_Manage_Status_Last ModificationValue");
	if(MAS_Manage_Status_LastModificationValue.contains("GMT"))
	{
		Reporter.log("Service status is displayed as : " + MAS_Manage_Status_LastModificationValue);
		Reporter.log("Last Modification is :"+ MAS_Manage_Status_LastModificationValue);
	}else{
		Reporter.log("Incorrect modification time format");
		Reporter.log("Incorrect modification time format");
	}
	
	
	////
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_StatusLink,"Status");

	if(MAS_ServiceStatusChangeRequired=="Yes")
	{
		boolean ServiceStatusPage= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_Servicestatus_popup);
		if(ServiceStatusPage)
		{
			scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_Device_Status_OK);
			click(APT_VOIPAccess_Obj.VOIPAccess.MAS_Device_Status_OK,"Click on OK to change Status");

			boolean MAS_servicestatushistoryValue= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_servicestatushistoryValue);
			try
			{
				if(MAS_servicestatushistoryValue)
				{
					Report.LogInfo("Info", "Step : Service status change request logged","PASS");
				}
				else
					Report.LogInfo("Info", "Step : Service status change request is not logged","PASS");
			}
			catch(StaleElementReferenceException e)
			{
				Report.LogInfo("Info", "No service history to display","FAIL");
			}
		}
		else
			Report.LogInfo("Info", "Status link is not working","FAIL");
	}
	else
	{
		Report.LogInfo("Info", "Step : Service status change not reqired","PASS");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_servicestatus_popupclose,"Close");
	}

	
	
	////synchronize panel in manage colt page
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Synchronization_SynchronizeLink,"Synchronize");
//		compareText(application, "Synchronize Success Message", "MAS_Manage_SynchronizeSuccessMessage", "Sync started successfully. Please check the sync status of this device.");
			verifysuccessmessage("Sync started successfully. Please check the sync status of this device.");

		Reporter.log("------  Sync started successfully. Please check the sync status of this device   ------");
		
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Manage Colt Network page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in Manage Colt Network page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in Manage Colt Network page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in Manage Colt Network page");
	}
	
	}else {
		Report.LogInfo("Info", "'Manage COLT's Network - Manage Network' page not navigated","FAIL");
		Reporter.log("'Manage COLT's Network - Manage Network' page not navigated");
	}

	}else {
		Report.LogInfo("Info", "'View MAS Device' page not navigated","FAIL");
		Reporter.log("'View MAS Device' page not navigated");
		}
	
}

	
public void routerPanel_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String commandIPv4= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"MAS_CommandIPV4");
	String commandIPv6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"MAS_CommandIPV6");
	
	String vrfname_ipv4= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"MAS_vrf_Ipv4");
	String vrfname_ipv6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"MAS_vrf_Ipv6");
	
	scrollIntoTop();
	//

	String vendorModel=(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_VendorModelValue);
	String vendor=getTextFrom(vendorModel,"Vendor model");
	String manageAddress=(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_ManagementAddressValue);
	String ipAddress=getTextFrom(manageAddress,"management add");
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_VendorModelValue);
	//
	
	if(vendor.startsWith("Cisco")) {
		
	//Command IPV4	
		addDropdownValues_commonMethod("Command IPV4", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4CommandsDropdown , commandIPv4);
		
		hostnametextField_IPV4_MAS(commandIPv4, ipAddress);
		
		vrfNametextField_IPV4_MAS(commandIPv4, vrfname_ipv4);
		
		executeCommandAndFetchTheValue_MAS(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4Command_Executebutton);
		
		
	//Commmand IPV6
		addDropdownValues_commonMethod("Command IPV6", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV6CommandsDropdown, commandIPv6);
		
		hostnametextField_IPV6_MAS(commandIPv6, ipAddress);
		
		vrfNametextField_IPV6_MAS(commandIPv6, vrfname_ipv6);
		
		executeCommandAndFetchTheValue_MAS(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV6Command_Executebutton);
		
	}
	else if(vendor.startsWith("Juniper ")){
		
		//Command IPV4	
				addDropdownValues_commonMethod("Command IPV4", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4CommandsDropdown, commandIPv4);
				
				hostnametextField_IPV4_MAS(commandIPv4, ipAddress);
				
				vrfNametextField_IPV4_MAS(commandIPv4, vrfname_ipv4);
				
				executeCommandAndFetchTheValue_MAS(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4Command_Executebutton);
				
		
	}
	else {
		Report.LogInfo("Info", "Router Panel will not display for the selected vendorModel: "+vendorModel,"Info");
		Reporter.log("Router Panel will not display for the selected vendorModel: "+vendorModel);
	}
	

}
	
public void routerPanel_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	String commandIPv4= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PE_CommandIPV4");
	String commandIPv6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PE_CommandIPV6");
	
	String vrfname_ipv4= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PE_vrf_Ipv4");
	String vrfname_ipv6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"PE_vrf_Ipv6");
scrollIntoTop();
	
	
	String vendorModel=(APT_VOIPAccess_Obj.VOIPAccess.PE_View_VendorModelValue);
	String vendor=getTextFrom(vendorModel,"Vendor model");
	String manageAddress=APT_VOIPAccess_Obj.VOIPAccess.PE_View_ManagementAddressValue;
	String ipAddress=getTextFrom(manageAddress,"Management address");
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PE_View_VendorModelValue);
	
	
	if(vendor.startsWith("Cisco")) {
		
	//Command IPV4	
		addDropdownValues_commonMethod("Command IPV4", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4CommandsDropdown, commandIPv4);
		
		hostnametextField_IPV4_PE(commandIPv4, ipAddress);
		
		vrfNametextField_IPV4_PE(commandIPv4, vrfname_ipv4);
		
		executeCommandAndFetchTheValue_PE(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4Command_Executebutton);
		
		
	//Commmand IPV6
		addDropdownValues_commonMethod("Command IPV6", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV6CommandsDropdown , commandIPv6);
		
		hostnametextField_IPV6_PE(commandIPv6, ipAddress);
		
		vrfNametextField_IPV6_PE(commandIPv6, vrfname_ipv6);
		
		executeCommandAndFetchTheValue_PE(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV6Command_Executebutton);
		
	}
	else if(vendor.startsWith("Juniper ")){
		
		//Command IPV4	
				addDropdownValues_commonMethod("Command IPV4", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4Command_Executebutton , commandIPv4);
				
				hostnametextField_IPV4_PE(commandIPv4, ipAddress);
				
				vrfNametextField_IPV4_PE(commandIPv4, vrfname_ipv4);
				
				executeCommandAndFetchTheValue_PE(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4Command_Executebutton);
				
		
	}
	else {
		Report.LogInfo("Info", "Router Panel will not display for the selected vendorModel: "+vendorModel,"Info");
		Reporter.log("Router Panel will not display for the selected vendorModel: "+vendorModel);
	}
	

}
	
public void deleteServiceFunction(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException{
	
	String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ServiceIdentification");
	waitforPagetobeenable();
	try {
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
		click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown, "Action");
		
		click(APT_VOIPAccess_Obj.VOIPAccess.DeleteLink,"Delete Link");
		compareText("Delete Trunk Warning Message from View Trunk page", APT_VOIPAccess_Obj.VOIPAccess.DeleteServiceWarningMessage, "Are you sure that you want to delete this item?");
		click(APT_VOIPAccess_Obj.VOIPAccess.deletebutton,"Delete Button");
		waitforPagetobeenable();
		compareText("Delete Service Successful Message", APT_VOIPAccess_Obj.VOIPAccess.deletesuccessmsg, "Service successfully deleted.");
		
	
	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	   
}	

	
public void verifyAddMASswitch(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
	
	String MAS_IMSPOPLocation= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"MAS_IMSPOPLocation");
	
try {
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.ProviderEquipment_header);
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_AddMASSwitchLink,"Add MAS Switch Link");
		
		if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_AddMASSwitch_header)) {
			Report.LogInfo("Info", "'Add MAS Switch' page navigated as expected","PASS");
			Reporter.log("'Add MAS Switch' page navigated as expected");
			
		try {
		compareText("Add MAS Switch header", APT_VOIPAccess_Obj.VOIPAccess.MAS_AddMASSwitch_header, "Add MAS Switch");

		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_OKbutton,"OK button");//
		compareText("Add Switch Warning Message for MAS Switch", APT_VOIPAccess_Obj.VOIPAccess.MAS_AddSwitchWarningMessage, "IMS POP Location is required");//

		addDropdownValues_commonMethod("IMS POP Location", APT_VOIPAccess_Obj.VOIPAccess.MAS_IMSPOPLocationDropdown, MAS_IMSPOPLocation);
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_OKbutton,"OK button");
		compareText("Add Switch Successful Message for MAS Switch", APT_VOIPAccess_Obj.VOIPAccess.MAS_AddSwitchSuccessfulMessage, "MAS switch added successfully");
		Report.LogInfo("Info", "MAS switch added successfully","PASS");
		
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed in Add MAS Switch page","FAIL");
			Reporter.log(  e+ " : Field is not displayed in Add MAS Switch page");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed in Add MAS Switch page ","FAIL");
			Reporter.log(  e+" : Field is not displayed in Add MAS Switch page");
		}

		}else {
			Report.LogInfo("Info", "'Add MAS Switch' page not navigated","");
			Reporter.log("'Add MAS Switch' page not navigated");
		}
		
		
		Reporter.log("------ MAS Switch added successfully ------");
		
}catch(NoSuchElementException e) {
	e.printStackTrace();
	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	Reporter.log(  e+ " : Field is not displayed");
}catch(Exception e) {
	e.printStackTrace();
	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	Reporter.log(  e+" : Field is not displayed");
}

}




	
public void verifyAddedMASswitchInformation_View(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {

	String MAS_IMSPOPLocation =  DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_IMSPOPLocation");
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View MAS Switch' page navigated as expected","Info");
		Reporter.log("'View MAS Switch' page navigated as expected");
		
	try {
	// Verify All Device Information under Device panel for MAS Switch
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_DeviceNameValue,"Device Name");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_VendorModelValue,"Vendor/Model");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_ManagementAddressValue,"Management Address");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_SnmproValue,"Snmpro");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_CountryValue,"Country");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_CityValue,"City");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_SiteValue,"Site");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_PremiseValue,"Premise");
//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_TestColumnName,"Test");
//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_StatusColumnName,"Status");
//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_LastRefresh,"Last Refresh");
	
	waitforPagetobeenable();
	waitForAjax();
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in View MAS Switch page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in View MAS Switch page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in View MAS Switch page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in View MAS Switch page");
	}

	
	}else {
		Report.LogInfo("Info", "'View MAS Switch' page not navigated","FAIL");
		Reporter.log("'View MAS Switch' page not navigated");
	}
	
	
	Reporter.log("------ Verified Added MAS Switch Information successfully ------");
}
	
public void executeCommandAndFetchTheValue_MAS(String executeButton) throws InterruptedException, IOException {
	
	click(executeButton, "Execute");
	waitforPagetobeenable();
	waitToPageLoad();
boolean resultField=false;	
try {	
resultField=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_result_textArea);
if(resultField) {
	 Report.LogInfo("Info", "'Result' text field is displaying","PASS");
	Reporter.log( "'Result' text field is displaying");
	
	String remarkvalue=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_result_textArea,"MAS_PE_result_textArea");
	 Report.LogInfo("Info", "value under 'Result' field displaying as "+ remarkvalue,"PASS");
	Reporter.log("value under 'Result' field displaying as "+ remarkvalue);

}else {
	 Report.LogInfo("Info", "'Result' text field is not displaying","FAIL");
	Reporter.log( "'Result' text field is not displaying");
}
}catch(Exception e) {
e.printStackTrace();
 Report.LogInfo("Info", "'Result' text field is not displaying","FAIL");
Reporter.log("'Result' text field is not displaying");
}
	
}

public void executeCommandAndFetchTheValue_PE(String executeButton) throws InterruptedException, IOException {
	
	click(executeButton, "Execute");
	waitforPagetobeenable();
	waitToPageLoad();
boolean resultField=false;	
try {	
resultField=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_result_textArea);
if(resultField) {
	 Report.LogInfo("Info", "'Result' text field is displaying","PASS");
	Reporter.log( "'Result' text field is displaying");
	
	String remarkvalue=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_result_textArea,"MAS_PE_result_textArea");
	 Report.LogInfo("Info", "value under 'Result' field displaying as "+ remarkvalue,"PASS");
	Reporter.log("value under 'Result' field displaying as "+ remarkvalue);

}else {
	 Report.LogInfo("Info", "'Result' text field is not displaying","FAIL");
	Reporter.log( "'Result' text field is not displaying");
}
}catch(Exception e) {
e.printStackTrace();
 Report.LogInfo("Info", "'Result' text field is not displaying","FAIL");
Reporter.log("'Result' text field is not displaying");
}
	
}

public void hostnametextField_IPV6_MAS(String commandIPv6, String ipv6Address) {
	boolean IPV4availability=false;
	try {  
		IPV4availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_hostnameTextfield);
	  
	  if(IPV4availability) {
		  
		  sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_hostnameTextfield, ipv6Address, "IP Address or Hostname");
		  
	  }else {
		   Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"Info");
			Reporter.log("'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
	  }
	}catch(Exception e) {
		e.printStackTrace();
		
		 Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"Info");
		Reporter.log("'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
	}
}

public void hostnametextField_IPV6_PE(String commandIPv6, String ipv6Address) {
	boolean IPV4availability=false;
	try {  
		IPV4availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_hostnameTextfield);
	  
	  if(IPV4availability) {
		  
		  sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_hostnameTextfield, ipv6Address, "IP Address or Hostname");
		  
	  }else {
		   Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"Info");
			Reporter.log("'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
	  }
	}catch(Exception e) {
		e.printStackTrace();
		
		 Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6,"Info");
		Reporter.log("'Hostname or IpAddress' for 'IPV6' text field is not displaying for "+ commandIPv6);
	}
}


public void vrfNametextField_IPV6_MAS(String commandIPV6, String vrfname_IPV6) {
	boolean IPV6availability=false;
	
	if(commandIPV6.equalsIgnoreCase("vrf")) {
		try {  
			IPV6availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_vrfnameTextField);
			
			if(IPV6availability) {
				sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_vrfnameTextField, vrfname_IPV6, "Router Vrf Name");
			}else {
				 Report.LogInfo("Info", "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"Info");
				Reporter.log("'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
			}
		}catch(Exception e) {
			e.printStackTrace();
			 Report.LogInfo("Info", "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"Info");
			Reporter.log("'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
		}
	}
	else {
		 Report.LogInfo("Info", "'VRF Name IPv6' text field is not displaying for "+ commandIPV6,"PASS");
		Reporter.log("'VRF Name IPv6' text field is not displaying for "+ commandIPV6 +" command");
	}
	
}	

public void vrfNametextField_IPV6_PE(String commandIPV6, String vrfname_IPV6) {
	boolean IPV6availability=false;
	
	if(commandIPV6.equalsIgnoreCase("vrf")) {
		try {  
			IPV6availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_vrfnameTextField);
			
			if(IPV6availability) {
				sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv6_vrfnameTextField, vrfname_IPV6, "Router Vrf Name");
			}else {
				 Report.LogInfo("Info", "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"Info");
				Reporter.log("'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
			}
		}catch(Exception e) {
			e.printStackTrace();
			 Report.LogInfo("Info", "'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6,"Info");
			Reporter.log("'VRF Name' for 'IPv6' text field is not displaying for "+ commandIPV6);
		}
	}
	else {
		 Report.LogInfo("Info", "'VRF Name IPv6' text field is not displaying for "+ commandIPV6,"PASS");
		Reporter.log("'VRF Name IPv6' text field is not displaying for "+ commandIPV6 +" command");
	}
	
}	

public void hostnametextField_IPV4_MAS(String command_ipv4, String ipAddress) {
	boolean IPV4availability=false;
	try {  
		IPV4availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_hostnameTextfield);
	  
	  if(IPV4availability) {
		  
		  sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_hostnameTextfield, ipAddress, "IP Address or Hostname");
		  
	  }else {
		  Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4,"Info");
			Reporter.log("'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4);
	  }
	}catch(Exception e) {
		e.printStackTrace();
		
		Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4,"Info");
		Reporter.log("'Hostname or IpAddress' for 'Ipv4' text field is not displaying for "+ command_ipv4);
	}
}

public void hostnametextField_IPV4_PE(String command_ipv4, String ipAddress) {
	boolean IPV4availability=false;
	try {  
		IPV4availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_hostnameTextfield);
	  
	  if(IPV4availability) {
		  
		  sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_hostnameTextfield, ipAddress, "IP Address or Hostname");
		  
	  }else {
		  Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4,"Info");
			Reporter.log("'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4);
	  }
	}catch(Exception e) {
		e.printStackTrace();
		
		Report.LogInfo("Info", "'Hostname or IpAddress' for 'IPv4' text field is not displaying for "+ command_ipv4,"Info");
		Reporter.log("'Hostname or IpAddress' for 'Ipv4' text field is not displaying for "+ command_ipv4);
	}
}


public void vrfNametextField_IPV4_MAS(String command_ipv4, String vrfname_ipv4) {
	boolean IPV4availability=false;
	  
		
	if(command_ipv4.contains("vrf")) {
		try {
			IPV4availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_vrfnameTextField);
			
			if(IPV4availability) {
				sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_vrfnameTextField, vrfname_ipv4, "Router Vrf Name");
			}else {
				Report.LogInfo("Info", "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"FAIL");
				Reporter.log("'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
			}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"FAIL");
			Reporter.log("'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
		}
		
	}else {
		Report.LogInfo("Info", "'VRF Name IPv4' text field is not displaying for "+ command_ipv4,"PASS");
		Reporter.log("'VRF Name IPv4' text field is not displaying for "+ command_ipv4 +" command");
	}
}	

public void vrfNametextField_IPV4_PE(String command_ipv4, String vrfname_ipv4) {
	boolean IPV4availability=false;
	  
		
	if(command_ipv4.contains("vrf")) {
		try {
			IPV4availability=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_vrfnameTextField);
			
			if(IPV4availability) {
				sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_commandIPv4_vrfnameTextField, vrfname_ipv4, "Router Vrf Name");
			}else {
				Report.LogInfo("Info", "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"FAIL");
				Reporter.log("'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
			}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4,"FAIL");
			Reporter.log("'VRF Name' for 'IPv4' text field is not displaying for "+ command_ipv4);
		}
		
	}else {
		Report.LogInfo("Info", "'VRF Name IPv4' text field is not displaying for "+ command_ipv4,"PASS");
		Reporter.log("'VRF Name IPv4' text field is not displaying for "+ command_ipv4 +" command");
	}
}		
	
	
public void navigateToViewDevicePage_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String PE_DeviceName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_DeviceName");
	try {
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_AddMASSwitchLink);//TrunkGroupSiteOrders_header //ProviderEquipment_header //MASswitch_header//PE_AddPEDeviceLink
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.existingPEdevicegrid1))
		{
			click(APT_VOIPAccess_Obj.VOIPAccess.PE1, "View Link");

			if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_ViewDevice_Header)) {
				Report.LogInfo("Info", "'View PE Device' page navigated as expected","PASS");
				Reporter.log("'View PE Device' page navigated as expected");
			}else {
				Report.LogInfo("Info", "'View PE Device' page not navigated","FAIL");
				Reporter.log("'View PE Device' page not navigated");
			}
			
		}else{
			Report.LogInfo("Info", "No Device added in grid","FAIL");
		}
		
		
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
			Reporter.log(  e+ " : Field is not displayed");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
			Reporter.log(  e+" : Field is not displayed");
		}
}

public void navigateToDevicePageFromManageColtNetwork_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String MAS_DeviceName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_DeviceName");
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_DeviceValue)){

			   getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_DeviceValue,"Device Name in Manage Colt page under Status Panel");
			   click(APT_VOIPAccess_Obj.VOIPAccess.MAS_Manage_Status_DeviceValue, "Device Link ");
			   waitforPagetobeenable();
			   
	}else{
		Report.LogInfo("Info",  "VOIP ACCESS Device Link is not displayed under Manage Colt Network page","FAIL");
		Reporter.log("VOIP ACCESS Device Link is not displayed under Manage Colt Network page");
	}

}
   	
public void deleteTrunk(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String Trunk_ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	
	scrollIntoTop();
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_DeleteLink,"Delete DLink");
	click(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_DeleteLink,"Delete DLink");
	compareText("Delete Trunk Warning Message from View Trunk page", APT_VOIPAccess_Obj.VOIPAccess.ViewTrunk_DeleteWarningMessage, "Are you sure that you want to delete?");
	click(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_DeleteButton,"Delete Button");
	
	compareText("Delete Trunk Successful Message", APT_VOIPAccess_Obj.VOIPAccess.DeleteTrunkSuccessMessage, "Trunk deleted successfully");

}


public void verifyEditMASswitchFunction(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String MAS_DeviceName_Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_DeviceName_Edit");
	String MAS_VendorModelEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_VendorModelEdit");
	String MAS_ManagementAddressEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_ManagementAddressEdit");
	String MAS_SnmproEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_SnmproEdit");
	String MAS_CountryEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_CountryEdit");
	String MAS_CityEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_CityEdit");
	String MAS_SiteEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_SiteEdit");
	String MAS_PremiseEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_PremiseEdit");
	
	if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View MAS Device' page navigated as expected","PASS");
		Reporter.log("'View MAS Device' page navigated as expected");
		
		//click(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_ActionLink,"ACTION link");
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_Action_EditLink);
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_Action_EditLink,"Edit Link");
	
	if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.Edit_MASSwitch_Header)) {
		Report.LogInfo("Info", "'Edit MAS Device' page navigated as expected","PASS");
		Reporter.log("'Edit MAS Device' page navigated as expected");
		
		try { 	
		
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_Edit_PremiseLevel);
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_OKbutton,"OK in Edit MAS Device");
 waitforPagetobeenable();
//	compareText(application, "MAS Switch Update message", "MAS_UpdateSwitchSuccessfulMessage", "MAS switch updated successfully");
	verifysuccessmessage("MAS switch updated successfully");
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Edit MAS Device page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in Edit MAS Device page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in Edit MAS Device page ","Info");
		Reporter.log(  e+" : Field is not displayed in Edit MAS Device page");
	}

	}else {
		Report.LogInfo("Info", "'Edit MAS Switch' page not navigated","Info");
		Reporter.log("'Edit MAS Switch' page not navigated");	
	}
	
	Reporter.log("------  MAS switch updated successfully   ------");
	
	
	}else {
		Report.LogInfo("Info", "'View MAS Device' page not navigated","FAIL");
		Reporter.log("'View MAS Device' page navigated");
	}
}

public void verifyRouterToolFunction_CPE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String CPE_CommandIPV4= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_CommandIPV4");
	String CPE_CommandIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_CommandIPV6");
	String CPE_ManagementAddress= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_ManagementAddress");
	
	try {
	scrollIntoTop();
	
	
	webDriver.findElement(By.xpath("//a[text()='"+ ServiceIdentification +"']")).click();
	
	waitforPagetobeenable();
	
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.CPE_View);
	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View,"View Link For CPE Device under Trunk Device panel");
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.RouterTool_header);
	compareText("Router Tools header", APT_VOIPAccess_Obj.VOIPAccess.RouterTool_header, "Router Tools");
	
	addDropdownValues_commonMethod("Command IPV4", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4CommandsDropdown, CPE_CommandIPV4);	
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV4CommandTextfield, CPE_ManagementAddress, "Commands IPV4");
	executeCommandAndFetchTheValue_PE(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV4Command_Executebutton);
	
	
//Commmand IPV6
	addDropdownValues_commonMethod("Command IPV6", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV6CommandsDropdown , CPE_CommandIPV6);
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV6CommandTextfield, CPE_ManagementAddress, "Commands IPV6");
	executeCommandAndFetchTheValue_PE(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Router_IPV6Command_Executebutton);
	
	
	/*
	addDropdownValues_commonMethod("Commands IPV4",APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV4CommandsDropdown,CPE_CommandIPV4);
	//sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV4CommandTextfield,CPE_ManagementAddress, "Commands IPV4");
	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV4Command_Executebutton,"Execute button IPV4");
			
	addDropdownValues_commonMethod("Commands IPV6",APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV6CommandsDropdown,CPE_CommandIPV6);
	//sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV6CommandTextfield,CPE_ManagementAddress,"Commands IPV6");
	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_Router_IPV6Command_Executebutton,"Execute button IPV6");
	*/
	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	  
}

public void verifyDeleteDeviceFunction_CPE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	
	try {
	scrollIntoTop();
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_Device_Header)) {
		Report.LogInfo("Info", "'View CPE Device' page navigated as expected","PASS");
		Reporter.log("'View CPE Device' page navigated as expected");
		
	//Delete MAS Device from View Device Page
		waitforPagetobeenable();
//	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_ActionLink,"ACTION link");
	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_Action_DeleteLink,"Delete Device from View Device page");
	compareText("Delete PE Device Warning Message from View Device page", APT_VOIPAccess_Obj.VOIPAccess.CPE_ViewDevice_Action_DeletePEDeviceWarningMessage, "Are you sure that you want to delete this item?");
	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_ViewDevice_Action_DeleteButton,"Delete Button");
	waitforPagetobeenable();
	scrollIntoTop();
	compareText("Delete CPE Device Successful Message", APT_VOIPAccess_Obj.VOIPAccess.CPE_DeleteCPEDeviceSuccessMessage, "CPE Device deleted successfully");
	
	}else {
		Report.LogInfo("Info", "'View CPE Device' page not navigated","FAIL");
		Reporter.log("'View CPE Device' page not navigated");
	}
	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	    
	}

public void verifyConfigureInterfaceFunction_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String MAS_GenerateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_GenerateConfiguration");
	String MAS_Configuration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Configuration");
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.portalaccess_header);//ProviderEquipment_header//portalaccess_header//managementoptions_header
	//***click(application, "Show Interfaces Link", "MAS_ShowInterfaceLink");
	
	//click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Checkbox1_ViewServicePage,"Select Checkbox interface"); 
	//click(APT_VOIPAccess_Obj.VOIPAccess.MASSwitch_ACTION_Device1,"ACTION MAS Switch"); 
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ACTION_ConfigureLink_Device1,"Configure interface"); 
	
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_ConfigureInterface_header)) {
		Report.LogInfo("Info", "'MAS Select Interface' page navigated as expected","PASS");
		Reporter.log("'MAS Select Interface' page navigated as expected");
		
	
		try {
	// Verify All Device Information under Device panel for MAS Switch
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_DeviceNameValue,"Device Name");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_InterfaceValue,"Interface");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_LinkCircuitIdValue,"Link/Circuit Id");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_InterfaceAddressRangeValue,"Interface Address Range");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_InterfaceAddressMaskValue,"Interface Address/Mask");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_HSRPIPValue,"HSRP IP");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_GroupNumberValue,"Group Number");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_BearerTypeValue,"Bearer Type");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_BandWidthValue,"Band Width");
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_Configuration_header);
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_VLANIdValue,"VLAN Id");

	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configuration_label);
	
	
	addDropdownValues_commonMethod("Configuration", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_GenerateConfigurationDropdown, MAS_GenerateConfiguration);
	
	compareText("Generate Configuration for All CPE Routes Link", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_GenerateConfigurationForAllCPERoutesButton, "Generate Configuration for All CPE Routes");
	compareText("Save Configuration Link", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_SaveConfigurationButton, "Save Configuration");
	compareText("Execute Configuration on Device Link", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_ExecuteConfigurationonDeviceButton, "Execute Configuration on Device");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_ConfigurationTextfield,"Configuration information after configuration generated");
	Reporter.log("------ Verified Configure Interfaces Information successfully ------");
	
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed in Select Interface page","FAIL");
			Reporter.log(  e+ " : Field is not displayed in  Select Interface page");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed in  Select Interface page ","FAIL");
			Reporter.log(  e+" : Field is not displayed in  Select Interface page");
		}
	}else {
		Report.LogInfo("Info", "'MAS Select Interface' page not navigated","FAIL");
		Reporter.log("'MAS Select Interface' page not navigated");
	}
}

public void verifyDeleteInterfaceFunction_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	waitForAjax();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ACTION_ResetLink_Device1);//ProviderEquipment_header//portalaccess_header//managementoptions_header
//	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Checkbox1_ViewServicePage,"Select Checkbox interface"); 
//	click(APT_VOIPAccess_Obj.VOIPAccess.MASSwitch_ACTION_Device1,"ACTION MAS Switch"); 
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ACTION_ResetLink_Device1,"Delete interface link"); 
	
	compareText("Delete Interface Warning Message from View Service page", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_DeleteInterfaceWarningMessage, "Are you sure that you want to delete?");
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_DeleteButton,"Delete Button");
	waitforPagetobeenable();
	compareText("Delete Interface Successfull Message", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_DeleteInterfaceSuccessfullMessage, "Interface successfully removed from this service.");
}

public void verifyConfigureInterfaceFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String PE_GenerateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_GenerateConfiguration");

	String PE_Configuration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_Configuration");

	//Configure Interface - MAS Device from View Device Page
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrders_header);
	//click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Checkbox1_ViewServicePage,"Select Checkbox interface"); 
	//click(APT_VOIPAccess_Obj.VOIPAccess.PE_ACTION_Device1,"ACTION PE");
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ACTION_ConfigureLink_Device1,"Configure");  
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_ConfigureInterface_header,"Select Interfaces header");
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_ConfigureInterface_header)) {
		Report.LogInfo("Info", "'Configure Interface' page navigated as expected","PASS");
		Reporter.log("'Configure Interface' page navigated as expected");
	
		try {
	// Verify All Device Information under Device panel for MAS Switch
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_DeviceNameValue,"Device Name");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_InterfaceValue,"Interface");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_LinkCircuitIdValue,"Link/Circuit Id");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_InterfaceAddressRangeValue,"Interface Address Range");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_InterfaceAddressMaskValue,"Interface Address/Mask");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_VRRPIPValue,"VRRP IP");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_GroupNumberValue,"Group Number");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_BearerTypeValue,"Bearer Type");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_BandWidthValue,"Band Width");
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_Configuration_header);
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_VLANIdValue,"VLAN Id");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_VRFValue,"VRF Id");
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configuration_label);
	
	addDropdownValues_commonMethod("Configuration",APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_GenerateConfigurationDropdown,PE_GenerateConfiguration);
	//click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_GenerateConfigurationButton,"Generate Configuration Button");
	
	compareText("Generate Configuration for All CPE Routes Link", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_GenerateConfigurationForAllCPERoutesButton, "Generate Configuration for All CPE Routes");
	compareText("Save Configuration Link", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_SaveConfigurationButton, "Save Configuration");
	compareText("Execute Configuration on Device Link", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_ExecuteConfigurationonDeviceButton, "Execute Configuration on Device");
	
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configure_ConfigurationTextfield,"Configuration information after configuration generated");
	
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", "Field is not displayed in 'Configure Interface' page","FAIL");
			Reporter.log( "Field is not displayed in 'Configure Interface' page");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "Field is not displayed in 'Configure Interface' page","FAIL");
			Reporter.log( "Field is not displayed in 'Configure Interface' page");
		}
		
	}else {
		Report.LogInfo("Info", "'Configure Interface' page not navigated","FAIL");
		Reporter.log("'Configure Interface' page navigated");
	}
	
	Reporter.log("------ Verified Configure Interfaces Information successfully ------");
}


public void verifyDeleteInterfaceFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	//Delete Interface - MAS Device from View Service Page
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrders_header);
	//click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Checkbox1_ViewServicePage,"Select Checkbox interface"); 
	//click(APT_VOIPAccess_Obj.VOIPAccess.PE_ACTION_Device1,"ACTION MAS Switch");
	
	click(APT_VOIPAccess_Obj.VOIPAccess.PE_Delete_Interface,"Delete Button");  
			
	compareText("Delete Interface Warning Message from View Service page", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_DeleteInterfaceWarningMessage, "Are you sure that you want to delete?");
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_DeleteButton,"Delete Button");
	//waitforPagetobeenable();
	//compareText("Delete Interface Successfull Message", APT_VOIPAccess_Obj.VOIPAccess.PE_DeleteInterfaceSuccessfullMessage, "Interface deleted successfully");
	
}

public void verifyDeleteDeviceFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	//Delete MAS Device from View Device Page
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrders_header);
	click(APT_VOIPAccess_Obj.VOIPAccess.PE1,"View Link"); 
	
	scrollIntoTop();
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View PE Device' page navigated as expected","PASS");
		Reporter.log("'View PE Device' page navigated as expected");

		
	//click(APT_VOIPAccess_Obj.VOIPAccess.PE_View_ActionLink,"ACTION link");
	click(APT_VOIPAccess_Obj.VOIPAccess.PE_View_Action_DeleteLink,"Delete Device from View Device page");
	compareText("Delete PE Device Warning Message from View Device page", APT_VOIPAccess_Obj.VOIPAccess.PE_ViewDevice_Action_DeletePEDeviceWarningMessage, "Are you sure that you want to delete this item?");
	click(APT_VOIPAccess_Obj.VOIPAccess.PE_ViewDevice_Action_DeleteButton,"Delete Button");
	waitforPagetobeenable();
compareText("Delete MAS Device Successful Message", APT_VOIPAccess_Obj.VOIPAccess.PE_DeletePEDeviceSuccessfulMessage, "PE Device deleted successfully");
	
	
	
	//Delete from Service "From View Service page"
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PE_AddPEDeviceLink);
	click(APT_VOIPAccess_Obj.VOIPAccess.deviceDeleteFromService,"Delete From Service Link");
	compareText("Delete PE Device Warning Message from View Service page", APT_VOIPAccess_Obj.VOIPAccess.PE_ViewService_DeletePEDeviceWarningMessage, "Are you sure that you want to delete?");
	click(APT_VOIPAccess_Obj.VOIPAccess.PE_ViewService_DeleteButton,"Delete Button");
	waitforPagetobeenable();
	compareText("Delete PE Device Successfull Message", APT_VOIPAccess_Obj.VOIPAccess.PE_DeletePEDeviceSuccessfulMessage, "PE Device deleted successfully");
	
	}else {
		Report.LogInfo("Info", "'View PE Device' page not navigated","FAIL");
		Reporter.log("'View PE Device' page navigated");
	}


}

public void verifyDRPlansBulkInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String bulkjob_filepath= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "bulkjob_filepath");

	String ExistingTrunkName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingTrunkName");

try {
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.customizedmedia_header);
	
	click(APT_VOIPAccess_Obj.VOIPAccess.recovery_header, "Disaster Recovery Header");
	
	click(APT_VOIPAccess_Obj.VOIPAccess.DRplans_bulkinterface_link, "DR Plans Bulk Interface");
	
	waitforPagetobeenable();
	compareText("Bulk Interface Header", APT_VOIPAccess_Obj.VOIPAccess.bulkinterfaceheader, "Bulk Interface");
	try {
	String BulkJob_Choosefile_button= APT_VOIPAccess_Obj.VOIPAccess.bulkjob_choosefilebutton;
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.bulkjob_choosefilebutton,bulkjob_filepath,"File path");
	click(APT_VOIPAccess_Obj.VOIPAccess.bulkjobsubmit, "Submit");
	}
	catch (Exception e) {
		Report.LogInfo("Info", "Step : Upload file path '"+bulkjob_filepath+"' is not found","FAIL");
	}
	//Archive link in bulk interface page
	click(APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_actiondropdown, "Action dropdown");
	click(APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_archivelink, "Archive");
	
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.archive_backbutton);
	click(APT_VOIPAccess_Obj.VOIPAccess.archive_backbutton, "Back");

	//Refresh link in bulk interface page
	scrollIntoTop();
	click(APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_actiondropdown, "Action dropdown");
	click(APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_refreshlink, "Refresh");
	waitforPagetobeenable();
	compareText("Bulk Interface Header", APT_VOIPAccess_Obj.VOIPAccess.bulkinterfaceheader, "Bulk Interface");
	Report.LogInfo("Info", "Step : Bulk Interface page refresh successful","PASS");
	Reporter.log("Bulk Interface page refresh successful");
	

	addedBulkInterface(ExistingTrunkName);
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_backbutton);
	click(APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_backbutton, "Back");
	waitforPagetobeenable();
}catch(NoSuchElementException e) {
	e.printStackTrace();
	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	Reporter.log(  e+ " : Field is not displayed");
}catch(Exception e) {
	e.printStackTrace();
	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	Reporter.log(  e+" : Field is not displayed");
}

}

public void trunkHistory(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String TrunkGroupSiteOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");
	String ExistingTrunkName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingTrunkName");
	
	try {
	scrollIntoTop();
	waitToPageLoad();
	boolean header =webDriver.findElement(By.xpath("(//div//*[contains(text(),'')])[2]")).isDisplayed();
	if(header) {
		Report.LogInfo("Info", "'View Trunk' Page displayed as expected","PASS");
		Reporter.log("'View Trunk' Page displayed as expected");
		
		//Action button	
	//	click(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_ActionButton, "Action");
		//click on History link
		click(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_HistoryLink, "History");

		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.HistoryPage_Header)) {
			Report.LogInfo("Info", "'History Trunk' Page displayed as expected","PASS");
			Reporter.log("'History Trunk' Page displayed as expected");
		
			try {
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Rivision_columnName, "Rivision Column");	
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.RestoredForm_columnName, "Restored Form column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.RevisionType_columnName, "Revision Type_column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Date_columnName, "Date column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.LastModifiedBy_columnName, "Last Modified By column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupName_columnName, "Trunk Group Name column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Gateway_columnName, "Gateway column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.VoIPProtocol_columnName, "VoIPProtocol column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.SignallingTransportProtocol_columnName, "Signalling Transport Protocol column");
		//**verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Action_columnName, "Action column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.BackButton_History, "Back Button");

			}catch(NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", e+ " : Field is not displayed in History page","FAIL");
				Reporter.log(  e+ " : Field is not displayed in History page");
			}catch(Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info",  e+" : Field is not displayed in History page ","FAIL");
				Reporter.log(  e+" : Field is not displayed in History page");
			}

		}else {
		Report.LogInfo("Info", "'History Trunk' Page is not navigated as expected","FAIL");
		Reporter.log("'History Trunk' Page is not navigated as expected");
		  }
	
	}else {
		Report.LogInfo("Info", "'View Trunk' Page is not navigated as expected","FAIL");
		Reporter.log("'View Trunk' Page is not navigated as expected");
	}
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	
	
	
}

public void deleteSBC_manualExecutionConfig() throws InterruptedException, IOException, IOException {
	try {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	
	click(APT_VOIPAccess_Obj.VOIPAccess.SBC_selectCreatedValue, "SBCcreated");
	
	click(APT_VOIPAccess_Obj.VOIPAccess.SBCManualConfig_actionDropdown, "Action");  //click acton dropdown
	click(APT_VOIPAccess_Obj.VOIPAccess.SBC_deleteLink, "Delete link");   //click on edit link
	
	 boolean DeleteAlertPopup= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.delete_alertpopup);
     if(DeleteAlertPopup)
     {
   	 String deletPopUpMessage= getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.DeleteMessae_PSX_GSX_SBC,"DeleteMessae_PSX_GSX_SBC");
   	 Report.LogInfo("Info", "Delete Pop up message displays as: "+ deletPopUpMessage,"PASS");
   	 
     click(APT_VOIPAccess_Obj.VOIPAccess.DeleteButton_PSX_GSX_SBC, "Delete");
     waitforPagetobeenable();
     
     scrollIntoTop();
        compareText("SBC Manual Configuration deleted Successfully", APT_VOIPAccess_Obj.VOIPAccess.SBC_DeleteManualConfiguration_SuccessMessage, "Deleted SBC manual config successfully.");
     }
     else
     {
           Reporter.log("Delete alert popup is not displayed");
           Report.LogInfo("Info", "Step : Delete alert popup is not displayed","FAIL");
     }
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	

}

public void verifydownloadDRplans(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String DRPlansfilename= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DRPlansfilename");
	String browserfiles_downloadspath= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "browserfiles_downloadspath");
	try {
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.customizedmedia_header);
	
	click(APT_VOIPAccess_Obj.VOIPAccess.recovery_header, "Disaster Recovery Header");
	
	click(APT_VOIPAccess_Obj.VOIPAccess.downloadDRplans_link, "Download DR Plans");
	
	waitforPagetobeenable();
	isFileDownloaded(DRPlansfilename, browserfiles_downloadspath);
	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	    
}

public void addedBulkInterface(String ExistingTrunkName) throws InterruptedException, IOException {

	compareText("TrunkGroup Name", APT_VOIPAccess_Obj.VOIPAccess.trunkgroupname_columnheader, "TrunkGroup Name");
	compareText("User Name", APT_VOIPAccess_Obj.VOIPAccess.username_columnheader, "User Name");
	compareText("Submit Time", APT_VOIPAccess_Obj.VOIPAccess.submittime_columnheader, "Submit Time");
	compareText("Start Time", APT_VOIPAccess_Obj.VOIPAccess.starttime_columnheader, "Start Time");
	compareText("End Time", APT_VOIPAccess_Obj.VOIPAccess.endtime_columnheader, "End Time");
	compareText("Status", APT_VOIPAccess_Obj.VOIPAccess.status_columnheader, "Status");
	compareText("Completion (%)", APT_VOIPAccess_Obj.VOIPAccess.completion_columnheader, "Completion (%)");
	compareText("Log", APT_VOIPAccess_Obj.VOIPAccess.log_columnheader, "Log");
	compareText("Upload File",APT_VOIPAccess_Obj.VOIPAccess.uploadfile_columnheader, "Upload File");

	int TotalPages;
	String TotalPagesText = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbTotal']")).getText();
	TotalPages = Integer.parseInt(TotalPagesText);
	Reporter.log("Total number of pages in table is: " + TotalPages);

	if (TotalPages != 0) {

		outerloop:
			for (int k = 1; k <= TotalPages; k++) {
				String AddedDRplan_BulkInterface= findWebElement(APT_VOIPAccess_Obj.VOIPAccess.addedbulkinterface_tablelist).getAttribute("style");
				if(!AddedDRplan_BulkInterface.contains("height: 1px"))
				{
					List<WebElement> results = findWebElements((APT_VOIPAccess_Obj.VOIPAccess.addedbulkinterface_trunkgroupname).replace("value", ExistingTrunkName));

					int numofrows = results.size();
					Reporter.log("no of results: " + numofrows);

					if ((numofrows == 0)) {

						Report.LogInfo("Info", "Bulk Interface table is empty","PASS");
					}
					else {
						// Current page
						String CurrentPage = webDriver.findElement(By.xpath("//div[@class='ag-div-margin row']//div//span[@ref='lbCurrent']")).getText();
						int Current_page = Integer.parseInt(CurrentPage);
						Reporter.log("The current page is: " + Current_page);

						Reporter.log("Currently we are in page number: " + Current_page);
						for (int i = 0; i < numofrows; i++) {
							try {

								String AddedBulkInterfacedata = results.get(i).getText();
								Reporter.log(AddedBulkInterfacedata);
								if (AddedBulkInterfacedata.equalsIgnoreCase(ExistingTrunkName)) {

									String BulkInterfaceRowID= getAttributeFrom((APT_VOIPAccess_Obj.VOIPAccess.addedbulkinterface_rowid).replace("value", ExistingTrunkName),"row-id");
									System.out.println("Bulk Interface row id: "+BulkInterfaceRowID);
									String BulkInterface_Trunkgroupname= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_trunkgroupname_value).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : Trunk Group Name column value:"+BulkInterface_Trunkgroupname,"PASS");
									String BulkInterface_Username= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_username_value).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : User Name column value:"+BulkInterface_Username,"PASS");
									String BulkInterface_SubmitTime= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_submittime_value).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : Submit Time column value:"+BulkInterface_SubmitTime,"PASS");
									String BulkInterface_startTime= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_starttime_value).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : Start Time column value:"+BulkInterface_startTime,"PASS");
									String BulkInterface_endTime= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_endtime_value).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : End Time column value:"+BulkInterface_endTime,"PASS");
									String BulkInterface_status= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_status_value).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : Status column value:"+BulkInterface_status,"PASS");
									String BulkInterface_completionPercentage= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_completion_value).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : Completion Percentage column value:"+BulkInterface_completionPercentage,"PASS");
									String BulkInterface_Log= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_log).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : Log column value:"+BulkInterface_Log,"PASS");
									String BulkInterface_Uploadfile= getTextFrom((APT_VOIPAccess_Obj.VOIPAccess.bulkinterface_file).replace("value", BulkInterfaceRowID));
									Report.LogInfo("Info", "Step : Upload file column value:"+BulkInterface_Uploadfile,"PASS");
									break outerloop;
								}

							} catch (StaleElementReferenceException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();

							}
						}
						
					}
				}
				else
				{
					Report.LogInfo("Info", "No Bulk Interface added in table","FAIL");
				}
			}

	}else {

		Reporter.log("No data available in table");
		Reporter.log("No data available in table");
		Report.LogInfo("Info", "No data available in table","FAIL");
	}
}


public void trunkPSXQueue(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String TrunkGroupSiteOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");
	String ExistingTrunkName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingTrunkName");
	
try {
	 
	 scrollIntoTop();
	waitToPageLoad();
	boolean header =webDriver.findElement(By.xpath("(//div//*[contains(text(),'')])[2]")).isDisplayed();
	if(header) {
		Report.LogInfo("Info", "'View Trunk' Page displayed as expected","PASS");
		Reporter.log("'View Trunk' Page displayed as expected");
		
		//Action button	
	//	click(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_ActionButton, "Action");
		//click on History link
		click(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_PSXQueueLink, "PSX Queue Link");

		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PSXQueuePage_Header)) {
			Report.LogInfo("Info", "'PSX Queue Trunk' Page displayed as expected","PASS");
			Reporter.log("'PSX Queue Trunk' Page displayed as expected");
		
			try {
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.LastStartTime_columnName, "Last Start Time Column");	
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Customer_columnName, "Customer column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Service_columnName, "Service column");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.TrunkName_columnName, "Trunk Name column");

			}catch(NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", e+ " : Field is not displayed in PSX Queue page","FAIL");
				Reporter.log(  e+ " : Field is not displayed in PSX Queue page");
			}catch(Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info",  e+" : Field is not displayed in PSX Queue page ","FAIL");
				Reporter.log(  e+" : Field is not displayed in PSX Queue page");
			}

		}else {
		Report.LogInfo("Info", "'PSX Queue' Page is not navigated as expected","FAIL");
		Reporter.log("'PSX Queue' Page is not navigated as expected");
		  }
	
	}else {
		Report.LogInfo("Info", "'View Trunk' Page is not navigated as expected","FAIL");
		Reporter.log("'View Trunk' Page is not navigated as expected");
	}
}catch(NoSuchElementException e) {
	e.printStackTrace();
	Report.LogInfo("Info", e+ " : Field is not displayed in PSX Queue page","FAIL");
	Reporter.log(  e+ " : Field is not displayed in PSX Queue page");
}catch(Exception e) {
	e.printStackTrace();
	Report.LogInfo("Info",  e+" : Field is not displayed in PSX Queue page ","FAIL");
	Reporter.log(  e+" : Field is not displayed in PSX Queue page");
}

	
}

public void viewTrunk_PSX_executeConfiguration(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String expectedConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "viewtrunk_PSXconfiguration");
	
	try {
	waitToPageLoad();
	waitforPagetobeenable();
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.DDIRange_Header);
	
	
	addDropdownValues_commonMethod("PSX Configuration",  APT_VOIPAccess_Obj.VOIPAccess.PSXconfigurationDropdown_viewtrunk, expectedConfiguration);
	
     
     try {
     if(expectedConfiguration.equalsIgnoreCase("Delete Trunk Group")) {
    	 //compareText(application, "PSX config sucess Message", "PSXconfig_WarningMessage", "PSX sync could not be started because this Trunk Group is already Queued / Processing or 3 step PSX sync is not completed.", xml);
    	// verifysuccessmessage("PSX sync started successfully.");
     }else if(expectedConfiguration.equalsIgnoreCase("Add Destination IP Address")) {
    	// String alertMessage1=alert.getText();
    	
     }
     }catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed ","FAIL");
			Reporter.log(  e+ " : Field is not displayedpage");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed ","FAIL");
			Reporter.log(  e+" : Field is not displayed");
		}
     
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	

}

public void viewTrunk_SBC_executeConfiguration(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String expectedConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "viewtrunk_SBCconfiguration");

	
	try {
	
	Reporter.log("expected value "+ expectedConfiguration);
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.DDIRange_Header);
	
	
	addDropdownValues_commonMethod("SBC Configuration", APT_VOIPAccess_Obj.VOIPAccess.SBCconfigurationDropdown_viewtrunk, expectedConfiguration);
	
	click(APT_VOIPAccess_Obj.VOIPAccess.viewTrunk_SBC_executeButton , "Execute");
	
	 Alert alert = webDriver.switchTo().alert();		
		
     // Capturing alert message.    
       String alertMessage= webDriver.switchTo().alert().getText();
       if(alertMessage.isEmpty()) {
    	   Report.LogInfo("Info", "No mEssage displays","FAIL");
	       Reporter.log("No Message displays"); 
       }else {
    	   Report.LogInfo("Info", "Alert message displays as: "+alertMessage,"PASS");
	       Reporter.log("text message for alert displays as: "+alertMessage);
       }
       
       waitforPagetobeenable();
     
       //TODO 
     try {  
       alert.accept();
       
       
       Reporter.log("accept 2");
       alert.accept();
     }catch(Exception e) {
    	 e.printStackTrace();
     }

     
     //fetch success Message
     if(expectedConfiguration.equalsIgnoreCase("Cease Trunk Group")) {
    	 verifysuccessmessage("SBC sync started successfully. Please check the sync status of this Trunk Group.");
     }else if(expectedConfiguration.equalsIgnoreCase("Synchronize All")) {
    	 verifysuccessmessage("SBC sync has not started because of the manual change in the SBC. Uncheck the 'Manual Configuration' flag "
					+ "to overwrite the changes.");
     }
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	
}

public void viewTrunk_GSX_executeConfiguration(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String expectedConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "viewtrunk_GSXconfiguration");

	
	try {
	Reporter.log("expected value "+ expectedConfiguration);
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.DDIRange_Header);
	
	
	addDropdownValues_commonMethod("GSX Configuration", APT_VOIPAccess_Obj.VOIPAccess.GSXconfigurationDropdown_viewtrunk, expectedConfiguration);
	
	click(APT_VOIPAccess_Obj.VOIPAccess.viewTrunk_GSX_generateConfigurationButton, "Generate Configuration");
	
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
    Iterator<String> itr = allwindows.iterator();
    while(itr.hasNext())
    {
          String childWindow = itr.next();
          if(!mainWindow.equals(childWindow)){
                webDriver.switchTo().window(childWindow);
                Reporter.log(webDriver.switchTo().window(childWindow).getTitle());
                  
                
                
              String gsxConfiguredValue = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.GSXcongig_textArea);
              if(gsxConfiguredValue.isEmpty()) {
            	  Report.LogInfo("Info", "No values displaying under GSX Configuration","PASS");
              }else {
            	  Report.LogInfo("Info", "'Under GSX Configuration' value is displaying as: "+ gsxConfiguredValue,"PASS");
              }
//               Write here  whatever you want to do and perform
                Reporter.log("came inside child window");
                
               //
                ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
                
                click(APT_VOIPAccess_Obj.VOIPAccess.GSX_config_executeButton, "Execute");
          }
    }
    
   waitforPagetobeenable();
   webDriver.close();
    webDriver.switchTo().window(mainWindow);
    

    scrollIntoTop();
   String gsxSuccessMesage= getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.GSXconfig_sucessMessage);
   if(gsxSuccessMesage.isEmpty()) {
	   Report.LogInfo("Info", "NO message displays after clicking on 'Execute' button","FAIL");
   }
   else {
	   Report.LogInfo("Info", "After clicking on 'Execute' button, success Message displays as: "+gsxSuccessMesage,"PASS");
   }
   
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	
}

public void addSBC_manualExecutionConfig(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String manualConfigurationValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SBCmanualConfigvalue");


	
	try {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	
	boolean SBCHeder=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.SBCmanualConfig_PanelHeader);
	if(SBCHeder) {
		Report.LogInfo("Info", "'SBC Manully Executed Configuration' panel is displaying","PASS");
		Reporter.log("'SBC Manully Executed Configuration' panel is displaying");
		
		
		click(APT_VOIPAccess_Obj.VOIPAccess.SBCManualConfig_actionDropdown, "Action");
		click(APT_VOIPAccess_Obj.VOIPAccess.SBC_addLink, "Add link");
		
		//TODO compare Header name
		
		String mainWindow=webDriver.getWindowHandle();
		Set<String> allwindows = webDriver.getWindowHandles();
	    Iterator<String> itr = allwindows.iterator();
	    while(itr.hasNext())
	    {
	          String childWindow = itr.next();
	          if(!mainWindow.equals(childWindow)){
	                webDriver.switchTo().window(childWindow);
	                Reporter.log(webDriver.switchTo().window(childWindow).getTitle());
	                  
	                
	                
	              String psxConfiguredValue = getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.manualConfiguration_textArea);
	              if(psxConfiguredValue.isEmpty()) {
	            	  Report.LogInfo("Info", "No values displaying under SBC Manually Executed Configurations","PASS");
	              }else {
	            	  Report.LogInfo("Info", "'Under SBC Manually Executed Configurations' value is displaying as: "+ psxConfiguredValue,"PASS");
	              }
//	               Write here  whatever you want to do and perform
	                Reporter.log("came inside child window");
	                
	            	//Text Area	
	                sendKeys(APT_VOIPAccess_Obj.VOIPAccess.manualConfiguration_textArea, manualConfigurationValue,  "SBC Manual Configuration");	 
	                ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	        		click(APT_VOIPAccess_Obj.VOIPAccess.saveButton_manualConfiguration, "Save");   //click on Save buttton
	        		
	        			                
	          }
	    }
	    
	    waitforPagetobeenable();
	    //**webDriver.close();//Automatically getting closed
	     webDriver.switchTo().window(mainWindow);
	     

	     scrollIntoTop();
	    compareText("SBC Manual Configuration added Successfully", APT_VOIPAccess_Obj.VOIPAccess.AddManualConfiguration_SuccessMessage, "Manual Configuration added Successfully");
	    
	}else {
		Report.LogInfo("Info", "'SBC Manully Executed Configuration' panel is not displaying","FAIL");
		Reporter.log("'SBC Manully Executed Configuration' panel is not displaying");
	}
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	
}


public void verifySBCfileAdded() throws InterruptedException, IOException, IOException {
	
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	
	String filename=(APT_VOIPAccess_Obj.VOIPAccess.SBC_selectCreatedValue);
	String SBCfilenameCreated=getTextFrom(filename);
	
	if(SBCfilenameCreated.isEmpty()) {
		Report.LogInfo("Info", "SBC Manually Executed configuration file name is not displaying","FAIL");
		Reporter.log("SBC Manually Executed configuration file name is not displaying");
	}else {
		
		int i=0; int k=0; int j=0;
		
	//Fetching Column Names	
		List<WebElement> columns=webDriver.findElements(By.xpath(APT_VOIPAccess_Obj.VOIPAccess.SBC_columnNames));
		int listOfColumns=columns.size();
		
		String columnName[]=new String[listOfColumns];
		for (WebElement column : columns) {
			
			columnName[k]=column.getText();
			k++;
		}
		
		Report.LogInfo("Info", "column names display as: "+ columnName[1] + "  " + columnName[2],"Info");  //printing column Names
		
		
	//Fetching value under File Column	
		List<WebElement> files=webDriver.findElements(By.xpath(APT_VOIPAccess_Obj.VOIPAccess.SBC_filenames));
		int listOfFiles=files.size();
		
		String[] fileNameValues=new String[listOfFiles];
		
		for(WebElement filenametransfer : files) {
			
			fileNameValues[i] = filenametransfer.getText();
			i++;
		}
		
	
	//Fetching value under Date Column
		List<WebElement> dates=webDriver.findElements(By.xpath(APT_VOIPAccess_Obj.VOIPAccess.SBC_dates));
		int listOfDates=dates.size();
		
		String[] dateValues=new String[listOfDates];
		
		for(WebElement dateList : dates) {
			
			dateValues[j] = dateList.getText();
			j++;
		}
	
	
		Report.LogInfo("Info", "Values under 'SBC Manually Executed Configuration' value displays as: ","Info");
		
	for(int y=0; y<fileNameValues.length; y++) {
	Report.LogInfo("Info",  fileNameValues[y] +"      "+ dateValues[y],"PASS");
	}
	
	}
}

public void editSBC_manualExecutionConfig(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException {
	
	String editGSXmanualConfigvalur= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editSBCmanualConfigvalue");

	try {
	
	click(APT_VOIPAccess_Obj.VOIPAccess.SBC_selectCreatedValue, "PSXcreated");
	
	click(APT_VOIPAccess_Obj.VOIPAccess.SBCManualConfig_actionDropdown, "Action");  //click acton dropdown
	click(APT_VOIPAccess_Obj.VOIPAccess.SBC_editLink, "Edit link");   //click on edit link
	
	
	waitforPagetobeenable();
	
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
    Iterator<String> itr = allwindows.iterator();
    while(itr.hasNext())
    {
          String childWindow = itr.next();
          if(!mainWindow.equals(childWindow)){
                webDriver.switchTo().window(childWindow);
                Reporter.log(webDriver.switchTo().window(childWindow).getTitle());
                  
                
                
//               Write here  whatever you want to do and perform
                Reporter.log("came inside child window");
                
                sendKeys("GSX manual Configuration", APT_VOIPAccess_Obj.VOIPAccess.GSX_editPage_teaxtArea, editGSXmanualConfigvalur);
               
                ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
                
                click(APT_VOIPAccess_Obj.VOIPAccess.GSX_editPage_SaveButton, "Save");
                
          }
    }
    
    webDriver.switchTo().window(mainWindow);
    
//    compareText(application, "Update SBC Manual Configuration Successfully", "UpdateManualConfiguration_SuccessMessage", "Manual Configuration updated Successfully", xml);
    verifysuccessmessage("Manual Configuration updated Successfully");
	}catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	    

    
}

public void verifyAddCPEDeviceFunction(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws Exception {
	
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String TrunkGroupSiteOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");

	String ExistingTrunkName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingTrunkName");

	
	String AddNewCPEDevice= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddNewCPEDevice");

	String AddExistingCPEDevice= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddExistingCPEDevice");
 
	String  CPE_RouterID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_RouterID");

	String  CPE_DeviceName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_DeviceName");

	String  CPE_VendorModel= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_VendorModel");

	String   CPE_ManagementAddress= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_ManagementAddress");

	String  CPE_Snmpro= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Snmpro");

	String  CPE_Snmprw = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Snmprw");

	String   CPE_SnmpV3ContextName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3ContextName");

	String   CPE_SnmpV3ContextEngineID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3ContextEngineID");

	String  CPE_SnmpV3SecurityUsername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3SecurityUsername");

	String  CPE_SnmpV3AuthProto = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3AuthProto");

	String   CPE_SnmpV3AuthPassword= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3AuthPassword");

	String   CPE_Country= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Country");

	String   CPE_City= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_City");

	String CPE_Site = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Site");

	String CPE_Premise= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Premise");

	boolean TrunkNameToAddCPEDeviceText=false;
	boolean trunkgrupOrderErrMsg= false;
	boolean TrunkGroupSiteOrderNumberText=false;
	WebElement Trunknumber1=null;
	
	waitforPagetobeenable();
	try {
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.addTrunkSiteOrderlink);
	
		
	//// OR
	//***TrunkNameToAddCPEDeviceText= isElementPresent(xml.getlocator("//locators/" +application+ "/selectTrunkName").replace("value", ExistingTrunkName));
			
		webDriver.findElement(By.xpath("(//tr//td[contains(.,'')]//following-sibling::td//a[text()='Add CPE Device'])")).click();;
	
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.AddCPEDevice_header)) {
		Report.LogInfo("Info", "'Add CPE Device' navigated as expected","PASS");
		Reporter.log("'Add CPE Device' navigated as expected");
		
	if(AddNewCPEDevice.equalsIgnoreCase("Yes")) {
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_OKButton,"OK Button");
		
		try { 
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_Country,"Country");
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_City,"City");
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_Site,"Site");
		scrollIntoTop();
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_RouterID,"Router ID");	
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_DeviceName,"Device Name");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_VendorModel,"Vendor Model");
		//*verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_ManagementAddress,"Management Address");//Issues reported
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_WarningMessage_Snmpro,"Snmpro");
		
		
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_RouterIdTextfield,CPE_RouterID, "Router ID");
		//**EnterTextValue(application, CPE_DeviceName, "Device Name", "CPE_DeviceNameTextfield");//Bydefault
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.DeviceName,CPE_DeviceName,"Device Name");
		
		addDropdownValues_commonMethod("Vendor/Model",APT_VOIPAccess_Obj.VOIPAccess.CPE_VendorModelDropdown,CPE_VendorModel); 
		webDriver.findElement(By.xpath("//span[contains(text(),'"+CPE_VendorModel+"')]")).click();

		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_ManagementAddressTextfield,CPE_ManagementAddress,"Management Address");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_SnmproTextfield,CPE_Snmpro,"Snmpro");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_SnmprwTextfield,CPE_Snmprw,"Snmprw");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_Snmpv3ContextNameTextfield,CPE_SnmpV3ContextName,"Snmp v3 Context Name");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_snmpv3ContextEngineIdTextfield,CPE_SnmpV3ContextEngineID,"Snmp V3 Context Engine ID");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_snmpv3SecurityUserNameTextfield,CPE_SnmpV3SecurityUsername,"Snmp V3 Security Username");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.CPE_snmpv3AuthPasswordTextfield, CPE_SnmpV3AuthPassword,"Snmp V3 Auth Password");
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.CPE_CountryDropdown,CPE_Country,"Country");
		selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.CPE_CityDropdown,CPE_City,"City");
		selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.CPE_SiteDropdown,CPE_Site,"Site");

		//**SelectDropdownValueUnderSelectTag(application, "Premise", CPE_Premise, "CPE_PremiseDropdown", "commonDropdownValueTag");

		click(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton,"OK Button");
		
//		compareText(application, "Add CPE Device Success Message", "CPE_AddCPEDeviceSuccessMessage", "CPE Device added successfully");
		verifysuccessmessage("CPE Device added successfully");
		}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Add CPE Device page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in Add CPE Device page");
		}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in Add CPE Device page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in Add CPE Device page");
		}

		
			}else {
			Report.LogInfo("Info", "'Add CPE Device' page not navigated","FAIL");
			Reporter.log("'Add CPE Device' page not navigated");
				  }
		
			}else {
			Report.LogInfo("Info", "Expected 'Trunk Name' is not displaying as expected under Trunk Panel in 'view Service' page","FAIL");
			Reporter.log("Expected 'Trunk Name'' is not displaying as expected under Trunk Panel in 'view Service' page");
					}
			
	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	    
}

public void verifyAddedCPEDeviceInformation_View(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String TrunkGroupSiteOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");

	String ExistingTrunkName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ExistingTrunkName");

	String AddNewCPEDevice= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddNewCPEDevice");

	String AddExistingCPEDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddExistingCPEDevice");

	String  CPE_RouterID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_RouterID");

	String  CPE_DeviceName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_DeviceName");

	String  CPE_VendorModel= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_VendorModel");

	String   CPE_ManagementAddress= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_ManagementAddress");

	String  CPE_Snmpro= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Snmpro");

	String  CPE_Snmprw = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Snmprw");

	String   CPE_SnmpV3ContextName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3ContextName");

	String   CPE_SnmpV3ContextEngineID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3ContextEngineID");

	String  CPE_SnmpV3SecurityUsername   = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3SecurityUsername");

	String  CPE_SnmpV3AuthProto= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3AuthProto");
 
	String   CPE_SnmpV3AuthPassword= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3AuthPassword");

	String   CPE_Country= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Country");

	String   CPE_City= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_City");

	String CPE_Site = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Site");

	String CPE_Premise= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_Premise");

	
	
try {
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrders_header);

	
	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View,"View Link For CPE Device under Trunk panel");
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_Device_Header)) {
		Report.LogInfo("Info", "'View CPE Device' page navigated as expected","PASS");
		Reporter.log("'View CPE Device' page navigated as expected");
	
	try {
	// Verify All Device Information under View CPE Device page//Not working
	
//	compareText_InViewPage("Router Id", CPE_RouterID);
	
//	String CompleteDeviceName=CPE_RouterID+"-"+ServiceIdentification+"..ipc.colt.net";
//	compareText_InViewPage("Device Name",CompleteDeviceName);

//	compareText_InViewPage("Vendor/Model",CPE_VendorModel);

//	compareText_InViewPage("Management Address",CPE_ManagementAddress);

	compareText_InViewPage("Snmpro",CPE_Snmpro);

	compareText_InViewPage("Snmprw",CPE_Snmprw);

	compareText_InViewPage("Snmp V3 Context Name",CPE_SnmpV3ContextName);
	
	compareText_InViewPage("Snmp V3 Context Engine ID",CPE_SnmpV3ContextEngineID);
	
	compareText_InViewPage("Snmp V3 Security Username",CPE_SnmpV3SecurityUsername);
	
	//scrollDown(getwebelement(APT_VOIPAccess_Obj.VOIPAccess.RouterTool_header")));
	//**compareText_InViewPage(application, "Snmp V3 Auth Proto", CPE_SnmpV3AuthProto, xml);//Novalues

	compareText_InViewPage("Snmp V3 Auth Password",CPE_SnmpV3AuthPassword);

	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_CountryValue,"Country");//Compare Won't work here
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_CityValue,"City");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_SiteValue,"Site");
	//**getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_PremiseValue,"Premise");

//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_TestColumnName,"Test");
	//getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_StatusColumnName,"Status");
//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_LastRefresh,"Last Refresh");
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in View CPE Device page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in View CPE Device page");
		}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in View CPE Device page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in View CPE Device page");
		}
	
	}else {
		Report.LogInfo("Info", "'View CPE Device' page not navigated","FAIL");
		Reporter.log("'View CPE Device' page not navigated");
		 }
	
	Report.LogInfo("Info", "All Fields values verified in View CPE Device Page","PASS");
	Reporter.log("------ Verified Added CPE Device Information successfully ------");
	
}catch(NoSuchElementException e) {
e.printStackTrace();
Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
Reporter.log(  e+ " : Field is not displayed");
}catch(Exception e) {
e.printStackTrace();
Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
Reporter.log(  e+" : Field is not displayed");
}


}

public void verifyEditCPEDeviceFunction(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String CPE_RouterIDEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_RouterIDEdit");
	String CPE_DeviceNameEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_DeviceNameEdit");
	String CPE_VendorModelEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_VendorModelEdit");
	String CPE_ManagementAddressEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_ManagementAddressEdit");
	String CPE_SnmproEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmproEdit");
	String CPE_SnmprwEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmprwEdit");
	String CPE_SnmpV3ContextNameEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3ContextNameEdit");
	String CPE_SnmpV3ContextEngineIDEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3ContextEngineIDEdit");
	String CPE_SnmpV3SecurityUsernameEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3SecurityUsernameEdit");
	String CPE_SnmpV3AuthProtoEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3AuthProtoEdit");
	
	String CPE_SnmpV3AuthPasswordEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SnmpV3AuthPasswordEdit");
	String CPE_CountryEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_CountryEdit");
	String CPE_CityEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_CityEdit");
	String CPE_SiteEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_SiteEdit");
	String CPE_PremiseEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_PremiseEdit");
	
	
	try {
	scrollIntoTop();

	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_Device_Header)) {
		Report.LogInfo("Info", "'View CPE Device' page navigated as expected","PASS");
		Reporter.log("'View CPE Device' page navigated as expected");
		
	//	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_ActionLink,"ACTION Link");

		
		click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_Action_EditLink,"Edit Link");

		
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_EditDevice_Header)) {
			Report.LogInfo("Info", "'Edit CPE Device' page navigated as expected","PASS");
			Reporter.log("'Edit CPE Device' page navigated as expected");
	
	
	try {
		
		ClearAndEnterTextValue("Router ID", APT_VOIPAccess_Obj.VOIPAccess.CPE_RouterIdTextfield,CPE_RouterIDEdit);

		//***ClearAndEnterTextValue(application, "Device Name", "CPE_DeviceNameTextfield", CPE_DeviceNameEdit);
		
		//NW**SelectDropdownValueUnderDivTag(application, "Vendor/Model", CPE_VendorModelEdit, "CPE_VendorModelDropdown", "commonDropdownValueTag"); //div[@class='sc-htpNat AUGYd']/div
//addDropdownValues_commonMethod("Vendor/Model", APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrders_header,CPE_VendorModelEdit);

		
		//***ClearAndEnterTextValue(application, "Management Address", "CPE_ManagementAddressTextfield", CPE_ManagementAddressEdit);
		ClearAndEnterTextValue("Snmpro", APT_VOIPAccess_Obj.VOIPAccess.CPE_SnmproTextfield,CPE_SnmproEdit);
		ClearAndEnterTextValue("Snmprw", APT_VOIPAccess_Obj.VOIPAccess.CPE_SnmprwTextfield,CPE_SnmprwEdit);
		ClearAndEnterTextValue("Snmp v3 Context Name", APT_VOIPAccess_Obj.VOIPAccess.CPE_Snmpv3ContextNameTextfield, CPE_SnmpV3ContextNameEdit);
		ClearAndEnterTextValue("Snmp V3 Context Engine ID", APT_VOIPAccess_Obj.VOIPAccess.CPE_snmpv3ContextEngineIdTextfield,CPE_SnmpV3ContextEngineIDEdit);
		ClearAndEnterTextValue("Snmp V3 Security Username", APT_VOIPAccess_Obj.VOIPAccess.CPE_snmpv3SecurityUserNameTextfield,CPE_SnmpV3SecurityUsernameEdit);
		//***SelectDropdownValueUnderDivTag("Snmp V3 Auth Proto", CPE_SnmpV3AuthProtoEdit, "CPE_SnmpV3AuthProtoDropdown", "commonDropdownValueTag");
		//***SelectDropdownValueUnderSelectTag(application, "Snmp V3 Auth Proto", CPE_SnmpV3AuthProtoEdit, "CPE_SnmpV3AuthProtoDropdown", xml);

		ClearAndEnterTextValue("Snmp V3 Auth Password", APT_VOIPAccess_Obj.VOIPAccess.CPE_snmpv3AuthPasswordTextfield,CPE_SnmpV3AuthPasswordEdit);
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		//**SelectDropdownValueUnderDivTag(application, "Country", CPE_CountryEdit, "CPE_CountryDropdown", "commonDropdownValueTag");
		//**SelectDropdownValueUnderDivTag(application, "City", CPE_CityEdit, "CPE_CityDropdown", "commonDropdownValueTag");
		//**SelectDropdownValueUnderDivTag(application, "Site", CPE_SiteEdit, "CPE_SiteDropdown", "commonDropdownValueTag");
		//****SelectDropdownValueUnderDivTag(application, "Premise", CPE_PremiseEdit, "CPE_PremiseDropdown", "commonDropdownValueTag")
		
		selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.CPE_CountryDropdown,CPE_CountryEdit,"Country");
		selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.CPE_CityDropdown,CPE_CityEdit,"City");
		selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.CPE_SiteDropdown,CPE_SiteEdit,"Site");
		
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click(APT_VOIPAccess_Obj.VOIPAccess.AddCPE_OKButton,"OK in Edit CPE Device");
		//Not Working Issues raised
		//**click(application, "Cancel in Edit CPE Device", "AddCPE_CancelButton");
		
		waitforPagetobeenable();
//		compareText(application, "CPE Device Update message", "CPE_UpdateCPEDeviceSuccessMessage", "CPE Device updated successfully");
		verifysuccessmessage("CPE Device updated successfully");
		Reporter.log("------  CPE Device updated successfully   ------");
		
		
		
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", "Field is not displayed in Edit CPE Device page","FAIL");
		Reporter.log( "Field is not displayed in Edit CPE Device page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info", "Field is not displayed in Edit CPE Device page ","FAIL");
		Reporter.log( "Field is not displayed in Edit CPE Device page");
	}
		
		}else {
			Report.LogInfo("Info", "'Edit CPE Device' page not  navigated","FAIL");
			Reporter.log("'Edit CPE Device' page not navigated");
		}
		
	}else {
		Report.LogInfo("Info", "'View CPE Device' page not navigated","FAIL");
		Reporter.log("'View CPE Device' page not navigated");
	}
	
	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	    
		
}

public void veriyFetchDeviceInterfacesFunction_CPE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String CPE_ServiceStatusChangeRequired= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_ServiceStatusChangeRequired");

	try{
		scrollIntoTop();
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_Device_Header)) {
		Report.LogInfo("Info", "'View CPE Device' page navigated as expected","PASS");
		Reporter.log("'View CPE Device' page navigated as expected");
		waitToPageLoad();
	//	click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_ActionLink,"ACTION link");
		click(APT_VOIPAccess_Obj.VOIPAccess.CPE_View_Action_FetchDeviceInterfacesLink,"Fetch Device Interfaces Link");
		
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_FetchDeviceInterfacesSuccessMessage)){
			Report.LogInfo("Info", "Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ","PASS");
			Reporter.log("Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ");
		}else {
			Report.LogInfo("Info", "Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ","PASS");
			Reporter.log("Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ");
		}
		
		click(APT_VOIPAccess_Obj.VOIPAccess.CPE_hereLink_UnderFetchDeviceInterfacesSuccessMessage,"Click here Link for CPE");
		
		
		//Manage COLT's Network - Manage Network
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_ManageCOLTsNetworkManageNetwork_header)) {
			Report.LogInfo("Info", "'Manage COLT's Network - Manage Network' page navigated as expected","PASS");
			Reporter.log("'Manage COLT's Network - Manage Network' page navigated as expected");
			
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Synchronization_SynchronizeLink);
		
		try {
		compareText("Manage COLT's Network - Manage Network header", APT_VOIPAccess_Obj.VOIPAccess.CPE_ManageCOLTsNetworkManageNetwork_header, "Manage COLT's Network - Manage Network");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_DeviceValue,"Device Name in Manage Colt page under Status Panel");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_StatusValue,"Status in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_LastModificationValue,"Last Modification in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_StatusLink,"Status Link in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_ViewInterfacesLink,"View Interface Link in Manage Colt page");
		
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Synchronization_DeviceValue, "Device Name in Manage Colt page under Synchronization Panel");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Synchronization_SyncStatusValue,"Sync Status in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Synchronization_FetchInterfacesValue,"Fetch Interfaces in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Synchronization_SynchronizeLink,"Synchronize Link in Manage Colt page");

		
		scrollIntoTop();
		String CPE_Manage_Status_LastModificationValue= getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_LastModificationValue,"CPE_Manage_Status_LastModificationValue");
		if(CPE_Manage_Status_LastModificationValue.contains("GMT"))
		{
			Reporter.log("Service status is displayed as : " + CPE_Manage_Status_LastModificationValue);
			Reporter.log("Last Modification is :"+ CPE_Manage_Status_LastModificationValue);
		}
		else
		{
			Reporter.log("Incorrect modification time format");
			Reporter.log("Incorrect modification time format");
		}
		
		
		////
		click(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_StatusLink,"Status");

		if(CPE_ServiceStatusChangeRequired=="Yes")
		{
			boolean ServiceStatusPage= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Status_Servicestatus_popup);
			if(ServiceStatusPage)
			{
				scrollDown(APT_VOIPAccess_Obj.VOIPAccess.CPE_Device_Status_OK);
				click(APT_VOIPAccess_Obj.VOIPAccess.CPE_Device_Status_OK,"Click on OK to change Status");

				boolean PE_servicestatushistoryValue= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CPE_servicestatushistoryValue);
				try
				{
					if(PE_servicestatushistoryValue)
					{
						Report.LogInfo("Info", "Step : Service status change request logged","PASS");
					}
					else
						Report.LogInfo("Info", "Step : Service status change request is not logged","PASS");
				}
				catch(StaleElementReferenceException e)
				{
					Report.LogInfo("Info", "No service history to display","FAIL");
				}
			}
			else
				Report.LogInfo("Info", "Status link is not working","FAIL");
		}
		else
		{
			Report.LogInfo("Info", "Step : Service status change not reqired","PASS");
			click(APT_VOIPAccess_Obj.VOIPAccess.CPE_servicestatus_popupclose,"Close");
		}

		
		
		////synchronize panel in manage colt page
			//((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			click(APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_Synchronization_SynchronizeLink,"Synchronize");
			compareText("Synchronize Warning Message", APT_VOIPAccess_Obj.VOIPAccess.CPE_Manage_SynchronizeWarningMessage, "Error while synchronizing device, Reason is :: null.");
			
			}catch(NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", "Field is not displayed in 'Manage COLT's Network - Manage Network' page","FAIL");
				Reporter.log( "Field is not displayed in 'Manage COLT's Network - Manage Network' page");
			}catch(Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "Field is not displayed in 'Manage COLT's Network - Manage Network' page","FAIL");
				Reporter.log( "Field is not displayed in 'Manage COLT's Network - Manage Network' page");
			}
			
	}else {
		Report.LogInfo("Info", "'Manage COLT's Network - Manage Network' page not navigated","FAIL");
		Reporter.log("'Manage COLT's Network - Manage Network' page not navigated");
	}
	
}else {
	Report.LogInfo("Info", "'View CPE Device' page not navigated","FAIL");
	Reporter.log("'View CPE Device' page not navigated");
}
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}


		
	}

public void deleteTrunkGroupSiteOrderNumber(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String TrunkGroupSiteOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");

	try {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	boolean TrunkGroupSiteOrderNumberText=false;
	//boolean ViewService_Trunk_DeleteSiteOrderSuccessMessage=false;
	
	boolean deletelink=webDriver.findElement(By.xpath("//div[div[span[text()='"+ TrunkGroupSiteOrderNumber +"']]]//a[contains(text(),'Delete')]")).isDisplayed();
	
	TrunkGroupSiteOrderNumberText=webDriver.findElement(By.xpath("//span[text()='"+ TrunkGroupSiteOrderNumber +"']")).isDisplayed();
	
	boolean ViewService_Trunk_DeleteSiteOrderSuccessMessage=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.ViewService_Trunk_DeleteSiteOrderSuccessMessage);

	if(TrunkGroupSiteOrderNumberText){
		
		Report.LogInfo("Info", TrunkGroupSiteOrderNumber + " 'Site Order' is displaying under 'Trunk' panel","PASS");
		Reporter.log(TrunkGroupSiteOrderNumber + " 'Site Order' is displaying under 'Trunk' panel");
		
		if(deletelink) {
			Report.LogInfo("Info", "'Delete Link in Trunk Group/Site Order' page is displaying as expected","PASS");
			Reporter.log("'Delete Link Trunk Group/Site Order' page is displaying as expected");
			
		//Trunk group Order
			webDriver.findElement(By.xpath("//div[div[span[text()='"+ TrunkGroupSiteOrderNumber +"']]]//a[contains(text(),'Delete')]")).click();
		//	compareText("Delete Site Order warning message", "ViewService_Trunk_DeleteSiteOrderWarningMessage", "Are you sure that you want to delete this item?");
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ViewService_Trunk_DeleteButton,"Delete Button");
			click(APT_VOIPAccess_Obj.VOIPAccess.ViewService_Trunk_DeleteButton,"Delete Button");
			waitforPagetobeenable();
			waitForAjax();
			
			
			if((ViewService_Trunk_DeleteSiteOrderSuccessMessage)) {
				Report.LogInfo("Info", "'Trunk Group Site Order' Deleted Successfully","PASS");
				Reporter.log("'Trunk Group Site Order' Deleted Successfully");
																				}else {
				//Report.LogInfo("Info", "'Trunk Group Site Order' not Deleted","FAIL");
				Reporter.log("'Trunk Group Site Order' not Deleted");
																					   } 
	    							}else {
	    		Report.LogInfo("Info", "'Delete Link in Trunk Group/Site Order' page is not displaying as expected","PASS");
	    		Reporter.log("'Delete Link Trunk Group/Site Order' page is not  displaying as expected");
	    							}
		
		
		
						}else {
		Report.LogInfo("Info", TrunkGroupSiteOrderNumber + " 'Site Order' is not displaying under 'Trunk' panel","FAIL");
		Reporter.log(TrunkGroupSiteOrderNumber + " 'Site Order' is not displaying under 'Trunk' panel");
							  }

	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	    
}

public void verifyingservicecreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	
	String sid= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String ResellerCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerCode");

	String Remarks= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");

	String EmailService= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EmailService");

	String PhoneService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneService");

	String ManageService= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManageService");

	String SyslogEventView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SyslogEventView");

	String ServiceStatusView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatusView");

	String RouterConfigurationView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RouterConfigurationView");

	String PerformanceReporting= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PerformanceReporting");

	String ProactiveNotification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ProactiveNotification");

	String NotificationManagementTeam= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NotificationManagementTeam");

	String DialUserAdministration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DialUserAdministration");

	String orderno= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");

	String rfireqno= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");

	String servicetype= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");

	
	waitforPagetobeenable();
	Report.LogInfo("Info", "'Verifying Service Creation Functionality","Info");
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.createorderservice_header)) {
		Report.LogInfo("Info", "'Servce Creation' page navigated as expected","PASS");
		Reporter.log("'Servce Creation' page navigated as expected");
	
	try {
		
	clearTextBox(APT_VOIPAccess_Obj.VOIPAccess.serviceidentificationtextfield);
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.OKbutton_ServiceCreation);
	
	
	click(APT_VOIPAccess_Obj.VOIPAccess.OKbutton_ServiceCreation,"OK");
	//
	scrollIntoTop();
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.sidwarngmsg, "Service Identification");
	
	// service identification
	clearTextBox(APT_VOIPAccess_Obj.VOIPAccess.serviceidentificationtextfield);
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.serviceidentificationtextfield,sid,"Service Identification");
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.ResellerCodetextfield,ResellerCode,"Reseller Code");
	
	// remarks
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.remarktextarea,Remarks,"Remarks");

	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.emailtextfield,EmailService,"Email");
	click(APT_VOIPAccess_Obj.VOIPAccess.emailarrow,"Email Arrow");
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.phonecontacttextfield,PhoneService,"Phone Contact");
	click(APT_VOIPAccess_Obj.VOIPAccess.phonearrow,"Phone Contact Arrow");
	
	
	//Package
	if(isEnable(APT_VOIPAccess_Obj.VOIPAccess.PackageDropdownDisabled)){
		Report.LogInfo("Info", "Step : 'Package' Dropdown is Enabled","Info");
		Reporter.log("Step : 'Package' Dropdown is Enabled");
	}else {
		Report.LogInfo("Info", "Step : 'Package' Dropdown is Disabled, So can't make any changes in Dropdown status","Info");
		Reporter.log("Step : 'Package' Dropdown is Disabled, So can't make any changes in Dropdown status");
	}

	
	// management options- Manage Service Checkbox
	if(isEnable(APT_VOIPAccess_Obj.VOIPAccess.ManageServicecheckbox)){
		if (ManageService.equalsIgnoreCase("YES")) {
			click(APT_VOIPAccess_Obj.VOIPAccess.ManageServicecheckbox,"'Manage Service' checkbox");
			Report.LogInfo("Info", "Step : 'Manage Service' checkbox is Enabled and 'Manage Service' checkbox Selected","Info");
			Reporter.log("Step : 'Manage Service' checkbox is Enabled and 'Manage Service' checkbox Selected");
		} else {
			Report.LogInfo("Info", "Step : 'Manage Service' checkbox is not Selected","Info");
			Reporter.log("Step : 'Manage Service' checkbox is not Selected");
		}
	
		}else {
			Report.LogInfo("Info", "Step : 'Manage Service' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
			Reporter.log("Step : 'Manage Service' checkbox is Disabled, So can't make any changes in Checkbox status");
		}
	
	
	
	
	//Syslog Event View checkbox
	if(isEnable(APT_VOIPAccess_Obj.VOIPAccess.SyslogEventViewcheckbox)){
		if (SyslogEventView.equalsIgnoreCase("YES")) {
			click(APT_VOIPAccess_Obj.VOIPAccess.SyslogEventViewcheckbox, "'Syslog Event View' checkbox");
			Report.LogInfo("Info", "Step : 'Syslog Event View' checkbox is Enabled and 'Syslog Event View' checkbox Selected","Info");
			Reporter.log("Step : 'Syslog Event View' checkbox is Enabled and 'Syslog Event View' checkbox Selected");
		} else {
			Report.LogInfo("Info", "Step : 'Syslog Event View' checkbox is not Selected","Info");
			Reporter.log("Step : 'Syslog Event View' checkbox is not Selected");
		}
	
		}else {
			Report.LogInfo("Info", "Step : 'Syslog Event View' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
			Reporter.log("Step : 'Syslog Event View' checkbox is Disabled, So can't make any changes in Checkbox status");
		}
	
	//Service Status View checkbox
	if(isEnable(APT_VOIPAccess_Obj.VOIPAccess.ServiceStatusViewcheckbox)){
		if (ServiceStatusView.equalsIgnoreCase("YES")) {
			click(APT_VOIPAccess_Obj.VOIPAccess.ServiceStatusViewcheckbox,"'Service Status View' checkbox");
			Report.LogInfo("Info", "Step : 'Service Status View' checkbox is Enabled and 'Service Status View' checkbox Selected","Info");
			Reporter.log("Step : 'Service Status View' checkbox is Enabled and 'Service Status View' checkbox Selected");
		} else {
			Report.LogInfo("Info", "Step : 'Service Status View' checkbox is not Selected","Info");
			Reporter.log("Step : 'Service Status View' checkbox is not Selected");
		}
	
		}else {
			Report.LogInfo("Info", "Step : 'Service Status View' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
			Reporter.log("Step : 'Service Status View' checkbox is Disabled, So can't make any changes in Checkbox status");
		}
	
	
	//Router Configuration View checkbox
	if(isEnable(APT_VOIPAccess_Obj.VOIPAccess.RouterConfigurationViewcheckbox)){
		if (RouterConfigurationView.equalsIgnoreCase("YES")) {
			click(APT_VOIPAccess_Obj.VOIPAccess.RouterConfigurationViewcheckbox,"'Router Configuration View' checkbox");
			Report.LogInfo("Info", "Step : 'Router Configuration View' checkbox is Enabled and 'Router Configuration View' checkbox Selected","Info");
			Reporter.log("Step : 'Router Configuration View' checkbox is Enabled and 'Router Configuration View' checkbox Selected");
		} else {
			Report.LogInfo("Info", "Step : 'Router Configuration View' checkbox is not Selected","Info");
			Reporter.log("Step : 'Router Configuration View' checkbox is not Selected");
		}
	
		}else {
			Report.LogInfo("Info", "Step : 'Router Configuration View' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
			Reporter.log("Step : 'Router Configuration View' checkbox is Disabled, So can't make any changes in Checkbox status");
		}

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		//Performance Reporting checkbox
			if(isEnable(APT_VOIPAccess_Obj.VOIPAccess.PerformanceReportingcheckbox)){
				if (PerformanceReporting.equalsIgnoreCase("YES")) {
					click(APT_VOIPAccess_Obj.VOIPAccess.PerformanceReportingcheckbox,"'Performance Reporting' checkbox");
					Report.LogInfo("Info", "Step : 'Performance Reporting' checkbox is Enabled and 'Performance Reporting' checkbox Selected","Info");
					Reporter.log("Step : 'Performance Reporting' checkbox is Enabled and 'Performance Reporting' checkbox Selected");
				} else {
					Report.LogInfo("Info", "Step : 'Performance Reporting' checkbox is not Selected","Info");
					Reporter.log("Step : 'Performance Reporting' checkbox is not Selected");
				}
			
				}else {
					Report.LogInfo("Info", "Step : 'Performance Reporting' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
					Reporter.log("Step : 'Performance Reporting' checkbox is Disabled, So can't make any changes in Checkbox status");
				}
			

	
	//Dial User Administration checkbox
	if(isEnable(APT_VOIPAccess_Obj.VOIPAccess.DialUserAdministrationcheckbox)){
		if (DialUserAdministration.equalsIgnoreCase("YES")) {
			click(APT_VOIPAccess_Obj.VOIPAccess.DialUserAdministrationcheckbox,"'Dial User Administration' checkbox");
			Report.LogInfo("Info", "Step : 'Dial User Administration' checkbox is Enabled and 'Dial User Administration' checkbox Selected","Info");
			Reporter.log("Step : 'Dial User Administration' checkbox is Enabled and 'Dial User Administration' checkbox Selected");
		} else {
			Report.LogInfo("Info", "Step : 'Dial User Administration' checkbox is not Selected","Info");
			Reporter.log("Step : 'Dial User Administration' checkbox is not Selected");
		}
	
		}else {
			Report.LogInfo("Info", "Step : 'Dial User Administration' checkbox is Disabled, SO can't make any changes in Checkbox status","Info");
			Reporter.log("Step : 'Dial User Administration' checkbox is Disabled, SO can't make any changes in Checkbox status");
		}

			click(APT_VOIPAccess_Obj.VOIPAccess.okbutton,"OK");
			compareText("Service creation success msg", APT_VOIPAccess_Obj.VOIPAccess.servicecreationmessage, "Service successfully created");
		
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Service Creation page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in Service Creation page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in Service Creation page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in Service Creation page");
	}
	
	}else {
		Report.LogInfo("Info", "'Servce Creation' page not navigated","FAIL");
		Reporter.log("'Servce Creation' page navigated");
	}
			
}

public void editService(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String ResellerCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerCode");

	String Remarks= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");

	String EmailService= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EmailService");

	String PhoneService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneService");

	String ManageService= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneService");

	String SyslogEventView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SyslogEventView");

	String ServiceStatusView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatusView");

	String RouterConfigurationView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RouterConfigurationView");

	String PerformanceReporting= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PerformanceReporting");

	String ProactiveNotification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ProactiveNotification");

	String NotificationManagementTeam= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NotificationManagementTeam");

	String DialUserAdministration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DialUserAdministration");

	String ServiceType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");

	String EditServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditManageService");

	String EditResellerCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditResellerCode");

	String EditRemarks= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditRemarks");

	String EditEmailService= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditEmailService");

	String EditPhoneService= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditPhoneService");

	String EditManageService= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditManageService");

	String EditSyslogEventView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSyslogEventView");

	String EditServiceStatusView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditServiceStatusView");

	String EditRouterConfigurationView= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditRouterConfigurationView");

	String EditPerformanceReporting= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditPerformanceReporting");

	String EditProactiveNotification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditProactiveNotification");

	String EditNotificationManagementTeam= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditNotificationManagementTeam");

	String EditDialUserAdministration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditDialUserAdministration");

	
	
	
	waitforPagetobeenable();
	waitforPagetobeenable();
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
	
	try { 
	
	click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown, "Action dropdown");
	click(APT_VOIPAccess_Obj.VOIPAccess.edit,"Edit");
	waitforPagetobeenable();
	clearTextBox(APT_VOIPAccess_Obj.VOIPAccess.remarktextarea);
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.remarktextarea,EditRemarks,"Remarks");
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.cancelbutton);
	click(APT_VOIPAccess_Obj.VOIPAccess.cancelbutton,"Cancel");
	
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.servicepanel_header))
	{
		compareText("Remarks", APT_VOIPAccess_Obj.VOIPAccess.servicepanel_remarksvalue, Remarks);	
	}
	else {
		Report.LogInfo("Info", "Step : Didn't navigate to view service page","FAIL");
	}
	
	//Edit service
	click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown, "Action dropdown");
	click(APT_VOIPAccess_Obj.VOIPAccess.edit,"Edit");
	
	//service identification
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.serviceidentificationtextfield, ServiceIdentification, "Service Identification");

	//Service Type
	compareText_InViewPage("Service Type", ServiceType);
	
	//Remark	
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.remarktextarea, Remarks,"Remark");
	

	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.OKbutton_ServiceCreation);


//Performance Reporting
	editcheckbox_commonMethod(PerformanceReporting, APT_VOIPAccess_Obj.VOIPAccess.PerformanceReportingcheckbox, "Performance Reporting");
	
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.OKbutton_ServiceCreation);
	click(APT_VOIPAccess_Obj.VOIPAccess.OKbutton_ServiceCreation,"OK");
	waitforPagetobeenable();
	
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.CustomerDetailsHeader);
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.CustomerDetailsHeader))
	{
		Reporter.log("Navigated to view service page");
		Reporter.log("Navigated to view service page");
//		compareText(application, "Service updated success message", "serviceupdate_successmsg", "Service successfully updated");	
		verifysuccessmessage("Service successfully updated");
	}
	else
	{
		Reporter.log("Service not updated");
		Reporter.log("Service not updated");
	}
	
	
	
	searchorder(ServiceIdentification);//ByMe
	//Verify all the links available in service actions list
	waitforPagetobeenable();
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.orderpanelheader);
	
	click(APT_VOIPAccess_Obj.VOIPAccess.serviceactiondropdown,"Action dropdown");
	compareText("Edit Link", APT_VOIPAccess_Obj.VOIPAccess.EditLink, "Edit");
	compareText("Delete Link", APT_VOIPAccess_Obj.VOIPAccess.DeleteLink, "Delete");
	compareText("Manage Subnets Ipv6 Link", APT_VOIPAccess_Obj.VOIPAccess.ManageSubnetsIpv6Link, "Manage Subnets Ipv6");
	compareText("Dump Link", APT_VOIPAccess_Obj.VOIPAccess.DumpLink, "Dump");
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in View Service page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in View Service page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in View Service page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in View Service page");
	}
	
	//Service delete is performed in the last test case
}

public void verifyAddInterfaceFunction_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	
	

	String MAS_AccessMedia= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_AccessMedia");

	String MAS_Network= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Network");

	
	String MAS_HSRPBGP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPBGP");

	String MAS_GenerateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_GenerateConfiguration");

	String	MAS_Interface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Interface");

	String MAS_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceAddressRange");

	
	String MAS_InterfaceAddressMask= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceAddressMask");

	String	MAS_HSRPIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPIP");

	String	MAS_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceAddressRangeIPV6");

	String MAS_HSRPIPv6Address = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceAddressRangeIPV6");

	String	MAS_PrimaryIPv6onMas1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_PrimaryIPv6onMas1");

	String MAS_SecondaryIPv6onMas2= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_SecondaryIPv6onMas2");

	String MAS_GroupNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_GroupNumber");

	String MAS_Link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Link");

	String MAS_VLANID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_VLANID");

	
	String	MAS_IVManagement= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_IVManagement");

	String MAS_Configuration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Configuration");

	String MAS_HSRPTrackInterface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPTrackInterface");

	String MAS_HSRPAuthentication= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPAuthentication");

	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterfaceLink);//RouterTool_header//MAS_PE_CommandIPV4_header//MAS_PE_InterfacesPanel_header
//	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_InterfacesActionLink,"ACTION Link");
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterfaceLink,"Add Interface Link");
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterface_header)) {
		Report.LogInfo("Info", "'MAS Add Interface' page navigated as expected","PASS");
		Reporter.log("'MAS Add Interface' page navigated as expected");
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configuration_label);
	
	try {
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton,"OK Button");
	
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationWarningMessage,"Configuration is required");
	scrollIntoTop();
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceWarningMessage,"Interface is required");

	
	
	if(MAS_AccessMedia.equalsIgnoreCase("VPN")) {
		Report.LogInfo("Info",  " Selected Access Media is : "+MAS_AccessMedia,"PASS");
		//addDropdownValues_commonMethod("Access Media", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);
		//webDriver.findElement(By.xpath("//div[text()='"+MAS_AccessMedia+"']"));

		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceTextfield,"Interface textfield");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceTextfield,"Interface textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceTextfield,MAS_Interface,"Interface textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield,MAS_InterfaceAddressRange,"Interface Address Range Textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield,MAS_InterfaceAddressMask,"Interface Address Mask textfield");
		//EnterTextValue(application, MAS_HSRPIP, "HSRP IP textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_HSRPIPTextfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeIPV6Textfield,MAS_InterfaceAddressRangeIPV6,"Interface Address Range IPV6 textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_HSRPIPv6AddressTextfield,MAS_HSRPIPv6Address,"HSRP IPv6 Address textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_PrimaryIPv6onMas1Textfield,MAS_PrimaryIPv6onMas1,"Primary IPv6 on Mas1 textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SecondaryIPv6onMas2Textfield,MAS_SecondaryIPv6onMas2,"Secondary IPv6 on Mas2 textfield");
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox);
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield,MAS_GroupNumber,"Group Number textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield,MAS_Link,"Link textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield,MAS_VLANID,"Interface textfield");
		
		
//		//IV Management Checkbox
//		if(getwebelement(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox")).isEnabled()){
//			if (MAS_IVManagement.equalsIgnoreCase("YES")) {
//				click(application, "'IV Management' checkbox", "MAS_PE_IVManagementCheckbox");
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected");
//			} else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is not Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is not Selected");
//			}
//		
//			}else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status");
//			}

		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox, "IV Management", MAS_IVManagement);
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configuration_label);
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		
		

		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,MAS_GenerateConfiguration,"Enter Generate Configuration");
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox);
		
		webDriver.findElement(By.xpath("//div[text()='"+ MAS_GenerateConfiguration +"']")).click();
		
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SaveConfigurationtoFileButton,"Save Configuration to File Button");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration test", "Configuration");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1,"OK Button");

		compareText("Interface Added Successfully Message", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterfaceSuccessfullMessage, "Interface added successfully");
		Report.LogInfo("Info", "Interfaces sucessfully added by selecting VPN as Access Media","PASS");
		
	}else if(MAS_AccessMedia.equalsIgnoreCase("EPN")) {
		Report.LogInfo("Info",  " Selected Access Media is : "+MAS_AccessMedia,"PASS");

		//click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AccessMediaDropdown,"Access Media Dropdown");
		//webDriver.findElement(By.xpath("//div[text()='"+MAS_AccessMedia+"']"));
		addDropdownValues_commonMethod("Access Media", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AccessMediaDropdown, MAS_AccessMedia);
		webDriver.findElement(By.xpath("//div[text()='"+MAS_AccessMedia+"']"));

		
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceTextfield,MAS_Interface,"Interface textfield");
		
		addDropdownValues_commonMethod("HSRP BGP", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_HSRPBGPDropdown, MAS_HSRPBGP);
		
//		EnterTextValue(application, MAS_InterfaceAddressRange, "Interface Address Range Textfield", "MAS_PE_InterfaceAddressRangeTextfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield,MAS_InterfaceAddressMask,"Interface Address Mask textfield");
//		EnterTextValue(application, MAS_HSRPIP, "HSRP IP textfield", "MAS_PE_HSRPIPTextfield");
//		EnterTextValue(application, MAS_InterfaceAddressRangeIPV6, "Interface Address Range IPV6 textfield", "MAS_PE_InterfaceAddressRangeIPV6Textfield");
		
//		scrollDown(getwebelement(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox")));
//		EnterTextValue(application, MAS_HSRPIPv6Address, "HSRP IPv6 Address textfield", "MAS_PE_HSRPIPv6AddressTextfield");
//		EnterTextValue(application, MAS_PrimaryIPv6onMas1, "Primary IPv6 on Mas1 textfield", "MAS_PE_PrimaryIPv6onMas1Textfield");
//		EnterTextValue(application, MAS_SecondaryIPv6onMas2, "Secondary IPv6 on Mas2 textfield", "MAS_PE_SecondaryIPv6onMas2Textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield,MAS_GroupNumber,"Group Number textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield,MAS_Link,"Link textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield,MAS_VLANID,"Interface textfield");

//		//IV Management Checkbox
//		if(getwebelement(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox")).isEnabled()){
//			if (MAS_IVManagement.equalsIgnoreCase("YES")) {
//				click(application, "'IV Management' checkbox", "MAS_PE_IVManagementCheckbox");
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected");
//			} else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is not Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is not Selected");
//			}
//		
//			}else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status");
//			}

		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox, "IV Management", MAS_IVManagement);
	
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configuration_label);
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		
		
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,MAS_GenerateConfiguration,"Enter Generate Configuration");
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox);
		
		webDriver.findElement(By.xpath("//div[text()='"+ MAS_GenerateConfiguration +"']")).click();
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1, "OK Button");
		
		compareText("Interface Added Successfully Message", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterfaceSuccessfullMessage, "Interface added successfully");
		Report.LogInfo("Info", "Interfaces sucessfully added by selecting EPN as Access Media","PASS");
		Reporter.log("------ Interface Added successfully ------");
		
	}else {
		Reporter.log("Access Media Dropdown is not selected as VPN or EPN");
		Report.LogInfo("Info", "Access Media Dropdown is not selected as VPN or EPN","FAIL");
	}
	
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Add Interface page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in  Add Interface page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in  Add Interface page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in  Add Interface page");
	}

	
	}else {
		Report.LogInfo("Info", "'MAS Add Interface' page not navigated","FAIL");
		Reporter.log("'MAS Add Interface' page not navigated");
	}
	
	
}

public void verifyEditInterfaceFunction_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String MAS_AccessMediaEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_AccessMediaEdit");
	String MAS_NetworkEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_NetworkEdit");
	String MAS_HSRPBGPEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPBGPEdit");
	String MAS_GenerateConfigurationEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_GenerateConfigurationEdit");
	String	MAS_InterfaceEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceEdit");
	String MAS_InterfaceAddressRangeEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceAddressRangeEdit");
	String MAS_InterfaceAddressMaskEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceAddressMaskEdit");
	String	MAS_HSRPIPEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPIPEdit");
	String	MAS_InterfaceAddressRangeIPV6Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_InterfaceAddressRangeIPV6Edit");
	String MAS_HSRPIPv6AddressEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPIPv6AddressEdit");
	String	MAS_PrimaryIPv6onMas1Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_PrimaryIPv6onMas1Edit");
	String MAS_SecondaryIPv6onMas2Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_SecondaryIPv6onMas2Edit");
	String MAS_GroupNumberEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_GroupNumberEdit");
	String MAS_LinkEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_LinkEdit");
	String MAS_VLANIDEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_VLANIDEdit");
	String	MAS_IVManagementEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_IVManagementEdit");
	String MAS_ConfigurationEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_ConfigurationEdit");
	String MAS_HSRPTrackInterfaceEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPTrackInterfaceEdit");
	String MAS_HSRPAuthenticationEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_HSRPAuthenticationEdit");
	

	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.portalaccess_header);//ProviderEquipment_header//portalaccess_header//managementoptions_header
	
//	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Checkbox1_ViewServicePage,"Select Checkbox interface"); 
//	click(APT_VOIPAccess_Obj.VOIPAccess.MASSwitch_ACTION_Device1, "ACTION MAS Switch"); 
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ACTION_EditLink_Device1,"ACTION MAS Switch"); 
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_EditInterface_header)) {
		Report.LogInfo("Info", "'MAS Edit Interface' page navigated as expected","PASS");
		Reporter.log("'MAS Edit Interface' page navigated as expected");
		
	try {
	if(MAS_AccessMediaEdit.equalsIgnoreCase("VPN")) {
		Report.LogInfo("Info",  " Selected Access Media is : "+MAS_AccessMediaEdit,"PASS");

		ClearAndEnterTextValue("Interface textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceTextfield, MAS_InterfaceEdit);
//		ClearAndEnterTextValue(application, "Interface Address Range Textfield", "MAS_PE_InterfaceAddressRangeTextfield", MAS_InterfaceAddressRangeEdit);
		ClearAndEnterTextValue("Interface Address Mask textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMaskEdit);
		//ClearAndEnterTextValue(application, "HSRP IP textfield", "MAS_PE_HSRPIPTextfield", MAS_HSRPIPEdit);
//		ClearAndEnterTextValue(application, "Interface Address Range IPV6 textfield", "MAS_PE_InterfaceAddressRangeIPV6Textfield", MAS_InterfaceAddressRangeIPV6Edit);
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox);
//		ClearAndEnterTextValue(application, "HSRP IPv6 Address textfield", "MAS_PE_HSRPIPv6AddressTextfield", MAS_HSRPIPv6AddressEdit);
//		ClearAndEnterTextValue(application,  "Primary IPv6 on Mas1 textfield", "MAS_PE_PrimaryIPv6onMas1Textfield" , MAS_PrimaryIPv6onMas1Edit);
		ClearAndEnterTextValue("Secondary IPv6 on Mas2 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2Edit);
		ClearAndEnterTextValue("Group Number textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield, MAS_GroupNumberEdit);
		ClearAndEnterTextValue("Link textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield, MAS_LinkEdit);
		ClearAndEnterTextValue("VLAN ID textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield, MAS_VLANIDEdit);


		editcheckbox_commonMethod(MAS_IVManagementEdit, APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox, "IV Management");
	

		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		//**EnterTextValue(application, MAS_GenerateConfigurationEdit, "Enter Generate Configuration", "MAS_PE_GenerateConfigurationDropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		webDriver.findElement(By.xpath("//div[text()='"+ MAS_GenerateConfigurationEdit +"']")).click();;
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SaveConfigurationtoFileButton,"Save Configuration to File Button");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration test", "Configuration");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1,"OK Button");
//		compareText(application, "Interface Updated Successfully Message", "MAS_PE_UpdateInterfaceSuccessfullMessage", "Interface updated successfully");
		verifysuccessmessage("Interface updated successfully");
		Report.LogInfo("Info", "Interfaces sucessfully updated by selecting VPN as Access Media","PASS");
		
	
	}else if(MAS_AccessMediaEdit.equalsIgnoreCase("EPN")) {
		Report.LogInfo("Info",  " Selected Access Media is : "+MAS_AccessMediaEdit,"PASS");
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AccessMediaDropdown,"Access Media Dropdown");
		webDriver.findElement(By.xpath("//div[text()='"+ MAS_AccessMediaEdit +"']")).click();;
		
		
		ClearAndEnterTextValue("Interface textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceTextfield, MAS_InterfaceEdit);
		
//		SelectDropdownValueUnderDivTag(application, "HSRP BGP Dropdown", MAS_HSRPBGPEdit, "MAS_PE_HSRPBGPDropdown", "commonDropdownValueTag");
//		
//		ClearAndEnterTextValue(application, "Interface Address Range Textfield", "MAS_PE_InterfaceAddressRangeTextfield", MAS_InterfaceAddressRangeEdit);
		ClearAndEnterTextValue("Interface Address Mask textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield, MAS_InterfaceAddressMaskEdit);
		//ClearAndEnterTextValue(application, "HSRP IP textfield", "MAS_PE_HSRPIPTextfield", MAS_HSRPIPEdit);
//		ClearAndEnterTextValue(application, "Interface Address Range IPV6 textfield", "MAS_PE_InterfaceAddressRangeIPV6Textfield", MAS_InterfaceAddressRangeIPV6Edit);
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox);
		
//		ClearAndEnterTextValue(application, "HSRP IPv6 Address textfield", "MAS_PE_HSRPIPv6AddressTextfield", MAS_HSRPIPv6AddressEdit);
//		ClearAndEnterTextValue(application,  "Primary IPv6 on Mas1 textfield", "MAS_PE_PrimaryIPv6onMas1Textfield" , MAS_PrimaryIPv6onMas1Edit);
		ClearAndEnterTextValue("Secondary IPv6 on Mas2 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SecondaryIPv6onMas2Textfield, MAS_SecondaryIPv6onMas2Edit);
		ClearAndEnterTextValue("Group Number textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield, MAS_GroupNumberEdit);
		ClearAndEnterTextValue("Link textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield, MAS_LinkEdit);
		ClearAndEnterTextValue("VLAN ID textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield, MAS_VLANIDEdit);
//		ClearAndEnterTextValue(application, "HSRP Track Interface textfield", "MAS_PE_HSRPTrackInterfaceTextfield", MAS_HSRPTrackInterfaceEdit);
//		ClearAndEnterTextValue(application, "HSRP Authentication textfield", "MAS_PE_HSRPAuthenticationTextfield", MAS_HSRPAuthenticationEdit);

		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox,"IV Management Checkbox");
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		webDriver.findElement(By.xpath("//div[text()='"+ MAS_GenerateConfigurationEdit +"']")).click();;
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		//((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1,"OK Button");
		
//		compareText(application, "Interface Updated Successfully Message", "MAS_PE_UpdateInterfaceSuccessfullMessage", "Interface updated successfully");
		verifysuccessmessage("Interface updated successfully");
		Report.LogInfo("Info", "Interfaces sucessfully updated by selecting EPN as Access Media","PASS");
		Reporter.log("------ Interface Updated successfully ------");
		
	}else {
		Reporter.log("Access Media Dropdown is not selected as VPN or EPN");
		Report.LogInfo("Info", "Access Media Dropdown is not selected as VPN or EPN","FAIL");
	}

	
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Edit Interface page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in  Edit Interface page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in  Edit Interface page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in  Edit Interface page");
	}

	}else {
		Report.LogInfo("Info", "'MAS Edit Interface' page not navigated","FAIL");
		Reporter.log("'MAS Edit Interface' page not navigated");
	}
	
	
	
}

public void verifyAddPEDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String PE_IMSPOPLocation= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_IMSPOPLocation");

	
try {
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrders_header);
		
		click(APT_VOIPAccess_Obj.VOIPAccess.PE_AddPEDeviceLink,"Add PE Device Link");
		
		if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_AddPEDevice_header)) {
			Report.LogInfo("Info", "'Add PE Device' page navigated as expected","PASS");
			Reporter.log("'Add PE Device' page navigated as expected");
			
		try {
		compareText("Add PE Device header", APT_VOIPAccess_Obj.VOIPAccess.PE_AddPEDevice_header, "Add PE Device");

		click(APT_VOIPAccess_Obj.VOIPAccess.PE_OKbutton,"OK button");
		compareText("Add Device Warning Message for PE Device", APT_VOIPAccess_Obj.VOIPAccess.PE_AddPEDeviceWarningMessage, "IMS POP Location is required");
		
		// Select  IMS POP Location dropdown
		addDropdownValues_commonMethod("IMS POP Location", APT_VOIPAccess_Obj.VOIPAccess.PE_IMSPOPLocationDropdown, PE_IMSPOPLocation);

		click(APT_VOIPAccess_Obj.VOIPAccess.PE_OKbutton,"OK button");
		compareText("Add Device Successful Message for PE Device", APT_VOIPAccess_Obj.VOIPAccess.PE_AddPEDeviceSuccessfulMessage, "PE Device added successfully");

		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed in Add PE Device page","FAIL");
			Reporter.log(  e+ " : Field is not displayed in Add PE Device page");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed in Add PE Device page ","FAIL");
			Reporter.log(  e+" : Field is not displayed in Add PE Device page");
		}
		
		}else {
			Report.LogInfo("Info", "'Add PE Device' page not navigated","FAIL");
			Reporter.log("'Add PE Device' page not navigated");
		}
		
		Reporter.log("------ PE Device added successfully ------");
		
}catch(NoSuchElementException e) {
	e.printStackTrace();
	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	Reporter.log(  e+ " : Field is not displayed");
}catch(Exception e) {
	e.printStackTrace();
	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	Reporter.log(  e+" : Field is not displayed");
}

	}

public void verifyAddedPEDeviceInformation_View(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {

	String PE_IMSPOPLocation= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_IMSPOPLocation");

	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View PE Device' page navigated as expected","PASS");
		Reporter.log("'View PE Device' page navigated as expected");
		
	try {
		
	// Verify All Device Information under Device panel for PE Device
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_DeviceNameValue,"Device Name");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_VendorModelValue,"Vendor/Model");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_ManagementAddressValue,"Management Address");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_SnmproValue,"Snmpro");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_CountryValue,"Country");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_CityValue,"City");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_SiteValue,"Site");
	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_PremiseValue,"Premise");
//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_TestColumnName,"Test");
//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_StatusColumnName,"Status");
//	getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_View_LastRefresh,"Last Refresh");
	
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in View PE Device page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in View PE Device page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in View PE Device page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in View PE Device page");
	}
	
	}else {
		Report.LogInfo("Info", "'View PE Device' page not navigated","FAIL");
		Reporter.log("'View PE Device' page not navigated");
	}
	
	Reporter.log("------ Verified Added PE Device Information successfully ------");
}
	
public void verifyEditPEDeviceFunction(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String PE_DeviceName_Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_DeviceName_Edit");

	String PE_VendorModelEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VendorModelEdit");

	String PE_ManagementAddressEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_ManagementAddressEdit");

	String PE_SnmproEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_SnmproEdit");

	String PE_CountryEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_CountryEdit");

	
	String editExistingCity= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_ExistingCitySelection");

	String editNewCity= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_newCitySelection");

	String editExistingCityValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editExistingCity");

	String editNewCityName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editNewCityname");

	String editNewCityCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editNewCityCode");

	String editExistingSite= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_ExistingSiteSelection");

	String editNewSite= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_newSiteSelection");

	String editExistingSiteValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editExistingSite");

	String editNewSiteName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editNewSitename");

	String editNewSiteCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editNewSiteCode");

	String editExistingPremise= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_ExistingPremiseSelection");

	String editNewPremise= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_newPremiseSelection");

	String editExistingPremiseValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editExistingPremise");

	String editNewPremiseName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editNewPremiseName");

	String editNewPremiseCode= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editNewPremiseCode");

	
	
	
	Report.LogInfo("Info", "Verifying 'Edit PE Device' Functionality","Info");
	
	try {
	scrollIntoTop();
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View PE Device' page navigated as expected","PASS");
		Reporter.log("'View PE Device' page navigated as expected");
		
	//	click(APT_VOIPAccess_Obj.VOIPAccess.PE_View_ActionLink,"ACTION link");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.PE_View_Action_EditLink,"Edit Link");
		click(APT_VOIPAccess_Obj.VOIPAccess.PE_View_Action_EditLink,"Edit Link");
		waitForAjax();
	
	if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.Edit_PEDevice_Header)) {
		Report.LogInfo("Info", "'Edit PE Device' page navigated as expected","PASS");
		Reporter.log("'Edit PE Device' page navigated as expected");
		
		try { 	

			///////////////////////////////
		//Device name	
		//edittextFields_commonMethod(application, "Device name", "MAS_PE_Edit_deviceName", PE_DeviceName_Edit, xml);
			
		//vendor/model	
			//SelectDropdownValueUnderSpanTag(application, "Vendor/Model", PE_VendorModelEdit, "MAS_PE_Edit_vendorModel", "commonDropdownValueTag", xml);
			
		//Management Address	
			//edittextFields_commonMethod(application, "Management Address", "MAS_PE_Edit_managementAddress", PE_ManagementAddressEdit, xml);
			
		//Snmrpo	
			//edittextFields_commonMethod(application, "Snmpro", "MAS_PE_Edit_snmpro", PE_SnmproEdit, xml);
		
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			
			
			
			//Country
					if(!PE_CountryEdit.equalsIgnoreCase("Null")) {
						
//						addDropdownValues_commonMethod(application, "Country", "countryinput", editCountry, xml);
						selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.countryinput,PE_CountryEdit,"Country");
						
						//New City		
						if(editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("yes")) {
							Clickon_addToggleButton(APT_VOIPAccess_Obj.VOIPAccess.addcityswitch);
						   //City name
							sendKeys("City Name", APT_VOIPAccess_Obj.VOIPAccess.citynameinputfield, editNewCityName);
						   //City Code	
							sendKeys("City Code", APT_VOIPAccess_Obj.VOIPAccess.citycodeinputfield, editNewCityCode);
						   //Site name
							sendKeys("Site Name", APT_VOIPAccess_Obj.VOIPAccess.sitenameinputfield_addCityToggleSelected, editNewSiteName);
						   //Site Code
							sendKeys("Site Code", APT_VOIPAccess_Obj.VOIPAccess.sitecodeinputfield_addCityToggleSelected, editNewSiteCode);
						   //Premise name	
							sendKeys("Premise Name", APT_VOIPAccess_Obj.VOIPAccess.premisenameinputfield_addCityToggleSelected, editNewPremiseName);
						   //Premise Code	
							sendKeys("Premise Code", APT_VOIPAccess_Obj.VOIPAccess.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode);
						}	
					
					//Existing City	
						else if(editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
							
						   //**addDropdownValues_commonMethod(application, "City", "citydropdowninput", editExistingCityValue, xml);
						   //**selectValueInsideDropdown(application, "citydropdowninput", "City", editExistingCityValue, xml);
						   selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.citydropdowninput,editExistingCityValue,"City");
							
							
						 //Existing Site
							  if(editExistingSite.equalsIgnoreCase("yes") & editNewSite.equalsIgnoreCase("no")) {
								  //addDropdownValues_commonMethod(application, "Site", "sitedropdowninput", editExistingSiteValue, xml);
								 //** selectValueInsideDropdown(application, "sitedropdowninput", "Site", editExistingSiteValue, xml);
								  selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.sitedropdowninput, editExistingSiteValue,"Site");
								  
							 //Existing Premise
								  if(editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
									  //addDropdownValues_commonMethod(application, "Premise", "premisedropdowninput", editExistingPremiseValue, xml);
									 //** selectValueInsideDropdown(application, "premisedropdowninput", "Premise", editExistingPremiseValue, xml);
									selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.premisedropdowninput,"Premise", editExistingPremiseValue);
						          	 }
								  
								//New Premise  
								  else if(editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("yes")) {
									  
									  Clickon_addToggleButton(APT_VOIPAccess_Obj.VOIPAccess.addpremiseswitch);
									  //Premise name	
										sendKeys(APT_VOIPAccess_Obj.VOIPAccess.premisenameinputfield_addPremiseToggleSelected, editNewPremiseName, "Premise Name");
									   //Premise Code	
										sendKeys(APT_VOIPAccess_Obj.VOIPAccess.premisecodeinputfield_addPremiseToggleSelected, editNewPremiseCode, "Premise Code");
								  } 
					         	}
				  		
						  else if(editExistingSite.equalsIgnoreCase("no") & editNewSite.equalsIgnoreCase("yes")) {
							  	
							  Clickon_addToggleButton(APT_VOIPAccess_Obj.VOIPAccess.addsiteswitch);
							  //Site name
							  sendKeys(APT_VOIPAccess_Obj.VOIPAccess.sitenameinputfield_addSiteToggleSelected, editNewSiteName, "Site Name");
							   //Site Code
							 sendKeys(APT_VOIPAccess_Obj.VOIPAccess.sitecodeinputfield_addSiteToggleSelected, editNewSiteCode, "Site Code");
							   //Premise name	
							sendKeys(APT_VOIPAccess_Obj.VOIPAccess.premisenameinputfield_addSiteToggleSelected, editNewPremiseName, "Premise Name");
							   //Premise Code	
							sendKeys(APT_VOIPAccess_Obj.VOIPAccess.premisecodeinputfield_addSiteToggleSelected, editNewPremiseCode, "Premise Code");
							  }
						}
						
					}
					else if(PE_CountryEdit.equalsIgnoreCase("Null")) {
						
						Report.LogInfo("Info", " No changes made for 'Country' dropdown","PASS");
					
					
					}
					
					
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			click(APT_VOIPAccess_Obj.VOIPAccess.PE_OKbutton,"OK in Edit PE Device");
			waitforPagetobeenable();
//			compareText(application, "PE Device Update message", APT_VOIPAccess_Obj.VOIPAccess.PE_UpdatePEDeviceSuccessfulMessage", "PE Device updated successfully");
			verifysuccessmessage("PE Device updated successfully");
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Edit PE Device page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in Edit PE Device page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in Edit PE Device page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in Edit PE Device page");
	}
	
		}else {
			Report.LogInfo("Info", "'Edit PE Device' page not navigated","FAIL");
			Reporter.log("'Edit PE Device' page not navigated");	
		}
		
		Reporter.log("------  PE Device updated successfully   ------");
	
		
	}else {
		Report.LogInfo("Info", "'View PE Device' page not navigated","FAIL");
		Reporter.log("'View PE Device' page navigated");
	}
	
	 }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	    	Reporter.log(  e+ " : Field is not displayed");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	    	Reporter.log(  e+" : Field is not displayed");
	    }
	    
}


public void verifFetchDeviceInterfacesFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {

	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String PE_ServiceStatusChangeRequired= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_ServiceStatusChangeRequired");

	
	try {
	
	scrollIntoTop();
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View PE Device' page navigated as expected","PASS");
		Reporter.log("'View PE Device' page navigated as expected");
	
	//	click(APT_VOIPAccess_Obj.VOIPAccess.PE_View_ActionLink,"ACTION link");
		click(APT_VOIPAccess_Obj.VOIPAccess.PE_View_Action_FetchDeviceInterfacesLink,"Fetch Device Interfaces Link");
	
		
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_FetchDeviceInterfacesSuccessMessage)){
			Report.LogInfo("Info", "Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ","PASS");
			Reporter.log("Step : Device fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' displayed ");
		}else {
			Report.LogInfo("Info", "Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ","FAIL");
			Reporter.log("Step : Device not fetched successfully and confirmation message 'Fetch Interfaces started successfully. Please check the sync status of this device here' Not displayed ");
		}
		
		
		
		click(APT_VOIPAccess_Obj.VOIPAccess.PE_hereLink_UnderFetchDeviceInterfacesSuccessMessage,"Click here Link for PE");
		
		
		//Manage COLT's Network - Manage Network
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_ManageCOLTsNetworkManageNetwork_header)) {
		Report.LogInfo("Info", "'Manage COLT's Network - Manage Network' page navigated as expected","PASS");
		Reporter.log("'Manage COLT's Network - Manage Network' page navigated as expected");
					
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_SynchronizeLink);
				
		try {
			scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_SynchronizeLink);
		//Manage COLT's Network - Manage Network
		compareText("Manage COLT's Network - Manage Network header", APT_VOIPAccess_Obj.VOIPAccess.PE_ManageCOLTsNetworkManageNetwork_header, "Manage COLT's Network - Manage Network");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_DeviceValue,"Device Name in Manage Colt page under Status Panel");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_StatusValue,"Status in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_LastModificationValue,"Last Modification in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_StatusLink,"Status Link in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_ViewInterfacesLink,"View Interface Link in Manage Colt page");
		
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_DeviceValue,"Device Name in Manage Colt page under Synchronization Panel");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_SyncStatusValue,"Sync Status in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_SmartsValue,"Smarts in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_FetchInterfacesValue,"Fetch Interfaces in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_VistaMartDeviceValue,"VistaMart Device in Manage Colt page");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_SynchronizeLink,"Synchronize Link in Manage Colt page");

		
		scrollIntoTop();
		String PE_Manage_Status_LastModificationValue= getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_LastModificationValue,"PE_Manage_Status_LastModificationValue");
		if(PE_Manage_Status_LastModificationValue.contains("GMT"))
		{
			Reporter.log("Service status is displayed as : " + PE_Manage_Status_LastModificationValue);
			Reporter.log("Service status is :"+ PE_Manage_Status_LastModificationValue);
		}
		else
		{
			Reporter.log("Incorrect modification time format");
			Reporter.log("Incorrect modification time format");
		}
		
		
		////
		click(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_StatusLink,"Status");

		if(PE_ServiceStatusChangeRequired=="Yes")
		{
			boolean ServiceStatusPage= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Status_Servicestatus_popup);
			if(ServiceStatusPage)
			{
				scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PE_Device_Status_OK);
				click(APT_VOIPAccess_Obj.VOIPAccess.PE_Device_Status_OK,"Click on OK to change Status");

				boolean PE_servicestatushistoryValue= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.PE_servicestatushistoryValue);
				try
				{
					if(PE_servicestatushistoryValue)
					{
						Report.LogInfo("Info", "Step : Service status change request logged","PASS");
					}
					else
						Report.LogInfo("Info", "Step : Service status change request is not logged","PASS");
				}
				catch(StaleElementReferenceException e)
				{
					Report.LogInfo("Info", "No service history to display","FAIL");
				}
			}
			else
				Report.LogInfo("Info", "Status link is not working","FAIL");
		}
		else
		{
			Report.LogInfo("Info", "Step : Service status change not reqired","PASS");
			click(APT_VOIPAccess_Obj.VOIPAccess.PE_servicestatus_popupclose,"Close");
		}

		
		
		////synchronize panel in manage colt page
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			click(APT_VOIPAccess_Obj.VOIPAccess.PE_Manage_Synchronization_SynchronizeLink,"Synchronize");
//			compareText(application, "Synchronize Success Message", "PE_Manage_SynchronizeSuccessMessage", "Sync started successfully. Please check the sync status of this device.");
			verifysuccessmessage("Sync started successfully. Please check the sync status of this device.");	
			Reporter.log("------  Sync started successfully. Please check the sync status of this device   ------");

		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", e+ " : Field is not displayed in Manage Colt Network page","FAIL");
			Reporter.log(  e+ " : Field is not displayed in Manage Colt Network page");
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  e+" : Field is not displayed in Manage Colt Network page ","FAIL");
			Reporter.log(  e+" : Field is not displayed in Manage Colt Network page");
		}
		
}else {
	Report.LogInfo("Info", "'Manage COLT's Network - Manage Network' page not navigated","FAIL");
	Reporter.log("'Manage COLT's Network - Manage Network' page not navigated");
}	
		
}else {
	Report.LogInfo("Info", "'View PE Device' page not navigated","FAIL");
	Reporter.log("'View PE Device' page navigated");
}
}catch(NoSuchElementException e) {
	e.printStackTrace();
	Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
	Reporter.log(  e+ " : Field is not displayed");
}catch(Exception e) {
	e.printStackTrace();
	Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
	Reporter.log(  e+" : Field is not displayed");
}


	}

public void verifyAddInterfaceFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String PE_AccessMedia= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_AccessMedia");

	String PE_Network= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_Network");

	
	String PE_VRRPBGP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPBGP");

	String PE_GenerateConfiguration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_GenerateConfiguration");

	String	PE_Interface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_Interface");

	String PE_InterfaceAddressRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceAddressRange");

	
	String PE_InterfaceAddressMask= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceAddressMask");

	String	PE_VRRPIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPIP");

	String	PE_InterfaceAddressRangeIPV6= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceAddressRangeIPV6");

	String PE_VRRPIPv6Address= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPIPv6Address");

	String	PE_PrimaryIPv6onMas1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_PrimaryIPv6onMas1");

	String PE_SecondaryIPv6onMas2= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_SecondaryIPv6onMas2");

	String PE_GroupNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_GroupNumber");

	String PE_Link= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_Link");

	String PE_VLANID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VLANID");

	String PE_VRRPGroupName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPGroupName");

	String PE_VRF= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRF");

	
	String	PE_IVManagement= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_IVManagement");

	String PE_Configuration= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_Configuration");

	String PE_VRRPTrackInterface= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPTrackInterface");

	String PE_VRRPAuthentication= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPAuthentication");

	
	
	try {
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_CommandIPV4_header);//RouterTool_header//MAS_PE_CommandIPV4_header//MAS_PE_InterfacesPanel_header
	//	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_InterfacesActionLink,"ACTION Link");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterfaceLink,"Add Interface Link");
		
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterface_header)) {
			Report.LogInfo("Info", "'PE Add Interface' page navigated as expected","PASS");
			Reporter.log("'PE Add Interface' page navigated as expected");
			
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configuration_label);
		
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1,"OK Button");
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationWarningMessage,"Configuration is required");
	scrollIntoTop();
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.PE_InterfaceSuggestionMessage,"Interface Suggestion Message");
	
	
	
	if(PE_AccessMedia.equalsIgnoreCase("VPN")) {
		
		scrollIntoTop();
		Report.LogInfo("Info",  " Selected Access Media is : "+PE_AccessMedia,"PASS");
		
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield, PE_InterfaceAddressRange, "Interface Address Range Textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield, PE_InterfaceAddressMask, "Interface Address Mask textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPIPTextfield, PE_VRRPIP, "VRRP IP textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeIPV6Textfield, PE_InterfaceAddressRangeIPV6, "Interface Address Range IPV6 textfield");
		
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_PrimaryIPv6onMas1Textfield, PE_PrimaryIPv6onMas1, "Primary IPv6 on Mas1 textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SecondaryIPv6onMas2Textfield, PE_SecondaryIPv6onMas2, "Secondary IPv6 on Mas2 textfield");
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox);
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield,PE_GroupNumber,"Group Number textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield,PE_Link,"Link textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield,PE_VLANID,"Interface textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRFTextfield,PE_VRF,"VRF textfield");

		//***click(application, "IV Management Checkbox", "MAS_PE_IVManagementCheckbox");
//		//IV Management Checkbox
//		if(getwebelement(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox")).isEnabled()){
//			if (PE_IVManagement.equalsIgnoreCase("YES")) {
//				click(application, "'IV Management' checkbox", "MAS_PE_IVManagementCheckbox");
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected");
//			} else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is not Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is not Selected");
//			}
//		
//			}else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status");
//			}

		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox, "IV Management", PE_IVManagement);
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton);
		//**scrollDown(getwebelement(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Configuration_label")));
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		
		
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown, PE_GenerateConfiguration, "Enter Generate Configuration");
		
		webDriver.findElement(By.xpath("//div[text()='"+ PE_GenerateConfiguration +"']")).click();
		
		waitforPagetobeenable();
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SaveConfigurationtoFileButton,"Save Configuration to File Button");
		
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton,"OK Button");
		
		compareText("Interface Added Successfully Message", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterfaceSuccessfullMessage, "Interface added successfully");
		Report.LogInfo("Info", "Interfaces sucessfully added by selecting VPN as Access Media","PASS");
		
	}else if(PE_AccessMedia.equalsIgnoreCase("EPN")) {
		scrollIntoTop();
		
		scrollIntoTop();
		Report.LogInfo("Info",  " Selected Access Media is : "+PE_AccessMedia,"PASS");
	//	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AccessMediaDropdown, "Access Media");
	//	waitforPagetobeenable();
	//	webDriver.findElement(By.xpath("//div[text()='"+PE_AccessMedia+"']")).click();;
				
		//addDropdownValues_commonMethod("VRRP BGP Dropdown", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPBGPDropdown, PE_VRRPBGP);
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield,"Interface Address Range Textfield");

		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield,PE_InterfaceAddressRange,"Interface Address Range Textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield,PE_InterfaceAddressMask,"Interface Address Mask textfield");
	//	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPIPTextfield,PE_VRRPIP, "VRRP IP textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeIPV6Textfield,PE_InterfaceAddressRangeIPV6, "Interface Address Range IPV6 textfield");
	//	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPIPv6AddressTextfield,PE_VRRPIPv6Address, "VRRP IPv6 Address textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_PrimaryIPv6onMas1Textfield,PE_PrimaryIPv6onMas1, "Primary IPv6 on Mas1 textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SecondaryIPv6onMas2Textfield,PE_SecondaryIPv6onMas2, "Secondary IPv6 on Mas2 textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield,PE_GroupNumber, "Group Number textfield");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield,PE_Link, "Link textfield");
	//	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield,PE_VLANID, "Interface textfield");
		//*sendKeys(PE_VRRPGroupName, "VRRP-Group Name textfield", "MAS_PE_VRRPGroupNameTextfield");//Bydefault
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRFTextfield,PE_VRF, "VRF textfield");

		//**click(application, "IV Management Checkbox", "MAS_PE_IVManagementCheckbox");
//		//IV Management Checkbox
//		if(getwebelement(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox")).isEnabled()){
//			if (PE_IVManagement.equalsIgnoreCase("YES")) {
//				click(application, "'IV Management' checkbox", "MAS_PE_IVManagementCheckbox");
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Enabled and 'IV Management' checkbox Selected");
//			} else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is not Selected","Info");
//				Reporter.log("Step : 'IV Management' checkbox is not Selected");
//			}
//		
//			}else {
//				Report.LogInfo("Info", "Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status","Info");
//				Reporter.log("Step : 'IV Management' checkbox is Disabled, So can't make any changes in Checkbox status");
//			}

		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox, "IV Management", PE_IVManagement);
		
		//sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPTrackInterfaceTextfield, PE_VRRPTrackInterface, "VRRP Track Interface textfield");
		//sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPAuthenticationTextfield, PE_VRRPAuthentication, "VRRP Authentication textfield");
		
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		//**EnterTextValue(application, PE_GenerateConfiguration, "Enter Generate Configuration", "MAS_PE_GenerateConfigurationDropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		webDriver.findElement(By.xpath("//div[text()='"+PE_GenerateConfiguration+"']")).click();
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SaveConfigurationtoFileButton,"Save Configuration to File Button");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration test", "Configuration");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1,"OK Button");
		
		compareText("Interface Added Successfully Message", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AddInterfaceSuccessfullMessage, "Interface added successfully");
		Report.LogInfo("Info", "Interfaces sucessfully added by selecting EPN as Access Media","PASS");			
		Reporter.log("------ Interface Added successfully ------");
		
	}else {
		Reporter.log("Access Media Dropdown is not selected as VPN or EPN");
		Report.LogInfo("Info", "Access Media Dropdown is not selected as VPN or EPN","FAIL");
	}
	
		}else {
			Report.LogInfo("Info", "'PE Add Interface' page not navigated","FAIL");
			Reporter.log("'PE Add Interface' page not navigated");
		}
		
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
		
	
}


public void verifyEditInterfaceFunction_PE(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	String PE_AccessMediaEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_AccessMediaEdit");

	String PE_NetworkEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_NetworkEdit");

	String PE_VRRPBGPEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPBGPEdit");

	String PE_GenerateConfigurationEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_GenerateConfigurationEdit");

	String	PE_InterfaceEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceEdit");

	String PE_InterfaceAddressRangeEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceAddressRangeEdit");

	
	String PE_InterfaceAddressMaskEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceAddressMaskEdit");

	String	PE_VRRPIPEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPIPEdit");

	String	PE_InterfaceAddressRangeIPV6Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceAddressRangeIPV6Edit");

	String PE_VRRPIPv6AddressEdit = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPIPv6AddressEdit");

	String	PE_PrimaryIPv6onMas1Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_PrimaryIPv6onMas1Edit");

	String PE_SecondaryIPv6onMas2Edit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_SecondaryIPv6onMas2Edit");

	String PE_GroupNumberEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_GroupNumberEdit");

	String PE_LinkEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_LinkEdit");

	String PE_VLANIDEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VLANIDEdit");

	String PE_VRRPGroupNameEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPGroupNameEdit");

	String PE_VRFEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRFEdit");

	String	PE_IVManagementEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_IVManagementEdit");

	String PE_ConfigurationEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_ConfigurationEdit");

	String PE_VRRPTrackInterfaceEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPTrackInterfaceEdit");

	String PE_VRRPAuthenticationEdit= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VRRPAuthenticationEdit");

	
	try {
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.ProviderEquipment_header);//ProviderEquipment_header//portalaccess_header//managementoptions_header
		//***click(application, "Show Interfaces Link", "MAS_ShowInterfaceLink");
		
	//	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_Checkbox1_ViewServicePage,"Select Checkbox interface"); 
	//	click(APT_VOIPAccess_Obj.VOIPAccess.PE_ACTION_Device1,"ACTION PE Device"); 
	//	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ACTION_EditLink_Device1); 
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ACTION_EditLink_Device1,"Edit Link"); 
	
		if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_EditInterface_header)) {
			Report.LogInfo("Info", "'PE Edit Interface' page navigated as expected","PASS");
			Reporter.log("'PE Edit Interface' page navigated as expected");
			
		try {
			
	if(PE_AccessMediaEdit.equalsIgnoreCase("VPN")) {
		Report.LogInfo("Info",  " Selected Access Media is : "+PE_AccessMediaEdit,"PASS");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_AccessMediaDropdown,"Access Media Dropdown");
		webDriver.findElement(By.xpath("//div[text()='"+ PE_AccessMediaEdit +"']")).click();
		
		waitForAjax();
		//*ClearAndEnterTextValue(application, "Interface textfield", "MAS_PE_InterfaceTextfield", PE_InterfaceEdit);
		ClearAndEnterTextValue("Interface Address Range Textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield, PE_InterfaceAddressRangeEdit);
		ClearAndEnterTextValue("Interface Address Mask textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield, PE_InterfaceAddressMaskEdit);
		ClearAndEnterTextValue("VRRP IP textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPIPTextfield, PE_VRRPIPEdit);
		ClearAndEnterTextValue("Interface Address Range IPV6 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeIPV6Textfield, PE_InterfaceAddressRangeIPV6Edit);
		
		ClearAndEnterTextValue("Primary IPv6 on Mas1 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_PrimaryIPv6onMas1Textfield, PE_PrimaryIPv6onMas1Edit);
		ClearAndEnterTextValue("Secondary IPv6 on Mas2 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SecondaryIPv6onMas2Textfield, PE_SecondaryIPv6onMas2Edit);
		ClearAndEnterTextValue("Group Number textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield, PE_GroupNumberEdit);
		ClearAndEnterTextValue("Link textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield, PE_LinkEdit);
		ClearAndEnterTextValue("VLAN ID textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield, PE_VLANIDEdit);
		ClearAndEnterTextValue("VRRP Group textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPGroupNameTextfield, PE_VRRPGroupNameEdit);
		ClearAndEnterTextValue("VRF textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRFTextfield, PE_VRFEdit);

		editcheckbox_commonMethod(PE_IVManagementEdit, APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox, "IV Management");
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton);
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		
		
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,PE_GenerateConfigurationEdit,"Enter Generate Configuration");
		webDriver.findElement(By.xpath("//div[text()='"+ PE_GenerateConfigurationEdit +"']")).click();

		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SaveConfigurationtoFileButton,"Save Configuration to File Button");
		
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1,"OK Button");
			
//		compareText(application, "Interface Updated Successfully Message", "MAS_PE_UpdateInterfaceSuccessfullMessage", "Interface updated successfully");
		verifysuccessmessage("Interface updated successfully");
		Report.LogInfo("Info", "Interfaces sucessfully updated by selecting VPN as Access Media","PASS");
		
	}else if(PE_AccessMediaEdit.equalsIgnoreCase("EPN")) {
		Report.LogInfo("Info",  " Selected Access Media is : "+PE_AccessMediaEdit,"PASS");
	//	click(APT_VOIPAccess_Obj.VOIPAccess.Acess_PE,"Access Media Dropdown");
		addDropdownValues_commonMethod("Access Media",APT_VOIPAccess_Obj.VOIPAccess.Acess_PE,PE_AccessMediaEdit);
		webDriver.findElement(By.xpath("//div[text()='"+ PE_AccessMediaEdit +"']")).click();
		
		//waitForAjax();
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield,"Interface text field");
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield,"Interface text field");

		ClearAndEnterTextValue("Interface Address Range Textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeTextfield, PE_InterfaceAddressRangeEdit);
		ClearAndEnterTextValue("Interface Address Mask textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressMaskTextfield, PE_InterfaceAddressMaskEdit);
		ClearAndEnterTextValue("VRRP IP textfield",APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPIPTextfield, PE_VRRPIPEdit);
		ClearAndEnterTextValue("Interface Address Range IPV6 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_InterfaceAddressRangeIPV6Textfield, PE_InterfaceAddressRangeIPV6Edit);
		ClearAndEnterTextValue("VRRP IPV6 Address textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPIPv6AddressTextfield, PE_VRRPIPv6AddressEdit);
		ClearAndEnterTextValue("Primary IPv6 on Mas1 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_PrimaryIPv6onMas1Textfield, PE_PrimaryIPv6onMas1Edit);
		ClearAndEnterTextValue("Secondary IPv6 on Mas2 textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SecondaryIPv6onMas2Textfield, PE_SecondaryIPv6onMas2Edit);
		ClearAndEnterTextValue("Group Number textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GroupNumberTextfield, PE_GroupNumberEdit);
		ClearAndEnterTextValue("Link textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_LinkTextfield, PE_LinkEdit);
		ClearAndEnterTextValue("VLAN ID textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VLANIDTextfield, PE_VLANIDEdit);
		ClearAndEnterTextValue("VRRP Group textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPGroupNameTextfield, PE_VRRPGroupNameEdit);
		ClearAndEnterTextValue("VRF textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRFTextfield, PE_VRFEdit);


		editcheckbox_commonMethod(PE_IVManagementEdit, APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox, "IV Management");

		ClearAndEnterTextValue("VRRP Track Interface textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPTrackInterfaceTextfield, PE_VRRPTrackInterfaceEdit);
		ClearAndEnterTextValue("VRRP Authentication textfield", APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_VRRPAuthenticationTextfield, PE_VRRPAuthenticationEdit);
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton);
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,"Generate Configuration Dropdown");
		
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_IVManagementCheckbox);
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationDropdown,PE_GenerateConfigurationEdit,"Enter Generate Configuration");
		webDriver.findElement(By.xpath("//div[text()='"+PE_GenerateConfigurationEdit+"']")).click();
	
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_GenerateConfigurationButton,"Generate Configuration Button");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_SaveConfigurationtoFileButton,"Save Configuration to File Button");
		sendKeys(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration test", "Configuration");
		getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_ConfigurationTextfield,"Configuration information after configuration generated");
		
		click(APT_VOIPAccess_Obj.VOIPAccess.MAS_PE_OKButton1,"OK Button");
		verifysuccessmessage("Interface updated successfully");
		Report.LogInfo("Info", "Interfaces sucessfully updated by selecting EPN as Access Media","PASS");
		Reporter.log("------ Interface Updated successfully ------");
	
}else {
	Reporter.log("Access Media Dropdown is not selected as VPN or EPN");
	Report.LogInfo("Info", "Access Media Dropdown is not selected as VPN or EPN","FAIL");
}



}catch(NoSuchElementException e) {
	e.printStackTrace();
	Report.LogInfo("Info", e+ " : Field is not displayed in Edit Interface page","FAIL");
	Reporter.log(  e+ " : Field is not displayed in  Edit Interface page");
}catch(Exception e) {
	e.printStackTrace();
	Report.LogInfo("Info",  e+" : Field is not displayed in  Edit Interface page ","FAIL");
	Reporter.log(  e+" : Field is not displayed in  Edit Interface page");
}

		}else {
			Report.LogInfo("Info", "'PE Edit Interface' page not navigated","FAIL");
			Reporter.log("'PE Edit Interface' page not navigated");
		}
		
		
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed in Edit Interface page","FAIL");
		Reporter.log(  e+ " : Field is not displayed in  Edit Interface page");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed in  Edit Interface page ","FAIL");
		Reporter.log(  e+" : Field is not displayed in  Edit Interface page");
	}
	
}

public void addTrunkGroupSiteOrderNumber(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, IOException { 
	
	String TrunkGroupOrderCheckboxStatus= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupOrderCheckboxStatus");

	String TrunkGroupSiteOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");

	
	boolean TrunkGroupSiteOrderPanel_header=false;
	boolean addtrunkSiteorderPage_panelheader=false;
	boolean trunkgrupOrderErrMsg= false;
	try {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PE_AddPEDeviceLink);
	TrunkGroupSiteOrderPanel_header= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.TrunkGroupSiteOrderPanel_header);
	if(TrunkGroupSiteOrderPanel_header) {
		Report.LogInfo("Info", "'Trunk Group/Site Orders' panel is displaying as expected in 'view Service' page","PASS");
		Reporter.log("'Trunk Group/Site Orders' panel is displaying as expected in 'view Service' page");
		
		
	//Perform Add Site order
		click(APT_VOIPAccess_Obj.VOIPAccess.addTrunkSiteOrderlink, "Add TrunkGroup/SiteOrder link");
		
		addtrunkSiteorderPage_panelheader= isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.addtrunkSiteorderPage_panelheader);
		if(addtrunkSiteorderPage_panelheader) {
			
			Report.LogInfo("Info", "'Add Trunk Group/Site Order' page is displaying as expected","PASS");
			Reporter.log("'Add Trunk Group/Site Order' page is displaying as expected");
			
		//verify mandatory Warning Message
			click(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton, "OK");
			
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.trunkGrouporder_warningmsg, "Trunk Group Order");
			
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.trunkGroupOrderName_warningmsg, "Trunk group Order Number");
			
		//Add trunkgroup Order checkbox
			if(TrunkGroupOrderCheckboxStatus.equalsIgnoreCase("yes")) {
				
				addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.trunkGroupOrder_checkbox, "Trunk Group Order", TrunkGroupOrderCheckboxStatus);
			}else {
				Report.LogInfo("Info", " ' Trunk Group order' checkbox is a mandatory field. No values passed","FAIL");
				Reporter.log(" ' Trunk Group order' checkbox is a mandatory field. No values passed");

			}
			
		//Add Trunk Group order Number text field
			sendKeys(APT_VOIPAccess_Obj.VOIPAccess.trunkGroupOrderName_textField, TrunkGroupSiteOrderNumber,"Trunk Group Order Number");
			
			
		//Click on OK button
			click(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton, "OK");
			//waitforPagetobeenable();
			
//			compareText(application, "Add Trunk Group/Site Order Creation success message", "alertMSg_siteorderCreation", "Trunk Group created successfully", xml);
			verifysuccessmessage("Trunk Group created successfully");
			try {
			trunkgrupOrderErrMsg=isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.alertMSg_siteorderCreation);
			String actualMsg=getTextFrom(APT_VOIPAccess_Obj.VOIPAccess.alertMSg_siteorderCreation,"AlertMSg_siteorderCreation");
			if(trunkgrupOrderErrMsg) {
				if(actualMsg.contains("1.trunkgroup number already exists")) {
					
					Report.LogInfo("Info", " Error message we are getting as: "+ actualMsg,"FAIL");
					Reporter.log(" Error message we are getting as: "+ actualMsg);
				}
				else if(actualMsg.equalsIgnoreCase("Trunk Group created successfully")) {
					
					Report.LogInfo("Info", " Success Message displays as 'Trunk Group created successfully'","PASS");
					Reporter.log(" Success Message displays as 'Trunk Group created successfully'");
				}
				
			}
			}catch(Exception e) {
				e.printStackTrace();
				
			}
			
			
		}else {
			Report.LogInfo("Info", "'Add Trunk Group/Site Order' page is not displaying","PASS");
			Reporter.log("'Add Trunk Group/Site Order' page is not displaying");
		}
		
	}else {
		Report.LogInfo("Info", "'Trunk Group/Site Orders' panel is not displaying in 'view Service' page","PASS");
		Reporter.log("'Trunk Group/Site Orders' panel is not displaying in 'view Service' page");
	}
	
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	
}

public void verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException, IOException, IOException {
	
	String TrunkGroupSiteOrderNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupSiteOrderNumber");


	
	try {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	
	WebElement siteOrderUnderTrunkPanel;
	
	
	siteOrderUnderTrunkPanel=webDriver.findElement(By.xpath("//span[text()='"+ TrunkGroupSiteOrderNumber +"']"));
	
	if(siteOrderUnderTrunkPanel.isDisplayed()) {
		
		Report.LogInfo("Info", TrunkGroupSiteOrderNumber + " 'Site Order' is displaying under 'Trunk' panel","PASS");
		Reporter.log(TrunkGroupSiteOrderNumber + " 'Site Order' is displaying under 'Trunk' panel");
		
	//Click on Add trunk link	
		
		click(APT_VOIPAccess_Obj.VOIPAccess.AddTrunkLink,"Add Trunk");
		
		
								}else {
		Report.LogInfo("Info", TrunkGroupSiteOrderNumber + " 'Site Order' is not displaying under 'Trunk' panel","FAIL");
		Reporter.log(TrunkGroupSiteOrderNumber + " 'Site Order' is not displaying under 'Trunk' panel");
									 }
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		Report.LogInfo("Info", e+ " : Field is not displayed","FAIL");
		Reporter.log(  e+ " : Field is not displayed");
	}catch(Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info",  e+" : Field is not displayed","FAIL");
		Reporter.log(  e+" : Field is not displayed");
	}
	
}

public void verifyDeleteDeviceFunction_MAS(String testDataFile, String dataSheet, String scriptNo, String dataSetNo
) throws InterruptedException,  IOException {
	
	String ServiceIdentification= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

	//Delete MAS Device from View Device Page
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.portalaccess_header);//ProviderEquipment_header//portalaccess_header//managementoptions_header
	click(APT_VOIPAccess_Obj.VOIPAccess.devicefirst,"View Link"); 
	
	scrollIntoTop();
	if (isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice_Header)) {
		Report.LogInfo("Info", "'View MAS Device' page navigated as expected","PASS");
		Reporter.log("'View MAS Device' page navigated as expected");
		
	
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_View_Action_DeleteLink,"Delete Device from View Device page");
	compareText("Delete MAS Device Warning Message from View Device page", APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice_Action_DeleteMASDeviceWarningMessage, "Are you sure that you want to delete this item?");
	click(APT_VOIPAccess_Obj.VOIPAccess.MAS_ViewDevice_Action_DeleteButton,"Delete Button");
	compareText("Delete MAS Device Successful Message", APT_VOIPAccess_Obj.VOIPAccess.MAS_DeleteSwitchSuccessfulMessage, "MAS switch deleted successfully");
	
	
	
	}else {
		Report.LogInfo("Info", "'View MAS Device' page not navigated","FAIL");
		Reporter.log("'View MAS Device' page not navigated");
	}
	
}

public void Clickon_addToggleButton(String xpath) throws InterruptedException, IOException {

	//Add Toggle button
	click(xpath,"Toggle button");
	waitforPagetobeenable();
}


public static Boolean isFileDownloaded(String fileName, String downloadspath) {
	boolean flag = false;
	//paste your directory path below
	//eg: C:\\Users\\username\\Downloads
	String dirPath = downloadspath; 
	File dir = new File(dirPath);
	File[] files = dir.listFiles();
	try {
	if (files.length == 0 || files == null) {
		System.out.println("The directory is empty");
		Report.LogInfo("Info", "Step : Downloads folder is empty","FAIL");
		flag = false;
	} else {
		for (File listFile : files) {
			if (listFile.getName().contains(fileName)) {
				System.out.println(fileName + " is present");
				Report.LogInfo("Info", "Step : '"+fileName+"' excel file is downloaded successfully","PASS");
				break;
			}
			flag = true;
		}
	}
	}
	catch (Exception e) {
		Report.LogInfo("Info", "Step : Downloads folder path '"+dirPath+"' is not found","FAIL");
	}
	return flag;
}

public void verifyAddDRPlans(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String Add_DRplanA=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanA"); 
	String Add_DRplanB=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanB"); 
	String Add_DRplanC=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanC"); 
	String Add_DRplanD=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanD"); 
	String Add_DRplanE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Add_DRplanE"); 
	String rangestart_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangestart_cc"); 
	String rangestart_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangestart_lac"); 
	String rangestart_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangestart_num"); 
	String rangefinish_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangefinish_cc"); 
	String rangefinish_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangefinish_lac"); 
	String rangefinish_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"rangefinish_num"); 
	String destinationnumber_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"destinationnumber_cc"); 
	String destinationnumber_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"destinationnumber_lac"); 
	String destinationnumber_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"destinationnumber_num"); 
	String activate_deactivateDRplan_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"activate_deactivateDRplan_dropdownvalue"); 
	String edit_rangestart_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangestart_cc"); 
	String edit_rangestart_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangestart_lac"); 
	String edit_rangestart_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangestart_num"); 
	String edit_rangefinish_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangefinish_cc"); 
	String edit_rangefinish_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangefinish_lac"); 
	String edit_rangefinish_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_rangefinish_num"); 
	String edit_destinationnumber_cc=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_destinationnumber_cc"); 
	String edit_destinationnumber_lac=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_destinationnumber_lac");  
	String edit_destinationnumber_num=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_destinationnumber_num");  
	String edit_activate_deactivateDRplan_dropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"edit_activate_deactivateDRplan_dropdownvalue"); 

	waitforPagetobeenable();
	
try {
	waitforPagetobeenable();
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.customizedmedia_header);
	
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.recovery_header,"Disaster Recovery Header");
	click(APT_VOIPAccess_Obj.VOIPAccess.recovery_header,"Disaster Recovery Header");
	
	//click_commonMethod( "Add DR Plan", "addDRplans_link");
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.addDRplans_link,"Add DR Plan");
	click(APT_VOIPAccess_Obj.VOIPAccess.addDRplans_link,"Add DR Plan");
	
	waitforPagetobeenable();
	compareText( "Add DR Plans Header", APT_VOIPAccess_Obj.VOIPAccess.addDRplan_header, "Disaster Recovery Plans");
	
	if(Add_DRplanA.equalsIgnoreCase("Yes")) {
		scrollIntoTop();
		compareText( "DR Plan A_header", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_header, "DR Plan A");
		
		//click_commonMethod( "DR Plan A Action dropdown", "DRplanA_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanA_actiondropdown,"DR Plan A Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanA_actiondropdown,"DR Plan A Action dropdown");
		
		//click_commonMethod( "Add", "DRplan_addlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_addlink,"Add");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		addedDRplan( "DR Plan A", rangestart_cc, rangestart_lac, rangestart_num);
		//click_commonMethod( "DR Plan A Action dropdown", "DRplanA_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanA_actiondropdown,"DR Plan A Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanA_actiondropdown,"DR Plan A Action dropdown");
		
		//click_commonMethod( "Edit", "DRplan_editlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_editlink,"Edit");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_editlink,"Edit");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		if(edit_rangestart_cc.equalsIgnoreCase("Yes") || edit_rangestart_cc.equalsIgnoreCase("null") || edit_rangestart_lac.equalsIgnoreCase("Yes")|| edit_rangestart_lac.equalsIgnoreCase("null") || edit_rangestart_num.equalsIgnoreCase("Yes")||edit_rangestart_num.equalsIgnoreCase("null"))
		{
			editedDRplan( "DR Plan A", rangestart_cc, rangestart_lac, rangestart_num, edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num);
		}
		else
		{
			addedDRplan( "DR Plan A", rangestart_cc, rangestart_lac, rangestart_num);	
		}
		//click_commonMethod( "DR Plan A Action dropdown", "DRplanA_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanA_actiondropdown,"DR Plan A Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanA_actiondropdown,"DR Plan A Action dropdown");
		
		//click_commonMethod( "Delete", "DRplan_Deletelink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_Deletelink,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_Deletelink,"Delete");
		
		deleteDRPlan();

	}
	if(Add_DRplanB.equalsIgnoreCase("Yes")) {
		scrollDown( "DRplanA_header");
		
		compareText( "DR Plan B_header", APT_VOIPAccess_Obj.VOIPAccess.DRplanB_header, "DR Plan B");
		//click_commonMethod( "DR Plan B Action dropdown", "DRplanB_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanB_actiondropdown, "DR Plan B Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanB_actiondropdown, "DR Plan B Action dropdown");
		
		//click_commonMethod( "Add", "DRplan_addlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_addlink,"Add");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		scrollDown( APT_VOIPAccess_Obj.VOIPAccess.DRplanA_header);
		
		addedDRplan( "DR Plan B", rangestart_cc, rangestart_lac, rangestart_num);
		
		//click_commonMethod( "DR Plan B Action dropdown", "DRplanB_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanB_actiondropdown, "DR Plan B Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanB_actiondropdown, "DR Plan B Action dropdown");
		
		//click_commonMethod( "Edit", "DRplan_editlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_editlink,"Edit");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_editlink,"Edit");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		scrollDown( APT_VOIPAccess_Obj.VOIPAccess.DRplanA_header);
		
		if(edit_rangestart_cc.equalsIgnoreCase("Yes") || edit_rangestart_cc.equalsIgnoreCase("null") || edit_rangestart_lac.equalsIgnoreCase("Yes")|| edit_rangestart_lac.equalsIgnoreCase("null") || edit_rangestart_num.equalsIgnoreCase("Yes")||edit_rangestart_num.equalsIgnoreCase("null"))
		{
			editedDRplan( "DR Plan B", rangestart_cc, rangestart_lac, rangestart_num, edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num);
		}
		else
		{
			addedDRplan( "DR Plan B", rangestart_cc, rangestart_lac, rangestart_num);	
		}
		//click_commonMethod( "DR Plan B Action dropdown", "DRplanB_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanB_actiondropdown,"DR Plan B Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanB_actiondropdown,"DR Plan B Action dropdown");
		
		//click_commonMethod( "Delete", "DRplan_Deletelink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_Deletelink,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_Deletelink,"Delete");
		
		deleteDRPlan();
	}
	if(Add_DRplanC.equalsIgnoreCase("Yes")) {

		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.DRplanB_header);
		
		compareText( "DR Plan C_header", APT_VOIPAccess_Obj.VOIPAccess.DRplanC_header, "DR Plan C");
		//click_commonMethod( "DR Plan C Action dropdown", "DRplanC_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanC_actiondropdown,"DR Plan C Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanC_actiondropdown,"DR Plan C Action dropdown");
		
		//click_commonMethod( "Add", "DRplan_addlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_addlink,"Add");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		scrollDown( "DRplanB_header");
		
		addedDRplan( "DR Plan C", rangestart_cc, rangestart_lac, rangestart_num);
		//click_commonMethod( "DR Plan C Action dropdown", "DRplanC_actiondropdown");
		
		//click_commonMethod( "Edit", "DRplan_editlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanC_actiondropdown, "DR Plan C Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanC_actiondropdown, "DR Plan C Action dropdown");
		
		//click_commonMethod( "Edit", "DRplan_editlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_editlink,"Edit");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_editlink,"Edit");
		
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		scrollDown( "DRplanB_header");
		
		if(edit_rangestart_cc.equalsIgnoreCase("Yes") || edit_rangestart_cc.equalsIgnoreCase("null") || edit_rangestart_lac.equalsIgnoreCase("Yes")|| edit_rangestart_lac.equalsIgnoreCase("null") || edit_rangestart_num.equalsIgnoreCase("Yes")||edit_rangestart_num.equalsIgnoreCase("null"))
		{
			editedDRplan( "DR Plan C", rangestart_cc, rangestart_lac, rangestart_num, edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num);
		}
		else
		{
			addedDRplan( "DR Plan C", rangestart_cc, rangestart_lac, rangestart_num);	
		}
	//	click_commonMethod( "DR Plan C Action dropdown", "DRplanC_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanC_actiondropdown,"DR Plan C Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanC_actiondropdown,"DR Plan C Action dropdown");
		
		//click_commonMethod( "Delete", "DRplan_Deletelink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplan_Deletelink,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplan_Deletelink,"Delete");
		
		deleteDRPlan();

	}
	if(Add_DRplanD.equalsIgnoreCase("Yes")) {
		scrollDown( "DRplanC_header");
		
		compareText( "DR Plan D_header", APT_VOIPAccess_Obj.VOIPAccess.DRplanD_header, "DR Plan D");
		//click_commonMethod( "DR Plan D Action dropdown", "DRplanD_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_actiondropdown,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_actiondropdown,"Delete");
		
	//	click_commonMethod( "Add", "DRplanD_E_addlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_addlink,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_addlink,"Delete");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangestart_columnheader, "Range Start");
		compareText( "Range Finish", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		scrollDown( APT_VOIPAccess_Obj.VOIPAccess.DRplanC_header);
		
		addedDRplan( "DR Plan D", rangestart_cc, rangestart_lac, rangestart_num);
		//click_commonMethod( "DR Plan D Action dropdown", "DRplanD_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_actiondropdown,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_actiondropdown,"Delete");
		
		//click_commonMethod( "Edit", "DRplanD_E_editlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_editlink,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_editlink,"Delete");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		scrollDown( APT_VOIPAccess_Obj.VOIPAccess.DRplanC_header);
		
		if(edit_rangestart_cc.equalsIgnoreCase("Yes") || edit_rangestart_cc.equalsIgnoreCase("null") || edit_rangestart_lac.equalsIgnoreCase("Yes")|| edit_rangestart_lac.equalsIgnoreCase("null") || edit_rangestart_num.equalsIgnoreCase("Yes")||edit_rangestart_num.equalsIgnoreCase("null"))
		{
			editedDRplan( "DR Plan D", rangestart_cc, rangestart_lac, rangestart_num, edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num);
		}
		else
		{
			addedDRplan( "DR Plan D", rangestart_cc, rangestart_lac, rangestart_num);	
		}
		//click_commonMethod( "DR Plan D Action dropdown", "DRplanD_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_actiondropdown,"DR Plan D Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_actiondropdown,"DR Plan D Action dropdown");
		
		//click_commonMethod( "Delete", "DRplanD_E_Deletelink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_Deletelink,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_Deletelink,"Delete");
		
		deleteDRPlan();

	}
	if(Add_DRplanE.equalsIgnoreCase("Yes")) {
		scrollDown( "DRplanD_header");
		
		compareText( "DR Plan E_header", "DRplanE_header", "DR Plan E");
	//	click_commonMethod( "DR Plan E Action dropdown", "DRplanE_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanE_actiondropdown,"DR Plan E Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanE_actiondropdown,"DR Plan E Action dropdown");
		//click_commonMethod( "Add", "DRplanD_E_addlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_addlink,"Add");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_addlink,"Add");
		
		waitforPagetobeenable();
		AddDRPlan( rangestart_cc, rangestart_lac, rangestart_num, rangefinish_cc, rangefinish_lac, rangefinish_num, destinationnumber_cc, destinationnumber_lac, destinationnumber_num, activate_deactivateDRplan_dropdownvalue);
		compareText( "Range Start", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangestart_columnheader, "Range Start");
		compareText( "Range Finish",APT_VOIPAccess_Obj.VOIPAccess.DRplanA_rangefinish_columnheader, "Range Finish");
		compareText( "Destination Number(DN)",APT_VOIPAccess_Obj.VOIPAccess.DRplanA_destinationnumber_columnheader, "Destination Number(DN)");
		compareText( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.DRplanA_activateDeactivate_columnheader, "Activate/Deactivate DR Plan");

		//Edit DR Plan
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_header);
		
		addedDRplan( "DR Plan E", rangestart_cc, rangestart_lac, rangestart_num);
		
		//click_commonMethod( "DR Plan E Action dropdown", "DRplanE_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanE_actiondropdown,"DR Plan E Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanE_actiondropdown,"DR Plan E Action dropdown");
		
		//click_commonMethod( "Edit", "DRplanD_E_editlink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_editlink,"Edit");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_editlink,"Edit");
		
		waitforPagetobeenable();
		EditDRPlan( edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num, edit_rangefinish_cc, edit_rangefinish_lac, edit_rangefinish_num, edit_destinationnumber_cc, edit_destinationnumber_lac, edit_destinationnumber_num, edit_activate_deactivateDRplan_dropdownvalue);

		//Delete DR Plan
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_header);
		
		if(edit_rangestart_cc.equalsIgnoreCase("Yes") || edit_rangestart_cc.equalsIgnoreCase("null") || edit_rangestart_lac.equalsIgnoreCase("Yes")|| edit_rangestart_lac.equalsIgnoreCase("null") || edit_rangestart_num.equalsIgnoreCase("Yes")||edit_rangestart_num.equalsIgnoreCase("null"))
		{
			editedDRplan( "DR Plan E", rangestart_cc, rangestart_lac, rangestart_num, edit_rangestart_cc, edit_rangestart_lac, edit_rangestart_num);
		}
		else
		{
			addedDRplan( "DR Plan E", rangestart_cc, rangestart_lac, rangestart_num);	
		}
		//click_commonMethod( "DR Plan E Action dropdown", "DRplanE_actiondropdown");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanE_actiondropdown,"DR Plan E Action dropdown");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanE_actiondropdown,"DR Plan E Action dropdown");
		
		//click_commonMethod( "Delete", "DRplanD_E_Deletelink");
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_Deletelink,"Delete");
		click(APT_VOIPAccess_Obj.VOIPAccess.DRplanD_E_Deletelink,"Delete");
		
		deleteDRPlan();
	}
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.viewpage_backbutton);
	//click_commonMethod( "Back", "viewpage_backbutton");
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.viewpage_backbutton,"Back");
	click(APT_VOIPAccess_Obj.VOIPAccess.viewpage_backbutton,"Back");
	
	waitforPagetobeenable();
}catch(NoSuchElementException e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, e+ " : Field is not displayed");
	Report.LogInfo("INFO",  e+ " : Field is not displayed","FAIL");
}catch(Exception e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL,  e+" : Field is not displayed");
	Report.LogInfo("INFO", e+" : Field is not displayed", "FAIL"); 
}

}

public void verifyAddTrunkFunction(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String customerName= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Trunk_CustomerName");
	String servicename= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String ServiceType= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
	String PrimaryTrunk= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PrimaryTrunk");
	String voipProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "voipProtocol");
	String country= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "BillingCountry");
	String CDRdelivery= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CDRdelivery");
	String ResellerID= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerID");
	String gateway= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "gateway");
	String quality= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "quality");
	String ipAddresstype= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ipAddresstype");
	String IPPBXRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IPPBXRange");
	String  IPPBXAddress= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IPPBXAddress");
	String SIPsignallingPort= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SIPsignallingPort");
	String internetBasedCustomer= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "internetBasedCustomer");
	String vlanTag= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vlanTag");
	String subInterfaceSlot= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "subInterfaceSlot");
	String signallngZone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "signallngZone");
	String signallingtransportProtocol= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "signallingtransportProtocol");
	String  TLSfield= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TLSfield");
	String  srtp= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "srtp");
	String SignalingPort= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SignalingPort");
	String CustomerDefaultNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerDefaultNumber");
	String ReuseNetworkSelectorTable= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ReuseNetworkSelectorTable");
	String reuseNIFgroup= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "reuseNIFgroup");
	String reuseSigZonePart= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "reuseSigZonePart");
	String coltSignalingIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "coltSignalingIP");
	String mediaIP= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "mediaIP");
	String callAdmissionControl= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "callAdmissionControl");
	String CallLimitDropdwon= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CallLimitDropdwon");
	String CallRateLimitCheckboxSelection= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CallRateLimitCheckboxSelection");
	String limitNumber= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "limitNumber");
	String callrateLimiteValue= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "callrateLimiteValue");
	String sourceAddressFiltering= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "sourceAddressFiltering");
	String relSupport= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "relSupport");
	String sipSessionkeepAliveTimer= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "sipSessionkeepAliveTimer");
	String PBXTYPE=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PBXTYPE");
	String PBXProfile= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PBXProfile");
	String PSXManualConfigurationTrunkGroup= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PSXManualConfigurationTrunkGroup");
	String PSXManualConfigurationDDIRange= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PSXManualConfigurationDDIRange");
	String ManualConfigurationonGSX= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManualConfigurationonGSX");
	String Carrier= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Carrier");
	String IPSignalingProfile= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IPSignalingProfile");
	String EgressIPSignalingProfile= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EgressIPSignalingProfile");
	  
	  
	String gatewayCode=null;
	String subInterfacename_starting="SIF-1-";
	String subInterfacename_middle="-2-";
	
	String NIFgroupDEfaultValue_starting="SIF-1-";
	String NIFgroupDEfaultValue_middle="-2-";
	String NIGgroupdefaultValue_last="-OUTSIDE";
	
	String ipInterfaceDEfaultValue="EXTERNAL_IPIF_";
	String addressContextDefaultValue="EXTERNAL_AC_";
	String ipInterfaceGroupDefaultvalue="EXTERNAL_IPIG_";
	String signallingZoneDefaultValue="OUT-UNTRUSTED";
	
	
	String SubInterfaceName=null;
	String NIFgroup=null;
	String ipInterface=null;
	String addressContext=null;
	String ipInterfaceGroup=null;
	String prefix_code=null;
	
	try {
	scrollIntoTop();
	
	if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.AddTrunk_Header)) {
	
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Trunk' Page navigated as expected");
		Report.LogInfo("INFO","'Add Trunk' Page navigated as expected","PASS");
		
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	//click_commonMethod("OK Button", "trunk_okButton");
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton,"OK Button");
	click(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton,"OK Button");
	scrollIntoTop();
	
			//Trunk Group Description
		String expectedValue1=customerName+"_"+servicename+"_"+ServiceType.toUpperCase();
		compareText("Trunk Group Description", APT_VOIPAccess_Obj.VOIPAccess.trunkGroupDEscription_textField, expectedValue1);

	
			//Primary Trunk	
	//**SelectDropdownValueUnderSelectTag("Primary Trunk", PrimaryTrunk, "PrimaryTrunk_Dropdown");
	
		
			//VOIP Protocol
		//SelectDropdownValueUnderSelectTag("VOIP Protocol", voipProtocol, APT_VOIPAccess_Obj.VOIPAccess.voipProtocol_Dropdown);
		selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.voipProtocol_Dropdown, voipProtocol, "VOIP Protocol");
		
		//Billing COuntry
	//warningMessage_commonMethod("country_warningMessage", "Billing Country");   //validate warning message
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.country_warningMessage,"Billing Country");
	
	selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.billingCoutry_Dropdown, country, "Billing Country");
	
	String country_code=getAttributeFrom(APT_VOIPAccess_Obj.VOIPAccess.billingCoutry_Dropdown,"Value");
	
	//System.out.println("country dropdon value is: "+country_code);
	
	
			//CDR Delivery
	//SelectDropdownValueUnderSelectTag("CDR Delivery", CDRdelivery, APT_VOIPAccess_Obj.VOIPAccess.CDRdelivery_Dropdown);
	selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.CDRdelivery_Dropdown, CDRdelivery, "CDR Delivery");
	
	 
	 	//Gateway
	//SelectDropdownValueUnderSelectTag("Gateway", gateway, APT_VOIPAccess_Obj.VOIPAccess.gateway_Dropdown);
	selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.gateway_Dropdown, gateway, "Gateway");

	gatewayCode=gateway_code(gateway);
	
	WebElement PrimaryTrunk2=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.PrimaryTrunk_Dropdown);
	scrolltoview(PrimaryTrunk2);
	
	
	
		//Quality
	//SelectDropdownValueUnderSelectTag("Quality", quality, APT_VOIPAccess_Obj.VOIPAccess.quality_Dropdown);
	selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.quality_Dropdown, quality, "Quality");
	String quality_code=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.quality_Dropdown).getAttribute("value");
	
	
		//IP Address Type
	//SelectDropdownValueUnderSelectTag("IP Address Type", ipAddresstype, APT_VOIPAccess_Obj.VOIPAccess.IPaddresstype_Dropdown);
	selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.IPaddresstype_Dropdown, ipAddresstype, "IP Address Type");

	WebElement IPAddressType=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.IPaddresstype_Dropdown);
	scrolltoview(IPAddressType);
	
			//IP PBX Range
	//warningMessage_commonMethod("IPPBXRange_warningMessage", "IP PBX Range");
	
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.IPPBXRange_warningMessage,"IP PBX Range");
	
	//EnterTextValue(IPPBXRange, "IP PBX Range text field", APT_VOIPAccess_Obj.VOIPAccess.IPPBXRange_textField);
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.IPPBXRange_textField,IPPBXRange,"IP PBX Range text field");
	
	
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.IPPBXRange_addButtton,"Click on >>");
	click(APT_VOIPAccess_Obj.VOIPAccess.IPPBXRange_addButtton,"Click on >>");
	
	GetTheValuesInsideDropdown(findWebElement(APT_VOIPAccess_Obj.VOIPAccess.IPPBXRange_addedValue_selectDropdownField), "IP PBX range Selection Box");
	
	
		//IP PBX Address
	//warningMessage_commonMethod("IPPBXAddress_warningMessage", "IP PBX Address");
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.IPPBXAddress_warningMessage,"IP PBX Address");
	
	//EnterTextValue(IPPBXAddress, "IP PBX Address text field", "IPPBXAddress_textField");
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.IPPBXAddress_textField,IPPBXAddress,"IP PBX Address text field");
	
	
	
		//SIP Signalling Port
  if(voipProtocol.equalsIgnoreCase("SIP")) {
	  edittextFields_commonMethod("SIP Signaling Port", APT_VOIPAccess_Obj.VOIPAccess.SIPsignallingport_textField, SIPsignallingPort);
	  
	  //message displaying under "SIP Signalling Port" text field	
		methodToFindMessagesUnderTextField(APT_VOIPAccess_Obj.VOIPAccess.SIPsignallingPOrt_defaultValue_textvalue, "SIP Signalling Port", "Default Port:5060");
  }else {
	  ExtentTestManager.getTest().log(LogStatus.PASS, "'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-1'");
	  Report.LogInfo("INFO","'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-I'","PASS");
  }
	

  
  	//Splitting the Gateway functionality into 2 SBC & Non-SBC 
  if(!gateway.contains("SBC")) {    //CASE-1 For NO-SBC
	
	  if(internetBasedCustomer.equalsIgnoreCase("Yes")) {  //case A with Interface Based Customer selection
		  
		  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
		  
		//Internet Based Customer
			addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
			
		  
		  String vlanDefaultvalue=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).getAttribute("value");
		  if(vlanDefaultvalue.isEmpty()) {
			  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
		  }else {
			 
			  ExtentTestManager.getTest().log(LogStatus.PASS, "When 'Internet Based Customer' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue);
			  if(vlanTag.equalsIgnoreCase("null")) {
					
					//Sub Interface slot
					selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
					
					if(subInterfaceSlot.equalsIgnoreCase("null")) {
						
						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue;
						compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
						
						//NIF Group  
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
						
						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue;
						compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
						
						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
					}
					
				}else {
					
					//VLAN Tag
					edittextFields_commonMethod("VLAN Tag", APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField, vlanTag);

				//Sub Interface slot
					selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
					
					if(subInterfaceSlot.equalsIgnoreCase("null")) {

						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTag;
						compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
						
						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
						compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
					}
					else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
						
						//Sub Interface Name
						SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanTag;
						compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
						
						//NIF Group
						NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
						//System.out.println("NIF Group value is: "+NIFgroup);
						compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
					}
				}
		  }
		  
		  
		  
		  
	  } else {       // case B without Interface Based Customer selection
		if(vlanTag.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' field is  a mandatory field and not values are passed as an input");
			
			//Sub Interface slot
			selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
			
			if(subInterfaceSlot.equalsIgnoreCase("null")) {
				
				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfacename_middle;
				compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+NIGgroupdefaultValue_last;
				compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
			}
			else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
				
				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle;
				compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+NIGgroupdefaultValue_last;
				//System.out.println("NIF Group value is: "+NIFgroup);
				compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
			}
			
			
		}else {
			
			//VLAN Tag
			edittextFields_commonMethod("VLAN Tag", APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField, vlanTag);

		//Sub Interface slot
			selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
			
			if(subInterfaceSlot.equalsIgnoreCase("null")) {

				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTag;
				compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
				compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
			}
			else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
				
				//Sub Interface Name
				SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanTag;
				compareText("Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
				
				//NIF Group
				NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
				//System.out.println("NIF Group value is: "+NIFgroup);
				compareText("NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
			}
		}
		
	//Signalling port
		signallingPort(gateway);
	  }
  }
 
  
  
  //CASE-2 For SBC
  else if(gateway.contains("SBC")) {
	  
	  if(gateway.startsWith("FRA")) {//Case A 
		  
		  if(!internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  
			  if(vlanTag.equalsIgnoreCase("null")) {
				 
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is a mandatory field. No values has been passed as an input");
			 
				//IP Interface
				  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
				  if(ipInterface_Enabled) {
					  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				  }
				  compareText("IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values
				  
			//Address Context
				  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
				  if(addressContext_Enabled) {
					  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				  }
				  
				  compareText("Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField, addressContextDefaultValue);  //verify default values
				
				  
			//IP Interface Group
				  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
				  if(ipInterfaceGroup_Enabled) {
					  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				  }
				  
				  compareText("IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values
				
				  
			//Signalling port
					signallingPort(gateway);	 
					
					
		
			  }else if(!vlanTag.equalsIgnoreCase("null")) {
				 
				//VLAN Tag
					edittextFields_commonMethod("VLAN Tag", APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField, vlanTag);

					//IP Interface
					  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
					  if(ipInterface_Enabled) {
						  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
						  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
					  }else {
						  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
						  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
					  }
					  
					  ipInterface = ipInterfaceDEfaultValue +vlanTag;
					  compareText("IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField,ipInterface );  
					  
				//Address Context
					  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
					  if(addressContext_Enabled) {
						  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
						  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
					  }else {
						  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
						  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
					  }
					  
					  addressContext=addressContextDefaultValue+vlanTag;
					  compareText("Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField,addressContext );  //verify default values
					
					  
				//IP Interface Group
					  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
					  if(ipInterfaceGroup_Enabled) {
						  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
						  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
					  }else {
						  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
						  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
					  }
					  
					  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanTag;
					  compareText("IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
			 
			  }
			  
			  
			  
			  
		  }else if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
			  
				//Internet Based Customer
					addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
					
			  String vlanDefaultvalue=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).getAttribute("value");
			  if(vlanDefaultvalue.isEmpty()) {
					  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
			  }else {
				//VLAN tag  
				  boolean VlanEnability=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).isEnabled();
				  if(VlanEnability) {
					  Report.LogInfo("INFO","VLAN Tag is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","VLAN Tag is disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
				  } 
			  }
			  
				//IP Interface
			  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
			  if(ipInterface_Enabled) {
				  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
			  }
			  
			  ipInterface = ipInterfaceDEfaultValue +vlanDefaultvalue;
			  compareText("IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField,ipInterface );  
			  
		//Address Context
			  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
			  if(addressContext_Enabled) {
				  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
			  }
			  
			  addressContext=addressContextDefaultValue+vlanDefaultvalue;
			  compareText("Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField,addressContext );  //verify default values
			
			  
		//IP Interface Group
			  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
			  if(ipInterfaceGroup_Enabled) {
				  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
			  }
			  
			  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanDefaultvalue;
			  compareText("IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
		  }
		  
		  
	  }else {  //CASE-B non-FRA
		 if(!internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  
			//VLAN tag  
				  boolean VlanEnability=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).isEnabled();
				  if(VlanEnability) {
					  Report.LogInfo("INFO","VLAN Tag is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","VLAN Tag is disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
				  }
				  
			//IP Interface
				  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
				  if(ipInterface_Enabled) {
					  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				  }
				  compareText("IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values
				  
			//Address Context
				  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
				  if(addressContext_Enabled) {
					  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				  }
				  
				  compareText("Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField, addressContextDefaultValue);  //verify default values
				
				  
			//IP Interface Group
				  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
				  if(ipInterfaceGroup_Enabled) {
					  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				  }
				  
				  compareText("IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values
				
				  
			//Signalling port
					signallingPort(gateway);	  
					
			}
		 
		 if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
			  
			  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
			  
			//Internet Based Customer
				addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
				
		  String vlanDefaultvalue=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).getAttribute("value");
		  if(vlanDefaultvalue.isEmpty()) {
			  Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
		  }else {
			//VLAN tag  
			  boolean VlanEnability=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).isEnabled();
			  if(VlanEnability) {
				  Report.LogInfo("INFO","VLAN Tag is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","VLAN Tag is disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
			  } 
		  }
		  
			//IP Interface
		  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
		  if(ipInterface_Enabled) {
			  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
		  }else {
			  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
			  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
		  }
		  
		  ipInterface = ipInterfaceDEfaultValue +vlanDefaultvalue;
		  compareText("IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField,ipInterface );  
		  
	//Address Context
		  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
		  if(addressContext_Enabled) {
			  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
		  }else {
			  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
			  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
		  }
		  
		  addressContext=addressContextDefaultValue+vlanDefaultvalue;
		  compareText("Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField,addressContext );  //verify default values
		
		  
	//IP Interface Group
		  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
		  if(ipInterfaceGroup_Enabled) {
			  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
		  }else {
			  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
			  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
		  }
		  
		  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanDefaultvalue;
		  compareText("IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
		 }
	 }
  }
  

  								//STARTED SHOW ALL PANEL
  		//Signalling Zone
  if(internetBasedCustomer.equalsIgnoreCase("yes")) {
	  
	  compareText("Signaling Zone", APT_VOIPAccess_Obj.VOIPAccess.signallingZone_textField, signallingZoneDefaultValue);
	  
	  edittextFields_commonMethod("Signaling Zone", APT_VOIPAccess_Obj.VOIPAccess.signallingZone_textField, signallngZone);
	  
  }else {
	  edittextFields_commonMethod("Signaling Zone", APT_VOIPAccess_Obj.VOIPAccess.signallingZone_textField, signallngZone);
  }
	


  		//Signalling Transport Protocol
	selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.signallingTransportProtocol_Dropdown, "Signalling Transport Protocol", signallingtransportProtocol);

	WebElement internetBasedCusotmerView=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox);
	scrolltoview(internetBasedCusotmerView);
	
	
	
	if(signallingtransportProtocol.equalsIgnoreCase("sip-tls-tcp")) {
		
		//TLS Profile
		edittextFields_commonMethod("TLS Profile", APT_VOIPAccess_Obj.VOIPAccess.TLS_textField, TLSfield);
		
		//SRTP
		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.srtp_checkbox, "SRTP", srtp);
	}
	
	
		// Customer Default Number	CustomerDefaultNumber_WarningMessage
	//warningMessage_commonMethod("CustomerDefaultNumber_WarningMessage", "Customer Default Number");
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.CustomerDefaultNumber_WarningMessage,"Customer Default Number");
	edittextFields_commonMethod("Customer Default Number", APT_VOIPAccess_Obj.VOIPAccess.CustomerDefaultNumber_textfield, CustomerDefaultNumber);

	//Show All
//			scrolltoview(getwebelement(xml.getlocator("//locators/" + application + "/CustomerDefaultNumber_Header")));
//			click_commonMethod("Show ALL Button", "ShowAllButton_ViewTrunk");
			scrollDown(APT_VOIPAccess_Obj.VOIPAccess.AddTrunk_CENTREX_Header);
			
	
		// Reuse Network Selector Table
		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.ReuseNetworkSelectorTable_checkbox, "Reuse Network Selector Table", ReuseNetworkSelectorTable);

		
		//Reuse NIF Group
		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.reuseNIFgroup_checkbox, "Reuse NIF Group", reuseNIFgroup);
		
				
		//Reuse Sig Zone/Part
		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.reuseSigZonePart_checkbox, "Reuse Sig Zone/Part", reuseSigZonePart);
	
	
	
	
		// Colt Signalling IP
		edittextFields_commonMethod("Colt Signalling IP", APT_VOIPAccess_Obj.VOIPAccess.coltSignalingIP_textField, coltSignalingIP);
	
		//Media IP
	edittextFields_commonMethod("Media IP", APT_VOIPAccess_Obj.VOIPAccess.mediaIP_textField, mediaIP);
	
	
	
	
		// PBX Type	
	edittextFields_commonMethod("PBX Type", APT_VOIPAccess_Obj.VOIPAccess.PBXType_textfield, PBXTYPE);

		// PBX Profile
	selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.PBXProfile_Dropdown, "PBX Profile", PBXProfile);


	
			// PSX Manual Configuration-Trunk Group	
		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.PSXManualConfigurationTrunkGroup_checkbox, "PSX Manual Configuration-Trunk Group", PSXManualConfigurationTrunkGroup );
		
			// PSX Manual Configuration-DDI-Range
		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.PSXManualConfigurationDDIRange_checkbox, "PSX Manual Configuration-DDI-Range", PSXManualConfigurationDDIRange);


			// Manual Configuration On GSX
		addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.ManualConfigurationonGSX_checkbox, "Manual Configuration On GSX", ManualConfigurationonGSX);
		
		
		
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.NonServiceImpacting_Header);
		//Call Admission Control
	addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.callAdmissionControl_checkbox, "Call Admission Control", callAdmissionControl);

	if(callAdmissionControl.equalsIgnoreCase("yes")) {
		//Call Limit
		selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.callLimit_Dropdown, "Call Limit", CallLimitDropdwon);
		
		if(CallLimitDropdwon.equalsIgnoreCase("Defined")) {
			edittextFields_commonMethod("Limit Number", APT_VOIPAccess_Obj.VOIPAccess.limitNumber_textField, limitNumber);
			
		}
	}
	
	
		//Call Rate Limit
	addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.callrateLimit_checkbox, "Call Rate Limit", CallRateLimitCheckboxSelection);
	if(CallRateLimitCheckboxSelection.equalsIgnoreCase("yes")) {
		
		String callratelimitactualvalue=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.callRateLimitt_textField).getAttribute("value");
		 Report.LogInfo("INFO"," 'Call rate Limit' value is displaying as "+callratelimitactualvalue,"PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'Call rate Limit' value is displaying as "+callratelimitactualvalue);
	
		if(!callrateLimiteValue.equalsIgnoreCase("null")) {
			int i=Integer.parseInt(callrateLimiteValue);
				
			if(i>100) {
				 Report.LogInfo("INFO","The CallRateLimit should be less 100 for all Trunks","FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "The CallRateLimit should be less 100 for all Trunks");
			}
			else if(i<=100){
				edittextFields_commonMethod("Call rate Limit", APT_VOIPAccess_Obj.VOIPAccess.callRateLimitt_textField, callrateLimiteValue);
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Call rate Limit' value is not edited");
			 Report.LogInfo("INFO","'Call rate Limit' value is not edited","PASS");
		}
	}
	
	
	
	
	
	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.SIPSERVICES_Header);
		//Source Address Filtering
	selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.sourceAddressFiltering_Dropdown, "Source Address Filtering", sourceAddressFiltering);
	
		//100rel Support
	selectValueInsideDropdown(APT_VOIPAccess_Obj.VOIPAccess.relsupport_Dropdown, "100rel Support", relSupport);
	
		//SIP Session Keepalive Timer(Sec)
	edittextFields_commonMethod("SIP Session Keepalive Timer(Sec)", APT_VOIPAccess_Obj.VOIPAccess.sipSessionKeepAliverTimer_textField, sipSessionkeepAliveTimer);

	//Text message under "SIP Session Keepalive timer"
		methodToFindMessagesUnderTextField(APT_VOIPAccess_Obj.VOIPAccess.defaultTextMessageUnderSIPsessionTimer, "SIP Session Keepalive Timer(Sec)", "Default SIP Session Keepalive Timer (sec): 1800");
	
		
		
		
		
		
		scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PSXTRUNKGROUPFIELDS_Header);
		//Carrier
		verifyExists(APT_VOIPAccess_Obj.VOIPAccess.Carrier_warningMessage, "Carrier");   //validate warning message
		
		edittextFields_commonMethod("Carrier textfield", APT_VOIPAccess_Obj.VOIPAccess.carriers_TextField, Carrier);
		
		
		//IP Signalling Profile
		edittextFields_commonMethod("IP Signaling Profile", APT_VOIPAccess_Obj.VOIPAccess.IPsignallingProfile_textField, IPSignalingProfile);
				
				
		//Egress IP Signaling Profile
		edittextFields_commonMethod("Egress IP Signaling Profile", APT_VOIPAccess_Obj.VOIPAccess.EgressipSignal_TextField, EgressIPSignalingProfile);

	
		
		WebElement reusenifgroup=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.reuseNIFgroup_checkbox);
		scrolltoview(reusenifgroup);
		
		
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	
	
	verifyExists(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton,"OK");
	click(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton,"OK");
	
	waitforPagetobeenable();

	
	
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add Trunk' Page is not navigated");
		Report.LogInfo("INFO","'Add Trunk' Page is not navigated","FAIL");
	}
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, e+ " : Field is not displayed");
		Report.LogInfo("INFO",  e+ " : Field is not displayed","FAIL");
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL,  e+" : Field is not displayed");
		Report.LogInfo("INFO", e+" : Field is not displayed","FAIL");
	}
	
	
  }

public void verifyAddedTrunkInformationInviewTrunk( String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {


	String customerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Trunk_CustomerName");
	String servicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String ServiceType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
	String PrimaryTrunk=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PrimaryTrunk");
	String voipProtocol=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voipProtocol");
	
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BillingCountry");
	String CDRdelivery=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CDRdelivery");
	String ResellerID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResellerID");
	
	String gateway=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"gateway");
	String quality=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"quality");
	String ipAddresstype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ipAddresstype");
	String IPPBXRange=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPPBXRange");
	String  IPPBXAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPPBXAddress");
	String SIPsignallingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPsignallingPort");
	
	String internetBasedCustomer=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"internetBasedCustomer");
	String vlanTag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vlanTag");
	String subInterfaceSlot=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"subInterfaceSlot");
	String signallngZone=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"signallngZone");
	String signallingtransportProtocol=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"signallingtransportProtocol");
	
	String  TLSfield=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TLSfield");
	String   srtp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"srtp");
	String SignalingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SignalingPort");
	String CustomerDefaultNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CustomerDefaultNumber");
	String ReuseNetworkSelectorTable=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ReuseNetworkSelectorTable");
	
	String reuseNIFgroup=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"reuseNIFgroup");
	String reuseSigZonePart=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"reuseSigZonePart");
	String coltSignalingIP=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"coltSignalingIP");
	String mediaIP=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"mediaIP");
	String callAdmissionControl=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callAdmissionControl");
	String CallLimitDropdwon=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallLimitDropdwon");
	String CallRateLimitCheckboxSelection =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallRateLimitCheckboxSelection");
	String limitNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"limitNumber");
	
	String callrateLimiteValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimiteValue");
	String sourceAddressFiltering=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"sourceAddressFiltering");
	String relSupport=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"relSupport");
	String sipSessionkeepAliveTimer=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"sipSessionkeepAliveTimer");
	String PBXTYPE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PBXTYPE");
	String PBXProfile=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PBXProfile");
	String PSXManualConfigurationTrunkGroup=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXManualConfigurationTrunkGroup");
	String PSXManualConfigurationDDIRange=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXManualConfigurationDDIRange");
	String ManualConfigurationonGSX=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManualConfigurationonGSX");
	String Carrier=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Carrier");
	String IPSignalingProfile=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPSignalingProfile");
	String EgressIPSignalingProfile=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EgressIPSignalingProfile");

	scrollIntoTop();
	waitforPagetobeenable();
		
		String AddressContext="EXTERNAL_AC_";
		String IPINTERFACEGROUP ="EXTERNAL_IPIG_";
		String IPINTERFACE=	"EXTERNAL_IPIF_";
		
		String subInterfacename_starting="SIF-1-";
		String subInterfacename_middle="-2-";
		String NIFgroupDEfaultValue_starting="SIF-1-";
		String NIFgroupDEfaultValue_middle="-2-";
		String NIGgroupdefaultValue_last="-OUTSIDE";
try {
	waitforPagetobeenable();
		scrollIntoTop();
		
			ExtentTestManager.getTest().log(LogStatus.PASS, "'View Trunk' Page navigated as expected");
			Report.LogInfo("INFO","'View Trunk' Page navigated as expected","PASS");

			//Quality
				compareText_InViewPage( "Quality", quality);
				
				
			//Trunk Group Name
			//***	compareText_InViewPage( "Trunk Group Name", primarytrunkGroupname);//Need to do
				
				
			//IP Address Type
				compareText_InViewPage( "IP Address Type", ipAddresstype);
				
			//IP PBX Range
				compareText_InViewPage( "IP PBX Range", IPPBXRange);
				
			//IP PBX Address
				compareText_InViewPage( "IP PBX Address", IPPBXAddress);
			

			//SIP Signaling Port
				  if(voipProtocol.equalsIgnoreCase("SIP")) {
					  compareText_InViewPage( "SIP Signalling Port", SIPsignallingPort);
				  }else {
					  System.out.println(" 'SIP Signalling port' will not display, if voip protocol selected as 'SIP-1'");
				  }
				
		  //Sub Interface Slot
			 String subinterfaceSlot_viewPage=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'Sub Interface Slot')]]]/div[2]")).getText();
				 
		//VLAN Tag 
				  if(!gateway.contains("SBC")) {
						
					  if(internetBasedCustomer.equalsIgnoreCase("no")) {
						  
						  //VLAN Tag
//						  compareText_InViewPage( "VLAN Tag", vlanTag);
						  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
						  
						  //Sub Interface Name
						  	String SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlan_actualValue;
							compareText_InViewPage( "Sub Interface Name", SubInterfaceName);
							
							//NIF Group
							String NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlan_actualValue+NIGgroupdefaultValue_last;
							
							compareText_InViewPage( "NIF Group", NIFgroup);
						  
						  //Signalling Zone
							if(signallngZone.equalsIgnoreCase("null")) {
						  compareText_InViewPage( "Signalling Zone", "OUT-UNTRUSTED");
							}
							else if(!signallngZone.equalsIgnoreCase("null")) {
								  compareText_InViewPage( "Signalling Zone", signallngZone);
									}
						  
							
					  }else if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
						 
						  //VLAN Tag
//						  compareText_InViewPage( "VLAN Tag", vlanTag);
						  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
						  
						  //Sub Interface Name
						  	String SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlan_actualValue;
							compareText_InViewPage( "Sub Interface Name", SubInterfaceName);
							
							//NIF Group
							String NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlan_actualValue+NIGgroupdefaultValue_last;
							
							compareText_InViewPage( "NIF Group", NIFgroup);
						  
							//Signalling Zone
							if(signallngZone.equalsIgnoreCase("null")) {
						  compareText_InViewPage( "Signalling Zone", "OUT-UNTRUSTED");
							}
							else if(!signallngZone.equalsIgnoreCase("null")) {
								  compareText_InViewPage( "Signalling Zone", signallngZone);
									}
						  
					  }
					  
					  
				  }else if(gateway.contains("SBC")) {
					  if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
						  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
						
						//Signalling Zone
							if(signallngZone.equalsIgnoreCase("null")) {
						  compareText_InViewPage( "Signalling Zone", "OUT-UNTRUSTED");
							}
							else if(!signallngZone.equalsIgnoreCase("null")) {
								  compareText_InViewPage( "Signalling Zone", signallngZone);
									}
						  
						  
					  }else if(internetBasedCustomer.equalsIgnoreCase("No")) {
						  
						  //VLAN Tag
						  compareText_InViewPage( "VLAN Tag", vlanTag);
						  
						  String vlan_actualValue=webDriver.findElement(By.xpath("//div[div[label[contains(text(),'VLAN Tag')]]]/div[2]")).getText();
			
					  }
				  }
			//Signalling Transport Protocol
			compareText_InViewPage( "Signalling Transport Protocol", signallingtransportProtocol);
				
				
			
			//Signalling Port
			String actualSignalingPort=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.SignallingPortValue).getText();
					if(internetBasedCustomer.equalsIgnoreCase("Yes")) {	
					
						//Or signallingPort_viewPage( gateway);
					}
					
					
			
			//Customer Default Number
			compareText_InViewPage( "Customer Default Number", CustomerDefaultNumber);


			
			//Show All
			scrollDown(APT_VOIPAccess_Obj.VOIPAccess.CustomerDefaultNumber_Header);
			verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ShowAllButton_ViewTrunk,"Show ALL Button");
			click(APT_VOIPAccess_Obj.VOIPAccess.ShowAllButton_ViewTrunk,"Show ALL Button");
			//scrolltoview(getwebelement(xml.getlocator("//locators/" + application + "/CENTREX_Header")));

			
			
			//Reuse Network Selector Table	
			compareText_InViewPage( "Reuse Network Selector Table", ReuseNetworkSelectorTable);
					
			//Reuse NIF Group	
			compareText_InViewPage( "Reuse NIF Group", reuseNIFgroup);
						
			//Reuse Sig Zone/Port
			compareText_InViewPage( "Reuse Sig Zone/Part", reuseSigZonePart);				
					
			
			
			
			scrollDown(APT_VOIPAccess_Obj.VOIPAccess.CENTREX_Header);
			//Colt Signalling IP
			compareText_InViewPage( "Colt Signalling IP", coltSignalingIP);
			
			//Media IP
			compareText_InViewPage( "Media IP", mediaIP);
				

				
			// PBX Type	
			compareText_InViewPage( "PBX Type", PBXTYPE);

			// PBX Profile
			compareText_InViewPage( "PBX Profile", PBXProfile);



			// PSX Manual Configuration-Trunk Group	
			compareText_InViewPage( "PSX Manual Configuration-Trunk Group", PSXManualConfigurationTrunkGroup);
		
			// PSX Manual Configuration-DDI-Range
			compareText_InViewPage( "PSX Manual Configuration-DDI-Range", PSXManualConfigurationDDIRange);

			// Manual Configuration On GSX
			compareText_InViewPage( "Manual Configuration on GSX", ManualConfigurationonGSX);
			
			
			//scrolltoview(getwebelement(xml.getlocator("//locators/" + application + "/MANUALCONFIGURATION_Header")));
			//Call Admission Control
				compareText_InViewPage( "Call Admission Control", callAdmissionControl);
				
				if(callAdmissionControl.equalsIgnoreCase("yes")) {
				  //call limit	
					compareText_InViewPage( "Call Limit",CallLimitDropdwon );
					
					if(CallLimitDropdwon.equalsIgnoreCase("Defined")) {
						//Limit Number
						compareText_InViewPage( "Limit Number", limitNumber );
					}
				}
			
			//Call Rate Limit
				if(CallRateLimitCheckboxSelection.equalsIgnoreCase("Yes")) {
					
					//call rate limit value
					compareText_InViewPage("Call Rate Limit", callrateLimiteValue);
				}
				
				
			//Source Address Filtering
				compareText_InViewPage( "Source Address Filtering", sourceAddressFiltering);
				
			//100rel Support	
				compareText_InViewPage( "100rel Support", relSupport);
				
			//SIP session Keepalive timer
				compareText_InViewPage( "SIP Session Keepalive Timer(Sec)", sipSessionkeepAliveTimer);
				


			// Carrier	
				compareText_InViewPage( "Carrier", Carrier);
				
			// IP Signaling Profile
				compareText_InViewPage( "IP Signaling Profile", IPSignalingProfile);
				
			// Egress IP Signaling Profile
				compareText_InViewPage( "Egress IP Signaling Profile", EgressIPSignalingProfile);
				
				
			
			
	}catch(NoSuchElementException e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, e+ " : Field is not displayed");
		Report.LogInfo("INFO",  e+ " : Field is not displayed","FAIL");
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL,  e+" : Field is not displayed");
		Report.LogInfo("INFO",  e+" : Field is not displayed","FAIL");
	}

			
				
		}

public void verifyEditTrunkFunction (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String customerName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Trunk_CustomerName");
	String servicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
	String ServiceType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
	String PrimaryTrunk=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PrimaryTrunkEdit");
	String voipProtocol=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"voipProtocolEdit");
	String country =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BillingCountryEdit");
	String CDRdelivery=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CDRdeliveryEdit");
	String ResellerID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ResellerIDEdit");
	String gateway=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"gatewayEdit");
	String quality=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"qualityEdit");
	String ipAddresstype=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ipAddresstypeEdit");
	String IPPBXRange=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPPBXRangeEdit");
	String  IPPBXAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPPBXAddressEdit");
	String SIPsignallingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SIPsignallingPortEdit");
	String internetBasedCustomer=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"internetBasedCustomerEdit");
	String vlanTag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vlanTagEdit");
	String subInterfaceSlot=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"subInterfaceSlotEdit");
	String signallngZone =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"signallngZoneEdit");
	String signallingtransportProtocol =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"signallingtransportProtocolEdit");
	String  TLSfield=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TLSfieldEdit");
	String   srtp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"srtpEdit");
	String SignalingPort=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SignalingPortEdit");
	String CustomerDefaultNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CustomerDefaultNumberEdit");
	String ReuseNetworkSelectorTable=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ReuseNetworkSelectorTableEdit");
	String reuseNIFgroup=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"reuseNIFgroupEdit");
	String reuseSigZonePart=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"reuseSigZonePartEdit");
	String coltSignalingIP=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"coltSignalingIPEdit");
	String mediaIP=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"mediaIPEdit");
	String callAdmissionControl=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callAdmissionControlEdit");
	String CallLimitDropdwon=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallLimitDropdwonEdit");
	String CallRateLimitCheckboxSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallRateLimitCheckboxSelectionEdit");
	String limitNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"limitNumberEdit");
	String callrateLimiteValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"callrateLimiteValueEdit");
	String sourceAddressFiltering=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"sourceAddressFilteringEdit");
	String relSupport=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"relSupportEdit");
	String sipSessionkeepAliveTimer=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"sipSessionkeepAliveTimerEdit");
	String PBXTYPE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PBXTYPEEdit");
	String PBXProfile=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PBXProfileEdit");
	String PSXManualConfigurationTrunkGroup=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXManualConfigurationTrunkGroupEdit");
	String PSXManualConfigurationDDIRange=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PSXManualConfigurationDDIRangeEdit");
	String ManualConfigurationonGSX=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManualConfigurationonGSXEdit");
	String Carrier=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CarrierEdit");
	String IPSignalingProfile=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPSignalingProfileEdit");
	String EgressIPSignalingProfile=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EgressIPSignalingProfileEdit");
	String ExistingTrunkName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingTrunkName");
  
  
String gatewayCode=null;
String subInterfacename_starting="SIF-1-";
String subInterfacename_middle="-2-";

String NIFgroupDEfaultValue_starting="SIF-1-";
String NIFgroupDEfaultValue_middle="-2-";
String NIGgroupdefaultValue_last="-OUTSIDE";

String ipInterfaceDEfaultValue="EXTERNAL_IPIF_";
String addressContextDefaultValue="EXTERNAL_AC_";
String ipInterfaceGroupDefaultvalue="EXTERNAL_IPIG_";
String signallingZoneDefaultValue="OUT-UNTRUSTED";


String SubInterfaceName=null;
String NIFgroup=null;
String ipInterface=null;
String addressContext=null;
String ipInterfaceGroup=null;
String prefix_code=null;

waitforPagetobeenable();


try {
scrollIntoTop();
//Action button	
//verifyExists(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_ActionButton,"Action");//viewPage_ActionButton
//click(APT_VOIPAccess_Obj.VOIPAccess.ViewTrunkPage_ActionButton,"Action");

//click on Edit link
//click_commonMethod( "Edit", "ViewTrunkPage_EditLink");//editLink//ViewTrunkPage_EditLink
((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
click(APT_VOIPAccess_Obj.VOIPAccess.EditTrunkInTable,"Edit in table");
//webDriver.findElement(By.xpath("(//tr//td[contains(.,'"+ExistingTrunkName+"')]//following-sibling::td//a[text()='Edit'])[1]")).click();

waitforPagetobeenable();
if(isElementPresent(APT_VOIPAccess_Obj.VOIPAccess.EditTrunk_Header)) {
	ExtentTestManager.getTest().log(LogStatus.PASS, "'Edit Trunk' Page navigated as expected");
	Report.LogInfo("INFO","'Edit Trunk' Page navigated as expected","PASS");
//scrollIntoTop();


try {

		//Trunk Group Description
	String expectedValue1=customerName+"_"+servicename+"_"+ServiceType.toUpperCase();
	compareText( "Trunk Group Description", APT_VOIPAccess_Obj.VOIPAccess.trunkGroupDEscription_textField, expectedValue1);


		//VOIP Protocol
	selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.voipProtocol_Dropdown,voipProtocol, "VOIP Protocol"  );

	
	//Billing COuntry
	selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.billingCoutry_Dropdown,country,"Billing Country");

String country_code=findWebElement( APT_VOIPAccess_Obj.VOIPAccess.billingCoutry_Dropdown).getAttribute("value");



		//CDR Delivery
selectByVisibleText( APT_VOIPAccess_Obj.VOIPAccess.CDRdelivery_Dropdown,CDRdelivery,"CDR Delivery" );

//Gateway
selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.gateway_Dropdown, gateway,"Gateway" );
gatewayCode=gateway_code( gateway);

WebElement PrimaryTrunk2=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.PrimaryTrunk_Dropdown);
scrolltoview(PrimaryTrunk2);



	//Quality
selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.quality_Dropdown, quality, "Quality");

String quality_code=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.quality_Dropdown).getAttribute("value");


	//IP Address Type
selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.IPaddresstype_Dropdown, ipAddresstype, "IP Address Type");

WebElement IPAddressType=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.IPaddresstype_Dropdown);
scrolltoview(IPAddressType);

//IP PBX Address
sendKeys( APT_VOIPAccess_Obj.VOIPAccess.IPPBXAddress_textField,IPPBXAddress, "IP PBX Address text field");

edittextFields_commonMethod( "IP PBX Address text field", APT_VOIPAccess_Obj.VOIPAccess.IPPBXAddress_textField, IPPBXAddress);

	//SIP Signalling Port
if(voipProtocol.equalsIgnoreCase("SIP")) {
  edittextFields_commonMethod( "SIP Signaling Port", APT_VOIPAccess_Obj.VOIPAccess.SIPsignallingport_textField, SIPsignallingPort);
  
  //message displaying under "SIP Signalling Port" text field	
	methodToFindMessagesUnderTextField( APT_VOIPAccess_Obj.VOIPAccess.SIPsignallingPOrt_defaultValue_textvalue, "SIP Signalling Port", "Default Port:5060");
}else {
  ExtentTestManager.getTest().log(LogStatus.PASS, "'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-1'");
Report.LogInfo("INFO","'SIP Signalling Port' text field will not display, if 'VOIP Protocol' selected as 'SIP-I'","PASS");
}
	//Splitting the Gateway functionality into 2 SBC & Non-SBC 
if(!gateway.contains("SBC")) {    //CASE-1 For NO-SBC

  if(internetBasedCustomer.equalsIgnoreCase("Yes")) {  //case A with Interface Based Customer selection
	  
	  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
	  
	//Internet Based Customer
		addCheckbox_commonMethod( APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
		
	  
	  String vlanDefaultvalue=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).getAttribute("value");
	  if(vlanDefaultvalue.isEmpty()) {
		  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
		  Report.LogInfo("INFO", "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected","FAIL");
	  }else {
		 
		  ExtentTestManager.getTest().log(LogStatus.PASS, "When 'Internet Based Customer' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue);
		  Report.LogInfo("INFO","When 'Internet Based Customer' is selected, 'VLAN tag' field value is displaying as "+vlanDefaultvalue,"PASS");
		  if(vlanTag.equalsIgnoreCase("null")) {
				
				//Sub Interface slot
				selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
				
				if(subInterfaceSlot.equalsIgnoreCase("null")) {
					
					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanDefaultvalue;
					compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
					
					//NIF Group  
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
					compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
				}
				else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
					
					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanDefaultvalue;
					compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
					
					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanDefaultvalue+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
				}
				
			}else {
				
				//VLAN Tag
				edittextFields_commonMethod( "VLAN Tag", APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField, vlanTag);

			//Sub Interface slot
				selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
				
				if(subInterfaceSlot.equalsIgnoreCase("null")) {

					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTag;
					compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
					
					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
					compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
				}
				else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
					
					//Sub Interface Name
					SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanTag;
					compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
					
					//NIF Group
					NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
					//System.out.println("NIF Group value is: "+NIFgroup);
					compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
				}
			}
	  }
	    
  } else {       // case B without Interface Based Customer selection
	if(vlanTag.equalsIgnoreCase("null")) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' field is  a mandatory field and not values are passed as an input");
		 Report.LogInfo("INFO", " 'VLAN Tag' field is  a mandatory field and not values are passed as an input","FAIL");
		//Sub Interface slot
		selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
		
		if(subInterfaceSlot.equalsIgnoreCase("null")) {
			
			//Sub Interface Name
			SubInterfaceName=subInterfacename_starting+subInterfacename_middle;
			compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
			
			//NIF Group
			NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+NIGgroupdefaultValue_last;
			compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
		}
		else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
			
			//Sub Interface Name
			SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle;
			compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
			
			//NIF Group
			NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+NIGgroupdefaultValue_last;
		//	System.out.println("NIF Group value is: "+NIFgroup);
			compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
		}
		
		
	}else {
		
		//VLAN Tag
		edittextFields_commonMethod( "VLAN Tag", APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField, vlanTag);

	//Sub Interface slot
		selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.subInterfaceSlot_Dropdown, "Sub Interface Slot", subInterfaceSlot);
		
		if(subInterfaceSlot.equalsIgnoreCase("null")) {

			//Sub Interface Name
			SubInterfaceName=subInterfacename_starting+subInterfacename_middle+vlanTag;
			compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
			
			//NIF Group
			NIFgroup=NIFgroupDEfaultValue_starting+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
			compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
		}
		else if(!subInterfaceSlot.equalsIgnoreCase("null")) {
			
			//Sub Interface Name
			SubInterfaceName=subInterfacename_starting+subInterfaceSlot+subInterfacename_middle+vlanTag;
			compareText( "Sub Interface Name", APT_VOIPAccess_Obj.VOIPAccess.subInterfaceName_textField, SubInterfaceName);
			
			//NIF Group
			NIFgroup=NIFgroupDEfaultValue_starting+subInterfaceSlot+NIFgroupDEfaultValue_middle+vlanTag+NIGgroupdefaultValue_last;
			//System.out.println("NIF Group value is: "+NIFgroup);
			compareText( "NIF Group", APT_VOIPAccess_Obj.VOIPAccess.NIFgrouopp_textField, NIFgroup);
		}
	}
	
//Signalling port
	signallingPort( gateway);
  }
}

//CASE-2 For SBC
else if(gateway.contains("SBC")) {
  
  if(gateway.startsWith("FRA")) {//Case A 
	  
	  if(!internetBasedCustomer.equalsIgnoreCase("Yes")) {
		  
		  if(vlanTag.equalsIgnoreCase("null")) {
			 
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is a mandatory field. No values has been passed as an input");
			  Report.LogInfo("INFO", " 'VLAN Tag' text field is a mandatory field. No values has been passed as an input","FAIL");
			//IP Interface
			  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
			  if(ipInterface_Enabled) {
				  Report.LogInfo("INFO", "'Ip Interface' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
			  }else {
				  Report.LogInfo("INFO", "'Ip Interface' text fieldis disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
			  }
			  compareText( "IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField, ipInterfaceDEfaultValue);  //verify default values
			  
		//Address Context
			  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
			  if(addressContext_Enabled) {
				  Report.LogInfo("INFO", "'Address Context' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
			  }else {
				  Report.LogInfo("INFO", "'Address Context' text fieldis disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
			  }
			  
			  compareText( "Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField, addressContextDefaultValue);  //verify default values
			
		//IP Interface Group
			  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
			  if(ipInterfaceGroup_Enabled) {
				  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
			  }
			  
			  compareText( "IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values
			
		//Signalling port
				signallingPort( gateway);	 
				
		  }else if(!vlanTag.equalsIgnoreCase("null")) {
			 
			//VLAN Tag
			  editcheckbox_commonMethod( "VLAN Tag", APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField, vlanTag);

				//IP Interface
				  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
				  if(ipInterface_Enabled) {
					  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
				  }
				  
				  ipInterface = ipInterfaceDEfaultValue +vlanTag;
				  compareText( "IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField,ipInterface );  
				  
			//Address Context
				  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
				  if(addressContext_Enabled) {
					  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
				  }
				  
				  addressContext=addressContextDefaultValue+vlanTag;
				  compareText( "Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField,addressContext );  //verify default values
				
				  
			//IP Interface Group
				  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
				  if(ipInterfaceGroup_Enabled) {
					  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
					  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
				  }else {
					  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
					  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
				  }
				  
				  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanTag;
				  compareText( "IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
		 
		  }
		  
		  
		  
		  
	  }else if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
		  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
		  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
			//Internet Based Customer
				addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
				
		  String vlanDefaultvalue=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).getAttribute("value");
		  if(vlanDefaultvalue.isEmpty()) {
				  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
				  Report.LogInfo("INFO","No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected","FAIL");
		  }else {
			//VLAN tag  
			  boolean VlanEnability=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).isEnabled();
			  if(VlanEnability) {
				  Report.LogInfo("INFO","VLAN Tag is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","VLAN Tag is disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
			  } 
		  }
		  
			//IP Interface
		  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
		  if(ipInterface_Enabled) {
			  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
		  }else {
			  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
			  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
		  }
		  
		  ipInterface = ipInterfaceDEfaultValue +vlanDefaultvalue;
		  compareText( "IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField,ipInterface );  
		  
	//Address Context
		  boolean addressContext_Enabled=isEnable(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField);
		  if(addressContext_Enabled) {
			  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
		  }else {
			  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
			  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
		  }
		  
		  addressContext=addressContextDefaultValue+vlanDefaultvalue;
		  compareText( "Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField,addressContext );  //verify default values
		
		  
	//IP Interface Group
		  boolean ipInterfaceGroup_Enabled=isEnable(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField);
		  if(ipInterfaceGroup_Enabled) {
			  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
		  }else {
			  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
			  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
		  }
		  
		  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanDefaultvalue;
		  compareText( "IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
	  }

  }else {  //CASE-B non-FRA
	 if(!internetBasedCustomer.equalsIgnoreCase("Yes")) {
		  
		//VLAN tag  
			  boolean VlanEnability=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).isEnabled();
			  if(VlanEnability) {
				  Report.LogInfo("INFO","VLAN Tag is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","VLAN Tag is disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
			  }
			  
		//IP Interface
			  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
			  if(ipInterface_Enabled) {
				  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
			  }
			  compareText( "IP Interface", "ipInterface_textField", ipInterfaceDEfaultValue);  //verify default values
			  
		//Address Context
			  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
			  if(addressContext_Enabled) {
				  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
			  }
			  
			  compareText( "Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField, addressContextDefaultValue);  //verify default values
			
			  
		//IP Interface Group
			  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
			  if(ipInterfaceGroup_Enabled) {
				  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
			  }else {
				  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
				  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
			  }
			  
			  compareText( "IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField, ipInterfaceGroupDefaultvalue);    //verify default values
			
			  
		//Signalling port
				signallingPort( gateway);	  
				
		}
	 
	 if(internetBasedCustomer.equalsIgnoreCase("Yes")) {
		  
		  ExtentTestManager.getTest().log(LogStatus.INFO, " 'Signalling Port' text field will not display, if 'Internet Based Custoer' is selected");
		  
		//Internet Based Customer
			addCheckbox_commonMethod( APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox, "Internet Based Customer", internetBasedCustomer);
			
	  String vlanDefaultvalue=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).getAttribute("value");
	  if(vlanDefaultvalue.isEmpty()) {
			  ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'VLAN tag' field by default, when 'Internet Based Customer' is selected");
	  }else {
		//VLAN tag  
		  boolean VlanEnability=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.vlanTag_textField).isEnabled();
		  if(VlanEnability) {
			  Report.LogInfo("INFO","VLAN Tag is enabled","FAIL");
			  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VLAN Tag' text field is enabled");
		  }else {
			  Report.LogInfo("INFO","VLAN Tag is disabled","PASS");
			  ExtentTestManager.getTest().log(LogStatus.PASS, " 'VLAN Tag' text field is disabled");
		  } 
	  }
	  
		//IP Interface
	  boolean ipInterface_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField).isEnabled();
	  if(ipInterface_Enabled) {
		  Report.LogInfo("INFO","'Ip Interface' text field is enabled","FAIL");
		  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Ip Interface' text field is enabled");
	  }else {
		  Report.LogInfo("INFO","'Ip Interface' text fieldis disabled","PASS");
		  ExtentTestManager.getTest().log(LogStatus.PASS, "'Ip Interface' text field is disabled");
	  }
	  
	  ipInterface = ipInterfaceDEfaultValue +vlanDefaultvalue;
	  compareText( "IP Interface", APT_VOIPAccess_Obj.VOIPAccess.ipInterface_textField,ipInterface );  
	  
//Address Context
	  boolean addressContext_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField).isEnabled();
	  if(addressContext_Enabled) {
		  Report.LogInfo("INFO","'Address Context' text field is enabled","FAIL");
		  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Address Context' text field is enabled");
	  }else {
		  Report.LogInfo("INFO","'Address Context' text fieldis disabled","PASS");
		  ExtentTestManager.getTest().log(LogStatus.PASS, "'Address Context' text field is disabled");
	  }
	  
	  addressContext=addressContextDefaultValue+vlanDefaultvalue;
	  compareText( "Address Context", APT_VOIPAccess_Obj.VOIPAccess.AddressContext_textField,addressContext );  //verify default values
	
	  
//IP Interface Group
	  boolean ipInterfaceGroup_Enabled=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField).isEnabled();
	  if(ipInterfaceGroup_Enabled) {
		  Report.LogInfo("INFO","'IP Inteface Group' text field is enabled","FAIL");
		  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'IP Inteface Group' text field is enabled");
	  }else {
		  Report.LogInfo("INFO","'IP Inteface Group' text field is disabled","PASS");
		  ExtentTestManager.getTest().log(LogStatus.PASS, "'IP Inteface Group' text field is disabled");
	  }
	  
	  ipInterfaceGroup=ipInterfaceGroupDefaultvalue+vlanDefaultvalue;
	  compareText( "IP Interface Group", APT_VOIPAccess_Obj.VOIPAccess.ipInterfaceGroup_textField,ipInterfaceGroup );    //verify default values
	 }
 }
}


								//STARTED SHOW ALL PANEL
		//Signalling Zone
if(internetBasedCustomer.equalsIgnoreCase("yes")) {
  
  compareText( "Signaling Zone", APT_VOIPAccess_Obj.VOIPAccess.signallingZone_textField, signallingZoneDefaultValue);
  edittextFields_commonMethod( "Signaling Zone", APT_VOIPAccess_Obj.VOIPAccess.signallingZone_textField, signallngZone);
}else {
  edittextFields_commonMethod( "Signaling Zone", APT_VOIPAccess_Obj.VOIPAccess.signallingZone_textField, signallngZone);
}



		//Signalling Transport Protocol
selectByVisibleText(APT_VOIPAccess_Obj.VOIPAccess.signallingTransportProtocol_Dropdown, signallingtransportProtocol,"Signalling Transport Protocol");

scrollDown(APT_VOIPAccess_Obj.VOIPAccess.internetBasedCustomer_checkbox);




if(signallingtransportProtocol.equalsIgnoreCase("sip-tls-tcp")) {
	
	//TLS Profile
	edittextFields_commonMethod( "TLS Profile", APT_VOIPAccess_Obj.VOIPAccess.TLS_textField, TLSfield);

	//SRTP
	addCheckbox_commonMethod(  APT_VOIPAccess_Obj.VOIPAccess.srtp_checkbox,"SRTP", srtp);
}


	// Customer Default Number	CustomerDefaultNumber_WarningMessage
edittextFields_commonMethod( "Customer Default Number",  APT_VOIPAccess_Obj.VOIPAccess.CustomerDefaultNumber_textfield, CustomerDefaultNumber);


		scrollDown( APT_VOIPAccess_Obj.VOIPAccess.AddTrunk_CENTREX_Header);
		

  
	// Reuse Network Selector Table
//	addCheckbox_commonMethod(  APT_VOIPAccess_Obj.VOIPAccess.ReuseNetworkSelectorTable_checkbox, "Reuse Network Selector Table", ReuseNetworkSelectorTable);
	
	//Reuse NIF Group
//	addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess. reuseNIFgroup_checkbox, "Reuse NIF Group", reuseNIFgroup);

			
	//Reuse Sig Zone/Part
//	addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess. reuseSigZonePart_checkbox, "Reuse Sig Zone/Part", reuseSigZonePart);




	// Colt Signalling IP
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.coltSignalingIP_textField, coltSignalingIP,"Colt Signalling IP");


	//Media IP
	//  ClearAndEnterTextValue( "Media IP", mediaIP_textField, mediaIP);
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.mediaIP_textField, mediaIP,"Media IP");




	// PBX Type	
	 // ClearAndEnterTextValue( "PBX Type", PBXType_textfield, PBXTYPE);
	sendKeys(APT_VOIPAccess_Obj.VOIPAccess.PBXType_textfield, PBXTYPE,"PBX Type");

	// PBX Profile
selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.PBXProfile_Dropdown, "PBX Profile", PBXProfile);



		// PSX Manual Configuration-Trunk Group	
//	addCheckbox_commonMethod( APT_VOIPAccess_Obj.VOIPAccess.PSXManualConfigurationTrunkGroup_checkbox, "PSX Manual Configuration-Trunk Group", PSXManualConfigurationTrunkGroup);
	
		// PSX Manual Configuration-DDI-Range
//	addCheckbox_commonMethod( APT_VOIPAccess_Obj.VOIPAccess.PSXManualConfigurationDDIRange_checkbox, "PSX Manual Configuration-DDI-Range", PSXManualConfigurationDDIRange);


		// Manual Configuration On GSX
//	addCheckbox_commonMethod( APT_VOIPAccess_Obj.VOIPAccess.ManualConfigurationonGSX_checkbox, "Manual Configuration On GSX", ManualConfigurationonGSX);
	
	
	
scrollDown(APT_VOIPAccess_Obj.VOIPAccess.NonServiceImpacting_Header);
	//Call Admission Control
//addCheckbox_commonMethod( APT_VOIPAccess_Obj.VOIPAccess.callAdmissionControl_checkbox, "Call Admission Control", callAdmissionControl);

if(callAdmissionControl.equalsIgnoreCase("yes")) {
	//Call Limit
	selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.callLimit_Dropdown, "Call Limit", CallLimitDropdwon);
	
	if(CallLimitDropdwon.equalsIgnoreCase("Defined")) {
		edittextFields_commonMethod( "Limit Number", APT_VOIPAccess_Obj.VOIPAccess.limitNumber_textField, limitNumber);
		
	}
}


	//Call Rate Limit
//addCheckbox_commonMethod(APT_VOIPAccess_Obj.VOIPAccess.callrateLimit_checkbox, "Call Rate Limit", CallRateLimitCheckboxSelection);
if(CallRateLimitCheckboxSelection.equalsIgnoreCase("yes")) {
	
	String callratelimitactualvalue=getAttributeFrom(APT_VOIPAccess_Obj.VOIPAccess.callRateLimitt_textField,"value");
	Report.LogInfo("INFO"," 'Call rate Limit' value is displaying as "+callratelimitactualvalue,"PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, " 'Call rate Limit' value is displaying as "+callratelimitactualvalue);

	if(!callrateLimiteValue.equalsIgnoreCase("null")) {
		int i=Integer.parseInt(callrateLimiteValue);
			
		if(i>100) {
			Report.LogInfo("INFO","The CallRateLimit should be less 100 for all Trunks","FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "The CallRateLimit should be less 100 for all Trunks");
		}
		else if(i<=100){
		//	edittextFields_commonMethod( "Call rate Limit", APT_VOIPAccess_Obj.VOIPAccess.callRateLimitt_textField, callrateLimiteValue);
		}
	}else {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Call rate Limit' value is not edited");
		Report.LogInfo("INFO","'Call rate Limit' value is not edited","PASS");
	}
}

scrollDown( APT_VOIPAccess_Obj.VOIPAccess.SIPSERVICES_Header);
	//Source Address Filtering
selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.sourceAddressFiltering_Dropdown, "Source Address Filtering", sourceAddressFiltering);

	//100rel Support
selectValueInsideDropdown( APT_VOIPAccess_Obj.VOIPAccess.relsupport_Dropdown, "100rel Support", relSupport);

	//SIP Session Keepalive Timer(Sec)
edittextFields_commonMethod( "SIP Session Keepalive Timer(Sec)", APT_VOIPAccess_Obj.VOIPAccess.sipSessionKeepAliverTimer_textField, sipSessionkeepAliveTimer);


//Text message under "SIP Session Keepalive timer"
	methodToFindMessagesUnderTextField( APT_VOIPAccess_Obj.VOIPAccess.defaultTextMessageUnderSIPsessionTimer, "SIP Session Keepalive Timer(Sec)", "Default SIP Session Keepalive Timer (sec): 1800");

	
	scrollDown(APT_VOIPAccess_Obj.VOIPAccess.PSXTRUNKGROUPFIELDS_Header);
	//Carrier
	edittextFields_commonMethod( "Carrier textfield", APT_VOIPAccess_Obj.VOIPAccess.carriers_TextField, Carrier);
	
	
	//IP Signalling Profile
	edittextFields_commonMethod( "IP Signaling Profile",APT_VOIPAccess_Obj.VOIPAccess.IPsignallingProfile_textField, IPSignalingProfile);
			
			
	//Egress IP Signaling Profile
	edittextFields_commonMethod( "Egress IP Signaling Profile",APT_VOIPAccess_Obj.VOIPAccess.EgressipSignal_TextField, EgressIPSignalingProfile);


	
	WebElement reusenifgroup=findWebElement(APT_VOIPAccess_Obj.VOIPAccess.reuseNIFgroup_checkbox);
	scrolltoview(reusenifgroup);
	
	

((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

verifyExists(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton,"OK");
click(APT_VOIPAccess_Obj.VOIPAccess.trunk_okButton,"OK");



}catch(NoSuchElementException e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, e+ " : Field is not displayed in Edit Trunk page");
	Report.LogInfo("INFO",  e+ " : Field is not displayed in Edit Trunk page","FAIL");
}catch(Exception e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL,  e+" : Field is not displayed in Edit Trunk page ");
	Report.LogInfo("INFO", e+" : Field is not displayed in Edit Trunk page","FAIL");
}

}else {
	ExtentTestManager.getTest().log(LogStatus.FAIL, "'Edit Trunk' Page is not navigated");
	Report.LogInfo("INFO","'Edit Trunk' Page is not navigated","FAIL");
}
}catch(NoSuchElementException e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, e+ " : Field is not displayed");
	Report.LogInfo("INFO",  e+ " : Field is not displayed","FAIL");
}catch(Exception e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL,  e+" : Field is not displayed");
	Report.LogInfo("INFO",  e+" : Field is not displayed","FAIL");
}


}

public void signallingPort( String gateway) throws InterruptedException {
	String signallingPort_expectedVaue=signalingport( gateway);
	compareText("Signaling Port", APT_VOIPAccess_Obj.VOIPAccess.signallingPort_textField, signallingPort_expectedVaue);
}

public String signalingport(String gateway) {
	
	String code=null;
	
	if(gateway.equals("DEVGSX1")) {
		code="542";
	}
	
	else if(gateway.equals("DEVSBC1")) {
		code="1029";
	}
	
	else if(gateway.equals("DEVSBC2")) {
		code="1014";		
	}
			
	else if(gateway.equals("OPSGSX1")) {
		code="508";
	}
			
	else if(gateway.equals("MILGSX1")) {
		code="696";
	}
			
	else if(gateway.equals("ZRHNBS1")) {
		code="1006";
	}
			
	else if(gateway.equals("PARNBS1")) {
		code="896";
	}
			
	else if(gateway.equals("LONNBS1")) {
		code="820";
	}
			
	else if(gateway.equals("FRANBS1")) {
		code="1137";
	}
			
	else if(gateway.equals("MADGSX1")) {
		code="741";
	}
			
	else if(gateway.equals("BHXNBS1")) {
		code="1013";
	}
			
	else if(gateway.equals("PARGSX2")) {
		code="923";
	}
			
	else if(gateway.equals("FRAGSX2")) {
		code="752";
	}
			
	else if(gateway.equals("PARGSX3")) {
		code="629";
	}
			
	else if(gateway.equals("FRASBC1")) {
		code="1026";
	}
			
	else if(gateway.equals("PARSBC1")) {
		code="1006";
	}
			
	else if(gateway.equals("ZRHGSX2")) {
		code="506";
	}
			
	else if(gateway.equals("FRASBC2")) {
		code="1024";
	}
			
	else if(gateway.equals("PARSBC2")) {
		code="1013";
	}
			
	else if(gateway.equals("FRASBC3")) {
		code="1009";
	}
			
	else if(gateway.equals("PARSBC3")) {
		code="1006";
	}
	
	return code;
}
public void GetTheValuesInsideDropdown(WebElement el, String labelname) throws IOException, InterruptedException
{
	
	List<String> ls = new ArrayList<String>();
	Select sel=new Select(el);
	 List<WebElement> we = sel.getOptions();
	   
	    for(WebElement a : we)
	    {
	        if(!a.getText().equals("select")){
	            ls.add(a.getText());
	            
	        }
	    }
	
	System.out.println("Value displaying under "+labelname+" is: "+ ls);
	ExtentTestManager.getTest().log(LogStatus.PASS, "Value displaying under "+labelname+" is: "+ ls);

}	

public String gateway_code(String gateway) {
	
	String code=null;
	
	if(gateway.equals("DEVGSX1")) {
		code="DEV";
	}
	
	else if(gateway.equals("DEVSBC1")) {
		code="DEA";
	}
	
	else if(gateway.equals("DEVSBC2")) {
		code="DEB";		
	}
			
	else if(gateway.equals("OPSGSX1")) {
		code="OPS";
	}
			
	else if(gateway.equals("MILGSX1")) {
		code="MIA";
	}
			
	else if(gateway.equals("ZRHNBS1")) {
		code="ZHA";
	}
			
	else if(gateway.equals("PARNBS1")) {
		code="PSA";
	}
			
	else if(gateway.equals("LONNBS1")) {
		code="LNA";
	}
			
	else if(gateway.equals("FRANBS1")) {
		code="FTA";
	}
			
	else if(gateway.equals("MADGSX1")) {
		code="MDA";
	}
			
	else if(gateway.equals("BHXNBS1")) {
		code="BHA";
	}
			
	else if(gateway.equals("PARGSX2")) {
		code="PSB";
	}
			
	else if(gateway.equals("FRAGSX2")) {
		code="FTB";
	}
			
	else if(gateway.equals("PARGSX3")) {
		code="PSC";
	}
			
	else if(gateway.equals("FRASBC1")) {
		code="FTC";
	}
			
	else if(gateway.equals("PARSBC1")) {
		code="PSD";
	}
			
	else if(gateway.equals("ZRHGSX2")) {
		code="ZRB";
	}
			
	else if(gateway.equals("FRASBC2")) {
		code="FTD";
	}
			
	else if(gateway.equals("PARSBC2")) {
		code="PSE";
	}
			
	else if(gateway.equals("FRASBC3")) {
		code="FTF";
	}
			
	else if(gateway.equals("PARSBC3")) {
		code="PSF";
	}
	
	return code;
}

public void methodToFindMessagesUnderTextField(String xpath, String labelname, String expectedmsg) {

	boolean defaultPortValueUnderSIPSignalling=false;
	try {	
		defaultPortValueUnderSIPSignalling=findWebElement(xpath).isDisplayed();
		if(defaultPortValueUnderSIPSignalling) {

			WebElement defaultValue=findWebElement(xpath);
			String defaultValueText= defaultValue.getText();
			if(defaultValueText.contains(expectedmsg)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Under '"+ labelname +"' text field', text message displays as "+ defaultValueText);
				Report.LogInfo("INFO","Under '"+ labelname +"' text field', text message displays as "+ defaultValueText,"PASS");
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Under '"+ labelname +"' text field', text message displays as "+ defaultValueText);
				Report.LogInfo("INFO","Under '"+ labelname +"' text field', text message displays as "+ defaultValueText,"FAIL");
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'");
			Report.LogInfo("INFO","No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'","FAIL");
		}
	}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'");
		Report.LogInfo("INFO","No text message displays under '"+ labelname +"' text field. It should display as '"+ expectedmsg +"'","FAIL");
	}
}

public void deleteDRPlan() throws InterruptedException, IOException, AWTException {

	
	//Alert alert = webDriver.switchTo().alert();		

	// Capturing alert message   
	//String alertMessage= webDriver.switchTo().alert().getText();
	
	
	
	//if(alertMessage.isEmpty()) {
		//ExtentTestManager.getTest().log(LogStatus.FAIL, "No message displays");
		//Report.LogInfo("INFO","No Message displays","FAIL"); 
	//}else {
		//ExtentTestManager.getTest().log(LogStatus.PASS, "Alert message displays as: "+alertMessage);
		//Report.LogInfo("INFO","Text message for alert displays as: "+alertMessage,"PASS");
	//}

	try {  
		Robot r = new Robot();
		Thread.sleep(1000);
		r.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
	
	}catch(Exception e) {
		e.printStackTrace();
	}
}
public void editedDRplan(String DRPlanHeader, String rangestart_cc, String rangestart_lac, String rangestart_num, String edit_rangestart_cc, String edit_rangestart_lac, String edit_rangestart_num) throws InterruptedException {

	String rangestart_cc_value;
	String rangestart_lac_value;
	String rangestart_num_value;

	if(edit_rangestart_cc.equalsIgnoreCase("null"))
	{
		rangestart_cc_value= rangestart_cc;
	}
	else
	{
		rangestart_cc_value= edit_rangestart_cc;
	}
	if(edit_rangestart_lac.equalsIgnoreCase("null"))
	{
		rangestart_lac_value= rangestart_lac;
	}
	else
	{
		rangestart_lac_value= edit_rangestart_lac;
	}
	if(edit_rangestart_num.equalsIgnoreCase("null"))
	{
		rangestart_num_value= rangestart_num;
	}
	else
	{
		rangestart_num_value= edit_rangestart_num;
	}
	String RangeStartValue= rangestart_cc_value+"-"+rangestart_lac_value+"-"+rangestart_num_value;
	//String AddedDRplan= findWebElement(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_tablelist).replace("value", DRPlanHeader).getAttribute("style");
	String AddedDRplan= findWebElement(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_tablelist1+DRPlanHeader+APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_tablelist2).getAttribute("style");
	if(!AddedDRplan.contains("height: 1px"))
	{
		//List<WebElement> results = findWebElements(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_rangestart).replace("value", DRPlanHeader);
		List<WebElement> results = findWebElements(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_rangestart1+DRPlanHeader+APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_rangestart2);
		int numofrows = results.size();
		//System.out.println("no of results: " + numofrows);

		if ((numofrows == 0)) {

			ExtentTestManager.getTest().log(LogStatus.PASS, DRPlanHeader+" table is empty");
			Report.LogInfo("INFO", DRPlanHeader+" table is empty","PASS");
		}
		else {
			for (int i = 0; i < numofrows; i++) {
				try {

					String AddedDRplandata = results.get(i).getText();
					//System.out.println(AddedDRplandata);
					if (AddedDRplandata.equalsIgnoreCase(RangeStartValue)) {

						String DRPlanRowID= webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@col-id='rangeStart'][text()='"+RangeStartValue+"']/parent::div")).getAttribute("row-id");
						//System.out.println("DR Plan row id: "+DRPlanRowID);
						if(webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).isDisplayed())
						{
							//WebElement AddedDRPlan_Checkbox= findWebElement("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]");
							//Clickon(AddedDRPlan_Checkbox);
							
							webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).click();
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is checked");
							Report.LogInfo("INFO", "Added DR Plan checkbox is checked","PASS");
						}
						else
						{
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is already checked");
							Report.LogInfo("INFO","Added DR Plan checkbox is already checked","PASS");
						}

					}

				} catch (StaleElementReferenceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
			waitforPagetobeenable();
		}
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No DR Plan added in table");
		Report.LogInfo("INFO","No DR Plan added in table","FAIL");
	}

}

public void AddDRPlan(String rangestart_cc, String rangestart_lac, String rangestart_num, String rangefinish_cc, String rangefinish_lac, String rangefinish_num, String destinationnumber_cc, String destinationnumber_lac, String destinationnumber_num, String activate_deactivateDRplan_dropdownvalue) throws InterruptedException, IOException {

	compareText( "Add DR Plan Header", APT_VOIPAccess_Obj.VOIPAccess.addDRplan_header1, "Disaster Recovery Plans");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.okbutton,"OK");
	click( APT_VOIPAccess_Obj.VOIPAccess.okbutton,"OK");
	
	waitforPagetobeenable();
	//verify warning messages
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.rangestart_cc_warngmsg, "Range Start CC");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.rangestart_lac_warngmsg, "Range Start LAC");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.rangestart_num_warngmsg, "Range Start Num");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.rangefinish_cc_warngmsg, "Range Finish CC");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.rangefinish_lac_warngmsg, "Range Finish LAC");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.rangefinish_num_warngmsg, "Range Finish Num");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_cc_warngmsg, "Destination Number CC");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_lac_warngmsg, "Destination Number LAC");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_num_warngmsg, "Destination Number Num");

	//verify Add DR plan
	compareText( "Range Start", APT_VOIPAccess_Obj.VOIPAccess.rangestart_label, "Range Start");
	edittextFields_commonMethod( "Range Start CC", APT_VOIPAccess_Obj.VOIPAccess.rangestart_cc, rangestart_cc);
	edittextFields_commonMethod( "Range Start LAC", APT_VOIPAccess_Obj.VOIPAccess.rangestart_lac, rangestart_lac);
	edittextFields_commonMethod( "Range Start Num", APT_VOIPAccess_Obj.VOIPAccess.rangestart_num, rangestart_num);
	compareText( "Range Finish", APT_VOIPAccess_Obj.VOIPAccess.rangefinish_label, "Range Finish");
	edittextFields_commonMethod( "Range Finish CC", APT_VOIPAccess_Obj.VOIPAccess.rangefinish_cc, rangefinish_cc);
	edittextFields_commonMethod( "Range Finish LAC", APT_VOIPAccess_Obj.VOIPAccess.rangefinish_lac, rangefinish_lac);
	edittextFields_commonMethod( "Range Finish Num",APT_VOIPAccess_Obj.VOIPAccess.rangefinish_num, rangefinish_num);
	compareText( "Destination Number(DN)", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_label,"Destination Number(DN)");
	edittextFields_commonMethod( "Destination Number CC", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_cc, destinationnumber_cc);
	edittextFields_commonMethod( "Destination Number LAC", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_lac, destinationnumber_lac);
	edittextFields_commonMethod( "Destination Number Num", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_num, destinationnumber_num);
	compareText( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.activate_deactivate_label, "Activate/Deactivate DR Plan");
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.okbutton,"OK");
	click( APT_VOIPAccess_Obj.VOIPAccess.okbutton,"OK");
	waitforPagetobeenable();
}

public void addedDRplan(String DRPlanHeader, String rangestart_cc, String rangestart_lac, String rangestart_num) throws InterruptedException {

	String RangeStartValue= rangestart_cc+"-"+rangestart_lac+"-"+rangestart_num;
	//String AddedDRplan= findWebElement(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_tablelist).replace("value", DRPlanHeader).getAttribute("style");
	String AddedDRplan= findWebElement(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_tablelist1+DRPlanHeader+APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_tablelist2).getAttribute("style");
	
	if(!AddedDRplan.contains("height: 1px"))
	{
		//List<WebElement> results = findWebElements(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_rangestart).replace("value", DRPlanHeader);
		List<WebElement> results = findWebElements(APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_rangestart1+DRPlanHeader+APT_VOIPAccess_Obj.VOIPAccess.addedDRplan_rangestart2);

		int numofrows = results.size();
		//System.out.println("no of results: " + numofrows);

		if ((numofrows == 1)) {

			ExtentTestManager.getTest().log(LogStatus.PASS, DRPlanHeader+" table is empty");
		}
		else {
			for (int i = 1; i < numofrows; i++) {
				try {

					String AddedDRplandata = results.get(i).getText();
					//System.out.println(AddedDRplandata);
					if (AddedDRplandata.equalsIgnoreCase(RangeStartValue)) {

						String DRPlanRowID= webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@col-id='rangeStart'][text()='"+RangeStartValue+"']/parent::div")).getAttribute("row-id");
						//System.out.println("DR Plan row id: "+DRPlanRowID);
						if(webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).isDisplayed())
						{
							//WebElement AddedDRPlan_Checkbox= findWebElement("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]");
							//Clickon(AddedDRPlan_Checkbox);
							webDriver.findElement(By.xpath("(//div[text()='"+DRPlanHeader+"']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@row-id='"+DRPlanRowID+"']/div//span[contains(@class,'unchecked')]")).click();
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is checked");
							Report.LogInfo("INFO","Added DR Plan checkbox is checked","PASS");
						}
						else
						{
							ExtentTestManager.getTest().log(LogStatus.PASS, "Added DR Plan checkbox is already checked");
							Report.LogInfo("INFO","Added DR Plan checkbox is already checked","PASS");
						}

					}

				} catch (StaleElementReferenceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
			waitforPagetobeenable();
		}
	}
	else
	{
		Report.LogInfo("INFO","No DR Plan added in table","FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No DR Plan added in table");
	}

}

public void EditDRPlan(String edit_rangestart_cc, String edit_rangestart_lac, String edit_rangestart_num, String edit_rangefinish_cc, String edit_rangefinish_lac, String edit_rangefinish_num, String edit_destinationnumber_cc, String edit_destinationnumber_lac, String edit_destinationnumber_num, String edit_activate_deactivateDRplan_dropdownvalue) throws InterruptedException, IOException {

	compareText( "Add DR Plan Header", APT_VOIPAccess_Obj.VOIPAccess.addDRplan_header1, "Disaster Recovery Plans");
	compareText( "Range Start", APT_VOIPAccess_Obj.VOIPAccess.rangestart_label, "Range Start");
	edittextFields_commonMethod( "Range Start CC", APT_VOIPAccess_Obj.VOIPAccess.rangestart_cc, edit_rangestart_cc);
	edittextFields_commonMethod( "Range Start LAC", APT_VOIPAccess_Obj.VOIPAccess.rangestart_lac, edit_rangestart_lac);
	edittextFields_commonMethod( "Range Start Num", APT_VOIPAccess_Obj.VOIPAccess.rangestart_num, edit_rangestart_num);
	compareText( "Range Finish", APT_VOIPAccess_Obj.VOIPAccess.rangefinish_label, "Range Finish");
	edittextFields_commonMethod( "Range Finish CC", APT_VOIPAccess_Obj.VOIPAccess.rangefinish_cc, edit_rangefinish_cc);
	edittextFields_commonMethod( "Range Finish LAC", APT_VOIPAccess_Obj.VOIPAccess.rangefinish_lac, edit_rangefinish_lac);
	edittextFields_commonMethod( "Range Finish Num", APT_VOIPAccess_Obj.VOIPAccess.rangefinish_num, edit_rangefinish_num);
	compareText( "Destination Number(DN)", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_label, "Destination Number(DN)");
	edittextFields_commonMethod( "Destination Number CC", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_cc, edit_destinationnumber_cc);
	edittextFields_commonMethod( "Destination Number LAC", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_lac, edit_destinationnumber_lac);
	edittextFields_commonMethod( "Destination Number Num", APT_VOIPAccess_Obj.VOIPAccess.destinationnumber_num, edit_destinationnumber_num);
	compareText( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.activate_deactivate_label, "Activate/Deactivate DR Plan");
	//activate_deactivateDRPlan_dropdown( "Activate/Deactivate DR Plan", APT_VOIPAccess_Obj.VOIPAccess.activate_deactivate_dropdownvalue, edit_activate_deactivateDRplan_dropdownvalue);
	verifyExists( APT_VOIPAccess_Obj.VOIPAccess.okbutton,"OK");
	click( APT_VOIPAccess_Obj.VOIPAccess.okbutton,"OK");
	waitforPagetobeenable();
}









	
   
    
}
