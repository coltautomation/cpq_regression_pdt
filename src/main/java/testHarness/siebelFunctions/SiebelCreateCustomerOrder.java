package testHarness.siebelFunctions;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CLoginObj;
import pageObjects.c4cObjects.C4COpportunityObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.siebelObjects.SiebelLoginObj;
import pageObjects.siebelObjects.SiebelQuoteCreationObj;
import testHarness.commonFunctions.ReusableFunctions;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;

public class SiebelCreateCustomerOrder extends SeleniumUtils
{
	GlobalVariables g = new GlobalVariables();
	
	ReusableFunctions Reusable = new ReusableFunctions();

	ReadingAndWritingTextFile readText=new ReadingAndWritingTextFile();
	
	static ReadExcelFile read = new ReadExcelFile();
	String dataFileName = "Siebel_testdata.xls";
	File path = new File("./src/test/resources/"+dataFileName); 
	String testDataFile = path.toString();
//	String sheetName = "NewOrderOffnet";

	public void createCustomerOrder(String testDataFile, String tsSheetName,String scriptNo,String dataSetNo) throws Exception {
		
		String OpportunityNumber = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Opportunity#");
		String Delivery_Channel = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Delivery_Channel");
		String Ordering_Party = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Ordering_Party");
		String Ordering_Party_Address = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Ordering_Party_Address");
		String Ordering_Party_Contract = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Ordering_Party_Contract");
		String Sales_Channel = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Sales_Channel");
														
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.RequestReceivedDate,"Request Received Date textbox");
		sendKeys(SiebelQuoteCreationObj.CustomerOrderPage.RequestReceivedDate, Reusable.CurrentDate(),"Enter Request Received Date");
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.OpportunityNo,"Opportunity ID textbox");
		System.out.println("Opportunity Value:"+OpportunityNumber);
		sendKeys(SiebelQuoteCreationObj.CustomerOrderPage.OpportunityNo,OpportunityNumber,"Enter Opportunity No");
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.DeliveryChannel,"Delivery Channel textbox");
		sendKeys(SiebelQuoteCreationObj.CustomerOrderPage.DeliveryChannel, Delivery_Channel,"Enter Delivery Channel");
	
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.OrderingPartySearch,"Ordering Party Search textbox");
		click(SiebelQuoteCreationObj.CustomerOrderPage.OrderingPartySearch,"Click on Ordering Party Search");
				
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.dropdown,"Popup Custom Dropdown");
		click(SiebelQuoteCreationObj.CustomerOrderPage.dropdown,"Click on Party Contact Popup Dropdown");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.newpick,"Ordering Party Search textbox");
		click(SiebelQuoteCreationObj.CustomerOrderPage.newpick,"Click on Party Name");
		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.InputPartyname,"Ordering Party Search textbox");
		sendKeys(SiebelQuoteCreationObj.CustomerOrderPage.InputPartyname,Ordering_Party,"Enter Ordering Party Name");
		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.PickAccountOk,"Pick Account Ok Button");
		click(SiebelQuoteCreationObj.CustomerOrderPage.PickAccountOk,"Click on Pick Account Ok");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.OrderingPartyAddrSearch,"Ordering Party Address Search");
		click(SiebelQuoteCreationObj.CustomerOrderPage.OrderingPartyAddrSearch,"Click on Odering Party Address Search");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.PopupCustomDropdown,"Popup Custom Dropdown");
		click(SiebelQuoteCreationObj.CustomerOrderPage.PopupCustomDropdown,"Click on Party Contact Popup Dropdown");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.newparty,"Party Address");
		click(SiebelQuoteCreationObj.CustomerOrderPage.newparty,"Click on Ordering Party Address");
		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.InputPartyAddr,"Input Party Address");
		sendKeys(SiebelQuoteCreationObj.CustomerOrderPage.InputPartyAddr,Ordering_Party_Address,"Enter Input Party Address");
	
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.PickAddrSearch,"Address Search");
		click(SiebelQuoteCreationObj.CustomerOrderPage.PickAddrSearch,"Click on Pick Address Submit");
		Reusable.waitForSiebelLoader();
		
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.PickAddrSubmit1,"Pick Address Submit");
		javaScriptclick(SiebelQuoteCreationObj.CustomerOrderPage.PickAddrSubmit1,"Click on Pick Address Submit");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.OrderingPartyContactSearch,"Ordering Party Contact Search");
		click(SiebelQuoteCreationObj.CustomerOrderPage.OrderingPartyContactSearch,"Click on Ordering Party Contact Search");
			
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.PopupCustomDropdown,"Popup Custom Dropdown");
		click(SiebelQuoteCreationObj.CustomerOrderPage.PopupCustomDropdown,"Click on Party Contact Popup Dropdown");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.newname,"Last Name");
		click(SiebelQuoteCreationObj.CustomerOrderPage.newname,"Click on Last Name");
		sendKeys(SiebelQuoteCreationObj.CustomerOrderPage.InputFirstname,Ordering_Party_Contract, "Enter Input Last Name");
		
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.PickContactSearch,"Pick Contact Search");
		click(SiebelQuoteCreationObj.CustomerOrderPage.PickContactSearch,"Click on Pick Contact Search");
		Reusable.waitForSiebelLoader();
				
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.FirstnameSubmit,"First name Submit");
		click(SiebelQuoteCreationObj.CustomerOrderPage.FirstnameSubmit,"Click on First Name Submit");
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.SalesChannel,"Sales Channel");
		sendKeys(SiebelQuoteCreationObj.CustomerOrderPage.SalesChannel,Sales_Channel, "Enter Sales Channel");
		
		verifyExists(SiebelQuoteCreationObj.CustomerOrderPage.NewServiceOrder,"New Service Order");
		click(SiebelQuoteCreationObj.CustomerOrderPage.NewServiceOrder, "Click on New Service Order");
		Reusable.waitForSiebelLoader();
	
		
	
	}
		
	
	/*//Quote Creation for the IP Access Product
	public void AddProductToQuote(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{		
		
		//***************************************************************************
				
		if(g.getBrowser().equalsIgnoreCase("chrome"))
		{
			waitForAjax();
			if(verifyExists(CPQQuoteCreationObj.TransactionPage.quoteIDElem))
			{
				Report.LogInfo("AddQuote","Clicking Add Quote successfully redirected to CPQ automatically.", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicking Add Quote successfully redirected to CPQ automatically.");
			}else
			{
				Report.LogInfo("AddQuote", "Clicking Add Quote failed to redirect to CPQ automatically.", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Clicking Add Quote failed to redirect to CPQ automatically.");
			
				((JavascriptExecutor) webDriver).executeScript("window.open()");
			
				ArrayList<String> tabs = new ArrayList<String> (webDriver.getWindowHandles());
				webDriver.switchTo().window(tabs.get(1)); //switches to new tab
				webDriver.get(Configuration.CPQ_URL);
		    
			//	Login.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
		    
				webDriver.switchTo().window(tabs.get(0)); // switch back to main screen        
		    			
				waitForElementToAppear(C4COpportunityObj.Opportunity.overviewLnk, 15);
				click(C4COpportunityObj.Opportunity.overviewLnk,"Overview Link");
		    
				waitForElementToAppear(C4COpportunityObj.Opportunity.quotesLnk, 15);
				click(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");
				
				webDriver.switchTo().window(tabs.get(1)); //switches to new tab
				webDriver.close();
				
				webDriver.switchTo().window(tabs.get(0));		
			    
			    Reusable.WaitforCPQloader();
				waitForAjax();
				Reusable.Waittilljquesryupdated();
			}
		}
		
		Reusable.WaitforC4Cloader();
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.TransactionPage.QuoteId, 15);
		verifyExists(CPQQuoteCreationObj.TransactionPage.QuoteId, "Quote Id");
		String quoteId = getAttributeFrom(CPQQuoteCreationObj.TransactionPage.QuoteId, "value");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Quote_ID", quoteId);	
		
		verifyExists(CPQQuoteCreationObj.TransactionPage.Products_SectionHeader,"Products Section");
		verifyExists(CPQQuoteCreationObj.TransactionPage.Products_SectionContent,"Products Contents");
		
		//***************************************************************************
	
		waitForElementToAppear(CPQQuoteCreationObj.TransactionPage.AddProductsBtn, 15);
		verifyExists(CPQQuoteCreationObj.TransactionPage.AddProductsBtn,"Add Products button");
		click(CPQQuoteCreationObj.TransactionPage.AddProductsBtn,"Add Products button");
		
		waitForAjax();
		verifyExists(CPQQuoteCreationObj.TransactionPage.IPAccess_DropDown,"'IP Access' drop down");
		click(CPQQuoteCreationObj.TransactionPage.IPAccess_DropDown,"'IP Access' drop down");
		
		//Select Colt IP Access option from drop down
		verifyExists(CPQQuoteCreationObj.TransactionPage.ColtIPAccess_Option,"'Colt IP Access' option");
		click(CPQQuoteCreationObj.TransactionPage.ColtIPAccess_Option,"'Colt IP Access' option");
		
		//Landing on 'Business Internet Services > IP Access' page.
		waitForAjax();
		verifyExists(CPQQuoteCreationObj.TransactionPage.StartBtn,"Start Button on 'IP Access' landing page");
		click(CPQQuoteCreationObj.TransactionPage.StartBtn,"Start button on 'IP Access' landing page");
	}
	
	public void AddProductDetailsToQuote() throws Exception
	{
		//******************Primary Connection > PRIMARY ADDRESS DETAILS**********************************
		waitForAjax();
		String SiteA_Address = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_Address");
		
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryAddressHdr, "'PRIMARY ADDRESS' section header");
		
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAddLbl, "'Site Address' field label");
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAddTxFld, "'Site Address' text field");
		sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteAddTxFld, SiteA_Address,"Site Address");
		
		//Click on the 'Search for an Address' icon
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
		click(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
		
		Reusable.WaitforC4Cloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 20);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		
		//**********************Primary Connection > Primary Connection******************************
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryConnectionPage.ServiceBandwidthLbl, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.ServiceBandwidthLbl, "Service Bandwidth field label");		
		verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.ServiceBandwidthDrpDwn, "Service Bandwidth Drop Down");
		click(CPQQuoteCreationObj.PrimaryConnectionPage.ServiceBandwidthDrpDwn, "Service Bandwidth Drop Down");
		
		//Select the 10 GBPS value
		String BandwithValue = read.getExcelColumnValue(testDataFile, sheetName, "Service_Bandwidth");
		String serviceBandwidth = CPQQuoteCreationObj.PrimaryConnectionPage.ServiceBandwidthVal1 + BandwithValue + CPQQuoteCreationObj.PrimaryConnectionPage.ServiceBandwidthVal2;
		waitForElementToAppear(serviceBandwidth, 15);
		verifyExists(serviceBandwidth, "10 GBPS Service Bandwidth");
		click(serviceBandwidth, "10 GBPS Service Bandwidth");
		
		
		//Click on 'Next' button
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 10);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		
		//**********************Primary Connection > IP Features******************************
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.RouterTypeLbl, 15);
		verifyExists(CPQQuoteCreationObj.IPFeaturesPage.RouterTypeLbl, "Router Type field label");		
		verifyExists(CPQQuoteCreationObj.IPFeaturesPage.RouterTypeDrpDwn, "Router Type Drop Down");
		click(CPQQuoteCreationObj.IPFeaturesPage.RouterTypeDrpDwn, "Router Type Drop Down");
		
		String RouterTypeVal = read.getExcelColumnValue(testDataFile, sheetName, "Router_Type");
		String routerType = CPQQuoteCreationObj.IPFeaturesPage.RouterTypeVal1 + RouterTypeVal + CPQQuoteCreationObj.IPFeaturesPage.RouterTypeVal2;
		verifyExists(routerType, "Customer Provided Router Value");
		click(routerType, "Customer Provided Router Value");
		
		//Click on 'Next' button
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		
		//Click on 'Next' button
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		
		//**********************L3 RESILIENCE*******************************
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//Click on 'Next' button
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//Click on 'Next' button
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//Click on 'Next' button
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//Click on 'Next' button
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//**********************Additional Product Data > Primary*******************************
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.PrimaryGrpHdr, 15);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.PrimaryGrpHdr, "Primary Site' section");
		
		//Select Site Access Technology
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.SiteAccessTechnologylbl, "Site Access Technology Label");
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.SiteAccessTechnologyDrpDwn, "Site Access Technology Drop Down");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.SiteAccessTechnologyDrpDwn, "Site Access Technology Drop Down");
		
		String AccessTechVal = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_Access_Technology");
		String AccessTech = CPQQuoteCreationObj.AdditionalProductDataPage.AccessTechVal1 + AccessTechVal + CPQQuoteCreationObj.AdditionalProductDataPage.AccessTechVal2;
		verifyExists(AccessTech, "Access Technology Drop Down Value");
		click(AccessTech, "Access Technology Drop Down Value");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//Send Cabinet ID
		String CabinetId = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_Cabinet_ID");
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.SiteCabinetlbl, 15);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.SiteCabinetlbl, "Site Cabinet test field Label");
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.SiteCabinettxtfld, "Site Cabinet Text Field");
		sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.SiteCabinettxtfld, CabinetId,"Site Cabinet ID");
		
		//Click on 'Next' button
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//**********************Additional Product Data > Additional Service Info*******************************
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.AdditionalServiceInfoGrpHdr, 10);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.AdditionalServiceInfoGrpHdr, "Additional Service Info Header");
		
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.routerTechDropdown, 10);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.routerTechDropdown, "Router Technology Dropdown");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.routerTechDropdown, "Managed Virtual Router");
		
		String RouterTechValue = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_Router_Technology");
		String RouterTech = CPQQuoteCreationObj.AdditionalProductDataPage.virtualRouterValue1 + RouterTechValue + CPQQuoteCreationObj.AdditionalProductDataPage.virtualRouterValue2;
		verifyExists(RouterTech, "Managed Virtual Router Value");
		click(RouterTech, "Managed Virtual Router Value");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.capLeadTimeDropdown, 10);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.capLeadTimeDropdown, "Capacity Lead Time Dropdown");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.capLeadTimeDropdown, "Capacity Lead Time Dropdown");
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.NoValueInCapLead, "No Value");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.NoValueInCapLead, "No Value");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//Send First Name
		String FirstName = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_IP_First_Name");
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.FirstNameTxtFld, 10);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.FirstNameTxtFld, "First Name Text Field");
		sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.FirstNameTxtFld, FirstName,"First Name");
		
		//Send Last Name
		String LastName = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_IP_Last_Name");
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.LastNameTxtFld, "Last Name Text Field");
		sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.LastNameTxtFld, LastName,"Last Name");
		
		//Send Contact Number
		String ContactNo = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_IP_Contact_Number");
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.ContactNumberTxtFld, "Contact Number Text Field");
		sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.ContactNumberTxtFld, ContactNo,"Contact Number");
		
		//Send Mobile Number
		String MobileNo = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_IP_Mobile_Number");
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.MobileNumberTxtFld, "Mobile Number Text Field");
		sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.MobileNumberTxtFld, MobileNo,"Mobile Number");
		
		String EmailId = read.getExcelColumnValue(testDataFile, sheetName, "SiteA_IP_Email");
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.emailIdTxtFld, "Email Id Text Field");
		sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.emailIdTxtFld, EmailId,"Email Id Text Field");
		
		//Click on 'Next' button
		verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		javaScriptclick1(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		//**********************Pricing And Promotions*******************************
		waitForElementToAppear(CPQQuoteCreationObj.PricingAndPromotionsPage.PromotionsItm, 10);
		verifyExists(CPQQuoteCreationObj.PricingAndPromotionsPage.PromotionsItm, "Promotions Item Image");
		click(CPQQuoteCreationObj.PricingAndPromotionsPage.PromotionsItm, "Promotions Item Image");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		waitForElementToAppear(CPQQuoteCreationObj.PricingAndPromotionsPage.SaveToQuoteBtn, 10);
		verifyExists(CPQQuoteCreationObj.PricingAndPromotionsPage.SaveToQuoteBtn, "Save to Quote Button");
		click(CPQQuoteCreationObj.PricingAndPromotionsPage.SaveToQuoteBtn, "Save to Quote Button");
	}
	
	public void EnterLegalNTechContactDetails() throws Exception
	{
		//**********************Enter Legal & Technical Contact Details*******************************
		Reusable.WaitforC4Cloader();
		Reusable.WaitforCPQloader();
		waitForAjax();
				
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.LegalContactlink, 20);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.LegalContactlink, "Get Legal Contact Details link ");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.LegalContactlink, "Get Legal Contact Details link ");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetLegalcontactbtn, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetLegalcontactbtn, "Get Legal Contact Details ");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetLegalcontactbtn, "Get Legal Contact Details ");
		
		String currentHandle =webDriver.getWindowHandle();
		switchWindow(Configuration.ContactDetails_URL,"Manage contacts");
		
		String Legal_Contact = read.getExcelColumnValue(testDataFile, sheetName, "Legal_Contact");
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, "Search Text Box");
		sendKeys(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, Legal_Contact, "Search Text Box");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectLegalcontact, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectLegalcontact, "Select Legal Contact Details ");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectLegalcontact, "Select Legal Contact Details ");
		
		//waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.OCHLegalContact, 10);
		//verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.OCHLegalContact, "OCH Legal Contact");
		//click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.OCHLegalContact, "OCH Legal Contact");
		Reusable.WaitforC4Cloader();
		Reusable.WaitforCPQloader();
		waitForAjax();
		webDriver.switchTo().window(currentHandle);
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.TechnicalContactslink, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.TechnicalContactslink, "Get Technical Contact Details link");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.TechnicalContactslink, "Get Technical Contact Details link");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetTechnicalContactsbtn, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetTechnicalContactsbtn, "Get Technical Contact Details");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetTechnicalContactsbtn, "Get Technical Contact Details");
		
		switchWindow(Configuration.ContactDetails_URL,"Manage contacts");
		
		String Technical_Contact = read.getExcelColumnValue(testDataFile, sheetName, "Technical_Contact");
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, "Search Text Box");
		sendKeys(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, Technical_Contact, "Search Text Box");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectTechnicalcontact, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectTechnicalcontact, "Select Technical Contact Details");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectTechnicalcontact, "Select Technical Contact Details");
		
		//waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.OCHTechnicalContact, 10);
		//verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.OCHTechnicalContact, "OCH Technical Contact");
		//click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.OCHTechnicalContact, "OCH Technical Contact");
		Reusable.WaitforC4Cloader();
		Reusable.WaitforCPQloader();
		waitForAjax();
		webDriver.switchTo().window(currentHandle);
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
	}
	
	public void Submit4CommercialApproval() throws Exception
	{
		//**********************Submission for Commercial Approval*******************************
		scrollUp();
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.CommercialApprovalLink, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.CommercialApprovalLink, "Commercial Approval Link");
		click(CPQQuoteCreationObj.SubmissionforCommercialApproval.CommercialApprovalLink, "Commercial Approval Link");
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn, "Submit To Approval Button");
		click(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn, "Submit To Approval Button");
		
	}
	
	public void Submit4TechnicalApproval() throws Exception
	{
		//**********************Submission for Technical Approval*******************************
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		
		//verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.ReadyforTechAppl, "Ready For Technical Approval Button");
		//click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.ReadyforTechAppl, "Ready For Technical Approval Button");
		
		Reusable.WaitforCPQloader();
		scrollIntoView(webDriver.findElement(By.xpath(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprCbxElem)));
		Reusable.WaitforCPQloader();
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprovalCbx, "Ready For Technical Approval Checkbox");
		Reusable.WaitforCPQloader();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn, 20);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn,"Log out button");
		click(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn,"Log out button");
		waitForAjax();		
	}*/
	
}