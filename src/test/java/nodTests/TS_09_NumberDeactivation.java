package nodTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

import testHarness.commonFunctions.ReusableFunctions;
import testHarness.nodFunctions.NODLoginPage;
import testHarness.nodFunctions.NODLogoutPage;
import testHarness.nodFunctions.NODMyOrderPage;
import testHarness.nodFunctions.NODNumberDeactivation;
import testHarness.nodFunctions.NODTelephoneNumberSearchPage;


public class TS_09_NumberDeactivation extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	NODLoginPage Login = new NODLoginPage();
	GlobalVariables g = new GlobalVariables();
	NODMyOrderPage MyOrder = new NODMyOrderPage();
	NODLogoutPage Logout = new NODLogoutPage();
	NODTelephoneNumberSearchPage TelPhNumberSearch = new NODTelephoneNumberSearchPage();
	NODNumberDeactivation  Deactivation = new NODNumberDeactivation();
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test(groups={"NumberDeactivationFromActivatedNumberOnTelPage"})
	public void NumberDeactivationFromActivatedNumberOnTelPage(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	

		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

			// load a properties file
			prop.load(input);               

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();

		try
		{
			openurl(Configuration.NOD_URL);
			Login.NODLogin();
			MyOrder.NavigationtoHomePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			Deactivation.NumberDeactivate(testDataFile, dataSheet, scriptNo, dataSetNo);
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
		//		Terminating the Execution once it gets ended
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}	
	
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test(groups={"NumberDeactivationFromActivatedNumberOnTXPage"})
	public void NumberDeactivationFromActivatedNumberOnTXPage(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	

		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

			// load a properties file
			prop.load(input);               

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();

		try
		{
			openurl(Configuration.NOD_URL);
			Login.NODLogin();
			MyOrder.NavigationtoHomePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			Deactivation.NumberDeactivateOnTXpage(testDataFile, dataSheet, scriptNo, dataSetNo);
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
		//		Terminating the Execution once it gets ended
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}	
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test(groups={"NumberDeactivationFromPortInNumberOnTXPage"})
	public void NumberDeactivationFromPortInNumberOnTXPage(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	

		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

			// load a properties file
			prop.load(input);               

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();

		try
		{
			openurl(Configuration.NOD_URL);
			Login.NODLogin();
			MyOrder.NavigationtoHomePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonTXPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			Deactivation.NumberDeactivateOnTXpage(testDataFile, dataSheet, scriptNo, dataSetNo);
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
		//		Terminating the Execution once it gets ended
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();

	}	
	
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test(groups={"NumberDeactivationFromPortInNumberOnTelPage"})
	public void NumberDeactivationFromPortInNumberOnTelPage(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	

		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

			// load a properties file
			prop.load(input);               

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();

		try
		{
			openurl(Configuration.NOD_URL);
			Login.NODLogin();
			MyOrder.NavigationtoHomePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.NavigateonMyTelephoneNumbersPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			TelPhNumberSearch.SearchOrderByNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			Deactivation.NumberDeactivate(testDataFile, dataSheet, scriptNo, dataSetNo);
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();
	}	
	

}
