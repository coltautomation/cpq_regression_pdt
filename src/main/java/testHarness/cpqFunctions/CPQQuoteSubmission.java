package testHarness.cpqFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import baseClasses.ExtentTestManager;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.GridHelper;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CLoginObj;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import testHarness.commonFunctions.ReusableFunctions;
import pageObjects.cpqObjects.CPQLoginObj;

public class CPQQuoteSubmission extends SeleniumUtils {
	
	GlobalVariables g = new GlobalVariables();
	CPQLoginPage Login = new CPQLoginPage();
	ReusableFunctions Reusable = new ReusableFunctions();
	GridHelper Grid = new GridHelper();
	Properties prop = new Properties();
	
	
public void addBillingInformation(String ProductName, String BCN_ID, String ColName) throws IOException, InterruptedException {
        
        try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

 

            // load a properties file
            prop.load(input);               

 

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String Environment = prop.getProperty("Environment");
		String Dataset_Region = "Europe";
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		if(ProductName.equalsIgnoreCase("Colt Ip Domain")) {ProductName="IP Domain";}
		if(ProductName.equalsIgnoreCase("Colt Ip Guardian")) {ProductName="IP Guardian";}
		if(ProductName.equalsIgnoreCase("Colt Managed Virtual Firewall")) {ProductName="IP Managed Virtual Firewall";}
		if(ProductName.equalsIgnoreCase("Colt Managed Dedicated Firewall")) {ProductName="IP Managed Dedicated Firewall";}
		else { ProductName = ProductName.replaceAll("(?!^)([A-Z])", " $1"); }
		
//		Clicking on the rows from the table
		if(ProductName.equalsIgnoreCase("EthernetLine")||ProductName.equalsIgnoreCase("EthernetHub")) {
			Reusable.WebTableCellAction("Product", "Container Model", null,"Click", null);
		}else {
			Reusable.WebTableCellAction("Product", ProductName, null,"Click", null);
		}
		
//		Initializing the driver
		WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
        
//      Clicking the Billing Information Button
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInformationBtn);
		clickByJS(CPQQuoteSubmissionObj.QuoteSubmit.billingInformationBtn);   
		
        String WindowHandle = driver.getWindowHandle();
        String sContact = null;
        
//      Capturing the new window handle
        for (String WindowHandleAfter : driver.getWindowHandles()) {
              driver.switchTo().window(WindowHandleAfter);
        }
		
//	      Selecting the values from the contact
        if (waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.bcnSearchTxb,75)) {
        	Reusable.waitForpageloadmask();
//        	WebInteractUtil.click(CPQ_Objects.bcnSearchTxb);
        	sendKeys(CPQQuoteSubmissionObj.QuoteSubmit.bcnSearchTxb, BCN_ID);
        	Reusable.waitForpageloadmask();
            waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.pickDefaultBCNCbx, 120);
            click(CPQQuoteSubmissionObj.QuoteSubmit.pickDefaultBCNCbx, "BCN Checkbox");
            Reusable.waitForpageloadmask();
            waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn, 120);
            ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn);
            click(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn, "Select All Button");
            Reusable.waitForpageloadmask();
            click(CPQQuoteSubmissionObj.QuoteSubmit.bcnUpdatedCloseBtn, "Update and Close Button");
            
//	        waiting till the driver gets closed
            /*try {
            	FluentWait<WebDriver> fluentWait = new FluentWait<>(driver) 
					.withTimeout(120, TimeUnit.SECONDS)
    		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
    		        .ignoring(NoSuchElementException.class);
    				fluentWait.until(ExpectedConditions.invisibilityOf(webDriver.findElement(By.xpath(CPQQuoteSubmissionObj.QuoteSubmit.bcnUpdatedCloseBtnString))));
            } catch(Exception e) {
            	if (e.toString().contains("no such window")) {
            		System.out.println("Bcn window closed");
            	} else {
            		ExtentTestManager.getTest().log(LogStatus.FAIL, "Error in Selecting BCN lookup");
        			System.out.println("Error in Selecting BCN Contact lookup");
        			driver.close();
        			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
        			
            	}                
            }*/
            ExtentTestManager.getTest().log(LogStatus.PASS, "BCN Has Selected "+BCN_ID);
			System.out.println("BCN Has Selected "+BCN_ID);
        } else {
        	ExtentTestManager.getTest().log(LogStatus.FAIL, "BCN lookup is not Visible, Please Verify");
			System.out.println("BCN lookup is not Visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
        }
       
        // Switch back to first browser window
        driver.switchTo().window(WindowHandle);
        Reusable.WaitforCPQloader();			
        
//      clicking the Billing Information Checkbox
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
		isEnable(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
		click(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx, "Billing Info Checkbox");
		for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
		
		//String BCN = Reusable.getTextFromGridFromDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable, ProductName, ColName).trim();
		/*String BCN = Reusable.MultiLineWebTableCellAction("BCN MRC", "Colt IP Access", "BCN MRC","GetValue", null,1);
		if (BCN.equals(BCN_ID)) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "mrcBCN "+BCN+" Successfully Selected for the product "+ProductName);
			System.out.println("mrcBCN "+BCN+" Successfully Selected for the product "+ProductName);
		} else {
		    ExtentTestManager.getTest().log(LogStatus.FAIL, "mrcBCN "+BCN+" not Selected for the product "+ProductName);
			System.out.println("mrcBCN "+BCN+" not Selected for the product "+ProductName);
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}*/
		        
//      Unchecking the Billing Information Checkbox
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
		click(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx, "Billing Info Checkbox");    
		Reusable.waitForpageloadmask();
		Reusable.WaitforCPQloader();		
	}
	
public String addMultiLineBillingInformation(String ProductName, String BCN_ID, int RowNumber) throws IOException, InterruptedException {

    
//  Initializing the Variable
  String sResult;
  
  try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {



      // load a properties file
      prop.load(input);               



  } catch (IOException ex) {
      ex.printStackTrace();
  }
  
  String Environment = prop.getProperty("Environment");
  
  if(ProductName.equalsIgnoreCase("Colt Ip Domain")) {ProductName="IP Domain";}
  else if(ProductName.equalsIgnoreCase("Colt Ip Guardian")) {ProductName="IP Guardian";}
  else if(ProductName.equalsIgnoreCase("Colt Managed Virtual Firewall")) {ProductName="IP Managed Virtual Firewall";}
  else if(ProductName.equalsIgnoreCase("Colt Managed Dedicated Firewall")) {ProductName="IP Managed Virtual Firewall";}
  
//  Clicking on the rows from the table
  sResult = Reusable.MultiLineWebTableCellAction("Product", ProductName, null,"Click", null, RowNumber);
  if (sResult.equalsIgnoreCase("False")){ return "False"; }
  
  WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
  
//Clicking the Billing Information Button
  ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInformationBtn);
  clickByJS(CPQQuoteSubmissionObj.QuoteSubmit.billingInformationBtn);   
  
  String WindowHandle = driver.getWindowHandle();
  String sContact = null;
  
//Capturing the new window handle
  for (String WindowHandleAfter : driver.getWindowHandles()) {
        driver.switchTo().window(WindowHandleAfter);
  }
  
//    Selecting the values from the contact
  if (waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.bcnSearchTxb,75)) {
      Reusable.waitForpageloadmask();
//      WebInteractUtil.click(CPQ_Objects.bcnSearchTxb);
      sendKeys(CPQQuoteSubmissionObj.QuoteSubmit.bcnSearchTxb, BCN_ID);
      Reusable.waitForpageloadmask();
      waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.pickDefaultBCNCbx, 120);
      click(CPQQuoteSubmissionObj.QuoteSubmit.pickDefaultBCNCbx, "BCN Checkbox");
      Reusable.waitForpageloadmask();
      waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn, 120);
      ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn);
      click(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn, "Select All Button");
      Reusable.waitForpageloadmask();
      click(CPQQuoteSubmissionObj.QuoteSubmit.bcnUpdatedCloseBtn, "Update and Close Button");
      
      
      ExtentTestManager.getTest().log(LogStatus.PASS, "BCN Has Selected "+BCN_ID);
      System.out.println("BCN Has Selected "+BCN_ID);
  }
  else 
  {
  ExtentTestManager.getTest().log(LogStatus.FAIL, "BCN lookup is not Visible, Please Verify");
  System.out.println("BCN lookup is not Visible, Please Verify");
  ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
  
  }

// Switch back to first browser window
driver.switchTo().window(WindowHandle);
Reusable.WaitforCPQloader();
          
//clicking the Billing Information Checkbox
  ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
  isEnable(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
  click(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
  for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }



  
//  Verifying the nrcBCN selected successfully or not
  String refColName = null;
  if (Environment.equalsIgnoreCase("PRD")||ProductName.equalsIgnoreCase("Container Model")) { refColName = "BCN"; } else { refColName = "BCN NRC"; }
  String nrcBCN = Reusable.MultiLineWebTableCellAction("Product", ProductName, refColName,"Store", null, RowNumber).trim();
  if (nrcBCN.equals(BCN_ID)) {
      ExtentTestManager.getTest().log(LogStatus.PASS, "nrcBCN "+BCN_ID+" Successfully Selected for the product "+ProductName);
      System.out.println("nrcBCN "+BCN_ID+" Successfully Selected for the product "+ProductName);
  } else {
      ExtentTestManager.getTest().log(LogStatus.FAIL, "nrcBCN "+BCN_ID+" not Selected for the product "+ProductName);
      System.out.println("nrcBCN "+BCN_ID+" not Selected for the product "+ProductName);
      ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
  }

//Unchecking the Billing Information Checkbox
  ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
  click(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);    
  Reusable.waitForpageloadmask();
  Reusable.WaitforCPQloader();
  return "True";



}
	
	
	public void SendProposalToCustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		waitForAjax();
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature, 10);
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature);
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature,"Customer Signature on Quote section");		
		click(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature,"Customer Signature");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.ProposalNoteTxBox,"Proposal Note Text Box in Customer Signature section");
		sendKeys(CPQQuoteSubmissionObj.QuoteSubmit.ProposalNoteTxBox,"Automation Test");
		
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.GenerateProposalBtn,"Generate Proposal Button in Customer Signature section");
		click(CPQQuoteSubmissionObj.QuoteSubmit.GenerateProposalBtn,"Generate Proposal Button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.ProposalGeneratedMsg, 10);
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.ProposalGeneratedMsg,"Proposal Generated Message");
		
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.SendProposalSection,"Send Proposal Section");
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.SelectWorkflowDropdown,"Select Workflow Dropdown");
		click(CPQQuoteSubmissionObj.QuoteSubmit.SelectWorkflowDropdown,"Select Workflow Dropdown");
		click(CPQQuoteSubmissionObj.QuoteSubmit.EmailOption,"Email Option From Workflow Dropdown");
						
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		String To_Reciepient = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"To_Reciepient");
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.ToRecipientTextbox,"To Recipient Textbox");
		sendKeys(CPQQuoteSubmissionObj.QuoteSubmit.ToRecipientTextbox, To_Reciepient);
		
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.SendProposalBtn,"Send Proposal Button");
		click(CPQQuoteSubmissionObj.QuoteSubmit.SendProposalBtn,"Send Proposal Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
	}
	
	public void AcceptOnBehalfOfCustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{		
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.OrderBtn, 10);
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.OrderBtn);
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.OrderBtn,"Order button");
		click(CPQQuoteSubmissionObj.QuoteSubmit.OrderBtn,"Order button");
		
		String Quote_Action = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quote_Action");
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.QuoteActionField,"Quote Action");
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.QuoteActionDropDown,"Quote Action Drop Down");
		click(CPQQuoteSubmissionObj.QuoteSubmit.QuoteActionDropDown,"Quote Action drop down");
		ClickonElementByString("//li[normalize-space(.)='"+Quote_Action+"']", 30);
		
		String Status_Reason = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Status_Reason");
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.StatusDropDownlable,"Reason For Status drop down Label");
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.SelectStatusDropDown,"Reason For Status drop down");
		click(CPQQuoteSubmissionObj.QuoteSubmit.SelectStatusDropDown,"Reason For Status drop down");
		ClickonElementByString("//li[normalize-space(.)='"+Status_Reason+"']", 30);
		
		String uploadFile = g.getRelativePath()+"\\Resources\\uploadfile.txt";		
		Reusable.UploadFile(CPQQuoteSubmissionObj.QuoteSubmit.uploadFileBtn, uploadFile);
				
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.RadioBtn,"Correct signed order radio button");
		click(CPQQuoteSubmissionObj.QuoteSubmit.RadioBtn,"Correct signed order radio button");
		
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.CustSignedDateLable,"Customer Signed Date Lable");
		click(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignedDateField,"Customer Signed Date Field");
		click(CPQQuoteSubmissionObj.QuoteSubmit.CurrentDate,"Current Date");
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
	
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.ConfirmQuoteBtn, 10);
		scrollIntoView(webDriver.findElement(By.xpath(CPQQuoteSubmissionObj.QuoteSubmit.ConfirmQuoteBtnElem)));
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.ConfirmQuoteBtn,"Confirm Quote Button");
		click(CPQQuoteSubmissionObj.QuoteSubmit.ConfirmQuoteBtn,"Confirm Quote Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		pause(30000);
		Reusable.waitForpageloadmask();
	}
	
	public void AddContactInformation(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow);
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow, 20);
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
		click(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
		for (int i = 1; i < 5; i++) {Reusable.waitForpageloadmask(); }
		
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.ContactInformation, 20);
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.ContactInformation,"Contact Information Section");
		click(CPQQuoteSubmissionObj.QuoteSubmit.ContactInformation,"Contact Information");
		for (int i = 1; i < 5; i++) {Reusable.waitForpageloadmask(); }
		
		String ProductName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Product_Name");
		String Company_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Company_Name");
		String Quote_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quote_ID");
		
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTable,"Additional Quote Info Table");
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTable);
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTable, 25);
		
		if(ProductName.contentEquals("CPESolutionsSite"))
		{
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, Quote_ID+"-2", "Company Name A End", Company_Name);
			//Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, Quote_ID+"-2", "Company Name B End", Company_Name);
			
			//Reusable.MultiLineWebTableCellAction("Product", "Colt IP Access", "Company Name A End","Store", null, Integer.parseInt(Rows[0]));
		}else
		{
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, Quote_ID+"-1", "Company Name A End", Company_Name);
			if(!Product_Name.equalsIgnoreCase("ColtIpAccess")&&!Product_Name.equalsIgnoreCase("EthernetHub")&&!Product_Name.equalsIgnoreCase("EthernetSpoke")) {
			Reusable.setTextToGridInDesiredRowNColumn(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTableElm, Quote_ID+"-1", "Company Name B End", Company_Name);
			}
		}
		
		Reusable.waitForpageloadmask();
	}
	
	public void SubmitOrder() throws InterruptedException, AWTException, IOException 
	{
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.CreateOrderBtn, 10);
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.CreateOrderBtn,"Create Order Button");
		click(CPQQuoteSubmissionObj.QuoteSubmit.CreateOrderBtn,"Create Order Button");
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.OrderCreatedMsg,"Your Order request is on-going");
		Reusable.WaitforCPQloader();
		waitForAjax();
		for (int i = 0; i < 3; i++) { Reusable.waitForpageloadmask(); }
		waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.backwardArrow, 10);
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.backwardArrow);
		verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.backwardArrow, "Backward Arrow");
		click(CPQQuoteSubmissionObj.QuoteSubmit.backwardArrow, "Backward Arrow");
		Reusable.waitForpageloadmask();
				
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.waitForpageloadmask();
	}
	
	public void captureServiceOrder(String file_name, String Sheet_Name, String iScript,String iSubScript) throws IOException, InterruptedException {
	
//		Reading the runtime data values
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
		if (waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.orderDetailsCbx, 75)) {
			ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.orderDetailsCbx);
			Reusable.waitForpageloadmask();
			clickByAction(CPQQuoteSubmissionObj.QuoteSubmit.orderDetailsCbx);
			for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "orderDetailsCbx in CPQ is not visible, Please verify");
			System.out.println("orderDetailsCbx in CPQ is not visible, Please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
//      capturing the service order for the respective product
		if (Product_Name.equalsIgnoreCase("CPESolutionsSite")) {			
			String sProduct = "CPE Solutions Service|CPE Solutions Site";
			String sField = "Service_Order|Site_Order";
		    String[] Product_Split = sProduct.split("\\|");
		    String[] Field_Name = sField.split("\\|");
		    for (int i=0; i < Product_Split.length; i++)
		    {
		    	//String Service_Order = Reusable.getTextFromGridFromDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, Product_Split[i], "Service Order No").trim();
		    	String Service_Order = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Service Order No", "GetValue", null, 1);
				//Printing the order reference
				System.out.println("Service Order for the product "+Product_Split[i]+" is "+Service_Order);
				Report.LogInfo("captureServiceOrder","<b><i>"+Product_Split[i]+": "+Service_Order +"<i></b> is generated for the product.", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service Order for the product "+Product_Split[i]+" is "+Service_Order);
//				Exporting Service Order values to the testdata sheet
				DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, Field_Name[i], Service_Order);
		    }
			
		} else {
//			String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
			String sProduct = null;
			if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) {sProduct = Product_Name;}
			else if(Product_Name.equalsIgnoreCase("ColtIpDomain")) {sProduct="IP Domain";}
			else if(Product_Name.equalsIgnoreCase("ColtIpGuardian")) {sProduct="IP Guardian";}
			else if(Product_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {sProduct="IP Managed Virtual Firewall";}
			else if(Product_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {sProduct="IP Managed Virtual Firewall";}
			else {sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");}
			
			//String Service_Order = Reusable.getTextFromGridFromDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTableElm, sProduct, "Service Order No").trim();
			String Service_Order = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Service Order No", "GetValue", null, 1);
			System.out.println("Service Order for the product "+sProduct+" is "+Service_Order);
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Order for the product "+sProduct+" is "+Service_Order);
			Report.LogInfo("captureServiceOrder","<b><i>"+sProduct+": "+Service_Order +"<i></b> is generated for the product.", "PASS");
			//			Exporting Service Order values to the testdata sheet
			DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Service_Order", Service_Order);
		}
				
//      Unchecking the Order Details Checkbox
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.orderDetailsCbx);
		clickByAction(CPQQuoteSubmissionObj.QuoteSubmit.orderDetailsCbx);    
		Reusable.waitForpageloadmask();		
		
	}
	
public void addBillingInformationCPE(String sProduct_Name, String BCN_ID, String ColName) throws IOException, InterruptedException {
        
        try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

 

            // load a properties file
            prop.load(input);               

 

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String Environment = prop.getProperty("Environment");
		String Dataset_Region = "Europe";
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		String sResult, Product_Name;
		String sProduct = null;
		if (sProduct_Name.contains("Data -")||sProduct_Name.contains("Voice -")){ Product_Name = sProduct_Name; }
		else if(sProduct_Name.equalsIgnoreCase("VPNNetwork")) {Product_Name="VPN Network";}
		else if(sProduct_Name.equalsIgnoreCase("ColtIpDomain")) {Product_Name="IP Domain";}
		else if(sProduct_Name.equalsIgnoreCase("ColtIpGuardian")) {Product_Name="IP Guardian";}
		else if(sProduct_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {Product_Name="IP Managed Virtual Firewall";}
		else if(sProduct_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {Product_Name="IP Managed Dedicated Firewall";}
		else if(sProduct_Name.equalsIgnoreCase("CPESolutionsSite")) {Product_Name="CPE Solutions Site";}
		else { Product_Name = sProduct_Name.replaceAll("(?!^)([A-Z])", " $1"); }
		
		sResult = Reusable.WebTableCellAction("Product", Product_Name, null,"Click", null);
		
//		Initializing the driver
		WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
        
//      Clicking the Billing Information Button
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInformationBtn);
		clickByJS(CPQQuoteSubmissionObj.QuoteSubmit.billingInformationBtn);   
		
        String WindowHandle = driver.getWindowHandle();
        String sContact = null;
        
//      Capturing the new window handle
        for (String WindowHandleAfter : driver.getWindowHandles()) {
              driver.switchTo().window(WindowHandleAfter);
        }
		
//	      Selecting the values from the contact
        if (waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.bcnSearchTxb,75)) {
        	Reusable.waitForpageloadmask();
//        	WebInteractUtil.click(CPQ_Objects.bcnSearchTxb);
        	sendKeys(CPQQuoteSubmissionObj.QuoteSubmit.bcnSearchTxb, BCN_ID);
        	Reusable.waitForpageloadmask();
            waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.pickDefaultBCNCbx, 120);
            click(CPQQuoteSubmissionObj.QuoteSubmit.pickDefaultBCNCbx, "BCN Checkbox");
            Reusable.waitForpageloadmask();
            waitForElementToBeVisible(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn, 120);
            ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn);
            click(CPQQuoteSubmissionObj.QuoteSubmit.bcnSelectAllBtn, "Select All Button");
            Reusable.waitForpageloadmask();
            click(CPQQuoteSubmissionObj.QuoteSubmit.bcnUpdatedCloseBtn, "Update and Close Button");
            
//	        waiting till the driver gets closed
            /*try {
            	FluentWait<WebDriver> fluentWait = new FluentWait<>(driver) 
					.withTimeout(120, TimeUnit.SECONDS)
    		        .pollingEvery(1000, TimeUnit.MILLISECONDS)
    		        .ignoring(NoSuchElementException.class);
    				fluentWait.until(ExpectedConditions.invisibilityOf(webDriver.findElement(By.xpath(CPQQuoteSubmissionObj.QuoteSubmit.bcnUpdatedCloseBtnString))));
            } catch(Exception e) {
            	if (e.toString().contains("no such window")) {
            		System.out.println("Bcn window closed");
            	} else {
            		ExtentTestManager.getTest().log(LogStatus.FAIL, "Error in Selecting BCN lookup");
        			System.out.println("Error in Selecting BCN Contact lookup");
        			driver.close();
        			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
        			
            	}                
            }*/
            ExtentTestManager.getTest().log(LogStatus.PASS, "BCN Has Selected "+BCN_ID);
			System.out.println("BCN Has Selected "+BCN_ID);
        } else {
        	ExtentTestManager.getTest().log(LogStatus.FAIL, "BCN lookup is not Visible, Please Verify");
			System.out.println("BCN lookup is not Visible, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
        }
       
        // Switch back to first browser window
        driver.switchTo().window(WindowHandle);
        Reusable.WaitforCPQloader();			
        
//      clicking the Billing Information Checkbox
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
		isEnable(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
		click(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx, "Billing Info Checkbox");
		for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
		
		//String BCN = Reusable.getTextFromGridFromDesiredRowNColumn(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable, ProductName, ColName).trim();
		/*String BCN = Reusable.MultiLineWebTableCellAction("BCN MRC", "Colt IP Access", "BCN MRC","GetValue", null,1);
		if (BCN.equals(BCN_ID)) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "mrcBCN "+BCN+" Successfully Selected for the product "+ProductName);
			System.out.println("mrcBCN "+BCN+" Successfully Selected for the product "+ProductName);
		} else {
		    ExtentTestManager.getTest().log(LogStatus.FAIL, "mrcBCN "+BCN+" not Selected for the product "+ProductName);
			System.out.println("mrcBCN "+BCN+" not Selected for the product "+ProductName);
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}*/
		        
//      Unchecking the Billing Information Checkbox
		ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx);
		click(CPQQuoteSubmissionObj.QuoteSubmit.billingInfoCbx, "Billing Info Checkbox");    
		Reusable.waitForpageloadmask();
		Reusable.WaitforCPQloader();		
	}

}
