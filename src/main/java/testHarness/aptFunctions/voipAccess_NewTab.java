package testHarness.aptFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.testng.Reporter;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_HSS_Obj;
import pageObjects.aptObjects.Lanlink_NewTab_Obj;
import pageObjects.aptObjects.voipAccess_NewTab_Obj;

public class voipAccess_NewTab extends SeleniumUtils {
	Lanlink_NewTab NewTab = new Lanlink_NewTab();

	public void searchOrderORservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String existingService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceName");

		mouseMoveOn(Lanlink_NewTab_Obj.Lanlink_NewTab.ManageCustomerServiceLink);
		waitForAjax();

		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchorderORservicelink, "SearchorderORservice");

		sendKeys(Lanlink_NewTab_Obj.Lanlink_NewTab.searchService_serviceTextField, existingService, "Service");
		scrollDown(Lanlink_NewTab_Obj.Lanlink_NewTab.searchButton);

		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchButton, "Search");
		waitToPageLoad();

		// String selectTheSearchedService =
		// getTextFrom(Lanlink_NewTab_Obj.Lanlink_NewTab.selectService).replace("value",
		// existingService);
		// click(selectTheSearchedService);
		click(Lanlink_NewTab_Obj.Lanlink_NewTab.selectService, "Select service checkbox");

		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchSerice_ActionDropdown, "Action");
		click(Lanlink_NewTab_Obj.Lanlink_NewTab.searchService_viewLink, "View");
		waitForAjax();
		waitToPageLoad();
	}

	public void closeChildtab() throws InterruptedException {

		CloseProposalwindow();
		CloseProposalwindow();
	}

	public void clickOnBreadCrump(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String breadCrumpLink = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceName");

		scrollIntoTop();
		waitForAjax();
		String breadcrumb = null;

		try {
			breadcrumb = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumb).replace("value", breadCrumpLink);
			if (isElementPresent(breadcrumb)) {
				click(breadcrumb, "Breadcrump");
			} else {
				Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
		}
	}

	public void MASswitch__DeleteFromServiceFunctionality(String existingdevicename)
			throws InterruptedException, IOException {

		Reporter.log("Verifying 'Delete MAS Switch' Functionality");

		scrollDown(voipAccess_NewTab_Obj.voipAccess_NewTab.MASSwitch);

		if (isElementPresent(voipAccess_NewTab_Obj.voipAccess_NewTab.existingdevicegrid)) {
			scrollDown(voipAccess_NewTab_Obj.voipAccess_NewTab.existingdevicegrid);
			webDriver.findElement(By.xpath("//tr//*[contains(.,'" + existingdevicename
					+ "')]//following-sibling::span//a[contains(.,'Delete from Service')]")).click();
			// click(voipAccess_NewTab_Obj.voipAccess_NewTab.deleteFromService1+existingdevicename+voipAccess_NewTab_Obj.voipAccess_NewTab.deleteFromService2,"Delete
			// from service");
			/*
			 * Robot r = new Robot(); r.keyPress(KeyEvent. VK_ENTER);
			 * r.keyRelease(KeyEvent. VK_ENTER);
			 */

			verifyExists(voipAccess_NewTab_Obj.voipAccess_NewTab.deleteBtnAlert, "Delete alert button");
			click(voipAccess_NewTab_Obj.voipAccess_NewTab.deleteBtnAlert, "Deete alert button");
			NewTab.verifysuccessmessage("MAS switch deleted successfully");
		} else {
			Reporter.log("No Device added in grid");
		}

	}

	/*
	 * public void addDropdownValues_ForSpanTag(String labelname, String xpath,
	 * String expectedValueToAdd) throws InterruptedException, IOException {
	 * boolean availability=false; List<String> ls = new ArrayList<String>();
	 * 
	 * try { availability=isElementPresent(xpath); if(availability) {
	 * Reporter.log(labelname + " dropdown is displaying");
	 * 
	 * if(expectedValueToAdd.equalsIgnoreCase("null")) {
	 * 
	 * Reporter.log(" No values selected under "+ labelname + " dropdown");
	 * }else {
	 * 
	 * click("//div[label[text()='"+ labelname +"']]//div[text()='�']");
	 * waitForAjax();
	 * 
	 * //verify list of values inside dropdown List<String> listofvalues = new
	 * ArrayList<>(Arrays.asList(getTextFrom(voipAccess_NewTab_Obj.
	 * voipAccess_NewTab.listofvalues)));
	 * 
	 * Reporter.log( " List of values inside "+ labelname + "dropdown is:  ");
	 * 
	 * for (String valuetypes : listofvalues) {
	 * Reporter.log("service sub types : " + valuetypes);
	 * 
	 * ls.add(valuetypes); }
	 * 
	 * 
	 * Reporter.log("list of values inside "+labelname+" dropdown is: "+ls);
	 * 
	 * waitForAjax(); sendKeys(("//div[label[text()='"+ labelname
	 * +"']]//input"), expectedValueToAdd); waitForAjax();
	 * 
	 * click(("(//span[contains(text(),'"+ expectedValueToAdd +"')])[1]"));
	 * waitForAjax();
	 * 
	 * String actualValue=getTextFrom("//label[text()='"+ labelname
	 * +"']/following-sibling::div//span"); Reporter.log( labelname +
	 * " dropdown value selected as: "+ actualValue);
	 * 
	 * } }else { Reporter.log(labelname + " is not displaying"); }
	 * }catch(Exception ee) { ee.printStackTrace();
	 * Reporter.log(" NO value selected under "+ labelname + " dropdown"); }
	 * 
	 * }
	 */

	public void verifyAddMASswitch(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws Exception {

		String MAS_IMSPOPLocation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"IMSpopLocation");
		String customerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "customerName");
		String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceName");
		String expectedPageTitle = "Add MAS Switch";

		scrollDown(voipAccess_NewTab_Obj.voipAccess_NewTab.MASSwitch);

		waitForAjax();

		openLinkInNewTab(voipAccess_NewTab_Obj.voipAccess_NewTab.addMASswitch_link, "Add MAS switch"); // opens
																										// MAS
																										// SWitch
																										// link
																										// in
																										// new
																										// tab
		Switchtotab();

		String actualPageTitle = webDriver.getTitle();
		if (expectedPageTitle.equals(actualPageTitle)) {

			Reporter.log("Page Title is displaying as " + actualPageTitle + " as expected");

			String el1 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumb).replace("value", "Home");
			verifyExists(el1, "Home");

			String el2 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumb).replace("value", customerName);
			verifyExists(el2, "Customer Name");

			String el3 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumb).replace("value", serviceName);
			verifyExists(el3, "Service Name");

			String el4 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumbForcurrentPage).replace("value",
					"Add MAS Switch");
			verifyExists(el4, "Add MAS Switch");

			compareText("Add MAS Switch header", voipAccess_NewTab_Obj.voipAccess_NewTab.MAS_AddMASSwitch_header,
					"Add MAS Switch"); // compare MAS switch Header

			click(voipAccess_NewTab_Obj.voipAccess_NewTab.okButton, "OK");
			verifyExists(voipAccess_NewTab_Obj.voipAccess_NewTab.MASswitch_warningmessage, "IMS POP Location");

			addDropdownValues_commonMethod("IMS POP Location",
					voipAccess_NewTab_Obj.voipAccess_NewTab.AddmasSWitch_IMSpopswitch_dropdown, MAS_IMSPOPLocation);

			click(voipAccess_NewTab_Obj.voipAccess_NewTab.okButton, "OK");
			waitForAjax();

			NewTab.verifysuccessmessage("MAS switch added successfully.");

		} else {
			Reporter.log("Page Title mismatches. It is displaying as " + actualPageTitle);
		}

		Reporter.log("------ MAS Switch added successfully ------");
	}

	public void editMASdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws Exception {

		String customerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "customerName");
		// String editDeviceName = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "existingdevicename");
		String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceName");
		String editVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"MAS_editVendor");
		String editmanageAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"MAS_manageAddress");
		String editSnmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAS_Snmpro");
		String deviceName1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAS_deviceName");
		String editLink = voipAccess_NewTab_Obj.voipAccess_NewTab.editMASSwitch1 + deviceName1
				+ voipAccess_NewTab_Obj.voipAccess_NewTab.editMASSwitch2;
		scrollDown(voipAccess_NewTab_Obj.voipAccess_NewTab.MASSwitch);

		waitForAjax();

		String expectedPageTitle = "Edit MAS Switch";
		openLinkInNewTab(voipAccess_NewTab_Obj.voipAccess_NewTab.editSwitchLink, "Edit"); // opens
																							// Edit
																							// MAS
																							// SWitch
																							// link
																							// in
																							// new
																							// tab
		Switchtotab();

		waitToPageLoad();
		String actualPageTitle = webDriver.getTitle();
		if (expectedPageTitle.equals(actualPageTitle)) {

			Reporter.log("Page Title is displaying as " + actualPageTitle + " as expected");

			String el1 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumb).replace("value", "Home");
			verifyExists(el1, "Home");

			String el2 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumb).replace("value", customerName);
			verifyExists(el2, "Customer Name");

			String el3 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumb).replace("value", serviceName);
			verifyExists(el3, "Service Name");

			String el4 = (voipAccess_NewTab_Obj.voipAccess_NewTab.breadCrumbForcurrentPage).replace("value",
					"Edit MAS Switch");
			verifyExists(el4, "Edit MAS Switch");

			// Device name
			// sendKeys("Device name",
			// voipAccess_NewTab_Obj.voipAccess_NewTab.MAS_deviceName",
			// editDeviceName);

			// vendor/model
			verifyExists(voipAccess_NewTab_Obj.voipAccess_NewTab.MAS_vendorModel, "Vendor/Model");
			// addDropdownValues_commonMethod("Vendor/Model",
			// voipAccess_NewTab_Obj.voipAccess_NewTab.vendermodel,
			// editVendorModel);

			// Management Address
			verifyExists(voipAccess_NewTab_Obj.voipAccess_NewTab.MAS_managementAddress, "Management Address");
			sendKeys(voipAccess_NewTab_Obj.voipAccess_NewTab.MAS_managementAddress, editmanageAddress,
					"Management Address");

			// Snmrpo
			verifyExists(voipAccess_NewTab_Obj.voipAccess_NewTab.MAS_snmpro, "Snmpro");
			sendKeys(voipAccess_NewTab_Obj.voipAccess_NewTab.MAS_snmpro, editSnmpro, "Snmpro");

			scrollDown(voipAccess_NewTab_Obj.voipAccess_NewTab.okButton);
			click(voipAccess_NewTab_Obj.voipAccess_NewTab.okButton, "OK");

			NewTab.verifysuccessmessage("MAS switch updated successfully");

		} else {

			Reporter.log("Page Title is mismatches. It is displaying as " + actualPageTitle);
		}
	}

}
