package testHarness.aptFunctions;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.searchForDeviceObj;
import testHarness.commonFunctions.ReusableFunctions;


public class searchForDevice extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void clickOnSearchForDevice() throws InterruptedException, IOException
	{
		verifyExists(searchForDeviceObj.searchDevice.mcnlink, "Manage Colt Network Link");
		mouseOver(searchForDeviceObj.searchDevice.mcnlink);
		click(searchForDeviceObj.searchDevice.mcnlink, "Manage Colt Network Link");
		
		verifyExists(searchForDeviceObj.searchDevice.searchForDeviceLink, "search For Device Link");
		click(searchForDeviceObj.searchDevice.searchForDeviceLink, "search For Device Link");
	}
	
	public void search_Device(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String deviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"deviceName");
		String MgmtAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManagementAddress");
		String vendorModel= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vendorModel");
		String[] vendorModelDropdown = vendorModel.split(",");
		String deviceType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"deviceTypes");
		String searchCity= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"searchCity");
		String searchCO= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"searchCO");
		String cityValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cityvaluestoBeSelected");
		String coValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"COvaluesToBeSelected");
		String RouterID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"routerId");
		
		verifyExists(searchForDeviceObj.searchDevice.deviceName_textField, "Device Name textField");
		sendKeys(searchForDeviceObj.searchDevice.deviceName_textField,deviceName, "Device Name textField");
		
		//verifyExists(searchForDeviceObj.searchDevice.managementAddress, "Management Address");
		//sendKeys(searchForDeviceObj.searchDevice.managementAddress, MgmtAddress, "Management Address");
		
		verifyExists(searchForDeviceObj.searchDevice.vendorModelDropdown1 +vendorModelDropdown[1] +searchForDeviceObj.searchDevice.vendorModelDropdown2, "Vendor Model Dropdown");
		click(searchForDeviceObj.searchDevice.vendorModelDropdown1 +vendorModelDropdown[1] +searchForDeviceObj.searchDevice.vendorModelDropdown2, "Vendor Model Dropdown");
		
		//verifyExists(searchForDeviceObj.searchDevice.vendorModelDropdown, "Vendor Model Dropdown");
		//ClickonElementByString("//select[@id='availableVendorModel']//option[@label='"+ vendorModelDropdown[1] +"']", 30);
		
		verifyExists(searchForDeviceObj.searchDevice.addButton_vendorModel, "Add Button Vendor Model");
		click(searchForDeviceObj.searchDevice.addButton_vendorModel, "Add Button Vendor Model");
		
		verifyExists(searchForDeviceObj.searchDevice.deviceTypeDropdown1 +deviceType+ searchForDeviceObj.searchDevice.deviceTypeDropdown2, "Device Type Dropdown");
		click(searchForDeviceObj.searchDevice.deviceTypeDropdown1 +deviceType+ searchForDeviceObj.searchDevice.deviceTypeDropdown2, "Device Type Dropdown");
		
		verifyExists(searchForDeviceObj.searchDevice.addButton_deviceType, "Add Button Vendor Model");
		click(searchForDeviceObj.searchDevice.addButton_deviceType, "Add Button Vendor Model");
		//verifyExists(searchForDeviceObj.searchDevice.deviceTypeDropdown, "Device Type Dropdown");
		//ClickonElementByString("//select[@id='availableDeviceTypes']//option[@label='"+ deviceType +"']", 30);
		
		//verifyExists(searchForDeviceObj.searchDevice.searchCity, "Search City");
		//sendKeys(searchForDeviceObj.searchDevice.searchCity, searchCity);
		//Reusable.SendkeaboardKeys(searchForDeviceObj.searchDevice.searchCity, Keys.ENTER);
		waitForAjax();
		
		//verifyExists(searchForDeviceObj.searchDevice.cityDropdown1 +cityValue+ searchForDeviceObj.searchDevice.cityDropdown2, "City Dropdown");
		//click(searchForDeviceObj.searchDevice.cityDropdown1 +cityValue+ searchForDeviceObj.searchDevice.cityDropdown2, "City Dropdown");
		//select()
		
		//verifyExists(searchForDeviceObj.searchDevice.addButton_city, "Add Button City");
		//click(searchForDeviceObj.searchDevice.addButton_city, "Add Button City");
		
		//verifyExists(searchForDeviceObj.searchDevice.searchCO, "Search CO");
		//sendKeys(searchForDeviceObj.searchDevice.searchCO, searchCO);
		//Reusable.SendkeaboardKeys(searchForDeviceObj.searchDevice.searchCity, Keys.ENTER);
		//SendkeyusingAction(Keys.ENTER);
		waitForAjax();

		//verifyExists(searchForDeviceObj.searchDevice.avaialableCOvalue1 +coValue+ searchForDeviceObj.searchDevice.avaialableCOvalue2, "CO Dropdown");
		//click(searchForDeviceObj.searchDevice.avaialableCOvalue1 +coValue+ searchForDeviceObj.searchDevice.avaialableCOvalue2, "CO Dropdown");
		
		//verifyExists(searchForDeviceObj.searchDevice.addButton_CO, "Add Button CO");
		//click(searchForDeviceObj.searchDevice.addButton_CO, "Add Button CO");
		
		verifyExists(searchForDeviceObj.searchDevice.routerId_tetField, "Router Id tetField");
		sendKeys(searchForDeviceObj.searchDevice.routerId_tetField, RouterID);
		
		verifyExists(searchForDeviceObj.searchDevice.searchButton, "Search Button");
		click(searchForDeviceObj.searchDevice.searchButton, "Search Button");
		Reusable.WaitforCPQloader();
	}
	
	public void selectValueUnderRecord() throws IOException, InterruptedException
	{
		verifyExists(searchForDeviceObj.searchDevice.routerId_tetField, "Router Id tetField");		
		scrollDown(searchForDeviceObj.searchDevice.routerId_tetField);
		
		verifyExists(searchForDeviceObj.searchDevice.clickOnSearcheddevice, "Click On Searched Device");
		click(searchForDeviceObj.searchDevice.clickOnSearcheddevice, "Click On Searched Device");
		
		verifyExists(searchForDeviceObj.searchDevice.actiondropdown, "Action Dropdown");
		click(searchForDeviceObj.searchDevice.actiondropdown, "Action Dropdown");
		
		verifyExists(searchForDeviceObj.searchDevice.viewLink, "View Link");
		click(searchForDeviceObj.searchDevice.viewLink, "View Link");
	}
	
	
	public void fetchValueInViewDevicePage() throws InterruptedException, IOException
	{	
		GetValues();		//Device Name
	}
/*
	public String GetValues(String labelname) throws InterruptedException {

		String text = null;
		WebElement element = null;

		try {
			
			waitForAjax();
			element = findWebElement(searchForDeviceObj.searchDevice.fetchValueFromViewPage);
			String value = element.getText();
			//element = getwebelement(xml.getlocator("//locators/" + application + "/fetchValueFromViewPage").replace("value", labelname));
			String ele = element.getText();
			
			if(element==null)
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname +"' is not found");
			}
			else if (ele!=null && ele.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname +"' value is empty");
			}
			else {   

				text = element.getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname +"' value is displayed as : '"+text+"'");

			}
		}catch (Exception e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname +"' value is not displaying");
			e.printStackTrace();
		}
		return text;

	}
*/
	public void GetValues() throws InterruptedException, IOException
	{		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_NameTab, "Name Tab");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_NameTab, "Name Tab");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_DeviceType, "Device Type");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_DeviceType, "Device Type");//String deviceType =
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_Vendor_Model, "Vendor Model");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_SNMP, "SNMP");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_SNMP, "SNMP");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_RouterId, "Router Id");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_RouterId, "Router Id");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_FullIQNET, "Full IQNET");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_ManagementAdd, "Management Address");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_Country, "Country");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_Country, "Country");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_City, "City");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_City, "City");
		
		verifyExists(searchForDeviceObj.searchDevice.fetchValueFromViewPage_Site, "Site");
		getTextFrom(searchForDeviceObj.searchDevice.fetchValueFromViewPage_Site, "Site");
		
	}
	
}
