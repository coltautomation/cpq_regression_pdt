package testHarness.c4cFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4COpportunityObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;

public class C4COpportunity extends SeleniumUtils
{
	CPQLoginPage Login = new CPQLoginPage();
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void NavigateToOpportunities(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String Account_Name = null;
		Account_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Account_Name");
		
		Reusable.WaitforC4Cloader();
		String accHeaderObj = C4CAccountsObj.Accounts.accountHeader1+ Account_Name +C4CAccountsObj.Accounts.accountHeader2;
		verifyExists(accHeaderObj,"Account Header");
		
		Reusable.WaitforC4Cloader();
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.opportunityMenuItem, 90);
		verifyExists(C4COpportunityObj.Opportunity.opportunityMenuItem,"Opportunity Menu Item");
		click(C4COpportunityObj.Opportunity.opportunityMenuItem,"Opportunity Menu Item");		
	}
	
	public void OpenNewOpportunity() throws InterruptedException, IOException 
	{
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.oppNewBtn,"Opportunity New Button");
		click(C4COpportunityObj.Opportunity.oppNewBtn,"Opportunity New Button");
		
		Reusable.WaitforC4Cloader();
		pause(2000);
		verifyExists(C4COpportunityObj.Opportunity.newOpportunityTab,"New Opportunity Tab");
	}
	
	public void EnterOpportunityDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String oppName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Opportunity_Name") + timestamp.getTime();
		
		String offerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offer_Type");
		String primaryProg = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary_Programme");
		String primaryAttr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary_Attribute");
		String oppCurrency = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Opportunity_Currency");
		String campaign = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Campaign");
		
		Reusable.WaitforC4Cloader();
		isEnable(C4COpportunityObj.Opportunity.nameTxb);
		verifyExists(C4COpportunityObj.Opportunity.nameTxb, "Name Textbox");
		sendKeys(C4COpportunityObj.Opportunity.nameTxb, oppName, "Name Textbox");
		
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.offerTypeList, "Offer Type List");
		click(C4COpportunityObj.Opportunity.offerTypeList,"More Save Options");
		ClickonElementByString("//li[normalize-space(.)='"+ offerType +"']", 30);
		
		Reusable.WaitforC4Cloader();
		
		if(verifyExists(C4COpportunityObj.Opportunity.campaignTxb))
		{
			waitForElementToAppear(C4COpportunityObj.Opportunity.campaignTxb, 15);
			verifyExists(C4COpportunityObj.Opportunity.campaignTxb, "Campaign Textbox");
			sendKeys(C4COpportunityObj.Opportunity.campaignTxb, campaign, "Campaign Textbox");
			pause(1000);
			keyPress(C4COpportunityObj.Opportunity.campaignTxb,Keys.ENTER);
			pause(1000);
		}	
		
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.primaryProgList, "Primary Program List");
		click(C4COpportunityObj.Opportunity.primaryProgList,"Primary Program List");
		ClickonElementByString("//li[normalize-space(.)='"+ primaryProg +"']", 30);
		
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.primaryAttriList, "Primary Attribute List");
		click(C4COpportunityObj.Opportunity.primaryAttriList,"Primary Attribute List");
		ClickonElementByString("//li[normalize-space(.)='"+ primaryAttr +"']", 30);
		
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.oppCurrencyList, "Opportunity Currency List");
		click(C4COpportunityObj.Opportunity.oppCurrencyList,"Opportunity Currency List");
		ClickonElementByString("//li[normalize-space(.)='"+ oppCurrency +"']", 30);
		
		Reusable.WaitforC4Cloader();
		
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.moreSaveOptions, 60);
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.moreSaveOptions);
		click(C4COpportunityObj.Opportunity.moreSaveOptions,"More Save Options");
		pause(1000);
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.saveNOpenBtn);
		click(C4COpportunityObj.Opportunity.saveNOpenBtn,"Save and Open Button");
		pause(3000);
		Reusable.WaitforC4Cloader();
		pause(3000);
		Reusable.Waittilljquesryupdated();
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.opprtunityIdBx, 60);
		String oppId = getTextFrom(C4COpportunityObj.Opportunity.opprtunityIdBx);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Opportunity_ID", oppId);		
	}
	
	public void editOpportunity(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException, AWTException 
	{
		String Legal_Complexity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Legal_Complexity");
		String Technical_Complexity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Technical_Complexity");
	
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.moreLink, 120);
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.moreLink, "More Link");
		click(C4COpportunityObj.Opportunity.moreLink,"More Link");
		
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.editOpportunityLnk, 120);
		verifyExists(C4COpportunityObj.Opportunity.editOpportunityLnk, "Edit Opportunity Link");
		click(C4COpportunityObj.Opportunity.editOpportunityLnk,"Edit Opportunity Link");
		
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.legalComplexityTxb, "Legal Complexity List");
		clickByJS(C4COpportunityObj.Opportunity.legalComplexityTxb);
		ClickonElementByString("//li[normalize-space(.)='"+ Legal_Complexity +"']", 30);
		
		Reusable.WaitforC4Cloader();
		verifyExists(C4COpportunityObj.Opportunity.techComplexityTxb, "Tech Complexity List");
		clickByJS(C4COpportunityObj.Opportunity.techComplexityTxb);
		ClickonElementByString("//li[normalize-space(.)='"+ Technical_Complexity +"']", 30);
		pause(1500);
		
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.saveBtn);
		click(C4COpportunityObj.Opportunity.saveBtn,"Save Button");
		Reusable.WaitforC4Cloader();
		//waitForElementToBeVisible(C4COpportunityObj.Opportunity.headerBar, 120);
	}
	
	public void ClickToAddQuote() throws InterruptedException, AWTException, IOException 
	{
		
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.quotesLnk, 90);
		click(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");
		
		pause(3000);
		Reusable.WaitforC4Cloader(); 
		Reusable.Waittilljquesryupdated();
		isPresent(C4COpportunityObj.Opportunity.addQuoteBtn, 60);	
		ScrollIntoViewByString(C4COpportunityObj.Opportunity.addQuoteBtn);
		Reusable.WaitforC4Cloader();
		click(C4COpportunityObj.Opportunity.addQuoteBtn,"Add Quote Button");		
		
	}
	
	public void NavigateToQuotes(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String Account_Name = null;
		Account_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Account_Name");
		
		/*String accNameObj = C4CAccountsObj.Accounts.accountName1+ Account_Name +C4CAccountsObj.Accounts.accountName2;
		click(accNameObj,"Account Name");
		
		waitForAjax();
		String accHeaderObj = C4CAccountsObj.Accounts.accountHeader1+ Account_Name +C4CAccountsObj.Accounts.accountHeader2;
		verifyExists(accHeaderObj,"Account Header");*/
		
		waitForElementToBeVisible(C4COpportunityObj.Opportunity.quotesLnk,75);
		verifyExists(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");
		click(C4COpportunityObj.Opportunity.quotesLnk,"Quotes Link");	
		for (int i = 1; i < 3; i++) { Reusable.WaitforC4Cloader(); }		
	}
	
}
