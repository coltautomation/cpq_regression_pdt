package pageObjects.cpqObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CPQQuoteCreationObj {

	public static class TransactionPage
	{	
		public static final String ConfigureText="@xpath=//div[contains(text(),'to configure')]"; //Configure Text

		public static final String Products_SectionHeader="@xpath=//h3[@id='oj-collapsible-1-header']"; //Product Section Header
		public static final String Products_SectionContent="@xpath=//div[@id='oj-collapsible-1-content']"; //Product Section Content
		//public static final String AddProducts_Button="@xpath=//button[@name='add_product']";
		
		//public static final String QuoteId="@xpath=//div[contains(@class, 'oj-inputtext')]/input[@id='quoteID_t']"; //Quote Id
		
		public static final String quoteNameTxb1="@xpath=//input[contains(@id,'quoteName')]";
		public static final String quoteTypeElem1="@xpath=//input[contains(@id,'quoteType_t')]";
		public static final String seRevLnk="@xpath=//span[text()='SE Rev']";
				
		public static final String QuoteId="@xpath=//div[contains(@class, 'oj-text-field-middle')]/input[@id='quoteID_t|input']";
		public static final String quoteNameTxb="@xpath=//div[contains(@class, 'oj-inputtext')]/input[@id='quoteID_t']"; //Quote Name Textbox
		public static final String approvedLnk="@xpath=//span[text()='Approved']"; //Quote Name Textbox
		public static final String quoteTypeElem="@xpath=//descendant::input[contains(@id,'quoteType')][2]"; //Quote Name Textbox
		public static final String quoteIDElem="@xpath=//descendant::input[contains(@id,'quoteID')]"; //Quote Id
		public static final String additionalQuoteDetailsLnk="@xpath=//div[@id='additionalQuoteDetails_t-wrapper']";
		public static final String opportunityVal="@xpath=//oj-input-text[@id='opportunityID_t']";	
		public static final String opportunityLnk="@xpath=//div[@id='opportunityInformation_t-wrapper']";
		public static final String SiebelId="@xpath=//input[@id='siebelTicketID_t|input']";
		
		//public static final String AddProductsBtn="@name=add_product"; //Add Product button

		//public static final String IPAccess_DropDown="@id=dataVoice-iPProduct"; //IP Access Drop down
		//public static final String ColtIPAccess_Option="@xpath=//li[@id='dataVoice-iPProduct']//a[contains(text(),'Colt IP Access')]"; //Colt IP Access drop down option

		//Landing page elements
		public static final String StartBanner="@xpath=//div[@class='start banner']"; //Banner on landing page
		public static final String StartBtn="@xpath=//div[@id='startBanner_html']//a[contains(text(),'Start')]"; //Start button on IP Access page
		
//		switch to proxy user
		public static final String adminBtn="@xpath=//img[@title='Admin']";
		public static final String internalUsersLnk="@xpath=//a[text()='Internal Users']";
		public static final String orderToQuoteManagerLnk="@xpath=//a[contains(text(),'Manager')]";
		public static final String proxyLogoutBtn="@xpath=//img[@title='Proxy Logout']";
		public static final String transactionsLnk="@xpath=(//a[@class='header-item'])[3]";
		public static final String copyTransactionsLnk="@xpath=descendant::a[text()='Copy'][1]";
		
		public static final String newTransactionBtn="@xpath=//a[@id='new_transaction']";
		public static final String tractTicketsBtn="@xpath=//button[contains(text(),'Track Tickets')]";
	
	}
	
	public static class AddProducts
	{ 
		//Add Products Page		
		public static final String AddProductBtn = "@xpath=//span[text()='Add Product']"; //Add Product Button
		public static final String CPESolutionLnk = "@xpath=//a[text()='CPE Solutions']"; //CPE Solutions product link
		public static final String CPEServiceLnk = "@xpath=//a[text()='CPE Solutions Service']";
		public static final String CPESiteLnk = "@xpath=//a[text()='CPE Solutions Site']";
		
		public static final String EthernetLnk = "@xpath=//a[text()='Ethernet']";
		public static final String EthernetLineLnk = "@xpath=//a[text()='Ethernet Line']";
		public static final String EthernetHubLnk = "@xpath=//a[text()='Ethernet Hub']";
		public static final String EthernetSpokeLnk = "@xpath=//a[text()='Ethernet Spoke']";
		
		public static final String OpticalLnk = "@xpath=//a[text()='Optical']";
		public static final String WaveLnk = "@xpath=//a[text()='Wave']";
		
		public static final String IPAccessLnk = "@xpath=//a[text()='IP Access']";
		public static final String ColtIpAccessLnk = "@xpath=//a[text()='Colt IP Access']";
		public static final String ColtIpDomainLnk = "@xpath=//a[text()='Colt IP Domain']";
		public static final String ColtIpGuardianLnk = "@xpath=//a[text()='Colt IP Guardian']";
		public static final String ColtManagedVirtualFirewallLnk = "@xpath=//a[text()='Colt Managed Virtual Firewall']";
		public static final String ColtManagedDedicatedFirewallLnk = "@xpath=//a[text()='Colt Managed Dedicated Firewall']";
		public static final String ColtIpAccessStartLnk = "@xpath=//a[text()='Start']";
		
		public static final String ProfessionalServicesLnk = "@xpath=//a[text()='Professional Services']";
		public static final String ProfessionalServicesDataLnk = "@xpath=//a[text()='Professional Services - Data']";
		public static final String ProfessionalServicesVoiceLnk = "@xpath=//a[text()='Professional Services - Voice']";
		
		public static final String DataConsultancyLnk = "@xpath=//a[text()='Data - Consultancy']";
		public static final String DataProjectManagement= "@xpath=//a[text()='Data - Project Management']";
		public static final String DataServiceManagement = "@xpath=//a[text()='Data - Service Management']";
		
		public static final String VoiceConsultancyLnk = "@xpath=//a[text()='Voice - Consultancy']";
		public static final String VoiceProjectManagement = "@xpath=//a[text()='Voice - Project Management']";
		public static final String VoiceServiceManagement = "@xpath=//a[text()='Voice - Service Management']";
		
		public static final String DataLink = "@xpath=//a[text()='Data']";
		public static final String VpnLink = "@xpath=//a[text()='VPN']";		
		public static final String VpnDataLink = "@xpath=//a[text()='VPN Data']";
		public static final String VpnNetworkLink = "@xpath=//a[text()='VPN Network']";
		
		public static final String productsLnk = "@xpath=//div[text()='Products']";
		public static final String productsTxb = "@xpath=//bdi[text()='Product']//parent::span//following-sibling::div//div//div//input";		
		public static final String productsSubLst = "@xpath=//ul[contains(@id,'inputField-popup-list-listUl')]";
		
		public static final String quotesLnk = "@xpath=//div[text()='Quotes']";
		public static final String addQuoteBtn = "@xpath=//bdi[text()='Add']//parent::span//parent::span//parent::button";		
		public static final String actionBtn = "@xpath=//bdi[contains(text(),'Action')]";
		public static final String EditBtn = "@xpath=//bdi[text()='Edit']";
		public static final String ViewBtn = "@xpath=//bdi[text()='View']";
		public static final String save4cBtn = "@xpath=//a[@id='save']";		
		public static final String update4cBtn = "@xpath=//span[text()='Update']";
		public static final String validate4cBtn = "@xpath=//span[text()='Validate']";
		
		public static final String copyQuoteC4CBtn = "@xpath=//bdi[text()='Copy']//parent::span";
		public static final String proceedC4CBtn = "@xpath=//bdi[text()='Proceed']//parent::span";		
		public static final String ojUpdateEnabled = "@xpath=//oj-button[contains(@class,'oj-enabled')]//span[text()='Update']";
		public static final String ojValidateEnabled = "@xpath=//oj-button[contains(@class,'oj-enabled')]//span[text()='Validate']";
		
		public static final String contContractsLenMnthTxb = "@xpath=//bdi[text()='Contract Length In Months']//parent::span//following-sibling::div//div//div//input";
		public static final String containerbandWidthLst = "@xpath=//bdi[text()='Bandwidth']//parent::span//following-sibling::div//div//div//input";		
		public static final String containerAddBtn = "@xpath=(//bdi[text()='Add']//parent::span//parent::span//parent::button)[1]";
		public static final String addNewcontainerBtn = "@xpath=//bdi[text()='Container - New Business']//parent::span//parent::span//parent::button";
		public static final String addCloudServiceProLst = "@xpath=//bdi[text()='Cloud Service Provider']//parent::span//following-sibling::div//div//div//input";
		
		
		
//		Container Upload Files
		public static final String containerUploadBtn = "@xpath=//span[contains(@onclick,'listPage_upload_quote_line')]";
		public static final String contUpldInternalRdb = "@xpath=//input[@value='internal']//parent::div";		
		public static final String contUpldcustomerRdb = "@xpath=//input[@value='customer_facing']//parent::div";
		
		public static final String returnToC4CBtn = "@xpath=//button[@name='return_to_c4c']";
		public static final String quoteNameTxb = "@xpath=//input[contains(@id,'quoteName')]";		
		public static final String quoteTypeElem = "@xpath=//div[contains(@id,'quoteType_')]";
		public static final String quoteIDElem = "@xpath=//descendant::input[contains(@id,'quoteID')]";
		
		
		public static final String quoteStageElem = "@xpath=//descendant::input[contains(@id,'status_t')]";		
		public static final String quoteStatusElem = "@xpath=//descendant::input[contains(@id,'statusNew')]";
		
		public static final String seRevLnk = "@xpath=//span[text()='SE Rev']";
		public static final String CSTRevLnk = "@xpath=//span[text()='CST Rev']";		
		public static final String TechnicalLnk = "@xpath=//span[text()='Technical Approval']";
		
		public static final String contUpldDocTxt = "@xpath=//h4[text()='Upload Document(s)']";
		public static final String contSelectDocDrp = "@xpath=//div[@class='modal-body']//select[@id='document_type']";		
		public static final String contchooseFile = "@xpath=//div[@class='modal-body']//b[text()='Choose File:']//parent::div//following-sibling::div//div//input";
		public static final String contUploadFile = "@xpath=//div[@class='modal-footer']//button[@id='upload_button']";
		public static final String contwhyisContainerFileLst = "@xpath=//div[@id='oj-select-choice-whyIsContainerJourneySelected_t']//a";	
		
		public static final String technicalApprovalLnk = "@xpath=//a[@title='Technical Approval']";	
		public static final String saveBtn = "@xpath=//descendant::bdi[text()='Save'][2]";	
		
		public static final String ATTACHMENTSTab="@xpath=//a[text()='ATTACHMENTS']";
		
		public static final String ipvalidateaddress= "@xpath=//span[text()='Validate Address']";
	
	}
	
	public static class CPEConfiguration
	{	
		public static final String NetworkTopologyLst="@xpath=//span[@id='ojChoiceId_networkTopology_selected']"; //Network Topology List
		public static final String saveToQuoteBtn="@xpath=//oj-button[@name='_add_to_transaction']";	
		public static final String cpeSiteConfigurationLnk="@xpath=//span[text()='CPE Site Configuration']";
		public static final String cpeSolutionServiceLst="@xpath=//div[contains(@id,'cPESolutionService')]//following-sibling::a";
		public static final String cpeSolutionServiceSelect="@xpath=//input[contains(@id, 'selectServiceRef')]";
		public static final String cpeSolutionServiceTable="//*[@id='cPEServiceReferenceArraySet-arrayset-table']/table";
		public static final String relatedServiceReferenceTxb="@xpath=//input[contains(@id,'relatedCPESolutionServiceReference')]";
		public static final String relatedNetworkProductLst="@xpath=//div[contains(@id,'relatedNetworkProductReferenceType')]//following-sibling::a";
		public static final String relatedNetworkReferenceTxb="@xpath=//input[contains(@id,'relatedNetworkProductReference')]";
		public static final String cpeSolutionServiceSubLst="@xpath=//ul[contains(@id,'cPESolutionService')]";
		public static final String relatedNetworkProductSubLst="@xpath=//ul[contains(@id,'relatedNetworkProductReferenceType')]";
		public static final String connectionTypeLst="@xpath=//div[contains(@id,'connectionType')]//following-sibling::a";
		public static final String connectionTypeSubLst="@xpath=//ul[contains(@id,'connectionType')]";
		public static final String serviceBandwidthLst="@xpath=//div[contains(@id,'serviceBandwidth')]//following-sibling::a";
		public static final String serviceBandwidthDropdown="@xpath=//*[@id='ojChoiceId_serviceBandwidth_selected']";
		public static final String serviceBandwidthTxb="@xpath=//input[contains(@aria-controls,'serviceBandwidth')]";
		public static final String serviceBandwidthQry="@xpath=//input[contains(@aria-controls,'serviceBandwidth')]//following-sibling::span//child::span";
		public static final String managedCPELst="@xpath=//div[contains(@id,'managedCPE')]//following-sibling::a";
		public static final String managedCPESubLst="@xpath=//ul[contains(@id,'managedCPE')]";
		public static final String cpeSolutionTypeLst="@xpath=//div[contains(@id,'cPESolutionType')]//following-sibling::a";
		public static final String cpeSolutionTypeSubLst="@xpath=//ul[contains(@id,'cPESolutionType')]";	
		
		public static final String cpeSolutionServiceTxb="@xpath=//div[@data-oj-containerid='ojChoiceId_cPESolutionService']//input";
		public static final String relatedNetworkProductTxb="@xpath=//div[@data-oj-containerid='ojChoiceId_relatedNetworkProductReferenceType']//input";
		
		public static final String CpeConfigurationLnk="@xpath=//span[text()='CPE CONFIGURATION']";
		public static final String ConnectionTypeLst="@xpath=//div[contains(@id,'connectionType')]//following-sibling::a";
		public static final String ConnectionTypeSubLst="@xpath=//ul[contains(@id,'connectionType')]";
		public static final String ServiceBandwidthLst="@xpath=//div[contains(@id,'serviceBandwidth')]//following-sibling::a";
		public static final String ServiceBandwidthTxb="@xpath=//input[contains(@aria-controls,'serviceBandwidth')]";
		public static final String ServiceBandwidthQry="@xpath=//input[contains(@aria-controls,'serviceBandwidth')]//following-sibling::span//child::span";
		public static final String ServiceBandwidthSubLst="@xpath=//ul[contains(@id,'serviceBandwidth')]";
		public static final String ManagedCPELst="@xpath=//div[contains(@id,'managedCPE')]//following-sibling::a";
		public static final String ManagedCPESubLst="@xpath=//ul[contains(@id,'managedCPE')]";
		public static final String CpeSolutionTypeLst="@xpath=//div[contains(@id,'cPESolutionType')]//following-sibling::a";
		public static final String CpeSolutionTypeSubLst="@xpath=//ul[contains(@id,'cPESolutionType')]";
		public static final String CpeModelLabel="@xpath=//label[text()='CPE Model']";
		public static final String InterfaceLst="@xpath=//div[contains(@id,'interface')]//following-sibling::a";
		public static final String InterfaceSubLst="@xpath=//ul[contains(@id,'interface')]";
		public static final String ResiliancyTypeLst="@xpath=//div[contains(@id,'resiliencyServiceLevel')]//following-sibling::a";
		public static final String ResiliancyTypeSubLst="@xpath=//ul[contains(@id,'resiliencyServiceLevel')]";
		public static final String ContractTermLst="@xpath=//div[contains(@id,'contractTermInYears')]//following-sibling::a";
		public static final String ContractTermSubLst="@xpath=//ul[contains(@id,'contractTermInYears')]";
		public static final String plannedBandwidthLst="@xpath=//div[contains(@id,'plannedBandwidth')]//following-sibling::a";
		public static final String plannedbwCbx="@xpath=//oj-switch[@id='isPlannedBWSelectedPrim']";
		public static final String plannedBandwidthTxb="@xpath=//input[contains(@aria-controls,'plannedBandwidth')]";
		public static final String plannedBandwidthQry="@xpath=//input[contains(@aria-controls,'plannedBandwidth')]//following-sibling::span//child::span";
		public static final String plannedBandwidthSubLst="@xpath=//ul[contains(@id,'plannedBandwidth')]";
		
		
	}
	
	public static class PrimaryAddressPage
	{	

		public static final String PrimaryAddressHdr="@xpath=//label[@id='siteAddressPrim-label|label']"; //PRIMARY ADDRESS DETAILS Section
		public static final String PrimaryConnectionLbl="@xpath=//span[text()='Primary Connection']";
		public static final String SiteAAddLbl="@id=siteAddressAEnd-label"; //Site Address label
		public static final String SiteBAddLbl="@id=siteAddressBEnd-label"; //Site Address label
		public static final String SiteAAddTxFld="@xpath=//input[contains(@aria-describedby,'siteAddressAEnd')]"; //Site Address Text Field
		public static final String SiteBAddTxFld="@xpath=//input[contains(@aria-describedby,'siteAddressBEnd')]"; //Site Address Text Field
		
		public static final String SiteAAddressTxb="@xpath=//input[contains(@aria-describedby,'siteAddressAEnd')]";
		public static final String SiteBAddressTxb="@xpath=//input[contains(@aria-describedby,'siteAddressBEnd')]";
		public static final String siteASearch="@xpath=//div[@id='searchPrimary-wrapper']";
		public static final String SiteAinput="@xpath=//input[@id='siteAddressPrim|input']";
		public static final String SiteAAddressLnk="@xpath=descendant::h3[text()='Site Address Details'][1]";
		public static final String SiteBAddressLnk="@xpath=descendant::h3[text()='Site Address Details'][2]";
		
		public static final String Datacenterchbox="@xpath=//oj-switch[@id='selectADataCenterPrim']";
		public static final String Datacenteradd="@xpath=//input[@id='dataCenterNamePrimary|input']";
		
		public static final String siteAPhoneNumberTxb="@xpath=//input[contains(@id,'siteTelephoneNumberAEnd')]";
		public static final String siteBPhoneNumberTxb="@xpath=//input[contains(@id,'siteTelephoneNumberBEnd')]";
		public static final String NextBtn="@xpath=//div[@id='html_next']//*[contains(text(),'Next')]"; //Next Button
		public static final String NextBtnElem="@xpath=//div[@id='html_next']//*[contains(text(),'Next')]"; //Next Button
		public static final String NextBtnId="@xpath=//span[text()='Next']"; //Next Button
		public static final String Nextbutt="@xpath=//*[@id='primaryVerifyAction']/div/span/label";
		public static final String SiteDetailsEntryBtn="@xpath=//div[@id='manuallyEnterAEndAddress']";
		public static final String SiteDetailsEntryButt="@xpath=//h3[text()='Site Address Details']";
		public static final String SiteDetailsphnum="@xpath=//input[contains(@id,'siteTelephoneNumberPrim')]";
		public static final String SearchBtnImg="@xpath=//div[@id='searchButtonAEnd']//img"; //Search Button img
		public static final String SiteBSearchBtnImg="@xpath=//div[@id='searchButtonBEnd']//img[contains(@title,'Search for an Address')]"; //Search Button img
		public static final String connectCheckElem="@xpath=//div[contains(text(), 'Connectivity Check In Progress')]"; //Next Button
	
	}
	public static class DataCenterAddressPage
	{
		public static final String DataCenterRdb="@xpath=//div[@aria-labelledby='selectADataCenterAEnd-label|label']";
		public static final String DataCenterName="@xpath=//input[contains(@aria-describedby,'datacenterAddressAEnd')]";
		public static final String AEndButtonOnMap="@xpath=//div//button[@title='Go to A end']";
		public static final String CopyTOBEndBtn="@xpath=//img//following-sibling::*[contains(text(),'Copy to B end')]";
		public static final String SelectDataCenterNameValue="@xpath=//div[@id='datacenterAddressAEnd|inputautocomplete-list']//div[1]";
	
		public static final String DataCenterBRdb="@xpath=//div[@aria-labelledby='selectADataCenterBEnd-label|label']";
		public static final String DataCenterNameB="@xpath=//input[contains(@aria-describedby,'datacenterAddressBEnd')]";
		public static final String SelectDataCenterNameValueB="@xpath=//div[@id='datacenterAddressBEnd|inputautocomplete-list']//div[1]";
		
		public static final String SelectDataCenterNameValueBForFidessa="@xpath=//div[@id='datacenterAddressBEnd|inputautocomplete-list']//div[2]";
		public static final String SelectDataCenterNameValueForFidessa="@xpath=//div[@id='datacenterAddressAEnd|inputautocomplete-list']//div[2]";
	
	}
	
	public static class OverrideBuilding
	{
		//Override Building type
		public static final String OverrideBuildingtype="@xpath=//oj-switch[@id='overrideBuildingTypePrimary']";
		public static final String OverrideBuildingtypedropdown="@xpath=//div[contains(@aria-labelledby,'overriddenBuildingTypePrimary')]//following-sibling::a";
		public static final String OverrideBuildingtypereason="@xpath=//textarea[contains(@id,'overrideReasonPrimary')]";
		public static final String OverrideBuildingtypeconfirm="@xpath=//oj-switch[contains(@id,'pleaseConfirmIfTheDataEnteredIsCorrectPrim')]";
				
	}
	
	public static class PrimaryConnectionPage
	{	
		public static final String ServiceBandwidthLbl="@id=serviceBandwidth_label"; //Service Bandwidth label
		public static final String ServiceBandwidthDrpDwn="@id=ojChoiceId_serviceBandwidth_selected"; //Service Bandwidth Drop down
		public static final String ServiceBandwidthVal1="@xpath=//select[@id='serviceBandwidth']//option[contains(@value,'"; //Service Bandwidth Drop down
		public static final String ServiceBandwidthVal2="')]";
		public static final String InterfaceLst="@xpath=//div[contains(@id,'interface')]//following-sibling::a";
		public static final String InterfaceSubLst="@xpath=//ul[contains(@id,'interface')]";
		public static final String siteDetailsLnk="@xpath=//span[text()='Site Details']"; //Site Details Link
		public static final String overrideToULLAEndCbx="@xpath=//*[@id='overrideToULLAEnd']/div/div/div"; //Override to ULL Aend Radio Button
		public static final String overrideToOnnetAEndCbx="@xpath=//*[@id='overrideToOnnetAEnd']/div/div/div"; //Override to ULL Aend Radio Button
		public static final String overrideToOnnetAEndSwitch="@xpath=//oj-switch[@id='overrideToOnnetAEnd']";
		public static final String overrideReasonAEndTxb="@xpath=//*[@id='overrideReasonAEnd|input']"; //Override Reason Aend
		public static final String AEndCorrectCbx="@xpath=//*[@id='pleaseConfirmTheDataYouHaveInputtedIsCorrectAEnd']/div/div"; //Confirm Aend Override Radio Button
		public static final String overrideToULLBEndCbx="@xpath=//oj-switch[@id='overrideToULLBEnd']"; //Override to ULL BendRadio Button
		public static final String overrideToOnnetBEndCbx="@xpath=//*[@id='overrideToOnnetBEnd']/div/div/div";
		public static final String overrideToOnnetBEndSwitch="@xpath=//oj-switch[@id='overrideToOnnetBEnd']";
		public static final String overrideReasonBEndTxb="@xpath=//textarea[contains(@id,'overrideReasonBEnd')]"; //Override Reason Bend
		public static final String BEndCorrectCbx="@xpath=//oj-switch[contains(@id,'InputtedIsCorrectBEnd')]"; //Confirm Bend Override Radio Button
		public static final String OverrideAEndBuildingType="@xpath=//div[@aria-labelledby='overrideBuildingTypeAEnd-label|label']";
		public static final String OverrideBEndBuildingType="@xpath=//div[@aria-labelledby='overrideBuildingTypeBEnd-label|label']";
		public static final String ojMarkPartialSaveSwt="@xpath=//*[@id='markforSave']"; //partial save button
		public static final String validatesitedetailsbtn="@xpath=//span[text()='Validate']"; //Validate Site details Button

		public static final String savetoquotebtn="@xpath=//span[text()='Save to Quote']"; //save to quote button
		public static final String return2c4cbtn="xpath=//button[@name='return_to_c4c']"; //Return to C4C Button
		
		public static final String reConfigureBtn="@xpath=//oj-button[@title='Reconfigure']";
		public static final String deleteProductBtn="@xpath=//oj-button[@title='Remove']";
		public static final String okdialogButton="@xpath=//div[@class='oj-dialog-footer']//..//span[text()='OK']";
		public static final String savetoquotebtn1="@xpath=//*[@name='_add_to_transaction']"; //save to quote button
		public static final String savebtn="@xpath=//oj-button[@name='save']"; //save to quote button
		public static final String return2c4cbtn1="@xpath=//button[@name='return_to_c4c'][1]"; //Return to C4C Button
		
		public static final String diversityFlagCbx="@xpath=//*[@id='diversityFlag_pf']/div/div/div";

	}
	
	public static class engagementpf
	{
		public static final String engagementpf="@xpath=//span[text()='Engage Portfolio Pricing']";
		public static final String quotestatus="@xpath=//input[@id='status_t-ro']";
		public static final String SelectProduct="@xpath=//*[@id=\'lig-table\']/table/tbody/tr[1]";
		public static final String reConfigureBtn="@xpath=//oj-button[@title='Reconfigure']";
		public static final String buildingtype="@xpath=//oj-select-one[@id='siteTypeAEnd']";
		public static final String PricingExcTab="@xpath=//span[text()='Pricing Exception']";
		public static final String PricingCityHub="@xpath=//oj-label[@id='pricingCityAendPPT-label']";
		public static final String PricingCityBEnd="@xpath=//oj-label[@id='pricingCityBendPPT-label']";
		public static final String PricingCityLst="@xpath=//*[@id='oj-combobox-choice-pricingCityAendPPT-combobox']/a";
		public static final String PricingCityLst1="@xpath=//*[@id='oj-combobox-choice-pricingCityBendPPT-combobox']/a";
		public static final String PricingCityLbl="@xpath=//label[@id='pricingCityHubPPT-label|label']";
		public static final String PricingCityHubLst="@xpath=//*[@id='oj-combobox-choice-pricingCityHubPPT-combobox']/a";
		
		public static final String POALabel="@xpath=//h3[contains(text(),'Price not found / POA')]";
		public static final String NRCTxb="@xpath=//input[@id='basePriceWithoutFeatures1YrTermNRC|input']";
		public static final String MRCTxb="@xpath=//input[@id='basePriceWithoutFeatures1YrTermMRC|input']";
		
		public static final String BaseCostLabel="@xpath=//h3[contains(text(),'Base Cost Exception')]";
		public static final String BaseCostTxb="@xpath=//input[@id='lDNCapexCost_PPT|input']";
		
		public static final String PricingCityLbl_Spoke="@xpath=//oj-label[text()='Pricing City - Spoke']";
	    public static final String PricingCitySpokeLst="@xpath=//*[@id='oj-combobox-choice-pricingCityAendPPT-combobox']/a";

	}
	
	public static class EngportfolioPage
	{
		public static final String engportpricing="@xpath=//span[text()='Engage Portfolio Pricing']";
		public static final String pandl="@xpath=//span[text()='P & L']";
		public static final String selectuser="@xpath=//div[@id='oj-select-choice-portfolioTeamAssignment']";
		public static final String selectuserdrop="@xpath=//*[@is='ojChoiceId_portfolioTeamAssignment_selected']";
		public static final String assignquote="@xpath=//span[text()='Assign Quote']";
		//public static final String lineItemstable="@xpath=//table[@aria-describedby='lig-summary']";
		public static final String lineItemstable="@xpath=//table[@aria-label='Line Item Grid']";
		public static final String savebtn="@xpath=//span[text()='save'][2]";
		public static final String refreshallprices="@xpath=//span[text()='Refresh All Prices']";
		public static final String sendtosales="@xpath=//span[text()='Send To Sales']";
	}
	
	public static class IPFeaturesPage
	{	
		public static final String RouterTypeLbl="@id=selectRouterTypeAEnd_label"; //Router Type label
		public static final String RouterTypeDrpDwn="@id=selectRouterTypeAEnd"; //Router Type Drop Down
		public static final String RouterTypeVal1="@xpath=//select[@id='selectRouterTypeAEnd']//option[contains(@value,'"; //Router Type Value
		public static final String RouterTypeVal2="')]";
		
	//  Ip Access Router Type Configuration
		public static final String IpIPRouterTypeLst="@xpath=//*[@id='oj-select-choice-routerTypePrimary']/a";
		public static final String IpIPRouterTypeSubLst="@xpath=//ul[contains(@id,'routerTypePrimary')]";
		public static final String IpSelectInterFaceLst="@xpath=//select[@id='selectInterfaceTypeAEnd']";
		public static final String IpNATCbx="@xpath=//div[contains(@aria-labelledby,'nATPrimary')]";
		public static final String IpColtIdTbx="@xpath=//input[contains(@id,'coltCPEIDPrimary')]";
		public static final String IpColtCostTbx="@xpath=//input[contains(@id,'costOfCPEPrimary')]";
		public static final String IpColtRouterModelLst="@xpath=//input[contains(@id,'routerModelPrimary')]";

		//  IP Access BGP4 Type
		public static final String IpBGP4FeedCbx="@xpath=//div[contains(@aria-describedby,'bGP4FeedPrimary')]";
		public static final String IpBGP4FeedTypeLst="@xpath=//div[contains(@id,'bGP4FeedTypePrimary')]//following-sibling::a";
		public static final String IpBGP4FeedTypeSubLst="@xpath=//ul[contains(@id,'bGP4FeedTypePrimary')]";
		public static final String CpeBGPTypeAsEndLst="@xpath=//div[contains(@id,'typeOfASPrimary')]//following-sibling::a";
		public static final String CpeBGPTypeAsEndSubLst="@xpath=//ul[contains(@id,'typeOfASPrimary')]";
		public static final String IpPresentationIntLst="@xpath=//oj-select-one[contains(@id,'servicePresentInterfaceTypeServiceLvl')]//following-sibling::a";
		public static final String IpPresentationIntSubLst="@xpath=//ul[contains(@id,'servicePresentInterfaceTypeServiceLvl')]";
		public static final String ipLANTypeLst="@xpath=//div[@id='oj-select-choice-routerLANInterfaceTypePrimary']//following-sibling::a";
		public static final String ipLANTypeSubLst="@xpath=//ul[contains(@id,'routerLANInterfaceTypePrimary')]";

		//  IP Access SMTP Type
		public static final String IpSMTPFeedCbx="@xpath=//div[contains(@aria-describedby,'sMTPPrimary')]";

		//  PRESENTATION INTERFACE
		public static final String IpServicePresenattionLst="@xpath=//select[@id='servicePresentationInterfaceType']";
		public static final String IpinterfaceAEndLst="@xpath=//div[contains(@id,'interfaceAEnd')]//following-sibling::a";
		public static final String IpinterfaceAEndSubLst="@xpath=//ul[contains(@id,'interfaceAEnd')]";
		public static final String IpFibreTypeLst="@xpath=//div[contains(@id,'fiberTypeAEnd')]//following-sibling::a";
		public static final String IpFibreTypeSubLst="@xpath=//ul[contains(@id,'fiberTypeAEnd')]";
		public static final String IpConnectorLst="@xpath=//div[contains(@id,'connectorAEnd')]//following-sibling::a";
		public static final String IpConnectorSubLst="@xpath=//ul[contains(@id,'connectorAEnd')]";
		public static final String IpPresentationInterFaceLst="@xpath=//span[text()='PRESENTATION INTERFACE']";
		public static final String IpServiceInterFaceLst="@xpath=//div[contains(@id,'servicePresentationInterfaceType')]//following-sibling::a";
		public static final String IpServiceInterSubLst="@xpath=//ul[contains(@id,'servicePresentationInterfaceType')]";

		//  IP ADDRESSING
		public static final String IpAddressingFormatLst="@xpath=//div[contains(@id,'iPAddressingFormatAEnd')]//following-sibling::a";
		public static final String IpAddressingFormatSubLst="@xpath=//ul[contains(@id,'iPAddressingFormatAEnd')]";
		public static final String IpAddressingTypeAEndLst="@xpath=//div[contains(@id,'iPv4AddressingTypeAEnd')]//following-sibling::a";
		public static final String IpAddressingTypeAEndSubLst="@xpath=//ul[contains(@id,'iPv4AddressingTypeAEnd')]";
		public static final String IpAddressingIPv4Lst="@xpath=//div[contains(@id,'numberOfIPv4Addresses')]//following-sibling::a";
		public static final String IpAddressingIPv4SubLst="@xpath=//ul[contains(@id,'numberOfIPv4Addresses')]";
		public static final String IpCarrierHotelLst="@xpath=//input[@name='carrierHotelCrossConnectAEnd']";
		public static final String CarrierHotelcbx="@xpath=//oj-switch[@id='carrierHotelCrossConnectPrimary']";
	}

	
	public static class SiteAddonsPage
	{	
		public static final String AddOnsHeader="@xpath=//span[contains(text(),'ADDONS')]"; //ADDONS Header
		public static final String IpSiteAddonsLnk="@xpath=descendant::span[text()='Site Addons'][2]";
		
	}
	
	public static class L3RESILIENCEPage
	{	
		public static final String L3ResilienceGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'L3 Resilience')]"; //L3RESILIENCE Group Header
		public static final String IPL3ResilienceLnk="@xpath=//a[@title='Primary Features']";
		public static final String L3Resiliency="@xpath=//a[@title='L3 Resiliency']";
		public static final String ResiliancyTypeLst="@xpath=//div[contains(@id,'resiliencyServiceLevel')]//following-sibling::a";
		public static final String ipResiliancyTypeLst="@xpath=//div[contains(@id,'layer2ResiliencePrim')]//following-sibling::a";
		public static final String Accesstype="@xpath=//*[@id='connOption']";
		public static final String ResiliancyTypeSubLst="@xpath=//ul[contains(@id,'resiliencyServiceLevel')]";
		public static final String IPContractTermLst="@xpath=//div[contains(@id,'contractTermAEnd')]//following-sibling::a";
		public static final String IPContractTermSubLst="@xpath=//ul[contains(@id,'contractTermAEnd')]";
		public static final String IPBillingTypeLst="@xpath=//div[contains(@id,'billingTypePrim')]//following-sibling::a";
		public static final String IPBillingTypeSubLst="@xpath=//ul[contains(@id,'billingTypePrim')]";
		public static final String IPcomittedBandwidthLnk="@xpath=//div[contains(@id,'committedBandwidth')]//following-sibling::a";
		public static final String IPcomittedBandwidthSubLnk="@xpath=//ul[contains(@id,'committedBandwidth')]";
		
		public static final String IpBillingTypeLst="@xpath=//div[contains(@id,'billingTypeAEnd')]//following-sibling::a";
		public static final String IpL2ReilienceLst="@xpath=//div[contains(@id,'layer2ResilienceAEnd')]//following-sibling::a";
		public static final String IpL2ReilienceSubLst="@xpath=//ul[contains(@id,'layer2ResilienceAEnd')]";
		public static final String ServiceBandwidthSubLst="@xpath=//ul[contains(@id,'serviceBandwidth')]";
		
		public static final String IpmaximumBandwidthLnk="@xpath=//div[contains(@id,'maximumBandwidth')]//following-sibling::a";
		public static final String IpmaximumBandwidthSubLnk="@xpath=//ul[contains(@id,'maximumBandwidth')]";
		
	}
	
	public static class DiversityPage
	{	
		public static final String DiversityGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Diversity')]"; //DIVERSITY Group Header
		
	}
	
	public static class ServiceAddonsPage
	{	
		public static final String ServiceLevelAddonsGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'SERVICE LEVEL ADDONS')]"; //SERVICE LEVEL ADDONS Group Header
		
	}
	
	public static class BespokeFeaturesPage
	{	
		public static final String BespokeFeaturesGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Bespoke Features')]"; //Bespoke Features Group Header
		
	}
	
	public static class AdditionalProductDataPage
	{	
		//Additional Product Data
		public static final String addtionalProductDataLnk="@xpath=//h3[contains(text(),'ADDITIONAL PRODUCT DATA')]";
		public static final String ipAccessaddtionalProductDataLnk="@xpath=//h3[text()='Additional Product Data']";
		public static final String existingCapacityLeadTimeLst="@xpath=//div[contains(@id,'existingCapacityLeadTime')]//following-sibling::a";
		public static final String existingCapacityLeadTimeSubLst="@xpath=//span[contains(@id,'existingCapacityLeadTime')]";
		public static final String AendSitePresentationInterface="@xpath=//span[@id='ojChoiceId_sitePresentationInterfacePrimary_Qto_selected']";
		public static final String BendSitePresentationInterface="@xpath=//span[@id='ojChoiceId_sitePresentationInterfaceBEndPrimary_Qto_selected']";
		public static final String AendSitePresentationInterfaceLst="@xpath=//div[@aria-labelledby='sitePresentationInterfacePrimary_Qto-label|label']//following-sibling::a";
		public static final String BendSitePresentationInterfaceLst="@xpath=//div[@aria-labelledby='sitePresentationInterfaceBEndPrimary_Qto-label|label']//following-sibling::a";
		public static final String AendSitePresentationInterfaceTxb="@xpath=//div[@data-oj-containerid='ojChoiceId_sitePresentationInterfacePrimary_Qto']//input";
		public static final String BndSitePresentationInterfaceTxb="@xpath=//div[@data-oj-containerid='ojChoiceId_sitePresentationInterfaceBEndPrimary_Qto']//input";
		public static final String AendSitePresentationDropdown="@xpath=//div[@id='ojChoiceId_sitePresentationInterfacePrimary_Qto']";
		public static final String BendSitePresentationDropdown="@xpath=//div[@id='ojChoiceId_sitePresentationInterfaceBEndPrimary_Qto']";
		
		//AEnd Details Entry
		public static final String cabinetTypeALst="@xpath=//div[contains(@id,'siteCabinetTypeAEndPrimary')]//following-sibling::a";
		public static final String cabinetTypeASubLst="@xpath=//ul[contains(@id,'siteCabinetTypeAEndPrimary')]";
		public static final String siteTypeLst="@xpath=//div[contains(@id,'siteType')]//following-sibling::a";
		public static final String siteTypeSubLst="@xpath=//ul[contains(@id,'siteType')]";
		public static final String presentationInterfaceALst="@xpath=//div[contains(@id,'sitePresentationInterfacePrimary')]//following-sibling::a";
		public static final String presentationInterfaceASubLst="@xpath=//ul[contains(@id,'sitePresentationInterfacePrimary')]";	
		public static final String cabinetIDATxb="@xpath=//input[contains(@id,'siteCabinetIDAEndPrimary')]";
		public static final String accessTechnologyALst="@xpath=//div[contains(@id,'siteAccessTechnologyPrimaru')]//following-sibling::a";
		public static final String accessTechnologyASubLst="@xpath=//ul[contains(@id,'siteAccessTechnologyPrimaru')]";
		public static final String customerPopStatusALst="@xpath=//div[contains(@id,'siteCustomerSitePOPStatusPrimary')]//following-sibling::a";
		public static final String customerPopStatusASubLst="@xpath=//ul[contains(@id,'siteCustomerSitePOPStatusPrimary')]";
		public static final String siteIDATxb="@xpath=//input[contains(@id,'siteIdAEnd')]";
		public static final String connectorTypeALst="@xpath=//div[contains(@id,'siteConnectorType')]//following-sibling::a";
		public static final String connectorTypeASubLst="@xpath=//ul[contains(@id,'siteConnectorType')]";
		public static final String portRoleALst="@xpath=//div[contains(@id,'sitePortRole')]//following-sibling::a";
		public static final String portRoleASubLst="@xpath=//ul[contains(@id,'sitePortRole')]";
		public static final String validate4cBtn="@xpath=//oj-button[@name='update']";
		public static final String update4cBtn="@xpath=//span[text()='Update']";
		public static final String validate="@xpath=//oj-button[@name='update']";
		public static final String validated="@xpath=//img[@title='Completed']";
		public static final String primaryfeatures="@xpath=//a[@title='Primary Features']";
		//BEnd Details Entry
		public static final String cabinetIDBTxb="@xpath=//input[contains(@id,'siteCabinetIDBEndPrimary')]";
		public static final String cabinetTypeBLst="@xpath=//div[contains(@id,'siteCabinetTypeBEndPrimary')]//following-sibling::a";
		public static final String cabinetTypeBSubLst="@xpath=//ul[contains(@id,'siteCabinetTypeBEndPrimary')]";
		public static final String accessTechnologyBLst="@xpath=//div[contains(@id,'siteAccessTechnologyBendPrimary')]//following-sibling::a";
		public static final String accessTechnologyBSubLst="@xpath=//ul[contains(@id,'siteAccessTechnologyBendPrimary')]";
		public static final String customerPopStatusBLst="@xpath=//div[contains(@id,'siteCustomerSitePOPStatusBEndPrimary')]//following-sibling::a";
		public static final String customerPopStatusBSubLst="@xpath=//ul[contains(@id,'siteCustomerSitePOPStatusBEndPrimary')]";
		public static final String siteIDBTxb="@xpath=//input[contains(@id,'siteIdBPrimary')]";
		public static final String presentationInterfaceBLst="@xpath=//div[contains(@id,'sitePresentationInterfaceBEndPrimary')]//following-sibling::a";
		public static final String presentationInterfaceBSubLst="@xpath=//ul[contains(@id,'sitePresentationInterfaceBEndPrimary')]";
		public static final String connectorTypeBLst="@xpath=//div[contains(@id,'siteConnectorTypeBEndPrimary')]//following-sibling::a";
		public static final String connectorTypeBSubLst="@xpath=//ul[contains(@id,'siteConnectorTypeBEndPrimary')]";
		public static final String portRoleBLst="@xpath=//div[contains(@id,'sitePortRoleBEndPrimary')]//following-sibling::a";
		public static final String portRoleBSubLst="@xpath=//ul[contains(@id,'sitePortRoleBEndPrimary')]";
		public static final String imgLoadComplete="@xpath=//img[@class='page-progress-complete']";
		
		public static final String IPinProgressWarning="@xpath=//div[contains(text(),'Primary: The selected building is')]";
		public static final String inProgressWarningAend="@xpath=//div[contains(text(),'A end: The selected building is')]";
		public static final String inProgressWarningBend="@xpath=//div[contains(text(),'B end: The selected building is')]";
				
		//VPN Add Product
		public static final String vpnCustomerPopLst="@xpath=//div[contains(@id,'siteCustomerSitePOPStatus_VPN')]//following-sibling::a";
		public static final String vpnCustomerPopSubLst="@xpath=//ul[contains(@id,'siteCustomerSitePOPStatus_VPN')]";
		public static final String vpnCabinetTypeLst="@xpath=//div[contains(@id,'siteCabinetType_VPN')]//following-sibling::a";
		public static final String vpnCabinetTypeSubLst="@xpath=//ul[contains(@id,'siteCabinetType_VPN')]";
		public static final String vpnCabinetIdTxb="@xpath=//input[contains(@id,'siteCabinetID_VPN')]";
		public static final String vpnSitePresentationLst="@xpath=//div[contains(@id,'sitePresentationInterface_VPN')]//following-sibling::a";
		public static final String vpnSitePresentationSubLst="@xpath=//ul[contains(@id,'sitePresentationInterface_VPN')]";
		public static final String vpnSiteConnectorLst="@xpath=//div[contains(@id,'siteConnectorType_VPN')]//following-sibling::a";
		public static final String vpnSiteConnectorSubLst="@xpath=//ul[contains(@id,'siteConnectorType_VPN')]";
		public static final String vpnSiteIdTxb="@xpath=//input[contains(@id,'siteID_VPN')]";
		public static final String vpnNetworkTreeLink="@xpath=(//span[@class='oj-treeview-item-text'])[1]";
		
		//IP Access
		public static final String ipSiteAddonsLnk="@xpath=descendant::span[text()='Site Addons'][2]";
		public static final String cpeIPAcessNextBtn="@xpath=//oj-option[@id='secondaryVerifyAction0']";
		
		//L3 Resiliency Id
		public static final String ipL3ResilianceLnk="@xpath=//div[contains(@id,'layer3Resiliency')]//following-sibling::a";
		public static final String ipL3ResilianceSubLnk="@xpath=//ul[contains(@id,'layer3Resiliency')]";
		public static final String ipFeaturesLnk="@xpath=//a[@title='Secondary Connection']";
		public static final String SecFeatures="@xpath=//a[@title='Secondary Features']";
		public static final String ipBackUpBandWidthLst="@xpath=//div[contains(@id,'backupBandwidth')]//following-sibling::a";
		public static final String ipBackUpBandWidthSubLst="@xpath=//ul[contains(@id,'backupBandwidth')]";
		public static final String accesssec="@xpath=//input[@id='connOptionSec']";
		
		//Address selection
		public static final String siteAAddressTxb="@xpath=//input[@id='siteAddressSec|input']";
		public static final String siteASearchImg="@xpath=//div[@id='searchSecondary-wrapper']";
		public static final String connectedViaColtElem="@xpath=//img//following-sibling::*[contains(text(),'Connected via Colt')]";
		public static final String connectedviacolt="@xpath=//div[text()='Connected via Colt']";
		public static final String connectionFoundORNFoundColtElem="@xpath=//*[text()='Connected via Colt' or text()='Could not find Onnet and Automated Nearnet Connectivity' or text()='Automated Nearnet']";
		
		//Presentation Interface
		public static final String ipinterfaceAEndLst="@xpath=//span[@id='ojChoiceId_interfaceAEnd_selected']";
		public static final String interfacetype="@xpath=//div[@id='oj-select-choice-routerLANInterfaceTypeSecondary']/a";
		public static final String ipinterfaceAEndSubLst="@xpath=//ul[contains(@id,'interfaceAEnd')]";
		public static final String ipFibreTypeLst="@xpath=//div[contains(@id,'oj-select-choice-interfaceSecondary')]//following-sibling::a";
		public static final String ipFibreTypeSubLst="@xpath=//ul[contains(@id,'fiberTypeAEnd')]";
		
		//Diversity
		public static final String ipDiversitySelector="@xpath=//div[contains(@aria-describedby,'diversity')]";
		
		//Ip Access Additional Product Data Primary
		public static final String ipCabinetType="@xpath=//div[contains(@id,'siteCabinetType')]//following-sibling::a";
		public static final String ipCabinetSubType="@xpath=//ul[contains(@id,'siteCabinetType')]";
		public static final String ipCabinetID="@xpath=//input[contains(@id,'siteCabinetID')]";
		public static final String ipCabinetTypesec="@xpath=//div[contains(@id,'siteCabinetTypeSecQTO')]//following-sibling::a";
		public static final String ipCabinetSubTypesec="@xpath=//ul[contains(@id,'siteCabinetTypeSecQTO')]";
		public static final String ipCabinetIDsec="@xpath=//input[contains(@id,'siteCabinetIDSecQTO')]";
		
		public static final String ipSiteAccessTechnologyLst="@xpath=//div[contains(@id,'siteAccessTechnology')]//following-sibling::a";
		public static final String ipSiteAccessTechnologySubLst="@xpath=//ul[contains(@id,'siteAccessTechnology')]";
				
		//Primary Section
		public static final String PrimaryGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Primary')]"; //Primary Group Header
		public static final String SiteAccessTechnologylbl="@id=siteAccessTechnology_label"; //Site Access Technology label
		public static final String SiteAccessTechnologyDrpDwn="@id=siteAccessTechnology"; //Site Access Technology Drop Down
		public static final String AccessTechVal1="@xpath=//select[@id='siteAccessTechnology']//option[contains(@value,'"; //Ethernet Value
		public static final String AccessTechVal2="')][1]";
		public static final String SiteCabinetlbl="@id=siteCabinetID_label"; //Site Cabinet Field label
		public static final String SiteCabinettxtfld="@id=siteCabinetID"; //Site Cabinet Text Field

		//Additional Service Info
		public static final String AdditionalServiceInfoGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Additional Service Info')]"; //Additional Service Info Group Header
		public static final String FirstNamelbl="@xpath=//label[@id='firstName-label|label']"; //First Name Label
		public static final String FirstNameTxtFld="@xpath=//input[@id='firstName|input']"; //First Name Text Field
		public static final String LastNamelbl="@xpath=//label[@id='lastName-label|label']"; //Last Name Label
		public static final String LastNameTxtFld="@xpath=//input[@id='lastName|input']"; //Last Name Text Field
		public static final String ContactNumberlbl="@xpath=//label[@id='contactNumber-label|label']"; //Contact Number Label
		public static final String ContactNumberTxtFld="@xpath=//input[@id='contactNumber|input']"; //Contact Number Text Field
		public static final String MobileNumberlbl="@xpath=//label[@id='mobileNumber-label|label']"; //Mobile Number Label
		public static final String MobileNumberTxtFld="@xpath=//input[@id='mobileNumber|input']"; //Mobile Number Text Field
		public static final String emailIdTxtFld="@xpath=//input[@id='email|input']"; //Email Id Text Field

		public static final String ipFaxNumber="@xpath=//input[contains(@id,'faxNumber')]";
		public static final String ipRouterTechnologyLst="@xpath=//div[contains(@id,'routerTechnology')]//following-sibling::a";
		public static final String ipRouterTechnologySubLst="@xpath=//ul[contains(@id,'routerTechnology')]";
		public static final String ipexistingCapacityLeadTimeLst="@xpath=//div[contains(@id,'existingCapacityLeadTimeAEnd')]//following-sibling::a";
		public static final String ipexistingCapacityLeadTimeSubLst="@xpath=//ul[contains(@id,'existingCapacityLeadTimeAEnd')]";
		public static final String ipVoiceBundleService="@xpath=//div[contains(@aria-describedby,'voiceBundledService')]";
		public static final String ipSecondarySite="@xpath=//span[text()='Secondary Site']";
		public static final String ojUpdateEnabled="@xpath=//oj-button[contains(@class,'oj-enabled')]//span[text()='Update']";
		public static final String ojValidateEnabled="@xpath=//oj-button[contains(@class,'oj-enabled')]//span[text()='Validate']";

		//Saving the Quote
		public static final String saveToQuoteBtn="@xpath=//span[text()='Save to Quote']";
		public static final String saveToQuoteBtn1="@xpath=//oj-button[@name='_add_to_transaction']";
		public static final String errorContentCPQ="@xpath=//div[contains(@class,'error-content')]";
		public static final String ManualReqRejErrorAEnd="@xpath=//span[@class='oj-message-content']//div[text()='A end - The selected option is REJECTED. Please select a valid alternative option.']";
		public static final String ManualReqRejErrorBEnd="@xpath=//span[@class='oj-message-content']//div[text()='B end - The selected option is REJECTED. Please select a valid alternative option.']";
		public static final String ManualReqRejErrorSpokeEnd="@xpath=//span[@class='oj-message-content']//div[text()='Spoke end - The selected option is REJECTED. Please select a valid alternative option.']";
		public static final String ManualReqRejErrorIpAccess="@xpath=//span[@class='oj-message-content']//div[text()='Primary - The selected option is REJECTED. Please select a valid alternative option.']";
		
		public static final String contractTermLst = "@xpath=//div[contains(@id,'contractTermInYears')]//following-sibling::a";
        public static final String contractTermSubLst = "@xpath=//ul[contains(@id,'contractTermInYears')]";
        public static final String contractTermMonthly = "@xpath=//input[@id='contractTermInMonths|input']";
		
//		CPQ Home Page
		public static final String returnToC4CBtn="@xpath=//oj-button[@name='return_to_c4c']";
		public static final String quoteStageElem="@xpath=//descendant::input[contains(@id,'status_t')]";
		
				
//		Save C4C
		public static final String saveCPQBtn="@xpath=//descendant::oj-button[@name='save']";
		public static final String childSaveCPQBtn="@xpath=//span[text()='Save']";		
		
		

	}
	
	public static class PricingAndPromotionsPage
	{	
		//Primary Section
		public static final String PromotionsGrpHdr="@xpath=//h3[@class='group-header']//span[contains(text(),'Promotions')]"; //Promotions Group Header
		public static final String PromotionsItm="@xpath=//div[@class='cfg-im-item selected']//img"; //Promotion Item Image

		public static final String UpdateBtn="@id=update"; //Update Button
		public static final String SaveToQuoteBtn="@id=add_to_transaction"; //Save to Quote Button

	
	}
	
	public static class LegalandTechnicalContactDetails
	{	
		//Primary Section
		public static final String LegalContactlink="@xpath=//*[@id=\'customerInformation_t-label\']";
		public static final String GetLegalcontactbtn="@xpath=//*[@id=\'legalGetContacts_d\']/input"; 

		public static final String searchText="@name=searchEmail";
		public static final String searchBtn="@id=searchButton";
		public static final String SelectLegalcontact="@xpath=//tr[1]//td[1]//i[1]";
		public static final String OCHLegalContact="@xpath=//a[@class='btn btn-success btn-block']";
		
		public static final String TechnicalContactslink="@xpath=//*[@id=\'technicalContactsDetails_t-label\']";
		public static final String GetTechnicalContactsbtn="@xpath=//div[@id='technicalGetContacts_d']//input"; 

		public static final String SelectTechnicalcontact="@xpath=//tr[1]//td[1]//i[1]";
		public static final String OCHTechnicalContact="@xpath=//a[@class='btn btn-success btn-block']";
		
		public static final String Savebutton="@xpath=//span[@id='ui-id-7']";
		public static final String generalInformationLnk="@xpath=//span[text()='General Information']//parent::a";
		

	}
	
	public static class SubmissionforCommercialApproval
	{	
		
		//public static final String CommercialApprovalLink="@xpath=//span[text()='Commercial Approval']//parent::a";
		public static final String ApprovalLink="@xpath=//span[text()='Approval']//parent::a";
		public static final String SubmittoAppvlBtn="@xpath=//span[@class='oj-button-text'][text()='Submit to Approval']";
		public static final String VPSalesTxtElem="@xpath=//div[contains(text(),'VP Sales -')]";
		public static final String dpReviewTxtElem="@xpath=//div[contains(text(),'Deal Pricing Review')]";
		public static final String VPSalesSubmitBtn="@xpath=//div[@id='approval-comment']//span[text()='Submit']";
		public static final String approvalTxb="@xpath=//span[contains(text(),'Approval is needed')]//following-sibling::oj-text-area//div//textarea";
		public static final String submitApprovalPopUpBtn="@xpath=//div[contains(@class,'-dialog-footer')]//span[text()='Submit']";
		public static final String LinktoOpportunity="@xpath=//div[contains(text(),'Please link this')]";
		public static final String dealPriceCommentTxb="@xpath=//input[contains(@id,'dealPricingReview-comment')]";
		public static final String dealPriceRejectBtn="@xpath=//div[@class='oj-button-label']//span[text()='Reject']";
		public static final String reviseCommercialBtn="@xpath=descendant::span[text()='Revise Commercial'][2]";
		public static final String reviseBtn="@xpath=//oj-button[@name='revise']";
		public static final String DP_approveDiscountBtn="@xpath=//oj-button[@title='Approve']";
	    public static final String DP_rejectDiscountBtn="@xpath=//oj-button[@title='Reject']";
		public static final String rejectDiscountBtn="@xpath=//div[contains(@aria-labelledby,'vPSales2-label')]//oj-button[@title='Reject']";
		public static final String approveDiscountBtn="@xpath=//div[contains(@aria-labelledby,'vPSales2-label')]//oj-button[@title='Approve']";
		public static final String rejectDiscountBtn_vp1="@xpath=//div[contains(@aria-labelledby,'vPSales1-label')]//oj-button[@title='Reject']";
		public static final String approveDiscountBtn_vp1="@xpath=//div[contains(@aria-labelledby,'vPSales1-label')]//oj-button[@title='Approve']";
		
		}
	
	public static class SubmissionforTechnicalApproval
	{	
		
		public static final String TechnicalApprovalLink="@xpath=//span[@class='oj-tabbar-item-label'][text()='Technical Approval']";
		public static final String ReadyforTechAppl="@xpath=//input[@type='radio' and @id='fastlaneOption_t0']";
		public static final String readyForTechApprovalCbx="@xpath=//input[@value='readyForTechnicalApproval']//parent::span";
		public static final String readyForTechApprCbxElem="@xpath=//input[@value='readyForTechnicalApproval']//parent::span";
		public static final String SubmitForTechApplBtn="@xpath=//oj-button[@name='submit_for_technical_approval']"; 
		public static final String addInternalNotesBtn="@xpath=//span[text()='Add Internal Note']";
		public static final String addInternalNotesTxb="@xpath=//textarea[@id='qtonotes_t']";
		public static final String SEEngagment="@xpath=//span[text()='SE Engagement']//parent::a";
		public static final String addBtn="@xpath=//span[text()='Add']";
		
		public static final String selectOptionsTechValLst="@xpath=//div[contains(@id,'choice-selectOptionsTechApproval')]//child::a";
		public static final String selectOptionsTechValSubLst="@xpath=//ul[contains(@id,'selectOptionsTechApproval')]";
		public static final String proceedToQuoteBtn="@xpath=//span[text()='Proceed To Quote']";
		
//		Project Quote 
		public static final String projectQuoteBtn="@xpath=//button[@name='proceed_to_quote']";
				
		public static final String fastSignInitiateCbx="@xpath=//input[@value='customerCommittedInitiateFastSignClosureOfTheOpportunity']//parent::span";
		public static final String fileAttachmentLnk="@xpath=//span[contains(@id,'file_attachment')]//a";
		public static final String notificationBarCPQ="@xpath=/div[@id='notificationsAndMessages_t']";
		
//		FastSign Process
		public static final String closeFastSignBtn="@xpath=//span[text()='Close Fast Sign Process']";
		public static final String fastSignReasonLst="@xpath=//div[contains(@id,'reason')]//child::a";
		public static final String fastSignReasonSubLst="@xpath=//ul[contains(@id,'reason')]";
		public static final String customerProofUploadTxb="@xpath=//input[@id='customerCommitmentProof_t']";
		
	}
	
	public static class BulkUpload
	{
//		Bulk Upload Products
		public static final String bulkUploadLnk="@xpath=//span[text()='Bulk Upload']";
		public static final String bulkAddNewLineBtn="@xpath=//input[@value='Add New Line Items to Quote']";
		public static final String bulkUploadNewFlieBtn="@xpath=//input[@value='Upload New File']";
		public static final String bulkfileInputBtn="@xpath=//input[@id='fileInput']";
		public static final String bulkUploadBtn="@xpath=//input[@value='Upload']";
		public static final String pendingConnCheckBtn="@xpath=//button[@id='checkConnectivityPending']/b[text()='Please wait.Address cleansing is in progress..']";
		public static final String bulkPleaseWaitText="@xpath=//div[text()='Please wait,File Upload in Progress....']";
		public static final String bulkUploadedTxt="@xpath=//span[text()='Uploaded']";
		public static final String bulkDetailsTxt="@xpath=//span[text()='Details']";
		public static final String bulkRefreshBtn="@xpath=//button[text()='Refresh']";
		public static final String bulkCheckConnectivityBtn="@xpath=//button[text()=' Check Connectivity']";
		public static final String bulkConnectivityTxt="@xpath=//span[text()='Connectivity Check in Progress.Pending']";
		public static final String connectivityStatusTxt="@xpath=//span[text()='Getting Connectivity Status.']";
		public static final String bulkOnnetACheckRdb="@xpath=//input[@id='fromAend']//preceding-sibling::span[.='ONNET'  and not(contains(@disabled,'disabled'))]";
		public static final String bulkOnnetBCheckRdb="@xpath=//input[@id='fromBend']//preceding-sibling::span[.='ONNET'  and not(contains(@disabled,'disabled'))]";
		public static final String bulkSelectAllCbx="@xpath=//span[text()='Select All']//preceding-sibling::input";
		public static final String bulkAddToQuoteBtn="@xpath=//button[text()='Add To Quote']";
		public static final String bulkPendingBtn="@xpath=//button[text()='Pending']";
		public static final String bulkContinueToQuoteBtn="@xpath=//button[text()='Continue To Quote']";	
		
		public static final String cpqTableOnGenInfo="@xpath=//div[@class='cpq-table-body oj-table-body']";
		public static final String cpqTableNumOfRows="@xpath=//div[@class='cpq-table-body oj-table-body']/div[contains(@class, 'cpq-table-body-row')]";
		
		public static final String cpqTableSelectRecord1="//div[@class='cpq-table-body oj-table-body']/div[@data-row-key='";
		public static final String cpqTableSelectRecord2="']/descendant::input";
		public static final String stageColRecord2="']/descendant::div[contains(@class, 'col-status_l')]/span";
		public static final String productColRecord2="']/descendant::div[contains(@class, 'col-_part_custom_field1-_model_name')]/span";
		
		public static final String nextPageNavEnabled="@xpath=//div[@class='cpq-table-bottom']/descendant::a[@aria-label='Next Page' and contains(@class, 'oj-enabled')]";
		public static final String nextPageArrow="@xpath=//div[@class='cpq-table-bottom']/descendant::a[@aria-label='Next Page']";
		
//		Opportunity Info
		public static final String opportunityLnk="@xpath=//button[text()='Pending']";
		public static final String dealTypeElem="@xpath=//button[text()='Pending']";
		public static final String quoteStageElem="@xpath=//descendant::input[contains(@id,'status_t')]";		
		
	}
	
	public static class OptionQuote
	{
//		Option Quote
		public static final String optionQuoteRBtn="@xpath=//oj-switch[@aria-labelledby='optionsQuote_t-label']";
		public static final String projectQuoteRBtn="@xpath=//div[@aria-labelledby='projectQuote_t-label']";
		public static final String noOfOptionsTxb="@xpath=//input[@id='numberOfOptions_t']";
		public static final String selectPrimaryOptionsLst="@xpath=//div[contains(@id,'selectPrimaryOptions')]//child::a";
		public static final String selectPrimaryOptionsSubLst="@xpath=//ul[contains(@id,'selectPrimaryOptions')]";
		public static final String discountCbx="@xpath=//input[@value='Discount']//parent::span";
		public static final String calculateLineLevelDiscountBtn="@xpath=//span[text()='Calculate Discount']";		
		
	}
	
	public static class AddresstypeConnectivity
	{
		public static final String Siteloadinprogress = "@xpath=//div[text()='Connectivity Check In Progress']";
		public static final String ConnectedViaColtElem = "@xpath=//img//following-sibling::*[contains(text(),'Connected via Colt')]";
		public static final String ConnectionFoundORNFoundColtElem = "@xpath=//*[text()='Connected via Colt' or text()='Could not find Onnet and Automated Nearnet Connectivity' or text()='Automated Nearnet']";
		public static final String ConnectionNotFound = "@xpath=//div//*[contains(text(),'Could not find Onnet and Automated Nearnet Connectivity')]";
		public static final String DataCenterConnectedElem = "@xpath=//div//*[contains(text(),'Datacenter Connected via Colt')]";
		public static final String AutomatedNearnetElem = "@xpath=//*[contains(text(),'Automated Nearnet')]";
		public static final String ConnectionNotFndIncomplete = "@xpath=//*[contains(text(),'Not Connectivity Found - Address Incomplete')]";
		public static final String NextBtn = "@xpath=//td[text()='Next']";
		public static final String SiteDetailsEntryBtn = "@xpath=//div[@id='manuallyEnterAEndAddress']";
		public static final String ValidateAddressABtn = "@xpath=//div[@id='validate_address_button_a']";
		public static final String ValidateAddressBBtn = "@xpath=//div[@id='validate_address_button_b']";
	}
	
	public static class EthernetSpokeConfig
	{
		public static final String HubTypeLst= "@xpath=//div[contains(@id,'hubType')]//following-sibling::a";
		public static final String HubTypeSubLst= "@xpath=//ul[contains(@id,'hubType')]";
		public static final String HubReferenceTxb= "@xpath=//input[contains(@id,'hubReferenceID')]";
		public static final String HubAddressTxb= "@xpath=//input[contains(@id,'siteAddressHubExisting')]";
		public static final String HubSearchImgLnk= "@xpath=//div[@id='searchButtonHub']//img[contains(@title,'Search for an Address')]";
		public static final String SopkeHubAddressTxb= "@xpath=//input[@id='siteAddressAEnd|input']";
		public static final String SpokeSearchImgLnk= "@xpath=//descendant::img[@title='Search for an Address'][2]";
		
		public static final String ValidateReferanceBtn="@xpath=//oj-option[@id='hubReferenceValidate_spoke0']";
		public static final String HelpTextForValidateReferance="@xpath=//div[@id='helpTextforReferenceValidation_spoke']";
		public static final String ValidMessagePopup="@xpath=//oj-dialog//div[text()='Hub Reference ID validation is successful. The reference is found in Siebel.']";
		public static final String ValidMessagePopupOKBtn="@xpath=//oj-dialog//div[@class='oj-button-label']";
		public static final String InValidMessagePopup="@xpath=//oj-dialog//div[contains(text(),'The Hub Reference is not found in Siebel.')]";
		
	}
	
	public static class VPNNetwork
	{
		public static final String vpnSelectChoiceNetworkLnk="@xpath=//div[@id='oj-select-choice-networkType']//a";
		public static final String VPNLoadBarPorcess="@xpath=//oj-progress[contains(@title,'Processing')]";
		public static final String VPNAddBTN="@xpath=//span[text()='ADD']";
		public static final String VPNTreeLink="@xpath=(//span[@class='oj-treeview-item-text'])[2]";
		public static final String VPNAddOnLink="@xpath=//span[text()='Add-Ons']";
		public static final String VPNOusideBusinessCbx="@xpath=//input[@name='outsideBusinessInstallationHoursVPN']";
		public static final String VPNLongLiningCbx="@xpath=//input[@name='longLiningVPN']";
		public static final String VPNNetworkTreeLink="@xpath=(//span[@class='oj-treeview-item-text'])[1]";
	}
	
	public static class VPNAddress
	{
		public static final String vpnSiteAddressTxb="@xpath = //input[contains(@id,'siteAddressSearchVPN')]";
		public static final String vpnSiteSearchImg="@xpath = //div[@id='searchButton']//img";
		public static final String vpnNextButton="@xpath = //span[text()='Next']";
		public static final String vpnsiteConfiguartionLink="@xpath = //span[text()='Site Configuration']";
		public static final String vpnlinkTypeLink="@xpath = //div[@id='oj-select-choice-linkTypeVPN']//a";
	}
	
	public static class PartialSave
	{
		public static final String PartialSaveAddressCbx="@xpath=//input[@name='partialSave_SiteAddress']";
		public static final String IPPartialSaveAddressCbx="@xpath=//input[@name='markForPartialSaveSysConfig']";
		public static final String PartialSaveSiteCbx="@xpath=//input[@name='partialSave_SiteDetails']";
		public static final String PartialSaveFeaturesCbx="@xpath=//input[@name='partialSave_Features']";
		public static final String PartialSaveAdditionalCbx="@xpath=//input[@name='partialSave_AdditionalTab']";
		public static final String SiteDetailsLnk="@xpath=//span[text()='Site Details']";
		public static final String SiteDetailsTab="@xpath=//span[text()='Site Details']/ancestor::li";
		public static final String OjMarkPartialSaveSwt="@xpath=//oj-switch[contains(@id,'partialSave_SiteDetails')]";
		public static final String OjIPMarkPartialSaveSwt="@xpath=//button[@id='markforSave']";
		public static final String OjMarkPartialSaveSiteCPE="@xpath=//oj-switch[contains(@id,'partialSave_SiteAddress')]";
		public static final String OjMarkPartialSaveAddonsBtn="@xpath=//oj-switch[contains(@id,'partialSave_Features')]";
	}
	
	public static class VPNDualEntry
	{
		public static final String VPNAddONLink ="@xpath=//span[text()='Add-Ons']";
		public static final String VPNDualEntryLink ="@xpath=//div[@id='oj-select-choice-exploreCheckboxActionsDualEntryVPN']//a";
		public static final String VPNExploreClsBtn ="@xpath=//input[@name='closeWindowForManualRequestDE']";
	}
	
	public static class VPNSiteConfig
	{
		public static final String VPNContractYearLnk="@xpath=//div[@id='oj-select-choice-contractTermSiteDetails']//a";
		public static final String VPNServiceBandLnk="@xpath=//div[@id='oj-select-choice-serviceBandwidthSiteDetails']//a";
		public static final String VPNResilienceLnk="@xpath=//div[@id='oj-select-choice-resilienceSiteDetails']//a";

	}	
	
	public static class Offnet_Nearnet_ManualOrder
	{
		public static final String OffnetCheckAEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestAEnd0')]";
		public static final String ManualEngagementAEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestAEnd5')]";		
		public static final String DslCheckAEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestAEnd2|cb')]";
		public static final String DslCheckBEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestBEnd2|cb')]";
		public static final String DslCheckAEndBtn_MultiReq="@xpath=//label[contains(@for,'offNetDSLCCRequestAEnd3|cb')]";
		public static final String DslCheckBEndBtn_MultiReq="@xpath=//label[contains(@for,'offNetDSLCCRequestBEnd3|cb')]";
		public static final String ExploreActionsAEndLst="@xpath=//div[contains(@id,'exploreCheckboxActionsEndA')]//following-sibling::a";
		public static final String ExploreActionsAEndSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndA')]";
		public static final String ExploreActionsRevalidateAEndSubLst="@xpath=//ul[contains(@id,'CheckboxActionsEndA')]";
		public static final String OffnetALinkTab="@xpath=//div[@data-tab='offNetA']/parent::li";
		public static final String OffnetBLinkTab="@xpath=//div[@data-tab='offNetB']/parent::li";
		public static final String DSLALinkTab="@xpath=//div[@data-tab='dslNetA']/parent::li";
		public static final String DSLBLinkTab="@xpath=//div[@data-tab='dslNetB']/parent::li";

		public static final String OffnetCheckBEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestBEnd0')]";
		public static final String ManualEngagementBEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestBEnd5')]";
		public static final String ExploreActionsBEndLst="@xpath=//div[contains(@id,'exploreCheckboxActionsEndB')]//following-sibling::a";
		public static final String ExploreActionsBEndSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndB')]";
		public static final String ExploreActionsRevalidateBEndSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndB')]";
		public static final String PriorityTxb="@xpath=//span[text()='Priority']//preceding-sibling::input";
		public static final String DiversityTypeTxb="@xpath=//span[text()='Diversity Type']//preceding-sibling::input";
		public static final String TechnologyTxb="@xpath=//span[text()='technology']//preceding-sibling::input";
		public static final String DualEntryCbx="@xpath=//span[@id='dualEntryStandard']//child::span[contains(@class,'CheckBox')]";
		public static final String GetQuoteBtn="@xpath=//span[text()='Get Quote']";
		public static final String ExpandArrowExploreIcn="@xpath=//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'dgrid-expando-icon ui-icon ui-icon-triangle')]";
		public static final String ExploreRequestIDTdElem="@xpath=//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'grid-expand')]//parent::td//following-sibling::td";
		public static final String RequestIDElem="@xpath=//span[contains(text(),'Request ID:')]";
		public static final String IpAccessexploreCloseBtn="@xpath=//label[contains(@for,'addToTableCheckBox_explore0')]";
		public static final String ExploreCloseBtn="@xpath=//oj-button[contains(@on-oj-action,'confirmClicked')]";
		public static final String ExploreOnnetCloseBtn="@xpath=//td[text()='X']";
		public static final String ExploreOLODSLCloseBtn="@xpath=(//td[text()='X'])[2]";
		public static final String ExploreIPAccessCloseBtn="@xpath=//oj-option[@class='oj-complete']//child::span[text()='X']";

		public static final String ExploreSubmitBtn="@xpath=//div[@id='application']//.//span[text()='Submit']";
		public static final String ExpSubmitDualEntry="@xpath=//span[text()='Submit']";
		public static final String AutomatedOffnetDefAEndRadioBtn="@xpath=descendant::div[@id='offNetA']//input[@name='connRadioVarNameAEnd'][1]";
		public static final String AutomatedOffnetDefBEndRadioBtn="@xpath=descendant::div[@id='offNetB']//input[@name='connRadioVarNameBEnd'][1]";
		public static final String AutomatedDSLDefAEndRadioBtn="@xpath=descendant::div[@id='dslNetA']//input[@name='connRadioVarNameAEnd'][1]";
		public static final String AutomatedDSLDefBEndRadioBtn="@xpath=descendant::div[@id='dslNetB']//input[@name='connRadioVarNameBEnd'][1]";		
		public static final String EtherNetAManualOffnetRdB="@xpath=//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd') and contains(@value,'1')]";
		public static final String EtherNetBManualOffnetRdB="@xpath=//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameBEnd') and contains(@value,'1')]";
		public static final String IpAccessManualOffnetRdB="@xpath=//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connOption') and contains(@data-cterm,'1')]";
		public static final String EtherNetManualNearnetARdBtn="@xpath=//td[contains(text(),'Waiting')]/preceding-sibling::td//input[contains(@name,'connRadioVarNameAEnd')]";
		public static final String EtherNetManualNearnetBRdBtn="@xpath=//td[contains(text(),'Waiting')]/preceding-sibling::td//input[contains(@name,'connRadioVarNameBEnd')]";
		public static final String EtherNetAutoNearnetARdBtn="@xpath=//td[text()='Automated']/preceding-sibling::td//input[contains(@name,'connRadioVarNameAEnd')]";
		public static final String EtherNetAutoNearnetBRdBtn="@xpath=//td[text()='Automated']/preceding-sibling::td//input[contains(@name,'connRadioVarNameBEnd')]";
		public static final String IpAccessManualNearnetARdBtn="@xpath=//td[contains(text(),'Waiting')]/preceding-sibling::td//input[contains(@name,'connOption')]";
		
		public static final String EtherNetDualEntryARdBtn="@xpath=//td[text()='Manual']/preceding-sibling::td//input[contains(@name,'connRadioVarIdA')]";
		public static final String EtherNetDualEntryBRdBtn="@xpath=//td[text()='Manual']/preceding-sibling::td//input[contains(@name,'connRadioVarIdB')]";

		public static final String EtherNetSpokeRevalidateOffnet="@xpath=(//td[text()='ACTUAL COST'])[2]/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd')]";
		public static final String ConnectionType="@xpath=//span[text()='Connection Type']//preceding-sibling::input";
		public static final String EtherNetSpokeDSLOffnet="@xpath=//input[@id='radioAEnd_0DSLCC']";
		public static final String OffnetWaveincrementalCapex="@xpath=//div[contains(@id,'Replacement_capex_interface')]//following-sibling::a";
		public static final String OffnetWaveCapexSubLst="@xpath=//ul[contains(@id,'incrementalReplacement_capex_interface')]";
		public static final String OffnetWaveincrementalOpex="@xpath=//div[contains(@id,'incrementalReplacement_opex_interface')]//following-sibling::a";
		public static final String OffnetWaveOpexSubLst="@xpath=//ul[contains(@id,'incrementalReplacement_opex_interface')]";
		public static final String OffnetWaveFrequency="@xpath=//div[contains(@id,'frequency_interface')]//following-sibling::a";
		public static final String OffnetWaveFrequencySubLst="@xpath=//ul[contains(@id,'frequency_interface')]";
		public static final String OffnetcopyQuoteOKBtn="@xpath=//button[@name='ok']";
		public static final String ExploreBuldingNumberTxb="@xpath=//input[@id='dijit_form_ValidationTextBox_15']";
		public static final String ExploreNearnetBuldingNumberTxb="@xpath=//input[@id='dijit_form_ValidationTextBox_4']";
		public static final String DslManualCheckAEndRadiBtn="@xpath=//td[text()='3rd Party DSL']/preceding-sibling::td//input[contains(@name,'connRadioVarNameAEnd')]";
		public static final String DslManualCheckBEndRadiBtn="@xpath=//td[text()='3rd Party DSL']/preceding-sibling::td//input[contains(@name,'connRadioVarNameBEnd')]";
		public static final String IpAccessDslManualCheckBtn="@xpath=//td[text()='Waiting for 3rd Party']/preceding-sibling::td//input[contains(@name,'connOption')]";
		
		public static final String IpOffnetCheckAEndBtn="@xpath=//td[@id='offnetcheckAEnd']";
		public static final String Ipoffnetcheckbtn="@xpath=//oj-option[@id='connectivityActionPrimary0']";
		public static final String Ipoffsetselectbtn="@xpath=//input[@id='connOption']";
		public static final String IpManualEngagementAEndBtn="@xpath=//td[@id='connectivityActionPrimary4']";
		public static final String IpManualengagementbtn="@xpath=//oj-option[@id='connectivityActionPrimary4']";
		public static final String IpExploreActionsAEndLst="@xpath=//div[contains(@id,'exploreManualEngagementCheckboxActionsAEnd')]//following-sibling::a";
		public static final String Ipexploredropdown="@xpath=//div[contains(@id,'raiseManualRequest_Prim')]//following-sibling::a";
		public static final String IpExploreActionsAEndSubLst="@xpath=//ul[contains(@id,'exploreManualEngagementCheckboxActionsAEnd')]";
		public static final String Ipexplorelst="@xpath=//ul[contains(@id,'raiseManualRequest_Prim')]";
		public static final String IpSupplierOnnetClose="@xpath=//input[contains(@id,'closeButtonForAEnd')]";
		public static final String IpDSLCheck="@xpath=//span[text()='DSL CHECK']";
		
		//Reuse Manual Request
		public static final String ReuseManualbtnAend="@xpath=//span[text()='REUSE MANUAL REQUEST']";
		public static final String ReuseManualbtnBend="@xpath=//label[@for='offNetDSLCCRequestBEnd4|cb']";
		public static final String Exploreidtxt="@xpath=//input[@id='exploreID']";
		public static final String Exploresearchbtn="@xpath=//a[@id='searchLink']";
		public static final String Exploreresultsel="@xpath=//tr[contains(@class,'jqgfirstrow')]//following-sibling::tr/td/input[@type='checkbox']";
		public static final String Explorereusebtn="@xpath=//div[@id='MyReuseID']/button";
		public static final String BCPReuseAend="@xpath=//td[contains(text(),'Manual Reuse')]/parent::tr/td/input[contains(@id,'radioAEnd')]";
		public static final String BCPReuseBend="@xpath=//td[contains(text(),'Manual Reuse')]/parent::tr/td/input[contains(@id,'radioBEnd')]";
		public static final String ManualReuseAccesstypAend="@xpath=//td[text()='Manual Reuse']//parent::td//following-sibling::td//input[@data-sitety='A']";
		public static final String ManualReuseAccesstypBend="@xpath=//td[text()='Manual Reuse']//parent::td//following-sibling::td//input[@data-sitety='B']";
		public static final String ManualReuseOLO="@xpath=//input[@value='OLO'][@class='radio']";
		public static final String errorMessage="@xpath=//div[contains(text(), 'Request cannot')]";
		public static final String ipBCPReuseAend="@xpath=//td[contains(text(),'Manual Reuse')]/parent::tr/td/input[@name='connOption']";
		
		//VPN Offnet Entry
		public static final String VpnOffNetCheckBtn="@xpath=//label[contains(@for,'connectivityCheckActions0')]";
		public static final String VpnManualCheckBtn="@xpath=//label[contains(@for,'connectivityCheckActions3')]";
		public static final String VpnExploreRequestVPN="@xpath=//div[@id='oj-select-choice-raiseManualRequestVPN']//a";
		public static final String VpnExploreRequestVPNSubLst="@xpath=//ul[contains(@id,'raiseManualRequestVPN')]";
		public static final String VpnExploreOffCloseBtn="@xpath=//input[contains(@name,'closeWindowForMR')]";
		public static final String VpnOffnetRadioBtn="@xpath=//span[@class='oj-radiocheckbox-icon']//input[@name='contractTerm1Year-0']";
		
		public static final String VpnOffNetDSLCheckBtn="@xpath=//label[contains(@for,'connectivityCheckActions1')]";
		public static final String DslManualCheckAEndRadiBtn1="@xpath=//td[text()='Waiting for 3rd Party']/preceding-sibling::td//input[contains(@name,'connRadioVarNameAEnd')]";
        public static final String DslManualCheckBEndRadiBtn1="@xpath=//td[text()='Waiting for 3rd Party']/preceding-sibling::td//input[contains(@name,'connRadioVarNameBEnd')]";
	}
	
	public static class Onnet_DualEntry_Features
	{
		public static final String FeaturesLnk="@xpath=//span[text()='Features']";
		public static final String FeaturesTab="@xpath=//span[text()='Features']/ancestor::li";
		public static final String SiteAddresslnk="@xpath=//span[text()='Site Address']";
		public static final String FeaturesLnkSelected="@xpath=//a[@title='Features'][@aria-selected='true']";
		public static final String BEndLink="@xpath=//div[text()='B End']";
		public static final String OnnetManualRequestLst="@xpath=//div[contains(@id,'exploreCheckboxActionsEndDE')]//following-sibling::a";
		public static final String OnnetManualRequestSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndDE')]";
		public static final String ReValidateRBtn="@xpath=//input[@value='REVALIDATE']";
		public static final String ReNegotiateRBtn="@xpath=//input[@value='RENEGOTIATE']";
		public static final String ExploreReValidateBtn="@xpath=//span[text()='Revalidate']";
		public static final String ExploreReNegotiateBtn="@xpath=//span[text()='Renegotiate']";
		
		//A-End Features
		public static final String OutsideBusinessHoursInstallationAEndCbx="@xpath=//oj-switch[@id='outsideBusinessHoursInstallationAEnd']";
		public static final String LongLiningAEndCbx="@xpath=//oj-switch[@id='longLiningAEnd']";
		public static final String InternalCablingAEndCbx="@xpath=//oj-switch[@id='internalCablingAEnd']";
		public static final String ipOutsideBusinessHoursInstallationAEndCbx="@xpath=//oj-switch[@id='outsideBusinessHoursInstallPrimary']";
		public static final String ipLongLiningAEndCbx="@xpath=//oj-switch[@id='longLiningPrimary']";
		//B_End Features
		public static final String OutsideBusinessHoursInstallationBEndCbx="@xpath=//oj-switch[@id='outsideBusinessHoursInstallationBEnd']";
		public static final String LongLiningBEndCbx="@xpath=//oj-switch[@id='longLiningBEnd']";
		public static final String InternalCablingBEndCbx="@xpath=//oj-switch[@id='internalCablingBEnd']";
		public static final String FastTractCbx="@xpath=//oj-switch[@id='fasttrackAddon']";
		public static final String DemarcationAEndCbx="@xpath=//oj-switch[@id='demarcationDeviceAEnd']";
		public static final String DemarcationBEndCbx="@xpath=//oj-switch[@id='demarcationDeviceBEnd']";
	}
	public static class CrossConnect_Featuers
	{
		//A-End features
		public static final String CarrierHotelCrossConnectAEnd="@xpath=//div[@aria-labelledby='carrierHotelCrossConnectAEnd-label|label']";
		public static final String SupplierAEndDropDown="@xpath=//div[@id='ojChoiceId_supplierAEnd_PickList_Primary-combobox']//a";
		public static final String SupplierAEndDropDownValue="@xpath=//ul[@aria-labelledby='supplierAEnd_PickList_Primary-label|label']//li[2]";
		
		//B-End Features
		public static final String CarrierHotelCrossConnectBEnd="@xpath=//div[@aria-labelledby='carrierHotelCrossConnectBEnd-label|label']";
		public static final String SupplierBEndDropDown="@xpath=//div[@id='ojChoiceId_supplierBEnd_PickList_Primary-combobox']//a";
		public static final String SupplierBEndDropDownValue="@xpath=//ul[@aria-labelledby='supplierBEnd_PickList_Primary-label|label']//li[2]";
		
	}
	public static class Product_Configuration
	{
//		line item grid table
		public static final String LineItemGridTable="@xpath=//table[@aria-label='Line Item Grid']";
		public static final String LineItemGridEditBox="@xpath=//input[@type='text']";

	}
	
	public static class ProductConfigration
	{	
		//OnNet AEnd and BEnd Site details
		public static final String onnetAendRadioBtn="@xpath=//input[contains(@name,'connRadioVarNameAEnd')]"; //Onnet AEnd Radio button
		public static final String onnetBendRadioBtn="@xpath=//input[contains(@name,'connRadioVarNameBEnd')]"; //Onnet BEnd Radio button
		public static final String ULLAendRadioBtn="@xpath=//input[@id='radioAEnd_0ULLCC']"; //Onnet AEnd Radio button
		public static final String ULLBendRadioBtn="@xpath=//input[@id='radioBEnd_0ULLCC']"; //Onnet BEnd Radio button
		public static final String onnetAlnk="@xpath=//div[@data-tab='onNetA']/parent::li"; //Onnet A link
		public static final String AendRadioBtn="@xpath=//td[text()='APPROVED']/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd') and contains(@value,'1')]"; //Onnet AEnd Radio button
        public static final String BendRadioBtn="@xpath=//td[text()='APPROVED']/following-sibling::td//*[contains(@name,'connRadioVarNameBEnd') and contains(@value,'1')]"; //Onnet BEnd Radio button
		public static final String onnetBlnk="@xpath=//div[@data-tab='onNetB']/parent::li"; //Onnet B link
		public static final String ULLAlnk="@xpath=//oj-option[@id='offNetDSLCCRequestAEnd6']"; //Onnet B link
		public static final String ULLBlnk="@xpath=//oj-option[@id='offNetDSLCCRequestBEnd6']"; //Onnet B link
		public static final String offnetCheckAEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestAEnd0')]"; //Offnet AEnd button
		public static final String offnetCheckBEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestBEnd0')]"; //Offnet AEnd button
		public static final String dslCheckAEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestAEnd1')]"; //Offnet A link
		public static final String dslCheckBEndBtn="@xpath=//label[contains(@for,'offNetDSLCCRequestBEnd1')]"; //Offnet B link
		public static final String automatedOffnetDefAEndRadioBtn="@xpath=descendant::div[@id='offNetA']//input[@name='connRadioVarNameAEnd'][1]"; //Offnet AEnd radio button
		public static final String automatedOffnetDefBEndRadioBtn="@xpath=descendant::div[@id='offNetB']//input[@name='connRadioVarNameBEnd'][1]"; //Offnet AEnd radio button
		public static final String automatedOffnetDefRadioBtn="@xpath=//tr[3]//td//input[@name='connOption']"; 
		public static final String AendBuildingStatus="@xpath=//input[@name='connRadioVarNameAEnd']//parent::td/following-sibling::td[2]";
		public static final String IPBuildingStatus="@xpath=//input[@id='connOption']//parent::td/following-sibling::td[2]";
		public static final String BendBuildingStatus="@xpath=//input[@name='connRadioVarNameBEnd']//parent::td/following-sibling::td[2]";
		
		public static final String etherNetAutoNearnetARdBtn="@xpath=//td[text()='Automated']/preceding-sibling::td//input[contains(@name,'connRadioVarNameAEnd')]"; //Nearnet AEnd Radio button
		public static final String etherNetAutoNearnetBRdBtn="@xpath=//td[text()='Automated']/preceding-sibling::td//input[contains(@name,'connRadioVarNameBEnd')]"; //Nearnet BEnd Radio button
	
		public static final String automatedDSLDefAEndRadioBtn="@xpath=//input[@id='radioAEnd_0DSLCC']"; //DSL AEnd Radio button
		public static final String automatedDSLDefBEndRadioBtn="@xpath=//input[@id='radioBEnd_0DSLCC']"; //DSL BEnd Radio button
		
		public static final String ipOffnetCheckAEndBtn="@xpath=//td[@id='offnetcheckAEnd']"; //IPOffnet AEnd Radio button
		
		public static final String vpnOffNetCheckBtn="@xpath=//label[contains(@for,'connectivityCheckActions0')]"; //VPNOffnet AEnd Radio button
		public static final String vpnOffnetRadioBtn="@xpath=//span[@class='oj-radiocheckbox-icon']//input[@name='contractTerm1Year-0']"; //VPNOffnet AEnd Radio button
		
		public static final String dslCheckBtn="@xpath=//td[@id='dslcheckAEnd']"; //ipDSL link
		
		public static final String onnetManualRequestLst="@xpath=//div[contains(@id,'exploreCheckboxActionsEndDE')]//following-sibling::a"; //
		public static final String onnetManualRequestSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndDE')]"; //
		
		public static final String vpnDualEntryLink="@xpath=//div[@id='oj-select-choice-exploreCheckboxActionsDualEntryVPN']//a"; //
		public static final String vpnExploreClsBtn="@xpath=//input[@name='closeWindowForManualRequestDE']"; //
		
		public static final String exploreSubmitBtn="@xpath=//div[@id='application']//.//span[text()='Submit']"; //
		public static final String exploreNearnetBuldingNumberTxb="@xpath=//input[@id='dijit_form_ValidationTextBox_4']"; //
		
		public static final String exploreRequestIDTdElem="@xpath=//div[contains(@id,'servicesDialog_')]//..//div[contains(@class,'grid-expand')]//parent::td//following-sibling::td"; //
		
		public static final String exploreCloseBtn="@xpath=//oj-button[contains(@on-oj-action,'[[confirmClicked')]"; //
		
		public static final String ipaccessAutoNearnetRdBtn="@xpath=//td[text()='Automated']/preceding-sibling::td//input[contains(@name,'connOption')]";
		
		
}
	public static class reValidatereNegotiateEntries
	
	{	
		public static final String exploreActionsAEndLst="@xpath=//div[contains(@id,'exploreCheckboxActionsEndA')]//following-sibling::a"; //
		public static final String exploreActionsAEndSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndA')]"; //
		public static final String exploreActionsRevalidateAEndSubLst="@xpath=//ul[contains(@id,'CheckboxActionsEndA')]"; //
		public static final String exploreActionsBEndLst="@xpath=//div[contains(@id,'exploreCheckboxActionsEndB')]//following-sibling::a"; //
		public static final String exploreActionsBEndSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndB')]"; //
		public static final String exploreActionsRevalidateBEndSubLst="@xpath=//ul[contains(@id,'exploreCheckboxActionsEndB')]"; //
		public static final String etherNetAManualOffnetRdB="@xpath=//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameAEnd') and contains(@value,'1')]"; //
		public static final String etherNetBManualOffnetRdB="@xpath=//td[text()='Waiting for 3rd Party']/following-sibling::td//*[contains(@name,'connRadioVarNameBEnd') and contains(@value,'1')]"; //
		public static final String siteDetailsLnk="@xpath=//span[text()='Site Details']"; //
		public static final String ipExploreActionsAEndLst="@xpath=//div[contains(@id,'exploreManualEngagementCheckboxActionsAEnd')]//following-sibling::a"; //
		public static final String ipExploreActionsAEndSubLst="@xpath=//ul[contains(@id,'exploreManualEngagementCheckboxActionsAEnd')]"; //
		public static final String vpnOffNetCheckBtn="@xpath=//label[contains(@for,'connectivityCheckActions0')]"; //
		public static final String vpnManualCheckBtn="@xpath=//label[contains(@for,'connectivityCheckActions3')]"; //
		public static final String vpnExploreRequestVPN="@xpath=//div[@id='oj-select-choice-raiseManualRequestVPN']//a"; //
		public static final String vpnExploreRequestVPNSubLst="@xpath=//ul[contains(@id,'raiseManualRequestVPN')]"; //
	

	}
	
	public static class professionalServicesConfiguration
	{
		public static final String ServiceTypeLst="@xpath=//div[contains(@id,'serviceType')]//following-sibling::a";
		public static final String EffortEstimationManDaysTxb="@xpath=//input[contains(@id,'effortEstimationInManDays')]";	
		public static final String GrosssNRCElem="@xpath=//input[contains(@id,'grossNonRecurringChargesNRC')]";
		public static final String PackageTypeLst="@xpath=//div[contains(@id,'packageType')]//following-sibling::a";
		public static final String ResourceRequired="@xpath=//input[contains(@id,'resourceRequired0')]";
		public static final String ContractTermInYears="@xpath=//div[contains(@id,'contractTermInYears')]//following-sibling::a";
	}
	
	public static class ipAddonProducts
	{
		public static final String IpDomainCountryLst="@xpath=//div[contains(@id,'countryIPD')]//following-sibling::a";
		public static final String IpAddOnCountrySubLst="@xpath=descendant::ul[@role='listbox'][1]";
		public static final String IpDomainCurrencyLst="@xpath=//div[contains(@id,'currencyDisplay')]//following-sibling::a";
		public static final String IpDomainCurrencySubLst="@xpath=//ul[contains(@id,'currencyDisplay')]";
		public static final String IpAccessServiceReferenceTxb="@xpath=//input[contains(@id,'iPAccessServiceReference')]";
		public static final String ClickDefaultCellIpDomainTble="@xpath=descendant::span[text()='Select...'][1]";
		public static final String DomainTypeGenericLst="@xpath=//div[contains(@id,'domainOrderTypeCountrySpecific')]//following-sibling::a";
		public static final String DomainTypeGenericSubLst="@xpath=//ul[contains(@id,'domainOrderTypeCountrySpecific')]";
		public static final String TopLevelDomainLst="@xpath=//div[contains(@id,'topLevelDomainGenericPickList')]//following-sibling::a";
		public static final String TopLevelDomainSubLst="@xpath=descendant::ul[contains(@id,'topLevelDomainGenericPickList')]";
		public static final String IpDomainNameTxb="@xpath=//input[contains(@id,'domainName')]";
		public static final String IpGuardianBandwidthLst="@xpath=//select[@name='serviceBandwidthIpGuardian']";
		public static final String CustomerTypeLst="@xpath=//div[contains(@id,'customerType')]//following-sibling::a";
		public static final String IpGuardianCountryLst="@xpath=//div[contains(@id,'countryIPG')]//following-sibling::a";
		public static final String IpGuardianServiceReferenceTxb="@xpath=//input[contains(@id,'iPAccessServiceReferenceipguardian')]";
		public static final String MvfCountryLst="@xpath=//div[contains(@id,'countryIPMVF')]//following-sibling::a";
		public static final String MvfBandwidthLst="@xpath=//select[@name='serviceBandwidthIPMVF']";
		public static final String MvfServiceReferenceTxb="@xpath=//input[contains(@id,'iPAccessServiceReferenceIPMVF')]";
		public static final String MdfCountryLst="@xpath=//div[contains(@id,'managedDedicatedFirewallCountryAEnd')]//following-sibling::a";
		public static final String MdfBandwidthLst="@xpath=//select[@name='serviceBandwidthIPMDF']";
		public static final String MdfServiceReferenceTxb="@xpath=//input[contains(@id,'iPAccessServiceReferenceIPMDF')]";
		public static final String IpAddonsNewSelectBtn="@xpath=//button[text()='SELECT']";
	}
	
	public static class MultiQuote
	{
		public static final String quoteCurrencyDBtn="@xpath=//bdi[text()='Quote Currency']//parent::span//following-sibling::div//input";
		public static final String quoteCountry="@xpath=//bdi[text()='Colt Organisation Country']//parent::span//following-sibling::div//input";
		public static final String quoteSalesChannel="@xpath=//bdi[text()='SalesChannel']//parent::span//following-sibling::div//input";
		public static final String legalComplexityDBtn="@xpath=//bdi[text()='Legal Complexity']//parent::span//following-sibling::div//input";
		public static final String techicalComplexityDBtn="@xpath=//bdi[text()='Technical Complexity']//parent::span//following-sibling::div//input";
		public static final String proceedBtn="@xpath=//bdi[text()='Proceed']/parent::*/parent::*/parent::*";
		public static final String quoteTypeBtn="@xpath=//div[contains(@id,'ojChoiceId_quoteType_t')]//a";
		public static final String multiTypeBtn="@xpath=//li//div[text()='Multi-quote']";
		public static final String multiQuoteCbx="@xpath=//input[@value='Multi-Quote']//parent::span";
		public static final String downloadQuote="@xpath=//span[text()='Download Quote']";
		public static final String downloadQuoteExternal="@xpath=//span[text()='Download Quote Excel-External']";
		public static final String procedToQuoteBtn="@xpath=//span[text()='Proceed to Quote']";
		public static final String accountDetailsLnk="@xpath=//label[text()='Account Details']";
		public static final String ocnTxt="@xpath=//input[@id='oCN_t|input']";
		public static final String ocnReadTxt="@xpath=//input[@id='oCN_t' and @readonly='readonly']";
		public static final String priceSegmentDrp="@xpath=//div[@id='oj-select-choice-pricingSegment_t']";
		public static final String cancelConfiguration="@xpath=//td[@id='cancel_configuration']";
	}
	
	public static class discountingQuotePage
    {
        public static final String quoteLnk="@xpath=//span[text()='Quote']";
        public static final String basePriceNRCDiscountTxb="@xpath=//input[@id='discountNRCPerc_t|input']";
        public static final String basePriceMRCDiscountTxb="@xpath=//input[@id='discountMRCPerc_t|input']";
        public static final String calculateDiscountBtn="@xpath=descendant::span[text()='Calculate Discount'][1]";

    }
	
	public static class PLTabInfo
	{
		public static final String plLnk="@xpath=descendant::span[text()='Quote']//parent::a//parent::li//following-sibling::li//a[1]";
		public static final String dealBackgroundTxb="@xpath=//textarea[@id='dealBackgroundTextArea_t|input']";
		public static final String competitorsTxb="@xpath=//textarea[@id='competitorsTextArea_t|input']";
		public static final String technicalSolutionsTxb="@xpath=//textarea[@id='technicalSolutionTextArea_t|input']";
		public static final String acvValueElem="@xpath=//input[contains(@id,'aCVAnnualContract')][1]";
		public static final String tcvValueElem="@xpath=//input[contains(@id,'tCVTotalContractValue')][1]";
		public static final String dealPriceReviewLbl="@xpath=//label[@id='summaryLabel_t-label']";
		public static final String dealPriceTeamLbl="@xpath=//label[@id='dealPricingTeamMembers_t-label|label']";
		public static final String DPUserLst="@xpath=//*[@id='oj-select-choice-dealPricingTeamMembers_t']/a";
		public static final String DPUserSubLst="@xpath=//ul[contains(@id,'dealPricingTeamMembers')]";
		public static final String DPUserTxb="@xpath=//*[@id='oj-listbox-drop']/div/div/input";
		public static final String assignQuote="@xpath=//oj-button[contains(@class,'assignQuote')]";
		public static final String releaseQuote="@xpath=//oj-button[contains(@class,'release')]";
		public static final String pricePositioning="@xpath=//textarea[@id='pricePositioningTextArea_t|input']";
		public static final String commercialRisk="@xpath=//textarea[@id='commercialRiskTextArea_t|input']";
	}
	
	public static class CopyQuote
	{
		
		public static final String copyquoteBtn="@xpath=//span[text()='Copy Quote']";
		public static final String copyLineItemsBtn="@xpath=//span[text()='Copy Line Items']//parent::span";
		public static final String numberOfCopiesTxb="@xpath=//input[contains(@aria-label,'Number Of Copies')]";
		public static final String copyPromptOKBtn="@xpath=//oj-button[contains(@on-oj-action,'confirmClicked')]//span[text()='OK']";
        public static final String updateConfigBtn="@xpath=//oj-button[@name='update_configuration']//span[text()='Update Configuration']";
		public static final String copyQuotePopup="@xpath=//div[contains(@class,'popup_on_various_updates')]";
		public static final String selectAll="@xpath=//div[contains(@class,'cpq-table-header-row')]/div[contains(@class,'cpq-table-select')]";
		public static final String copyOKBtn="@xpath=//oj-button[@name='ok']";
		
		public static final String optionQuoteRBtn="@xpath=//div[@aria-labelledby='optionsQuote_t-label']";
		public static final String projectQuoteRBtn="@xpath=//div[@aria-labelledby='projectQuote_t-label']";
		
		public static final String adminBtn="@xpath=//img[@title='Admin']";
		public static final String internalUsersLnk="@xpath=//a[text()='Internal Users']";
		public static final String orderToQuoteManagerLnk="@xpath=//a[contains(text(),'Manager')]";
		
		public static final String proxyLogoutBtn="@xpath=//img[@title='Proxy Logout']";
		public static final String transactionsLnk="@xpath=//a[contains(text(),'Transaction')]";
		public static final String copyTransactionsLnk="@xpath=descendant::a[text()='Copy'][1]";
		
		public static final String quotesLnk="@xpath=//div[text()='Quotes']";
		public static final String addQuoteBtn="@xpath=//bdi[text()='Add']//parent::span//parent::span//parent::button";
		public static final String actionBtn="@xpath=//bdi[contains(text(),'Action')]";
		
		public static final String copyQuoteC4CBtn="@xpath=//bdi[text()='Copy']//parent::span";
		public static final String proceedC4CBtn="@xpath=//bdi[text()='Proceed']//parent::span";
											      
		public static final String contactInformationLnk="@xpath=//span[text()='Contact Information']";
		public static final String additonalInfoTable="@xpath=//oj-table[contains(@id,'additionalQuoteInformation')]";
		
		public static final String serviceOrderNotesTxb="@xpath=//textarea[@id='serviceOrderNote_t']";
		public static final String serviceOrdersubmitBtn="@xpath=(//span[text()='Submit'])[3]";
		
		public static final String cpqSummaryErrorMsgElem="@xpath=//div[contains(@class,'message-summary message-error')]";
		public static final String cpqErrorMsgElem="@xpath=//div[contains(@class,'message-detail')]";
		
		public static final String copyContactDetailsRadiobtn="@xpath=//input[@value='copyContactDetailsFromLegalOrTechnicalOrdeingContact']//parent::span";
		public static final String copyOrderingDetailsRadiobtn="@xpath=//input[@value='Ordering Contact']";
		public static final String siteContactRadiobtn="@xpath=//input[@value='siteContactAEnd']";
		public static final String technicalContactAEndRadiobtn="@xpath=//input[@value='technicalContactAEnd']";
		public static final String electricianContactAEndRadiobtn="@xpath=//input[@value='electricianContactAEnd']";
		public static final String siteContactBEndRadiobtn="@xpath=//input[@value='siteContactBEnd']";
		public static final String technicalContactBEndRadiobtn="@xpath=//input[@value='technicalContactBEnd']";
		public static final String electricianContactBEndRadiobtn="@xpath=//input[@value='electricianContactBEnd']";
		public static final String copyContactBtn="@xpath=//span[text()='Copy Contact']";
		public static final String selectQuote="@xpath=//input[@id='selectJET-1']";
		public static final String orderDetailsCbx="@xpath=//input[@value='Order' and @type='checkbox']//parent::span";
		public static final String offnetcopyQuoteOKBtn="@xpath=//button[@name='ok']";	
}

		public static class WholeSalesTeam
		{
			public static final String supportLnk="@xpath=//span[text()='Support']//parent::a";
			public static final String wholeSaleSupportTeamLst="@xpath=//div[contains(@id,'wholesaleSupportTeamMember')]//following-sibling::a";
			public static final String wholeSaleSupportTeamSubLst="@xpath=//ul[contains(@id,'wholesaleSupportTeamMember')]";
			public static final String wholeSaleAssignedDate="@xpath=//input[contains(@id,'wholesaleuserAssignedDate')]";
			public static final String wholeSaledatePicker="@xpath=//div[contains(@class,'oj-datetimepicker-switcher-buttons')]//a[text()='Done']";
			public static final String wholeSaleEmailSubjectTxb="@xpath=//input[contains(@id,'wholesaleEmailSubject')]";
			public static final String wholeSaletoggleButton="@xpath=//oj-switch[contains(@aria-labelledby,'disableQuoteCommercialReview')]";
			public static final String wholeSalepricingTxb="@xpath=//input[contains(@id,'pricingSource')]";
			public static final String wholesaleC4CQuoteViewTxt="@xpath=//bdi[text()='Wholesale Support Member']/parent::span/following-sibling::div//span";
			public static final String wholesaleteamNumberTxt="@xpath=//div[contains(@id,'wholesaleSupportTeamMembe')]//span";
		}
		
		public static class ServiceFeatures
		{
		//IP features
		public static final String servicefeatures="@xpath=//a[@title='Service Features']";
		public static final String Ipv4Addressingformat="@xpath=//span[text()='IPv4 format only']";
		public static final String Ipv4Addressingtype="@xpath=//div[contains(@id,'iPv4AddressingTypePrimary')]//span[text()='Provider Aggregated IP (PA)']";
		public static final String Ipv4NumberofPAaddress="@xpath=//div[contains(@id,'numberOfIPv4AddressesPrimary')]//span[text()='/29 (8)']";
		public static final String Ipv4Addressingdropdown="@xpath=//div[contains(@id,'iPAddressingFormatPrimary')]//following-sibling::a";
		public static final String Ipv6Addressingformat="@xpath=//span[text()='IPv4 format only']";
		public static final String Ipv6Addressingtype="@xpath=//div[contains(@id,'iPv6AddressingTypePrimary')]//span[text()='Provider Aggregated IP (PA)']";
		public static final String Ipv6NumberofPAaddress="@xpath=//div[contains(@id,'numberOfIPv6AddressesPrimary')]//span[text()='/56']";
		public static final String performanceReporting="@xpath=//oj-switch[@id='performanceReportingServiceLvl']";
		public static final String proactiveManagement="@xpath=//oj-switch[@id='proActiveManagementServiceLvl']";
		public static final String voiceBundleService="@xpath=//oj-switch[@id='voiceBundledServiceLvl']";
		public static final String voipRefNumService="@xpath=//input[@id='vOIPRefNumServiceLvl|input']";
		}
				
		public static class SiteNameAlias
		{
			public static final String ServiceFeaturesTab="@xpath=//h3[contains(text(),'Service Features')]";
			public static final String PerformanceRepflag="@xpath=//oj-switch[@id='performanceReportingAddon']";
			public static final String AEndSiteNameAlias="@xpath=//input[@id='siteNameAlias|input']";
			public static final String BEndSiteNameAlias="@xpath=//input[@id='siteNameAliasBEnd|input']";
			public static final String PerformanceRepflagWave="@xpath=//oj-switch[@id='performanceReportingWave']";			
			
		}
			
		public static class Bespoke
		{
			public static final String BespokeTab="@xpath=//h3[contains(text(),'Bespoke Features')]";
			public static final String Bespokeflag="@xpath=//oj-switch[@id='bespokeSolution']";
			public static final String Bespoketype="@xpath=//div[@id='oj-select-choice-bespokeType']";
			public static final String SolutionRefId="@xpath=//input[@id='solutionReferenceID|input']";
			public static final String CommentsfromSales="@xpath=//textarea[@id='commentsFromSales_bespoke|input']";
			
			public static final String IpBespokeTab="@xpath=//h3[contains(text(),'Bespoke')]";
			public static final String IpBespokeflag="@xpath=//oj-switch[@id='bespokeSolution']";
			public static final String IpBespoketype="@xpath=//div[@id='oj-select-choice-bespokeType']";
			public static final String IpSolutionRefId="@xpath=//input[@id='solutionReferenceID|input']";
			public static final String IpCommentsfromSales="@xpath=//textarea[@id='commentsFromSales_bespoke|input']";
			
			public static final String WaveBespokeTab="@xpath=//h3[contains(text(),'BESPOKE')]";
			public static final String WaveBespokeflag="@xpath=//oj-switch[@id='bespokeSolution']";
			public static final String WaveBespoketype="@xpath=//span[@id='ojChoiceId_bespokeType_selected']";
			public static final String WaveSolutionRefId="@xpath=//input[@id='solutionReferenceID|input']";
			public static final String WaveCommentsfromSales="@xpath=//textarea[@id='commentsFromSales_bespoke|input']";
		}	
		public static class Diversity
        {
            public static final String DiversityLnk="@xpath=//*[contains(text(),'DIVERSITY')]";
            public static final String Diversityflag="@xpath=//oj-switch[@id='diversityFlag_pf']";
            public static final String DiversefromService="@xpath=//span[@id='ojChoiceId_diverseFromService_pf_selected']";
            public static final String DiversityReference="@xpath=//input[@id='diverseFromServiceReferenceExt_pf|input']";
            public static final String ValidateReference="@xpath=//oj-option[@id='diversityReferenceValidate0']";
            public static final String Diverseoption="@xpath=//span[@id='ojChoiceId_diversityOption_pf_selected']";
            public static final String DiversityIDSelectbutton="@xpath=//input[@id='selectLineItemForDiversity_pf-0true|cb']";
            public static final String ReferencePopUpOKBtn="@xpath=//oj-dialog//div[@class='oj-button-label']";
            
            public static final String IpDiversityLnk="@xpath=//h3[contains(text(),'Diversity')]";
            public static final String IpDiversityflag="@xpath=//oj-switch[@id='diversityServiceLvl']";
            public static final String IpDiversefromService="@xpath=//div[@id='oj-select-choice-diverseFromServiceServiceLvl']";
            public static final String IpDiversityReference="@xpath=//input[@id='diverseFromServiceRefExtServiceLvl|input']";
           
        }
				
		public static class NetworkEncryption 
		{
			
			public static final String NetworkEncryptionflag="@xpath=//oj-switch[@id='networkEncryptionAddon']";
			public static final String SelectEncryptionKey="@xpath=//div[@id='oj-select-choice-aEndEncryptionKeyManagement']";
			public static final String AEndEncryptionEquipmentLocation="@xpath=//span[@id='ojChoiceId_aEndEncryptionEquipmentLocation_selected']";
			public static final String BEndEncryptionEquipmentLocation="@xpath=//span[@id='ojChoiceId_bEndEncryptionEquipmentLocation_selected']";			
		
		}
		
		public static class SEEngagement
        {
          
            public static final String SEEngagementTab="@xpath=//span[text()='SE Engagement']//parent::a";
            public static final String ViewReasonBtn="@xpath=//*[@name='view_/_update_reason_details']";
            public static final String ViewReasontable="@xpath=//div[@id='sEEngagementReasonsDetails_t-arraySet-table-wrapper']";
            //public static final String PSEngagementTab="@xpath=//*[contains(text(),'PS Engagement')]";  
            public static final String PSEngagementTab="@xpath=//*[contains(text(),'Engagement')]";
            public static final String PSEngagementEditBtn="@xpath=//button[@title='Edit']";  
            public static final String SERequriedflag="@xpath=//div[@title='Request SE']//div[contains(@class,'sapMSwt sapMSwtDefault')]";  
            public static final String SEEngagementdetail="@xpath=//span[contains(@title,'SE Engagement')]//following-sibling::div//input[contains(@class,'apMComboBoxInner')]";  
            public static final String Savebtn="@xpath=//bdi[text()='Save']";  
            public static final String Quotetab="@xpath=//div[@id='navigationitemNAVIGATIONITEMID_2763f338610d4674b802562f110d0bb5_600-text']";  
            public static final String EditBtn="@xpath=//span[@id='navigationitemp8ddFON7LaopZPHmYki9zW_1967-content']";
            public static final String technicalfeacompleteBtn="@xpath=//*[@name='technical_feasibility_complete']";
            public static final String CustomerDefinedRoutetitle="@xpath=//h3[contains(text(),'CUSTOMER DEFINED ROUTE')]";
    		public static final String CustomerDefinedRouteflag="@xpath=//oj-switch[@id='customerDefinedRoute_boolean']";
    		public static final String AdditionalcostsforBespoke="@xpath=//span[@id='ojChoiceId_additionalCostsForBespoke_selected']";  
    		public static final String NotifySEReviewDropdown="@xpath=//span[@id='ojChoiceId_seReviewSelection_t_selected']";
    		public static final String ReasonDropdown="@xpath=//span[@id='ojChoiceId_reasonForSEReview_t_selected']";
    		public static final String ConfirmSelectionBtn="@xpath=//*[@name='confirm_selection']";
    		
    		public static final String BespokeCustViewDescription="@xpath=//textarea[@id='bespokeDescription_besoke|input']";
    		public static final String BespokeColttViewNote="@xpath=//textarea[@id='notesProductDescription|input']";
      
        }
		
		public static class ManualRequestStatus 
		{
			
			public static final String ManualRequestTab_CommPge="@xpath=//span[text()='View Manual Request']";
			public static final String UpdateExpDetailsBtn="@xpath=//span[text()='Update Explore Details']";
			public static final String ManualRequestTab_ConfPge="@xpath=//a[@title='View Manual Request']";
			public static final String DSLAEndCheck="@xpath=//oj-option[@id='offNetDSLCCRequestAEnd3']//span[text()='DSL CHECK']";
			public static final String DSLBEndCheck="@xpath=//oj-option[@id='offNetDSLCCRequestBEnd3']//span[text()='DSL CHECK']";
			public static final String ManualRequestTbl_ConfPge="@xpath=//div[@id='responseTableExplore']//table[@class='general_table']";
			public static final String ManualRequestTbl_CommPge="@xpath=//div[contains(@id,'ExploreResponseFromAllProducts')]//table[@class='general_table']";
		
		}
		
		public static class ViewPrices
		{
		public static final String WarningMessage="@xpath=//span[@class='oj-message-content']";
		public static final String ViewPriceTab="@xpath=//oj-buttonset-many[@id='toggle-rightsidebar-btn']";
		public static final String PricingSegment="@xpath=//*[contains(text(),'(Pricing Segment = CTLP-Google)')]";
		public static final String viewAllContractualPricesBtn="@xpath=//button[@id='viewAllContractualPrices']";
		public static final String pricesPopup="@xpath=//h1[text()='Please select appropriate customer specific contractual price']";
		public static final String PricingCriteria="@xpath=//div[@class='oj-dialog-body dialog-content']//tr[2]//td[2]";
		public static final String NRCValues="@xpath=//div[@class='oj-dialog-body dialog-content']//tr[2]//td[13]";
		//public static final String NRCValues="@xpath=//td[contains(text(),'value')]";
		public static final String MRCValues="@xpath=//div[@class='oj-dialog-body dialog-content']//tr[2]//td[14]";
		public static final String CancelButton="@xpath=//button[@id='cancelContractualPrices']";
		public static final String ClosePopup="@xpath=//div[@id='right-sidebar']//oj-button[@title='Close']";
		public static final String EngagePortfolioPricingbtn="@xpath=//oj-button[@name='engage_portfolio_pricing']";
		public static final String PriceBookValueGoogle="@xpath=//div//span[@title='CTLP-Google']";
		public static final String SelectPricingCriteria1="@xpath=//input[@id='1xCircuit1']";
		public static final String SelectPricingCriteria2="@xpath=//input[@id='2xCircuit2']";

		public static final String Bandwidth="@xpath=//div//span[@title='10 Gbps']";
		public static final String Bandwidth1="@xpath=//div//span[@title='5 Gbps']";
		public static final String BandwidthDropdown="@xpath=//div[@id='oj-select-choice-bandwidthSL_l-2']";
		public static final String BandwidthDropdownValue="@xpath=//div[@aria-label='5 Gbps']";
		public static final String SaveButton="@xpath=//div[@class='oj-button-label']//span[text()='Save']";
		public static final String ValidateErrorMsg="@xpath=//div[text()='Validation errors exist on the following 1 attribute(s): Quote Line Item Id']";
		public static final String BandwidthDropdownValuePre="@xpath=//div[@aria-label='10 Gbps']";
		//Fidessa Segment
		public static final String PricingSegment_Fidessa="@xpath=//*[contains(text(),'(Pricing Segment = CTLP-Fidessa)')]";
		public static final String PriceBookValueFidessa="@xpath=//div//span[@title='CTLP-Fidessa']";
		public static final String ViewPriceNRCValues="@xpath=//div[@id='mySiteTable']//tr[2]/td[2]";
		public static final String ViewPriceMRCValues="@xpath=//div[@id='mySiteTable']//tr[2]/td[3]";
		
		}
		
}