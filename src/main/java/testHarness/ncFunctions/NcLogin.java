package testHarness.ncFunctions;

import java.io.IOException;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CLoginObj;
import pageObjects.ncObjects.NcLoginObj;

public class NcLogin extends SeleniumUtils {
	
	
	
	public void Login(String username, String password) throws InterruptedException, IOException
	{
//		String username = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"uname");
//		String password = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"pwd");
		
		webDriver.get(Configuration.NC_URL);
		verifyExists(NcLoginObj.Login.Username,"Username textbox");
		sendKeys(NcLoginObj.Login.Username,username,"Username textbox");
	
		verifyExists(NcLoginObj.Login.Password,"Password textbox");
		sendKeys(NcLoginObj.Login.Password,password,"Password textbox");
		
		verifyExists(NcLoginObj.Login.Loginbutton,"Login Button");
		click(NcLoginObj.Login.Loginbutton,"Login Button");
		waitForAjax();
	
	}
	public void VerifySuccessLogin() throws InterruptedException, IOException
	{
	 //need to check 
	//Assert.assertEquals(Getkeyvalue(application+"_LoggedinUser").contains(Gettext(getwebelement(xml.getlocator("//locators/"+application+"/Userinfo")))),true);

		verifyExists(NcLoginObj.Login.Userinfo,"User Info on Landing Page");     
		
	}
	public void SkipWarning() throws InterruptedException, IOException
	{
		click(NcLoginObj.Login.skipwarning,"Click on continue to skip the timezone warning");
		
	}

}
