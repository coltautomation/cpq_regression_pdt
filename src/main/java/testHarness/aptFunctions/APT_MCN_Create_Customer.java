package testHarness.aptFunctions;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;
import pageObjects.aptObjects.APT_MCN_Create_CustomerObj;

public class APT_MCN_Create_Customer extends SeleniumUtils {

	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();

	public void navigateToManageCustomerServicePage() throws InterruptedException, IOException {
		//mouseMoveOn(APT_MCN_Create_CustomerObj.CreateCustomer.ManageCustomerServiceLink);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.ManageCustomerServiceLink,"Manage Customer");
		waitForAjax();
	}

	public void navigateToCreateCustomerPage() throws InterruptedException, IOException {
		navigateToManageCustomerServicePage();
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.CreateCustomerLink, "Create Customer");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.CreateCustomerLink, "Create Customer");

	}

	public void createCustomerFunctionality(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String MainDomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String CountryToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CountryToBeSelected");
		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String TechnicalContactName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechnicalContactName");
		String TypeToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TypeToBeSelected");
		String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String Phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String Fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");
		String enableDedicatedPortal = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"enableDedicatedPortal");
		String DedicatedPortalValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DedicatedPortal");

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_CreateCustomer);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_CreateCustomer, "OK Button");
		scrollUp();

		// Warning Messages
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.name_validationmsg, "Legal Customer Name");
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.country_validationmsg, "Country");
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.OCN_validationmsg, "OCN");
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.type_validationmsg, "Type");
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.email_Validationmsg, "Email");

		// Name
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Name, "Legal Customer Name");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Name, Name);
		// addtextFields_commonMethod(application, "Legal Customer Name",
		// "Name", Name, xml);

		// Main Domain
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.MainDomain, "Main Domain");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.MainDomain, MainDomain);
		// addtextFields_commonMethod(application, "Main Domain", "MainDomain",
		// MainDomain, xml);

		// Country dropdown
		//selectByValue(APT_MCN_Create_CustomerObj.CreateCustomer.CountrySelect, CountryToBeSelected, "Country");
		addDropdownValues_commonMethod("Country",APT_MCN_Create_CustomerObj.CreateCustomer.CountrySelect, CountryToBeSelected);

		// OCN
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.OCN, "OCN");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.OCN, OCN);
		// addtextFields_commonMethod(application, "OCN", "OCN", OCN, xml);

		// Reference
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Referance_Build4, "reference");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Referance_Build4, Reference);
		// addtextFields_commonMethod(application, "reference",
		// "Referance_Build4", Reference, xml);

		// technical Contact name
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.TechinicalContactName_Build4, "Technical Contact Name");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.TechinicalContactName_Build4, TechnicalContactName);
		// addtextFields_commonMethod(application, "Technical Contact Name",
		// "TechinicalContactName_Build4", TechnicalContactName, xml);

		// Type dropdown
		//selectByValue(APT_MCN_Create_CustomerObj.CreateCustomer.TypeSelect_Build4, TypeToBeSelected, "Type");
		 addDropdownValues_commonMethod("Type",APT_MCN_Create_CustomerObj.CreateCustomer.TypeSelect_Build4, TypeToBeSelected);

		// Email
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Email, "Email");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Email, Email);
		// addtextFields_commonMethod(application, "Email", "Email", Email,
		// xml);

		// Phone
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Phone, "Phone");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Phone, Phone);
		// addtextFields_commonMethod(application, "Phone", "Phone", Phone,
		// xml);

		// FAX
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Fax, "Fax");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Fax, Fax);
		// addtextFields_commonMethod(application, "Fax", "Fax", Fax, xml);
		
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_CreateCustomer);
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_CreateCustomer,"OK Button");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_CreateCustomer,"OK Button");
		waitForAjax();
		// click_commonMethod(application, "OK", "OkButton_CreateCustomer",
		// xml);
	}

	public void verifyCustomerdetails_InviewCustomerPage(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String MainDomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String CountryToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CountryToBeSelected");
		String TypeToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TypeToBeSelected");
		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String TechnicalContactName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechnicalContactName");
		String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String Phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String Fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		compareText_InViewPage("Legal Customer Name", Name);

		if (MainDomain.equalsIgnoreCase("null")) {
		} else {
			compareText_InViewPage("Main Domain", MainDomain);
		}
		compareText_InViewPage("Country", CountryToBeSelected);
		compareText_InViewPage("Type", TypeToBeSelected);
		compareText_InViewPage("OCN", OCN);
		compareText_InViewPage("Reference", Reference);
		compareText_InViewPage("Technical Contact Name", TechnicalContactName);
		compareText_InViewPage("Email", Email);
		compareText_InViewPage("Phone", Phone);
		compareText_InViewPage("Fax", Fax);

	}

	public void verifyEditCustomerFunction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String MainDomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String CountryToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CountryToBeSelected");
		String Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String TechnicalContactName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechnicalContactName");
		String TypeToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TypeToBeSelected");
		String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String Phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String Fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");
		String enableDedicatedPortal = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"enableDedicatedPortal");
		String DedicatedPortalValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DedicatedPortalValue");

		click(APT_MCN_Create_CustomerObj.CreateCustomer.CustomerDetails_Action, "Action");
		waitForAjax();
		// click_commonMethod(application, "Action", "CustomerDetails_Action",
		// xml);
		// Thread.sleep(1000);

		click(APT_MCN_Create_CustomerObj.CreateCustomer.EditCustomerLink, "Edit");
		// click_commonMethod(application, "Edit", "EditCustomerLink", xml);

		edittextFields_commonMethod("Legal Customer Name", APT_MCN_Create_CustomerObj.CreateCustomer.Name_EditCustomer,
				Name); // name field

		edittextFields_commonMethod("Main Domain", APT_MCN_Create_CustomerObj.CreateCustomer.domain_EditCustomer,
				MainDomain); // main Domain field

		//selectByValue(APT_MCN_Create_CustomerObj.CreateCustomer.CountrySelect, CountryToBeSelected, "Country");
		addDropdownValues_commonMethod("Country", APT_MCN_Create_CustomerObj.CreateCustomer.CountrySelect,CountryToBeSelected); //Country dropdown

		edittextFields_commonMethod("Reference", APT_MCN_Create_CustomerObj.CreateCustomer.reference_EditCustomer,
				Reference); // reference field

		edittextFields_commonMethod("Technical Contact Name",
				APT_MCN_Create_CustomerObj.CreateCustomer.Technicalname_EditCustomer, TechnicalContactName); // technical
																												// contact
																												// name
																												// field

		//selectByValue(APT_MCN_Create_CustomerObj.CreateCustomer.TypeSelect_Build4, TypeToBeSelected, "Type");
		addDropdownValues_commonMethod("Type", APT_MCN_Create_CustomerObj.CreateCustomer.TypeSelect_Build4,TypeToBeSelected); //Type dropdown

		edittextFields_commonMethod("Email", APT_MCN_Create_CustomerObj.CreateCustomer.Email_EditCustomer, Email); // Email
																													// field

		edittextFields_commonMethod("Phone", APT_MCN_Create_CustomerObj.CreateCustomer.phone_EditCustomer, Phone); // Phone
																													// field

		edittextFields_commonMethod("Fax", APT_MCN_Create_CustomerObj.CreateCustomer.fax_EditCustomer, Fax);

		// Enable dedicated portal checkbox
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.EnableDedicatedPortalCheckbox,
				"Enable Dedicated Portal", enableDedicatedPortal);


		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_EditCustomer);
		waitForAjax();
		// Thread.sleep(1000);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_EditCustomer, "Ok Button");
		// click_commonMethod(application, "OK", "OkButton_EditCustomer" , xml);

	}

	public void verifyCustomerdetails_InviewCustomerPage_edited(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String MainDomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String CountryToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CountryToBeSelected");
		String Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String TechnicalContactName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechnicalContactName");
		String TypeToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TypeToBeSelected");
		String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String Phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String Fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");

		// Name
		if (Name.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "Customer Name is not edited", "PASS");
		} else {
			compareText_InViewPage("Legal Customer Name", Name);
		}

		// Main Domain
		if (MainDomain.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "Main Domain' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Main Domain", MainDomain);
		}

		// Country
		if (CountryToBeSelected.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'Country' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Country", CountryToBeSelected);
		}

		// Type
		if (TypeToBeSelected.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'Type' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Type", TypeToBeSelected);
		}

		// OCN
		if (OCN.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'OCN' field is not edited", "PASS");
		} else {
			compareText_InViewPage("OCN", OCN);
		}

		// Reference
		if (Reference.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'Reference' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Reference", Reference);
		}

		// Technical Contact Name
		if (TechnicalContactName.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'Technical Contact Name' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Technical Contact Name", TechnicalContactName);
		}

		// Email
		if (Email.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'Email' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Email", Email);
		}

		// Phone
		if (Phone.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'Phone' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Phone", Phone);
		}

		// OCN
		if (Fax.equalsIgnoreCase("null")) {
			Report.LogInfo("INFO", "'Fax' field is not edited", "PASS");
		} else {
			compareText_InViewPage("Fax", Fax);
		}
	}

	public void VerifyAddUserFunctionality(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String RolesToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RolesToBeSelected");
		String HideRouterToolsIPv4CommandsCisco_ToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName,
				scriptNo, dataSetNo, "HideRouterToolsIPv4CommandsCisco_ToBeSelected");
		String HideRouterToolsIPv4CommandsHuiwai_ToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName,
				scriptNo, dataSetNo, "HideRouterToolsIPv4CommandsHuiwai_ToBeSelected");
		String HideServicesToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"HideServicesToBeSelected");
		String HideSiteOrderToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"HideSiteOrderToBeSelected");
		String HideRouterToolsIPv6CommandsCisco_ToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName,
				scriptNo, dataSetNo, "HideRouterToolsIPv6CommandsCisco_ToBeSelected");
		String UserName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserName");
		String FirstName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "FirstName");
		String SurName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SurName");
		String PostalAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PostalAddress");
		String Email_AddUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email_AddUser");
		String Phone_AddUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone_AddUser");
		String IPGuardianAccountGroup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"IPGuardianAccountGroup");
		String ColtOnlineUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ColtOnlineUser");
		String GeneratePassword = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"GeneratePassword");

		// Thread.sleep(1000);

		String[] rolestobeSelectedList = RolesToBeSelected.split(",");
		String[] routerToolIPv4CiscoTobeSelectedList = HideRouterToolsIPv4CommandsCisco_ToBeSelected.split(",");
		String[] routerToolIPv4HuaweiTobeSelectedList = HideRouterToolsIPv4CommandsHuiwai_ToBeSelected.split(",");
		String[] ServicesTobeSelectedlist = HideServicesToBeSelected.split(",");
		String[] siteOrdersToBeselectedList = HideSiteOrderToBeSelected.split(",");
		String[] routerToolIPv6ToBeSelectedList = HideRouterToolsIPv6CommandsCisco_ToBeSelected.split(",");

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action, "User action");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.AddUserLink, "Add User");
		waitForAjax();

		scrollUp();
		waitForAjax();
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.UserName, "User Name");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.UserName, UserName);
		// addtextFields_commonMethod(application, "User Name", "UserName",
		// UserName, xml); // User
		// Name
		// Field
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.FirstName, "First Name");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.FirstName, FirstName);
		// addtextFields_commonMethod(application, "First Name", "FirstName",
		// FirstName, xml); // First
		// Name
		// Field

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SurName, "Sur Name");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.SurName, SurName);
		// addtextFields_commonMethod(application, "Sur Name", "SurName",
		// SurName, xml); // Sur
		// Name
		// Field
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.PostalAddress, "Postal Address");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.PostalAddress, PostalAddress);
		// addtextFields_commonMethod(application, "Postal Address",
		// "PostalAddress", PostalAddress, xml); // Postal
		// Address
		// Field
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Email, "Email_AddUser");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Email, Email_AddUser);
		// addtextFields_commonMethod(application, "Email", "Email",
		// Email_AddUser, xml); // Email
		// field

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Phone, "Phone AddUser");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Phone, Phone_AddUser);
		// addtextFields_commonMethod(application, "Phone", "Phone",
		// Phone_AddUser, xml); // Phone
		// Field
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.IPGuardianAccountGroup, "IP Guardian Account Group");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.IPGuardianAccountGroup, IPGuardianAccountGroup);
		// addtextFields_commonMethod(application, "IPGuardian Account Group",
		// "IPGuardianAccountGroup",
		// IPGuardianAccountGroup, xml); // IPGuardian Account Group Field

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.ColtOnlineUser, "Colt On line User");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.ColtOnlineUser, ColtOnlineUser);
		// addtextFields_commonMethod(application, "Colt Online User",
		// "ColtOnlineUser", ColtOnlineUser, xml); // Colt
		// online
		// User

		click(APT_MCN_Create_CustomerObj.CreateCustomer.GeneratePasswordLink, "Generate Password");
		// click_commonMethod(application, "Generate Password",
		// "GeneratePasswordLink", xml); // Generate
		// Password
		// Link

		/*
		 * String password = getwebelement(xml.getlocator("//locators/" +
		 * application + "/Password_Textfield")) .getAttribute("value");
		 * Log.info("Generated Password is : " + password);
		 */
		String password = getAttributeFrom(APT_MCN_Create_CustomerObj.CreateCustomer.Password_Textfield, "value");

		if (password.isEmpty()) {
			Report.LogInfo("INFO",
					"Password Field is empty. No values displaying after clicked on 'Generate password link", "PASS");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Password_Textfield, GeneratePassword);
			waitForAjax();
			Report.LogInfo("INFO", "Password entered manually not automatically generated :  " + GeneratePassword,
					"PASS");
		} else {
			Report.LogInfo("INFO", "Password generated and the value is displaying as :  " + password, "PASS");
		}

		// WebElement Email = getwebelement(xml.getlocator("//locators/" +
		// application + "/EmailLabelname"));
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.EmailLabelname);

		// Role
		selectAndAddValueFromLeftDropdown("Role", APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_available,
				rolestobeSelectedList, APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_addButton);
		verifySelectedValuesInsideRightDropdown("Role",
				APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_selectedValues);

		// Hide Service
		selectAndAddValueFromLeftDropdown("Hide Service",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideService_Available, ServicesTobeSelectedlist,
				APT_MCN_Create_CustomerObj.CreateCustomer.HideService_addButton);
		verifySelectedValuesInsideRightDropdown("Hide Services",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideServicesDropdown_selectedValues);

		// Hide Site Order
		selectAndAddValueFromLeftDropdown("Hide Site Order",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideSiteOrder_Available, siteOrdersToBeselectedList,
				"hideSiteOrder_addbutton");
		verifySelectedValuesInsideRightDropdown("Hide Site Order",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideSiteOrderDropdown_selectedValues);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Cisco_Available);
		waitForAjax();
		// scrolltoend();
		// Thread.sleep(1000);

		// Hide Router Tool IPv4 Commands(Cisco)
		selectAndAddValueFromLeftDropdown("Hide Router Tool IPv4 Commands(Cisco)",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Cisco_Available,
				routerToolIPv4CiscoTobeSelectedList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Cisco_addButton);
		verifySelectedValuesInsideRightDropdown("Hide Router Tool IPv4 Commands(Cisco)",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIpv4_Cisco_selectedvalues);

		// Hide Router Tool IPv4 Commands(Huawei)
		selectAndAddValueFromLeftDropdown("Hide Router Tool IPv4 Commands(Huawei)",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Huawei_available,
				routerToolIPv4HuaweiTobeSelectedList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4__Huawei_addButton);
		verifySelectedValuesInsideRightDropdown("Hide Router Tool IPv4 Commands(Huawei)",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIpv4_Huawei_selectedvalues);

		// //Hide Router Tool IPv6 Commands(Cisco)
		selectAndRemoveValueFromRightDropdown("Hide Router Tool IPv6 Commands(Cisco)",
				APT_MCN_Create_CustomerObj.CreateCustomer.hidenRouterIPv6_cisco, routerToolIPv6ToBeSelectedList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv6_Cisco_removeButton);
		// selectAndAddValueFromLeftDropdown(application, "Hide Router Tool IPv6
		// Commands(Cisco)" , "HideRouterToolIPv6_Cisco_Available" ,
		// selectValue, xpathForAddButton);
		// verifySelectedValuesInsideRightDropdown(application, "Hide Router
		// Tool IPv6 Commands(Cisco)" , xpath);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_AddUser);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_AddUser, "OK Button");
		// click_commonMethod(application, "OK", "OkButton_AddUser", xml);
	}

	public void verifyAddedUserValuesInViewUserPage(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String UserName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserName");
		String FirstName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "FirstName");
		String SurName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SurName");
		String PostalAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PostalAddress");
		String Email_AddUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email_AddUser");
		String Phone_AddUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone_AddUser");
		String IPGuardianAccountGroup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"IPGuardianAccountGroup");
		String ColtOnlineUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ColtOnlineUser");

		// Thread.sleep(1000);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.legalcustomerName_labelName);
		waitForAjax();
		// WebElement
		// customerName=getwebelement(xml.getlocator("//locators/"+application+"/legalcustomerName_labelName"));
		// scrolltoview(customerName);

		// Thread.sleep(1000);

		// WebElement username = getwebelement(
		// xml.getlocator("//locators/" + application +
		// "/selectUser").replace("value", UserName));
		// WebElement username =
		// webDriver.findElement(By.xpath("//div[div[text()='Users']]//following-sibling::div//div[text()='value']"));
		click(APT_MCN_Create_CustomerObj.CreateCustomer.selectUser1 + UserName
				+ APT_MCN_Create_CustomerObj.CreateCustomer.selectUser2,"Select User");
		// Clickon(username);
		Report.LogInfo("INFO", UserName + " is selected", "PASS");
		waitForAjax();

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action, "User Action");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action, "User Action");
		waitForAjax();

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.ViewuserLink, "View user Link");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.ViewuserLink, "View user Link");
		waitForAjax();

		// User Name
		compareTextForViewUserPage("User Name", UserName);

		// First Name
		compareTextForViewUserPage("First Name", FirstName);

		// Sur Name
		compareTextForViewUserPage("Surname", SurName);

		// Postal Address
		compareTextForViewUserPage("Postal Address", PostalAddress);

		// Email
		compareTextForViewUserPage("Email", Email_AddUser);

		// Phone
		compareTextForViewUserPage("Phone", Phone_AddUser);

		// IP Guardian Accouunt Group
		compareTextForViewUserPage("IPGuardian Account Group", IPGuardianAccountGroup);

		// Colt Online User
		compareTextForViewUserPage("Colt Online User", ColtOnlineUser);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_userNameLabelName);
		waitForAjax();
		/*
		 * WebElement userNameLabelName = getwebelement(
		 * xml.getlocator("//locators/" + application +
		 * "/viewUser_userNameLabelName")); scrolltoview(userNameLabelName);
		 * Thread.sleep(1000);
		 */

		// Roles
		// compareTextForViewUserPage(application, labelname, ExpectedText,
		// xml);

		// Hidden Router Tools IPv4 (Cisco)
		// List<WebElement> HRcisco = getwebelements(
		// xml.getlocator("//locators/" + application +
		// "/viewUser_HiddenRouterToolIPv4Cisco"));
		List<WebElement> HRcisco = findWebElements(
				APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_HiddenRouterToolIPv4Cisco);

		for (WebElement listofHiddenCiscoValues : HRcisco) {
			Report.LogInfo("INFO",
					"list of values in Hide router Tool Command IPv4(Cisco) are: " + listofHiddenCiscoValues.getText(),
					"PASS");
		}

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_HiddenRouterToolCommandIPv4Huawei);
		waitForAjax();
		// Thread.sleep(2000);

		// Hidden Router Tool IPv4 (Huawei)
		List<WebElement> Ipv4CommandHuawei = findWebElements(
				APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_HiddenRouterToolCommandIPv4Huawei);

		// List<WebElement> Ipv4CommandHuawei = getwebelements(
		// xml.getlocator("//locators/" + application +
		// "/viewUser_HiddenRouterToolCommandIPv4Huawei"));

		for (WebElement listofHuaweiValues : Ipv4CommandHuawei) {
			Report.LogInfo("INFO",
					"list of values in Hide router Tool Command (Cisco) are: " + listofHuaweiValues.getText(), "PASS");
		}

		// Hidden Router Tools IPv6 (Cisco)
		List<WebElement> HiddenIPv6cisco = findWebElements(
				APT_MCN_Create_CustomerObj.CreateCustomer.viewUser_HiddenRouterToolCommandIPv6Cisco);

		// List<WebElement> HiddenIPv6cisco = getwebelements(
		// xml.getlocator("//locators/" + application +
		// "/viewUser_HiddenRouterToolCommandIPv6Cisco"));

		for (WebElement listofHiddenIPv6CiscoValues : HiddenIPv6cisco) {
			Report.LogInfo("INFO",
					"List of Hidden Router Tool IPv6 Commands(Cisco) are: " + listofHiddenIPv6CiscoValues.getText(),
					"PASS");
		}

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.backbutton);
		waitForAjax();
		// Thread.sleep(2000);

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.backbutton, "back button");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.backbutton, "back button");
		waitForAjax();

	}

	public void selectUserTOEdit(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String UserName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserName");

		// WebElement
		// customerName=getwebelement(xml.getlocator("//locators/"+application+"/legalcustomerName_labelName"));
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.legalcustomerName_labelName);
		// Thread.sleep(1000);
		waitForAjax();

		// Log.info("user name is: "+username);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.selectUser1 + UserName
				+ APT_MCN_Create_CustomerObj.CreateCustomer.selectUser2,"Select User");
		waitForAjax();
		// WebElement
		// Username=getwebelement(xml.getlocator("//locators/"+application+"/selectUser").replace("value",
		// username));
		// Clickon(Username);
		// ExtentTestManager.getTest().log(LogStatus.PASS, Username +" is
		// selected");
		// Thread.sleep(1000);

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action, "User action");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action, "User action");
		// click_commonMethod(application, "User_Action", "Users_Action" , xml);

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.EditUserLink, "Edit User");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.EditUserLink, "Edit User");

	}

	public void verifyEditUserFunction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String editRolesToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editRolesToBeSelected");
		String edit_RoleToBeHidden = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_RoleToBeHidden");
		String HideRouterToolsIPv4CommandsCisco_ToBeAvailable = DataMiner.fngetcolvalue(testDataFile, sheetName,
				scriptNo, dataSetNo, "HideRouterToolsIPv4CommandsCisco_ToBeAvailable");
		String HideRouterToolsIPv4CommandsCisco_ToBeHidden = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "HideRouterToolsIPv4CommandsCisco_ToBeHidden");
		String RouterToolsIPv4CommandsHuiwai_ToBeAvailable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "RouterToolsIPv4CommandsHuiwai_ToBeAvailable");
		String HideRouterToolsIPv4CommandsHuiwai_ToBeHidden = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "HideRouterToolsIPv4CommandsHuiwai_ToBeHidden");
		String Services_ToBeAvailable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Services_ToBeAvailable");
		String Services_ToBeHidden = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Services_ToBeHidden");
		String SiteOrders_ToBeAvailable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrders_ToBeAvailable");
		String SiteOrders_ToBeHidden = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrders_ToBeHidden");

		String UserName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserName");
		String FirstName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "FirstName");
		String SurName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SurName");
		String PostalAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PostalAddress");
		String Email_AddUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email_AddUser");
		String Phone_AddUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone_AddUser");
		String IPGuardianAccountGroup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"IPGuardianAccountGroup");
		String ColtOnlineUser = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ColtOnlineUser");

		String[] rolestobeAvailableList = editRolesToBeSelected.split(",");
		String[] rolestobeHiddenList = edit_RoleToBeHidden.split(",");

		String[] routerToolIPv4CiscoTobeAvailableList = HideRouterToolsIPv4CommandsCisco_ToBeAvailable.split(",");
		String[] routerToolIPv4CiscoTobeHiddenList = HideRouterToolsIPv4CommandsCisco_ToBeHidden.split(",");

		String[] routerToolIPv4HuaweiTobeAvailableList = RouterToolsIPv4CommandsHuiwai_ToBeAvailable.split(",");
		String[] routerToolIPv4HuaweiTobeHiddenList = HideRouterToolsIPv4CommandsHuiwai_ToBeHidden.split(",");

		String[] ServicesTobeAvailablelist = Services_ToBeAvailable.split(",");
		String[] ServicesTobeHiddenlist = Services_ToBeHidden.split(",");

		String[] siteOrdersToBeAvailableList = SiteOrders_ToBeAvailable.split(",");
		String[] siteOrdersToBeHiddenList = SiteOrders_ToBeHidden.split(",");

		scrollUp();
		waitForAjax();
		// Thread.sleep(3000);

		edittextFields_commonMethod("User Name", APT_MCN_Create_CustomerObj.CreateCustomer.UserName, UserName); // User
		// Name
		// Field

		edittextFields_commonMethod("First Name", APT_MCN_Create_CustomerObj.CreateCustomer.FirstName, FirstName); // First
		// Name
		// Field

		edittextFields_commonMethod("Sur Name", APT_MCN_Create_CustomerObj.CreateCustomer.SurName, SurName); // Sur
																												// Name
		// Field

		edittextFields_commonMethod("Postal Address", APT_MCN_Create_CustomerObj.CreateCustomer.PostalAddress,
				PostalAddress); // Postal
		// Address
		// Field

		edittextFields_commonMethod("Email", APT_MCN_Create_CustomerObj.CreateCustomer.Email, Email_AddUser); // Email
		// field

		edittextFields_commonMethod("Phone", APT_MCN_Create_CustomerObj.CreateCustomer.Phone, Phone_AddUser); // Phone
		// Field

		edittextFields_commonMethod("IPGuardian Account Group",
				APT_MCN_Create_CustomerObj.CreateCustomer.IPGuardianAccountGroup, IPGuardianAccountGroup); // IPGuardian
		// Account
		// Group
		// Field

		edittextFields_commonMethod("Colt Online User", APT_MCN_Create_CustomerObj.CreateCustomer.ColtOnlineUser,
				ColtOnlineUser); // Colt
		
		String password = getAttributeFrom(APT_MCN_Create_CustomerObj.CreateCustomer.Password_Textfield, "value");
		// Log.info("Generated Password is : " + password);

		if (password.isEmpty()) {
			Report.LogInfo("INFO", "Password Field is empty. No values displaying under'Generate password link",
					"PASS");
			click(APT_MCN_Create_CustomerObj.CreateCustomer.GeneratePasswordLink, "generate password");
			// click_commonMethod(application, "Generate Password",
			// "GeneratePasswordLink", xml); // Generate// Password // Link
		} else {
			Report.LogInfo("INFO", "Automatically generated Password value is : " + password, "PASS");
		}

		// WebElement Email = getwebelement(xml.getlocator("//locators/" +
		// application + "/EmailLabelname"));
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.EmailLabelname);
		waitForAjax();
		// Thread.sleep(2000);

		// Role
		selectAndRemoveValueFromRightDropdown("Roles_Hidden",
				APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_selectedValues, rolestobeAvailableList,
				APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_removeButton);
		selectAndAddValueFromLeftDropdown("Role_Available",
				APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_available, rolestobeHiddenList,
				APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_addButton);
		verifySelectedValuesInsideRightDropdown("Role_Hidden",
				APT_MCN_Create_CustomerObj.CreateCustomer.roleDropdown_selectedValues);

		// Hide Service
		selectAndRemoveValueFromRightDropdown("Service_Hidden",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideServicesDropdown_selectedValues,
				ServicesTobeAvailablelist, APT_MCN_Create_CustomerObj.CreateCustomer.HideService_removeButton);
		selectAndAddValueFromLeftDropdown("Service_Available",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideService_Available, ServicesTobeHiddenlist,
				APT_MCN_Create_CustomerObj.CreateCustomer.HideService_addButton);
		verifySelectedValuesInsideRightDropdown("Hidden Services",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideServicesDropdown_selectedValues);

		// Hide Site Order
		selectAndRemoveValueFromRightDropdown("SiteOrder_Hidden",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideSiteOrderDropdown_selectedValues,
				siteOrdersToBeAvailableList, APT_MCN_Create_CustomerObj.CreateCustomer.hideSiteOrder_removeButton);
		selectAndAddValueFromLeftDropdown("SiteOrder_Available",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideSiteOrder_Available, siteOrdersToBeHiddenList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideSiteOrder_addbutton);
		verifySelectedValuesInsideRightDropdown("Hiden Site Orders",
				APT_MCN_Create_CustomerObj.CreateCustomer.HideSiteOrderDropdown_selectedValues);

		// scrolltoend();
		// Thread.sleep(1000);

		// Hide Router Tool IPv4 Commands(Cisco)
		selectAndRemoveValueFromRightDropdown("Router Tool IPv4 Commands(Cisco)_Available",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIpv4_Cisco_selectedvalues,
				routerToolIPv4CiscoTobeAvailableList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Cisco_removeButton);
		selectAndAddValueFromLeftDropdown("Router Tool IPv4 Commands(Cisco)_Hidden",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Cisco_Available,
				routerToolIPv4CiscoTobeHiddenList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Cisco_addButton);
		verifySelectedValuesInsideRightDropdown("Hiden Router Tool IPv4 Commands(Cisco)",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIpv4_Cisco_selectedvalues);

		// Hide Router Tool IPv4 Commands(Huawei)
		selectAndRemoveValueFromRightDropdown("Router Tool IPv4 Commands(Huawei)_Hidden",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIpv4_Huawei_selectedvalues,
				routerToolIPv4HuaweiTobeAvailableList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Huawei_removeButton);
		selectAndAddValueFromLeftDropdown("Router Tool IPv4 Commands(Huawei)_Available",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4_Huawei_available,
				routerToolIPv4HuaweiTobeHiddenList,
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIPv4__Huawei_addButton);
		verifySelectedValuesInsideRightDropdown("Hideen Router Tool IPv4 Commands(Huawei)",
				APT_MCN_Create_CustomerObj.CreateCustomer.hideRouterToolIpv4_Huawei_selectedvalues);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_AddUser);
		waitForAjax();
		// Thread.sleep(3000);

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_AddUser, "Add User");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.OkButton_AddUser, "Add User");
		// click_commonMethod(application, "OK", "OkButton_AddUser", xml);

	}

	public void deleteUser(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String username = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserName");

		// WebElement
		// customerName=getwebelement(xml.getlocator("//locators/"+application+"/legalcustomerName_labelName"));
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.legalcustomerName_labelName);
		waitForAjax();
		// Thread.sleep(1000);

		// Log.info("user name is: "+username);
		// WebElement
		// Username=getwebelement(xml.getlocator("//locators/"+application+"/selectUser").replace("value",
		// username));
		// Clickon(Username);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.selectUser1 + username
				+ APT_MCN_Create_CustomerObj.CreateCustomer.selectUser2,"Select User");
		waitForAjax();
		
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action, "User Action");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Users_Action, "User Action");

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.DeleteUserLink, "Delete User Link");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.DeleteUserLink, "Delete User Link");

		Alert alert = webDriver.switchTo().alert();
		alert.accept();
	}

	public void deleteCustomer() throws InterruptedException, IOException {
		if(isVisible(APT_MCN_Create_CustomerObj.CreateCustomer.breadCrumbNav))
		{
			click(APT_MCN_Create_CustomerObj.CreateCustomer.breadCrumbNav,"BreadCrumb");
			waitForAjax();
		}

		scrollUp();
		waitForAjax();
		// Thread.sleep(2000);
		
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.CustomerDetails_Action, "Customer Details Action");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.CustomerDetails_Action, "Customer Details Action");
		// click_commonMethod(application, "Action", "CustomerDetails_Action",
		// xml);
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.DeleteCustomerLink, "Delete Customer Link");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.DeleteCustomerLink, "Delete Customer Link");

		// click_commonMethod(application, "Delete Customer Link",
		// "DeleteCustomerLink" , xml);

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.DeleteButton_DeleteCustomer,
				"Delete Button Customer Link");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.DeleteButton_DeleteCustomer, "Delete Button Customer Link");
		waitForAjax();
		Report.LogInfo("INFO", "Clicked on Delete button", "PASS");
		waitForAjax();
		// Clickon(getwebelement(xml.getlocator("//locators/"+application+"/DeleteButton_DeleteCustomer")));
		// Thread.sleep(1000);
		// ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Delete
		// button");
		// Log.info(" Clicked on Delete button ");
		// Thread.sleep(2000);
	}

	public void createexistingorderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String existingorder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Existing_OrderService");
		String orderno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "orderno");
		String rfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "rfireqno");

		waitforPagetobeenable();
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.Orders_Services_Action);
		// Thread.sleep(1000);
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Orders_Services_Action, "Orders Services Action");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Orders_Services_Action, "Orders Services Action");

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.AddOrder_Link, "Add Order link");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.AddOrder_Link, "Add Order link");

		Report.LogInfo("INFO", "Clicked on 'Add Order' link under Order/Services pannel", "PASS");
		waitforPagetobeenable();

		if (existingorder.equalsIgnoreCase("YES")) {
			scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.existingorderdropdown);
			//selectByValue(APT_MCN_Create_CustomerObj.CreateCustomer.existingorderdropdown, orderno,
					//"'Order/Contract Number(Parent SID)");
			addDropdownValues_commonMethod("'Order/ContractNumber(Parent SID)", APT_MCN_Create_CustomerObj.CreateCustomer.existingorderdropdown,orderno);
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.rfireqiptextfield,
					"RFI / RFQ /IP Voice Line number");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.rfireqiptextfield, rfireqno);
			// addtextFields_commonMethod(application, "RFI / RFQ /IP Voice Line
			// number", "rfireqiptextfield", rfireqno,
			// xml);
		} else {
			Report.LogInfo("INFO", " Existing order is not selected ", "PASS");

		}
	}

	public void createneworderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "New_OrderService");
		String neworderno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewRFIREQNumber");

		if (neworder.equalsIgnoreCase("YES")) {
			scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.newordertextfield);
			//click(APT_MCN_Create_CustomerObj.CreateCustomer.selectorderswitch, "select order switch");
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/select order switch")));
			waitforPagetobeenable();

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.newordertextfield,
					"Order/Contract Number(Parent SID)");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.newordertextfield, neworderno);

			// addtextFields_commonMethod(application, "Order/Contract
			// Number(Parent SID)", "newordertextfield", neworderno, xml);

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.newrfireqtextfield,
					"RFI / RFQ /IP Voice Line number");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.newrfireqtextfield, newrfireqno);

			// addtextFields_commonMethod(application, "RFI / RFQ /IP Voice Line
			// number", "newrfireqtextfield", newrfireqno, xml);

			click(APT_MCN_Create_CustomerObj.CreateCustomer.createorderbutton, "create order button");
			// click_commonMethod(application, "Create Order",
			// "createorderbutton", xml);

		} else {
			Report.LogInfo("INFO", "New order is not selected", "PASS");
		}
	}

	public void verifyselectservicetype(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String servicetye = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String subtype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service_SubType");

		try {
			scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.servicetypetextfield);
			waitForAjax();
			// Thread.sleep(1000);

			// select service type
			//selectByValue(APT_MCN_Create_CustomerObj.CreateCustomer.servicetypetextfield, servicetye, "Service Type");

			addDropdownValues_commonMethod("Service Type",APT_MCN_Create_CustomerObj.CreateCustomer.servicetypetextfield, servicetye);

			// select subtype
			//selectByValue(APT_MCN_Create_CustomerObj.CreateCustomer.networkconfigurationinputfield, subtype,
					//"Network Configuration");
			addDropdownValues_commonMethod("Network Configuration", APT_MCN_Create_CustomerObj.CreateCustomer.networkconfigurationinputfield, subtype);

			// click on next button
			click(APT_MCN_Create_CustomerObj.CreateCustomer.nextbutton, "Next");
			// click_commonMethod(application, "Next", "nextbutton", xml);

		} catch (StaleElementReferenceException e) {

			List<WebElement> servicetypes = findWebElements(
					"//div[label[text()='Service Type']]//div[@role='list']//span[@role='option']");
			List<WebElement> servicesubtypes = findWebElements("//div[@role='list']//span[@role='option']");

		}
	}

	public void Verify_createorderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String serviceidenfication = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Service_Identification");
		String billingselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"BillingTypeSelection");
		String email1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");

		try {
			waitforPagetobeenable();

			scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.nextbutton);
			waitForAjax();
			click(APT_MCN_Create_CustomerObj.CreateCustomer.nextbutton, "Next Button");
			waitforPagetobeenable();

			scrollUp();
			waitForAjax();

			// check warning messages
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.serviceidentificationerror,
					"Service Identification");
			// warningMessage_commonMethod(application,
			// "serviceidentificationerror" , "Service Identification", xml);

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.billingtypeerror, "Biling Type");
			// warningMessage_commonMethod(application, "billingtypeerror" ,
			// "Biling Type", xml);

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.serviceidentificationtextfield, "Service Id");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.serviceidentificationtextfield, serviceidenfication);
			// addtextFields_commonMethod(application, "Service Id",
			// "serviceidentificationtextfield", serviceidenfication, xml);
			// //Service Identification Text Field

			// displayed service type
			// String displayedServiceTypevalue = getwebelement(
			// xml.getlocator("//locators/" + application +
			// "/servicetypevalue")).getText();
			String displayedServiceTypevalue = getTextFrom(APT_MCN_Create_CustomerObj.CreateCustomer.servicetypevalue,"service type value");
			Report.LogInfo("INFO", "Displayed service type is : " + displayedServiceTypevalue, "PASS");

			// displayed network configuration
			String displayednetworkconfiguration = getTextFrom(
					APT_MCN_Create_CustomerObj.CreateCustomer.networkconfigurationvalue,"network configuration value");
			Report.LogInfo("INFO", "Displayed network configuration is : " + displayednetworkconfiguration, "PASS");

			// String displayednetworkconfiguration = getwebelement(
			// xml.getlocator("//locators/" + application +
			// "/networkconfigurationvalue")).getText();

			// billingtype
			selectByVisibleText(APT_MCN_Create_CustomerObj.CreateCustomer.billingType_selectTag, billingselection,
					"Billing Type");
			//selectValueInsideDropdown(application, "billingType_selectTag",
			// "Billing Type", billingselection, xml);

			// Email
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.emailtextfield, "Email");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.emailtextfield, email1);

			// addtextFields_commonMethod(application, "Email",
			// "emailtextfield", email, xml);

			// phone contact
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.phonecontacttextfield, "Phone");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.phonecontacttextfield, phone);

			// addtextFields_commonMethod(application, "Phone Contact",
			// "phonecontacttextfield", phone, xml);

			// remarks
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.remarktextfield, "remarks");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.remarktextfield, remarks);

			// addtextFields_commonMethod(application, "Remarks",
			// "remarktextfield", remarks, xml);

		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void Verify_Selectmanagementoptions(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String manageserviceselectmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Managedservice");
		String ipguardainselectmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"IPGuardian");
		String ivp4selectmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router_Configuration_View_IPv4");
		String ipv6selectmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router_Configuration_View_IPv6");
		String deliverychannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DeliveryChannel");
		String snmpselectmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmpselectmode");
		String Traptargetadress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Traptargetaddress");

		// Managed Service
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.managedservicecheckbox, "Managed Service",
				manageserviceselectmode);

		// IP Guardian
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.ipguardianchecbox, "IP Guardian",
				ipguardainselectmode);

		// Router Configuration View IPv4
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.routerconfigurationviewipv4checkbox,
				"Router Configuration View IPv4", ivp4selectmode);

		// Router Configuration View IPv6
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.routerconfigurationviewipv6checkbox,
				"Router Configuration View IPv6", ipv6selectmode);

		// delivery channel
		selectValueInsideDropdown(APT_MCN_Create_CustomerObj.CreateCustomer.deliveryChannel_selectTag,
				"Delivery Channel", deliverychannel);

		// SNMP Notification
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.snmpNotificationcheckbox,
				"SNMP Notification", snmpselectmode);

		// Trap target Address
		if (snmpselectmode.equalsIgnoreCase("YES")) {

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.traptargetAddress, "Trap target adress");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.traptargetAddress, Traptargetadress);

			// addtextFields_commonMethod(application, "Trap Target Address",
			// "traptargetAddress", Traptargetadress, xml);

		} else {
			Report.LogInfo("INFO",
					" 'Trap target Address' text field is not available as 'SNMP notification' is not selected",
					"PASS");
			waitForAjax();
			// ExtentTestManager.getTest().log(LogStatus.PASS, " 'Trap target
			// Address' text field is not available as 'SNMP notification' is
			// not selected");
			// Thread.sleep(5000);
		}

	}

	public void Verify_ConfigurationOptions(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String firewall = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "routerbasedfirewall");
		String Qos = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Qos");

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.routerbasedfirewalcheckbox);
		waitForAjax();
		// Router Based Firewall
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.routerbasedfirewalcheckbox,
				"Router Based Firewall", firewall);
		// Qos
		addCheckbox_commonMethod(APT_MCN_Create_CustomerObj.CreateCustomer.Qoscheckbox, "Qos", Qos);
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.nextbutton);
		waitForAjax();
		// click on next
		click(APT_MCN_Create_CustomerObj.CreateCustomer.nextbutton, "next Button");
		// click_commonMethod(application, "Next", "nextbutton" , xml);
		waitforPagetobeenable();
		waitForAjax();

	}

	public void clickOnSearchCustomerLink() throws InterruptedException, IOException {
		mouseMoveOn(APT_MCN_Create_CustomerObj.CreateCustomer.ManageCustomerServiceLink);
		waitForAjax();
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerLink, "Search Customer");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerLink, "Search Customer");
	}

	public void searchCustomerAndperformSupply(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws Exception {

		String SearchName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String OrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String SupplyServiceToCustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SupplyServiceToCustomerName");

		try {
			// Name Field
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Name, "Search Name");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Name, SearchName);
			// addtextFields_commonMethod(application, "Name", "Name",
			// SearchName, xml);

			// Search Button
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Search_Button, "Search Button");
			click(APT_MCN_Create_CustomerObj.CreateCustomer.Search_Button, "Search Button");
			// click_commonMethod(application, "Search", "Search_Button", xml);

			scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton);

			// select the Customer
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton,
					"Search Result Rows Radio Button");
			click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton,
					"Search Result Rows Radio Button");
			Report.LogInfo("INFO", SearchName + " is selected under 'Search Customer Result", "PASS");
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/SearchResultRowsRadioButton")));
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerResult_Actionmenu,
					"Search Customer Result Action menu");
			click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerResult_Actionmenu,
					"Search Customer Result Action menu");

			// click_commonMethod(application, "Action",
			// "SearchCustomerResult_Actionmenu", xml); // action
			// button

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.View_Link, "View Link");
			click(APT_MCN_Create_CustomerObj.CreateCustomer.View_Link, "View Link");

			// click_commonMethod(application, "View_Link", "View_Link", xml);
			// // click
			// on
			// view
			// link

			waitforPagetobeenable();
			//scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel);
			Thread.sleep(1000);

			// Select an order to perform Supply
			// WebElement valueUnderOrderPanel = getwebelement(
			// xml.getlocator("//locators/" + application +
			// "/selectRowUnderOrderPanel").replace("value",
			// OrderNumber));
			// safeJavaScriptClick(valueUnderOrderPanel);
			click(APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel1+OrderNumber+APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel2,
					"adio Button in Order/Service Panel");

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Order_Service_Action, "Order Service Action");
			click(APT_MCN_Create_CustomerObj.CreateCustomer.Order_Service_Action, "Order Service Action");
			waitForAjax();
			Report.LogInfo("INFO", "Clicked on Action link under Order Service panel ", "PASS");

			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/Order_Service_Action")));
			// Thread.sleep(1000);
			// ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on
			// Action link under Order Service panel ");
			// Log.info(" Clicked on Action link under Order Service pannel ");

			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Supply_Link, "Supply_Link");
			click(APT_MCN_Create_CustomerObj.CreateCustomer.Supply_Link, "Supply_Link");
			waitForAjax();
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/Supply_Link")));
			// Thread.sleep(1000);
			// ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on
			// Supply Link");
			// Log.info(" Clicked on Supply Link ");

			// Supply _Name Field
			verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_Name,
					"Supply Service To Customer Name");
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_Name,
					SupplyServiceToCustomerName);
			waitForAjax();
			// addtextFields_commonMethod(application, "Name",
			// "SupplyServicetoCustomer_Name", SupplyServiceToCustomerName,
			// xml);
			// Thread.sleep(4000);
			//sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_Name, "*");
			waitForAjax();
			// SendKeys(getwebelement(xml.getlocator("//locators/" + application
			// + "/SupplyServicetoCustomer_Name")), "*");
			// Thread.sleep(1000);

			// Select Choose a customer dropdown
			click(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_ChooseACustomer,
					"Supply Service to Customer Choose A Customer");
			
			sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_ChooseACustomer,
					SupplyServiceToCustomerName);
			
			waitForAjax();
			

			boolean chooseCustomerdropodnVaue = false;
			try {
				// chooseCustomerdropodnVaue = ChooseACustomer.isDisplayed();
				chooseCustomerdropodnVaue = isVisible(
						APT_MCN_Create_CustomerObj.CreateCustomer.selectvalueUnderChooseAcustomerDropdown1
								+ SupplyServiceToCustomerName
								+ APT_MCN_Create_CustomerObj.CreateCustomer.selectvalueUnderChooseAcustomerDropdown2);
				if (chooseCustomerdropodnVaue) {

					click(APT_MCN_Create_CustomerObj.CreateCustomer.selectvalueUnderChooseAcustomerDropdown1
							+ SupplyServiceToCustomerName
							+ APT_MCN_Create_CustomerObj.CreateCustomer.selectvalueUnderChooseAcustomerDropdown2, "");
					Report.LogInfo("INFO",
							SupplyServiceToCustomerName + " is selected under 'Choose A Customer' dropdown", "PASS");

				} else {
					// clearing the name text field value and entering value
					// again
					edittextFields_commonMethod("Name",
							APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_Name,
							SupplyServiceToCustomerName);

					// select value under 'Choose A Customer' dropdown
					click(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_ChooseACustomer,
							"Supply Service to Customer ChooseACustomer");
					waitForAjax();
					
					click(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyServicetoCustomer_ChooseACustomer,
							"Supply Service to Customer ChooseACustomer");

					click(APT_MCN_Create_CustomerObj.CreateCustomer.closeX, "X");
					waitForAjax();
					

					webDriver.findElement(By.xpath("//div[text()='" + SupplyServiceToCustomerName + "']")).click();
					Report.LogInfo("INFO",
							SupplyServiceToCustomerName + " is selected under 'Chooose A Customer' dropdown", "PASS");
					waitForAjax();
					
				}

			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("INFO",
						SupplyServiceToCustomerName + " value is not displaying under 'Chooose A Customer' dropdown",
						"PASS");
				}

			click(APT_MCN_Create_CustomerObj.CreateCustomer.OK_Button, "OK Button");
			
		} catch (NullPointerException e) {
			e.printStackTrace();
			Report.LogInfo("INFO", "Expected Value is not displayed in Search Customer Result Grid ", "FAIL");

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("INFO", "Expected Value is not displayed in Search Customer Result Grid", "FAIL");
		}
		// sa.assertAll();
	}

	public void verifySuppliesPanelInformation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String SupplyServiceToCustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SupplyServiceToCustomerName");
		String NewOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Service_Identification");
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String Status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Status");
		String SyncStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SyncStatus");

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyHeadertab);
		waitForAjax();
		// Thread.sleep(1000);
		// Verify Supplies pannel information
		boolean suppliesPanel = false;
		try {

			// suppliesPanel=getwebelement(xml.getlocator("//locators/"+application+"/SupplyHeadertab")).isDisplayed();
			if (isVisible(APT_MCN_Create_CustomerObj.CreateCustomer.SupplyHeadertab)) {
				Report.LogInfo("INFO", "Supplies table is displaying as expected", "PASS");

				// Customer Name
				compareText("Customer", APT_MCN_Create_CustomerObj.CreateCustomer.suppliesPanel_customerName,
						SupplyServiceToCustomerName);

				// Order Column
				compareText("Order", APT_MCN_Create_CustomerObj.CreateCustomer.suppliesPanel_orderNumber,
						NewOrderNumber);

				// Service Id Column
				compareText("Service", APT_MCN_Create_CustomerObj.CreateCustomer.suppliesPanel_ServiceId,
						ServiceIdentification);

				// Service Type Column
				compareText("Service Type", APT_MCN_Create_CustomerObj.CreateCustomer.suppliesPanel_serviceType,
						ServiceType);

				// Status Column
				compareText("Status", APT_MCN_Create_CustomerObj.CreateCustomer.suppliesPanel_statusColmn, Status);

				// Sync Status
				compareText("Sync Status", APT_MCN_Create_CustomerObj.CreateCustomer.suppliesPanel_syncStatus,
						SyncStatus);

			} else {
				Report.LogInfo("INFO", "Supplies table is not displaying", "FAIL");
			}

		} catch (Exception rr) {
			rr.printStackTrace();
			Report.LogInfo("INFO", "Supplies table is not displaying", "FAIL");

		}
	}
	
	public void verifySearchCustomerFunctionalityAndView(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String SearchSubscribedCustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Name");

		// Customer Name Field
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Name, "Search Subscribed Customer");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Name, SearchSubscribedCustomer);
		// addtextFields_commonMethod(application, "Name", "Name",
		// SearchSubscribedCustomer, xml);

		// Search Button
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Search_Button, "Search Button");
		// click_commonMethod(application, "Search", "Search_Button", xml);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton);
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton,
				"Search Result Rows Radio Button");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton, "Search Result Rows Radio Button");
		waitForAjax();

		Report.LogInfo("INFO", SearchSubscribedCustomer + " is selected under 'Search Customer Result", "PASS");

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerResult_Actionmenu,
				"Search Customer Result Action menu");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerResult_Actionmenu,
				"Search Customer Result Action menu");

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.View_Link, "View_Link");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.View_Link, "View_Link");

		waitForAjax();
	}

	public void verifySearchCustomerFunctionality(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String SearchSubscribedCustomer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SupplyServiceToCustomerName");

		// Customer Name Field
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Name, "Search Subscribed Customer");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Name, SearchSubscribedCustomer);
		// addtextFields_commonMethod(application, "Name", "Name",
		// SearchSubscribedCustomer, xml);

		// Search Button
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Search_Button, "Search Button");
		// click_commonMethod(application, "Search", "Search_Button", xml);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton);
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton,
				"Search Result Rows Radio Button");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton, "Search Result Rows Radio Button");
		waitForAjax();

		Report.LogInfo("INFO", SearchSubscribedCustomer + " is selected under 'Search Customer Result", "PASS");

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerResult_Actionmenu,
				"Search Customer Result Action menu");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerResult_Actionmenu,
				"Search Customer Result Action menu");

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.View_Link, "View_Link");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.View_Link, "View_Link");

		waitForAjax();
	}

	public void verifySubscribedCustomers(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String SupplyServiceToCustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Name");
		String NewOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Service_Identification");
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String Status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Status");
		String SyncStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SyncStatus");

		boolean subscriberTable = false;

		waitforPagetobeenable();

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.SubscriberPanel);
		try {

			if (isVisible(APT_MCN_Create_CustomerObj.CreateCustomer.SubscriberPanel)) {
				Report.LogInfo("INFO", "'Subscriber' Table is displaying as expected", "PASS");

				// Customer column
				verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_CustomerColumnValue+"[contains(.,'"+SupplyServiceToCustomerName+"')]");
				/*compareText("Customer", APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_CustomerColumnValue,
						SupplyServiceToCustomerName);
				*/
				
				// Order Column
				verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_orderClumnValue+"[contains(.,'"+NewOrderNumber+"')]");
				/*compareText("Order", APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_orderClumnValue,
						NewOrderNumber);
				*/
				// Service Id Column
				verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_serviceIdColumnValue+"[contains(.,'"+ServiceIdentification+"')]");
				/*compareText("Service", APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_serviceIdColumnValue,
						ServiceIdentification);
				 */
				// Service Type Column
				compareText("Service Type",
						APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_ServiceTypeColumnValue, ServiceType);

				// Status Column
				compareText("Status", APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_StatusColumnValue,
						Status);

				// Sync Status Column
				compareText("Sync Status",
						APT_MCN_Create_CustomerObj.CreateCustomer.subscribesPanel_syncStatusColumnValue, SyncStatus);

				// select Customer and navigate to view service page
				selectRowForSubscriber_toViewService(SupplyServiceToCustomerName);

				// verify whether it got navigated to View Service page
				boolean viewServicePage = false;
				try {

					waitforPagetobeenable();

					scrollUp();
					
					if (isVisible(APT_MCN_Create_CustomerObj.CreateCustomer.viewServicePage_customerDetailsPanel)) {
						Report.LogInfo("INFO", "got navigated to view Service page as expected", "PASS");
						
					} else {
						Report.LogInfo("INFO", "not getting navigated to 'view Service' page", "FAIL");
					}
				} catch (Exception er) {
					er.printStackTrace();
					Report.LogInfo("INFO", "not getting navigated to 'view Service' page", "FAIL");

				}

			} else {
				Report.LogInfo("INFO", "'Subscribes' Table is not displaying", "FAIL");

			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", " 'Subscribes' Table is not displaying", "FAIL");
			// ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Subscribes'
			// Table is not displaying");
			// Log.info(" 'Subscriber' Table is not displaying");
		}

	}

	public void selectRowForSubscriber_toViewService(String customerName) throws InterruptedException, IOException {
		
		WebElement element = webDriver.findElement(By.xpath("(//div[div[text()='Subscribes']]/following-sibling::div//div[text()='"+customerName+"'])[1]//preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
		element.click();		
		
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.subscribePanel_ActionDropdown);
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.subscribePanel_ActionDropdown,"subscribe Panel Action Dropdown");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.subscribePanel_ActionDropdown,"subscribe Panel Action Dropdown");
		waitForAjax();
		
		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.subscribePanel_viewlink,"subscribe Panel Viewlink");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.subscribePanel_viewlink,"subscribe Panel Viewlink");
		waitForAjax();		
		
	}

	public void deletSupplyFunctionalityAndSelectOrderInsideTable(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String SearchName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String SupplyServiceToCustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SupplyServiceToCustomerName");
		String OrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Name, "Name");
		sendKeys(APT_MCN_Create_CustomerObj.CreateCustomer.Name, SearchName);
		// addtextFields_commonMethod(application, "Name", "Name", SearchName,
		// xml); //Enter Customer name

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.Search_Button, "Search");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Search_Button, "Search");
		waitForAjax();
		// click_commonMethod(application, "Search", "Search_Button", xml);
		// //click on Search button
		// Thread.sleep(1000);

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchResultRowsRadioButton, "Search Result Rows Radio Button");
		waitForAjax();
		Report.LogInfo("INFO", SearchName + " is selected under 'Search Customer Result", "PASS");
		// Clickon(getwebelement(xml.getlocator("//locators/" + application +
		// "/SearchResultRowsRadioButton")));
		// Thread.sleep(2000);
		// ExtentTestManager.getTest().log(LogStatus.PASS, SearchName + " is
		// selected under 'Search Customer Result");
		// Log.info(SearchName + " is selected under 'Search Customer Result");

		click(APT_MCN_Create_CustomerObj.CreateCustomer.SearchCustomerResult_Actionmenu,
				"Search Customer Result Action menu");
		// click_commonMethod(application, "Action",
		// "SearchCustomerResult_Actionmenu", xml); // click on Action dropdown
		click(APT_MCN_Create_CustomerObj.CreateCustomer.View_Link, "View Link");
		// click_commonMethod(application, "view_Link", "View_Link", xml); //
		// click on view link

		waitforPagetobeenable();
		// scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.);
		// Thread.sleep(2000);

		// Remove Supply
		// WebElement
		// selectValueUnderSupliesTable=getwebelement(xml.getlocator("//locators/"
		// + application + "/selectRowUnderSupplyPanel").replace("value",
		// SupplyServiceToCustomerName));

		click(APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderSupplyPanel1 + SupplyServiceToCustomerName
				+ APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderSupplyPanel2, "Click on Supply service");
		waitForAjax();
		// Thread.sleep(1000);
		Report.LogInfo("INFO", "Selected " + SupplyServiceToCustomerName + " value under 'Supplies' table", "PASS");
		// ExtentTestManager.getTest().log(LogStatus.PASS, "Selected "+
		// SupplyServiceToCustomerName + " value under 'Supplies' table");
		// Log.info("Selected "+ SupplyServiceToCustomerName + " value under
		// 'Supplies' table");

		click(APT_MCN_Create_CustomerObj.CreateCustomer.suppliesTable_ActionDropdown, "supplies Table Action Dropdown");
		// click_commonMethod(application, "Action",
		// "suppliesTable_ActionDropdown", xml);

		click(APT_MCN_Create_CustomerObj.CreateCustomer.suppliesTable_removeLink, "supplies Table Action remove link");
		// click_commonMethod(application, "Remove_Link",
		// "suppliesTable_removeLink", xml);

		waitforPagetobeenable();

		// scrollUp();
		// verifysuccessmessage(application, "Supplied service successfully
		// removed");

		// Click on View without selecting value under Order Panel
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.Order_Service_Action);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Order_Service_Action, "Order Service Action");
		// click_commonMethod(application, "Action", "Order_Service_Action",
		// xml); //click on Action dropdown in view Customer Page

		try {
			click(APT_MCN_Create_CustomerObj.CreateCustomer.order_viewLink, "order viewLink");
			// click_commonMethod(application, "View_Link", "order_viewLink",
			// xml); //click on view link

			// boolean alertpopup=false;
			// alertpopup=getwebelement(xml.getlocator("//locators/" +
			// application +
			// "/alertPopupForviewLink_underOrderpanel")).isDisplayed();
			if (isVisible(APT_MCN_Create_CustomerObj.CreateCustomer.alertPopupForviewLink_underOrderpanel)) {
				Report.LogInfo("INFO",
						"Alert popup displays as expected, when we click on 'view' without selecting any Order ",
						"PASS");
				// ExtentTestManager.getTest().log(LogStatus.PASS, "Alert popup
				// displays as expected, when we click on 'view' without
				// selecting any Order ");

				// String alertmsg=getwebelement(xml.getlocator("//locators/" +
				// application +
				// "/alertmsgForviewlink_underOrderPanel")).getText();
				String alertmsg = getTextFrom(
						APT_MCN_Create_CustomerObj.CreateCustomer.alertmsgForviewlink_underOrderPanel,"alert msg For view link_under OrderPanel");
				Report.LogInfo("INFO", "Alert popup message displays as: " + alertmsg, "PASS");

				click(APT_MCN_Create_CustomerObj.CreateCustomer.xbuttonForviewlink_underOrderPanel,
						"x button For view link under Order Panel");
				waitForAjax();
				// click_commonMethod(application, "x",
				// "xbuttonForviewlink_underOrderPanel", xml);
				// Thread.sleep(2000);

			} else {
				Report.LogInfo("INFO", "Alert popup did not display", "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", "Alert popup did not display", "FAIL");
		}

		// Select an Order Number and click on View Link
		// WebElement
		// selectValueUnderOrderTable=getwebelement(xml.getlocator("//locators/"
		// + application + "/selectRowUnderOrderPanel").replace("value",
		// OrderNumber));
		click(APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel1 + OrderNumber
				+ APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel2, "Order number");
		waitForAjax();
		// Thread.sleep(1000);
		Report.LogInfo("INFO", "Selected " + OrderNumber + " value under 'Order' table", "PASS");
		// ExtentTestManager.getTest().log(LogStatus.PASS, "Selected "+
		// OrderNumber + " value under 'Order' table");
		// Log.info("Selected "+ OrderNumber + " value under 'Order' table");

		click(APT_MCN_Create_CustomerObj.CreateCustomer.Order_Service_Action, "Action");
		// click_commonMethod(application, "Action", "Order_Service_Action",
		// xml);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.order_viewLink, "View link");
		// click_commonMethod(application, "View_Link", "order_viewLink", xml);
	}

	public void deleteService() throws InterruptedException, IOException {

		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.viewServicepage_OrderPanel);
		waitForAjax();
		click(APT_MCN_Create_CustomerObj.CreateCustomer.Editservice_actiondropdown, "Action");
		click(APT_MCN_Create_CustomerObj.CreateCustomer.viewService_deletLink, "Delete");
		waitForAjax();
		Alert alert = webDriver.switchTo().alert();

		// Capturing alert message.
		String alertMessage = webDriver.switchTo().alert().getText();
		if (alertMessage.isEmpty()) {
			Report.LogInfo("INFO", "No message displays", "FAIL");
		} else {
			Report.LogInfo("INFO", "Alert message displays as: " + alertMessage, "PASS");
		}

		alert.accept();
		Thread.sleep(2000);
	}

	public void verifySuccessMessageFor_deleteService() throws InterruptedException, IOException {

		//waitforPagetobeenable();
		waitForAjax();
		scrollUp();
		//waitForAjax();

		verifyExists(APT_MCN_Create_CustomerObj.CreateCustomer.service_deleteSuccessMessage, "Delete Message");
	}

	public void deletOrderFunctionality_selectTheOrderInsideTable(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String OrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		waitForAjax();
		
		scrollDown(APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel1 + OrderNumber
				+ APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel2);
		// Thread.sleep(1000);

		// Select an Order Number and click on View Link
		// WebElement
		// selectValueUnderOrderTable=getwebelement(xml.getlocator("//locators/"
		// + application + "/selectRowUnderOrderPanel").replace("value",
		// OrderNumber));
		click(APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel1 + OrderNumber
				+ APT_MCN_Create_CustomerObj.CreateCustomer.selectRowUnderOrderPanel2, "Select Value Under table");
		waitForAjax();
		// Clickon(selectValueUnderOrderTable);
		// Thread.sleep(1000);
		Report.LogInfo("INFO", "Selected " + OrderNumber + " value under 'Order' table", "PASS");

		click(APT_MCN_Create_CustomerObj.CreateCustomer.Order_Service_Action, "Action");
		// click_commonMethod(application, "Action", "Order_Service_Action",
		// xml);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.order_viewLink, "View link");
		// click_commonMethod(application, "View_Link", "order_viewLink", xml);

	}

	public void deleteOrder() throws InterruptedException, IOException {
		
		click(APT_MCN_Create_CustomerObj.CreateCustomer.actionbutton_orderDeletion,"action button order Deletion");
		waitForAjax();
		//click_commonMethod(application, "Action", "actionbutton_orderDeletion", xml);
		//Thread.sleep(1000);
		click(APT_MCN_Create_CustomerObj.CreateCustomer.viewService_deletLink,"view Service delete Link");
		waitForAjax();
		//click_commonMethod(application, "Delete", "viewService_deletLink", xml);
		//Thread.sleep(1000);

		Alert alert = webDriver.switchTo().alert();

		// Capturing alert message.
		String alertMessage = webDriver.switchTo().alert().getText();
		if (alertMessage.isEmpty()) {
			Report.LogInfo("INFO", "No mEssage displays", "PASS");
		} else {
			Report.LogInfo("INFO", "Alert message displays as: " + alertMessage, "PASS");
		}

		alert.accept();
		waitForAjax();
		//Thread.sleep(2000);

		verifysuccessmessage("Order successfully deleted.");
	}
	
	public void verifysuccessmessage(String expected) throws InterruptedException {
		
		waitforPagetobeenable();		
		scrollUp();
		waitForAjax();
		//Thread.sleep(1000);
		try {	
			
			//boolean successMsg=getwebelement(xml.getlocator("//locators/" + application + "/serivceAlert")).isDisplayed();

			if(isVisible(APT_MCN_Create_CustomerObj.CreateCustomer.serivceAlert)) {
				
				//String alrtmsg=getwebelement(xml.getlocator("//locators/" + application + "/AlertForServiceCreationSuccessMessage")).getText();
				String alrtmsg=getTextFrom(APT_MCN_Create_CustomerObj.CreateCustomer.AlertForServiceCreationSuccessMessage,"Alert For Service Creation Success Message");
				if(expected.contains(alrtmsg)) {					
					Report.LogInfo("INFO", "Message is verified. It is displaying as: "+alrtmsg, "PASS");
										
				}else {
					Report.LogInfo("INFO", "Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg +" .The Expected value is: "+ expected, "FAIL");
				}
				
			}else {
				Report.LogInfo("INFO", " Success Message is not displaying", "FAIL");
			}
			
		}catch(Exception e) {
			Report.LogInfo("INFO", "failure in fetching success message - 'Service created Successfully'  ", "FAIL");

		}

	}

}
