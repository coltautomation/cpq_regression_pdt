package pageObjects.c4cObjects;

import java.io.File;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class C4COpportunityObj 
{
	
	public static class Opportunity
	 { 
		//Opportunities Screen Related Elements
		public static final String opportunityMenuItem="@xpath=//div[contains(@id, 'navigationitem') and contains(text(), 'Opportunities')]"; //Opportunity Menu Item
		public static final String oppNewBtn="@xpath=//bdi[contains(@id, 'button') and text()='New']"; //Opportunity New Button
		
		//New Opportunity Related Elements
		public static final String newOpportunityTab="@xpath=//span[@data-sap-automation-id='multiTab-Text2-New Opportunity']"; //New Opportunity Tab
		public static final String nameTxb="@xpath=//span[@title='Name']//following-sibling::div//child::input"; //Name Textbox
		public static final String offerTypeDwn="@xpath=//span[@title='Offer Type']//following-sibling::div//child::input"; //Offer Type Dropdown
		public static final String offerTypeList="@xpath=//span[@title='Offer Type']//following-sibling::div//child::span"; //Offer Type List
		public static final String primaryProgDwn="@xpath=//span[@title='Primary Programme']//following-sibling::div//child::input"; //Primary Programme Dropdown
		public static final String primaryProgList="@xpath=//span[@title='Primary Programme']//following-sibling::div//child::span"; //Primary Programme List
		public static final String primaryAttriDwn="@xpath=//span[@title='Primary Attribute']//following-sibling::div//child::input"; //Primary Attribute Dropdown
		public static final String primaryAttriList="@xpath=//span[@title='Primary Attribute']//following-sibling::div//child::span"; //Primary Attribute List
		public static final String oppCurrencyDwn="@xpath=//span[@title='Opportunity Currency']//following-sibling::div//child::input"; //Opportunity Currency Dropdown
		public static final String oppCurrencyList="@xpath=//span[@title='Opportunity Currency']//following-sibling::div//child::span"; //Opportunity Currency List
		public static final String moreSaveOptions="@xpath=//div[contains(@id,'button')]//button[@title='More']"; //More Save Options
		public static final String saveMoreLink="//div[contains(@id,'button')]//button[@title='More']"; //Save More Link
		public static final String saveNOpenBtn="@xpath=//bdi[text()='Save and Open']"; //Save and Open button
		public static final String saveNOpenLink="//bdi[text()='Save and Open']"; //Save and Open button
		public static final String opprtunityIdLabel="@xpath=//bdi[contains(text(), 'Opportunity ID')]"; //Opportunity Id Label
		public static final String opprtunityIdBx="@xpath=//span[@title='Opportunity ID']//following-sibling::div//child::span"; //Opportunity Id Text
		public static final String opprtunityIdElem="//span[@title='Opportunity ID']//following-sibling::div//child::span"; //Opportunity Id Element
		public static final String headerBar="//div[@data-help-id='messageBar-headerBar']//..//span[contains(@data-help-id,'messageBar')]"; //headerBar
	
		//Edit Opportunity Related Elements
		public static final String moreLink="@xpath=//bdi[text()='More']//parent::span//parent::span//parent::button"; //More Link
		public static final String moreLinkElem="//bdi[text()='More']//parent::span//parent::span//parent::button"; //More Link Element
		public static final String moreButton="@id=formpaneBB5WKDimPKIJmo6oJb3hkm_1245-moreLessButton-img"; //More Button
		public static final String editOpportunityLnk="@xpath=//button[@title='Edit']"; //Edit Opportunity Link
		public static final String editOpportunityLnkElem="//button[@title='Edit']"; //Edit Opportunity Link
		public static final String salesUnitTxb="@xpath=//span[@title='Sales Unit']//following-sibling::div//..//input"; //Sales Unit
		public static final String campaignTxb="@xpath=//span[@title='Campaign']//following-sibling::div//..//input"; //Campaign Textbox
		public static final String legalComplexityTxb="@xpath=//span[@title='Legal complexity']//following-sibling::div//child::span"; //Legal Complexity Textbox
		public static final String legalComplexityTxbElem="//span[@title='Legal complexity']//following-sibling::div//child::span"; //Legal Complexity Textbox
		public static final String techComplexityTxb="@xpath=//span[@title='Technical complexity']//following-sibling::div//child::span"; //Technical Complexity Textbox
		public static final String techComplexityTxbElem="//span[@title='Technical complexity']//following-sibling::div//child::span"; //Technical Complexity Textbox
		public static final String saveBtn="@xpath=//descendant::bdi[contains(@id, 'button') and text()='Save']"; //Save Button
		
		// Quotes Screen
		public static final String quotesLnk="@xpath=//div[text()='Quotes']"; //Quotes Link
		public static final String overviewLnk="@xpath=//div[text()='Overview']"; //Overview Link
		public static final String addQuoteBtn="@xpath=//bdi[text()='Add']//parent::span//parent::span//parent::button"; //Add Quote Button
	
		public static final String cpqUserId="@id=Ecom_User_ID"; //CPQ Login Container
	 }
	
}
