package siebelTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentTestManager;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelCEOS;
import testHarness.siebelFunctions.SiebelCreateCustomerOrder;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelFunctions.SiebelManualValidation;
import testHarness.siebelFunctions.SiebelMode;
import testHarness.siebelFunctions.SiebelNumberManagement;

public class TS_16_SiebeltoXtrac extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
/*	ReusableFunctions Reusable = new ReusableFunctions();
	String dataFileName = "Siebel_testData.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "Xtrac";
	String scriptNo = "16";
	String dataSetNo = "22";
	String Amount = "2";*/
	
	
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCreateCustomerOrder AddProduct = new SiebelCreateCustomerOrder();
	SiebelAccounts Accounts = new SiebelAccounts();
	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelMode siebelmod = new SiebelMode();
	SiebelNumberManagement NumMana = new SiebelNumberManagement();
	SiebelManualValidation Validation = new SiebelManualValidation();
	SiebelCEOS CEOS = new SiebelCEOS();

Properties prop = new Properties();
	
	
@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
@Test
	
public void testSiebelToXtracOrder(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
	Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
				
		try
		{
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.createCustomerOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Product.productSelection(testDataFile, dataSheet, scriptNo, dataSetNo);
			Product.openServiceOrderNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			siebelmod.enterMandatoryFieldsInHeader(testDataFile, dataSheet, scriptNo, dataSetNo);	
			
						
			siebelmod.addProductAndCompleteOrdertoXtrac(testDataFile, dataSheet, scriptNo, dataSetNo);
	
		}catch(Exception e)
	    {
            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
            ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
            ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
    }
        Report.createTestCaseReportFooter();
        Report.SummaryReportlog(ScenarioName);
		
	}	
	
}
