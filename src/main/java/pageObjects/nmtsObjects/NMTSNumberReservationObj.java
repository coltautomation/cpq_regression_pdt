package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSNumberReservationObj {
	
	public static class NumberReservation
	{
		public static final String DirectAllocation="@xpath=//font[contains(@onclick,'NumberSearchFree.aspx?M=ALLOC')]"; 
		//public static final String DirectAllocation="@xpath=//font[contains(@onclick,'M=ALLOC')]";
		//public static final String DirectAllocation="@xpath=(//font[contains(@onclick,'NumberSearchFree')])[2]";
		public static final String DirectAllocationCH="@xpath=//font[contains(@onclick,'M=ALLOC')]";
		public static final String SelectWholesaleCh="@xpath=//input[@id='ucxSearchCriteria_radColt_1']";
		//public static final String DirectAllocationDE="@xpath=(//table[@id='tblNumberManagement']/tbody/tr)[7]/td";
		//public static final String DirectAllocationforIT="@xpath=//input[@id='ucxSearchCriteria_radColt_1']";
		//public static final String DirectAllocation="@xpath=(//table[@id='tblNumberManagement']/tbody/tr)[8]/td"; 
		public static final String SelectHostedNumber="@xpath=//select[@id='ucxSearchCriteria_cboHostedNumber']";
		public static final String ReturnAvailabilityIT="@xpath=//select[@id='ucxSearchCriteria_cboAvailability']";
		                                                                      //ucxSearchCriteria_cboAvailability
		public static final String ReturnAvailability="@xpath=//select[@id='ucxSearchCriteria_cboAvailabilityUK']";
		//public static final String AreaCodePrefix="@xpath=//input[@id='btnArea']";
		public static final String AreaCodePrefix="@xpath=//input[@id='ucxSearchCriteria_txtNationalCode']";// those locator taken from shashank xml if not work the replace 
		public static final String AreaCodeExt="@xpath=//input[@id='ucxSearchCriteria_txtAreaCode']";
		public static final String RangeFrom="@xpath=//input[@id='ucxSearchCriteria_txtLowerBound']";
		public static final String Rangeto="@xpath=//input[@id='ucxSearchCriteria_txtUpperBound']";
		public static final String SubmitButton="@xpath=//input[@id='ucxSearchCriteria_btnSubmitSearch']";
		public static final String GriddataNMTS="@xpath=//table[@id='ucxRange_grdView']//td[text()='IP Soft Switch']";
		public static final String checkbox3="@xpath=//input[@id='ucxRange_grdView__ctl2_chkMark']";
		public static final String Selectmark="@xpath=//a[@id='ucxRange_btnDeleteMarked']";
		public static final String AllocationCheckboxagain="@xpath=//input[@id='ucxSelectRange_grdView__ctl2_chkMark']";
		public static final String submitSelectedRange="@xpath=//input[@id='btnRequestSelectedRanges']";
		public static final String ocn="@xpath=//input[@id='ucxCustomerSearch_txtSearchText']";
		public static final String SubmitOCN="@xpath=//input[@id='ucxCustomerSearch_btnSubmitSearch']";
		public static final String GriddataforOCN="@xpath=(//table[@id='ucxCustomerSearch_grdView']//tr)[2]/td[1]";
		public static final String doubleClickonOCN="@xpath=//table[@id='ucxCustomerSearch_grdView']//tr[2]/td[1]";
		//public static final String doubleClickonOCN="@xpath=//tr[contains(@ondblclick,'OnCallDoubleClick')]";
		//public static final String doubleClickonOCN="@xpath=(//table[@id='ucxCustomerSearch_grdView']/tbody/tr)[2]";
	    public static final String selectDesc="@xpath=//select[@id='ucxRequestAttributes_cboServiceDesc']";
		public static final String EmailNotification="@xpath=//select[@id='ucxRequestAttributes_cboEmailNitification']";
		public static final String CheckboxforAllocation="@xpath=//input[@id='ucxRequestRange_grdView__ctl2_chkMark']";
		public static final String SubmitOrderBtn="@xpath=//input[@id='btnSubmitRequest']";
		public static final String ServiceProfile="@xpath=//input[@id='ucxRequestAttributes_txtServiceProfile']";
		public static final String TransactionreferenceNo="@xpath=//input[@id='ucxRequestAttributes_txtTRN']";
		//public static final String CheckboxforAllocation="@xpath=//input[@id='ucxRequestAttributes_txtTRN']";
		public static final String Reservationsuccessmsg="@xpath=//span[@id='ucxOrderAttributes_lblOrderStatus']";
		
		public static final String UserName="@xpath=//span[@id='ucxSystemMenu_lblGroupName']";
		public static final String Changegroup="@xpath=//font[contains(@onclick,'SwitchGroup')]";
		public static final String selectPhoneNOmanager="@xpath=//span[text()='Select New Group']/../..//select[@id='cboSelectNewGroup']";
		public static final String ChangeGroupbtn="@xpath=//input[@id='btnChangeGroup']";
		public static final String CountryCount="@xpath=//table[@id='tblEntityExplorer']//tr/td";
		public static final String PhnMgnCountry="@xpath=(//table[@id='tblEntityExplorer']//tr)[index]/td";
		public static final String CountryButton="@xpath=//font[contains(@onclick,'SwitchCountry')]";	
		public static final String CountryName="@xpath=//select[@id='cboSwitchCountry']";
	//	public static final String CountryNameButton="@id=btnChangeCountry"; 
		public static final String CountryNameButton="@xpath=//*[@id='btnChangeCountry']"; 
		
		public static final String NMTSReservationNo="@xpath=//*[@id='ucxOrderAttributes_lblOrderNo']"; 
		public static final String OCNTable="//table[@id='ucxCustomerSearch_grdView']"; 
		
		public static final String RemovedMarkedlink="//a[@id='ucxSelectRange_btnDeleteMarked']"; 
		public static final String FRCustomerDetailsPage="//*[@id='General Information']"; 
		
		public static final String FR_NMTSReservationNo="@xpath=//*[@id='ucxRequestAttributes_lblOrderNo']"; 
		
	}

}