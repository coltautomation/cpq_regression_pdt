package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODCancelReservationObj;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODCancelReservationPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	

	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void cancelReservationonTelPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		if(isElementPresent(By.xpath("(//*[@class='tabulator-row tabulator-selectable tabulator-row-odd'])[1]")))
        {                              
	        javaScriptclick(NODPortInRequestObj.QuickNote.ActionOption,"Action Option");
	        waitForElementToBeVisible(NODCancelReservationObj.Cancel.CancelReservationOption,15);
	        click(NODCancelReservationObj.Cancel.CancelReservationOption,"Cancel Reservation link");
	        waitForElementToBeVisible(NODCancelReservationObj.Cancel.ReturnNumberBtn,15);
	        click(NODCancelReservationObj.Cancel.ReturnNumberBtn,"Return Number button");
	        verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
        }
        else{
    		ExtentTestManager.getTest().log(LogStatus.FAIL, "Order is not found");	
    		
        }
		
	}
	
	public void cancelReservationOnTXpage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		Reusable.Select(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Cancel Reservation");
		waitForElementToBeVisible(NODCancelReservationObj.Cancel.ReturnNumberBtn,15);
        click(NODCancelReservationObj.Cancel.ReturnNumberBtn,"Return Number button");
        verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
	}

}



