package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSAddNumberPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	
	private static HashMap<String, String> Number= new HashMap<String, String>();

	
	public void AddNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String Areacode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Area_Code");
		String NumberStatusStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_Status/Status");
		String AddingnumberSwitch = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Adding_number_Switch");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String ColtCategory = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Category");
		String GeoNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Geo_Number");
	
		UserGroupforSuperUser(testDataFile, sheetName, scriptNo, dataSetNo);

		Reusable.switchtodefault();
		Reusable.waitForAjax();
		Reusable.switchtoframe("fmMenu");
		Reusable.waitForAjax();
	//	waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.IEAddButton,15);
		verifyExists(NMTSAddNumberObj.AddNumber.IEAddButton,"Add Button");
		javaScriptclick(NMTSAddNumberObj.AddNumber.IEAddButton,"Add Button");
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMain");
		
		Reusable.Select(NMTSAddNumberObj.AddNumber.Switch, AddingnumberSwitch);
		
		for(int i=0;i<15;i++)
		{
		String NMTSAreaCodeExtension = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NMTS_Area_Code_Extension");
		
		if (NMTSAreaCodeExtension=="999")
		{
			NMTSAreaCodeExtension="100";
		}
		int j= 0;
			Random rand = new Random();
			int RandomNumber = rand.nextInt(750) + 100;
	      String s1 = Integer.toString(j);
	      String s2 = Integer.toString(RandomNumber);
	  
	     // Concatenate both strings
	     String RangeStartFrom = s1 + s2;
	     System.out.println(RangeStartFrom);
	        // Convert the concatenated string
	        // to integer
	      int k1  = Integer.parseInt(RangeStartFrom);
	      
	      int k2 = k1+ 99;
	      String RangeEndTo = s1 + k2;
	      
	      System.out.println(RangeEndTo);
		if (Country.toString().contains("BE"))
		{
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.AreacodePrefix,15);
			verifyExists(NMTSAddNumberObj.AddNumber.AreacodePrefix,"Area Code Prefix");
			javaScriptclick(NMTSAddNumberObj.AddNumber.AreacodePrefix,"Area Code Prefix");
			Reusable.waitForAjax();
			Reusable.switchtofram(webDriver.findElement(By.xpath("//iframe[@id='popupFrame']")));
	
			verifyExists(NMTSAddNumberObj.AddNumber.AreaInformationWin.replace("index",Areacode),"Area Code Prefix");
			javaScriptclick(NMTSAddNumberObj.AddNumber.AreaInformationWin.replace("index",Areacode),"Area Code Prefix");
			
			Reusable.switchtodefault();
			Reusable.switchtoframe("fmMain");

		} 
		
		else {
			verifyExists(NMTSAddNumberObj.AddNumber.Areacodeother,"Area Code Other");
			sendKeys(NMTSAddNumberObj.AddNumber.Areacodeother,Areacode,"Area Code Other");
		}
		
		verifyExists(NMTSAddNumberObj.AddNumber.IEAreaCode,"Area Code");
		sendKeys(NMTSAddNumberObj.AddNumber.IEAreaCode,NMTSAreaCodeExtension,"Area Code");
		
		int NMTSAreaCodeExtension1 = Integer.parseInt(NMTSAreaCodeExtension);
		NMTSAreaCodeExtension1 = NMTSAreaCodeExtension1 +1;
		String NMTSAreaCodeExtension2 = Integer.toString(NMTSAreaCodeExtension1);
 		DataMiner.fnsetcolvalue(testDataFile,sheetName,scriptNo, dataSetNo, "NMTS_Area_Code_Extension", NMTSAreaCodeExtension2);
   		verifyExists(NMTSAddNumberObj.AddNumber.IEMergeRangeStart,"Range Start");
   		sendKeys(NMTSAddNumberObj.AddNumber.IEMergeRangeStart,RangeStartFrom,"Range Start");
				
		verifyExists(NMTSAddNumberObj.AddNumber.IEMergeRangeEnd,"Range End");
		sendKeys(NMTSAddNumberObj.AddNumber.IEMergeRangeEnd,RangeEndTo,"Range End");
	
		Reusable.Select(NMTSAddNumberObj.AddNumber.AddnumStatus, NumberStatusStatus);
		
		if(Country.toString().equals("DE NON-GEO"))
		{
			System.out.println("GEO/non-GEO is not availble for country - DE NON-GEO");
		}
		else {
			
			Reusable.Select(NMTSAddNumberObj.AddNumber.AddnumGeo,GeoNumber);
		}
		Reusable.Select(NMTSAddNumberObj.AddNumber.AddnumCtg,ColtCategory);
		
		if(Country.toString().equals("DE NON-GEO")) {
			
			System.out.println("Availbility is not availble for country - DE NON-GEO");
			
		}
		else {
			
			Reusable.Select(NMTSAddNumberObj.AddNumber.AddingAvailability, Availablity);
		
		}
		
		waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.AddingSubmit,15);
		verifyExists(NMTSAddNumberObj.AddNumber.AddingSubmit,"Submit Button");
		javaScriptclick(NMTSAddNumberObj.AddNumber.AddingSubmit,"Submit Button");
		Reusable.waitForAjax();
		if(isElementPresent(By.xpath("//span[contains(text(),'has been saved')]")))
		{
			Reusable.waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Number range added in system");	
			break;
			}
		
			}	
		
	}
	
	
	public void UserGroupforSuperUser(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");

		
	    Reusable.switchtoframe("fmMenu");
		String check = getTextFrom(NMTSAddNumberObj.AddNumber.UserName,"User Name field");
		if (check.equals("[SuperUser]")) 
		{
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.CountryButton,15);
			verifyExists(NMTSAddNumberObj.AddNumber.CountryButton,"Country Button");
		//	String test=getTextFrom(NMTSAddNumberObj.AddNumber.CountryButton);
			javaScriptclick(NMTSAddNumberObj.AddNumber.CountryButton,"Country Button");
			Reusable.switchtodefault();
			Reusable.switchtoframe("fmMain");
			
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.CountryName,15);
			
			Reusable.Select(NMTSAddNumberObj.AddNumber.CountryName,Country);
			
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.CountryNameButton,15);
			verifyExists(NMTSAddNumberObj.AddNumber.CountryNameButton,"Country Name Button");
			javaScriptclick2(NMTSAddNumberObj.AddNumber.CountryNameButton,"Country Name Button");
		//	Reusable.waitForAjax();
			
		} else {
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.Changegroup,15);
			verifyExists(NMTSAddNumberObj.AddNumber.Changegroup,"Change Group");
			javaScriptclick(NMTSAddNumberObj.AddNumber.Changegroup,"Change Group");
			
			Reusable.switchtodefault();
			Reusable.switchtoframe("fmMain");
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.selectPhoneNOmanager,15);
			
			Reusable.Select(NMTSAddNumberObj.AddNumber.selectPhoneNOmanager, "SuperUser");

			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.ChangeGroupbtn,15);
			verifyExists(NMTSAddNumberObj.AddNumber.ChangeGroupbtn,"Change Group Button");
			javaScriptclick(NMTSAddNumberObj.AddNumber.ChangeGroupbtn,"Change Group Button");
			Reusable.WaitForAjax();
	        //--------------
			Reusable.switchtodefault();
			Reusable.WaitForAjax();
			Reusable.switchtoframe("fmMenu");
			//---------------
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.CountryButton,15);
			
			ScrollIntoViewByString(NMTSAddNumberObj.AddNumber.CountryButton);
			verifyExists(NMTSAddNumberObj.AddNumber.CountryButton,"Country Button");
			javaScriptclick(NMTSAddNumberObj.AddNumber.CountryButton,"Country Button");
			WaitForAjax();
			Reusable.switchtodefault();
			WaitForAjax();
			Reusable.switchtoframe("fmMain");
			
			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.CountryName,15);
			verifyExists(NMTSAddNumberObj.AddNumber.CountryName,"Country Name");
			
			Reusable.Select(NMTSAddNumberObj.AddNumber.CountryName,Country);

			waitForElementToBeVisible(NMTSAddNumberObj.AddNumber.CountryNameButton,15);
			verifyExists(NMTSAddNumberObj.AddNumber.CountryNameButton,"Country Name Button");
			javaScriptclick2(NMTSAddNumberObj.AddNumber.CountryNameButton,"Country Name Button");
			//WaitForAjax();
		
			}
			
		}
		public void setvalue(String key, String value) {
			Number.put(key, value);
	    }

	    public String getvalue(String key) {
	        return Number.get(key);       
	    }
	
	
	}
	
	
	

