package testHarness.siebelFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.stat.descriptive.summary.Product;
import org.apache.poi.util.DocumentFormatException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.opera.core.systems.internal.input.KeyEvent;
import com.relevantcodes.extentreports.LogStatus;

import java.awt.event.InputEvent;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GridHelper;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelCeaseObj;
import pageObjects.siebelObjects.SiebelHubSiteObj;
import pageObjects.siebelObjects.SiebelLoginObj;
import pageObjects.siebelObjects.SiebelManualValidationObj;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

public class SiebelMode extends SeleniumUtils{
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();

	WebDriver driver;
	static ReadExcelFile read = new ReadExcelFile();
	String dataFileName = "Siebel_testData.xls";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	Random rnd = new Random();
	public  static ThreadLocal<String> NetworkReferenceIPVPN = new ThreadLocal<>();
	SiebelLoginPage Login = new SiebelLoginPage();
	
	GridHelper Grid = new GridHelper();

	public void addProductConfigurationDetailsAndCompleteOrderStatus(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		SiebelHubSite  Hub = new SiebelHubSite();
		SiebelSpokeSite Spoke = new SiebelSpokeSite();
		SiebelNumberManagement NumMana = new SiebelNumberManagement();
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelCEOS CEOS = new SiebelCEOS();

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OffnetCheck = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");

		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) {
			Hub.hubSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo); 
		} else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) {
			Spoke.spokeSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
			{		
				Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
				Reusable.waitForSiebelSpinnerToDisappear();	
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
			}
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
		} else {
			enterMandatoryDetailsInMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceConfigTab(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceFeatureTab(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.AddDataUnderNumberManagement(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.OperationAttribute(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.MandatoryFields(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationB();
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationA();
			Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.BillingStatus(testDataFile, sheetName, scriptNo, dataSetNo);
		
		}

	}	


	public void addProductConfigurationDetailsAndCompleteOrderStatusOffNet(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String username, String Password ) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		SiebelHubSite Hub = new SiebelHubSite();
		SiebelSpokeSite Spoke = new SiebelSpokeSite();
		SiebelNumberManagement NumMana = new SiebelNumberManagement();
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelCEOS CEOS = new SiebelCEOS();

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OffnetCheck = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Offnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String username1 = Configuration.CEOS_Username;
		String password = Configuration.CEOS_Password;

		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) 
		{
			Hub.hubSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			if (OffnetCheck.toString().equals("Offnet")) 
			{
				CEOS.CEOS_Offnet(testDataFile, sheetName, scriptNo, dataSetNo);
				Login.SiebelLogout();
				Product.LaunchingCEOSApplication(testDataFile, sheetName, scriptNo, dataSetNo,username1,Password);
				openBrowser("chrome");
				openurl(Configuration.Siebel_URL);
				Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
				Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
				CEOS.CEOS_Offnet_Status();
			}
			Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.validateSlaMatrix(testDataFile, sheetName, scriptNo, dataSetNo);
			
		}
		else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) 
		{
			Spoke.spokeSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) {		
				Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
				Reusable.waitForSiebelSpinnerToDisappear();	
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
			}
			if (OffnetCheck.toString().equals("Offnet")) 
			{
				CEOS.CEOS_Offnet(testDataFile, sheetName, scriptNo, dataSetNo);
				Login.SiebelLogout();
				Product.LaunchingCEOSApplication(testDataFile, sheetName, scriptNo, dataSetNo,username1,Password);
				openBrowser("chrome");
				openurl(Configuration.Siebel_URL);
				Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
				Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
				CEOS.CEOS_Offnet_Status();
			}
			Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			//	Product.validateSlaMatrix(testDataFile, sheetName, scriptNo, dataSetNo);
			 
		} 
		else 
		{
			enterMandatoryDetailsInMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceConfigTab(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceFeatureTab(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.AddDataUnderNumberManagement(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.OperationAttribute(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.MandatoryFields(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationB();
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationA();

			if (!Product_Type.equals("IP VPN Service")) 
			{
				if (Offnet.toString().equals("Offnet"))
				{
					CEOS.CEOS_Offnet(testDataFile, sheetName, scriptNo, dataSetNo);
					Login.SiebelLogout();
					Product.LaunchingCEOSApplication(testDataFile, sheetName, scriptNo, dataSetNo,username1,Password);
					openBrowser("chrome");
					openurl(Configuration.Siebel_URL);
					Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
					Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
					CEOS.CEOS_Offnet_Status();
				}

				Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
				
			}
			else
			{
				Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			
		}


		//	Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);

		//	Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		//	Product.validateSlaMatrix(testDataFile, sheetName, scriptNo, dataSetNo);



	}


	public void updateProductConfigurationDetailsAndCompleteOrderStatusModCom(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");

		installationTimeUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) {
			Product.ColtPromissDate(testDataFile, sheetName, scriptNo, dataSetNo);
		}

		EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		if ((Samle.toString().equalsIgnoreCase("IP VPN Access"))||
				(Samle.toString().equalsIgnoreCase("IP VPN Plus"))||
				(Samle.toString().equalsIgnoreCase("IP VPN Wholesale"))||
				(Samle.toString().equalsIgnoreCase("PrizmNet"))||(Samle.toString().equalsIgnoreCase("SWIFTNet")))
		{
			//	Product.ServiceChargeforIPVPNSite(testDataFile, sheetName, scriptNo, dataSetNo, "4");// updated
			Product.ServiceChargeforIPVPNSite(Samle,testDataFile, sheetName, scriptNo, dataSetNo, "4");
		}


		Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		String CompValidation= null;
		CompValidation = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");

		if(CompValidation.equalsIgnoreCase("Completed")) {
			Report.LogInfo("verify","<i></b> order status completed", "PASS");
		}else{
			Report.LogInfo("verify","<i></b> order status not competed", "Fail");

		}

		//newOrderOnnnet.get().getReferenceNo(Data);// added new
		/*	ProductSpecificCompleted(testDataFile, sheetName, scriptNo, dataSetNo);

		if (!(Samle.toString().equalsIgnoreCase("IP VPN Access"))&&
				!(Samle.toString().equalsIgnoreCase("IP VPN Plus")) &&
				!(Samle.toString().equalsIgnoreCase("IP VPN Wholesale"))&&
				!(Samle.toString().equalsIgnoreCase("PrizmNet")))
		{
			Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);// updated
		}

		 */
	}


	public void updateProductConfigurationDetailsAndCompleteOrderStatusModTech(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelModTech ModTechObj = new SiebelModTech();

		ModTechObj.ModTech(testDataFile, sheetName, scriptNo, dataSetNo);
		ModTechModCommWaveAndLine(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);

		Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
		//Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);

		Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); // updated
		LeadCapacity(testDataFile, sheetName, scriptNo, dataSetNo);
		Validation.clickOnManualValidationB();
		Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		Validation.clickOnManualValidationA();

		//newOrderOnnnet.get().EnterInstallationChargeInFooter(Data);
		ProductSpecificCompleted(testDataFile, sheetName, scriptNo, dataSetNo);
		if ((!Samle.toString().equalsIgnoreCase("IP VPN Access"))&&
				!(Samle.toString().equalsIgnoreCase("IP VPN Plus")) &&
				!(Samle.toString().equalsIgnoreCase("IP VPN Wholesale"))&&
				!(Samle.toString().equalsIgnoreCase("PrizmNet"))&&
				!(Samle.toString().equalsIgnoreCase("SWIFTNet")))
		{
			Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);// updated
		}
	}


	public void addProductConfigurationPremiseMaster(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String username, String Password ) throws Exception 
	{


		SiebelAddProduct Product = new SiebelAddProduct();
		SiebelPremiseMasterHelper PM = new SiebelPremiseMasterHelper();
		SiebelLoginPage Login = new SiebelLoginPage();

		String username2 = Configuration.Premise_Username;
		String password = Configuration.Premise_Password;

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) 
		{
			PM.SearchSiteHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			//			PM.SearchSite(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.AddSiteAndBuilding(testDataFile, sheetName, scriptNo, dataSetNo);
			if (ProdType.toString().equalsIgnoreCase("IP VPN Site")|| ProdType.toString().equalsIgnoreCase("IP VPN Access")) 
			{
				ClickHereSave();
			}
			//			Login.SiebelLogout();
			PM.LaunchingPremiseMasterApplication(testDataFile, sheetName, scriptNo, dataSetNo, username2, password);
			PM.ClickIcon();
			PM.SearchSiteAndVerified(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchBuildingAndEdit(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchSiteAndEdit(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.GetUpdateSiteId(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.PremiseLogout();
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
			Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchSiteHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.BuildingVerificationInSiebel(testDataFile, sheetName, scriptNo, dataSetNo);

		} 
		else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) 
		{
			//				Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchSiteHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			//			PM.SearchSite(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.AddSiteAndBuilding(testDataFile, sheetName, scriptNo, dataSetNo);
			if (ProdType.toString().equalsIgnoreCase("IP VPN Site")|| ProdType.toString().equalsIgnoreCase("IP VPN Access")|| ProdType.toString().equalsIgnoreCase("IP Access")) 
			{
				ClickHereSave();
			}
			//			Login.SiebelLogout();
			PM.LaunchingPremiseMasterApplication(testDataFile, sheetName, scriptNo, dataSetNo, username2, password);
			PM.ClickIcon();
			PM.SearchSiteAndVerified(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchBuildingAndEdit(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchSiteAndEdit(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.GetUpdateSiteId(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.PremiseLogout();
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
			Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchSiteHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.BuildingVerificationInSiebel(testDataFile, sheetName, scriptNo, dataSetNo);
		} 
		else 
		{
			PM.SearchSiteHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			//			PM.SearchSite(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.AddSiteAndBuilding(testDataFile, sheetName, scriptNo, dataSetNo);
			if (ProdType.toString().equalsIgnoreCase("IP VPN Site")|| ProdType.toString().equalsIgnoreCase("IP VPN Access")|| ProdType.toString().equalsIgnoreCase("IP Access")) 
			{
				ClickHereSave();
			}
			//			Login.SiebelLogout();
			PM.LaunchingPremiseMasterApplication(testDataFile, sheetName, scriptNo, dataSetNo, username2, password);
			PM.ClickIcon();
			PM.SearchSiteAndVerified(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchBuildingAndEdit(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchSiteAndEdit(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.GetUpdateSiteId(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.PremiseLogout();
			openurl(Configuration.Siebel_URL);
			Login.SiebelLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
			Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.SearchSiteHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			PM.BuildingVerificationInSiebel(testDataFile, sheetName, scriptNo, dataSetNo);
			if (ProdType.toString().equalsIgnoreCase("IP VPN Site")|| ProdType.toString().equalsIgnoreCase("IP VPN Access")) 
			{
				ClickHereSave();
			}
		}
	}	



	public void addProductAndCompleteOrdertoXtrac(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		SiebelHubSite  Hub = new SiebelHubSite();
		SiebelSpokeSite Spoke = new SiebelSpokeSite();
		SiebelNumberManagement NumMana = new SiebelNumberManagement();
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelCEOS CEOS = new SiebelCEOS();

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OffnetCheck = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");

		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) 
		{
			//				Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			Hub.hubSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			validateXtrac();
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo); 
			validateXtracComplete();
		} 
		else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) 
		{
			//				Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			Spoke.spokeSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			validateXtrac();
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			validateXtracComplete();
		} 
		else 
		{
			enterMandatoryDetailsInMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceConfigTab(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceFeatureTab(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.AddDataUnderNumberManagement(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.OperationAttribute(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.MandatoryFields(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationB();
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationA();
			validateXtrac();
			//Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			validateXtracComplete();
		}

	}	


	public void updateProductConfigurationDetailsAndCompleteOrderStatusCarnor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelModTech ModTechObj = new SiebelModTech();
		
		
		installationTimeUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		
		if (ProdType.toString().equalsIgnoreCase("IP VPN Service")) 
		{
			Product.ServiceChargeforIPVPNSite(Samle,testDataFile, sheetName, scriptNo, dataSetNo, "4");
		}		
		EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.Carnor(testDataFile, sheetName, scriptNo, dataSetNo);
		installationTimeUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		ModTechObj.Carnor_SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
		if (ProdType.toString().equalsIgnoreCase("DCA Ethernet")) 
		{
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		ModTechObj.CarnorReasonIQNET(testDataFile, sheetName, scriptNo, dataSetNo); 
		Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")||ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) 
		{
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		Validation.clickOnManualValidationB();
		Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		if (ProdType.toString().equalsIgnoreCase("Number Hosting")) 
		{
		EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		Validation.clickOnManualValidationA();
		Product.getReferenceNo(testDataFile, sheetName, scriptNo, dataSetNo);
		if (ProdType.toString().equalsIgnoreCase("IP VPN Service")) 
		{
			if (isElementPresent(By.xpath("//*[text()='Reason Not IQNET']/following-sibling::input"))) 
			{		
				Reusable.SelectDropDownValue("Reason Not IQNET","No IQNet network coverage");
				Reusable.waitForSiebelSpinnerToDisappear();	
				Reusable.waitForSiebelLoader();
				Reusable.savePage();
				Reusable.waitForSiebelSpinnerToDisappear();	
				Reusable.waitForSiebelLoader();
			}	
		}
		Product.getReferenceNoforIPVPNSite(testDataFile, sheetName, scriptNo, dataSetNo);
		ModTechObj.OperationAttribute_Carnor(testDataFile, sheetName, scriptNo, dataSetNo);
		ModTechObj.CarnorCompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
	//	Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
	}

	public void addProductDetailsCancelAndAbandonOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		SiebelHubSite  Hub = new SiebelHubSite();
		SiebelSpokeSite Spoke = new SiebelSpokeSite();
		SiebelNumberManagement NumMana = new SiebelNumberManagement();
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelAbandonHelper Abandon = new SiebelAbandonHelper();
			
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");

		
		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) 
		{

			Hub.hubSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			if (Comments.contains("Cancel")) 
			{
				EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
				EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
				EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
				Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
				Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			}
			Abandon.statusReason(testDataFile, sheetName, scriptNo, dataSetNo);
			Abandon.CancelAbandonOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Abandon.verifyOrderAbandonedorCancelled(testDataFile, sheetName, scriptNo, dataSetNo);
		} 
		else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) 
		{
			Spoke.spokeSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			if (Comments.contains("Cancel")) 
			{
				EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
				EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
				EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
				Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
				Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			Abandon.statusReason(testDataFile, sheetName, scriptNo, dataSetNo);
			Abandon.CancelAbandonOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Abandon.verifyOrderAbandonedorCancelled(testDataFile, sheetName, scriptNo, dataSetNo);
		} 
		else if (ProdType.toString().equalsIgnoreCase("IP VPN Service")) 
		{
			enterMandatoryDetailsInMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
			if (Comments.contains("Cancel"))
			{	
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationB();
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationA();
			Abandon.CompletedValidationforCancel(testDataFile, sheetName, scriptNo, dataSetNo);
		}	
			Abandon.statusReason(testDataFile, sheetName, scriptNo, dataSetNo);
			Abandon.CancelAbandonOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			if (Comments.contains("Cancel"))
			{
			Abandon.verifyOrderAbandonedorCancelled(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			else
			{
				Abandon.verifyIPVPNOrderAbandoned(testDataFile, sheetName, scriptNo, dataSetNo);
			}
		}
		else
		{
			Product.InputAndOpenServiceOrderNumber(testDataFile, sheetName, scriptNo, dataSetNo);
			enterMandatoryDetailsInMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
			if (Comments.contains("Cancel"))
			{
				VoiceConfigTab(testDataFile, sheetName, scriptNo, dataSetNo);
				VoiceFeatureTab(testDataFile, sheetName, scriptNo, dataSetNo);
				NumMana.AddDataUnderNumberManagement(testDataFile, sheetName, scriptNo, dataSetNo);
				EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
				EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
				EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.OperationAttribute(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
				NumMana.MandatoryFields(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
				Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
				Validation.clickOnManualValidationB();
				Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
				Validation.clickOnManualValidationA();
			}
			Abandon.statusReason(testDataFile, sheetName, scriptNo, dataSetNo);
			Abandon.CancelAbandonOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Abandon.verifyOrderAbandonedorCancelled(testDataFile, sheetName, scriptNo, dataSetNo);
		}
	}	
	
	
	
	
	





















	public void WaveLineMidleApplet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, DocumentFormatException, IOException
	{
		Reusable.waitForSiebelLoader();
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
		String ProductType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Coverageval = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Coverage");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");

		if (ProductType.toString().equalsIgnoreCase("Ethernet Line")|| ProductType.toString().equalsIgnoreCase("Wave"))
		{

			if (ProductType.toString().equalsIgnoreCase("Ethernet Line"))
			{
			Reusable.AEndDropdownSelection("Connector Type","LC");
			Reusable.BEndDropdownSelection("Connector Type","LC");
			Reusable.savePage();
			}
			
			verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
			click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
			click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
			click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
			click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
			Reusable.waitForSiebelLoader();
			//		Save();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoB,"Show full info B");
			click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoB,"Click on Show full info B");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
			click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
			click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
			click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
			Reusable.waitForSiebelLoader();
			//		Save();
			Reusable.waitForSiebelSpinnerToDisappear();
			//Service BandWidth
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "Service Bandwidth"),"Service Bandwidth");
			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "Service Bandwidth"),"Service Bandwidth");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", ServiceBandwidth),"Service Bandwidth");
			Reusable.waitForSiebelLoader();
			if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {

				verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				Reusable.waitForSiebelLoader();
			}

			//A End Resilience Option
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "A End Resilience Option"),"A End Resilience Option");
			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "A End Resilience Option"),"A End Resilience Option");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", AEndResilienceOption),"A End Resilience Option Select");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "B End Resilience Option"),"B End Resilience Option");

			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "B End Resilience Option"),"B End Resilience Option");
			//Reusable.waitForSiebelSpinnerToDisappear();	
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", BEndResilienceOption),"B End Resilience Option");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			//Coverage
			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "Coverage"),"Coverage");
			Reusable.waitForSiebelLoader();

			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", Coverageval),"Coverage");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
				verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				Reusable.waitForSiebelLoader();
			}

			Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown, 10);

			if(ProductType.toString().equalsIgnoreCase("Wave"))
			{
				//OSS Platform Flag
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "Sub Sea Cable System (Protected Path)"),"Sub Sea Cable System (Protected Path)");
				click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "Sub Sea Cable System (Protected Path)"),"Sub Sea Cable System (Protected Path)");
				Reusable.waitForSiebelLoader();

				String ServiceParty = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party");

				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", ServiceParty),"Sub Sea Cable System (Protected Path)");

				Reusable.waitForSiebelLoader();

				//Sub Sea Cable System (Worker Path)
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "Sub Sea Cable System (Worker Path)"),"Sub Sea Cable System (Worker Path)");
				click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "Sub Sea Cable System (Worker Path)"),"Sub Sea Cable System (Worker Path)");
				Reusable.waitForSiebelLoader();
				String SiteContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Contact");

				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", SiteContact),"Sub Sea Cable System (Worker Path)");

			}

			//OSS Platform Flag
			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown.replace("Value", "OSS Platform Flag"),"OSS Platform Flag");
			Reusable.waitForSiebelLoader();
			String OSSPlatformFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");

			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", OSSPlatformFlag),"OSS Platform Flag");

			Save();
			Reusable.waitForSiebelLoader();
		}	

	}

	public void Save() throws InterruptedException, DocumentFormatException {
		try
		{
			Reusable.waitForSiebelLoader();
			waitForElementToBeVisible(SiebelModeObj.WaveLineMidleApplet.SaveWaveLineMidApplet,90);
			verifyExists(SiebelModeObj.WaveLineMidleApplet.SaveWaveLineMidApplet,"Save Wave line Mid Applet");
			ScrollIntoViewByString(SiebelModeObj.WaveLineMidleApplet.SaveWaveLineMidApplet);
			click(SiebelModeObj.WaveLineMidleApplet.SaveWaveLineMidApplet,"Save Wave Line Mid Applet");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void addSiteADetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Aend_CountryValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
		String Aend_City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");
		String Aend_Postal_CodeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Postal_Code");
		String Aend_PremisesValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Premises");

		if(ProdType.toString().equalsIgnoreCase("Wave")||ProdType.toString().equalsIgnoreCase("Ethernet Line"))
		{
			/*verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA, "Search Address SiteA");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA,"Click on Search Address SiteA");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.StreetNamerfs,"Enter Street Name");

			String Street_Namerfs = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Street_Name");

			sendKeys(SiebelHubSiteObj.Customize.StreetNamerfs, Street_Namerfs, "Street Name Value");

			verifyExists(SiebelHubSiteObj.Customize.Country,"County");
			click(SiebelHubSiteObj.Customize.Country,"County");
			verifyExists(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Aend_CountryValue),"Country Value");
			click(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value",Aend_CountryValue),"Country Value");
			verifyExists(SiebelHubSiteObj.Customize.City,"City");
			sendKeys(SiebelHubSiteObj.Customize.City, Aend_City, "City");

			verifyExists(SiebelHubSiteObj.Customize.Premises,"Premises");
			sendKeys(SiebelHubSiteObj.Customize.Premises, Aend_PremisesValue, "Premises");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelHubSiteObj.Customize.PostalCode,"Postal Code");
			click(SiebelHubSiteObj.Customize.PostalCode," Click PostalCode");
			sendKeys(SiebelHubSiteObj.Customize.PostalCode, Aend_Postal_CodeValue, "PostalCode Value");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelHubSiteObj.Customize.Search,"Search");
			click(SiebelHubSiteObj.Customize.Search,"Search");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PickAddress," Verify Pick Address");
			click(SiebelModeObj.addSiteADetailsObj.PickAddress," Click on Pick Address");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Search AddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PickBuilding,"verify Pick Building");
			click(SiebelModeObj.addSiteADetailsObj.PickBuilding,"Click on Row and Pick Building");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
			click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
			Reusable.waitForSiebelLoader();
			 */
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			ScrollIntoViewByString(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess);
			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Service Party Search Access");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click on Service Party SearchAccess");

			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify Service party drop down Acccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click on Service party drop down Acccess");
			Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify Site Contact drop down Acccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click on site Contact drop down Acccess");
			Reusable.waitForSiebelLoader();
			String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Alerting_Notification_Email_Address");


			//	verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection,"Verify SiteABSelection");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", AlertingNotificationEmailAddress),"Click on SiteABSelection");
			Reusable.waitForSiebelLoader();
			String InputPartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Auto_Mitigation");
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, InputPartyName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Part Name");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click on search");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify Submit");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");
			Reusable.waitForSiebelLoader();
			if (isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) 
			{
				verifyExists(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"verify Save order changes");
				click(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"Click on Save order Changes");
				Reusable.waitForSiebelLoader();
				System.out.println("page load succesfuuly now come to middle applet");
				Reusable.waitForSiebelLoader();
			}
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"verify Site Contact Search Access");
			javaScriptclick(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click on Site Contact Search");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			waitToPageLoad(); 
			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"SiteContact Dropdown");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"SiteContact Dropdown");
			Reusable.waitForSiebelLoader();
			String CustomerNSResolvers = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Customer_DNS_Resolvers");


			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", CustomerNSResolvers),"SiteABSelection");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", CustomerNSResolvers),"SiteABSelection");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Site name");
			String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess, sitename);
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Last name site search Access");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess, "Last name site search Access");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Last name site search submit");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess, "Click On Submit");

			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave);
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		}


	}	

	//////// 0306

	public void enterMandatoryFieldsInHeader(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		waitToPageLoad(); 
		String OrderSubtype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Subtype");
		String ContractId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract");
		String ColName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String ExistingCapacityLeadTimePrimary = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Capacity_ Lead_Time_Primary");
		String MaintenanceParty = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Party");
		String MaintenanceContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Contact");
		String MaintenanceAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Address");
		String NetworkRefNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallAdmissionControl");


		Reusable.waitForSiebelLoader();

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch,"Order Sub Type Search");
		click(SiebelModeObj.MandatoryFieldsInHeader.OrderSubTypeSearch,"Order Sub Type Search");
		Reusable.waitForSiebelLoader();

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType,"Add Order Sub Type");
		click(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType,"Add Order Sub Type");
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputOrderSubType, OrderSubtype, "Input Order Sub Type");
		Reusable.waitForSiebelLoader();
		Robot robot = new Robot();
		// robot.keyPress(KeyEvent.VK_ENTER);
		waitForAjax();
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType,"Submit Sub Order Type");
		click(SiebelModeObj.MandatoryFieldsInHeader.SubmitSubOrderType,"Submit Sub Order Type");
		Reusable.waitForSiebelLoader();
		//	waitforPagetobeenable();


		// robot.keyPress(KeyEvent.VK_TAB);


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ContractSearch, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.ContractSearch,"Contract Search");
		click(SiebelModeObj.MandatoryFieldsInHeader.ContractSearch,"Contract Search");

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputContractId, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.InputContractId,"InputContractId");

		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputContractId, ContractId, "Input Contract Id");


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ContractIdSearch, 10);
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.ContractIdSearch,"Contract Id Search");
		click(SiebelModeObj.MandatoryFieldsInHeader.ContractIdSearch,"Contract Id Search");
		Reusable.waitForSiebelLoader();

		//	waitforPagetobeenable();



		if (ColName.toString().equals("Ethernet Hub")) {
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
			click(SiebelModeObj.MandatoryFieldsInHeader.AddNetworkReferenceSearch,"Add Network Reference Search");


			//		HubNetworkReference.set(Gettext(getwebelement(xml.getlocator("//locators/TextNetworkReference"))));

			//		ExtentTestManager.getTest().log(LogStatus.PASS,
			//				" Step: Generated Hub Network Reference No: " + HubNetworkReference.get());
			//		Log.info(HubNetworkReference.get());
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceCheckbox,"Bespoke Reference checkbox");
			click(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceCheckbox,"Select Bespoke Reference checkbox");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceValue,"Bespoke Reference value");
			click(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceValue,"Bespoke Reference value");
			sendKeys(SiebelModeObj.MandatoryFieldsInHeader.BespokeReferenceValue,"BSR1234", "Bespoke Reference value");
			Reusable.waitForSiebelLoader();

			//		waitforPagetobeenable();

		} else if (ColName.toString().equals("Ethernet Spoke") || ColName.toString().equals("Private Wave Node")) {

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");


			//		SendKeys(getwebelement(xml.getlocator("//locators/InputNetworkReference")), Inputdata[121].toString());
			//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Network reference ");
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.InputNetworkReference,"Network Reference input");

			sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputNetworkReference, NetworkRefNum, "Network Reference input");


			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SearchNetworkReference,"Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SearchNetworkReference,"Network Reference");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			//	waitforPagetobeenable();


		} else if (ColName.toString().equals("HNS")) {



			//		SendKeys(getwebelement(xml.getlocator("//locators/InputNetworkReference")), HubNetworkReference.get());

			//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Network reference from New Hub");

		}

		if (!ColName.toString().equals("Cloud Unified Communications")
				&& !ColName.toString().equals("IP Voice Solutions")
				&& !ColName.toString().equals("Professional Services")
				&& !ColName.toString().equalsIgnoreCase("Wave")
				&& !ColName.toString().equalsIgnoreCase("Ethernet Line")
				&& !ColName.toString().equals("Ethernet Spoke") && !ColName.toString().equals("Ethernet Hub")
				&& !ColName.toString().equals("Private Ethernet")
				&& !ColName.toString().equals("Private Wave Service")
				&& !ColName.toString().equals("DCA Ethernet")
				&& !ColName.toString().equals("Ultra Low Latency")
				&& !ColName.toString().equals("Ethernet Spoke")
				&& !ColName.toString().equals("Ethernet Access")
				&& !ColName.toString().equals("Private Wave Node")
				&& !ColName.toString().equals("IP VPN Service")) {
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch, 10);
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");

			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.NetworkPlusSign, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.NetworkPlusSign,"Network Plus Sign");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SelectNetworkReference,"Select Network Reference");

			click(SiebelModeObj.MandatoryFieldsInHeader.SelectNetworkReference,"Select Network Reference");

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			click(SiebelModeObj.MandatoryFieldsInHeader.SubmitNetworkReference,"Submit Network Reference");
			//	waitforPagetobeenable();

		}



		Reusable.waitForSiebelLoader();


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ExistingCapacityLeadTimePrimary, 10);

		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.ExistingCapacityLeadTimePrimary,"Existing Capacity Lead Time Primary");
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.ExistingCapacityLeadTimePrimary, ExistingCapacityLeadTimePrimary, "Existing Capacity Lead Time Primary");

		//	sendKeys(SiebelModeObj.MandatoryFieldsInHeader.AddOrderSubType, ExistingCapacityLeadTimePrimary, "Existing Capacity Lead Time Primary");

		//	SendkeaboardKeys(getwebelement(xml.getlocator("//locators/ExistingCapacityLeadTimePrimary")), Keys.ENTER);
		//	SendkeaboardKeys(getwebelement(xml.getlocator("//locators/ExistingCapacityLeadTimePrimary")), Keys.TAB);

		//robot.keyPress(KeyEvent.VK_ENTER);

		// robot.keyPress(KeyEvent.VK_TAB);

		//	waitforPagetobeenable();


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartySearch, 10);
		//	waitforPagetobeenable();
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartySearch,"Maintenance Party Search");
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyPopupDropdown, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyPopupDropdown,"Maintenance Party Popup Dropdown");
		javaScriptclick(SiebelModeObj.MandatoryFieldsInHeader.AccountStatus,"Account Status");

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputAccountStatus, 10);

		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputAccountStatus, MaintenanceParty, "Input Account Status");
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSearch, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSearch,"Account Status Search");
		Reusable.waitForSiebelLoader();


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSubmit, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.AccountStatusSubmit,"Account Status Submit");
		Reusable.waitForSiebelLoader();

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContact, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContact,"Maintenance Party Contact");
		Reusable.waitForSiebelLoader();

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactPopupDropdown, 10);

		javaScriptclick(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactPopupDropdown,"Maintenance Party Contact Popup Dropdown");

		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenanceLastName,"Maintenance Last Name");
		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastName, 10);
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastName, MaintenanceContact, "Input Maintenance LastName");

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastNameSearch, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.InputMaintenanceLastNameSearch,"Input Maintenance LastName Search");

		Reusable.waitForSiebelLoader();

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactSubmit, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContactSubmit,"Maintenance Party Contact Submit");


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddress, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddress,"Maintenance Party Address");	
		Reusable.waitForSiebelLoader();


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddresPopupDropdown, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddresPopupDropdown,"Maintenance Party Address popup Dropdown");	

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.PartyAddresStreetName, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.PartyAddresStreetName,"Street Name");	

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputPartyAddresStreetName, 10);
		sendKeys(SiebelModeObj.MandatoryFieldsInHeader.InputPartyAddresStreetName, MaintenanceAddress, "Input Party Addres Street Name");

		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.InputPartyAddresStreetNameSearch, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.InputPartyAddresStreetNameSearch,"Input Party Addres Street Name Search");	

		Reusable.waitForSiebelLoader();


		Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddressSubmit, 10);
		click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyAddressSubmit,"Maintenance Party Address Submit");	
		Reusable.waitForSiebelLoader();

		/*		Reusable.waitForElementToAppear("//input[@aria-labelledby='COLT_ProContact_FullName_Label']/following-sibling::span", 10);
			click("//input[@aria-labelledby='COLT_ProContact_FullName_Label']/following-sibling::span");
			Reusable.waitForElementToAppear("//button[@aria-label='Pick Contact:OK']",10);
			click("//button[@aria-label='Pick Contact:OK']");
		 */		
		//savePage();

		//		waitToPageLoad();


		if (ColName.toString().equals("IP VPN Service")||ColName.toString().equals("Ultra Low Latency")
				||ColName.toString().equals("Private Ethernet") ||ColName.toString().equals("DCA Ethernet")) 
		{
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSearch, 10);
			// waitforPagetobeenable();
			click(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSearch,"Proactive Contact Search");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContact, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContact,"Proactive Contact");

			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSubmit, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.ProactiveContactSubmit,"Proactive Contact Submit");

		}
		//		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) {

			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges,"Save Order Changes");	
			Reusable.waitForSiebelLoader();

		}
		if(isElementPresent(By.xpath("//*[contains(@class,'colt-error-panel')]"))) {
			Reusable.refreshPage();
			Reusable.waitForSiebelLoader();
		}
		Reusable.savePage();
	}


	public void ProductSpecificCompleted(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws InterruptedException, AWTException, IOException
	{


		String ColName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		if (ColName.equalsIgnoreCase("IP VPN Access")|| ColName.equalsIgnoreCase("IP VPN Plus")|| ColName.equalsIgnoreCase("PrizmNet")|| ColName.equalsIgnoreCase("IP VPN Wholesale")|| ColName.equalsIgnoreCase("SWIFTNet"))
		{
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.ProductSpecificCompleted.OrderStatusDropdown,"Order status drop down");
			click(SiebelModeObj.ProductSpecificCompleted.OrderStatusDropdown,"Order status drop down");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ProductSpecificCompleted.SelectCompleted,"Completed Status");
			click(SiebelModeObj.ProductSpecificCompleted.SelectCompleted,"Completed Status");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ProductSpecificCompleted.OrderComplete,"Order Complete");
			click(SiebelModeObj.ProductSpecificCompleted.OrderComplete,"Order Complete");
			Reusable.waitForSiebelLoader();			

			if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
				//	System.out.println("Alert Present");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
			}
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']"))) {
				//	System.out.println("Alert Present");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"AlertAccept");
				click(SiebelAddProdcutObj.CompletedValidation.AlertAccept,"Click on Alert Accept");
			}
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//button[@class='colt-primary-btn']"))) {
				//	System.out.println("");
				//	System.out.println("Alert Present");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"SubnetworkPopUP");
				click(SiebelAddProdcutObj.CompletedValidation.SubnetworkPopUP,"Click on SubnetworkPopUP");
			}
			Reusable.waitForSiebelLoader();
			String Orderstatus = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");
			if(Orderstatus.contains("Progress")) {
				verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"OrderStatusDropdown");
				click(SiebelAddProdcutObj.CompletedValidation.OrderStatusDropdown,"Click on Order status drop down");
				verifyExists(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"SelectCompleted");
				click(SiebelAddProdcutObj.CompletedValidation.SelectCompleted,"Click Completed Status");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
				if(isElementPresent(By.xpath("//button[text()='Yes']"))) {
					verifyExists(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
					click(SiebelAddProdcutObj.CompletedValidation.OrderComplete,"Order Complete Yes");
					Reusable.waitForSiebelLoader();
					Reusable.waitForAjax();
				}
			}
			System.out.println("Order complete");
			String CompValidation= null;
			CompValidation = getAttributeFrom(SiebelAddProdcutObj.CompletedValidation.OrderStatusInput,"value");

			if(CompValidation.equals("Completed")) {
				Report.LogInfo("verify","<i></b> order status completed", "PASS");
			}else{
				Report.LogInfo("verify","<i></b> order status not competed", "Fail");

			}
			Reusable.waitForSiebelLoader();	
		}



	}

	public void LeadCapacity(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws InterruptedException, AWTException, IOException
	{
		waitToPageLoad();		
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		if (ProductName.equalsIgnoreCase("Private Ethernet")&&ProductName.equalsIgnoreCase("DCA Ethernet"))
		{
			waitToPageLoad();
			verifyExists(SiebelModeObj.LeadCapacity.ExistingCapacityLeadTimePrimary,"ExistingCapacityLeadTimePrimary");
			String ExistingCapacityLeadTimePrimary = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Capacity_ Lead_Time_Primary");
			sendKeys(SiebelModeObj.LeadCapacity.ExistingCapacityLeadTimePrimary,ExistingCapacityLeadTimePrimary,"Click  on Alert Accept");		

			Reusable.SendkeaboardKeys(SiebelModeObj.LeadCapacity.ExistingCapacityLeadTimePrimary, Keys.ENTER);	
			Reusable.SendkeaboardKeys(SiebelModeObj.LeadCapacity.ExistingCapacityLeadTimePrimary, Keys.TAB);	
			waitToPageLoad();
			Reusable.savePage();		
		}
	}



	public void VoiceConfigTab(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{


		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City/Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Premises= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
		String AccessLineType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AccessLineType");

		Reusable.waitForSiebelLoader();	
		Reusable.savePage();
		Reusable.waitForSiebelLoader();	
		if (Product_Type.equals("Voice Line V")) 
		{
			verifyExists(SiebelModeObj.Interconnect.TrunkSequence,"Trunk Sequence");
			click(SiebelModeObj.Interconnect.TrunkSequence,"Trunk Sequence");
			clearTextBox(SiebelModeObj.Interconnect.TrunkSequence);
			sendKeys(SiebelModeObj.Interconnect.TrunkSequence,"1", "trunk sequence");

			verifyExists(SiebelModeObj.VoiceConfigTab.TrunkName,"Trunk Name");
			click(SiebelModeObj.VoiceConfigTab.TrunkName,"Trunk Name");	

			sendKeys(SiebelModeObj.VoiceConfigTab.TrunkName,"Trunk" + timestamp + "");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.AccessLineTypeDropDown, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.AccessLineTypeDropDown,"Access Line Type DropDown");
			click(SiebelModeObj.VoiceConfigTab.AccessLineTypeDropDown,"Click on AccessLineDropDown");	

			//		Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.AccessLineType, 10);
			//		verifyExists(SiebelModeObj.VoiceConfigTab.AccessLineType,"Access Line Type");
			//		click(SiebelModeObj.VoiceConfigTab.AccessLineType,"Click on Access Line Type");		
			//		clearTextBox(SiebelModeObj.VoiceConfigTab.AccessLineType);
			//		sendKeys(SiebelModeObj.VoiceConfigTab.AccessLineType,AccessLineTypevalue,"Access line type");		

			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType),"Verify AccessLineType");
			click(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceConfigTab.ExternalAccessServiceReference,"External Access Service Id");
			click(SiebelModeObj.VoiceConfigTab.ExternalAccessServiceReference,"Click on External Access Service Id");	
			sendKeys(SiebelModeObj.VoiceConfigTab.ExternalAccessServiceReference,"123", "External Access Service");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Verify SIPTrunkSiteNameAlias");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Alias1");

			//===
			/*verifyExists(SiebelModeObj.VoiceConfigTab.AccessServiceIdlookup,"Access ServiceId Search");
			click(SiebelModeObj.VoiceConfigTab.AccessServiceIdlookup,"Access ServiceId Search");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceConfigTab.PickServiceSubmit,"Submit Pick Service");
			click(SiebelModeObj.VoiceConfigTab.PickServiceSubmit,"Submit Pick Service");
			
			//======*/
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.showfullinfo,"show full info");
			click(SiebelModeObj.VoiceConfigTab.showfullinfo,"show full info");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader(); 
			verifyExists(SiebelModeObj.VoiceConfigTab.TrunkConfigurationplus1,"Trunk Configuration Plus");
			click(SiebelModeObj.VoiceConfigTab.TrunkConfigurationplus1,"Trunk Configuration Plus");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.outsidebuissnesshourinstallation1,"outside buissness hour installation");
			click(SiebelModeObj.VoiceConfigTab.outsidebuissnesshourinstallation1,"outside buissness hour installation");
			Reusable.waitForSiebelLoader(); 
			waitToPageLoad();
			verifyExists(SiebelModeObj.VoiceConfigTab.CrossButton,"Cross button");
			click(SiebelModeObj.VoiceConfigTab.CrossButton,"Cross button");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.SaveButtonClick,"Save Button");
			click(SiebelModeObj.VoiceConfigTab.SaveButtonClick,"Save Button");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			Reusable.waitForSiebelLoader(); 
			//===========
			/*
			verifyExists(SiebelModeObj.VoiceConfigTab.TrunckConfigplus,"Trunk Configuration Plus");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.TrunckConfigplus,"Trunk Configuration Plus"); 
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader(); 
			====//
				*/	
			verifyExists(SiebelModeObj.VoiceConfigTab.OtherTab,"Other Tab");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.OtherTab,"Other Tab");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			Reusable.waitForSiebelLoader();


			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.SelectSiteSearchAccess, 10);
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectSiteSearchAccess,"Search Site");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.StreetNameAccess, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.StreetNameAccess,"StreetNameAccess");

			sendKeys(SiebelModeObj.VoiceConfigTab.StreetNameAccess,StreetName,"Street Name Access");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CountryAccess, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CountryAccess,"Enter Country");
			sendKeys(SiebelModeObj.VoiceConfigTab.CountryAccess, Country, "Country");
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelModeObj.VoiceConfigTab.CityTownAccess, CityTown, "Enter City");

			verifyExists(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,"Postal Code");
			sendKeys(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,PostalCode, "Enter Postal Code");

			verifyExists(SiebelModeObj.VoiceConfigTab.PremisesAccess,"Premises");
			sendKeys(SiebelModeObj.VoiceConfigTab.PremisesAccess,Premises, "Enter Premises");




			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.SearchButtonAccess, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.SearchButtonAccess,"Search Button Access");
			click(SiebelModeObj.VoiceConfigTab.SearchButtonAccess,"Click on Search");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Pick Address Access");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Address for Site");

			verifyExists(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
			click(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();



			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");

			verifyExists(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Buiding for Site");
			click(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Address for Site");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			
			///////////////////
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortIDDropDown, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.PortIDDropDown,"Port ID DropDown");
			click(SiebelModeObj.VoiceConfigTab.PortIDDropDown,"Port ID DropDown");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortId, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.PortId,"S Port ID");
			click(SiebelModeObj.VoiceConfigTab.PortId," Port ID");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.TrafficDirectionDropDown, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.TrafficDirectionDropDown,"Traffic Direction DropDown");
			click(SiebelModeObj.VoiceConfigTab.TrafficDirectionDropDown,"Traffic Direction DropDown");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.TrafficDirection, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.TrafficDirection,"Traffic Direction");
			click(SiebelModeObj.VoiceConfigTab.TrafficDirection,"Traffic Direction");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CabinetTypeDropDown, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CabinetTypeDropDown,"Cabinet DropDown");
			click(SiebelModeObj.VoiceConfigTab.CabinetTypeDropDown,"Cabinet DropDown");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.Cabinet_Type, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.Cabinet_Type,"Cabinet Type");
			click(SiebelModeObj.VoiceConfigTab.Cabinet_Type,"Cabinet Type");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.Cabinet_Id,"Cabinet Id");
			click(SiebelModeObj.VoiceConfigTab.Cabinet_Id,"Cabinet Id");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.VoiceConfigTab.Cabinet_Id,"Test","Cabinet Id");

			verifyExists(SiebelModeObj.VoiceConfigTab.Shelf_ID,"Shelf ID");
			click(SiebelModeObj.VoiceConfigTab.Shelf_ID,"Shelf ID");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.VoiceConfigTab.Shelf_ID,"Test","Shelf ID");

			verifyExists(SiebelModeObj.VoiceConfigTab.SIPShowfullinfo,"Show full info");
			click(SiebelModeObj.VoiceConfigTab.SIPShowfullinfo,"Show full info");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.Isdnport, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.Isdnport,"Isdnport");
			click(SiebelModeObj.VoiceConfigTab.Isdnport,"Isdnport");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess,"POrtID DropDown Access");
			click(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess,"POrtID DropDown Access");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue,"PortID DropDown Access Value");
			click(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue,"PortID DropDown Access Value");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.Isdnport, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.Isdnport,"Isdnport");
			click(SiebelModeObj.VoiceConfigTab.Isdnport,"Isdnport");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess2, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.Isdnport,"Port ID DropDown");
			click(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess2,"Port ID DropDown");

			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue2, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue2,"POrtID Value");
			click(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue2,"POrtID Value");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.TrafficDirectionDropDown2, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.TrafficDirectionDropDown2,"Traffic Direction DropDown");
			click(SiebelModeObj.VoiceConfigTab.TrafficDirectionDropDown2,"Traffic Direction DropDown");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.TrafficDirection1, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.TrafficDirection1,"TRaffic Direction Value");
			click(SiebelModeObj.VoiceConfigTab.TrafficDirection1,"TRaffic Direction Value");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.SipTDMGateway, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.SipTDMGateway,"sip TDM Gateway");
			click(SiebelModeObj.VoiceConfigTab.SipTDMGateway,"sip TDM Gateway");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess3, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess3,"PortId DropDown");
			click(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccess3,"PortId DropDown");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue3, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue3,"PortId Value");
			click(SiebelModeObj.VoiceConfigTab.PortIDDropDownAccessValue3,"PortId Value");
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CabinetTypeDropDown2, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CabinetTypeDropDown2,"Cabinet DropDown");
			click(SiebelModeObj.VoiceConfigTab.CabinetTypeDropDown2,"Cabinet DropDown");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CabinetType1, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CabinetType1,"Cabinet Type");
			click(SiebelModeObj.VoiceConfigTab.CabinetType1,"Cabinet Type");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceConfigTab.Cabinet_Id1,"Cabinet ID");
			click(SiebelModeObj.VoiceConfigTab.Cabinet_Id1,"Cabinet ID");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.VoiceConfigTab.Cabinet_Id1,"Test","Cabinet ID");

			verifyExists(SiebelModeObj.VoiceConfigTab.ShelfID1,"ShelfID");
			click(SiebelModeObj.VoiceConfigTab.ShelfID1,"ShelfID");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.VoiceConfigTab.ShelfID1,"Test","ShelfID");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceConfigTab.CrossButtonVLV,"CrossButtonVLV");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			//verifyExists(SiebelModeObj.VoiceConfigTab.ClickheretoSaveAccess1,"Click on Save");
			//click(SiebelModeObj.VoiceConfigTab.ClickheretoSaveAccess1,"Click on Save");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			waitForAjax();
			verifyExists(SiebelModeObj.VoiceConfigTab.SelectSiteSearchAccess,"Select Site Search Access");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectSiteSearchAccess,"Select Site Search Access");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.StreetNameAccess,"StreetNameAccess");
			sendKeys(SiebelModeObj.VoiceConfigTab.StreetNameAccess,StreetName,"Street Name Access");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CountryAccess, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CountryAccess,"Enter Country");
			sendKeys(SiebelModeObj.VoiceConfigTab.CountryAccess, Country, "Input Account Status");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.CityTownAccess,"Enter City");
			sendKeys(SiebelModeObj.VoiceConfigTab.CityTownAccess, CityTown, "Enter City");

			verifyExists(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,"Postal Code");
			sendKeys(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,PostalCode, "Enter Postal Code");

			verifyExists(SiebelModeObj.VoiceConfigTab.PremisesAccess,"Premises");
			sendKeys(SiebelModeObj.VoiceConfigTab.PremisesAccess,Premises, "Enter Premises");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.SearchButtonAccess, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.SearchButtonAccess,"Search Button Access");
			click(SiebelModeObj.VoiceConfigTab.SearchButtonAccess,"Click on Search");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Pick Address Access");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Address for Site");

			verifyExists(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
			click(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");

			verifyExists(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Buiding for Site");
			click(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Address for Site");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");

			verifyExists(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			click(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.Cabinet_Id1new,"Cabinet ID");
			click(SiebelModeObj.VoiceConfigTab.Cabinet_Id1new,"Cabinet ID");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.VoiceConfigTab.Cabinet_Id1new,"Test","Cabinet ID");

			verifyExists(SiebelModeObj.VoiceConfigTab.Shelf_Id1new,"ShelfID");
			click(SiebelModeObj.VoiceConfigTab.Shelf_Id1new,"ShelfID");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.VoiceConfigTab.Shelf_Id1new,"Test","ShelfID");
			waitToPageLoad();
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.TypeoflineDropDown, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.TypeoflineDropDown,"TypeofLine DropDown");
			click(SiebelModeObj.VoiceConfigTab.TypeoflineDropDown,"TypeofLine DropDown");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.TypeofLinevalue, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.TypeofLinevalue,"TypeofLine DropDown");
			click(SiebelModeObj.VoiceConfigTab.TypeofLinevalue,"TypeofLine DropDown");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.NumberOfLines, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.NumberOfLines,"Number OfLines");
			clearTextBox(SiebelModeObj.VoiceConfigTab.NumberOfLines);
			sendKeys(SiebelModeObj.VoiceConfigTab.NumberOfLines,"2","Number OfLines");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CRC4DropDown, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CRC4DropDown,"CRC4 DropDown");
			click(SiebelModeObj.VoiceConfigTab.CRC4DropDown,"CRC4 DropDown");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CRC4Value, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CRC4Value,"CRC4 Value");
			click(SiebelModeObj.VoiceConfigTab.CRC4Value,"CRC4 Value");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.MaxChannelDropDown, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.MaxChannelDropDown,"MaxChannel DropDown");
			click(SiebelModeObj.VoiceConfigTab.MaxChannelDropDown,"MaxChannel DropDown");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.MaxChannelvalue, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.MaxChannelvalue,"MaxChannel Value");
			click(SiebelModeObj.VoiceConfigTab.MaxChannelvalue,"MaxChannel Value");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.VoiceOptionshowinfo,"Show full info");
			click(SiebelModeObj.VoiceConfigTab.VoiceOptionshowinfo,"Show full info");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceConfigTab.ColtDDIRanges,"ColtDDIRanges");
			click(SiebelModeObj.VoiceConfigTab.ColtDDIRanges,"ColtDDIRanges");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.POrtInDDIRanges, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.POrtInDDIRanges,"PortInDDIRanges");
			click(SiebelModeObj.VoiceConfigTab.POrtInDDIRanges,"PortInDDIRanges");
			/*
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.POrtInDDIRanges1, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.POrtInDDIRanges1,"PortInDDIRanges");
			click(SiebelModeObj.VoiceConfigTab.POrtInDDIRanges1,"PortInDDIRanges");
			*/
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.ResellerConfiguration, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.ResellerConfiguration,"Reseller Configuration");
			click(SiebelModeObj.VoiceConfigTab.ResellerConfiguration,"Reseller Configuration");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.UnratedCDRs, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.UnratedCDRs,"UnratedCRD's");
			click(SiebelModeObj.VoiceConfigTab.UnratedCDRs,"UnratedCRD's");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.NOofsingleColtDDIs,"NOofsingleColtDDIs");
			click(SiebelModeObj.VoiceConfigTab.NOofsingleColtDDIs,"NOofsingleColtDDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.NOofsingleColtDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.NOofsingleColtDDIs,"1","NO of single ColtDDIs");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.NOofsingleColtDDIs,Keys.TAB);

			verifyExists(SiebelModeObj.VoiceConfigTab.Noof10ColtDDIs,"Noof10ColtDDIs");
			click(SiebelModeObj.VoiceConfigTab.Noof10ColtDDIs,"Noof10ColtDDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.Noof10ColtDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.Noof10ColtDDIs,"1","No of 10ColtDDIs");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.Noof10ColtDDIs, Keys.TAB);

			verifyExists(SiebelModeObj.VoiceConfigTab.Noof100coltDDIs,"Noof100ColtDDIs");
			click(SiebelModeObj.VoiceConfigTab.Noof100coltDDIs,"Noof100ColtDDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.Noof100coltDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.Noof100coltDDIs,"1","No of 100ColtDDIs");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.Noof100coltDDIs, Keys.TAB);
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.VoiceConfigTab.Noof1000coldDDIs,"Noof1000oltDDIs");
			click(SiebelModeObj.VoiceConfigTab.Noof1000coldDDIs,"Noof1000ColtDDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.Noof1000coldDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.Noof1000coldDDIs,"1","No of 1000ColtDDIs");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.Noof1000coldDDIs, Keys.TAB);
			Reusable.waitForSiebelLoader();

			ScrollIntoViewByString(SiebelModeObj.VoiceConfigTab.NoOfsinglePOrtedDDIs);
			verifyExists(SiebelModeObj.VoiceConfigTab.NoOfsinglePOrtedDDIs,"No  Of single POrted DDIs");
			click(SiebelModeObj.VoiceConfigTab.NoOfsinglePOrtedDDIs,"No Of single POrted DDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.NoOfsinglePOrtedDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.NoOfsinglePOrtedDDIs,"1","No  Of single POrted DDIs");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.NoOfsinglePOrtedDDIs, Keys.TAB);

			verifyExists(SiebelModeObj.VoiceConfigTab.Noof10PortedDDIs,"No of 10 Ported DDIs");
			click(SiebelModeObj.VoiceConfigTab.Noof10PortedDDIs,"No of 10 Ported DDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.Noof10PortedDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.Noof10PortedDDIs,"1","No of 10 Ported DDIs");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.Noof10PortedDDIs, Keys.TAB);

			verifyExists(SiebelModeObj.VoiceConfigTab.NOof100PortedDDIs,"No of 100 Ported DDIs");
			click(SiebelModeObj.VoiceConfigTab.NOof100PortedDDIs,"No of 100 Ported DDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.NOof100PortedDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.NOof100PortedDDIs,"1","No of 10 Ported DDIs");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.NOof100PortedDDIs, Keys.TAB);

			verifyExists(SiebelModeObj.VoiceConfigTab.Noof1000PortedDDIs,"No of 1000 Ported DDIs");
			click(SiebelModeObj.VoiceConfigTab.Noof1000PortedDDIs,"No of 1000 Ported DDIs");
			clearTextBox(SiebelModeObj.VoiceConfigTab.Noof1000PortedDDIs);
			sendKeys(SiebelModeObj.VoiceConfigTab.Noof1000PortedDDIs,"1","No of 1000 Ported DDIs");
			click(SiebelModeObj.VoiceConfigTab.CrossButtonVLV,"Cross Button VLV");
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceConfigTab.OtherTab,"Other Tab");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.OtherTab,"Other Tab");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();

			//CPE value
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.SIPGatewaySetting,"SettingsButton");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SIPGatewaySetting,"SettingsButton");
			Reusable.waitForSiebelLoader();	 

			click(SiebelModeObj.VoiceConfigTab.AttributeValueCPEName,"AttributeValue CPE Name");
			sendKeys(SiebelModeObj.VoiceConfigTab.AttributeValueCPEName,"test1","AttributeValue CPE Name");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.AttributeValueCPEName, Keys.TAB);

			ScrollIntoViewByString(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute);
			verifyExists(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute,"OkButtonOperationalAttribute");
			click(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute,"OkButtonOperationalAttribute");
			Reusable.waitForSiebelLoader();	
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.SIPGatewaySetting1,"SettingsButton");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SIPGatewaySetting1,"SettingsButton");
			Reusable.waitForSiebelLoader();	 

			click(SiebelModeObj.VoiceConfigTab.AttributeValueCPEName,"AttributeValue CPE Name");
			sendKeys(SiebelModeObj.VoiceConfigTab.AttributeValueCPEName,"test2","AttributeValue CPE Name");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceConfigTab.AttributeValueCPEName, Keys.TAB);
			
			ScrollIntoViewByString(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute);
			verifyExists(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute,"OkButtonOperationalAttribute");
			click(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute,"OkButtonOperationalAttribute");

			///////////
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			waitToPageLoad();
			Reusable.waitForSiebelLoader();
		}
	}


	public void VoiceFeatureTab(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{

		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");


		if (Product_Type.equals("Voice Line V")) {
			waitToPageLoad();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceFeatureTab.VoicefeaturesTab,"Voice features Tab");
			click(SiebelModeObj.VoiceFeatureTab.VoicefeaturesTab,"Voice features Tab");

			/*Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ClickheretoSaveAccess, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ClickheretoSaveAccess,"SaveAccess");
			click(SiebelModeObj.VoiceFeatureTab.ClickheretoSaveAccess,"SaveAccess");
			 */

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceFeatureTab.showfullinfo,"Show full Info");
			click(SiebelModeObj.VoiceFeatureTab.showfullinfo,"Show full Info");

			verifyExists(SiebelModeObj.VoiceFeatureTab.ByPassNumber,"ByPassNumber");
			click(SiebelModeObj.VoiceFeatureTab.ByPassNumber,"ByPassNumber");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.VoiceFeatureTab.CallBarring,"CallBarring");
			click(SiebelModeObj.VoiceFeatureTab.CallBarring,"CallBarring");
			Reusable.waitForSiebelSpinnerToDisappear();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.CallBarringDropDown, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.CallBarringDropDown,"CallBarringDropDown");
			click(SiebelModeObj.VoiceFeatureTab.CallBarringDropDown,"CallBarringDropDown");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.Callbarringvalue, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.Callbarringvalue,"Callbarringvalue");
			click(SiebelModeObj.VoiceFeatureTab.Callbarringvalue,"Callbarringvalue");
			Reusable.waitForSiebelSpinnerToDisappear();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.Connectivity, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.Connectivity,"Connectivity");
			click(SiebelModeObj.VoiceFeatureTab.Connectivity,"Connectivity");
			Reusable.waitForSiebelSpinnerToDisappear();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.carrierHotel, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.carrierHotel,"carrierHotel");
			click(SiebelModeObj.VoiceFeatureTab.carrierHotel,"carrierHotel");

			verifyExists(SiebelModeObj.VoiceFeatureTab.FastTrack,"FastTrack");
			click(SiebelModeObj.VoiceFeatureTab.FastTrack,"FastTrack");
			//Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.VoiceFeatureTab.InternalCabling,"InternalCabling");
			javaScriptclick(SiebelModeObj.VoiceFeatureTab.InternalCabling,"InternalCabling");
			//Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.VoiceFeatureTab.LOngLining,"LOngLining");
			javaScriptclick(SiebelModeObj.VoiceFeatureTab.LOngLining,"LOngLining");

			//Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecovery,"DisasterRecovery");
			javaScriptclick(SiebelModeObj.VoiceFeatureTab.DisasterRecovery,"DisasterRecovery");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.VoiceFeatureTab.NumberOfDrPlans,"NumberOfDrPlans");
			click(SiebelModeObj.VoiceFeatureTab.NumberOfDrPlans,"NumberOfDrPlans");

			verifyExists(SiebelModeObj.VoiceFeatureTab.NumberofDRplansValue,"NumberofDRplansValue");
			click(SiebelModeObj.VoiceFeatureTab.NumberofDRplansValue,"NumberofDRplansValue");

			verifyExists(SiebelModeObj.VoiceFeatureTab.InboundCallRerouting,"InboundCallRerouting");
			click(SiebelModeObj.VoiceFeatureTab.InboundCallRerouting,"InboundCallRerouting");
			Reusable.waitForSiebelSpinnerToDisappear();

			Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.DestinationNumberforRerouting,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.DestinationNumberforRerouting,"DestinationNumberforRerouting");
			click(SiebelModeObj.VoiceFeatureTab.DestinationNumberforRerouting,"DestinationNumberforRerouting");
			sendKeys(SiebelModeObj.VoiceFeatureTab.DestinationNumberforRerouting,"1234","DestinationNumberforRerouting");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.MinimumSpendCommitment,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.MinimumSpendCommitment,"Minimum Spend Commitment");
			click(SiebelModeObj.VoiceFeatureTab.MinimumSpendCommitment,"Minimum Spend Commitment");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.PartialNumberReplacement,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.PartialNumberReplacement,"Partial Number Replacement");
			click(SiebelModeObj.VoiceFeatureTab.PartialNumberReplacement,"PartialNumberReplacement");
			//Reusable.waitForSiebelSpinnerToDisappear();
			//Reusable.Pagerefresh();
			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.internationalCode,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.internationalCode,"internationalCode");
			click(SiebelModeObj.VoiceFeatureTab.internationalCode,"internationalCode");
			sendKeys(SiebelModeObj.VoiceFeatureTab.internationalCode,"1","internationalCode");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode,"LOcalAreaCode");
			click(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode,"LOcalAreaCode");
			sendKeys(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode,"1","LOcalAreaCode");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart,"NUmberRangeStart");
			click(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart,"NUmberRangeStart");
			sendKeys(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart,"1","NUmberRangeStart");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd,"NumberRangeEnd");
			click(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd,"NumberRangeEnd");
			sendKeys(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd,"1","NumberRangeEnd");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode,"Replacement LOcal Area Code");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode,"Replacement LOcal Area Code");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode,"1","Replacement LOcal Area Code");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber,"ReplacementMainNumber");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber,"ReplacementMainNumber");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber,"1","ReplacementMainNumber");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart,"ReplacementNumberRangeStart");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart,"ReplacementNumberRangeStart");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart,"1","ReplacementNumberRangeStart");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd,"ReplacementNumberRangeEnd");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd,"ReplacementNumberRangeEnd");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd,"1","ReplacementNumberRangeEnd");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.PartialNumberConfiguration,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.PartialNumberConfiguration,"PartialNumberConfiguration");
			click(SiebelModeObj.VoiceFeatureTab.PartialNumberConfiguration,"PartialNumberConfiguration");
			Reusable.waitForSiebelSpinnerToDisappear();

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.internationalCode1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.internationalCode1,"international Code");
			click(SiebelModeObj.VoiceFeatureTab.internationalCode1,"international Code");
			sendKeys(SiebelModeObj.VoiceFeatureTab.internationalCode1,"2","international Code");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode1,"LOcalAreaCode1");
			click(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode1,"LOcal Area Code1");
			sendKeys(SiebelModeObj.VoiceFeatureTab.LOcalAreaCode1,"2","LOcal Area Code");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart1,"NUmberRangeStart1");
			click(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart1,"NUmberRangeStart1");
			sendKeys(SiebelModeObj.VoiceFeatureTab.NUmberRangeStart1,"2","NUmberRangeStart1");

			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd1,"Number Range End ");
			click(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd1,"Click on Number Range End");
			sendKeys(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd1,"2","Number Range End ");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceFeatureTab.NumberRangeEnd1,Keys.TAB);
			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode1,"Replacement LOcal Area Code");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode1,"Replacement LOcal Area Code");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode1,"2","Replacement LOcal Area Code");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceFeatureTab.ReplacementLOcalAreaCode1,Keys.TAB);
			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber1,"Replacement Main Number");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber1,"Replacement Main Number");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber1,"2","Replacement Main Number");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceFeatureTab.ReplacementMainNumber1,Keys.TAB);
			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart1,"Replacement Number Range Start");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart1,"Replacement Number Range Start");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart1,"2","Replacement Number Range Start");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeStart1,Keys.TAB);
			waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd1,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd1,"Replacement Number Range End");
			click(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd1,"Replacement Number Range End");
			sendKeys(SiebelModeObj.VoiceFeatureTab.ReplacementNumberRangeEnd1,"2","Replacement Number Range End");
			
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ServiceActivationSupport,10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.ServiceActivationSupport,"Service Activation Support");
			click(SiebelModeObj.VoiceFeatureTab.ServiceActivationSupport,"Click on Service Activation Support");
			Reusable.waitForSiebelSpinnerToDisappear();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.LanguageDropDown, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.LanguageDropDown,"Language DropDown");
			click(SiebelModeObj.VoiceFeatureTab.LanguageDropDown,"Click on Language DropDown");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.LanguageValue, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.LanguageValue,"Language Value");
			click(SiebelModeObj.VoiceFeatureTab.LanguageValue,"Click on Language Value");

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.TelephoneDirectoryServices, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.TelephoneDirectoryServices,"Telephone Directory Services");
			click(SiebelModeObj.VoiceFeatureTab.TelephoneDirectoryServices,"Click on Telephone Directory Services");
			Reusable.waitForSiebelSpinnerToDisappear();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CrossButton, 10);
			verifyExists(SiebelModeObj.VoiceConfigTab.CrossButton,"Cross Button");
			click(SiebelModeObj.VoiceConfigTab.CrossButton,"Click on Cross Button");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"Disaster Recovery Contact");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"Click on Disaster Recovery Contact");

			
			//Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ClickSaveConfig, 10);
			//verifyExists(SiebelModeObj.VoiceFeatureTab.ClickSaveConfig,"Save Config Button");
			//click(SiebelModeObj.VoiceFeatureTab.ClickSaveConfig,"Click on Save Config Button");
			//waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"DisasterRecoveryContact");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"Click on Disaster Recovery Contact");

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterAdd2,"Disaste Add");
			click(SiebelModeObj.VoiceFeatureTab.DisasterAdd2,"Click on Disaster Add");

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Verify DisasterRecoveryContactAdd button");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Click on DisasterRecoveryContactAdd button");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterAdd3,"Disaste Add");
			click(SiebelModeObj.VoiceFeatureTab.DisasterAdd3,"Click on Disaster Add");

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Verify DisasterRecoveryContactAdd button");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Click on DisasterRecoveryContactAdd button");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();										
			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryOk,"Disaster Recovery Ok");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryOk,"Click on DisasterRecoveryOk");
			Reusable.waitForSiebelLoader();
		}
		else if(Product_Type.equals("SIP Trunking")) {

			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceFeatureTab.VoicefeaturesTab,"Voice features Tab");
			click(SiebelModeObj.VoiceFeatureTab.VoicefeaturesTab,"Voice features Tab");
			/*
				Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.ClickheretoSaveAccess, 10);
				verifyExists(SiebelModeObj.VoiceFeatureTab.ClickheretoSaveAccess,"SaveAccess");
				click(SiebelModeObj.VoiceFeatureTab.ClickheretoSaveAccess,"SaveAccess");
			 */
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceFeatureTab.showfullinfo,"Show full Info");
			click(SiebelModeObj.VoiceFeatureTab.showfullinfo,"Show full Info");

			verifyExists(SiebelModeObj.VoiceFeatureTab.ByPassNumber,"ByPassNumber");
			click(SiebelModeObj.VoiceFeatureTab.ByPassNumber,"ByPassNumber");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceFeatureTab.CallBarring,"CallBarring");
			click(SiebelModeObj.VoiceFeatureTab.CallBarring,"CallBarring");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.CallBarringDropDown, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.CallBarringDropDown,"CallBarringDropDown");
			click(SiebelModeObj.VoiceFeatureTab.CallBarringDropDown,"CallBarringDropDown");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.Callbarringvalue, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.Callbarringvalue,"Callbarringvalue");
			click(SiebelModeObj.VoiceFeatureTab.Callbarringvalue,"Callbarringvalue");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.DisasterRecovery, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecovery,"DisasterRecovery");
			javaScriptclick(SiebelModeObj.VoiceFeatureTab.DisasterRecovery,"DisasterRecovery");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.NumberOfDrPlans, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.NumberOfDrPlans,"NumberOfDrPlans");
			click(SiebelModeObj.VoiceFeatureTab.NumberOfDrPlans,"NumberOfDrPlans");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			Reusable.waitForElementToAppear(SiebelModeObj.VoiceFeatureTab.NumberofDRplansValue, 10);
			verifyExists(SiebelModeObj.VoiceFeatureTab.NumberofDRplansValue,"NumberofDRplansValue");
			click(SiebelModeObj.VoiceFeatureTab.NumberofDRplansValue,"NumberofDRplansValue");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			//		int iCount=getwebelementscount(xml.getlocator("//locators/SIPCheckBoxCount"));

			int iCount = getXPathCount(SiebelModeObj.VoiceFeatureTab.SIPCheckBoxCount);
			System.out.println("count of element is"+iCount);

			while(iCount>1) {
				if(iCount<6){
					if(iCount<5) {

						click(SiebelModeObj.VoiceFeatureTab.SIPCheckBox.replace("value","3"));
						iCount--;
						Reusable.waitForSiebelSpinnerToDisappear();
						//		Reusable.waitForSiebelLoader();
					}
					else {	

						click(SiebelModeObj.VoiceFeatureTab.SIPCheckBox.replace("value","2"));
						iCount--;
						Reusable.waitForSiebelSpinnerToDisappear();
						//	Reusable.waitForSiebelLoader();
					}
				}
				else
				{
					click(SiebelModeObj.VoiceFeatureTab.SIPCheckBox.replace("value","1"));

					iCount--;
					Reusable.waitForSiebelSpinnerToDisappear();
					//		Reusable.waitForSiebelLoader();
				}

			}

			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","International Code"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","International Code"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Local Area Code"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Local Area Code"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Number Range Start"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Number Range Start"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			//click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Local Area End"));
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Number Range End"));
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Number Range End"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Local Area Code"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Local Area Code"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Main Number"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Main Number"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Number Range Start"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Number Range Start"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Number Range End"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Replacement Number Range End"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Number of Channels"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Number of Channels"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Destination Number for Re-Routing"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Destination Number for Re-Routing"),"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			click(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Language"));
			sendKeys(SiebelModeObj.VoiceFeatureTab.SIPVoiceFeatureInput.replace("Value","Language"),"English");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceFeatureTab.CrossButton1,"Cross Button");
			click(SiebelModeObj.VoiceFeatureTab.CrossButton1," Click on CrossButton");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"DisasterRecoveryContact");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"Click on DisasterRecoveryContact");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceFeatureTab.ClickSaveConfig,"ClickSaveConfig Button");
			click(SiebelModeObj.VoiceFeatureTab.ClickSaveConfig,"Click on ClickSaveConfig button");

			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"DisasterRecoveryContact");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContact,"Click on Disaster Recovery Contact");

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterAdd2,"Disaste Add");
			click(SiebelModeObj.VoiceFeatureTab.DisasterAdd2,"Click on Disaster Add");

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Verify DisasterRecoveryContactAdd button");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Click on DisasterRecoveryContactAdd button");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterAdd3,"Disaste Add");
			click(SiebelModeObj.VoiceFeatureTab.DisasterAdd3,"Click on Disaster Add");

			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Verify DisasterRecoveryContactAdd button");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryContactAdd,"Click on DisasterRecoveryContactAdd button");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();										
			verifyExists(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryOk,"Disaster Recovery Ok");
			click(SiebelModeObj.VoiceFeatureTab.DisasterRecoveryOk,"Click on DisasterRecoveryOk");
			Reusable.waitForSiebelLoader();
		}
	}	

	public void alertPopUp() throws InterruptedException, IOException
	{
		if (isVisible(SiebelModeObj.AlertPopUp.AlertAccept)) {
			click(SiebelModeObj.AlertPopUp.AlertAccept,"Click on Alerts Accept");
		}
	}

	public void EnterDateInFooter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		//	waitToPageLoad();
		Reusable.waitForSiebelLoader();
		alertPopUp();
		mouseMoveOn(SiebelModeObj.EnterDateInFooter.OrderDates);

		verifyExists(SiebelModeObj.EnterDateInFooter.OrderDates,"Order Dates Tab");
		click(SiebelModeObj.EnterDateInFooter.OrderDates," Click on Order Dates");
		//	waitToPageLoad();
		Reusable.waitForSiebelLoader();
		//	alertPopUp();
		Reusable.savePage();
		verifyExists(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date Icon");
		//	sendKeysByJS(SiebelModeObj.EnterDateInFooter.CustomerRequestedDate, Reusable.CurrentDate());
		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date");

		verifyExists(SiebelModeObj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date Icon");
		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date");

		//		verifyExists(SiebelModeObj.EnterDateInFooter.ColtActualDateIcon,"Colt Actual Date Icon");
		//		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.ColtActualDateIcon,"Colt Actual Date");

		verifyExists(SiebelModeObj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date Icon");
		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date");

		verifyExists(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day Icon");
		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		if(Product_Type.equalsIgnoreCase("Private Wave Service"))
		{
			Reusable.waitForSiebelLoader();
			//	alertPopUp();
			Reusable.savePage();
			verifyExists(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date Icon");
			//	sendKeysByJS(SiebelModeObj.EnterDateInFooter.CustomerRequestedDate, Reusable.CurrentDate());
			Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.CustomerRequestedDateIcon,"Customer Requested Date");

			verifyExists(SiebelModeObj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date Icon");
			Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.OrderSignedDateIcon,"Order Signed Date");

			//		verifyExists(SiebelModeObj.EnterDateInFooter.ColtActualDateIcon,"Colt Actual Date Icon");
			//		Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.ColtActualDateIcon,"Colt Actual Date");

			verifyExists(SiebelModeObj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date Icon");
			Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.OrderReceivedDateIcon,"Order Received Date");

			verifyExists(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day Icon");
			Reusable.SelectTodaysDate(SiebelModeObj.EnterDateInFooter.ColtPromissdayIcon,"Colt Promiss day");

			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		}
	}

	public void ClickHereSave() throws InterruptedException, AWTException, IOException  {
		try {		

			verifyExists(SiebelModeObj.ClickHereSave.ClickheretoSaveAccess,"SaveAccess");
			click(SiebelModeObj.ClickHereSave.ClickheretoSaveAccess,"Click on SaveAccess");

			Reusable.waitForSiebelLoader();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void EnterBillingDateInFooter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String ContractRenewalFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ContractRenewalFlag");
		String ContractTerm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ContractTerm");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.EnterBillingDateInFooter.Billing,"Billing Tab");
		click(SiebelModeObj.EnterBillingDateInFooter.Billing,"Click on Billing Tab");
		Reusable.waitForSiebelLoader();

		//	ClickHereSave();
		/*		if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) {
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges,"Save Order Changes");	
			Reusable.waitForSiebelLoader();
			}
		 */	
		if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) {
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges,"Save Order Changes");	
			Reusable.waitForSiebelLoader();
		}
		ScrollIntoViewByString(SiebelModeObj.EnterBillingDateInFooter.ContractRenewalFlag);
		verifyExists(SiebelModeObj.EnterBillingDateInFooter.ContractRenewalFlag,"Contract Renewal Flag");
		sendKeys(SiebelModeObj.EnterBillingDateInFooter.ContractRenewalFlag,ContractRenewalFlag, "Enter Contract Renewal Flag");
		Reusable.SendkeaboardKeys(SiebelModeObj.EnterBillingDateInFooter.ContractRenewalFlag,Keys.TAB);

		verifyExists(SiebelModeObj.EnterBillingDateInFooter.ContractTerm,"Contract Term");
		sendKeys(SiebelModeObj.EnterBillingDateInFooter.ContractTerm,ContractTerm, "Enter Contract Term");
		Reusable.SendkeaboardKeys(SiebelModeObj.EnterBillingDateInFooter.ContractTerm,Keys.TAB);

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.EnterBillingDateInFooter.BillingStartDateAccessIcon,"Billing Start Date Access Icon");
		Reusable.SelectTodaysDate(SiebelModeObj.EnterBillingDateInFooter.BillingStartDateAccessIcon,"Billing Start Date Access");

		verifyExists(SiebelModeObj.EnterBillingDateInFooter.POStartDateAccessIcon,"PO Start Date Access Icon");
		Reusable.SelectTodaysDate(SiebelModeObj.EnterBillingDateInFooter.POStartDateAccessIcon,"PO Start Date Access");

		verifyExists(SiebelModeObj.EnterBillingDateInFooter.POEndDateAccessIcon,"PO End Date Access Icon");
		Reusable.SelectTodaysDate(SiebelModeObj.EnterBillingDateInFooter.POEndDateAccessIcon,"PO End Date Access");

		Reusable.savePage();
		Reusable.waitForSiebelLoader();	
		Reusable.waitForAjax();
	}


	public void EnterServiceChargeInFooter(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		String BCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BCN");
		String Amount = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Billing_Amount");
		
		if (!Product_Type.equalsIgnoreCase("IP VPN Service")&& !Product_Type.equalsIgnoreCase("Ethernet Access")) {
			verifyExists(SiebelModeObj.EnterServiceChargeInFooter.ExpandAllButton,"Expand All Button");
			click(SiebelModeObj.EnterServiceChargeInFooter.ExpandAllButton,"Expand All Button");
			Reusable.waitForSiebelLoader();

			int RowCount = getXPathCount(SiebelModeObj.EnterServiceChargeInFooter.BillingRow) - 1;

			if (RowCount!=0)
			{
				while (!getAttributeFrom(SiebelModeObj.EnterServiceChargeInFooter.BillingLastRow, "class")
						.contains("highlight")) {

					//int RowCount = getXPathCount(SiebelModeObj.EnterServiceChargeInFooter.BillingRow) - 1;
					System.out.println(RowCount);

					for (int i = 1; i <= RowCount; i++) {
						Reusable.waitForSiebelLoader();
						verifyExists(SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmount.replace("Value",String.valueOf(i)));
						javaScriptclick(SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmount.replace("Value",String.valueOf(i)));
						Reusable.waitForSiebelLoader();

						if (!getAttributeFrom((SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmount.replace("Value",String.valueOf(i))), "class").contains("disabled"))
						{	
							sendKeys(SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmountInput.replace("Value", String.valueOf(i)), Amount);
							Reusable.waitForSiebelLoader();
						} else {
							System.out.println("Not Required to fill");
						}

						javaScriptclick(SiebelModeObj.EnterServiceChargeInFooter.BillingRowBCN.replace("Value",String.valueOf(i)));
						Reusable.waitForSiebelLoader();

						if (!getAttributeFrom((SiebelModeObj.EnterServiceChargeInFooter.BillingRowBCN.replace("Value",String.valueOf(i))), "class").contains("disabled"))
						{
							click(SiebelModeObj.EnterServiceChargeInFooter.BCNSearchClick,"Click BCN Search Click");
							Reusable.waitForSiebelLoader();
							sendKeys(SiebelModeObj.EnterServiceChargeInFooter.BCNInstallationChargeNRCInput,BCN, "Enter BCNInstallationChargeNRCInput");
							javaScriptclick(SiebelModeObj.EnterServiceChargeInFooter.BCNNRCSearch,"Click BCN Search ");
							Reusable.waitForSiebelLoader();
							Reusable.waitForSiebelSpinnerToDisappear();
						} else {
							System.out.println("Not Required to fill");
						}
												
					}
					if (Product_Type.toString().equals("Cloud Unified Communications") 
							|| Product_Type.toString().equals("Professional Services")
							|| Product_Type.toString().equals("Voice Line V")
							|| Product_Type.toString().equals("SIP Trunking")
							|| Product_Type.toString().equals("IP Voice Solutions")) {

						String NextArrowObj ="@xpath="+ SiebelModeObj.EnterServiceChargeInFooter.ServiceTable + "/tbody" + "/tr[1]/td[3]";
						String nextPageClass = getAttributeFrom(NextArrowObj, "class");
					//click(SiebelModeObj.EnterServiceChargeInFooter.FirstLineitem,"Click FirstLineitem");
					if(!nextPageClass.contains("ui-state-disabled"))
						{	
						//click(SiebelModeObj.EnterServiceChargeInFooter.FirstLineitem,"Click FirstLineitem");
						Grid.clickInGrid(SiebelModeObj.EnterServiceChargeInFooter.ServiceTable,1,3);
						verifyExists(SiebelModeObj.EnterServiceChargeInFooter.FirstLineitem,"Click FirstLineitem");
						click(SiebelModeObj.EnterServiceChargeInFooter.FirstLineitem,"Click FirstLineitem");
						}
					else{
						break;
					}
					
					}
					Reusable.waitForSiebelLoader();
					Reusable.savePage();
					Reusable.waitForSiebelLoader();
			
				}
			}
		}
	}

	public void ServiceTab(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String ServiceOrderReference_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		String Opportunity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Opportunity#");
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Existing_Capacity_Lead_Time_Primary = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Capacity_ Lead_Time_Primary");
		String Maintenance_Contact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Contact");
		String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String Maintenance_Party = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Party");
		String Maintenance_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maintenance_Address");
		String Aend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");



		Reusable.waitForSiebelLoader();

		try {
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		} catch (Exception e) {
			try {
				javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} catch (Exception e1) {

				e1.printStackTrace();
			}
		}
		Reusable.waitForSiebelLoader();
		//		waitToPageLoad();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderReference_Number,"Service OrderReference Number");
		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
		click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
		Reusable.waitForSiebelLoader();
		for (int i=0;i<15;i++) {

			if (isElementPresent(By.xpath("//*[text()='BILLING ERROR']"))) {
				break;
			}

			if (isElementPresent(By.xpath("//*[text()='DOWNSTREAM SYSTEM ERROR']"))) {

				break;
			}

			if(!isElementPresent(By.xpath("//*[text()='COMPLETE']"))) 
			{

				click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click on Service Order Go ");
				Reusable.waitForSiebelSpinnerToDisappear();
			}

			else if (isElementPresent(By.xpath("//*[text()='COMPLETE']"))) {
				break;
			}

		}

		Reusable.waitForSiebelLoader();

		try				
		{
			verifyExists(SiebelModeObj.ServiceTab.ModifyButtonClick,"Modify Button Click");
			click(SiebelModeObj.ServiceTab.ModifyButtonClick,"Click Modify Button");
		}
		catch(Exception e)
		{
			verifyExists(SiebelModeObj.ServiceTab.ModifyBtn,"Modify Button ");
			click(SiebelModeObj.ServiceTab.ModifyBtn,"Click Modify Button");
		}


		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.ServiceTab.OpportunityNo,"OpportunityNo");
		sendKeys(SiebelModeObj.ServiceTab.OpportunityNo,Opportunity,"Enter Opportunity No");

		verifyExists(SiebelModeObj.ServiceTab.RequestReceivedDate,"Request Received Date ");
		sendKeys(SiebelModeObj.ServiceTab.RequestReceivedDate, Reusable.CurrentDate());
		Reusable.waitForSiebelLoader();
		String ModifyOrderNumber = getTextFrom(SiebelModeObj.ServiceTab.ModifyOrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modify_ServiceOrderNumber", ModifyOrderNumber);	
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceOrderReference_Number", ModifyOrderNumber);

		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderClickOn,"Service Order number");
		click(SiebelModeObj.ServiceTab.ServiceOrderClickOn,"Service Order number");


		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.ServiceTab.OrderSubTypeSearch,"Order SubType Search");
		click(SiebelModeObj.ServiceTab.OrderSubTypeSearch,"Click on Order Sub TypeSearch");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.ServiceTab.AddOrderSubType,"Order Sub Type");
		click(SiebelModeObj.ServiceTab.AddOrderSubType,"Click on Add Order Sub Type");
		Reusable.waitForSiebelLoader();
		
		/*if (Comments.contains("Com")
				|| Comments.contains("Carnor")) 

		{
			verifyExists(SiebelModeObj.ServiceTab.InputOrderSubType, "InputOrderSubType");
			sendKeys(SiebelModeObj.ServiceTab.InputOrderSubType,"BCN Change", "InputOrderSubType");

		} else if (Comments.contains("Tech")) {

			sendKeys(SiebelModeObj.ServiceTab.InputOrderSubType,"Upgrade Bandwidth", "InputOrderSubType");
		}*/
		
		if (Comments.contains("Com")) 
		{
			verifyExists(SiebelModeObj.ServiceTab.InputOrderSubType, "InputOrderSubType");
			sendKeys(SiebelModeObj.ServiceTab.InputOrderSubType,"BCN Change", "InputOrderSubType");

		} else if (Comments.contains("Carnor"))
		{
			verifyExists(SiebelModeObj.ServiceTab.InputOrderSubType, "InputOrderSubType");
			sendKeys(SiebelModeObj.ServiceTab.InputOrderSubType,"A Site Change", "InputOrderSubType");
		}
		else if (Comments.contains("Tech")) {
			
			sendKeys(SiebelModeObj.ServiceTab.InputOrderSubType,"Upgrade Bandwidth", "InputOrderSubType");
		}

		Reusable.SendkeaboardKeys(SiebelModeObj.ServiceTab.InputOrderSubType, Keys.ENTER);

		verifyExists(SiebelModeObj.ServiceTab.SubmitSubOrderType,"Submit Sub Order Type");
		click(SiebelModeObj.ServiceTab.SubmitSubOrderType,"Click on Submit Sub Order Type");




		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.ServiceTab.COLTProContactFullNameLabel,"Click on COLT ProContact FullName Label");
		click(SiebelModeObj.ServiceTab.COLTProContactFullNameLabel,"Click on COLT ProContact FullName Label");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.ServiceTab.OkButton,"Click on Ok Button");
		click(SiebelModeObj.ServiceTab.OkButton,"Click on Ok Button");

		Reusable.waitForSiebelLoader();

		if(Product_Type.equalsIgnoreCase("Ethernet VPN Access") 
				|| (Product_Type.equalsIgnoreCase("IP Access"))
				|| (Product_Type.equalsIgnoreCase("Ethernet Access"))
				|| (Product_Type.equalsIgnoreCase("IP VPN Service"))
				|| (Product_Type.equals("Private Wave Node"))) 
		{


			if (isElementPresent(By.xpath("(//span[text()='Install Time']/..//span)[3]"))) {
				verifyExists(SiebelModeObj.ServiceTab.InstallTimeDropdownAccess,"InstallTimeDropdownAccess");
				click(SiebelModeObj.ServiceTab.InstallTimeDropdownAccess,"Click on InstallTimeDropdownAccess");
				//Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.ServiceTab.SiteABSelection.replace("Value",Aend_Install_Time),Aend_Install_Time);
				click(SiebelModeObj.ServiceTab.SiteABSelection.replace("Value",Aend_Install_Time),Aend_Install_Time);
				Reusable.waitForSiebelSpinnerToDisappear();
			//	verifyExists(SiebelModeObj.ServiceTab.InstallTimeSelectAccess,"InstallTimeSelectAccess");
			//	click(SiebelModeObj.ServiceTab.InstallTimeSelectAccess,"Click on Install Time Select Access");
				Reusable.waitForSiebelSpinnerToDisappear();

				//	verifyExists(SiebelModeObj.ServiceTab.ClickheretoSaveAccess,"ClickheretoSaveAccess ");
				//	click(SiebelModeObj.ServiceTab.ClickheretoSaveAccess,"Click on ClickheretoSaveAccess");
				Reusable.savePage();
				Reusable.waitForSiebelLoader();


				if (isElementPresent(By.xpath("//button[@id='colt-formalerts-ok-btn1']"))) {
					click(SiebelModeObj.ServiceTab.coltformalertsokbtn,"Click on coltformalertsokbtn");
				}

			}

		}
		else if(Product_Type.equals("Private Wave Service")
				|| Product_Type.equals("DCA Ethernet")
				|| Product_Type.equals("Dark Fibre")
				||Product_Type.equals("Private Ethernet"))
		{
			verifyExists(SiebelModeObj.ServiceTab.InstallTimeDropdownAccess,"InstallTimeDropdownAccess");
			click(SiebelModeObj.ServiceTab.InstallTimeDropdownAccess,"Click on InstallTimeDropdownAccess");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.ServiceTab.SiteABSelection.replace("Value",Aend_Install_Time),Aend_Install_Time);
			click(SiebelModeObj.ServiceTab.SiteABSelection.replace("Value",Aend_Install_Time),Aend_Install_Time);
		
			//verifyExists(SiebelModeObj.ServiceTab.InstallTimeSelectAccess,"InstallTimeSelectAccess");
			//click(SiebelModeObj.ServiceTab.InstallTimeSelectAccess,"Click on Install Time Select Access");
			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelModeObj.ServiceTab.InstalltimeBside,"InstallTimeBside");
			click(SiebelModeObj.ServiceTab.InstalltimeBside,"Click on Install Time Bside");
			Reusable.waitForSiebelSpinnerToDisappear();
		//	verifyExists(SiebelModeObj.ServiceTab.InstallTimeSelectAccess,"InstallTimeSelectAccess");
		//	click(SiebelModeObj.ServiceTab.InstallTimeSelectAccess,"Click on Install Time Select Access");
			verifyExists(SiebelModeObj.ServiceTab.SiteABSelection.replace("Value",Bend_Install_Time));
			click(SiebelModeObj.ServiceTab.SiteABSelection.replace("Value",Bend_Install_Time));

			Reusable.waitForSiebelSpinnerToDisappear();

			//	verifyExists(SiebelModeObj.ServiceTab.ClickheretoSaveAccess,"Click on Save");
			//	click(SiebelModeObj.ServiceTab.ClickheretoSaveAccess,"Click on Save");
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

		}

		verifyExists(SiebelModeObj.ServiceTab.ExistingCapacityLeadTimePrimary,"Existing Capacity Lead Time Primary");
		sendKeys(SiebelModeObj.ServiceTab.ExistingCapacityLeadTimePrimary,Existing_Capacity_Lead_Time_Primary, " Enter Existing Capacity Lead Time Primary");		
		Reusable.SendkeaboardKeys(SiebelModeObj.ServiceTab.ExistingCapacityLeadTimePrimary, Keys.ENTER);
		Reusable.SendkeaboardKeys(SiebelModeObj.ServiceTab.ExistingCapacityLeadTimePrimary, Keys.TAB);
		Reusable.savePage();
		Reusable.waitForSiebelLoader();	

		if (!Product_Type.equalsIgnoreCase("Wave")
				&& !Product_Type.toString().equalsIgnoreCase("Ethernet Line")) // added shivananda
		{
			verifyExists(SiebelModeObj.ServiceTab.MaintenancePartySearch,"Maintenance Party Search");
			click(SiebelModeObj.ServiceTab.MaintenancePartySearch,"Maintenance Party Search");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.ServiceTab.MaintenancePartyPopupDropdown,"Maintenance Party Popup Dropdown");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.ServiceTab.AccountStatus,"AccountStatus");
			click(SiebelModeObj.ServiceTab.AccountStatus,"AccountStatus");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.ServiceTab.InputAccountStatus, Maintenance_Party, "Input Account Status");
			verifyExists(SiebelModeObj.ServiceTab.AccountStatusSearch,"Account Status Search");
			click(SiebelModeObj.ServiceTab.AccountStatusSearch,"Account Status Search");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ServiceTab.AccountStatusSubmit,"Account Status Submit");
			click(SiebelModeObj.ServiceTab.AccountStatusSubmit,"Account Status Submit");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContact,"Maintenance Party Contact");
			click(SiebelModeObj.MandatoryFieldsInHeader.MaintenancePartyContact,"Maintenance Party Contact");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ServiceTab.MaintenancePartyContactPopupDropdown,"Maintenance Party Contact Popup Dropdown");
			click(SiebelModeObj.ServiceTab.MaintenancePartyContactPopupDropdown,"Click on MaintenancePartyContactPopupDropdown");

			verifyExists(SiebelModeObj.ServiceTab.MaintenanceLastName,"Maintenance Last Name");
			click(SiebelModeObj.ServiceTab.MaintenanceLastName,"Click on Maintenance Last Name");

			verifyExists(SiebelModeObj.ServiceTab.InputMaintenanceLastName,"Maintenance Last Name");
			sendKeys(SiebelModeObj.ServiceTab.InputMaintenanceLastName, Maintenance_Contact, "Maintenance Contact");

			verifyExists(SiebelModeObj.ServiceTab.InputMaintenanceLastNameSearch,"Maintenance Last Name");
			click(SiebelModeObj.ServiceTab.InputMaintenanceLastNameSearch,"Click on Input Maintenance LastNameSearch");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ServiceTab.MaintenancePartyContactSubmit,"MaintenancePartyContactSubmit");
			click(SiebelModeObj.ServiceTab.MaintenancePartyContactSubmit,"Click on MaintenancePartyContactSubmit");
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.ServiceTab.MaintenancePartyAddress,"MaintenancePartyContactSubmit");
			click(SiebelModeObj.ServiceTab.MaintenancePartyAddress,"Click on MaintenancePartyContactSubmit");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ServiceTab.MaintenancePartyAddresPopupDropdown,"MaintenancePartyAddresPopupDropdown");
			click(SiebelModeObj.ServiceTab.MaintenancePartyAddresPopupDropdown,"Click on MaintenancePartyAddresPopupDropdown");


			verifyExists(SiebelModeObj.ServiceTab.PartyAddresStreetName,"PartyAddresStreetName");
			click(SiebelModeObj.ServiceTab.PartyAddresStreetName,"Click on PartyAddresStreetName");

			verifyExists(SiebelModeObj.ServiceTab.InputPartyAddresStreetName,"InputPartyAddresStreetName");

			sendKeys(SiebelModeObj.ServiceTab.InputPartyAddresStreetName, Maintenance_Address, "Maintenance Address");

			verifyExists(SiebelModeObj.ServiceTab.InputPartyAddresStreetNameSearch,"InputPartyAddresStreetNameSearch");
			click(SiebelModeObj.ServiceTab.InputPartyAddresStreetNameSearch,"Click on InputPartyAddresStreetNameSearch");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.ServiceTab.MaintenancePartyAddressSubmit,"MaintenancePartyAddressSubmit");
			click(SiebelModeObj.ServiceTab.MaintenancePartyAddressSubmit,"Click on IMaintenancePartyAddressSubmit");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.ServiceTab.COLTProContactFullNameLabel,"Click on COLT ProContact FullName Label");
			click(SiebelModeObj.ServiceTab.COLTProContactFullNameLabel,"Click on COLT ProContact FullName Label");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.ServiceTab.OkButton,"Click on Ok Button");
			click(SiebelModeObj.ServiceTab.OkButton,"Click on Ok Button");
			Reusable.waitForSiebelLoader();
		}
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}


	public void PrivateEthernetMiddleAplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException,IOException
	{

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String ServieOrdere_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServieordereReference");
		String Alerting_NotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String A_EndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String B_EndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
		String OSSPlatformFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
		String BandWidthType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");
		String HardModifyFlagType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_DNS_Resolvers");


		if (ProdType.equalsIgnoreCase("DCA Ethernet")) 
		{
			verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Coverage"),"Coverage");
			click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Coverage"),"Coverage");
			verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value", ServieOrdere_Reference),"Coverage");
			click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",ServieOrdere_Reference),"Coverage");
		}
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Connection Type"),"Connection Type");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value", "Connection Type"), "Connection Type");
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",Alerting_NotificationEmailAddress),"Select Connection Type");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",Alerting_NotificationEmailAddress),"Select Connection Type");

		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Service Bandwidth"),"Service Bandwidth");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Service Bandwidth"),"Service Bandwidth");
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",ServiceBandwidth),"Select Service Bandwidth");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",ServiceBandwidth),"Service Bandwidth");

		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","A End Resilience Option"),"A End Resilience Option");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","A End Resilience Option"),"A End Resilience Option");
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",A_EndResilienceOption),"Select A_End Resilience Option");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",A_EndResilienceOption),"A End Resilience Option");

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","B End Resilience Option"),"B End Resilience Option");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","B End Resilience Option"),"B End Resilience Option");
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.BMiddleLi.replace("Value",B_EndResilienceOption),"Select B_End Resilience Option");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.BMiddleLi.replace("Value",B_EndResilienceOption),"B End Resilience Option");

		Reusable.waitForSiebelLoader();
		if (ProdType.equalsIgnoreCase("DCA Ethernet")) 
		{
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Topology"),"Topology");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Topology"),"Topology");
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value","Point to Point"),"Topology");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value","Point to Point"),"Topology");
		}
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","OSS Platform Flag"),"OSS Platform Flag");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","OSS Platform Flag"),"OSS Platform Flag");
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",OSSPlatformFlag),"Select OSS Platform Flag");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",OSSPlatformFlag),"OSS Platform Flag");

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Bandwidth Type"),"Bandwidth Type");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Bandwidth Type"),"Bandwidth Type");
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",BandWidthType),"Select Bandwidth Type");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",BandWidthType),"Bandwidth Type");

		Reusable.waitForSiebelLoader();
		
		//		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Hard Modify Flag"),"Hard Modify Flag");
		//		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleDropDown.replace("Value","Hard Modify Flage"),"Hard Modify Flag");
		//		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",HardModifyFlagType),"Select Hard Modify Flag");
		//		click(SiebelModeObj.PrivateEthernetMiddleApletObj.MiddleLi.replace("Value",HardModifyFlagType),"Hard Modify Flag");

		if(isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) 
		{
			Reusable.waitForElementToAppear(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges, 10);
			click(SiebelModeObj.MandatoryFieldsInHeader.SaveOrderChanges,"Save Order Changes");	
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		}
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
	}

	public void ShowfullInfo() throws InterruptedException,IOException
	{
		verifyExists(SiebelModeObj.PrivateEthernetMiddleApletObj.ShowFullinfo,"Verify Show Full info");
		click(SiebelModeObj.PrivateEthernetMiddleApletObj.ShowFullinfo,"Click On : Show Full info");
		Reusable.waitForSiebelLoader();

	}
	/*public void WaveLineMidleApplet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, DocumentFormatException, IOException
	{

		Reusable.waitForSiebelLoader();
		String ProductType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		if (ProductType.toString().equalsIgnoreCase("Ethernet Line")|| ProductType.toString().equalsIgnoreCase("Wave"))
		{

		//A End Resilience Option
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"A End Resilience Option");
			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"A End Resilience Option");
			Reusable.waitForSiebelLoader();

		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi,"A End Resilience Option Select");
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		sendKeys(SiebelModeObj.WaveLineMidleApplet.MiddleLi, AEndResilienceOption, "A End Resilience Option Select");	


		//B End Resilience Option

		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"B End Resilience Option");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"B End Resilience Option");
		Reusable.waitForSiebelLoader();

		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi,"B End Resilience Option");
		sendKeys(SiebelModeObj.WaveLineMidleApplet.MiddleLi, BEndResilienceOption, "B End Resilience Option Select");	


		//Coverage
		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Coverage");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Coverage");
		Reusable.waitForSiebelLoader();
		String Coverageval = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Coverage");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi,"Coverage");
		sendKeys(SiebelModeObj.WaveLineMidleApplet.MiddleLi, Coverageval, "Coverage");	

		//Service BandWidth
		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Service Bandwidth");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Service Bandwidth");
		Reusable.waitForSiebelLoader();
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi,"Service Bandwidth");
		sendKeys(SiebelModeObj.WaveLineMidleApplet.MiddleLi, ServiceBandwidth, "Service Bandwidth");	


		if(ProductType.toString().equalsIgnoreCase("Wave"))
		{
			//OSS Platform Flag
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Sub Sea Cable System (Protected Path)");
			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Sub Sea Cable System (Protected Path)");
			Reusable.waitForSiebelLoader();
			String AendPostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Postal_Code");
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi,"Sub Sea Cable System (Protected Path)");
			sendKeys(SiebelModeObj.WaveLineMidleApplet.MiddleLi, AendPostalCode, "Sub Sea Cable System (Protected Path)");	


			//Sub Sea Cable System (Worker Path)
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Sub Sea Cable System (Worker Path)");
			click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"Sub Sea Cable System (Worker Path)");
			Reusable.waitForSiebelLoader();
			String AendPremises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Premises");
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi,"Sub Sea Cable System (Worker Path)");
			sendKeys(SiebelModeObj.WaveLineMidleApplet.MiddleLi, AendPremises, "Sub Sea Cable System (Worker Path)");	


		}
		//OSS Platform Flag
		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"OSS Platform Flag");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleDropDown,"OSS Platform Flag");
		Reusable.waitForSiebelLoader();
		String OSSPlatformFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi,"OSS Platform Flag");
		sendKeys(SiebelModeObj.WaveLineMidleApplet.MiddleLi, OSSPlatformFlag, "OSS Platform Flag");	

		Save();
		Reusable.waitForSiebelLoader();
		}
	}


	public void DiversityCircuitEntry(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String TestWindow = read.getExcelColumnValue(testDataFile, sheetName, "Test_Window");
		String Circuit_IPAddressInput = read.getExcelColumnValue(testDataFile, sheetName, "CircuitIPAddressInput");

		verifyExists(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Attachment Link"));
		sendKeys(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Attachment Link"), TestWindow,"Attachment Link");

		verifyExists(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Diverse From Service Reference"));
		sendKeys(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Diverse From Service Reference"), Circuit_IPAddressInput,"Diversity Circuit");

		verifyExists(SiebelModeObj.DiversityCircuitEntryObj.PopClose,"Verify on Close");
		click(SiebelModeObj.DiversityCircuitEntryObj.PopClose,"Click  on Close");

	}
	 */

	public void SiteAServiceParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
	{

		Reusable.waitForSiebelLoader();
		if(isElementPresent(By.xpath("//span[text()='Ok']")))
		{
			verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
			click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
			Reusable.waitForSiebelLoader();
		}
		verifyExists(SiebelModeObj.SiteAServicePartyObj.SiteADropdownClick.replace("Value","Service Party"));
		javaScriptclick(SiebelModeObj.SiteAServicePartyObj.SiteADropdownClick.replace("Value","Service Party"),"Service Party");
		Reusable.waitForSiebelLoader();
	}

	public void PickServiceParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException
	{
		//		String NumberofIPv4Addresses = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_ of_IPv4_Addresses");
		String ServicePartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party_Name");

		Reusable.waitForSiebelLoader();
		Reusable.waitToPageLoad();
		verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Verify on Pick Account Dropdown");
		click(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Click  on Pick Account Dropdown");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.PickServicePartyObj.AList.replace("Value","Party Name"));
		click(SiebelModeObj.PickServicePartyObj.AList.replace("Value","Party Name"));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Verify on Pick Account Dropdown Value");
		click(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Click  on Pick Account Dropdown Value");
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelModeObj.PickServicePartyObj.PickAccountValue,ServicePartyName,"Account Dropdown Value");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountGo,"Verify Go");
		click(SiebelModeObj.PickServicePartyObj.PickAccountGo,"Click on Go");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();

		verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountOk,"Verify Ok");
		click(SiebelModeObj.PickServicePartyObj.PickAccountOk,"Click on Ok");
		Reusable.waitForSiebelLoader();
	}

	public void SiteBServiceParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SiteBServicePartyObj.SiteBDropdownClick.replace("Value","Service Party"));
		click(SiebelModeObj.SiteBServicePartyObj.SiteBDropdownClick.replace("Value","Service Party"));
		Reusable.waitForSiebelLoader();

	}

	public void SiteASiteContact(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SiteAServicePartyObj.SiteADropdownClick.replace("Value","Site Contact"),"Site Contact");
		click(SiebelModeObj.SiteAServicePartyObj.SiteADropdownClick.replace("Value","Site Contact"),"Site Contact");
		Reusable.waitForSiebelLoader();
	}

	/*  for R4 Products */
	public void SiteBSiteContact(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SiteBServicePartyObj.SiteBDropdownClick.replace("Value","Site Contact"));
		click(SiebelModeObj.SiteBServicePartyObj.SiteBDropdownClick.replace("Value","Site Contact"));
		Reusable.waitForSiebelLoader();
	}

	public void PickSiteContactParty(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException 
	{
		String IP_GuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
		String Include_ColtTechnical_ContactInTestCalls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");

		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Verify on Pick Account Dropdown");
		click(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Click  on Pick Account Dropdown");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.PickServicePartyObj.AList.replace("Value","First Name"));
		click(SiebelModeObj.PickServicePartyObj.AList.replace("Value","First Name"));
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Verify on Pick Account Dropdown Value");
		click(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Click  on Pick Account Dropdown Value");
		sendKeys(SiebelModeObj.PickServicePartyObj.PickAccountValue,Include_ColtTechnical_ContactInTestCalls,"Account Dropdown Value");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.PickSiteContactPartyObj.PickContactGo,"Verify Go");
		click(SiebelModeObj.PickSiteContactPartyObj.PickContactGo,"Click on Go");
		Reusable.waitForSiebelLoader();
		Reusable.waitforAttributeloader();

		verifyExists(SiebelModeObj.PickSiteContactPartyObj.PickContactOk,"Verify Ok");
		click(SiebelModeObj.PickSiteContactPartyObj.PickContactOk,"Click on Ok");
		Reusable.waitForSiebelLoader();


	}
	public void SearchSiteA() throws InterruptedException, DocumentFormatException,IOException
	{
		Reusable.waitForpageloadmask();	
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SearchSiteAObj.LeftSiteSearch,"Verify Search Site A");
		click(SiebelModeObj.SearchSiteAObj.LeftSiteSearch,"Click Search Site A");
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();
	}

	public void SearchSiteB() throws InterruptedException, DocumentFormatException,IOException
	{
		Reusable.waitForpageloadmask();	
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SearchSiteBObj.RightSiteSearch,"Verify Search Site B");
		javaScriptclick(SiebelModeObj.SearchSiteBObj.RightSiteSearch,"Click Search Site B");
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();	
	}

	public void SearchSiteAEntry(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException 
	{
		String Aend_StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
		String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName,scriptNo,dataSetNo, "Aend_Premises");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Street Name"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Street Name"),Aend_StreetName,"Street Name");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchDropdown.replace("Value","Country"));
		click(SiebelModeObj.SearchSiteAEntryObj.SearchDropdown.replace("Value", "Country"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value","Country"));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",Country));

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","City / Town"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","City / Town"),CityORTown,"City / Town");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Postal Code"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Postal Code"),PostalCode,"Postal Code");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value", "Premises"),"A end Premises");
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value", "Premises"),Premises,"Premises");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchButton,"Verify Search");
		click(SiebelModeObj.SearchSiteAEntryObj.SearchButton,"Click on Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickAddress1,"Verify PickAddress");
		click(SiebelModeObj.addSiteADetailsObj.PickAddress1,"Click on row and Pick Address");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickBuilding1,"Verify Row and Pick Building");
		click(SiebelModeObj.addSiteADetailsObj.PickBuilding1,"Click on Row and Pick Building");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite1,"Verify Row and Pick Site");
		click(SiebelModeObj.addSiteADetailsObj.PickSite1,"Click on Row and Pick Site");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
	}
	public void SearchSiteBEntry(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException
	{

		String Bend_StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Country");
		String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_City/Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend_Premises");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Street Name"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Street Name"),Bend_StreetName,"Street Name");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchDropdown.replace("Value","Country"));
		click(SiebelModeObj.SearchSiteAEntryObj.SearchDropdown.replace("Value", "Country"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value","Country"));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",Country));

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","City / Town"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","City / Town"),CityORTown,"City / Town");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Postal Code"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Postal Code"),PostalCode,"Postal Code");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value", "Premises"),"B end Premises");
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value", "Premises"),Premises,"Premises");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchButton,"Verify Search");
		click(SiebelModeObj.SearchSiteAEntryObj.SearchButton,"Click on Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickAddress1,"Verify PickAddress");
		click(SiebelModeObj.addSiteADetailsObj.PickAddress1,"Click on row and Pick Address");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickBuilding1,"Verify Row and Pick Building");
		click(SiebelModeObj.addSiteADetailsObj.PickBuilding1,"Click on Row and Pick Building");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite1,"Verify Row and Pick Site");
		click(SiebelModeObj.addSiteADetailsObj.PickSite1,"Click on Row and Pick Site");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();

	}
	public void AEndSite(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException
	{
		Reusable.waitForSiebelLoader();

		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OffnetStr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
		String AendAccessTech = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Access_Technology");
		String ThirdpartyaccessProvidervalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Third_party_access_provider");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String OrderReceivedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Received_Date");
		String OrderSignedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
		String AendSiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Site_ID");
		String AendBuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Building_Type");
		String BCP_Referance = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BCP_Reference");
		String AendCustomerSitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Customer_Site_Pop_Status");
		String Coverage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Coverage");
		String ThirdPartyConnectionReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_Party_Connection_Reference");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		
		//InputData[187].toString())));


		if (ProductName.equalsIgnoreCase("DCA Ethernet") || ProductName.equalsIgnoreCase("Ethernet Access") || ProductName.equalsIgnoreCase("Private Ethernet"))
		{

			if(OffnetStr.equalsIgnoreCase("offnet"))
			{
				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Access Type Drop down");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click on Access type Drop down");
				verifyExists(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("Value",AccessType),"Aend Access Type");
				click(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("Value",AccessType),"Aend Access Type");				

				//Click on save button to populate extra fields//
				verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
				click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");

				Reusable.waitForSiebelLoader();


				// Access Tech
				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Technology"));
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Technology"));

				verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendAccessTech),"Access Technology");
				click(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendAccessTech),"Access Technology");

				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click on ThirdpartyaccessproviderDropDown");
				
				ScrollIntoViewByString(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1);
				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",ThirdpartyaccessProvidervalue),"Third party Access provider");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",ThirdpartyaccessProvidervalue),"Third party Access provider");

				verifyExists(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1," Verify Thirdpartyconectionreference");
				click(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Click on Thirdpartyconectionreference");
				sendKeys(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"no colt reference");

				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Third Party SLA Tier"));
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Third Party SLA Tier"));


				verifyExists(SiebelModeObj.AEndSiteObj.MiddleLi.replace("Value",IPRange));
				click(SiebelModeObj.AEndSiteObj.MiddleLi.replace("Value",IPRange));

				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Verify Thirdparty DropDown");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Click on Thirdparty DropDown");

				verifyExists(SiebelModeObj.AEndSiteObj.MiddleLi.replace("Value",IPRange));
				click(SiebelModeObj.AEndSiteObj.MiddleLi.replace("Value",IPRange));

				verifyExists(SiebelModeObj.AEndSiteObj.SiteTypeDropDown," Verify SiteType DropDown");
				click(SiebelModeObj.AEndSiteObj.SiteTypeDropDown,"Click on SiteType DropDown");
				verifyExists(SiebelModeObj.AEndSiteObj.SiteTypeDropDownvalue," Verify SiteType DropDownvalue");
				click(SiebelModeObj.AEndSiteObj.SiteTypeDropDownvalue,"Click on SiteType DropDownvalue");

				verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
				click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");


				Reusable.waitForSiebelLoader();

			}

			else {

				// Access Type
				//String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");

				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Type"),"Access Type");
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Type"),"Access Type");
				verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",AccessType),AccessType);
				click(SiebelModeObj.AEndSiteObj.AList.replace("Value",AccessType),AccessType);
				Reusable.waitForSiebelSpinnerToDisappear();

				// Access Tech
				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Technology"),"Access Technology");
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Technology"),"Access Technology");
				verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendAccessTech),"A end Access Technology");
				click(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendAccessTech),"A end Access Technology");
				Reusable.waitForSiebelSpinnerToDisappear();

				// EFM
			/*	verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","EFM Enhanced Bandwidth Availability"),"EFM Enhanced Bandwidth Availability");
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","EFM Enhanced Bandwidth Availability"),"EFM Enhanced Bandwidth Availability");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",OrderReceivedDate),"EFM Bandwidth Availability");
				click(SiebelModeObj.AEndSiteObj.AList.replace("Value",OrderReceivedDate),"EFM Bandwidth Availability");
				Reusable.waitForSiebelSpinnerToDisappear();*/
			/*	// Demarcation Device
				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Demarcation Device Required"),"Demarcation Device Required");
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Demarcation Device Required"),"Demarcation Device Required");
				verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",OrderSignedDate),"Demarcation Device Required");
				click(SiebelModeObj.AEndSiteObj.AList.replace("Value",OrderSignedDate),"Demarcation Device Required");
				Reusable.waitForSiebelSpinnerToDisappear();*/

				// Site Type
				
				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Site Type"),"Site Type");
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Site Type"),"Site Type");
				verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value","Customer"),"Site Type");
				click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value","Customer"),"Site Type");
				Reusable.waitForSiebelSpinnerToDisappear();
			}
		}
		verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Building Type"),"Building Type");
		click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Building Type"),"Building Type");
		verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendBuildingType),"A end Building Type");
		click(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendBuildingType),"A end Building Type");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value","BCP Reference"),"BCP Reference");
		click(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value","BCP Reference"),"BCP Reference");
		sendKeys(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value","BCP Reference"), BCP_Referance);
		Reusable.waitForSiebelSpinnerToDisappear();


		verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Customer Site Pop Status"),"Customer Site Pop Status");
		click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Customer Site Pop Status"),"Customer Site Pop Status");
		verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendCustomerSitePopStatus),"Aend Customer Site Pop Status");
		ScrollIntoViewByString(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendCustomerSitePopStatus));
		click(SiebelModeObj.AEndSiteObj.AList.replace("Value",AendCustomerSitePopStatus),"Aend Customer Site Pop Status");
		Reusable.waitForSiebelSpinnerToDisappear();

		/*verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","DSL SLA Class"),"DSL SLA Class");
		click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","DSL SLA Class"),"DSL SLA Class");
		verifyExists(SiebelModeObj.AEndSiteObj.AList.replace("Value",ServiceBandwidth),ServiceBandwidth);
		click(SiebelModeObj.AEndSiteObj.AList.replace("Value",ServiceBandwidth),ServiceBandwidth);
		Reusable.waitForSiebelSpinnerToDisappear();*/

		verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value","Site Name Alias"),"Site Name Alias");
		click(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value","Site Name Alias"),"Site Name Alias");
		//	sendKeys(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value",Coverage), "Site Name Alias");
		sendKeys(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value","Site Name Alias"),Coverage);

		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
		click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
	}

	public void BEndSite(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OffnetStr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String BendAccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_term");
		String BendAccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Access_Technology");
		String Invlid_CLITreatmet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InvlidCLITreatmet");
		String TotalNumber_DDIs = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TotalNumberDDIs");
		String Bend_Building_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Building_Type");
		String BCPReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BCP_Reference");
		String BendCustomerSitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Customer_Site_Pop_Status");
		String SiteNameAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ThirdCodec");
		String DslSlaClass = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TLSCertifiedSupplier");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
		String BendBuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Building_Type");
		String partyaccesprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_party_access_provider");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		
		if(OffnetStr.contains("Offnet"))
		{	
			if (ProductName.equalsIgnoreCase("Private Ethernet"))
			{
				// Access Type
				verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Type"),"Access Type");
				click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Type"),"Access Type");
				verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value","Colt fiber"),"Colt fiber");
				click(SiebelModeObj.BEndSiteObj.BList.replace("Value","Colt fiber"),"Colt fiber");
				
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForSiebelLoader();
				//Click on save button to populate extra fields//
				//verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
				//click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");
				Reusable.savePage();
				Reusable.waitForSiebelLoader();
				
				/*verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Third Party Access Provider"),"Third Party Access Provider");
				click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Third Party Access Provider"),"Third Party Access Provider");
				verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",partyaccesprovider),"Third Party Access Provider");
				click(SiebelModeObj.BEndSiteObj.BList.replace("Value",partyaccesprovider),"Third Party Access Provider");
				Reusable.waitForSiebelSpinnerToDisappear();
				//for 3rd party connection reference//
				
				verifyExists(SiebelModeObj.BEndSiteObj.ThirdpartyconectionreferenceB,"ThirdpartyconectionreferenceB");
				sendKeys(SiebelModeObj.BEndSiteObj.ThirdpartyconectionreferenceB,"12","ThirdpartyconectionreferenceB");
				Reusable.waitForSiebelSpinnerToDisappear();
				
				// Third Party SLA Tier
				
				verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Third Party SLA Tier"),"Third Party SLA Tier");
				click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Third Party SLA Tier"),"Third Party SLA Tier");
				verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",IPRange),IPRange);
				click(SiebelModeObj.BEndSiteObj.BList.replace("Value",IPRange),IPRange);
				Reusable.waitForSiebelSpinnerToDisappear();
				*/
			}
			//			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			//			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");
			//			Reusable.waitForSiebelLoader();
			//			
			//For access type technology//
			verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Technology"));
			click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Technology"));

			verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",BendAccessTechnology));
			click(SiebelModeObj.BEndSiteObj.BList.replace("Value",BendAccessTechnology));

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");

			Reusable.waitForSiebelLoader();
		}//out of if inner loop

		else
		{
			if (ProductName.equalsIgnoreCase("Private Ethernet"))
			{
				// Access Type
				verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Type"),"Access Type");
				click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Type"),"Access Type");
				verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",AccessType),AccessType);
				click(SiebelModeObj.BEndSiteObj.BList.replace("Value",AccessType),AccessType);
				Reusable.waitForSiebelSpinnerToDisappear();
				
			}
			if (ProductName.equalsIgnoreCase("DCA Ethernet")||(ProductName.equalsIgnoreCase("Private Ethernet")))
			{
				// Access Tech
				verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Technology"));
				click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Access Technology"));

				verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",BendAccessTechnology));
				click(SiebelModeObj.BEndSiteObj.BList.replace("Value",BendAccessTechnology));

				// EFM
			/*	verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","EFM Enhanced Bandwidth Availability"));
				click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","EFM Enhanced Bandwidth Availability"));

				verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",TotalNumber_DDIs));
				click(SiebelModeObj.BEndSiteObj.BList.replace("Value",TotalNumber_DDIs));
            */
			}
		}
		verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Building Type"));
		click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Building Type"));

		verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",Bend_Building_Type));
		click(SiebelModeObj.BEndSiteObj.BList.replace("Value",Bend_Building_Type));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteInput.replace("Value","BCP Reference"));
		click(SiebelModeObj.BEndSiteObj.BEndSiteInput.replace("Value","BCP Reference"));
		sendKeys(SiebelModeObj.BEndSiteObj.BEndSiteInput.replace("Value","BCP Reference"),BCPReference);
		Reusable.waitForSiebelLoader();
		

		verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Customer Site Pop Status"));
		click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","Customer Site Pop Status"));

		verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",BendCustomerSitePopStatus));
		click(SiebelModeObj.BEndSiteObj.BList.replace("Value",BendCustomerSitePopStatus));

		verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteInput.replace("Value","Site Name Alias"));
		click(SiebelModeObj.BEndSiteObj.BEndSiteInput.replace("Value","Site Name Alias"));
		sendKeys(SiebelModeObj.AEndSiteObj.AEndSiteInput.replace("Value","Site Name Alias"),SiteNameAlias);
		Reusable.waitForSiebelLoader();

	/*	verifyExists(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","DSL SLA Class"));
		click(SiebelModeObj.BEndSiteObj.BEndSiteDropDown.replace("Value","DSL SLA Class"));

		verifyExists(SiebelModeObj.BEndSiteObj.BList.replace("Value",DslSlaClass));
		click(SiebelModeObj.BEndSiteObj.BList.replace("Value",DslSlaClass));*/

	}

	/*public void addSiteADetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
	{
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		if(ProdType.toString().equalsIgnoreCase("Wave")||ProdType.toString().equalsIgnoreCase("Ethernet Line"))
		{
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA, "Search Address SiteA");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA,"Click on Search Address SiteA");
			verifyExists(SiebelModeObj.addSiteADetailsObj.StreetNamerfs,"Enter Street Name");

			String Street_Namerfs = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Street_Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.StreetNamerfs, Street_Namerfs, "Enter Street Name");	

			verifyExists(SiebelModeObj.addSiteADetailsObj.Country,"Verify Country");
			click(SiebelModeObj.addSiteADetailsObj.Country,"Click on Country");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection,"Verify SiteABSelection");

			String AendCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
			click(AendCountry,"Click on A end Country");
			sendKeys(SiebelModeObj.addSiteADetailsObj.StreetNamerfs, AendCountry, "Enter Street Name");	

			String Aend_City_Town = read.getExcelColumnValue(testDataFile, sheetName, "Aend_CityORTown");
			verifyExists(SiebelModeObj.addSiteADetailsObj.City,"A end City");
			sendKeys(SiebelModeObj.addSiteADetailsObj.City, Aend_City_Town);

			///Postal Code
			String PostalCode = read.getExcelColumnValue(testDataFile, sheetName, "Aend_Postal_Code");
			verifyExists(SiebelModeObj.addSiteADetailsObj.PostalCode,"A end Postal Code");
			sendKeys(SiebelModeObj.addSiteADetailsObj.PostalCode, PostalCode);

			///Premise
			String Premises = read.getExcelColumnValue(testDataFile, sheetName, "Aend_Premises");
			verifyExists(SiebelModeObj.addSiteADetailsObj.Premises,"A end Premises");
			sendKeys(SiebelModeObj.addSiteADetailsObj.Premises, Premises);

			///
			verifyExists(SiebelModeObj.addSiteADetailsObj.Search,"Verify Search");
			click(SiebelModeObj.addSiteADetailsObj.Search,"Click on Search");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");

			verifyExists(SiebelModeObj.addSiteADetailsObj.PickAddress," Verify Pick Address");
			click(SiebelModeObj.addSiteADetailsObj.PickAddress," Click on Pick Address");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Search AddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on Search Address RowSelection");

			verifyExists(SiebelModeObj.addSiteADetailsObj.PickBuilding,"verify Pick Building");
			click(SiebelModeObj.addSiteADetailsObj.PickBuilding,"Click on Row and Pick Building");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on Search Address RowSelection");

		    verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
			click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Service Party Search Access");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click on Service Party SearchAccess");

			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify Service party drop down Acccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click on Service party drop down Acccess");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify Site Contact drop down Acccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click on site Contact drop down Acccess");
			//String Premises = read.getExcelColumnValue(testDataFile, sheetName, "Aend_Premises");
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection,"Verify SiteABSelection");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection,"Click on SiteABSelection");

			String InputPartyName = read.getExcelColumnValue(testDataFile, sheetName, "Auto_Mitigation");
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, InputPartyName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Part Name");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click on search");



			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify Submit");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");

			if (isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) 
			{
				verifyExists(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"verify Save order changes");
				click(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"Click on Save order Changes");
				Reusable.waitForSiebelLoader();
				System.out.println("page load succesfuly now come to middle applet");
				Reusable.waitForSiebelLoader();
			}
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"verify Site Contact Search Access");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click on Site Contact Search");

			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection,"");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection,"");

			verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Site name");
			String sitename = read.getExcelColumnValue(testDataFile, sheetName, "Site_Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess, sitename);

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Last name site search Access");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess, "Last name site search Access");
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Last name site search submit");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess, "Click On Submit");

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave);
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave);

			Reusable.waitForSiebelLoader();
		}
	}*/	
	public void addSiteBDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{


		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String BendCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Country");
		String Street_NamerfsBend = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Street_Name");
		String Bend_City_Town = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_CityORTown");
		String PostalCodeB_End = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend_Postal_Code");
		String PremisesBEnd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend_Premises");
		String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Test_Window");
		String IncludeColtTechnicalContactinTestCalls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Include_Colt_Technical_Contact_in_Test_Calls");



		if(ProdType.toString().equalsIgnoreCase("Wave")||ProdType.toString().equalsIgnoreCase("Ethernet Line"))
		{
		/*	verifyExists(SiebelModeObj.addSiteBDetailsObj.SearchAddressSiteB, "Search Address Site B");
			click(SiebelModeObj.addSiteBDetailsObj.SearchAddressSiteB,"Click on Search Address Site B");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.StreetNamerfs,"Enter Street Name");

			sendKeys(SiebelModeObj.addSiteADetailsObj.StreetNamerfs,Street_NamerfsBend, "Enter Street Name");	

			verifyExists(SiebelModeObj.addSiteADetailsObj.Country,"Verify Country");
			click(SiebelModeObj.addSiteADetailsObj.Country,"Click on Country");
			Reusable.waitForSiebelLoader();
			//		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", Inputdata[104].toString()),"Verify SiteABSelection");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", BendCountry),"SiteABSelection");
			//sendKeys(SiebelModeObj.addSiteADetailsObj.StreetNamerfs, AendCountry, "Enter Street Name");	
			verifyExists(SiebelModeObj.addSiteADetailsObj.City,"B end City");
			sendKeys(SiebelModeObj.addSiteADetailsObj.City, Bend_City_Town);

			///Postal Code
			verifyExists(SiebelModeObj.addSiteADetailsObj.PostalCode,"B end Postal Code");
			sendKeys(SiebelModeObj.addSiteADetailsObj.PostalCode, PostalCodeB_End);

			///Premise
			verifyExists(SiebelModeObj.addSiteADetailsObj.Premises,"B end Premises");
			sendKeys(SiebelModeObj.addSiteADetailsObj.Premises, PremisesBEnd);

			///
			verifyExists(SiebelModeObj.addSiteADetailsObj.Search,"Verify Search");
			click(SiebelModeObj.addSiteADetailsObj.Search,"Click on Search");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PickAddress," Verify Pick Address");
			click(SiebelModeObj.addSiteADetailsObj.PickAddress," Click on Pick Address");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Search AddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PickBuilding,"verify Pick Building");
			click(SiebelModeObj.addSiteADetailsObj.PickBuilding,"Click on Row and Pick Building");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
			click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on Search Address RowSelection");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
			click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
		 	*/
			addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.addSiteBDetailsObj.ServicePartySearchSiteB,"Service Party Search Site B");
			mouseMoveOn(SiebelModeObj.addSiteBDetailsObj.ServicePartySearchSiteB);
			click(SiebelModeObj.addSiteBDetailsObj.ServicePartySearchSiteB,"Search Service Party");
			Reusable.waitForSiebelLoader();
			/*			
			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Service Party Search Access");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click on Service Party SearchAccess");

			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify Service party drop down Acccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click on Service party drop down Acccess");
			 */
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify Site Contact drop down Acccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click on site Contact drop down Acccess");
			//String Premises = read.getExcelColumnValue(testDataFile, sheetName, "Aend_Premises");
			Reusable.waitForSiebelLoader();
			//	verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", Inputdata[31].toString()),"Verify SiteABSelection");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", IncludeColtTechnicalContactinTestCalls),"Click on SiteABSelection");
			Reusable.waitForSiebelLoader();

			String InputPartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Auto_Mitigation");
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, InputPartyName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Part Name");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click on search");
			Reusable.waitForSiebelLoader();


			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify Submit");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			if (isElementPresent(By.xpath("//a[text()='Click here to save your order changes.']"))) 
			{
				verifyExists(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"verify Save order changes");
				click(SiebelModeObj.addSiteADetailsObj.SaveOrderChanges,"Click on Save order Changes");
				Reusable.waitForSiebelLoader();
				System.out.println("page load succesfuly now come to middle applet");
				Reusable.waitForSiebelLoader();
			}
			waitForElementToAppear(SiebelModeObj.addSiteBDetailsObj.SiteContactSearchSiteB, 8);
			verifyExists(SiebelModeObj.addSiteBDetailsObj.SiteContactSearchSiteB,"verify Site Contact Search Access");
			click(SiebelModeObj.addSiteBDetailsObj.SiteContactSearchSiteB,"Click on Site Contact Search SiteB");
			Reusable.waitForSiebelLoader();
			//		waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess, 8);
			//		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"verify Site Contact Search Access");
			//		click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click on Site Contact Search");

			waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess, 8);
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"SiteContactDropdownAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"SiteContactDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", TestWindow),"Enter Service");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", TestWindow),"Enter Service");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Site name");
			String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess, sitename);

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Last name site search Access");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess, "Last name site search Access");
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Last name site search submit");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess, "Click On Submit");

			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave);
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave);

			Reusable.waitForSiebelSpinnerToDisappear();


			Reusable.waitForSiebelLoader();
		}	




	}

	public void PrivateEthernetEntry(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException,IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Servie_Ordere_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServieordereReference");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String A_EndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		String B_EndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");

		Reusable.waitForSiebelLoader();
		//		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","Service Bandwidth"),"Service Bandwidth");
		click(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","Service Bandwidth"),"Service Bandwidth");
		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", ServiceBandwidth),ServiceBandwidth);
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",ServiceBandwidth),ServiceBandwidth);
		Reusable.waitForSiebelSpinnerToDisappear();

		if (ProductName.contains("Ethernet Access")) {
			verifyExists(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","Coverage"),"Coverage");
			click(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","Coverage"),"Coverage");
			verifyExists(SiebelModeObj.PrivateEthernetEntryObj.InsideDropdownValues.replace("Value", Servie_Ordere_Reference),Servie_Ordere_Reference);
			click(SiebelModeObj.PrivateEthernetEntryObj.InsideDropdownValues.replace("Value",Servie_Ordere_Reference),Servie_Ordere_Reference);
			Reusable.waitForSiebelSpinnerToDisappear();
		}

		verifyExists(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","A End Resilience Option"),"A End Resilience Option");
		click(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","A End Resilience Option"),"A End Resilience Option");
		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", A_EndResilienceOption),A_EndResilienceOption);
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",A_EndResilienceOption),A_EndResilienceOption);
		Reusable.waitForSiebelSpinnerToDisappear();

		if (!ProductName.contains("Ethernet Access")) {
			verifyExists(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","B End Resilience Option"));
			click(SiebelModeObj.PrivateEthernetEntryObj.PopDropdownClick.replace("Value","B End Resilience Option"));
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", B_EndResilienceOption),"A End Resilience Option");
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",B_EndResilienceOption));
			Reusable.waitForSiebelSpinnerToDisappear();
		}
	}


	public void AEndDropdownSelection(String DropdownName, String DropdownValue) throws InterruptedException,IOException
	{

		String eleLoct=SiebelModeObj.enterMandatoryDetailsInMiddleApple.SiteADropdownClick.replace("Value", DropdownName);
		waitForElementToAppear(eleLoct, 8);
		click(eleLoct);

		eleLoct= SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value", DropdownValue);
		Reusable.waitForSiebelLoader();

		waitForElementToAppear(eleLoct, 8);
		click(eleLoct);

	}


	/*	public void ModTechModCommWaveAndLine(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		waitToPageLoad();		
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		if (ProductName.toString().equalsIgnoreCase("Ethernet Line")&&ProductName.toString().equalsIgnoreCase("Wave"))
		{

			Reusable.ClickHereSave();
			// A End And B End Installation Entries Start
			String InstallTime95 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
			Reusable.AEndDropdownSelection("Aend_Install_Time",InstallTime95);
			String ColName123 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InvlidCLITreatmet");
			Reusable.AEndInputEnter("Access Notification Period",ColName123);
			String ColName125 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutingSequence");
			Reusable.AEndInputEnter("Access Time Window",ColName125);

			String BInstallTime112 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");
			Reusable.BEndDropdownSelection("Bend_Install_Time",BInstallTime112);
			String ColName124 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TotalNumberDDIs");
			Reusable.AEndInputEnter("Access Notification Period",ColName124);
			String ColName126 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FirstCodec");
			Reusable.AEndInputEnter("Access Time Window",ColName126);

			String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
			if (ProductName.equalsIgnoreCase("Ethernet Line")&&Comments.equalsIgnoreCase("Tech"))
			{
				//Update bandwidth with new Values
				//Service BandWidth
				verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown,"MiddleDropDown");
				click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "Service Bandwidth"));
				waitToPageLoad();		

				String ColName40 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
				click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleLi.replace("Value", ColName40));
			}

			if (ProductName.equalsIgnoreCase("Wave")&&Comments.equalsIgnoreCase("Tech"))
			{
				//Update bandwidth with new Values
				//Service BandWidth
				verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown,"MiddleDropDown");
				click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "A End Resilience Option"));
				waitToPageLoad();		

				String ColName76 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
				click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleLi.replace("Value", ColName76));
			}


			}


	}*/

	/////////////////////k 10 dec


	public void SiteAInstallationTime(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException,IOException
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Aend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet")||ProductName.equalsIgnoreCase("Ethernet Access")) 
		{
			verifyExists(SiebelModeObj.SiteAServicePartyObj.SiteADropdownClick.replace("Value","Install Time"),"Install Time");
			click(SiebelModeObj.SiteAServicePartyObj.SiteADropdownClick.replace("Value","Install Time"),"Install Time");
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",Aend_Install_Time),Aend_Install_Time);
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",Aend_Install_Time),Aend_Install_Time);
			Reusable.waitForSiebelSpinnerToDisappear();
		}

	}

	public void SiteBInstallationTime(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet")) 
		{
			verifyExists(SiebelModeObj.installationTimeUpdate.SiteBDropdownClick.replace("Value","Install Time"));
			click(SiebelModeObj.installationTimeUpdate.SiteBDropdownClick.replace("Value","Install Time"));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",Bend_Install_Time));
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
		}
	}

	public void SiteATerminationTime(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException 
	{
		Random rand = new Random();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String FourthCodec = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Cabinet_Type");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet") || ProductName.equalsIgnoreCase("Ethernet Access")) 
		{
			int rand_int1 = rand.nextInt(1000);

			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Cabinet ID"),"Cabinet ID");
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Cabinet ID"),String.valueOf(rand_int1));	
			//sendKeys(SiebelModeObj.EnterServiceChargeInFooter.BillingRowAmountInput.replace("Value", String.valueOf(i)), Amount);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.installationTimeUpdate.SiteADropdownClick.replace("Value","Cabinet Type"),"Cabinet Type");
			click(SiebelModeObj.installationTimeUpdate.SiteADropdownClick.replace("Value","Cabinet Type"),"Cabinet Type");
			verifyExists(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",FourthCodec),FourthCodec);
			click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",FourthCodec),FourthCodec);
			Reusable.waitForSiebelSpinnerToDisappear();

			if(!ProductName.contains("Ethernet Access"))
			{
				waitToPageLoad();
				verifyExists(SiebelModeObj.installationTimeUpdate.SiteADropdownClick.replace("Value","Link Aggregation Required"),"Link Aggregation Required");
				click(SiebelModeObj.installationTimeUpdate.SiteADropdownClick.replace("Value","Link Aggregation Required"),"Link Aggregation Required");
				verifyExists(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value","No"),"No");
				click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value","No"),"No");
				Reusable.waitForSiebelSpinnerToDisappear();
			}

			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Shelf ID"),"Shelf ID");
			//	sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", String.valueOf(rand_int1)),"Shelf ID");	
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Shelf ID"),String.valueOf(rand_int1));	

			Reusable.waitForSiebelSpinnerToDisappear();
		}	
	}

	public void SiteBTerminationTime(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException 
	{
		Random rand = new Random();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Bend_Cabinet_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Cabinet_Type");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet"))
		{
			int rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Cabinet ID"));
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", "Cabinet ID"),String.valueOf(rand_int1));	
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.installationTimeUpdate.SiteBDropdownClick.replace("Value","Cabinet Type"));
			click(SiebelModeObj.installationTimeUpdate.SiteBDropdownClick.replace("Value","Cabinet Type"));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",Bend_Cabinet_Type),Bend_Cabinet_Type);
			click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",Bend_Cabinet_Type),Bend_Cabinet_Type);
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			

			verifyExists(SiebelModeObj.installationTimeUpdate.SiteBDropdownClick.replace("Value","Link Aggregation Required"));
			click(SiebelModeObj.installationTimeUpdate.SiteADropdownClick.replace("Value","Link Aggregation Required"));
			click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value","No"));
			waitToPageLoad();

			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Shelf ID"));
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", "Shelf ID"),String.valueOf(rand_int1));	
			waitToPageLoad();

		}
	}
	public void SiteAAccessPort(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException 
	{
		Random rand = new Random();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Aend_Presentation_Interface99 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Presentation_Interface");
		String Connector_Type54 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
		String Fibre_Type55 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
		String Aend_VLAN_Tagging_Mode100 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FourthCodec");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet")|| ProductName.equalsIgnoreCase("Ethernet Access")) 
		{
			int rand_int1 = rand.nextInt(1000);
			// Presentation Interface
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Presentation Interface"),"Presentation Interface");
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Presentation Interface"),"Presentation Interface");
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Aend_Presentation_Interface99),Aend_Presentation_Interface99);
			Reusable.waitForSiebelSpinnerToDisappear();

			// Connector Type
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Connector Type"),"Connector Type");
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Connector Type"),"Connector Type");
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Connector_Type54),Connector_Type54);
			Reusable.waitForSiebelSpinnerToDisappear();

			// Fibre Type
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Fibre Type"),"Fibre Type");
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Fibre Type"),"Fibre Type");
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Fibre_Type55),Fibre_Type55);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			// VLAN Tagging Mode
			/*ScrollIntoViewByString(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","VLAN Tagging Mode"));
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","VLAN Tagging Mode"),"VLAN Tagging Mode");
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","VLAN Tagging Mode"),"VLAN Tagging Mode");
			Reusable.waitForSiebelSpinnerToDisappear();
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Aend_VLAN_Tagging_Mode100),Aend_VLAN_Tagging_Mode100);
			Reusable.waitForSiebelSpinnerToDisappear();*/

			// Physical Port ID
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Physical Port ID"),"Physical Port ID");
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", "Physical Port ID"),Integer.toString(rand_int1));	
			Reusable.waitForSiebelSpinnerToDisappear();

			// Slot ID
			rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Slot ID"),"Slot ID");
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Slot ID"),Integer.toString(rand_int1));	
			Reusable.waitForSiebelSpinnerToDisappear();	
		}
	}

	public void SiteBAccessPort(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException 
	{
		Random rand = new Random();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Bend_Presentation_Interface116 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Presentation_Interface");
		String Connector_Type54 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
		String FibreType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
		String Aend_VLAN_Tagging_Mode100 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FourthCodec");

		if (ProductName.equalsIgnoreCase("Private Ethernet") || ProductName.equalsIgnoreCase("DCA Ethernet")) 
		{
			// Presentation Interface
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Presentation Interface"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Presentation Interface"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Bend_Presentation_Interface116),Bend_Presentation_Interface116);
			waitToPageLoad();

			// Connector Type
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Connector Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Connector Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Connector_Type54),Connector_Type54);
			waitToPageLoad();

			// Fibre Type
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Fibre Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Fibre Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",FibreType),FibreType);
			waitToPageLoad();

			// VLAN Tagging Mode
		/*	verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","VLAN Tagging Mode"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","VLAN Tagging Mode"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value",Aend_VLAN_Tagging_Mode100));
			waitToPageLoad();
		 */
			// Physical Port ID
			int rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Physical Port ID"));
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", "Physical Port ID"),Integer.toString(rand_int1));	
			waitToPageLoad();

			// Slot ID
			rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Slot ID"));
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", "Slot ID"),Integer.toString(rand_int1));	
			waitToPageLoad();		
		}

	}

	public void SiteBInstallationTimePUD(String testDataFile,String sheetName,String scriptNo,String dataSetNo)throws InterruptedException, IOException
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Dark Fibre")) || (ProductName.equalsIgnoreCase("Ultra Low Latency")) || (ProductName.equalsIgnoreCase("Private Wave Service"))) 
		{
			Reusable.waitForSiebelLoader();
			// Entering Install Time
			verifyExists(SiebelModeObj.SiteBInstallationTimePUD.SiteBDropdownClick.replace("Value","Install Time"),"SiteADropdownClick");
			click(SiebelModeObj.SiteBInstallationTimePUD.SiteBDropdownClick.replace("Value","Install Time"));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SiteBInstallationTimePUD.SiteABSelection.replace("Value",Bend_Install_Time),"SiteABSelection");
			click(SiebelModeObj.SiteBInstallationTimePUD.SiteABSelection.replace("Value",Bend_Install_Time));
			Reusable.waitForSiebelLoader();

		}

	}

	public void SiteADiversityCircuitConfig(String testDataFile,String sheetName,String scriptNo,String dataSetNo)	throws InterruptedException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String NATRequired151 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NATRequired");
		String NNIWANIPv4Address152 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NNIWANIPv4Address");
		String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
		Reusable.waitForSiebelLoader();


		switch (ProductName) 
		{
		case "Private Ethernet": 
		{
			// Entering Customer Dedicated Access Ring
			waitToPageLoad();		
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Customer Dedicated Access Ring"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Customer Dedicated Access Ring"));
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value","NATRequired"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",NATRequired151));
			waitToPageLoad();		

			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Dual Customer Power Source"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick.replace("Value","Dual Customer Power Source"));
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value","NNIWANIPv4Address"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",NNIWANIPv4Address152));
			waitToPageLoad();	
			break;
		}

		case "Ethernet Access": 
		{
			Reusable.waitForSiebelLoader();	
			verifyExists(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Attachment Link"),"Attachment Link");
			click(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Attachment Link"),"Attachment Link");
			sendKeys(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value", "Attachment Link"),TestWindow,TestWindow);

			waitToPageLoad();
			Reusable.waitForSiebelSpinnerToDisappear();	
			verifyExists(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Diverse From Service Reference"),"Diverse From Service Reference");
			click(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Diverse From Service Reference"),"Diverse From Service Reference");
			sendKeys(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value", "Diverse From Service Reference"),"already exist");
			Reusable.waitForSiebelSpinnerToDisappear();	
			break;

		}
		case "Private Wave Service": 
		{
					
			waitToPageLoad();	
			verifyExists(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Attachment Link"));
			click(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Attachment Link"));
			sendKeys(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value", "Attachment Link"),"Ayush.colt.net");

			waitToPageLoad();	
			verifyExists(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Diverse From Service Reference"));
			click(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value","Diverse From Service Reference"));
			sendKeys(SiebelModeObj.DiversityCircuitEntryObj.PopInput.replace("Value", "Diverse From Service Reference"),"already exist");
			break;
		}
		default: 
		{}

		}

	}

	public void SiteAAccessPortPUD(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException 
	{


		Random rand = new Random();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Aend_Presentation_Interface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Presentation_Interface");
		String Aend_VLAN_Tagging_Mode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_VLAN_Tagging_Mode");
		String Connector_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
		String Fibre_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
		String Aend_VLAN_Port_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_VLAN_Port_ID"); 
		String Third_party_access_provider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Third_party_access_provider");
		String IP_Addressing_Format = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Addressing_Format");

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Ultra Low Latency"))) 
		{
			int rand_int1 = rand.nextInt(1000);
			// Presentation Interface
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","Presentation Interface"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","Presentation Interface"),"Presentation Interface");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Aend_Presentation_Interface),"select Presentation Interface");
			Reusable.waitForSiebelLoader();
			if ((ProductName.equalsIgnoreCase("Private Ethernet"))) 
			{
				// VLAN Tagging Mode
				waitToPageLoad();
				verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","VLAN Tagging Mode"));
				click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","VLAN Tagging Mode"));

				Reusable.waitForSiebelLoader();
				click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Aend_VLAN_Tagging_Mode));
				Reusable.waitForSiebelLoader();
			}
		}
		if ((ProductName.equalsIgnoreCase("Dark Fibre")) || (ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Ultra Low Latency")) || (ProductName.equalsIgnoreCase("Private Wave Service"))) 
		{
			int rand_int1 = rand.nextInt(1000);
			// Connector Type
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","Connector Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","Connector Type"),"Connector Type");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Connector_Type),"select Connector Type");
			Reusable.waitForSiebelLoader();
			// Fibre Type
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","Fibre Type"),"Fibre Type");
			javaScriptclick(SiebelModeObj.ModTechModCommWaveAndLine.SiteADropdownClick1.replace("Value","Fibre Type"),"Fibre Type");
			Reusable.waitForSiebelLoader();
			javaScriptclick(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Fibre_Type),"select Fibre Type");
			Reusable.waitForSiebelLoader();
			// Physical Port ID
			//waitToPageLoad();
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Physical Port ID"),"Physical Port ID");
			if (Aend_VLAN_Port_ID.toString() == "") 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Physical Port ID" ), Integer.toString(rand_int1));	
			}
			else 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", "Physical Port ID"),Aend_VLAN_Port_ID,"enter Physical port ID");	

			}
			waitToPageLoad();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			// Slot ID
			rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value","Slot ID"));
			if (IP_Addressing_Format.toString() == "") 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", Integer.toString(rand_int1)),"Slot ID");	
			}
			else 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", "Slot ID"),IP_Addressing_Format,"enter Slot ID");	

			}
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
		}	
	}

	public void SiteBAccessPortPUD(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException 
	{
		Random rand = new Random();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Connector_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
		String Fibre_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
		String Bend_Port_Role = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Port_Role");
		String Ipv_Addressing_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ipv_Addressing_Type");
		String Bend_Presentation_Interface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Presentation_Interface");

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Dark Fibre")) || (ProductName.equalsIgnoreCase("Private Wave Service")) || (ProductName.equalsIgnoreCase("Ultra Low Latency"))) 
		{
			// Connector Type
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Connector Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Connector Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Connector_Type));

			// Fibre Type
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Fibre Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Fibre Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Fibre_Type));

			// Physical Port ID
			int rand_int1 = rand.nextInt(1000);
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Physical Port ID"));
			if (Bend_Port_Role.toString() == "") 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Physical Port ID"),Integer.toString(rand_int1));	
			}
			else 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Physical Port ID" ),Bend_Port_Role);	

			}
			waitToPageLoad();

			// Slot ID
			rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Slot ID"));
			if (Ipv_Addressing_Type.toString() == "") 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Slot ID" ),Integer.toString(rand_int1));	
			}
			else 
			{
				sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value","Slot ID" ),Ipv_Addressing_Type);	

			}
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Ultra Low Latency"))) 
		{
			// Presentation Interface
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Presentation Interface"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Presentation Interface"));
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value",Bend_Presentation_Interface));
			Reusable.waitForSiebelLoader();
			if ((ProductName.equalsIgnoreCase("Private Ethernet"))) 
			{
				// VLAN Tagging Mode
				waitToPageLoad();
				verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","VLAN Tagging Mode"));
				click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","VLAN Tagging Mode"));
				waitToPageLoad();

			}
		}
	}

	public void SiteBTerminationTimePUD(String testDataFile,String sheetName,String scriptNo,String dataSetNo)throws InterruptedException, IOException
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		Random rand = new Random();

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Dark Fibre"))|| (ProductName.equalsIgnoreCase("Dark Fibre"))|| (ProductName.equalsIgnoreCase("Ultra Low Latency")) || (ProductName.equalsIgnoreCase("Private Wave Service"))) 
		{
			// CabinetID
			int rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteAInput.replace("Value", "Cabinet ID"),"SiteBInput");
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", "Cabinet ID"),Integer.toString(rand_int1));
			Reusable.waitForSiebelLoader();
			// CabinetType
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Cabinet Type"));
			click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value","Customer Cabinet"));
			Reusable.waitForSiebelLoader();

			// LinkAggregationRequired
			if (!(ProductName.contains("Ultra Low Latency")) && (!ProductName.contains("Dark Fibre")) && !(ProductName.contains("Private Wave Service")))
			{
				click(SiebelModeObj.ModTechModCommWaveAndLine.SiteBDropdownClick.replace("Value","Link Aggregation Required"));
				click(SiebelModeObj.ModTechModCommWaveAndLine.SiteABSelection.replace("Value","No"));
				Reusable.waitForSiebelLoader(); 
			}

			// Shelf ID
			rand_int1 = rand.nextInt(1000);
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", "Shelf ID"),"SiteAInput");
			sendKeys(SiebelModeObj.ModTechModCommWaveAndLine.SiteBInput.replace("Value", "Shelf ID"),Integer.toString(rand_int1));
			Reusable.waitForSiebelLoader();
		}	

	}

	public void CSPInterconnectSiteAEntry(String testDataFile,String sheetName,String scriptNo,String dataSetNo)throws InterruptedException, IOException
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.PopPlus,"Add Sign");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.PopPlus,"Add Sign");
		Reusable.waitForSiebelLoader();

		// Attribute
		verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.MiddleLi.replace("Value", "CSP interconnect A-end"));
		Reusable.waitForSiebelLoader();

		// Attribute
		verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.MiddleLi.replace("Value", "Dedicated Port"));
		Reusable.SendkeaboardKeys(SiebelModeObj.CSPInterconnectSiteAEntry.AttributeValue, Keys.ENTER);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK,"OK Clicked");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK,"OK Clicked");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
	}

	public void CSPInterconnectSiteBEntry(String testDataFile,String sheetName,String scriptNo,String dataSetNo)throws InterruptedException, IOException
	{
		// Attribute
		verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.PopAttributeDrodown,"Add Sign");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.MiddleLi.replace("Value", "Dedicated Port"));
		//		Reusable.SendkeaboardKeys(SiebelModeObj.CSPInterconnectSiteAEntry.AttributeValue, Keys.ENTER);
		Reusable.waitForSiebelLoader();
		Reusable.ScrollIntoViewByString(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK);
		verifyExists(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK,"OK Clicked");
		click(SiebelModeObj.CSPInterconnectSiteAEntry.SiteASettingOK,"OK Clicked");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();

	}

	public void ClickSection(String SectionName) throws InterruptedException, IOException
	{
		//String temp=SiebelModeObj.R5DataCoverage.SectionSelection.replace("Value",SectionName);
		verifyExists(SiebelModeObj.R5DataCoverage.SectionSelection.replace("Value",SectionName),SectionName);
		click(SiebelModeObj.R5DataCoverage.SectionSelection.replace("Value",SectionName),SectionName);
		Reusable.waitForSiebelSpinnerToDisappear();
	}



	public void R5DataCoverage1(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception 
	{
		String Auto_Mitigation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");
		String Customer_DNS_Resolvers = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_DNS_Resolvers");
		String Alerting_Notification_Email_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");

		String LocalValue=null;
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.R5DataCoverage.EnableFeature,"Show Full Info Access");
		mouseMoveOn(SiebelModeObj.R5DataCoverage.EnableFeature);
		click(SiebelModeObj.R5DataCoverage.ClickShowFullInfoAccess,"Show Full Info Access");
		Reusable.waitForSiebelLoader();
		
		ClickSection("Consultancy Services");
		verifyExists(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Consultancy Service Type"),"Consultancy Service Type drop down");
		click(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Consultancy Service Type"),"Consultancy Service Type drop down");
		click(SiebelModeObj.R5DataCoverage.SelectValueDropdown.replace("Value", Alerting_Notification_Email_Address),Alerting_Notification_Email_Address);
		Reusable.waitForSiebelSpinnerToDisappear();

		//String temp1=SiebelModeObj.R5DataCoverage.InputBox.replace("Value", "Statement of Work").replace("-1", "1");
		//LocalValue=RandomName();


		//sendKeys(temp1, LocalValue, "Statement of Work");
		//Reusable.SendkeaboardKeys(temp1, Keys.ENTER);
	//	Reusable.waitForSiebelSpinnerToDisappear();
		//Reusable.SendkeaboardKeys(temp1, Keys.TAB);
		String temp2=SiebelModeObj.R5DataCoverage.InputBox.replace("Value", "Number of Days").replace("-1", "1");
		LocalValue=Integer.toString(rnd.nextInt(1000));

		sendKeys(temp2,LocalValue, "Number of Days");
		//Reusable.SendkeaboardKeys(Keys.ENTER);
		Reusable.waitForSiebelSpinnerToDisappear();
		
		ClickSection("Service Management");

		verifyExists(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type"),"Package Type drop down");
		click(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type"),"Package Type drop down");
		click(SiebelModeObj.R5DataCoverage.SelectValueDropdown.replace("Value", Auto_Mitigation),Auto_Mitigation);
		Reusable.waitForSiebelSpinnerToDisappear();
		ClickSection("Project Management");
		verifyExists(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type").replace("2", "4"),"Package Type drop down");
		click(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type").replace("2", "4"),"Package Type drop down");
		click(SiebelModeObj.R5DataCoverage.SelectValueDropdown.replace("Value", Customer_DNS_Resolvers),Customer_DNS_Resolvers);
		Reusable.waitForSiebelSpinnerToDisappear();

		String temp=SiebelModeObj.R5DataCoverage.InputBox.replace("Value", "Number of Days").replace("-1", "1");
		LocalValue=Integer.toString(rnd.nextInt(1000));
		sendKeys(temp, LocalValue, "Number of days");
		Reusable.SendkeaboardKeys(temp, Keys.ENTER);
		Reusable.waitForSiebelSpinnerToDisappear();

		
		//ScrollIntoViewByString(SiebelModeObj.R5DataCoverage.InputBox.replace("Value", "Statement of Work"));
		//to perform Scroll on application using Selenium
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("window.scrollBy(0,350)", "");
		

		Reusable.closePopUp();
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		//	ClickHereSave();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();

	}
	public void R5DataCoverage(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
	String Auto_Mitigation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");
	String Customer_DNS_Resolvers = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_DNS_Resolvers");
	String Alerting_Notification_Email_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");

	String LocalValue=null;
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelModeObj.R5DataCoverage.EnableFeature,"Show Full Info Access");
	mouseMoveOn(SiebelModeObj.R5DataCoverage.EnableFeature);
	click(SiebelModeObj.R5DataCoverage.ClickShowFullInfoAccess,"Show Full Info Access");
	Reusable.waitForSiebelLoader();
	ClickSection("Service Management");

	verifyExists(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type"),"Package Type drop down");
	click(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type"),"Package Type drop down");
	click(SiebelModeObj.R5DataCoverage.SelectValueDropdown.replace("Value", Auto_Mitigation),Auto_Mitigation);
	Reusable.waitForSiebelSpinnerToDisappear();
	ClickSection("Project Management");
	verifyExists(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type").replace("2", "4"),"Package Type drop down");
	click(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Package Type").replace("2", "4"),"Package Type drop down");
	click(SiebelModeObj.R5DataCoverage.SelectValueDropdown.replace("Value", Customer_DNS_Resolvers),Customer_DNS_Resolvers);
	Reusable.waitForSiebelSpinnerToDisappear();

	String temp=SiebelModeObj.R5DataCoverage.InputBox.replace("Value", "Number of Days").replace("-1", "1");
	LocalValue=Integer.toString(rnd.nextInt(1000));
	sendKeys(temp, LocalValue, "Number of days");
	Reusable.SendkeaboardKeys(temp, Keys.ENTER);
	Reusable.waitForSiebelSpinnerToDisappear();

	ClickSection("Consultancy Services");
	//ScrollIntoViewByString(SiebelModeObj.R5DataCoverage.SectionSelection.replace("Value","Equipment"));
	verifyExists(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Consultancy Service Type"),"Consultancy Service Type drop down");
	click(SiebelModeObj.R5DataCoverage.ClickDropdown.replace("Value", "Consultancy Service Type"),"Consultancy Service Type drop down");
	click(SiebelModeObj.R5DataCoverage.SelectValueDropdown.replace("Value", Alerting_Notification_Email_Address),Alerting_Notification_Email_Address);
	Reusable.waitForSiebelSpinnerToDisappear();

	String temp1=SiebelModeObj.R5DataCoverage.InputBox.replace("Value", "Statement of Work").replace("-1", "1");
	LocalValue=RandomName();


	sendKeys(temp1, LocalValue, "Statement of Work");
	Reusable.SendkeaboardKeys(temp1, Keys.ENTER);
	Reusable.waitForSiebelSpinnerToDisappear();

	String temp2=SiebelModeObj.R5DataCoverage.InputBox.replace("Value", "Number of Days").replace("-1", "2");
	LocalValue=Integer.toString(rnd.nextInt(1000));

	sendKeys(temp2, LocalValue, "Number of Days");
	Reusable.SendkeaboardKeys(temp2, Keys.ENTER);
	Reusable.waitForSiebelSpinnerToDisappear();

	Reusable.closePopUp();
	Reusable.waitForSiebelLoader();
	waitToPageLoad();
	// ClickHereSave();
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	}
	private String RandomName()
	{
		String[] BuildNames= {"APIAutomation","Automation","Selenium","Chrome","Opera","Safari","FireFox","Ghost","Edge"};
		int index=rnd.nextInt(BuildNames.length);
		return BuildNames[index];
	}


	//////////////////////B 10 Dec


	public void ModTechModCommWaveAndLine(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{

		waitToPageLoad();		
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String InstallTime95 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String ColName123 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InvlidCLITreatmet");
		String ColName125 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutingSequence");
		String BInstallTime112 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");
		String ColName124 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TotalNumberDDIs");
		String ColName126 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FirstCodec");
		String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String ColName76 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
		String ColName40 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		
		if (ProductName.toString().equalsIgnoreCase("Ethernet Line")||ProductName.toString().equalsIgnoreCase("Wave"))
		{

			Reusable.savePage();
			// A End And B End Installation Entries Start
			Reusable.AEndDropdownSelection("Install Time",InstallTime95);
			Reusable.AEndInputEnter("Access Notification Period",ColName123);
			Reusable.AEndInputEnter("Access Time Window",ColName125);

			Reusable.BEndDropdownSelection("Install Time",BInstallTime112);
			Reusable.BEndInputEnter("Access Notification Period",ColName124);
			Reusable.BEndInputEnter("Access Time Window",ColName126);

			if (ProductName.equalsIgnoreCase("Ethernet Line")&&Comments.equalsIgnoreCase("ModTech"))
			{
			//Update bandwidth with new Values
			//Service BandWidth
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "Service Bandwidth"),"MiddleDropDown");
			click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "Service Bandwidth"));
			waitToPageLoad();

			click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleLi.replace("Value", ColName40));
			}

			if (ProductName.equalsIgnoreCase("Wave")&&Comments.equalsIgnoreCase("ModTech"))
			{
			//Update bandwidth with new Values
			//Service BandWidth
			verifyExists(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "A End Resilience Option"),"MiddleDropDown");
			click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleDropDown.replace("Value", "A End Resilience Option"));
			waitToPageLoad();

			click(SiebelModeObj.ModTechModCommWaveAndLine.MiddleLi.replace("Value", ColName76));
			}

			Reusable.savePage();
			Reusable.waitForSiebelLoader();
		}
	}



	public void middleAppletPrivateWaveNode(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception {
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelModeObj.middleAppletPrivateWaveNode.MiddleDropDown.replace("Value", "Network Topology"), 10);
		verifyExists(SiebelModeObj.middleAppletPrivateWaveNode.MiddleDropDown.replace("Value", "Network Topology"));
		click(SiebelModeObj.middleAppletPrivateWaveNode.MiddleDropDown.replace("Value", "Network Topology"));

		verifyExists(SiebelModeObj.middleAppletPrivateWaveNode.MiddleLi.replace("Value", "Point to Point Single Node"));
		click(SiebelModeObj.middleAppletPrivateWaveNode.MiddleLi.replace("Value", "Point to Point Single Node"));
		OperationalAttributeUltra(testDataFile,sheetName,scriptNo,dataSetNo);

		//verifyExists(SiebelModeObj.middleAppletPrivateWaveNode.SaveButton,"SaveButton");
		//click(SiebelModeObj.middleAppletPrivateWaveNode.SaveButton, "Click on  Save Button");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();	

	}


	public void OperationalAttributeUltra(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception {
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.OperationalAttributeUltra.SettingsButton,"Settings Button");

		click(SiebelModeObj.OperationalAttributeUltra.SettingsButton,"Click on setting button");
		Reusable.waitForSiebelLoader();

		int count = getXPathCount(SiebelModeObj.OperationalAttributeUltra.OperationalAttribueCount);
		System.out.println(count);
		for (int i = 0; i < count; i++) {
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.OperationalAttributeUltra.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"OperationalAttribueClick");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.OperationalAttributeUltra.OperationalAttribueClick.replace("index",String.valueOf(i + 1)),"Operational Attribue Click");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.OperationalAttributeUltra.OperationalAttributeText,"OperationalAttributeText");

			sendKeys(SiebelModeObj.OperationalAttributeUltra.OperationalAttributeText, "Test1","Operational Attribute Text");
			Reusable.waitForSiebelLoader();
		}
		//Reusable.waitForSiebelLoader();
		ScrollIntoViewByString(SiebelModeObj.OperationalAttributeUltra.OperationalAttributeOK);
		verifyExists(SiebelModeObj.OperationalAttributeUltra.OperationalAttributeOK,"OperationalAttributeOK");
		click(SiebelModeObj.OperationalAttributeUltra.OperationalAttributeOK, "Click on  OperationalAttributeOK");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
	}


	public void SiteAInstallationTimePUD(String testDataFile,String sheetName,String scriptNo,String dataSetNo)throws InterruptedException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String AendInstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Dark Fibre"))
				|| (ProductName.equalsIgnoreCase("Ultra Low Latency"))
				|| (ProductName.equalsIgnoreCase("Private Wave Node"))
				|| (ProductName.equalsIgnoreCase("Private Wave Service"))) {
			// Entering Install Time
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SiteAInstallationTimePUD.SiteADropdownClick.replace("Value","Install Time"),"SiteADropdownClick");
			click(SiebelModeObj.SiteAInstallationTimePUD.SiteADropdownClick.replace("Value","Install Time"));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SiteAInstallationTimePUD.SiteABSelection.replace("Value",AendInstallTime),"SiteABSelection");
			click(SiebelModeObj.SiteAInstallationTimePUD.SiteABSelection.replace("Value",AendInstallTime));
			Reusable.waitForSiebelLoader();

		}
	}



	public void SiteATerminationTimePUD(String testDataFile,String sheetName,String scriptNo,String dataSetNo)throws InterruptedException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		Random rand = new Random();

		if ((ProductName.equalsIgnoreCase("Private Ethernet")) || (ProductName.equalsIgnoreCase("Dark Fibre"))
				|| (ProductName.equalsIgnoreCase("Ultra Low Latency"))
				|| (ProductName.equalsIgnoreCase("Private Wave Service"))
				|| (ProductName.equalsIgnoreCase("Private Wave Node"))) {
			// CabinetID
			int rand_int1 = rand.nextInt(1000);

			verifyExists(SiebelModeObj.SiteATerminationTimePUD.SiteAInput.replace("Value", "Cabinet ID"),"SiteAInput");
			sendKeys(SiebelModeObj.SiteATerminationTimePUD.SiteAInput.replace("Value", "Cabinet ID"),Integer.toString(rand_int1));
			Reusable.waitForSiebelLoader();
			// Shelf ID
			rand_int1 = rand.nextInt(1000);

			verifyExists(SiebelModeObj.SiteATerminationTimePUD.SiteAInput.replace("Value", "Shelf ID"),"SiteAInput");
			sendKeys(SiebelModeObj.SiteATerminationTimePUD.SiteAInput.replace("Value", "Shelf ID"),Integer.toString(rand_int1));
			Reusable.waitForSiebelLoader();


			// CabinetType

			click(SiebelModeObj.SiteAInstallationTimePUD.SiteADropdownClick.replace("Value","Cabinet Type"));
			click(SiebelModeObj.SiteAInstallationTimePUD.SiteABSelection.replace("Value","Customer Cabinet"));
			Reusable.waitForSiebelLoader();


			// LinkAggregationRequired

			if (!(ProductName.contains("Ultra Low Latency"))
					&& (!ProductName.contains("Dark Fibre")) &&  (!ProductName.contains("Private Wave Node")) &&
					!(ProductName.contains("Private Wave Service")))
			{
				click(SiebelModeObj.SiteAInstallationTimePUD.SiteADropdownClick.replace("Value","Link Aggregation Required"));
				click(SiebelModeObj.SiteAInstallationTimePUD.SiteADropdownClick.replace("Value","No"));

				Reusable.waitForSiebelLoader();			}

		}
	}





	public void AEndSitePUD(String testDataFile,String sheetName,String scriptNo,String dataSetNo)throws InterruptedException, IOException 
	{

		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Offnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
		String ColtActualDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String OrderSignedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		String ThirdpartyAccessProviderValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_party_access_provider");



		waitToPageLoad();
		Reusable.waitForSiebelLoader();	
		if (ProductName.contains("Ultra Low Latency")
				|| (ProductName.contains("Private Wave Node")|| (ProductName.contains("Dark Fibre"))))

		{					
			if(Offnet.contains("Offnet"))
			{

				System.out.println("Enter into offnet part of A end site pud");

				verifyExists(SiebelModeObj.AEndSitePUD.AccessTypeDropdownAccess,"AccessType DropdownAccess");
				click(SiebelModeObj.AEndSitePUD.AccessTypeDropdownAccess,"Click on AccessType DropdownAccess");

				//WaitforElementtobeclickable(xml.getlocator("//locators/AccesstypeOffnet")+"[1]");
				//Clickon(getwebelement(xml.getlocator("//locators/AccesstypeOffnet")+"[1]"));

				verifyExists(SiebelModeObj.AEndSitePUD.AccesstypeOffnet.replace("AccessTypeValue",AccessType),"AccesstypeOffnet");
				click(SiebelModeObj.AEndSitePUD.AccesstypeOffnet.replace("AccessTypeValue",AccessType));

				verifyExists(SiebelModeObj.AEndSitePUD.IpGurdianSave,"AccesstypeOffnet");
				click(SiebelModeObj.AEndSitePUD.IpGurdianSave,"IpGurdianSave");
				Reusable.waitForSiebelLoader();

				if (!ProductName.contains("Dark Fibre")){
					
				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Technology"),"Access Technology");
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Access Technology"),"Access Technology");
				//verifyExists(SiebelModeObj.AEndSitePUD.AList,"AList");
				//click(SiebelModeObj.AEndSitePUD.AList.replace("Value","Access Technology"));

				verifyExists(SiebelModeObj.AEndSitePUD.AList.replace("Value",ColtActualDate),"Access Technology");
				click(SiebelModeObj.AEndSitePUD.AList.replace("Value",ColtActualDate),"Access Technology");
				}


				//for 3rd party connection provider//
				verifyExists(SiebelModeObj.AEndSitePUD.ThirdpartyaccessproviderDropDown,"ThirdpartyaccessproviderDropDown");
				click(SiebelModeObj.AEndSitePUD.ThirdpartyaccessproviderDropDown,"ThirdpartyaccessproviderDropDown");

				verifyExists(SiebelModeObj.AEndSitePUD.ThirdpartyaccessProvidervalue.replace("Value",ThirdpartyAccessProviderValue),"ThirdpartyaccessProvidervalue");
				click(SiebelModeObj.AEndSitePUD.ThirdpartyaccessProvidervalue.replace("Value",ThirdpartyAccessProviderValue),"ThirdpartyaccessProvidervalue");

				//for 3rd party connection reference//

				verifyExists(SiebelModeObj.AEndSitePUD.Thirdpartyconectionreference,"Thirdpartyconectionreference");
				click(SiebelModeObj.AEndSitePUD.Thirdpartyconectionreference,"Thirdpartyconectionreference");
				sendKeys(SiebelModeObj.AEndSitePUD.Thirdpartyconectionreference,"12","Thirdpartyconectionreference");

				//for third party SLA tier//
				verifyExists(SiebelModeObj.AEndSitePUD.ThirdpartyDropDown,"ThirdpartyDropDown");
				click(SiebelModeObj.AEndSitePUD.ThirdpartyDropDown,"ThirdpartyDropDown");

				verifyExists(SiebelModeObj.AEndSitePUD.ThirdpartySLATiervalue.replace("SLAValue", IPRange),"ThirdpartySLATiervalue");
				click(SiebelModeObj.AEndSitePUD.ThirdpartySLATiervalue.replace("SLAValue", IPRange));

				//verifyExists(SiebelModeObj.AEndSitePUD.IpGurdianSave,"IpGurdianSave");
				//click(SiebelModeObj.AEndSitePUD.IpGurdianSave,"IpGurdianSave");
				Reusable.savePage();
				Reusable.waitForSiebelLoader();

			}//out of if inner loop	

			else
			{
				System.out.println("Enter into new order part of B end site pud");

				verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Access Type"),"AEnd Site DropDown");
				click(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Access Type"));


				verifyExists(SiebelModeObj.AEndSitePUD.AList.replace("Value",ColtActualDate),"AList");
				click(SiebelModeObj.AEndSitePUD.AList.replace("Value",ColtActualDate));

				if (!ProductName.contains("Dark Fibre")){
					verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Access Technology"),"AEnd Site DropDown");
					click(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Access Technology"));

					verifyExists(SiebelModeObj.AEndSitePUD.AList.replace("Value",AccessType),"AList");
					click(SiebelModeObj.AEndSitePUD.AList.replace("Value",AccessType));
	               }

			//	verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Demarcation Device Required"),"AEnd Site DropDown");
			//	click(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Demarcation Device Required"));

				//verifyExists(SiebelModeObj.AEndSitePUD.AList.replace("Value",OrderSignedDate),"AList");
				//click(SiebelModeObj.AEndSitePUD.AList.replace("Value",OrderSignedDate));


			}}

		verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Building Type"),"AEnd Site DropDown");
		click(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Building Type"));

		verifyExists(SiebelModeObj.AEndSitePUD.AList.replace("Value","NA"),"AList");
		click(SiebelModeObj.AEndSitePUD.AList.replace("Value","NA"));

		verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value","BCP Reference"),"AList");
		click(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value","BCP Reference"));
		sendKeys(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value", "BCP Reference"),"NA"/* InputData[180].toString() */);

		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Customer Site Pop Status"),"AEnd Site DropDown");
		click(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","Customer Site Pop Status"));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.AEndSitePUD.AList.replace("Value","NA"),"AList");
		click(SiebelModeObj.AEndSitePUD.AList.replace("Value","NA"));


		//verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","DSL SLA Class"),"AEnd Site DropDown");
		//click(SiebelModeObj.AEndSitePUD.AEndSiteDropDown.replace("Value","DSL SLA Class"));

		//verifyExists(SiebelModeObj.AEndSitePUD.AList.replace("Value",ServiceBandwidth),"AList");
		//click(SiebelModeObj.AEndSitePUD.AList.replace("Value",ServiceBandwidth));

		verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value","Site Name Alias"),"AEndSiteInput");
		click(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value","Site Name Alias"));
		sendKeys(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value", "Site Name Alias"),"NA");

		if (ProductName.contains("Private Wave Node") || ProductName.contains("Private Wave Service")) 
		{

			verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value","Node Site Name"));
			click(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value","Node Site Name"));
			sendKeys(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value", "Node Site Name"),"NA");

		}


		if(ProductName.contains("Private Wave Service") ) 
		{

			verifyExists(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value", "Node Service ID"),"AEndSiteInput");
			click(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value","Node Service ID"));
			sendKeys(SiebelModeObj.AEndSitePUD.AEndSiteInput.replace("Value", "Node Service ID"),"BIGT14124");

		}	

	}



	public void EthernetAccessNewFields(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		/*	verifyExists(SiebelModeObj.EthernetAccessNewFields.NetworkTopology,"NetworkTopology");
		click(SiebelModeObj.EthernetAccessNewFields.NetworkTopology,"Click network topology");
		Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.NetworkTopology, Keys.TAB);
		 */	





		verifyExists(SiebelModeObj.EthernetAccessNewFields.Topology,"Topology");
		click(SiebelModeObj.EthernetAccessNewFields.Topology,"topology");
		Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.Topology, Keys.TAB);


		//for enter value in circuit reference
		if(ProductName.contains("Ethernet Access"))
		{
			verifyExists(SiebelModeObj.EthernetAccessNewFields.CircuitReference,"Circuit Reference");
			sendKeys(SiebelModeObj.EthernetAccessNewFields.CircuitReference,"2","Enter value in circuit reference");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.CircuitReference, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
		}
		//for enter value in oss platform

		verifyExists(SiebelModeObj.EthernetAccessNewFields.ClickDropdown.replace("Value","OSS Platform Flag"),"OSS Platform Flag Dropdown");
		click(SiebelModeObj.EthernetAccessNewFields.ClickDropdown.replace("Value","OSS Platform Flag"),"OSS Platform Flag dropdown");

		verifyExists(SiebelModeObj.EthernetAccessNewFields.SelectValueDropdown.replace("Value","Legacy"),"Legacy");
		click(SiebelModeObj.EthernetAccessNewFields.SelectValueDropdown.replace("Value","Legacy"),"Legacy");
		Reusable.waitForSiebelSpinnerToDisappear();
		ClickHereSave();

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");


		// click(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
		javaScriptclick(SiebelModeObj.EthernetAccessNewFields.SettingsButton,"SettingsButton");
		Reusable.waitForSiebelLoader();	 
		if(ProductName.contains("Ethernet Access"))
		{
			System.out.println("go to if loop");
			for (int i=0; i<5; i++) {
				System.out.println("go to for loop");

				if(!isElementPresent(By.xpath("//td[text()='NC Technical Service Id']/..//td/following-sibling::td/following-sibling::td/input")))
				{
					Reusable.waitForSiebelLoader(); 
				}
				else {
					click(SiebelModeObj.EthernetAccessNewFields.AttributeValueEthernetAccess,"AttributeValueEthernetAccess");
					sendKeys(SiebelModeObj.EthernetAccessNewFields.AttributeValueEthernetAccess,"test1","AttributeValueEthernetAccess");
					Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.AttributeValueEthernetAccess, Keys.TAB);
					
					ScrollIntoViewByString(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute);
					//click(SiebelAddProdcutObj.CompletedValidation_offnet.OperationalAttributeOK,"OperationalAttributeOK");
					
					verifyExists(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute,"OkButtonOperationalAttribute");
					click(SiebelModeObj.EthernetAccessNewFields.OkButtonOperationalAttribute,"OkButtonOperationalAttribute");
					Reusable.waitForSiebelLoader();
					if (isElementPresent(By.xpath("//div[@class='ui-dialog-buttonset']/button)[2]"))) 
					{
						boolean t = isElementPresent(By.xpath("(//div[@class='ui-dialog-buttonset']/button)[2]"));
						System.out.println(t);
						click(SiebelModeObj.EthernetAccessNewFields.Dialogbuttonset,"Dialogbuttonset");
						Reusable.waitForSiebelLoader();
						break;
					}//end of if loop
					break;
				}//end of else loop

			}//end of for loop
		}//end of if loop
		if(ProductName.contains("Private Wave Service"))
		{
			System.out.println("enter if loop of private wave service");

			verifyExists(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection,"InputValuesProtection");
			click(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection,"InputValuesProtection");
			sendKeys(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection,"No Protection");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.InputValuesProtection, Keys.TAB);

			/*verifyExists(SiebelModeObj.EthernetAccessNewFields.VendorValuecell,"VendorValuecell");
			click(SiebelModeObj.EthernetAccessNewFields.VendorValuecell,"VendorValuecell");
			sendKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecellInput,"ADVA");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecellInput, Keys.TAB);

			verifyExists(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell,"TechonolgyValuecell");
			click(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecell,"TechonolgyValuecell");
			sendKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecellinput,"CWDM");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecellinput, Keys.TAB);*/
			
			verifyExists(SiebelModeObj.EthernetAccessNewFields.VendorValueField,"VendorValuecell");
			click(SiebelModeObj.EthernetAccessNewFields.VendorValueField,"VendorValuecell");
			sendKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecellInput,"ADVA");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.VendorValuecellInput, Keys.TAB);

			verifyExists(SiebelModeObj.EthernetAccessNewFields.TechonolgyValueField,"TechonolgyValuecell");
			click(SiebelModeObj.EthernetAccessNewFields.TechonolgyValueField,"TechonolgyValuecell");
			sendKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecellinput,"CWDM");
			Reusable.SendkeaboardKeys(SiebelModeObj.EthernetAccessNewFields.TechonolgyValuecellinput, Keys.TAB);
			ScrollIntoViewByString(SiebelModeObj.EthernetAccessNewFields.OKButton);
			verifyExists(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");
			click(SiebelModeObj.EthernetAccessNewFields.OKButton,"OKButton");

			if (isElementPresent(By.xpath("//div[@class='ui-dialog-buttonset']/button)[2]"))) 
			{
				System.out.println("enter if loop");
				boolean t = isElementPresent(By.xpath("//div[@class='ui-dialog-buttonset']/button)[2]"));
				System.out.println(t);

				click(SiebelModeObj.EthernetAccessNewFields.Dialogbuttonset,"Dialogbuttonset");		 
				Reusable.waitForSiebelLoader();
			}	 

		}//end of if inner loop 
	} //end of if outer loop


	public void middleUltraLowLatency(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		middleAppletDarkFibre(testDataFile, sheetName, scriptNo, dataSetNo);
	}


	public void middleAppletDarkFibre(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{

		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String ServieordereReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServieordereReference");
		String AEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resilience_Option");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		if (ProductName.contains("Ultra Low Latency")
				|| ProductName.contains("Private Wave Service")) {

			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Service Bandwidth"),"Service Bandwidth");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Service Bandwidth"));

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value",ServiceBandwidth),"Select Service Bandwidth");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value",ServiceBandwidth));

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Bandwidth Type"),"Bandwidth Type");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Bandwidth Type"));

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Layer 1"),"Select Bandwidth Type");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Layer 1"));

		}

		if ((ProductName.contains("Dark Fibre"))
				|| (ProductName.contains("Ultra Low Latency"))) {

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Coverage"),"MiddleDropDown");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Coverage"));


			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value",ServieordereReference),"MiddleLi");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value",ServieordereReference));
		}

		verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","A End Resilience Option"),"A End Resilience Option");
		click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","A End Resilience Option"),"A End Resilience Option");

		if (!(ProductName.contains("Ultra Low Latency"))
				&& !(ProductName.contains("Private Wave Service"))) {

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Single Pair"));
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Single Pair"));

		}
		if (ProductName.contains("Ultra Low Latency")
				|| (ProductName.contains("Private Wave Service"))) {


			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value",AEndResilienceOption),"A End Resilience Option Value");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value",AEndResilienceOption));

		}

		verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","B End Resilience Option"),"MiddleDropDown");
		click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","B End Resilience Option"));


		if (!(ProductName.contains("Ultra Low Latency"))
				&& !(ProductName.contains("Private Wave Service"))) {
			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Single Pair"));
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Single Pair"));
		}
		if ((ProductName.contains("Ultra Low Latency")
				|| (ProductName.contains("Private Wave Service")))) {

			//verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Protected"),"MiddleLi");
			//click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Protected"));
			/// Updated  for IP2020
			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Protected"),"B End Resilience Option");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Protected"),"B End Resilience Option");
		}


		verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","OSS Platform Flag"),"MiddleDropDown");
		click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","OSS Platform Flag"));

		verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Legacy"),"MiddleLi");
		click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Legacy"));

		verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Hard Modify Flag"),"MiddleLi");
		click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Hard Modify Flag"));

		verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Y"),"MiddleLi");
		click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","Y"));

		if ((ProductName.contains("Private Wave Service"))) {

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown,"MiddleLi");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Circuit Category"));

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi,"MiddleLi");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","LANLINK"));

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown,"MiddleLi");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Service Type"));

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi,"MiddleLi");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","100 Gbps Ethernet"));

			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown,"MiddleLi");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleDropDown.replace("Value","Circuit Prefix"));


			verifyExists(SiebelModeObj.middleAppletDarkFibre.MiddleLi,"MiddleLi");
			click(SiebelModeObj.middleAppletDarkFibre.MiddleLi.replace("Value","LE-"));

		}

		verifyExists(SiebelModeObj.middleAppletDarkFibre.SaveButton,"SaveButton");
		click(SiebelModeObj.middleAppletDarkFibre.SaveButton,"SaveButton");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		///////////////////Technical Contact//////////////////////
		verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
		click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
		click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
		click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
		click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
		Reusable.waitForSiebelLoader();
		//		Save();
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoB,"Show full info A");
		click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoB,"Click on Show full info A");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
		click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
		click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
		click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
		Reusable.waitForSiebelLoader();
		//		Save();
		Reusable.waitForSiebelSpinnerToDisappear();

	}



	public void middleApplet(Object Inputdata[]) throws InterruptedException,  IOException
	{
		System.out.println("middle applet start");
		Reusable.waitForSiebelLoader();
		String TempDate= Reusable.CurrentDate();

		verifyExists(SiebelModeObj.middleApplet.SupportStarDate,"SupportStarDate");
		click(SiebelModeObj.middleApplet.SupportStarDate,"SupportStarDate");
		sendKeys(SiebelModeObj.middleApplet.SupportStarDate,TempDate,"SupportStarDate");

		System.out.println("middle applet start1");

		TempDate = Reusable.CurrentDate() + 60;

		verifyExists(SiebelModeObj.middleApplet.SupprtEndDate,"SupprtEndDate");
		click(SiebelModeObj.middleApplet.SupprtEndDate,"SupprtEndDate");
		System.out.println("the End date value is" + TempDate);
		sendKeys(SiebelModeObj.middleApplet.SupprtEndDate,TempDate,"SupportStarDate");

		verifyExists(SiebelModeObj.middleApplet.SaveButton,"SaveButton");
		click(SiebelModeObj.middleApplet.SaveButton,"Click on Save Button");
		Reusable.waitForSiebelLoader();

	}


	public void DiversityCircuitEntry(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
		String Circuit_IPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
		//String TestWindow =	read.getExcelColumnValue(testDataFile, sheetName, "Test_Window");
		//String Circuit_IPAddressInput = read.getExcelColumnValue(testDataFile, sheetName, "CircuitIPAddressInput");


		verifyExists(SiebelModeObj.DiversityCircuitEntry.PopInput.replace("Value","Attachment Link"));
		sendKeys(SiebelModeObj.DiversityCircuitEntry.PopInput.replace("Value","Attachment Link"), TestWindow,"Attachment Link");

		verifyExists(SiebelModeObj.DiversityCircuitEntry.PopInput.replace("Value","Diverse From Service Reference"));
		sendKeys(SiebelModeObj.DiversityCircuitEntry.PopInput.replace("Value","Diverse From Service Reference"), Circuit_IPAddressInput,"Diversity Circuit");

		verifyExists(SiebelModeObj.DiversityCircuitEntry.PopClose,"Verify on Close");
		click(SiebelModeObj.DiversityCircuitEntry.PopClose,"Click  on Close");

	}



	public void middleAppletManagedDedicatedFirewall(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{

		String CPEc = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SupplierCeos");
		String HighAvai = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SLA_Tier_values");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.middleAppletManagedDedicatedFirewall.CPECombinationIDGeneric.replace("Value", "CPE Combination ID"),"CPE Combination ID");
		sendKeys(SiebelModeObj.middleAppletManagedDedicatedFirewall.CPECombinationIDGeneric.replace("Value", "CPE Combination ID"), CPEc);
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.middleAppletManagedDedicatedFirewall.HighAvailabilityRequired.replace("Value", "High Availability Required"),"High Availability Required dropdown");
		click(SiebelModeObj.middleAppletManagedDedicatedFirewall.HighAvailabilityRequired.replace("Value", "High Availability Required"),"High Availability Required dropdown");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.middleAppletManagedDedicatedFirewall.HighAvailabilityRequiredli,"HighAvailabilityRequired");
		click(SiebelModeObj.middleAppletManagedDedicatedFirewall.HighAvailabilityRequiredli, "High Availability Required value");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.middleAppletManagedDedicatedFirewall.CPECombinationIDGeneric.replace("Value", "Security Policy Attachment Link"),"Security Policy Attachment Link");
		//	 click(SiebelModeObj.middleAppletManagedDedicatedFirewall.CPECombinationIDGeneric.replace("Value", "Security Policy Attachment Link"));
		sendKeys(SiebelModeObj.middleAppletManagedDedicatedFirewall.CPECombinationIDGeneric.replace("Value", "Security Policy Attachment Link"), HighAvai);
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.middleAppletManagedDedicatedFirewall.SaveButton,"SaveButton");
		click(SiebelModeObj.middleAppletManagedDedicatedFirewall.SaveButton,"Click  on SaveButton");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
	}


	//////////////////********  09-12-2020    ************////////////////////////////////////////




	public void SiteAVLan(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String InstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
		String AccessTimeWindowDiversityType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Time_Window/DiversityType");

		if (ProductName.equalsIgnoreCase("DCA Ethernet"))
		{
			verifyExists(SiebelModeObj.SiteAVLan.AEndSiteInput.replace("Value", "Ethertype"));
			click(SiebelModeObj.SiteAVLan.AEndSiteInput.replace("Value", "Ethertype"));
			sendKeys(SiebelModeObj.SiteAVLan.AEndSiteInput.replace("Value", "Ethertype"), InstallTime);

			verifyExists(SiebelModeObj.SiteAVLan.AEndSiteInput.replace("Value", "VLAN Tag ID"));
			click(SiebelModeObj.SiteAVLan.AEndSiteInput.replace("Value", "VLAN Tag ID"));
			sendKeys(SiebelModeObj.SiteAVLan.AEndSiteInput.replace("Value", "VLAN Tag ID"), AccessTimeWindowDiversityType);
		}
	}

	public void SiteBVLan(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String RouterCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Country");
		String RouterModelPortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Model/PortRole");


		verifyExists(SiebelModeObj.SiteBVLan.BEndSiteInput.replace("Value", "Ethertype"));
		click(SiebelModeObj.SiteBVLan.BEndSiteInput.replace("Value", "Ethertype"));
		sendKeys(SiebelModeObj.SiteBVLan.BEndSiteInput.replace("Value", "Ethertype"), RouterCountry);

		verifyExists(SiebelModeObj.SiteBVLan.BEndSiteInput.replace("Value", "VLAN Tag ID"));
		click(SiebelModeObj.SiteBVLan.BEndSiteInput.replace("Value", "VLAN Tag ID"));
		sendKeys(SiebelModeObj.SiteBVLan.BEndSiteInput.replace("Value", "VLAN Tag ID"), RouterModelPortRole);

	}

	public void SiteBDedicatedCloudAccess(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String SiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Name");
		String CSPBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City/Town");
		String CSPName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
		String ConnectionAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connection_alias");
		String CSPRegion = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CSP_Region");
		String CSPPortType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CSP_Port Type");
		String DeliveryTeamCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeliveryTeamCountry");
		String RoutingDomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Routing_Domain");
		String TrunkSequence = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TrunkSequence");
		String TrunkName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TrunkName");

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","AWS Account Number"),"AWS Account Number");
		click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value", "AWS Account Number"));
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","AWS Account Number"),SiteName);

		// CSP bandwidth

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","CSP Bandwidth"),"CSP Bandwidth");
		click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","CSP Bandwidth"),"CSP Bandwidth");
		verifyExists(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",CSPBandwidth),CSPBandwidth);
		click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",CSPBandwidth),CSPBandwidth);

		// CSp NNI Ref Primary

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP NNI Reference Primary"),"CSP NNI Reference Primary");
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP NNI Reference Primary"),Country);

		// CSP NNI Ref Secondary

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP NNI Reference Secondary"),"CSP NNI Reference Secondary");
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP NNI Reference Secondary"),CityTown);

		// CSP Name

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","CSP Name"),"CSP Name");
		click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","CSP Name"),"CSP Name");
		verifyExists(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",CSPName),CSPName);
		click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",CSPName),CSPName);


		//		 verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteABSelection,"SiteABSelection");
		//		 click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteABSelection.replace("Value", PostalCode));

		// CSP Pop

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP PoP (Primary)"),"CSP PoP (Primary)");
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP PoP (Primary)"),Premises);

		// CSP Region

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","CSP Port Type"),"CSP Port Type");
		click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","CSP Port Type"),"CSP Port Type");
		verifyExists(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value","Dedicated"),"Dedicated");
		click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value","Dedicated"),"Dedicated");

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP Region"),"CSP Region");
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","CSP Region"),CSPRegion);

		// Connection Alias

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","Connection Alias"),"Connection Alias");
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","Connection Alias"),ConnectionAlias);

		// NAT

		//verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick,"SiteBDropdownClick");
		//click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value", "NAT Range Required"));

		//verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick,"SiteBDropdownClick");
		//click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value", DeliveryTeamCountry));
		Reusable.waitForSiebelLoader();

		// Routing Domain


		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","Routing Domain"),"Routing Domain");
		click(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBDropdownClick.replace("Value","Routing Domain"),"Routing Domain");
		verifyExists(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value","Public"),"Public");
		click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value","Public"),"Public");

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","S VLAN Tag ID"),"S VLAN Tag ID");
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","S VLAN Tag ID"),TrunkSequence);
		Reusable.waitForSiebelLoader();

		// Service Key
		verifyExists(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","Service Key"),"Service Key");
		sendKeys(SiebelModeObj.SiteBDedicatedCloudAccess.SiteBInput.replace("Value","Service Key"),TrunkName);

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
		click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");

		Reusable.waitForSiebelLoader();

	}


	public void IPVPNServicePlusAccess(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		String AutoMitigation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");

		verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickDropdown.replace("Value", "Service Type"),"Service Type DropDwon");
		click(SiebelModeObj.IPVPNServicePlusAccess.ClickDropdown.replace("Value", "Service Type"),"Service Type DropDwon");

		verifyExists(SiebelModeObj.IPVPNServicePlusAccess.SelectValueDropdown.replace("Value", Samle),Samle);
		click(SiebelModeObj.IPVPNServicePlusAccess.SelectValueDropdown.replace("Value", Samle),Samle);
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.ClickHereSave();
		
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPVPNServicePlusAccess.TextInput.replace("Value", "Customer Alias"),"SiteBInput");
		sendKeys(SiebelModeObj.IPVPNServicePlusAccess.TextInput.replace("Value", "Customer Alias"), "Abhay");

		verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickDropdown.replace("Value", "IP Addressing Format"),"ClickDropdown");
		click(SiebelModeObj.IPVPNServicePlusAccess.ClickDropdown.replace("Value", "IP Addressing Format"));

		verifyExists(SiebelModeObj.IPVPNServicePlusAccess.SelectValueDropdown.replace("Value", AutoMitigation),AutoMitigation);
		click(SiebelModeObj.IPVPNServicePlusAccess.SelectValueDropdown.replace("Value", AutoMitigation),AutoMitigation);
		Reusable.waitForSiebelSpinnerToDisappear();

		Reusable.ClickHereSave();
		Reusable.waitForSiebelLoader();
	}



	public void IPVPNServicePlusAccess1(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws Exception
	{


		String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
		Reusable.waitForSiebelLoader();

		if (!Samle.equalsIgnoreCase("IP VPN Wholesale"))
		{
			verifyExists(SiebelModeObj.IPVPNServicePlusAccess1.ClickDropdown.replace("Value", "Network Topology"),"Network Topology DropDown");
			click(SiebelModeObj.IPVPNServicePlusAccess1.ClickDropdown.replace("Value", "Network Topology"),"Network Topology DropDown");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPVPNServicePlusAccess1.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);
			click(SiebelModeObj.IPVPNServicePlusAccess1.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.savePage();
			mouseMoveOn(SiebelModeObj.IPVPNServicePlusAccess1.ClickDropdown.replace("Value", "Contains Wholesale NNIs"));
			verifyExists(SiebelModeObj.IPVPNServicePlusAccess1.ClickDropdown.replace("Value", "Contains Wholesale NNIs"),"Contains Wholesale Dropdown");
			click(SiebelModeObj.IPVPNServicePlusAccess1.ClickDropdown.replace("Value", "Contains Wholesale NNIs"),"Contains Wholesale Dropdown");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPVPNServicePlusAccess1.SelectValueDropdown1.replace("Value","No"),"No value");
			click(SiebelModeObj.IPVPNServicePlusAccess1.SelectValueDropdown1.replace("Value","No"),"No value");

			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.ClickHereSave();
			Reusable.waitForSiebelLoader();
		}
		if (Samle.equalsIgnoreCase("IP VPN Wholesale")) 
		{

			verifyExists(SiebelModeObj.IPVPNServicePlusAccess1.ClickDropdown.replace("Value", "Business 1 DSCP"));
			click(SiebelModeObj.IPVPNServicePlusAccess1.ClickDropdown.replace("Value", "Business 1 DSCP"));

			verifyExists(SiebelModeObj.IPVPNServicePlusAccess1.SelectValueDropdown.replace("Value", "AF11"));
			click(SiebelModeObj.IPVPNServicePlusAccess1.SelectValueDropdown.replace("Value", "AF11"));
			Reusable.waitForSiebelLoader();

			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNServicePlusAccess1.Bus1Dscp, Keys.TAB);
			sendKeys(SiebelModeObj.IPVPNServicePlusAccess1.Bus2Dscp,"AF12");

			Reusable.waitForSiebelLoader();

			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNServicePlusAccess1.Bus2Dscp, Keys.TAB);
			sendKeys(SiebelModeObj.IPVPNServicePlusAccess1.Bus3Dscp,"AF13");

			Reusable.waitForSiebelLoader();

			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNServicePlusAccess1.Bus3Dscp, Keys.TAB);
			sendKeys(SiebelModeObj.IPVPNServicePlusAccess1.PreDscp,"AF11");

			Reusable.waitForSiebelLoader();

			Reusable.SendkeaboardKeys(SiebelModeObj.IPVPNServicePlusAccess1.PreDscp, Keys.TAB);
			sendKeys(SiebelModeObj.IPVPNServicePlusAccess1.StandDscp,"AF12");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNServicePlusAccess1.ClickheretoSaveAccess,"ClickheretoSaveAccess");
			click(SiebelModeObj.IPVPNServicePlusAccess1.ClickheretoSaveAccess,"Click on save");
			Reusable.waitForSiebelLoader();
		}

	}



	public void NetworkReferenceFillService(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{
		String Samle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");

		if (Samle.equalsIgnoreCase("SWIFTNet")
				|| Samle.equalsIgnoreCase("PrizmNet")) 
		{
			verifyExists(SiebelModeObj.NetworkReferenceFillService.NetworkReferenceSearch,"NetworkReferenceSearch");
			click(SiebelModeObj.NetworkReferenceFillService.NetworkReferenceSearch,"Click on Network Reference Search");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NetworkReferenceFillService.NetworkPlusSign,"NetworkPlusSign");
			click(SiebelModeObj.NetworkReferenceFillService.NetworkPlusSign,"Click on NetworkPlusSign");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.NetworkReferenceFillService.SubmitNetworkReference,"SubmitNetworkReference");
			click(SiebelModeObj.NetworkReferenceFillService.SubmitNetworkReference,"Click on SubmitNetworkReference");

			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}

	}


	public void IPVPNServiceNetworkReference(String testDataFile, String sheetName, String scriptNo,String dataSetNo) throws Exception 
	{
		String NetworkReference = null;
		String NetworkReference2 = null;
		Reusable.waitForSiebelLoader();
		click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceSearch,"Network Reference Search");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceCancelBtn,"NetworkReference cance button");
		click(SiebelModeObj.MandatoryFieldsInHeader.NetworkReferenceCancelBtn,"NetworkReference cance button");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();

		try {

		NetworkReference = getAttributeFrom(SiebelModeObj.IPVPNServiceNetworkReference.NetworkReference,"title");
		System.out.println("try block using value" + NetworkReference);
		} catch (Exception e) {
		System.out.println("Catch block using value" + NetworkReference);
		e.printStackTrace();

		}
		try {
		NetworkReference2 = getAttributeFrom(SiebelModeObj.IPVPNServiceNetworkReference.NetworkReference,"innerHTML");

		System.out.println("try using inner html" + NetworkReference2);
		} catch (Exception e) {
		e.printStackTrace();
		System.out.println("Catch using inner html" + NetworkReference2);
		}

		System.out.println(NetworkReference);


		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NetworkReference_Number", NetworkReference);
		System.out.println(NetworkReference);


	}

	public void installationTimeUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
		String Aend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String Bend_Install_Time = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");



		switch (Product_Type) {
		case "Ethernet Spoke": {
			click(SiebelModeObj.installationTimeUpdate.ClickDropdownB.replace("Value","Install Time"));
			click(SiebelModeObj.installationTimeUpdate.SelectValueDropdown.replace("Value",Install_Time));
			Reusable.waitForSiebelLoader();
			break;
		}
		case "Ethernet Hub": {

			click(SiebelModeObj.installationTimeUpdate.ClickDropdown.replace("Value","Install Time"));
			click(SiebelModeObj.installationTimeUpdate.SelectValueDropdown.replace("Value",Install_Time));
			Reusable.waitForSiebelLoader();

			break;
		}
		case "Ethernet Line": {

			ModTechModCommWaveAndLine(testDataFile, sheetName, scriptNo, dataSetNo);

			break;
		}
		case "Private Ethernet": {

			click(SiebelModeObj.installationTimeUpdate.SiteADropdownClick.replace("Value","Install Time"));
			click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",Aend_Install_Time));

			Reusable.waitForSiebelLoader();

			click(SiebelModeObj.installationTimeUpdate.SiteBDropdownClick.replace("Value","Install Time"));
			click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",Bend_Install_Time));

			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();


			break;
		}

		
		case "Ultra Low Latency": {

			click(SiebelModeObj.installationTimeUpdate.SiteADropdownClick.replace("Value","Install Time"));
			click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",Aend_Install_Time));
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.installationTimeUpdate.SiteBDropdownClick.replace("Value","Install Time"));
			click(SiebelModeObj.installationTimeUpdate.SiteABSelection.replace("Value",Bend_Install_Time));
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			//Reusable.savePage();
			break;
		}
		
		case "Dark Fibre": {
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			
			break;
		}

		case "Wave": {
			ModTechModCommWaveAndLine(testDataFile, sheetName, scriptNo, dataSetNo);
			break;
		}
		default: {
			break;
		}
		
		}
		
	}


	public void WaveLineSiteEntries(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException
	{


		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String OffnetCol = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
		String AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Access_Technology");
		String ThirdpartyAccessprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Third_party_access_provider");
		String BCPReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BCP_Reference");
		String BuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Building_Type");
		String CustSitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Customer_Site_Pop_Status");
		String BEndCustSitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Customer_Site_Pop_Status");
		String Site_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Name");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String BendAccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
		String BendAccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Access_Technology");
		String Thirdpartyaccessprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Third_party_access_provider");
		String BEndThirdpartyaccessprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BEnd_Third_party_access_provider");
		String BendBuilding_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Building_Type");

		String Aend_InstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Install_Time");
		String InvlidCLITreatmet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InvlidCLITreatmet");
		String RoutingSequence = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RoutingSequence");
		String Bend_InstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Install_Time");
		String TotalNumberDDIs = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"TotalNumberDDIs");
		String FirstCodec = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"FirstCodec");

		String Aend_CabinetType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Cabinet_Type");
		String Bend_CabinetType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Cabinet_Type");
		String Aend_PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Presentation_Interface");
		String ConnectorType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
		String FibreType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
		String Aend_PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Port_Role");
		String Bend_PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Presentation_Interface");
		String InstallTime = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
		String Router_Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Country");
		String Bend_PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Port_Role");

		String Aend_AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Access_Technology");
		String Aend_BuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Building_Type");
		String Order_Signed_Date = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
		String Bend_Customer_SitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Customer_Site_Pop_Status");
		String Bend_Customer_Site_Pop_Status = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Port_Role");
		String CallAdmissionControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CallAdmissionControl");
		String NumberOfSignallingTrunks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberOfSignallingTrunks");
		String Aend_Presentation_Interface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Presentation_Interface");


		if(ProdType.toString().equalsIgnoreCase("Wave")||ProdType.toString().equalsIgnoreCase("Ethernet Line"))
		{
			waitForAjax();
			Reusable.waitForSiebelLoader();
			Random rand = new Random();
			int rand_int1 = rand.nextInt(1000);
			// A End And B End Site Entries Start
			//Access type

			if(OffnetCol.toString().equalsIgnoreCase("Offnet"))
			{				
				Reusable.AEndDropdownSelection("Access Type",AccessType);
				Reusable.AEndDropdownSelection("Access Technology",AccessTechnology);
				if(AccessType.toString().equalsIgnoreCase("3rd Party Leased Line"))
				{
					Reusable.AEndDropdownSelection("Third Party Access Provider",ThirdpartyAccessprovider);
					rand_int1 = rand.nextInt(1000);
					Reusable.AEndInputEnter("3rd Party Connection Reference",Integer.toString(rand_int1));
					Reusable.AEndInputEnter("BCP Reference",CustSitePopStatus);
					Reusable.AEndInputEnter("Site Name Alias",Site_Name);
					Reusable.AEndDropdownSelection("Third Party SLA Tier",IPRange);
				}				
				Reusable.AEndDropdownSelection("Building Type",BuildingType);
				Reusable.AEndDropdownSelection("Customer Site Pop Status",CustSitePopStatus);

				Reusable.BEndDropdownSelection("Access Type",BendAccessType);
				Reusable.BEndDropdownSelection("Access Technology",BendAccessTechnology);
				if(BendAccessType.toString().equalsIgnoreCase("3rd Party Leased Line"))
				{
					Reusable.BEndDropdownSelection("Third Party Access Provider",BEndThirdpartyaccessprovider);
					rand_int1 = rand.nextInt(1000);
					Reusable.BEndInputEnter("3rd Party Connection Reference",Integer.toString(rand_int1));
					Reusable.BEndInputEnter("BCP Reference",BCPReference);
					Reusable.BEndInputEnter("Site Name Alias",Site_Name);
					Reusable.BEndDropdownSelection("Third Party SLA Tier",IPRange);
				}

				Reusable.BEndDropdownSelection("Building Type",BendBuilding_Type);
				Reusable.BEndDropdownSelection("Customer Site Pop Status",BEndCustSitePopStatus);

				Reusable.waitForSiebelLoader();
				Reusable.savePage();

				//A End And B End Installation Entries Start
				Reusable.AEndDropdownSelection("Install Time",Aend_InstallTime);
				Reusable.AEndInputEnter("Access Notification Period",InvlidCLITreatmet);
				Reusable.AEndInputEnter("Access Time Window",RoutingSequence);
				Reusable.BEndDropdownSelection("Install Time",Bend_InstallTime);
				Reusable.BEndInputEnter("Access Notification Period",TotalNumberDDIs);
				Reusable.BEndInputEnter("Access Time Window",FirstCodec);
				//A End And B End Installation Entries Ended

				Reusable.savePage();
				Reusable.waitForSiebelLoader();

				//A End And B End CPE Information Entries Start
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("Cabinet ID",Integer.toString(rand_int1));
				Reusable.AEndDropdownSelection("Cabinet Type",Aend_CabinetType);
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("Shelf ID",Integer.toString(rand_int1));

				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Cabinet ID",Integer.toString(rand_int1));
				Reusable.BEndDropdownSelection("Cabinet Type",Bend_CabinetType);
				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Shelf ID",Integer.toString(rand_int1));

				//A End And B End CPE Information Entries Ended
				Reusable.savePage();
				Reusable.waitForSiebelLoader();

				//A End And B End CPE Access Port Entries Start
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("Slot ID",Integer.toString(rand_int1));
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("Physical Port ID",Integer.toString(rand_int1));
				Reusable.AEndDropdownSelection("Presentation Interface",Aend_PresentationInterface);
				Reusable.AEndDropdownSelection("Connector Type",ConnectorType);
				Reusable.AEndDropdownSelection("Fibre Type",FibreType);
				Reusable.AEndDropdownSelection("Port Role",Aend_PortRole);

				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Slot ID",Integer.toString(rand_int1));
				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Physical Port ID",Integer.toString(rand_int1));
				Reusable.BEndDropdownSelection("Presentation Interface",Bend_PresentationInterface);
				//Reusable.BEndDropdownSelection("Connector Type",InstallTime);
				Reusable.BEndDropdownSelection("Connector Type",ConnectorType);
				Reusable.BEndDropdownSelection("Fibre Type",FibreType);
				Reusable.BEndDropdownSelection("Port Role",Bend_PortRole);
				//A End And B End CPE Access Port Entries End

			}
			else
			{
				Reusable.AEndDropdownSelection("Access Type",AccessType);
				Reusable.AEndDropdownSelection("Access Technology",Aend_AccessTechnology);
				Reusable.AEndDropdownSelection("Building Type",Aend_BuildingType);
				Reusable.AEndDropdownSelection("Customer Site Pop Status",CustSitePopStatus);
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("3rd Party Connection Reference",Integer.toString(rand_int1));
				Reusable.AEndInputEnter("BCP Reference",CustSitePopStatus);
				Reusable.AEndInputEnter("Site Name Alias",Site_Name);

				Reusable.BEndDropdownSelection("Access Type",BendAccessType);
				Reusable.BEndDropdownSelection("Access Technology",BendAccessTechnology);
				Reusable.BEndDropdownSelection("Building Type",BendBuilding_Type);
				Reusable.BEndDropdownSelection("Customer Site Pop Status",Bend_Customer_SitePopStatus);

				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("3rd Party Connection Reference",Integer.toString(rand_int1));
				Reusable.BEndInputEnter("BCP Reference",CallAdmissionControl);
				Reusable.BEndInputEnter("Site Name Alias",NumberOfSignallingTrunks);

				//A End And B End Site Entries Ended
				Reusable.savePage();
				Reusable.waitForSiebelLoader();

				//A End And B End Installation Entries Start
				Reusable.AEndDropdownSelection("Install Time",Aend_InstallTime);
				Reusable.AEndInputEnter("Access Notification Period",InvlidCLITreatmet);
				Reusable.AEndInputEnter("Access Time Window",RoutingSequence);
				Reusable.BEndDropdownSelection("Install Time",Bend_InstallTime);
				Reusable.BEndInputEnter("Access Notification Period",TotalNumberDDIs);
				Reusable.BEndInputEnter("Access Time Window",FirstCodec);
				//A End And B End Installation Entries Ended

				Reusable.savePage();
				Reusable.waitForSiebelLoader();

				//A End And B End CPE Information Entries Start
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("Cabinet ID",Integer.toString(rand_int1));
				Reusable.AEndDropdownSelection("Cabinet Type",Aend_CabinetType);
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("Shelf ID",Integer.toString(rand_int1));

				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Cabinet ID",Integer.toString(rand_int1));
				Reusable.BEndDropdownSelection("Cabinet Type",Bend_CabinetType);
				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Shelf ID",Integer.toString(rand_int1));

				//A End And B End CPE Information Entries Ended
				Reusable.savePage();
				Reusable.waitForSiebelLoader();

				//A End And B End CPE Access Port Entries Start
				rand_int1 = rand.nextInt(1000);
				Reusable.AEndInputEnter("Slot ID",Integer.toString(rand_int1));
				rand_int1 = rand.nextInt(1000);
				Reusable. AEndInputEnter("Physical Port ID",Integer.toString(rand_int1));
				Reusable.AEndDropdownSelection("Presentation Interface",Aend_PresentationInterface);
				Reusable.AEndDropdownSelection("Connector Type",ConnectorType);
				
				Reusable.AEndDropdownSelection("Fibre Type",FibreType);
				Reusable.AEndDropdownSelection("Port Role",Aend_PortRole);

				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Slot ID",Integer.toString(rand_int1));
				rand_int1 = rand.nextInt(1000);
				Reusable.BEndInputEnter("Physical Port ID",Integer.toString(rand_int1));
				Reusable.BEndDropdownSelection("Presentation Interface",Bend_PresentationInterface);
				Reusable.BEndDropdownSelection("Connector Type",ConnectorType);
				Reusable.BEndDropdownSelection("Fibre Type",FibreType);

				Reusable.BEndDropdownSelection("Port Role",Bend_PortRole);
				//A End And B End CPE Access Port Entries End

			} 
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			//    Reusable.ClickHereSave();
			Reusable.savePage();
		}	
	}

	/*	
	public void SearchSiteA() 
	{
		Reusable.waitForpageloadmask();	
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SearchSiteAObj.LeftSiteSearch,"Verify Search Site A");
		click(SiebelModeObj.SearchSiteAObj.LeftSiteSearch,"Click Search Site A");
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();	
	}
	public void SearchSiteAEntry(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, DocumentFormatException, IOException 
	{
		String Aend_StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
		String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Postal_Code");
		String Premises = read.getExcelColumnValue(testDataFile, sheetName, "Aend_Premises");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Street Name"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","Street Name"),Aend_StreetName,"Street Name");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchDropdown.replace("Value","Country"));
		click(SiebelModeObj.SearchSiteAEntryObj.SearchDropdown.replace("Value", "Country"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value","Country"));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",Country));

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","City / Town"));
		sendKeys(SiebelModeObj.SearchSiteAEntryObj.SearchInput.replace("Value","City / Town"),CityORTown,"City / Town");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PostalCode.replace("Value","Postal Code"));
		sendKeys(SiebelModeObj.addSiteADetailsObj.PostalCode.replace("Value","Postal Code"),PostalCode,"Postal Code");

		verifyExists(SiebelModeObj.addSiteADetailsObj.Premises,"A end Premises");
		sendKeys(SiebelModeObj.addSiteADetailsObj.Premises.replace("Value", "Premises"),Premises,"Premises");

		verifyExists(SiebelModeObj.SearchSiteAEntryObj.SearchButton,"Verify Search");
		click(SiebelModeObj.SearchSiteAEntryObj.SearchButton,"Click on Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickAddress,"Verify PickAddress");
		click(SiebelModeObj.addSiteADetailsObj.PickAddress,"Click on row and Pick Address");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickBuilding,"Verify Row and Pick Building");
		click(SiebelModeObj.addSiteADetailsObj.PickBuilding,"Click on Row and Pick Building");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"Click on SearchAddressRowSelection");

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Row and Pick Site");
		click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Row and Pick Site");
		Reusable.waitForSiebelLoader();

	}
	 */

	public void GetReference(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, DocumentFormatException,IOException
	{

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		verifyExists(SiebelModeObj.GetReferenceObj.GetReference,"Verify on GetReference");
		click(SiebelModeObj.GetReferenceObj.GetReference,"Click  on GetReference");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		String CircuitReference_Value = getTextFrom(SiebelModeObj.IPAccessObj.CircuitReferenceValue);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	

		////Generate reference number
		//	if (ProdType.toString().equalsIgnoreCase("Ultra Low Latency")) {

		Random rand = new Random();
		String siteA = null;
		String siteB = null;
		String CircuitRefNumber =null;
		if(CircuitReference_Value.isEmpty()==true)
		{
			if(isElementPresent(By.xpath("//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site A address"))
			{
				String siteA1 = getAttributeFrom("@xpath=//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']","title");
				siteA=siteA1.substring(0, 3);
			}
			if(isElementPresent(By.xpath("//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site B address"))
			{
				String siteB1 = getAttributeFrom("@xpath=//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']","title");

				siteB=siteB1.substring(0, 3);
			}

			if (siteA != null && siteB!= null)
			{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitRefNumber = siteA+"/"+siteB+"/"+"LE-"+randnumb;
			}else{
				if(siteA != null)
				{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitRefNumber = siteA+"/"+siteA+"/"+"LE-"+randnumb;
				}else{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitRefNumber = siteB+"/"+siteB+"/"+"LE-"+randnumb;
				}

			}
			sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, CircuitRefNumber,"Circuit Reference Number");
			Reusable.SendkeaboardKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			//CircuitReference_Value.set(CircuitRefNumber);
		}
		//	}

	}


	////////////////////////////////////////////////////////////////////11 dec ////////////////////////////


	public void enterMandatoryDetailsInMiddleApplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		
		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		switch (ProdType.toString()){
		case "Ethernet Line": 
		{
			WaveLineMidleApplet(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteADetails(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteBDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			WaveLineSiteEntries(testDataFile,sheetName,scriptNo,dataSetNo);

			break;
		}
		case "Wave": 
		{
			WaveLineMidleApplet(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteADetails(testDataFile,sheetName,scriptNo,dataSetNo);
			addSiteBDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			WaveLineSiteEntries(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "Private Ethernet": 
		{
			PrivateEthernetMiddleAplet(testDataFile,sheetName,scriptNo,dataSetNo);
			ShowfullInfo();
			DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Save();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			SearchSiteA();
			SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			SearchSiteB();
			SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			//Reusable.savePage();
			//Reusable.waitForSiebelLoader();
			//System.out.println("Sites Search and Entered");
			AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			BEndSite(testDataFile,sheetName,scriptNo,dataSetNo); 
			System.out.println("Site A & B Filled");
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			ClickHereSave();
			SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBTerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			ClickHereSave();
			SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			ClickHereSave();
			//			GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			//			ClickHereSave();
			break;
		}
		case "Private Wave Node": {
			middleAppletPrivateWaveNode(testDataFile,sheetName,scriptNo,dataSetNo);
			alertPopUp();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			alertPopUp();
			//ClickHereSave();
			Reusable.savePage();
			SearchSiteA();
			SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			break;
		}
		case "Ethernet Access": {

			ShowfullInfo();
			PrivateEthernetEntry(testDataFile,sheetName,scriptNo,dataSetNo);	
			SiteADiversityCircuitConfig(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.closePopUp();
			SaveAndCloseMask();
			EthernetAccessNewFields(testDataFile,sheetName,scriptNo,dataSetNo);
			//		SaveAndCloseMask();
			Reusable.savePage();
			SearchSiteA();
			SearchSiteEntery();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SaveAndCloseMask();
			AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			//SaveAndCloseMask();
			SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			break;

		}
		case "Private Wave Service": {

			EthernetAccessNewFields(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			ShowfullInfo();
			privateWaveServiceEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteADiversityCircuitConfig(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.closePopUp();
			SaveAndCloseMask();
			//SaveAndCloseMask();

			SearchSiteA();
			SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			//SearchSiteEntery();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SaveAndCloseMask();
			Reusable.waitForSiebelLoader();
			SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SaveAndCloseMask();
			Reusable.waitForSiebelLoader();
			//Start site B
			SearchSiteB();
			SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			//SaveAndCloseMask();
			Reusable.waitForSiebelLoader();
			GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			break;


			}
	/*	case "Private Wave Service": {

			ShowfullInfo();
			privateWaveServiceEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteADiversityCircuitConfig(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.closePopUp();
			SaveAndCloseMask();
			EthernetAccessNewFields(testDataFile,sheetName,scriptNo,dataSetNo);
			//SaveAndCloseMask();
			Reusable.savePage();
			SearchSiteA();
			SearchSiteEntery();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SaveAndCloseMask();
			SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SaveAndCloseMask();
			//Start site B
			SearchSiteB();
			SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.savePage();
			BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			//SaveAndCloseMask();
			GetReference(testDataFile,sheetName,scriptNo,dataSetNo);	
			break;


		}*/
		case "Ultra Low Latency": {
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			alertPopUp();
			OperationalAttributeUltra(testDataFile,sheetName,scriptNo,dataSetNo);
			alertPopUp();
			ShowfullInfo();
			DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Save();
			middleUltraLowLatency(testDataFile,sheetName,scriptNo,dataSetNo);
			alertPopUp();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			//Save();
			alertPopUp();
			ClickHereSave();
			SearchSiteA(); 
			SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			SearchSiteB();
			SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			ClickHereSave();
			//GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.waitForSiebelLoader();
			break;
		}

		case "Cloud Unified Communications": {
			middleApplet();
			R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "Professional Services":  {
			middleApplet();
			R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "IP Voice Solutions": {
			middleApplet();
			R5DataCoverage1(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}
		case "Managed Dedicated Firewall": {
			alertPopUp();
			middleAppletManagedDedicatedFirewall(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}

		case "Dark Fibre": {
			Reusable.waitForSiebelLoader();
			alertPopUp();
			OperationalAttributeUltra(testDataFile,sheetName,scriptNo,dataSetNo);
			alertPopUp();			
			middleAppletDarkFibre(testDataFile,sheetName,scriptNo,dataSetNo);
			ShowfullInfo();
			DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Save();
			Reusable.waitForSiebelLoader();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.savePage();
			SearchSiteA();
			SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			SearchSiteB();
			SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.waitForSiebelLoader();
			GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Reusable.waitForSiebelLoader();
			break;
		}

		case "DCA Ethernet": 
		{
			SiteBSettingClick();
			CSPInterconnectSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			PrivateEthernetMiddleAplet(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
				ShowfullInfo();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
			ClickHereSave();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();
			SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
			PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			SearchSiteA();
			SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			SearchSiteB();
			SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.waitForpageloadmask();
			AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			BEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
			Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			//Reusable.savePage();
			//Save();
			Reusable.waitForSiebelLoader();
			SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteBInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			//ClickHereSave();
			Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteBTerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
			Save();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			//ClickHereSave();
			SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteBAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			ClickHereSave();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteAVLan(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteBVLan(testDataFile,sheetName,scriptNo,dataSetNo);
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			ClickHereSave();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			SiteBDedicatedCloudAccess(testDataFile,sheetName,scriptNo,dataSetNo);
			//      	ClickHereSave();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			//          Save();
			break;
		}

		case "CPE Solutions Service": 
		{
			IPCPESolutionSite(testDataFile,sheetName,scriptNo,dataSetNo);
			break;
		}

		case "IP VPN Service": 
		{
			IPVPNServicePlusAccess(testDataFile,sheetName,scriptNo,dataSetNo);
			NetworkReferenceFillService(testDataFile,sheetName,scriptNo,dataSetNo);	
			IPVPNServicePlusAccess1(testDataFile,sheetName,scriptNo,dataSetNo);
			IPVPNServiceNetworkReference(testDataFile,sheetName,scriptNo,dataSetNo);

			break;
		}

		case "Voice Line V": {
			String DeliveryTeamCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeliveryTeamCountry");
			String DeliveryTeamCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeliveryTeamCity");

			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.VoiceServiceCountryDropdownAccess,"Verify VoiceServiceCountryDropdownAccess");
			click(SiebelModeObj.VoiceLineVObj.VoiceServiceCountryDropdownAccess,"Click On :VoiceServiceCountryDropdownAccess");
			verifyExists(SiebelModeObj.VoiceLineVObj.VoiceServiceInputValue,"Verify  Voice Service Country");
			click(SiebelModeObj.VoiceLineVObj.VoiceServiceInputValue,"Click On  Voice Service Country");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
			clearTextBox(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks);
			sendKeys(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"1");
			Reusable.waitForSiebelSpinnerToDisappear();


			verifyExists(SiebelModeObj.VoiceLineVObj.EgressNumberFormatDropDownAccess,"Verify EgressNumberFormatDropDownAccess");
			click(SiebelModeObj.VoiceLineVObj.EgressNumberFormatDropDownAccess,"Click On :EgressNumberFormatDropDownAccess");
			verifyExists(SiebelModeObj.VoiceLineVObj.EgreeNUmberFormat,"Verify  EgreeNUmberFormat");
			click(SiebelModeObj.VoiceLineVObj.EgreeNUmberFormat,"Click On  EgreeNUmberFormat");

			verifyExists(SiebelModeObj.VoiceLineVObj.TopologyDropDown,"Verify Topology");
			click(SiebelModeObj.VoiceLineVObj.TopologyDropDown,"Click On :Topology");
			verifyExists(SiebelModeObj.VoiceLineVObj.Topology,"Verify  Topology");
			click(SiebelModeObj.VoiceLineVObj.Topology,"Click On Topology");

			verifyExists(SiebelModeObj.VoiceLineVObj.TotalDDi,"Verify TotalDDi");
			click(SiebelModeObj.VoiceLineVObj.TotalDDi,"Click On :TotalDDi");
			sendKeys(SiebelModeObj.VoiceLineVObj.TotalDDi, "4");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.TotalDDi,Keys.ENTER);


			verifyExists(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,"Verify CustomerdefaultNumber");
			click(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,"Click On :CustomerdefaultNumber");
			sendKeys(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber, "1234");
			Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,Keys.ENTER);


			//		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			//		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On :ClickheretoSaveAccess");
			Reusable.ClickHereSave();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.Clicktoshowfullinfo,"Verify Clicktoshowfullinfo");
			click(SiebelModeObj.VoiceLineVObj.Clicktoshowfullinfo,"Click On Show full infor");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.SiteContactlookup,"Verify SiteContactlookup");
			click(SiebelModeObj.VoiceLineVObj.SiteContactlookup,"Click On Site Contact Search");


			verifyExists(SiebelModeObj.VoiceLineVObj.Site_Contact,"Verify Site_Contact");
			click(SiebelModeObj.VoiceLineVObj.Site_Contact,"Click On Submit Site Contac");


			verifyExists(SiebelModeObj.VoiceLineVObj.ServicePartsearch,"Verify ServicePartsearch");
			click(SiebelModeObj.VoiceLineVObj.ServicePartsearch,"Click On Site Contac Search");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.PickAccntOk,"Verify PickAccntOk");
			click(SiebelModeObj.VoiceLineVObj.PickAccntOk,"Click On Submit Service party");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamDropDown,"Verify Delivery Team DropDown");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamDropDown,"Click On Delivery Team Drop Down");

			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,"Verify Delivery team country");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,"Verify Delivery team country");
			sendKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,DeliveryTeamCountry);

			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCityDropDown,"Verify Delivery Team DropDown");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCityDropDown,"Click On Delivery Team Drop Down");

			verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,"Verify DeliveryTeamCity");
			click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,"Click On :DeliveryTeamCity");
			sendKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,DeliveryTeamCity);
			//Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,Keys.ENTER);


			//			verifyExists(SiebelModeObj.VoiceLineVObj.PerformanceReporting,"Verify PerformanceReporting");
			//			click(SiebelModeObj.VoiceLineVObj.PerformanceReporting,"Click On :PerformanceReporting");


			verifyExists(SiebelModeObj.VoiceLineVObj.WLECInvoicing,"Verify WLECInvoicing");
			click(SiebelModeObj.VoiceLineVObj.WLECInvoicing,"Click On :WLECInvoicing");


			verifyExists(SiebelModeObj.VoiceLineVObj.Crossbutton,"Verify Crossbutton");
			click(SiebelModeObj.VoiceLineVObj.Crossbutton,"Click On :Crossbutton");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			//		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			//		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.savePage();
			Reusable.waitForSiebelLoader();


			break;
		}

		case "IP Access":{
			  String SLA_TierValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SLA_Tier_value");
	    	  String Offnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
	    	  String capacitycheckreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Capacity_Check_Reference");
	    	  String OSSPlatformFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
	    	  String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
	    	  String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
	    	  String RouterTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Technology");
	    	  String Layer3Resilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Layer_3_Resilience");
	    	  String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
	    	  String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
	    	  String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City/Town");
	    	  String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
	    	  String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
	    	  String Service_PartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party_Name");
	    	  String Site_LastName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Last Name");
	    	  String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
	    	  String AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Technology");
	    	  String BuildingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Type");
	    	  String CustomerSitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Site_Pop_Status");
	    	  String CabinetID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_ID");
	    	  String ShelfID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Shelf_ID");
	    	  String CabinetType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_Type");
	    	  String SlotID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
	    	  String PhysicalPortID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
	    	  String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
	    	  String ConnectorType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
	    	  String FibreType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
	    	  String PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Port_Role");
	    	  String Access_Time_WindowORDiversityType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Time_Window/DiversityType");
	    	  String RouterCountry_Access = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Country");
	    	  String RouterModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Model/PortRole");
	    	  String IpAddressingFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Addressing_Format");
	    	  String Ipv4AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ipv_Addressing_Type");
	    	  String BackupBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");				
	    	  String InputPartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_LastName");
				
				if (Offnet.equals("IP Access_PartialDeliver"))
				{
				
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ServiceBandwidth));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ServiceBandwidth));
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.capacitycheckreference,"Verify capacitycheckreference");
					click(SiebelModeObj.IPAccessObj.capacitycheckreference,"Click On :capacitycheckreference");
					sendKeys(SiebelModeObj.IPAccessObj.capacitycheckreference,capacitycheckreference);
					
					verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Inputossplatformflag");
					click(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Click On :Inputossplatformflag");
					sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);
				
					verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Verify SelectRouterTypeDropDownAccess");
					click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Click On RouterTypeDropDownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Verify RouterTypeDropDownAccess");
//					click(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Click On RouterTypeDropDownAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					Reusable.waitForSiebelLoader();

				    verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitforAttributeloader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Verify Layer3ResillanceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Click On Layer3ResillanceDropdownAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitforAttributeloader();
					
//					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillancePartialSelectDropdownAccess,"Verify Layer3ResillancePartialSelectDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.Layer3ResillancePartialSelectDropdownAccess,"Click On Layer3ResillancePartialSelectDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					Reusable.waitForSiebelLoader();
								
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
					click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Click On SelectSiteSearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"Verify StreetNameAccess");
					sendKeysByJS(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetName);
					
					verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"Verify CountryAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);
					
					verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"Verify CityTownAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);
					
					verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"Verify PostalCodeAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);
					
					verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"Verify PremisesAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);
					
					verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Verify SearchButtonAccess");
					click(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Click On SearchButtonAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On SelectPickAddressAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On ServicePartyDropdownAccess");
			
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Verify InputPartyNameAccess");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,Service_PartyName);
			
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On Search");
					
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyName Submit Access");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click On Site Contact SearchAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click On Site Contact DropdownAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Verify InputPartyNameAccess");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,Site_LastName);
			
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click On LastNameSiteSearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessType Dropdown Access");
					click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessType Dropdown Access");
//					verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click On AccessTypeSelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessType));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify Access Technology Dropdown Access");
					click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click On Access Technology Dropdown Access");
//					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify Access Technology SelectAccess");
//					click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click On Access Technology SelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click On BuildingTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify Building Type SelectAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click On Building Type SelectAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click On CustomerSitePopStatusSelectAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						
					}

					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click On CabinetTypeDropdownAccess");
					
//					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click On CabinetTypeDropdownAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CabinetType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CabinetType));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify CabinetID");
					sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalPortID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Verify PresentationInterfaceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Click On PresentationInterfaceDropdownAccess");
					
					
//					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
//					click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PresentationInterface));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PresentationInterface));
					Reusable.waitForSiebelLoader();

					
					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click On ConnectorTypeDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Verify ConnectorTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Click On ConnectorTypeSelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click On FibreTypeDropdownAccess");
									
//					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
//					click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));

					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
					click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click On PortRoleDropDown");
//					verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
//					click(SiebelModeObj.IPAccessObj.PortValue,"Click On PortValue");
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click On InstallTimeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click On InstallTimeSelectAccess");
					
					Reusable.waitForSiebelLoader();
					
					
					verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
					}
			
					verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry_Access);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
				

					verifyExists(SiebelModeObj.IPAccessObj.routermodel,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.routermodel,RouterModel);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel, Keys.TAB);
								
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
					click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");
					

					verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
					click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
//					click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");

					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
					
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPV4AdressingTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Verify Crossbuttonforipaccess");
					click(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Click On Crossbuttonforipaccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.Secondarybtn,"Verify Secondarybtn");
					click(SiebelModeObj.IPAccessObj.Secondarybtn,"Click On Secondarybtn");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Input oss platform flag");
					sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag, Keys.TAB);
					
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.BackupbandwidthDropDownAccess,"Verify BackupbandwidthDropDownAccess");
					click(SiebelModeObj.IPAccessObj.BackupbandwidthDropDownAccess,"Click On BackupbandwidthDropDownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.BackupBandwidthSelectValue,"Verify BackupBandwidthSelectValue");
//					click(SiebelModeObj.IPAccessObj.BackupBandwidthSelectValue,"Click On BackupBandwidthSelectValue");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BackupBandwidth));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BackupBandwidth));
				
					
					Reusable.waitForSiebelLoader();
					

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}

//					repeated
					verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
					click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Click On SelectSiteSearchAccess");
					
					String StreetNameAccess = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
					verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetNameAccess);
				
					verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);
				
					verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"Verify CityTownAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);
				
					verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"Verify PostalCodeAccesss");
					sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);
					
					verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);
				
					verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Verify SearchButtonAccess");
					sendKeys(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Click on SearchButtonAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify PickAddressButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On PickAddressButtonAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					verifyExists(SiebelModeObj.IPAccessObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
					click(SiebelModeObj.IPAccessObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify PartyNameAccess");
					click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On PartyNameAccess");
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, Service_PartyName);
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On PartyNameSearchAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyNameSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On PartyNameSubmitAccess");
				
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click On SiteContactSearchAccess");
				
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click On SiteContactDropdownAccess");
				
					String SiteLastName = read.getExcelColumnValue(testDataFile, sheetName, "Site_LastName");
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Input Party Name");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess, SiteLastName);
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click On LastNameSiteSearchAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
					click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
					Reusable.waitForSiebelLoader();
					
					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On LastNameSiteSubmitAccess");
				
					verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click On AccessTypeSelectAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.ProceedButton,"Verify ProceedButton");
					click(SiebelModeObj.IPAccessObj.ProceedButton,"Click On ProceedButton");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
					click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click On AccesstechnologyDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
					click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click On AccesstechnologySelectAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click On BuildingTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify BuildingTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click On BuildingTypeSelectAccess");
				
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click On CustomerSitePopStatusSelectAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click On CabinetTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click On CabinetTypeSelectAccess");
					
					Reusable.waitForSiebelLoader();

					String Cabinet_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_ID");
					verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.CabinetID,Cabinet_ID);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					String shelfid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Shelf_ID");
					verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.shelfid,shelfid);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					
					String Slotid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
					verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Slotid,Slotid);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					String PhysicalportId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
					verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalportId);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Verify PresentationInterfaceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Click On PresentationInterfaceDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
					click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click On ConnectorTypeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Verify ConnectorTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Click On ConnectorTypeSelectAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click On FibreTypeDropdownAccess");
					
//					String FibreType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
					verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
					click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
					click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click On PortRoleDropDown");
					verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
					click(SiebelModeObj.IPAccessObj.PortValue,"Click On PortValue");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click On InstallTimeDropdownAccess");
					verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
					click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click On InstallTimeSelectAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Verify Accesstimewindow");
					sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					if(isElementPresent(By.xpath("//span[text()='Ok']")))
					{
						verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
						click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
						
					}
					verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry_Access);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess, Keys.TAB);
					
					
					Reusable.waitForSiebelLoader();
				
					verifyExists(SiebelModeObj.IPAccessObj.routermodel,"Verify Router Country Access");
					sendKeys(SiebelModeObj.IPAccessObj.routermodel,RouterModel);
					Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel, Keys.TAB);
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
					
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
					click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
					click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");
					Reusable.savePage();
					Reusable.waitForSiebelLoader();

					String CircuitReference_Value = getTextFrom(SiebelModeObj.IPAccessObj.CircuitReferenceValue);
					DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	
				}	
				else
				{
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Inputossplatformflag");
					click(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Click On :Inputossplatformflag");
					sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Type"),"Router Type");
					click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Type"),"Router Type");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterType));
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();

					verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Technology"));
					click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess.replace("Value","Router Technology"));
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterTechnology));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",RouterTechnology));
					Reusable.waitForSiebelLoader();

//				    verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
//					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
//					Reusable.waitForSiebelLoader();
//					Reusable.waitForSiebelSpinnerToDisappear();
//					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Layer3ResillanceDropdownAccess");
					click(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Layer3ResillanceDropdownAccess");
					Reusable.waitForSiebelLoader();
					
//					verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceSelectDropdownAccess,"Verify Layer3ResillanceSelectDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.Layer3ResillanceSelectDropdownAccess,"Click On Layer3ResillanceSelectDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Layer3Resilience));
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.capacitycheckreference,"Capacitycheckreference");
					click(SiebelModeObj.IPAccessObj.capacitycheckreference,"Capacitycheckreference");
					sendKeys(SiebelModeObj.IPAccessObj.capacitycheckreference,capacitycheckreference);
					Reusable.waitForSiebelLoader();
					
					//moved this at top
					/*verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");
					
					verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					click(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
					
					Reusable.waitForSiebelLoader();*/
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					Reusable.waitForSiebelLoader();
					addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
				/*	verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"SelectSiteSearchAccess");
					click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"SelectSiteSearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"StreetNameAccess");
					sendKeys(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetName);
					
					verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"CountryAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);
					
					verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"CityTownAccess");
					sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);
					
					verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"PostalCodeAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);
					
					verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"PremisesAccess");
					sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);
				
					verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"SearchButtonAccess");
					click(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"SearchButtonAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify PickAddressButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On PickAddressButtonAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
					click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
					click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
					Reusable.waitForSiebelLoader();
					*/
					Reusable.waitForSiebelSpinnerToDisappear();
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify ServicePartyDropdownAccess");
					click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On ServicePartyDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Verify InputPartyNameAccess");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,Service_PartyName);
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On Search");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyName Submit Access");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Site Contact SearchAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"SiteContactSearchAccess");
					click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Site Contact DropdownAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.IPAccessObj.LastNameAccess,"SiteContractDropdownAccess");
					click(SiebelModeObj.IPAccessObj.LastNameAccess,"SiteContractDropdownAccess");
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
					sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, InputPartyName);
					
					verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Search");
					click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Search");
					
					Reusable.waitForSiebelLoader();
					verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
					click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
					Reusable.waitForSiebelLoader();

					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
					Reusable.waitForSiebelLoader();
					
			     if(Offnet.equals("Offnet"))	
			     {		    	 
						String Third_Party_Connection_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_Party_Connection_Reference");
						String Third_party_access_provider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_party_access_provider");
						String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
						
						verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
						click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
						Reusable.waitForSiebelLoader();
						verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						Reusable.waitForSiebelLoader();
										
						
			    	 	verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
						click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
						Reusable.waitForSiebelLoader();
						
						if(SLA_TierValue.contains("SLA"))
						{
							verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
							click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
							
						}
						else 
						{
						//	verifyExists(SiebelModeObj.IPAccessObj.Customersitepopupstatusoffnet1,"Verify Customersitepopupstatusoffnet");
						//	click(SiebelModeObj.IPAccessObj.Customersitepopupstatusoffnet1,"Click Customersitepopupstatusoffnet");
							verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
							click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
						}
						Reusable.waitForSiebelLoader();
						
						verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
						click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
						Reusable.waitForSiebelLoader();
						verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
						Reusable.waitForSiebelLoader();//
						
											
						verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
						click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click On ThirdpartyaccessproviderDropDown");
						Reusable.waitForSiebelLoader();
						if(SLA_TierValue.contains("SLA"))
						{
							verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Third_Party_Connection_Reference));
							click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Third_Party_Connection_Reference));
							//waitforAttributeloader();
						}
						else 
						{
							verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
							click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
						}
						verifyExists(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conection reference");
						click(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conection reference");
						sendKeys(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,Third_Party_Connection_Reference,"Enter Third party conection reference");
						Reusable.waitForSiebelLoader();
						
						if (!AccessType.equalsIgnoreCase("ULL Fibre")) 
						{
							verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown);
							click(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown);
						
							if (SLA_TierValue.contains("SLA"))  {
							
								verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",IPRange));
								click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",IPRange));
							}
						
						else {
							
							verifyExists(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
							click(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
							}
						}	
						Reusable.waitForSiebelLoader();
						verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
						click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
						Reusable.waitForSiebelLoader();
			     }
				  
			     else
			     {
			    	 verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
			    	 click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
//			    	 if(SLA_TierValue.contains("SLA"))
//			    	 {
			    		 verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
			    		 click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
			    		 //waitforAttributeloader();
//			    	 }
//			    	 else 
//			    	 {
//			    		 verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Verify AccessTypeSelectAccess");
//			    		 click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Verify AccessTypeSelectAccess");
//			    	 }	
			    	 
			    	 Reusable.waitForSiebelLoader();
			    	 
			    	 verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
			    	 click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
			    	 Reusable.waitForSiebelLoader();
//							verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
//							click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess," Verify AccesstechnologySelectAccess");
						
			    	verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",AccessTechnology));
					Reusable.waitForSiebelLoader();
					
//					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify BuildingTypeDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify BuildingTypeDropdownAccess");
//					Reusable.waitForSiebelLoader();
//					verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify BuildingTypeSelectAccess");
//					click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify BuildingTypeSelectAccess");
					
		    		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Building Type"));
		    		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Building Type"));
		    		Reusable.waitForSiebelLoader();
		    		
		    		verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BuildingType));
		    		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",BuildingType));
		    		Reusable.waitForSiebelLoader();
					
		    		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Customer Site Pop Status"));
		    		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Customer Site Pop Status"));
		    		Reusable.waitForSiebelLoader();
		    		
		    		verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CustomerSitePopStatus));
		    		click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",CustomerSitePopStatus));
		    		Reusable.waitForSiebelLoader();

//		    		verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess," Verify CustomerSitePopStatusDropdownAccess");
//					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess," Verify CustomerSitePopStatusDropdownAccess");
//					verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess," Verify CustomerSitePopStatusSelectAccess");
//					click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess," Verify CustomerSitePopStatusSelectAccess");

					
					Reusable.waitForSiebelLoader();
					
					verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"ClickheretoSaveAccess");
					Reusable.waitForSiebelLoader();
					Reusable.waitForSiebelSpinnerToDisappear();
				}
			}
				
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"CabinetType DropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"CabinetTypeDropdownAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"CabinetTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"CabinetTypeSelectAccess");
			
			Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
			
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify shelfid");
			sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			
			Reusable.waitForSiebelLoader();
			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
				
			}
			
			verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Slotid");
			sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Physicalportid");
			sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalPortID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
		
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Verify PresentationInterfaceDropdownAccess");
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Click PresentationInterfaceDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface ));
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Verify ConnectorTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Click ConnectorTypeDropdownAccess");
			Reusable.waitForSiebelLoader();
//				verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess," Verify ConnectorTypeSelectAccess");
//				click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess," Click ConnectorTypeSelectAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",ConnectorType));
			Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click FibreTypeDropdownAccess");
			Reusable.waitForSiebelLoader();
//				verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreTypeDropdownAccess),"Fibre Type Select Access");
//				click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreTypeDropdownAccess ),"Fibre Type SelectAccess");
		
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",FibreType));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role"));
			Reusable.waitForSiebelLoader();
//				verifyExists(SiebelModeObj.IPAccessObj.SelectValueDropdown.replace("Value","Physical Port"));
//				click(SiebelModeObj.IPAccessObj.SelectValueDropdown.replace("Value","Physical Port" ));
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",PortRole));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Verify InstallTimeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Click InstallTimeDropdownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Click InstallTimeSelectAccess");
			Reusable.waitForSiebelLoader();
			
//				verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
//				click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
//				Reusable.waitForSiebelLoader();
//				Reusable.waitForSiebelSpinnerToDisappear();
//				Reusable.waitForSiebelLoader();
		
			verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Access time window");
			sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType,"access time window");
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess1,"Router Country");
			click(SiebelModeObj.IPAccessObj.RouterCountryAccess1,"Router Country");
			Reusable.waitForSiebelLoader();
			//sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry,"Router Country");
			click(SiebelModeObj.IPAccessObj.RouterCountrySelect.replace("Value",RouterCountry_Access),"Select Router Country");
			//Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess1, Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.routermodel1,"router model");
			click(SiebelModeObj.IPAccessObj.routermodel1,"router model");
			Reusable.waitForSiebelLoader();
			click(SiebelModeObj.IPAccessObj.routermodelSelect.replace("Value",RouterModel),"Select Router model");
			//sendKeys(SiebelModeObj.IPAccessObj.routermodel,routermodel,"router model");
			//Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel1, Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			
			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
			Reusable.waitForSiebelLoader();
			
			//verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
			//click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");
			//Reusable.waitForSiebelLoader();
			//Reusable.waitForSiebelSpinnerToDisappear();
			
			String Alerting_NotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String SiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Name");

			if(SLA_TierValue.contains("SLA"))
			{
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Alerting_NotificationEmailAddress));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Alerting_NotificationEmailAddress));
				//waitforAttributeloader();
			}
			else 
			{
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
			}	
			
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		
	/*		verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
			click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");
		
			verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
//				verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
//				click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
//				verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPAdressingFormatSelectAccess");
//				click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");
			
			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",Ipv4AddressingType));
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();*/
		
			verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
			click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");

			verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
			// verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
			// click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");

			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value",IpAddressingFormat));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPAdressingFormatSelectAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
			click(SiebelModeObj.IPAccessObj.NextHopIp,"Next Hop IP");
			sendKeys(SiebelModeObj.IPAccessObj.NextHopIp,"10.91.112.101");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Routing Option"));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.searchInput.replace("Value","BGP"));
			click(SiebelModeObj.IPAccessObj.searchInput.replace("Value","BGP"));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Verify Crossbuttonforipaccess");
			click(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Click On Crossbuttonforipaccess");
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			Reusable.waitForSiebelSpinnerToDisappear();
			
			//GetReference(testDataFile,sheetName,scriptNo, dataSetNo);
		/*	verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
			click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");
			
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		
			String CircuitReference_Value = getTextFrom(SiebelModeObj.IPAccessObj.CircuitReferenceValue);
			DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);
			Reusable.waitForSiebelLoader();
			*/
			break;
	  
		}

		case "SIP Trunking":	{

			String IPGuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String IncludeColtTechnicalContact_in_Test_Calls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
			String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
			String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
			String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
			String IP_Range = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
			String AccessLineType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AccessLineType");


			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.VoiceServiceCountry,"Verify Voice Service Country");
			click(SiebelModeObj.SIPTrunkingObj.VoiceServiceCountry);
			verifyExists(SiebelModeObj.SIPTrunkingObj.NewVoiceServiceCountry,"Verify New Voice Service Country");
			click(SiebelModeObj.SIPTrunkingObj.NewVoiceServiceCountry);

			//	Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.CallAdmissionControl,"Verify CallAdmissionControl");
			sendKeys(SiebelModeObj.SIPTrunkingObj.CallAdmissionControl,"12");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
			sendKeys(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.EgressNumberFormatDropDownAccess,"Verify EgressNumberFormatDropDownAccess");
			click(SiebelModeObj.SIPTrunkingObj.EgressNumberFormatDropDownAccess);
			verifyExists(SiebelModeObj.SIPTrunkingObj.EgreeNUmberFormat,"Verify EgreeNUmberFormat");
			click(SiebelModeObj.SIPTrunkingObj.EgreeNUmberFormat);
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.InvlidCLITreatmet,"Verify InvlidCLITreatmet");
			sendKeys(SiebelModeObj.SIPTrunkingObj.InvlidCLITreatmet,"Allow");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs,"Verify TotalNumberDDIs");
			click(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs);
			sendKeys(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.IncomingDDIs,"Verify IncomingDDIs");
			click(SiebelModeObj.SIPTrunkingObj.IncomingDDIs);
			sendKeys(SiebelModeObj.SIPTrunkingObj.IncomingDDIs,"10");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.RoutingSequence,"Verify RoutingSequence");
			click(SiebelModeObj.SIPTrunkingObj.RoutingSequence);
			sendKeys(SiebelModeObj.SIPTrunkingObj.RoutingSequence,"Sequential");
			Reusable.waitForSiebelSpinnerToDisappear();
			//	Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","First Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","First Codec"));
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IPGuardianVariant));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IPGuardianVariant));

			verifyExists(SiebelModeObj.SIPTrunkingObj.ClickLink.replace("Value","Show More"));
			click(SiebelModeObj.SIPTrunkingObj.ClickLink.replace("Value","Show More"));

			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Second Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Second Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IncludeColtTechnicalContact_in_Test_Calls));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IncludeColtTechnicalContact_in_Test_Calls));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Third Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Third Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",ServiceBandwidth));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",ServiceBandwidth));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fourth Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fourth Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",TestWindow));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",TestWindow));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fifth Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fifth Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",CircuitIPAddressInput));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",CircuitIPAddressInput));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Sixth Codec"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Sixth Codec"));

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IP_Range));
			click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IP_Range));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.ClickToShowFullInfo,"Verify ClickToShowFullInfo");
			click(SiebelModeObj.SIPTrunkingObj.ClickToShowFullInfo);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSiteContact,"Verify SIPSiteContact");
			click(SiebelModeObj.SIPTrunkingObj.SIPSiteContact);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.ClickOK,"Verify OK");
			click(SiebelModeObj.SIPTrunkingObj.ClickOK);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			//	Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.ServiceParty,"Verify ServiceParty");
			click(SiebelModeObj.SIPTrunkingObj.ServiceParty);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.OKSelection,"Verify OKSelection");
			click(SiebelModeObj.SIPTrunkingObj.OKSelection);
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			//		Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry,"Verify SIPDeliveryTeamNewCountry");
			click(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry,"United Kingdom");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity,"Verify SIPDeliveryTeamNewCity");
			click(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity);
			sendKeys1(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity,"London");
			//			Reusable.SendkeaboardKeys(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity, Keys.ENTER);
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.CloseSymbol,"Verify CloseSymbol");
			click(SiebelModeObj.SIPTrunkingObj.CloseSymbol,"CloseSymbol");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.Save();

			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbol,"Verify SIPVoiceConfigExpandSymbol");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbol);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence,"Verify SIPTrunkSequence");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkName,"Verify SIPTrunkName");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkName,"Trunk2");
			Reusable.waitForSiebelSpinnerToDisappear();
			//Some below codes commented as we are facing functional issue.

			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessLineTypeDropDown,"Verify AccessLineTypeDropDown");
			click(SiebelModeObj.SIPTrunkingObj.AccessLineTypeDropDown);

			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType),"Verify AccessLineType");
			click(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType));


			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup,"Verify AccessServiceIdlookup");
			click(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Verify SearchServiceIDbox");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox);
		//	sendKeys(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Product");
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Status"); //Updated for IP 2020

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchText,"Verify SearchText");
			click(SiebelModeObj.SIPTrunkingObj.SearchText);
		//	sendKeys(SiebelModeObj.SIPTrunkingObj.SearchText,"IP Access");
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchText,"Completed"); //Updated for IP 2020

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess,"Verify SearchButtonProcess");
			click(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess);
			Reusable.waitForSiebelLoader();

			ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton);
			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton,"Verify SearchServiceIdOKButton");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4,"Verify SIPCustomerOriginatingIPv4");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4,"10.7.235.29/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6,"Verify SIPCustomerOriginatingIPv6");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6,"10.7.235.29/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4,"Verify SIPCustomerTerminatingIPv4");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4,"10.7.235.2/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6,"Verify SIPCustomerTerminatingIPv6");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6,"10.7.235.2/24");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocol,"Verify SIPSignalingTransportProtocol");
			click(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocol);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocolDropdown,"Verify SIPSignalingTransportProtocolDropdown");
			click(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocolDropdown);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocol,"Verify SIPvoIPProtocol");
			click(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocol);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocolDropDown,"Verify SIPvoIPProtocolDropDown");
			click(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocolDropDown);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType,"Verify SIPEncryptionType");
			click(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType,"TLS on, SRTP off");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent,"Verify SIPProportionPercent");
			click(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent,"12");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Verify SIPTrunkSiteNameAlias");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Alias1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNATRequired,"Verify SIPNATRequired");
			click(SiebelModeObj.SIPTrunkingObj.SIPNATRequired);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNATRequired,"N");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address,"Verify SIPNNIWANIPv4Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address,"Verify SIPNNIWANIPv6Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT,"Verify SIPIPv4AddressRangeNAT");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT,"2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange,"Verify SIPVLANStartEndRange");
			click(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange,"2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVLANID,"Verify SIPVLANID");
			click(SiebelModeObj.SIPTrunkingObj.SIPVLANID);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPVLANID,"2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPASNumber,"Verify SIPASNumber");
			click(SiebelModeObj.SIPTrunkingObj.SIPASNumber);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPASNumber,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address,"Verify SIPColtHostEquipmentIPv4Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address,"Verify SIPColtHostEquipmentIPv6Address");
			click(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address,"10.7.235.24/2");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls,"Verify SIPTrunkMaxNumberofSimoultaniousCalls");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls,"4");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit,"Verify SIPTrunkInboundOutboundSplit");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit,"No");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls,"Verify SIPTrunkMaxInboundCalls");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk,"Verify SIPTrunkOverCallLimitForTrunk");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk,"Verify SIPTrunkIncallLimitForTrunk");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk,"10");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk,"Verify SIPTrunkOutCallLimitForTrunk");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC,"Verify SIPTrunkTypeofCAC");
			click(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC,"Individual Trunk CAC");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerIPPbxIdTab,"Verify SIPCustomerIPPbxIdTab");
			click(SiebelModeObj.SIPTrunkingObj.SIPCustomerIPPbxIdTab);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPAddrecordSymbolIPPBX,"Verify SIPAddrecordSymbolIPPBX");
			click(SiebelModeObj.SIPTrunkingObj.SIPAddrecordSymbolIPPBX);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasName,"Verify SIPIPPBXAliasName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasName);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField,"Verify SIPIPPBXAliasNameInputField");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField,"test1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModel,"Verify SIPIPPBXModel");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModel);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModelInput,"SWE Lite7.0");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendor,"Verify SIPIPPBXVendor");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendor);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput,"Verify SIPIPPBXVendorInput");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput,"Sonus");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSOftwareVersionField,"Verify SIPSOftwareVersionField");
			click(SiebelModeObj.SIPTrunkingObj.SIPSOftwareVersionField);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput,"Verify SIPSoftwareVersionInput");
			click(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput,"1");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUID,"Verify SIPIPPBXAddressUID");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUID);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXLookupoptionClick,"Verify SIPIPPBXLookupoptionClick");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXLookupoptionClick);

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName,"Verify SIPIPPBXAddressUIDStreetName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName,"Schmidtstrasse");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName,"Verify SIPIPPBXAddressUIDStreetName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName,"Germany");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName,"Verify SIPIPPBXAddressUIDStreetName");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName,"Frankfurt");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode,"Verify SIPIPPBXAddressUIDPostalCode");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode,"60326");
			Reusable.SendkeaboardKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode, Keys.TAB);
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode,"Verify SIPIPPBXAddressUIDPremiseCode");
			click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode,"Verify SIPIPPBXAddressUIDPremiseCode");
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode,"16");

			// Click Address search button
			Reusable.waitForSiebelLoader();
			ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPAddressUIDSearchButton);
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPAddressUIDSearchButton,"Verify SIPAddressUIDSearchButton");
			click(SiebelModeObj.SIPTrunkingObj.SIPAddressUIDSearchButton,"Search button");	

			// Select address record
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSelectAddressRecord,"Verify SIPSelectAddressRecord");
			click(SiebelModeObj.SIPTrunkingObj.SIPSelectAddressRecord,"Address Record");

			// Click Pick address button
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPickAddressButtonForUID,"Verify SIPPickAddressButtonForUID");
			click(SiebelModeObj.SIPTrunkingObj.SIPPickAddressButtonForUID,"Pick Address button");

			// IPBX Technical Contact
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContact,"Verify SIPPBXTechnicalContact");
			click(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContact);
			Reusable.waitForSiebelLoader();
					
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTecnicalContactLookup,"Verify SIPPBXTecnicalContactLookup");
			click(SiebelModeObj.SIPTrunkingObj.SIPPBXTecnicalContactLookup,"SIPPBXTecnicalContactLookup");
			Reusable.waitForSiebelLoader();
			waitToPageLoad();
			
			ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContactLookupOKButton);
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContactLookupOKButton,"Verify SIPPBXTechnicalContactLookupOKButton");
			click(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContactLookupOKButton,"SIPPBXTechnicalContactLookupOKButton");
			Reusable.savePage();
			//		Reusable.Save();
			Reusable.waitForSiebelLoader();

			// click on Voice Config tab to enter the IP PBX details
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigTab,"Verify SIPVoiceConfigTab");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigTab,"SIPVoiceConfigTab");

			// Selection of IP PBX in TRUNK --
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbolIPPBXSelewction,"Verify SIPVoiceConfigExpandSymbolIPPBXSelewction");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbolIPPBXSelewction,"SIPVoiceConfigExpandSymbolIPPBXSelewction");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPReClickToIPPBXLookUp,"Verify SIPReClickToIPPBXLookUp");
			click(SiebelModeObj.SIPTrunkingObj.SIPReClickToIPPBXLookUp,"SIPReClickToIPPBXLookUp");
			Reusable.waitForSiebelLoader();

			ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPOKSelectionButton);
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPOKSelectionButton,"Verify SIPOKSelectionButton");
			click(SiebelModeObj.SIPTrunkingObj.SIPOKSelectionButton,"SIPOKSelectionButton");
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();
			/* ABove code for Trunk including IPPBX code */

			// Other Tab in Voice Config

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPOtherTab,"Verify Other Tab");
			click(SiebelModeObj.SIPTrunkingObj.SIPOtherTab,"Other Tab");
			Reusable.waitForSiebelLoader();
			/*	
			if (isElementPresent(By.xpath("//div[@id='colt-s-order-connection-header']//a[text()='Save']"))) {
				Reusable.Save();
				Reusable.waitForAjax();
			}

			 */	
			waitForElementToAppear(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,30);
			ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,"Verify SIPVoiceConfigOthersTabShowFullInfo");
			//		Reusable.waitForSiebelLoader();
			//		ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);
			//		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,"SIPVoiceConfigOthersTabShowFullInfo");
			javaScriptclick(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);

			Reusable.waitForSiebelLoader();	

			/*			if (isElementPresent(By.xpath("(//*[normalize-space(text()) and normalize-space(.)='Colt DDI Ranges'])[1]/preceding::span[1]"))) {
				break;
			}else{
				javaScriptclick(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);

			}

			Reusable.waitForSiebelLoader();

			 */	
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRanges,"Verify SIPVoiceConfigOthersTabColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRanges,"SIPVoiceConfigOthersTabColtDDIRanges");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs,"Verify SIPNumberOfSingleColtDDIs");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges,"Verify SIPNumberOf10ColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges,"Verify SIPNumberOf100ColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges,"Verify SIPNumberOf1000ColtDDIRanges");
			click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges,"1");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRangesCloseWindow,"Verify SIPVoiceConfigOthersTabColtDDIRangesCloseWindow");
			click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRangesCloseWindow,"SIPVoiceConfigOthersTabColtDDIRangesCloseWindow");

			Reusable.Save();
			//	Reusable.savePage();
			Reusable.waitForSiebelLoader();
			break;  
		} 

		case "Managed Virtual Firewall": {

			//verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFBillingTab,"Verify MVFBilling Tab");
			//click(SiebelModeObj.ManagedVirtualFirewallObj.MVFBillingTab,"MVFBilling Tab");
			//Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFShowfullInfo,"Verify MVFShowfullInfo");
			click(SiebelModeObj.ManagedVirtualFirewallObj.MVFShowfullInfo);

			verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink,"Verify MVFSecurityPolicyAttachmentlink");
			//		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink);
			sendKeys(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink, "1234");
			Reusable.waitForSiebelSpinnerToDisappear();


			//	verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFAddbutton,"Verify MVFAddbutton");
			//	click(SiebelModeObj.ManagedVirtualFirewallObj.MVFAddbutton);

			verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFClosebutton,"Verify MVFClosebutton");
			click(SiebelModeObj.ManagedVirtualFirewallObj.MVFClosebutton,"MVF Close button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			//		verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFSavebutton,"Verify MVFSavebutton");
			//		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFSavebutton);
			//		Reusable.waitForSiebelLoader();	

			break;
		}
		case "Ethernet VPN Access": {

			String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
			String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
			String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
			String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City/Town");
			String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
			String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
			String InputPartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party_Name");	
			String InputSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_LastName");
			String Offnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Offnet");
			String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Access_Type");
			String Third_party_access_provider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "3rd_party_access_provider");
			String Third_Party_Connection_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "3rd_Party_Connection_Reference");
			String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IP_Range");
			String BCPReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BCP_Reference");
			String CabinetID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_ID");
			String ShelfID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Shelf_ID");
			String SlotID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
			String Physical_PortID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
			String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
			String FibreType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
			String CustomerSitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Site_Pop_Status");
						
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
			click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Click on ServiceBandwidthDropdownAccess");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
			click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.LinkTypeDropDown,"Link Type Drop Down");
			click(SiebelModeObj.EthernetVPNAccessObj.LinkTypeDropDown,"Link Type Drop Down");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.LinkTYpeValue,"Verify LinkTYpeValue");
			click(SiebelModeObj.EthernetVPNAccessObj.LinkTYpeValue,"Click on LinkTYpeValue");
			Reusable.waitForSiebelSpinnerToDisappear();


			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ResilenceOptionDropDown,"Verify ResilenceOptionDropDown");
			click(SiebelModeObj.EthernetVPNAccessObj.ResilenceOptionDropDown,"Click on ResilenceOptionDropDown");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ResilienceValue,"Verify Resilience Value");
			click(SiebelModeObj.EthernetVPNAccessObj.ResilienceValue,"Click on Resilience Value");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.OSSPlatformDropDown,"Verify OSSPlatformDropDown");
			click(SiebelModeObj.EthernetVPNAccessObj.OSSPlatformDropDown,"Click on OSSPlatformDropDown");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.OSSplatformValue,"Verify OSSplatform Value");
			click(SiebelModeObj.EthernetVPNAccessObj.OSSplatformValue,"Click on OSSplatform Value");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
			click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
			//Reusable.savePage();
			Reusable.waitForSiebelLoader();
		//Below commented site id code moved at top
			/*
			//ScrollIntoViewByString(SiebelModeObj.EthernetVPNAccessObj.SelectSiteSearchAccess);
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
			click(SiebelModeObj.EthernetVPNAccessObj.SelectSiteSearchAccess,"Click on SelectSiteSearchAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForElementToAppear(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput, 10);
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"Site ID input");

			if(Offnet.equals("Offnet"))
			{
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"LON06215/026");
			}else{
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"MAD99900/011");
			}

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.SiteIDSearchBtn,"SiteIDSearchBtn");
			click(SiebelModeObj.EthernetVPNAccessObj.SiteIDSearchBtn,"SiteIDSearchBtn");
			Reusable.waitForSiebelLoader();
			 */





			//////////////////////////// below code commets
			/*
		Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.StreetNameAccess, 10);
		verifyExists(SiebelModeObj.VoiceConfigTab.StreetNameAccess,"StreetNameAccess");
		sendKeys(SiebelModeObj.VoiceConfigTab.StreetNameAccess,StreetName);

		Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CountryAccess, 10);
		verifyExists(SiebelModeObj.VoiceConfigTab.CountryAccess,"Enter Country");
		sendKeys(SiebelModeObj.VoiceConfigTab.CountryAccess,Country);

		verifyExists(SiebelModeObj.VoiceConfigTab.CityTownAccess,"Enter CityTownAccess");
		sendKeys(SiebelModeObj.VoiceConfigTab.CityTownAccess,CityORTown);

		verifyExists(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,"Postal Code");
		sendKeys(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,PostalCode);

		verifyExists(SiebelModeObj.VoiceConfigTab.PremisesAccess,"Premises");
		sendKeys(SiebelModeObj.VoiceConfigTab.PremisesAccess,Premises);

		verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Verify SearchButtonAccess");
		click(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Click on SearchButtonAccess");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Pick Address Access");
		javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Address for Site");
		verifyExists(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
		click(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
		Reusable.waitForSiebelSpinnerToDisappear();


		verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
		javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
		verifyExists(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Buiding for Site");
		click(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Address for Site");
		Reusable.waitForSiebelSpinnerToDisappear();
			 */
			////////////above code commets





/*
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
			javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			click(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
*/
			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Service Party Search Access");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click on Service Party SearchAccess");


			///////////////////////////////////////// below code commets
			/*
		waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess, 8);
		verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify Service party drop down Acccess");
		click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click on Service party drop down Acccess");

		verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify Site Contact drop down Acccess");
		click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click on site Contact drop down Acccess");

		verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
		sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,InputPartyName);

		verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
		click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click on search");

			 */
			///////////////////////////above code commets


			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyNameSubmitAccess");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click on PartyName Submit");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click on SiteContactSearchAccess");
			Reusable.waitForSiebelLoader();


			////////////////////////////////////////////////////below code comment

			/*
		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactDropdownAccess");
		click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click on SiteContactDropdownAccess");

		verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Input Site Name");
		sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,InputSiteName);

		verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
		click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click on LastNameSiteSearch");
			 */
			/////////////////////////////////////////////////////////Above code commet


			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify Pick Contact:OK");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click on Pick Contact:OK");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();





			if(Offnet.equals("Offnet"))
			{
				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click on AccessTypeDropdownAccess");
				verifyExists(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("Value",AccessType));
				click(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("Value",AccessType));
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click on ThirdpartyaccessproviderDropDown");
				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click on AccesstechnologyDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyEhternetValue,"Verify AccesstechnologySelectAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologyEhternetValue,"Click on AccesstechnologySelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conectionre ference");
				sendKeys(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,Third_Party_Connection_Reference);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click on BuildingTypeDropdownAccess");
				Reusable.waitForSiebelSpinnerToDisappear();
				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeOFFNetSelectAccess,"Verify BuildingTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeOFFNetSelectAccess,"Click on BuildingTypeSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click on CustomerSitePopStatusDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusOFFnetSelectAccess.replace("Value",CustomerSitePopStatus),"Verify CustomerSitePopStatusSelectAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusOFFnetSelectAccess.replace("Value",CustomerSitePopStatus),"Click on CustomerSitePopStatusSelectAccess");
				
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Verify BCPReference");
				click(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Click on BCPReference");
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.BCPReference,BCPReference);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForSiebelLoader();


				//verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Verify ThirdpartyDropDown");
				//click(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Click on ThirdpartyDropDown");
				verifyExists(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Third Party SLA Tier"),"Third Party SLA Tier");
				click(SiebelModeObj.AEndSiteObj.AEndSiteDropDown.replace("Value","Third Party SLA Tier"),"Third Party SLA Tier");
				
				verifyExists(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
				click(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
				click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
				Reusable.waitForSiebelLoader();

			}
			else
			{

				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click on AccessTypeDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click on AccessTypeSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();


				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click on AccesstechnologyDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click on AccesstechnologySelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click on BuildingTypeDropdownAccess");
				Reusable.waitForSiebelSpinnerToDisappear();
				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify BuildingTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click on BuildingTypeSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click on CustomerSitePopStatusDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click on CustomerSitePopStatusSelectAccess");
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Verify BCPReference");
				click(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Click on BCPReference");
				sendKeys(SiebelModeObj.EthernetVPNAccessObj.BCPReference,BCPReference);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
				click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
				Reusable.waitForSiebelLoader();

			}
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click on CabinetTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click on CabinetTypeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify ShelfID");
			sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			//	verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			//	click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");

			Reusable.savePage();

			verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Slotid");
			sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Physicalportid");
			sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,Physical_PortID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceinput,"Verify PresentationInterfaceinput");
			sendKeys(SiebelModeObj.IPAccessObj.PresentationInterfaceinput,PresentationInterface);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.PresentationInterfaceinput, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click on ConnectorTypeDropdownAccess");

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.ConnectorTypeSelect,"Verify ConnectorTypeSelect");
			click(SiebelModeObj.EthernetVPNAccessObj.ConnectorTypeSelect,"Click on ConnectorTypeSelect");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click on FibreTypeDropdownAccess");

			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
			click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
			click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click on PortRoleDropDown");

			verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
			click(SiebelModeObj.IPAccessObj.PortValue,"Click on PortValue");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click on InstallTimeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click on InstallTimeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();


			addNetwork();
			verifyExists(SiebelModeObj.Site.SiteTab,"SiteTab");
			click(SiebelModeObj.Site.SiteTab,"SiteTab");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click on RouterSiteNameDropdownAccess");
			verifyExists(SiebelModeObj.EthernetVPNAccessObj.sitenamevaluevpn,"Verify sitenamevaluevpn");
			click(SiebelModeObj.EthernetVPNAccessObj.sitenamevaluevpn,"Click on sitenamevaluevpn");
			//	Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
			click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
			click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
			click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
			click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.savePage();


			/*	

		Random rand = new Random();
		String siteA = null;
		String siteB = null;
		String CircuitReference_Value =null;

		verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
		click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");

	//	String CircuitReference_Value = getTextFrom((SiebelModeObj.IPAccessObj.CircuitReferenceValue),"Value");
		CircuitReference_Value = getAttributeFrom((SiebelModeObj.IPAccessObj.CircuitReferenceValue),"Value");



		if(CircuitReference_Value.isEmpty()==true)
		{
			if(isElementPresent(By.xpath("//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site A address"))
			{

				     String siteA1 = getAttributeFrom("@xpath=//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']","title");     
				     siteA=siteA1.substring(0, 3);
			}
			if(isElementPresent(By.xpath("//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site B address"))
			{
			     String siteB1 = getAttributeFrom("@xpath=//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']","title");

			     siteB=siteB1.substring(0, 3);
			}


			if (siteA != null && siteB!= null)
			{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitReference_Value = siteA+"/"+siteB+"/"+"LE-"+randnumb;
			}else{
				if(siteA != null)
				{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitReference_Value = siteA+"/"+siteA+"/"+"LE-"+randnumb;
				}else{
					int randnumb = rand.nextInt(900000) + 100000;
					CircuitReference_Value = siteB+"/"+siteB+"/"+"LE-"+randnumb;
				}

			}
			sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, CircuitReference_Value,"Circuit Reference Number");		
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
		}
		Reusable.savePage();
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	

			 */	
			Reusable.waitForSiebelLoader();
			break;
		}

		////0306
		case "Interconnect": 

		{
			String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
			String Coverage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Coverage");
			String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String IPGuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String IncludeColtTechnicalContactinTestCalls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
			String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
			String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
			String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
			String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
			String SiteContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Contact");
			String OrderReceivedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Received_Date");
			String ColtActualDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
			String Contractterm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_term");
			String OrderSignedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
			String AendCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
			String AendCityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");


			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountry,"VoiceServiceCountry");
			click(SiebelModeObj.Interconnect.VoiceServiceCountry,"Click on voice service country");

			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", BEndResilienceOption),BEndResilienceOption);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", BEndResilienceOption),BEndResilienceOption);
			System.out.println("go to if loop of call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.CallAdmissionControl,"CallAdmissionControl");
			sendKeys(SiebelModeObj.Interconnect.CallAdmissionControl,"8", "Enter call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.CallAdmissionControl, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.NumberOfSignallingTrunks,"CallAdmissionControl");
			sendKeys(SiebelModeObj.Interconnect.NumberOfSignallingTrunks,"4", "Enter call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.NumberOfSignallingTrunks, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.VoiceConfigurationReference,"CallAdmissionControl");
			sendKeys(SiebelModeObj.Interconnect.VoiceConfigurationReference,"yes", "Enter call admission control");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.VoiceConfigurationReference, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.TrafficDirection,"TrafficDirection");
			click(SiebelModeObj.Interconnect.TrafficDirection,"Click on TrafficDirection");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", Coverage),Coverage);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", Coverage));
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Incoming DDI Digits"),"Incoming DDI Digits Drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Incoming DDI Digits"),"Incoming DDI Digits Drop down");
			verifyExists(SiebelModeObj.Interconnect.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);
			click(SiebelModeObj.Interconnect.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "First Codec"),"First Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "First Codec"),"First Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value", IPGuardianVariant),IPGuardianVariant);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value", IPGuardianVariant),IPGuardianVariant);

			verifyExists(SiebelModeObj.Interconnect.ClickLink.replace("Value", "Show More"),"Show More link");
			click(SiebelModeObj.Interconnect.ClickLink.replace("Value", "Show More"),"Show More link");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Second Codec"),"Second Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Second Codec"),"Second Codec drop down");
			
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPGuardianVariant),IPGuardianVariant);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPGuardianVariant ),IPGuardianVariant);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Third Codec"),"Third Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Third Codec"),"Third Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",ServiceBandwidth ),ServiceBandwidth);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",ServiceBandwidth ),ServiceBandwidth);


			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fourth Codec"),"fourth Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fourth Codec"),"fourth Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",TestWindow),TestWindow);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",TestWindow),TestWindow);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fifth Codec"),"fifth Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fifth Codec"),"fifth Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",CircuitIPAddressInput),CircuitIPAddressInput);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",CircuitIPAddressInput),CircuitIPAddressInput);

			verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Sixth Codec"),"Sixth Codec drop down");
			click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Sixth Codec"),"Sixth Codec drop down");
			verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPRange),IPRange);
			click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPRange),IPRange);

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"Save Button");
			click(SiebelModeObj.Interconnect.SaveButton,"save button");
			Reusable.waitForSiebelLoader();



			verifyExists(SiebelModeObj.Interconnect.ClickToShowFullInfo,"Click To Show Full Info");
			click(SiebelModeObj.Interconnect.ClickToShowFullInfo,"click to show full info");
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.Interconnect.SiteContact,"Site Contact");
			click(SiebelModeObj.Interconnect.SiteContact,"site contact");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.Interconnect.OkButton,"Ok Button");
			click(SiebelModeObj.Interconnect.OkButton,"Click on ok button of site contact");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.ServiceParty,"Service Party");
			click(SiebelModeObj.Interconnect.ServiceParty,"Click on service party");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.Interconnect.OkButtonService,"OkButtonService");
			click(SiebelModeObj.Interconnect.OkButtonService,"Click on ok button of service party");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.DelieveryTeamCountry,"Delievery Team Country drop down");
			click(SiebelModeObj.Interconnect.DelieveryTeamCountry,"Delievery Team Country drop down");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",SiteContact),SiteContact);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",SiteContact),SiteContact);
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.Interconnect.DeliveryTeamCity,"Delivery Team City");
			sendKeys(SiebelModeObj.Interconnect.DeliveryTeamCity,"Shanghai", "Delivery team city");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.DeliveryTeamCity, Keys.TAB);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.CrossButtonTrunk,"CrossButtonTrunk");
			click(SiebelModeObj.Interconnect.CrossButtonTrunk,"Click on cross button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
			click(SiebelModeObj.Interconnect.SaveButton,"Click on SaveButton");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.TrunkSequence,"Trunk Sequence");
			sendKeys(SiebelModeObj.Interconnect.TrunkSequence,"1", "trunk sequence");


			verifyExists(SiebelModeObj.Interconnect.TrunkName,"Trunk Name");
			sendKeys(SiebelModeObj.Interconnect.TrunkName,"Trunk1", "Enter value in trunk name");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.AccessLineType,"AccessLineType");
			click(SiebelModeObj.Interconnect.AccessLineType,"Click on access line type");	
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderSignedDate),OrderSignedDate);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderSignedDate),OrderSignedDate);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.AccessServiceID,"AccessServiceID");
			click(SiebelModeObj.Interconnect.AccessServiceID,"Click on access service id");
			Reusable.waitForSiebelLoader();
			//New code//
			verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Verify on Pick Account Dropdown");
			click(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Click on Pick Account Dropdown");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.PickServicePartyObj.AList.replace("Value","Status"));
			click(SiebelModeObj.PickServicePartyObj.AList.replace("Value","Status"));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Verify on Pick Account Dropdown Value");
			click(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Click on Pick Account Dropdown Value");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Completed","Account Dropdown Value");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.NumberHostingObj.PickAccountGo,"Verify Go");
			click(SiebelModeObj.NumberHostingObj.PickAccountGo,"Click on Go");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			//////////////////////////////

			ScrollIntoViewByString(SiebelModeObj.Interconnect.OkButtonAccess);
			verifyExists(SiebelModeObj.Interconnect.OkButtonAccess,"OkButtonAccess");
			click(SiebelModeObj.Interconnect.OkButtonAccess,"Click on ok button");

			Reusable.waitForSiebelLoader();				

			if(isElementPresent(By.xpath("//a[text()='Show full info']")))	
			{								
				System.out.println("full trunk info is available");
				verifyExists(SiebelModeObj.Interconnect.ShowFullInfoTrunk,"OkButtonAccess");
				click(SiebelModeObj.Interconnect.ShowFullInfoTrunk,"Click on ShowFullInfoTrunk");
				Reusable.waitForSiebelLoader();
			}

			verifyExists(SiebelModeObj.Interconnect.AddButtonTrunk,"AddButtonTrunk");
			click(SiebelModeObj.Interconnect.AddButtonTrunk,"Click on plus button");


			verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP4Address,"CustomerOriginatingIP4Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP4Address,"192.168.1.1", "value in customer originating IPV4 address");

			verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"CustomerOriginatingIP6Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"192.168.1.1", "value in customer originating IPV6 address");

			verifyExists(SiebelModeObj.Interconnect.CustomerTerminatingIP4Address,"CustomerTerminatingIP4Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerTerminatingIP4Address,"38.168.1.1", "value in customer originating IPV4 address");

			verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"CustomerOriginatingIP6Address");
			sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"38.168.1.1", "value in customer originating IPV6 address");

			verifyExists(SiebelModeObj.Interconnect.SignallingTransportProtocol,"SignallingTransportProtocol");
			click(SiebelModeObj.Interconnect.SignallingTransportProtocol,"Click on signalling transport protocol");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderReceivedDate),OrderReceivedDate);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderReceivedDate),OrderReceivedDate);

			verifyExists(SiebelModeObj.Interconnect.VoipProtocol,"VoipProtocol");
			click(SiebelModeObj.Interconnect.VoipProtocol,"Click on voip protocol");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",ColtActualDate),ColtActualDate);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",ColtActualDate),ColtActualDate);

			verifyExists(SiebelModeObj.Interconnect.EncryptionProtocol,"EncryptionProtocol");
			click(SiebelModeObj.Interconnect.EncryptionProtocol,"Click on encryption protocol");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",Contractterm),Contractterm);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",Contractterm),Contractterm);

			verifyExists(SiebelModeObj.Interconnect.NATRequired,"NATRequired");
			sendKeys(SiebelModeObj.Interconnect.NATRequired,"N", "Enter value in NATR");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.NATRequired, Keys.TAB);

			verifyExists(SiebelModeObj.Interconnect.InboundOutboundSplit,"InboundOutboundSplit");
			sendKeys(SiebelModeObj.Interconnect.InboundOutboundSplit,"No", "Enter value in InboundOutboundSplit field");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.InboundOutboundSplit, Keys.TAB);

			verifyExists(SiebelModeObj.Interconnect.TypeOfCAC,"TypeOfCAC");
			click(SiebelModeObj.Interconnect.TypeOfCAC,"TypeOfCAC");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCountry),AendCountry);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCountry),AendCountry);

			verifyExists(SiebelModeObj.Interconnect.CrossButtonTrunk,"CrossButtonTrunk");
			click(SiebelModeObj.Interconnect.CrossButtonTrunk,"Click on cross button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
			click(SiebelModeObj.Interconnect.SaveButton,"Click on Save Button");
			Reusable.waitForSiebelLoader();
			Reusable.savePage();

			verifyExists(SiebelModeObj.Interconnect.VoiceTab,"VoiceTab");
			click(SiebelModeObj.Interconnect.VoiceTab,"Click on voice feature tab");
			Reusable.waitForSiebelLoader();
			// 			waitToPageLoad();

			verifyExists(SiebelModeObj.Interconnect.Language,"Language");
			sendKeys(SiebelModeObj.Interconnect.Language,"English", "Select language");
			Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.Language, Keys.TAB);

			verifyExists(SiebelModeObj.Interconnect.Resillence,"resillence dropdown");
			click(SiebelModeObj.Interconnect.Resillence,"Click on resillence dropdown");
			verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCityTown),AendCityTown);
			click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCityTown),AendCityTown);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
			click(SiebelModeObj.Interconnect.SaveButton,"Click on SaveButton");
			Reusable.waitForSiebelLoader();
			//	waitToPageLoad();
			try {
				verifyExists(SiebelModeObj.Interconnect.ServiceGroupTab,"ServiceGroupTab");
				click(SiebelModeObj.Interconnect.ServiceGroupTab,"Click on service tab");
			}
			catch(Exception ex) {
				Reusable.Select1(SiebelModeObj.Interconnect.InstalltionDropdown,"Service Group");
			}

			Reusable.waitForSiebelLoader();
			//		waitToPageLoad();

			verifyExists(SiebelModeObj.Interconnect.ClickOnPlusButtonService,"ServiceGroupReference");
			click(SiebelModeObj.Interconnect.ClickOnPlusButtonService,"Click on plus button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.ServiceGroupReference,"ServiceGroupReference");
			click(SiebelModeObj.Interconnect.ServiceGroupReference,"Click on ServiceGroupReference button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.Interconnect.ClickOkButton,"ClickOkButton");
			click(SiebelModeObj.Interconnect.ClickOkButton,"Click on ok button");
			Reusable.waitForSiebelLoader();

			break;

		}
		case "IP Domain": {

	  		String DomainName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Domain_Name");


	  		System.out.println("Enter Middle Applet");
	  		
	  		System.out.println("Move Performed");

	  		verifyExists(SiebelModeObj.IPDomain.Clicktoshowfullinfomiddle,"Verify Clicktoshowfullinfomiddle");
	  		click(SiebelModeObj.IPDomain.Clicktoshowfullinfomiddle,"Click onClicktoshowfullinfomiddle");
	  		verifyExists(SiebelModeObj.IPDomain.Domainname," Verify SearchInput");
	  		sendKeys(SiebelModeObj.IPDomain.Domainname,"1","DomainName");
			
	  		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Domain Order Type"));
			click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Domain Order Type"));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPDomain.DomainordertypeData,"Verify DomainordertypeData");
	  		click(SiebelModeObj.IPDomain.DomainordertypeData,"Click on DomainordertypeData");
			Reusable.waitForSiebelLoader();

	  		verifyExists(SiebelModeObj.IPDomain.DNStype,"Verify DNStype");
	  		click(SiebelModeObj.IPDomain.DNStype,"Click on DNStype");
			Reusable.waitForSiebelLoader();
	  		verifyExists(SiebelModeObj.IPDomain.DNSTypeInput,"Verify DNSTypeInput");
	  		click(SiebelModeObj.IPDomain.DNSTypeInput,"Click on DNSTypeInput");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPDomain.Crossbutton,"Verify Crossbutton");
	  		click(SiebelModeObj.IPDomain.Crossbutton,"Click on Crossbutton");
	  		//verifyExists(SiebelModeObj.IPDomain.SaveButtonClick,"Verify SaveButtonClick");
	  		//click(SiebelModeObj.IPDomain.SaveButtonClick,"Click on SaveButtonClick");
	  		//System.out.println("Middle Complete");
	  		Reusable.savePage();
	  		break;
		}
		case "IP Guardian": {

  			String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
  			String AutoMitigation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");
  			String CustomerDNSResolvers = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_DNS_Resolvers");
  			String IPGuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
  			String IncludeColtTechnicalContactinTestCalls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
  			String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
  			String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
  			String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
  			String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");

  			System.out.println("Enter Gurdian Middle Applet");
  			
  			verifyExists(SiebelModeObj.IPGuardian.AlertingNotification," Verify AlertingNotification");
  			sendKeys(SiebelModeObj.IPGuardian.AlertingNotification,AlertingNotificationEmailAddress,"Enter AlertingNotification");

  			verifyExists(SiebelModeObj.IPGuardian.Automigration," Verify AutoMitigation");
  			sendKeys(SiebelModeObj.IPGuardian.Automigration,AutoMitigation,"Enter AutoMitigation");

  			verifyExists(SiebelModeObj.IPGuardian.Customerdnsresolve," Verify Customerdnsresolve");
  			sendKeys(SiebelModeObj.IPGuardian.Customerdnsresolve,CustomerDNSResolvers,"Enter Customer dns resolve");

  			verifyExists(SiebelModeObj.IPGuardian.IpguardianVariant," Verify IpguardianVariant");
  			sendKeys(SiebelModeObj.IPGuardian.IpguardianVariant,IPGuardianVariant,"Enter IP guardiant variant");
  			verifyExists(SiebelModeObj.IPGuardian.Colttechnicaltestcalls," Verify Colttechnicaltestcalls");
  			sendKeys(SiebelModeObj.IPGuardian.Colttechnicaltestcalls,IncludeColtTechnicalContactinTestCalls,"Enter Enter Colt technical cells");

  			verifyExists(SiebelModeObj.IPGuardian.servicebandwidth," Verify servicebandwidth");
  			sendKeys(SiebelModeObj.IPGuardian.servicebandwidth,ServiceBandwidth,"Enter service bandwidth");

  			verifyExists(SiebelModeObj.IPGuardian.Testwindowipaccess," Verify servicebandwidth");
  			sendKeys(SiebelModeObj.IPGuardian.Testwindowipaccess,TestWindow,"Enter Test Window");

  			//verifyExists(SiebelModeObj.IPGuardian.IpGurdianSave," Verify IpGurdianSave");
  			//click(SiebelModeObj.IPGuardian.IpGurdianSave,"Click on IpGurdianSave");

  			Reusable.waitForSiebelLoader();
  		  //	waitToPageLoad();

  			mouseMoveOn(SiebelModeObj.IPGuardian.CircuitipaddressClick);
  		
  			verifyExists(SiebelModeObj.IPGuardian.CircuitipaddressClick," Verify CircuitipaddressClick");
  			click(SiebelModeObj.IPGuardian.CircuitipaddressClick,"click on circuit ip address");

  			verifyExists(SiebelModeObj.IPGuardian.CircuitIPAddressInput," Verify CircuitIPAddressInput");
  			click(SiebelModeObj.IPGuardian.CircuitIPAddressInput,"click on circuit ip address");

  			verifyExists(SiebelModeObj.IPGuardian.CircuitIPAddressInput," Verify CircuitIPAddressInput");
  			sendKeys(SiebelModeObj.IPGuardian.CircuitIPAddressInput,CircuitIPAddressInput,"Input IP address");

  			verifyExists(SiebelModeObj.IPGuardian.IpRange," Verify IpRange");
  			sendKeys(SiebelModeObj.IPGuardian.IpRange,IPRange,"Input IP Range");
  			verifyExists(SiebelModeObj.IPGuardian.CrossButtonGurdian," Verify CrossButtonGurdian");
  			click(SiebelModeObj.IPGuardian.CrossButtonGurdian,"click on CrossButtonGurdian");
  			Reusable.savePage();
  			//verifyExists(SiebelModeObj.IPGuardian.IpGurdianSave," Verify IpGurdianSave");
  			//click(SiebelModeObj.IPGuardian.IpGurdianSave,"click on IpGurdianSave");
			break;

		}
		case "Number Hosting":
		{
			String B_End_Resilience_Option = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
			String Coverage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Coverage");
			String Alerting_Notification_Email_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
			String IP_Guardian_Variant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
			String Include_Colt_Technical_Contact_in_Test_Calls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
			String Service_Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
			String Test_Window = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
			String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
			String IP_Range = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
			String Site_Contact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Contact");
			String Order_Signed_Date = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
			String Order_Received_Date = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Received_Date");
			String Colt_Actual_Date = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
			String Contract_term = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_term");
			String Aend_Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
			String Aend_CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");

			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountry,"voice service country");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountry,"voice service country");
			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", B_End_Resilience_Option));
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", B_End_Resilience_Option));

			verifyExists(SiebelModeObj.NumberHostingObj.CallAdmissionControl,"8");
			sendKeys(SiebelModeObj.NumberHostingObj.CallAdmissionControl,"8");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.CallAdmissionControl, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks,"4");
			sendKeys(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks,"4");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference,"yes");
			sendKeys(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference,"yes");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.TrafficDirection,"Traffic Direction");
			click(SiebelModeObj.NumberHostingObj.TrafficDirection,"Traffic Direction");
			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Coverage));
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Coverage));

			//R5 Coverage Updating Code From Excel
			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Incoming DDI Digits"));
			click(SiebelModeObj.NumberHostingObj.SelectValueDropdown.replace("Value", Alerting_Notification_Email_Address));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "First Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", IP_Guardian_Variant));

			click(SiebelModeObj.NumberHostingObj.ClickLink.replace("Value", "Show More"));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Second Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Include_Colt_Technical_Contact_in_Test_Calls));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Third Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Service_Bandwidth));
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fourth Codec"));
			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fourth Codec"));
			verifyExists(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Test_Window));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Test_Window));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fifth Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", CircuitIPAddressInput));

			click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Sixth Codec"));
			click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value",IP_Range));

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"Save Button");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"Save Button");
			waitToPageLoad();

			verifyExists(SiebelModeObj.NumberHostingObj.ShowMore,"Show More");
			click(SiebelModeObj.NumberHostingObj.ShowMore,"Show More");
			waitToPageLoad();

			verifyExists(SiebelModeObj.NumberHostingObj.ClickToShowFullInfo,"click to show full info");
			click(SiebelModeObj.NumberHostingObj.ClickToShowFullInfo,"click to show full info");

			verifyExists(SiebelModeObj.NumberHostingObj.SiteContact,"Site Contact");
			click(SiebelModeObj.NumberHostingObj.SiteContact,"Site Contact");
			waitToPageLoad();
			verifyExists(SiebelModeObj.NumberHostingObj.OkButton,"Ok Button");
			click(SiebelModeObj.NumberHostingObj.OkButton,"Ok Button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ServiceParty,"Service Party");
			click(SiebelModeObj.NumberHostingObj.ServiceParty,"Service Party");
			waitToPageLoad();
			verifyExists(SiebelModeObj.NumberHostingObj.OkButtonService,"Ok Button Service");
			click(SiebelModeObj.NumberHostingObj.OkButtonService,"Ok Button Service");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.DelieveryTeamCountry,"DelieveryTeamCountry");
			click(SiebelModeObj.NumberHostingObj.DelieveryTeamCountry,"DelieveryTeamCountry");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Site_Contact));
			waitToPageLoad();

			sendKeys(SiebelModeObj.NumberHostingObj.DeliveryTeamCity,"Shanghai");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.DeliveryTeamCity, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"cross button");
			click(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"cross button");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"Save button");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"Save button");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			verifyExists(SiebelModeObj.NumberHostingObj.TrunkConfiguration,"Trunk Configuration");
			sendKeys(SiebelModeObj.NumberHostingObj.TrunkSequence,"1");
			sendKeys(SiebelModeObj.NumberHostingObj.TrunkName,"Trunk1");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.NumberHostingObj.AccessLineType,"AccessLine Type");
			click(SiebelModeObj.NumberHostingObj.AccessLineType,"AccessLine Type");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value",Order_Signed_Date));
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			//Popup loading takeing time

			verifyExists(SiebelModeObj.NumberHostingObj.AccessServiceID,"AccessServiceID");
			click(SiebelModeObj.NumberHostingObj.AccessServiceID,"AccessServiceID");

			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			//New code//
			verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Verify on Pick Account Dropdown");
			click(SiebelModeObj.PickServicePartyObj.PickAccountDropdown,"Click  on Pick Account Dropdown");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.PickServicePartyObj.AList.replace("Value","Status"));
			click(SiebelModeObj.PickServicePartyObj.AList.replace("Value","Status"));
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Verify on Pick Account Dropdown Value");
			click(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Click  on Pick Account Dropdown Value");
			Reusable.waitForSiebelLoader();
			sendKeys(SiebelModeObj.PickServicePartyObj.PickAccountValue,"Completed","Account Dropdown Value");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.NumberHostingObj.PickAccountGo,"Verify Go");
			click(SiebelModeObj.NumberHostingObj.PickAccountGo,"Click on Go");
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			//////////////////////////////
			
			ScrollIntoViewByString(SiebelModeObj.NumberHostingObj.OkButtonAccess);
			verifyExists(SiebelModeObj.NumberHostingObj.OkButtonAccess,"OkButtonAccess");
			click(SiebelModeObj.NumberHostingObj.OkButtonAccess,"OkButtonAccess");

			//Used in SIP Trunking//
			/*
			verifyExists(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup,"Verify AccessServiceIdlookup");
			click(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Verify SearchServiceIDbox");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Product");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchText,"Verify SearchText");
			click(SiebelModeObj.SIPTrunkingObj.SearchText);
			sendKeys(SiebelModeObj.SIPTrunkingObj.SearchText,"IP Access");

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess,"Verify SearchButtonProcess");
			click(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton,"Verify SearchServiceIdOKButton");
			click(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton);
			Reusable.waitForSiebelLoader();
			 */
			////////////////////////////////////////

			if (isElementPresent(By.xpath("//a[text()='Show full info']")))
			{
				verifyExists(SiebelModeObj.NumberHostingObj.ShowFullInfoTrunk,"ShowFullInfoTrunk");
				click(SiebelModeObj.NumberHostingObj.ShowFullInfoTrunk,"ShowFullInfoTrunk");

			}
			verifyExists(SiebelModeObj.NumberHostingObj.AddButtonTrunk,"AddButtonTrunk");
			click(SiebelModeObj.NumberHostingObj.AddButtonTrunk,"AddButtonTrunk");

			verifyExists(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP4Address,"CustomerOriginatingIP4Address");
			sendKeys(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP4Address,"192.168.1.1");
			sendKeys(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP6Address,"192.18.1.2");

			sendKeys(SiebelModeObj.NumberHostingObj.CustomerTerminatingIP4Address,"38.68.1.1");
			sendKeys(SiebelModeObj.NumberHostingObj.CustomerTerminatingIP6Address,"38.168.1.1");

			verifyExists(SiebelModeObj.NumberHostingObj.SignallingTransportProtocol,"SignallingTransportProtocol");
			click(SiebelModeObj.NumberHostingObj.SignallingTransportProtocol,"SignallingTransportProtocol");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Order_Received_Date));

			verifyExists(SiebelModeObj.NumberHostingObj.VoipProtocol,"VoipProtocol");
			click(SiebelModeObj.NumberHostingObj.VoipProtocol,"VoipProtocol");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Colt_Actual_Date));

			verifyExists(SiebelModeObj.NumberHostingObj.EncryptionProtocol,"EncryptionProtocol");
			click(SiebelModeObj.NumberHostingObj.EncryptionProtocol,"EncryptionProtocol");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Contract_term));

			sendKeys(SiebelModeObj.NumberHostingObj.NATRequired,"N");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.NATRequired, Keys.TAB);

			verifyExists(SiebelModeObj.NumberHostingObj.InboundOutboundSplit,"InboundOutboundSplit");
			sendKeys(SiebelModeObj.NumberHostingObj.InboundOutboundSplit,"No");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.InboundOutboundSplit, Keys.TAB);

			verifyExists(SiebelModeObj.NumberHostingObj.TypeOfCAC,"TypeOfCAC");
			click(SiebelModeObj.NumberHostingObj.TypeOfCAC,"TypeOfCAC");
			verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_Country));
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_Country));

			verifyExists(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"CrossButtonTrunk");
			click(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"CrossButtonTrunk");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
			Reusable.waitForAjax();
			click(SiebelModeObj.NumberHostingObj.OtherTabClick,"OtherTabClick");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelModeObj.NumberHostingObj.TextInput.replace("Value", "Internal Routing Prefix"),"45");
			sendKeys(SiebelModeObj.NumberHostingObj.TextInput.replace("Value", "Reseller Profile"),"colt");

			verifyExists(SiebelModeObj.NumberHostingObj.VoiceTab,"VoiceTab");
			click(SiebelModeObj.NumberHostingObj.VoiceTab,"VoiceTab");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			sendKeys(SiebelModeObj.NumberHostingObj.Language,"English");
			Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.Language, Keys.TAB);

			verifyExists(SiebelModeObj.NumberHostingObj.Resillence,"Resillence");
			click(SiebelModeObj.NumberHostingObj.Resillence,"Resillence");
			click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_CityTown));

			verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			click(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			/*try
			{
				verifyExists(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");
				click(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");

			}
			catch(Exception ex)
			{
				verifyExists(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"InstalltionDropdown");
				Reusable.Select1(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"Service Group");

			}*/
			if (isVisible(SiebelModeObj.NumberHostingObj.ServiceGroupTab))
			{
				verifyExists(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");
				click(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");

			}
			else
			{
				verifyExists(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"InstalltionDropdown");
				Reusable.Select1(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"Service Group");

			}

			waitToPageLoad();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ClickOnPlusButtonService,"ClickOnPlusButtonService");
			click(SiebelModeObj.NumberHostingObj.ClickOnPlusButtonService,"ClickOnPlusButtonService");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ServiceGroupReference,"ServiceGroupReference");
			click(SiebelModeObj.NumberHostingObj.ServiceGroupReference,"ServiceGroupReference");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.NumberHostingObj.ClickOkButton,"ClickOkButton");
			click(SiebelModeObj.NumberHostingObj.ClickOkButton,"ClickOkButton");
			Reusable.waitForSiebelLoader();
			break;

		}



		default:{
			System.out.println("Above product is not exist in current list");
			break;
		}
		}

	}

	public void SaveAndCloseMask() throws Exception
	{
		if (isElementPresent(By.xpath("//div[@id='colt-s-order-connection-header']//a[text()='Save']"))) 
		{
			//	System.out.println("enter if loop of save button");
			verifyExists(SiebelModeObj.SaveAndCloseMask.SaveEthernetAccess,"Verify SaveEthernetAccess");
			click(SiebelModeObj.SaveAndCloseMask.SaveEthernetAccess,"Click  on SaveEthernetAccess");
			Reusable.waitForSiebelLoader();
		}

		if (isElementPresent(By.xpath("(//a[contains(text(),'Click here to')])[1]"))) 
		{
			//	System.out.println("enter if loop of configuration");
			ClickHereSave();
		}
	}


	public void BEndSitePUD(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 

	{

		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Offnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
		String ColtActualDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String OrderSignedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		String ThirdpartyAccessProviderValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_party_access_provider");
		String Contractterm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ContractTerm");


		waitToPageLoad();
		Reusable.waitForSiebelLoader();	
		if (ProductName.contains("Ultra Low Latency")
				|| (ProductName.contains("Private Wave Node")|| (ProductName.contains("DArk Fibre"))))

		{						
			if(Offnet.contains("Offnet"))
			{

				System.out.println("Enter into offnet part of B end site pud");

				verifyExists(SiebelModeObj.BEndSitePUD.AccesstypedropdownB,"AccesstypedropdownB");
				click(SiebelModeObj.BEndSitePUD.AccesstypedropdownB,"Click on AccesstypedropdownB");

				verifyExists(SiebelModeObj.BEndSitePUD.AccesstypeOffnet.replace("AccessTypeValue","Colt Fibre"),"AccesstypeOffnet");
				click(SiebelModeObj.BEndSitePUD.AccesstypeOffnet.replace("AccessTypeValue","Colt Fibre"));

				verifyExists(SiebelModeObj.BEndSitePUD.IpGurdianSave,"IpGurdianSave");
				click(SiebelModeObj.BEndSitePUD.IpGurdianSave,"IpGurdianSave");
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForSiebelLoader();
				//verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value",ColtActualDate),"BList");
				//click(SiebelModeObj.BEndSitePUD.BList.replace("Value",ColtActualDate));
				if(!ProductName.contains("Dark Fibre")){
				
				verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Access Technology"),"BEnd Site DropDown");
				click(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Access Technology"));

				verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value",ColtActualDate),"BList");
				click(SiebelModeObj.BEndSitePUD.BList.replace("Value",ColtActualDate));
				}
				//for 3rd party connection provider//
				/*
				verifyExists(SiebelModeObj.BEndSitePUD.ThirdpartyaccessproviderDropDownB,"Verify ThirdpartyaccessproviderDropDownB");
				click(SiebelModeObj.BEndSitePUD.ThirdpartyaccessproviderDropDownB,"ThirdpartyaccessproviderDropDownB");
				verifyExists(SiebelModeObj.BEndSitePUD.ThirdpartyaccessProvidervalue.replace("Value",ThirdpartyAccessProviderValue),"Verify ThirdpartyaccessProvidervalue");
				click(SiebelModeObj.BEndSitePUD.ThirdpartyaccessProvidervalue.replace("Value",ThirdpartyAccessProviderValue));

				//for 3rd party connection reference//
				verifyExists(SiebelModeObj.BEndSitePUD.ThirdpartyconectionreferenceB,"Verify ThirdpartyconectionreferenceB");
				click(SiebelModeObj.BEndSitePUD.ThirdpartyconectionreferenceB,"ThirdpartyconectionreferenceB");
				sendKeys(SiebelModeObj.BEndSitePUD.ThirdpartyconectionreferenceB,"12","ThirdpartyconectionreferenceB");

				//for third party SLA tier//

				verifyExists(SiebelModeObj.BEndSitePUD.ThirdpartyDropDownB,"Verify ThirdpartyDropDownB");
				click(SiebelModeObj.BEndSitePUD.ThirdpartyDropDownB,"ThirdpartyDropDownB");

				verifyExists(SiebelModeObj.BEndSitePUD.ThirdpartySLATiervalue.replace("SLAValue",IPRange),"Verify ThirdpartySLATiervalue");
				click(SiebelModeObj.BEndSitePUD.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
				*/
				//verifyExists(SiebelModeObj.BEndSitePUD.IpGurdianSave,"IpGurdianSave");
				//click(SiebelModeObj.BEndSitePUD.IpGurdianSave,"IpGurdianSave");
				Reusable.savePage();
				Reusable.waitForSiebelLoader();

			}//out of if inner loop

			else
			{

				verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Access Type"),"BEnd Site DropDown");
				click(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Access Type"));

				verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value",ColtActualDate),"BList");
				click(SiebelModeObj.BEndSitePUD.BList.replace("Value",ColtActualDate));
				
				if (!ProductName.contains("Dark Fibre")){
					verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Access Technology"),"BEnd Site DropDown");
					click(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Access Technology"));

					verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value",AccessType),"BList");
					click(SiebelModeObj.BEndSitePUD.BList.replace("Value",AccessType));
					
	               }

			/*	verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Demarcation Device Required"),"BEnd Site DropDown");
				click(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Demarcation Device Required"));
*/
				
				//verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value",OrderSignedDate),"BList");
				//click(SiebelModeObj.BEndSitePUD.BList.replace("Value",OrderSignedDate));

			}}

		verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Building Type"),"BEnd Site DropDown");
		click(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Building Type"));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value","NA"),"BList");
		click(SiebelModeObj.BEndSitePUD.BList.replace("Value","NA"));

		verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","BCP Reference"),"BEndSiteInput");
		click(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","BCP Reference"));
		sendKeys(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value", "BCP Reference"),"NA");
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		Reusable.waitForSiebelLoader();			
		verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Customer Site Pop Status"),"BEnd Site DropDown");
		click(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","Customer Site Pop Status"));
		Reusable.waitForSiebelLoader();	
		verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value","NA"),"BList");
		click(SiebelModeObj.BEndSitePUD.BList.replace("Value","NA"));

		/*verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","DSL SLA Class"),"BEnd Site DropDown");
		click(SiebelModeObj.BEndSitePUD.BEndSiteDropDown.replace("Value","DSL SLA Class"));
		verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value",ServiceBandwidth),"BList");
		click(SiebelModeObj.BEndSitePUD.BList.replace("Value",ServiceBandwidth));
		 */
		verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","Site Name Alias"),"BEndSiteInput");
		click(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","Site Name Alias"));
		sendKeys(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value", "Site Name Alias"),"NA");

		if ( ProductName.contains("Private Wave Node") || ProductName.contains("Private Wave Service")) 
		{
			verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","Node Site Name"));
			click(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","Node Site Name"));
			sendKeys(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value", "Node Site Name"),"NA");
		}
		if(ProductName.contains("Private Wave Service") ) 
		{
			verifyExists(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","Node Service ID"));
			click(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value","Node Service ID"));
			sendKeys(SiebelModeObj.BEndSitePUD.BEndSiteInput.replace("Value", "Node Service ID"),"BIGT14124");

		}
	}


	public void SearchSiteEntery() throws InterruptedException,  IOException 
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SearchSiteEntery.SearchInputEthernetAccess.replace("Value","Street Name")," Verify SearchInputEthernetAccess");
		sendKeys(SiebelModeObj.SearchSiteEntery.SearchInputEthernetAccess.replace("Value","Street Name"),"Buckingham Avenue Slough");

		verifyExists(SiebelModeObj.SearchSiteEntery.SearchDropdown.replace("Value","Country"),"Country Dropdown");
		click(SiebelModeObj.SearchSiteEntery.SearchDropdown.replace("Value","Country"),"Country Dropdown");
		verifyExists(SiebelModeObj.SearchSiteEntery.SiteABSelection.replace("Value","United Kingdom"),"United Kingdom");
		click(SiebelModeObj.SearchSiteEntery.SiteABSelection.replace("Value","United Kingdom"),"United Kingdom");
		verifyExists(SiebelModeObj.SearchSiteEntery.SearchInput.replace("Value","City / Town"),"City / Town Input");
		sendKeys(SiebelModeObj.SearchSiteEntery.SearchInput.replace("Value","City / Town"),"Slough");

		verifyExists(SiebelModeObj.SearchSiteEntery.SearchInput.replace("Value","Postal Code"),"Postal Code Input");
		sendKeys(SiebelModeObj.SearchSiteEntery.SearchInput.replace("Value","Postal Code"),"SL14AX");
		verifyExists(SiebelModeObj.SearchSiteEntery.SearchInput.replace("Value","Premises"),"Premises Input");
		sendKeys(SiebelModeObj.SearchSiteEntery.SearchInput.replace("Value","Premises"),"8");

		verifyExists(SiebelModeObj.SearchSiteEntery.Searchbutton,"Verify SearchButton");
		click(SiebelModeObj.SearchSiteEntery.Searchbutton,"SearchButton");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.SearchSiteEntery.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.SearchSiteEntery.SearchAddressRowSelection,"SearchAddressRowSelection");
		verifyExists(SiebelModeObj.SearchSiteEntery.PickAddress,"Verify PickAddress");
		click(SiebelModeObj.SearchSiteEntery.PickAddress,"PickAddress");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SearchSiteEntery.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.SearchSiteEntery.SearchAddressRowSelection,"SearchAddressRowSelection");
		verifyExists(SiebelModeObj.SearchSiteEntery.PickBuilding,"Verify PickBuilding");
		click(SiebelModeObj.SearchSiteEntery.PickBuilding,"PickBuilding");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SearchSiteEntery.SearchAddressRowSelection,"Verify SearchAddressRowSelection");
		click(SiebelModeObj.SearchSiteEntery.SearchAddressRowSelection,"SearchAddressRowSelection");

		verifyExists(SiebelModeObj.SearchSiteEntery.PickSite,"Verify PickSite");
		click(SiebelModeObj.SearchSiteEntery.PickSite,"PickSite");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		addSiteATechnicalContact();
	}

	public void privateWaveServiceEntry(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String BEndResellience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resellience");
		String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");
		String Servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_type");

		Reusable.waitForSiebelLoader();
		//Reusable.savePage();
		System.out.println("product is:" + ProductName);
		if (ProductName.contains("Private Wave Service")) {
			System.out.println("Inside the case private wave service");

			verifyExists(SiebelModeObj.privateWaveServiceEntry.PopDropdownClick.replace("Value","Service Bandwidth"));
			click(SiebelModeObj.privateWaveServiceEntry.PopDropdownClick.replace("Value","Service Bandwidth"));
			verifyExists(SiebelModeObj.privateWaveServiceEntry.InsideDropdownValues.replace("Data",ServiceBandwidth));
			click(SiebelModeObj.privateWaveServiceEntry.InsideDropdownValues.replace("Data",ServiceBandwidth));
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.privateWaveServiceEntry.PopDropdownClick.replace("Value","A End Resilience Option"));
			click(SiebelModeObj.privateWaveServiceEntry.PopDropdownClick.replace("Value","A End Resilience Option"));
			verifyExists(SiebelModeObj.privateWaveServiceEntry.InsideDropdownValues.replace("Data","Protected"),"Verify PopDropdownClick");
			click(SiebelModeObj.privateWaveServiceEntry.InsideDropdownValues.replace("Data","Protected"),"Click on PopDropdownClick");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.privateWaveServiceEntry.BendResillenceOption,"Verify B end resillience option");
			click(SiebelModeObj.privateWaveServiceEntry.BendResillenceOption,"Click on B end resillience option");
			verifyExists(SiebelModeObj.BEndSitePUD.BList.replace("Value",BEndResellience),"Verify ValuesInsideDropdownWithIndex");
			click(SiebelModeObj.BEndSitePUD.BList.replace("Value",BEndResellience),"Click on ValuesInsideDropdownWithIndex");
			//verifyExists(SiebelModeObj.privateWaveServiceEntry.ValuesInsideDropdownWithIndex.replace("Data",BEndResellience).replace("Value", "2"),"Verify ValuesInsideDropdownWithIndex");
			//click(SiebelModeObj.privateWaveServiceEntry.ValuesInsideDropdownWithIndex.replace("Data",BEndResellience).replace("Value", "2"),"Click on ValuesInsideDropdownWithIndex");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.privateWaveServiceEntry.NetworkTopologyPrivateWave,"Verify NetworkTopologyPrivateWave");
			click(SiebelModeObj.privateWaveServiceEntry.NetworkTopologyPrivateWave,"Click on  network topology");
			verifyExists(SiebelModeObj.privateWaveServiceEntry.ValueInsideDropdown.replace("Data",Topology));
			click(SiebelModeObj.privateWaveServiceEntry.ValueInsideDropdown.replace("Data",Topology));
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.privateWaveServiceEntry.ServiceTypePrivate,"Verify ServiceTypePrivate");
			click(SiebelModeObj.privateWaveServiceEntry.ServiceTypePrivate,"Click on service type");
			verifyExists(SiebelModeObj.privateWaveServiceEntry.ValueInsideDropdown.replace("Data",Servicetype));
			click(SiebelModeObj.privateWaveServiceEntry.ValueInsideDropdown.replace("Data",Servicetype));
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
			//Reusable.savePage();
		}

	}


	public void middleApplet() throws InterruptedException,  IOException
	{
		Reusable.waitForSiebelLoader();
		String TempStartDate= Reusable.CurrentDate();

		verifyExists(SiebelModeObj.middleApplet.SupportStarDate,"SupportStarDate");
		click(SiebelModeObj.middleApplet.SupportStarDate,"SupportStarDate");
		sendKeys(SiebelModeObj.middleApplet.SupportStarDate,TempStartDate,"SupportStarDate");
		Reusable.waitForSiebelSpinnerToDisappear();

		String TempEndDate = Reusable.FutureDate(60);
		verifyExists(SiebelModeObj.middleApplet.SupprtEndDate,"SupprtEndDate");
		click(SiebelModeObj.middleApplet.SupprtEndDate,"SupprtEndDate");
		sendKeys(SiebelModeObj.middleApplet.SupprtEndDate,TempEndDate,"SupportStarDate");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.middleApplet.SaveButton,"SaveButton");
		click(SiebelModeObj.middleApplet.SaveButton,"Click on Save Button");
		Reusable.waitForSiebelLoader();

	}

	public void SiteASettingClick() throws InterruptedException, DocumentFormatException, IOException {

		verifyExists(SiebelModeObj.SiteASettingClick.SiteASetting,"Site A Setting");
		click(SiebelModeObj.SiteASettingClick.SiteASetting,"Site A Setting Click");
		Reusable.waitForSiebelLoader();
	}

	public void SiteBSettingClick() throws InterruptedException, DocumentFormatException, IOException {
		verifyExists(SiebelModeObj.SiteBSettingClick.SiteBSetting,"Site B Setting");
		javaScriptclick(SiebelModeObj.SiteBSettingClick.SiteBSetting,"Site B Setting Click");
		Reusable.waitToPageLoad();
		Reusable.waitForSiebelLoader();
	}

	public void IPCPESolutionSite(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

		String A_End_Resillence = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Resillence");
		String B_End_Resellience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resellience");
		String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");
		String Service_bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String Service_type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_type");

		//System.out.println("enter into if loop of cpe solution");
		//javascriptexecutor(getwebelement(xml.getlocator("//locators/CPESolutionService/ScrollDownAend")));

		verifyExists(SiebelModeObj.IPCPESolutionSite.AendResillenceOption,"Aend Resillence Option");
		click(SiebelModeObj.IPCPESolutionSite.AendResillenceOption,"Aend Resillence Option");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", A_End_Resillence),"Select value from A end resillence option");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", A_End_Resillence),"Select value from A end resillence option");

		verifyExists(SiebelModeObj.IPCPESolutionSite.BendResillenceOption,"B end resillience option");
		click(SiebelModeObj.IPCPESolutionSite.BendResillenceOption,"Click on B end resillience option");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", B_End_Resellience),"Select value from B end resillence option");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value", B_End_Resellience),"Select value from B end resillence option");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.IPCPESolutionSite.NetworkTopology,"Network Topology");
		click(SiebelModeObj.IPCPESolutionSite.NetworkTopology,"Network Topology");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Topology),"Value Inside Dropdown");
		//click(SiebelModeObj.IPCPESolutionSite.ValueInsideDropdown.replace("Data",Topology.toString()));
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Topology),"Value Inside Dropdown");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPCPESolutionSite.ServiceBandwidthCpe,"Service Bandwidth Cpe");
		click(SiebelModeObj.IPCPESolutionSite.ServiceBandwidthCpe,"Click on service bandwidth");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Service_bandwidth),"Value from service bandwidth");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Service_bandwidth),"Value from service bandwidth");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPCPESolutionSite.ServiceTypecpe,"service type");
		click(SiebelModeObj.IPCPESolutionSite.ServiceTypecpe,"Click on service type");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",
				Service_type.toString()),"Value from Service type");
		click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",
				Service_type.toString()));
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPCPESolutionSite.SaveButtonClick,"Save Button");
		click(SiebelModeObj.IPCPESolutionSite.SaveButtonClick,"Save Button");
		Reusable.waitForSiebelLoader();

		String serviceNo = getTextFrom(SiebelModeObj.IPCPESolutionSite.SaveButtonClick);
		System.out.println("Service order no is:" + serviceNo);
		// ServiceOrder.set(Gettext(getwebelement(xml.getlocator("//locators/ServiceOrderNumber"))));
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Generated Service Order No: " + ServiceOrder.get());
	}





	public void addNetwork() throws Exception 

	{
		verifyExists(SiebelModeObj.Network.NetworkTab,"Network Tab");
		click(SiebelModeObj.Network.NetworkTab,"Network Tab");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SubNetworkSubTab,"Sub Network Tab");
		click(SiebelModeObj.Network.SubNetworkSubTab,"Sub Network Tab");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SubNetworkNewPlusIcon,"Sub Network New Plus Icon");
		click(SiebelModeObj.Network.SubNetworkNewPlusIcon,"Sub Network New Plus Icon");
		verifyExists(SiebelModeObj.Network.TechnologyInput,"TechnologyInput");
		sendKeys(SiebelModeObj.Network.TechnologyInput,"IQ Net");
		//Reusable.SendkeaboardKeys(SiebelModeObj.LeadCapacity.ExistingCapacityLeadTimePrimary, Keys.TAB);
		//verifyExists(SiebelModeObj.Network.SwitchingNodeSiteFld,"Switching Node Site Fld");
		click(SiebelModeObj.Network.SwitchingNodeSiteFld,"Switching Node Site Fld");
		verifyExists(SiebelModeObj.Network.SwitchingNodeSiteSearchIcon,"Switching Node Site Search icon");
		click(SiebelModeObj.Network.SwitchingNodeSiteSearchIcon,"Switching Node Site Search icon");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SwitchingSiteGoArrow,"Switching Site Go Arrow");
		click(SiebelModeObj.Network.SwitchingSiteGoArrow,"Switching Site Go Arrow");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SwitchingSiteOkBtn,"Switching Site Ok Btn");
		click(SiebelModeObj.Network.SwitchingSiteOkBtn,"Switching Site Ok Btn");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SwitchingNodeSubTab,"Switching Node Sub Tab");
		click(SiebelModeObj.Network.SwitchingNodeSubTab,"Switching Node Sub Tab");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SwitchingNodeNewPlusIcon,"Switching Node New Plus Icon");
		click(SiebelModeObj.Network.SwitchingNodeNewPlusIcon,"Switching Node New Plus Icon");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SubNetworkFld,"Sub Network Fld");
		click(SiebelModeObj.Network.SubNetworkFld,"Sub Network Fld");
		verifyExists(SiebelModeObj.Network.SubNetworkSearchIcon,"SubNetworkSearchIcon");
		click(SiebelModeObj.Network.SubNetworkSearchIcon,"SubNetworkSearchIcon");
		Reusable.waitForSiebelLoader();
		click(SiebelModeObj.Network.SubNetworkIDOkBtn,"SubNetworkIDOkBtn");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SwitchingNodeSiteFld1,"Switching Node Site Fld");
		click(SiebelModeObj.Network.SwitchingNodeSiteFld1,"Switching Node Site Fld");
		verifyExists(SiebelModeObj.Network.SwitchingNodeSiteSearchIcon,"Switching Node Site Search icon");
		click(SiebelModeObj.Network.SwitchingNodeSiteSearchIcon,"Switching Node Site Search icon");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SwitchingSiteGoArrow,"Switching Site Go Arrow");
		click(SiebelModeObj.Network.SwitchingSiteGoArrow,"Switching Site Go Arrow");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Network.SwitchingSiteOkBtn,"Switching Site Ok Btn");
		click(SiebelModeObj.Network.SwitchingSiteOkBtn,"Switching Site Ok Btn");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
	}


	public void addSiteATechnicalContact() throws InterruptedException,  IOException 
	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
		click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
		click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
		click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
		click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();

	}


	public void NavigateOnBillingTab() throws Exception 

	{
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.EnterBillingDateInFooter.Billing,"Billing Tab");
		click(SiebelModeObj.EnterBillingDateInFooter.Billing,"Click on Billing Tab");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
	}



	public void validateXtrac() throws InterruptedException,  IOException
	{
		
		verifyExists(SiebelModeObj.Workflow.WorkflowTab,"Workflow Tab");
		click(SiebelModeObj.Workflow.WorkflowTab,"Workflow Tab");
		Reusable.waitForSiebelLoader();

		//	List<WebElement> wb = GetWebElements("//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td/following-sibling::td[contains(@id,'EXTERNAL_URL')]");
		//	List<WebElement> statusWf = GetWebElements("//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td[contains(@id,'Workflow_Status')]");

		Reusable.waitForSiebelSpinnerToDisappear();
		List<WebElement> wb = webDriver.findElements(By.xpath("//td[contains(@id,'EXTERNAL_URL')]"));
		//	System.out.println("List of elements is"+wb);
		//	List<WebElement> status = webDriver.findElements(By.xpath(SiebelModeObj.Workflow.status));
		List<WebElement> status = webDriver.findElements(By.xpath("//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td[contains(@id,'Workflow_Status')]"));
		System.out.println("List of Status is"+status);

		int rowCount = getXPathCount(SiebelModeObj.Workflow.rowCount);
		System.out.println("count of element is"+rowCount);

		for(int i=0;i<rowCount;i++)
		{
			if(wb.get(i).isDisplayed())
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Workflow from xtrac is displayed in row no. : "+i);
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Step: Workflow from xtrac is not displayed in row no. : "+i);
			}
			if(GetTextFrom(status.get(i)).equalsIgnoreCase("In Progress"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Workflow status is displayed as In Progress");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Step: Workflow status is not displayed as In Progress");
			}
		}
	}

	public void validateXtracComplete() throws InterruptedException,  IOException 
	{
		Reusable.Pagerefresh();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Workflow.WorkflowTab,"Workflow Tab");
		click(SiebelModeObj.Workflow.WorkflowTab,"Workflow Tab");
		
		if(isElementPresent(By.xpath("//span[text()='Ok']")))
		{
			verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
			click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.Workflow.WorkflowTab,"Workflow Tab");
			click(SiebelModeObj.Workflow.WorkflowTab,"Workflow Tab");

		}
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		//	List<WebElement> wb = GetWebElements("//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td/following-sibling::td[contains(@id,'EXTERNAL_URL')]");
		//	List<WebElement> statusWf = GetWebElements("//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td[contains(@id,'Workflow_Status')]");

		
		List<WebElement> wb = webDriver.findElements(By.xpath("//td[contains(@id,'EXTERNAL_URL')]"));
		//	System.out.println("List of elements is"+wb);

		//	List<WebElement> status = webDriver.findElements(By.xpath(SiebelModeObj.Workflow.status));
		List<WebElement> status = webDriver.findElements(By.xpath("//table[@summary='Workflow']/tbody/tr/following-sibling::tr/td/following-sibling::td[contains(@id,'Workflow_Status')]"));
		//	System.out.println("List of Status is"+status);

		int rowCount = getXPathCount(SiebelModeObj.Workflow.rowCount);
		System.out.println("count of element is"+rowCount);

		for (int i = 0; i < rowCount; i++) 
		{
			if (wb.get(i).isDisplayed()) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Workflow from xtrac is displayed in row no. : " + i);
				Report.LogInfo("Verify","<b><i>"+"Step: Workflow from xtrac is displayed in row no. : "  + i, "PASS");	
			} else 
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Workflow from xtrac is not displayed in row no. : " + i);
				Report.LogInfo("Verify","<b><i>"+"Step: Workflow from xtrac is not displayed in row no. : "  + i, "FAIL");	
			}

			if (GetTextFrom(status.get(i)).equalsIgnoreCase("Completed")) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Workflow status is displayed as Completed");
				Report.LogInfo("Verify","<b><i>"+"Step: Workflow status is displayed as Completed ", "PASS");
			} 
			else 
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Step: Workflow status is not displayed as Completed");
				Report.LogInfo("Verify","<b><i>"+"Step: Workflow status is displayed as Completed ", "FAIL");
			}
		}
		Reusable.waitForSiebelSpinnerToDisappear();
	}



	public void addProductConfigurationDetailsAndCompleteInFlightOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		
		SiebelAddProduct Product = new SiebelAddProduct();
		SebielInFlightOrder Inflight = new SebielInFlightOrder();
		SiebelHubSite Hub = new SiebelHubSite();
		SiebelSpokeSite Spoke = new SiebelSpokeSite();
		SiebelNumberManagement NumMana = new SiebelNumberManagement();
		SiebelManualValidation Validation = new SiebelManualValidation();

		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) {

			Hub.hubSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Inflight.ServiceTabInFlight(testDataFile, sheetName, scriptNo, dataSetNo);
		} else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) {
			Spoke.spokeSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Inflight.ServiceTabInFlight(testDataFile, sheetName, scriptNo, dataSetNo);
		} else {

			enterMandatoryDetailsInMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceConfigTab(testDataFile, sheetName, scriptNo, dataSetNo);
			VoiceFeatureTab(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.AddDataUnderNumberManagement(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.OperationAttribute(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			NumMana.MandatoryFields(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Validation.clickOnManualValidationB();
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			
			// newOrderOnnnet.get().clickOnManualValidationA();
			Inflight.ServiceTabInFlight(testDataFile, sheetName, scriptNo, dataSetNo);
		}

	}

	public void addProductConfigurationDetailsAndCompleteOrderStatusXNG(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String username, String password ) throws Exception 
	{
		SiebelAddProduct Product = new SiebelAddProduct();
		SiebelHubSite  Hub = new SiebelHubSite();
		SiebelSpokeSite Spoke = new SiebelSpokeSite();
		SiebelNumberManagement NumMana = new SiebelNumberManagement();
		SiebelManualValidation Validation = new SiebelManualValidation();
		SiebelNewOrderOnnetHelper newOrdrOnnnet = new SiebelNewOrderOnnetHelper();



		String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		if (ProdType.toString().equalsIgnoreCase("Ethernet Hub")) {
			Hub.hubSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.GetReferenceXNG(testDataFile, sheetName, scriptNo, dataSetNo);
			//Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo); 
			Login.SiebelLogout();
			newOrdrOnnnet.LaunchingXNGApplication(testDataFile, sheetName, scriptNo, dataSetNo,username,password);//for xng code			
		} else if (ProdType.toString().equalsIgnoreCase("Ethernet Spoke")) {
			Spoke.spokeSiteCustomize(testDataFile, sheetName, scriptNo, dataSetNo);
			EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo); 
			EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.GetReferenceXNG(testDataFile, sheetName, scriptNo, dataSetNo);
			Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
			//Product.CircuitReferenceGeneration(testDataFile, sheetName, scriptNo, dataSetNo); 
			Product.OrderCompleteEthernetHubSpoke(testDataFile, sheetName, scriptNo, dataSetNo);
			Login.SiebelLogout();
			newOrdrOnnnet.LaunchingXNGApplication(testDataFile, sheetName, scriptNo, dataSetNo,username,password);//for xng code
		} else {
	
		enterMandatoryDetailsInXNGMiddleApplet(testDataFile, sheetName, scriptNo, dataSetNo);
		VoiceConfigTab(testDataFile, sheetName, scriptNo, dataSetNo);
		VoiceFeatureTab(testDataFile, sheetName, scriptNo, dataSetNo);
		NumMana.AddDataUnderNumberManagement(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		EnterServiceChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.SelectAttachmentTab(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.UploadDocument(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.SelectServiceGroupTab(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.OperationAttribute(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.EnterInstallationChargeInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		NumMana.MandatoryFields(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.CommercialValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.GetReferenceXNG(testDataFile, sheetName, scriptNo, dataSetNo);
		Product.TechnicalValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Validation.clickOnManualValidationB();
		Product.DeliveryValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		Validation.clickOnManualValidationA();
		Product.CompletedValidation(testDataFile, sheetName, scriptNo, dataSetNo);
		
		Login.SiebelLogout();
		newOrdrOnnnet.LaunchingXNGApplication(testDataFile, sheetName, scriptNo, dataSetNo,username,password);//for xng code


		}
	}	


	public void addSiteAXNGDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SiteAID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Site_ID");

		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA, "Search Address SiteA");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressSiteA,"Click on Search Address SiteA");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.addSiteAXNGDetails.SiteID,"Enter Site ID");
		Reusable.waitForSiebelLoader();
		sendKeys(SiebelModeObj.addSiteAXNGDetails.SiteID,SiteAID,"Site ID Value");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		click(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
		click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

	}

	public void addSiteBXNGDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
		String SiteBID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bend_Site_ID");


		verifyExists(SiebelModeObj.addSiteAXNGDetails.SearchAddressSiteB, "Search Address SiteA");
		click(SiebelModeObj.addSiteAXNGDetails.SearchAddressSiteB,"Click on Search Address SiteA");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteAXNGDetails.SiteID,"Enter Site ID");
		sendKeys(SiebelModeObj.addSiteAXNGDetails.SiteID,SiteBID,"Site ID Value");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		click(SiebelModeObj.addSiteAXNGDetails.SearchButton,"Search");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection,"SearchAddressRowSelection");
		click(SiebelModeObj.addSiteADetailsObj.SearchAddressRowSelection," Click on Search Address RowSelection");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.PickSite,"Verify Site");
		click(SiebelModeObj.addSiteADetailsObj.PickSite,"Click on Site");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();
	}
	
	//==================XNG===================
	public void enterMandatoryDetailsInXNGMiddleApplet(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{
	
	String ProdType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

	switch (ProdType.toString()){
	case "Ethernet Line": 
	{
		WaveLineMidleApplet(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteADetails(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteBDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		WaveLineSiteEntries(testDataFile,sheetName,scriptNo,dataSetNo);

		break;
	}
	case "Wave": 
	{
		WaveLineMidleApplet(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteADetails(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteBDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		WaveLineSiteEntries(testDataFile,sheetName,scriptNo,dataSetNo);
		break;
	}
	case "Private Ethernet": 
	{
		PrivateEthernetMiddleAplet(testDataFile,sheetName,scriptNo,dataSetNo);
		ShowfullInfo();

		DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		Save();
		SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		ClickHereSave();
		//SearchSiteA();
		//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		//SearchSiteB();
		//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		ClickHereSave();
		Reusable.waitForSiebelLoader();
		System.out.println("Sites Search and Entered");
		AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
		BEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
		System.out.println("Site A & B Filled");
		ClickHereSave();
		Reusable.waitForSiebelLoader();
		SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		ClickHereSave();
		SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBTerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		ClickHereSave();
		SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
		ClickHereSave();
		//			GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
		//			ClickHereSave();
		break;
	}
	case "Private Wave Node": {
		middleAppletPrivateWaveNode(testDataFile,sheetName,scriptNo,dataSetNo);
		alertPopUp();
		SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		alertPopUp();
		//ClickHereSave();
		Reusable.savePage();
		SearchSiteA();
		SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		//ClickHereSave();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		break;
	}
	case "Ethernet Access": {

		ShowfullInfo();
		PrivateEthernetEntry(testDataFile,sheetName,scriptNo,dataSetNo);	
		SiteADiversityCircuitConfig(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.closePopUp();
		SaveAndCloseMask();
		EthernetAccessNewFields(testDataFile,sheetName,scriptNo,dataSetNo);
		//		SaveAndCloseMask();
		Reusable.savePage();
		SearchSiteA();
		SearchSiteEntery();
		SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SaveAndCloseMask();
		AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
		SaveAndCloseMask();
		SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.savePage();
		break;

	}
	case "Private Wave Service": {

		ShowfullInfo();
		privateWaveServiceEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteADiversityCircuitConfig(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.closePopUp();
		SaveAndCloseMask();
		EthernetAccessNewFields(testDataFile,sheetName,scriptNo,dataSetNo);
		//SaveAndCloseMask();
		Reusable.savePage();
		//SearchSiteA();
		//SearchSiteEntery();
		addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.savePage();
		AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SaveAndCloseMask();
		SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SaveAndCloseMask();
		//Start site B
		//SearchSiteB();
		//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.savePage();
		BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
		//SaveAndCloseMask();
		//GetReference(testDataFile,sheetName,scriptNo,dataSetNo);	
		break;


	}
	
	
	case "Ultra Low Latency": {
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		alertPopUp();
		OperationalAttributeUltra(testDataFile,sheetName,scriptNo,dataSetNo);
		alertPopUp();
		ShowfullInfo();
		DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		Save();
		middleUltraLowLatency(testDataFile,sheetName,scriptNo,dataSetNo);
		alertPopUp();
		SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		//Save();
		alertPopUp();
		ClickHereSave();
		addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		//SearchSiteA(); 
		//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		//SearchSiteB();
		//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
		ClickHereSave();
		//GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
		//ClickHereSave();
		Reusable.waitForSiebelLoader();
		break;
	}

	case "Cloud Unified Communications": {
		middleApplet();
		R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
		break;
	}
	case "Professional Services":  {
		middleApplet();
		R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
		break;
	}
	case "IP Voice Solutions": {
		middleApplet();
		R5DataCoverage(testDataFile,sheetName,scriptNo,dataSetNo);
		break;
	}
	case "Managed Dedicated Firewall": {
		alertPopUp();
		middleAppletManagedDedicatedFirewall(testDataFile,sheetName,scriptNo,dataSetNo);
		break;
	}

	case "Dark Fibre": {
		Reusable.waitForSiebelLoader();
		alertPopUp();
		OperationalAttributeUltra(testDataFile,sheetName,scriptNo,dataSetNo);
		alertPopUp();			
		middleAppletDarkFibre(testDataFile,sheetName,scriptNo,dataSetNo);
		ShowfullInfo();
		DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		Save();
		Reusable.waitForSiebelLoader();
		SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		//ClickHereSave();
		Reusable.savePage();
		//SearchSiteA();
		//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		//SearchSiteB();
		//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		AEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		BEndSitePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBInstallationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteATerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBTerminationTimePUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteAAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
		SiteBAccessPortPUD(testDataFile,sheetName,scriptNo,dataSetNo);
		//ClickHereSave();
		Reusable.waitForSiebelLoader();
		//GetReference(testDataFile,sheetName,scriptNo,dataSetNo);
		//ClickHereSave();
		Reusable.waitForSiebelLoader();
		break;
	}

	case "DCA Ethernet": 
	{
		//PrivateEthernetMiddleAplet(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		//	ShowfullInfo();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		//	DiversityCircuitEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		Save();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();
		SiteAServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteBServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		PickServiceParty(testDataFile,sheetName,scriptNo,dataSetNo);
		ClickHereSave();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();
		SiteASiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		SiteBSiteContact(testDataFile,sheetName,scriptNo,dataSetNo);
		PickSiteContactParty(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		Save();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();
		//SearchSiteA();
		//SearchSiteAEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();
		//SearchSiteB();
		//SearchSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.waitForpageloadmask();
		AEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		BEndSite(testDataFile,sheetName,scriptNo,dataSetNo);
		Save();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		//Reusable.savePage();
		SiteBSettingClick();
		CSPInterconnectSiteBEntry(testDataFile,sheetName,scriptNo,dataSetNo);
		Save();
		Reusable.waitForSiebelLoader();
		SiteAInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteBInstallationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		//ClickHereSave();
		Save();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteATerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteBTerminationTime(testDataFile,sheetName,scriptNo,dataSetNo);
		Save();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		//ClickHereSave();
		SiteAAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteBAccessPort(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		ClickHereSave();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteAVLan(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteBVLan(testDataFile,sheetName,scriptNo,dataSetNo);
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		ClickHereSave();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		SiteBDedicatedCloudAccess(testDataFile,sheetName,scriptNo,dataSetNo);
		//      	ClickHereSave();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		//          Save();
		break;
	}

	case "CPE Solutions Service": 
	{
		IPCPESolutionSite(testDataFile,sheetName,scriptNo,dataSetNo);
		break;
	}

	case "IP VPN Service": 
	{
		IPVPNServicePlusAccess(testDataFile,sheetName,scriptNo,dataSetNo);
		NetworkReferenceFillService(testDataFile,sheetName,scriptNo,dataSetNo);	
		IPVPNServicePlusAccess1(testDataFile,sheetName,scriptNo,dataSetNo);
		IPVPNServiceNetworkReference(testDataFile,sheetName,scriptNo,dataSetNo);

		break;
	}

	case "Voice Line V": {


		String DeliveryTeamCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeliveryTeamCountry");
		String DeliveryTeamCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DeliveryTeamCity");

		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.VoiceLineVObj.VoiceServiceCountryDropdownAccess,"Verify VoiceServiceCountryDropdownAccess");
		click(SiebelModeObj.VoiceLineVObj.VoiceServiceCountryDropdownAccess,"Click On :VoiceServiceCountryDropdownAccess");
		verifyExists(SiebelModeObj.VoiceLineVObj.VoiceServiceInputValue,"Verify  Voice Service Country");
		click(SiebelModeObj.VoiceLineVObj.VoiceServiceInputValue,"Click On  Voice Service Country");

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
		clearTextBox(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks);
		sendKeys(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"1");
		Reusable.waitForSiebelSpinnerToDisappear();


		verifyExists(SiebelModeObj.VoiceLineVObj.EgressNumberFormatDropDownAccess,"Verify EgressNumberFormatDropDownAccess");
		click(SiebelModeObj.VoiceLineVObj.EgressNumberFormatDropDownAccess,"Click On :EgressNumberFormatDropDownAccess");
		verifyExists(SiebelModeObj.VoiceLineVObj.EgreeNUmberFormat,"Verify  EgreeNUmberFormat");
		click(SiebelModeObj.VoiceLineVObj.EgreeNUmberFormat,"Click On  EgreeNUmberFormat");

		verifyExists(SiebelModeObj.VoiceLineVObj.TopologyDropDown,"Verify Topology");
		click(SiebelModeObj.VoiceLineVObj.TopologyDropDown,"Click On :Topology");
		verifyExists(SiebelModeObj.VoiceLineVObj.Topology,"Verify  Topology");
		click(SiebelModeObj.VoiceLineVObj.Topology,"Click On Topology");

		verifyExists(SiebelModeObj.VoiceLineVObj.TotalDDi,"Verify TotalDDi");
		click(SiebelModeObj.VoiceLineVObj.TotalDDi,"Click On :TotalDDi");
		sendKeys(SiebelModeObj.VoiceLineVObj.TotalDDi, "4");
		Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.TotalDDi,Keys.ENTER);


		verifyExists(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,"Verify CustomerdefaultNumber");
		click(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,"Click On :CustomerdefaultNumber");
		sendKeys(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber, "1234");
		Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.CustomerdefaultNumber,Keys.ENTER);


		//		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
		//		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On :ClickheretoSaveAccess");
		Reusable.ClickHereSave();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.VoiceLineVObj.Clicktoshowfullinfo,"Verify Clicktoshowfullinfo");
		click(SiebelModeObj.VoiceLineVObj.Clicktoshowfullinfo,"Click On Show full infor");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.VoiceLineVObj.SiteContactlookup,"Verify SiteContactlookup");
		click(SiebelModeObj.VoiceLineVObj.SiteContactlookup,"Click On Site Contact Search");


		verifyExists(SiebelModeObj.VoiceLineVObj.Site_Contact,"Verify Site_Contact");
		click(SiebelModeObj.VoiceLineVObj.Site_Contact,"Click On Submit Site Contac");


		verifyExists(SiebelModeObj.VoiceLineVObj.ServicePartsearch,"Verify ServicePartsearch");
		click(SiebelModeObj.VoiceLineVObj.ServicePartsearch,"Click On Site Contac Search");


		verifyExists(SiebelModeObj.VoiceLineVObj.PickAccntOk,"Verify PickAccntOk");
		click(SiebelModeObj.VoiceLineVObj.PickAccntOk,"Click On Submit Service party");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamDropDown,"Verify Delivery Team DropDown");
		click(SiebelModeObj.VoiceLineVObj.DeliveryTeamDropDown,"Click On Delivery Team Drop Down");

		verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,"Verify Delivery team country");
		click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,"Verify Delivery team country");
		sendKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCountry,DeliveryTeamCountry);

		verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCityDropDown,"Verify Delivery Team DropDown");
		click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCityDropDown,"Click On Delivery Team Drop Down");

		verifyExists(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,"Verify DeliveryTeamCity");
		click(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,"Click On :DeliveryTeamCity");
		sendKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,DeliveryTeamCity);
		//Reusable.SendkeaboardKeys(SiebelModeObj.VoiceLineVObj.DeliveryTeamCity,Keys.ENTER);


		//			verifyExists(SiebelModeObj.VoiceLineVObj.PerformanceReporting,"Verify PerformanceReporting");
		//			click(SiebelModeObj.VoiceLineVObj.PerformanceReporting,"Click On :PerformanceReporting");


		verifyExists(SiebelModeObj.VoiceLineVObj.WLECInvoicing,"Verify WLECInvoicing");
		click(SiebelModeObj.VoiceLineVObj.WLECInvoicing,"Click On :WLECInvoicing");


		verifyExists(SiebelModeObj.VoiceLineVObj.Crossbutton,"Verify Crossbutton");
		click(SiebelModeObj.VoiceLineVObj.Crossbutton,"Click On :Crossbutton");


		//		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
		//		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
		Reusable.ClickHereSave();
		Reusable.waitForSiebelLoader();


		break;
	}

	case "IP Access":{


		String SLA_TierValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SLA_Tier_value");
		String Offnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String capacitycheckreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Capacity_Check_Reference");
		String OSSPlatformFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City/Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
		String CabinetID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_ID");
		String ShelfID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Shelf_ID");
		String Service_PartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party_Name");

		if (Offnet.equals("IP Access_PartialDeliver"))
		{

			verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");

			verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
			click(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.capacitycheckreference,"Verify capacitycheckreference");
			click(SiebelModeObj.IPAccessObj.capacitycheckreference,"Click On :capacitycheckreference");
			sendKeys(SiebelModeObj.IPAccessObj.capacitycheckreference,capacitycheckreference);

			verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Inputossplatformflag");
			click(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Click On :Inputossplatformflag");
			sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);

			verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Verify SelectRouterTypeDropDownAccess");
			click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Click On RouterTypeDropDownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Verify RouterTypeDropDownAccess");
			click(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Click On RouterTypeDropDownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitforAttributeloader();

			verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Verify Layer3ResillanceDropdownAccess");
			click(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Click On Layer3ResillanceDropdownAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitforAttributeloader();

			verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillancePartialSelectDropdownAccess,"Verify Layer3ResillancePartialSelectDropdownAccess");
			click(SiebelModeObj.IPAccessObj.Layer3ResillancePartialSelectDropdownAccess,"Click On Layer3ResillancePartialSelectDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
			click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Click On SelectSiteSearchAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"Verify StreetNameAccess");
			sendKeysByJS(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetName);

			verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"Verify CountryAccess");
			sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);

			verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"Verify CityTownAccess");
			sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);

			verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"Verify PostalCodeAccess");
			sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);

			verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"Verify PremisesAccess");
			sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);

			verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Verify SearchButtonAccess");
			click(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Click On SearchButtonAccess");

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify SelectPickAddressAccess");
			click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On SelectPickAddressAccess");

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify ServicePartyDropdownAccess");
			click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On ServicePartyDropdownAccess");

			verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Verify InputPartyNameAccess");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,Service_PartyName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On Search");


			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyName Submit Access");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click On Site Contact SearchAccess");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click On Site Contact DropdownAccess");

			String Site_LastName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Last Name");
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Verify InputPartyNameAccess");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,Site_LastName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click On LastNameSiteSearchAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessType Dropdown Access");
			click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessType Dropdown Access");
			verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click On AccessTypeSelectAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify Access Technology Dropdown Access");
			click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click On Access Technology Dropdown Access");
			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify Access Technology SelectAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click On Access Technology SelectAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click On BuildingTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify Building Type SelectAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click On Building Type SelectAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click On CustomerSitePopStatusSelectAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click On CabinetTypeDropdownAccess");

			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click On CabinetTypeDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			String SlotID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
			verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);

			Reusable.waitForSiebelLoader();

			String PhysicalPortID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
			verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify CabinetID");
			sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalPortID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Verify PresentationInterfaceDropdownAccess");
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Click On PresentationInterfaceDropdownAccess");

			String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));

			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click On ConnectorTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Verify ConnectorTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Click On ConnectorTypeSelectAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click On FibreTypeDropdownAccess");

			String Fibre_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",Fibre_Type));
			click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",Fibre_Type));

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
			click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click On PortRoleDropDown");
			verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
			click(SiebelModeObj.IPAccessObj.PortValue,"Click On PortValue");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click On InstallTimeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click On InstallTimeSelectAccess");

			Reusable.waitForSiebelLoader();


			String Access_Time_WindowORDiversityType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Time_Window/DiversityType");
			verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Verify Accesstimewindow");
			sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");
			}

			String RouterCountry_Access = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Country");
			verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess,"Verify Router Country Access");
			sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry_Access);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess, Keys.TAB);

			Reusable.waitForSiebelLoader();


			String Router_ModelORPortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Model/PortRole");
			verifyExists(SiebelModeObj.IPAccessObj.routermodel,"Verify Router Country Access");
			sendKeys(SiebelModeObj.IPAccessObj.routermodel,Router_ModelORPortRole);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel, Keys.TAB);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");

			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");

			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
			click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");


			verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
			click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");


			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPV4AdressingTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");

			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Verify Crossbuttonforipaccess");
			click(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Click On Crossbuttonforipaccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			verifyExists(SiebelModeObj.IPAccessObj.Secondarybtn,"Verify Secondarybtn");
			click(SiebelModeObj.IPAccessObj.Secondarybtn,"Click On Secondarybtn");

			Reusable.waitForSiebelLoader();

			String OSS_PlatformFlag = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
			verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Input oss platform flag");
			sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSS_PlatformFlag);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag, Keys.TAB);


			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.BackupbandwidthDropDownAccess,"Verify BackupbandwidthDropDownAccess");
			click(SiebelModeObj.IPAccessObj.BackupbandwidthDropDownAccess,"Click On BackupbandwidthDropDownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.BackupBandwidthSelectValue,"Verify BackupBandwidthSelectValue");
			click(SiebelModeObj.IPAccessObj.BackupBandwidthSelectValue,"Click On BackupBandwidthSelectValue");

			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
			click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Click On SelectSiteSearchAccess");

			String StreetNameAccess = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
			verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"Verify Router Country Access");
			sendKeys(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetNameAccess);

			verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"Verify Router Country Access");
			sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);

			verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"Verify CityTownAccess");
			sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);

			verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"Verify PostalCodeAccesss");
			sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);

			verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"Verify Router Country Access");
			sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);

			verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Verify SearchButtonAccess");
			sendKeys(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Click on SearchButtonAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify PickAddressButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On PickAddressButtonAccess");

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			verifyExists(SiebelModeObj.IPAccessObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
			click(SiebelModeObj.IPAccessObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");

			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify PartyNameAccess");
			click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On PartyNameAccess");

			String InputPartyName = read.getExcelColumnValue(testDataFile, sheetName, "Service_Party_Name");
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess, InputPartyName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On PartyNameSearchAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyNameSubmitAccess");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On PartyNameSubmitAccess");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click On SiteContactSearchAccess");

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactDropdownAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click On SiteContactDropdownAccess");

			String SiteLastName = read.getExcelColumnValue(testDataFile, sheetName, "Site_LastName");
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Input Party Name");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess, SiteLastName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click On LastNameSiteSearchAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify LastNameSiteSubmitAccess");
			click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On LastNameSiteSubmitAccess");

			verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click On AccessTypeSelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.ProceedButton,"Verify ProceedButton");
			click(SiebelModeObj.IPAccessObj.ProceedButton,"Click On ProceedButton");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click On AccesstechnologyDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click On AccesstechnologySelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click On BuildingTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify BuildingTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click On BuildingTypeSelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click On CustomerSitePopStatusSelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click On CabinetTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click On CabinetTypeSelectAccess");

			Reusable.waitForSiebelLoader();

			String Cabinet_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_ID");
			verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify Accesstimewindow");
			sendKeys(SiebelModeObj.IPAccessObj.CabinetID,Cabinet_ID);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
			Reusable.waitForSiebelLoader();

			String shelfid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Shelf_ID");
			verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify Accesstimewindow");
			sendKeys(SiebelModeObj.IPAccessObj.shelfid,shelfid);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}

			String Slotid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
			verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Accesstimewindow");
			sendKeys(SiebelModeObj.IPAccessObj.Slotid,Slotid);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
			Reusable.waitForSiebelLoader();

			String PhysicalportId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
			verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Accesstimewindow");
			sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalportId);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Verify PresentationInterfaceDropdownAccess");
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess,"Click On PresentationInterfaceDropdownAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
			click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click On ConnectorTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Verify ConnectorTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess,"Click On ConnectorTypeSelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click On FibreTypeDropdownAccess");

			String FibreType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
			verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
			click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
			click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click On PortRoleDropDown");
			verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
			click(SiebelModeObj.IPAccessObj.PortValue,"Click On PortValue");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click On InstallTimeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
			click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click On InstallTimeSelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Verify Accesstimewindow");
			sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			if(isElementPresent(By.xpath("//span[text()='Ok']")))
			{
				verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
				click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

			}
			verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess,"Verify Router Country Access");
			sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry_Access);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess, Keys.TAB);


			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.routermodel,"Verify Router Country Access");
			sendKeys(SiebelModeObj.IPAccessObj.routermodel,Router_ModelORPortRole);
			Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel, Keys.TAB);

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
			click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
			click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");
			Reusable.savePage();
			Reusable.waitForSiebelLoader();

			String CircuitReference_Value = getTextFrom(SiebelModeObj.IPAccessObj.CircuitReferenceValue);
			DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	
		}	
		else{
			
			verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
			click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");

			verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
			click(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.capacitycheckreference,"Verify capacitycheckreference");
			click(SiebelModeObj.IPAccessObj.capacitycheckreference,"Click On :capacitycheckreference");
			sendKeys(SiebelModeObj.IPAccessObj.capacitycheckreference,capacitycheckreference);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Verify Inputossplatformflag");
			click(SiebelModeObj.IPAccessObj.Inputossplatformflag,"Click On :Inputossplatformflag");
			sendKeys(SiebelModeObj.IPAccessObj.Inputossplatformflag,OSSPlatformFlag);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Verify SelectRouterTypeDropDownAccess");
			click(SiebelModeObj.IPAccessObj.RouterTypeDropdownAccess,"Click On RouterTypeDropDownAccess");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Verify RouterTypeDropDownAccess");
			click(SiebelModeObj.IPAccessObj.SelectRouterTypeDropDownAccess,"Click On RouterTypeDropDownAccess");


			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Verify Layer3ResillanceDropdownAccess");
			click(SiebelModeObj.IPAccessObj.Layer3ResillanceDropdownAccess,"Click On Layer3ResillanceDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.Layer3ResillanceSelectDropdownAccess,"Verify Layer3ResillanceSelectDropdownAccess");
			click(SiebelModeObj.IPAccessObj.Layer3ResillanceSelectDropdownAccess,"Click On Layer3ResillanceSelectDropdownAccess");

			Reusable.waitForSiebelLoader();

			//moved this at top
			//verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
			//click(SiebelModeObj.IPAccessObj.ServiceBandwidthDropdownAccess,"Click On ServiceBandwidthDropdownAccess");

			//verifyExists(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
			//click(SiebelModeObj.IPAccessObj.ServiceBandwidthSelectAccess.replace("Value",ServiceBandwidth));
			//Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();
			
/*
			verifyExists(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess");
			click(SiebelModeObj.IPAccessObj.SelectSiteSearchAccess,"Click On SelectSiteSearchAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.StreetNameAccess,"Verify StreetNameAccess");
			sendKeys(SiebelModeObj.IPAccessObj.StreetNameAccess,StreetName);

			verifyExists(SiebelModeObj.IPAccessObj.CountryAccess,"Verify CountryAccess");
			sendKeys(SiebelModeObj.IPAccessObj.CountryAccess,Country);

			verifyExists(SiebelModeObj.IPAccessObj.CityTownAccess,"Verify CityTownAccess");
			sendKeys(SiebelModeObj.IPAccessObj.CityTownAccess,CityORTown);

			verifyExists(SiebelModeObj.IPAccessObj.PostalCodeAccess,"Verify PostalCodeAccess");
			sendKeys(SiebelModeObj.IPAccessObj.PostalCodeAccess,PostalCode);

			verifyExists(SiebelModeObj.IPAccessObj.PremisesAccess,"Verify PremisesAccess");
			sendKeys(SiebelModeObj.IPAccessObj.PremisesAccess,Premises);

			verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Verify SearchButtonAccess");
			click(SiebelModeObj.IPAccessObj.SearchButtonAccess1,"Click On SearchButtonAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Verify SelectPickAddressAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickAddressAccess,"Click On SelectPickAddressAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Verify PickAddressButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickAddressButtonAccess,"Click On PickAddressButtonAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Verify SelectPickBuildingAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickBuildingAccess,"Click On SelectPickBuildingAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Verify PickBuildingButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickBuildingButtonAccess,"Click On PickBuildingButtonAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Verify SelectPickSiteAccess");
			click(SiebelModeObj.IPAccessObj.SelectPickSiteAccess,"Click On SelectPickSiteAccess");

			verifyExists(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Verify PickSiteButtonAccess");
			click(SiebelModeObj.IPAccessObj.PickSiteButtonAccess,"Click On PickSiteButtonAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();
			//Reusable.waitforAttributeloader();
*/
			
			addSiteAXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
			
			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			clickByJS(SiebelModeObj.addSiteADetailsObj.IpGurdianSave);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Verify ServicePartySearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click On ServicePartySearchAccess");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify ServicePartyDropdownAccess");
			click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click On ServicePartyDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify ServicePartyDropdownAccess");
			click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click On ServicePartyDropdownAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Verify InputPartyNameAccess");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,Service_PartyName);
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click On Search");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyName Submit Access");
			click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click On Submit");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click On Site Contact SearchAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click On Site Contact DropdownAccess");
			Reusable.waitForSiebelLoader();

			String Site_LastName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_LastName");
			verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Verify InputPartyNameAccess");
			sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,Site_LastName);

			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click On LastNameSiteSearchAccess");
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify LastNameSiteSubmitAccess");
			click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click On LastNameSiteSubmitAccess");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
			click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
			Reusable.waitForSiebelLoader();

			if(Offnet.equals("Offnet"))	
			{		    	 
				String CustomerSitePopStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Site_Pop_Status");
				String Third_Party_Connection_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_Party_Connection_Reference");
				String Third_party_access_provider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_party_access_provider");
				String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");

				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
				Reusable.waitForSiebelLoader();


				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusSelectAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click On CustomerSitePopStatusSelectAccess");
				Reusable.waitForSiebelLoader();

				if(SLA_TierValue.contains("SLA"))
				{
					verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));
					click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",CustomerSitePopStatus));

				}
				else {
					verifyExists(SiebelModeObj.IPAccessObj.Customersitepopupstatusoffnet1,"Verify Customersitepopupstatusoffnet");
					click(SiebelModeObj.IPAccessObj.Customersitepopupstatusoffnet1,"Click Customersitepopupstatusoffnet");
				}
				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
				Reusable.waitForSiebelLoader();//


				verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
				click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click On ThirdpartyaccessproviderDropDown");
				Reusable.waitForSiebelLoader();
				if(SLA_TierValue.contains("SLA"))
				{
					verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Third_Party_Connection_Reference));
					click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Third_Party_Connection_Reference));
					//waitforAttributeloader();
				}
				else {
					verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
					click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
				}
				verifyExists(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conection reference");
				click(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conection reference");
				sendKeys(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,Third_Party_Connection_Reference,"Enter Third party conection reference");
				Reusable.waitForSiebelLoader();

				if (!AccessType.equalsIgnoreCase("ULL Fibre")) 
				{
					verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown);
					click(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown);

					if (SLA_TierValue.contains("SLA"))  {

						verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",IPRange));
						click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",IPRange));
					}

					else {

						verifyExists(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
						click(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
					}
				}	
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
				click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
				Reusable.waitForSiebelLoader();
			}

			else{
				verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
				click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
				if(SLA_TierValue.contains("SLA"))
				{
					verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
					click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
					//waitforAttributeloader();
				}
				else {
					verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Verify AccessTypeSelectAccess");
					click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Verify AccessTypeSelectAccess");
				}	

				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
				click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess," Verify AccesstechnologySelectAccess");

				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify BuildingTypeDropdownAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify BuildingTypeDropdownAccess");
				Reusable.waitForSiebelLoader();
				verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify BuildingTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify BuildingTypeSelectAccess");

				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess," Verify CustomerSitePopStatusDropdownAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess," Verify CustomerSitePopStatusDropdownAccess");
				verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess," Verify CustomerSitePopStatusSelectAccess");
				click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess," Verify CustomerSitePopStatusSelectAccess");

				Reusable.waitForSiebelLoader();

				verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
				click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
				Reusable.waitForSiebelLoader();
			} 

			//Commented this as seems it reseting the values
			/* 
	     	verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
			click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click On AccessTypeDropdownAccess");
			if(SLA_TierValue.contains("SLA"))
			{
				verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
				click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",AccessType));
				//waitforAttributeloader();
			}
			else {
				verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Verify AccessTypeSelectAccess");
				click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess," Click AccessTypeSelectAccess");
				}	

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess," Verify AccesstechnologyDropdownAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess," Click AccesstechnologyDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess," Verify AccesstechnologySelectAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess," Click AccesstechnologySelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify AccesstechnologyDropdownAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Click BuildingTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify BuildingTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Click BuildingTypeSelectAccess");

			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Verify CustomerSitePopStatusDropdownAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess," Click CustomerSitePopStatusDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Verify CustomerSitePopStatusSelectAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess," Click CustomerSitePopStatusSelectAccess");

			Reusable.waitForSiebelLoader();
			verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
			click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
			Reusable.waitForSiebelLoader();	*/
		}

		verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess," Verify CabinetTypeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess," Click CabinetTypeDropdownAccess");

		verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess," Verify CabinetTypeSelectAccess");
		click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess," Click CabinetTypeSelectAccess");

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
		sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify shelfid");
		sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");

		Reusable.waitForSiebelLoader();
		if(isElementPresent(By.xpath("//span[text()='Ok']")))
		{
			verifyExists(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Verify on Alert Accept");
			click(SiebelModeObj.SiteAServicePartyObj.AlertAccept,"Click  on Alert Accept");

		}

		String SlotID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
		verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Slotid");
		sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
		
		Reusable.waitForSiebelLoader();

		String PhysicalPortID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
		verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Physicalportid");
		sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,PhysicalPortID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
		
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Verify PresentationInterfaceDropdownAccess");
		click(SiebelModeObj.IPAccessObj.PresentationInterfaceDropdownAccess," Click PresentationInterfaceDropdownAccess");
		Reusable.waitForSiebelLoader();
		String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
		verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface));
		click(SiebelModeObj.IPAccessObj.PresentationInterfaceSelectAccess.replace("Value",PresentationInterface ));

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Verify ConnectorTypeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess," Click ConnectorTypeDropdownAccess");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess," Verify ConnectorTypeSelectAccess");
		click(SiebelModeObj.IPAccessObj.ConnectorTypeSelectAccess," Click ConnectorTypeSelectAccess");

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click FibreTypeDropdownAccess");
		Reusable.waitForSiebelLoader();
		String FibreTypeDropdownAccess = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
		verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreTypeDropdownAccess),"Fibre Type Select Access");
		click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreTypeDropdownAccess ),"Fibre Type SelectAccess");

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role"));
		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Port Role" ));
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.IPAccessObj.SelectValueDropdown.replace("Value","Physical Port"));
		click(SiebelModeObj.IPAccessObj.SelectValueDropdown.replace("Value","Physical Port" ));


		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Verify InstallTimeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess," Click InstallTimeDropdownAccess");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Verify InstallTimeSelectAccess");
		click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess," Click InstallTimeSelectAccess");

		Reusable.waitForSiebelLoader();

		String Access_Time_WindowORDiversityType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Time_Window/DiversityType");
		verifyExists(SiebelModeObj.IPAccessObj.Accesstimewindow,"Access time window");
		sendKeys(SiebelModeObj.IPAccessObj.Accesstimewindow,Access_Time_WindowORDiversityType,"access time window");
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Accesstimewindow, Keys.TAB);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
		Reusable.waitForSiebelLoader();

		String RouterCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Country");
		verifyExists(SiebelModeObj.IPAccessObj.RouterCountryAccess1,"Router Country");
		click(SiebelModeObj.IPAccessObj.RouterCountryAccess1,"Router Country");
		Reusable.waitForSiebelLoader();
		//sendKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess,RouterCountry,"Router Country");
		click(SiebelModeObj.IPAccessObj.RouterCountrySelect.replace("Value",RouterCountry),"Select Router Country");
		//Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterCountryAccess1, Keys.TAB);
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		String routermodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Model/PortRole");
		verifyExists(SiebelModeObj.IPAccessObj.routermodel1,"router model");
		click(SiebelModeObj.IPAccessObj.routermodel1,"router model");
		Reusable.waitForSiebelLoader();
		click(SiebelModeObj.IPAccessObj.routermodelSelect.replace("Value",routermodel),"Select Router model");
		//sendKeys(SiebelModeObj.IPAccessObj.routermodel,routermodel,"router model");
		//Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.routermodel1, Keys.TAB);
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
		click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click On RouterSiteNameDropdownAccess");
		Reusable.waitForSiebelLoader();

		//verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Verify RouterSiteNameSelectAccess");
		//click(SiebelModeObj.IPAccessObj.RouterSiteNameSelectAccess,"Click On RouterSiteNameSelectAccess");
		//Reusable.waitForSiebelLoader();
		//Reusable.waitForSiebelSpinnerToDisappear();

		String Alerting_NotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
		String SiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Name");

		if(SLA_TierValue.contains("SLA"))
		{
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Alerting_NotificationEmailAddress));
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",Alerting_NotificationEmailAddress));
			//waitforAttributeloader();
		}
		else {
			verifyExists(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
			click(SiebelModeObj.WaveLineMidleApplet.MiddleLi.replace("Value",SiteName));
		}	

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Verify ClickheretoSaveAccess");
		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess1,"Click On ClickheretoSaveAccess");
		Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Verify ClickShowFullInfoAccess");
		click(SiebelModeObj.IPAccessObj.ClickShowFullInfoAccess,"Click On ClickShowFullInfoAccess");

		verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Verify IPAdressingFormatDropdownAccess");
		click(SiebelModeObj.IPAccessObj.IPAdressingFormatDropdownAccess,"Click On IPAdressingFormatDropdownAccess");
		verifyExists(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Verify IPAdressingFormatSelectAccess");
		click(SiebelModeObj.IPAccessObj.IPAdressingFormatSelectAccess,"Click On IPAdressingFormatSelectAccess");

		Reusable.waitForSiebelLoader();


		verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Verify IPV4AdressingTypeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeDropdownAccess,"Click On IPV4AdressingTypeDropdownAccess");
		verifyExists(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Verify IPAdressingFormatSelectAccess");
		click(SiebelModeObj.IPAccessObj.IPV4AdressingTypeSelectAccess,"Click On IPV4AdressingTypeSelectAccess");

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Verify Crossbuttonforipaccess");
		click(SiebelModeObj.IPAccessObj.Crossbuttonforipaccess,"Click On Crossbuttonforipaccess");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
		click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click On IpGurdianSave");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
		click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");

		Reusable.waitForSiebelLoader();
		Reusable.savePage();

		Reusable.waitForSiebelLoader();

		String CircuitReference_Value = getTextFrom(SiebelModeObj.IPAccessObj.CircuitReferenceValue);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	
		break;


	}

	case "SIP Trunking":	{


		String IPGuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
		String IncludeColtTechnicalContact_in_Test_Calls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
		String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
		String IP_Range = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String AccessLineType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AccessLineType");


		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.VoiceServiceCountry,"Verify Voice Service Country");
		click(SiebelModeObj.SIPTrunkingObj.VoiceServiceCountry);
		verifyExists(SiebelModeObj.SIPTrunkingObj.NewVoiceServiceCountry,"Verify New Voice Service Country");
		click(SiebelModeObj.SIPTrunkingObj.NewVoiceServiceCountry);

		//	Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.CallAdmissionControl,"Verify CallAdmissionControl");
		sendKeys(SiebelModeObj.SIPTrunkingObj.CallAdmissionControl,"12");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"Verify NumberOfSignallingTrunks");
		sendKeys(SiebelModeObj.SIPTrunkingObj.NumberOfSignallingTrunks,"1");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.EgressNumberFormatDropDownAccess,"Verify EgressNumberFormatDropDownAccess");
		click(SiebelModeObj.SIPTrunkingObj.EgressNumberFormatDropDownAccess);
		verifyExists(SiebelModeObj.SIPTrunkingObj.EgreeNUmberFormat,"Verify EgreeNUmberFormat");
		click(SiebelModeObj.SIPTrunkingObj.EgreeNUmberFormat);
		//	Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.InvlidCLITreatmet,"Verify InvlidCLITreatmet");
		sendKeys(SiebelModeObj.SIPTrunkingObj.InvlidCLITreatmet,"Allow");
		Reusable.waitForSiebelSpinnerToDisappear();
		//	Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs,"Verify TotalNumberDDIs");
		click(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs);
		sendKeys(SiebelModeObj.SIPTrunkingObj.TotalNumberDDIs,"1");
		Reusable.waitForSiebelSpinnerToDisappear();
		//	Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.IncomingDDIs,"Verify IncomingDDIs");
		click(SiebelModeObj.SIPTrunkingObj.IncomingDDIs);
		sendKeys(SiebelModeObj.SIPTrunkingObj.IncomingDDIs,"10");
		Reusable.waitForSiebelSpinnerToDisappear();
		//	Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.RoutingSequence,"Verify RoutingSequence");
		click(SiebelModeObj.SIPTrunkingObj.RoutingSequence);
		sendKeys(SiebelModeObj.SIPTrunkingObj.RoutingSequence,"Sequential");
		Reusable.waitForSiebelSpinnerToDisappear();
		//	Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","First Codec"));
		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","First Codec"));
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IPGuardianVariant));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IPGuardianVariant));

		verifyExists(SiebelModeObj.SIPTrunkingObj.ClickLink.replace("Value","Show More"));
		click(SiebelModeObj.SIPTrunkingObj.ClickLink.replace("Value","Show More"));

		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Second Codec"));
		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Second Codec"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IncludeColtTechnicalContact_in_Test_Calls));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IncludeColtTechnicalContact_in_Test_Calls));
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Third Codec"));
		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Third Codec"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",ServiceBandwidth));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",ServiceBandwidth));
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fourth Codec"));
		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fourth Codec"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",TestWindow));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",TestWindow));
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fifth Codec"));
		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Fifth Codec"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",CircuitIPAddressInput));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",CircuitIPAddressInput));
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Sixth Codec"));
		click(SiebelModeObj.IPAccessObj.ClickDropdown.replace("Value","Sixth Codec"));

		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IP_Range));
		click(SiebelModeObj.addSiteADetailsObj.SiteABSelection.replace("Value",IP_Range));
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SIPTrunkingObj.ClickToShowFullInfo,"Verify ClickToShowFullInfo");
		click(SiebelModeObj.SIPTrunkingObj.ClickToShowFullInfo);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSiteContact,"Verify SIPSiteContact");
		click(SiebelModeObj.SIPTrunkingObj.SIPSiteContact);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.ClickOK,"Verify OK");
		click(SiebelModeObj.SIPTrunkingObj.ClickOK);
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		//	Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.ServiceParty,"Verify ServiceParty");
		click(SiebelModeObj.SIPTrunkingObj.ServiceParty);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.OKSelection,"Verify OKSelection");
		click(SiebelModeObj.SIPTrunkingObj.OKSelection);
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		//		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry,"Verify SIPDeliveryTeamNewCountry");
		click(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCountry,"United Kingdom");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity,"Verify SIPDeliveryTeamNewCity");
		click(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity);
		sendKeys1(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity,"London");
		//			Reusable.SendkeaboardKeys(SiebelModeObj.SIPTrunkingObj.SIPDeliveryTeamNewCity, Keys.ENTER);
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.CloseSymbol,"Verify CloseSymbol");
		click(SiebelModeObj.SIPTrunkingObj.CloseSymbol,"CloseSymbol");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelLoader();
		Reusable.Save();

		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbol,"Verify SIPVoiceConfigExpandSymbol");
		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbol);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence,"Verify SIPTrunkSequence");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkSequence,"1");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkName,"Verify SIPTrunkName");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkName);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkName,"Trunk2");
		Reusable.waitForSiebelSpinnerToDisappear();
		//Some below codes commented as we are facing functional issue.

		verifyExists(SiebelModeObj.SIPTrunkingObj.AccessLineTypeDropDown,"Verify AccessLineTypeDropDown");
		click(SiebelModeObj.SIPTrunkingObj.AccessLineTypeDropDown);

		verifyExists(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType),"Verify AccessLineType");
		click(SiebelModeObj.SIPTrunkingObj.AccessLineType.replace("Value",AccessLineType));


		verifyExists(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup,"Verify AccessServiceIdlookup");
		click(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup);

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Verify SearchServiceIDbox");
		click(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Product");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchText,"Verify SearchText");
		click(SiebelModeObj.SIPTrunkingObj.SearchText);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SearchText,"IP Access");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess,"Verify SearchButtonProcess");
		click(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton,"Verify SearchServiceIdOKButton");
		click(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4,"Verify SIPCustomerOriginatingIPv4");
		click(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv4,"10.7.235.29/24");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6,"Verify SIPCustomerOriginatingIPv6");
		click(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerOriginatingIPv6,"10.7.235.29/24");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4,"Verify SIPCustomerTerminatingIPv4");
		click(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv4,"10.7.235.2/24");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6,"Verify SIPCustomerTerminatingIPv6");
		click(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPCustomerTerminatingIPv6,"10.7.235.2/24");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocol,"Verify SIPSignalingTransportProtocol");
		click(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocol);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocolDropdown,"Verify SIPSignalingTransportProtocolDropdown");
		click(SiebelModeObj.SIPTrunkingObj.SIPSignalingTransportProtocolDropdown);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocol,"Verify SIPvoIPProtocol");
		click(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocol);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocolDropDown,"Verify SIPvoIPProtocolDropDown");
		click(SiebelModeObj.SIPTrunkingObj.SIPvoIPProtocolDropDown);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType,"Verify SIPEncryptionType");
		click(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPEncryptionType,"TLS on, SRTP off");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent,"Verify SIPProportionPercent");
		click(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPProportionPercent,"12");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Verify SIPTrunkSiteNameAlias");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkSiteNameAlias,"Alias1");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNATRequired,"Verify SIPNATRequired");
		click(SiebelModeObj.SIPTrunkingObj.SIPNATRequired);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNATRequired,"N");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address,"Verify SIPNNIWANIPv4Address");
		click(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv4Address,"10.7.235.24/2");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address,"Verify SIPNNIWANIPv6Address");
		click(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNNIWANIPv6Address,"10.7.235.24/2");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT,"Verify SIPIPv4AddressRangeNAT");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPv4AddressRangeNAT,"2");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange,"Verify SIPVLANStartEndRange");
		click(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPVLANStartEndRange,"2");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVLANID,"Verify SIPVLANID");
		click(SiebelModeObj.SIPTrunkingObj.SIPVLANID);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPVLANID,"2");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPASNumber,"Verify SIPASNumber");
		click(SiebelModeObj.SIPTrunkingObj.SIPASNumber);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPASNumber,"1");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address,"Verify SIPColtHostEquipmentIPv4Address");
		click(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv4Address,"10.7.235.24/2");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address,"Verify SIPColtHostEquipmentIPv6Address");
		click(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPColtHostEquipmentIPv6Address,"10.7.235.24/2");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls,"Verify SIPTrunkMaxNumberofSimoultaniousCalls");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxNumberofSimoultaniousCalls,"4");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit,"Verify SIPTrunkInboundOutboundSplit");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkInboundOutboundSplit,"No");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls,"Verify SIPTrunkMaxInboundCalls");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkMaxInboundCalls,"1");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk,"Verify SIPTrunkOverCallLimitForTrunk");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkOverCallLimitForTrunk,"1");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk,"Verify SIPTrunkIncallLimitForTrunk");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkIncallLimitForTrunk,"10");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk,"Verify SIPTrunkOutCallLimitForTrunk");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkOutCallLimitForTrunk,"1");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC,"Verify SIPTrunkTypeofCAC");
		click(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPTrunkTypeofCAC,"Individual Trunk CAC");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPCustomerIPPbxIdTab,"Verify SIPCustomerIPPbxIdTab");
		click(SiebelModeObj.SIPTrunkingObj.SIPCustomerIPPbxIdTab);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPAddrecordSymbolIPPBX,"Verify SIPAddrecordSymbolIPPBX");
		click(SiebelModeObj.SIPTrunkingObj.SIPAddrecordSymbolIPPBX);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasName,"Verify SIPIPPBXAliasName");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasName);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField,"Verify SIPIPPBXAliasNameInputField");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAliasNameInputField,"test1");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModel,"Verify SIPIPPBXModel");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModel);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXModelInput,"SWE Lite7.0");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendor,"Verify SIPIPPBXVendor");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendor);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput,"Verify SIPIPPBXVendorInput");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXVendorInput,"Sonus");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSOftwareVersionField,"Verify SIPSOftwareVersionField");
		click(SiebelModeObj.SIPTrunkingObj.SIPSOftwareVersionField);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput,"Verify SIPSoftwareVersionInput");
		click(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPSoftwareVersionInput,"1");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUID,"Verify SIPIPPBXAddressUID");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUID);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXLookupoptionClick,"Verify SIPIPPBXLookupoptionClick");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXLookupoptionClick);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName,"Verify SIPIPPBXAddressUIDStreetName");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDStreetName,"Parker");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName,"Verify SIPIPPBXAddressUIDStreetName");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCountryName,"United Kingdom");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName,"Verify c");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDCityName,"London");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode,"Verify SIPIPPBXAddressUIDPostalCode");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode,"*");
		Reusable.SendkeaboardKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPostalCode, Keys.TAB);
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode,"Verify SIPIPPBXAddressUIDPremiseCode");
		click(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPIPPBXAddressUIDPremiseCode,"*");

		// Click Address search button
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPAddressUIDSearchButton,"Verify SIPAddressUIDSearchButton");
		click(SiebelModeObj.SIPTrunkingObj.SIPAddressUIDSearchButton,"Search button");	

		// Select address record
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPSelectAddressRecord,"Verify SIPSelectAddressRecord");
		click(SiebelModeObj.SIPTrunkingObj.SIPSelectAddressRecord,"Address Record");

		// Click Pick address button
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPickAddressButtonForUID,"Verify SIPPickAddressButtonForUID");
		click(SiebelModeObj.SIPTrunkingObj.SIPPickAddressButtonForUID,"Pick Address button");

		// IPBX Technical Contact
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContact,"Verify SIPPBXTechnicalContact");
		click(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContact);

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTecnicalContactLookup,"Verify SIPPBXTecnicalContactLookup");
		click(SiebelModeObj.SIPTrunkingObj.SIPPBXTecnicalContactLookup,"SIPPBXTecnicalContactLookup");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContactLookupOKButton,"Verify SIPPBXTechnicalContactLookupOKButton");
		click(SiebelModeObj.SIPTrunkingObj.SIPPBXTechnicalContactLookupOKButton,"SIPPBXTechnicalContactLookupOKButton");
		Reusable.savePage();
		//		Reusable.Save();
		Reusable.waitForSiebelLoader();

		// click on Voice Config tab to enter the IP PBX details
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigTab,"Verify SIPVoiceConfigTab");
		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigTab,"SIPVoiceConfigTab");

		// Selection of IP PBX in TRUNK --
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbolIPPBXSelewction,"Verify SIPVoiceConfigExpandSymbolIPPBXSelewction");
		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigExpandSymbolIPPBXSelewction,"SIPVoiceConfigExpandSymbolIPPBXSelewction");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPReClickToIPPBXLookUp,"Verify SIPReClickToIPPBXLookUp");
		click(SiebelModeObj.SIPTrunkingObj.SIPReClickToIPPBXLookUp,"SIPReClickToIPPBXLookUp");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPOKSelectionButton,"Verify SIPOKSelectionButton");
		click(SiebelModeObj.SIPTrunkingObj.SIPOKSelectionButton,"SIPOKSelectionButton");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		/* ABove code for Trunk including IPPBX code */

		// Other Tab in Voice Config

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPOtherTab,"Verify Other Tab");
		click(SiebelModeObj.SIPTrunkingObj.SIPOtherTab,"Other Tab");
		Reusable.waitForSiebelLoader();
		/*	
		if (isElementPresent(By.xpath("//div[@id='colt-s-order-connection-header']//a[text()='Save']"))) {
			Reusable.Save();
			Reusable.waitForAjax();
		}

		 */	
		waitForElementToAppear(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,30);
		ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,"Verify SIPVoiceConfigOthersTabShowFullInfo");
		//		Reusable.waitForSiebelLoader();
		//		ScrollIntoViewByString(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);
		//		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo,"SIPVoiceConfigOthersTabShowFullInfo");
		javaScriptclick(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);

		Reusable.waitForSiebelLoader();	

		/*			if (isElementPresent(By.xpath("(//*[normalize-space(text()) and normalize-space(.)='Colt DDI Ranges'])[1]/preceding::span[1]"))) {
			break;
		}else{
			javaScriptclick(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabShowFullInfo);

		}

		Reusable.waitForSiebelLoader();

		 */	
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRanges,"Verify SIPVoiceConfigOthersTabColtDDIRanges");
		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRanges,"SIPVoiceConfigOthersTabColtDDIRanges");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs,"Verify SIPNumberOfSingleColtDDIs");
		click(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOfSingleColtDDIs,"1");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges,"Verify SIPNumberOf10ColtDDIRanges");
		click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf10ColtDDIRanges,"1");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges,"Verify SIPNumberOf100ColtDDIRanges");
		click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf100ColtDDIRanges,"1");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges,"Verify SIPNumberOf1000ColtDDIRanges");
		click(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SIPNumberOf1000ColtDDIRanges,"1");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRangesCloseWindow,"Verify SIPVoiceConfigOthersTabColtDDIRangesCloseWindow");
		click(SiebelModeObj.SIPTrunkingObj.SIPVoiceConfigOthersTabColtDDIRangesCloseWindow,"SIPVoiceConfigOthersTabColtDDIRangesCloseWindow");

		Reusable.Save();
		//	Reusable.savePage();
		Reusable.waitForSiebelLoader();
		break;  
	} 

	case "Managed Virtual Firewall": {

		verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFBillingTab,"Verify MVFBilling Tab");
		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFBillingTab);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFShowfullInfo,"Verify MVFShowfullInfo");
		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFShowfullInfo);

		verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink,"Verify MVFSecurityPolicyAttachmentlink");
		//		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink);
		sendKeys(SiebelModeObj.ManagedVirtualFirewallObj.MVFSecurityPolicyAttachmentlink, "1234");
		Reusable.waitForSiebelSpinnerToDisappear();


		//	verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFAddbutton,"Verify MVFAddbutton");
		//	click(SiebelModeObj.ManagedVirtualFirewallObj.MVFAddbutton);

		verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFClosebutton,"Verify MVFClosebutton");
		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFClosebutton,"MVF Close button");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		//		verifyExists(SiebelModeObj.ManagedVirtualFirewallObj.MVFSavebutton,"Verify MVFSavebutton");
		//		click(SiebelModeObj.ManagedVirtualFirewallObj.MVFSavebutton);
		//		Reusable.waitForSiebelLoader();	

		break;
	}
	case "Ethernet VPN Access": {

		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City/Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
		String InputPartyName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party_Name");	
		String InputSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_LastName");
		String Offnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Offnet");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Access_Type");
		String Third_party_access_provider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "3rd_party_access_provider");
		String Third_Party_Connection_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "3rd_Party_Connection_Reference");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IP_Range");
		String BCPReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BCP_Reference");
		String CabinetID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_ID");
		String ShelfID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Shelf_ID");
		String SlotID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Slot_ID");
		String Physical_PortID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Physical_Port_ID");
		String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
		String FibreType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");


		verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Verify ServiceBandwidthDropdownAccess");
		click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthDropdownAccess,"Click on ServiceBandwidthDropdownAccess");
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
		click(SiebelModeObj.EthernetVPNAccessObj.ServiceBandwidthSelect.replace("Value",ServiceBandwidth),ServiceBandwidth);
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.EthernetVPNAccessObj.LinkTypeDropDown,"Link Type Drop Down");
		click(SiebelModeObj.EthernetVPNAccessObj.LinkTypeDropDown,"Link Type Drop Down");
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.LinkTYpeValue,"Verify LinkTYpeValue");
		click(SiebelModeObj.EthernetVPNAccessObj.LinkTYpeValue,"Click on LinkTYpeValue");
		Reusable.waitForSiebelSpinnerToDisappear();


		verifyExists(SiebelModeObj.EthernetVPNAccessObj.ResilenceOptionDropDown,"Verify ResilenceOptionDropDown");
		click(SiebelModeObj.EthernetVPNAccessObj.ResilenceOptionDropDown,"Click on ResilenceOptionDropDown");
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.ResilienceValue,"Verify Resilience Value");
		click(SiebelModeObj.EthernetVPNAccessObj.ResilienceValue,"Click on Resilience Value");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.EthernetVPNAccessObj.OSSPlatformDropDown,"Verify OSSPlatformDropDown");
		click(SiebelModeObj.EthernetVPNAccessObj.OSSPlatformDropDown,"Click on OSSPlatformDropDown");
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.OSSplatformValue,"Verify OSSplatform Value");
		click(SiebelModeObj.EthernetVPNAccessObj.OSSplatformValue,"Click on OSSplatform Value");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
		click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.EthernetVPNAccessObj.SelectSiteSearchAccess,"Verify SelectSiteSearchAccess Value");
		click(SiebelModeObj.EthernetVPNAccessObj.SelectSiteSearchAccess,"Click on SelectSiteSearchAccess");

		Reusable.waitForElementToAppear(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput, 10);
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"Site ID input");


		if(Offnet.equals("Offnet"))
		{
			sendKeys(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"LON06215/026");
		}else{
			sendKeys(SiebelModeObj.EthernetVPNAccessObj.SiteIDInput,"MAD99900/011");
		}

		verifyExists(SiebelModeObj.EthernetVPNAccessObj.SiteIDSearchBtn,"SiteIDSearchBtn");
		click(SiebelModeObj.EthernetVPNAccessObj.SiteIDSearchBtn,"SiteIDSearchBtn");
		Reusable.waitForSiebelLoader();






		//////////////////////////// below code commets
		/*
	Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.StreetNameAccess, 10);
	verifyExists(SiebelModeObj.VoiceConfigTab.StreetNameAccess,"StreetNameAccess");
	sendKeys(SiebelModeObj.VoiceConfigTab.StreetNameAccess,StreetName);

	Reusable.waitForElementToAppear(SiebelModeObj.VoiceConfigTab.CountryAccess, 10);
	verifyExists(SiebelModeObj.VoiceConfigTab.CountryAccess,"Enter Country");
	sendKeys(SiebelModeObj.VoiceConfigTab.CountryAccess,Country);

	verifyExists(SiebelModeObj.VoiceConfigTab.CityTownAccess,"Enter CityTownAccess");
	sendKeys(SiebelModeObj.VoiceConfigTab.CityTownAccess,CityORTown);

	verifyExists(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,"Postal Code");
	sendKeys(SiebelModeObj.VoiceConfigTab.PostalCodeAccess,PostalCode);

	verifyExists(SiebelModeObj.VoiceConfigTab.PremisesAccess,"Premises");
	sendKeys(SiebelModeObj.VoiceConfigTab.PremisesAccess,Premises);

	verifyExists(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Verify SearchButtonAccess");
	click(SiebelModeObj.IPAccessObj.SearchButtonAccess,"Click on SearchButtonAccess");
	Reusable.waitForSiebelSpinnerToDisappear();

	verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Pick Address Access");
	javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickAddressAccess,"Select Address for Site");
	verifyExists(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
	click(SiebelModeObj.VoiceConfigTab.PickAddressButtonAccess,"Submit Address for Site");
	Reusable.waitForSiebelSpinnerToDisappear();


	verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
	javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickBuildingAccess,"Select Buiding for Site");
	verifyExists(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Buiding for Site");
	click(SiebelModeObj.VoiceConfigTab.PickBuildingButtonAccess,"Submit Address for Site");
	Reusable.waitForSiebelSpinnerToDisappear();
		 */
		////////////above code commets


		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
		javaScriptclick(SiebelModeObj.VoiceConfigTab.SelectPickSiteAccess,"Select Site");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
		click(SiebelModeObj.VoiceConfigTab.PickSiteButtonAccess,"Submit Site");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Service Party Search Access");
		click(SiebelModeObj.addSiteADetailsObj.ServicePartySearchAccess,"Click on Service Party SearchAccess");


		///////////////////////////////////////// below code commets
		/*
	waitForElementToAppear(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess, 8);
	verifyExists(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Verify Service party drop down Acccess");
	click(SiebelModeObj.addSiteADetailsObj.ServicePartyDropdownAccess,"Click on Service party drop down Acccess");

	verifyExists(SiebelModeObj.IPAccessObj.PartyNameAccess,"Verify Site Contact drop down Acccess");
	click(SiebelModeObj.IPAccessObj.PartyNameAccess,"Click on site Contact drop down Acccess");

	verifyExists(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,"Input Party Name");
	sendKeys(SiebelModeObj.addSiteADetailsObj.InputPartyNameAccess,InputPartyName);

	verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Verify PartyNameSearchAccess");
	click(SiebelModeObj.addSiteADetailsObj.PartyNameSearchAccess,"Click on search");

		 */
		///////////////////////////above code commets


		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Verify PartyNameSubmitAccess");
		click(SiebelModeObj.addSiteADetailsObj.PartyNameSubmitAccess,"Click on PartyName Submit");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Verify SiteContactSearchAccess");
		click(SiebelModeObj.addSiteADetailsObj.SiteContactSearchAccess,"Click on SiteContactSearchAccess");
		Reusable.waitForSiebelLoader();


		////////////////////////////////////////////////////below code comment

		/*
	verifyExists(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Verify SiteContactDropdownAccess");
	click(SiebelModeObj.addSiteADetailsObj.SiteContactDropdownAccess,"Click on SiteContactDropdownAccess");

	verifyExists(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,"Input Site Name");
	sendKeys(SiebelModeObj.addSiteADetailsObj.InputSiteNameAccess,InputSiteName);

	verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Verify LastNameSiteSearchAccess");
	click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSearchAccess,"Click on LastNameSiteSearch");
		 */
		/////////////////////////////////////////////////////////Above code commet


		verifyExists(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Verify Pick Contact:OK");
		click(SiebelModeObj.addSiteADetailsObj.LastNameSiteSubmitAccess,"Click on Pick Contact:OK");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Verify IpGurdianSave");
		click(SiebelModeObj.addSiteADetailsObj.IpGurdianSave,"Click on IpGurdianSave");
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();





		if(Offnet.equals("Offnet"))
		{
			verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
			click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click on AccessTypeDropdownAccess");
			verifyExists(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("AccessTypeValue",AccessType));
			click(SiebelModeObj.AEndSiteObj.AccesstypeOffnet.replace("AccessTypeValue",AccessType));
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Verify ThirdpartyaccessproviderDropDown");
			click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessproviderDropDown,"Click on ThirdpartyaccessproviderDropDown");
			verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
			click(SiebelModeObj.AEndSiteObj.ThirdpartyaccessProvidervalue.replace("Value",Third_party_access_provider));
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click on AccesstechnologyDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyEhternetValue,"Verify AccesstechnologySelectAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologyEhternetValue,"Click on AccesstechnologySelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,"Third party conectionre ference");
			sendKeys(SiebelModeObj.AEndSiteObj.Thirdpartyconectionreference1,Third_Party_Connection_Reference);
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click on BuildingTypeDropdownAccess");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeOFFNetSelectAccess,"Verify BuildingTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeOFFNetSelectAccess,"Click on BuildingTypeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click on CustomerSitePopStatusDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusOFFnetSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusOFFnetSelectAccess,"Click on CustomerSitePopStatusSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Verify BCPReference");
			click(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Click on BCPReference");
			sendKeys(SiebelModeObj.EthernetVPNAccessObj.BCPReference,BCPReference);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();


			verifyExists(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Verify ThirdpartyDropDown");
			click(SiebelModeObj.AEndSiteObj.ThirdpartyDropDown,"Click on ThirdpartyDropDown");

			verifyExists(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
			click(SiebelModeObj.IPAccessObj.ThirdpartySLATiervalue.replace("SLAValue",IPRange));
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
			click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
			Reusable.waitForSiebelLoader();

		}
		else
		{

			verifyExists(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Verify AccessTypeDropdownAccess");
			click(SiebelModeObj.AEndSiteObj.AccessTypeDropdownAccess,"Click on AccessTypeDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Verify AccessTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.AccessTypeSelectAccess,"Click on AccessTypeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();


			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Verify AccesstechnologyDropdownAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologyDropdownAccess,"Click on AccesstechnologyDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Verify AccesstechnologySelectAccess");
			click(SiebelModeObj.IPAccessObj.AccesstechnologySelectAccess,"Click on AccesstechnologySelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Verify BuildingTypeDropdownAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeDropdownAccess,"Click on BuildingTypeDropdownAccess");
			Reusable.waitForSiebelSpinnerToDisappear();
			verifyExists(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Verify BuildingTypeSelectAccess");
			click(SiebelModeObj.IPAccessObj.BuildingTypeSelectAccess,"Click on BuildingTypeSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Verify CustomerSitePopStatusDropdownAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusDropdownAccess,"Click on CustomerSitePopStatusDropdownAccess");
			verifyExists(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Verify CustomerSitePopStatusSelectAccess");
			click(SiebelModeObj.IPAccessObj.CustomerSitePopStatusSelectAccess,"Click on CustomerSitePopStatusSelectAccess");
			Reusable.waitForSiebelSpinnerToDisappear();

			verifyExists(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Verify BCPReference");
			click(SiebelModeObj.EthernetVPNAccessObj.BCPReference,"Click on BCPReference");
			sendKeys(SiebelModeObj.EthernetVPNAccessObj.BCPReference,BCPReference);
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"ClickheretoSaveAccess");
			click(SiebelModeObj.IPVPNServicePlusAccess.ClickheretoSaveAccess,"Click on save");
			Reusable.waitForSiebelLoader();

		}
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Verify CabinetTypeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.CabinetTypeDropdownAccess,"Click on CabinetTypeDropdownAccess");
		verifyExists(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Verify CabinetTypeSelectAccess");
		click(SiebelModeObj.IPAccessObj.CabinetTypeSelectAccess,"Click on CabinetTypeSelectAccess");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.IPAccessObj.CabinetID,"Verify CabinetID");
		sendKeys(SiebelModeObj.IPAccessObj.CabinetID,CabinetID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.CabinetID, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.IPAccessObj.shelfid,"Verify ShelfID");
		sendKeys(SiebelModeObj.IPAccessObj.shelfid,ShelfID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.shelfid, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		//	verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
		//	click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");

		Reusable.savePage();

		verifyExists(SiebelModeObj.IPAccessObj.Slotid,"Verify Slotid");
		sendKeys(SiebelModeObj.IPAccessObj.Slotid,SlotID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Slotid, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.Physicalportid,"Verify Physicalportid");
		sendKeys(SiebelModeObj.IPAccessObj.Physicalportid,Physical_PortID);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.Physicalportid, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.PresentationInterfaceinput,"Verify PresentationInterfaceinput");
		sendKeys(SiebelModeObj.IPAccessObj.PresentationInterfaceinput,PresentationInterface);
		Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.PresentationInterfaceinput, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Verify ConnectorTypeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.ConnectorTypeDropdownAccess,"Click on ConnectorTypeDropdownAccess");

		verifyExists(SiebelModeObj.EthernetVPNAccessObj.ConnectorTypeSelect,"Verify ConnectorTypeSelect");
		click(SiebelModeObj.EthernetVPNAccessObj.ConnectorTypeSelect,"Click on ConnectorTypeSelect");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Verify FibreTypeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.FibreTypeDropdownAccess,"Click on FibreTypeDropdownAccess");

		verifyExists(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
		click(SiebelModeObj.IPAccessObj.FibreTypeSelectAccess.replace("Value",FibreType));
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Verify PortRoleDropDown");
		click(SiebelModeObj.IPAccessObj.PortRoleDropDown,"Click on PortRoleDropDown");

		verifyExists(SiebelModeObj.IPAccessObj.PortValue,"Verify PortValue");
		click(SiebelModeObj.IPAccessObj.PortValue,"Click on PortValue");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Verify InstallTimeDropdownAccess");
		click(SiebelModeObj.IPAccessObj.InstallTimeDropdownAccess,"Click on InstallTimeDropdownAccess");
		verifyExists(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Verify InstallTimeSelectAccess");
		click(SiebelModeObj.IPAccessObj.InstallTimeSelectAccess,"Click on InstallTimeSelectAccess");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Verify ClickheretoSaveAccess");
		click(SiebelModeObj.VoiceLineVObj.ClickheretoSaveAccess,"Click On ClickheretoSaveAccess");
		Reusable.waitForSiebelLoader();


		addNetwork();
		verifyExists(SiebelModeObj.Site.SiteTab,"SiteTab");
		click(SiebelModeObj.Site.SiteTab,"SiteTab");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Verify RouterSiteNameDropdownAccess");
		click(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess,"Click on RouterSiteNameDropdownAccess");
		verifyExists(SiebelModeObj.EthernetVPNAccessObj.sitenamevaluevpn,"Verify sitenamevaluevpn");
		click(SiebelModeObj.EthernetVPNAccessObj.sitenamevaluevpn,"Click on sitenamevaluevpn");
		//	Reusable.SendkeaboardKeys(SiebelModeObj.IPAccessObj.RouterSiteNameDropdownAccess, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		Reusable.waitForAjax();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Show full info A");
		click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoA,"Click on Show full info A");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
		click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
		click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
		click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();


		/*	

	Random rand = new Random();
	String siteA = null;
	String siteB = null;
	String CircuitReference_Value =null;

	verifyExists(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Verify CircuitReferenceAccess");
	click(SiebelModeObj.IPAccessObj.CircuitReferenceAccess,"Click On CircuitReferenceAccess");

//	String CircuitReference_Value = getTextFrom((SiebelModeObj.IPAccessObj.CircuitReferenceValue),"Value");
	CircuitReference_Value = getAttributeFrom((SiebelModeObj.IPAccessObj.CircuitReferenceValue),"Value");



	if(CircuitReference_Value.isEmpty()==true)
	{
		if(isElementPresent(By.xpath("//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site A address"))
		{

			     String siteA1 = getAttributeFrom("@xpath=//*[@id='colt-site-left']//*[@class='premise-master-link colt-popup-link-ctrl']","title");     
			     siteA=siteA1.substring(0, 3);
		}
		if(isElementPresent(By.xpath("//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']"),"Site B address"))
		{
		     String siteB1 = getAttributeFrom("@xpath=//*[@id='colt-site-right']//*[@class='premise-master-link colt-popup-link-ctrl']","title");

		     siteB=siteB1.substring(0, 3);
		}


		if (siteA != null && siteB!= null)
		{
			int randnumb = rand.nextInt(900000) + 100000;
			CircuitReference_Value = siteA+"/"+siteB+"/"+"LE-"+randnumb;
		}else{
			if(siteA != null)
			{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitReference_Value = siteA+"/"+siteA+"/"+"LE-"+randnumb;
			}else{
				int randnumb = rand.nextInt(900000) + 100000;
				CircuitReference_Value = siteB+"/"+siteB+"/"+"LE-"+randnumb;
			}

		}
		sendKeys(SiebelAddProdcutObj.CircuitReferenceGeneration.CircuitReferenceValue, CircuitReference_Value,"Circuit Reference Number");		
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
	}
	Reusable.savePage();
	DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Circuitreference_Number",CircuitReference_Value);	

		 */	
		Reusable.waitForSiebelLoader();
		break;
	}

	////0306
	case "Interconnect": 

	{
		String BEndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
		String Coverage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Coverage");
		String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
		String IPGuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
		String IncludeColtTechnicalContactinTestCalls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
		String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String SiteContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Contact");
		String OrderReceivedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Received_Date");
		String ColtActualDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
		String Contractterm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_term");
		String OrderSignedDate = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
		String AendCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
		String AendCityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");


		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountry,"VoiceServiceCountry");
		click(SiebelModeObj.Interconnect.VoiceServiceCountry,"Click on voice service country");

		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", BEndResilienceOption),BEndResilienceOption);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", BEndResilienceOption),BEndResilienceOption);
		System.out.println("go to if loop of call admission control");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.CallAdmissionControl,"CallAdmissionControl");
		sendKeys(SiebelModeObj.Interconnect.CallAdmissionControl,"8", "Enter call admission control");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.CallAdmissionControl, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.NumberOfSignallingTrunks,"CallAdmissionControl");
		sendKeys(SiebelModeObj.Interconnect.NumberOfSignallingTrunks,"4", "Enter call admission control");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.NumberOfSignallingTrunks, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.VoiceConfigurationReference,"CallAdmissionControl");
		sendKeys(SiebelModeObj.Interconnect.VoiceConfigurationReference,"yes", "Enter call admission control");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.VoiceConfigurationReference, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.TrafficDirection,"TrafficDirection");
		click(SiebelModeObj.Interconnect.TrafficDirection,"Click on TrafficDirection");
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", Coverage),Coverage);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value", Coverage));
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Incoming DDI Digits"),"Incoming DDI Digits Drop down");
		click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Incoming DDI Digits"),"Incoming DDI Digits Drop down");
		verifyExists(SiebelModeObj.Interconnect.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);
		click(SiebelModeObj.Interconnect.SelectValueDropdown.replace("Value", AlertingNotificationEmailAddress),AlertingNotificationEmailAddress);

		verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "First Codec"),"First Codec drop down");
		click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "First Codec"),"First Codec drop down");
		verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value", IPGuardianVariant),IPGuardianVariant);
		click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value", IPGuardianVariant),IPGuardianVariant);

		verifyExists(SiebelModeObj.Interconnect.ClickLink.replace("Value", "Show More"),"Show More link");
		click(SiebelModeObj.Interconnect.ClickLink.replace("Value", "Show More"),"Show More link");

		verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Second Codec"),"Second Codec drop down");
		click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Second Codec"),"Second Codec drop down");
		verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IncludeColtTechnicalContactinTestCalls ),IncludeColtTechnicalContactinTestCalls);
		click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IncludeColtTechnicalContactinTestCalls ),IncludeColtTechnicalContactinTestCalls);

		verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Third Codec"),"Third Codec drop down");
		click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Third Codec"),"Third Codec drop down");
		verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",ServiceBandwidth ),ServiceBandwidth);
		click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",ServiceBandwidth ),ServiceBandwidth);


		verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fourth Codec"),"fourth Codec drop down");
		click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fourth Codec"),"fourth Codec drop down");
		verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",TestWindow),TestWindow);
		click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",TestWindow),TestWindow);

		verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fifth Codec"),"fifth Codec drop down");
		click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Fifth Codec"),"fifth Codec drop down");
		verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",CircuitIPAddressInput),CircuitIPAddressInput);
		click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",CircuitIPAddressInput),CircuitIPAddressInput);

		verifyExists(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Sixth Codec"),"Sixth Codec drop down");
		click(SiebelModeObj.Interconnect.ClickDropdown.replace("Value", "Sixth Codec"),"Sixth Codec drop down");
		verifyExists(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPRange),IPRange);
		click(SiebelModeObj.Interconnect.SiteABSelection.replace("Value",IPRange),IPRange);

		verifyExists(SiebelModeObj.Interconnect.SaveButton,"Save Button");
		click(SiebelModeObj.Interconnect.SaveButton,"save button");
		Reusable.waitForSiebelLoader();



		verifyExists(SiebelModeObj.Interconnect.ClickToShowFullInfo,"Click To Show Full Info");
		click(SiebelModeObj.Interconnect.ClickToShowFullInfo,"click to show full info");
		Reusable.waitForSiebelLoader();


		verifyExists(SiebelModeObj.Interconnect.SiteContact,"Site Contact");
		click(SiebelModeObj.Interconnect.SiteContact,"site contact");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Interconnect.OkButton,"Ok Button");
		click(SiebelModeObj.Interconnect.OkButton,"Click on ok button of site contact");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.ServiceParty,"Service Party");
		click(SiebelModeObj.Interconnect.ServiceParty,"Click on service party");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Interconnect.OkButtonService,"OkButtonService");
		click(SiebelModeObj.Interconnect.OkButtonService,"Click on ok button of service party");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.DelieveryTeamCountry,"Delievery Team Country drop down");
		click(SiebelModeObj.Interconnect.DelieveryTeamCountry,"Delievery Team Country drop down");
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",SiteContact),SiteContact);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",SiteContact),SiteContact);
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.Interconnect.DeliveryTeamCity,"Delivery Team City");
		sendKeys(SiebelModeObj.Interconnect.DeliveryTeamCity,"Shanghai", "Delivery team city");
		Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.DeliveryTeamCity, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.CrossButtonTrunk,"CrossButtonTrunk");
		click(SiebelModeObj.Interconnect.CrossButtonTrunk,"Click on cross button");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
		click(SiebelModeObj.Interconnect.SaveButton,"Click on SaveButton");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.Interconnect.TrunkSequence,"Trunk Sequence");
		sendKeys(SiebelModeObj.Interconnect.TrunkSequence,"1", "trunk sequence");


		verifyExists(SiebelModeObj.Interconnect.TrunkName,"Trunk Name");
		sendKeys(SiebelModeObj.Interconnect.TrunkName,"Trunk1", "Enter value in trunk name");
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.AccessLineType,"AccessLineType");
		click(SiebelModeObj.Interconnect.AccessLineType,"Click on access line type");	
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderSignedDate),OrderSignedDate);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderSignedDate),OrderSignedDate);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.Interconnect.AccessServiceID,"AccessServiceID");
		click(SiebelModeObj.Interconnect.AccessServiceID,"Click on access service id");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.Interconnect.OkButtonAccess,"OkButtonAccess");
		click(SiebelModeObj.Interconnect.OkButtonAccess,"Click on ok button");

		Reusable.waitForSiebelLoader();				

		if(isElementPresent(By.xpath("//a[text()='Show full info']")))	
		{								
			System.out.println("full trunk info is available");
			verifyExists(SiebelModeObj.Interconnect.ShowFullInfoTrunk,"OkButtonAccess");
			click(SiebelModeObj.Interconnect.ShowFullInfoTrunk,"Click on ShowFullInfoTrunk");
			Reusable.waitForSiebelLoader();
		}

		verifyExists(SiebelModeObj.Interconnect.AddButtonTrunk,"AddButtonTrunk");
		click(SiebelModeObj.Interconnect.AddButtonTrunk,"Click on plus button");


		verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP4Address,"CustomerOriginatingIP4Address");
		sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP4Address,"192.168.1.1", "value in customer originating IPV4 address");

		verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"CustomerOriginatingIP6Address");
		sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"192.168.1.1", "value in customer originating IPV6 address");

		verifyExists(SiebelModeObj.Interconnect.CustomerTerminatingIP4Address,"CustomerTerminatingIP4Address");
		sendKeys(SiebelModeObj.Interconnect.CustomerTerminatingIP4Address,"38.168.1.1", "value in customer originating IPV4 address");

		verifyExists(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"CustomerOriginatingIP6Address");
		sendKeys(SiebelModeObj.Interconnect.CustomerOriginatingIP6Address,"38.168.1.1", "value in customer originating IPV6 address");

		verifyExists(SiebelModeObj.Interconnect.SignallingTransportProtocol,"SignallingTransportProtocol");
		click(SiebelModeObj.Interconnect.SignallingTransportProtocol,"Click on signalling transport protocol");
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderReceivedDate),OrderReceivedDate);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",OrderReceivedDate),OrderReceivedDate);

		verifyExists(SiebelModeObj.Interconnect.VoipProtocol,"VoipProtocol");
		click(SiebelModeObj.Interconnect.VoipProtocol,"Click on voip protocol");
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",ColtActualDate),ColtActualDate);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",ColtActualDate),ColtActualDate);

		verifyExists(SiebelModeObj.Interconnect.EncryptionProtocol,"EncryptionProtocol");
		click(SiebelModeObj.Interconnect.EncryptionProtocol,"Click on encryption protocol");
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",Contractterm),Contractterm);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",Contractterm),Contractterm);

		verifyExists(SiebelModeObj.Interconnect.NATRequired,"NATRequired");
		sendKeys(SiebelModeObj.Interconnect.NATRequired,"N", "Enter value in NATR");
		Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.NATRequired, Keys.TAB);

		verifyExists(SiebelModeObj.Interconnect.InboundOutboundSplit,"InboundOutboundSplit");
		sendKeys(SiebelModeObj.Interconnect.InboundOutboundSplit,"No", "Enter value in InboundOutboundSplit field");
		Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.InboundOutboundSplit, Keys.TAB);

		verifyExists(SiebelModeObj.Interconnect.TypeOfCAC,"TypeOfCAC");
		click(SiebelModeObj.Interconnect.TypeOfCAC,"TypeOfCAC");
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCountry),AendCountry);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCountry),AendCountry);

		verifyExists(SiebelModeObj.Interconnect.CrossButtonTrunk,"CrossButtonTrunk");
		click(SiebelModeObj.Interconnect.CrossButtonTrunk,"Click on cross button");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
		click(SiebelModeObj.Interconnect.SaveButton,"Click on Save Button");
		Reusable.waitForSiebelLoader();
		Reusable.savePage();

		verifyExists(SiebelModeObj.Interconnect.VoiceTab,"VoiceTab");
		click(SiebelModeObj.Interconnect.VoiceTab,"Click on voice feature tab");
		Reusable.waitForSiebelLoader();
		// 			waitToPageLoad();

		verifyExists(SiebelModeObj.Interconnect.Language,"Language");
		sendKeys(SiebelModeObj.Interconnect.Language,"English", "Select language");
		Reusable.SendkeaboardKeys(SiebelModeObj.Interconnect.Language, Keys.TAB);

		verifyExists(SiebelModeObj.Interconnect.Resillence,"resillence dropdown");
		click(SiebelModeObj.Interconnect.Resillence,"Click on resillence dropdown");
		verifyExists(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCityTown),AendCityTown);
		click(SiebelModeObj.Interconnect.VoiceServiceCountryValue.replace("Value",AendCityTown),AendCityTown);
		Reusable.waitForSiebelSpinnerToDisappear();

		verifyExists(SiebelModeObj.Interconnect.SaveButton,"SaveButton");
		click(SiebelModeObj.Interconnect.SaveButton,"Click on SaveButton");
		Reusable.waitForSiebelLoader();
		//	waitToPageLoad();
		try {
			verifyExists(SiebelModeObj.Interconnect.ServiceGroupTab,"ServiceGroupTab");
			click(SiebelModeObj.Interconnect.ServiceGroupTab,"Click on service tab");
		}
		catch(Exception ex) {
			Reusable.Select1(SiebelModeObj.Interconnect.InstalltionDropdown,"Service Group");
		}

		Reusable.waitForSiebelLoader();
		//		waitToPageLoad();

		verifyExists(SiebelModeObj.Interconnect.ClickOnPlusButtonService,"ServiceGroupReference");
		click(SiebelModeObj.Interconnect.ClickOnPlusButtonService,"Click on plus button");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.Interconnect.ServiceGroupReference,"ServiceGroupReference");
		click(SiebelModeObj.Interconnect.ServiceGroupReference,"Click on ServiceGroupReference button");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.Interconnect.ClickOkButton,"ClickOkButton");
		click(SiebelModeObj.Interconnect.ClickOkButton,"Click on ok button");
		Reusable.waitForSiebelLoader();

		break;

	}
	case "IP Domain": {

		String DomainName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Domain_Name");


		System.out.println("Enter Middle Applet");

		System.out.println("Move Performed");

		verifyExists(SiebelModeObj.IPDomain.Clicktoshowfullinfomiddle,"Verify Clicktoshowfullinfomiddle");
		click(SiebelModeObj.IPDomain.Clicktoshowfullinfomiddle,"Click onClicktoshowfullinfomiddle");
		verifyExists(SiebelModeObj.IPDomain.Domainname," Verify SearchInput");
		sendKeys(SiebelModeObj.IPDomain.Domainname,"1","DomainName");
		verifyExists(SiebelModeObj.IPDomain.DomainordertypeData,"Verify DomainordertypeData");
		click(SiebelModeObj.IPDomain.DomainordertypeData,"Click on DomainordertypeData");
		verifyExists(SiebelModeObj.IPDomain.DNStype,"Verify DNStype");
		click(SiebelModeObj.IPDomain.DNStype,"Click on DNStype");
		verifyExists(SiebelModeObj.IPDomain.DNSTypeInput,"Verify DNSTypeInput");
		click(SiebelModeObj.IPDomain.DNSTypeInput,"Click on DNSTypeInput");

		verifyExists(SiebelModeObj.IPDomain.Crossbutton,"Verify Crossbutton");
		click(SiebelModeObj.IPDomain.Crossbutton,"Click on Crossbutton");
		verifyExists(SiebelModeObj.IPDomain.SaveButtonClick,"Verify SaveButtonClick");
		click(SiebelModeObj.IPDomain.SaveButtonClick,"Click on SaveButtonClick");
		System.out.println("Middle Complete");
		break;
	}
	case "IP Guardian": {

		String AlertingNotificationEmailAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
		String AutoMitigation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");
		String CustomerDNSResolvers = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_DNS_Resolvers");
		String IPGuardianVariant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
		String IncludeColtTechnicalContactinTestCalls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
		String TestWindow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
		String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");

		System.out.println("Enter Gurdian Middle Applet");

		verifyExists(SiebelModeObj.IPGuardian.AlertingNotification," Verify AlertingNotification");
		sendKeys(SiebelModeObj.IPGuardian.AlertingNotification,AlertingNotificationEmailAddress,"Enter AlertingNotification");

		verifyExists(SiebelModeObj.IPGuardian.Automigration," Verify AutoMitigation");
		sendKeys(SiebelModeObj.IPGuardian.Automigration,AutoMitigation,"Enter AutoMitigation");

		verifyExists(SiebelModeObj.IPGuardian.Customerdnsresolve," Verify Customerdnsresolve");
		sendKeys(SiebelModeObj.IPGuardian.Customerdnsresolve,CustomerDNSResolvers,"Enter Customer dns resolve");

		verifyExists(SiebelModeObj.IPGuardian.IpguardianVariant," Verify IpguardianVariant");
		sendKeys(SiebelModeObj.IPGuardian.IpguardianVariant,IPGuardianVariant,"Enter IP guardiant variant");
		verifyExists(SiebelModeObj.IPGuardian.Colttechnicaltestcalls," Verify Colttechnicaltestcalls");
		sendKeys(SiebelModeObj.IPGuardian.Colttechnicaltestcalls,IncludeColtTechnicalContactinTestCalls,"Enter Enter Colt technical cells");

		verifyExists(SiebelModeObj.IPGuardian.servicebandwidth," Verify servicebandwidth");
		sendKeys(SiebelModeObj.IPGuardian.servicebandwidth,ServiceBandwidth,"Enter service bandwidth");

		verifyExists(SiebelModeObj.IPGuardian.Testwindowipaccess," Verify servicebandwidth");
		sendKeys(SiebelModeObj.IPGuardian.Testwindowipaccess,TestWindow,"Enter Test Window");

		//verifyExists(SiebelModeObj.IPGuardian.IpGurdianSave," Verify IpGurdianSave");
		//click(SiebelModeObj.IPGuardian.IpGurdianSave,"Click on IpGurdianSave");

		Reusable.waitForSiebelLoader();
		//waitToPageLoad();

		mouseMoveOn(SiebelModeObj.IPGuardian.CircuitipaddressClick);

		verifyExists(SiebelModeObj.IPGuardian.CircuitipaddressClick," Verify CircuitipaddressClick");
		click(SiebelModeObj.IPGuardian.CircuitipaddressClick,"click on circuit ip address");

		verifyExists(SiebelModeObj.IPGuardian.CircuitIPAddressInput," Verify CircuitIPAddressInput");
		click(SiebelModeObj.IPGuardian.CircuitIPAddressInput,"click on circuit ip address");

		verifyExists(SiebelModeObj.IPGuardian.CircuitIPAddressInput," Verify CircuitIPAddressInput");
		sendKeys(SiebelModeObj.IPGuardian.CircuitIPAddressInput,CircuitIPAddressInput,"Input IP address");

		verifyExists(SiebelModeObj.IPGuardian.IpRange," Verify IpRange");
		sendKeys(SiebelModeObj.IPGuardian.IpRange,IPRange,"Input IP Range");
		verifyExists(SiebelModeObj.IPGuardian.CrossButtonGurdian," Verify CrossButtonGurdian");
		click(SiebelModeObj.IPGuardian.CrossButtonGurdian,"click on CrossButtonGurdian");

		verifyExists(SiebelModeObj.IPGuardian.IpGurdianSave," Verify IpGurdianSave");
		click(SiebelModeObj.IPGuardian.IpGurdianSave,"click on IpGurdianSave");

		break;

	}
	case "Number Hosting":
	{

		String B_End_Resilience_Option = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Resilience_Option");
		String Coverage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Coverage");
		String Alerting_Notification_Email_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
		String IP_Guardian_Variant = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Guardian_Variant");
		String Include_Colt_Technical_Contact_in_Test_Calls = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Include_Colt_Technical_Contact_in_Test_Calls");
		String Service_Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		String Test_Window = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Test_Window");
		String CircuitIPAddressInput = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitIPAddressInput");
		String IP_Range = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
		String Site_Contact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Contact");
		String Order_Signed_Date = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Signed_Date");
		String Order_Received_Date = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Received_Date");
		String Colt_Actual_Date = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Actual_Date");
		String Contract_term = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_term");
		String Aend_Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
		String Aend_CityTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");

		verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountry,"voice service country");
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountry,"voice service country");
		verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", B_End_Resilience_Option));
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", B_End_Resilience_Option));

		verifyExists(SiebelModeObj.NumberHostingObj.CallAdmissionControl,"8");
		sendKeys(SiebelModeObj.NumberHostingObj.CallAdmissionControl,"8");
		Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.CallAdmissionControl, Keys.TAB);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks,"4");
		sendKeys(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks,"4");
		Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.NumberOfSignallingTrunks, Keys.TAB);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference,"yes");
		sendKeys(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference,"yes");
		Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.VoiceConfigurationReference, Keys.TAB);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.TrafficDirection,"Traffic Direction");
		click(SiebelModeObj.NumberHostingObj.TrafficDirection,"Traffic Direction");
		verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Coverage));
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Coverage));

		//R5 Coverage Updating Code From Excel
		click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Incoming DDI Digits"));
		click(SiebelModeObj.NumberHostingObj.SelectValueDropdown.replace("Value", Alerting_Notification_Email_Address));

		click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "First Codec"));
		click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", IP_Guardian_Variant));

		click(SiebelModeObj.NumberHostingObj.ClickLink.replace("Value", "Show More"));

		click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Second Codec"));
		click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Include_Colt_Technical_Contact_in_Test_Calls));

		click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Third Codec"));
		click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Service_Bandwidth));
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fourth Codec"));
		click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fourth Codec"));
		verifyExists(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Test_Window));
		click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", Test_Window));

		click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Fifth Codec"));
		click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value", CircuitIPAddressInput));

		click(SiebelModeObj.NumberHostingObj.ClickDropdown.replace("Value", "Sixth Codec"));
		click(SiebelModeObj.NumberHostingObj.SiteABSelection.replace("Value",IP_Range));

		verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"Save Button");
		click(SiebelModeObj.NumberHostingObj.SaveButton,"Save Button");
		waitToPageLoad();

		verifyExists(SiebelModeObj.NumberHostingObj.ShowMore,"Show More");
		click(SiebelModeObj.NumberHostingObj.ShowMore,"Show More");
		waitToPageLoad();

		verifyExists(SiebelModeObj.NumberHostingObj.ClickToShowFullInfo,"click to show full info");
		click(SiebelModeObj.NumberHostingObj.ClickToShowFullInfo,"click to show full info");

		verifyExists(SiebelModeObj.NumberHostingObj.SiteContact,"Site Contact");
		click(SiebelModeObj.NumberHostingObj.SiteContact,"Site Contact");
		waitToPageLoad();
		verifyExists(SiebelModeObj.NumberHostingObj.OkButton,"Ok Button");
		click(SiebelModeObj.NumberHostingObj.OkButton,"Ok Button");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.ServiceParty,"Service Party");
		click(SiebelModeObj.NumberHostingObj.ServiceParty,"Service Party");
		waitToPageLoad();
		verifyExists(SiebelModeObj.NumberHostingObj.OkButtonService,"Ok Button Service");
		click(SiebelModeObj.NumberHostingObj.OkButtonService,"Ok Button Service");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.DelieveryTeamCountry,"DelieveryTeamCountry");
		click(SiebelModeObj.NumberHostingObj.DelieveryTeamCountry,"DelieveryTeamCountry");
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Site_Contact));
		waitToPageLoad();

		sendKeys(SiebelModeObj.NumberHostingObj.DeliveryTeamCity,"Shanghai");
		Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.DeliveryTeamCity, Keys.TAB);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"cross button");
		click(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"cross button");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"Save button");
		click(SiebelModeObj.NumberHostingObj.SaveButton,"Save button");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.TrunkConfiguration,"Trunk Configuration");
		sendKeys(SiebelModeObj.NumberHostingObj.TrunkSequence,"1");
		sendKeys(SiebelModeObj.NumberHostingObj.TrunkName,"Trunk1");

		verifyExists(SiebelModeObj.NumberHostingObj.AccessLineType,"AccessLine Type");
		click(SiebelModeObj.NumberHostingObj.AccessLineType,"AccessLine Type");
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value",Order_Signed_Date));
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		//Popup loading takeing time

		verifyExists(SiebelModeObj.NumberHostingObj.AccessServiceID,"AccessServiceID");
		click(SiebelModeObj.NumberHostingObj.AccessServiceID,"AccessServiceID");

		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.NumberHostingObj.OkButtonAccess,"OkButtonAccess");
		click(SiebelModeObj.NumberHostingObj.OkButtonAccess,"OkButtonAccess");

		//Used in SIP Trunking//
		/*
		verifyExists(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup,"Verify AccessServiceIdlookup");
		click(SiebelModeObj.SIPTrunkingObj.AccessServiceIdlookup);

		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Verify SearchServiceIDbox");
		click(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SearchServiceIDbox,"Product");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchText,"Verify SearchText");
		click(SiebelModeObj.SIPTrunkingObj.SearchText);
		sendKeys(SiebelModeObj.SIPTrunkingObj.SearchText,"IP Access");

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess,"Verify SearchButtonProcess");
		click(SiebelModeObj.SIPTrunkingObj.SearchButtonProcess);
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton,"Verify SearchServiceIdOKButton");
		click(SiebelModeObj.SIPTrunkingObj.SearchServiceIdOKButton);
		Reusable.waitForSiebelLoader();
		 */
		////////////////////////////////////////

		if (isElementPresent(By.xpath("//a[text()='Show full info']")))
		{
			verifyExists(SiebelModeObj.NumberHostingObj.ShowFullInfoTrunk,"ShowFullInfoTrunk");
			click(SiebelModeObj.NumberHostingObj.ShowFullInfoTrunk,"ShowFullInfoTrunk");

		}
		verifyExists(SiebelModeObj.NumberHostingObj.AddButtonTrunk,"AddButtonTrunk");
		click(SiebelModeObj.NumberHostingObj.AddButtonTrunk,"AddButtonTrunk");

		verifyExists(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP4Address,"CustomerOriginatingIP4Address");
		sendKeys(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP4Address,"192.168.1.1");
		sendKeys(SiebelModeObj.NumberHostingObj.CustomerOriginatingIP6Address,"192.18.1.2");

		sendKeys(SiebelModeObj.NumberHostingObj.CustomerTerminatingIP4Address,"38.68.1.1");
		sendKeys(SiebelModeObj.NumberHostingObj.CustomerTerminatingIP6Address,"38.168.1.1");

		verifyExists(SiebelModeObj.NumberHostingObj.SignallingTransportProtocol,"SignallingTransportProtocol");
		click(SiebelModeObj.NumberHostingObj.SignallingTransportProtocol,"SignallingTransportProtocol");
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Order_Received_Date));

		verifyExists(SiebelModeObj.NumberHostingObj.VoipProtocol,"VoipProtocol");
		click(SiebelModeObj.NumberHostingObj.VoipProtocol,"VoipProtocol");
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Colt_Actual_Date));

		verifyExists(SiebelModeObj.NumberHostingObj.EncryptionProtocol,"EncryptionProtocol");
		click(SiebelModeObj.NumberHostingObj.EncryptionProtocol,"EncryptionProtocol");
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Contract_term));

		sendKeys(SiebelModeObj.NumberHostingObj.NATRequired,"N");
		Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.NATRequired, Keys.TAB);

		verifyExists(SiebelModeObj.NumberHostingObj.InboundOutboundSplit,"InboundOutboundSplit");
		sendKeys(SiebelModeObj.NumberHostingObj.InboundOutboundSplit,"No");
		Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.InboundOutboundSplit, Keys.TAB);

		verifyExists(SiebelModeObj.NumberHostingObj.TypeOfCAC,"TypeOfCAC");
		click(SiebelModeObj.NumberHostingObj.TypeOfCAC,"TypeOfCAC");
		verifyExists(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_Country));
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_Country));

		verifyExists(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"CrossButtonTrunk");
		click(SiebelModeObj.NumberHostingObj.CrossButtonTrunk,"CrossButtonTrunk");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
		click(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		click(SiebelModeObj.NumberHostingObj.OtherTabClick,"OtherTabClick");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		sendKeys(SiebelModeObj.NumberHostingObj.TextInput.replace("Value", "Internal Routing Prefix"),"45");
		sendKeys(SiebelModeObj.NumberHostingObj.TextInput.replace("Value", "Reseller Profile"),"colt");

		verifyExists(SiebelModeObj.NumberHostingObj.VoiceTab,"VoiceTab");
		click(SiebelModeObj.NumberHostingObj.VoiceTab,"VoiceTab");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		sendKeys(SiebelModeObj.NumberHostingObj.Language,"English");
		Reusable.SendkeaboardKeys(SiebelModeObj.NumberHostingObj.Language, Keys.TAB);

		verifyExists(SiebelModeObj.NumberHostingObj.Resillence,"Resillence");
		click(SiebelModeObj.NumberHostingObj.Resillence,"Resillence");
		click(SiebelModeObj.NumberHostingObj.VoiceServiceCountryValue.replace("Value", Aend_CityTown));

		verifyExists(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
		click(SiebelModeObj.NumberHostingObj.SaveButton,"SaveButton");
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		try
		{
			verifyExists(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");
			click(SiebelModeObj.NumberHostingObj.ServiceGroupTab,"ServiceGroupTab");

		}
		catch(Exception ex)
		{
			verifyExists(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"InstalltionDropdown");
			Reusable.Select1(SiebelModeObj.NumberHostingObj.InstalltionDropdown,"Service Group");

		}

		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.ClickOnPlusButtonService,"ClickOnPlusButtonService");
		click(SiebelModeObj.NumberHostingObj.ClickOnPlusButtonService,"ClickOnPlusButtonService");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.ServiceGroupReference,"ServiceGroupReference");
		click(SiebelModeObj.NumberHostingObj.ServiceGroupReference,"ServiceGroupReference");
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelModeObj.NumberHostingObj.ClickOkButton,"ClickOkButton");
		click(SiebelModeObj.NumberHostingObj.ClickOkButton,"ClickOkButton");
		Reusable.waitForSiebelLoader();
		break;

	}



	default:{
		System.out.println("Above product is not exist in current list");
		break;
	}
	}

}
	
	public void OpenServiceOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		
		String ServiceOrderReference_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Completed_Service_order");
		Reusable.waitForSiebelLoader();
		
		try {
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		} catch (Exception e) {
			try {
				javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} catch (Exception e1) {

				e1.printStackTrace();
			}
		}
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderReference_Number,"Service OrderReference Number");
		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
		click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Click Service Order Go");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo, "Verify ServiceOrderReferenceNo");
		click(SiebelCeaseObj.CeaseMainMethodObj.ServiceOrderReferenceNo, "Click on ServiceOrderReferenceNo");
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		}

}







