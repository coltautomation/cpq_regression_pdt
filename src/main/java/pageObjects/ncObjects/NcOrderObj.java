package pageObjects.ncObjects;

public class NcOrderObj {
	
	public static class searchOrder
	{ 
		public static final String NameFiltering="@xpath=//a[contains(text(),'Name')]/parent::*/following-sibling::*";
		public static final String FilterSelectName="@xpath=//*[@class='inputs']";
		public static final String FilterInputValue="@xpath=//*[@name='inp']";
		public static final String ApplyButton="@xpath=//*[@class='FilterButtons'] //*[text()='Apply']";
	}
	
	public static class createOrder
	{ 
		public static final String NewCompositeOrder="@xpath=//a[text()='New Composite Order']";
		public static final String OrderNumber="@xpath=//td[@id='vv_-1']/span";
		public static final String LinkforOrder="@xpath//a[contains(text(),'Order #')]";
		public static final String OrderDescription="@xpath=//*[@name='common_descr']";
		public static final String ProductID="@xpath=//td/a[text()='Product ID']/parent::*/following-sibling::*";
		public static final String Update="@xpath=//a[text()='Update']";
	}
	
	public static class addProduct
	 { 
		public static final String OrderTab="@xpath=//a[text()='Orders']";
		public static final String AddonOrderTab="@xpath=//a[text()='Add']";
		public static final String Productifram="@xpath=//iframe[@id='linkage']";
		public static final String Featureframe="@xpath=//html/frameset/frame[1]";
		public static final String FeatureTitle="@xpath=//a[contains(text(),'IP Access Product')]";
		public static final String Framename="@xpath=//div[@class='objInfo']";
		public static final String ServiceInfoTab="@xpath=//a[text()='Service Information']";
		public static final String OrderLinkTab="@xpath=//a[text()='Order Links']";
		public static final String SpecificInfoTab="@xpath=//a[text()='Specification Information']";
		public static final String EthernetProductCheckBox="@xpath=//a[text()='Ethernet Connection Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String HubProductCheckBox="@xpath=//a[text()='Hub Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String IpAccessCheckBox="@xpath=//a[text()='IP Access Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String IpvpnserviceCheckBox="@xpath=//a[text()='IP VPN Service Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String IpvpnSiteCheckBox="@xpath=//a[text()='IP VPN Site Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String IpVpnEndPointProdCheckBox="@xpath=//a[text()='VPN Endpoint Product']/parent::*/parent::*/parent::*/parent::*/td//input";
		public static final String Addbutton="@xpath=//button[text()='Add']";
	 }
	
	public static class addProductDetails
	 { 
		public static final String UnderlyingIpOrder="@xpath=//span[contains(text(),'IP Access')]/parent::a"; 
		public static final String UnderlyingEthernetOrder="@xpath=//span[contains(text(),'Ethernet Connection')]/parent::a";
		public static final String UnderLyingEndSiteOrder="@xpath=//span[contains(text(),'End Site')]/parent::a";
		public static final String Edit="@xpath=//a[text()='Edit']";
		public static final String OrderSystemName="@xpath=//td/a[text()='Order System']/parent::*/following-sibling::*//select";
		public static final String Orderreferencenumber="@xpath=//td/a[text()='Order System Service ID']/parent::*/following-sibling::*//input";
		public static final String Ordernumber="@xpath=//td/a[text()='Order Number']/parent::*/following-sibling::*//input";
		public static final String CommercialProductName="@xpath=//td/a[text()='Commercial Product Name']/parent::*/following-sibling::*//input";
		public static final String Topology="@xpath=//td/a[text()='Topology']/parent::*/following-sibling::*//select";
		public static final String RouterType="@xpath=//td/a[text()='Router Type']/parent::*/following-sibling::*//select";
		public static final String RouterTechnology="@xpath=//td/a[text()='Router Technology']/parent::*/following-sibling::*//select";
		public static final String ServiceBandwidth="@xpath=//td/a[text()='Service Bandwidth']/parent::*/following-sibling::*//select";
		public static final String ServiceBand="@xpath=//td/a[contains(text(),'Service Bandwidth')]/parent::*/following-sibling::*//td/div/div/input";
		public static final String CircuitCategory="@xpath=//tr/td/a[text()='Circuit Category']/parent::*/following-sibling::*//select";
		public static final String Layer3Resilience="@xpath=//td/a[text()='Layer 3 Resilience']/parent::*/following-sibling::*//select";
		public static final String TypeOfBilling="@xpath=//td/a[text()='Type of Billing']/parent::*/following-sibling::*//select";
		public static final String RelatedVoipService="@xpath=//td/a[text()='Related VoIP Service']/parent::*/following-sibling::*//select";
		public static final String ParallelRun="@xpath=//td/a[text()='Parallel Run']/parent::*/following-sibling::*//select";
		public static final String ReplacedTechSerID="@xpath=//td/a[text()='Replaced Technical Service ID']/parent::*/following-sibling::*//input[@type='text'][1]";
		public static final String BespokeReference="@xpath=//td/a[text()='Bespoke Reference']/parent::*/following-sibling::*//input";
		public static final String PrefixInput="@xpath=//td/a[contains(text(),'Prefix')]/parent::*/following-sibling::*//input";
		public static final String Typeofinput="@xpath=//td/a[contains(text(),'Type of')]/parent::*/following-sibling::*//select";
		public static final String NoOfIpAddresses="@xpath=//td/a[contains(text(),'Number of')]/parent::*/following-sibling::*//select";
	 } 

	public static class generalInformation
	{ 
		public static final String GeneralInformationTab="@xpath=//a[text()='General Information']";
		public static final String AddFeaturelink="@xpath=//a[text()='Add Feature']";
		public static final String TypeofFeature="@xpath=//a[text()='Add Feature']/following-sibling::*//textarea";
		public static final String Browse="@xpath=//div/a[text()='Browse']";
		public static final String SelectFeature="@xpath=//input[@value='Select']";
		public static final String SelectButton="@xpath=//div/a[contains(text(),'Select')]";
		public static final String SubIpOrder="@xpath=//a/span[contains(text(),'New IP Access Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Suborder="@xpath=//a/span[contains(text(),'New Ethernet Connection Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String HubSuborder="@xpath=//a/span[contains(text(),'Hub Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String Decompose="@xpath=//a[text()='Decompose']";
		public static final String NewEndSiteProductAend="@xpath=//td[text()='Entering']/parent::*/td/a";
		public static final String NewEndSiteProductBend="@xpath=(//a/span[contains(text(),'New End Site Product')])[2]/parent::*";
		public static final String Accountbredcrumb="@xpath=//a[@class=' path_pri txt_separator path_prev_item']";
		public static final String TransportCfs="@xpath=//a[starts-with(@tooltip,'New Transport CFS Order')]";
		public static final String AccountNameSorting="@xpath=//a[text()='Name']";
		public static final String OrderCheckbox="@xpath=//input[@class='checkbox checkbox_input']";
		public static final String StartProccessing="@xpath=//a[text()='Start Processing']";
	}
	
	public static class addIpAddressing
	{ 
		public static final String IpAddressingLink="@xpath=//span[text()='IP Addressing']/parent::*";
		public static final String IpAddressingFormat="@xpath=//td/a[text()='IP Addressing Format']/parent::*/following-sibling::*//select";
		public static final String Ipv4AddressingType="@xpath=//td/a[text()='IPv4 Addressing Type']/parent::*/following-sibling::*//select";
		public static final String Ipv6AddressingType="@xpath=//td/a[text()='IPv6 Addressing Type']/parent::*/following-sibling::*//select";
		public static final String ProviderAggIpLink="@xpath=//span[contains(text(),'Provider Aggregated')]/parent::*";
		public static final String NoOfIpv4Addresses="@xpath=//td/a[text()='Number of IPv4 Addresses']/parent::*/following-sibling::*//select";
		public static final String PrimaryRange="@xpath=//td/a[text()='Primary Range']/parent::*/following-sibling::*//select";
		public static final String SiebelCompId="@xpath=//td/a[text()='Siebel Component ID']/parent::*/following-sibling::*//input";
		public static final String ProviderAggIpv4Link="@xpath=(//span[contains(text(),'Provider Aggregated')]/parent::*)[1]";
		public static final String ProviderAggIpv6Link="@xpath=(//span[contains(text(),'Provider Aggregated')]/parent::*)[2]";
		public static final String NoOfIpv6Addresses="@xpath=(//td/a[contains(text(),'IPv6 Addresses')]/parent::*/following-sibling::*//select)[1]";
//				(//td/a[text()='Number of New PI IPv6 Addresses']/parent::*/following-sibling::*//select)[1]";
//				(//td/a[text()='Number of IPv6 Addresses']/parent::*/following-sibling::*//select)[1]";
		public static final String ProviderIndpIp4link="@xpath=(//span[contains(text(),'Provider Independent IPv4')]/parent::*)[1]";
		public static final String ProviderIndepIpv4Link="@xpath=//span[contains(text(),'Provider Independent IPv4')]/parent::*";
		public static final String ProviderIndepIpv6Link="@xpath=//span[contains(text(),'Provider Independent IPv6')]/parent::*";
		public static final String LanInterfaceIpv4Prefix="@xpath=//td/a[text()='LAN Interface IPv4 Prefix']/parent::*/following-sibling::*//input";
		public static final String LanInterfaceIpv4Address="@xpath=//td/a[text()='LAN Interface IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String LanInterfaceIpv6Address="@xpath=//td/a[text()='LAN Interface IPv6 Address']/parent::*/following-sibling::*//input";
		public static final String TypeofPIIpv4Addresses="@xpath=//td/a[text()='Type of PI IPv4 Addresses']/parent::*/following-sibling::*//select";
		public static final String TypeofPIIpv6Addresses="@xpath=//td/a[text()='Type of PI IPv6 Addresses']/parent::*/following-sibling::*//select";
	}
	
	public static class addBgpFeed
	{ 
		public static final String Bgp4FeedLink="@xpath=//span[text()='BGP4 Feed']/parent::*";
		public static final String TypeofAS="@xpath=//td/a[text()='Type of AS']/parent::*/following-sibling::*//select";
		public static final String ASNumber="@xpath=//td/a[text()='AS Number']/parent::*/following-sibling::*//input";
		public static final String BGP4FeedType="@xpath=//td/a[text()='BGP4 Feed Type']/parent::*/following-sibling::*//select";
		public static final String BGP4FeedReq="@xpath=//td/a[text()='BGP4 Feed Required']/parent::*/following-sibling::*//select";
	}
	
	public static class addDhcp
	{ 
		public static final String DhcpLink="@xpath=//span[contains(text(),'DHCP')]/parent::*";
		public static final String DhcpIpv4Link="@xpath=//span[contains(text(),'IPv4 DHCP')]/parent::*";
		public static final String DomainName="@xpath=//td/a[text()='Domain Name']/parent::*/following-sibling::*//input";
		public static final String DhcpIpv4Address="@xpath=//td/a[text()='DHCP IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String DhcpAddress="@xpath=//a[contains(text(),'Domain Name')]/parent::*/parent::*/following-sibling::*//input";
		public static final String DhcpIpv4Prefix="@xpath=//td/a[text()='DHCP IPv4 Prefix']/parent::*/following-sibling::*//input";
		public static final String DhcpPrefix="@xpath=//a[contains(text(),'Prefix')]/parent::*/following-sibling::*//input";
		public static final String PrimWinsAdd="@xpath=//td/a[text()='Primary WINS Server IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String SecondWinsAdd="@xpath=//td/a[text()='Secondary WINS Server IPv4 Address']/parent::*/following-sibling::*//input";
		public static final String PrimNameServerAdd="@xpath=//td/a[text()='Primary DHCP IPv4 Name Server Address']/parent::*/following-sibling::*//input";
		public static final String SeconNameServerAdd="@xpath=//td/a[text()='Secondary DHCP IPv4 Name Server Address']/parent::*/following-sibling::*//input";
		public static final String Format="@xpath=//td/a[contains(text(),'Format')]/parent::*/following-sibling::*//select";
		public static final String GatewayIpv4Add="@xpath=//td/a[text()='Gateway IPv4 Address']/parent::*/following-sibling::*//input";
	}
	
	public static class addSnmp
	{ 
		public static final String IpSnmpLink="@xpath=//span[contains(text(),'SNMP')]/parent::*";
		public static final String IpSnmp4Link="@xpath=//span[contains(text(),'IPv4 SNMP')]/parent::*";
		public static final String IpSnmp6Link="@xpath=//span[contains(text(),'IPv6 SNMP')]/parent::*";
		public static final String CommunityString="@xpath=//td/a[contains(text(),'Community String')]/parent::*/following-sibling::*//input";
		public static final String TrapCommunityString="@xpath=//td/a[contains(text(),'Traps Community String')]/parent::*/following-sibling::*//input";
		public static final String TrapsHostIpAddress="@xpath=//td/a[contains(text(),'Traps Receiving Host IP Address')]/parent::*/following-sibling::*//input";
		public static final String TrapsRequired="@xpath=//td/a[contains(text(),'Traps Required')]/parent::*/following-sibling::*//select";
		public static final String SnmpDeviceLink="@xpath=//span[contains(text(),'SNMP Device')]/parent::*";
		public static final String SnmpDeviceIpv4Link="@xpath=//span[contains(text(),'SNMP Device IPv4')]/parent::*";
		public static final String SnmpDeviceIpv6Link="@xpath=//span[contains(text(),'SNMP Device IPv6')]/parent::*";
		public static final String DeviceAddress="@xpath=//td/a[contains(text(),'SNMP Device')]/parent::*/following-sibling::*//input";
		public static final String SnmpReadLink="@xpath=//span[contains(text(),'SNMP Read-Only Access')]/parent::*";
		public static final String Acessrequired="@xpath=//td/a[contains(text(),'Access Required')]/parent::*/following-sibling::*//select";
		public static final String SnmpVersion="@xpath=//td/a[contains(text(),'SNMP Version')]/parent::*/following-sibling::*//select";
	}
	
	public static class addNat
	{ 
		public static final String NatLink="@xpath=//span[text()='NAT']/parent::*";
		public static final String DynamicNatLink="@xpath=//span[text()='Dynamic NAT']/parent::*";
		public static final String StaticNatLink="@xpath=//span[text()='Static NAT Route']/parent::*";
		public static final String StaticIPLink="@xpath=//span[contains(text(),'Static IP')]/parent::*";
		public static final String NatType="@xpath=//td/a[contains(text(),'Type of NAT')]/parent::*/following-sibling::*//select";
		public static final String NatMask="@xpath=//td/a[contains(text(),'NAT From Mask')]/parent::*/following-sibling::*//input";
		public static final String NatAddRangeFrom="@xpath=//td/a[contains(text(),'NAT IPv4 Address Range From')]/parent::*/following-sibling::*//input";
		public static final String NatAddRangeTo="@xpath=//td/a[contains(text(),'NAT IPv4 Address Range To')]/parent::*/following-sibling::*//input";
		public static final String NatPrivateport="@xpath=//td/a[contains(text(),'Private IP Port')]/parent::*/following-sibling::*//input";
		public static final String NatPublicport="@xpath=//td/a[contains(text(),'Public IP Port')]/parent::*/following-sibling::*//input";
	}
	
	public static class addStaticRoute
	{ 
		public static final String StaticRouteLink="@xpath=//span[contains(text(),'Route')]/parent::*";
		public static final String NextHopIp="@xpath=//td/a[text()='Next Hop IP']/parent::*/following-sibling::*//input";
		public static final String TargetNetwork="@xpath=//td/a[text()='Target Network']/parent::*/following-sibling::*//input";
		public static final String SubnetMask="@xpath=//td/a[text()='Sub Net Mask']/parent::*/following-sibling::*//input";
		public static final String StaticRoute4Link="@xpath=//span[contains(text(),'Static IPv4 Route')]/parent::*";
		public static final String StaticRoute6Link="@xpath=//span[contains(text(),'Static IPv6 Route')]/parent::*";
	}
	
	public static class addEndSiteDetails
	{ 
		public static final String ResilienceOption="@xpath=//td/a[text()='Resilience Option']/parent::*/following-sibling::*//select";
		public static final String AccessTechnology="@xpath=//td/a[text()='Access Technology']/parent::*/following-sibling::*//select";
		public static final String AccessType="@xpath=//td/a[text()='Access Type']/parent::*/following-sibling::*//select";
		public static final String SiteEnd="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*//select";
		public static final String SiteID="@xpath=//td/a[text()='Site ID']/parent::*/following-sibling::*//input";
		public static final String ReturnTask="@xpath=//span[text()='CPE Information']/parent::*";
	}
	
	public static class addAccessPort
	{ 
		public static final String AccessPortLink="@xpath=//span[text()='Access Port']/parent::*";
		public static final String PresentConnectType="@xpath=//td/a[text()='Presentation Connector Type']/parent::*/following-sibling::*//select";
		public static final String AccessportRole="@xpath=//td/a[text()='Port Role']/parent::*/following-sibling::*//select";
		public static final String VlanTaggingMode="@xpath=//td/a[text()='VLAN Tagging Mode']/parent::*/following-sibling::*//select";
		public static final String CPElink="@xpath=//span[text()='CPE Information']/parent::*";
		public static final String CabinetType="@xpath=//td/a[text()='Cabinet Type']/parent::*/following-sibling::*//select";
		public static final String CabinetID="@xpath=//td/a[text()='Cabinet ID']/parent::*/following-sibling::*//input";
		public static final String VLANLink="@xpath=//span[text()='VLAN']/parent::*";
		public static final String VlanTagId="@xpath=//td/a[text()='VLAN Tag ID']/parent::*/following-sibling::*//input";
		public static final String Ethertype="@xpath=//*[text()[contains(.,'Ethertype')]]/parent::*/following-sibling::*";
	}
	
	public static class taskDetails
	{ 
		public static final String TaskTab="@xpath=//a[text()='Tasks']";
		public static final String ExecutionFlowlink="@xpath=//a[text()='Execution Flow']/parent::*/following-sibling::*//a[2]";
		public static final String TaskTitle="@xpath=//div[@id='user-task-context']//span[contains(text(),'[New]')]";
		public static final String Workitems="@xpath=//a[text()='Work Items']";
		public static final String Scheduler="@xpath=//a[text()='Scheduler']";
		public static final String Errors="@xpath=//a[text()='Errors']";
	}
	
	public static class workItems
	{ 
		public static final String TaskReadytoComplete="@xpath=//td[text()='Ready']/parent::*/td//a";
		public static final String EndCheck="@xpath=//td/a[text()='Site End']/parent::*/following-sibling::*//select";
		public static final String AccessNetworkElement="@xpath=//td/a[text()='Access Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AccessPort="@xpath=//td/a[text()='Access Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort1="@xpath=//*[text()='CPE NNI Port 1']/parent::*/following-sibling::*//input[@type='text']";
		public static final String CPENNIPort2="@xpath=//*[text()='CPE NNI Port 2']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PENetworkElement="@xpath=//*[text()='PE Network Element']/parent::*/following-sibling::*//input[@type='text']";
		public static final String PePort="@xpath=//*[text()='PE Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String VCXController="@xpath=//*[text()='VCX Controller']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Beacon="@xpath=//*[text()='Beacon']/parent::*/following-sibling::*//input[@type='text']";
		public static final String EthernetLinkTransportRFS_1="@xpath=(//span[text()='Ethernet Link Transport RFS']/parent::*)[1]";
		public static final String EthernetLinkTransportRFS_2="@xpath=(//span[text()='Ethernet Link Transport RFS']/parent::*)[2]";
		public static final String L3cpeName="@xpath=//td/a[text()='L3 CPE']/parent::*/following-sibling::*//input[@type='text']";
		public static final String L3cpeUNIPort="@xpath=//td/a[text()='L3 CPE UNI Port']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Asr="@xpath=//td/a[text()='ASR']/parent::*/following-sibling::*//input[@type='text']";
		public static final String AsrGil="@xpath=//td/a[text()='ASR GIL']/parent::*/following-sibling::*//input[@type='text']";
		public static final String BespokeHQoSEntries="@xpath=//a[text()='Bespoke HQoS Entries']";
		public static final String WanIpSubnetSize="@xpath=//td/a[text()='WAN IPv4 Subnet Size']/parent::*/following-sibling::*//select";
		public static final String WanIpSubnetSizeFlag="@xpath=(//td/a[text()='WAN IPv4 Subnet Size']/parent::*/following-sibling::*//select)[2]";
		public static final String OtherWanIpSubnetSize="@xpath=//td/a[text()='Other WAN IPv4 Subnet Size']/parent::*/following-sibling::*//input";
		public static final String SerialTaskName="@xpath=//td/a[text()='Name']/parent::*/following-sibling::*[1]";
        public static final String AntSerialNo="@xpath=//td/a[text()='ANT Serial Number']/parent::*/following-sibling::*//input[@type='text']";
        public static final String GxLtsSerialNo="@xpath=//td/a[text()='GX/LTS Serial Number']/parent::*/following-sibling::*//input[@type='text']";
		public static final String UpdateAntSerialNumber="@xpath=//a[text()='Update ANT Serial Number']";
		public static final String UpdateSerialNumber="@xpath=//a[text()='Update Serial Number']";
		public static final String OAMProfile="@xpath=//*[text()='OAM Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LanIpAddress="@xpath=//td/a[text()='LAN IP Address']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LanIpRange="@xpath=//td/a[contains(text(),'Range Ref')]/parent::*/following-sibling::*//span/a[2]";
		public static final String RequestNewIp="@xpath=//a[text()='Request New IP Range in EIP']";
		public static final String ReleaseLanRange="@xpath=//*[text()='Release LAN Range']/parent::*/following-sibling::*//select";
		public static final String EgressPolicyMap="@xpath=//td/a[text()='Egress Policy Map']/parent::*/following-sibling::*//input[@type='text']";
		public static final String Duplex="@xpath=//td/a[text()='duplex']/parent::*/following-sibling::*//select";
		public static final String DuplexYesorNo="@xpath=(//td/a[text()='duplex']/parent::*/following-sibling::*//select)[2]";
		public static final String Speed="@xpath=//td/a[text()='speed']/parent::*/following-sibling::*//select";
		public static final String SpeedYesorNo="@xpath=(//td/a[text()='speed']/parent::*/following-sibling::*//select)[2]";
		public static final String BfdInterval="@xpath=//td/a[text()='bfd interval/min_rx']/parent::*/following-sibling::*//input";
		public static final String BfdIntervalYesorNo="@xpath=//td/a[text()='bfd interval/min_rx']/parent::*/following-sibling::*//select";
		public static final String Multiplier="@xpath=//td/a[text()='multiplier']/parent::*/following-sibling::*//input";
		public static final String MultiplierYesorNo="@xpath=//td/a[text()='multiplier']/parent::*/following-sibling::*//select";
		public static final String PremiumCir="@xpath=//td/a[text()='Premium CIR %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String InternetCir="@xpath=//td/a[text()='Internet CIR %']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LegacyCpeProfile="@xpath=//*[text()='Legacy CPE Profile']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LegacyCpeOamLevel="@xpath=//*[text()='Legacy CPE OAM Level']/parent::*/following-sibling::*//input[@type='text']";
		public static final String LegacyCpeSupportsCFM="@xpath=//*[text()='Legacy CPE supports CFM']/parent::*/following-sibling::*//select</LegacyCpeSupportsCFM>"; 
		public static final String Complete="@xpath=//a[@tooltip='Complete']";
		public static final String TransComplete="@xpath=//a[text()='Complete']";
		public static final String WorkItemSelect="@xpath=//a/span[contains(text(),'Reserve Access Resources')]/parent::*/parent::*/parent::*/td//input";
		public static final String View="@xpath=//a[text()='View']";
		public static final String OrderDifferenceTab="@xpath=//a[text()='Order Difference']";
		public static final String Cancelbutton="@xpath=//a[text()='Cancel']";
		public static final String Ipadress="@xpath=//a[text()='Management IP Address']/parent::*/following-sibling::*//input";
		public static final String RetryTask="@xpath=//a[text()='Retry']";
//		public static final String OrderCompleted="@xpath=//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),"Process Completed")]";
	}

	public static class searchCompOrder
	{ 
		public static final String Fastsearch="@xpath=//input[@id='fast_search_val']";
		public static final String ServiceInventorySearch="@xpath=//span[text()='COLT Service Inventory Search Folder']/parent::*";
		public static final String ProductOrderSearch="@xpath=//span[text()='COLT Product Order Search Profile']/parent::*";
		public static final String ServiceIDsearchcriterion="@xpath=//input[@tooltip='The input field for NC Service ID search criterion']";
		public static final String searchButton="@xpath=//div[@id='w_searchButton']//a";
		public static final String OrderNolink="@xpath=//td[text()='Processing']/parent::tr//a[@class='commonReferenceLink']/span";
	}
	
	public static class carNor
	{ 
		public static final String IpProductInstNumber="@xpath=//span[contains(text(),'IP Access Product Instance')]";
		public static final String ProductInstNumber="@xpath=//span[contains(text(),'Ethernet Connection Product Instance')]";
		public static final String ProductInstTab = "@xpath=//a[text()='Product Instances']";
		public static final String CreateDisconnectProductOrder = "@xpath=//a[text()='Create Disconnect Product Order']";
		public static final String DisconnectIpAccessOrder = "@xpath=//span[contains(text(),'Disconnect IP Access Product Order')]/parent::a";
		public static final String SelectDisconnectIpAccessOrder = "@xpath=//a/span[contains(text(),'Disconnect IP Access Product Order #')]/parent::*/parent::*/parent::*/td//input";
		public static final String HardCeaseDelay = "@xpath=//*[text()='Hard Cease Delay In Days']/parent::*/following-sibling::*//input[@type='text']";
		public static final String ColtPromiseDate = "@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][2]";
		public static final String ColtPromiseMonth = "@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][1]";
		public static final String ColtPromiseYear = "@xpath=//*[text()='CPD']/parent::*/following-sibling::*//input[@type='text'][3]";
		public static final String ReleaseLanRange = "@xpath=//*[text()='Release LAN Range']/parent::*/following-sibling::*//select";
	}

}
