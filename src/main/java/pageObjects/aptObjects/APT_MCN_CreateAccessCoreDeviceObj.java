package pageObjects.aptObjects;

public class APT_MCN_CreateAccessCoreDeviceObj {

	public static class CreateAccessCoreDevice {
		public static final String mcnlink = "@xpath=//b[contains(text(),'MANAGE COLT')]";
		public static final String createaccesscoredevicelink = "@xpath=//li[contains(text(),'Create Access/Core Device')]";

		// !-- Create Device Page --=
		public static final String nametextfield = "@xpath=//input[@id='name']";
		public static final String devicetypeinput = "@xpath=//select[@id='deviceTypeId']";
		public static final String devicetypeinput1 = "@xpath=//div[label[text()='Device Type']]//input";
		public static final String vendormodelinput = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String routeridtextfield = "@xpath=//input[@id='routerId']";
		public static final String snmproinputfield = "@xpath=//input[@id='snmpro']";
		public static final String telnetradiobutton = "@xpath=//input[@value='telnet']";
		public static final String sshradiobutton = "@xpath=//input[@value='ssh']";
		public static final String c2cradiobutton = "@xpath=//input[@value='2c']";
		public static final String c3radiobutton = "@xpath=//input[@value='3']";
		public static final String snmprotextfield = "@xpath=//input[@id='snmpro']";
		public static final String snmprwtextfield = "@xpath=//input[@id='snmprw']";
		public static final String snmpv3username = "@xpath=//input[@id='snmpV3UserName']";
		public static final String snmpv3authpassword = "@xpath=//input[@id='snmpV3AuthPassword']";
		public static final String snmpv3privpassword = "@xpath=//input[@id='snmpV3PrivPassword']";
		public static final String modularmspcheckbox = "@xpath=//input[@id='modularMSP']";
		public static final String fulliqnetcheckbox = "@xpath=//input[@id='fullIqnet']";
		//public static final String countryinput = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String countryinput="@xpath=//div[label[contains(text(),'Country')]]//select";//modified

		public static final String managementaddresstextbox = "@xpath=//input[@id='ipField']";
		//public static final String citydropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'City')]]//input";
		public static final String citydropdowninput="@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'City')]]//select"; //modified

		//public static final String sitedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//input";
		public static final String sitedropdowninput="@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Site')]]//select"; //modified
		
		//public static final String premisedropdowninput = "@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//input";
		public static final String premisedropdowninput="@xpath=//div[label[@class='form-label labelStyle'][contains(text(),'Premise')]]//select";//modified
		
		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String iosxrcheckbox = "@xpath=//input[@id='iosXr']";
		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String DSLAMdevice_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String DSLAMdevice_alertmessage = "@xpath=//div[@class='modal-body']";
		public static final String DSLAMdevice_popupOkbutton = "@xpath=//div[@class='modal-content']//span[text()='OK']";

		public static final String successMessage_deviceUpdation = "@xpath=//span[text()='Device updated successfully']";
		public static final String successMessage_Alert = "@xpath=(//div[@role='alert'])[1]//span";
		public static final String succesMsg_alertDisplay = "@xpath=(//div[@role='alert'])[1]";

		// !-- Warning Message --=
		public static final String WarningMesassage_site = "@xpath=//div[text()='Site']";
		public static final String warningMessage_city = "@xpath=//div[text()='City']";
		public static final String warningMessage_Country = "@xpath=//div[text()='Country']";
		public static final String warningMessage_name = "@xpath=//div[text()='Name']";
		public static final String warningMessage_deviceType = "@xpath=//div[text()='Device Type']";
		public static final String warningMessage_vendor = "@xpath=//div[text()='Vendor/Model']";
		public static final String warningMessage_routerId = "@xpath=//div[text()='Router Id']";

		// !-- fetch device Interface --=
		public static final String fetchDeviceinterfacelink_viewDevicePage = "@xpath=//a[text()='Fetch device interfaces']";
		public static final String ClickhereLink_fetchInterface = "@xpath=//a[text()='here']";

		// !-- manage link --="
		public static final String manageLink_viewDevicepage = "@xpath=//a[contains(text(),'Manage')]";

		public static final String addCitytogglebutton = "@xpath=//div[div[label[text()='Add City']]]//div[@class='react-switch-bg']";

		public static final String citynameinputfield = "@xpath=//input[@id='cityName']";
		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode']";
		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";

		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2]";
		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2]";
		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";
		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";

		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";
		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		public static final String clearbutton = "@xpath=//span[contains(text(),'Clear')]";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String deviceAlert = "@xpath=//div[@role='alert']//span[contains(.,'Error while creating device')]";

		// !-- view device page --
		public static final String devicecreationsuccessmsg = "@xpath=//span[contains(text(),'Device created successfully')]";

		// !-- Edit Device --
		public static final String Action = "@xpath=//div[text()='Device']/following-sibling::div//button[text()='Action']";
		public static final String Edit = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Edit']";

		// !-- Delete Device --
		public static final String Delete = "@xpath=//div[text()='Device']/following-sibling::div//a[text()='Delete']";
		public static final String DeleteDeviceAlertHeader = "@xpath=//div[text()='Delete Device']";
		public static final String DeleteConfirmButton = "@xpath=//button[text()='Delete']";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deleteMessages_textMessage = "@xpath=//div[@class='modal-body']";
		public static final String deletebutton = "@xpath=//button[@class='btn btn-danger']";

		// !-- Search Device --
		public static final String SearchDeviceLink = "@xpath=//li[contains(text(),'Search for Device')]";
		public static final String DeviceName = "@xpath=//input[@id='deviceName']";

		public static final String managementaddressbutton = "@xpath=//button//span[text()='Management Address']";

		public static final String IQNet_alertHeader = "@xpath=//div[div[@class='modal-header']]";
		public static final String IQNet_alertBody = "@xpath=//div[@class='modal-body']";
		public static final String IONet_xButton = "@xpath=//span[contains(text(),'�')]";

		// !-- Router Panel --=
		public static final String commandIPV4_dropdown = "@xpath=//div[label[text()='Command IPV4']]//input";
		public static final String commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String commandIPv4_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[1]";

		public static final String commandIPV6_dropdown = "@xpath=//div[label[text()='Command IPV6']]//input";
		public static final String commandIPv6_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String commandIPv6_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])";

		public static final String executebutton_Ipv4 = "@xpath=(//span[text()='Execute'])[1]";
		public static final String executebutton_IPv6 = "@xpath=(//span[text()='Execute'])[2]";

		public static final String result_textArea = "@xpath=//div[label[text()='Result']]//textarea";

		// !-- select tag Dropdown --
		public static final String countryDropdown_selectTag = "@xpath=//select[@id='countryId']";
		public static final String cityDropdown_selectTag = "@xpath=//select[@id='cityId']";
		public static final String siteDropdown_selectTag = "@xpath=//select[@id='siteId']";
		public static final String premiseDropdown_selectTag = "@xpath=//select[@id='premiseId']";

		public static final String testCommandTable = "@xpath=//div[text()='* Device reachability tests require the device to have an IP address.']";

		// New
		public static final String devicetypeinput2="']";//added
		
		public static final String vendormodelinput1 = "@xpath=//div[label[text()='Vendor/Model']]//div[text()='";// added
		public static final String vendormodelinput2 = "']";// added
		public static final String countryinput1 = "@xpath=//div[label[contains(text(),'Country')]]//option[@label='";// added
		public static final String countryinput2 = "']";// added
		public static final String citydropdowninput1 = "@xpath=//select[@id='cityId']//option[@label='";// added
		public static final String citydropdowninput2 = "']";// added
		public static final String sitedropdowninput1 = "@xpath=//select[@id='siteId']//option[@label='";// added
		public static final String sitedropdowninput2 = "']";// added
		public static final String premisedropdowninput1 = "@xpath=//select[@id='premiseId']//option[@label='";// added
		public static final String premisedropdowninput2 = "'][1]";// added
		public static final String deleteDeviceConfMsg = "@xpath=//span[text()='Device successfully deleted.']";

		// -- Device view Page --
		public static final String fetchValueFromViewPage = "@xpath=//div[label[text()='value']]//following-sibling::div";
		public static final String fetchValueFromViewPage_NameTab = "@xpath=//div//label[text()='Name']//following::div[1]"; // added
		public static final String fetchValueFromViewPage_DeviceType = "@xpath=//div//label[text()='Device Type']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Vendor_Model = "@xpath=//div//label[text()='Vendor/Model']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Connectivity = "@xpath=//div//label[text()='Connectivity Protocols']//following::div[1]";// added
		public static final String fetchValueFromViewPage_SNMP_version = "@xpath=//div//label[text()='SNMP Version']//following::div[1]";// added
		public static final String fetchValueFromViewPage_SNMP_Auth_pass = "@xpath=//div//label[text()='Snmp v3 Auth password']//following::div[1]";
		public static final String fetchValueFromViewPage_SNMP = "@xpath=//div//label[text()='SNMP Version']//following::div[1]";// added
		public static final String fetchValueFromViewPage_snmprw = "@xpath=//div//label[text()='Snmprw']//following::div[1]";// added
		public static final String fetchValueFromViewPage_snmpro = "@xpath=//div//label[text()='Snmpro']//following::div[1]";// added
		public static final String fetchValueFromViewPage_SNMP_priv_pass = "@xpath=//div//label[text()='Snmp v3 Priv password']//following::div[1]";
		// public static final String
		// fetchValueFromViewPage_SNMP_Auth_pass="@xpath=//div//label[text()='Snmp
		// v3 Auth password']//following::div[1]";
		public static final String fetchValueFromViewPage_SNMP_V3_Username = "@xpath=//div//label[text()='Snmp v3 Username']//following::div[1]";
		public static final String fetchValueFromViewPage_RouterId = "@xpath=//div//label[text()='Router Id']//following::div[1]";// added
		public static final String fetchValueFromViewPage_ManagementAdd = "@xpath=//div//label[text()='Management Address']//following::div[1]";// added
		public static final String fetchValueFromViewPage_FullIQNET = "@xpath=//div//label[text()='Full IQNET']//following::div[1]";// added
		public static final String fetchValueFromViewPage_ModularMSP = "@xpath=//div//label[text()='Modular MSP']//following::div[1]";
		public static final String fetchValueFromViewPage_IOS_XR = "@xpath=//div//label[text()='IOS-XR']//following::div[1]";
		public static final String fetchValueFromViewPage_Country = "@xpath=//div//label[text()='Country']//following::div[1]";// added
		public static final String fetchValueFromViewPage_City = "@xpath=//div//label[text()='City']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Site = "@xpath=//div//label[text()='Site']//following::div[1]";// added
		public static final String fetchValueFromViewPage_Premise = "@xpath=//div//label[text()='Premise']//following::div[1]";// added
		public static final String tableRow = "@xpath=//tbody/tr";
		
		public static final String compareText_InViewPage_ForNonEditedFields1 ="@xpath=//div[div[label[contains(text(),'";
		public static final String compareText_InViewPage_ForNonEditedFields2 ="";

	}

}
