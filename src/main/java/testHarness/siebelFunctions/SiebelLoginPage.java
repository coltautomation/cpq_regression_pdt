package testHarness.siebelFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4CLoginObj;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.siebelObjects.SiebelLoginObj;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;
import pageObjects.cpqObjects.CPQLoginObj;

public class SiebelLoginPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void SiebelLogin(String username, String password) throws InterruptedException, AWTException, IOException 
	{
		webDriver.get(Configuration.Siebel_URL);
		Reusable.waitForSiebelLoader();
		if (isElementPresent(By.xpath("//*[@id='details-button']")))
		{
		verifyExists(SiebelLoginObj.Login.Advanced,"Advanced button");
		click(SiebelLoginObj.Login.Advanced,"Advanced button");
		
		verifyExists(SiebelLoginObj.Login.Navigate,"Navigate button");
		click(SiebelLoginObj.Login.Navigate,"Navigate button");
		}
		waitForElementToAppear(SiebelLoginObj.Login.userNameTxb, 5);
		verifyExists(SiebelLoginObj.Login.userNameTxb,"User ID textbox");
		sendKeys(SiebelLoginObj.Login.userNameTxb, username,"User ID textbox field");
	
		verifyExists(SiebelLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(SiebelLoginObj.Login.passWordTxb, password,"Password textbox field");
		verifyExists(SiebelLoginObj.Login.loginBtn,"Login button");
		click(SiebelLoginObj.Login.loginBtn,"Login button");
		
		waitForAjax();
		Reusable.waitForSiebelLoader();
	//	waitForElementToAppear(SiebelLoginObj.Login.SiebelUserinfo1, 10);
	//	verifyExists(SiebelLoginObj.Login.SiebelUserinfo1,"Verify Correct user has been logged in");
		
			
	}
	
	
	public void SelectQuote(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		verifyExists(CPQLoginObj.Login.GearIcon,"Gear Icon on Header of Landing Page");
		click(CPQLoginObj.Login.GearIcon,"Gear Icon");
		
		//Open Transactions list
		waitForElementToAppear(CPQLoginObj.Login.TransactionIcon, 10);
		verifyExists(CPQLoginObj.Login.TransactionIcon,"Transaction Icon on Header of Landing Page");
		click(CPQLoginObj.Login.TransactionIcon,"Transaction Icon");
		
		//Open Quote details
		String Quote_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quote_ID");
		String quoteIdObj = CPQLoginObj.Login.QuoteID1+ Quote_ID +CPQLoginObj.Login.QuoteID2;
		waitForElementToAppear(quoteIdObj, 10);
		verifyExists(quoteIdObj,"Quote ID on Transaction list Page");
		click(quoteIdObj,"Quote ID");
		
	} 
	public void SiebelLogout() throws InterruptedException, AWTException, IOException
	{
	// waitForElementToAppear(SiebelLoginObj.Login.Settings, 5);
	verifyExists(SiebelLoginObj.Login.Settings,"Setting button");
	click(SiebelLoginObj.Login.Settings,"Setting button");
	verifyExists(SiebelLoginObj.Login.Logout,"Logout button");
	click(SiebelLoginObj.Login.Logout,"Logout button");
	closeWebdriver();
	}
	
}
