package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import pageObjects.nodObjects.NOD_NHTransactionScreenObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NOD_NHTransactionScreenPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	NODMyOrderPage myOrder=new NODMyOrderPage();
	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void NumberHistory(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String TelephoneNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number");
		String TelephoneNumberRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number_Range");

		verifyExists(NODPortInRequestObj.portIn.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODPortInRequestObj.portIn.QuickLinkDrpDwnArw,"Quick Link Drop down");

		click(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoOpt,"My Telephone Number Option");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoPage,15);	

		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);

		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,15);
		click(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,"Select drop down");
		click(NODTelephoneNumberSearchObj.MyNumber.NumberOpt,"Number Option");
		Reusable.waitForAjax();

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, TelephoneNumber, "Telephone Number");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id is Present");
			click(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"Order ID");

			//Number History Section
			verifyExists(NODTelephoneNumberSearchObj.MyNumber.NumberHistorysSection,"Number History Section");
			click(NODTelephoneNumberSearchObj.MyNumber.NumberHistorysSection,"Number History Section");

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.SearchNumberFldLbl,"Search for a number Field");
			sendKeys(NOD_NHTransactionScreenObj.NumberDetails.SearchNumberFld, TelephoneNumberRange, "Telephone Number Range");
			click(NOD_NHTransactionScreenObj.NumberDetails.SearchIcon,"Search Icon");
			Reusable.waitForAjax();

			String NumberStatus=getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.NumberStatus, "Number Status");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Number Status is :"+NumberStatus);		

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Is not Present");	
		}		
	}

	public void PortinDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String TelephoneNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number");
		String TelephoneNumberRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number_Range");

		verifyExists(NODPortInRequestObj.portIn.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODPortInRequestObj.portIn.QuickLinkDrpDwnArw,"Quick Link Drop down");

		click(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoOpt,"My Telephone Number Option");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoPage,15);	

		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);

		click(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,"Select drop down");
		click(NODTelephoneNumberSearchObj.MyNumber.NumberOpt,"Number Option");
		Reusable.waitForAjax();

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, TelephoneNumber, "Telephone Number");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id is Present");
			click(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"Order ID");

			//Order Details section
			verifyExists(NODTelephoneNumberSearchObj.MyNumber.OrderDetailsSection,"Order Details Section");
			click(NODTelephoneNumberSearchObj.MyNumber.OrderDetailsSection,"Order Details Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//*[contains(text(),'Order Details')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");

				ScrollIntoViewByString(NOD_NHTransactionScreenObj.NumberDetails.PortingDetailsLabel);
				verifyExists(NOD_NHTransactionScreenObj.NumberDetails.PortingDetailsLabel,"Porting Details Section");

				verifyExists(NOD_NHTransactionScreenObj.NumberDetails.PortingDateLabel,"Porting Date field");
				String PortingDate=getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.PortingDate, "Porting Date");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Porting Date is :"+PortingDate);		

				verifyExists(NOD_NHTransactionScreenObj.NumberDetails.PortingWindoeLabel,"Porting Window field");
				String PortingWindowDate=getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.PortingWindowDate, "Porting Window Date");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Porting Window Date is :"+PortingWindowDate);		

			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
			}

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Is not Present");	
		}

	}

	public void NumberActivation_FieldValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		boolean custnameErr = false;
		boolean streetnameErr = false;
		boolean citynameErr = false;
		boolean postalcodeErr = false;

		verifyExists(NODPortInRequestObj.portIn.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODPortInRequestObj.portIn.QuickLinkDrpDwnArw,"Quick Link Drop down");

		click(NOD_NHTransactionScreenObj.NumberDetails.ActivationOption,"Activation Option");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NOD_NHTransactionScreenObj.NumberDetails.ActivationPage,15);	

		click(NOD_NHTransactionScreenObj.NumberDetails.ValidateAddressbtn,"Validation Address button");

		try {
			custnameErr = isElementPresent(By.xpath("//div[contains(text(),'Customer Name is required')]"));
			if (custnameErr) 
			{
				String custnameErrMsg = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.CustNameErr);
				Reporter.log("Customer Name message displayed as : " + custnameErr);
				Reporter.log("Customer Name message displayed as : " + custnameErrMsg);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Customer Name message is not dipslaying");
		} 

		//Street name
		try {
			streetnameErr = isElementPresent(By.xpath("//div[contains(text(),'Street Name is required')]"));
			if (streetnameErr) 
			{
				String streetnameErrMsg = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.StreetNameErr);
				Reporter.log("Street Name message displayed as : " + streetnameErr);
				Reporter.log("Street Name message displayed as : " + streetnameErrMsg);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Street Name message is not dipslaying");
		} 

		//City name
		try {
			citynameErr = isElementPresent(By.xpath("//div[contains(text(),'City / Town is required')]"));
			if (citynameErr) 
			{
				String citynameErrMsg = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.CityNameErr);
				Reporter.log("City Name message displayed as : " + citynameErr);
				Reporter.log("City Name message displayed as : " + citynameErrMsg);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("City Name message is not dipslaying");
		} 

		//Postal Code
		try {
			postalcodeErr = isElementPresent(By.xpath("//div[contains(text(),'Postal Code is required')]"));
			if (postalcodeErr) 
			{
				String postalcodeErrMsg = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.PostalCodeErr);
				Reporter.log("Postal Code message displayed as : " + postalcodeErr);
				Reporter.log("POstal Code message displayed as : " + postalcodeErrMsg);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Postal Code message is not dipslaying");
		} 
		catch (Exception er) {
			er.printStackTrace();
		}
	}

	public void NumberDeactivation_FieldValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String DeactivateOrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Deactivate_Order_ID");

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, DeactivateOrderID, "Order ID");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);

		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");

		//Deactivated number details
		verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		click(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		Reusable.waitForAjax();
		if(isElementPresent(By.xpath("//*[contains(text(),'Order Details')]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderIdLbl,"Order Id label");
			String orderId = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderId,"Order ID");
			Reporter.log("Order Id displayed as : " + orderId);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Id displayed as : "+ orderId);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderTypeLbl,"Order Type label");
			String orderType = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderType,"Order Type");
			Reporter.log("Order Type displayed as : " + orderType);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Type displayed as :  "+ orderType);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderStatusLbl,"Order Status label");
			String orderstatus = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderStatus,"Order Status");
			Reporter.log("Order Status displayed as : " + orderstatus);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Status displayed as : "+ orderstatus);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.UserNameLbl,"User Name label");
			String UserName = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.UserName,"UserName");
			Reporter.log("Order User Name sdisplayed as : " + UserName);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order User Name sdisplayed as : "+ UserName);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDateLbl,"Order Date label");
			String OrderDate = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDate,"OrderDate");
			Reporter.log("Order Date displayed as : " + OrderDate);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Date displayed as : "+ OrderDate);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDescriptionLbl,"Order Description label");
			String OrderDescription = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDescription,"OrderDescription");
			Reporter.log("Order Description displayed as : " + OrderDescription);		
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Description displayed as : "+ OrderDescription);

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
		}

	}

	public void NumberReservation_FieldValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String NumberReservation_OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberReservation_OrderID");

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, NumberReservation_OrderID, "Order ID");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);

		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");

		//Reservation number details
		verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		click(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		Reusable.waitForAjax();

		if(isElementPresent(By.xpath("//*[contains(text(),'Order Details')]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");
			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderIdLbl,"Order Id label");
			String orderId = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderId,"Order ID");
			Reporter.log("Order Id displayed as : " + orderId);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Id displayed as : "+ orderId);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderTypeLbl,"Order Type label");
			String orderType = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderType,"Order Type");
			Reporter.log("Order Type displayed as : " + orderType);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Type displayed as :  "+ orderType);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderStatusLbl,"Order Status label");
			String orderstatus = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderStatus,"Order Status");
			Reporter.log("Order Status displayed as : " + orderstatus);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Status displayed as : "+ orderstatus);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.UserNameLbl,"User Name label");
			String UserName = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.UserName,"UserName");
			Reporter.log("Order User Name sdisplayed as : " + UserName);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order User Name sdisplayed as : "+ UserName);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDateLbl,"Order Date label");
			String OrderDate = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDate,"OrderDate");
			Reporter.log("Order Date displayed as : " + OrderDate);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Date displayed as : "+ OrderDate);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDescriptionLbl,"Order Description label");
			String OrderDescription = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDescription,"OrderDescription");
			Reporter.log("Order Description displayed as : " + OrderDescription);		
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Description displayed as : "+ OrderDescription);
			
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
		}

	}

	public void NumberReactivation_FieldValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String NumberReactivation_OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberReactivation_OrderID");

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, NumberReactivation_OrderID, "Order ID");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);

		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");

		//Reactivation number details
		verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		click(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		Reusable.waitForAjax();

		if(isElementPresent(By.xpath("//*[contains(text(),'Order Details')]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderIdLbl,"Order Id label");
			String orderId = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderId,"Order ID");
			Reporter.log("Order Id displayed as : " + orderId);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Id displayed as : "+ orderId);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderTypeLbl,"Order Type label");
			String orderType = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderType,"Order Type");
			Reporter.log("Order Type displayed as : " + orderType);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Type displayed as :  "+ orderType);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderStatusLbl,"Order Status label");
			String orderstatus = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderStatus,"Order Status");
			Reporter.log("Order Status displayed as : " + orderstatus);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Status displayed as : "+ orderstatus);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.UserNameLbl,"User Name label");
			String UserName = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.UserName,"UserName");
			Reporter.log("Order User Name sdisplayed as : " + UserName);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order User Name sdisplayed as : "+ UserName);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDateLbl,"Order Date label");
			String OrderDate = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDate,"OrderDate");
			Reporter.log("Order Date displayed as : " + OrderDate);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Date displayed as : "+ OrderDate);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDescriptionLbl,"Order Description label");
			String OrderDescription = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDescription,"OrderDescription");
			Reporter.log("Order Description displayed as : " + OrderDescription);		
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Description displayed as : "+ OrderDescription);		

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
		}
	}

	public void NewPortIn_FieldValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String NumberPortIn_OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NumberPortIn_OrderID");

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, NumberPortIn_OrderID, "Order ID");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);

		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");

		//Reactivation number details
		verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		click(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		Reusable.waitForAjax();

		if(isElementPresent(By.xpath("//*[contains(text(),'Order Details')]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderIdLbl,"Order Id label");
			String orderId = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderId,"Order ID");
			Reporter.log("Order Id displayed as : " + orderId);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Id displayed as : "+ orderId);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderTypeLbl,"Order Type label");
			String orderType = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderType,"Order Type");
			Reporter.log("Order Type displayed as : " + orderType);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Type displayed as :  "+ orderType);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderStatusLbl,"Order Status label");
			String orderstatus = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderStatus,"Order Status");
			Reporter.log("Order Status displayed as : " + orderstatus);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Status displayed as : "+ orderstatus);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.UserNameLbl,"User Name label");
			String UserName = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.UserName,"UserName");
			Reporter.log("Order User Name sdisplayed as : " + UserName);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order User Name sdisplayed as : "+ UserName);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDateLbl,"Order Date label");
			String OrderDate = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDate,"OrderDate");
			Reporter.log("Order Date displayed as : " + OrderDate);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Date displayed as : "+ OrderDate);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDescriptionLbl,"Order Description label");
			String OrderDescription = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDescription,"OrderDescription");
			Reporter.log("Order Description displayed as : " + OrderDescription);		
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Description displayed as : "+ OrderDescription);	

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
		}
	}

	public void PortinActivated_FieldValidation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String PortInActivated_RangeEnd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PortInActivated_RangeEnd");

		verifyExists(NODMyOrdersObj.MyOrder.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODMyOrdersObj.MyOrder.QuickLinkDrpDwnArw,"Quick Link Drop down");
		Reusable.waitForAjax();

		click(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoOpt,"My Telephone Number Option");
		Reusable.waitForAjax();
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.myTelephoneNoPage,15);	

		waitForElementToDisappear(NODTelephoneNumberSearchObj.MyNumber.TableRecordLoader,120);
		waitForElementToBeVisible(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,15);	

		click(NODTelephoneNumberSearchObj.MyNumber.SelectDrpDwn,"Select drop down");
		click(NODTelephoneNumberSearchObj.MyNumber.NumberOpt,"Number Option");
		Reusable.waitForAjax();

		sendKeys(NODTelephoneNumberSearchObj.MyNumber.SearchTxtFld, PortInActivated_RangeEnd, "Telephone Number");
		click(NODTelephoneNumberSearchObj.MyNumber.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		//Order ID Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.OrderIDColumn,"Order ID Column");

		String TransID = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.OrderIDVal,"OrderID");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+TransID);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Id Is not Present");	
		}

		//Order Status Column & data
		verifyExists(NODTelephoneNumberSearchObj.MyNumber.NumberStatusColumn,"Number Status Column");
		String NumberStatus = getTextFrom(NODTelephoneNumberSearchObj.MyNumber.NumberVal,"Number Status");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Number Status Is: "+NumberStatus);

		click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");

		//Reactivation number details
		verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		click(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
		Reusable.waitForAjax();

		if(isElementPresent(By.xpath("//*[contains(text(),'Order Details')]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderIdLbl,"Order Id label");
			String orderId = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderId,"Order ID");
			Reporter.log("Order Id displayed as : " + orderId);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Id displayed as : "+ orderId);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderTypeLbl,"Order Type label");
			String orderType = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderType,"Order Type");
			Reporter.log("Order Type displayed as : " + orderType);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Type displayed as :  "+ orderType);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderStatusLbl,"Order Status label");
			String orderstatus = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderStatus,"Order Status");
			Reporter.log("Order Status displayed as : " + orderstatus);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Status displayed as : "+ orderstatus);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.UserNameLbl,"User Name label");
			String UserName = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.UserName,"UserName");
			Reporter.log("Order User Name sdisplayed as : " + UserName);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order User Name sdisplayed as : "+ UserName);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDateLbl,"Order Date label");
			String OrderDate = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDate,"OrderDate");
			Reporter.log("Order Date displayed as : " + OrderDate);
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Date displayed as : "+ OrderDate);

			verifyExists(NOD_NHTransactionScreenObj.NumberDetails.OrderDescriptionLbl,"Order Description label");
			String OrderDescription = getTextFrom(NOD_NHTransactionScreenObj.NumberDetails.OrderDescription,"OrderDescription");
			Reporter.log("Order Description displayed as : " + OrderDescription);		
			ExtentTestManager.getTest().log(LogStatus.INFO, "Order Description displayed as : "+ OrderDescription);

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
		}
	}





}



