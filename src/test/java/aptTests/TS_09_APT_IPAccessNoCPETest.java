package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_Login;

public class TS_09_APT_IPAccessNoCPETest extends SeleniumUtils {

	APT_IPAccessNoCPEHelper NoCPE = new APT_IPAccessNoCPEHelper();
	APT_Login Login = new APT_Login();

	// ReadExcelFile read = new ReadExcelFile();
	// ReusableFunctions Reusable = new ReusableFunctions();
	// String dataFileName = "APT_IPACCESS";
	// File path = new File("./src/test/resources/"+dataFileName);
	// String testDataFile = path.toString();
	// String dataSheet = "NoCPE";
	// String scriptNo = "1";
	// String dataSetNo = "1";

	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-01", priority = 1)
	public void createcustomers(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerCreation");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");
			
			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {

				NoCPE.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
				NoCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
				NoCPE.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			NoCPE.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.verifyServicetypeAndNetworkConfigurationSelection(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.verifyServiceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.verifyCustomerDetailsInformation(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.verifyservicepanelInformationinviewservicepage(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.verifyServicePanelActions_NoCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.verifyManageService(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.addExistingPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.verifyExistingDevice_ViewDevicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.testStatus_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.deleteDevice(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.navigateToAddNewDevicepage_PE();
			NoCPE.verifyadddevicefields_PE();
			NoCPE.addNewPEDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.verifyViewpage_Devicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.testStatus_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.verifyEditDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.navigateToViewDevicePage(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.verifyViewpage_UpdatedDevicedetails_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			Reporter.log("==========Route Panel ================");
			NoCPE.routerPanel_PE(testDataFile, dataSheet, scriptNo, dataSetNo);

			NoCPE.addRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.editRouteFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String VendorModel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VendorModel");
			String Juniper = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Juniper");

			if ((VendorModel).contains(Juniper)) {
				NoCPE.verifyInterfaceConfigHistory(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
		
			NoCPE.verifyFetchDeviceInterface_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
						
			Reporter.log("==========Add / Edit Interface ================");
			Report.LogInfo("INFO", "==========Add / Edit Interface ================", "PASS");
			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			if ((VendorModel).contains("Cisco")) {
				//Report.SummaryReportlog("TC-18 : verify_CiscoVendor_AddEditInterface_NoCPE");

				NoCPE.verify_CiscoVendor_AddInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
				NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				NoCPE.verify_CiscoVendor_EditInterface(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else {
				//Report.SummaryReportlog("TC-18 : VerifyAdd_EditInterface_JuniperVendor_NoCPE");

				NoCPE.VerifyAddInterface_JuniperVendor(testDataFile, dataSheet, scriptNo, dataSetNo);
				NoCPE.VerifyEditInterface_JuniperVendor(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
		
			Reporter.log("==========Add Multlink ================");
			Report.LogInfo("INFO", "Add Multi link", "PASS");

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			if ((VendorModel).contains("Cisco")) {
				//Report.SummaryReportlog("TC-19 : verify_CiscoVendor_AddMultilink_NoCPE");

				NoCPE.verify_CiscoVendor_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else {
				//Report.SummaryReportlog("TC-19 : verify_JuniperVendor_AddMultilink_NoCPE");
				NoCPE.verify_JuniperVendor_AddMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

			String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RemoveInterface_Selection");
			String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"AddInterface_Selection");

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.selectInterfacelinkforDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			Report.LogInfo("INFO","==========Remove Iterface From Service ================","PASS");
			if ((RemoveInterface_Selection).equalsIgnoreCase("yes")) {
				NoCPE.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else {
				System.out.println("interfaces are not removed");
			}
			Report.LogInfo("INFO","==========Select Interface To Add With Service ================","PASS");

			if ((AddInterface_Selection).equalsIgnoreCase("yes")) {
				NoCPE.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);

			} else {
				System.out.println("Interfaces are not added");
			}

			Report.LogInfo("INFO","==========Delete PE Device ================","PASS");

			NoCPE.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			NoCPE.deleteDevice_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			Report.LogInfo("INFO","==========Delete Service ================","PASS");
			NoCPE.deleteSevice1(testDataFile, dataSheet, scriptNo, dataSetNo);

		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}

}
