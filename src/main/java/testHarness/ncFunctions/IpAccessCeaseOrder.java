package testHarness.ncFunctions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.CeaseOrderObj;
import pageObjects.ncObjects.IpVpnOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class IpAccessCeaseOrder extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void CreateIpAccessCeaseOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
	{
 		String OrderNumber= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order Number");

		verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on AccountName Filter");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Name Filter");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,OrderNumber,"FilterInputValue");		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering Order number for cease");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Apply button");
		String CompOrdernumber=OrderNumber;
		
		//Log.info(CompOrdernumber.get());
		
		String[] arrOfStr = CompOrdernumber.split("#", 0);
		//Log.info(arrOfStr[1]);
		waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Order to be Completed");
		ClickonElementByString("//a/span[contains(text(),'\"+arrOfStr[1]+\"')]/parent::*",10);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Order");
		String ProductInstancenumber=getTextFrom(CeaseOrderObj.CeaseOrder.IPAccessProductInstNumber);
		DataMiner.fnsetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo,"ProductInstancenumber",ProductInstancenumber);

		//Log.info(ProductInstancenumber.get());
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Pick the Instance Number");
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0);
		//Log.info(arrOfStr1[1]);
		
		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Accounts Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		click(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Product Instance Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Name Filter");
		
		selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Selected Name in Filter");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,arrOfStr1[1],"FilterInputValue");	
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Instance number for cease provided ");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Apply button");
		
		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/parent::*/td//input",10);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Instance ");
		
		verifyExists(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		click(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Create Disconnect Product Order ");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		String CeaseOrdernumber=getTextFrom(CeaseOrderObj.CeaseOrder.LinkforOrder);
		DataMiner.fnsetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo,"Order Number",CeaseOrdernumber);

		
		
		//Log.info(CeaseOrdernumber.get());
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order Number Generated : "+CeaseOrdernumber);
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		//Log.info(arrOfStr2[1]);
		String CeaseOrderscreenURL = getAttributeFrom(CeaseOrderObj.CeaseOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo,"Order_URL",CeaseOrderscreenURL);

		//Log.info(CeaseOrderscreenURL.get());
		
	}
	
	public void UpdateDescription() throws Exception
	{

		verifyExists(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"GeneralInformationTab");
		click(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"GeneralInformationTab");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();

		verifyExists(CeaseOrderObj.CeaseOrder.EditDesc,"EditDesc");
		click(CeaseOrderObj.CeaseOrder.EditDesc,"EditDesc");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		sendKeys(CeaseOrderObj.CeaseOrder.OrderDescription,"Cease Order created using Automation script","OrderDescription");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order Description Updated");	

		verifyExists(CeaseOrderObj.CeaseOrder.Update,"Update");
		click(CeaseOrderObj.CeaseOrder.Update,"Update");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Update button");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();	

}



public void EditProduct(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
{
    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order_URL");
	getUrl(CeaseOrderscreenURL);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Cease Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.Update,"DisconnectIpAccessOrder");
	click(CeaseOrderObj.CeaseOrder.Update,"DisconnectIpAccessOrder");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Disconnect IP Access Product Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.Edit,"Edit");
	click(CeaseOrderObj.CeaseOrder.Edit,"Edit");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
	
	verifyExists(CeaseOrderObj.CeaseOrder.HardCeaseDelay,"HardCeaseDelay");
	click(CeaseOrderObj.CeaseOrder.HardCeaseDelay,"HardCeaseDelay");
	clearTextBox(CeaseOrderObj.CeaseOrder.HardCeaseDelay);
	
	sendKeys(CeaseOrderObj.CeaseOrder.HardCeaseDelay,"0","HardCeaseDelay");	
	Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.HardCeaseDelay, Keys.ENTER);
	
	
	DateFormat Year = new SimpleDateFormat("yyyy");
	DateFormat Month = new SimpleDateFormat("MMM");
	DateFormat Day = new SimpleDateFormat("dd");
	Calendar c = Calendar.getInstance();
	c.add(Calendar.DATE, -1);
	Date modifiedDate = c.getTime();
	
	verifyExists(CeaseOrderObj.CeaseOrder.ColtPromiseMonth,"ColtPromiseMonth");
	click(CeaseOrderObj.CeaseOrder.ColtPromiseMonth,"ColtPromiseMonth");
	
	sendKeys(CeaseOrderObj.CeaseOrder.ColtPromiseMonth,Month.format(modifiedDate),"ColtPromiseMonth");	

	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	
	Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.ColtPromiseMonth, Keys.ENTER);
	
	
	verifyExists(CeaseOrderObj.CeaseOrder.ColtPromiseDate,"ColtPromiseDate");
	click(CeaseOrderObj.CeaseOrder.ColtPromiseDate,"ColtPromiseDate");
	sendKeys(CeaseOrderObj.CeaseOrder.ColtPromiseDate,Day.format(modifiedDate),"ColtPromiseDate");	
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.ColtPromiseDate, Keys.ENTER);

		
	verifyExists(CeaseOrderObj.CeaseOrder.ColtPromiseYear,"ColtPromiseYear");
	click(CeaseOrderObj.CeaseOrder.ColtPromiseYear,"ColtPromiseYear");
	sendKeys(CeaseOrderObj.CeaseOrder.ColtPromiseYear,Year.format(modifiedDate),"ColtPromiseYear");	


	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	Reusable.SendkeaboardKeys(CeaseOrderObj.CeaseOrder.ColtPromiseYear, Keys.ENTER);
	
	
	verifyExists(CeaseOrderObj.CeaseOrder.Update,"Update");
	click(CeaseOrderObj.CeaseOrder.Update,"Update");
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	}

public void DecomposeOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
{
    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order_URL");
	getUrl(CeaseOrderscreenURL);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Ceases Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.OrderTab,"OrderTab");
	click(CeaseOrderObj.CeaseOrder.OrderTab,"OrderTab");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Orders Tab");
	
	verifyExists(CeaseOrderObj.CeaseOrder.SelectDisconnectIpAccessOrder,"SelectDisconnectIpAccessOrder");
	click(CeaseOrderObj.CeaseOrder.SelectDisconnectIpAccessOrder,"SelectDisconnectIpAccessOrder");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Disconnect IP Access Product Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
	click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Decompose button");
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	
	verifyExists(CeaseOrderObj.CeaseOrder.Suborder,"Suborder");
	click(CeaseOrderObj.CeaseOrder.Suborder,"Suborder");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Disconnect Ethernet Connection Product Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
	click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Decompose button");
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	getUrl(CeaseOrderscreenURL);
}



public void ProcessOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
{
    String CeaseOrdernumber= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order Number");

	verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
	click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Orders Tab");
	
	verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
	
	ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/parent::*/td//input",10);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Cease Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
	click(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Start Processing button");
}

public void CompleteOrder(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
{
    String CeaseOrdernumber= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order Number");
    String WorkItem= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "WorkItem");
    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order_URL");


	String[] arrOfStr2 = CeaseOrdernumber.split("#", 0);
	
	verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	
	ClickonElementByString("//a/span[contains(text(),'\"+arrOfStr2[1]+\"')]/parent::*",10);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Cease Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
	click(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Tasks Tab");
	
	verifyExists(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
	click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Execution Flow link");
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	
	verifyExists(CeaseOrderObj.CeaseOrder.Workitems,"Workitems");
	click(CeaseOrderObj.CeaseOrder.Workitems,"Workitems");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Workitems Tab");
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	
	for (int k=1; k<=Integer.parseInt(WorkItem);k++){
		waitForElementToAppear(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,60,10000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Manual Workitems to be displayed");
		
		verifyExists(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,"TaskReadytoComplete");
		click(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,"TaskReadytoComplete");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Workitem in  Ready status");
	    Completeworkitem(GetText(CeaseOrderObj.CeaseOrder.DisconnectTaskTitle),dataFileName, SheetName, scriptNo, dataSetNo);

		}
	getUrl(CeaseOrderscreenURL);
	
	verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
	click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Orders Tab");
	
	verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();
	
	waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Disconnect Order to be Completed");
	Reusable.waitForAjax();
	Reusable.waitForpageloadmask();

}

public void Completeworkitem(String[] taskname,String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException  
{ 
	//Log.info("In Switch case with TaskName :"+taskname);
	switch(taskname[0])
	{
		case "Legacy Deactivation Completed":
		{
			verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
			click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
			Reusable.waitForAjax();
		}
		break;
		
		case "Waiting for Hard Cease Date for L3":
		{
			verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
			click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
			Reusable.waitForAjax();
		}
		break;
		
		case "Waiting for RIR Process Completion":
		{
			verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
			click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
			Reusable.waitForAjax();
		}
		case "Release\\Reserve LAN Range":
        {
        	  selectByVisibleText(CeaseOrderObj.CeaseOrder.ReleaseLanRange,"Yes","ReleaseLanRange");
              verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
          	  click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
          	  Reusable.waitForAjax();
        }
		break;
		default:
			// Nothing Found it will try to complete the Task	
			verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
			click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
	}
	
}

public void GotoErrors(String dataFileName,String SheetName,String scriptNo,String dataSetNo) throws Exception
{
    String CeaseOrdernumber= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order Number");
    String CeaseOrderscreenURL= DataMiner.fngetcolvalue(dataFileName, SheetName, scriptNo, dataSetNo, "Order_URL");

	String[] arrOfStr2 = CeaseOrdernumber.split("#", 0);
	getUrl(CeaseOrderscreenURL);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Order");
	
	verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
	click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Accounts Composite Orders Tab");
	
	verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
	click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");

	String Errors=getTextFrom("//a/span[contains(text(),'\"+arrOfStr2[1]+\"')]/parent::*/parent::*/following-sibling::*[4]");
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Get the value of In Error column for the order");
	if(Errors.equalsIgnoreCase("Blocked by Errors"))
	{
		ClickonElementByString("//a/span[contains(text(),'\"+arrOfStr2[1]+\"')]/parent::*",10);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Composite Order");
		
		verifyExists(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
		click(CeaseOrderObj.CeaseOrder.TaskTab,"TaskTab");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Tasks Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
		click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Execution Flow link");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		verifyExists(CeaseOrderObj.CeaseOrder.Errors,"Errors");
		click(CeaseOrderObj.CeaseOrder.Errors,"Errors");
		ExtentTestManager.getTest().log(LogStatus.FAIL, " Step: Click on Errors Tab");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		}
	else 
	{
		//Log.info("Not required to Navigate to Errors tab");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order did not have any errors to be Captured");
	}
}
	

}



