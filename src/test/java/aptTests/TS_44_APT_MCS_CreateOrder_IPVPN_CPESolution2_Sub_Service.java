package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_MCS_CreateOrder_IPVPN;
import testHarness.aptFunctions.APT_MCS_CreateOrder_IPVPN_CPE_SolutionL2;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_44_APT_MCS_CreateOrder_IPVPN_CPESolution2_Sub_Service extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	APT_Login Login=new APT_Login();
	APT_MCS_CreateOrder_IPVPN_CPE_SolutionL2 CreateOrderIPVPN = new APT_MCS_CreateOrder_IPVPN_CPE_SolutionL2();
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void APT_MCS_CreateOrder_IPVPNSecSubService(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)throws IOException, InterruptedException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);
	
	try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
	{

        // load a properties file
        prop.load(input);               

    }
	catch (IOException ex) 
	{
        ex.printStackTrace();
    }


	String dataFileName = prop.getProperty("TestDataSheet");
	File path = new File("./TestData/"+dataFileName);
	String testDataFile = path.toString();
	
	String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "New_Customer");
	String Service_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
	String Servicesubtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceSubType");
	String VPNName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VPN_Name");
	String VPNAlis = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VPN_Alis");
	String ManageUserName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManageUserName");
	String EditUserName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditUserName");
	//String VPNSiteOrderHeader=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VPNSiteOrderHeader");
	String DSLsiteOrder=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DSLsiteOrder");
  	
	try
	{
		openurl(Configuration.APT_URL);
		Login.APT_VoiceService();
		/*if(newCustomerName.equalsIgnoreCase("Yes")) {
			CreateOrderIPVPN.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);		
			
			}else {
			
			CreateOrderIPVPN.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				
			}
			*/
		CreateOrderIPVPN.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);		
		CreateOrderIPVPN.createneworderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifyservicetypeandSubtype(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.createservice(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);	
		
		Report.LogInfo("INFO", "=====verifyorderpanelinformation_Neworder====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifyorderpanelinformation_Neworder(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====verifyorderpanel_editorder====", "PASS");
		CreateOrderIPVPN.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====verifyorderpanel_changeorder====", "PASS");
		CreateOrderIPVPN.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);	
		
		Report.LogInfo("INFO", "=====verifyservicepanel_links====", "PASS");
		CreateOrderIPVPN.verifyservicepanel_links(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====Editservice====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.Editservice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====syncservices====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.syncservices(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.Dumpservice();
		
		Report.LogInfo("INFO", "=====verifyManagementOptionspanel====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifyManagementOptionspanel(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====AddManageUser====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddManageUser(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====ViewManageUser====", "PASS");
		CreateOrderIPVPN.ViewManageUser(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====EditManageUser====", "PASS");
		CreateOrderIPVPN.EditManageUser(testDataFile, dataSheet, scriptNo, dataSetNo,ManageUserName,EditUserName);
		
		Report.LogInfo("INFO", "=====DeleteManageUser====", "PASS");
		CreateOrderIPVPN.DeleteManageUser(Servicesubtype,EditUserName);
		
		//Report.LogInfo("INFO", "=====AddVPNAlis====", "PASS");
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.AddVPNAlis(testDataFile, dataSheet, scriptNo, dataSetNo,Servicesubtype,VPNName,VPNAlis);
		
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.EditVPNAlis(testDataFile, dataSheet, scriptNo, dataSetNo,Servicesubtype,VPNName,VPNAlis);
		
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.DeleteVPNAlis(Servicesubtype,VPNName,VPNAlis);
		
		Report.LogInfo("INFO", "=====Add VPN Site Order====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddVPNSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				
		Report.LogInfo("INFO", "=====VerifyVPNSiteOrder====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.VerifyVPNSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			
		Report.LogInfo("INFO", "=====AddNewDevice====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddNewDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			
		
		Report.LogInfo("INFO", "=====DeleteVPNSiteOrder====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.DeleteVPNSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.DeleteVPNSiteOrder4(testDataFile, dataSheet, scriptNo, dataSetNo);
	}
	catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();
	}

	
}
