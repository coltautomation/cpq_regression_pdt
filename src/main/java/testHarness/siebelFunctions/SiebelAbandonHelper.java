package testHarness.siebelFunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelCancelHelperObj;
import pageObjects.siebelObjects.SiebelManualValidationObj.Validation;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;



public class SiebelAbandonHelper extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	
	SiebelReusableFunctions Reusable1 = new SiebelReusableFunctions();
	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelManualValidation Validation = new SiebelManualValidation();
	
	SiebelMode siebelmod = new SiebelMode();
	
	public void statusReason(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception
	{
		String StatusReason = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Reason");
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
		waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.statusReasonDropDown, 10);
		click(SiebelCancelHelperObj.CancelHelper.statusReasonDropDown,"Status Reason drop down");
		Reusable.waitForSiebelLoader();
		click(SiebelCancelHelperObj.CancelHelper.dropDownValue.replace("Value", StatusReason));
		System.out.println("status reason");
		Reusable.waitForSiebelLoader();
		Reusable.SendkeaboardKeys(SiebelCancelHelperObj.CancelHelper.statusReasonField, Keys.ENTER);
		Reusable.waitForSiebelLoader();
		Reusable.SendkeaboardKeys(SiebelCancelHelperObj.CancelHelper.statusReasonField, Keys.TAB);
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
	}

	public void CancelAbandonOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");

	
		waitToPageLoad();
		Reusable.waitForSiebelLoader();
		
		waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.orderStatus, 10);
		verifyExists(SiebelCancelHelperObj.CancelHelper.orderStatus, "Order Status Dropdown");
		click(SiebelCancelHelperObj.CancelHelper.orderStatus,"Order Satus");
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		if (Comments.contains("Cancel")) 
		{
			click(SiebelCancelHelperObj.CancelHelper.orderCancelled,"Order Cancelled");
			Reusable.waitForSiebelLoader();
			click(SiebelCancelHelperObj.CancelHelper.orderComplete,"Order Complete");
			Reusable.waitForSiebelLoader();
			Reusable.waitForSiebelSpinnerToDisappear();
		}
		else 
		{
			click(SiebelCancelHelperObj.CancelHelper.orderAbandoned,"Order Abandoned");
			
			if (Product_Type.equals("IP Access")) 
			{
																						   
				click(SiebelCancelHelperObj.CancelHelper.IPAccessAbandoned,"IPAccess Abandoned");
				Reusable.waitForSiebelSpinnerToDisappear();
			}
			
		}
	
		
	/*	
		if (Product_Type.equals("IP Access")) 
		{
																					   
			click(SiebelCancelHelperObj.CancelHelper.orderComplete,"Order Complete");
			Reusable.waitForSiebelLoader();
		}
	*/	
		

	
	}
	
	public void verifyIPVPNOrderAbandoned(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception
	{
	String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
	String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");

	String SiteServiceOrderNumber = null;
	
		SiteServiceOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
	
	Reusable.waitForSiebelLoader();
	waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceTab, 10);
	javaScriptclick(SiebelCancelHelperObj.CancelHelper.serviceTab);
	Reusable.waitForSiebelSpinnerToDisappear();
	verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
	click(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");

	Reusable.waitForSiebelLoader();
	Reusable.waitForSiebelSpinnerToDisappear();
	sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,SiteServiceOrderNumber,"Service OrderReference Number");
	Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceOrderArrow, 10);
	click(SiebelCancelHelperObj.CancelHelper.serviceOrderArrow, "Service Order Arrow");
	Reusable.waitForSiebelSpinnerToDisappear();

	if (Comments.contains("Cancel")) 
	{
		String expected = getTextFrom(SiebelCancelHelperObj.CancelHelper.verifyCancelled);
		System.out.println(expected);
		String actual="Cancelled";
		Assert.assertEquals(expected, actual);  
	}
	else
	{
		String expected = getTextFrom(SiebelCancelHelperObj.CancelHelper.verifyAbandoned);
		System.out.println(expected);
		String actual="Abandoned";
	    Assert.assertEquals(expected, actual);
	}
}

	
	public void verifyOrderAbandonedorCancelled(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception
	{
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");

		String SiteServiceOrderNumber = null;
		if (Product_Type.toString().equals("IP VPN Service"))
		{
			SiteServiceOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteOrderReference_Number");
		}
		else
		{
			SiteServiceOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		}
		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceTab, 10);
		javaScriptclick(SiebelCancelHelperObj.CancelHelper.serviceTab);
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Click on Input Service Order");

//		Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceOrderSearch, 10);
//		click(SiebelCancelHelperObj.CancelHelper.serviceOrderSearch, "Service Order Search");
  
//		waitForAjax();
//		Reusable.waitForSiebelLoader();
//		sendKeys(SiebelCancelHelperObj.CancelHelper.serviceOrderSearch, "Service Order Search");

		Reusable.waitForSiebelLoader();
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,SiteServiceOrderNumber,"Service OrderReference Number");
		Reusable.waitForElementToAppear(SiebelCancelHelperObj.CancelHelper.serviceOrderArrow, 10);
		click(SiebelCancelHelperObj.CancelHelper.serviceOrderArrow, "Service Order Arrow");
		Reusable.waitForSiebelSpinnerToDisappear();
	
		if (Comments.contains("Cancel")) 
		{
			String expected = getTextFrom(SiebelCancelHelperObj.CancelHelper.verifyCancelled);
			System.out.println(expected);
			String actual="Cancelled";
			Assert.assertEquals(expected, actual);  
		}
		else
		{
			String expected = getTextFrom(SiebelCancelHelperObj.CancelHelper.verifyAbandoned);
			System.out.println(expected);
			String actual="Abandoned";
		    Assert.assertEquals(expected, actual);
		}
	}

	public void CompletedValidationforCancel(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String SamleName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		String ColName74 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String ServOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		
		Reusable.savePage();
	
		verifyExists(SiebelAddProdcutObj.CompletedValidation.ClickLink.replace("Value", "Customer Orders"));
		javaScriptclick(SiebelAddProdcutObj.CompletedValidation.ClickLink.replace("Value", "Customer Orders"));
		Reusable.waitForSiebelLoader();
					
		
		
		String[] parts = ServOrder.split("/");
		String part1 = parts[0];
		String part2 = parts[1];
		Reusable.waitForSiebelLoader();
		
		sendKeys(SiebelAddProdcutObj.CompletedValidation.CustOrder, part2);
		click(SiebelAddProdcutObj.CompletedValidation.CustOrderGo,"CustOrderGo");
		Reusable.waitForSiebelLoader();
		
		click(SiebelAddProdcutObj.CompletedValidation.ClickSeibelOrder,"Click Seibel Order");
		Reusable.waitForSiebelLoader();
		
		click(SiebelAddProdcutObj.CompletedValidation.NewServiceOrder,"New Service Order");
		Reusable.waitForSiebelLoader();
		
		Product.Selectproduct("IP VPN Site");
		
		
		Product.openIPVPNSite(testDataFile, sheetName, scriptNo,dataSetNo);
		
		siebelmod.enterMandatoryFieldsInHeader(testDataFile,sheetName,scriptNo, dataSetNo);
		
		Product.NetworkReferenceFill(testDataFile, sheetName, scriptNo, dataSetNo);
		
		Product.IPVPNSITEMiddleApplet(testDataFile,sheetName,scriptNo, dataSetNo);
		
		siebelmod.EnterDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		siebelmod.EnterBillingDateInFooter(testDataFile, sheetName, scriptNo, dataSetNo);
		if (!SamleName.equals("IP VPN Wholesale")) 
		{
			Product.ServiceChargeforIPVPNSite(SamleName,testDataFile, sheetName, scriptNo, dataSetNo, "2");
		}
		if (!SamleName.equals("IP VPN Wholesale")) 
		{
			Product.OperationalAttributesforIPVPN(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		Product.EnterInstallationChargeInFooter(testDataFile, sheetName,  scriptNo, dataSetNo);
		Product.CommercialValidation(testDataFile, sheetName,  scriptNo, dataSetNo);
	
		Product.TechnicalValidation(testDataFile, sheetName,  scriptNo, dataSetNo);
		Product.DeliveryValidation(testDataFile, sheetName,  scriptNo, dataSetNo);
		Reusable.alertPopUp();
		Validation.clickOnManualValidationA();

	
	}

	
	
	public void AbandonedforIPVPN(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception
	{

		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String SamleName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		String ColName74 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
		String username = Configuration.CEOS_Username;
		String password = Configuration.CEOS_Password;
		
		SiebelMode Siebelmod = new SiebelMode();
		SiebelAddProduct Product = new SiebelAddProduct();


	

		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	
		String ServOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		
		Reusable.savePage();
		Reusable.waitForSiebelLoader();

		verifyExists(SiebelAddProdcutObj.CompletedValidation.ClickLink.replace("Value", "Customer Orders"));
		click(SiebelAddProdcutObj.CompletedValidation.ClickLink.replace("Value", "Customer Orders"));
		Reusable.waitForSiebelLoader();
					
		
		String[] parts = ServOrder.split("/");
		String part1 = parts[0];
		String part2 = parts[1];
		Reusable.waitForSiebelLoader();
		
		sendKeys(SiebelAddProdcutObj.CompletedValidation.CustOrder, part2);
		click(SiebelAddProdcutObj.CompletedValidation.CustOrderGo,"CustOrderGo");
		Reusable.waitForSiebelLoader();
		
		click(SiebelAddProdcutObj.CompletedValidation.ClickSeibelOrder,"Click Seibel Order");
		Reusable.waitForSiebelLoader();
		
		click(SiebelAddProdcutObj.CompletedValidation.NewServiceOrder,"New Service Order");
		Reusable.waitForSiebelLoader();
		
		Product.Selectproduct("IP VPN Site");
		Product.openIPVPNSite(testDataFile, sheetName, scriptNo,dataSetNo);
		Siebelmod.enterMandatoryFieldsInHeader(testDataFile,sheetName,scriptNo, dataSetNo);
		Product.NetworkReferenceFill(testDataFile, sheetName, scriptNo, dataSetNo);
	}
	
	public void OpenServiceOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception 
	{
		String ServiceOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");

//		String SiteServiceOrderNumber = null;
//		SiteServiceOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceOrderReference_Number");
		Reusable.waitForSiebelLoader();
		try
		{
			verifyExists(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			click(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
		} 
		catch (Exception e) 
		{
			try 
			{
				javaScriptclick(SiebelModeObj.ServiceTab.ServiceOrderTab,"Click on Service Order Tab ");
			} 
			catch (Exception e1) 
			{
				e1.printStackTrace();
			}
		}
		Reusable.waitForSiebelLoader();
		waitToPageLoad();
		verifyExists(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		click(SiebelModeObj.ServiceTab.InputServiceOrder,"Input Service Order");
		sendKeys(SiebelModeObj.ServiceTab.InputServiceOrder,ServiceOrderNumber,"Service OrderReference Number");
		verifyExists(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		click(SiebelModeObj.ServiceTab.ServiceOrderGo,"Service Order Go");
		Reusable.waitForSiebelLoader();

		Reusable.waitForSiebelLoader();
		waitForElementToAppear(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo, 10);
		verifyExists(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		click(SiebelAddProdcutObj.serviceOrder.ServiceOrderReferenceNo,"Service Order Reference Number");
		Reusable.waitForSiebelLoader();		
	}
	
}
