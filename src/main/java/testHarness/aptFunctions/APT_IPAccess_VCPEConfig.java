package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.aptObjects.APT_IPAccessResilientConfigObj;
import pageObjects.aptObjects.APT_IPAccess_VCPEConfigObj;

public class APT_IPAccess_VCPEConfig extends SeleniumUtils {

	APT_IPAccessResilientConfig IPConfig = new APT_IPAccessResilientConfig();
	APT_IPAccessNoCPEHelper noCPE = new APT_IPAccessNoCPEHelper();
	
	public void verifyL2Technology(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_panelheader, "L2 Technology");
		
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown, "L2technology_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown, "L2technology_actiondropdown");

		//Thread.sleep(1000);
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuitwizardlink, "addL2circuitwizardlink");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuitwizardlink, "addL2circuitwizardlink");

		//click_commonMethod("Add L2 circuit wizard", "addL2circuitwizardlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//if(getwebelement(xml.getlocator("//locators/" + application + "/addL2circuit_modal")).isDisplayed()) {
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_modal)){
			compareText("L2 Circuit header", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "L2 Circuit");
			
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea, "addL2circuit_remarktextarea");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea,addL2circuit_remarkValue);
			
			//addtextFields_commonMethod("Remark", "addL2circuit_remarktextarea", addL2circuit_remarkValue);
			
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield, "addL2circuit_circuitreference_textfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield,circuitreference_value);
			
			//addtextFields_commonMethod("Circuit Reference", "addL2circuit_circuitreference_textfield", circuitreference_value);
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button, "OK_button");
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button, "OK_button");

			//click_commonMethod("OK", "OK_button");
			//Thread.sleep(2000);
			waitforPagetobeenable();
			IPConfig.verifysuccessmessage("Circuit successfully created.");
			waitforPagetobeenable();

			//verify added circuit
			//Thread.sleep(1000);
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
			//Thread.sleep(1000);
			compareText("Remark column header", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkcolumnheader, "Remark");
			compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkvalue, addL2circuit_remarkValue);
			//Thread.sleep(2000);

		}
	}
	
	public void verifyViewL2Circuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox, "addedL2circuit_checkbox");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox, "addedL2circuit_checkbox");

		//click_commonMethod("Added L2 circuit", "addedL2circuit_checkbox");
		//Thread.sleep(1000);
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown, "L2technology_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown, "L2technology_actiondropdown");

		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_viewlink, "L2technology_viewlink");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_viewlink, "L2technology_viewlink");

		//Thread.sleep(2000);
		waitforPagetobeenable();
		scrollIntoTop();
		//GetText("Circuits header", "viewcircuits_header");
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.configoptions_l2technology, L2technology_dropdownvalue);
		compareText("Circuit Reference", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_circuitreferencevalue, circuitreference_value);
		compareText("Remarks", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_remarkvalue, addL2circuit_remarkValue);
		//verify Edit link in view circuits page
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_actiondropdown, "viewcircuits_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_actiondropdown, "viewcircuits_actiondropdown");

		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_editlink, "viewcircuits_editlink");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_editlink, "viewcircuits_editlink");

		//Thread.sleep(1000);
		waitforPagetobeenable();
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.editcircuits_modalheader)){
			Report.LogInfo("INFO", "Step: Navigated to Edit L2 Circuits page as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to Edit L2 Circuits page as expected");
			
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.popup_close, "popup_close");
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.popup_close, "popup_close");

		}
		else
		{
			Report.LogInfo("INFO", "Step: Didn't navigate to Edit L2 Circuits page as expected", "FAIL");

			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to Edit L2 Circuits page as expected");
		}
		//Thread.sleep(1000);
	}
	
	public void verifySiteA(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String SiteA_DeviceCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_DeviceCountry");
		String siteA_existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_ExistingCity");
		String siteA_newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_NewCity");
		String SiteA_cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_CityName");
		String SiteA_Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_CityCode");
		String SiteA_existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_ExistingCityValue");
		String SiteA_physicalsite_dropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_PhysicalSiteDropdown");
		String SiteA_physicalsite_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_PhysicalSiteDropdownValue");
		String siteA_CSRName_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_CSRNameValue");
		String siteA_vlanid_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_VLANIDValue");
		String siteA_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Remarks");
		String siteA_devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_DeviceName");
		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String edit_siteA_vlanidvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteA_VLANIDValue");
		String edit_siteA_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteA_Remarks");
		String edit_siteA_devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteA_DeviceName");
		
		//Add Site A
		compareText("SITE A header", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteAheader, "SITE A");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_actiondropdown,"siteA_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_addlink,"siteA_addlink");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"Ok");

		//verify warning messages
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicecountry, "Device Country");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicexngcity, "Device Xng City");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_csrname, "CSR Name");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicename, "Device Name");


		selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.countryinput, "Device Country", SiteA_DeviceCountry);

		//New City
		if(siteA_existingcity.equalsIgnoreCase("no") & siteA_newcity.equalsIgnoreCase("yes")) {
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectcity,"circuitsite_selectcity");
			//City name
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield, "citynameinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield,SiteA_cityname);
					
			//City Code	
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield, "citycodeinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield,SiteA_Citycode);
		}	

		//Existing City	
		else if(siteA_existingcity.equalsIgnoreCase("yes") & siteA_newcity.equalsIgnoreCase("no")) {

			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.citydropdowninput, "Device Xng City", SiteA_existingcityvalue);

		} 

		if(SiteA_physicalsite_dropdown.equalsIgnoreCase("yes") & siteA_existingcity.equalsIgnoreCase("yes"))
		{
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectsite,"circuitsite_selectsite");
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.physicalsite_dropdown, "Physical Site", SiteA_physicalsite_dropdownvalue);
		}
		else
		{
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field, "csrname_field");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field,siteA_CSRName_value);
		}

		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, "vlanid_field");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field,siteA_vlanid_value);
		
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, "remarks_textarea");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea,siteA_remarks);
		
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_devicename, "circuitsite_devicename");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_devicename,siteA_devicename);

		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Site Order successfully created.");
		//Thread.sleep(1000);

		//view SiteA details
		compareText("Site A", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteA_header, "Site A");
		compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_remarkvalue, siteA_remarks);
		//String DeviceCountryValue= GetText("Device Country", "viewsite_devicecountry");
		String DeviceCountryValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecountry,"view site_device country");
		if(SiteA_DeviceCountry.contains(DeviceCountryValue)) {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCountryValue, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		else {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCountryValue, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		//String DeviceCityValue= GetText("Device Xng City", "viewsite_devicecity");
		String DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecity,"view site_device city");

		if(siteA_existingcity.equalsIgnoreCase("yes")) {
			if(SiteA_existingcityvalue.contains(DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		else
		{
			if(SiteA_cityname.contains(DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_L2technologyvalue, L2technology_dropdownvalue);
		//GetText("Protected", "viewsite_protectedvalue");
		compareText("Device Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicename, siteA_devicename);
		//Thread.sleep(1000);
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		waitforPagetobeenable();
		//waitforPagetobeenable();

		//verify siteA table columns
		compareText("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_namecolumn, "Name");
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecountry, "Device Country");
		compareText("Device Xing City", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecity, "Device Xing City");
		compareText("Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_technology, "Technology");

		//verify siteA table values
		compareText("Name value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_namevalue, "Site A");
		//verify country value
		String DeviceCountry= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecountryvalue,"site A_device country value");
		if(SiteA_DeviceCountry.contains(DeviceCountry)) {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCountry, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCountry);
		}
		else {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCountry, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+DeviceCountry);
		}
		//verify city value
		String DeviceCity= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecityvalue,"site A_device city value");
		if(siteA_existingcity.equalsIgnoreCase("yes")) {
			if(SiteA_existingcityvalue.contains(DeviceCity)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+DeviceCity, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+DeviceCity);
			}
		}
		else
		{
			if(SiteA_cityname.contains(DeviceCity)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+DeviceCity, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+DeviceCity);
			}
		}
		compareText("Technology value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_technologyvalue, L2technology_dropdownvalue);
		//Thread.sleep(1000);

		//Edit Site
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_actiondropdown,"siteA_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_editlink,"siteA_editlink");
		//Thread.sleep(1000);
		//compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecountryvalue, SiteA_DeviceCountry);
		//verify city field in edit site page
		String EditPage_DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecityvalue,"edit site_device city value");
		if(siteA_existingcity.equalsIgnoreCase("yes")) {
			if(SiteA_existingcityvalue.contains(EditPage_DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		else
		{
			if(SiteA_cityname.contains(EditPage_DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		//verify site field in edit site page	
		String EditPage_PhysicalSiteValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_physicalsitevalue,"edit site_physical site value");
		if(SiteA_physicalsite_dropdown.equalsIgnoreCase("yes")) {
			if(SiteA_physicalsite_dropdownvalue.contains(EditPage_PhysicalSiteValue)) {
				Report.LogInfo("INFO", "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}
		else
		{
			if(siteA_CSRName_value.contains(EditPage_PhysicalSiteValue)) {
				Report.LogInfo("INFO", "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}

		edittextFields_commonMethod("VLAN Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, edit_siteA_vlanidvalue);
		edittextFields_commonMethod("Remarks", APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, edit_siteA_remarks);
		edittextFields_commonMethod("Device Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_devicename, edit_siteA_devicename);
		//Thread.sleep(1000);
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"OK");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Site Order successfully updated.");
		//Thread.sleep(1000);
		scrollIntoTop();
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		//waitforPagetobeenable();
		waitforPagetobeenable();

		//verify view siteA link
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_actiondropdown,"siteA_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_viewlink,"siteA_viewlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Site A", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteA_header, "Site A");
		//Thread.sleep(1000);
		scrollIntoTop();
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		waitforPagetobeenable();
		//waitforPagetobeenable();
	}
	
	public void verifySiteB(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
		
		String SiteB_DeviceCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_DeviceCountry");
		String siteB_existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_ExistingCity");
		String siteB_newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_NewCity");
		String SiteB_cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_CityName");
		String SiteB_Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_CityCode");
		String SiteB_existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_ExistingCityValue");
		String SiteB_physicalsite_dropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_PhysicalSiteDropdown");
		String SiteB_physicalsite_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_PhysicalSiteDropdownValue");
		String siteB_CSRName_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_CSRNameValue");
		String siteB_vlanid_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_VLANIDValue");
		String siteB_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Remarks");
		String siteB_devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_DeviceName");
		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String edit_siteB_vlanidvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteB_VLANIDValue");
		String edit_siteB_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteB_Remarks");
		String edit_siteB_devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteB_DeviceName");
		
		//Add Site B
		compareText("SITE B header", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteBheader, "SITE B");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_actiondropdown,"siteB_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_addlink,"siteB_addlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");

		//verify warning messages
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicecountry, "Device Country");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicexngcity, "Device Xng City");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_csrname, "CSR Name");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicename, "Device Name");


		selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.countryinput, "Device Country", SiteB_DeviceCountry);

		//New City	
		if(siteB_existingcity.equalsIgnoreCase("no") & siteB_newcity.equalsIgnoreCase("yes")) {
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectcity,"circuitsite_selectcity");
			//City name
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield, "citynameinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield,SiteB_cityname);

			//City Code	
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield, "citycodeinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield,SiteB_Citycode);
		}	

		//Existing City	
		else if(siteB_existingcity.equalsIgnoreCase("yes") & siteB_newcity.equalsIgnoreCase("no")) {

			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.citydropdowninput, "Device Xng City", SiteB_existingcityvalue);

		} 

		if(SiteB_physicalsite_dropdown.equalsIgnoreCase("yes") & siteB_existingcity.equalsIgnoreCase("yes"))
		{
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectsite,"circuitsite_selectsite");
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.physicalsite_dropdown, "Physical Site", SiteB_physicalsite_dropdownvalue);
		}
		else
		{
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field, "csrname_field");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field,siteB_CSRName_value);

			//addtextFields_commonMethod("CSR Name", "csrname_field", siteB_CSRName_value);
		}

		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, "vlanid_field");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field,siteB_vlanid_value);
		
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, "remarks_textarea");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea,siteB_remarks);
		
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_devicename, "circuitsite_devicename");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_devicename,siteB_devicename);

		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Site Order successfully created.");
		//Thread.sleep(1000);

		//view SiteB details
		compareText("Site B", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteB_header, "Site B");
		compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_remarkvalue, siteB_remarks);
		String DeviceCountryValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecountry,"view site_device country");
		if(SiteB_DeviceCountry.contains(DeviceCountryValue)) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		else {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCountryValue, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		String DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecity,"view site_device city");
		if(siteB_existingcity.equalsIgnoreCase("yes")) {
			if(SiteB_existingcityvalue.contains(DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		else
		{
			if(SiteB_cityname.contains(DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_L2technologyvalue, L2technology_dropdownvalue);
		//GetText("Protected", "viewsite_protectedvalue");
		compareText("Device Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicename, siteB_devicename);
		//Thread.sleep(1000);
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		//waitForpageload();
		waitforPagetobeenable();

		//verify siteB table columns
		compareText("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_namecolumn, "Name");
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecountry, "Device Country");
		compareText("Device Xing City", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecity, "Device Xing City");
		compareText("Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_technology, "Technology");

		//verify siteB table values
		compareText("Name value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_namevalue, "Site B");
		//verify country value
		String SiteB_DeviceCountryValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecountryvalue,"site B_device country value");
		if(SiteB_DeviceCountry.contains(SiteB_DeviceCountryValue)) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+SiteB_DeviceCountryValue);
		}
		else {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+SiteB_DeviceCountryValue, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+SiteB_DeviceCountryValue);
		}
		//verify city value
		String SiteB_DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecityvalue,"site B_device city value");
		if(siteB_existingcity.equalsIgnoreCase("yes")) {
			if(SiteB_existingcityvalue.contains(SiteB_DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+SiteB_DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+SiteB_DeviceCityValue);
			}
		}
		else
		{
			if(SiteB_cityname.contains(SiteB_DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+SiteB_DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+SiteB_DeviceCityValue);
			}
		}

		compareText("Technology value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_technologyvalue, L2technology_dropdownvalue);
		//Thread.sleep(1000);

		//Edit Site
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_actiondropdown,"siteB_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_editlink,"siteB_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecountryvalue, SiteB_DeviceCountry);
		//verify city field in edit site page
		String EditPage_DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecityvalue,"edit site_device city value");
		if(siteB_existingcity.equalsIgnoreCase("yes")) {
			if(SiteB_existingcityvalue.contains(EditPage_DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		else
		{
			if(SiteB_cityname.contains(EditPage_DeviceCityValue)) {
				Report.LogInfo("INFO", "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		//verify site field in edit site page	
		String EditPage_PhysicalSiteValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_physicalsitevalue,"edit site_physical site value");
		if(SiteB_physicalsite_dropdown.equalsIgnoreCase("yes")) {
			if(SiteB_physicalsite_dropdownvalue.contains(EditPage_PhysicalSiteValue)) {
				Report.LogInfo("INFO", "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}
		else
		{
			if(siteB_CSRName_value.contains(EditPage_PhysicalSiteValue)) {
				Report.LogInfo("INFO", "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}

		edittextFields_commonMethod("VLAN Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, edit_siteB_vlanidvalue);
		edittextFields_commonMethod("Remarks", APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, edit_siteB_remarks);
		edittextFields_commonMethod("Device Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_devicename, edit_siteB_devicename);
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Site Order successfully updated.");
		//Thread.sleep(1000);
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		waitforPagetobeenable();

		//verify view siteB link
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_actiondropdown,"siteB_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_viewlink,"siteB_viewlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Site B", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteB_header, "Site B");
		//Thread.sleep(1000);
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewpage_backbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewpage_backbutton,"viewpage_backbutton");
		//Thread.sleep(1000);
	}
	
	public void verifyEditL2Circuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String editL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_L2Circuit_RemarkValue");
		String edit_circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_CircuitReference_Value");
		
		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox,"addedL2circuit_checkbox");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_editlink,"L2technology_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
	//	if(getwebelement(xml.getlocator("//locators/" + application + "/addL2circuit_modal")).isDisplayed()) {
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_modal)){
			compareText("L2 Circuit header", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "L2 Circuit");
			edittextFields_commonMethod("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea, editL2circuit_remarkValue);
			edittextFields_commonMethod("Circuit Reference", APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield, edit_circuitreference_value);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(2000);
			waitforPagetobeenable();
			IPConfig.verifysuccessmessage("Circuit successfully updated.");
		}

	}
	
	public void verifyActelisL2Technology(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		String Actelis_newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_NewCity");
		String Actelis_cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_CityName");
		String Actelis_DeviceCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_DeviceCountry");
		String Actelis_existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingCity");
		String Actelis_Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_CityCode");
		String Actelis_sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_SiteName");
		String Actelis_sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_SiteCode");
		String Actelis_existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingSiteValue");
		String Actelis_existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingSite");
		String Actelis_newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_NewSite");
		String Actelis_vlanidValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_VLANIDValue");
		String Actelis_existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingCityValue");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_panelheader, "L2 Technology");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuitwizardlink,"addL2circuitwizardlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//if(getwebelement(xml.getlocator("//locators/" + application + "/addL2circuit_modal")).isDisplayed()) {
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_modal)){	
			compareText("L2 Circuit header", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "L2 Circuit");
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK");
			//Thread.sleep(2000);
			//verify warning messages
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_warngmsg_circuitreference, "Circuit Reference");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_warngmsg_devicecountry, "Device Country");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_warngmsg_devicecity, "Device Xng City");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_warngmsg_physicalsite, "Physical Site");
			
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea, "addL2circuit_remarkValue");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea,addL2circuit_remarkValue);
			
			//addtextFields_commonMethod("Remark", "addL2circuit_remarktextarea", addL2circuit_remarkValue);
			
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield, "addL2circuit_circuitreference_textfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield,circuitreference_value);
			
			//addtextFields_commonMethod("Circuit Reference", "addL2circuit_circuitreference_textfield", circuitreference_value);

			addDropdownValues_commonMethod_ForSpantag("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_Countrydropdown, Actelis_DeviceCountry);
			//New City		
			if(Actelis_existingcity.equalsIgnoreCase("no") & Actelis_newcity.equalsIgnoreCase("yes")) {
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.Actelis_addcityswitch,"Actelis_addcityswitch");
				//City name
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield, "citynameinputfield");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield,Actelis_cityname);
			
				//addtextFields_commonMethod("City Name", "citynameinputfield", Actelis_cityname);
				//City Code	
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield, "citycodeinputfield");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield,Actelis_Citycode);
			
				//addtextFields_commonMethod("City Code", "citycodeinputfield", Actelis_Citycode);
				//Site name
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuite_sitename, "Acteliscircuite_sitename");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuite_sitename,Actelis_sitename);
			
				//addtextFields_commonMethod("Site Name", "Acteliscircuite_sitename", Actelis_sitename);
				//Site Code
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_sitecode, "Acteliscircuit_sitecode");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_sitecode,Actelis_sitecode);
			
				//addtextFields_commonMethod("Site Code", "Acteliscircuit_sitecode", Actelis_sitecode);
			}	

			//Existing City	
			else if(Actelis_existingcity.equalsIgnoreCase("yes") & Actelis_newcity.equalsIgnoreCase("no")) {

				addDropdownValues_commonMethod_ForSpantag("Device Xng City", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_Citydropdown, Actelis_existingcityvalue);

				//Existing Site
				if(Actelis_existingsite.equalsIgnoreCase("yes") & Actelis_newsite.equalsIgnoreCase("no")) {
					addDropdownValues_commonMethod_ForSpantag("Physical Site", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_Sitedropdown, Actelis_existingsitevalue);
				}

				else if(Actelis_existingsite.equalsIgnoreCase("no") & Actelis_newsite.equalsIgnoreCase("yes")) {

					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.Actelis_addsiteswitch,"Actelis_addsiteswitch");
					//Site name
					verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuite_sitename, "Acteliscircuite_sitename");
					sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuite_sitename,Actelis_sitename);
				
					//Site Code
					verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_sitecode, "Acteliscircuit_sitecode");
					sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_sitecode,Actelis_sitecode);
				
				}
			}

			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, "vlanid_field");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field,Actelis_vlanidValue);
		
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(2000);
			waitforPagetobeenable();
			IPConfig.verifysuccessmessage("Circuit successfully created.");
			waitforPagetobeenable();

			//verify added circuit
			//Thread.sleep(1000);
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
			//Thread.sleep(1000);
			compareText("Remark column header", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkcolumnheader, "Remark");
			compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkvalue, addL2circuit_remarkValue);
			//Thread.sleep(2000);

		}
	}
	
	
	public void verifyView_ActelisL2Circuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {


		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		String Actelis_DeviceCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_DeviceCountry");
		String Actelis_existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingCity");
		String Actelis_existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingCityValue");
		String Actelis_cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_CityName");
		String Actelis_existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingSite");
		String Actelis_existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_ExistingSiteValue");
		String Actelis_sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_SiteName");
		String Actelis_vlanidValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Actelis_VLANIDValue");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox,"addedL2circuit_checkbox");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_viewlink,"L2technology_viewlink");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		scrollIntoTop();
		//GetText("Circuits header", "viewcircuits_header");
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.configoptions_l2technology, L2technology_dropdownvalue);
		compareText("Circuit Reference", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_circuitreferencevalue, circuitreference_value);
		compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewActelis_remarkvalue, addL2circuit_remarkValue);
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewActelis_devicecountry, Actelis_DeviceCountry);
		if(Actelis_existingcity.equalsIgnoreCase("yes")) {
			compareText("Device Xng City", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewActelis_devicecity, Actelis_existingcityvalue);
		}
		else {
			compareText("Device Xng City", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewActelis_devicecity, Actelis_cityname);
		}

		if(Actelis_existingsite.equalsIgnoreCase("yes")) {
			compareText("Physical Site", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewActelis_physicalsite, Actelis_existingsitevalue);
		}
		else {
			compareText("Physical Site", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewActelis_physicalsite, Actelis_sitename);
		}

		compareText("VLAN Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewActelis_vlanid, Actelis_vlanidValue);

		//verify Edit link in view circuits page
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_actiondropdown,"viewcircuits_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_editlink,"viewcircuits_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//if(getwebelement(xml.getlocator("//locators/" + application + "/editcircuits_modalheader")).isDisplayed()) {
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.editcircuits_modalheader))	{
			Report.LogInfo("INFO", "Step: Navigated to Edit L2 Circuits page as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to Edit L2 Circuits page as expected");
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.popup_close,"popup_close");
		}
		else
		{
			Report.LogInfo("INFO", "Step: Didn't navigate to Edit L2 Circuits page as expected", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to Edit L2 Circuits page as expected");
		}
		//Thread.sleep(1000);
		waitforPagetobeenable();
	}
	
	public void verifyEdit_ActelisL2Circuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {

		String edit_Actelis_existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_ExistingCityValue");
		String editL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_L2Circuit_RemarkValue");
		String edit_circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_CircuitReference_Value");
		String edit_Actelis_DeviceCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_DeviceCountry");
		String edit_Actelis_newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_NewCity");
		String edit_Actelis_cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_CityName");
		String edit_Actelis_Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_CityCode");
		String edit_Actelis_sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_SiteName");
		String edit_Actelis_sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_SiteCode");
		String edit_Actelis_existingsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_ExistingSite");
		String edit_Actelis_existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_ExistingCity");
		String edit_Actelis_vlanidValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_VLANIDValue");
		String edit_Actelis_newsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"edit_Actelis_newsite");
		String edit_Actelis_existingsitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_Actelis_ExistingSiteValue");
		
		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox,"addedL2circuit_checkbox");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_editlink,"L2technology_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//if(getwebelement(xml.getlocator("//locators/" + application + "/addL2circuit_modal")).isDisplayed()) {
			if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_modal)){
			compareText("L2 Circuit header", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "L2 Circuit");
			edittextFields_commonMethod("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea, editL2circuit_remarkValue);
			edittextFields_commonMethod("Circuit Reference", APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield, edit_circuitreference_value);

			addDropdownValues_commonMethod_ForSpantag("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_Countrydropdown, edit_Actelis_DeviceCountry);
			//New City		
			if(edit_Actelis_existingcity.equalsIgnoreCase("no") & edit_Actelis_newcity.equalsIgnoreCase("yes")) {
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.Actelis_addcityswitch,"Actelis_addcityswitch");
				//City name
				edittextFields_commonMethod("City Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield, edit_Actelis_cityname);
				//City Code	
				edittextFields_commonMethod("City Code", APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield, edit_Actelis_Citycode);
				//Site name
				edittextFields_commonMethod("Site Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuite_sitename, edit_Actelis_sitename);
				//Site Code
				edittextFields_commonMethod("Site Code", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_sitecode, edit_Actelis_sitecode);
			}	

			//Existing City	
			else if(edit_Actelis_existingcity.equalsIgnoreCase("yes") & edit_Actelis_newcity.equalsIgnoreCase("no")) {
				addDropdownValues_commonMethod_ForSpantag("Device Xng City", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_Citydropdown, edit_Actelis_existingcityvalue);

				//Existing Site
				if(edit_Actelis_existingsite.equalsIgnoreCase("yes") & edit_Actelis_newsite.equalsIgnoreCase("no")) {
					addDropdownValues_commonMethod_ForSpantag("Physical Site", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_Sitedropdown, edit_Actelis_existingsitevalue);
				}

				else if(edit_Actelis_existingsite.equalsIgnoreCase("no") & edit_Actelis_newsite.equalsIgnoreCase("yes")) {

					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.Actelis_addsiteswitch,"Actelis_addsiteswitch");
					//Site name
					edittextFields_commonMethod("Site Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuite_sitename, edit_Actelis_sitename);
					//Site Code
					edittextFields_commonMethod("Site Code", APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_sitecode, edit_Actelis_sitecode);
				}
			}

			edittextFields_commonMethod("VLAN Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, edit_Actelis_vlanidValue);

			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(2000);
			waitforPagetobeenable();
			IPConfig.verifysuccessmessage("Circuit successfully updated.");
		}

	}
	
	public void verifyOvertureL2Technology(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {

		String existingManagementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingManagementOrder");
		String ManagementOrder_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManagementOrder_DropdownValue");
		String managementorder_fieldvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManagementOrder_FieldValue");
		String circuittype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitType_DropdownValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		String interfacespeed_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceSpeed_DropdownValue");
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		String ENNI_Checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ENNI_Checkbox");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_panelheader, "L2 Technology");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuitwizardlink,"addL2circuitwizardlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		try {
			//if(getwebelement(xml.getlocator("//locators/" + application + "/addL2circuit_modal")).isDisplayed()) {
			if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_modal)){

				compareText("L2 Circuit header", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "L2 Circuit");
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
				//Thread.sleep(2000);

				//verify warning messages
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.warngmsg_overture_circuittype, "Circuit Type");
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.warngmsg_overture_interfacespeed, "Interface Speed");
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_warngmsg_circuitreference, "Circuit Reference");

				if(existingManagementOrder.equalsIgnoreCase("Yes"))
				{
					addDropdownValues_commonMethod_ForSpantag("Management Order", APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_dropdown, ManagementOrder_dropdownvalue);
				}
				else
				{
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.selectorder_toggle,"selectorder_toggle");
					
					verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield, "managementorder_textfield");
					sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield,managementorder_fieldvalue);
					//addtextFields_commonMethod("Management Order", "managementorder_textfield", managementorder_fieldvalue);
				}
				addDropdownValues_commonMethod_ForSpantag("Circuit Type", APT_IPAccess_VCPEConfigObj.ipaVCPE.circuittype_dropdown, circuittype_dropdownvalue);
				addDropdownValues_commonMethod_ForSpantag("Interface Speed", APT_IPAccess_VCPEConfigObj.ipaVCPE.interfacespeed_dropdown, interfacespeed_dropdownvalue);
				
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield, "addL2circuit_circuitreference_textfield");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield,circuitreference_value);
				
				//addtextFields_commonMethod("Circuit Reference", "addL2circuit_circuitreference_textfield", circuitreference_value);
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea, "addL2circuit_circuitreference_textfield");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea,addL2circuit_remarkValue);
				
				//addtextFields_commonMethod("Remark", "addL2circuit_remarktextarea", addL2circuit_remarkValue);

				if(circuittype_dropdownvalue.equalsIgnoreCase("OLO - (GCR/EU)"))
				{
					verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.ENNI_checkbox, "ENNI_checkbox");
					sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.ENNI_checkbox,ENNI_Checkbox);
					
					//addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.ENNI_checkbox, "ENNI", ENNI_Checkbox, "no");
				}
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
				//Thread.sleep(2000);
				waitforPagetobeenable();
				IPConfig.verifysuccessmessage("Circuit successfully created.");
				waitforPagetobeenable();

				//verify added circuit
				//Thread.sleep(1000);
				scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
				//Thread.sleep(1000);
				compareText("Remark column header", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkcolumnheader, "Remark");
				compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkvalue, addL2circuit_remarkValue);
				compareText("Interface Speed column header", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_interfacespeedcolumn, "Interface Speed");
				compareText("Interface Speed", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_interfacespeedvalue, interfacespeed_dropdownvalue);
				compareText("Circuit Type column header", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_circuittypecolumn, "Circuit Type");
				compareText("Circuit Type", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_circuittypevalue, circuittype_dropdownvalue);
				//Thread.sleep(2000);

			}
		}catch (Exception e) {
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.closesymbol,"closesymbol");
		}
	}
	
	public void verifyViewOvertureL2Circuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		String interfacespeed_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceSpeed_DropdownValue");
		String circuittype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitType_DropdownValue");
		String ENNI_Checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ENNICheckbox");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox,"addedL2circuit_checkbox");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_viewlink,"L2technology_viewlink");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		scrollIntoTop();
		//GetText("Circuits header", "viewcircuits_header");
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.configoptions_l2technology, L2technology_dropdownvalue);
		compareText("Circuit Reference", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_circuitreferencevalue, circuitreference_value);
		compareText("Remarks", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_remarkvalue, addL2circuit_remarkValue);
		compareText("Interface Speed", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_interfacespeedvalue, interfacespeed_dropdownvalue);
		compareText("Circuit Type", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_circuittypevalue, circuittype_dropdownvalue);
		//GetText("Management Order", "viewL2circuit_managementordervalue");
		if(circuittype_dropdownvalue.equalsIgnoreCase("OLO - (GCR/EU)"))
		{
			if(ENNI_Checkbox.equalsIgnoreCase("Yes"))
			{
				compareText("ENNI", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_ENNIValue, "yes");
			}
			else
			{
				compareText("ENNI", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_ENNIValue, "no");
			}
		}
		//verify Edit link in view circuits page
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_actiondropdown,"viewcircuits_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_editlink,"viewcircuits_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//if(getwebelement(xml.getlocator("//locators/" + application + "/editcircuits_modalheader")).isDisplayed()) {
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.editcircuits_modalheader)){
			Report.LogInfo("INFO", "Step: Navigated to Edit L2 Circuits page as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to Edit L2 Circuits page as expected");
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.popup_close,"popup_close");
		}
		else
		{
			Report.LogInfo("INFO", "Step: Didn't navigate to Edit L2 Circuits page as expected", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to Edit L2 Circuits page as expected");
		}

		//Thread.sleep(1000);
	}
	
	public void verifyOvertureSiteA(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String SiteA_DeviceCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_DeviceCountry");
		String siteA_existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_ExistingCity");
		String siteA_newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_NewCity");
		String SiteA_cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_CityName");
		String SiteA_existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_ExistingCityValue");
		String SiteA_physicalsite_dropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_PhysicalSiteDropdown");
		String SiteA_Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_CityCode");
		String SiteA_physicalsite_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_PhysicalSiteDropdownValue");
		String siteA_CSRName_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_CSRNameValue");
		String siteA_vlanid_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_VLANIDValue");
		String siteA_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Remarks");
		String protected_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Protected_Checkbox");
		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String edit_siteA_vlanidvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteA_VLANIDValue");
		String edit_siteA_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteA_Remarks");
		String edit_protected_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteA_Protected_Checkbox");
		
		//Add Site A
		compareText("SITE A header", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteAheader, "SITE A");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_actiondropdown,"siteA_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_addlink,"siteA_addlink");
		//Thread.sleep(1000);
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(1000);
		//verify warning messages
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicecountry, "Device Country");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicexngcity, "Device Xng City");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_csrname, "CSR Name");

		selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.countryinput, "Device Country", SiteA_DeviceCountry);

		//New City	
		if(siteA_existingcity.equalsIgnoreCase("no") & siteA_newcity.equalsIgnoreCase("yes")) {
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectcity,"circuitsite_selectcity");
			//City name
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield, "citynameinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield,SiteA_cityname);
			
			//addtextFields_commonMethod("City Name", "citynameinputfield", SiteA_cityname);
			//City Code	
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield, "citycodeinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield,SiteA_Citycode);
			//addtextFields_commonMethod("City Code", "citycodeinputfield", SiteA_Citycode);
		}	

		//Existing City	
		else if(siteA_existingcity.equalsIgnoreCase("yes") & siteA_newcity.equalsIgnoreCase("no")) {

			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.citydropdowninput, "Device Xng City", SiteA_existingcityvalue);

		} 

		if(SiteA_physicalsite_dropdown.equalsIgnoreCase("yes") & siteA_existingcity.equalsIgnoreCase("yes"))
		{
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectsite,"circuitsite_selectsite");
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.physicalsite_dropdown, "Physical Site", SiteA_physicalsite_dropdownvalue);
		}
		else
		{
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field, "csrname_field");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field,siteA_CSRName_value);
			//addtextFields_commonMethod("CSR Name", "csrname_field", siteA_CSRName_value);
		}

		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, "vlanid_field");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field,siteA_vlanid_value);
		//addtextFields_commonMethod("VLAN Id", "vlanid_field", siteA_vlanid_value);
		
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, "remarks_textarea");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea,siteA_remarks);
		//addtextFields_commonMethod("Remarks", "remarks_textarea", siteA_remarks);
		addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.protected_checkbox, "Protected", protected_checkbox);
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Site Order successfully created.");
		//Thread.sleep(1000);

		//view SiteA details
		compareText("Site A", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteA_header, "Site A");
		compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_remarkvalue, siteA_remarks);
		String DeviceCountryValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecountry,"view site_device country");
		if(SiteA_DeviceCountry.contains(DeviceCountryValue)) {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCountryValue, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		else {
			Report.LogInfo("INFO", "Step: Device Country value is displayed as: "+DeviceCountryValue, "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		String DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecity,"view site_device city");
		if(siteA_existingcity.equalsIgnoreCase("yes")) {
			if(SiteA_existingcityvalue.contains(DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		else
		{
			if(SiteA_cityname.contains(DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_L2technologyvalue, L2technology_dropdownvalue);
		if(protected_checkbox.equalsIgnoreCase("Yes")) {
			compareText("Protected", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_protectedvalue, "yes");
		}
		else
		{
			compareText("Protected", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_protectedvalue, "no");
		}
		//Thread.sleep(1000);
		scrollIntoTop();
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		waitforPagetobeenable();

		//verify siteA table columns
		compareText("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_namecolumn, "Name");
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecountry, "Device Country");
		compareText("Device Xing City", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecity, "Device Xing City");
		compareText("Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_technology, "Technology");

		//verify siteA table values
		compareText("Name value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_namevalue, "Site A");
		//verify country value
		String DeviceCountry= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecountryvalue,"site A_device country value");
		if(SiteA_DeviceCountry.contains(DeviceCountry)) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCountry);
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+DeviceCountry);
		}
		//verify city value
		String DeviceCity= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_devicecityvalue,"site A_device city value");
		if(siteA_existingcity.equalsIgnoreCase("yes")) {
			if(SiteA_existingcityvalue.contains(DeviceCity)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+DeviceCity);
			}
		}
		else
		{
			if(SiteA_cityname.contains(DeviceCity)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+DeviceCity);
			}
		}
		compareText("Technology value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_technologyvalue, L2technology_dropdownvalue);
		//Thread.sleep(1000);

		//Edit Site
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_actiondropdown,"siteA_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_editlink,"siteA_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecountryvalue, SiteA_DeviceCountry);
		//verify city field in edit site page
		String EditPage_DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecityvalue,"edit site_device city value");
		if(siteA_existingcity.equalsIgnoreCase("yes")) {
			if(SiteA_existingcityvalue.contains(EditPage_DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		else
		{
			if(SiteA_cityname.contains(EditPage_DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		//verify site field in edit site page	
		String EditPage_PhysicalSiteValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_physicalsitevalue,"edit site_physical site value");
		if(SiteA_physicalsite_dropdown.equalsIgnoreCase("yes")) {
			if(SiteA_physicalsite_dropdownvalue.contains(EditPage_PhysicalSiteValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}
		else
		{
			if(siteA_CSRName_value.contains(EditPage_PhysicalSiteValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}

		edittextFields_commonMethod("VLAN Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, edit_siteA_vlanidvalue);
		edittextFields_commonMethod("Remarks", APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, edit_siteA_remarks);
		editcheckbox_commonMethod(edit_protected_checkbox, APT_IPAccess_VCPEConfigObj.ipaVCPE.protected_checkbox, "Protected");
		//Thread.sleep(1000);
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Site Order successfully updated.");
		//Thread.sleep(1000);
		scrollIntoTop();
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		
		waitforPagetobeenable();

		//verify view siteA link
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_actiondropdown,"siteA_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_viewlink,"siteA_viewlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Site A", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteA_header, "Site A");
		//Thread.sleep(1000);
		scrollIntoTop();
		clickOnBreadCrumb("L2 Circuit");
		//click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "Breadcrumb");
		
		waitforPagetobeenable();
	}
	
	public void verifyOvertureSiteA_AddEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String existing_equipmentCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Existing_EquipmentCPE");
		String equipment_cpedevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Equipment_CPEDeviceName");
		String equipment_vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Equipment_VendorModel");
		String equipment_snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Equipment_Snmpro");
		String existing_equipment_managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_ExistingEquipment_ManagementAddress");
		String equipment_managementaddress_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteAEquipment_ManagementAddress_DropdownValue");
		String equipment_managementaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteAEquipment_ManagementAddress_FieldValue");
		String MACaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_MACAddress_Value");
		String MEPID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_MEPID_Value");
		String poweralarm_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_PowerAlarm_DropdownValue");
		String mediaselection_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_MediaSelection_DropdownValue");
		String linklostforwarding_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_LinkLostForwarding_Checkbox");
		String equipment_existingCPEdeviceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteAEquipment_ExistingCPEdeviceValue");
		
		//View Site A
		compareText("SITE A header", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteAheader, "SITE A");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_actiondropdown,"siteA_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteA_viewlink,"siteA_viewlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//Equipment panel in view Site A
		compareText("Equipment", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_header, "Equipment");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_adddevicelink,"equipment_adddevicelink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Add CPE Device", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_cpedevice_header, "Add CPE Device");
		if(!existing_equipmentCPE.equalsIgnoreCase("Yes"))
		{
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button);
			//Thread.sleep(1000);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(1000);
			//verify warning messages
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_vendormodel, "Vendor/Model");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_managementaddress, "Management Address");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equiment_warngmsg_MACaddress, "MAC Address");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_poweralarm, "Power Alarm");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_mediaselection, "Media Selection");

			edittextFields_commonMethod("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_cpeName, equipment_cpedevicename);
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.vendormodelinput, "Vendor/Model", equipment_vendormodel);
			edittextFields_commonMethod("Snmpro", APT_IPAccess_VCPEConfigObj.ipaVCPE.snmproinputfield, equipment_snmpro);
			compareText("Network", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_networkvalue, "Network LAN");
			if(existing_equipment_managementaddress.equalsIgnoreCase("Yes"))
			{
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_changeaddresstoggle,"equipment_changeaddresstoggle");
				selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_dropdown, "Management Address", equipment_managementaddress_dropdownvalue);
			}
			else
			{
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field, "equipment_managementaddress_field");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field,equipment_managementaddress_value);
				
				//addtextFields_commonMethod("Management Address", "equipment_managementaddress_field", equipment_managementaddress_value);
			}
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field, "equipment_MACaddress_field");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field,MACaddress_value);
			
			//addtextFields_commonMethod("MAC Address", "equipment_MACaddress_field", MACaddress_value);
			edittextFields_commonMethod("MEP ID", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MEPID, MEPID_value);
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_poweralarm_dropdown, "Power Alarm", poweralarm_dropdownvalue);
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_mediaselection_dropdown, "Media Selection", mediaselection_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_linklostforwarding_checkbox, "Link Lost Forwarding", linklostforwarding_checkbox);
			//Thread.sleep(2000);
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_cpeName);
			//Thread.sleep(1000);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(1000);
		}
		else
		{
			addDropdownValues_commonMethod("Choose a Device", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_choosedevice_dropdown, equipment_existingCPEdeviceValue);
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.Next_Button);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.Next_Button,"Next_Button");
			//Thread.sleep(1000);
		}

		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Device successfully created.");

	}
	
	
	public void verifyOvertureSiteA_ViewEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String equipment_cpedevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Equipment_CPEDeviceName");
		String equipment_existingCPEdeviceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteAEquipment_ExistingCPEdeviceValue");
		String existing_equipmentCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Existing_EquipmentCPE");
		String equipment_vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Equipment_VendorModel");
		String equipment_snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_Equipment_Snmpro");
		String existing_equipment_managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_ExistingEquipment_ManagementAddress");
		String equipment_managementaddress_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteAEquipment_ManagementAddress_DropdownValue");
		String equipment_managementaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteAEquipment_ManagementAddress_FieldValue");
		String MEPID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_MEPID_Value");
		String poweralarm_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_PowerAlarm_DropdownValue");
		String linklostforwarding_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteA_LinkLostForwarding_Checkbox");

		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_header);
		//Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.existingequipmentdevicegrid))
		{
			//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addedequipmentdevicename"));
			List<WebElement> addeddevicesList= findWebElements(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevicename);
			System.out.println(addeddevicesList);
			int AddedDevicesCount= addeddevicesList.size();

			for(int i=0;i<AddedDevicesCount;i++) {
				String AddedDeviceNameText= addeddevicesList.get(i).getText();
				System.out.println(AddedDeviceNameText);
				String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
				if(AddedDeviceNameText.contains(equipment_cpedevicename) || AddedDeviceNameText.contains(equipment_existingCPEdeviceValue))
				{
					//WebElement AddedDevice_ViewLink= getwebelement(xml.getlocator("//locators/" + application + "/addedequipmentdevice_viewlink").replace("value", AddedDevice_SNo));
					
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_viewlink1+AddedDevice_SNo+APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_viewlink2,"AddedDevice_ViewLink");
					//Thread.sleep(2000);

					//GetText("Router Id", "viewequipment_routerid");
					String CPEDeviceName= "";
					if(existing_equipmentCPE.equalsIgnoreCase("Yes")) {
						CPEDeviceName= equipment_existingCPEdeviceValue;
					}
					else {
						CPEDeviceName= equipment_cpedevicename;
					}
					compareText("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_name, CPEDeviceName);
					compareText("Vendor/Model", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_vendormodel, equipment_vendormodel);
					compareText("Snmpro", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_snmpro, equipment_snmpro);

					String ManagementAddress= "";
					if(existing_equipment_managementaddress.equalsIgnoreCase("Yes")) {
						ManagementAddress= equipment_managementaddress_dropdownvalue;
					}
					else {
						ManagementAddress= equipment_managementaddress_value;
					}
					compareText("Management Address", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_managementaddress, ManagementAddress);
					compareText("MEP Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_mepid, MEPID_value);
					compareText("Power Alarm", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_poweralarm, poweralarm_dropdownvalue);
					compareText("Link Lost Forwarding", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_linklostforwarding, linklostforwarding_checkbox);
					//GetText("Hexa Serial Number", "viewequipment_hexaserialnumber");
					//GetText("Serial Number", "viewequipment_serialnumber");
					break;
				}
			}

			//clickOnBreadCrumb("Site A");
			clickOnBreadCrumb("Site A");
			waitforPagetobeenable();
		}
		else
		{
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}
	}
	
	public void verifyOvertureSiteA_EditEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String edit_equipment_cpedevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAEquipment_CPEDeviceName");
		String edit_equipment_existingCPEdeviceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAEquipment_ExistingCPEdeviceValue");
		String edit_existing_equipmentCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAExisting_EquipmentCPE");
		String edit_equipment_vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAEquipment_VendorModel");
		String edit_equipment_snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAEquipment_Snmpro");
		String edit_existing_equipment_managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAExistingEquipment_ManagementAddress");
		String edit_equipment_managementaddress_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAEquipment_ManagementAddress_DropdownValue");
		String edit_equipment_managementaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAEquipment_ManagementAddress_FieldValue");
		String edit_MACaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAMACAddress_Value");
		String edit_MEPID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAMEPID_Value");
		String edit_poweralarm_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAPowerAlarm_DropdownValue");
		String edit_mediaselection_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteAMediaSelection_DropdownValue");
		String edit_linklostforwarding_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteALinkLostForwarding_Checkbox");
		
		//Equipment panel in view Site A
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_header);
		//Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");

		//if(getwebelement(xml.getlocator("//locators/" + application + "/existingequipmentdevicegrid")).isDisplayed())
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.existingequipmentdevicegrid))
		{
			//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addedequipmentdevicename"));
			List<WebElement> addeddevicesList= findWebElements(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevicename);
			System.out.println(addeddevicesList);
			int AddedDevicesCount= addeddevicesList.size();

			for(int i=0;i<AddedDevicesCount;i++) {
				String AddedDeviceNameText= addeddevicesList.get(i).getText();
				System.out.println(AddedDeviceNameText);
				String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
				if(AddedDeviceNameText.contains(edit_equipment_cpedevicename) || AddedDeviceNameText.contains(edit_equipment_existingCPEdeviceValue))
				{
					//WebElement AddedDevice_EditLink= getwebelement(xml.getlocator("//locators/" + application + "/addedequipmentdevice_editlink").replace("value", AddedDevice_SNo));
					//click(AddedDevice_EditLink);
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_editlink1+AddedDevice_SNo+APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_editlink2,"AddedDevice_ViewLink");

					//Thread.sleep(2000);

					edittextFields_commonMethod("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_cpeName, edit_existing_equipmentCPE);
					selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.vendormodelinput, "Vendor/Model", edit_equipment_vendormodel);
					edittextFields_commonMethod("Snmpro", APT_IPAccess_VCPEConfigObj.ipaVCPE.snmproinputfield, edit_equipment_snmpro);
					compareText("Network", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_networkvalue, "LAN");
					if(edit_existing_equipment_managementaddress.equalsIgnoreCase("Yes"))
					{
						click(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_changeaddresstoggle,"equipment_changeaddresstoggle");
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_dropdown, "Management Address", edit_equipment_managementaddress_dropdownvalue);
					}
					else
					{
						verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field, "equipment_managementaddress_field");
						sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field,edit_equipment_managementaddress_value);
						
						//addtextFields_commonMethod("Management Address", "equipment_managementaddress_field", edit_equipment_managementaddress_value);
					}
					verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field, "equipment_MACaddress_field");
					sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field,edit_MACaddress_value);
					
					//addtextFields_commonMethod("MAC Address", "equipment_MACaddress_field", edit_MACaddress_value);
					edittextFields_commonMethod("MEP ID", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MEPID, edit_MEPID_value);
					selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_poweralarm_dropdown, "Power Alarm", edit_poweralarm_dropdownvalue);
					selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_mediaselection_dropdown, "Media Selection", edit_mediaselection_dropdownvalue);
					addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_linklostforwarding_checkbox, "Link Lost Forwarding", edit_linklostforwarding_checkbox);
					scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button);
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
					//Thread.sleep(1000);
					waitforPagetobeenable();
					IPConfig.verifysuccessmessage("Device successfully updated.");
					break;
				}
			}

			//clickOnBreadCrumb("Site A");
			clickOnBreadCrumb("Site A");
			waitforPagetobeenable();
		}
		else
		{
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}

	}
	
	public void verifyOvertureSiteB(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String SiteB_DeviceCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_DeviceCountry");
		String siteB_existingcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_ExistingCity");
		String siteB_newcity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_NewCity");
		String SiteB_cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_CityName");
		String SiteB_Citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_CityCode");
		String SiteB_existingcityvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_ExistingCityValue");
		String SiteB_physicalsite_dropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_PhysicalSiteDropdown");
		String SiteB_physicalsite_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_PhysicalSiteDropdownValue");
		String siteB_CSRName_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_CSRNameValue");
		String siteB_vlanid_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_VLANIDValue");
		String siteB_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Remarks");
		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String edit_siteB_vlanidvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteB_VLANIDValue");
		String edit_siteB_remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteB_Remarks");
		String edit_siteB_protected_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteB_DeviceName");
		String siteB_protected_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_DeviceName");
		
		//Add Site B
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteBheader);
		//Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		compareText("SITE B header", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteBheader, "SITE B");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_actiondropdown,"siteB_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_addlink,"siteB_addlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");

		//verify warning messages
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicecountry, "Device Country");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_devicexngcity, "Device Xng City");
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_warngmsg_csrname, "CSR Name");

		selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.countryinput, "Device Country", SiteB_DeviceCountry);

		//New City	
		compareText("Device Xng City", APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_citylabel, " Device Xng City");
		if(siteB_existingcity.equalsIgnoreCase("no") & siteB_newcity.equalsIgnoreCase("yes")) {
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectcity,"circuitsite_selectcity");
			//City name
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield, "citynameinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citynameinputfield,SiteB_cityname);
			
			//addtextFields_commonMethod("City Name", "citynameinputfield", SiteB_cityname);
			//City Code	
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield, "citycodeinputfield");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.citycodeinputfield,SiteB_Citycode);
			//addtextFields_commonMethod("City Code", "citycodeinputfield", SiteB_Citycode);
		}	

		//Existing City	
		else if(siteB_existingcity.equalsIgnoreCase("yes") & siteB_newcity.equalsIgnoreCase("no")) {
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.citydropdowninput, "Device Xng City", SiteB_existingcityvalue);

		} 

		if(SiteB_physicalsite_dropdown.equalsIgnoreCase("yes") & siteB_existingcity.equalsIgnoreCase("yes"))
		{
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.circuitsite_selectsite,"circuitsite_selectsite");
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.physicalsite_dropdown, "Physical Site", SiteB_physicalsite_dropdownvalue);
		}
		else
		{
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field, "csrname_field");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.csrname_field,siteB_CSRName_value);
			//addtextFields_commonMethod("CSR Name", "csrname_field", siteB_CSRName_value);
		}

		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, "vlanid_field");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field,siteB_vlanid_value);
		//addtextFields_commonMethod("VLAN Id", "vlanid_field", siteB_vlanid_value);
		
		verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, "remarks_textarea");
		sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea,siteB_remarks);
		//addtextFields_commonMethod("Remarks", "remarks_textarea", siteB_remarks);
		
		addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.protected_checkbox, "Protected", siteB_protected_checkbox);
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(2000);
		IPConfig.verifysuccessmessage("Site Order successfully created.");
		//Thread.sleep(1000);

		//view SiteB details
		compareText("Site B", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteB_header, "Site B");
		compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_remarkvalue, siteB_remarks);
		String DeviceCountryValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecountry,"view site_device country");
		if(SiteB_DeviceCountry.contains(DeviceCountryValue)) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		else {
			//ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+DeviceCountryValue);
		}
		String DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_devicecity,"view site_device city");
		if(siteB_existingcity.equalsIgnoreCase("yes")) {
			if(SiteB_existingcityvalue.contains(DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		else
		{
			if(SiteB_cityname.contains(DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+DeviceCityValue);
			}
		}
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_L2technologyvalue, L2technology_dropdownvalue);
		//getTextFrom("Protected", "viewsite_protectedvalue");
		if(siteB_protected_checkbox.equalsIgnoreCase("Yes")) {
			compareText("Protected", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_protectedvalue, "yes");
		}
		else
		{
			compareText("Protected", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsite_protectedvalue, "no");
		}

		//Thread.sleep(1000);
		scrollIntoTop();
		//clickOnBreadCrumb("L2 Circuit");
		clickOnBreadCrumb("L2 Circuit");
		waitforPagetobeenable();

		//verify siteB table columns
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteBheader);
		//Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		compareText("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_namecolumn, "Name");
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecountry, "Device Country");
		compareText("Device Xing City", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecity, "Device Xing City");
		compareText("Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_technology, "Technology");

		//verify siteB table values
		compareText("Name value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_namevalue, "Site B");
		//verify country value
		String SiteB_DeviceCountryValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecountryvalue,"site B_device country value");
		if(SiteB_DeviceCountry.contains(SiteB_DeviceCountryValue)) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Country value is displayed as: "+SiteB_DeviceCountryValue);
		}
		else {
			//ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Device Country value is displayed as: "+SiteB_DeviceCountryValue);
		}
		//verify city value
		String SiteB_DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_devicecityvalue,"site B_device city value");
		if(siteB_existingcity.equalsIgnoreCase("yes")) {
			if(SiteB_existingcityvalue.contains(SiteB_DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+SiteB_DeviceCityValue);
			}
		}
		else
		{
			if(SiteB_cityname.contains(SiteB_DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+SiteB_DeviceCityValue);
			}
		}

		compareText("Technology value", APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_technologyvalue, L2technology_dropdownvalue);
		//Thread.sleep(1000);

		//Edit Site
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteBheader);
		//Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_actiondropdown,"siteB_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_editlink,"siteB_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Device Country", APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecountryvalue, SiteB_DeviceCountry);
		//verify city field in edit site page
		String EditPage_DeviceCityValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_devicecityvalue,"edit site_device city value");
		if(siteB_existingcity.equalsIgnoreCase("yes")) {
			if(SiteB_existingcityvalue.contains(EditPage_DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		else
		{
			if(SiteB_cityname.contains(EditPage_DeviceCityValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Device Xng City value is displayed as: "+EditPage_DeviceCityValue);
			}
		}
		//verify site field in edit site page	
		String EditPage_PhysicalSiteValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.editsite_physicalsitevalue,"edit site_physical site value");
		if(SiteB_physicalsite_dropdown.equalsIgnoreCase("yes")) {
			if(SiteB_physicalsite_dropdownvalue.contains(EditPage_PhysicalSiteValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}
		else
		{
			if(siteB_CSRName_value.contains(EditPage_PhysicalSiteValue)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Physical Site value is displayed as: "+EditPage_PhysicalSiteValue);
			}
		}

		edittextFields_commonMethod("VLAN Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.vlanid_field, edit_siteB_vlanidvalue);
		edittextFields_commonMethod("Remarks", APT_IPAccess_VCPEConfigObj.ipaVCPE.remarks_textarea, edit_siteB_remarks);
		editcheckbox_commonMethod(edit_siteB_protected_checkbox, APT_IPAccess_VCPEConfigObj.ipaVCPE.protected_checkbox, "Protected");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.editservice_okbutton,"editservice_okbutton");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Site Order successfully updated.");
		//Thread.sleep(1000);
		scrollIntoTop();
		//clickOnBreadCrumb("L2 Circuit");
		clickOnBreadCrumb("L2 Circuit");
		//waitForpageload();
		waitforPagetobeenable();

		//verify view siteB link
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_actiondropdown,"siteB_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_viewlink,"siteB_viewlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Site B", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewsiteB_header, "Site B");
		//Thread.sleep(1000);
		//clickOnBreadCrumb("L2 Circuit");
		clickOnBreadCrumb("L2 Circuit");
		
		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewpage_backbutton);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewpage_backbutton,"viewpage_backbutton");
		//Thread.sleep(1000);
		waitforPagetobeenable();
	}
	
	public void verifyOvertureSiteB_AddEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String existing_equipmentCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Existing_EquipmentCPE");
		String equipment_cpedevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Equipment_CPEDeviceName");
		String equipment_vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Equipment_VendorModel");
		String equipment_snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Equipment_Snmpro");
		String existing_equipment_managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_ExistingEquipment_ManagementAddress");
		String equipment_managementaddress_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteBEquipment_ManagementAddress_DropdownValue");
		String equipment_managementaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteBEquipment_ManagementAddress_FieldValue");
		String MACaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_MACAddress_Value");
		String MEPID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_MEPID_Value");
		String poweralarm_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_PowerAlarm_DropdownValue");
		String mediaselection_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_MediaSelection_DropdownValue");
		String linklostforwarding_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_LinkLostForwarding_Checkbox");
		String equipment_existingCPEdeviceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteBEquipment_ExistingCPEdeviceValue");
		
		//View Site B
		compareText("SITE B header", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_siteAheader, "SITE B");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_actiondropdown,"siteB_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.siteB_viewlink,"siteB_viewlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//Equipment panel in view Site A
		compareText("Equipment", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_header, "Equipment");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_adddevicelink,"equipment_adddevicelink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		compareText("Add CPE Device", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_cpedevice_header, "Add CPE Device");
		if(!existing_equipmentCPE.equalsIgnoreCase("Yes"))
		{
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(1000);
			//verify warning messages
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_vendormodel, "Vendor/Model");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_managementaddress, "Management Address");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equiment_warngmsg_MACaddress, "MAC Address");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_poweralarm, "Power Alarm");
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_warngmsg_mediaselection, "Media Selection");

			edittextFields_commonMethod("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_cpeName, equipment_cpedevicename);
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.vendormodelinput, "Vendor/Model", equipment_vendormodel);
			edittextFields_commonMethod("Snmpro", APT_IPAccess_VCPEConfigObj.ipaVCPE.snmproinputfield, equipment_snmpro);
			compareText("Network", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_networkvalue, "LAN");
			if(existing_equipment_managementaddress.equalsIgnoreCase("Yes"))
			{
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_changeaddresstoggle,"equipment_changeaddresstoggle");
				selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_dropdown, "Management Address", equipment_managementaddress_dropdownvalue);
			}
			else
			{
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field, "equipment_managementaddress_field");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field,equipment_managementaddress_value);
				
				//addtextFields_commonMethod("Management Address", "equipment_managementaddress_field", equipment_managementaddress_value);
			}
			verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field, "equipment_MACaddress_field");
			sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field,MACaddress_value);
			
			//addtextFields_commonMethod("MAC Address", "equipment_MACaddress_field", MACaddress_value);
			edittextFields_commonMethod("MEP ID", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MEPID, MEPID_value);
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_poweralarm_dropdown, "Power Alarm", poweralarm_dropdownvalue);
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_mediaselection_dropdown, "Media Selection", mediaselection_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_linklostforwarding_checkbox, "Link Lost Forwarding", linklostforwarding_checkbox);
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(1000);
		}
		else
		{
			addDropdownValues_commonMethod("Choose a Device", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_choosedevice_dropdown, equipment_existingCPEdeviceValue);
			scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.Next_Button);
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.Next_Button,"Next_Button");
			//Thread.sleep(1000);
		}

		waitforPagetobeenable();
		IPConfig.verifysuccessmessage("Device successfully created.");

	}
	

	public void verifyOvertureSiteB_ViewEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String equipment_cpedevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Equipment_CPEDeviceName");
		String equipment_existingCPEdeviceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteBEquipment_ExistingCPEdeviceValue");
		String existing_equipmentCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Existing_EquipmentCPE");
		String equipment_vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Equipment_VendorModel");
		String equipment_snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_Equipment_Snmpro");
		String existing_equipment_managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_ExistingEquipment_ManagementAddress");
		String equipment_managementaddress_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteBEquipment_ManagementAddress_DropdownValue");
		String equipment_managementaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteBEquipment_ManagementAddress_FieldValue");
		String MEPID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_MEPID_Value");
		String poweralarm_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_PowerAlarm_DropdownValue");
		String linklostforwarding_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteB_LinkLostForwarding_Checkbox");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_header);
		//Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		//if(getwebelement(xml.getlocator("//locators/" + application + "/existingequipmentdevicegrid")).isDisplayed())
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.existingequipmentdevicegrid))
		{
			//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addedequipmentdevicename"));
			List<WebElement> addeddevicesList= findWebElements(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevicename);
			System.out.println(addeddevicesList);
			int AddedDevicesCount= addeddevicesList.size();

			for(int i=0;i<AddedDevicesCount;i++) {
				String AddedDeviceNameText= addeddevicesList.get(i).getText();
				System.out.println(AddedDeviceNameText);
				String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
				if(AddedDeviceNameText.contains(equipment_cpedevicename) || AddedDeviceNameText.contains(equipment_existingCPEdeviceValue))
				{
					//WebElement AddedDevice_ViewLink= getwebelement(xml.getlocator("//locators/" + application + "/addedequipmentdevice_viewlink").replace("value", AddedDevice_SNo));
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_viewlink1+AddedDevice_SNo+APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_viewlink2,"addedequipmentdevice_viewlink2");
					//Clickon(AddedDevice_ViewLink);
					//Thread.sleep(2000);

					//GetText("Router Id", "viewequipment_routerid");
					String CPEDeviceName= "";
					if(existing_equipmentCPE.equalsIgnoreCase("Yes")) {
						CPEDeviceName= equipment_existingCPEdeviceValue;
					}
					else {
						CPEDeviceName= equipment_cpedevicename;
					}
					compareText("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_name, CPEDeviceName);
					compareText("Vendor/Model", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_vendormodel, equipment_vendormodel);
					compareText("Snmpro", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_snmpro, equipment_snmpro);

					String ManagementAddress= "";
					if(existing_equipment_managementaddress.equalsIgnoreCase("Yes")) {
						ManagementAddress= equipment_managementaddress_dropdownvalue;
					}
					else {
						ManagementAddress= equipment_managementaddress_value;
					}
					compareText("Management Address", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_managementaddress, ManagementAddress);
					compareText("MEP Id", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_mepid, MEPID_value);
					compareText("Power Alarm", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_poweralarm, poweralarm_dropdownvalue);
					compareText("Link Lost Forwarding", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewequipment_linklostforwarding, linklostforwarding_checkbox);
					//GetText("Hexa Serial Number", "viewequipment_hexaserialnumber");
					//GetText("Serial Number", "viewequipment_serialnumber");
					break;
				}
			}

			//clickOnBreadCrumb("Site A");
			clickOnBreadCrumb("Site A");
			waitforPagetobeenable();
		}
		else
		{
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}
	}
	
	public void verifyOvertureSiteB_EditEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String edit_equipment_cpedevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBEquipment_CPEDeviceName");
		String edit_equipment_existingCPEdeviceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBEquipment_ExistingCPEdeviceValue");
		String edit_existing_equipmentCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBExisting_EquipmentCPE");
		String edit_equipment_vendormodel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBEquipment_VendorModel");
		String edit_equipment_snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBEquipment_Snmpro");
		String edit_existing_equipment_managementaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBExistingEquipment_ManagementAddress");
		String edit_equipment_managementaddress_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBEquipment_ManagementAddress_DropdownValue");
		String edit_equipment_managementaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBEquipment_ManagementAddress_FieldValue");
		String edit_MACaddress_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBMACAddress_Value");
		String edit_MEPID_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBMEPID_Value");
		String edit_poweralarm_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBPowerAlarm_DropdownValue");
		String edit_mediaselection_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBMediaSelection_DropdownValue");
		String edit_linklostforwarding_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_SiteBLinkLostForwarding_Checkbox");
		
		//Equipment panel in view Site B
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_header);
		//Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");

		//if(getwebelement(xml.getlocator("//locators/" + application + "/existingequipmentdevicegrid")).isDisplayed())
			if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.existingequipmentdevicegrid))
		{
			//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addedequipmentdevicename"));
			List<WebElement> addeddevicesList= findWebElements(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevicename);
			System.out.println(addeddevicesList);
			int AddedDevicesCount= addeddevicesList.size();

			for(int i=0;i<AddedDevicesCount;i++) {
				String AddedDeviceNameText= addeddevicesList.get(i).getText();
				System.out.println(AddedDeviceNameText);
				String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
				if(AddedDeviceNameText.contains(edit_equipment_cpedevicename) || AddedDeviceNameText.contains(edit_equipment_existingCPEdeviceValue))
				{
					//WebElement AddedDevice_EditLink= getwebelement(xml.getlocator("//locators/" + application + "/addedequipmentdevice_editlink").replace("value", AddedDevice_SNo));
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_editlink1+AddedDevice_SNo+APT_IPAccess_VCPEConfigObj.ipaVCPE.addedequipmentdevice_editlink2,"AddedDevice_ViewLink");

					//Clickon(AddedDevice_EditLink);
					//Thread.sleep(2000);

					edittextFields_commonMethod("Name", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_cpeName, edit_existing_equipmentCPE);
					selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.vendormodelinput, "Vendor/Model", edit_equipment_vendormodel);
					edittextFields_commonMethod("Snmpro", APT_IPAccess_VCPEConfigObj.ipaVCPE.snmproinputfield, edit_equipment_snmpro);
					compareText("Network", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_networkvalue, "LAN");
					if(edit_existing_equipment_managementaddress.equalsIgnoreCase("Yes"))
					{
						click(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_changeaddresstoggle,"equipment_changeaddresstoggle");
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_dropdown, "Management Address", edit_equipment_managementaddress_dropdownvalue);
					}
					else
					{
						verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field, "equipment_managementaddress_field");
						sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field,edit_equipment_managementaddress_value);
						
						//addtextFields_commonMethod("Management Address", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_managementaddress_field, edit_equipment_managementaddress_value);
					}
					verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field, "equipment_MACaddress_field");
					sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MACaddress_field,edit_MACaddress_value);
					
					//addtextFields_commonMethod("MAC Address", "equipment_MACaddress_field", edit_MACaddress_value);
					edittextFields_commonMethod("MEP ID", APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_MEPID, edit_MEPID_value);
					selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_poweralarm_dropdown, "Power Alarm", edit_poweralarm_dropdownvalue);
					selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_mediaselection_dropdown, "Media Selection", edit_mediaselection_dropdownvalue);
					addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.equipment_linklostforwarding_checkbox, "Link Lost Forwarding", edit_linklostforwarding_checkbox);
					scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button);
					//Thread.sleep(1000);
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"Ok Button");
					//Thread.sleep(1000);
					waitforPagetobeenable();
					IPConfig.verifysuccessmessage("Device successfully updated.");
					break;
				}
			}

			//clickOnBreadCrumb("Site B");
			clickOnBreadCrumb("Site B");
			waitforPagetobeenable();
		}
		else
		{
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}

	}	
	
	public void verify_EditAccedianL2Technology(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String existingManagementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ExistingManagementOrder");
		String managementorder_fieldvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ManagementOrder_FieldValue");
		String ManagementOrder_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ManagementOrder_DropdownValue");
		String circuittype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_CircuitType_DropdownValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_CircuitReference_Value");
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_L2Circuit_RemarkValue");
		String ENNI_Checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Edit_ENNICheckbox");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		////Thread.sleep(1000);
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_panelheader, "L2 Technology");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		////Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_editlink,"L2technology_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		try {

			if(existingManagementOrder.equalsIgnoreCase("Yes"))
			{
				addDropdownValues_commonMethod_ForSpantag("Management Order", APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_dropdown, ManagementOrder_dropdownvalue);
			}
			else
			{
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.selectorder_toggle,"selectorder_toggle");
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield, "managementorder_textfield");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield,managementorder_fieldvalue);
				
				//addtextFields_commonMethod("Management Order", APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield, managementorder_fieldvalue);
			}
			addDropdownValues_commonMethod_ForSpantag("Circuit Type", APT_IPAccess_VCPEConfigObj.ipaVCPE.circuittype_dropdown, circuittype_dropdownvalue);
			edittextFields_commonMethod("Circuit Reference", APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield, circuitreference_value);
			edittextFields_commonMethod("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea, addL2circuit_remarkValue);

			if(circuittype_dropdownvalue.equalsIgnoreCase("OLO - (GCR/EU)"))
			{
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.ENNI_checkbox, "ENNI_checkbox");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.ENNI_checkbox,ENNI_Checkbox);
				
				//addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.ENNI_checkbox, "ENNI", ENNI_Checkbox, "no");
			}
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
			//Thread.sleep(2000);
			waitforPagetobeenable();
			IPConfig.verifysuccessmessage("Circuit successfully updated.");
			//waitForpageload();
			waitforPagetobeenable();

		}catch (Exception e) {
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.closesymbol,"closesymbol");
		}
	}
	
	public void verifyAccedianL2Technology(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
		
		String existingManagementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingManagementOrder");
		String ManagementOrder_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManagementOrder_DropdownValue");
		String managementorder_fieldvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManagementOrder_FieldValue");
		String circuittype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitType_DropdownValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		String ENNI_Checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ENNICheckbox");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_panelheader, "L2 Technology");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuitwizardlink,"addL2circuitwizardlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		try {
			//if(getwebelement(xml.getlocator("//locators/" + application + "/addL2circuit_modal")).isDisplayed()) {
			if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_modal)){
				compareText("L2 Circuit header", APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_modalheader, "L2 Circuit");
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
				//Thread.sleep(2000);

				//verify warning messages
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.warngmsg_circuittype, "Circuit Type");
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.Acteliscircuit_warngmsg_circuitreference, "Circuit Reference");

				if(existingManagementOrder.equalsIgnoreCase("Yes"))
				{
					addDropdownValues_commonMethod("Management Order", APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_dropdown, ManagementOrder_dropdownvalue);
				}
				else
				{
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.selectorder_toggle,"selectorder_toggle");
					verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield, "managementorder_textfield");
					sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield,managementorder_fieldvalue);
					
					//addtextFields_commonMethod("Management Order", APT_IPAccess_VCPEConfigObj.ipaVCPE.managementorder_textfield, managementorder_fieldvalue);
				}
				addDropdownValues_commonMethod("Circuit Type", APT_IPAccess_VCPEConfigObj.ipaVCPE.circuittype_dropdown, circuittype_dropdownvalue);
				
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield, "addL2circuit_circuitreference_textfield");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_circuitreference_textfield,circuitreference_value);
				
				verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea, "addL2circuit_remarktextarea");
				sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.addL2circuit_remarktextarea,addL2circuit_remarkValue);
				
				//addtextFields_commonMethod("Remark", "addL2circuit_remarktextarea", addL2circuit_remarkValue);

				if(circuittype_dropdownvalue.equalsIgnoreCase("OLO - (GCR/EU)"))
				{
					addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.ENNI_checkbox, "ENNI", ENNI_Checkbox);
				}
				click(APT_IPAccess_VCPEConfigObj.ipaVCPE.OK_button,"OK_button");
				//Thread.sleep(2000);
				waitforPagetobeenable();
				IPConfig.verifysuccessmessage("Circuit successfully created.");
				
				waitforPagetobeenable();

				//verify added circuit
				//Thread.sleep(1000);
				scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
				//Thread.sleep(1000);
				compareText("Remark column header", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkcolumnheader, "Remark");
				compareText("Remark", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_remarkvalue, addL2circuit_remarkValue);
				compareText("Circuit Type column header", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_circuittypecolumn, "Circuit Type");
				compareText("Circuit Type", APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_circuittypevalue, circuittype_dropdownvalue);
				//Thread.sleep(2000);

			}
		}catch (Exception e) {
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.closesymbol,"closesymbol");
		}
	}
	
	public void verifyViewAccedianL2Circuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"L2Technology_DropdownValue");
		String circuitreference_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference_Value");
		String addL2circuit_remarkValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Add_L2Circuit_RemarkValue");
		String circuittype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitType_DropdownValue");
		String ENNI_Checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ENNICheckbox");
		
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox,"addedL2circuit_checkbox");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_viewlink,"L2technology_viewlink");
		//Thread.sleep(2000);
		waitforPagetobeenable();
		scrollIntoTop();
		//GetText("Circuits header", "viewcircuits_header");
		compareText("L2 Technology", APT_IPAccess_VCPEConfigObj.ipaVCPE.configoptions_l2technology, L2technology_dropdownvalue);
		compareText("Circuit Reference", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_circuitreferencevalue, circuitreference_value);
		compareText("Remarks", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_remarkvalue, addL2circuit_remarkValue);
		compareText("Circuit Type", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_circuittypevalue, circuittype_dropdownvalue);
		//GetText("Management Order", "viewL2circuit_managementordervalue");

		if(circuittype_dropdownvalue.equalsIgnoreCase("OLO - (GCR/EU)"))
		{
			if(ENNI_Checkbox.equalsIgnoreCase("Yes"))
			{
				compareText("ENNI", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_ENNIValue, "yes");
			}
			else
			{
				compareText("ENNI", APT_IPAccess_VCPEConfigObj.ipaVCPE.viewL2circuit_ENNIValue, "no");
			}
		}
		//verify Edit link in view circuits page
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_actiondropdown,"viewcircuits_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_editlink,"viewcircuits_editlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		//if(getwebelement(xml.getlocator("//locators/" + application + "/editcircuits_modalheader")).isDisplayed()) {
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.editcircuits_modalheader)){
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to Edit L2 Circuits page as expected");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to Edit L2 Circuits page as expected");
		}
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.popup_close,"popup_close");
		//Thread.sleep(1000);
	}

	public void deleteL2Technology() throws InterruptedException, IOException, AWTException {

		
		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader);
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.addedL2circuit_checkbox,"addedL2circuit_checkbox");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_actiondropdown,"L2technology_actiondropdown");
		//Thread.sleep(1000);
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_viewlink,"L2technology_viewlink");
		//Thread.sleep(1000);
		waitforPagetobeenable();
		scrollIntoTop();
		//GetText("Circuits header", "viewcircuits_header");
		//verify Delete link in view circuits page
		scrollIntoTop();
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_actiondropdown,"viewcircuits_actiondropdown");
		click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewcircuits_deletelink,"viewcircuits_deletelink");
		//Thread.sleep(1000);

		/*Alert alert = webDriver.switchTo().alert();       

		// Capturing alert message.    
		String alertMessage= webDriver.switchTo().alert().getText();
		if(alertMessage.isEmpty()) {
			Report.LogInfo("INFO", "No message displays", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No message displays");
			System.out.println("No Message displays"); 
		}else {
			Report.LogInfo("INFO", "No message displays as: "+alertMessage, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Alert message displays as: "+alertMessage);
			System.out.println("text message for alert displays as: "+alertMessage);
		}

		try {  
			alert.accept();
			//Thread.sleep(2000);
		}catch(Exception e) {
			e.printStackTrace();
		} */
		//Thread.sleep(2000);
		if(isAlertPresent()==true){
			Robot r = new Robot();
			Thread.sleep(1000);
			r.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
		}
		
		waitForAjax();
		IPConfig.verifysuccessmessage("Circuit successfully deleted.");

	}
	
	public void navigatetoViewDevicepage() throws InterruptedException, IOException {

		waitforPagetobeenable();
		scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.providerequipment_header);
		Thread.sleep(1000);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		//if(getwebelement(xml.getlocator("//locators/" + application + "/existingdevicegrid")).isDisplayed())
		if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.existingdevicegrid))
		{
			click(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewservicepage_viewdevicelink,"ipaVCPEviewservicepage_viewdevicelink");
			//Thread.sleep(5000);
			waitforPagetobeenable();
		}
		else
		{
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}
	}

	public String DeviceName() throws InterruptedException, IOException {
		waitForAjax();
		scrollIntoTop();
		String DeviceNameValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewpage_devicename,"viewpage_devicename");

		return DeviceNameValue;
	}

	public String VendorModel() throws InterruptedException, IOException {

		scrollIntoTop();
		String VendorModelValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewpage_vendormodel,"view page_vendor model");

		return VendorModelValue;
	}

	public String ManagementAddress() throws InterruptedException, IOException {

		scrollIntoTop();
		String ManagementAddessValue= getTextFrom(APT_IPAccess_VCPEConfigObj.ipaVCPE.viewpage_managementaddress,"view page_management address");

		return ManagementAddessValue;
	}

	 public void addDropdownValues_commonMethod_ForSpantag(String labelname, String xpath, String expectedValueToAdd) throws InterruptedException {
         boolean availability=false;
         List<String> ls = new ArrayList<String>();
         
       try {  
        // availability=getwebelement(xml.getlocator("//locators/" + application + "/"+ xpath +"")).isDisplayed();
        // if(availability) {
        if(isVisible(xpath)){
        	Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");
               ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
              // Log.info(labelname + " dropdown is displaying");
               
               if(expectedValueToAdd.equalsIgnoreCase("null")) {
               	Report.LogInfo("INFO", " No values selected under "+ labelname + " dropdown", "PASS");
                     ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under "+ labelname + " dropdown");
                     //Log.info(" No values selected under "+ labelname + " dropdown");
               }else {
                     webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//div[text()='�']")).click();
                     //Clickon(getwebelement("//div[label[text()='"+ labelname +"']]//div[text()='�']"));
                     Thread.sleep(3000);
                     
                     //verify list of values inside dropdown
                     List<WebElement> listofvalues = webDriver
                                     .findElements(By.xpath("//span[@role='option']"));
                     
                     //ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside "+ labelname + " dropdown is:  ");
                    // Log.info( " List of values inside "+ labelname + "dropdown is:  ");
                     
                         for (WebElement valuetypes : listofvalues) {
                                          // Log.info("service sub types : " + valuetypes.getText());
                                           ls.add(valuetypes.getText());
                         }
                         
                             //ExtentTestManager.getTest().log(LogStatus.PASS, "list of values inside "+labelname+" dropdown is: "+ls);
                         //Log.info("list of values inside "+labelname+" dropdown is: "+ls);
                         
                         Thread.sleep(2000);
                   //SendKeys(getwebelement("//div[label[text()='"+ labelname +"']]//input"), expectedValueToAdd);      
                   webDriver.findElement(By.xpath("//div[label[text()='"+ labelname +"']]//input")).sendKeys(expectedValueToAdd);
                   Thread.sleep(2000);
                      
                   webDriver.findElement(By.xpath("(//div[label[text()='"+ labelname +"']]//span[contains(text(),'"+ expectedValueToAdd +"')])[1]")).click();

                     //Clickon(getwebelement("(//div[label[text()='"+ labelname +"']]//span[contains(text(),'"+ expectedValueToAdd +"')])[1]"));
                     Thread.sleep(3000);
                     String actualValue= webDriver.findElement(By.xpath("//label[text()='"+ labelname +"']/following-sibling::div//span")).getText();
                    // String actualValue=getwebelement("//label[text()='"+ labelname +"']/following-sibling::div//span").getText();
                     //ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown value selected as: "+ actualValue );
                    // Log.info( labelname + " dropdown value selected as: "+ actualValue);
                     
               }
         }else {
        	 Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
               ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
               //Log.info(labelname + " is not displaying");
         }
       }catch(NoSuchElementException e) {
      	 Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");

             ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
               
       }catch(Exception ee) {
             ee.printStackTrace();
             Report.LogInfo("INFO", " NOt able to perform selection under "+ labelname + " dropdown", "FAIL");
             ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under "+ labelname + " dropdown");
             //Log.info(" NO value selected under "+ labelname + " dropdown");
       }
       
 }

	//=================================== Common Methods ==============================
	 public void verifyServiceCreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException
		{
			String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"ServiceIdentification");
			String BillingType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "BillingType");
			String TerminationDate = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"TerminationDate");
			String EmailService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
			String PhoneService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneService");
			String Remarks = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");
			String ManageService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManageService");
			String RouterConfigurationViewIPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RouterConfigurationViewIPv4");
			String RouterConfigurationViewIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RouterConfigurationViewIPv6");
			String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"PerformanceReporting");
			String IPGuardian = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "IPGuardian");
			String SNMPNotification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"SNMPNotification");
			String DeliveryChannel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"DeliveryChannel");
			String RouterBasedFirewall = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RouterBasedFirewall");
			String TrapTargetAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"TrapTargetAddress");
			String Qos = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Qos");
			String ExtendPErangetocustomerLAN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"ExtendPErangetocustomerLAN");
			String GeneratePassword = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"GeneratePassword");
			String BGPPassword = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"BGPPassword");
			String BGPTableDistribution = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"BGPTableDistribution");
			String package_dropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Package_DropdownValue");
			String snmpnotification_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"SnmpNotification_Checkbox");
			String routerconfigview_ipv4_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RouterConfigView_IPv4_Checkbox");
			String routerconfigview_ipv6_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RouterConfigView_IPv6_Checkbox");
			String routerbasedfirewall_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RouterbasedFirewall_Checkbox");
			String syslogeventview_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"SyslogEventView_Checkbox");
			String ipguardian_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"IPGuardian_Checkbox");
			String deliverychannel_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"DeliveryChannel_DropdownValue");
			String withlogicaltunnel_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"WithLogicalTunnel_Checkbox");
			String routingprotocol_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"RoutingProtocol_DropdownValue");
			String qos_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"QOS_Checkbox");
			String modularmsp_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"ModularMSP_Checkbox");
			String L2technology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"L2Technology_DropdownValue");
			String BGPasnumber_Value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"BGPAsNumber_Value");
			String customerdescription_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"CustomerDescription_Value");
			String managedservice_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"ManagedService_Checkbox");
			
			
			
			
			// if(isElementPresent(By.xpath("//div[text()='Create Order /
			// Service'])")))
			// {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");
			click(APTIPAccessNoCPE.APTNoCPE.nextbutton, "Next button");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.sidwarngmsg, "Service Identification");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_warngmsg, "Billing Type");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield, "new order textfield");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.serviceidentificationtextfield, ServiceIdentification, "newordertextfield");

			Thread.sleep(15000);
			verifyExists(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, "Billing Type");
			selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown, BillingType, "Billing Type");
			// click(APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,"billingtype");

			// addDropdownValues_commonMethod("Billing
			// Type",APTIPAccessNoCPE.APTNoCPE.billingtype_dropdown,BillingType);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.terminationdate_field, "Terminate Date");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.terminationdate_field, TerminationDate, "Terminate Date");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, "Email Service");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield_Service, EmailService, "Email Service");

			//verifyExists(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield, "new order textfield");
			//sendKeys(APTIPAccessNoCPE.APTNoCPE.phonecontacttextfield, PhoneService, "newordertextfield");

			//verifyExists(APTIPAccessNoCPE.APTNoCPE.remarktextarea, "new order textfield");
			//sendKeys(APTIPAccessNoCPE.APTNoCPE.remarktextarea, Remarks, "newordertextfield");

			if (ManageService.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageServicecheckbox, "Manage Service checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.ManageServicecheckbox, "Manage Service checkbox");
			} else {
				Reporter.log("Not Manage Service checkbox");
			}
			if (RouterConfigurationViewIPv4.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv4checkbox,
						"Router Configuration View IPv4 checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv4checkbox, "Router Configuration View IPv4");
			} else {
				Reporter.log("Not Router Configuration View IPv4 checkbox ");
			}

			if (RouterConfigurationViewIPv6.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv6checkbox,
						"Router Configuration View IPv6 checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.RouterConfigurationViewIPv6checkbox, "Router Configuration View IPv6");
			} else {
				Reporter.log("Not Router Configuration View IPv6 checkbox ");
			}
			if (PerformanceReporting.equalsIgnoreCase("Yes")) {
				//verifyExists(APTIPAccessNoCPE.APTNoCPE.performancereportingcheckbox, "performance reporting checkbox");
				//click(APTIPAccessNoCPE.APTNoCPE.performancereportingcheckbox, "performance reporting checkbox");
			} else {
				Reporter.log("Not performance reporting checkbox");
			}

			if (IPGuardian.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.IPGuardiancheckbox, "IP Guardian checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.IPGuardiancheckbox, "IP Guardian checkbox");
			} else {
				Reporter.log("Not IP Guardian checkbox");
			}
			if (SNMPNotification.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, "SNMP Notification checkbox");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.SNMPNotificationcheckbox, TrapTargetAddress);
			} else {
				Reporter.log("Not SNMP Notification checkbox");
			}

			verifyExists(APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown, "Delivery Channel");
			addDropdownValues_commonMethod("Delivery Channel", APTIPAccessNoCPE.APTNoCPE.DeliveryChannelDropdown,
					DeliveryChannel);

			if (RouterBasedFirewall.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.RouterBasedFirewallcheckbox, "Router Based Firewall checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.RouterBasedFirewallcheckbox, "Router Based Firewall checkbox");
			} else {
				Reporter.log("Not Router Based Firewall checkbox");
			}
			if (Qos.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox, "Qos checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.Qoscheckbox, "Qos checkbox");
			} else {
				Reporter.log("Not Qos checkbox");
			}
			if (ExtendPErangetocustomerLAN.equalsIgnoreCase("Yes")) {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ExtendPErangetocustomerLANcheckbox,
						"Extend PE rangeto customer LAN checkbox");
				click(APTIPAccessNoCPE.APTNoCPE.ExtendPErangetocustomerLANcheckbox,
						"Extend PE rangeto customer LAN checkbox");
			} else {
				Reporter.log("Not Extend PE rangeto customer LAN checkbox");
			}
			selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.package_dropdown, "Package", package_dropdownValue);
					if(package_dropdownValue.equalsIgnoreCase("Gold"))
					{
						//verify Managed Service checkbox
						
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.snmpnotification_checkbox, "SNMP Notification", snmpnotification_checkbox);
						//addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerconfigview_ipv4_checkbox, "Router Configuration View IPv4", routerconfigview_ipv4_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerconfigview_ipv6_checkbox, "Router Configuration View IPv6", routerconfigview_ipv6_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.syslogeventview_checkbox, "Syslog Event View", syslogeventview_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.ipguardian_checkbox, "IP Guardian", ipguardian_checkbox);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.deliverychannel_dropdown, "Delivery Channel", deliverychannel_dropdownvalue);

						//Configurations Options panel
						scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.syslogeventview_checkbox);
						compareText("Configuration Options", APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader, "Configuration Options");
						//addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerbasedfirewall_checkbox, "Router Based Firewall", routerbasedfirewall_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.withlogicaltunnel_checkbox, "With Logical Tunnel", withlogicaltunnel_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.qos_checkbox, "Qos", qos_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.modularmsp_checkbox, "Modular MSP", modularmsp_checkbox);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_dropdown, "L2 Technology", L2technology_dropdownvalue);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.routingprotocol_dropdown, "Routing Protocol", routingprotocol_dropdownvalue);
						if(!routingprotocol_dropdownvalue.equalsIgnoreCase("null")) {
							edittextFields_commonMethod("BGP AS Number", APT_IPAccess_VCPEConfigObj.ipaVCPE.BGPasnumber_textfield, BGPasnumber_Value);
							verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, "customerdescription_textfield");
							sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, customerdescription_value, "customerdescription_textfield");

							//addtextFields_commonMethod("Customer Description", APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, customerdescription_value);
						}
					}
					else if(package_dropdownValue.equalsIgnoreCase("Silver"))
					{
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.managedservice_checkbox, "Managed Service", managedservice_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.snmpnotification_checkbox, "SNMP Notification", snmpnotification_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerconfigview_ipv4_checkbox, "Router Configuration View IPv4", routerconfigview_ipv4_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerconfigview_ipv6_checkbox, "Router Configuration View IPv6", routerconfigview_ipv6_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.syslogeventview_checkbox, "Syslog Event View", syslogeventview_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.ipguardian_checkbox, "IP Guardian", ipguardian_checkbox);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.deliverychannel_dropdown, "Delivery Channel", deliverychannel_dropdownvalue);

						//Configurations Options panel
						scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.syslogeventview_checkbox);
						compareText("Configuration Options", APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader, "Configuration Options");
						//addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerbasedfirewall_checkbox, "Router Based Firewall", routerbasedfirewall_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.withlogicaltunnel_checkbox, "With Logical Tunnel", withlogicaltunnel_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.qos_checkbox, "Qos", qos_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.modularmsp_checkbox, "Modular MSP", modularmsp_checkbox);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_dropdown, "L2 Technology", L2technology_dropdownvalue);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.routingprotocol_dropdown, "Routing Protocol", routingprotocol_dropdownvalue);
						if(!routingprotocol_dropdownvalue.equalsIgnoreCase("null")) {
							edittextFields_commonMethod("BGP AS Number", APT_IPAccess_VCPEConfigObj.ipaVCPE.BGPasnumber_textfield, BGPasnumber_Value);
							verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, "customerdescription_textfield");
							sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, customerdescription_value, "customerdescription_textfield");

							//addtextFields_commonMethod("Customer Description", "customerdescription_textfield", customerdescription_value);
						}
					}
					else
					{
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step : package dropdown value is not selected");
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.managedservice_checkbox, "Managed Service", managedservice_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.snmpnotification_checkbox, "SNMP Notification", snmpnotification_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerconfigview_ipv4_checkbox, "Router Configuration View IPv4", routerconfigview_ipv4_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerconfigview_ipv6_checkbox, "Router Configuration View IPv6", routerconfigview_ipv6_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.syslogeventview_checkbox, "Syslog Event View", syslogeventview_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.ipguardian_checkbox, "IP Guardian", ipguardian_checkbox);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.deliverychannel_dropdown, "Delivery Channel", deliverychannel_dropdownvalue);

						//Configurations Options panel
						scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.syslogeventview_checkbox);
						compareText("Configuration Options", APT_IPAccess_VCPEConfigObj.ipaVCPE.configurationoptions_panelheader, "Configuration Options");
						//addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.routerbasedfirewall_checkbox, "Router Based Firewall", routerbasedfirewall_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.withlogicaltunnel_checkbox, "With Logical Tunnel", withlogicaltunnel_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.qos_checkbox, "Qos", qos_checkbox);
						addCheckbox_commonMethod(APT_IPAccess_VCPEConfigObj.ipaVCPE.modularmsp_checkbox, "Modular MSP", modularmsp_checkbox);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.L2technology_dropdown, "L2 Technology", L2technology_dropdownvalue);
						selectValueInsideDropdown(APT_IPAccess_VCPEConfigObj.ipaVCPE.routingprotocol_dropdown, "Routing Protocol", routingprotocol_dropdownvalue);
						if(!routingprotocol_dropdownvalue.equalsIgnoreCase("null")) {
							edittextFields_commonMethod("BGP AS Number", APT_IPAccess_VCPEConfigObj.ipaVCPE.BGPasnumber_textfield, BGPasnumber_Value);
							verifyExists(APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, "customerdescription_textfield");
							sendKeys(APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, customerdescription_value, "customerdescription_textfield");

							//addtextFields_commonMethod("Customer Description", APT_IPAccess_VCPEConfigObj.ipaVCPE.customerdescription_textfield, customerdescription_value);
						}
					}
					scrollDown(APT_IPAccess_VCPEConfigObj.ipaVCPE.nextbutton);
					Thread.sleep(2000);
					waitforPagetobeenable();
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.nextbutton,"Next Button");
					//Thread.sleep(2000);
					waitforPagetobeenable();
					IPConfig.verifysuccessmessage("Service successfully created");

		}
	 
	 public void EditServiceFunction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException {

			String BillingTypeEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Edit_BillingTypevalue");
			String EmailServiceEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Edit_Email");
			String PhoneServiceEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Edit_PhoneContact");
			String RemarksEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RemarksEdit");
			String ManageServiceEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"ManageServiceEdit");
			String RouterConfigurationViewIPv4Edit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"RouterConfigurationViewIPv4Edit");
			String RouterConfigurationViewIPv6Edit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"RouterConfigurationViewIPv6Edit");
			String PerformanceReportingEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"PerformanceReportingEdit");
			String IPGuardianEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IPGuardianEdit");
			String SmartMonitoringEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SmartMonitoringEdit");
			String CloudPrioritizationEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"CloudPrioritizationEdit");
			String ProactiveNotificationEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"ProactiveNotificationEdit");
			String SNMPNotificationEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SNMPNotificationEdit");
			String TrapTargetAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"TrapTargetAddressEdit");
			String RouterBasedFirewallEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"RouterBasedFirewallEdit");
			String QosEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "QosEdit");
			String ActelisBasedEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"ActelisBasedEdit");
			String BGPASNumberEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"BGPASNumberEdit");
			String BGPPasswordEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"BGPPasswordEdit");

			ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Edit Service Functionality");
			// Cancel edit service

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
			

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cancelbutton);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cancelbutton, "Cancel");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.cancelbutton, "Cancel");

			// ScrolltoElement(application, "orderpanelheader", xml);
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.orderpanelheader);

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.servicepanel_header)) {
				Report.LogInfo("INFO", "Step : Navigated to view service page", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Navigated to view service page");
			} 
			// Edit service
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.serviceactiondropdown, "Action dropdown");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.edit, "Edit");

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.billingtype_dropdown);
			SelectDropdownValueUnderSelectTag("Billing Type", BillingTypeEdit,
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.billingtype_dropdown);

			ClearAndEnterTextValue("Email", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.emailtextfield_Service,
					EmailServiceEdit);
			ClearAndEnterTextValue("Phone Contact",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.phonecontacttextfield, PhoneServiceEdit);
			ClearAndEnterTextValue("Remark", APT_IPAccessResilientConfigObj.IPAccessResilientConfig.remarktextarea,
					RemarksEdit);

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.managementoptions_header);// ConfigurationOptionEditServicePage_Header
			// management options panel
			if (ManageServiceEdit.equalsIgnoreCase("Yes")) {
				editcheckbox_commonMethod(ManageServiceEdit,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.ManageServicecheckbox, "Manage Service");
			} else {
				Report.LogInfo("INFO", "Step : Manage Service checkbox is not checked", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Manage Service checkbox is not checked");
			}

			if (RouterConfigurationViewIPv4Edit.equalsIgnoreCase("Yes")) {
				editcheckbox_commonMethod(RouterConfigurationViewIPv4Edit,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.RouterConfigurationViewIPv4checkbox,
						"Router Configuration View IPv4");
			} else {
				Report.LogInfo("INFO", "Step : Router Configuration View IPv4 checkbox is not checked", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : Router Configuration View IPv4 checkbox is not checked");
			}

			if (RouterConfigurationViewIPv6Edit.equalsIgnoreCase("Yes")) {
				editcheckbox_commonMethod(RouterConfigurationViewIPv6Edit,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.RouterConfigurationViewIPv6checkbox,
						"Router Configuration View IPv6");
			} else {
				Report.LogInfo("INFO", "Step : Router Configuration View IPv6 checkbox is not checked", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : Router Configuration View IPv6 checkbox is not checked");
			}

			if (PerformanceReportingEdit.equalsIgnoreCase("Yes")) {
				editcheckbox_commonMethod(PerformanceReportingEdit,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.performancereportingcheckbox,
						"Performance Reporting");
			} else {
				Report.LogInfo("INFO", "Step : Performance Reporting checkbox is not checked", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Performance Reporting checkbox is not checked");
			}

			if (IPGuardianEdit.equalsIgnoreCase("Yes")) {
				editcheckbox_commonMethod(IPGuardianEdit,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.IPGuardiancheckbox, "IP Guardian");
			} else {
				Report.LogInfo("INFO", "Step : IP Guardian checkbox is not checked", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : IP Guardian checkbox is not checked");
			}

			
			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.editservice_okbutton);
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.editservice_okbutton, "Ok");

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.customerdetailsheader)) {
				verifysuccessmessage("Service successfully updated");
			} else {
				System.out.println("Service not updated");
			}
		}

		public void verifysuccessmessage(String expected) throws InterruptedException {

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.alertMsg);

			try {

				// boolean successMsg=getwebelement(xml.getlocator("//locators/" +
				// application + "/alertMsg")).isDisplayed();

				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.alertMsg)) {

					// String alrtmsg=getwebelement(xml.getlocator("//locators/" +
					// application +
					// "/AlertForServiceCreationSuccessMessage")).getText();
					String alrtmsg = getTextFrom(
							APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AlertForServiceCreationSuccessMessage,"Alert For Service Creation Success Message");
					if (expected.contains(alrtmsg)) {
						Report.LogInfo("INFO", "Message is verified. It is displaying as: " + alrtmsg, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Message is verified. It is displaying as: " + alrtmsg);

					} else {
						Report.LogInfo("INFO", "Message is displaying and it gets mismatches. It is displaying as: "
								+ alrtmsg + " .The Expected value is: " + expected, "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								"Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg
										+ " .The Expected value is: " + expected);
					}

				} else {
					Report.LogInfo("INFO", " Success Message is not displaying", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");

				}

			} catch (Exception e) {
				Report.LogInfo("INFO", expected + " Message is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, expected + " Message is not displaying");

			}
		}
	 
		public void clickOnBreadCrumb(String sid) throws InterruptedException{

			scrollIntoTop();
			Thread.sleep(2000);
			WebElement breadcrumb=null;

			try {
				//breadcrumb=getwebelement(xml.getlocator("//locators/" + application + "/breadcrumb").replace("value", sid));
				
				//if(breadcrumb.isDisplayed())
				if(isVisible(APT_IPAccess_VCPEConfigObj.ipaVCPE.breadcrumb1+sid+APT_IPAccess_VCPEConfigObj.ipaVCPE.breadcrumb2))
				{
					click(APT_IPAccess_VCPEConfigObj.ipaVCPE.breadcrumb1+sid+APT_IPAccess_VCPEConfigObj.ipaVCPE.breadcrumb2,"Bread Crumb");
					//Clickon(breadcrumb);
				}else {
					System.out.println("Breadcrumb is not displaying for the element "+ breadcrumb);
				}
			}catch(Exception e) {
				e.printStackTrace();
				System.out.println("Breadcrumb is not displaying for the element "+ breadcrumb);
			}
		}
		
		public void deleteDevice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException, AWTException {

			waitForAjax();
			if (isVisible(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
				scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);
				scrollDown(APTIPAccessNoCPE.APTNoCPE.addeddevicesDeleteServiceLink);
				javaScriptclick(APTIPAccessNoCPE.APTNoCPE.addeddevicesDeleteServiceLink,"Delet link");
				
				Robot r = new Robot();
				Thread.sleep(1000);
				r.keyPress(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
				r.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
				
				}
			} 
		
		public void VerifyEditInterface_JuniperVendor(String testDataFile, String dataSheet, String scriptNo,
				String dataSetNo) throws InterruptedException, IOException, AWTException {
			String InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterfaceName");

			String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewDeviceName");
			String edit_eipallocation1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"edit_eipallocation1");
			String edit_eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"ExistingCityValue");
			String edit_eipallocation_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_EIPAllocation_Subnetsize");
			String edit_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_GetAddress");
			String edit_interfaceaddressrange_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_InterfaceAddressRange_value");
			String edit_eipallocation2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_EIPAllocation2");
			String edit_eipallocation_ipv6_subnetsize = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "Edit_EIPAllocation_IPv6_Subnetsize");
			String edit_ipv6_getaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_IPv6_GetAddress");
			String edit_interfaceaddressrangeIPv6_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "Edit_InterfaceAddressRangeIPv6_value");
			String edit_linkvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_LinkValue");
			// String edit_network = DataMiner.fngetcolvalue(testDataFile,
			// dataSheet, scriptNo, dataSetNo, "Edit_Network");
			String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_BearerType_value");
			String edit_bandwidth_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_Bandwidth_value");
			String edit_cardtype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_CardType_DropdownValue");
			String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_EncapsulationValue");
			String edit_framingtype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_FramingTypeValue");
			String edit_clocksource_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_ClockSourceValue");
			String edit_STM1Number_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_STM1NumberValue");
			String edit_bearerNo_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_BearerNumber_Value");
			String edit_unitid_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_UnitIDValue");
			String edit_slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_SlotValue");
			String edit_pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_PicValue");
			String edit_port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_PortValue");
			String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_VLANIDvalue");
			String edit_juniper_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "Edit_Juniper_ConfigInterface_checkbox");

			Reporter.log("Verifying Edit Interface Functionality - Juniper");

			if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
				if (isClickable(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid)) {
					

					scrollDown(APTIPAccessNoCPE.APTNoCPE.existingdevicegrid);
					waitForAjax();
					webDriver.findElement(By.xpath(
							"//tr//*[contains(text(),'" + InterfaceName + "')]//following-sibling::td//a[text()='Edit']"))
							.click();
					// verifyExists(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");
					// click(APTIPAccessNoCPE.APTNoCPE.edit, "Edit");

					if (isClickable(APTIPAccessNoCPE.APTNoCPE.editinterface_header)) {
						Reporter.log("Edit Interface' page navigated as expected");

						scrollDown(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox);
						waitForAjax();
						verifyExists(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox,
								"Configure Interface on Device");
						click(APTIPAccessNoCPE.APTNoCPE.configureinterface_checkbox, "Configure Interface on Device");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue, "Network");
						// selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.network_fieldvalue,edit_network,"Network");
						// verifyExists(APTIPAccessNoCPE.APTNoCPE.Interfacefield,
						// "Interface search");
						// sendKeys(APTIPAccessNoCPE.APTNoCPE.Interfacefield,
						// InterfaceName, "Interface send");

						if (edit_eipallocation1.equalsIgnoreCase("yes")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");
							click(APTIPAccessNoCPE.APTNoCPE.eipallocation1_button, "EIP Allocation button");

							if (isClickable(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header)) {
								verifyExists(APTIPAccessNoCPE.APTNoCPE.eipsubnetallocation_header, "EIP Subnet Allocation");
								verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_value, "Subnet Type");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown, "City");
								selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_citydropdown,
										edit_eipallocation_city, "City");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize, "Sub Net Size0");
								selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_subnetsize,
										edit_eipallocation_subnetsize, "Sub Net Size0");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_availablepools_value,
										"Available Pools");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet button");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
								click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
							} else {
								Reporter.log("EIP Allocation button is not working");

							}
						}
						if (edit_getaddress.equalsIgnoreCase("yes")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");
							click(APTIPAccessNoCPE.APTNoCPE.getaddress1_button, "Get Address");

							if (isClickable(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown)) {
								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
										"Interface Address Range");
								selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.interfacerange_Address_dropdown,
										edit_interfaceaddressrange_value, "Address dropdown");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
								click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
							} else {
								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
										"Interface Address Range");
								sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrange_textfield,
										edit_interfaceaddressrange_value, "Interface Address Range");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
										"Interface Address Range Arrow");
								click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow, "Interface Address Range Arrow");
							}
						}

						// verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
						// "Interface Address Range Arrow");
						// click(APTIPAccessNoCPE.APTNoCPE.interfaceaddress_Addarrow,
						// "Interface Address Range Arrow");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.networkipv6_fieldvalue, "Network");

						// verify EIP Allocation
						if (edit_eipallocation2.equalsIgnoreCase("yes")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");
							click(APTIPAccessNoCPE.APTNoCPE.eipallocation2_button, "EIP Allocation button");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocationIPv6_header,
									"EIP Address Allocation header");
							verifyExists(APTIPAccessNoCPE.APTNoCPE.subnettype_value, "Subnet Type");
							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_spacename, "Space Name");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize, "Sub Net Size");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.eipallocation_ipv6_subnetsize,
									edit_eipallocation_ipv6_subnetsize, "ipv6 sub net size");

							if (isClickable(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown)) {
								verifyExists(APTIPAccessNoCPE.APTNoCPE.availableblocks_dropdown, "Available Blocks");
								verifyExists(APTIPAccessNoCPE.APTNoCPE.allocatesubnet_button, "Allocate subnet");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");
								click(APTIPAccessNoCPE.APTNoCPE.closesymbol, "Close");

								// verify getaddress ipv6
								if (edit_ipv6_getaddress.equalsIgnoreCase("yes")) {
									verifyExists(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");
									click(APTIPAccessNoCPE.APTNoCPE.getaddress2_button, "Get Address");

									verifyExists(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown,
											"Sub Net Size");
									selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.interfacerange_AddressIpv6_dropdown,
											edit_interfaceaddressrangeIPv6_value, "IP6 value");

									verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
											"Interface Address Range IPv6 Arrow");
									click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
											"Interface Address Range IPv6 Arrow");
								} else {
									verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
											"Interface Address Range");
									sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
											edit_interfaceaddressrangeIPv6_value, "Interface Address Range IPv6");

									verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
											"Interface Address Range IPv6 Arrow");
									click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
											"Interface Address Range IPv6 Arrow");
								}
							} else {
								Reporter.log("Available Blocks dropdown values are not displaying");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.eipallocation_popupclose, "Close");
								click(APTIPAccessNoCPE.APTNoCPE.eipallocation_popupclose, "Close");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
										"Interface Address Range IPv6");
								sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceaddressrangeIPv6_textfield,
										edit_interfaceaddressrangeIPv6_value, "Interface Address Range IPv6");

								verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");
								click(APTIPAccessNoCPE.APTNoCPE.interfaceaddressIPv6_Addarrow,
										"Interface Address Range IPv6 Arrow");

							}
						}

						verifyExists(APTIPAccessNoCPE.APTNoCPE.link_textfield, "Link");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.link_textfield, edit_linkvalue, "Link");
						
						if(isVisible(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit)){
						verifyExists(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, "Bearer Type");
						selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bearertype_dropdownEdit, edit_bearertype_value,
								"Bearer Type");
						}
						if (edit_bearertype_value.equalsIgnoreCase("10Gigabit Ethernet")
								|| edit_bearertype_value.equalsIgnoreCase("Gigabit Ethernet")
								|| edit_bearertype_value.equalsIgnoreCase("Gigabit Ethernet")) {
							if(isVisible(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown)){
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
									"Bandwidth");
							}
							verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
									"Card Type");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
									edit_encapsulation_value, "Encapsulation");
																
							
							/*verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, edit_framingtype_value,
									"Framing Type");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, "Clock Source");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, edit_clocksource_value,
									"Clock Source");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
							*/	
						} else if (edit_bearertype_value.equalsIgnoreCase("E1")) {
							if(isVisible(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown)){
								verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
								selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
										"Bandwidth");
								}

							verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
									"Card Type");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
									edit_encapsulation_value, "Encapsulation");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, "Framing Type");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.framingtype_dropdown, edit_framingtype_value,
									"Framing Type");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, "Clock Source");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.clocksource_dropdown, edit_clocksource_value,
									"Clock Source");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
						} else if (edit_bearertype_value.equalsIgnoreCase("E3")
								|| edit_bearertype_value.equalsIgnoreCase("T3")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
									"Bandwidth");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
									"Card Type");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
									edit_encapsulation_value, "Encapsulation");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, "Bearer No");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.bearerno_textfield, edit_bearerNo_value, "Bearer No");
						} else if (edit_bearertype_value.equalsIgnoreCase("STM-1")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, "Bandwidth");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.bandwidth_dropdown, edit_bandwidth_value,
									"Bandwidth");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, "Card Type");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.cardtype_dropdown, edit_cardtype_dropdownvalue,
									"Card Type");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
									edit_encapsulation_value, "Encapsulation");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");
						} else {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit, "Encapsulation");
							selectByVisibleText(APTIPAccessNoCPE.APTNoCPE.encapsulation_dropdownEdit,
									edit_encapsulation_value, "Encapsulation");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, "STM1 Number");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.STM1Number_textfield, edit_STM1Number_value, "STM1 Number");
						}

						verifyExists(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, "Unit ID");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.unitid_textfield, edit_unitid_value, "Unit ID");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.slot_textfield, "Slot");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.slot_textfield, edit_slot_value, "Slot");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.pic_textfield, "Pic");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.pic_textfield, edit_pic_value, "Pic");

						verifyExists(APTIPAccessNoCPE.APTNoCPE.port_textfield, "Port");
						sendKeys(APTIPAccessNoCPE.APTNoCPE.port_textfield, edit_port_value, "Port");

						// String edit_Juniper_InterfaceName = getAttributeFrom(
						// APTIPAccessNoCPE.APTNoCPE.interfacename_textfield,
						// "value");
						if (edit_encapsulation_value.equalsIgnoreCase("802.1q")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, "VLAN Id");
							clearTextBox(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield);
							sendKeys(APTIPAccessNoCPE.APTNoCPE.vlanid_textfield, edit_vlanID_value, "VLAN Id");
						}
						verifyExists(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");
						click(APTIPAccessNoCPE.APTNoCPE.ivmanagement_checkbox, "IV Management");
						
						if(isVisible(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox)){
						verifyExists(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");
						click(APTIPAccessNoCPE.APTNoCPE.atricaconnected_checkbox, "Atrica Connected");
						}
						// verifyExists(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox,
						// "BGP");
						// click(APTIPAccessNoCPE.APTNoCPE.bgp_checkbox, "BGP");

						// configuration panel in edit interface page
						if (edit_juniper_configureinterface_checkbox.equalsIgnoreCase("Yes")) {
							verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_header, "Configuration");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");
							click(APTIPAccessNoCPE.APTNoCPE.generateconfiguration_button, "Generate Configuration");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");
							click(APTIPAccessNoCPE.APTNoCPE.GenerateConfigurationButton2, "Generate Configuration");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.configuration_textarea, "Configuration");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");
							click(APTIPAccessNoCPE.APTNoCPE.executeandsave_button, "Execute and Save");

							Alert alert = webDriver.switchTo().alert();

							// Capturing alert message.
							String alertMessage = webDriver.switchTo().alert().getText();
							if (alertMessage.isEmpty()) {
								Reporter.log("No message displays");

							} else {
								Reporter.log("Alert message displays as: " + alertMessage);

							}
							alert.accept();
						} else {
							noCPE.editedInterfaceName = getAttributeFrom(APTIPAccessNoCPE.APTNoCPE.Interfacefield, "value");

							verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
							click(APTIPAccessNoCPE.APTNoCPE.okbutton, "OK");
							if(isAlertPresent()==true){
								Robot r = new Robot();
								Thread.sleep(1000);
								r.keyPress(KeyEvent.VK_ENTER);
								Thread.sleep(1000);
								r.keyRelease(KeyEvent.VK_ENTER);
								Thread.sleep(1000);
							}
							
						}
						Reporter.log("Interface successfully updated.");
					} else {
						Reporter.log("Edit Interface' page not navigated");

					}
				} else {
					Reporter.log("Interface is not added");
				}
			} else {
				Reporter.log("No Device added in grid");
			}
		}
		
		public void SelectInterfacetoremovefromservice(String testDataFile, String dataSheet, String scriptNo,
				String dataSetNo) throws InterruptedException, IOException {
			//String Edit_InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					//"Edit_InterfaceName");
			// String InterfaceName = DataMiner.fngetcolvalue(testDataFile,
			// dataSheet, scriptNo, dataSetNo, "InterfaceName");
			waitforPagetobeenable();
			waitForAjax();
			

			scrollDown(APTIPAccessNoCPE.APTNoCPE.interfacesinservice_filter);
			waitForAjax();
			
			webDriver.findElement(By.xpath("(//div[@col-id='interfaceName'][text()='" + noCPE.editedInterfaceName + "'])[1]"))
			.click();
			waitForAjax();
			/*if (Edit_InterfaceName.equalsIgnoreCase("null")) {
				webDriver.findElement(By.xpath("(//div[@col-id='interfaceName'][text()='" + noCPE.editedInterfaceName + "'])[1]"))
						.click();
				waitForAjax();
			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_fitertext, "Interface in Service search");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_fitertext, Edit_InterfaceName,
						"Interface in Service search");

				String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interfaceinservice_gridselect)
						.replace("value", Edit_InterfaceName);
				click(CheckToAddInterface);
			}*/
			verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_Actiondropdown, "Action Dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_Actiondropdown, "Action Dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_removebuton, "Remove");
			click(APTIPAccessNoCPE.APTNoCPE.InterfaceInselect_removebuton, "Remove");
			verifyExists(APTIPAccessNoCPE.APTNoCPE.RemoveSelectInterfaceSuccessMessage, "Remove Interface Success Message");
		}
		
		public void SelectInterfacetoaddwithservcie(String testDataFile, String dataSheet, String scriptNo,
				String dataSetNo) throws InterruptedException, IOException {
			//String Edit_InterfaceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					//"Edit_InterfaceName");
			// String InterfaceName = DataMiner.fngetcolvalue(testDataFile,
			// dataSheet, scriptNo, dataSetNo, "InterfaceName");
			scrollDown(APTIPAccessNoCPE.APTNoCPE.InteraceColumn_Filter);
			waitForAjax();
			webDriver.findElement(By.xpath("(//div[@col-id='interfaceName'][text()='" + noCPE.editedInterfaceName + "'])[1]"))
			.click();
			waitForAjax();
			
			/*if (Edit_InterfaceName.equalsIgnoreCase("null")) {
				

			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfacefilterTxt, "Interface search");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.InterfacefilterTxt, Edit_InterfaceName, "Interface search");

				String CheckToAddInterface = getTextFrom(APTIPAccessNoCPE.APTNoCPE.interface_gridselect).replace("value",
						Edit_InterfaceName);
				click(CheckToAddInterface);

			}
			waitForAjax();*/
			verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_Actiondropdown, "Action Dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_Actiondropdown, "Action Dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_addbuton, "Add");
			click(APTIPAccessNoCPE.APTNoCPE.InterfaceToselect_addbuton, "Add");

			scrollDown(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");
			click(APTIPAccessNoCPE.APTNoCPE.viewpage_backbutton, "Back");
		}

		
}
