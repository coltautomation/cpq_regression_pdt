package testHarness.siebelFunctions;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.siebelObjects.SiebleInFlightOrderObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

public class SebielInFlightOrder extends SeleniumUtils {

	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();

	public void ServiceTabInFlight (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception  {
		
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String ProductName1=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Samle");
		if (!ProductName.equalsIgnoreCase("CPE Solutions Service")&&!ProductName.equalsIgnoreCase("IP VPN Service")) {
           //Open Workflow Tab 
			Reusable.waitForSiebelLoader();
			verifyExists(SiebleInFlightOrderObj.ServiceTab.ClickLink.replace("Value", "Workflows"));
			click(SiebleInFlightOrderObj.ServiceTab.ClickLink.replace("Value", "Workflows"));	
			Reusable.waitForSiebelLoader();

			verifyExists(SiebleInFlightOrderObj.ServiceTab.DeliveryLineItem,"Delivery Item line");
			click(SiebleInFlightOrderObj.ServiceTab.DeliveryLineItem,"Delivery Item line");	
			Reusable.waitForSiebelLoader();

			verifyExists(SiebleInFlightOrderObj.ServiceTab.ChangeWorkflowBtn,"Change Workflow Button");
			click(SiebleInFlightOrderObj.ServiceTab.ChangeWorkflowBtn,"Change Workflow Button");	
			Reusable.waitForSiebelLoader();
			
			verifyExists(SiebleInFlightOrderObj.ServiceTab.YesBtn,"Yes Button");
			click(SiebleInFlightOrderObj.ServiceTab.YesBtn,"Yes Button");	
			Reusable.waitForSiebelLoader();
						        
			switch (ProductName) {
			case "Number Hosting": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitToPageLoad();
				verifyExists(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection , "Verify Inflight Voice Selection");
				click(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection,"Click Inflight Voice Selection");
				Reusable.waitForSiebelLoader();
				break;
				
			}
			case "Interconnect": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection , "Verify Inflight Voice Selection");
				click(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection,"Click Inflight Voice Selection");
				Reusable.waitForSiebelLoader();
				break;
				
			}
			case "SIP Trunking": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection , "Verify Inflight Voice Selection");
				click(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection,"Click Inflight Voice Selection");
				Reusable.waitForSiebelLoader();
				break;
			}
			case "Voice Line V": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection , "Verify Inflight Voice Selection");
				click(SiebleInFlightOrderObj.ServiceTab.InflightVoiceSelection,"Click Inflight Voice Selection");
				Reusable.waitForSiebelLoader();
				break;
				
			}

			case "IP Access": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				break;
			}
			case "Ethernet Spoke": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				break;
			}
			case "Ethernet Hub": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				break;
			}

			case "Ethernet Line": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				break;
			}
			case "Wave": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				break;
			}

			case "Private Ethernet": {

				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Bandwidth");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				Reusable.waitForSiebelLoader();
				break;

			}
			case "Dark Fibre": {
		
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				Reusable.waitForSiebelLoader();
				break;

			}
			case "DCA Ethernet": {
				 //for selecting B end primary//
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				 //for selecting B end primary//
				verifyExists(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				break;
				
			}

			case "Ultra Low Latency": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Bandwidth");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				Reusable.waitForSiebelLoader();
				break;

			}

			case "Private Wave Node": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				Reusable.waitForSiebelLoader();
				break;

			}

			case "Private Wave Service": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.BSiteDropDwn,"B Site Dropdown");
				Reusable.waitForSiebelLoader();


				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Bandwidth");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				Reusable.waitForSiebelLoader();
				break;
			

			}

			case "Ethernet Access": {
				//for selecting A end primary//
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");	
				Reusable.waitForSiebelLoader();
				 				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");	
				Reusable.waitForSiebelLoader();
				
				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");	
				Reusable.waitForSiebelLoader();
				break;

			}

			case "Ethernet VPN Access": {
				verifyExists(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				click(SiebleInFlightOrderObj.ServiceTab.ASiteDropDwn,"A Site Dropdown");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				click(SiebleInFlightOrderObj.ServiceTab.DropDwnValue.replace("Value", "Bandwidth"),"Dropdown value");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				click(SiebleInFlightOrderObj.ServiceTab.ProceedBtn,"Proceed button");
				Reusable.waitForSiebelLoader();

				verifyExists(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				click(SiebleInFlightOrderObj.ServiceTab.OKBtn,"OK button");
				Reusable.waitForSiebelLoader();
				break;

			}

			case "IP VPN Service": {
				
				if (ProductName1.equalsIgnoreCase("IP VPN Plus")) {
					System.out.println("enter into else-if loop of vpn plus");
				} else if (ProductName1.equalsIgnoreCase("IP VPN Access")) {
					System.out.println("enter into else-if loop of vpn access");
				}
				
				else if (ProductName1.equalsIgnoreCase("IP VPN Wholesale")) {
					System.out.println("enter into else-if loop of vpn wholesale");
				}

				else if (ProductName1.equalsIgnoreCase("Swiftnet")) {
					System.out.println("enter into else-if loop of swiftnet");
				}

				else if (ProductName1.equalsIgnoreCase("Prizmnet")) {
					System.out.println("enter into else-if loop of prizmnet");
				}
				break;

			}

			}
			//Workflow save
			verifyExists(SiebleInFlightOrderObj.ServiceTab.WorkflowSave , "Verify Workflow Save");
			click(SiebleInFlightOrderObj.ServiceTab.WorkflowSave,"Click Workflow Save");			
			waitToPageLoad();
			Reusable.waitForSiebelLoader();
		} // end of inner if loop

		switch (ProductName) {
		case "Number Hosting": {
			NumberHostingInflight(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "SIP Trunking": {
			SIPTrunkingInflight(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "Interconnect": {
			NumberHostingInflight(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "Voice Line V": {
			VoiceLineVInflight(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Ethernet Spoke": {
			ethernetSpoke(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "Ethernet Hub": {
			ethernetSpoke(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "Ethernet Line": {
			ethernetSpoke(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "Wave": {
			ethernetSpoke(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}
		case "IP Access": {
			ipAccess(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Private Ethernet": {
			privateEthernet(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Dark Fibre": {
			privateEthernet(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "DCA Ethernet": {
			privateEthernet(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Ultra Low Latency": {
			privateEthernet(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Private Wave Node": {
			privateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Private Wave Service": {
			privateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Ethernet Access": {
			privateWaveNode(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "Ethernet VPN Access": {
			privateEthernet(testDataFile, sheetName, scriptNo,dataSetNo);
			break;
		}

		case "IP VPN Service": {
			if (ProductName1.equalsIgnoreCase("IP VPN Plus")) {
				PlusAccessWHSwifPrizm();

			} else if (ProductName1.equalsIgnoreCase("IP VPN Access")) {
				PlusAccessWHSwifPrizm();
			}

			else if (ProductName1.equalsIgnoreCase("IP VPN Wholesale")) {
				PlusAccessWHSwifPrizm();
			}

			else if (ProductName1.equalsIgnoreCase("SWIFTNet")) {
				PlusAccessWHSwifPrizm();
			}

			else if (ProductName1.equalsIgnoreCase("PrizmNet")) {
				PlusAccessWHSwifPrizm();
			}
			break;
		}
		case "CPE Solutions Service": {
			cpeSolutions();
			break;
		}

		default:
			System.out.println("Above product is not exist in current list");
			break;
		}
	}

	public void cpeSolutions() throws Exception {

		//String Fieldtitle1 =getAttributeFrom(SiebleInFlightOrderObj.cpeSolutions.FieldLabel.replace("ChangeFieldName", "Service Bandwidth"),"title");
		if(isElementPresent(By.xpath("//span[text()='Service Bandwidth']//i[@title='An in-flight change was made to this attribute']"),"In Flight Icon"))
		{
			Report.LogInfo("In-Flight Icon ","is pressent on controller", "INFO");
				
		}else{
			Report.LogInfo("In-Flight Icon ","is not pressent on controller", "INFO");
		}
			
		verifyExists(SiebleInFlightOrderObj.cpeSolutions.ServiceBandwidthCpe, "Service Bandwidth Cpe");	
		click(SiebleInFlightOrderObj.cpeSolutions.ServiceBandwidthCpe.replace("Value", "Service Bandwidth Cpe"));	
		Reusable.waitForSiebelLoader();
		click(SiebleInFlightOrderObj.cpeSolutions.ValueInsideDropdown.replace("Data", "6 Mbps"));	
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		//verifyExists(SiebleInFlightOrderObj.cpeSolutions.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.cpeSolutions.ClickheretoSaveAccess, "Click here to Save Access");	
		//Reusable.waitForSiebelLoader();
		 
		//Fieldtitle1 =getAttributeFrom(SiebleInFlightOrderObj.cpeSolutions.FieldLabel.replace("ChangeFieldName", "Service Bandwidth"),"title");
		if(isElementPresent(By.xpath("//span[text()='Service Bandwidth']//i[@title='An in-flight change was made to this attribute']"),"In Flight Icon"))
		{
			Report.LogInfo("In-Flight Icon ","Validated service bandwidth in flight icon is displayed after modification", "INFO");
				
		}else{
			Report.LogInfo("In-Flight Icon ","Validated service bandwidth in flight icon is not displayed after modification", "INFO");
		}	
		/*assertTrue(Fieldtitle3.contains("flight"), "After Change to In- Flight title appear for field");
		
		if (Fieldtitle3.contains("flight")) {
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated service bandwidth in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated service bandwidth in flight icon is not displayed after modification");
		}*/

	}

	public void PlusAccessWHSwifPrizm() throws Exception { 
		// Open IP Details
				
		verifyExists(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.ClickLink.replace("Value", "IP Details"));	
		click(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.ClickLink.replace("Value", "IP Details"));	
		Reusable.waitForSiebelLoader();
		
		
		//********

		//Update customer alias//

		String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.FieldLabelSwift.replace("ChangeFieldName", "Customer Alias"),"title");
		assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");

		click(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.TextInputSwift, "Text Input Swift");	
		clearTextBox(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.TextInputSwift);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clear Customer Alias");
		sendKeys(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.TextInputSwift, "Automation");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.TextInputSwift, Keys.TAB);
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		//verifyExists(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.ClickheretoSaveAccess,"Click here to Save Access");	
		Reusable.waitForSiebelLoader();
		
		Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.PlusAccessWHSwifPrizm.FieldLabelSwift.replace("ChangeFieldName", "Customer Alias"),"title");
		assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field");

		/*if (Fieldtitle1.contains("flight")) {
			ExtentTestManager.getTest().log(LogStatus.PASS,"Step: Validated customer alias in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL,"Step: Validated customer alias in flight icon is not displayed after modification");
		}*/
		if (Fieldtitle1.contains("flight")) {
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title appear for field", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated customer alias in flight icon is displayed after modification");
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is displayed after modification", "PASS");
		} else {
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title not appear for field", "PASS");
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated customer alias in flight icon is not displayed after modification");
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is not displayed after modification", "FAIL");
		}
	}

	public void privateEthernet(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

		// Open Sites Tab
		verifyExists(SiebleInFlightOrderObj.privateEthernet.ClickLink.replace("Value", "Sites"));	
		click(SiebleInFlightOrderObj.privateEthernet.ClickLink.replace("Value", "Sites"));	
		Reusable.waitForSiebelLoader();
		//********

		//Update physical port ID//

		ScrollIntoViewByString(SiebleInFlightOrderObj.privateEthernet.AccessPortAend);
		String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.privateEthernet.FieldLabelR4.replace("ChangeFieldName", "Physical Port ID"),"title");
		assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");
		click(SiebleInFlightOrderObj.privateEthernet.TextInputR4.replace("Value", "Physical Port ID"));
		clearTextBox(SiebleInFlightOrderObj.privateEthernet.TextInputR4.replace("Value", "Physical Port ID"));

		sendKeys(SiebleInFlightOrderObj.privateEthernet.TextInputR4.replace("Value", "Physical Port ID"), "98743");
		Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.privateEthernet.TextInputR4.replace("Value", "Physical Port ID"),Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		//verifyExists(SiebleInFlightOrderObj.privateEthernet.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.privateEthernet.ClickheretoSaveAccess,"Click here to Save Access");	
		//Reusable.waitForSiebelLoader();
		Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.privateEthernet.FieldLabelR4.replace("ChangeFieldName", "Physical Port ID"),"title");		
		assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field");

		/*if (Fieldtitle1.contains("flight")) {
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated physical port id in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated physical port id in flight icon is not displayed after modification");
		}*/
		if (Fieldtitle1.contains("flight")) {
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title appear for field", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated physical port ID in flight icon is displayed after modification");
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is displayed after modification", "PASS");
		} else {
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title not appear for field", "PASS");
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated physical port ID in flight icon is not displayed after modification");
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is not displayed after modification", "FAIL");
		}
	}

	public void privateWaveNode(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {

		// Open Sites Tab
		verifyExists(SiebleInFlightOrderObj.privateWaveNode.ClickLink.replace("Value", "Sites"));	
		click(SiebleInFlightOrderObj.privateWaveNode.ClickLink.replace("Value", "Sites"));	
		Reusable.waitForSiebelLoader();
		//********

		//Update shelf ID//
		Thread.sleep(6000);
		ScrollIntoViewByString(SiebleInFlightOrderObj.privateWaveNode.TerminationInformationAEnd);
		String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.privateWaveNode.FieldLabelR4.replace("ChangeFieldName", "Shelf ID"),"title");
		assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");

		click(SiebleInFlightOrderObj.privateWaveNode.TextInputR4.replace("Value", "Shelf ID"));
		clearTextBox(SiebleInFlightOrderObj.privateWaveNode.TextInputR4.replace("Value", "Shelf ID"));
		sendKeys(SiebleInFlightOrderObj.privateWaveNode.TextInputR4.replace("Value", "Shelf ID"), "42");
		Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.privateWaveNode.TextInputR4.replace("Value", "Shelf ID"),Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		
		//verifyExists(SiebleInFlightOrderObj.privateEthernet.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.privateEthernet.ClickheretoSaveAccess,"Click here to Save Access");	
		//Reusable.waitForSiebelLoader();
		
		Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.privateWaveNode.FieldLabelR4.replace("ChangeFieldName", "Shelf ID"),"title");

		assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field");
		if (Fieldtitle1.contains("flight")) {
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated Shelf ID in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated Shelf ID in flight icon is not displayed after modification");
		}

	}

	public void ethernetSpoke(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		Random rand = new Random();
		ReusableFunctions Reusable = new ReusableFunctions();
		//Random random = new Random();
		Reusable.waitForSiebelLoader();
		verifyExists(SiebleInFlightOrderObj.ethernetSpoke.ClickLink.replace("Value", "Sites"));	
		click(SiebleInFlightOrderObj.ethernetSpoke.ClickLink.replace("Value", "Sites"));	
		Reusable.waitForSiebelLoader();
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");

		if (ProductName.equalsIgnoreCase("Ethernet Spoke")
				|| ProductName.equalsIgnoreCase("Ethernet Line")
				|| ProductName.toString().equalsIgnoreCase("Wave")) {
						
			int rand_int1 = rand.nextInt(1000);

			String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.ethernetSpoke.FieldLabelB.replace("ChangeFieldName", "Shelf ID"),"Title");
			//assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");
			if	(!Fieldtitle1.contains("flight")){
				
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Before Change In- Flight not apprear for control", "PASS");
			}
			verifyExists(SiebleInFlightOrderObj.ethernetSpoke.TextInputB.replace("Value", "Shelf ID"),"Shelf ID");
			click(SiebleInFlightOrderObj.ethernetSpoke.TextInputB.replace("Value", "Shelf ID"),"Shelf ID");
			clearTextBox(SiebleInFlightOrderObj.ethernetSpoke.TextInputB.replace("Value", "Shelf ID"));

			sendKeys(SiebleInFlightOrderObj.ethernetSpoke.TextInputB.replace("Value", "Shelf ID"), Integer.toString(rand_int1),"Shelf ID");
			Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.ethernetSpoke.TextInputB.replace("Value", "Shelf ID"),Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			//verifyExists(SiebleInFlightOrderObj.ethernetSpoke.ClickheretoSaveAccess,"Click here to Save Access");	
			//click(SiebleInFlightOrderObj.ethernetSpoke.ClickheretoSaveAccess,"Click here to Save Access");	
			Reusable.waitForSiebelLoader();
			
			Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.ethernetSpoke.FieldLabelB.replace("ChangeFieldName", "Shelf ID"),"Title");
			//assertTrue(Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");		
			/*if	(Fieldtitle1.contains("flight")){
				
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title not appear for field", "PASS");
			}*/
			if (Fieldtitle1.contains("flight")) {
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title appear for field", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated Shelf ID in flight icon is displayed after modification");
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is displayed after modification", "PASS");
			} else {
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title not appear for field", "PASS");
				ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated Shelf ID in flight icon is not displayed after modification");
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is not displayed after modification", "FAIL");
			}
			
//			boolean sign = isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID"));
//			System.out.println(sign);
//			Assert.assertFalse("Element is present",isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID")));
//
//			Clear(getwebelement(xml.getlocator("//locators/TextInputSpoke").replace("value", "Shelf ID").replace("index", "2")));
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: clear shelf id");
//
//			SendKeys(getwebelement(xml.getlocator("//locators/TextInputSpoke").replace("value", "Shelf ID").replace("index", "2")),"3");
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: enter shelf id");
//			WaitforElementtobeclickable(xml.getlocator("//locators/ApplyChangesSpoke"));
//			Clickon(getwebelement(xml.getlocator("//locators/ApplyChangesSpoke")));
//			waitforPagetobeenable();
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: click to save product configuration");
//			Thread.sleep(6000);
//			boolean sign1 = isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID"));
//			System.out.println(sign1);
//			Thread.sleep(6000);
//			Assert.assertTrue("Element is not present",isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID")));
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Shelf id  has been verified");
		}

		else {
			
			int rand_int1 = rand.nextInt(1000);

			String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.ethernetSpoke.FieldLabel.replace("ChangeFieldName", "Shelf ID"),"Title");
			if	(!Fieldtitle1.contains("flight")){
				
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Before Change In- Flight apprear for control", "PASS");
			}
			click(SiebleInFlightOrderObj.ethernetSpoke.TextInput.replace("Value", "Shelf ID"));
			clearTextBox(SiebleInFlightOrderObj.ethernetSpoke.TextInput.replace("Value", "Shelf ID"));

			sendKeys(SiebleInFlightOrderObj.ethernetSpoke.TextInput.replace("Value", "Shelf ID"), Integer.toString(rand_int1));
			Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.ethernetSpoke.TextInput.replace("Value", "Shelf ID"),Keys.TAB);
			Reusable.waitForSiebelLoader();
			Reusable.savePage();
			Reusable.waitForSiebelSpinnerToDisappear();
			
			Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.ethernetSpoke.FieldLabel.replace("ChangeFieldName", "Shelf ID"),"Title");
			//assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field");
			Reusable.waitForSiebelLoader();
			if (Fieldtitle1.contains("flight")) {
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title appear for field", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated Shelf ID in flight icon is displayed after modification");
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is displayed after modification", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated Shelf ID in flight icon is not displayed after modification");
				Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is not displayed after modification", "FAIL");
			}
			
			
			
			
//			boolean sign = isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID"));
//			System.out.println(sign);
//			Assert.assertFalse("Element is present",isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID")));
//
//			Clear(getwebelement(xml.getlocator("//locators/TextInputForR1").replace("value", "Shelf ID")));
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: clear shelf id");
//
//			SendKeys(getwebelement(xml.getlocator("//locators/TextInputForR1").replace("value", "Shelf ID")), "3");
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: enter shelf id");
//			WaitforElementtobeclickable(xml.getlocator("//locators/ApplyChangesSpoke"));
//			Clickon(getwebelement(xml.getlocator("//locators/ApplyChangesSpoke")));
//			waitforPagetobeenable();
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Apply changes");
//			boolean sign1 = isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID"));
//			System.out.println(sign1);
//			Assert.assertTrue("Element is not present",isElementPresent(xml.getlocator("//locators/InflightLoctor").replace("value", "Shelf ID")));
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Shelf id has been verified");
		}
	}

	public void ipAccess(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
		//Open Sites Tab
		verifyExists(SiebleInFlightOrderObj.ipAccess.ClickLink.replace("Value", "Sites"));	
		click(SiebleInFlightOrderObj.ipAccess.ClickLink.replace("Value", "Sites"));	
		Reusable.waitForSiebelLoader();
		//********
		//Update capacity check reference and verified icon
		String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.ipAccess.FieldLabel.replace("ChangeFieldName", "Capacity Check Reference"),"title");
		assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");
		click(SiebleInFlightOrderObj.ipAccess.TextInput.replace("Value", "Capacity Check Reference"));
		clearTextBox(SiebleInFlightOrderObj.ipAccess.TextInput.replace("Value", "Capacity Check Reference"));
		Reusable.waitForSiebelLoader();
		sendKeys(SiebleInFlightOrderObj.ipAccess.TextInput.replace("Value", "Capacity Check Reference"),"123");
		Reusable.waitForSiebelLoader();
		//Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.ipAccess.TextInput,Keys.TAB);
		Reusable.savePage();
		Reusable.waitForSiebelSpinnerToDisappear();
		
		//verifyExists(SiebleInFlightOrderObj.ipAccess.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.ipAccess.ClickheretoSaveAccess,"Click here to Save Access");	
		
		Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.ipAccess.FieldLabel.replace("ChangeFieldName", "Capacity Check Reference"),"title");
		assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field");

		/*if (Fieldtitle1.contains("flight"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated capacity check reference in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated capacity check reference in flight icon is not displayed after modification");
		}*/
		if (Fieldtitle1.contains("flight")) {
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title appear for field", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated capacity check reference in flight icon is displayed after modification");
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is displayed after modification", "PASS");
		} else {
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> After Change to In- Flight title not appear for field", "PASS");
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated capacity check reference in flight icon is not displayed after modification");
			Report.LogInfo("Verify","<b><i>"+Fieldtitle1 +"</i></b> Step: in flight icon is not displayed after modification", "FAIL");
		}
	}

	public void NumberHostingInflight(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		//Open Voice Tab
		Reusable.waitForSiebelLoader();
		verifyExists(SiebleInFlightOrderObj.NumberHostingInflight.ClickLink.replace("Value", "Voice Config"),"Voice Config Tab");	
		click(SiebleInFlightOrderObj.NumberHostingInflight.ClickLink.replace("Value", "Voice Config"),"Voice Config Tab");	
		Reusable.waitForSiebelLoader();
		//********

		//Update Trunk Name and verified icon
		String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.NumberHostingInflight.FieldLabel.replace("ChangeFieldName", "Trunk Name"),"Title");
		assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");
		click(SiebleInFlightOrderObj.NumberHostingInflight.TextInput.replace("Value", "Trunk Name"),"Trunk Name");
		clearTextBox(SiebleInFlightOrderObj.NumberHostingInflight.TextInput.replace("Value", "Trunk Name"));

		sendKeys(SiebleInFlightOrderObj.NumberHostingInflight.TextInput.replace("Value", "Trunk Name"), "abcd");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.SIPTrunkingInflight.CallAdmissionControl,Keys.TAB);
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		
		//verifyExists(SiebleInFlightOrderObj.NumberHostingInflight.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.NumberHostingInflight.ClickheretoSaveAccess,"Click here to Save Access");	
		//Reusable.waitForSiebelLoader();
		 Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.NumberHostingInflight.FieldLabel.replace("ChangeFieldName", "Trunk Name"),"Title");
		 assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field");

		if (Fieldtitle1.contains("flight")) {
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated trunk name in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated trunk name in flight icon is not displayed after modification");
		}
	}

	public void SIPTrunkingInflight(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		verifyExists(SiebleInFlightOrderObj.SIPTrunkingInflight.ClickLink.replace("Value", "Voice Config"));	
		click(SiebleInFlightOrderObj.SIPTrunkingInflight.ClickLink.replace("Value", "Voice Config"));	
        Reusable.waitForSiebelLoader();
		
		
		String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.SIPTrunkingInflight.FieldLabel.replace("ChangeFieldName", "Call Admission Control (Number of Channels)"), "title");
		assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");
		verifyExists(SiebleInFlightOrderObj.SIPTrunkingInflight.CallAdmissionControl);
		click(SiebleInFlightOrderObj.SIPTrunkingInflight.CallAdmissionControl);
		clearTextBox(SiebleInFlightOrderObj.SIPTrunkingInflight.CallAdmissionControl);
		sendKeys(SiebleInFlightOrderObj.SIPTrunkingInflight.CallAdmissionControl, "128");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.SIPTrunkingInflight.CallAdmissionControl,Keys.TAB);
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
		//verifyExists(SiebleInFlightOrderObj.NumberHostingInflight.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.NumberHostingInflight.ClickheretoSaveAccess,"Click here to Save Access");	
		//Reusable.waitForSiebelLoader();
		 Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.SIPTrunkingInflight.FieldLabel.replace("ChangeFieldName", "Call Admission Control (Number of Channels)"), "title");
		 assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field" );

		if (Fieldtitle1.contains("flight")) {
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated call admission control in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated call admission control in flight icon is not displayed after modification");
		}

	}

	public void VoiceLineVInflight(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		//Open voice tab//
		verifyExists(SiebleInFlightOrderObj.VoiceLineVInflight.ClickLink.replace("Value", "Voice Config"));	
		click(SiebleInFlightOrderObj.SIPTrunkingInflight.ClickLink.replace("Value", "Voice Config"));	
        Reusable.waitForSiebelLoader();
		
		
		//Update DDIs and verified icon

		String Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.VoiceLineVInflight.FieldLabel.replace("ChangeFieldName", "Total Number of DDIs"),"Title");
		assertTrue(!Fieldtitle1.contains("flight"),"Before Change In- Flight apprear for control");
		verifyExists(SiebleInFlightOrderObj.VoiceLineVInflight.TotalDDi,"Total Number of DDIs field");
		click(SiebleInFlightOrderObj.VoiceLineVInflight.TotalDDi);
		sendKeys(SiebleInFlightOrderObj.VoiceLineVInflight.TotalDDi, "6");
		Reusable.SendkeaboardKeys(SiebleInFlightOrderObj.VoiceLineVInflight.TotalDDi, Keys.TAB);
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		Reusable.savePage();
		
		//verifyExists(SiebleInFlightOrderObj.NumberHostingInflight.ClickheretoSaveAccess,"Click here to Save Access");	
		//click(SiebleInFlightOrderObj.NumberHostingInflight.ClickheretoSaveAccess);	
		Reusable.waitForSiebelLoader();

		Fieldtitle1 = getAttributeFrom(SiebleInFlightOrderObj.VoiceLineVInflight.FieldLabel.replace("ChangeFieldName", "Total Number of DDIs"),"Title");
		assertTrue(Fieldtitle1.contains("flight"),"After Change to In- Flight title not appear for field");

		if (Fieldtitle1.contains("flight")) {
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Validated total no of DDIs in flight icon is displayed after modification");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL," Step: Validated total no of DDIs in flight icon is not displayed after modification");
		}

	}
}

