package testHarness.cpqFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.GridHelper;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4CLoginObj;
import pageObjects.c4cObjects.C4COpportunityObj;
import pageObjects.c4cObjects.C4CEngagementSalesObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj.engagementpf;
import pageObjects.cpqObjects.CPQQuoteSubmissionObj;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.exploreFunctions.ExploreConfig;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;

public class CPQQuoteCreation extends SeleniumUtils
{
	private static final String BandwithValue = null;

	GlobalVariables g = new GlobalVariables();
	
	ReusableFunctions Reusable = new ReusableFunctions();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CLoginPage C4CLogin = new C4CLoginPage();
	ExploreConfig ExpConfig = new ExploreConfig();
	//CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	ReadingAndWritingTextFile readText=new ReadingAndWritingTextFile();
	GridHelper Grid = new GridHelper();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	
	
	public void HandoverToCPQ(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{		
		//***************************************************************************
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitToPageLoad();
		waitForElementToAppear(CPQQuoteCreationObj.TransactionPage.QuoteId, 30);
		verifyExists(CPQQuoteCreationObj.TransactionPage.QuoteId, "Quote Id");
		String quoteId = getAttributeFrom(CPQQuoteCreationObj.TransactionPage.QuoteId, "value");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Quote_ID", quoteId);	
		
		verifyExists(CPQQuoteCreationObj.TransactionPage.Products_SectionHeader,"Products Section");
		verifyExists(CPQQuoteCreationObj.TransactionPage.Products_SectionContent,"Products Contents");
		verifyExists(CPQQuoteCreationObj.AddProducts.AddProductBtn,"Add Products button");
		
		//***************************************************************************
	}
			
	//Quote Creation
		public String AddProductToQuote(String ProductName) throws Exception
		{		
						
			//***************************************************************************
			
			waitForElementToBeVisible(CPQQuoteCreationObj.AddProducts.AddProductBtn, 180);			
			verifyExists(CPQQuoteCreationObj.AddProducts.AddProductBtn,"Add Products button");
			ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.AddProductBtn);
			canClickElement(isClickable(CPQQuoteCreationObj.AddProducts.AddProductBtn),findWebElement(CPQQuoteCreationObj.AddProducts.AddProductBtn));
			clickByAction(CPQQuoteCreationObj.AddProducts.AddProductBtn);
			Reusable.waitForpageloadmask();
			
			switch(ProductName) {
				case "CPESolutionsSite":
					isPresent(CPQQuoteCreationObj.AddProducts.CPESolutionLnk, 60);
					click(CPQQuoteCreationObj.AddProducts.CPESolutionLnk, "CPESolutionLnk");
					
					//Select CPE Services Option from drop down
					ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.CPEServiceLnk);
					isPresent(CPQQuoteCreationObj.AddProducts.CPEServiceLnk, 60);
					click(CPQQuoteCreationObj.AddProducts.CPEServiceLnk,"'CPE Service' option");	
					break;
					
				case "CPESite":	
					isPresent(CPQQuoteCreationObj.AddProducts.DataLink, 60);
					click(CPQQuoteCreationObj.AddProducts.DataLink, "Data Link");
												
					//Select CPE Solutions link from drop down
					isPresent(CPQQuoteCreationObj.AddProducts.CPESolutionLnk, 60);
					click(CPQQuoteCreationObj.AddProducts.CPESolutionLnk, "CPESolutionLnk");					
					
					ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.CPESiteLnk);
					isPresent(CPQQuoteCreationObj.AddProducts.CPESiteLnk, 60);
					click(CPQQuoteCreationObj.AddProducts.CPESiteLnk,"'CPE Site' Option");
					break;
					
				case "EthernetLine":				
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select Ethernet link from drop down
					verifyExists(CPQQuoteCreationObj.AddProducts.EthernetLnk,"'Ethernet Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.EthernetLnk,"'Ethernet Link' drop down");
					
					//Select EthernetLine Option
					verifyExists(CPQQuoteCreationObj.AddProducts.EthernetLineLnk,"'Ethernet Line' Option");
					click(CPQQuoteCreationObj.AddProducts.EthernetLineLnk,"'Ethernet Linr' Option");
					break;
					
				case "EthernetHub":				
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select Ethernet link from drop down
					verifyExists(CPQQuoteCreationObj.AddProducts.EthernetLnk,"'Ethernet Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.EthernetLnk,"'Ethernet Link' drop down");
					
					//Select Ethernet Hub Option
					verifyExists(CPQQuoteCreationObj.AddProducts.EthernetHubLnk,"'Ethernet Hub' Option");
					click(CPQQuoteCreationObj.AddProducts.EthernetHubLnk,"'Ethernet Hub' Option");
					break;
									
				case "EthernetSpoke":				
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select EthernetLine link from drop down
					verifyExists(CPQQuoteCreationObj.AddProducts.EthernetLnk,"'Ethernet Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.EthernetLnk,"'Ethernet Link' drop down");
					
					//Select Ethernet Spoke Option
					verifyExists(CPQQuoteCreationObj.AddProducts.EthernetSpokeLnk,"'Ethernet Spoke' Option");
					click(CPQQuoteCreationObj.AddProducts.EthernetSpokeLnk,"'Ethernet Spoke' Option");
					break;
					
				case "Wave":				
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select Optical link
					verifyExists(CPQQuoteCreationObj.AddProducts.OpticalLnk,"'Optical Link'");
					click(CPQQuoteCreationObj.AddProducts.OpticalLnk,"'Ethernet Link' drop down");
					
					//Select Wave Option
					verifyExists(CPQQuoteCreationObj.AddProducts.WaveLnk,"'Ethernet Spoke' Option");
					click(CPQQuoteCreationObj.AddProducts.WaveLnk,"'Ethernet Wave' Option");
					break;
				
				case "VPNNetwork":				
					verifyExists(CPQQuoteCreationObj.AddProducts.VpnLink,"'VPN Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.VpnLink,"'VPN Link' drop down");
					
					//Select VPN Data link
					verifyExists(CPQQuoteCreationObj.AddProducts.VpnDataLink,"'VPN Data Link'");
					click(CPQQuoteCreationObj.AddProducts.VpnDataLink,"'VPN Data Link'");
					
					//Select VPN Network Option
					verifyExists(CPQQuoteCreationObj.AddProducts.VpnNetworkLink,"'VPN Network' Option");
					click(CPQQuoteCreationObj.AddProducts.VpnNetworkLink,"'VPN Network' Option");
					break;
					
				case "ColtIpAccess":
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					verifyExists(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					click(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					
					//Select Colt IP Access option from drop down
					verifyExists(CPQQuoteCreationObj.AddProducts.ColtIpAccessLnk,"'Colt IP Access' option");
					ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.ColtIpAccessLnk);
					click(CPQQuoteCreationObj.AddProducts.ColtIpAccessLnk,"'Colt IP Access' option");
					
					//Landing on 'Business Internet Services > IP Access' page.
					waitForAjax();
					//verifyExists(CPQQuoteCreationObj.AddProducts.ColtIpAccessStartLnk,"Start Button on 'IP Access' landing page");
					//click(CPQQuoteCreationObj.AddProducts.ColtIpAccessStartLnk,"Start button on 'IP Access' landing page");
					break;
					
				case "ColtIpDomain":				
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select IP Access link
					verifyExists(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					click(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					
					//Select IP Domain Option
					verifyExists(CPQQuoteCreationObj.AddProducts.ColtIpDomainLnk,"'IP Domain' Option");
					click(CPQQuoteCreationObj.AddProducts.ColtIpDomainLnk,"'IP Domain' Option");
					break;
					
				case "ColtIpGuardian":				
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select IP Access link
					verifyExists(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					click(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					
					//Select IP Guardian Option
					verifyExists(CPQQuoteCreationObj.AddProducts.ColtIpGuardianLnk,"'IP Guardian' Option");
					click(CPQQuoteCreationObj.AddProducts.ColtIpGuardianLnk,"'IP Guardian' Option");
					break;
					
				case "ColtManagedVirtualFirewall":				
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select IP Access link
					verifyExists(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					click(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					
					//Select IP ManagedVirtualFirewall Option
					verifyExists(CPQQuoteCreationObj.AddProducts.ColtManagedVirtualFirewallLnk,"'IP ManagedVirtualFirewall' Option");
					click(CPQQuoteCreationObj.AddProducts.ColtManagedVirtualFirewallLnk,"'IP ManagedVirtualFirewall' Option");
					break;
					
				case "ColtManagedDedicatedFirewall":			
					verifyExists(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.DataLink,"'Data Link' drop down");
					
					//Select IP Access link
					verifyExists(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					click(CPQQuoteCreationObj.AddProducts.IPAccessLnk,"'IP Access' drop down");
					
					//Select IP ManagedVirtualFirewall Option
					verifyExists(CPQQuoteCreationObj.AddProducts.ColtManagedDedicatedFirewallLnk,"'IP ManagedDedicatedFirewall' Option");
					click(CPQQuoteCreationObj.AddProducts.ColtManagedDedicatedFirewallLnk,"'IP ManagedDedicatedFirewall' Option");
					break;
					
				case "Data - Consultancy":
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					
					//Select Professional Services Data
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesDataLnk,"'Professional Services Data' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesDataLnk,"'Professional Services Data' drop down");
					
					//Select IP DataConsultancy Option
					verifyExists(CPQQuoteCreationObj.AddProducts.DataConsultancyLnk,"'Data Consultancy' Option");
					click(CPQQuoteCreationObj.AddProducts.DataConsultancyLnk,"'Data Consultancy' Option");
					break;
					
				case "Data - Project Management":
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					
					//Select Professional Services Data
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesDataLnk,"'Professional Services Data' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesDataLnk,"'Professional Services Data' drop down");
					
					//Select IP Data Project Management Option
					verifyExists(CPQQuoteCreationObj.AddProducts.DataProjectManagement,"'Data Project Management' Option");
					click(CPQQuoteCreationObj.AddProducts.DataProjectManagement,"'Data Project Management' Option");
					break;
					
				case "Data - Service Management":
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					
					//Select Professional Services Data
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesDataLnk,"'Professional Services Data' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesDataLnk,"'Professional Services Data' drop down");
					
					//Select IP Data Service Management Option
					verifyExists(CPQQuoteCreationObj.AddProducts.DataServiceManagement,"'Data Service Management' Option");
					click(CPQQuoteCreationObj.AddProducts.DataServiceManagement,"'Data Service Management' Option");
					break;
					
				case "Voice - Consultancy":
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					
					//Select Professional Voice Services
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesVoiceLnk,"'Professional Services Voice' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesVoiceLnk,"'Professional Services Voice' drop down");
					
					//Select IP Voice Consultancy Option
					verifyExists(CPQQuoteCreationObj.AddProducts.VoiceConsultancyLnk,"'Voice Consultancy' Option");
					click(CPQQuoteCreationObj.AddProducts.VoiceConsultancyLnk,"'Voice Consultancy' Option");
					break;
					
				case "Voice - Project Management":
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					
					//Select Professional Voice Services 
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesVoiceLnk,"'Professional Services Voice' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesVoiceLnk,"'Professional Services Voice' drop down");
					
					//Select IP Voice Project Management Option
					verifyExists(CPQQuoteCreationObj.AddProducts.VoiceProjectManagement,"'Voice Project Management' Option");
					click(CPQQuoteCreationObj.AddProducts.VoiceProjectManagement,"'Voice Consultancy' Option");
					break;
					
				case "Voice - Service Management":
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesLnk,"'Professional Services Link' drop down");
					
					//Select Professional Voice Services 
					verifyExists(CPQQuoteCreationObj.AddProducts.ProfessionalServicesVoiceLnk,"'Professional Services Voice' drop down");
					click(CPQQuoteCreationObj.AddProducts.ProfessionalServicesVoiceLnk,"'Professional Services Voice' drop down");
					
					//Select IP Voice Service Management Option
					verifyExists(CPQQuoteCreationObj.AddProducts.VoiceServiceManagement,"'Voice Service Management' Option");
					click(CPQQuoteCreationObj.AddProducts.VoiceServiceManagement,"'Voice Consultancy' Option");
					break;				
			}	
			Reusable.waitForpageloadmask();
			Reusable.WaitforCPQloader();
			waitForAjax();
			return "True";
		}
		
	public void cpeServiceConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws InterruptedException, IOException 
	{				
			//Initializing the Variable
			String Network_Topology = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Network_Topology");
			
			waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.NetworkTopologyLst, 15);
			verifyExists(CPQQuoteCreationObj.CPEConfiguration.NetworkTopologyLst,"Network Topology List");
			click(CPQQuoteCreationObj.CPEConfiguration.NetworkTopologyLst,"Network Topology List");			
			//ClickonElementByString("//li[normalize-space(.)='"+ Network_Topology +"']", 30);
			ClickonElementByString("//div[@aria-label='"+Network_Topology+"']", 20);
			Reusable.waitForpageloadmask();
			waitForAjax();
			//Saving the Product Entries	
			updateSaveProductCPQ("SaveToQuote");	
			Reusable.WaitforCPQloader();
			waitForAjax();
		
	}

	
	public String cpeSiteConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws Exception 
	{			
		//CPE Intial configuration
		cpeIntialConfiguration(file_name, Sheet_Name, iScript, iSubScript);
					
		//calling the below method to enter the site address
		AddSiteDetails(file_name, Sheet_Name, iScript, iSubScript);
					
		//calling the below method to enter the cpe configuration details
		cpeConfiguration(file_name, Sheet_Name, iScript, iSubScript);
			
		//calling the below method to enter the additional product data
		addtionalProductdata(file_name, Sheet_Name, iScript, iSubScript);
			
		//Saving the Product Entries	
		updateSaveProductCPQ("SaveToQuote");	
		
		return "True";

	}
	
	public void fastSignProcess(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException, AWTException {
		/*----------------------------------------------------------------------
		Method Name : fastSignProcess
		Purpose     : This method is to submit the quote for Technical approval
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String sResult;
		String Status_Reason = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Status_Reason");
		
		Reusable.waitForpageloadmask();		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, 30);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx, 30);
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx, "Fast Sign Initiate Checkbox");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx, "Fast Sign Initiate Checkbox");
		Reusable.waitForpageloadmask();
		
		Submit.SendProposalToCustomer(file_name, Sheet_Name, iScript, iSubScript);
		
		String Quote_Id = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");
		String pdfName = Quote_Id+".pdf";	
		String Proposal_Path = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Proposal_Path");
		
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fileAttachmentLnk, 30);
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fileAttachmentLnk, "Proposal File Link");
		
//		Checking if the file is existing or not and printing the file location into the testdata sheet
		sResult = Reusable.isFileDownloaded(Proposal_Path, pdfName);
		if (sResult.equalsIgnoreCase("False")){ return; }
		
		Reusable.waitForpageloadmask();
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink);
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		Reusable.waitForpageloadmask();
		
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.closeFastSignBtn);
		selectByValueDIV(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignReasonLst, CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignReasonSubLst, Status_Reason);
		Reusable.waitForpageloadmask();	
		
		String Complete_Path = Proposal_Path+pdfName;
		sendKeys(CPQQuoteCreationObj.SubmissionforTechnicalApproval.customerProofUploadTxb, Complete_Path, "Customer Proof Upload Textbox");
		Reusable.waitForpageloadmask();		
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.closeFastSignBtn, "Close Fast Sign Btn");
		Reusable.waitForpageloadmask();	
			
		waitForInvisibilityOfElement(CPQQuoteCreationObj.SubmissionforTechnicalApproval.closeFastSignBtn, 180);
		//validateCPQErrorMsg();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		waitForInvisibilityOfElement(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, 180);
	
		//validateCPQErrorMsg();
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.waitForpageloadmask();	
		waitForAjax();
						
		Reusable.deleteFile(Proposal_Path, Complete_Path);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Quote is set for Fast Sign process with the text "+getTextFrom(CPQQuoteCreationObj.SubmissionforTechnicalApproval.notificationBarCPQ));
				
	}

	public void cpeIntialConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		//Initializing the Variable
		String CPE_Service_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"CPE_Service_Type");
		String Related_Network_Reference = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Related_Network_Reference");
		String Network_Reference_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Network_Reference_Type");
		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.cpeSiteConfigurationLnk, 15);
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.CPEConfiguration.cpeSolutionServiceLst, "CPE Solution Service List");
		click(CPQQuoteCreationObj.CPEConfiguration.cpeSolutionServiceLst,"CPE Solution Service List");
		MoveToElement("@xpath=//li[normalize-space(.)='"+ CPE_Service_Type +"']");
		click("@xpath=//li[normalize-space(.)='"+ CPE_Service_Type +"']","Value from CPE Solution Service List");
		//ClickonElementByString("//li[normalize-space(.)='"+ CPE_Service_Type +"']", 30);
		Reusable.waitForpageloadmask();
		//Reusable.WaitforC4Cloader();
		MoveToElement("@xpath="+CPQQuoteCreationObj.CPEConfiguration.cpeSolutionServiceTable);
		Grid.clickInGridInput(CPQQuoteCreationObj.CPEConfiguration.cpeSolutionServiceTable,1,5);
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.CPEConfiguration.relatedNetworkProductLst, "Related Network Product List");
		MoveToElement(CPQQuoteCreationObj.CPEConfiguration.relatedNetworkProductLst);
		click(CPQQuoteCreationObj.CPEConfiguration.relatedNetworkProductLst,"Related Network Product List");
		MoveToElement("@xpath=//li[normalize-space(.)='"+ Network_Reference_Type +"']");
		click("@xpath=//li[normalize-space(.)='"+ Network_Reference_Type +"']","Value from CPE Solution Service List");
		//ClickonElementByString("//li[normalize-space(.)='"+ Network_Reference_Type +"']", 30);
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.CPEConfiguration.relatedNetworkReferenceTxb, "Related Network Reference Textbox");
		MoveToElement(CPQQuoteCreationObj.CPEConfiguration.relatedNetworkReferenceTxb);
		sendKeys(CPQQuoteCreationObj.CPEConfiguration.relatedNetworkReferenceTxb, Related_Network_Reference,"Related Network Reference Textbox");
		findWebElement(CPQQuoteCreationObj.CPEConfiguration.relatedNetworkReferenceTxb).sendKeys(Keys.TAB);
		Reusable.waitForpageloadmask();
		}
		
		
	public String AddSiteDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		//**********************Update Site Details for different Products*******************************
		String BuildingStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BuildingStatus");
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPAccess_Type");
		System.out.println("Product Name in Product_Configuration:"+ProductName);
		
		if(IPAccess_Type.equalsIgnoreCase("New")) {
			ProductName="ColtIpAccess";
		}
				
		Reusable.WaitforProdConfigLoader();		
				
		String sResult;
		switch (ProductName) {
		
		case "CPESolutionsSite": 
			
			//Initializing the Variable
			String SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");			

			//verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryAddressHdr, "'PRIMARY ADDRESS' section header");
			
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddLbl, "'Site Address' field label");
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddTxFld, "'Site Address' text field");
			MoveToElement(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddTxFld);
			sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddTxFld, SiteA_Address,"Site Address");						
			
			//Click on the 'Search for an Address' icon
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
			//click(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
			keyPress(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb,Keys.ENTER);
			Reusable.WaitforC4Cloader();
			waitForAjax();
			break;
			
		case "EthernetLine": case "EthernetHub": case "Wave":
			
			
			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");
			String SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
			String Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
			System.out.println("I am in switchcase "+SiteA_Address);
			Reusable.waitForpageloadmask();

			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb, "'Site Address' text field");
			ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb);
			sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb, SiteA_Address,"Site Address");
			
			//keyPress(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb,Keys.ENTER);
			
			//Click on the 'Search for an Address' icon
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
			click(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
			Reusable.waitForAjax();
			Reusable.waitForAjax();
			Reusable.WaitforCPQloader();
			Reusable.WaitforProdConfigLoader();
			

            if(BuildingStatus.equalsIgnoreCase("Yes")) {
            for (int i = 1; i < 6; i++) {Reusable.waitForpageloadmask(); }
            }
			webDriver.switchTo().frame("siteAddressLink");	
			
			

			AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
			//if (sResult.equalsIgnoreCase("False")) { return "False"; }
			
//			Entering Site Contact Number if the flow type is Manual/Automated DSL
			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressLnk, "'Site A Address Link'");
				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressLnk, "'Site A Address Link'");				
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb, "'Site A Phone Number Textbox'");
				ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb);
				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb, Phone_Number, "'Site A Phone Number Textbox'");
				
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn, "'Validate Address A Button'");
				click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn, "'Validate Address A Button'");
				Reusable.waitForpageloadmask();
			}
			
			//Site B Address Entry
			if (!ProductName.equalsIgnoreCase("EthernetHub")) {
//				Reading the SiteAddress details
				String SiteB_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address");
				String SiteB_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address_Type");
				Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
				
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressTxb,"Site B Textfield");
				ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressTxb);
				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressTxb, SiteB_Address);
				Reusable.waitForpageloadmask();

							
				//keyPress(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressTxb,Keys.ENTER);

				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteBSearchBtnImg);
				Reusable.waitForAjax();
				Reusable.waitForAjax();
				Reusable.WaitforCPQloader();
				Reusable.WaitforProdConfigLoader();
				
				 if(BuildingStatus.equalsIgnoreCase("Yes")) {
			            for (int i = 1; i < 6; i++) {Reusable.waitForpageloadmask(); }
			            }
				 
				//By executing a java script
				webDriver.switchTo().frame("siteAddressLink");
				//Validate address type
				AddressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
				
				//if (sResult.equalsIgnoreCase("False")) { return "False"; }
				if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
					verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk,"Site B Address Link");
					click(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk, "'Site B Address Link'");	
					Reusable.waitForpageloadmask();
					
					verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteBPhoneNumberTxb,"Site B Phone Number Text field");
					ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryAddressPage.siteBPhoneNumberTxb);
					sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteBPhoneNumberTxb, Phone_Number,"Phone Number Text Box"); 
					Reusable.waitForpageloadmask();
					
					verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressBBtn,"Site B Validat Button");
					click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressBBtn);
					Reusable.waitForpageloadmask();
				}
				
			}
			break;
			
		case "EthernetSpoke":
			//Configuring Hub part if the network type is existing
			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");
			String SiteB_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address");
			String Hub_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hub_Type");
			String Hub_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Hub_Reference");
			SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
			String ValidateReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ValidateReference");
			
			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeLst,"'Ethernet Hub Type dropdown'");
			click(CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeLst);
			//sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeLst,Hub_Type,"'Hub type List'");
			ClickonElementByString("//div[@data-value='"+Hub_Type+"']", 20);
			
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
			
			
			
			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.HubReferenceTxb,"'Ethernet Hub Reference Text Box'");					
			sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.HubReferenceTxb,Hub_Reference,"'Ethernet Hub Reference Text Box'");
						
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
			
			if(ValidateReference.equalsIgnoreCase("Yes")) {
			
             verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.HelpTextForValidateReferance,"Verify Help Text for Validate Referance Id");
			
			if(isVisible(CPQQuoteCreationObj.EthernetSpokeConfig.ValidateReferanceBtn))
			{
				verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.ValidateReferanceBtn, "Verify Validate Referance Button");
				click(CPQQuoteCreationObj.EthernetSpokeConfig.ValidateReferanceBtn, "Click on Validate Referance Button");
				Reusable.waitForpageloadmask();
				if(isVisible(CPQQuoteCreationObj.EthernetSpokeConfig.ValidMessagePopup))
				{
				verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.ValidMessagePopup, "Verify Successful Validation Popup");
				verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.ValidMessagePopupOKBtn, "Verify Ok Button on Successful Validation Popup");
				click(CPQQuoteCreationObj.EthernetSpokeConfig.ValidMessagePopupOKBtn, "Click on Ok Button on Successful Validation Popup");
				Reusable.waitForpageloadmask();
				}
				else if(isVisible(CPQQuoteCreationObj.EthernetSpokeConfig.InValidMessagePopup))
				{
					verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.InValidMessagePopup, "Verify UnSuccessful Validation Popup");
					verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.ValidMessagePopupOKBtn, "Verify Ok Button on UnSuccessful Validation Popup");
					click(CPQQuoteCreationObj.EthernetSpokeConfig.ValidMessagePopupOKBtn, "Click on Ok Button on UnSuccessful Validation Popup");
					
					Reusable.waitForpageloadmask();
				}
				
			}
			else
			{
				System.out.println("Validate Referance Button is not displayed");
			}
			
			}
			
			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.HubAddressTxb,"'Ethernet Hub Address Text Box'");					
			sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.HubAddressTxb,SiteA_Address,"'Ethernet Hub Reference Text Box'");
			keyPress(CPQQuoteCreationObj.EthernetSpokeConfig.HubAddressTxb,Keys.ENTER);
			//click(CPQQuoteCreationObj.EthernetSpokeConfig.HubSearchImgLnk);
			Reusable.waitForAjax();
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
			
			 if(BuildingStatus.equalsIgnoreCase("Yes")) {
		            for (int i = 1; i < 6; i++) {Reusable.waitForpageloadmask(); }
		            }
			
				//By executing a java script
			webDriver.switchTo().frame("siteAddressLink");
				//Validate Address
			sResult = AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
			if (sResult.equalsIgnoreCase("False")) 
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Site A address text box is not visible in CPQ, please verify");
				System.out.println("Site A address text box is not visible in CPQ, please verify");
				//ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addScreenCapture(""));				
			}
			
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
						
//			Configuring Spoke Part
						

			String SiteB_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address_Type");
			Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
			
			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.SopkeHubAddressTxb,"'Ethernet Hub Address Text Box'");					
			sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.SopkeHubAddressTxb,SiteB_Address,"'Ethernet Hub Reference Text Box'");
			//keyPress(CPQQuoteCreationObj.EthernetSpokeConfig.SopkeHubAddressTxb,Keys.ENTER);
			click(CPQQuoteCreationObj.EthernetSpokeConfig.SpokeSearchImgLnk,"SpokeSearchImgLnk");
			Reusable.waitForAjax();
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
			
			 if(BuildingStatus.equalsIgnoreCase("Yes")) {
		            for (int i = 1; i < 6; i++) {Reusable.waitForpageloadmask(); }
		            }
			 
				//By executing a java script
			webDriver.switchTo().frame("siteAddressLink");
				//Validate Address
			sResult = AddressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) 
			{ 
				
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk,"'Site B Address Link'");
				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk,"'Site B Address Link'");				
				
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,"'Site A Phone Number Text Box'");
				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,Phone_Number,"'Site A Phone Number Text Box'");
				
				verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn,"'Validate Address Button'");
				click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn,"'Validate Address Button'");
				
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
			}
			if (sResult.equalsIgnoreCase("False"))
			{ 
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Site B address text box is not visible in CPQ, please verify");
				System.out.println("Site B address text box is not visible in CPQ, please verify");
				//ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addScreenCapture(""));
			}
						
			
			break;
			
		case "ColtIpAccess":
		
		//******************Primary Connection > PRIMARY ADDRESS DETAILS**********************************
			waitForAjax();
			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteA_Address");
			SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
			Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");

				
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryAddressHdr, "'PRIMARY ADDRESS' section header");
				
			//verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddLbl, "'Site Address' field label");
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAinput, "'Site Address' text field");
			sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteAinput, SiteA_Address,"Site Address");
			
			waitForAjax();
			//Click on the 'Search for an Address' icon
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteASearch, "Search Button Image");
			click(CPQQuoteCreationObj.PrimaryAddressPage.siteASearch, "Search Button Image");
			Reusable.waitForAjax();
			Reusable.waitForAjax();
			Reusable.WaitforCPQloader();
			Reusable.WaitforProdConfigLoader();
			
			webDriver.switchTo().frame("siteAddressPrimLink");
			//Validate Address
			AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
			
			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) 
			{ 
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteDetailsEntryButt,"'Site A Details Entry Button'");
				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteDetailsEntryButt,"'Site A Details Entry Button'");
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteDetailsphnum,"'Site A Phone Number Text Box'");
				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteDetailsphnum,Phone_Number,"'Site A Phone Number Text Box'");
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.AddProducts.ipvalidateaddress,"Update Button");
                click(CPQQuoteCreationObj.AddProducts.ipvalidateaddress,"Update Button");
                Reusable.waitForpageloadmask();
				waitForElementToBeVisible(CPQQuoteCreationObj.AddProducts.ojUpdateEnabled,10);
			}
						
			waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.Nextbutt, 20);
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.Nextbutt, "Next Button");
			ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryAddressPage.Nextbutt);
			click(CPQQuoteCreationObj.PrimaryAddressPage.Nextbutt, "Next Button");
			break;

			
		case "VPNNetwork":
						
			verifyExists(CPQQuoteCreationObj.VPNNetwork.vpnSelectChoiceNetworkLnk,"'VPN Selection Choice Network link'");
			
			String Network_Reference_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Network_Reference_Type");
			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");
			SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
			Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
			
			verifyExists(CPQQuoteCreationObj.VPNNetwork.vpnSelectChoiceNetworkLnk,"'VPN Selection Choice Network link'");
			click(CPQQuoteCreationObj.VPNNetwork.vpnSelectChoiceNetworkLnk,"'VPN Selection Choice Network link'");
						
			click("//li[normalize-space(.)='"+Network_Reference_Type+"']","'Click the Network Reference Type'");
			System.out.println("Netwrok Refrence Type is changed to "+Network_Reference_Type);			
			Reusable.WaitforProdConfigLoader();
			
			verifyExists(CPQQuoteCreationObj.VPNNetwork.VPNAddBTN,"'VPN Add Button'");
			click(CPQQuoteCreationObj.VPNNetwork.VPNAddBTN,"'VPN Add Button'");
			Reusable.WaitforProdConfigLoader();
						
			verifyExists(CPQQuoteCreationObj.VPNNetwork.VPNTreeLink,"'VPN Tree Link'");
			click(CPQQuoteCreationObj.VPNNetwork.VPNTreeLink,"'VPN Tree Link'");
			Reusable.WaitforCPQloader();
							
			verifyExists(CPQQuoteCreationObj.VPNAddress.vpnSiteAddressTxb,"'VPN Site Address Text Box'");
			Reusable.waitForpageloadmask();
			
			scrollToViewNClick(CPQQuoteCreationObj.VPNAddress.vpnSiteAddressTxb,"'VPN Site Address Text Box'");
			sendKeys(CPQQuoteCreationObj.VPNAddress.vpnSiteAddressTxb,SiteA_Address,"'VPN Site Address Text Box'");	
			Reusable.WaitforCPQloader();
			
			verifyExists(CPQQuoteCreationObj.VPNAddress.vpnSiteSearchImg,"'VPN Site Search Image'");
			scrollToViewNClick(CPQQuoteCreationObj.VPNAddress.vpnSiteSearchImg,"'VPN Site Search Image'");					
			Reusable.WaitforProdConfigLoader();
					//By executing a java script
			webDriver.switchTo().frame("siteAddressLink");
					//Validate address type
			AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) 
			{ 
				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,"'Site A Phone Number Text Box'");
				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,Phone_Number,"'Site A Phone Number Text Box'");				
			}
			
			break;
		case "ColtIpAccessDC":
			
			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteA_Address");
			SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
			
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.Datacenterchbox,"'Data Center'");
			click(CPQQuoteCreationObj.PrimaryAddressPage.Datacenterchbox,"'Data Center'");
			Reusable.WaitforCPQloader();
			
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.Datacenteradd, "'Data Center Address' text field");
			sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.Datacenteradd, SiteA_Address,"Site Address");
			
			keyPress(CPQQuoteCreationObj.PrimaryAddressPage.Datacenteradd,Keys.ARROW_DOWN);
			keyPress(CPQQuoteCreationObj.PrimaryAddressPage.Datacenteradd,Keys.ENTER);
				
			Reusable.WaitforC4Cloader();
			waitForAjax();
			
			webDriver.switchTo().frame("siteAddressPrimLink");
			AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
			break;
			
			
			}
					
	//Partial Save of the Operation
		if (!ProductName.equalsIgnoreCase("CPESolutionsSite")&&!ProductName.equalsIgnoreCase("ColtIpAccess")&&!ProductName.equalsIgnoreCase("VPNNetwork")&&!ProductName.equalsIgnoreCase("ColtIpAccessDC")) 
		{
			verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.NextBtn,"'Next Button'");
			Reusable.waitForpageloadmask();
			scrollToViewNClick(CPQQuoteCreationObj.AddresstypeConnectivity.NextBtn,"'Next Button'");
			Reusable.waitForAjax();
			Reusable.WaitforCPQloader();
			Reusable.waitForpageloadmask();
			
			verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"'Site Details Link'");
		}
		if (ProductName.equalsIgnoreCase("ColtIpAccessDC")) 
		{
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button in IPAccess'");			
			Reusable.waitForpageloadmask();
			click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button in IPAccess'");
			Reusable.waitForpageloadmask();
			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) 
			{ 
				click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button in IPAccess'");
				Reusable.waitForpageloadmask();
			}
		}
		if (ProductName.equalsIgnoreCase("VPNNetwork")) 
		{
			verifyExists(CPQQuoteCreationObj.VPNAddress.vpnNextButton,"'Next Button in VPN Network'");
			Reusable.waitForpageloadmask();
			click(CPQQuoteCreationObj.VPNAddress.vpnNextButton,"'Next Button in VPN Network'");
			Reusable.WaitforProdConfigLoader();
			verifyExists(CPQQuoteCreationObj.VPNAddress.vpnsiteConfiguartionLink,"'VPN Site Configuration Link'");	
		}
		return "True";

	}
	public String AddSiteDetails_ForCTLPAccount(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{

	 //**********************Update Site Details for different Products*******************************
	String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
	String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
	String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPAccess_Type");
	String Segment = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Segment");
	System.out.println("Product Name in Product_Configuration:"+ProductName);
	if(IPAccess_Type.equalsIgnoreCase("New")) {
	ProductName="ColtIpAccess";
	}
	Reusable.WaitforProdConfigLoader();
	String sResult;
	switch (ProductName) {
	case "EthernetLine": case "EthernetHub": case "Wave":
	String SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");
	String SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
	String Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
	System.out.println("I am in switchcase "+SiteA_Address);
	Reusable.waitForpageloadmask();

	 verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterRdb, "Verify Data center Radio button");
	click(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterRdb, "Click on Data center Radio button");
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterName, "Verify Data center field");
	click(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterName, "Click on Data center field");
	sendKeys(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterName,SiteA_Address,"Enter Data Center Name");
	Reusable.waitForpageloadmask();
	Reusable.WaitforCPQloader();
	keyPress(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterName,Keys.BACK_SPACE);
	if(Segment.equalsIgnoreCase("CLTP - Fidessa")){
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValueForFidessa, "Verify Data Center Name");
	click(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValueForFidessa, "Select Data Center Name");
	}
	else {
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValue, "Verify Data Center Name");
	click(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValue, "Select Data Center Name");
	}
	Reusable.WaitforCPQloader();
	Reusable.WaitforProdConfigLoader();
	/*verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.AEndButtonOnMap, "Verify A End Button");
	click(CPQQuoteCreationObj.DataCenterAddressPage.AEndButtonOnMap, "Click on A End Button");
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.CopyTOBEndBtn, "Verify Copy To B End Button");
	click(CPQQuoteCreationObj.DataCenterAddressPage.CopyTOBEndBtn, "Click on Copy To B End Button");
	*/
	Reusable.waitForpageloadmask();
	// AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
	// Entering Site Contact Number if the flow type is Manual/Automated DSL
	if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) {
	verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressLnk, "'Site A Address Link'");
	click(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressLnk, "'Site A Address Link'");
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb, "'Site A Phone Number Textbox'");
	sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb, Phone_Number, "'Site A Phone Number Textbox'");
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn, "'Validate Address A Button'");
	click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn, "'Validate Address A Button'");
	Reusable.waitForpageloadmask();
	}
	//Site B Address Entry
	if (!ProductName.equalsIgnoreCase("EthernetHub")) {
	// Reading the SiteAddress details
	String SiteB_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address");
	String SiteB_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address_Type");
	Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterBRdb, "Verify Data center Radio button");
	click(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterBRdb, "Click on Data center Radio button");
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterNameB, "Verify Data center field");
	click(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterNameB, "Click on Data center field");
	sendKeys(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterNameB, SiteB_Address,"Enter Data Center Name");
	Reusable.waitForpageloadmask();
	Reusable.WaitforCPQloader();
	keyPress(CPQQuoteCreationObj.DataCenterAddressPage.DataCenterName,Keys.BACK_SPACE);
	if(Segment.equalsIgnoreCase("CLTP - Fidessa")){
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValueBForFidessa, "Verify Data Center Name");
	click(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValueBForFidessa, "Select Data Center Name");
	}
	else {
	verifyExists(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValueB, "Verify Data Center Name");
	click(CPQQuoteCreationObj.DataCenterAddressPage.SelectDataCenterNameValueB, "Select Data Center Name");
	}
	waitForAjax();
	Reusable.WaitforCPQloader();
	Reusable.WaitforProdConfigLoader();
	//By executing a java script
	webDriver.switchTo().frame("siteAddressLink");
	//Validate address type
	AddressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
	//if (sResult.equalsIgnoreCase("False")) { return "False"; }
	if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) {
	verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk,"Site B Address Link");
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteBPhoneNumberTxb,"Site B Phone Number Text field");
	sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteBPhoneNumberTxb, Phone_Number,"Phone Number Text Box");
	Reusable.waitForpageloadmask();

	verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressBBtn,"Site B Validat Button");
	click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressBBtn);
	}
	Reusable.waitForpageloadmask();
	}

	break;
	}
	//Partial Save of the Operation
	if (!ProductName.equalsIgnoreCase("CPESolutionsSite")&&!ProductName.equalsIgnoreCase("ColtIpAccess")&&!ProductName.equalsIgnoreCase("VPNNetwork"))
	{
	verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.NextBtn,"'Next Button'");
	Reusable.waitForpageloadmask();
	scrollToViewNClick(CPQQuoteCreationObj.AddresstypeConnectivity.NextBtn,"'Next Button'");
	Reusable.waitForAjax();
	Reusable.WaitforCPQloader();
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"'Site Details Link'");
	}
	if (ProductName.equalsIgnoreCase("ColtIpAccess"))
	{
	verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button in IPAccess'");
	Reusable.waitForpageloadmask();
	click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button in IPAccess'");
	Reusable.waitForpageloadmask();
	if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL"))
	{
	click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button in IPAccess'");
	Reusable.waitForpageloadmask();
	}
	}
	if (ProductName.equalsIgnoreCase("VPNNetwork"))
	{
	verifyExists(CPQQuoteCreationObj.VPNAddress.vpnNextButton,"'Next Button in VPN Network'");
	Reusable.waitForpageloadmask();
	click(CPQQuoteCreationObj.VPNAddress.vpnNextButton,"'Next Button in VPN Network'");
	Reusable.WaitforProdConfigLoader();
	verifyExists(CPQQuoteCreationObj.VPNAddress.vpnsiteConfiguartionLink,"'VPN Site Configuration Link'");
	}
	return "True";

	}
	
	public String AddressTypeConfiguration(String Site_Address,String Site_Address_Type) throws InterruptedException, IOException
	{
	//**********************Used to validate the Address Entries in Map *******************************
	  switch (Site_Address_Type.toUpperCase()) {
	  case "ONNET": case "MANUALDSL": case "AUTOMATEDDSL": case "MANUALOLODSL": case "ONNETDUALENTRY":
		  if(verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.Siteloadinprogress))
		 {
			  waitForInvisibilityOfElement(CPQQuoteCreationObj.AddresstypeConnectivity.Siteloadinprogress, 240);
		 }
	 if(waitForElementToBeVisible(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectedViaColtElem,180))
	{
		 // waitForElementToBeVisible(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectedViaColtElem,180);
		 verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectedViaColtElem, "'Connected via Colt Network'");
		 Reusable.waitForpageloadmask();
		 scrollIntoView(findWebElement(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectedViaColtElem));
	}
	else
	{
		//waitForElementToBeVisible(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectedViaColtElem,180);
		verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.DataCenterConnectedElem, "'Connected via Data Center Network'");
		Reusable.waitForpageloadmask();
		scrollIntoView(findWebElement(CPQQuoteCreationObj.AddresstypeConnectivity.DataCenterConnectedElem));
	}
	// Switch back to first browser window
	webDriver.switchTo().defaultContent();
	break;

	 case "MANUALOFFNET": case "AUTOMATEDOFFNET":
		 if(verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.Siteloadinprogress))
		 {
			  waitForInvisibilityOfElement(CPQQuoteCreationObj.AddresstypeConnectivity.Siteloadinprogress, 240);
		 }
		 //waitForElementToBeVisible(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectedViaColtElem,180);
		 verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionFoundORNFoundColtElem, "'Manual and Automated Offnet'");
		 Reusable.waitForpageloadmask();
		 scrollIntoView(findWebElement(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionFoundORNFoundColtElem));
		 // Switch back to first browser window
		 webDriver.switchTo().defaultContent();
	break;
	case "MANUALNEARNET": case "AUTOMATEDNEARNET":
		 if(verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.Siteloadinprogress))
		 {
			  waitForInvisibilityOfElement(CPQQuoteCreationObj.AddresstypeConnectivity.Siteloadinprogress, 240);
		 }
		//waitForElementToBeVisible(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionFoundORNFoundColtElem,180);
		verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionFoundORNFoundColtElem, "'Manual and Automated Nearnet'");
		Reusable.waitForpageloadmask();
		scrollIntoView(findWebElement(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionFoundORNFoundColtElem));
	// Switch back to first browser window
		webDriver.switchTo().defaultContent();
	break;
	case "ULL":
		//waitForInvisibilityOfElement(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionNotFndIncomplete, 180);
		verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionNotFndIncomplete, "ULL");
		Reusable.waitForpageloadmask();
		scrollIntoView(findWebElement(CPQQuoteCreationObj.AddresstypeConnectivity.ConnectionNotFndIncomplete));
		// Switch back to first browser window
		webDriver.switchTo().defaultContent();
		break;
	}
	return "True";
	}
	
	public void overrideOnnetUllFibre(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ProductName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Product_Name");
		String Resilience= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Resilience");
		String A_End_Override = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_End_Override");
		String B_End_Override = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Override");
		String Override_Reason = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Override_Reason");
		String BandwidthValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service_Bandwidth");
		
		/*Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, 20);
		verifyExists(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");
		click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");

		sendKeys(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthTxb,BandwidthValue,"'Service Bandwidth List'");
		click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthQry,"'Service Bandwidth Query'");
		//selectByValueDIV(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthSubLst,BandwidthValue);
		ClickonElementByString("//div[@data-value='"+BandwidthValue+"']", 20);
		Reusable.waitForpageloadmask();
		
		//Resliance Type
		waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst, 20);	
		verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst,"'Resileince List'");
		click(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst,"'Resilience List'");
		sendKeys(CPQQuoteCreationObj.CPEConfiguration.ResiliancyTypeLst,Resilience,"'Resilience List'");
		ClickonElementByString("//div[@data-value='"+Resilience+"']", 20);
		//selectByValueDIV(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst,CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeSubLst,Resilience);
		Reusable.waitForpageloadmask();*/

		
		switch (ProductName) {
		case "EthernetLine": case "EthernetHub": case "EthernetSpoke": case "Wave": case "ColtIpAccess":
			
			if (A_End_Override.equals("ULL")) {
				System.out.println(A_End_Override);
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.OverrideAEndBuildingType, "Override to ULL");
				ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.OverrideAEndBuildingType);
				click(CPQQuoteCreationObj.PrimaryConnectionPage.OverrideAEndBuildingType);
				
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLAEndCbx, "Override to ULL");
				click(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLAEndCbx);						
				
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb, "'Override Reason' text field");
				sendKeys(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb, Override_Reason, "Override_reason");
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.AEndCorrectCbx, "Override to ULL");
				click(CPQQuoteCreationObj.PrimaryConnectionPage.AEndCorrectCbx);						
				
			} else if (A_End_Override.equals("Onnet")) {
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLAEndCbx, "Override to ULL");
				click(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLAEndCbx);						
				
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb, "'Override Reason' text field");
				sendKeys(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb, Override_Reason, "Override_reason");
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.AEndCorrectCbx, "Override to ULL");
				click(CPQQuoteCreationObj.PrimaryConnectionPage.AEndCorrectCbx);						
				
			}
			
			if (!ProductName.equalsIgnoreCase("EthernetHub") && !ProductName.equalsIgnoreCase("EthernetSpoke")) {
				if (B_End_Override.equals("ULL")) {
					verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.OverrideBEndBuildingType, "Override to ULL");
					ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.OverrideBEndBuildingType);
					click(CPQQuoteCreationObj.PrimaryConnectionPage.OverrideBEndBuildingType);
					
					verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLBEndCbx, "Override to ULL");
					click(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLBEndCbx);						
					verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb, "'Override Reason' text field");
					sendKeys(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb, Override_Reason, "Override_reason");
					verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.BEndCorrectCbx, "Override to ULL");
					click(CPQQuoteCreationObj.PrimaryConnectionPage.BEndCorrectCbx);						
					
				} else if (A_End_Override.equals("Onnet")) {
					verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLBEndCbx, "Override to ULL");
					click(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToULLBEndCbx);						
					verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb, "'Override Reason' text field");
					sendKeys(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb, Override_Reason, "Override_reason");
					verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.BEndCorrectCbx, "Override to ULL");
					click(CPQQuoteCreationObj.PrimaryConnectionPage.BEndCorrectCbx);						}
				break;
			} 
	}
						
		/*
		//Validate
		Reusable.waitForpageloadmask();
		waitForAjax();
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn, "Validate");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);

		//Partial Save
		Reusable.waitForpageloadmask();
		waitForAjax();
		verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt, "Partial Save");
		ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt);
		click(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt);
	
		//Save to Quote
		Reusable.waitForpageloadmask();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.PrimaryConnectionPage.savetoquotebtn, 15);
		verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.savetoquotebtn, "Save to Quote");
		click(CPQQuoteCreationObj.PrimaryConnectionPage.savetoquotebtn);*/
		
	}
	
	public void editpsEngagements(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Engagement_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Engagement_Type");
		String PS_Engagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PS_Engagement");
		Reusable.waitForpageloadmask();
		waitForAjax();
		waitForElementToAppear(C4CEngagementSalesObj.engagementsPage.engagementlnk, 15);
		verifyExists(C4CEngagementSalesObj.engagementsPage.engagementlnk, "Engagements");
		click(C4CEngagementSalesObj.engagementsPage.engagementlnk);		
		
		Reusable.waitForpageloadmask();
		waitForElementToAppear(C4CEngagementSalesObj.engagementsPage.editengagementlnk, 15);
		verifyExists(C4CEngagementSalesObj.engagementsPage.editengagementlnk, "Engagements");
		click(C4CEngagementSalesObj.engagementsPage.editengagementlnk);		
		waitForAjax();
		
		switch (Engagement_Type) {
		case "SalesEngineer":
			waitForElementToAppear(C4CEngagementSalesObj.engagementsPage.engagementSEcbx, 15);
			verifyExists(C4CEngagementSalesObj.engagementsPage.engagementSEcbx, "Engagements");
			click(C4CEngagementSalesObj.engagementsPage.engagementSEcbx);
			
			waitForAjax();
			verifyExists(C4CEngagementSalesObj.engagementsPage.seEngagementLst, "Engagements");
			//click(CPQQuoteCreationObj.engagementsPage.seEngagementLst);
			//selectByValue(CPQQuoteCreationObj.engagementsPage.seEngagementLst,PS_Engagement,"'PS_Engagement Type dropdown'");
			click(C4CEngagementSalesObj.engagementsPage.seEngagementLst,"'Engagements List'");
			//sendKeys(CPQQuoteCreationObj.engagementsPage.seEngagementLst,PS_Engagement,"'Engagements List'");
			ClickonElementByString("//li[normalize-space(.)='"+PS_Engagement+"']", 20);
			break;
		case "ConsultancyEngagement":
			waitForElementToAppear(C4CEngagementSalesObj.engagementsPage.consultantCbx, 15);
			verifyExists(C4CEngagementSalesObj.engagementsPage.consultantCbx, "Consultancy");
			click(C4CEngagementSalesObj.engagementsPage.consultantCbx);
			break;
		}
		sleep(5000);
		verifyExists(C4CEngagementSalesObj.engagementsPage.saveC4CBtn, "Save to C4C");
		click(C4CEngagementSalesObj.engagementsPage.saveC4CBtn);
	}
	
	public void navigateQuotesFromHomepage(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String Quote_ID) throws Exception
	{
						
		waitForElementToAppear(C4CEngagementSalesObj.salespage.salesLst, 15);
		verifyExists(C4CEngagementSalesObj.salespage.salesLst, "Consultancy");
		click(C4CEngagementSalesObj.salespage.salesLst);
		waitForAjax();
		verifyExists(C4CEngagementSalesObj.salespage.quotesTab, "Quotes Tab");
		click(C4CEngagementSalesObj.salespage.quotesTab);
		waitForAjax();
		waitForElementToAppear(C4CEngagementSalesObj.salespage.searchIcon, 15);
		verifyExists(C4CEngagementSalesObj.salespage.searchIcon, "Consultancy");
		click(C4CEngagementSalesObj.salespage.searchIcon);
		waitForAjax();
		waitForElementToAppear(C4CEngagementSalesObj.salespage.searchIcon, 15);
		verifyExists(C4CEngagementSalesObj.salespage.searchTxb, "'Quote_ID' text field");
		sendKeys(C4CEngagementSalesObj.salespage.searchTxb, Quote_ID, "Quote_ID");
		
		Reusable.waitForpageloadmask();
        Reusable.Waittilljquesryupdated();
		
		waitForAjax();
		verifyExists(C4CEngagementSalesObj.salespage.searchBtn, "Search Quote");
		click(C4CEngagementSalesObj.salespage.searchBtn);
		String quoteIdObj = CPQLoginObj.Login.QuoteID1+ Quote_ID +CPQLoginObj.Login.QuoteID2;
		
		Reusable.waitForpageloadmask();
        Reusable.Waittilljquesryupdated();
		
		waitForElementToAppear(quoteIdObj, 10);
		verifyExists(quoteIdObj,"Quote ID");
		click(quoteIdObj,"Quote ID");
		
		waitForAjax();
		waitForElementToAppear(C4CEngagementSalesObj.salespage.actionBtn, 15);
		verifyExists(C4CEngagementSalesObj.salespage.actionBtn, "Action Quote");
		click(C4CEngagementSalesObj.salespage.actionBtn);
		Reusable.waitForpageloadmask();	
		
		verifyExists(C4CEngagementSalesObj.salespage.EditBtn, "Edit Quote");
		click(C4CEngagementSalesObj.salespage.EditBtn);
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();		
	}
	
	public void editEthernetProductConfiguration() throws Exception
	{
		//String Hub_type=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub_type");
		//String Lead_time=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Lead_time");
		
		//Reusable.waitForpageloadmask();
		//waitForElementToAppear(CPQQuoteCreationObj.engagementpf.SelectProduct, 15);
		//verifyExists(CPQQuoteCreationObj.engagementpf.SelectProduct, "Select Product");
		//click(CPQQuoteCreationObj.engagementpf.SelectProduct);
		
		waitForAjax();
		Reusable.WaitforCPQloader();
		Reusable.waitToPageLoad();
		verifyExists(CPQQuoteCreationObj.engagementpf.engagementpf, "Engagement Portfolio pricing");
		click(CPQQuoteCreationObj.engagementpf.engagementpf);
		Reusable.waitForpageloadmask();
		
		
	//	waitForElementToAppear(CPQQuoteCreationObj.TransactionPage.Quotestatus, 15);
		//String quotestatus = getAttributeFrom(CPQQuoteCreationObj.TransactionPage.Quotestatus, "value");
		//System.out.println(quotestatus);
		//waitForElementToAppear(CPQQuoteCreationObj.engagementpf.buildingtype, 15);
		//selectByValue(CPQQuoteCreationObj.engagementpf.buildingtype,Hub_type,"'Ethernet Hub Type dropdown'");
		//click(CPQQuoteCreationObj.engagementpf.buildingtype);
		
		//Addon Details
		//verifyExists(CPQQuoteCreationObj.SiteAddonsPage.AddonDetails, "Add Product Details");
		//click(CPQQuoteCreationObj.SiteAddonsPage.AddonDetails);
		
		//Lead Time
		//selectByValue(CPQQuoteCreationObj.SiteAddonsPage.leadtime,Lead_time,"'Lead Time Type dropdown'");
		//click(CPQQuoteCreationObj.SiteAddonsPage.leadtime);
		
		
		//Partial Save
		//verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt, "Partial Save");
		//click(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt);
				
		//Save to Quote
		//verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.savebtn, "Save");
		//click(CPQQuoteCreationObj.PrimaryConnectionPage.savebtn);
			
    	
	}
	
	public void Engagementportfoliopricing(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
				
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.EngportfolioPage.pandl, 20);
		verifyExists(CPQQuoteCreationObj.EngportfolioPage.pandl,"P & L button");
		clickByJS(CPQQuoteCreationObj.EngportfolioPage.pandl);
		
		waitForAjax();
		Reusable.WaitforCPQloader();
		verifyExists(CPQQuoteCreationObj.EngportfolioPage.selectuser,"'Select Admin user'");
		click(CPQQuoteCreationObj.EngportfolioPage.selectuser,"'Select Admin user'");
		//sendKeys(CPQQuoteCreationObj.EngportfolioPage.selectuser,Configuration.ADUser_Username,"'Admin_User'");
		ClickonElementByString("//div[@data-value='"+Configuration.ADUser_Username+"']", 20);
		//selectByValue(CPQQuoteCreationObj.EngportfolioPage.selectuser,Admin_User,"'Admin user dropdown'");
		//click(CPQQuoteCreationObj.EngportfolioPage.selectuser);
		//selectByValueDIV(CPQQuoteCreationObj.EngportfolioPage.selectuser,CPQQuoteCreationObj.EngportfolioPage.selectuserdrop,Configuration.ADUser_Username);
		
		
		waitForAjax();
		verifyExists(CPQQuoteCreationObj.EngportfolioPage.assignquote,"Assign Quote button");
		click(CPQQuoteCreationObj.EngportfolioPage.assignquote,"Assign Quote button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		/*
		Grid.clickInGrid(CPQQuoteCreationObj.EngportfolioPage.lineItemstable, 2, 15);
		sleep(1000);
		Grid.sendTextToGrid(CPQQuoteCreationObj.EngportfolioPage.lineItemstable, 2, 15, "1000");
		sleep(1000);
		
		Grid.clickInGrid(CPQQuoteCreationObj.EngportfolioPage.lineItemstable, 2, 17);
		sleep(1000);
		Grid.sendTextToGrid(CPQQuoteCreationObj.EngportfolioPage.lineItemstable, 2, 17, "1000");
		sleep(1000);		*/
		
		
		/*verifyExists(CPQQuoteCreationObj.EngportfolioPage.refreshallprices,"Refresh All button");
		click(CPQQuoteCreationObj.EngportfolioPage.refreshallprices,"Refresh All button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send to Sales button");
		click(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send To Sales button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn, 20);
		verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn,"Log out button");
		click(CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn,"Log out button");
		waitForAjax();
		
		close();
		openBrowser(g.getBrowser());
		
		openurl(Configuration.CPQ_URL);		CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
		CPQLogin.SelectQuote(testDataFile, sheetName, scriptNo, dataSetNo);*/

				
	}

	
	public void cpeConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
			
		//Initializing the Variable
		String Connection_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connection_Type");
		String Service_Bandwidth = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Service_Bandwidth");
		String Managed_CPE = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Managed_CPE");
		String CPE_Solution_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"CPE_Solution_Type");

		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.connectionTypeLst, 15);		
		click(CPQQuoteCreationObj.CPEConfiguration.connectionTypeLst,"Connection Type List");
		Reusable.waitForpageloadmask();
		MoveToElement("@xpath=//li[normalize-space(.)='"+ Connection_Type +"']");
		click("@xpath=//li[normalize-space(.)='"+ Connection_Type +"']","Value from List");
		//ClickonElementByString("//li[normalize-space(.)='"+ Connection_Type +"']", 30);
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthDropdown, "Service Bandwidth List");
		sendKeys(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthTxb, Service_Bandwidth, "Service Bandwidth Textbox");
		click(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthQry, "Service Bandwidth Qry");
		Reusable.waitForpageloadmask();
		MoveToElement("@xpath=//li[normalize-space(.)='"+ Service_Bandwidth +"']");
		click("@xpath=//li[normalize-space(.)='"+ Service_Bandwidth +"']","Value from List");
		//ClickonElementByString("//div[@data-value='"+Service_Bandwidth+"']", 20);
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.managedCPELst, 15);
		//ScrollIntoViewByString(CPQQuoteCreationObj.CPEConfiguration.managedCPELst);
		click(CPQQuoteCreationObj.CPEConfiguration.managedCPELst,"Managed CPE List");
		MoveToElement("@xpath=//li[normalize-space(.)='"+ Managed_CPE +"']");
		click("@xpath=//li[normalize-space(.)='"+ Managed_CPE +"']","Value from List");
		//ClickonElementByString("//li[normalize-space(.)='"+ Managed_CPE +"']", 30);
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.cpeSolutionTypeLst, 15);
		//ScrollIntoViewByString(CPQQuoteCreationObj.CPEConfiguration.cpeSolutionTypeLst);
		click(CPQQuoteCreationObj.CPEConfiguration.cpeSolutionTypeLst,"CPE Solution Type List");
		MoveToElement("@xpath=//li[normalize-space(.)='"+ CPE_Solution_Type +"']");
		click("@xpath=//li[normalize-space(.)='"+ CPE_Solution_Type +"']","Value from List");
		//ClickonElementByString("//li[normalize-space(.)='"+ CPE_Solution_Type +"']", 30);
		Reusable.waitForpageloadmask();			
	}
	
	public void addtionalProductdata(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
        //Initializing the Variable
        String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
        String Flow_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
        String IPAccess_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IPAccess_Type");
        String Aend_Site_Interface = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Presentation_Interface");
        String Bend_Site_Interface = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Presentation_Interface");
        String Existing_Capacity_Lead_Time = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Existing_Capacity_Lead_Time");
        String A_End_Override = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"A_End_Override");
        
        
        switch (Product_Name) {        
        
        case "CPESolutionsSite":
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface, 15);
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface);
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface,"Aend Site Presentation Interface");
        click(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface, "Aend Site Presentation Interface");
        ClickonElementByString("//div[@data-value='"+Aend_Site_Interface+"']", 20);        
        Reusable.waitForpageloadmask();
        break;
        case "EthernetLine": case "EthernetHub": case "EthernetSpoke": case "Wave":
        
            if ((Flow_Type.equalsIgnoreCase("ReuseBCP"))||(Flow_Type.equalsIgnoreCase("ReuseOLO")))
            {
                refreshPage();
                Reusable.waitForpageloadmask();
                Reusable.WaitforCPQloader();
            }
            if(!Product_Name.equalsIgnoreCase("ColtIpAccess")) {
                scrollUp();
                waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
                ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
                click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
                Reusable.waitForpageloadmask();
                
                /*verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn,"Validate button");
                click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn,"Validate button");
                Reusable.waitForpageloadmask();
                Reusable.WaitforCPQloader();*/
                }

 

        //Entering A-End details
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface, 15);
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface);
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterfaceLst,"Aend Site Presentation Interface");
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterfaceLst);
        click(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterfaceLst,"Aend Site Presentation Interface");
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterfaceTxb,"Aend Site Presentation Interface");
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterfaceTxb);
        click(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterfaceTxb,"Aend Site Presentation Interface");
        sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterfaceTxb,Aend_Site_Interface);
        ClickonElementByString("//div[@data-value='"+Aend_Site_Interface+"']", 20);
        //ClickonElementByString("//li[normalize-space(.)='"+ Aend_Site_Interface +"']", 30);
        Reusable.waitForpageloadmask();
        if (!Product_Name.equalsIgnoreCase("EthernetSpoke")&&(!Product_Name.equalsIgnoreCase("EthernetHub")))
        {
            if ((Flow_Type.equalsIgnoreCase("ReuseBCP"))||(Flow_Type.equalsIgnoreCase("ReuseOLO")))
            {
                refreshPage();
                Reusable.waitForpageloadmask();
                Reusable.WaitforCPQloader();
                click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
                Reusable.waitForpageloadmask();
                Reusable.WaitforCPQloader();
        }
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterface, 15);
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterface);
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterfaceLst,"Bend Site Presentation Interface");
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterfaceLst);
        click(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterfaceLst,"Bend Site Presentation Interface");
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.BndSitePresentationInterfaceTxb,"Bend Site Presentation Interface");
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.BndSitePresentationInterfaceTxb);
        click(CPQQuoteCreationObj.AdditionalProductDataPage.BndSitePresentationInterfaceTxb,"Bend Site Presentation Interface");
        sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.BndSitePresentationInterfaceTxb,Bend_Site_Interface);
        ClickonElementByString("//div[@data-value='"+Bend_Site_Interface+"']", 20);
        //ClickonElementByString("//li[normalize-space(.)='"+ Bend_Site_Interface +"']", 30);
        Reusable.waitForpageloadmask();
        }
        /*waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.addtionalProductDataLnk, 15);
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.addtionalProductDataLnk);
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.addtionalProductDataLnk, "Addtional Product Data Llnk");
        Reusable.waitForpageloadmask(); */
        break;
        case "VPNNetwork":
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface, 15);
        ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface);
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface,"Aend Site Presentation Interface");
        click(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface,"Aend Site Presentation Interface");
        ClickonElementByString("//div[@data-value='"+Aend_Site_Interface+"']", 20);
        //ClickonElementByString("//li[normalize-space(.)='"+ Aend_Site_Interface +"']", 30);
        Reusable.waitForpageloadmask();
        break;
        
        case "ColtIpAccess":
            ipAccessConfiguration(file_name, Sheet_Name, iScript, iSubScript);
        break;
        }
        
        if(A_End_Override.equalsIgnoreCase("ULL"))
        {
            verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn,"Validate button");
            click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn,"Validate button");
            Reusable.waitForpageloadmask();
            waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete, 75);
        }
    }
	
	public void addtionalProductdataEntries(String file_name, String Sheet_Name, String iScript, String iSubScript, String Product_Name, String SiteName) throws IOException, InterruptedException 
	{
			
		//Initializing the Variable
		String Cabinet_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Cabinet_Type");
		String Site_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Site_Type");
		String Presentation_Interface = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Presentation_Interface");
		String Cabinet_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Cabinet_ID");
		String Connector_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Connector_Type");
		String Access_Technology = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Access_Technology");
		String Customer_Pop_Status = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Customer_Pop_Status");
		String Site_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Site_ID");
		String Port_Role = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Port_Role");
		String Flow_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");

		switch (Product_Name) {
		
			case "CPESolutionsSite":
							
				//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeALst, CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeASubLst, Cabinet_Type);
				click(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeALst,"Cabinet Type A List");
				ClickonElementByString("//li[normalize-space(.)='"+ Cabinet_Type +"']", 30);
				Reusable.waitForpageloadmask();
				//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.siteTypeLst, CPQQuoteCreationObj.AdditionalProductDataPage.siteTypeSubLst, Site_Type);
				click(CPQQuoteCreationObj.AdditionalProductDataPage.siteTypeLst,"Site Type List");
				ClickonElementByString("//li[normalize-space(.)='"+ Site_Type +"']", 30);
				Reusable.waitForpageloadmask();
				//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst, CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceASubLst, Presentation_Interface);
				click(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst,"Presentation Interface A List");
				ClickonElementByString("//li[normalize-space(.)='"+ Presentation_Interface +"']", 30);
				Reusable.waitForpageloadmask();
				sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetIDATxb, Cabinet_ID, "Cabinet IDA Textbox");
				findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetIDATxb).sendKeys(Keys.TAB);
				Reusable.waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Additional Product data's for CPE Soultions have been entered successfully");
				break;
				
			case "EthernetLine": case "EthernetHub": case "EthernetSpoke": case "Wave":
			{
				
	
				switch (SiteName) {
				
					case "A_End": 
						
						sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetIDATxb, Cabinet_ID, "Cabinet IDA Textbox");
						findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetIDATxb).sendKeys(Keys.TAB);
						
						//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeALst, CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeASubLst, Cabinet_Type);
						click(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeALst,"Cabinet Type A List");
						ClickonElementByString("//li[normalize-space(.)='"+ Cabinet_Type +"']", 30);
						Reusable.waitForpageloadmask();
						
						click(CPQQuoteCreationObj.AdditionalProductDataPage.accessTechnologyALst,"Access Technology AList");
						ClickonElementByString("//li[normalize-space(.)='"+ Access_Technology +"']", 30);
						Reusable.waitForpageloadmask();
						//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.accessTechnologyALst, CPQQuoteCreationObj.AdditionalProductDataPage.accessTechnologyASubLst, Access_Technology);
						
						if(!Flow_Type.equalsIgnoreCase("ManualOffnet")&&!Flow_Type.equalsIgnoreCase("ManualDSL")
								&&!Flow_Type.equalsIgnoreCase("ManualOLODSL")&&!Flow_Type.equalsIgnoreCase("AutomatedOffnet")
								&&!Flow_Type.equalsIgnoreCase("AutomatedDSL")) {
							
							click(CPQQuoteCreationObj.AdditionalProductDataPage.customerPopStatusALst,"Customer Pop Status AList");
							ClickonElementByString("//li[normalize-space(.)='"+ Customer_Pop_Status +"']", 30);
						}
						Reusable.waitForpageloadmask();
						sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.siteIDATxb, Site_ID, "Site IDA Textbox");
						findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.siteIDATxb).sendKeys(Keys.TAB);
						
						click(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst,"Presentation Interface AList");
						ClickonElementByString("//li[normalize-space(.)='"+ Presentation_Interface +"']", 30);
						Reusable.waitForpageloadmask();
						
						click(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst,"Connector Type AList");
						ClickonElementByString("//li[normalize-space(.)='"+ Connector_Type +"']", 30);
						//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst, CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeASubLst, Connector_Type);
						Reusable.waitForpageloadmask();
						
						if (!Product_Name.equalsIgnoreCase("EthernetSpoke")) {
							click(CPQQuoteCreationObj.AdditionalProductDataPage.portRoleALst,"Port Role AList");
							ClickonElementByString("//li[normalize-space(.)='"+ Port_Role +"']", 30);
							//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.portRoleALst, CPQQuoteCreationObj.AdditionalProductDataPage.portRoleASubLst, Port_Role);
							Reusable.waitForpageloadmask();
						}
						if(Product_Name.contains("Ethernet")) {
							waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn, 20);
						}else {
							waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.update4cBtn, 20);
						}
						ExtentTestManager.getTest().log(LogStatus.PASS, "Additional Product data's for A-End have been entered successfully");
						break;
						
					case "B_End": 
						
						sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetIDBTxb, Cabinet_ID, "Cabinet ID B Textbox");
						findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetIDBTxb).sendKeys(Keys.TAB);
						
						//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeALst, CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeASubLst, Cabinet_Type);
						click(CPQQuoteCreationObj.AdditionalProductDataPage.cabinetTypeBLst,"Cabinet Type B List");
						ClickonElementByString("//li[normalize-space(.)='"+ Cabinet_Type +"']", 30);
						Reusable.waitForpageloadmask();
						
						click(CPQQuoteCreationObj.AdditionalProductDataPage.accessTechnologyBLst,"Access Technology BList");
						ClickonElementByString("//li[normalize-space(.)='"+ Access_Technology +"']", 30);
						Reusable.waitForpageloadmask();
						//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.accessTechnologyALst, CPQQuoteCreationObj.AdditionalProductDataPage.accessTechnologyASubLst, Access_Technology);
						
						if(!Flow_Type.equalsIgnoreCase("ManualOffnet")&&!Flow_Type.equalsIgnoreCase("ManualDSL")
								&&!Flow_Type.equalsIgnoreCase("ManualOLODSL")&&!Flow_Type.equalsIgnoreCase("AutomatedOffnet")
								&&!Flow_Type.equalsIgnoreCase("AutomatedDSL")) {
							
							click(CPQQuoteCreationObj.AdditionalProductDataPage.customerPopStatusALst,"Customer Pop Status BList");
							ClickonElementByString("//li[normalize-space(.)='"+ Customer_Pop_Status +"']", 30);
						}
						Reusable.waitForpageloadmask();
						sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.siteIDBTxb, Site_ID, "Site IDB Textbox");
						findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.siteIDBTxb).sendKeys(Keys.TAB);
						
						click(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceBLst,"Presentation Interface BList");
						ClickonElementByString("//li[normalize-space(.)='"+ Presentation_Interface +"']", 30);
						Reusable.waitForpageloadmask();
						
						click(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeBLst,"Connector Type BList");
						ClickonElementByString("//li[normalize-space(.)='"+ Connector_Type +"']", 30);
						//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst, CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeASubLst, Connector_Type);
						Reusable.waitForpageloadmask();
						
						if (!Product_Name.equalsIgnoreCase("EthernetSpoke")) {
							click(CPQQuoteCreationObj.AdditionalProductDataPage.portRoleBLst,"Port Role BList");
							ClickonElementByString("//li[normalize-space(.)='"+ Port_Role +"']", 30);
							//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.portRoleALst, CPQQuoteCreationObj.AdditionalProductDataPage.portRoleASubLst, Port_Role);
							Reusable.waitForpageloadmask();
						}
						if(Product_Name.contains("Ethernet")) {
							waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn, 20);
							
						}else {
							waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.update4cBtn, 20);
							
						}
						ExtentTestManager.getTest().log(LogStatus.PASS, "Additional Product data's for B-End have been entered successfully");
						break;
				}
			}
			break;
			
			case "VPNNetwork":
				
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.vpnCustomerPopLst, CPQQuoteCreationObj.AdditionalProductDataPage.vpnCustomerPopSubLst, Customer_Pop_Status);
				Reusable.waitForpageloadmask();
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.vpnCabinetTypeLst, CPQQuoteCreationObj.AdditionalProductDataPage.vpnCabinetTypeSubLst, Cabinet_Type);
				Reusable.waitForpageloadmask();
				sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.vpnCabinetIdTxb, Cabinet_ID, "VPN CabinetId Textbox");
				sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.vpnSiteIdTxb, Site_ID, "VPN SiteId Textbox");
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.vpnSitePresentationLst, CPQQuoteCreationObj.AdditionalProductDataPage.vpnSitePresentationSubLst, Presentation_Interface);
				Reusable.waitForpageloadmask();
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.vpnSiteConnectorLst, CPQQuoteCreationObj.AdditionalProductDataPage.vpnSiteConnectorSubLst, Connector_Type);
				Reusable.waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Additional Product data's for A-End have been entered successfully");
				click(CPQQuoteCreationObj.AdditionalProductDataPage.vpnNetworkTreeLink, "VPN Network Tree Link");
				Reusable.waitForpageloadmask();
				break;	
			}
		
		}
		
			
	public void ipAccessConfiguration(String file_name,String Sheet_Name, String iScript, String iSubScript) throws InterruptedException, IOException {
			
		//Site Addons
	//	waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipSiteAddonsLnk, 15);
	//	waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, 15);
		Reusable.waitForpageloadmask();
		
		
//		L3 Resiliance
		System.out.println("IP L3 Resiliance Link");
		scrollUp();
		//waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.L3Resiliency, 15);
		verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.L3Resiliency,"L3 Resiliance Link");
		click(CPQQuoteCreationObj.L3RESILIENCEPage.L3Resiliency,"L3 Resiliance Link");
		Reusable.waitForpageloadmask();
		ipAccessL3Resilience(file_name, Sheet_Name, iScript, iSubScript);
			
				
//		 Diversity and Service Addons and BeSpoke Features
		scrollUp();
		click(CPQQuoteCreationObj.AdditionalProductDataPage.primaryfeatures, "Primary Features link");
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipAccessaddtionalProductDataLnk, 15);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.ipAccessaddtionalProductDataLnk, "Additional Product Data link");
		//click(CPQQuoteCreationObj.AdditionalProductDataPage.addtionalProductDataLnk, "Additional Product Data link");
		Reusable.waitForpageloadmask();		
		
		//Additional Product Data
		ipAccessAdditionalProductData(file_name, Sheet_Name, iScript, iSubScript);
		
	}
	
	public void ipAccessL3Resilience(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
	//Initializing the Variable
		
		String L3_Resilience_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"L3_Resilience_Type");
				
		switch(L3_Resilience_Type.toUpperCase()) {
		
			case "NO RESILIENCE":{
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceLnk, CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceSubLnk, L3_Resilience_Type);
				Reusable.waitForpageloadmask();
				//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "IP Access Next Button");
				//Reusable.waitForpageloadmask();
				break;
				}
			
			case "DUAL ACCESS UNMANAGED":{
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceLnk, CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceSubLnk, L3_Resilience_Type);
				Reusable.waitForpageloadmask();
				//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "IP Access Next Button");
				for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
				ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type);
				break;
				}  
			
			case "DUAL ACCESS PRIMARY & BACKUP":{
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceLnk, CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceSubLnk, L3_Resilience_Type);
				Reusable.waitForpageloadmask();
				//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "IP Access Next Button");
				for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
				ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type);
				break;
				}
			
			case "DUAL ACCESS LOAD SHARED":{
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceLnk, CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceSubLnk, L3_Resilience_Type);
				Reusable.waitForpageloadmask();
				//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "IP Access Next Button");
				for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
				ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type);
				break;
				}
			
			case "MULTI-ISP PRIMARY & BACKUP":{
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceLnk, CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceSubLnk, L3_Resilience_Type);
				Reusable.waitForpageloadmask();
				//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "IP Access Next Button");
				for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
				ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type);
				break;
				}
			
			case "DSL BACKUP":{
				selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceLnk, CPQQuoteCreationObj.AdditionalProductDataPage.ipL3ResilianceSubLnk, L3_Resilience_Type);
				Reusable.waitForpageloadmask();
				//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "IP Access Next Button");
				for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
				ipAccessL3Configuration(file_name, Sheet_Name, iScript, iSubScript,L3_Resilience_Type);
				break;
				}
		}
		
	}
	
	public void ipAccessL3Configuration(String file_name, String Sheet_Name, String iScript, String iSubScript,String L3_Resilience_Type) throws IOException, InterruptedException {
	
//		Initializing the Variable
		String BackUp_Bandwidth = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Backup_BandWidth");
		String Presentation_Interface = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Presentation_Interface");
		String InterFace = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"InterFace");
		
//		Site B Address Entry
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.siteAAddressTxb, 15);		
		
//			Reading the Address from the data sheet
			String SiteB_Address = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"SiteB_Address");
			String SiteB_Address_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"SiteB_Address_Type");
			Reusable.waitForpageloadmask();
			ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.siteAAddressTxb);
			sendKeysByJS(CPQQuoteCreationObj.AdditionalProductDataPage.siteAAddressTxb, SiteB_Address);
			click(CPQQuoteCreationObj.AdditionalProductDataPage.siteASearchImg, "Search Button Image");
			//clickByJS(CPQQuoteCreationObj.AdditionalProductDataPage.siteASearchImg);
			Reusable.waitForpageloadmask();
			//By executing a java script
			webDriver.switchTo().frame("siteAddressSecLink");
			//Validate address type
			addressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
			scrollUp();
			click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "IP Access Next Button");
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask();}
			//By executing a java script
		//	if(L3_Resilience_Type.equalsIgnoreCase("Multi-ISP Primary & Backup")) {
		//		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipFeaturesLnk, 20);				
		//	}else {waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipBackUpBandWidthLst, 20);}			
		
		
//		Backup Bandwidth
		if(!L3_Resilience_Type.equalsIgnoreCase("Multi-ISP Primary & Backup")) {
			scrollUp();
			waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipFeaturesLnk, 20);
			Reusable.waitForpageloadmask();
			//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipBackUpBandWidthLst, CPQQuoteCreationObj.AdditionalProductDataPage.ipBackUpBandWidthSubLst, BackUp_Bandwidth);
			//Reusable.waitForpageloadmask();
			//ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn);
			click(CPQQuoteCreationObj.AdditionalProductDataPage.accesssec);
			for (int i = 1; i < 4; i++) {Reusable.waitForpageloadmask(); }
			//waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipFeaturesLnk, 20);		
		}
		
		
//		ipFetaures
		scrollUp();
		click(CPQQuoteCreationObj.AdditionalProductDataPage.SecFeatures, "Secondary Features");
		Reusable.waitForpageloadmask();
		//ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.interfacetype);
		//verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.interfacetype, "IP FibreType List");
		//click(CPQQuoteCreationObj.AdditionalProductDataPage.interfacetype,"'IP Interface AEnd List'");
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.ipinterfaceAEndSubLst,Presentation_Interface,"'IP Interface AEnd List'");
		//ClickonElementByString("//div[@data-value='"+Presentation_Interface+"']", 20);
		//Reusable.waitForpageloadmask();
		//verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.ipFibreTypeLst, "IP FibreType List");
		//click(CPQQuoteCreationObj.AdditionalProductDataPage.ipFibreTypeLst,"'IP FibreType List'");
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.ipFibreTypeLst,InterFace,"'IP FibreType List'");
		//ClickonElementByString("//div[@data-value='"+InterFace+"']", 20);
		//Reusable.waitForpageloadmask();
		//ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn);
		//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "CPE IP Access Next Button");
		//	for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
		//	if(!L3_Resilience_Type.equalsIgnoreCase("Multi-ISP Primary & Backup")) {
		//		waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, 20);
		//		click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "CPE IP Access Next Button");
		//		for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
		//	}
			
	/*		int times=0;
			while(!waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.ipDiversitySelector, 20)||times==3) {
				ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn);
				click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "CPE IP Access Next Button");
				for (int i = 1; i < 4; i++) {Reusable.waitForpageloadmask(); }
				times++;
			}	*/	
	}
	
	
	public void addressTypeConfiguration(String Site_Address, String Site_Address_Type) throws IOException, InterruptedException {
	
			switch (Site_Address_Type.toUpperCase()) {
			
			case "ONNET": case "MANUALDSL": case "AUTOMATEDDSL": case "MANUALOLODSL": case "ONNETDUALENTRY":
				waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.connectedViaColtElem, 20);
				Reusable.waitForpageloadmask();
				ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.connectedViaColtElem);
				// Switch back to first browser window
				webDriver.switchTo().defaultContent();
				//ExtentTestManager.getTest().log(LogStatus.PASS,"Address " + Site_Address + " selected/entered successfully");
				System.out.println("Address " + Site_Address + " selected/entered successfully");
				
				break;			

			case "MANUALOFFNET": case "AUTOMATEDOFFNET": 
				waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.connectionFoundORNFoundColtElem, 20);
				Reusable.waitForpageloadmask();
				ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.connectionFoundORNFoundColtElem);
				// Switch back to first browser window
				webDriver.switchTo().defaultContent();
				ExtentTestManager.getTest().log(LogStatus.PASS,"Address " + Site_Address + " selected/entered successfully");
				System.out.println("Address " + Site_Address + " selected/entered successfully");
			
				break;
					
			case "MANUALNEARNET": case "AUTOMATEDNEARNET": 
				waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.connectionFoundORNFoundColtElem, 20);
				Reusable.waitForpageloadmask();
				ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.connectionFoundORNFoundColtElem);
				// Switch back to first browser window
				webDriver.switchTo().defaultContent();
				ExtentTestManager.getTest().log(LogStatus.PASS,"Address " + Site_Address + " selected/entered successfully");
				System.out.println("Address " + Site_Address + " selected/entered successfully");
				break;				
			}			
		}
	
	public void ipAccessAdditionalProductData(String file_name,String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
	
		//Initializing the Variable
		
		String Site_Cabinet_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Cabinet_Type");
		String Site_Cabinet_Id = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Site_ID");
		String Site_Access_Technology = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Access_Technology");
		String Router_Technology = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Router_Technology");
		String Existing_Capacity_Lead_Time = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Existing_Capacity_Lead_Time");
		String IP_First_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_First_Name");
		String IP_Last_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Last_Name");
		String IP_Contact_Number = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Contact_Number");
		String IP_Mobile_Number = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Mobile_Number");
		String IP_Email = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Email");
		String IP_Fax_Number = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Fax_Number");
		String L3_Resilience_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"L3_Resilience_Type");

		//Additional Data Primary Site
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetType, 20);
		selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetType, CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetSubType, Site_Cabinet_Type);
		Reusable.waitForpageloadmask();
		sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetID,Site_Cabinet_Id);
		//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipSiteAccessTechnologyLst, CPQQuoteCreationObj.AdditionalProductDataPage.ipSiteAccessTechnologySubLst, Site_Access_Technology);
		//Reusable.waitForpageloadmask();
		//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "CPE IP Access Next Button");
		Reusable.waitForpageloadmask();
				
		//Additional Secondary Site
		if(L3_Resilience_Type.equalsIgnoreCase("Dual Access Unmanaged")||L3_Resilience_Type.equalsIgnoreCase("Dual Access Primary & Backup")
				||L3_Resilience_Type.equalsIgnoreCase("Dual Access Load Shared")) {
			scrollUp();
			click(CPQQuoteCreationObj.AdditionalProductDataPage.SecFeatures);
			//waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetType, 20);
			selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetTypesec, CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetSubTypesec, Site_Cabinet_Type);
			Reusable.waitForpageloadmask();
			sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.ipCabinetIDsec,Site_Cabinet_Id);
			//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipSiteAccessTechnologyLst, CPQQuoteCreationObj.AdditionalProductDataPage.ipSiteAccessTechnologySubLst, Site_Access_Technology);
			Reusable.waitForpageloadmask();
			//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "CPE IP Access Next Button");
			//Reusable.waitForpageloadmask();
			//waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.ipRouterTechnologyLst, 20);			
		}
		
		//Additional Service Information
		//waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.ipRouterTechnologyLst, 20);
		//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipRouterTechnologyLst, CPQQuoteCreationObj.AdditionalProductDataPage.ipRouterTechnologySubLst, Router_Technology);
		//Reusable.waitForpageloadmask();
		//selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipexistingCapacityLeadTimeLst, CPQQuoteCreationObj.AdditionalProductDataPage.ipexistingCapacityLeadTimeSubLst, Existing_Capacity_Lead_Time);
		//Reusable.waitForpageloadmask();
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.FirstNameTxtFld,IP_First_Name, "First Name");
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.LastNameTxtFld,IP_Last_Name, "Last Name");
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.ContactNumberTxtFld,IP_Contact_Number, "Contact Number");
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.MobileNumberTxtFld,IP_Mobile_Number, "Mobile Number");
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.emailIdTxtFld,IP_Email, "Email");
		//sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.ipFaxNumber,IP_Fax_Number, "Fax Number");
		
		
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate,"validate Button");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.validate,"validate Button");
		//waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete,120);
		//waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.ojUpdateEnabled,120);
		//verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "Next Button");
		//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, "Next Button");
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.validated, 120);			
	}

	public String updateSaveProductCPQ(String file_name,String Sheet_Name, String iScript, String iSubScript, String SaveType) throws IOException, InterruptedException {
		
		String upButton = null; String upButtonEnabled=null;
		String BuildingStatus = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"BuildingStatus");
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		if (verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.update4cBtn)) { 
			upButton = CPQQuoteCreationObj.AdditionalProductDataPage.update4cBtn; 
			upButtonEnabled= CPQQuoteCreationObj.AdditionalProductDataPage.ojUpdateEnabled;
		}else if (verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn)) { 
			upButton = CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn; 
			upButtonEnabled=CPQQuoteCreationObj.AdditionalProductDataPage.ojValidateEnabled;
		}
		
		if (verifyExists(upButton)) {
			Reusable.waitForpageloadmask();
			ScrollIntoViewByString(upButton);			
			clickByJS(upButton);
			Reusable.waitForpageloadmask();
			waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete, 75);
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "saveToQuoteBtn Button is not visible in CPQ, please verify");
			System.out.println("saveToQuoteBtn Button is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		if(BuildingStatus.equalsIgnoreCase("Yes")) 
		{
			if(Product_Name.equalsIgnoreCase("ColtIpAccess")) {
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.IPinProgressWarning);
			}
			else {
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.inProgressWarningAend);
			
			if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave"))
			{
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.inProgressWarningBend);
			}
			
			}
		}
		
		switch (SaveType.toUpperCase()) {
		
			case "SAVETOQUOTE":
				//Saving the Quote Details
				String sButton = null;
								
				if (waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn1, 10)) { 
					sButton = CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn1; 
				} else if (waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn, 10)) { 
					sButton = CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn; 
				}
				waitForAjax();
				
				if (waitForElementToBeVisible(sButton, 25)) {
					Reusable.waitForpageloadmask();
					click(sButton, "Save to Quote Button");
					for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
					Reusable.waitToPageLoad();
					waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn, 90);
					for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
					Reusable.waitToPageLoad();
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "saveToQuoteBtn Button is not visible in CPQ, please verify");
					System.out.println("saveToQuoteBtn Button is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					
				}
				break;
				
			case "SAVE":
	//			Calling the below method to save the details
				saveCPQ("Sub");				
				break;
		}
		
		return "True";
	}

	
	public String updateSaveProductCPQ(String SaveType) throws IOException, InterruptedException {
		
		String upButton = null; String upButtonEnabled=null;
		if (verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.update4cBtn)) { 
			upButton = CPQQuoteCreationObj.AdditionalProductDataPage.update4cBtn; 
			upButtonEnabled= CPQQuoteCreationObj.AdditionalProductDataPage.ojUpdateEnabled;
		}else if (verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn)) { 
			upButton = CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn; 
			upButtonEnabled=CPQQuoteCreationObj.AdditionalProductDataPage.ojValidateEnabled;
		}
		
		if (verifyExists(upButton)) {
			Reusable.waitForpageloadmask();
			ScrollIntoViewByString(upButton);			
			clickByJS(upButton);
			Reusable.waitForpageloadmask();
			waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete, 75);
			
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "saveToQuoteBtn Button is not visible in CPQ, please verify");
			System.out.println("saveToQuoteBtn Button is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		switch (SaveType.toUpperCase()) {
		
			case "SAVETOQUOTE":
				//Saving the Quote Details
				String sButton = null;
								
				if (waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn1, 10)) { 
					sButton = CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn1; 
				} else if (waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn, 10)) { 
					sButton = CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn; 
				}
				waitForAjax();
				
				if (waitForElementToBeVisible(sButton, 25)) {
					Reusable.waitForpageloadmask();
					click(sButton, "Save to Quote Button");
					for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
					Reusable.waitToPageLoad();
					waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn, 90);
					for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
					Reusable.waitToPageLoad();
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "saveToQuoteBtn Button is not visible in CPQ, please verify");
					System.out.println("saveToQuoteBtn Button is not visible in CPQ, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					
				}
				break;
				
			case "SAVE":
	//			Calling the below method to save the details
				saveCPQ("Sub");				
				break;
		}
		
		return "True";
	}
	
	public String saveCPQ(String ButtonType) throws IOException, InterruptedException {
	    
        String sButton = null;
        switch (ButtonType) {
        
            case "Main":
                sButton = CPQQuoteCreationObj.AdditionalProductDataPage.saveCPQBtn;
                break;
            case "Sub":
                sButton = CPQQuoteCreationObj.AdditionalProductDataPage.childSaveCPQBtn;
                break;
        }
        
        if (waitForElementToBeVisible(sButton, 60)) {

 

            for (int i = 1; i < 5; i++) { Reusable.waitForpageloadmask(); }
            click(sButton, "Save Button");
            for (int i = 1; i < 5; i++) { Reusable.waitForpageloadmask(); }
        } else {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "Save Button in CPQ is not visible, Please verify");
            System.out.println("Save Button in CPQ is not visible, Please verify");
            ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
            
        }        
        return "True";
        
    }
	
	public void saveTOQuoteCPQ() throws IOException, InterruptedException {
		
		waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn, 20);
		ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn);
		Reusable.waitForpageloadmask();
		clickByJS(CPQQuoteCreationObj.AdditionalProductDataPage.saveToQuoteBtn);
		Reusable.waitForpageloadmask();
		if (waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.errorContentCPQ, 5)) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ configuration save got failed due to the error "+findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.errorContentCPQ).getText());
			System.out.println("CPQ configuration save got failed due to the error "+findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.errorContentCPQ).getText());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
	}
	
	public String verifyQuoteStage(String Quote_Stage) throws IOException, InterruptedException {
	       
        int i, Iter;
        WEB_DRIVER_THREAD_LOCAL.get();
       
        if (Quote_Stage.equals("Ordered")) { Iter = 25; } else { Iter = 2; };
       
        if (waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem, 60)) {
            Reusable.WaitforCPQloader();
            String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
            for (i = 1; i <= Iter; i++) {
                if (quoteStage.equals(Quote_Stage)) {
                    ExtentTestManager.getTest().log(LogStatus.PASS, "Quote stage is set to "+quoteStage);
                    Reusable.WaitforCPQloader();
                    System.out.println("Quote stage is set to "+quoteStage);
                    break;
                } else {
                    Thread.sleep(10000);
                    Reusable.WaitforCPQloader();
                    click(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem, "Quote Stage");
                    quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
                    scrollIntoTop();
                    continue;
                }
            }
            if (i > Iter) {
                ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote stage is still set as "+quoteStage+ " but the Expected status is "+Quote_Stage);
                System.out.println("Quote stage is still set as "+quoteStage+ " but the Expected status is "+Quote_Stage);
                ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
            }
        }
       
        return "True";
    }

		
	public void EnterLegalNTechContactDetails(String file_name,String Sheet_Name, String iScript, String iSubScript) throws Exception
	{
		//**********************Enter Legal & Technical Contact Details*******************************
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.LegalContactlink, 20);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.LegalContactlink, "Get Legal Contact Details link ");
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.LegalContactlink);
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.LegalContactlink, "Get Legal Contact Details link ");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetLegalcontactbtn, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetLegalcontactbtn, "Get Legal Contact Details ");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetLegalcontactbtn, "Get Legal Contact Details ");
		
		String currentHandle =webDriver.getWindowHandle();
		switchWindow(Configuration.ContactDetails_URL,"Manage contacts");
		
		String Legal_Contact = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Legal_Contact");
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, "Search Text Box");
		sendKeys(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, Legal_Contact, "Search Text Box");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectLegalcontact, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectLegalcontact, "Select Legal Contact Details ");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectLegalcontact, "Select Legal Contact Details ");
		
		Reusable.WaitforC4Cloader();
		Reusable.WaitforCPQloader();
		waitForAjax();
		webDriver.switchTo().window(currentHandle);
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.TechnicalContactslink, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.TechnicalContactslink, "Get Technical Contact Details link");
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.TechnicalContactslink);
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.TechnicalContactslink, "Get Technical Contact Details link");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetTechnicalContactsbtn, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetTechnicalContactsbtn, "Get Technical Contact Details");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.GetTechnicalContactsbtn, "Get Technical Contact Details");
		
		switchWindow(Configuration.ContactDetails_URL,"Manage contacts");
		
		String Technical_Contact = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Technical_Contact");
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, "Search Text Box");
		sendKeys(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchText, Technical_Contact, "Search Text Box");
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, 15);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.searchBtn, "Search Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectTechnicalcontact, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectTechnicalcontact, "Select Technical Contact Details");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.SelectTechnicalcontact, "Select Technical Contact Details");
		
		Reusable.WaitforC4Cloader();
		Reusable.WaitforCPQloader();
		waitForAjax();
		webDriver.switchTo().window(currentHandle);
		refreshPage();
		Reusable.WaitforCPQloader();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.Savebutton, "Save Button");
		Reusable.WaitforCPQloader();
		waitForAjax();
	}
	
	public String SubmitForApproval() throws Exception
	{
		//**********************Submission for Commercial Approval*******************************
		
		String Click_Submit = "False";
		
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();
		waitForAjax();
		
		scrollUp();
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,10);
		verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"Approval Link");
		click(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"Approval Link");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		if (verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.VPSalesTxtElem))  { Click_Submit = "True"; }
		if (verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.dpReviewTxtElem)) { Click_Submit = "DPUser"; }
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn, "Submit To Approval Button");
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn);
		click(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn, "Submit To Approval Button");
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();
		waitForAjax();
		
		if (Click_Submit.equals("True")) {
	          
            //verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.VPSalesSubmitBtn,"'Submit button popup'");
            //click(CPQQuoteCreationObj.SubmissionforCommercialApproval.VPSalesSubmitBtn,"'Submit button popup'");
            verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.submitApprovalPopUpBtn,"'Submit button popup'");
            click(CPQQuoteCreationObj.SubmissionforCommercialApproval.submitApprovalPopUpBtn,"'Submit button popup'");
            Reusable.waitForpageloadmask();
            
            }
        
        if (Click_Submit.equals("DPUser")) {
        
            waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.approvalTxb, 20);
            verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.approvalTxb,"CommentboxforSubmit");
            sendKeys(CPQQuoteCreationObj.SubmissionforCommercialApproval.approvalTxb,"Approved","CommentboxforSubmit");
            verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.submitApprovalPopUpBtn,"'Submit button popup'");
            click(CPQQuoteCreationObj.SubmissionforCommercialApproval.submitApprovalPopUpBtn,"'Submit button popup'");
            Reusable.waitForpageloadmask();
            
            }
        //waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.SubmittoAppvlBtn, 60);
		//String sResult = Approval.validateCPQErrorMsg();
		//if (sResult.equalsIgnoreCase("False")) { return "False"; }
		
        if (verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.LinktoOpportunity)) {
			System.out.println("This Quote Cannot be submitted as it is not linked to Opportunity");
			return "True";
		}
        
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.WaitforCPQloader();
		waitForAjax();	
		
		return "True";
	}
		
	
	public String Submit4TechnicalApproval() throws Exception
	{
		//**********************Submission for Technical Approval*******************************				
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		
		Reusable.WaitforCPQloader();
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprCbxElem,20);
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprCbxElem);
		Reusable.WaitforCPQloader();			
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprovalCbx, "Ready For Technical Approval Checkbox");
		Reusable.WaitforCPQloader();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();		
		waitForAjax();
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		scrollUp();
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.WaitforCPQloader();
		waitForAjax();
		return "True";
		
	}
	
	public String SE_EngagementTab() throws Exception
	{
		//**********************Submission for Technical Approval*******************************				
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SEEngagment, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SEEngagment, "SE Engagment Link");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SEEngagment, "SE Engagment Link");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		return "True";
		
	}
	public String siteDetailEntry(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
//		Initializing the Variable
		
		String BandwidthValue = DataMiner.fngetcolvalue(testDataFile, sheetName,scriptNo, dataSetNo, "Service_Bandwidth");
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Resilience= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Resilience");
		String Contract_Term_year = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_Term_Year");
		String Billing_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Billing_Type");
		String VPN_Link_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VPN_Link_Type");
		String Maximum_Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Maximum_Bandwidth");
		String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPAccess_Type");
		String Presentation_Interface_AEnd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
		String Presentation_Interface_BEnd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
		String FlowType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		String Connector_Type_AEnd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
		String Connector_Type_BEnd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");		

		if(IPAccess_Type.equalsIgnoreCase("New")) 
		{
			Product_Name="ColtIpAccess";
		}
		switch(Product_Name) 
		{		
		case "EthernetLine": case "EthernetHub": case "Wave": case "EthernetSpoke":
//			calling the below method to perform site details operation
			verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
			Reusable.waitForpageloadmask();	    	
			//waitForAjax();
			
			if (Product_Name.equalsIgnoreCase("Wave")) 
			{
				String Interface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interface");
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermLst,"'Contract Term List'");
				
				isEnable(CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceLst);
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceLst,"'Interface List'");
				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceLst,"'Interface List'");
				selectByValueDIV(CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceLst, CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceSubLst, Interface);
				Reusable.waitForpageloadmask();
		    }
									
			waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthLst, 20);
            verifyExists(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthLst,"'Service Bandwidth List'");
            //ScrollIntoViewByString(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthDropdown);
            scrollUp();
            click(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthLst,"'Service Bandwidth List'");
           
            sendKeys(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthTxb,BandwidthValue,"'Service Bandwidth List'");
            click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthQry,"'Service Bandwidth Query'");
            ClickonElementByString("//div[@data-value='"+BandwidthValue+"']", 20);
            Reusable.waitForpageloadmask();
				
			//Resliance Type
			waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst, 20);	
			verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst,"'Resiliancy Type List'");
			//ScrollIntoViewByString(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst);
			scrollUp();
			click(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst,"'Resiliancy Type List'");
			ClickonElementByString("//div[@data-value='"+Resilience+"']", 20);
			Reusable.waitForpageloadmask();		
			
			waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermLst, 20);
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermLst,"'Contract Term List'");
			ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermLst);
			selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermLst,CPQQuoteCreationObj.AdditionalProductDataPage.contractTermSubLst,Contract_Term_year);
			//click(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermLst,"'Contract Term List'");
			//ClickonElementByString("//div[@data-value='"+Contract_Term_year+"']", 20);
			Reusable.waitForpageloadmask();	
			/*
			//Selecing Presentation Interface
			scrollUp();
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
			Reusable.waitForpageloadmask();
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst,"'Interface List'");
			ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst);
			click(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst,"Presentation Interface AList");
			ClickonElementByString("//li[normalize-space(.)='"+ Presentation_Interface_AEnd +"']", 30);
			Reusable.waitForpageloadmask();

			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst,"Connector Type AList");
			ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst);
			click(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst,"Connector Type AList");
			ClickonElementByString("//li[normalize-space(.)='"+ Connector_Type_AEnd +"']", 30);			
			Reusable.waitForpageloadmask();
			
			if (!Product_Name.equalsIgnoreCase("EthernetHub")&&!Product_Name.equalsIgnoreCase("EthernetSpoke"))
			{
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceBLst,"Presentation Interface BList");
				ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceBLst);
				click(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceBLst,"Presentation Interface BList");
				ClickonElementByString("//li[normalize-space(.)='"+ Presentation_Interface_BEnd +"']", 30);
				Reusable.waitForpageloadmask();

				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeBLst,"Connector Type BList");
				ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeBLst);
				click(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeBLst,"Connector Type BList");
				ClickonElementByString("//li[normalize-space(.)='"+ Connector_Type_BEnd +"']", 30);				
				Reusable.waitForpageloadmask();
			}

			verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
			ScrollIntoViewByString(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk);
			click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
			Reusable.waitForpageloadmask();
		*/
			break;			
    		
case "ColtIpAccess": case "ColtIpAccessDC":
			
		//	String DataCenter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Data_Center");
	
	/*		waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, 20);
			verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, "Resiliancy Type List");
			Reusable.waitForpageloadmask(); 
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.ipcontractTermLst,"'Contract Term List'");
			ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.ipcontractTermLst);
			//click(CPQQuoteCreationObj.L3RESILIENCEPage.IPContractTermLst,"'Contract Term List'");
			selectByValueDIV(CPQQuoteCreationObj.AdditionalProductDataPage.ipcontractTermLst,CPQQuoteCreationObj.L3RESILIENCEPage.IPContractTermSubLst,Contract_Term_year);
			//ClickonElementByString("//div[@data-value='"+Contract_Term_year+"']", 20);
			Reusable.waitForpageloadmask(); */
			
			verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IPBillingTypeLst,"'IP Billing Type List'");
			click(CPQQuoteCreationObj.L3RESILIENCEPage.IPBillingTypeLst,"'Contract Term List'");			
			ClickonElementByString("//div[@data-value='"+Billing_Type+"']", 20);
			Reusable.waitForpageloadmask();  
			
			if(Billing_Type.equalsIgnoreCase("UBB")) 
			{
				verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IPcomittedBandwidthLnk,"'IP Comitted Bandwidth Sub link'");
				click(CPQQuoteCreationObj.L3RESILIENCEPage.IPcomittedBandwidthLnk,"'Contract Term List'");
				sendKeys(CPQQuoteCreationObj.L3RESILIENCEPage.IPcomittedBandwidthLnk,BandwidthValue,"'IPContractTerm List'");
				ClickonElementByString("//div[@data-value='"+BandwidthValue+"']", 20);				
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IpmaximumBandwidthLnk,"'IP Maximum Bandwidth Sub link'");
				click(CPQQuoteCreationObj.L3RESILIENCEPage.IpmaximumBandwidthLnk,"'Contract Term List'");
				sendKeys(CPQQuoteCreationObj.L3RESILIENCEPage.IpmaximumBandwidthLnk,Maximum_Bandwidth,"'IPContractTerm List'");
				ClickonElementByString("//div[@data-value='"+Maximum_Bandwidth+"']", 20);	
									
				Reusable.waitForpageloadmask();								
			}
			else 
			{

				waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, 20);
	            verifyExists(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'"); 
	            ScrollIntoViewByString(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst);
	            click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");
	           
	            sendKeys(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthTxb,BandwidthValue,"'Service Bandwidth List'");
	            click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthQry,"'Service Bandwidth Query'");
	            //selectByValueDIV(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthSubLst,BandwidthValue);
	            ClickonElementByString("//div[@data-value='"+BandwidthValue+"']", 20);
	            Reusable.waitForpageloadmask();		

				
				
			}
			//Resliance Type
			waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.ipResiliancyTypeLst, 20);	
			verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.ipResiliancyTypeLst,"'Resiliancy Type List'");
			//ScrollIntoViewByString(CPQQuoteCreationObj.L3RESILIENCEPage.ResiliancyTypeLst);
			scrollUp();
			click(CPQQuoteCreationObj.L3RESILIENCEPage.ipResiliancyTypeLst,"'Resiliancy Type List'");
			ClickonElementByString("//div[@data-value='"+Resilience+"']", 20);
			
						
			if(FlowType.equalsIgnoreCase("DC"))
			{
				
				verifyExists(CPQQuoteCreationObj.CPEConfiguration.plannedbwCbx,"'Planned Bandwidth'");
				click(CPQQuoteCreationObj.CPEConfiguration.plannedbwCbx);
				waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.plannedBandwidthLst, 20);
	            verifyExists(CPQQuoteCreationObj.CPEConfiguration.plannedBandwidthLst,"'Planned Bandwidth List'"); 
	            ScrollIntoViewByString(CPQQuoteCreationObj.CPEConfiguration.plannedBandwidthLst);
	            click(CPQQuoteCreationObj.CPEConfiguration.plannedBandwidthLst,"'Planned Bandwidth List'");
	           
	            sendKeys(CPQQuoteCreationObj.CPEConfiguration.plannedBandwidthTxb,Maximum_Bandwidth,"'Planned Bandwidth List'");
	            click(CPQQuoteCreationObj.CPEConfiguration.plannedBandwidthQry,"'Planned Bandwidth Query'");
	            //selectByValueDIV(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthSubLst,BandwidthValue);
	            ClickonElementByString("//div[@data-value='"+Maximum_Bandwidth+"']", 20);
	            Reusable.waitForpageloadmask();		
	            
	           
				OverrideBuildingtype(testDataFile, sheetName, scriptNo,dataSetNo);
				
				
			}
			break;
			
		case "VPNNetwork":
			waitForElementToAppear(CPQQuoteCreationObj.VPNAddress.vpnsiteConfiguartionLink, 20);
			verifyExists(CPQQuoteCreationObj.VPNAddress.vpnsiteConfiguartionLink,"'VPN Site Configuration Link'");
			click(CPQQuoteCreationObj.VPNAddress.vpnsiteConfiguartionLink,"'VPN Site Configuration Link'");
			Reusable.waitForpageloadmask();
			
			verifyExists(CPQQuoteCreationObj.VPNAddress.vpnlinkTypeLink,"'VPN Type Link'");
			click(CPQQuoteCreationObj.VPNAddress.vpnlinkTypeLink,"'VPN Type Link'");
			click("//li[normalize-space(.)='"+VPN_Link_Type+"']","'VPN Type Link'");
			Reusable.WaitforProdConfigLoader();
				
			verifyExists(CPQQuoteCreationObj.VPNSiteConfig.VPNContractYearLnk,"'VPN Contract Link'");
			click(CPQQuoteCreationObj.VPNSiteConfig.VPNContractYearLnk,"'VPN Contract Link'");
			click("//li[normalize-space(.)='"+Contract_Term_year+"']","'VPN Contract Link'");
			Reusable.WaitforProdConfigLoader();
			
			verifyExists(CPQQuoteCreationObj.VPNSiteConfig.VPNServiceBandLnk,"'VPN Service Bandwidth Link'");
			click(CPQQuoteCreationObj.VPNSiteConfig.VPNServiceBandLnk,"'VPN Service Bandwidth Link'");
			click("//li[normalize-space(.)='"+BandwithValue+"']","'VPN Service Bandwidth Link'");
			Reusable.WaitforProdConfigLoader();
			
			verifyExists(CPQQuoteCreationObj.VPNSiteConfig.VPNResilienceLnk,"'VPN Resilience Link'");
			click(CPQQuoteCreationObj.VPNSiteConfig.VPNResilienceLnk,"'VPN Resilience Link'");
			click("//li[normalize-space(.)='"+Contract_Term_year+"']","'VPN Resilience Link'");
			Reusable.waitForpageloadmask();
			break;
			}
		return "True";
	}
	

	public void offnetEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 

	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");
		String Technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Technology");
		String Connection_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Connection_Type");
		String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
		String exploreOptionsSubLsb = null;String offnetCheckRadioButton=null;
		
		switch (SiteName) {
		
		case "A_End":
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckAEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
			offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetAManualOffnetRdB;
			break;
			
		case "B_End":
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckBEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
			offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetBManualOffnetRdB;
			break;
		}
		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")&&!Product_Name.equalsIgnoreCase("ColtIpAccess")&&
				!Product_Name.equalsIgnoreCase("Wave")&&!Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{
			
			if(!Product_Name.equalsIgnoreCase("Wave")) {
			waitForElementToAppear(offnetCheckBtn, 20);

			verifyExists(offnetCheckBtn,"'Offnet Check Button'");
			scrollDown(offnetCheckBtn);
			click(offnetCheckBtn,"'Offnet Check Button'");			
			for(int i=1;i<=6; i++) { Reusable.waitForpageloadmask(); }
			
			waitForElementToAppear(manualEngagementBtn, 75);
			scrollIntoView(findWebElement(manualEngagementBtn));
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForAjax();
			ScrollIntoViewByString(manualEngagementBtn);
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			scrollIntoView(findWebElement(exploreOptionsLsb));
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			ScrollIntoViewByString(exploreOptionsLsb);
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			Reusable.WaitforProdConfigLoader();		
			switchToFrame("exploreEngagementComponent_oob");
			
			}
		}
		
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipoffnetcheckbtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualengagementbtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexploredropdown;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexplorelst;
			
			waitForElementToAppear(offnetCheckBtn,75);
			verifyExists(offnetCheckBtn,"'Offnet Check Button'");
			ScrollIntoViewByString(offnetCheckBtn);
			click(offnetCheckBtn,"'Offnet Check Button'");			
			for(int i=1;i<=6; i++) { Reusable.waitForpageloadmask(); }
			Reusable.waitForAjax();
			
			verifyExists(manualEngagementBtn,"Manual Engagement Button");
			ScrollIntoViewByString(manualEngagementBtn);
			click(manualEngagementBtn,"Manual Engagement Button");			
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb,75);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
						
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			switchToFrame("exploreEngagementComponent");			
		}
		
		if (Product_Name.equalsIgnoreCase("VPNNetwork")) {
			
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnOffNetCheckBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnManualCheckBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreRequestVPN;
			
			waitForElementToAppear(offnetCheckBtn,75);
			verifyExists(offnetCheckBtn,"'Offnet Check Button'");
			scrollToViewNClick(offnetCheckBtn,"'Offnet Check Button'");			

			Reusable.waitForpageloadmask();
			
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb,75);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			click(exploreOptionsLsb,"'Explore Option List box'");

			click("//li[normalize-space(.)='"+Explore_Options+"']", "'Click Explore Option'");

			selectByValueDIV(exploreOptionsLsb, Explore_Options,"'Select Explore Option'");

			Reusable.WaitforProdConfigLoader();	
			switchToFrame("exploreEngagementComponent_oob");	
		}
		
		if(!Product_Name.equalsIgnoreCase("Wave")) 
		{
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,"'Offnet Prioriry Text Box'");	
			Reusable.waitForpageloadmask();		
			
			String buliding_number=findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreBuldingNumberTxb).getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
			System.out.println("Explore Buliding Number is "+buliding_number);
						
			sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb, Priority);
			keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,Keys.ENTER);
			Reusable.waitForpageloadmask();
			
			if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
			{ 
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.TechnologyTxb,"'Offnet Prioriry Text Box'");
				sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.TechnologyTxb, Technology);
				keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.TechnologyTxb,Keys.ENTER);
				Reusable.waitForpageloadmask();
			}
			
			if (Connection_Type.equalsIgnoreCase("DualEntry")) 
			{ 
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx); 
			}
				
			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,20);				
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'");
			isClickable(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn);
			Thread.sleep(2000);
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();


            //WebInteractUtil.pause(4000);
            //Reusable.waitForAjax();
               
            //webDriver.switchTo().alert().accept();	
            //WebInteractUtil.pause(2000);
            //Reusable.waitForAjax();
               
            verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
            click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
				
			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,75);
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,"'Request ID'");

			String fullVal = findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem).getText();
			String[] bits = fullVal.split(":");
			String Request_ID = bits[bits.length-1].trim();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
			System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
	//				Passing this request ID to testdata sheet
			if(SiteName.equalsIgnoreCase("A_End"))
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
			}
			else
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID);
			}			
			switchToDefaultFrame();
				
			if (Product_Name.equalsIgnoreCase("VPNNetwork")) 
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			}
						
			/*else if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
			{ 
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"'IP Access Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"'IP Access Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			}*/ 
			else 
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			}			
		}
		if(Product_Name.contains("Ethernet")) 
		{
			waitForElementToAppear(offnetCheckRadioButton,60);
			verifyExists(offnetCheckRadioButton,"'Explore Close Button'");
			scrollIntoView(findWebElement(offnetCheckRadioButton));
			click(offnetCheckRadioButton,"'Explore Close Button'");
			Reusable.waitForpageloadmask();		
		}
		
		else if(Product_Name.equalsIgnoreCase("ColtIpAccess"))
		{
			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB,60);
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB,"'Explore Close Button'");
			scrollIntoView(findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB));
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB,"'Explore Close Button'");
			Reusable.waitForpageloadmask();
				
		}
		verifyExists(manualEngagementBtn,"'Manual Engagement Button'");
		click(manualEngagementBtn,"'Manual Engageent Button'");
		Reusable.waitForpageloadmask();

	}
	public String nearnetEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");
		String Connection_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connection_Type");	
		String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
		String exploreOptionsSubLsb = null; String manualRadioButton=null; String IpAccessmanualRadioButton=null;
		
		switch (SiteName) {
		
		case "A_End":
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
			manualRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetManualNearnetARdBtn;
			IpAccessmanualRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualNearnetARdBtn;
			
			break;
			
		case "B_End":
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
			manualRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetManualNearnetBRdBtn;
			break;
		}
		
		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")&&!Product_Name.equalsIgnoreCase("ColtIpAccess")&&!Product_Name.equalsIgnoreCase("Wave")
				&&!Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{
			
			if (!Product_Name.equalsIgnoreCase("Wave")) {
			ScrollIntoViewByString(manualEngagementBtn);
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForAjax();
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 30);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			switchToFrame("exploreEngagementComponent_oob");
			//WebInteractUtil.switchToFrame("exploreEngagementComponent_oob");
			}	
		}
		
		//Ip Access
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualengagementbtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexploredropdown;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexplorelst;
			
			ScrollIntoViewByString(manualEngagementBtn);
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForAjax();
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			//switchWindow("exploreEngagementComponent","Explore Page");
			switchToFrame("exploreEngagementComponent");				
		}		
		
		if (Product_Name.equalsIgnoreCase("VPNNetwork")) {
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnManualCheckBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreRequestVPN;
			
			waitForElementToAppear(manualEngagementBtn, 20);
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.WaitforProdConfigLoader();
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.WaitforProdConfigLoader();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);		
			click(exploreOptionsLsb,"'Explore Option List box'");
			click("//li[normalize-space(.)='"+Explore_Options+"']","'Explore Option List box'");
			Reusable.WaitforProdConfigLoader();
			switchToFrame("exploreEngagementComponent_oob");
		}
		
		if(!Product_Name.equalsIgnoreCase("Wave")) 
		{
			System.out.println("Connection_Type: "+Connection_Type);
			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn,75);
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn,"'Explore Submit Button'");
			Reusable.waitForpageloadmask();
			
			if(Product_Name.equalsIgnoreCase("ColtIpAccess")||Connection_Type.equalsIgnoreCase("New Building Dual Entry")) 
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ConnectionType,"'Connection Type'");
				sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ConnectionType, Connection_Type);
				keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ConnectionType,Keys.ENTER);
				Reusable.waitForpageloadmask();
			}
			
			String buliding_number=findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreNearnetBuldingNumberTxb).getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
			System.out.println("Explore Buliding Number is "+buliding_number);
			
			//waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn,75);
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn,"'Explore Submit Btn'");
			Reusable.waitForAjax();
			
			WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();

			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreRequestIDTdElem,75);
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreRequestIDTdElem,"'Request ID'");
			
			String Request_ID = findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreRequestIDTdElem).getText().trim();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
			System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//				Passing this request ID to testdata sheet
			if(SiteName.equalsIgnoreCase("A_End"))
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
			}
			else
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID);
			}
			switchToDefaultFrame();
			if (Product_Name.equalsIgnoreCase("VPNNetwork"))
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			} 
			
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'"); 
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
			Reusable.WaitforProdConfigLoader();
				
			Reusable.waitForpageloadmask();
		
			if(Product_Name.contains("Ethernet")) 
			{
				waitForElementToAppear(manualRadioButton, 60);
				scrollIntoView(findWebElement(manualRadioButton));
				verifyExists(manualRadioButton,"'Manual Radio Button'");
				click(manualRadioButton,"'Manual Radio Button'");
				Reusable.waitForpageloadmask();
			}
			else if(Product_Name.equalsIgnoreCase("ColtIpAccess"))
			{
				waitForElementToAppear(IpAccessmanualRadioButton, 60);
				scrollIntoView(findWebElement(IpAccessmanualRadioButton));
				verifyExists(IpAccessmanualRadioButton,"'Manual Radio Button'");
				click(IpAccessmanualRadioButton,"'Manual Radio Button'");
				Reusable.waitForpageloadmask();
			}
			
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForAjax();
			ScrollIntoViewByString(manualEngagementBtn);
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
		}
		return "True";
	}
	
	public String dslEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");		
		String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
		String dslCheckBtn = null; String exploreOptionsSubLsb = null; String dslManualCheckRadioBtn = null;
		
		switch (SiteName) {
		
		case "A_End":
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckAEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
			dslManualCheckRadioBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslManualCheckAEndRadiBtn;
			break;
			
		case "B_End":
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckBEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
			dslManualCheckRadioBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslManualCheckBEndRadiBtn;
			break;
		}
		
		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")||!Product_Name.equalsIgnoreCase("ColtIpAccess")
				&&!Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{			
			waitForElementToAppear(dslCheckBtn, 20);
			verifyExists(dslCheckBtn,"'DSL Check Button'");
			Reusable.waitForpageloadmask();
			ScrollIntoViewByString(dslCheckBtn);
			click(dslCheckBtn,"'DSL Check Button'");			
			for(int i=1;i<=9;i++) {Reusable.waitForpageloadmask();}
			Reusable.waitForAjax();
			
			
			
			waitForElementToAppear(manualEngagementBtn, 75);
			scrollIntoView(findWebElement(manualEngagementBtn));
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForAjax();
			ScrollIntoViewByString(manualEngagementBtn);
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			scrollIntoView(findWebElement(exploreOptionsLsb));
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			ScrollIntoViewByString(exploreOptionsLsb);
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			Reusable.WaitforProdConfigLoader();		
			switchToFrame("exploreEngagementComponent_oob");
		}
		
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpDSLCheck;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualengagementbtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexploredropdown;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexplorelst;
						
			waitForElementToAppear(dslCheckBtn, 20);
			verifyExists(dslCheckBtn,"'DSL Check Button'");
			Reusable.WaitforProdConfigLoader();
			scrollToViewNClick(dslCheckBtn,"'DSL Check Button'");			
			for(int i=1;i<=6;i++) {Reusable.waitForpageloadmask();}
			
			waitForElementToAppear(manualEngagementBtn, 30);
			verifyExists(manualEngagementBtn,"'Manual Engagement Button'");
			ScrollIntoViewByString(manualEngagementBtn);	
			click(manualEngagementBtn,"'Manual Engagement Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			Reusable.WaitforProdConfigLoader();
			switchToFrame("exploreEngagementComponent");	
		
						
		}
			
		if (Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnOffNetDSLCheckBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnManualCheckBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreRequestVPN;
			
			waitForElementToAppear(dslCheckBtn, 20);
			verifyExists(dslCheckBtn,"'DSL Check Button'");
			Reusable.WaitforProdConfigLoader();
			scrollToViewNClick(dslCheckBtn,"'DSL Check Button'");			
			Reusable.waitForpageloadmask();
				
			verifyExists(manualEngagementBtn,"'Manual Engagement Button'");
			ScrollIntoViewByString(manualEngagementBtn);
			click(manualEngagementBtn,"'Manual Engagement Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			click("//li[normalize-space(.)='"+Explore_Options+"']", "'Select Explore Option'");

			selectByValueDIV(exploreOptionsLsb, Explore_Options,"'Select Explore Option'");
			Reusable.WaitforProdConfigLoader();
			switchToFrame("exploreEngagementComponent_oob");					
		}
				
		//waitForElementToBeVisible(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,45);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,"'Offnet Prioriry Text Box'");	
		Reusable.waitForpageloadmask();
		
		String buliding_number=findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreBuldingNumberTxb).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
		System.out.println("Explore Buliding Number is "+buliding_number);
		
		sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb, Priority);
		keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,Keys.ENTER);
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
		click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
		//WebInteractUtil.pause(4000);
		Reusable.waitForAjax();
		
		//WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		//webDriver.switchTo().alert().accept();
		//WebInteractUtil.pause(2000);
		//Reusable.waitForAjax();
		
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'"); 
		click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
			
		waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,75);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,"'Request ID'");

			String fullVal = findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem).getText();
			String[] bits = fullVal.split(":");
			String Request_ID = bits[bits.length-1].trim();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
			System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//				Passing this request ID to testdata sheet
			if(SiteName.equalsIgnoreCase("A_End"))
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
			}
			else
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID);
			}

		    switchToDefaultFrame();               
		            /*if (Product_Name.equalsIgnoreCase("ColtIpAccess"))
		            {​​​​​​​
		                verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreIPAccessCloseBtn,"'IP Access Explore Close Button'");
		                click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreIPAccessCloseBtn,"'IP Access Explore Close Button'");
		                Reusable.WaitforProdConfigLoader();
		            }​​​​​​​
		            else
		            {​​​​​​​*/
		                verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
		                click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
		                Reusable.WaitforProdConfigLoader();
		            //}​​​​​​​

			Reusable.waitForpageloadmask();			
		
		if(Product_Name.contains("Ethernet")) 
		{
			waitForElementToAppear(dslManualCheckRadioBtn,60);
			verifyExists(dslManualCheckRadioBtn,"'IP Access Explore Close Button'");
			click(dslManualCheckRadioBtn,"'IP Access Explore Close Button'");
			Reusable.waitForpageloadmask();	
		}
		
		verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
		Reusable.waitForAjax();
		ScrollIntoViewByString(manualEngagementBtn);
		click(manualEngagementBtn,"'Manual Engageent Button'");
		Reusable.waitForpageloadmask();
		
		return "True";		
	}
	
	public String dslEntries_MultiRequest(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");		
		String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
		String dslCheckBtn = null; String dslManualCheckRadioBtn1 = null; String exploreOptionsSubLsb = null; String dslManualCheckRadioBtn = null;
		
		switch (SiteName) {
		
		case "A_End":
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckAEndBtn_MultiReq;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
			dslManualCheckRadioBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslManualCheckAEndRadiBtn;
			dslManualCheckRadioBtn1=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslManualCheckAEndRadiBtn1;
			break;
			
		case "B_End":
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckBEndBtn_MultiReq;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
			dslManualCheckRadioBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslManualCheckBEndRadiBtn;
			dslManualCheckRadioBtn1=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslManualCheckBEndRadiBtn1;
			break;
		}
		
		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")||!Product_Name.equalsIgnoreCase("ColtIpAccess")
				&&!Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{			
			waitForElementToAppear(dslCheckBtn, 20);
			verifyExists(dslCheckBtn,"'DSL Check Button'");
			Reusable.waitForpageloadmask();
			ScrollIntoViewByString(dslCheckBtn);
			click(dslCheckBtn,"'DSL Check Button'");			
			for(int i=1;i<=6;i++) {Reusable.waitForpageloadmask();}
			Reusable.waitForAjax();
			
			
			
			waitForElementToAppear(manualEngagementBtn, 75);
			scrollIntoView(findWebElement(manualEngagementBtn));
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForAjax();
			if(!verifyExists(exploreOptionsLsb)) 
			{
			ScrollIntoViewByString(manualEngagementBtn);
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			}
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			Reusable.WaitforProdConfigLoader();		
			switchToFrame("exploreEngagementComponent_oob");
		}
		
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpDSLCheck;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndSubLst;
						
			waitForElementToAppear(dslCheckBtn, 20);
			verifyExists(dslCheckBtn,"'DSL Check Button'");
			Reusable.WaitforProdConfigLoader();
			scrollToViewNClick(dslCheckBtn,"'DSL Check Button'");			
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(manualEngagementBtn, 30);
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			ScrollIntoViewByString(manualEngagementBtn);	
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			Reusable.WaitforProdConfigLoader();
			switchToFrame("exploreEngagementComponent_oob");	
		
						
		}
			
		if (Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnOffNetDSLCheckBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnManualCheckBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreRequestVPN;
			
			waitForElementToAppear(dslCheckBtn, 20);
			verifyExists(dslCheckBtn,"'DSL Check Button'");
			Reusable.WaitforProdConfigLoader();
			scrollToViewNClick(dslCheckBtn,"'DSL Check Button'");			
			Reusable.waitForpageloadmask();
				
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			ScrollIntoViewByString(manualEngagementBtn);
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			click("//li[normalize-space(.)='"+Explore_Options+"']", "'Select Explore Option'");

			selectByValueDIV(exploreOptionsLsb, Explore_Options,"'Select Explore Option'");
			Reusable.WaitforProdConfigLoader();
			switchToFrame("exploreEngagementComponent_oob");					
		}
				
		//waitForElementToBeVisible(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,45);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,"'Offnet Prioriry Text Box'");	
		Reusable.waitForpageloadmask();
		
		String buliding_number=findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreBuldingNumberTxb).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
		System.out.println("Explore Buliding Number is "+buliding_number);
		
		sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb, Priority);
		keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,Keys.ENTER);
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
		click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
		//WebInteractUtil.pause(4000);
		Reusable.waitForAjax();
		
		//WebDriver driver = DriverManagerUtil.WEB_DRIVER_THREAD_LOCAL.get();
		//webDriver.switchTo().alert().accept();
		//WebInteractUtil.pause(2000);
		//Reusable.waitForAjax();
		
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'"); 
		click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
			
		waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,75);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,"'Request ID'");

			String fullVal = findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem).getText();
			String[] bits = fullVal.split(":");
			String Request_ID = bits[bits.length-1].trim();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
			System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//				Passing this request ID to testdata sheet
			if(SiteName.equalsIgnoreCase("A_End"))
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
			}
			else
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID);
			}
			switchToDefaultFrame();				
			if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreIPAccessCloseBtn,"'IP Access Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreIPAccessCloseBtn,"'IP Access Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			} 
			else 
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'IP Access Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'IP Access Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			}
			Reusable.waitForpageloadmask();			
		
		if(Product_Name.contains("Ethernet")) 
		{
			waitForElementToAppear(dslManualCheckRadioBtn,60);
			verifyExists(dslManualCheckRadioBtn,"dsl Manula Check Radio Button");
			if(!verifyExists(dslManualCheckRadioBtn1)) {
				click(dslManualCheckRadioBtn,"dsl Manula Check Radio Button");
			}
			else
			{
				click(dslManualCheckRadioBtn1,"dsl Manula Check Radio Button");
			}
			Reusable.waitForpageloadmask();	
		}
		return "True";		
	}
	
	public String manualOLODSLConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{	
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		String sResult;
		
		switch (Flow_Type.toUpperCase()) {
				
			case "MANUALOLODSL":
				
//				Entering A-End details
				sResult = manualOLODSLEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					sResult = manualOLODSLEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				}
				break;				
		}				
		return "True";		
	}
	
	public String manualOLODSLEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");		
		String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
		String dslCheckBtn = null; String exploreOptionsSubLsb = null;String dslManualCheckRadioBtn = null;
		String offnetlinkTab=null; String dsllinkTab=null;
		switch (SiteName) {
		
		case "A_End":
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckAEndBtn;
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckAEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
			dslManualCheckRadioBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetAManualOffnetRdB;
			offnetlinkTab=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetALinkTab;
			dsllinkTab= CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DSLALinkTab;
			break;
			
		case "B_End":
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckBEndBtn;
			dslCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckBEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
			dslManualCheckRadioBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetBManualOffnetRdB;
			offnetlinkTab=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetBLinkTab;
			dsllinkTab= CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DSLBLinkTab;
			break;
		}
		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")||!Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{
			waitForElementToAppear(offnetCheckBtn, 20);
			verifyExists(offnetCheckBtn,"'Offnet Check Button'");
			scrollToViewNClick(offnetCheckBtn,"'Offnet Check Button'");			
			for(int i=1;i<=4;i++) { Reusable.waitForpageloadmask(); }
			
			verifyExists(dslCheckBtn,"'Offnet Check Button'");
			scrollToViewNClick(dslCheckBtn,"'Offnet Check Button'");			
			for(int i=1;i<=4;i++) { Reusable.waitForpageloadmask(); }
			
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			click(manualEngagementBtn,"'Manual Engagement Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			switchToFrame("exploreEngagementComponent_oob");	
		}
		
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpOffnetCheckAEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndSubLst;
			dslCheckBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpDSLCheck;
			
			waitForElementToAppear(offnetCheckBtn,75);
			verifyExists(offnetCheckBtn,"'Offnet Check Button'");
			Reusable.WaitforProdConfigLoader(); Reusable.waitForpageloadmask(); Reusable.Waittilljquesryupdated();
			click(offnetCheckBtn,"'Offnet Check Button'");			
			Reusable.WaitforProdConfigLoader(); Reusable.waitForpageloadmask(); Reusable.Waittilljquesryupdated();

			isVisible(C4CAccountsObj.Accounts.ProdConfigLoadingIcn);

			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(dslCheckBtn,"'DSL Check Button'");
			click(dslCheckBtn,"'DSL Check Button'");
			Reusable.WaitforProdConfigLoader(); Reusable.waitForpageloadmask(); Reusable.Waittilljquesryupdated();

			isVisible(C4CAccountsObj.Accounts.ProdConfigLoadingIcn);

			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
				
			waitForElementToAppear(exploreOptionsLsb, 20);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
				
			selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			switchToFrame("exploreEngagementComponent_oob");				
		}		
		waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,75);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,"'Offnet Prioriry Text Box'");
		
		sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb, Priority);
		keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,Keys.ENTER);
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
		click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
		//WebInteractUtil.pause(4000);
		Reusable.waitForAjax();
			
		//webDriver.switchTo().alert().accept();
		//WebInteractUtil.pause(2000);
		//Reusable.waitForAjax();
		
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'"); 
		click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
			
		waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,75);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,"'Request ID'");
		
		//isElementPresent(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem);
		String fullVal = getTextFrom(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem);
		String[] bits = fullVal.split(":");
		String Request_ID = bits[bits.length-1].trim();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
		System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//				Passing this request ID to testdata sheet
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
		switchToDefaultFrame();
		Reusable.waitForpageloadmask();
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"'IP Access Explore Close Button'"); 
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"'IP Access Explore Close Button'");
			Reusable.waitForpageloadmask();
		}
		else 
		{
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'"); 
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
			Reusable.waitForpageloadmask();
		}
		
		if(Product_Name.contains("Ethernet")) 
		{
			waitForElementToAppear(dsllinkTab, 60);
			click(dsllinkTab,"'Offnet Link Tab'");
			Reusable.waitForpageloadmask();
			click(dsllinkTab,"'Offnet Link Tab'");
			Reusable.waitForpageloadmask();
			scrollToViewNClick(dslManualCheckRadioBtn,"'DSL Radio Button'");
			Reusable.waitForpageloadmask();			
		}	
		return "True";		
	}
	
	public String selectProductFeatures(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Flow_Type");
		String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IPAccess_Type");		
		String sResult;
		
		if(IPAccess_Type.equalsIgnoreCase("New")) {
			Product_Name="ColtIpAccess";
		}		
		switch (Product_Name.toUpperCase()) {
				
			case "ETHERNETLINE": case "ETHERNETHUB": case "ETHERNETSPOKE": case "WAVE": case "COLTIPACCESS":
				
				
				//				Entering A-End details
				sResult = applicableFeatureSelection(testDataFile, sheetName, scriptNo, dataSetNo,"A_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave"))
				{
					sResult = applicableFeatureSelection(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				}
				
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					
				}
				break;
				
				
		}
		return "True";		
	}
	public String selectCrossConnectFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
	// Initializing the Variable
	String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
	String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Flow_Type");
	String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IPAccess_Type");
	String SegmentType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Segment");
	String sResult;

	 if(IPAccess_Type.equalsIgnoreCase("New")) {
	Product_Name="ColtIpAccess";
	}
	waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
	ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
	click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
	Reusable.waitForpageloadmask();
	switch (Product_Name.toUpperCase()) {

	 case "ETHERNETLINE": case "ETHERNETHUB": case "ETHERNETSPOKE": case "WAVE": case "COLTIPACCESS":

	 // Entering A-End details
	if (Product_Name.equalsIgnoreCase("EthernetLine") && (SegmentType.equalsIgnoreCase("CLTP - Google"))) {
	sResult = ApplyCrossConnectFeature_AEnd(testDataFile, sheetName, scriptNo, dataSetNo);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	}
	// Entering B-End details
	if (Product_Name.equalsIgnoreCase("EthernetLine") && (SegmentType.equalsIgnoreCase("CLTP - Google"))) {
	sResult = ApplyCrossConnectFeature_BEnd(testDataFile, sheetName, scriptNo, dataSetNo);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	}
	break;
	}
	return "True";
	}
	
	
	public String ApplyCrossConnectFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
	{		
//		  Initializing the Variable
		WebElement demarcationCheckBox=null;
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String PR_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PR_Type");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");

		
		if(!Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{
			scrollUp();
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
			Reusable.waitForpageloadmask();
		}
        
        switch (SiteName) {
		
		case "A_End":
			
			waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectAEnd, 60);
			verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectAEnd,"Carrier Hotel Cross Connect AEnd");
		    click(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectAEnd,"Carrier Hotel Cross Connect AEnd");		        
		    Reusable.waitForpageloadmask();
		        
		    waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDown, 60);
		    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDown,"Supplier A End DropDown");
		    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDown,"Supplier A End DropDown");		        
		    Reusable.waitForpageloadmask();
		        
		    waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDownValue, 60);
		    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDownValue,"Supplier A End DropDownValue");
		    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDownValue,"Supplier A End DropDownValue");		        
		    Reusable.waitForpageloadmask();
		  		  		
			break;
			
		case "B_End":
			
			waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectBEnd, 60);
			verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectBEnd,"Carrier Hotel Cross Connect B End");
		    click(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectBEnd,"Carrier Hotel Cross Connect B End");		        
		    Reusable.waitForpageloadmask();
		        
		    waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDown, 60);
		    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDown,"Supplier B End DropDown");
		    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDown,"Supplier B End DropDown");		        
		    Reusable.waitForpageloadmask();
		        
		    waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDownValue, 60);
		    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDownValue,"Supplier B End DropDownValue");
		    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDownValue,"Supplier B End DropDownValue");		        
		    Reusable.waitForpageloadmask();
		  		  	
			break;
        }        
        return "True";	
	}
	public String applicableFeatureSelection(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
	{
//      Initializing the Variable
		WebElement demarcationCheckBox=null;
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String PR_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PR_Type");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");			
		
		if(!Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{
			scrollUp();
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
			Reusable.waitForpageloadmask();
		}

		switch (SiteName) {
		case "A_End":
			if (Product_Name.equalsIgnoreCase("ColtIpAccess") || Product_Name.equalsIgnoreCase("ColtIpAccessDC")) {
			
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx, 60);
			verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");
			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx);
			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");		        
		    Reusable.waitForpageloadmask();
		        
		    waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipLongLiningAEndCbx, 60);
		    verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipLongLiningAEndCbx,"'Long Lining AEnd Check Box'");
		    ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipLongLiningAEndCbx);
		    click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipLongLiningAEndCbx,"'Long Lining AEnd AEnd Check Box'");		        
		    Reusable.waitForpageloadmask();
			}
		    else {
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationAEndCbx, 60);
			verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");
			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationAEndCbx);
			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");		        
		    Reusable.waitForpageloadmask();
		        
		    waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningAEndCbx, 60);
		    verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningAEndCbx,"'Long Lining AEnd Check Box'");
		    ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningAEndCbx);
		    click(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningAEndCbx,"'Long Lining AEnd AEnd Check Box'");		        
		    Reusable.waitForpageloadmask();
		    }    
		    if(PR_Type.equalsIgnoreCase("Fast Track")) 
		    {
		    	waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FastTractCbx, 60);
		        verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.FastTractCbx,"'Fast Track AEnd Check Box'");
		        scrollToViewNClick(CPQQuoteCreationObj.Onnet_DualEntry_Features.FastTractCbx,"'Fast Track Check Box'");			        
		    }		        
//		        	Below method is to unselect the demarcation when flow is onnet
		    if(PR_Type.equalsIgnoreCase("RejectedCost")&&Flow_Type.equalsIgnoreCase("ManualOffnet")) 
		    {
		        verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.DemarcationAEndCbx,"'Demarcation AEnd Check Box'");
		        click(CPQQuoteCreationObj.Onnet_DualEntry_Features.DemarcationAEndCbx,"'Demarcation AEnd Check Box'");		        			        	
		    }			
			break;
			
		case "B_End":						
			
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationBEndCbx, 60);
			verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationBEndCbx,"'Outside Business Hours Installation BEnd Check Box'");
			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationBEndCbx);
	        click(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationBEndCbx,"'Outside Business Hours Installation BEnd Check Box'");		        
	        Reusable.waitForpageloadmask();
	        
	        waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningBEndCbx, 60);
		  	verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningBEndCbx,"'Long Lining BEnd Check Box'");
		  	ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningBEndCbx);
	        click(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningBEndCbx,"'Long Lining BEnd AEnd Check Box'");		        
	        Reusable.waitForpageloadmask();		        
//		        	Below method is to unselect the demarcation when flow is onnet
		    if(PR_Type.equalsIgnoreCase("RejectedCost")&&Flow_Type.equalsIgnoreCase("ManualOffnet")) 
		    {
		    	verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.DemarcationBEndCbx,"'Demarcation BEnd Check Box'");
	        	click(CPQQuoteCreationObj.Onnet_DualEntry_Features.DemarcationBEndCbx,"'Demarcation BEnd Check Box'");	
		    }			
			break;
        }        
        return "True";
	}
	
	public String routerTypeConfiguration(String testDataFile,String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String sResult;
		String Router_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
		String BG4Feed_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BG4Feed_Type");
		String BG4Feed_Type_AS = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BG4Feed_Type_AS");
		String Presentation_Interface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPresentation_Type");
		String InterFace = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"InterFace");
		String Connector = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector");
		String IP_Addressing_Format = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Addressing_Format");
		String IP_Addressing_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Addressing_Type");
		String IP4V_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP4V_Address");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		String DataCenter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Data_Center");
		
		Reusable.waitForpageloadmask();
		scrollUp();
		//IPFeature
		//waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, 120);
		verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, "'Primary Features Link'");
		click(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk,"'Primary Features'");
		Reusable.waitForpageloadmask();
			
		waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 90);
		verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, "'Router Type Link'");
			//Update the product before Save
			
		if(Flow_Type.equalsIgnoreCase("AutomatedDSL")) 
		{
			isVisible(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId);
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button ID'");
			click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button ID'");
			Reusable.waitForpageloadmask();
			waitForElementToAppear(CPQQuoteCreationObj.SiteAddonsPage.IpSiteAddonsLnk, 120);
			return "True";
		}
		
		switch(Router_Type) {
		
		case "Customer Provided Router":
			//Router Type
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst,"'IP Router Type List'");
			click(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst,"'IP Router Type List'");
			System.out.println(Router_Type);
			Reusable.waitForpageloadmask();
			ClickonElementByString("//div[@data-value='"+Router_Type+"']", 20);
			//selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			Reusable.waitForpageloadmask();	
			break;
			
		case "Default Managed Router (no NAT Support)":
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			System.out.println(Router_Type);
			Reusable.waitForpageloadmask();
			
		/*	if (!(findWebElement(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx)).isSelected()) {
				System.out.println("test");
				ExtentTestManager.getTest().log(LogStatus.INFO,"NAT Checkbox is disabled in CPQ");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "NAT is Enabled in CPQ is not visible, Please verify");
				System.out.println("NAT is Enabled in CPQ is not visible, Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			} */
			break;
				
		case "Default Managed Router (Supports NAT)":
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			Reusable.waitForpageloadmask();
//				Check the NAT 
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			click(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			Reusable.waitForpageloadmask();
			break;
			
		case "Default Managed Router (Supports Full BGP / NAT)":
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			Reusable.waitForpageloadmask();
//				Check the NAT 
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			click(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			Reusable.waitForpageloadmask();
			break;
			
		case "Default Unmanaged Router (no NAT Support)":
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			Reusable.waitForpageloadmask();

			isChecked(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'NAT Checkbox'");
			break;
			
		case "Default Unmanaged Router (Supports NAT)":
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			Reusable.waitForpageloadmask();
//				Check the NAT 
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			click(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			Reusable.waitForpageloadmask();	
			break;
		
		case "Default Unmanaged Router (Supports Full BGP / NAT)":
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			Reusable.waitForpageloadmask();
//				Check the NAT 
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			click(CPQQuoteCreationObj.IPFeaturesPage.IpNATCbx,"'IP NAT Checkbox'");
			Reusable.waitForpageloadmask();	
			break;
				
		case "Specific Managed COLT Router": case "Specific Unmanaged COLT Router":
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpIPRouterTypeSubLst, Router_Type);
			Reusable.waitForpageloadmask();
				//ColtId
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpColtIdTbx, 30);
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpColtIdTbx,"'IP Colt ID Text Box'");
			sendKeys(CPQQuoteCreationObj.IPFeaturesPage.IpColtIdTbx, "C1234");
			Reusable.waitForpageloadmask();
								
				//Cost of CPE
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpColtCostTbx, 30);
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpColtIdTbx,"'IP Colt ID Text Box'");
			//clear(CPQQuoteCreationObj.IPFeaturesPage.IpColtCostTbx);
			Reusable.waitForpageloadmask();
			sendKeys(CPQQuoteCreationObj.IPFeaturesPage.IpColtCostTbx, "100","'IP Colt Cost Text field'");
			Reusable.waitForpageloadmask();
					
				//Router Model Name
			waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpColtRouterModelLst, 30);
			verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpColtRouterModelLst,"'IP Colt Router Mode Box'");
			sendKeys(CPQQuoteCreationObj.IPFeaturesPage.IpColtRouterModelLst, "Automate Router");
			Reusable.waitForpageloadmask();					
			break;
			}	
		if(Flow_Type.equalsIgnoreCase("DC"))
		{
		// BGP4
		waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpBGP4FeedCbx, 30);
		verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpBGP4FeedCbx,"'IP BGP4 Feed Check Box'");
		scrollToViewNClick(CPQQuoteCreationObj.IPFeaturesPage.IpBGP4FeedCbx,"'IP BGP4 Feed Check Box'");
		Reusable.waitForpageloadmask();

		selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpBGP4FeedTypeLst, CPQQuoteCreationObj.IPFeaturesPage.IpBGP4FeedTypeSubLst, BG4Feed_Type);
		Reusable.waitForpageloadmask();
		selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.CpeBGPTypeAsEndLst, CPQQuoteCreationObj.IPFeaturesPage.CpeBGPTypeAsEndSubLst, BG4Feed_Type_AS);
		Reusable.waitForpageloadmask();
		// SMTP
		waitForElementToAppear(CPQQuoteCreationObj.IPFeaturesPage.IpSMTPFeedCbx, 30);
		verifyExists(CPQQuoteCreationObj.IPFeaturesPage.IpSMTPFeedCbx,"'IP SMTP Feed Check Box'");
		click(CPQQuoteCreationObj.IPFeaturesPage.IpSMTPFeedCbx,"'IP SMTP Feed Check Box'");
		Reusable.waitForpageloadmask();
		}
				
		if (isVisible(CPQQuoteCreationObj.IPFeaturesPage.IpPresentationIntLst)){
			
			//click(CPQQuoteCreationObj.IPFeaturesPage.IpPresentationIntLst,"'IP Router Type List'");
			System.out.println(Presentation_Interface);
			//Reusable.waitForpageloadmask();
			//ClickonElementByString("//div[@data-value='"+Presentation_Interface+"']", 20);
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.IpPresentationIntLst, CPQQuoteCreationObj.IPFeaturesPage.IpPresentationIntSubLst, Presentation_Interface);
		}
		Reusable.waitForpageloadmask();	
		
		if (isVisible(CPQQuoteCreationObj.IPFeaturesPage.ipLANTypeLst)){
			Reusable.waitForpageloadmask();	
			selectByValueDIV(CPQQuoteCreationObj.IPFeaturesPage.ipLANTypeLst, CPQQuoteCreationObj.IPFeaturesPage.ipLANTypeSubLst, Presentation_Interface);
		}
		//isVisible(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId);
		//verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button ID'");
		//click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtnId,"'Next Button ID'");
		//Reusable.waitForpageloadmask();
		
		//waitForElementToAppear(CPQQuoteCreationObj.SiteAddonsPage.IpSiteAddonsLnk, 120);
		return "True";
	}

	
	public String offnetandNearNetConfigurationWave(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		String sResult;
		
		waitForElementToAppear(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk, 120);
		verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"'Site Details Link'");
		click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"'Site Details Link'");
		Reusable.waitForpageloadmask();
		
		switch (Flow_Type.toUpperCase()) {
				
			case "MANUALOFFNET":
				
//				Entering A-End details
				sResult = offnetEntriesWave(testDataFile, sheetName, scriptNo, dataSetNo, Flow_Type, "A_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				sResult = offnetEntriesWave(testDataFile, sheetName, scriptNo, dataSetNo, Flow_Type, "B_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				break;
				
			case "MANUALNEARNET":
				
//				Entering A-End details
				sResult = offnetEntriesWave(testDataFile, sheetName, scriptNo, dataSetNo, Flow_Type, "A_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				sResult = offnetEntriesWave(testDataFile, sheetName, scriptNo, dataSetNo, Flow_Type, "B_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				break;
				
			case "MANUALDIVERSE":
				
//				Entering A-End details
				ManualDiverseEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
				
//				Entering B-End details
				ManualDiverseEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");				
				break;
		}
		
		/*if(Flow_Type.toUpperCase().equalsIgnoreCase("MANUALOFFNET")) 
		{
			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveincrementalCapex,75);
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveincrementalCapex,"'Offnet Wave Incremental Capex'");
			scrollIntoView(findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveincrementalCapex));
			selectByValueDIV(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveincrementalCapex, CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveCapexSubLst, "Incremental");
			Reusable.waitForpageloadmask();
			
			selectByValueDIV(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveincrementalOpex, CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveOpexSubLst, "Incremental");
			Reusable.waitForpageloadmask();
			selectByValueDIV(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveFrequency, CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetWaveFrequencySubLst, "Monthly");
			Reusable.waitForpageloadmask();
		}*/
		return "True";
	}
	
	
	public String offnetEntriesWave(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String FlowType, String SiteName) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");
		String Connection_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connection_Type");
		String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;String exploreOptionsSubLsb=null;
		String offnetCheckRadioButton = null; String manualRadioButton = null;
		
		switch (SiteName) {
		
		case "A_End":
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckAEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetAManualOffnetRdB;
			break;
			
		case "B_End":
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckBEndBtn;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetBManualOffnetRdB;
			break;
		}
		waitForElementToAppear(manualEngagementBtn,75);
		verifyExists(manualEngagementBtn,"'Manual Engagement Button'");
		scrollToViewNClick(manualEngagementBtn,"'Manual Engagement Button'");
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(exploreOptionsLsb,75);
		verifyExists(exploreOptionsLsb,"'Explore Options List Box'");
		isEnable(exploreOptionsLsb);
		scrollToViewNClick(exploreOptionsLsb,"'Explore Options List Box'");
		Reusable.waitForpageloadmask();
		
		ClickonElementByString("//li[normalize-space(.)='"+Explore_Options+"']",30);
		switchToFrame("exploreEngagementComponent_oob");
			
		switch (FlowType.toUpperCase()) {
			
		case "MANUALOFFNET":
				waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,75);
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,"'Priority Text box'");
				sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb, Priority,"'Priority Text box'");
				keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,Keys.ENTER);
				Reusable.waitForpageloadmask();
				
				if (Connection_Type.equalsIgnoreCase("DualEntry")) 
				{ 
					verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx,"'Dual Entry check box'");
					click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx,"'Dual Entry check box'"); 
				}
				
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Get Quote Button'");
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Get Quote Button'");
				//WebInteractUtil.pause(4000);
				Reusable.waitForpageloadmask();
				Reusable.WaitforCPQloader();
				
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Arrow Explore Icon'");
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Arrow Explore Icon'");
				
				waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem, 60);
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,"'Request ID'");
						
				String fullVal = getTextFrom(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem);
				String[] bits = fullVal.split(":");
				String Request_ID = bits[bits.length-1].trim();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
				System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//					Passing this request ID to testdata sheet
				if(SiteName.equalsIgnoreCase("A_End"))
				{
					DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
				}
				else {
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID);
				}
				switchToDefaultFrame();
				
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");				
				Reusable.waitForpageloadmask();				
				
				waitForElementToAppear(offnetCheckRadioButton,60);
				verifyExists(offnetCheckRadioButton,"Access Type Radio Button");
				scrollToViewNClick(offnetCheckRadioButton,"Access Type Radio Button");
				//click(offnetCheckRadioButton,"Access Type Radio Button");
				Reusable.waitForpageloadmask();
				break;
				
			case "MANUALNEARNET":
		
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn,"'Explore Submit Button'");				
				Reusable.waitForpageloadmask();
				
				if(Product_Name.equalsIgnoreCase("ColtIpAccess") || Connection_Type.equalsIgnoreCase("New Building Dual Entry")) 
				{
					verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ConnectionType,"'Connection Type Text box'");
					sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ConnectionType, Connection_Type,"'Connection Type Text box'");
					findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ConnectionType).sendKeys("ENTER");	
					Reusable.waitForpageloadmask();
				}
					
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn,"'Explore Submit Button'");
				Reusable.waitForpageloadmask();
				Reusable.WaitforCPQloader();
				
				waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreRequestIDTdElem,75);
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreRequestIDTdElem,"'Explore Request ID Element'");
				
				String Request_ID1 = getTextFrom(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreRequestIDTdElem);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID1+" got Generated for the product "+Product_Name+" for the site "+sheetName);
				System.out.println("Request ID "+Request_ID1+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//				Passing this request ID to testdata sheet
				if(SiteName.equalsIgnoreCase("A_End"))
				{
					DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID1);
				}
				else {
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID1);
				}
				switchToDefaultFrame();
				
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");	
				Reusable.waitForpageloadmask();

				waitForElementToAppear(manualRadioButton, 60);
				scrollIntoView(findWebElement(manualRadioButton));
				verifyExists(manualRadioButton,"'Manual Radio Button'");
				click(manualRadioButton,"'Manual Radio Button'");
				Reusable.waitForpageloadmask();
				break;
			}
		return "True";
	}
	
	public String nearnetConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		String sResult;
		
		switch (Flow_Type.toUpperCase()) {
				
			case "MANUALNEARNET":
				
//				Entering A-End details
				sResult = nearnetEntries(testDataFile, sheetName,scriptNo, dataSetNo, "A_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					sResult = nearnetEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				}
				break;
				
			case "AUTOMATEDNEARNET":
//				Entering A-End details
				sResult = AutomatedEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
					sResult = AutomatedEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
					if (sResult.equalsIgnoreCase("False")){ return "False"; }
				}
				break;
		}			
		return "True";	
	}
	
	public void dslConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{
	
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		
		switch (Flow_Type.toUpperCase()) {
				
			case "MANUALDSL":
				
//				Entering A-End details
				dslEntries(testDataFile, sheetName, scriptNo, dataSetNo,  "A_End");
								
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					dslEntries(testDataFile, sheetName, scriptNo, dataSetNo,  "B_End");
					

				}
				break;
				
			case "AUTOMATEDDSL":
				
//				Entering A-End details

				automatedDslEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
								
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")) {
					automatedDslEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
					
				}
				break;
		}	
		
	}
	
	
	public String addHubrefernce(String Product_Name) throws IOException, InterruptedException 

	{
//		Initializing the Variable
		String product=Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		String HubId;
		switch(Product_Name) {
		
		case "EthernetSpoke":{

			HubId  = Reusable.WebTableCellAction("Product", product, "Hub ID","Store", null).trim();
			if(HubId.length()>0) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Hub ID is avaliable");
				

			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Hub Refrence ID is not avaliable, Please Verify");
				System.out.println("Hub Refrence ID is not avaliable, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));

			}
		}
		case "EthernetHub":{
			HubId  = Reusable.WebTableCellAction("Product", product, "Hub ID","Store", null).trim();
			if(HubId.length()>0) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Hub ID is avaliable");
				

				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Hub Refrence ID is not avaliable, Please Verify");
					System.out.println("Hub Refrence ID is not avaliable, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					
				}
			}
		}
		return "True";
	}
	
	

	
	
	//**********************Onnet Configuration *********************************************
	public String onnetConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
			
//			Initializing the Variable
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
			String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");			
			String BuildingStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BuildingStatus");
			switch (Flow_Type.toUpperCase())
			{
					
				case "ONNET": case "ONNETDUALENTRY":
					
					if(Product_Name.equalsIgnoreCase("ColtIpAccess")) {
						Reusable.waitForpageloadmask();
						verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.Accesstype,"'Access Type'");
						click(CPQQuoteCreationObj.L3RESILIENCEPage.Accesstype);
						Reusable.waitForpageloadmask();	
						
						if(BuildingStatus.equalsIgnoreCase("Yes")) {
							verifyExists(CPQQuoteCreationObj.ProductConfigration.IPBuildingStatus,"'Building Status'");
							String BuildingStatusAend = findWebElement(CPQQuoteCreationObj.ProductConfigration.IPBuildingStatus).getText();
							Assert.assertEquals(BuildingStatusAend, "IN Progress");
						}
						
					} else {
					
					waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.onnetAlnk, 20);
					verifyExists(CPQQuoteCreationObj.ProductConfigration.onnetAlnk,"'Onnet Check Button'");
					scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetAlnk);
					click(CPQQuoteCreationObj.ProductConfigration.onnetAlnk,"'Onnet Check Button'");			
					Reusable.waitForpageloadmask();
					
					scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetAendRadioBtn);
					click(CPQQuoteCreationObj.ProductConfigration.onnetAendRadioBtn,"'Onnet Check Button'");
					
					if(BuildingStatus.equalsIgnoreCase("Yes")) {
						verifyExists(CPQQuoteCreationObj.ProductConfigration.AendBuildingStatus,"'Building Status'");
						String BuildingStatusAend = findWebElement(CPQQuoteCreationObj.ProductConfigration.AendBuildingStatus).getText();
						Assert.assertEquals(BuildingStatusAend, "IN Progress");
					}
					
					}					
//					For EthernetLine
					if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave"))
					{
						waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.onnetBlnk, 20);
						verifyExists(CPQQuoteCreationObj.ProductConfigration.onnetBlnk,"'Onnet Check Button'");
						scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetBlnk);
						click(CPQQuoteCreationObj.ProductConfigration.onnetBlnk,"'Onnet Check Button'");			
						Reusable.waitForpageloadmask();
						
						scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetBendRadioBtn);
						click(CPQQuoteCreationObj.ProductConfigration.onnetBendRadioBtn,"'Onnet Check Button'");
						Reusable.waitForpageloadmask();
						
						if(BuildingStatus.equalsIgnoreCase("Yes")) {
							verifyExists(CPQQuoteCreationObj.ProductConfigration.BendBuildingStatus,"'Building Status'");
							String BuildingStatusAend = findWebElement(CPQQuoteCreationObj.ProductConfigration.BendBuildingStatus).getText();
							Assert.assertEquals(BuildingStatusAend, "IN Progress");
						}
															
					}
										
					break;
				}
			return "True";
			
		}
			
		//**********************AutomatedOffnet Configuration ********************************************************
		public void automatedOffnetEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException
			{
			
//			Initializing the Variable
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");	
			String offnetCheckBtn = null; String defOffnetRadioBtn = null; String exploreOptionsLsb = null;
			String defOffnetRadioBtn_IpAccess = null;
			
			switch (SiteName) {
			
			case "A_End":
				offnetCheckBtn = CPQQuoteCreationObj.ProductConfigration.offnetCheckAEndBtn;
				defOffnetRadioBtn = CPQQuoteCreationObj.ProductConfigration.automatedOffnetDefAEndRadioBtn;
				defOffnetRadioBtn_IpAccess = CPQQuoteCreationObj.ProductConfigration.automatedOffnetDefRadioBtn;
				break;
				
			case "B_End":
				offnetCheckBtn = CPQQuoteCreationObj.ProductConfigration.offnetCheckBEndBtn;
				defOffnetRadioBtn = CPQQuoteCreationObj.ProductConfigration.automatedOffnetDefBEndRadioBtn;
				break;
			}
			
			if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
					Product_Name.equalsIgnoreCase("EthernetSpoke")&&!Product_Name.equalsIgnoreCase("ColtIpAccess")
					&&!Product_Name.equalsIgnoreCase("VPNNetwork")) 
			{
				waitForElementToAppear(offnetCheckBtn, 20);
				verifyExists(offnetCheckBtn,"'Offnet Check Button'");
				scrollDown(offnetCheckBtn);
				click(offnetCheckBtn,"'Offnet Check Button'");			
				Reusable.waitForpageloadmask();
				
				click(defOffnetRadioBtn,"'defOffnet Check Button'");			
				Reusable.waitForpageloadmask();
				
			}	
				
			if (Product_Name.equalsIgnoreCase("ColtIpAccess"))
			{
			offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipoffnetcheckbtn;
			
			waitForElementToAppear(offnetCheckBtn,75);
			verifyExists(offnetCheckBtn,"'Offnet Check Button'");
			scrollDown(offnetCheckBtn);
			click(offnetCheckBtn,"'Offnet Check Button'");			
			Reusable.waitForpageloadmask();
			
			
			click(defOffnetRadioBtn_IpAccess,"'defOffnet Check Button'");			
			Reusable.waitForpageloadmask();
	    	}	
			
			
			if (Product_Name.equalsIgnoreCase("VPNNetwork")) 
			{
				offnetCheckBtn = CPQQuoteCreationObj.ProductConfigration.vpnOffNetCheckBtn;
				defOffnetRadioBtn = CPQQuoteCreationObj.ProductConfigration.vpnOffnetRadioBtn;
				
				waitForElementToAppear(offnetCheckBtn,75);
				verifyExists(offnetCheckBtn,"'Offnet Check Button'");
				scrollDown(offnetCheckBtn);
				click(offnetCheckBtn,"'Offnet Check Button'");			
				Reusable.waitForpageloadmask();
				
				click(defOffnetRadioBtn,"'defOffnet Check Button'");			
				Reusable.waitForpageloadmask();
			}				
		
		}
						
			//**********************AutomatedDSL Configuration ********************************************************	
		public void automatedDslEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException {
			{
			
//				Initializing the Variable
				String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");		
				String dslCheckBtn = null; String defDSLRadioBtn = null; String exploreOptionsLsb = null;
				
				switch (SiteName) {
				
				case "A_End":
					dslCheckBtn = CPQQuoteCreationObj.ProductConfigration.dslCheckAEndBtn;
					defDSLRadioBtn = CPQQuoteCreationObj.ProductConfigration.automatedDSLDefAEndRadioBtn;
					break;
					
				case "B_End":
					dslCheckBtn = CPQQuoteCreationObj.ProductConfigration.dslCheckBEndBtn;
					defDSLRadioBtn = CPQQuoteCreationObj.ProductConfigration.automatedDSLDefBEndRadioBtn;
					break;
				}
				
				if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
						Product_Name.equalsIgnoreCase("EthernetSpoke")||!Product_Name.equalsIgnoreCase("ColtIpAccess"))
				
				{
					waitForElementToAppear(dslCheckBtn, 20);
					verifyExists(dslCheckBtn,"'DSL Check Button'");
					scrollDown(dslCheckBtn);
					click(dslCheckBtn,"'DSL Check Button'");			
					Reusable.waitForpageloadmask();
					
					waitForElementToAppear(defDSLRadioBtn, 30);
					click(defDSLRadioBtn,"'defDSL Check Button'");			
					Reusable.waitForpageloadmask();
					
				}

				if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
				{
					dslCheckBtn = CPQQuoteCreationObj.ProductConfigration.dslCheckAEndBtn;
					defDSLRadioBtn = CPQQuoteCreationObj.ProductConfigration.automatedDSLDefAEndRadioBtn;
					
					waitForElementToAppear(dslCheckBtn,75);
					verifyExists(dslCheckBtn,"'ipDSL Check Button'");
					scrollDown(dslCheckBtn);
					click(dslCheckBtn,"'ipDSL Check Button'");			
					Reusable.waitForpageloadmask();
					
					waitForElementToAppear(defDSLRadioBtn,30);
					click(defDSLRadioBtn,"'defDSL Check Button'");			
					Reusable.waitForpageloadmask();
				}				
				
				}
						
			}
			
	//**********************AutomatedNearnet Configuration ************************************************************
		
		public String AutomatedEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException {
					
//			Initializing the Variable
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");	
			String AutomatedRadioButton=null; String AutomatedRadioButton_ipaccess=null;
			
			switch (SiteName) {
			
			case "A_End":
				AutomatedRadioButton=CPQQuoteCreationObj.ProductConfigration.etherNetAutoNearnetARdBtn;
				AutomatedRadioButton_ipaccess=CPQQuoteCreationObj.ProductConfigration.ipaccessAutoNearnetRdBtn;
				break;
				
			case "B_End":
				AutomatedRadioButton=CPQQuoteCreationObj.ProductConfigration.etherNetAutoNearnetBRdBtn;
				break;
			}
			if(Product_Name.contains("Ethernet")||Product_Name.equalsIgnoreCase("Wave")) 
			{
				waitForElementToAppear(AutomatedRadioButton,75);
				verifyExists(AutomatedRadioButton,"automated Radio Button");
				scrollDown(AutomatedRadioButton);
				click(AutomatedRadioButton,"automated Radio Button");			
				Reusable.waitForpageloadmask();
				
			}
			else if(Product_Name.equalsIgnoreCase("ColtIpAccess"))
			{
				waitForElementToAppear(AutomatedRadioButton_ipaccess,75);
				verifyExists(AutomatedRadioButton_ipaccess,"IpAccess automated Radio Button");
				scrollDown(AutomatedRadioButton_ipaccess);
				click(AutomatedRadioButton_ipaccess,"IpAccess automated Radio Button");			
				Reusable.waitForpageloadmask();
			}
			return "True";
		}
		
		//**********************OnnetDualEntries Configuration ************************************************************
		
		public void onnetDualEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
		{
 
			
//			Initializing the Variable
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");	
			String OnnetmanualEngagementBtn = CPQQuoteCreationObj.ProductConfigration.onnetManualRequestLst; 
			String OnnetexploreOptionsLsb = CPQQuoteCreationObj.ProductConfigration.onnetManualRequestSubLst;	
			String Explore_Options = null; String manualRadioButton=null;
						
			switch(SiteName) {
			
			case "A_End":
				Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");
				manualRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetDualEntryARdBtn;
				break;
			case "B_End":
				Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Explore_Options");
				manualRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetDualEntryBRdBtn;
			
			}
				
			switch(Product_Name) {
			
			case "EthernetLine": case "EthernetHub": case "EthernetSpoke": case "Wave": case "ColtIpAccess":
				
				waitForElementToAppear(OnnetmanualEngagementBtn, 20);
				verifyExists(OnnetmanualEngagementBtn, "'Manual Onnet Button'");			
				Reusable.waitForpageloadmask();
				scrollIntoView(findWebElement(OnnetmanualEngagementBtn));
							
				//sendKeys(CPQQuoteCreationObj.ProductConfigration.onnetManualRequestLst,Explore_Options,"'DualEntries List'");
				click(OnnetmanualEngagementBtn,"Manual Request ListBox");
				ClickonElementByString("//div[@aria-label='"+Explore_Options+"']", 20);
				//selectByValueDIV(OnnetmanualEngagementBtn, OnnetexploreOptionsLsb, Explore_Options);
				Reusable.waitForpageloadmask();
				switchToFrame("exploreEngagementComponent_features_oob");
				Reusable.waitForpageloadmask();
				break;
														
								
			case "VPNNetwork":
				
				waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.vpnDualEntryLink, 20);
				verifyExists(CPQQuoteCreationObj.ProductConfigration.vpnDualEntryLink, "'vpn Dual Entry Link'");		
				
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.ProductConfigration.vpnDualEntryLink,"'vpn Dual Entry Link'");
				Reusable.waitForpageloadmask();
				ClickonElementByString("//li[normalize-space(.)='"+Explore_Options+"']", 30);
				Reusable.waitForpageloadmask();
				break;
			}
				waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn, 75);
				Reusable.waitForpageloadmask();
				String buliding_number=findWebElement(CPQQuoteCreationObj.ProductConfigration.exploreNearnetBuldingNumberTxb).getAttribute("value");
				
				ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
				System.out.println("Explore Buliding Number is "+buliding_number);
				//Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreSubmitBtn,"Explore Submit Button");
				Reusable.waitForpageloadmask();
								
				waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.exploreRequestIDTdElem, 60);
				String Request_ID = findWebElement(CPQQuoteCreationObj.ProductConfigration.exploreRequestIDTdElem).getText().trim();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
				System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
				if(SiteName.equalsIgnoreCase("A_End"))
				{
					DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
				}
				else
				{
					DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID);
				}	
				switchToDefaultFrame();
					
				if(Product_Name.equalsIgnoreCase("VPNNetwork")) {
					click(CPQQuoteCreationObj.ProductConfigration.vpnExploreClsBtn);
					Reusable.waitForpageloadmask();
				} else {
					click(CPQQuoteCreationObj.ProductConfigration.exploreCloseBtn);
					Reusable.waitForpageloadmask();
				}	
					
				Reusable.waitForpageloadmask();
				
				if(Product_Name.contains("Ethernet")) 
				{
					waitForElementToAppear(manualRadioButton, 60);
					scrollIntoView(findWebElement(manualRadioButton));
					verifyExists(manualRadioButton,"'Manual Radio Button'");
					click(manualRadioButton,"'Manual Radio Button'");
					Reusable.waitForpageloadmask();
				}
				
					
		}
		
		//**********************product Configuration************************************************************	
		public String productConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
        {       
            String sResult;
            String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
            String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
            String Discount_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Discount_Applicable");
            String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPAccess_Type");
            String Diversity_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Applicable");
            String SiteNameAlias_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteNameAlias_Applicable");
            String Bespoke_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Applicable");
            String NetworkEncryption_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NetworkEncryption_Applicable");
            String CustomerDefRoute_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CustomerDefRoute_Applicable");
            String BuildingStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BuildingStatus");
            String OptionQuote_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OptionQuote_Applicable");
            
            if(IPAccess_Type.equalsIgnoreCase("New")) {
                Product_Name="ColtIpAccess";
            }
           
//            calling the below entry to enter the site details
            siteDetailEntry(testDataFile, sheetName, scriptNo, dataSetNo);           
                       
            switch (Flow_Type.toUpperCase())
            {   
                case "MANUALOFFNET": case "AUTOMATEDOFFNET":
                    offnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
                     break;
                   
                case "MANUALNEARNET":case "AUTOMATEDNEARNET":
                    nearnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
                    break;
                   
                case "MANUALDSL": case "AUTOMATEDDSL":
                    dslConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);                   
                    break;
                   
                case "MANUALOLODSL":
                    manualOLODSLConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
                    break;
                   
                case "ONNETDUALENTRY":
                    onnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
                      

                    if(Product_Name.equalsIgnoreCase("VPNNetwork"))
                    {
                        waitForElementToAppear(CPQQuoteCreationObj.VPNDualEntry.VPNAddONLink, 20);
                        verifyExists(CPQQuoteCreationObj.VPNDualEntry.VPNAddONLink,"'VPN DualEntry Link'");                   
                        click(CPQQuoteCreationObj.VPNDualEntry.VPNAddONLink,"'VPN DualEntry Link'");
                        Reusable.waitForpageloadmask();
                    }
                    scrollUp();
        			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
        			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
        			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
        			Reusable.waitForpageloadmask();
        			
                    onnetDualEntryConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
                    
                    verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
        			scrollUp();
        			click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
        			Reusable.waitForpageloadmask();
                  
                    break;
                case "ONNET":

                       onnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);

                    break;
                    
				case "REUSEOLO": case "REUSEBCP":
	            		ReuseManualConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	            		break;
				
                    
                case "MANUALDIVERSE":
                	scrollUp();
        			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
        			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
        			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
        			Reusable.waitForpageloadmask();
        			
        			selectProductFeaturesDiversity(testDataFile, sheetName, scriptNo, dataSetNo);
        			
        			verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
        			scrollUp();
        			click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
        			Reusable.waitForpageloadmask();
        			        			
        			if(!Product_Name.equalsIgnoreCase("Wave")) {
                	diverseConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
                	
                	scrollUp();
        			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
        			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
        			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
        			Reusable.waitForpageloadmask();
        			}
                	break;
                	                	
            }
            
//          Calling the below method if the ULL Fibre
          if (Flow_Type.equalsIgnoreCase("ULLFIBRE")) {
          ULLFibreConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
             
          }
           
            if(Product_Name.equalsIgnoreCase("ColtIpAccess")||Flow_Type.equalsIgnoreCase("DC")) {
                routerTypeConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
               
            }
           
//            Calling the below method if the discounts are applicable
            if (Discount_Applicable.equalsIgnoreCase("Yes")) {
              selectCPQProductFeatures(testDataFile, sheetName, scriptNo, dataSetNo);
              
            }
			
			if(Flow_Type.equalsIgnoreCase("DC")){
				ipAccessservicefeatures(testDataFile,sheetName, scriptNo, dataSetNo);
			}
           
//          calling the below method to select the features
            if(!Flow_Type.equalsIgnoreCase("AutomatedOffnet")&&!Flow_Type.equalsIgnoreCase("ManualOffnet")&&!Flow_Type.equalsIgnoreCase("ManualOLODSL")&&!Flow_Type.equalsIgnoreCase("ManualDiverse")) {
               selectProductFeatures(testDataFile, sheetName, scriptNo, dataSetNo);
             
            }
           
                  
//            Calling the below method if the discounts are applicable
            if (Diversity_Applicable.equalsIgnoreCase("Yes")) {
            selectProductFeaturesDiversity(testDataFile, sheetName, scriptNo, dataSetNo);
               
            }
           
//            Calling the below method if the discounts are applicable
           if (SiteNameAlias_Applicable.equalsIgnoreCase("Yes")) {
            selectProductFeaturesSiteNameAlias(testDataFile, sheetName, scriptNo, dataSetNo);
               
            }
           
//            Calling the below method if the discounts are applicable
            if (Bespoke_Applicable.equalsIgnoreCase("Yes")) {
            selectProductFeaturesBespoke(testDataFile, sheetName, scriptNo, dataSetNo);
               
            }
            
//          Calling the below method if the Customer Defined Route are applicable
            if (CustomerDefRoute_Applicable.equalsIgnoreCase("Yes")) {
            selectProductFeaturesCustomerDefRoute(testDataFile, sheetName, scriptNo, dataSetNo);
               
            }
           
//            Calling the below method if the discounts are applicable
            if (NetworkEncryption_Applicable.equalsIgnoreCase("Yes")) {
            selectProductFeaturesNetworkEncryption(testDataFile, sheetName, scriptNo, dataSetNo);
               
            }
                      
//            calling the below entry to add additional product data entries
            addtionalProductdata(testDataFile, sheetName, scriptNo, dataSetNo);
                       
//          Saving the Product Entries 
            if((Product_Name.equalsIgnoreCase("Wave")&& Flow_Type.equalsIgnoreCase("ManualOffnet"))||(Product_Name.equalsIgnoreCase("Wave")&& Flow_Type.equalsIgnoreCase("ManualDiverse"))
            		||(Product_Name.equalsIgnoreCase("Wave")&& Flow_Type.equalsIgnoreCase("ManualNearnet"))) { 
            	markPartialSave(Product_Name); 
            }   
            else if(BuildingStatus.equalsIgnoreCase("Yes")) {
            	updateSaveProductCPQ(testDataFile, sheetName, scriptNo, dataSetNo, "SaveToQuote");
            }
            else {             	
            	updateSaveProductCPQ("SaveToQuote"); 
            }  
            
            if (OptionQuote_Applicable.equalsIgnoreCase("Yes")) {
                
                Reusable.waitForpageloadmask();
                Reusable.waitForpageloadmask();
               
                waitForElementToBeVisible(CPQQuoteCreationObj.OptionQuote.optionQuoteRBtn, 60);
                ScrollIntoViewByString(CPQQuoteCreationObj.OptionQuote.optionQuoteRBtn);   
                click(CPQQuoteCreationObj.OptionQuote.optionQuoteRBtn, "Option Quote Button");   
                   Reusable.waitForpageloadmask();
                
                }
           
            return "True";
            
        }

		//*************************IP Access IP type validation********************
		public void ipAccessservicefeatures(String file_name,String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
		{
			String IPAddressFormat1 = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Address_Format1");
			String IPAddressFormat2 = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"IP_Address_Format2");
			String VoIPReference = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"VoIP_Reference");
			
			scrollUp();
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
			click(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv4Addressingformat,"IPv4 Address Format");
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv4Addressingtype,"IPv4 Address Type");
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv4NumberofPAaddress,"IPv4 PA Address");
			
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv4Addressingdropdown,"IPv4 Address Dropdown");
			click(CPQQuoteCreationObj.ServiceFeatures.Ipv4Addressingdropdown,"IPv4 Address Dropdown");
			Reusable.waitForpageloadmask();
			ClickonElementByString("//div[@data-value='"+IPAddressFormat1+"']", 20);
			
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv6Addressingtype,"IPv6 Address Type");
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv6NumberofPAaddress,"IPv6 PA Address");
			
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv4Addressingdropdown,"IPv4 Address Dropdown");
			click(CPQQuoteCreationObj.ServiceFeatures.Ipv4Addressingdropdown,"IPv4 Address Dropdown");
			Reusable.waitForpageloadmask();
			ClickonElementByString("//div[@data-value='"+IPAddressFormat2+"']", 20);
			Reusable.waitForpageloadmask();
			
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv4Addressingtype,"IPv4 Address Type");
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv4NumberofPAaddress,"IPv4 PA Address");
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv6Addressingtype,"IPv6 Address Type");
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.Ipv6NumberofPAaddress,"IPv6 PA Address");
			
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.performanceReporting,"Performance Reporting");
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.proactiveManagement,"proactive Management");
			
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.voiceBundleService,"voice Bundle Service");
			click(CPQQuoteCreationObj.ServiceFeatures.voiceBundleService,"voice Bundle Service");
			
			verifyExists(CPQQuoteCreationObj.ServiceFeatures.voipRefNumService,"voip RefNum Service");
			sendKeys(CPQQuoteCreationObj.ServiceFeatures.voipRefNumService,VoIPReference);
			
		}
		
		
		
		
		//********************REUSE MANUAL REQUESTS***************************
		public String ReuseManualConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
			String AExplore_ID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Request_ID");
			String XExplore_ID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Additional_Request_ID");
			String BExplore_ID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Request_ID");
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
			String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
			String ReuseType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Reuse_Type");
			
			//String Explore_ID="43396";
			String ReuseManualButtonAend=null; String ReuseManualButtonBend=null;String ExploreIDtxt=null; String exploreSearchbtn=null;
			String exploreResultsel=null; String ExploreResuse=null; String VerifyResuseAend=null; String ResuseAccessTypeAend=null;
			String VerifyResuseBend=null; String ResuseAccessTypeBend=null; String ReuseManualOLOchbox=null;
			String ReuseManualerrormessage=null;
			
			ReuseManualButtonAend = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ReuseManualbtnAend;
			ReuseManualButtonBend = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ReuseManualbtnBend;
			ExploreIDtxt = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Exploreidtxt;
			exploreSearchbtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Exploresearchbtn;
			exploreResultsel = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Exploreresultsel;
			ExploreResuse = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Explorereusebtn;
			
			if((Flow_Type.equalsIgnoreCase("ReuseBCP")) || (ReuseType.equalsIgnoreCase("ReuseBCP"))){ 
				if(Product_Name.equalsIgnoreCase("ColtIpAccess")){
					ResuseAccessTypeAend = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ipBCPReuseAend;
				} else {
					ResuseAccessTypeAend = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.BCPReuseAend;
					ResuseAccessTypeBend = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.BCPReuseBend; 
				}
			} else {
				ResuseAccessTypeAend = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualReuseAccesstypAend;
				ResuseAccessTypeBend = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualReuseAccesstypBend;
			}
					ReuseManualOLOchbox = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualReuseOLO;
			ReuseManualerrormessage = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.errorMessage;
			
			waitForElementToAppear(ReuseManualButtonAend, 60);
			scrollIntoView(findWebElement(ReuseManualButtonAend));
			verifyExists(ReuseManualButtonAend,"'Reuse Manual Button'");
			click(ReuseManualButtonAend,"'Reuse Manual Button'");
			Reusable.waitForpageloadmask();
			
			webDriver.switchTo().frame("reuseRequestComponent");
			
			if((Flow_Type.equalsIgnoreCase("ReuseOLO")) || (ReuseType.equalsIgnoreCase("ReuseOLO"))){
				waitForElementToAppear(ReuseManualOLOchbox, 60);
				scrollIntoView(findWebElement(ReuseManualOLOchbox));
				verifyExists(ReuseManualOLOchbox,"'Reuse Manual OLO'");
				click(ReuseManualOLOchbox,"'Reuse Manual OLO'");
				Reusable.waitForpageloadmask();
			}
			
			waitForElementToAppear(ExploreIDtxt, 60);
			scrollIntoView(findWebElement(ExploreIDtxt));
			verifyExists(ExploreIDtxt,"'Reuse Manual Button'");
			sendKeys(ExploreIDtxt, AExplore_ID);
			Reusable.waitForpageloadmask();
			
			verifyExists(exploreSearchbtn,"'Search Button'");
			click(exploreSearchbtn,"'Search Button'");
			Reusable.waitForpageloadmask();
			
			/*if (verifyExists(ReuseManualerrormessage, "Error Message")){
				waitForElementToAppear(ExploreIDtxt, 60);
				scrollIntoView(findWebElement(ExploreIDtxt));
				verifyExists(ExploreIDtxt,"'Reuse Manual Button'");
				sendKeys(ExploreIDtxt, AExplore_ID);
				Reusable.waitForpageloadmask();
				
				verifyExists(exploreSearchbtn,"'Search Button'");
				click(exploreSearchbtn,"'Search Button'");
				Reusable.waitForpageloadmask();
			
			} */
					
			waitForElementToAppear(exploreResultsel, 60);
			scrollIntoView(findWebElement(exploreResultsel));
			verifyExists(exploreResultsel,"'Select Explore Results'");
			click(exploreResultsel,"'Select Explore Results'");
			Reusable.waitForpageloadmask();
			
			switchToDefaultFrame();
			
			waitForElementToAppear(ExploreResuse, 60);
			scrollIntoView(findWebElement(ExploreResuse));
			verifyExists(ExploreResuse,"'Select & Close'");
			click(ExploreResuse,"'Select & Close'");
			Reusable.waitForpageloadmask();
			
			
			if(Product_Name.equalsIgnoreCase("ColtIpAccess")){
				click(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryConnectionLbl,"Primary Connection Link");
				}
				else {
					refreshPage();
					Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.PrimaryConnectionPage.siteDetailsLnk,"Site Details Link");
				};
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(ResuseAccessTypeAend, 60);
			scrollIntoView(findWebElement(ResuseAccessTypeAend));
			verifyExists(ResuseAccessTypeAend,"'Resuse Access Type'");
			click(ResuseAccessTypeAend,"'Resuse Access Type'");
			Reusable.waitForpageloadmask();
			Reusable.WaitforProdConfigLoader();
			
			if((Product_Name.equalsIgnoreCase("EthernetLine")) || (Product_Name.equalsIgnoreCase("Wave"))){
				refreshPage();
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.PrimaryConnectionPage.siteDetailsLnk,"Site Details Link");
				Reusable.waitForpageloadmask();
				waitForElementToAppear(ReuseManualButtonBend, 60);
				scrollIntoView(findWebElement(ReuseManualButtonBend));
				verifyExists(ReuseManualButtonBend,"'Reuse Manual Button'");
				click(ReuseManualButtonBend,"'Reuse Manual Button'");
				Reusable.waitForpageloadmask();
				Reusable.WaitforProdConfigLoader();
				
				webDriver.switchTo().frame("reuseRequestComponent");
				
				if((Flow_Type.equalsIgnoreCase("ReuseOLO")) || (ReuseType.equalsIgnoreCase("ReuseOLO"))){
					waitForElementToAppear(ReuseManualOLOchbox, 60);
					scrollIntoView(findWebElement(ReuseManualOLOchbox));
					verifyExists(ReuseManualOLOchbox,"'Reuse Manual OLO'");
					click(ReuseManualOLOchbox,"'Reuse Manual OLO'");
					Reusable.waitForpageloadmask();
				}
				
				waitForElementToAppear(ExploreIDtxt, 60);
				scrollIntoView(findWebElement(ExploreIDtxt));
				verifyExists(ExploreIDtxt,"'Reuse Manual Button'");
				sendKeys(ExploreIDtxt, BExplore_ID);
				Reusable.waitForpageloadmask();
				
				verifyExists(exploreSearchbtn,"'Search Button'");
				click(exploreSearchbtn,"'Search Button'");
				Reusable.waitForpageloadmask();
						
				waitForElementToAppear(exploreResultsel, 60);
				scrollIntoView(findWebElement(exploreResultsel));
				verifyExists(exploreResultsel,"'Select Explore Results'");
				click(exploreResultsel,"'Select Explore Results'");
				Reusable.waitForpageloadmask();
				
				switchToDefaultFrame();
				
				waitForElementToAppear(ExploreResuse, 60);
				scrollIntoView(findWebElement(ExploreResuse));
				verifyExists(ExploreResuse,"'Select & Close'");
				click(ExploreResuse,"'Select & Close'");
				Reusable.waitForpageloadmask();
				
				refreshPage();
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.PrimaryConnectionPage.siteDetailsLnk,"Site Details Link");
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(ResuseAccessTypeBend, 60);
				scrollIntoView(findWebElement(ResuseAccessTypeBend));
				verifyExists(ResuseAccessTypeBend,"'Resuse Access Type'");
				click(ResuseAccessTypeBend,"'Resuse Access Type'");
				Reusable.waitForpageloadmask();						
				
			}
			
			return "True";
		}


	//*********************Product Configuration for CTLP Flow******************************
		public String productConfigurationForCTLPFlow(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	    {        
			String sResult;
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
	        String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
	        String Discount_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Discount_Applicable");
	        String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPAccess_Type");
	        if(IPAccess_Type.equalsIgnoreCase("New")) {
	            Product_Name="ColtIpAccess";
	        }
	        
//	        calling the below entry to enter the site details
	        siteDetailEntry(testDataFile, sheetName, scriptNo, dataSetNo);	        
	        	        
	        switch (Flow_Type.toUpperCase()) 
	        {    
	            case "MANUALOFFNET": case "AUTOMATEDOFFNET":
	                offnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	                 break;
	                
	            case "MANUALNEARNET":case "AUTOMATEDNEARNET":
	                nearnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	                break;
	                
	            case "MANUALDSL": case "AUTOMATEDDSL":
	                dslConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);	                
	                break;
	                
	            case "MANUALOLODSL":
	                manualOLODSLConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	                break;
	                
	            case "ONNETDUALENTRY":
	                if(Product_Name.contains("Ethernet")) 
	                {
	                    onnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	                   
	                }
	                
	                if(Product_Name.equalsIgnoreCase("VPNNetwork"))
	                {
	                    waitForElementToAppear(CPQQuoteCreationObj.VPNDualEntry.VPNAddONLink, 20);
	                    verifyExists(CPQQuoteCreationObj.VPNDualEntry.VPNAddONLink,"'VPN DualEntry Link'");                    
	                    click(CPQQuoteCreationObj.VPNDualEntry.VPNAddONLink,"'VPN DualEntry Link'");
	                    Reusable.waitForpageloadmask();
	                } 
	                onnetDualEntryConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	               
	                break;
	            case "ONNET":
	                if(Product_Name.contains("Ethernet")|| (Product_Name.contains("Wave"))) {
	                   onnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	                   
	                }
	                break;
	        }
	        
	        if(Product_Name.equalsIgnoreCase("ColtIpAccess")) {
	            routerTypeConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	            
	        }
	        
//	        Calling the below method if the discounts are applicable
	        if (Discount_Applicable.equalsIgnoreCase("Yes")) {
	          selectCPQProductFeatures(testDataFile, sheetName, scriptNo, dataSetNo);
	            
	        }
	        
//	      calling the below method to select the features
	           selectCrossConnectFeature(testDataFile, sheetName, scriptNo, dataSetNo);
		        
//	           calling the below entry to add additional product data entries
	            addtionalProductdata(testDataFile, sheetName, scriptNo, dataSetNo);
	        
	        //Validate all data
	        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
	        click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
	    	Reusable.waitForpageloadmask();
	        
	        return "True";
	        
	    }
			//**********************Offnet Configuration ************************************************************
			
			public void offnetConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
		
				
//				Initializing the Variable
				String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		        String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
				
				
				switch (Flow_Type.toUpperCase()) {
						
					case "MANUALOFFNET": case "MANUALDSL":
						
//						Entering A-End details
						offnetEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");												
//						Entering B-End details
						if (Product_Name.equalsIgnoreCase("EthernetLine")) {
							offnetEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
							
						}
						break;
						
					case "AUTOMATEDOFFNET":
						
//						Entering A-End details
						automatedOffnetEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
												
//						Entering B-End details
						if (Product_Name.equalsIgnoreCase("EthernetLine")) {
							automatedOffnetEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
							
						}
						break;
				}
												
			}
			
			
			//**********************onnet DualEntry Configuration ************************************************************
			
			public void onnetDualEntryConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	
				
//				Initializing the Variable
				String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		        String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
				String sResult;
								
//				Entering A-End details
				onnetDualEntries(testDataFile, sheetName, scriptNo, dataSetNo,"A_End");
								
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
					onnetDualEntries(testDataFile, sheetName, scriptNo, dataSetNo,"B_End");
					}								
				
			}
			
		public String clickProductConfigurationBtn(String sProduct_Name) throws IOException, InterruptedException
		{
			String sResult, Product_Name;
			String sProduct = null;
			if (sProduct_Name.contains("Data -")||sProduct_Name.contains("Voice -")){ Product_Name = sProduct_Name; }
			else if(sProduct_Name.equalsIgnoreCase("VPNNetwork")) {Product_Name="VPN Network";}
			else if(sProduct_Name.equalsIgnoreCase("ColtIpDomain")) {Product_Name="IP Domain";}
			else if(sProduct_Name.equalsIgnoreCase("ColtIpGuardian")) {Product_Name="IP Guardian";}
			else if(sProduct_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {Product_Name="IP Managed Virtual Firewall";}
			else if(sProduct_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {Product_Name="IP Managed Dedicated Firewall";}
			else if(sProduct_Name.equalsIgnoreCase("CPESolutionsSite")) {Product_Name="CPE Solutions Site";}
			else { Product_Name = sProduct_Name.replaceAll("(?!^)([A-Z])", " $1"); }
			
			sResult = Reusable.WebTableCellAction("Product", Product_Name, null,"Click", null);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			// clicking on Reconfiguration link
			verifyExists(engagementpf.reConfigureBtn,"ReConfigure Button");
			Reusable.waitForpageloadmask();
			click(engagementpf.reConfigureBtn,"ReConfigure Button");
			Reusable.waitForpageloadmask();
			if (sProduct_Name.contains("Ethernet")||sProduct_Name.contains("Wave")) {
			waitForElementToAppear(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk, 120);
			verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
			Reusable.waitForpageloadmask();
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitForpageloadmask();
			click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
			Reusable.waitForpageloadmask();

			 }
			else if(sProduct_Name.equals("ColtIpAccess")) {
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryAddressHdr, 120);
			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryConnectionLbl, "Primary Connection Tab");
			click(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryConnectionLbl, "Primary Connection Tab");;
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
			//waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, 50);
			//verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn,"IPAccess Next Button");
			//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn,"IPAccess Next Button");
			//for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask();}
			}
			else if(sProduct_Name.equals("Colt Ip Domain")||sProduct_Name.equals("Colt Ip Guardian")
			||sProduct_Name.equals("Colt Managed Virtual Firewall")||sProduct_Name.equals("IP Managed Dedicated Firewall")) {
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask();}
			}

			 return "True";
			}

		public String searchQuoteC4C(String Quote_ID) throws IOException, InterruptedException 
		{
						
			Reusable.WaitforC4Cloader();
			waitForElementToAppear(C4CEngagementSalesObj.salespage.searchIcon,60);
			verifyExists(C4CEngagementSalesObj.salespage.searchIcon,"SearchIcon");
			Reusable.Waittilljquesryupdated();
			Reusable.WaitforC4Cloader();
			Reusable.Waittilljquesryupdated();
			//WebInteractUtil.pause(1000);
			scrollIntoView(findWebElement(C4CEngagementSalesObj.salespage.searchIcon));
			click(C4CEngagementSalesObj.salespage.searchIcon,"SearchIcon");
			
			waitForElementToAppear(C4CEngagementSalesObj.salespage.searchTxb,60);
			verifyExists(C4CEngagementSalesObj.salespage.searchTxb,"search Txb");
			sendKeys(C4CEngagementSalesObj.salespage.searchTxb, Quote_ID);
			click(C4CEngagementSalesObj.salespage.searchBtn,"Search Button");
			Reusable.WaitforC4Cloader();
			WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
			Reusable.Waittilljquesryupdated();
//					if (Environment.equalsIgnoreCase("Test2")) { WebInteractUtil.scrollIntoView(CPQ_Objects.defaultCkb); }
			driver.findElement(By.xpath("//descendant::a[text()='"+Quote_ID+"']")).isEnabled();
			driver.findElement(By.xpath("//descendant::a[text()='"+Quote_ID+"']")).click();
			//WebInteractUtil.pause(1000);
			Reusable.WaitforC4Cloader();
			verifyExists(C4CEngagementSalesObj.salespage.actionBtn);
			scrollIntoView(findWebElement(C4CEngagementSalesObj.salespage.actionBtn));
			Reusable.WaitforC4Cloader();
			click(C4CEngagementSalesObj.salespage.actionBtn);
			//WebInteractUtil.pause(1000);
			scrollIntoView(findWebElement(C4CEngagementSalesObj.salespage.EditBtn));
			click(C4CEngagementSalesObj.salespage.EditBtn);
//					WebInteractUtil.scrollIntoView(CPQ_Objects.ViewBtn);
//					WebInteractUtil.click(CPQ_Objects.ViewBtn);
			//waitForElementToAppear(CPQQuoteCreationObj.salespage.searchTxb, 180);
			Reusable.waitForpageloadmask();
			Reusable.WaitforCPQloader();
			Reusable.waitToPageLoad();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Quote Search with QuoteID '"+Quote_ID+"' is successfull in C4C");
			System.out.println("Quote Search with QuoteID '"+Quote_ID+"' is successfull in C4C");
			
			return "True";
		}
		
		public String navigateQuotesFromHomepage() throws IOException, InterruptedException 
		{

			waitForElementToAppear(C4CEngagementSalesObj.salespage.salesLst,90);
			verifyExists(C4CEngagementSalesObj.salespage.salesLst,"Sales List");
			Reusable.Waittilljquesryupdated();
			click(C4CEngagementSalesObj.salespage.salesLst);
			
			waitForElementToAppear(C4CEngagementSalesObj.salespage.quotesTab,90);
			verifyExists(C4CEngagementSalesObj.salespage.quotesTab,"Quotes Tab");
			click(C4CEngagementSalesObj.salespage.quotesTab,"Quotes Tab");
			Reusable.WaitforC4Cloader();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Navigation to Quotes tab was Successfull");
			System.out.println("Navigation to Quotes tab was Successfull");
			Reusable.Waittilljquesryupdated();				
			return "True";
		}
		
		public String logoutCPQ(String LogoutType) throws IOException, InterruptedException 
		{
			
			String Environment = util.getValue("Environment", "RFS").trim();
			String logOutBtn = null;
			String proxyLogOutBtn = null;
			
			switch (Environment) {
			case "RFS": case "PRD":case "TEST2":
				logOutBtn = CPQQuoteApprovalsObj.QuoteApprovals.cpqLogoutRFSBtn;
				proxyLogOutBtn = CPQQuoteApprovalsObj.QuoteApprovals.proxyLogoutRFSBtn;
				break;
			
			 case "TEST1":
				logOutBtn = CPQQuoteApprovalsObj.QuoteApprovals.Logoutbtn;
				proxyLogOutBtn = CPQQuoteApprovalsObj.QuoteApprovals.proxyLogoutBtn;
				break;		
			}
			
			if (LogoutType.equals("Proxy")) {
				waitForElementToAppear(proxyLogOutBtn, 60);
				click(proxyLogOutBtn);
				//WebInteractUtil.pause(2500);
				Reusable.waitForpageloadmask();
				System.out.println("Proxy User Logout was successfull");
			} else if (LogoutType.equals("Main")) {
				if (LogoutType.equals("Main")) {
					Reusable.Waittilljquesryupdated();
					click(logOutBtn);
//					if (Environment.equalsIgnoreCase("Test2")) {
//						WebInteractUtil.isEnabled(logOutBtn);
//					}
					Reusable.Waittilljquesryupdated();
					System.out.println("CPQ User Logout was successfull");
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ Logout Button was not Visible, Please Verify");
					System.out.println("CPQ logout Button was not Visible, Please Verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					return "False";
				}
			}
			
			return "True";
		}
		
		public void OverrideBuildingtype(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
		String BuildingType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Type");
		String OverrideReason=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Override_Reason");
		
		verifyExists(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtype, "Override Building");
		click(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtype);
		
		verifyExists(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtypedropdown, "Override Building dropdown");
		click(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtypedropdown);
		ClickonElementByString("//div[@data-value='"+BuildingType+"']", 20);
		
		waitForElementToAppear(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtypereason, 15);
		verifyExists(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtypereason, "'Reason' text field");
		sendKeys(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtypereason, OverrideReason, "OverrideReason");
		
		verifyExists(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtypeconfirm, "Override Building Confirm");
		click(CPQQuoteCreationObj.OverrideBuilding.OverrideBuildingtypeconfirm);

		}

		//**********************reValidatereNegotiateQuote Configuration ************************************************************

		public String reValidatereNegotiateQuote(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
		{
		
			
//			Initializing the Variable
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
	        String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
			String sResult;
		
					
//			Entering A-End details
			sResult = reValidatereNegotiateEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			Entering B-End details
			if (Product_Name.equalsIgnoreCase("EthernetLine")) {
				sResult = reValidatereNegotiateEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
							
			return "True";
			
		}
		
		public String reValidatereNegotiateEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 

		{		
//			Initializing the Variable
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
            String Re_Explore_Option = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Re_Explore_Option");              
            String Re_Explore_Value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Re_Explore_Value");
            String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
            String exploreOptionsSubLsb = null;String offnetCheckRadioButton=null;
			
			switch (SiteName) {
			
			case "A_End":
				offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckAEndBtn;
				manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
				exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
				exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
				offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetAManualOffnetRdB;
				break;
				
			case "B_End":
				offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckBEndBtn;
				manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
				exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
				exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
				offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetBManualOffnetRdB;
				break;
			}
			
			if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
					Product_Name.equalsIgnoreCase("EthernetSpoke")||Product_Name.equalsIgnoreCase("Wave")) 
			{				
				
				waitForElementToAppear(manualEngagementBtn, 75);
				scrollIntoView(findWebElement(manualEngagementBtn));
				verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
				Reusable.waitForAjax();
				ScrollIntoViewByString(manualEngagementBtn);
				click(manualEngagementBtn,"'Manual Engageent Button'");
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(exploreOptionsLsb, 20);
				scrollIntoView(findWebElement(exploreOptionsLsb));
				verifyExists(exploreOptionsLsb,"'Explore Option List box'");
				ScrollIntoViewByString(exploreOptionsLsb);
				isEnable(exploreOptionsLsb);
				Reusable.waitForpageloadmask();
				selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Re_Explore_Option);
				Reusable.WaitforProdConfigLoader();		
				switchToFrame("exploreEngagementComponent_oob");				
			}				
			
			if(Product_Name.equalsIgnoreCase("ColtIpAccess")) 
			{
				offnetCheckBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipoffnetcheckbtn;
				manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualengagementbtn;
				exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexploredropdown;
				exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.Ipexplorelst;							
				
				verifyExists(manualEngagementBtn,"Manual Engagement Button");
				ScrollIntoViewByString(manualEngagementBtn);
				click(manualEngagementBtn,"Manual Engagement Button");			
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(exploreOptionsLsb,75);
				verifyExists(exploreOptionsLsb,"'Explore Option List box'");
				isEnable(exploreOptionsLsb);
				Reusable.waitForpageloadmask();
							
				selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Re_Explore_Option);
				switchToFrame("exploreEngagementComponent");
				
				}
			
			if (Product_Name.equalsIgnoreCase("VPNNetwork")) {	
				
				manualEngagementBtn = CPQQuoteCreationObj.reValidatereNegotiateEntries.vpnManualCheckBtn;
				exploreOptionsLsb = CPQQuoteCreationObj.reValidatereNegotiateEntries.vpnExploreRequestVPN;
				exploreOptionsSubLsb = CPQQuoteCreationObj.reValidatereNegotiateEntries.vpnExploreRequestVPNSubLst;
					
				waitForElementToAppear(manualEngagementBtn, 20);
				verifyExists(manualEngagementBtn,"'Offnet Check Button'");
				scrollDown(manualEngagementBtn);
				click(manualEngagementBtn,"'Offnet Check Button'");			
				Reusable.waitForpageloadmask();
							
				waitForElementToAppear(exploreOptionsLsb, 20);
				verifyExists(exploreOptionsLsb,"'Explore Option List box'");
				isEnable(exploreOptionsLsb);
				Reusable.waitForpageloadmask();
				
				selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Re_Explore_Option);
				switchToFrame("exploreEngagementComponent_oob");
				
				}
			
						
			switch (Re_Explore_Value) {
			
			case "RENEGOTIATE":
				
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.ExploreReValidateBtn, 20);
			Reusable.waitForpageloadmask();
			
			String buliding_number=findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreBuldingNumberTxb).getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
			System.out.println("Explore Buliding Number is "+buliding_number);
				
				if (Re_Explore_Value.equalsIgnoreCase("RENEGOTIATE")) { click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ReNegotiateRBtn); }
				if (Re_Explore_Value.equalsIgnoreCase("REVALIDATE")) { click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ReValidateRBtn); }
				if (Re_Explore_Value.equalsIgnoreCase("RENEGOTIATE")) { click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ExploreReNegotiateBtn); }
				if (Re_Explore_Value.equalsIgnoreCase("REVALIDATE")) { click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ExploreReValidateBtn); }
				Reusable.waitForpageloadmask();
				
				//webDriver.switchTo().alert().accept();
				
				Reusable.waitForAjax();
				
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"Expand Explore Icon");
				waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem, 20);
				 
					String fullVal = findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem).getText();
					String[] bits = fullVal.split(":");
					String Request_ID = bits[bits.length-1].trim();
					ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
					System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//					Passing this request ID to testdata sheet
					if(SiteName.equalsIgnoreCase("A_End")) {
						DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Additional_Request_ID", Request_ID);
					}
					else
					{
						DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Additional_Request_ID", Request_ID);
					}					
					switchToDefaultFrame();
					
				
					verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
					click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
					Reusable.waitForpageloadmask();	
					break;
			}
						
			if(Product_Name.contains("Ethernet")) 
			{
				waitForElementToAppear(offnetCheckRadioButton, 20);
				click(offnetCheckRadioButton,"'Offnet Check Radio Button'");	
				Reusable.waitForpageloadmask();
							
			}
			else if(Product_Name.equalsIgnoreCase("ColtIpAccess"))
			{
				waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB,60);
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB,"Select option");
				scrollIntoView(findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB));
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessManualOffnetRdB,"'Explore Close Button'");
				Reusable.waitForpageloadmask();
					
			}
			verifyExists(manualEngagementBtn,"'Manual Engagement Button'");
			click(manualEngagementBtn,"'Manual Engagement Button'");
			Reusable.waitForpageloadmask();
			
			return "True";
			
		}
		
		public String professionalServicesConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript, String UserType, String PS_Product_Name) throws IOException, InterruptedException 
		{			
//			Initializing the Variable
			String sResult;
			String Service_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Service_Type");
			String Package_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Package_Type");
			System.out.println("Package Type:"+Package_Type);
			String Contract_Terms_Year = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Contract_Terms_Year_PS");
			String Total_Manday_Effort = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Total_Manday_Effort");
			
			switch(PS_Product_Name) {
			
			case "Data - Consultancy": case "Voice - Consultancy":
				waitForElementToAppear(CPQQuoteCreationObj.professionalServicesConfiguration.ServiceTypeLst,60);
				verifyExists(CPQQuoteCreationObj.professionalServicesConfiguration.ServiceTypeLst);				
				selectByValueDIV(CPQQuoteCreationObj.professionalServicesConfiguration.ServiceTypeLst, CPQQuoteCreationObj.ipAddonProducts.IpAddOnCountrySubLst, Service_Type);
				Reusable.waitForpageloadmask();
							
				break;
				
			case "Data - Project Management": case "Voice - Project Management":
				waitForElementToAppear(CPQQuoteCreationObj.professionalServicesConfiguration.PackageTypeLst,60);
				verifyExists(CPQQuoteCreationObj.professionalServicesConfiguration.PackageTypeLst);	
				click(CPQQuoteCreationObj.professionalServicesConfiguration.PackageTypeLst);	
				ClickonElementByString("//div[@data-value='"+Package_Type+"']", 20);
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.professionalServicesConfiguration.ResourceRequired);	
				click(CPQQuoteCreationObj.professionalServicesConfiguration.ResourceRequired);
				Reusable.waitForpageloadmask();
				break;
				
			case "Data - Service Management": case "Voice - Service Management":
				waitForElementToAppear(CPQQuoteCreationObj.professionalServicesConfiguration.ContractTermInYears,60);
				verifyExists(CPQQuoteCreationObj.professionalServicesConfiguration.ContractTermInYears, "Contract Term In Years");
				click(CPQQuoteCreationObj.professionalServicesConfiguration.ContractTermInYears, "Contract Term In Years");
				ClickonElementByString("//div[@data-value='"+Contract_Terms_Year+"']", 20);
				Reusable.waitForpageloadmask();					
				break;
			}
			
			if (UserType.equalsIgnoreCase("PS_User")) {
				Reusable.Waittilljquesryupdated();
				if(!PS_Product_Name.equalsIgnoreCase("Data - Service Management")&&!PS_Product_Name.equalsIgnoreCase("Voice - Service Management")) {
				
				verifyExists(CPQQuoteCreationObj.professionalServicesConfiguration.EffortEstimationManDaysTxb, "EffortEstimationManDaysTxb");
				click(CPQQuoteCreationObj.professionalServicesConfiguration.EffortEstimationManDaysTxb, "EffortEstimationManDaysTxb");
				sendKeys(CPQQuoteCreationObj.professionalServicesConfiguration.EffortEstimationManDaysTxb, Total_Manday_Effort,"EffortEstimationManDaysTxb");
				//findWebElement(CPQQuoteCreationObj.professionalServicesConfiguration.EffortEstimationManDaysTxb).sendKeys(Keys.TAB);
				Reusable.Waittilljquesryupdated();
				click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
				Reusable.waitForpageloadmask();
				
				waitForElementToAppear(CPQQuoteCreationObj.professionalServicesConfiguration.GrosssNRCElem,60);
				verifyExists(CPQQuoteCreationObj.professionalServicesConfiguration.GrosssNRCElem);

				String grossNRC = getTextFrom(CPQQuoteCreationObj.professionalServicesConfiguration.GrosssNRCElem);
				ExtentTestManager.getTest().log(LogStatus.PASS, "grosssNRCElem in CPQ "+grossNRC+" got generated");
				System.out.println("grosssNRCElem in CPQ "+grossNRC+" got generated");				
				}
				else {
					
					waitForElementToAppear(CPQQuoteCreationObj.professionalServicesConfiguration.PackageTypeLst,60);
					verifyExists(CPQQuoteCreationObj.professionalServicesConfiguration.PackageTypeLst);				
					click(CPQQuoteCreationObj.professionalServicesConfiguration.PackageTypeLst);	
					ClickonElementByString("//div[@data-value='"+Package_Type+"']", 20);
					Reusable.waitForpageloadmask();
				}

				}
			
//			Saving the Product Entries
			if (UserType.equalsIgnoreCase("Sales_User")) {
				sResult = updateSaveProductCPQ("SaveToQuote");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
			} else {
				sResult = updateSaveProductCPQ("Save");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
			Reusable.waitForpageloadmask();
			Reusable.waitToPageLoad();
			return "True";
		}
		
		public String editPSEngagement(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
		{			
//			Initializing the Variable
			String PS_Engagement = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PS_Engagement");
			String Engagement_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Engagement_Type");
			
			waitForElementToAppear(C4CEngagementSalesObj.engagementsPage.engagementlnk,120);
			verifyExists(C4CEngagementSalesObj.engagementsPage.engagementlnk,"Engagement Link");			
			isClickable(C4CEngagementSalesObj.engagementsPage.engagementlnk);
			Reusable.WaitforC4Cloader();
			click(C4CEngagementSalesObj.engagementsPage.engagementlnk);	
			
			for (int i = 1; i < 2; i++) { Reusable.WaitforC4Cloader(); }
						
			Reusable.WaitforC4Cloader();
			waitForElementToAppear(C4CEngagementSalesObj.engagementsPage.editengagementlnk,120);
			verifyExists(C4CEngagementSalesObj.engagementsPage.editengagementlnk,"Edit Engagement Link");									
			click(C4CEngagementSalesObj.engagementsPage.editengagementlnk,"Edit Engagement Link");
			Reusable.WaitforC4Cloader();

			switch (Engagement_Type) {
				case "SalesEngineer":
					waitForElementToAppear(C4CEngagementSalesObj.engagementsPage.engagementSEcbx,120);
					verifyExists(C4CEngagementSalesObj.engagementsPage.engagementSEcbx,"Engagement SE Checkbox");	
			    	click(C4CEngagementSalesObj.engagementsPage.engagementSEcbx,"Engagement SE Checkbox");
					Reusable.WaitforC4Cloader();
					
					verifyExists(C4CEngagementSalesObj.engagementsPage.seEngagementLst,"Engagement SE List");	
					click(C4CEngagementSalesObj.engagementsPage.seEngagementLst,"Engagement SE List");	
					Reusable.WaitforC4Cloader();
					ClickonElementByString("//li[normalize-space(.)='"+PS_Engagement+"']", 30);
					break;
				case "ConsultancyEngagement":
					verifyExists(C4CEngagementSalesObj.engagementsPage.consultantCbx,"Consultant Checkbox");	
					waitForElementToBeVisible(C4CEngagementSalesObj.engagementsPage.consultantCbx,60);
			    	click(C4CEngagementSalesObj.engagementsPage.consultantCbx,"Consultant Checkbox");
					break;
				}
			scrollIntoView(findWebElement(C4CEngagementSalesObj.engagementsPage.saveC4CBtn));
			click(C4CEngagementSalesObj.engagementsPage.saveC4CBtn);
			Reusable.WaitforC4Cloader();
			waitForElementToBeVisible(C4COpportunityObj.Opportunity.headerBar,120);
			String sText = getTextFrom(C4COpportunityObj.Opportunity.headerBar);
			if (sText.contains("saved")) {
				Reusable.WaitforC4Cloader();
				ExtentTestManager.getTest().log(LogStatus.PASS, "SE/Consultant User Engaged to the Quote with message "+sText);
				System.out.println("SE/Consultant User Engaged to the Quote with message "+sText);
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Edit Oppurtunity due to "+sText);
				System.out.println("Unable to Edit Oppurtunity due to "+sText);
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}				
			
			return "True";
		}
		
		public String returnC4CFromCPQ() throws IOException, InterruptedException 
		{	
			waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,120);
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
			Reusable.waitForpageloadmask();
			click(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
						
			waitForElementToBeVisible(C4CEngagementSalesObj.engagementsPage.engagementlnk, 120);
			verifyExists(C4CEngagementSalesObj.engagementsPage.engagementlnk,"Return To C4C Button");
			Reusable.WaitforC4Cloader();
			scrollIntoView(findWebElement(C4CEngagementSalesObj.engagementsPage.engagementlnk));
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }			

			return "True";
	}
		
		public String editProductConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
        {        
//            Initializing the Variable

 

            String sResult;
            String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
            String sProduct_Name = null;
            
            if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) {
                sProduct_Name = Product_Name;
            } else {
                sProduct_Name = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
            }
            
//            Selecting the product
            sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct_Name, null,"Click", null,1);
            if (sResult.equalsIgnoreCase("False")){ return "False"; }
            
            //Grid.clickInGrid(CPQQuoteApprovalsObj.QuoteApprovals.lineItemGridTable, 1, 1);
            
//            clicking on Reconfiguration link
            waitForElementToBeVisible(CPQQuoteCreationObj.engagementpf.reConfigureBtn, 120);
            verifyExists(CPQQuoteCreationObj.engagementpf.reConfigureBtn,"Re-Configure Button");
            Reusable.waitForpageloadmask();
            click(CPQQuoteCreationObj.engagementpf.reConfigureBtn,"Re-Configure Button");
            Reusable.waitForpageloadmask();
            Reusable.waitForpageloadmask();
            
            return "True";
        }
		
		public String ipAddonProductConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
				
//			Initializing the Variable
			String sResult;
			String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
			String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
			String Related_Network_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Related_Network_Reference");
			String Domain_Order_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Domain_Order_Type");
			String Domain_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Domain_Name");
			String Top_Level_Domain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Top_Level_Domain");
			String Service_Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
			String Customer_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Type");
			String Currency_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Currency_Type");
			String Domain_Country_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Domain_Country_Type");
			String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPAccess_Type");
			String Copy_Quote_At = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Copy_Quote_At");

			
			switch (Product_Name.toUpperCase()) 
			
			{	   
	                case "COLTIPDOMAIN":
	                    waitForElementToBeVisible(CPQQuoteCreationObj.ipAddonProducts.IpDomainCountryLst, 120);
	                    verifyExists(CPQQuoteCreationObj.ipAddonProducts.IpDomainCountryLst,"IP Domain Country List");
	                    Reusable.waitForpageloadmask();
	                   
	                        if(!IPAccess_Type.equalsIgnoreCase("New"))
	                        {
	                            verifyExists(CPQQuoteCreationObj.ipAddonProducts.IpDomainCountryLst,"IP Domain Country List");   
	                            click(CPQQuoteCreationObj.ipAddonProducts.IpDomainCountryLst,"IP Domain Country List");
	                            Reusable.waitToPageLoad();
	                           
	                            //ClickonElementByString("//div[@aria-label='"+Country+"']", 20);
	                            MoveToElement("@xpath=//div[@aria-label='"+Country+"']");
	                            click("@xpath=//div[@aria-label='"+Country+"']","Value from Country List");
	                           
	                            Reusable.waitForpageloadmask();
	                       
	                        verifyExists(CPQQuoteCreationObj.ipAddonProducts.IpAccessServiceReferenceTxb,"IPAccessService Reference TextBox");
	                        sendKeys(CPQQuoteCreationObj.ipAddonProducts.IpAccessServiceReferenceTxb, Related_Network_Reference,"IPAccessService Reference TextBox");
	                        Reusable.waitForpageloadmask();
	                        }
	                        else
	                        {
	                            verifyExists(CPQQuoteCreationObj.ipAddonProducts.IpAddonsNewSelectBtn,"IPAddon Section Button");
	                            click(CPQQuoteCreationObj.ipAddonProducts.IpAddonsNewSelectBtn);
	                            Reusable.waitForpageloadmask();
	                        }
	                        verifyExists(CPQQuoteCreationObj.ipAddonProducts.ClickDefaultCellIpDomainTble,"Default IP Domain Table");
	                        click(CPQQuoteCreationObj.ipAddonProducts.ClickDefaultCellIpDomainTble,"Default IP Domain Table");
	                        Reusable.waitForpageloadmask();
	                       
	                        ClickonElementByString("//div[@aria-label='"+Domain_Order_Type+"']", 20);
	               
	                        for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
	                        click(CPQQuoteCreationObj.ipAddonProducts.IpDomainNameTxb);
	                        sendKeys(CPQQuoteCreationObj.ipAddonProducts.IpDomainNameTxb, Domain_Name);
	                        for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
	                       
	                        verifyExists(CPQQuoteCreationObj.ipAddonProducts.TopLevelDomainLst,"Top Level Domain List");
	                        Reusable.WaitforCPQloader();
	                        click(CPQQuoteCreationObj.ipAddonProducts.TopLevelDomainLst,"Top Level Domain List");   
	                        //selectByValueDIV(CPQQuoteCreationObj.ipAddonProducts.TopLevelDomainLst,CPQQuoteCreationObj.ipAddonProducts.TopLevelDomainSubLst,Top_Level_Domain);
	               
	                        //Reusable.WaitforCPQloader();
	                        //click(CPQQuoteCreationObj.ipAddonProducts.TopLevelDomainTxt,"Top Level Domain List");
	                        //sendKeys(CPQQuoteCreationObj.ipAddonProducts.TopLevelDomainTxt,Top_Level_Domain,"Top Level Domain List");
	                        //for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }                       
	                        ClickonElementByString("//div[@aria-label='"+Top_Level_Domain+"']", 60);
	                       
	                        for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
	                   
	                        break;
					
				case "COLTIPGUARDIAN":
					
					waitForElementToBeVisible(CPQQuoteCreationObj.ipAddonProducts.IpGuardianCountryLst, 120);
					Reusable.waitForpageloadmask();
					
					verifyExists(CPQQuoteCreationObj.ipAddonProducts.IpGuardianCountryLst,"IP Domain Country List");   
                    click(CPQQuoteCreationObj.ipAddonProducts.IpGuardianCountryLst,"IP Domain Country List");
                    Reusable.waitToPageLoad();
                   
                    //ClickonElementByString("//div[@aria-label='"+Country+"']", 20);
                    MoveToElement("@xpath=//div[@aria-label='"+Country+"']");
                    click("@xpath=//div[@aria-label='"+Country+"']","Value from Country List");
                   
                    Reusable.waitForpageloadmask();
                    
					//selectByValueDIV(CPQQuoteCreationObj.ipAddonProducts.IpGuardianCountryLst, CPQQuoteCreationObj.ipAddonProducts.IpAddOnCountrySubLst, Country);
					//Reusable.waitForpageloadmask();
					
	                sendKeys(CPQQuoteCreationObj.ipAddonProducts.IpGuardianServiceReferenceTxb, Related_Network_Reference);
	                Reusable.waitForpageloadmask();
	                
	        		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, 20);
	                verifyExists(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");   
	                click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");
	               
	                sendKeys(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthTxb,Service_Bandwidth,"'Service Bandwidth List'");
	                click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthQry,"'Service Bandwidth Query'");
	                //selectByValueDIV(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthSubLst,BandwidthValue);
	                ClickonElementByString("//div[@data-value='"+Service_Bandwidth+"']", 20);
	                Reusable.waitForpageloadmask();
	                
	               /* verifyExists(CPQQuoteCreationObj.ipAddonProducts.CustomerTypeLst,"Customer Type List");   
                    click(CPQQuoteCreationObj.ipAddonProducts.CustomerTypeLst,"Customer Type List");
                    Reusable.waitToPageLoad();
                    
                    ClickonElementByString("//div[@aria-label='"+Customer_Type+"']", 20);*/
				    //selectByValueDIV(CPQQuoteCreationObj.ipAddonProducts.CustomerTypeLst, CPQQuoteCreationObj.ipAddonProducts.IpAddOnCountrySubLst, Customer_Type);	
				    Reusable.waitForpageloadmask();
					break;
					
				case "COLTMANAGEDVIRTUALFIREWALL":
					
					waitForElementToBeVisible(CPQQuoteCreationObj.ipAddonProducts.MvfCountryLst, 120);
					Reusable.waitForpageloadmask();
					
					verifyExists(CPQQuoteCreationObj.ipAddonProducts.MvfCountryLst,"IP Domain Country List");   
                    click(CPQQuoteCreationObj.ipAddonProducts.MvfCountryLst,"IP Domain Country List");
                    Reusable.waitToPageLoad();
                   
                    MoveToElement("@xpath=//div[@aria-label='"+Country+"']");
                    click("@xpath=//div[@aria-label='"+Country+"']","Value from Country List");
                    
                    //ClickonElementByString("//div[@aria-label='"+Country+"']", 20);
					//selectByValueDIV(CPQQuoteCreationObj.ipAddonProducts.MvfCountryLst, CPQQuoteCreationObj.ipAddonProducts.IpAddOnCountrySubLst, Country);
					Reusable.waitForpageloadmask();
					
	                sendKeys(CPQQuoteCreationObj.ipAddonProducts.MvfServiceReferenceTxb, Related_Network_Reference);
	                Reusable.waitForpageloadmask();
	                
	        		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, 20);
	                verifyExists(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");   
	                click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");
	               
	                sendKeys(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthTxb,Service_Bandwidth,"'Service Bandwidth List'");
	                click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthQry,"'Service Bandwidth Query'");
	                //selectByValueDIV(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthSubLst,BandwidthValue);
	                ClickonElementByString("//div[@data-value='"+Service_Bandwidth+"']", 20);
	                Reusable.waitForpageloadmask();
					break;
					
				case "COLTMANAGEDDEDICATEDFIREWALL":
					
					waitForElementToBeVisible(CPQQuoteCreationObj.ipAddonProducts.MdfCountryLst, 120);
					Reusable.waitForpageloadmask();
					
					verifyExists(CPQQuoteCreationObj.ipAddonProducts.MdfCountryLst,"IP Domain Country List");   
                    click(CPQQuoteCreationObj.ipAddonProducts.MdfCountryLst,"IP Domain Country List");
                    Reusable.waitToPageLoad();
                   
                    MoveToElement("@xpath=//div[@aria-label='"+Country+"']");
                    click("@xpath=//div[@aria-label='"+Country+"']","Value from Country List");
                    
                    //ClickonElementByString("//div[@aria-label='"+Country+"']", 20);
					//selectByValueDIV(CPQQuoteCreationObj.ipAddonProducts.MvfCountryLst, CPQQuoteCreationObj.ipAddonProducts.IpAddOnCountrySubLst, Country);
					Reusable.waitForpageloadmask();
					
	                sendKeys(CPQQuoteCreationObj.ipAddonProducts.MdfServiceReferenceTxb, Related_Network_Reference);
	                Reusable.waitForpageloadmask();
	                
	        		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, 20);
	                verifyExists(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");   
	                click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst,"'Service Bandwidth List'");
	               
	                sendKeys(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthTxb,Service_Bandwidth,"'Service Bandwidth List'");
	                click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthQry,"'Service Bandwidth Query'");
	                //selectByValueDIV(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthLst, CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthSubLst,BandwidthValue);
	                ClickonElementByString("//div[@data-value='"+Service_Bandwidth+"']", 20);
	                Reusable.waitForpageloadmask();
					break;
			}
			
			/*if(Copy_Quote_At.equalsIgnoreCase("Created")) 
			{
//				Calling below method to mark as partial save
				sResult =markPartialSave(Product_Name);
				if (sResult.equalsIgnoreCase("False")){  return "False";  }
			}*/
//			Saving the Product Entries
			sResult = updateSaveProductCPQ("SaveToQuote");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			return "True";
		}
		
		public String markPartialSave(String ProductName) throws InterruptedException, IOException
		{			
			
			
			switch (ProductName) {
		    case "EthernetLine": case "EthernetHub": case "Wave": case "EthernetSpoke":
		    	
			waitForElementToAppear(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt, 20);
			scrollUp();
			//ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt);
			Reusable.waitForpageloadmask();
			click(CPQQuoteCreationObj.PrimaryConnectionPage.ojMarkPartialSaveSwt);	
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();			
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();
			waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn, 90);
			break;
			
		    case "ColtIpAccess":
			waitForElementToAppear(CPQQuoteCreationObj.PartialSave.OjIPMarkPartialSaveSwt, 5);
			scrollUp();
			//ScrollIntoViewByString(CPQQuoteCreationObj.PartialSave.OjIPMarkPartialSaveSwt);
		    Reusable.waitForpageloadmask();
		    click(CPQQuoteCreationObj.PartialSave.OjIPMarkPartialSaveSwt);	
		    for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();
			waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn, 90);
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();
		    break;
		    
		    case "CPESolutionsSite":
		    waitForElementToAppear(CPQQuoteCreationObj.PartialSave.OjMarkPartialSaveSiteCPE, 5);
		    scrollUp();
		    //ScrollIntoViewByString(CPQQuoteCreationObj.PartialSave.OjMarkPartialSaveSiteCPE);
		    Reusable.waitForpageloadmask();
		    click(CPQQuoteCreationObj.PartialSave.OjMarkPartialSaveSiteCPE);	
		    for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();
			waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn, 90);
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();
			break;
		    
		    case "ColtIpDomain": case "ColtIpGuardian":  case "ColtManagedVirtualFirewall": case "ColtManagedDedicatedFirewall":
		    waitForElementToAppear(CPQQuoteCreationObj.PartialSave.OjMarkPartialSaveAddonsBtn, 5);
		    scrollUp();
		    //ScrollIntoViewByString(CPQQuoteCreationObj.PartialSave.OjMarkPartialSaveAddonsBtn);
		    Reusable.waitForpageloadmask();
		    click(CPQQuoteCreationObj.PartialSave.OjMarkPartialSaveAddonsBtn);	
		    for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();
			waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn, 90);
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitToPageLoad();
		    break;
			}
			return "True";
		}
		
		public String addProductandQuoteInC4C(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
		{
		// Initializing the variables
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Container_Months = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Container_Months");
		String Container_BandWidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Container_BandWidth");
		String Why_Container = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Why_Container");
		String CloudService_Provider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CloudService_Provider");
		String No_Of_Copies = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"No_Of_Copies");
		String sProduct;
		if(Product_Name.equalsIgnoreCase("ColtIPAccess")) {sProduct="Colt IP Access";}else {sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");}

		 for(int j=0; j<Integer.parseInt(No_Of_Copies); j++)
		{
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.productsLnk, 75);
		verifyExists(CPQQuoteCreationObj.AddProducts.productsLnk,"Add Products button");
		click(CPQQuoteCreationObj.AddProducts.productsLnk,"Add Products button");
		Reusable.waitForpageloadmask();
		for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
		isEnable(CPQQuoteCreationObj.AddProducts.addQuoteBtn);
		scrollDown(CPQQuoteCreationObj.AddProducts.addQuoteBtn);
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.AddProducts.addQuoteBtn,"Add Quote button");
		for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.productsTxb, 75);
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.AddProducts.productsTxb,"click on product name fields");
		sendKeys(CPQQuoteCreationObj.AddProducts.productsTxb, sProduct);
		System.out.println("Adding Product "+sProduct);
		Reusable.waitForpageloadmask();
		if(sProduct.equalsIgnoreCase("Colt IP Access")) {
		ClickonElementByString("//li[contains(@id,'objectvalue')]//div//span[text()='"+sProduct+"']//parent::label//following-sibling::div[text()='20']", 30);
		}else {
		ClickonElementByString("//li[contains(@id,'objectvalue')]//div//span[text()='"+sProduct+"']", 30);
		}
		ExtentTestManager.getTest().log(LogStatus.PASS, "Product Added "+sProduct);
		
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contContractsLenMnthTxb, 10);
		verifyExists(CPQQuoteCreationObj.AddProducts.contContractsLenMnthTxb, Container_Months);
		sendKeys(CPQQuoteCreationObj.AddProducts.contContractsLenMnthTxb, Container_Months,"Container_Months");
		
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.containerbandWidthLst, 10);
		ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.containerbandWidthLst);
		click(CPQQuoteCreationObj.AddProducts.containerbandWidthLst,"container bandWidth Lst");
		ClickonElementByString("//li[normalize-space(.)='"+Container_BandWidth+"']", 30);
		
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.addCloudServiceProLst, 10);
		ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.addCloudServiceProLst);
		click(CPQQuoteCreationObj.AddProducts.addCloudServiceProLst,"addCloudServiceProLst");
		ClickonElementByString("//li[normalize-space(.)='"+CloudService_Provider+"']", 30);
		
		click(CPQQuoteCreationObj.AddProducts.containerAddBtn,"container add button");
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.AddProducts.saveBtn,"save Btn");
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.quotesLnk, 75);
		click(CPQQuoteCreationObj.AddProducts.quotesLnk,"quotesLnk");
		for (int i = 1; i < 6; i++) {Reusable.waitForpageloadmask(); }
		isEnable(CPQQuoteCreationObj.AddProducts.addNewcontainerBtn);
		scrollDown(CPQQuoteCreationObj.AddProducts.addNewcontainerBtn);
		click(CPQQuoteCreationObj.AddProducts.addNewcontainerBtn,"Add Newcontainer Btn");
		Reusable.WaitforC4Cloader();
		Reusable.WaitforCPQloader();
		pause(60000);
		waitForAjax();
		waitForAjax();
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.quoteNameTxb, 240);
		verifyExists(CPQQuoteCreationObj.AddProducts.quoteNameTxb, "quoteNameTxb");
		Reusable.WaitforCPQloader();
		waitForAjax();
		//waitForElementToAppear(CPQQuoteCreationObj.AddProducts.seRevLnk, 120);
		//verifyExists(CPQQuoteCreationObj.AddProducts.seRevLnk, "seRevLnk");
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.quoteTypeElem, 240);
		verifyExists(CPQQuoteCreationObj.AddProducts.quoteTypeElem, "Quote Type Elem");
		Reusable.WaitforCPQloader();
		waitForAjax();

		String quoteType = findWebElement(CPQQuoteCreationObj.AddProducts.quoteTypeElem).getAttribute("value");
		String quoteID = findWebElement(CPQQuoteCreationObj.AddProducts.quoteIDElem).getAttribute("value");
		
		//Write Quote ID value in Test Data
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Quote_ID", quoteID);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Quote ID is generated "+quoteID);
		System.out.println("Quote ID is generated "+quoteID);
		//if (quoteType.equals("Container - New Business"))
		//{
		ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.contwhyisContainerFileLst);
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contwhyisContainerFileLst, 120);
		click(CPQQuoteCreationObj.AddProducts.contwhyisContainerFileLst,"contwhyis Container File Lst");
		ClickonElementByString("//li[normalize-space(.)='"+Why_Container+"']", 30);
		Reusable.waitForpageloadmask();
		return quoteID;
		//}
		}
		return "False";
		}
		
		public String uploadFilesTechnicalTab(String Product_name, String selectCategory, String Select_Type, int count, String Option, String Upload_Level) throws IOException, InterruptedException
		{
		// Initializing the variables
		String file, fileName = null;
		file=System.getProperty("user.dir")+"\\TestData\\EOFFiles\\EoFswithLanguageswitch\\";

		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.technicalApprovalLnk, 60);
		Reusable.waitForpageloadmask();
		scrollDown(CPQQuoteCreationObj.AddProducts.technicalApprovalLnk);
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.AddProducts.technicalApprovalLnk);
		Reusable.waitForpageloadmask();
		switchToFrame("ucmIframeApp");
		System.out.println("Switch to UCIMFrame");
		Reusable.waitForpageloadmask();
		WebDriver Driver = WEB_DRIVER_THREAD_LOCAL.get();
		Reusable.Waittilljquesryupdated();
		
		if(Upload_Level.equalsIgnoreCase("Quote_Level")) 
		{
			Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_doc()')])["+count+"]")).isEnabled();
			Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_doc()')])["+count+"]")).click();
		}
		else 
		{
			Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_line_item')])["+count+"]")).isEnabled();
			//Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_line_item')])["+count+"]")).click();
			clickByJS("@xpath=(//span[contains(@onclick,'listPage_upload_quote_line_item')])["+count+"]");
		}
		
		verifyExists(CPQQuoteCreationObj.AddProducts.contUpldDocTxt,"contUpldDocTxt");
		if(selectCategory.equalsIgnoreCase("Internal"))
		{
		click(CPQQuoteCreationObj.AddProducts.contUpldInternalRdb,"contUpldInternalRdb");
		}
		else {
		click(CPQQuoteCreationObj.AddProducts.contUpldcustomerRdb,"contUpldcustomerRdb");
		}
		Reusable.waitForpageloadmask();
			
				
			
			switch(Select_Type)
			{
			case "EOF(xls)":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				
				Driver.findElement(By.xpath("//option[@value='"+Option+"']")).click();

				Reusable.waitForpageloadmask();
				
								
				if(Product_name.equalsIgnoreCase("ColtEthernetLine")) {fileName="QtO\\EthernetLine_QtO_v4.2.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtEthernetHub")) {fileName="QtO\\EthernetHub_QtO_v4.3.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtEthernetSpoke")) {fileName="QtO\\EthernetSpoke_QtO_v4.1.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtWave")) {fileName="QtO\\Wave_QtO_v2.9.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtIPAccess")) {fileName="QtO\\IPAccess_QtO_v4.1.xlsx";}
				break;
				
			case "Confidential":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				//ClickonElementByString("//option[@value='"+Option+"']", 20);
				Driver.findElement(By.xpath("//option[@value='"+Option+"']")).click();
				Reusable.waitForpageloadmask();
			
				fileName="Confidential.pdf";
				break;
			case "EOF(pdf)":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				//ClickonElementByString("//option[@value='"+Option+"']", 20);
				Driver.findElement(By.xpath("//option[@value='"+Option+"']")).click();
				
				fileName="Form\\"+Product_name+" Form.pdf";
				break;
			}
			
			waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contchooseFile, 30);
			System.out.println(file+fileName);
			sendKeys(CPQQuoteCreationObj.AddProducts.contchooseFile, file+fileName,"contchooseFile");
			Reusable.waitForpageloadmask();
			waitForAjax();
								
			verifyExists(CPQQuoteCreationObj.AddProducts.contUploadFile,"contUploadFile");
			ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.contUploadFile);

			clickByJS(CPQQuoteCreationObj.AddProducts.contUploadFile);
			//waitForElementToBeVisible(CPQQuoteCreationObj.AddProducts.containerUploadBtn, 40);
			Reusable.waitForpageloadmask();
				
			waitForAjax();
			switchToDefaultFrame();
			ExtentTestManager.getTest().log(LogStatus.PASS, Product_name+" "+Select_Type+" added successfully");
			
			return "True";
		}
		
		public String uploadFilesAttachmentTab(String Product_name, String selectCategory, String Select_Type, int count, String Option) throws IOException, InterruptedException 
		{
	
//			Initializing the variables
			
			String file, fileName = null;
			file=System.getProperty("user.dir")+"\\src\\Data\\EOFFiles\\EoFswithLanguageswitch\\";
		
			
			    waitForElementToAppear(CPQQuoteCreationObj.AddProducts.ATTACHMENTSTab, 60);
				Reusable.waitForpageloadmask();
				scrollDown(CPQQuoteCreationObj.AddProducts.ATTACHMENTSTab);
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.AddProducts.ATTACHMENTSTab);
				Reusable.waitForpageloadmask();
				switchToFrame("ucmIframeApp");
				System.out.println("Switch to UCIMFrame");
				Reusable.waitForpageloadmask();
				WebDriver Driver = WEB_DRIVER_THREAD_LOCAL.get();
				Reusable.Waittilljquesryupdated();
				
				//Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_doc()')])["+count+"]")).isEnabled();
				//Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_doc()')])["+count+"]")).click();
				
				verifyExists(CPQQuoteCreationObj.AddProducts.contUpldDocTxt,"contUpldDocTxt");
				
				if(selectCategory.equalsIgnoreCase("Internal")) 
				{
					click(CPQQuoteCreationObj.AddProducts.contUpldInternalRdb,"contUpldInternalRdb");
					}
				else {
					click(CPQQuoteCreationObj.AddProducts.contUpldcustomerRdb,"contUpldcustomerRdb");
					}
				Reusable.waitForpageloadmask();
			
				
			
			switch(Select_Type)
			{
			case "EOF(xls)":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				ClickonElementByString("//option[@value='"+Option+"']", 20);
				Reusable.waitForpageloadmask();
				
								
				if(Product_name.equalsIgnoreCase("ColtEthernetLine")) {fileName="QtO\\EthernetLine_QtO_v4.2.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtEthernetHub")) {fileName="QtO\\EthernetHub_QtO_v4.3.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtEthernetSpoke")) {fileName="QtO\\EthernetSpoke_QtO_v4.1.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtWave")) {fileName="QtO\\Wave_QtO_v2.9.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtIPAccess")) {fileName="QtO\\IPAccess_QtO_v4.1.xlsx";}
				break;
				
			case "Confidential":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				ClickonElementByString("//option[@value='"+Option+"']", 20);
				Reusable.waitForpageloadmask();
			
				fileName="Confidential.pdf";
				break;
			case "EOF(pdf)":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				ClickonElementByString("//option[@value='"+Option+"']", 20);
				
				
				fileName="Form\\"+Product_name+" Form.pdf";
				break;
			}
			
			    waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contchooseFile, 30);
				System.out.println(file+fileName);
				sendKeys(CPQQuoteCreationObj.AddProducts.contchooseFile, file+fileName,"contchooseFile");
				Reusable.waitForpageloadmask();
								
				click(CPQQuoteCreationObj.AddProducts.contUploadFile,"contUploadFile");
				//waitForElementToBeVisible(CPQQuoteCreationObj.AddProducts.containerUploadBtn, 40);
				Reusable.waitForpageloadmask();
				
				
				switchToDefaultFrame();
				ExtentTestManager.getTest().log(LogStatus.PASS, Product_name+" "+Select_Type+" added successfully");
			
			return "True";
		}
		public String FileAttachment(String Product_name, String selectCategory, String Select_Type, int count, String Option) throws IOException, InterruptedException 
		{
	
//			Initializing the variables
			
			String file, fileName = null;
			file=System.getProperty("user.dir")+"\\src\\Data\\EOFFiles\\EoFswithLanguageswitch\\";
		
			
			    waitForElementToAppear(CPQQuoteCreationObj.AddProducts.technicalApprovalLnk, 60);
				Reusable.waitForpageloadmask();
				scrollDown(CPQQuoteCreationObj.AddProducts.technicalApprovalLnk);
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.AddProducts.technicalApprovalLnk);
				Reusable.waitForpageloadmask();
				switchToFrame("ucmIframeApp");
				System.out.println("Switch to UCIMFrame");
				Reusable.waitForpageloadmask();
				WebDriver Driver = WEB_DRIVER_THREAD_LOCAL.get();
				Reusable.Waittilljquesryupdated();
				
				Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_doc()')])["+count+"]")).isEnabled();
				Driver.findElement(By.xpath("(//span[contains(@onclick,'listPage_upload_quote_doc()')])["+count+"]")).click();
				
				verifyExists(CPQQuoteCreationObj.AddProducts.contUpldDocTxt,"contUpldDocTxt");
				
				if(selectCategory.equalsIgnoreCase("Internal")) 
				{
					click(CPQQuoteCreationObj.AddProducts.contUpldInternalRdb,"contUpldInternalRdb");
					}
				else {
					click(CPQQuoteCreationObj.AddProducts.contUpldcustomerRdb,"contUpldcustomerRdb");
					}
				Reusable.waitForpageloadmask();
			
				
			
			switch(Select_Type)
			{
			case "EOF(xls)":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				ClickonElementByString("//option[@value='"+Option+"']", 20);
				Reusable.waitForpageloadmask();
				
								
				if(Product_name.equalsIgnoreCase("ColtEthernetLine")) {fileName="QtO\\EthernetLine_QtO_v4.2.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtEthernetHub")) {fileName="QtO\\EthernetHub_QtO_v4.3.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtEthernetSpoke")) {fileName="QtO\\EthernetSpoke_QtO_v4.1.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtWave")) {fileName="QtO\\Wave_QtO_v2.9.xlsx";}
				else if(Product_name.equalsIgnoreCase("ColtIPAccess")) {fileName="QtO\\IPAccess_QtO_v4.1.xlsx";}
				break;
				
			case "Confidential":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				ClickonElementByString("//option[@value='"+Option+"']", 20);
				Reusable.waitForpageloadmask();
			
				fileName="Confidential.pdf";
				break;
			case "EOF(pdf)":
				waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contSelectDocDrp, 20);	
				verifyExists(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				click(CPQQuoteCreationObj.AddProducts.contSelectDocDrp,"'contSelectDocDrp'");
				ClickonElementByString("//option[@value='"+Option+"']", 20);
				
				
				fileName="Form\\"+Product_name+" Form.pdf";
				break;
			}
			
			    waitForElementToAppear(CPQQuoteCreationObj.AddProducts.contchooseFile, 30);
				System.out.println(file+fileName);
				sendKeys(CPQQuoteCreationObj.AddProducts.contchooseFile, file+fileName,"contchooseFile");
				Reusable.waitForpageloadmask();
								
				click(CPQQuoteCreationObj.AddProducts.contUploadFile,"contUploadFile");
				//waitForElementToBeVisible(CPQQuoteCreationObj.AddProducts.containerUploadBtn, 40);
				Reusable.waitForpageloadmask();
				
				
				switchToDefaultFrame();
				ExtentTestManager.getTest().log(LogStatus.PASS, Product_name+" "+Select_Type+" added successfully");
			
			return "True";
		}
		public String copyQuote(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		
//			Initializing Variable
			String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
			String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
			String No_Of_Copies = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"No_Of_Copies");
			String QuoteID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");
			
			String sResult = null;
			String sProduct;
			if(Product_Name.equalsIgnoreCase("VPNNetwork")) {sProduct="VPN Network";}
			else if(Product_Name.equalsIgnoreCase("Colt Ip Domain")) {sProduct="IP Domain";}
			else if(Product_Name.equalsIgnoreCase("Colt Ip Guardian")) {sProduct="IP Guardian";}
			else if(Product_Name.equalsIgnoreCase("Colt Managed Virtual Firewall")) {sProduct="IP Managed Virtual Firewall";}
			else if(Product_Name.equalsIgnoreCase("Colt Managed Dedicated Firewall")) {sProduct="IP Managed Dedicated Firewall";}
			
			else {sProduct= Product_Name.replaceAll("(?!^)([A-Z])", " $1");}
			
			
			switch (Copy_Type.toUpperCase()) {
			
			case "LINEITEM":
				
//				Clicking on the rows from the table
				sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, null,"Click", null, 1);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
							
				waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.copyLineItemsBtn, 40);
				ScrollIntoViewByString(CPQQuoteCreationObj.CopyQuote.copyLineItemsBtn);
			    click(CPQQuoteCreationObj.CopyQuote.copyLineItemsBtn,"copy Line Items Btn");
			    Reusable.waitForpageloadmask();
			    
			    verifyExists(CPQQuoteCreationObj.CopyQuote.numberOfCopiesTxb);
				sendKeys(CPQQuoteCreationObj.CopyQuote.numberOfCopiesTxb, No_Of_Copies);
				Reusable.waitForpageloadmask();
			    click(CPQQuoteCreationObj.CopyQuote.copyPromptOKBtn,"copy Prompt OK Btn");
			    Reusable.waitForpageloadmask();
			    break;

			case "TRANSACTION":
				
				waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.adminBtn, 40);
				ScrollIntoViewByString(CPQQuoteCreationObj.CopyQuote.adminBtn);
				click(CPQQuoteCreationObj.CopyQuote.adminBtn,"adminBtn");
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.CopyQuote.transactionsLnk);
				click(CPQQuoteCreationObj.CopyQuote.transactionsLnk,"transactionsLnk");
				Reusable.waitForpageloadmask();
				waitForAjax();
				WebDriver Driver = WEB_DRIVER_THREAD_LOCAL.get();
				Reusable.Waittilljquesryupdated();
				
				Driver.findElement(By.xpath("//a[text()='"+QuoteID+"']//parent::div//parent::td//preceding-sibling::td//input[@type='checkbox']")).isEnabled();
				Driver.findElement(By.xpath("//a[text()='"+QuoteID+"']//parent::div//parent::td//preceding-sibling::td//input[@type='checkbox']")).click();				
				Reusable.waitForpageloadmask();
				waitForAjax();
				click(CPQQuoteCreationObj.CopyQuote.copyTransactionsLnk,"copyTransactionsLnk");
				Reusable.waitForpageloadmask();
				Reusable.waitToPageLoad();
				waitForAjax();	
				
				waitForElementToAppear(CPQQuoteCreationObj.CopyQuote.copyQuotePopup, 120);
				verifyExists(CPQQuoteCreationObj.CopyQuote.copyQuotePopup, "Alert Box");
				verifyExists(CPQQuoteCreationObj.CopyQuote.copyOKBtn, "OK Button in Alert");
				click(CPQQuoteCreationObj.CopyQuote.copyOKBtn,"OK Button in Alert");
				Reusable.waitForpageloadmask();
				waitForAjax();
				
				break;
			
				
			case "C4C":
//				Calling the below method to return from cpq to c4c
				WebDriver Driver1 = WEB_DRIVER_THREAD_LOCAL.get();
				sResult = returnC4CFromCPQ();
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				
				waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.quotesLnk, 40);
				click(CPQQuoteCreationObj.CopyQuote.quotesLnk,"quotesLnk");
				Reusable.waitForpageloadmask();
				Reusable.Waittilljquesryupdated();
				
				Driver1.findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")).isEnabled();
				Driver1.findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")).click();
			    Reusable.waitForpageloadmask();
			    isEnable(CPQQuoteCreationObj.CopyQuote.actionBtn);
			    ScrollIntoViewByString(CPQQuoteCreationObj.CopyQuote.actionBtn);
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.CopyQuote.actionBtn,"actionBtn");
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.CopyQuote.copyQuoteC4CBtn,"copyQuoteC4CBtn");
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.CopyQuote.proceedC4CBtn,"proceedC4CBtn");
				Reusable.waitForpageloadmask();
				break;
						
			}
			
//			Capturing Quote Ref
			System.out.println("Copy_Type: "+Copy_Type);
			if ((Copy_Type.toUpperCase().equals("TRANSACTION")) || (Copy_Type.toUpperCase().equals("C4C")))
			{
				System.out.println("Inside the Transaction condition");
				Reusable.WaitforCPQloader();
				waitForElementToBeVisible(CPQQuoteCreationObj.TransactionPage.QuoteId,120);
				//Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.TransactionPage.QuoteId);
				verifyExists(CPQQuoteCreationObj.TransactionPage.approvedLnk);
				verifyExists(CPQQuoteCreationObj.TransactionPage.quoteTypeElem);
				//Reusable.waitForpageloadmask();
				//Reusable.Waittilljquesryupdated();
				String quoteID = getAttributeFrom(CPQQuoteCreationObj.TransactionPage.quoteIDElem,"value");
				//ExtentTestManager.getTest().log(LogStatus.PASS, "Copy Quote ID is generated "+quoteID);
				System.out.println("Copy Quote ID is generated "+quoteID);
				DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Copy_Quote_ID", quoteID);
				
			}
			
			return "True";
			
		}
		
		public String navigateContactInfoTab() throws IOException, InterruptedException {

			ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow);
			waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow, 20);
			verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
			click(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
			Reusable.waitForpageloadmask();
			
			  waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.contactInformationLnk, 40);
			  click(CPQQuoteCreationObj.CopyQuote.contactInformationLnk);
				for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
				Reusable.Waittilljquesryupdated();
				verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTable,"Additional Quote Info Table");
				ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTable);
				waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.additionalQuoteInfoTable, 25);
				Reusable.Waittilljquesryupdated();
					
			return "True";
			
		}
		
		public String addContactInformationCPQ(String refColumn, String rowRef, String actColumn, String ActionType, String ActionValue) throws IOException, InterruptedException {

			String tXpath, Row_Val;
			int row_number = 0;
			
			ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow);
			waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow, 20);
			verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
			click(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
			for (int i = 1; i < 5; i++) {Reusable.waitForpageloadmask(); }
			
			waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.ContactInformation, 20);
			verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.ContactInformation,"Contact Information Section");
			click(CPQQuoteSubmissionObj.QuoteSubmit.ContactInformation,"Contact Information");
			for (int i = 1; i < 5; i++) {Reusable.waitForpageloadmask(); }

			
			waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.additonalInfoTable, 40);
			Reusable.Waittilljquesryupdated();
			String sXpath = CPQQuoteCreationObj.CopyQuote.additonalInfoTable.toString();
			tXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
					
//			getting the row and column number
			WebElement additonalInfoTable = webDriver.findElement(By.xpath("//table[@aria-describedby='additionalQuoteInformation_t-arraySet-summary']"));	
			List<WebElement> rows = additonalInfoTable.findElements(By.tagName("tr"));
			List<WebElement> columns = rows.get(0).findElements(By.tagName("th"));
			int tot_row = rows.size();
			int tot_col = columns.size();
//			System.out.println("Total Column size is "+tot_col);
			int iCol, iRow, rColumn_number = 0, aColumn_number = 0;
			
			//Reading the column headers of table and set the column number with use of reference
			for(iCol = 1; iCol <= tot_col-1; iCol++){
				String Col_Val = columns.get(iCol).getAttribute("title").trim();
				if (Col_Val.equals(refColumn)){ 
					rColumn_number = iCol+1; 
//					System.out.println("ref Column number is "+rColumn_number); 
					break; 
				}
			}
			
//			Returns the function of reference column number
			if (iCol > tot_col) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+refColumn+" is not found in the webtable, Please Verify ");
				System.out.println("Column Name "+refColumn+" is not found in the webtable, Please Verify ");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
			
			
			if (!ActionType.equals("Click")) {
				//Reading the actual column number ff table and set the column number with use of reference
				for(iCol = 1; iCol <= tot_col-1; iCol++){
					String Col_Val = columns.get(iCol).getAttribute("title").trim();
					if (Col_Val.equals(actColumn)){ 
						aColumn_number = iCol+1; 
//						System.out.println("act Column number is "+aColumn_number); 
						break; 
					}
				}
			
//				Returns the function of column names are not matched
				if (iCol > tot_col) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
					System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					return "False";
				}
			}
			
			//Taking the row value
			for(iRow =1; iRow <= tot_row-1; iRow++){
				WebElement rowValue = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+iRow+"]/td["+rColumn_number+"]//child::span"));
				Row_Val = rowValue.getAttribute("title").trim();
				if (Row_Val.equals(rowRef)){ 
					row_number = iRow; 
//					System.out.println("Row number is "+row_number);
					break;
				}
			}
			
//			Returns the function if rows names are not matched
			if (iCol > tot_col) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Row Name "+rowRef+" is not found in the webtable, Please Verify ");
				System.out.println("Row Name "+rowRef+" is not found in the webtable, Please Verify ");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
			
			WebElement Cell; String sOut;
			switch (ActionType) {
				case "Edit":
					Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
					scrollIntoView(Cell);
					for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
					Cell.click();
					//WebElement edit_Box = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input"));
					sendKeys(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input", ActionValue);
					sOut = "True";
					break;
				case "Select":
					Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
					scrollIntoView(Cell);
					Cell.click();
					WebElement dropdown = webDriver.findElement(By.xpath("//descendant::ul[@role='listbox']"));
					List<WebElement> options = dropdown.findElements(By.tagName("li"));
					for (WebElement option : options)
					{
					    if (option.getText().equals(ActionValue))
					    {
					        option.click(); // click the desired option
					        break;
					    }
					}
					sOut = "True";
					break;
				case "Store":
					Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//child::span"));
					scrollIntoView(Cell);
					sOut = Cell.getAttribute("title");
					break;
					
				case "Click":
					Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+rColumn_number+"]"));
					scrollIntoView(Cell);
					Cell.click();
					sOut = "True";
					break;
				case "checkBox":
					Cell = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]"));
					scrollIntoView(Cell);
					for (int i = 1; i < 3; i++)  { Reusable.waitForpageloadmask(); }
					scrollIntoView(Cell);
					WebElement checkbox = webDriver.findElement(By.xpath(tXpath+"/tbody/tr["+row_number+"]/td["+aColumn_number+"]//input[contains(@id,'editServiceOrder')]"));
					checkbox.click();
					sOut = "True";
					break;
			}
			
			
			return "True";
		}
		
public String contactInfoEntry(String file_name, String Sheeet_Name, String iScript, String iSubScript, String Product_Name) throws IOException, InterruptedException {
			
			String sResult;
			String sProduct = null;
			String L3_Resilience_Type = DataMiner.fngetcolvalue(file_name, Sheeet_Name, iScript, iSubScript,"L3_Resilience_Type");
			
			ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow);
			waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow, 20);
			verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
			click(CPQQuoteSubmissionObj.QuoteSubmit.forwardArrow,"Forward Arrow");
			for (int i = 1; i < 5; i++) {Reusable.waitForpageloadmask(); }

			if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) { sProduct = Product_Name; } else { sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1"); }
			
			    waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.contactInformationLnk, 40);
			    click(CPQQuoteCreationObj.CopyQuote.contactInformationLnk);
				for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
				Reusable.Waittilljquesryupdated();
				verifyExists(CPQQuoteCreationObj.CopyQuote.additonalInfoTable);
				Reusable.Waittilljquesryupdated();

			
//			Calling the below method to fetch the quote ID
			String QuoteLineItem = Reusable.WebTableCellAction("Product", sProduct, "Quote Line Item Id","Store", null);
			if (QuoteLineItem.equalsIgnoreCase("False")){ return "False"; }
			
//			Calling the below function to enter A company name
			String Company_Name = DataMiner.fngetcolvalue(file_name, Sheeet_Name, iScript, iSubScript,"Company_Name");
			sResult = addContactInformationCPQ("Line Item ID", QuoteLineItem, "Company Name A End","Edit", Company_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			Calling the below function to enter B company name
			
			
			if (Product_Name.equals("EthernetLine") || Product_Name.equals("Wave")||L3_Resilience_Type.equalsIgnoreCase("Dual Access Unmanaged")
					||L3_Resilience_Type.equalsIgnoreCase("Dual Access Primary & Backup")
					||L3_Resilience_Type.equalsIgnoreCase("Dual Access Load Shared")) {
				sResult = addContactInformationCPQ("Line Item ID", QuoteLineItem, "Company Name B End","Edit", Company_Name);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
			
//			Calling below function to enter more character in service order 
			String PR_Type = DataMiner.fngetcolvalue(file_name, Sheeet_Name, iScript, iSubScript,"PR_Type");
			
			if(PR_Type.equalsIgnoreCase("Service Order Notes")) {
				String randomText =generateRandomText(file_name, Sheeet_Name, iScript, iSubScript);
				sResult = addContactInformationCPQ("Line Item ID", QuoteLineItem, "Edit Service Order Note","checkBox", null);
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service order pop has been opened");
				
				waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.serviceOrderNotesTxb, 40);
				sendKeys(CPQQuoteCreationObj.CopyQuote.serviceOrderNotesTxb,randomText);
				click(CPQQuoteCreationObj.CopyQuote.serviceOrdersubmitBtn);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service order Entered " +randomText.length()+" Charcaters");
					
					sResult = validateCPQErrorMsg();
					if (sResult.equalsIgnoreCase("False")) {
						sResult = addContactInformationCPQ("Line Item ID", QuoteLineItem, "Edit Service Order Note","Click", null);
						if (sResult.equalsIgnoreCase("False")){ return "False"; }
						sendKeys(CPQQuoteCreationObj.CopyQuote.serviceOrderNotesTxb,"Service Order");
						click(CPQQuoteCreationObj.CopyQuote.serviceOrdersubmitBtn);
						}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Service Order Text Box in CPQ is not visible, Please verify");
					System.out.println("Service Order Text Box in CPQ is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					return "False";
				}
						
		
			return "True";
			
		}


public String generateRandomText(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {

//	Initializing the Variable
	String characterCount = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Notes_Characters");

	 // chose a Character random from this String 
    String AlphaString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                + "abcdefghijklmnopqrstuvxyz"; 
    // create StringBuffer size of AlphaNumericString 
    StringBuilder sb = new StringBuilder(Integer.parseInt(characterCount)); 

    for (int i = 0; i < Integer.parseInt(characterCount); i++) { 
        // generate a random number between 
        // 0 to AlphaNumericString variable length 
        int index = (int)(AlphaString.length() 
                    * Math.random()); 
        // add Character one by one in end of sb 
        sb.append(AlphaString.charAt(index)); 
    } 

    return sb.toString(); 
}


public String validateCPQErrorMsg() throws IOException, InterruptedException {

     	waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.cpqSummaryErrorMsgElem, 40);
		String Main_Err = findWebElement(CPQQuoteCreationObj.CopyQuote.cpqSummaryErrorMsgElem).getText().trim();
		String Sub_Err = findWebElement(CPQQuoteCreationObj.CopyQuote.cpqErrorMsgElem).getText().trim();
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Process the quote due to the Error: "+Main_Err+" and "+Sub_Err);
		System.out.println("Unable to Process the quote due to the Error: "+Main_Err+" and "+Sub_Err);
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		return "False";
	
}

public String contactInfoEntryAddons(String file_name, String iScript, String iSubScript, String Product_Name) throws IOException, InterruptedException {

	String sResult;
	String sProduct = null;
	WebElement selectQuote;
	if(Product_Name.equalsIgnoreCase("ColtIpDomain")) {sProduct="IP Domain";}
	else if(Product_Name.equalsIgnoreCase("ColtIpGuardian")) {sProduct="IP Guardian";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {sProduct="IP Managed Virtual Firewall";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {sProduct="IP Managed Dedicated Firewall";}
	
	  waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.contactInformationLnk, 40);
	    click(CPQQuoteCreationObj.CopyQuote.contactInformationLnk);
		for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
		Reusable.Waittilljquesryupdated();
		verifyExists(CPQQuoteCreationObj.CopyQuote.additonalInfoTable);
		Reusable.Waittilljquesryupdated();


		verifyExists(CPQQuoteCreationObj.CopyQuote.additonalInfoTable);
		click(CPQQuoteCreationObj.CopyQuote.copyContactDetailsRadiobtn);
		for (int i = 1; i < 2; i++) { Reusable.waitForpageloadmask(); }
		int count =webDriver.findElements(By.xpath("//input[contains(@id,'selectJET')]")).size();
		for(int i=1; i<=count; i++) {
			selectQuote=webDriver.findElement(By.xpath("(//input[contains(@id,'selectJET')])["+i+"]"));
			selectQuote.click();
		
		}
		for (int i = 1; i < 2; i++) { Reusable.waitForpageloadmask(); }
		click(CPQQuoteCreationObj.CopyQuote.copyOrderingDetailsRadiobtn);
		click(CPQQuoteCreationObj.CopyQuote.siteContactRadiobtn);
		click(CPQQuoteCreationObj.CopyQuote.technicalContactAEndRadiobtn);
		click(CPQQuoteCreationObj.CopyQuote.electricianContactAEndRadiobtn);
		click(CPQQuoteCreationObj.CopyQuote.siteContactBEndRadiobtn);
		click(CPQQuoteCreationObj.CopyQuote.technicalContactBEndRadiobtn);
		click(CPQQuoteCreationObj.CopyQuote.electricianContactBEndRadiobtn);
		Reusable.waitForpageloadmask();
		waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.copyContactBtn, 40);
		click(CPQQuoteCreationObj.CopyQuote.copyContactBtn);
		for (int i = 1; i < 1; i++) { Reusable.waitForpageloadmask(); }
	
	return "True";
	
}

public String retriveServiceOrder(String Product_Name, int RowNumber) throws IOException, InterruptedException {

	String Service_Order;
	if(!Product_Name.contains("Container Model")) 
	{
		waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.orderDetailsCbx, 40);
	    ScrollIntoViewByString(CPQQuoteCreationObj.CopyQuote.orderDetailsCbx);
		{ Reusable.waitForpageloadmask(); }
		clickByAction(CPQQuoteCreationObj.CopyQuote.orderDetailsCbx);
		for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
	} else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "orderDetailsCbx in CPQ is not visible, Please verify");
		System.out.println("orderDetailsCbx in CPQ is not visible, Please verify");
		ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		return "False";
		}
		
//  capturing the service order for the respective product
	String sProduct = null;
	if (Product_Name.contains("Data -")||Product_Name.contains("Voice -")) { sProduct = Product_Name; } 
	else if(Product_Name.equalsIgnoreCase("Container Model")) {sProduct="Container Model";}
	else if(Product_Name.equalsIgnoreCase("ColtIpDomain")) {sProduct="IP Domain";}
	else if(Product_Name.equalsIgnoreCase("ColtIpGuardian")) {sProduct="IP Guardian";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {sProduct="IP Managed Virtual Firewall";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {sProduct="IP Managed Virtual Firewall";}
	else { sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1"); }
	
	if(sProduct.equalsIgnoreCase("Container Model")) {
		 Service_Order = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Work Item Reference No","Store", null, RowNumber).trim();
		if (Service_Order.equalsIgnoreCase("False")){ return "False"; }
	}else {
	 Service_Order = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Service Order No","Store", null, RowNumber).trim();
	if (Service_Order.equalsIgnoreCase("False")){ return "False"; }}
//	Printing the order reference
	System.out.println("Service Order for the product "+sProduct+" is "+Service_Order);
	ExtentTestManager.getTest().log(LogStatus.PASS, "Service Order for the product "+sProduct+" is "+Service_Order);

	
//  Unchecking the Order Details Checkbox
	if(!Product_Name.contains("Colt")) {
	ScrollIntoViewByString(CPQQuoteCreationObj.CopyQuote.orderDetailsCbx);
	clickByAction(CPQQuoteCreationObj.CopyQuote.orderDetailsCbx);    
	{ Reusable.waitForpageloadmask(); }
	}
	
	return Service_Order;
}

public String CopyQuoteOrderTypeAt(String testDataFile, String tsSheetName, String scriptNo, String dataSetNo, String Quote_ID ) throws Exception {
	
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Product_Name");
	String Copy_Quote_At = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Copy_Quote_At");
	String Copy_Type = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Copy_Type");
	String Product_Type = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Product_Type");
	String Reuse_Type = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Reuse_Type");

//	Quote_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");

	String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
	String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
	
	switch(Copy_Quote_At) {
	
	case "Created":
		System.out.println("Quote stage is "+quoteStage);
		
//		Create a copy quote if it is created
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			sResult = copyQuote(testDataFile, tsSheetName, scriptNo, dataSetNo);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

		if(!Product_Type.equalsIgnoreCase("Addon")) {	
		if (!Copy_Type.equalsIgnoreCase("Lineitem")) {
			
//			calling the below method reconfigure the product
			sResult = clickProductConfigurationBtn(Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			if((Reuse_Type.equalsIgnoreCase("ReuseBCP")) || (Reuse_Type.equalsIgnoreCase("ReuseOLO"))) {
//				Calling the below method for Reuse Configuration
				ReuseManualConfiguration(testDataFile, tsSheetName, scriptNo, dataSetNo);
				
			}
			
//			Add addtional Product data
			addtionalProductdata(testDataFile, tsSheetName, scriptNo, dataSetNo);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			Calling router type configuration for IP Access
			if(Product_Name.equalsIgnoreCase("ColtIpAccess")) {
//				Calling below method for router configuartion if it is IP Access 
				sResult = routerTypeConfiguration(testDataFile, tsSheetName, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			}
				
//			calling the below method to update and save the product in cpq
			sResult = updateSaveProductCPQ("Save");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			Quote_ID = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Copy_Quote_ID");
			
//			calling the below method to verify quote stage and status
			if(!Product_Name.equalsIgnoreCase("Wave")) 
			{
				sResult = verifyQuoteStage("Priced");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }	
			}
			else { verifyQuoteStage("SE Required"); }
			
		}else {
//			UnSelect the first line item grid
			sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, null,"Click", null,Integer.parseInt("1"));
			
			sResult=copyQuoteLineItemEntries(testDataFile, tsSheetName, scriptNo, dataSetNo,"Click");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
		}else {
			Quote_ID=copyQuoteAddonProducts(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
			if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		}
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", ");
		return Quote_ID;
		}
		break;
			
	case "Priced":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
		sResult=copyQuote(testDataFile, tsSheetName, scriptNo, dataSetNo);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		if(!Product_Type.equalsIgnoreCase("Addon")) {	
		if (!Copy_Type.equalsIgnoreCase("Lineitem")) {
			Quote_ID = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Copy_Quote_ID");
		}
		}else {				
			Quote_ID=copyQuoteAddonProducts(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
			if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Created");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }	
		
//		Calling the below method to update the configuration for the line items	
		clickUpdateConfigBtn();
		
		sResult = verifyQuoteStage("Priced");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
		return Quote_ID;
		}
		break;

		
	case "Waiting for 3rd Party":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			Quote_ID=copyQuoteWaitingfor3rdParty(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
		if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
			}
		break;
		
	case "Waiting for BCP":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			Quote_ID=copyQuoteWaitingforBCP(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
		if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		}
		break;
		
	case "To be Priced":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
		sResult=copyQuote(testDataFile, tsSheetName, scriptNo, dataSetNo);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
		if (!Copy_Type.equalsIgnoreCase("Lineitem")) {
			Quote_ID = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Copy_Quote_ID");
		}
		
		sResult = verifyQuoteStage("Created");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Calling the below method to update the configuration for the line items	
		clickUpdateConfigBtn();
		
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("To be Priced");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }	
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
		return Quote_ID;
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "copy quote is expected at "+Copy_Quote_At+" But Actual is "+quoteStage+", Please Verify");
			System.out.println("copy quote is expected at "+Copy_Quote_At+" But Actual is "+quoteStage+", Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
			}		
		
	case "Approved":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			Quote_ID=copyQuoteAtApporve(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
			if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
			}
		break;
		
		
		
	case "SE Required":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			Quote_ID=copyQuoteAtSERequired(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
			if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		}
		break;
			
	case "Copy MultiRequsts":
		System.out.println("Quote stage is "+quoteStage);
		Quote_ID=copyQuoteWithMultiRequests(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
		if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		break;
		
	/*case "Approved":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			Quote_ID=copyQuoteAtApproved(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
			if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		}
		break;*/
		
	case "Approval Denied":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			Quote_ID=copyQuoteApprovalDenied(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
			if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		}
		break;
		
	case "Estimated":
		System.out.println("Quote stage is "+quoteStage);
		if(Copy_Quote_At.equalsIgnoreCase(quoteStage)) {
			Quote_ID=copyQuoteEstimated(testDataFile, tsSheetName, scriptNo, dataSetNo,Quote_ID);
			if (Quote_ID.equalsIgnoreCase("False")){ return "False"; }
		}
		break;
	}

	
	return Quote_ID;
}

public String clickProductConfigurationBtnCreated(String sProduct_Name,String RowNumber) throws IOException, InterruptedException 
{

//	Initializing the Variable
	String sResult, Product_Name;
//	Selecting the product
//	Product_Name = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
	if (sProduct_Name.contains("Data -")||sProduct_Name.contains("Voice -")){ Product_Name = sProduct_Name; } 
	else if(sProduct_Name.equalsIgnoreCase("VPNNetwork")) {Product_Name="VPN Network";}
	else if(sProduct_Name.equalsIgnoreCase("ColtIpDomain")) {Product_Name="IP Domain";}
	else if(sProduct_Name.equalsIgnoreCase("ColtIpGuardian")) {Product_Name="IP Guardian";}
	else if(sProduct_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {Product_Name="IP Managed Virtual Firewall";}
	else if(sProduct_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {Product_Name="IP Managed Dedicated Firewall";}
	else { Product_Name = sProduct_Name.replaceAll("(?!^)([A-Z])", " $1"); }
	
//	sResult = Reusable.WebTableCellAction("Product", Product_Name, null,"Click", null);
//	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	System.out.println("Line Item will be selected");
	System.out.println("Product Name: "+Product_Name);
	Reusable.WaitforCPQloader();
	sResult = Reusable.MultiLineWebTableCellAction("Product", Product_Name, null,"Click", null,Integer.parseInt(RowNumber));
	if (sResult.equalsIgnoreCase("False")){ return "False";}
	
//	clicking on Reconfiguration link
	waitForElementToAppear(CPQQuoteCreationObj.engagementpf.reConfigureBtn, 120);
	ScrollIntoViewByString(CPQQuoteCreationObj.engagementpf.reConfigureBtn);
	verifyExists(CPQQuoteCreationObj.engagementpf.reConfigureBtn,"ReConfigure Button");
	Reusable.waitForpageloadmask();
	click(engagementpf.reConfigureBtn,"ReConfigure Button");
	Reusable.waitForpageloadmask();
	
	if (Product_Name.contains("Ethernet")||Product_Name.contains("Wave")) {
    		waitForElementToAppear(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk, 120);
    		verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
    		Reusable.waitForpageloadmask();
    		for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
			Reusable.waitForpageloadmask();
			click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
			Reusable.waitForpageloadmask(); 

    	}
	else if(Product_Name.equals("Colt Ip Access")) {
			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
			waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, 120);
			verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, "IP L3 Resilience Link");
			click(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, "IP L3 Resilience Link");
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
				
			/*waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, 50);
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn,"IPAccess Next Button");
			click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn,"IPAccess Next Button");*/
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask();}	    		
		}
		else if(Product_Name.equals("Colt Ip Domain")||Product_Name.equals("Colt Ip Guardian")
				||Product_Name.equals("Colt Managed Virtual Firewall")||Product_Name.equals("IP Managed Dedicated Firewall")) {
			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask();}
		}
	

	return "True";


}

public String copyQuoteLineItemEntries(String file_name, String Sheet_Name, String iScript, String iSubScript, String type) throws IOException, InterruptedException 
{

	
//	Initializing the Variables
	String sResult, Row_Value, Row_Range = null; int j = 0; int Temp_Row = 1;String refColName = null; 
	String sProduct;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String No_Of_Copies = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"No_Of_Copies");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	String Environment = g.getEnvironment();
	String BCN = DataMiner.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"BCN");
	String Company_Name = DataMiner.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"Company_Name");
	String Copy_Quote_At = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_At");
	String Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");
	String Product_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Type");

	
	if(Product_Name.equalsIgnoreCase("VPNNetwork")) {sProduct="VPN Network";}
	else if(Product_Name.equalsIgnoreCase("ColtIpDomain")) {sProduct="IP Domain";}
	else if(Product_Name.equalsIgnoreCase("ColtIpGuardian")) {sProduct="IP Guardian";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {sProduct="IP Managed Virtual Firewall";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {sProduct="IP Managed Dedicated Firewall";}
	else {sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");}
//	spliting the products
	
	
	String Rows[] = new String[Integer.parseInt(No_Of_Copies)+1];
	
	
//	Picking the Row Number of Each products
	for (j = 1; j <= Integer.parseInt(No_Of_Copies)+1; j++) {
		Row_Value = Reusable.MultiLineWebTableCellAction("Product",sProduct,"Product","GetRow",null,Temp_Row);		
		Rows[j-1] = Row_Value;
		Temp_Row = Integer.parseInt(Row_Value)+1;	
	}
	System.out.println("Row number is returned");
	
	switch(type) {
	case "Click":
		
//		Click on reconfigure using below method
		for (int k = 0; k < Integer.parseInt(No_Of_Copies)+1; k++) {
	
//		Below method to add product data if quote stage is created
		if(Copy_Quote_At.equalsIgnoreCase("Created")) {
			
//		calling the below method reconfigure the product	
		sResult = clickProductConfigurationBtnCreated(Product_Name,Rows[k]);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		if(!Product_Type.equalsIgnoreCase("Addon")) {	
//		Below method to add the addtional data
		addtionalProductdata(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
		}
		else if(Copy_Quote_At.equalsIgnoreCase("Estimated")) {
			
//			calling the below method reconfigure the product
			sResult= clickProductConfigurationBtn(Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			sResult = CopyQuoteoffnetConfiguration(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			Update and save the product
			sResult = updateSaveProductCPQ("Save");
			
//			calling the below method to verify quote stage and status
			sResult = verifyQuoteStage("Waiting for 3rd Party");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }	
			
//			calling the below method to verify quote stage and status
			sResult = logoutCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			calling the below method to configure the product
			close();
			openBrowser(g.getBrowser());		
			openurl(Configuration.EXPLORE_URL);
			sResult = ExpConfig.offnetExploreOperations(file_name, Sheet_Name, iScript, iSubScript);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
		
	//		Calling the Below method to perform C4C Login	
			close();
			openBrowser(g.getBrowser());		
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
	//		calling the below method to navigate to quotes from c4c main page
			navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
	//		calling the below method to search the quote from c4c
			sResult = searchQuoteC4C(Quote_ID);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }	
			
	//		calling the below method reconfigure the product
			sResult = clickProductConfigurationBtn(Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
		}
		else {
//			calling the below method reconfigure the product
			sResult= clickProductConfigurationBtn(Product_Name);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Update and save the product
		sResult = updateSaveProductCPQ("Save");
		}
		 break;
		
	
	case "Edit":
		if (Environment.equalsIgnoreCase("PRD")) { refColName = "Indicative Lead Time (Days)*"; } else { refColName = "Lead Time (Days)*"; }
		String Leadtime_Days = DataMiner.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"Leadtime_Days");
		for (int k = 0; k < Integer.parseInt(No_Of_Copies)+1; k++) {
		sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, refColName,"Edit", Leadtime_Days, Integer.parseInt(Rows[k]));
			}
		break;
		
	case "BCN":
		for (int k = 0; k < Integer.parseInt(No_Of_Copies)+1; k++) {
			Submit.addMultiLineBillingInformation(sProduct, BCN, Integer.parseInt(Rows[k]));
			Reusable.waitForpageloadmask();
			}
		break;
		
	case "Contact":
		for (int k = 0; k < Integer.parseInt(No_Of_Copies)+1; k++) {
			String pQuoteLineItem = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Quote Line Item Id","Store", null, Integer.parseInt(Rows[k]));
			sResult = addContactInformationCPQ("Line Item ID", pQuoteLineItem, "Company Name A End","Edit", Company_Name);
			if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
			sResult = addContactInformationCPQ("Line Item ID", pQuoteLineItem, "Company Name B End","Edit", Company_Name);
				}
			}
		break;
		
	case "ServiceOrder":
		String order = "";
		for (int k = 0; k < Integer.parseInt(No_Of_Copies)+1; k++) {
			String Service_Order = retriveServiceOrder(Product_Name, Integer.parseInt(Rows[k]));
			 order=Service_Order+"|"+order;
			}
		DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Service_Order", order);
		break;
		}
	
	return "True";
}

public String CopyQuoteoffnetConfiguration(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
//	Initializing the Variable
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
//	String Flow_Type = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Flow_Type");
	String sResult;
	
	switch(Product_Name) {
	
	case "EthernetLine": case "EthernetHub": case "Wave": case "EthernetSpoke":
	
		waitForElementToAppear(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk, 120);
		verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"'Site Details Link'");
		click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"'Site Details Link'");
		Reusable.waitForpageloadmask();
	    break;
	case "ColtIpAccess":
		waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, 20);
		verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, "Resiliancy Type List");
		Reusable.waitForpageloadmask();
		break;
	}
	
//	Below method is configure the offnet
//			Entering A-End details
			sResult = offnetEntriesForEstimated(file_name,Sheet_Name , iScript, iSubScript, "A_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//			Entering B-End details
			if (Product_Name.equalsIgnoreCase("EthernetLine")) {
				sResult = offnetEntriesForEstimated(file_name, Sheet_Name, iScript, iSubScript, "B_End");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
			
	return "True";
}

public String offnetEntriesForEstimated(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 
{

	
	//		Initializing the Variable
	String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
	String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
	String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");
	String Connection_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connection_Type");
	String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
	String exploreOptionsSubLsb = null;String offnetCheckRadioButton=null;
	
	switch (SiteName) {
	
	case "A_End":
		manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
		exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
		exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
		offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetAManualOffnetRdB;
		break;
		
	case "B_End":
		manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
		exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
		exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
		offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetBManualOffnetRdB;
		break;
	}
	if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
			Product_Name.equalsIgnoreCase("EthernetSpoke")&&!Product_Name.equalsIgnoreCase("ColtIpAccess")&&
			!Product_Name.equalsIgnoreCase("Wave")&&!Product_Name.equalsIgnoreCase("VPNNetwork")) 
	{
		verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
		click(manualEngagementBtn,"'Manual Engageent Button'");
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(exploreOptionsLsb, 20);
		verifyExists(exploreOptionsLsb,"'Explore Option List box'");
		isEnable(exploreOptionsLsb);
		Reusable.waitForpageloadmask();
		
		selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
		switchToFrame("exploreEngagementComponent_oob");				
	}
	
	if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
		
		
		manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualEngagementAEndBtn;
		exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndLst;
		exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndSubLst;

		
		click(manualEngagementBtn,"'Manual Engageent Button'");
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(exploreOptionsLsb,75);
		verifyExists(exploreOptionsLsb,"'Explore Option List box'");
		isEnable(exploreOptionsLsb);
		Reusable.waitForpageloadmask();
		
		verifyExists(exploreOptionsSubLsb,"'Explore Option Sub List box'");
		selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);

		switchToFrame("exploreEngagementComponent_oob");			
	}
	
	if (Product_Name.equalsIgnoreCase("VPNNetwork")) {
		
		manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnManualCheckBtn;
		exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreRequestVPN;
		
		waitForElementToAppear(offnetCheckBtn,75);
		verifyExists(offnetCheckBtn,"'Offnet Check Button'");
		scrollToViewNClick(offnetCheckBtn,"'Offnet Check Button'");			

		Reusable.waitForpageloadmask();
		
		click(manualEngagementBtn,"'Manual Engageent Button'");
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(exploreOptionsLsb,75);
		verifyExists(exploreOptionsLsb,"'Explore Option List box'");
		isEnable(exploreOptionsLsb);
		click(exploreOptionsLsb,"'Explore Option List box'");

		click("//li[normalize-space(.)='"+Explore_Options+"']", "'Click Explore Option'");

		selectByValueDIV(exploreOptionsLsb, Explore_Options,"'Select Explore Option'");

		Reusable.WaitforProdConfigLoader();	
		switchToFrame("exploreEngagementComponent_oob");	
	}
	
	if(!Product_Name.equalsIgnoreCase("Wave")) 
	{
		waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,75);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,"'Offnet Prioriry Text Box'");			
		
		String buliding_number=findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreBuldingNumberTxb).getAttribute("value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
		System.out.println("Explore Buliding Number is "+buliding_number);
					
		sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb, Priority);
		keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,Keys.ENTER);
		Reusable.waitForpageloadmask();
		
		if (Connection_Type.equalsIgnoreCase("DualEntry")) 
		{ 
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx); 
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx); 
		}
			
		waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,20);				
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'");
		isClickable(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn);
		Thread.sleep(2000);
		click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();

       
        verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
        click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
			
		waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,75);
		verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,"'Request ID'");

		String fullVal = findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem).getText();
		String[] bits = fullVal.split(":");
		String Request_ID = bits[bits.length-1].trim();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
		System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
//				Passing this request ID to testdata sheet
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
		switchToDefaultFrame();
			
		if (Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'"); 
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'");
			Reusable.WaitforProdConfigLoader();
		}
					
		else if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
		{ 
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"'IP Access Explore Close Button'"); 
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"'IP Access Explore Close Button'");
			Reusable.WaitforProdConfigLoader();
		} 
		else 
		{
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'"); 
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
			Reusable.WaitforProdConfigLoader();
		}			
	}
	if(Product_Name.contains("Ethernet")) 
	{
		waitForElementToAppear(offnetCheckRadioButton,60);
		verifyExists(offnetCheckRadioButton,"'Explore Close Button'");
		scrollIntoView(findWebElement(offnetCheckRadioButton));
		click(offnetCheckRadioButton,"'Explore Close Button'");
		Reusable.waitForpageloadmask();		
	}
		
	return "True";
	
}

public String copyQuoteAddonProducts(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws IOException, InterruptedException {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	
	String sProduct;
	if(Product_Name.equalsIgnoreCase("VPNNetwork")) {sProduct="VPN Network";}
	else if(Product_Name.equalsIgnoreCase("ColtIpDomain")) {sProduct="IP Domain";}
	else if(Product_Name.equalsIgnoreCase("ColtIpGuardian")) {sProduct="IP Guardian";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedVirtualFirewall")) {sProduct="IP Managed Virtual Firewall";}
	else if(Product_Name.equalsIgnoreCase("ColtManagedDedicatedFirewall")) {sProduct="IP Managed Dedicated Firewall";}
	else {sProduct= Product_Name.replaceAll("(?!^)([A-Z])", " $1");}
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	if(Copy_Type.equalsIgnoreCase("Lineitem")) {
//	Un Select the first line item grid
	sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, null,"Click", null,Integer.parseInt("1"));
	
	sResult=copyQuoteLineItemEntries(file_name, Sheet_Name, iScript, iSubScript,"Click");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	}else {
//		Capture copy quote id
		Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");

//		calling the below method reconfigure the product
		sResult = clickProductConfigurationBtn(Product_Name);
		if (sResult.equalsIgnoreCase("False")){ return "False";}
		
//		Update and save the product
		sResult = updateSaveProductCPQ("Save");
	}
	

	
	return Quote_ID;
		
}

public String copyQuoteWaitingfor3rdParty(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws IOException, InterruptedException {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
//	Quote_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");

	String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
	
	
//	calling the below method to verify quote stage and status
	sResult = logoutCPQ("Main");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	calling the below method to configure the product
	close();
	openBrowser(g.getBrowser());		
	openurl(Configuration.EXPLORE_URL);
	sResult = ExpConfig.offnetExploreOperations(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		

//		Calling the Below method to perform C4C Login	
	close();
	openBrowser(g.getBrowser());		
	openurl(Configuration.C4C_URL);
	C4CLogin.C4CLogin();
	if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
	
//	calling the below method to navigate to quotes from c4c main page
	navigateQuotesFromHomepage();
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	calling the below method to search the quote from c4c
	sResult = searchQuoteC4C(Quote_ID);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Waiting for 3rd Party");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	
	
//	Copy Quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	
	if (!Copy_Type.equalsIgnoreCase("Lineitem")) {
		click(CPQQuoteCreationObj.CopyQuote.offnetcopyQuoteOKBtn);
		Reusable.waitForpageloadmask();
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Created");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
		
//		calling the below method reconfigure the product
		sResult = clickProductConfigurationBtn(Product_Name);
		if (sResult.equalsIgnoreCase("False")){ return "False";}
		
//		calling the below method to update and save the product in cpq
		sResult =updateSaveProductCPQ("SAVE");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
	}else {
//		Un Select the first line item grid
		sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, null,"Click", null,Integer.parseInt("1"));
//		Below method for Copy Line Item Flow
		sResult=copyQuoteLineItemEntries(file_name, Sheet_Name, iScript, iSubScript,"Click");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Waiting for 3rd Party");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	
	
	System.out.println("copyQuote for "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote for  "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	
	return Quote_ID;

}

public String copyQuoteWaitingforBCP(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws IOException, InterruptedException {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		

//	calling the below method to verify quote stage and status
	sResult = logoutCPQ("Main");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }

	//		calling the below method to configure the product
	close();
	openBrowser(g.getBrowser());		
	openurl(Configuration.EXPLORE_URL);
	sResult = ExpConfig.nearnetExploreOperations(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		

//		Calling the Below method to perform C4C Login
	close();
	openBrowser(g.getBrowser());		
	openurl(Configuration.C4C_URL);
	C4CLogin.C4CLogin();
	if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }

//	calling the below method to navigate to quotes from c4c main page
	navigateQuotesFromHomepage();
	if (sResult.equalsIgnoreCase("False")){ return "False"; }

//	calling the below method to search the quote from c4c
	sResult = searchQuoteC4C(Quote_ID);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	

//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Waiting for BCP");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }

//	Below Method to process the copy Quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }

//	Below method to process the copy quote configuarion
	if (!Copy_Type.equalsIgnoreCase("Lineitem")) {
		click(CPQQuoteCreationObj.CopyQuote.offnetcopyQuoteOKBtn);
		Reusable.waitForpageloadmask();
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
	
//	calling the below method reconfigure the product
	sResult = clickProductConfigurationBtn(Product_Name);
	if (sResult.equalsIgnoreCase("False")){ return "False";}
	
//	calling the below method to update and save the product in cpq
	sResult =updateSaveProductCPQ("Save");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	}else {
//	Un Select the first line item grid
	sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, null,"Click", null,Integer.parseInt("1"));
	
	sResult=copyQuoteLineItemEntries(file_name, Sheet_Name, iScript, iSubScript,"Click");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	}


//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Priced");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	

	System.out.println("copyQuote for "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote for  "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	return Quote_ID;

}

public String copyQuoteAtApporve(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws Exception {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	
	//Below method to process the copy quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	if(!Copy_Type.equalsIgnoreCase("LineItem")) {
		
	//Get the Copy QuoteID
	Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
	
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Calling the below method to update the configuration for the line items	
	clickUpdateConfigBtn();
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Priced");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	
	
//  calling the below method to move the quote status to Commercial Approval
	sResult = SubmitForApproval();
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Approved");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	}
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Calling the below method to update the configuration for the line items	
	clickUpdateConfigBtn();
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Approved");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }

	System.out.println("copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID);
	ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	return Quote_ID;
	
}

public String copyQuoteAtSERequired(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws Exception {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	String SEEngagement_detail = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"SEEngagement_detail");
	
	//Below method to process the copy quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
	if(!Copy_Type.equalsIgnoreCase("LineItem")) {
	//Get the Copy QuoteID
	Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
	
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Calling the below method to update the configuration for the line items	
	clickUpdateConfigBtn();
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("SE Required");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	
	
//  calling the below method to configure SE in Opportunity
	C4CSEConfiguration(SEEngagement_detail,Quote_ID);
	navigateQuotesFromHomepage(file_name, Sheet_Name, iScript, iSubScript,Quote_ID);
	
//	calling the below method to Approve the Quote as SE User	
	CPQLogin.CPQLogout();
	close();
	openBrowser(g.getBrowser());
	openurl(Configuration.CPQ_URL);
	CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
	CPQLogin.SelectQuote(file_name, Sheet_Name, iScript, iSubScript);
	SEEngagement(file_name, Sheet_Name, iScript, iSubScript);
	
	CPQLogin.CPQLogout();
	close();
	openBrowser(g.getBrowser());
	openurl(Configuration.CPQ_URL);
	CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
	CPQLogin.SelectQuote(file_name, Sheet_Name, iScript, iSubScript);
	
//	calling the below method to verify the stage of the Quote
	sResult = verifyQuoteStage("Priced");
	if (sResult.equalsIgnoreCase("False")){  return "False"; }
	
	}
	
	/*
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Calling the below method to update the configuration for the line items	
	clickUpdateConfigBtn();
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("SE Required");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	*/
	System.out.println("copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID);
	ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	return Quote_ID;
}

/*public String copyQuoteAtCSTReview(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws IOException, InterruptedException, Exception {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	
//	calling the below method to verify quote stage and status
	sResult = logoutCPQ("Main");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Calling the Below method to perform C4C Login	
	close();
	openBrowser(g.getBrowser());		
	openurl(Configuration.C4C_URL);
	C4CLogin.C4CLogin();
	if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
	
//	calling the below method to navigate to quotes from c4c main page
	navigateQuotesFromHomepage();
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	calling the below method to search the quote from c4c
	sResult = searchQuoteC4C(Quote_ID);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	//Below method to process the copy quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
	if(!Copy_Type.equalsIgnoreCase("LineItem")) {
	//Get the Copy QuoteID
	Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Priced");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }	
	
//  calling the below method to move the quote status to Commercial Approval
	sResult = SubmitForApproval();
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	calling the below method to send quote for Technical approval
	sResult = Submit4TechnicalApproval();
	if (sResult.equalsIgnoreCase("False")){  return "False"; }
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("SE Technical Review");
	if (sResult.equalsIgnoreCase("False")){  return "False"; }	
	
//	calling the below method to add Legal and Technical Contact Info
	Approvals.SwitchCPQUser("SE_User", Quote_ID);
	if (sResult.equalsIgnoreCase("False")){  return "False"; }	
	
	
//	calling the below method to verify quote stage and status
	sResult = Approvals.QuoteSEApproval(file_name, Sheet_Name, iScript, iSubScript,"");
	if (sResult.equalsIgnoreCase("False")){  return "False"; }	
	}
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("CST Technical Review");
	if (sResult.equalsIgnoreCase("False")){  return "False"; }	

	System.out.println("copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID);
	ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	return Quote_ID;
}*/

/*public String copyQuoteAtApproved(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws IOException, InterruptedException, Exception {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	String Environment = DataMiner.fngetconfigvalue(file_name, "Environment");
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	
	//Below method to process the copy quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
	if (!Copy_Type.equalsIgnoreCase("LineItem")) {
		//Get the Copy QuoteID
		Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
		
//		calling the below method to verify quote stage and status		
		sResult = verifyQuoteStage("Created");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Calling the below method to update the configuration for the line items	
		clickUpdateConfigBtn();
		
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Priced");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }	
		
//      calling the below method to move the quote status to Commercial Approval
		sResult = SubmitForApproval();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Approved");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		calling the below method to send quote for Technical approval
		sResult = Submit4TechnicalApproval();
		if (sResult.equalsIgnoreCase("False")){  return "False"; }
		
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("SE Technical Review");
		if (sResult.equalsIgnoreCase("False")){  return "False"; }	
		
//		calling the below method to add Legal and Technical Contact Info
		Approvals.SwitchCPQUser("SE_User", Quote_ID);
		if (sResult.equalsIgnoreCase("False")){  return "False"; }	
		
//		calling the below method to verify quote stage and status
		sResult = Approvals.QuoteSEApproval(file_name, Sheet_Name, iScript, iSubScript,"");
		if (sResult.equalsIgnoreCase("False")){  return "False"; }	
		
//		calling the below method to login to cpq as cst user
		Approvals.SwitchCPQUser("CST_User", Quote_ID);
		if (sResult.equalsIgnoreCase("False")){  return "False"; }	
		
		
//		Calling the below method to enter Line item notes
//		Picking up the rows if the copy quote type is line item and copy quote at approved and ordered
		String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		String refColName = null; 
		if (Environment.equalsIgnoreCase("PRD")) { refColName = "Indicative Lead Time (Days)*"; } else { refColName = "Lead Time (Days)*"; }
		String Leadtime_Days = DataMiner.fngetcolvalue(file_name, "Other_Items", iScript, iSubScript,"Leadtime_Days");
			sResult = Reusable.WebTableCellAction("Product", sProduct, refColName,"Edit", Leadtime_Days);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Calling the below method to save the details
		sResult = saveCPQ("Main");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		calling the below method to verify quote stage and status
		sResult = Approvals.QuoteCSTApproval(file_name, Sheet_Name, iScript, iSubScript,"");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }	
		
//		calling the below method to verify quote stage and status
		sResult = logoutCPQ("Main");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }	
		
//		Calling the Below method to perform C4C Login
		
		if (Environment.equalsIgnoreCase("RFS")) { Thread.sleep(3000); }
//		Calling the Below method to perform C4C Login	
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.C4C_URL);
		C4CLogin.C4CLogin();
		if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
		
//		calling the below method to navigate to quotes from c4c main page
		navigateQuotesFromHomepage();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		calling the below method to search the quote from c4c
		sResult = searchQuoteC4C(Quote_ID);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
	}
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Calling the below method to update the configuration for the line items	
	clickUpdateConfigBtn();
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Approved");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }

	System.out.println("copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID);
	ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	return Quote_ID;
}*/

public String copyQuoteApprovalDenied(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws Exception {

	
//	Initializing the Variable
	String sResult;
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
	String Discount_Percentage = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Discount_Percentage");
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	
//	Below method to the copy quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	//Get the Copy QuoteID
	if(!Copy_Type.equalsIgnoreCase("LineItem")) {	
	Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
	}
	
//	calling the below method to verify quote stage and status		
	sResult = verifyQuoteStage("Created");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Calling the below method to update the configuration for the line items	
	clickUpdateConfigBtn();
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Priced");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	calling the below method to verify quote stage and status
	sResult = SubmitForApproval();
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	if (Integer.parseInt(Discount_Percentage) > 5) {
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Pending Governance Approval");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	if (Integer.parseInt(Discount_Percentage) > 10) {
//		calling the below method to switch to VP sales user
		Approvals.SwitchCPQUser("VPSales1_User", Quote_ID);
		if (sResult.equalsIgnoreCase("False")){  return "False"; }	
		
		} else {
//		calling the below method to switch to VP sales user
			Approvals.SwitchCPQUser("VPSales2_User", Quote_ID);
			if (sResult.equalsIgnoreCase("False")){  return "False"; }		
	
		}
			
//		calling the below method to switch to VP sales user
		sResult = quoteDiscountGovernanceApproval("Approve");
		if (sResult.equalsIgnoreCase("False")){ return "False";  }
			
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Approved");
		if (sResult.equalsIgnoreCase("False")){  return "False";  }
			
//		calling the below method to verify quote stage and status
		sResult = logoutCPQ("Main");
		if (sResult.equalsIgnoreCase("False")){ return "False";  }	
			
//		Calling the Below method to perform C4C Login
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.C4C_URL);
		C4CLogin.C4CLogin();
		if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
			
//		calling the below method to navigate to quotes from c4c main page
		navigateQuotesFromHomepage();
		if (sResult.equalsIgnoreCase("False")){  return "False";  }
			
//		calling the below method to search the quote from c4c
		sResult = searchQuoteC4C(Quote_ID);
		if (sResult.equalsIgnoreCase("False")){  return "False";  }
			
	} 
return Quote_ID;
	
}

public String copyQuoteEstimated(String file_name, String Sheet_Name, String iScript, String iSubScript, String Quote_ID) throws IOException, InterruptedException {

//	Initializing the Variable
	String sResult;
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
	String Copy_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Type");
//	Quote_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");

	String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
	
//	calling the below method to verify quote stage and status
	sResult = verifyQuoteStage("Estimated");
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
//	Copy Quote
	sResult=copyQuote(file_name, Sheet_Name, iScript, iSubScript);
	if (sResult.equalsIgnoreCase("False")){ return "False"; }
	
	if(Copy_Type.equalsIgnoreCase("Lineitem")) {
		
		sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, null,"Click", null,Integer.parseInt("1"));
		sResult=copyQuoteLineItemEntries(file_name, Sheet_Name, iScript, iSubScript,"Click");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
	}else {
		Quote_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Copy_Quote_ID");
		
//		calling the below method reconfigure the product
		sResult = clickProductConfigurationBtn(Product_Name);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Below method for offnetFlow
		sResult = CopyQuoteoffnetConfiguration(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		Update and save the product
		sResult = updateSaveProductCPQ("Save");		
		
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Waiting for 3rd Party");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }	
		
//		calling the below method to verify quote stage and status
		sResult = logoutCPQ("Main");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//		calling the below method to configure the product
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.EXPLORE_URL);
		sResult = ExpConfig.offnetExploreOperations(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
	
//		Calling the Below method to perform C4C Login	
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.C4C_URL);
		C4CLogin.C4CLogin();
		if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test");  }
		
//		calling the below method to navigate to quotes from c4c main page
		navigateQuotesFromHomepage();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		calling the below method to search the quote from c4c
		sResult = searchQuoteC4C(Quote_ID);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }	
		
//		calling the below method reconfigure the product
		sResult = clickProductConfigurationBtn(Product_Name);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
//		calling the below method to update and save the product in cpq
		sResult = updateSaveProductCPQ("Save");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
	}
	
	System.out.println("copyQuote for "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	ExtentTestManager.getTest().log(LogStatus.PASS, "copyQuote for  "+Copy_Type+" is Successfull and quote id is "+Quote_ID+", Please Verify");
	
	return Quote_ID;

}

public String quoteDiscountGovernanceApproval(String require) throws IOException, InterruptedException {
	 
	String disccountAprvRject = null;
	switch(require.toUpperCase()) {
	case "APPROVE":
		disccountAprvRject=CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn;
		break;
	case "REJECT":
		disccountAprvRject=CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn;
		break;
	}
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink, 60);
		Reusable.WaitforCPQloader();
		verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"Approval page link'");
		click(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"'Approval page link'");
		Reusable.WaitforCPQloader();
		Reusable.Waittilljquesryupdated();
		waitForElementToAppear(disccountAprvRject, 40);
		verifyExists(disccountAprvRject,"Approve or Reject Button");
		//scrollToViewNClick(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn, "Approve Button");
		ScrollIntoViewByString(disccountAprvRject);
		verifyExists(disccountAprvRject,"Approve or Reject Button");
		javaScriptclick(disccountAprvRject,"Approve or Reject Button");
		Reusable.WaitforCPQloader();
		Reusable.Waittilljquesryupdated();
		//verifyDoesNotExist(disccountAprvRject);
		//Reusable.WaitforCPQloader();
		//Reusable.Waittilljquesryupdated();
		
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"'General Information Link'");
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"General Information Link");
		return "True";
}

public String discountingProcess(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
{
	String Discount_Percentage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Discount_Percentage");

	waitForElementToAppear(CPQQuoteCreationObj.discountingQuotePage.quoteLnk, 20);
	verifyExists(CPQQuoteCreationObj.discountingQuotePage.quoteLnk,"'QuoteLink'");
	click(CPQQuoteCreationObj.discountingQuotePage.quoteLnk,"'QuoteLink'");
					
	Reusable.waitForpageloadmask();
	Reusable.waitForpageloadmask();
	
	waitForElementToAppear(CPQQuoteCreationObj.discountingQuotePage.basePriceNRCDiscountTxb, 60);
	verifyExists(CPQQuoteCreationObj.discountingQuotePage.basePriceNRCDiscountTxb,"'BasePriceNRCTab'");
	sendKeys(CPQQuoteCreationObj.discountingQuotePage.basePriceNRCDiscountTxb,Discount_Percentage,"'BasePriceNRCTab'");
	waitForElementToAppear(CPQQuoteCreationObj.discountingQuotePage.basePriceMRCDiscountTxb, 60);
	verifyExists(CPQQuoteCreationObj.discountingQuotePage.basePriceMRCDiscountTxb,"'BasePriceMRCTab'");
	sendKeys(CPQQuoteCreationObj.discountingQuotePage.basePriceMRCDiscountTxb,Discount_Percentage,"'BasePriceMRCTab'");
	verifyExists(CPQQuoteCreationObj.discountingQuotePage.calculateDiscountBtn,"Calculate Discount Button");
	click(CPQQuoteCreationObj.discountingQuotePage.calculateDiscountBtn,"Calculate Discount Button");
	Reusable.waitForpageloadmask();
	ExtentTestManager.getTest().log(LogStatus.PASS, "Discount with "+Discount_Percentage+" percentage has set properly for the Quote");
	System.out.println("Discount with "+Discount_Percentage+" percentage has set properly for the Quote");
	return "True";
		
}	

public String validateACVTCVValues(String file_name, String Sheet_Name, String iScript, String iSubScript, String quoteStage) throws IOException, InterruptedException 
{
//	Initializing the Variable
	String acvValue,tcvValue,acvLineItem,tcvLineItem;
	String PR_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PR_Type");
	String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");

	String quoteStageValue = getAttributeFrom(CPQQuoteCreationObj.AddProducts.quoteStageElem,"value");
	String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
	
	switch(quoteStage) {
	case "Priced":
		if(quoteStage.equalsIgnoreCase(quoteStageValue)) {
			verifyExists(CPQQuoteCreationObj.PLTabInfo.plLnk,"P&L Link");
			ScrollIntoViewByString(CPQQuoteCreationObj.PLTabInfo.plLnk);
			click(CPQQuoteCreationObj.PLTabInfo.plLnk,"P&L Link");
			
				 
			acvValue=getAttributeFrom(CPQQuoteCreationObj.PLTabInfo.acvValueElem,"value");
			tcvValue=getAttributeFrom(CPQQuoteCreationObj.PLTabInfo.tcvValueElem,"value");
			System.out.println(Product_Name+" ACV value is"+acvValue);
			System.out.println(Product_Name+" TCV value is"+tcvValue);
			acvLineItem = Reusable.WebTableCellAction("Product", sProduct, "ACV","Store", null).trim();
			tcvLineItem = Reusable.WebTableCellAction("Product", sProduct, "TCV (Net)","Store", null).trim();
			System.out.println(Product_Name+" Line Item ACV value is"+acvLineItem);
			System.out.println(Product_Name+" Line Item TCV value is"+tcvLineItem);
			if(acvValue.equalsIgnoreCase(acvLineItem)&&tcvValue.equalsIgnoreCase(tcvLineItem)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, Product_Name+" P&L Tab ACV value "+acvValue+" is equal to Line Item ACV "+acvLineItem);
				ExtentTestManager.getTest().log(LogStatus.PASS, Product_Name+" P&L Tab TCV value "+tcvValue+" is equal to Line Item TCV "+tcvLineItem);
				System.out.println("P&L Tab ACV value "+acvValue+" is equal to Line Item ACV "+acvLineItem);
				System.out.println("P&L Tab TCV value "+tcvValue+" is equal to Line Item TCV "+tcvLineItem);
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, Product_Name+" P&L Tab ACV value "+acvValue+" is equal to Line Item ACV "+acvLineItem+" Please verify");
			    ExtentTestManager.getTest().log(LogStatus.FAIL, Product_Name+" P&L Tab TCV value "+tcvValue+" is equal to Line Item TCV "+tcvLineItem+" Please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					return "False";
			}			
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote Stage expected is "+quoteStage+" But Actual is "+quoteStageValue+", Please Verify");
			System.out.println("Quote Stage expected is "+quoteStage+" But Actual is "+quoteStageValue+", Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
		break;
	case "SE Technical Review":
		if(quoteStage.equalsIgnoreCase(quoteStageValue)) {
			
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote Stage expected is "+quoteStage+" But Actual is "+quoteStageValue+", Please Verify");
			System.out.println("Quote Stage expected is "+quoteStage+" But Actual is "+quoteStageValue+", Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
	}
	return "True";
}
	public String submitForTechnicalApprovalForPR(String file_name, String Sheet_Name, String iScript, String iSubScript) throws Exception
	{
	//**********************Submission for Technical Approval*******************************	
		
		String PR_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"PR_Type");
		String InternalNotes = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"InternalNotes");
	
		Reusable.waitForpageloadmask();
	
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
	
		Reusable.WaitforCPQloader();
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprCbxElem,20);
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprCbxElem);
		Reusable.WaitforCPQloader();
	
		if(PR_Type.equalsIgnoreCase("InternalNotes")) {
			verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.addInternalNotesBtn,"AddInternal Notes Button");
			click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.addInternalNotesBtn,"AddInternal Notes Button");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Tap on Add InternalNotes Button");
			verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.addInternalNotesTxb, "Add Internal Notes Text Box");
			sendKeys(CPQQuoteCreationObj.SubmissionforTechnicalApproval.addInternalNotesTxb, InternalNotes);
			verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.addBtn,"Add Button");
			click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.addBtn,"Add Button");
			verifyDoesNotExist(CPQQuoteCreationObj.SubmissionforTechnicalApproval.addBtn);
			ExtentTestManager.getTest().log(LogStatus.PASS, "Entered Internal Notes "+InternalNotes+ " and submit");
		}
	
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.readyForTechApprovalCbx, "Ready For Technical Approval Checkbox");
		Reusable.WaitforCPQloader();
	
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, 10);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.SubmitForTechApplBtn, "Submit For Technical Approval Button");
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();		
		waitForAjax();
	
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		scrollUp();
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		return "True";
	}
	
	public String assignQuoteToDPUser(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
		
		String sResult = null;
		String[] DPUser = Configuration.DPUser_Username.split("\\.");
		String DPUser1= DPUser[0]+" "+DPUser[1].replace("@colt", "");
		System.out.println("DPUser1: "+DPUser1);
		waitForElementToAppear(CPQQuoteCreationObj.PLTabInfo.plLnk, 60);
        verifyExists(CPQQuoteCreationObj.PLTabInfo.plLnk,"'P&L Tab'");
        click(CPQQuoteCreationObj.PLTabInfo.plLnk,"'P&L Tab'");
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.PLTabInfo.dealPriceReviewLbl,"Deal Price Review Label");
		verifyExists(CPQQuoteCreationObj.PLTabInfo.dealPriceTeamLbl,"Deal Price Team Label");
		verifyExists(CPQQuoteCreationObj.PLTabInfo.DPUserLst,"DP User List Dropdown");
		ScrollIntoViewByString(CPQQuoteCreationObj.PLTabInfo.DPUserLst);
		click(CPQQuoteCreationObj.PLTabInfo.DPUserLst,"'P&L Tab'");
		Reusable.waitForpageloadmask();
		
		//click(CPQQuoteCreationObj.PLTabInfo.DPUserTxb,"UserName TextBox");
		sendKeys(CPQQuoteCreationObj.PLTabInfo.DPUserTxb,DPUser1,"UserName TextBox");		
		Reusable.waitForpageloadmask();
		ClickonElementByString("//div[@data-value='"+DPUser1+"']", 20);
		//selectByValueDIV(CPQQuoteCreationObj.PLTabInfo.DPUserLst,CPQQuoteCreationObj.PLTabInfo.DPUserSubLst,DPUser);
        Reusable.waitForpageloadmask();	
        verifyExists(CPQQuoteCreationObj.PLTabInfo.assignQuote,"Assign Quote Button");
        click(CPQQuoteCreationObj.PLTabInfo.assignQuote,"Assign Quote Button");
        Reusable.WaitforCPQloader();
        
        waitForElementToAppear(CPQQuoteCreationObj.PLTabInfo.releaseQuote, 120);
        
        verifyExists(CPQQuoteCreationObj.PLTabInfo.pricePositioning,"pricePositioning Text Area");
		ScrollIntoViewByString(CPQQuoteCreationObj.PLTabInfo.pricePositioning);
		sendKeys(CPQQuoteCreationObj.PLTabInfo.pricePositioning,"Automation","pricePositioning Text Area");
		
		verifyExists(CPQQuoteCreationObj.PLTabInfo.commercialRisk,"commercialRisk Text Area");
		ScrollIntoViewByString(CPQQuoteCreationObj.PLTabInfo.commercialRisk);
		sendKeys(CPQQuoteCreationObj.PLTabInfo.commercialRisk,"Automation","commercialRisk Text Area");
        
//			Calling the below method to save the details
			sResult = saveCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }			
			
		return "True";
	}
	
	public String selectCPQProductFeatures(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
		
//		Initializing the Variable
		String sResult;
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
		//Update the product before Save
		
		
		switch (Product_Name.toUpperCase()) {
			
		case "ETHERNETLINE": case "ETHERNETSPOKE": case "ETHERNETHUB": case "WAVE":
						
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
			verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
			ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
			click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationAEndCbx, 60);
			verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");
		    click(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");		        
		    Reusable.waitForpageloadmask();
		        
		    waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningAEndCbx, 60);
		    verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningAEndCbx,"'Long Lining AEnd Check Box'");
		    click(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningAEndCbx,"'Long Lining AEnd AEnd Check Box'");		        
		    Reusable.waitForpageloadmask();
		    
			if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave"))
			{
				waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationBEndCbx, 60);
				verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationBEndCbx,"'Outside Business Hours Installation BEnd Check Box'");
		        click(CPQQuoteCreationObj.Onnet_DualEntry_Features.OutsideBusinessHoursInstallationBEndCbx,"'Outside Business Hours Installation BEnd Check Box'");		        
		        Reusable.waitForpageloadmask();
		        
		        waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningBEndCbx, 60);
			  	verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningBEndCbx,"'Long Lining BEnd Check Box'");
		        click(CPQQuoteCreationObj.Onnet_DualEntry_Features.LongLiningBEndCbx,"'Long Lining BEnd AEnd Check Box'");		        
		        Reusable.waitForpageloadmask();
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "featuresLnk is not visible in CPQ, please verify");
				System.out.println("featuresLnk is not visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
			break;
			
		case "COLTIPACCESS":
			if (waitForElementToBeVisible(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx, 120)) 
			{
				waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx, 60);
				verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");
			    click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipOutsideBusinessHoursInstallationAEndCbx,"'Outside Business Hours Installation AEnd Check Box'");		        
			    Reusable.waitForpageloadmask();
			        
			    waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipLongLiningAEndCbx, 60);
			    verifyExists(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipLongLiningAEndCbx,"'Long Lining AEnd Check Box'");
			    click(CPQQuoteCreationObj.Onnet_DualEntry_Features.ipLongLiningAEndCbx,"'Long Lining AEnd AEnd Check Box'");		        
			    Reusable.waitForpageloadmask();
			}
			else 
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "SiteAddons is not visible in CPQ, please verify");
				System.out.println("SiteAddons is not visible in CPQ, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
			break;
			
		case "VPNNETWORK":
			if (waitForElementToBeVisible(CPQQuoteCreationObj.VPNNetwork.VPNAddOnLink, 120)) 
			{
				waitForElementToAppear(CPQQuoteCreationObj.VPNNetwork.VPNOusideBusinessCbx, 60);
				verifyExists(CPQQuoteCreationObj.VPNNetwork.VPNOusideBusinessCbx,"'Outside Business Hours Installation AEnd Check Box'");
			    click(CPQQuoteCreationObj.VPNNetwork.VPNOusideBusinessCbx,"'Outside Business Hours Installation AEnd Check Box'");		        
			    Reusable.waitForpageloadmask();
			        
			    waitForElementToAppear(CPQQuoteCreationObj.VPNNetwork.VPNLongLiningCbx, 60);
			    verifyExists(CPQQuoteCreationObj.VPNNetwork.VPNLongLiningCbx,"'Long Lining AEnd Check Box'");
			    click(CPQQuoteCreationObj.VPNNetwork.VPNLongLiningCbx,"'Long Lining AEnd AEnd Check Box'");		        
			    Reusable.waitForpageloadmask();
			}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "VPN Add on is not visible in CPQ, please verify");
			System.out.println("VPN Add on is not visible in CPQ, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
			}
			break;
		}
		return "True";
	}
	
	public String VerifyPricesInViewPrice(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{

		int i, Iter;
		
		String Segment=DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Segment_Price");
		String NRCValue=DataMiner.fngetcolvalue(file_name,Sheet_Name, iScript, iSubScript,"NRC_Value");
		String MRCValue=DataMiner.fngetcolvalue(file_name,Sheet_Name, iScript, iSubScript,"MRC_Value");
        WEB_DRIVER_THREAD_LOCAL.get();
        
       // double d_NRCValue=Double.parseDouble(NRCValue);
      //  double d_MRCValue=Double.parseDouble(MRCValue);
        
		if (NRCValue.equals("5000")) { Iter = 50; } else { Iter = 2; };
		
		switch(Segment){
		
		case "CLTP - Google":{	
			
			if (isVisible(CPQQuoteCreationObj.ViewPrices.WarningMessage)) {
				verifyExists(CPQQuoteCreationObj.ViewPrices.WarningMessage,"Verify Warning Message");
			}
		verifyExists(CPQQuoteCreationObj.ViewPrices.ViewPriceTab,"Verify View Price Tab");
		click(CPQQuoteCreationObj.ViewPrices.ViewPriceTab, "Click on View Price Tab");
		Reusable.waitForpageloadmask();
		
		waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.PricingSegment, 120);
		verifyExists(CPQQuoteCreationObj.ViewPrices.PricingSegment,"Verify Pricing Segment for 'CLTP - Google'");
		
		
		verifyExists(CPQQuoteCreationObj.ViewPrices.viewAllContractualPricesBtn,"Verify view All Contractual Prices Btn");
		click(CPQQuoteCreationObj.ViewPrices.viewAllContractualPricesBtn, "Click on view All Contractual Prices Btn");
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.ViewPrices.pricesPopup,"Verify Prices popup");
		verifyExists(CPQQuoteCreationObj.ViewPrices.SelectPricingCriteria1,"Verify Pricing Criteria");
		click(CPQQuoteCreationObj.ViewPrices.SelectPricingCriteria1,"Click on Pricing Criteria");
		verifyExists(CPQQuoteCreationObj.ViewPrices.SelectPricingCriteria2,"Pricing Criteria present on Popup");
		click(CPQQuoteCreationObj.ViewPrices.SelectPricingCriteria2,"Click on Pricing Criteria");
		waitForAjax();
			//Comparing NRC Values
		ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.NRCValues);
		
			if (waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.NRCValues, 100)) {
				Reusable.WaitforCPQloader();
				String NRCValueText= getTextFrom(CPQQuoteCreationObj.ViewPrices.NRCValues);
				// double d_NRCValueText=Double.parseDouble(NRCValueText);
				 System.out.println("NRC Value is"+NRCValueText);
				for (i = 1; i <= Iter; i++) {
					if(NRCValueText.equalsIgnoreCase(NRCValue))  {
						ExtentTestManager.getTest().log(LogStatus.PASS, "NRC Value is set to "+NRCValueText);
						Reusable.WaitforCPQloader();
						System.out.println("NRC Value is set to "+NRCValueText);
						break;
					} else {
						Thread.sleep(10000);
						Reusable.WaitforCPQloader();
						click(CPQQuoteCreationObj.ViewPrices.NRCValues);
						NRCValueText = findWebElement(CPQQuoteCreationObj.ViewPrices.NRCValues).getAttribute("value");
						scrollIntoTop();
						continue;
					}
				}
				if (i > Iter) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "NRC Value is still set as "+NRCValueText+ " but the Expected status is "+NRCValue);
					System.out.println("Quote stage is still set as "+NRCValueText+ " but the Expected status is "+NRCValue);
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "NRC Value is not visible, Please Verify");
				System.out.println("NRC Value is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}
			//Comparing MRC Values
			ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.MRCValues);
			if (waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.MRCValues, 100)) {
				Reusable.WaitforCPQloader();
				String MRCValue_Field = getTextFrom(CPQQuoteCreationObj.ViewPrices.MRCValues);
				// double d_MRCValue_Field=Double.parseDouble(MRCValue_Field);
				System.out.println("NRC Value is"+MRCValue_Field);
				for (i = 1; i <= Iter; i++) {
					if(MRCValue_Field.equalsIgnoreCase(MRCValue)) {
						ExtentTestManager.getTest().log(LogStatus.PASS, "MRC Value is set to "+MRCValue_Field);
						Reusable.WaitforCPQloader();
						System.out.println("MRC Value is set to "+MRCValue_Field);
						break;
					} else {
						
						Reusable.WaitforCPQloader();
						click(CPQQuoteCreationObj.ViewPrices.MRCValues);
						MRCValue_Field = findWebElement(CPQQuoteCreationObj.ViewPrices.MRCValues).getAttribute("value");
						scrollIntoTop();
						continue;
					}
				}
				if (i > Iter) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "MRC Value is still set as "+MRCValue_Field+ " but the Expected status is "+MRCValue);
					System.out.println("MRC Value is still set as "+MRCValue_Field+ " but the Expected status is "+MRCValue);
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "MRC Value is not visible, Please Verify");
				System.out.println("MRC Value is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}
			//Click on Apply button
			verifyExists(CPQQuoteCreationObj.ViewPrices.CancelButton,"Verify Cancel Button");
			click(CPQQuoteCreationObj.ViewPrices.CancelButton, "Click on Cancel Button");
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.ViewPrices.ClosePopup,"Verify Close Button");
			click(CPQQuoteCreationObj.ViewPrices.ClosePopup, "Click on Close Button");
			Reusable.waitForpageloadmask();
		}
		break;
		
		case "CTLP-Fidessa":{
		
		if (isVisible(CPQQuoteCreationObj.ViewPrices.WarningMessage)) {
		 verifyExists(CPQQuoteCreationObj.ViewPrices.WarningMessage,"Verify Warning Message");
		}
		verifyExists(CPQQuoteCreationObj.ViewPrices.ViewPriceTab,"Verify View Price Tab");
		click(CPQQuoteCreationObj.ViewPrices.ViewPriceTab, "Click on View Price Tab");
		Reusable.waitForpageloadmask();
		
		waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.PricingSegment_Fidessa, 120);
		verifyExists(CPQQuoteCreationObj.ViewPrices.PricingSegment_Fidessa,"Verify Pricing Segment is 'CLTP - Fidessa'");
				
		verifyExists(CPQQuoteCreationObj.ViewPrices.viewAllContractualPricesBtn,"Verify view All Contractual Prices Btn");
		click(CPQQuoteCreationObj.ViewPrices.viewAllContractualPricesBtn, "Click on view All Contractual Prices Btn");
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.ViewPrices.pricesPopup,"Verify Prices popup");
		verifyExists(CPQQuoteCreationObj.ViewPrices.PricingCriteria,"Verify Pricing Criteria");
		waitForAjax();
		//Comparing NRC Values
		ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.NRCValues);
		
			if (waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.NRCValues, 100)) {
				Reusable.WaitforCPQloader();
				String NRCValueText= getTextFrom(CPQQuoteCreationObj.ViewPrices.NRCValues);
				// double d_NRCValueText=Double.parseDouble(NRCValueText);
				 System.out.println("NRC Value is"+NRCValueText);
				for (i = 1; i <= Iter; i++) {
					if(NRCValueText.equalsIgnoreCase(NRCValue))  {
						ExtentTestManager.getTest().log(LogStatus.PASS, "NRC Value is set to "+NRCValueText);
						Reusable.WaitforCPQloader();
						System.out.println("NRC Value is set to "+NRCValueText);
						break;
					} else {
						Thread.sleep(10000);
						Reusable.WaitforCPQloader();
						click(CPQQuoteCreationObj.ViewPrices.NRCValues);
						NRCValueText = findWebElement(CPQQuoteCreationObj.ViewPrices.NRCValues).getAttribute("value");
						scrollIntoTop();
						continue;
					}
				}
				if (i > Iter) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "NRC Value is still set as "+NRCValueText+ " but the Expected status is "+NRCValue);
					System.out.println("Quote stage is still set as "+NRCValueText+ " but the Expected status is "+NRCValue);
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "NRC Value is not visible, Please Verify");
				System.out.println("NRC Value is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}
			//Comparing MRC Values
			ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.MRCValues);
			if (waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.MRCValues, 100)) {
				Reusable.WaitforCPQloader();
				String MRCValue_Field = getTextFrom(CPQQuoteCreationObj.ViewPrices.MRCValues);
				// double d_MRCValue_Field=Double.parseDouble(MRCValue_Field);
				System.out.println("NRC Value is"+MRCValue_Field);
				for (i = 1; i <= Iter; i++) {
					if(MRCValue_Field.equalsIgnoreCase(MRCValue)) {
						ExtentTestManager.getTest().log(LogStatus.PASS, "MRC Value is set to "+MRCValue_Field);
						Reusable.WaitforCPQloader();
						System.out.println("MRC Value is set to "+MRCValue_Field);
						break;
					} else {
						
						Reusable.WaitforCPQloader();
						click(CPQQuoteCreationObj.ViewPrices.MRCValues);
						MRCValue_Field = findWebElement(CPQQuoteCreationObj.ViewPrices.MRCValues).getAttribute("value");
						scrollIntoTop();
						continue;
					}
				}
				if (i > Iter) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "MRC Value is still set as "+MRCValue_Field+ " but the Expected status is "+MRCValue);
					System.out.println("MRC Value is still set as "+MRCValue_Field+ " but the Expected status is "+MRCValue);
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "MRC Value is not visible, Please Verify");
				System.out.println("MRC Value is not visible, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			}
			//Click on Apply button
			verifyExists(CPQQuoteCreationObj.ViewPrices.CancelButton,"Verify Cancel Button");
			click(CPQQuoteCreationObj.ViewPrices.CancelButton, "Click on Cancel Button");
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.ViewPrices.ClosePopup,"Verify Close Button");
			click(CPQQuoteCreationObj.ViewPrices.ClosePopup, "Click on Close Button");
			Reusable.waitForpageloadmask();
		
		}
		break;
		}
		return "True";
		}
	public String validateWholeSaleTeam(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
//		Initializing the Variable
//		String Product_Name = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String WholeSaleTeamID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"WholeSaleTeamID");
//		String characterLimt = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String QuoteID = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");

		String sResult,charcterCount, Sub_Err;
		String quoteStageValue = getAttributeFrom(CPQQuoteCreationObj.AddProducts.quoteStageElem,"value");

		switch(quoteStageValue) {
		case "Created":{
		
//		Below Condition is to Select and store the values from list
		if(waitForElementToBeVisible(CPQQuoteCreationObj.WholeSalesTeam.supportLnk, 120)) {
			verifyExists(CPQQuoteCreationObj.WholeSalesTeam.supportLnk,"Support Link");
			ScrollIntoViewByString(CPQQuoteCreationObj.WholeSalesTeam.supportLnk);
			click(CPQQuoteCreationObj.WholeSalesTeam.supportLnk,"Support Link");
			Reusable.waitForpageloadmask();
			
//			Below to Capture the list of Values
			String ListValues=storeByValueDIV(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleSupportTeamLst, CPQQuoteCreationObj.WholeSalesTeam.wholeSaleSupportTeamSubLst);
			ExtentTestManager.getTest().log(LogStatus.PASS, "List Of Wholes Sale Teams is coming in DropDown is "+ListValues);
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleSupportTeamLst);

//			Below to Select wholeSale User from the dropdown
			webDriver.findElement(By.xpath("//li/div[contains(text(),'"+WholeSaleTeamID+"')]")).click();
			
			ExtentTestManager.getTest().log(LogStatus.PASS, WholeSaleTeamID+" WholeSale user is Selected from DropDwon");
			String teamNumber=getTextFrom(CPQQuoteCreationObj.WholeSalesTeam.wholesaleteamNumberTxt);
			DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "WholeSaleTeamSelected", teamNumber); 
			Reusable.waitForpageloadmask();
		}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Support link is not visible, Please verfiy");
				System.out.println("Support link is not visible, Please verfiy");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
		
			
//			Below Condition is to select the date and time
			if(waitForElementToBeVisible(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleAssignedDate, 60)) {
				verifyExists(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleAssignedDate,"WholeSale Assigned Date");
				clickByAction(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleAssignedDate);
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.WholeSalesTeam.wholeSaledatePicker,"WholeSale Date Picker");
				clickByAction(CPQQuoteCreationObj.WholeSalesTeam.wholeSaledatePicker);
				Reusable.waitForpageloadmask();
				String date=getAttributeFrom(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleAssignedDate,"value");
				DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "WholeSaleDate", date); 
//			Save the page
				sResult = saveCPQ("Main");
				if (sResult.equalsIgnoreCase("False")){ return "False"; }
				Reusable.waitForpageloadmask();
//			Validate Time error message should not displayed
				Sub_Err = getTextFrom(CPQQuoteCreationObj.CopyQuote.cpqErrorMsgElem);
				if(!Sub_Err.contains("Please Select Date & Time when the order form was received over email")) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale user Date auto populated and selected "+date);
				}
				else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "WholeSale Team Date is not selected , Please verify");	
//				return "False";
				}
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Date text box is not visible, Please verfiy");
				System.out.println("Date text box is not visible, Please verfiy");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
			
//			Below method is enter the email
			if(waitForElementToBeVisible(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleEmailSubjectTxb, 60)) {
			charcterCount=generateRandomText(file_name, Sheet_Name, iScript, iSubScript);
			verifyExists(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleEmailSubjectTxb, "WholeSale Email Subject TextBox");
			sendKeys(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleEmailSubjectTxb, charcterCount);
			sResult = saveCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale team entered 1501 characters to verify the limit for Email Subject field ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
			verifyExists(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleEmailSubjectTxb, "WholeSale Email Subject TextBox");
			sendKeys(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleEmailSubjectTxb, "test@colt.net");
			ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale Team Eamil test@colt.net is Entered");
			String email=getAttributeFrom(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleEmailSubjectTxb,"value");
			DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "WholeSaleEmail", email); 

			sResult = saveCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.waitForpageloadmask();
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "WholeSale Team email is not visible, Please verfiy");
				System.out.println("WholeSale Team email is not visible, Please verfiy");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
			
//			Select the Toggle Button and enter pricing id
			if(waitForElementToBeVisible(CPQQuoteCreationObj.WholeSalesTeam.wholeSaletoggleButton, 60)) {
				verifyExists(CPQQuoteCreationObj.WholeSalesTeam.wholeSaletoggleButton,"WholeSale toggle Button");
				click(CPQQuoteCreationObj.WholeSalesTeam.wholeSaletoggleButton);
				ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale Team Disable Quote Commercial Review button is displayed and clicked");
				waitForElementToBeVisible(CPQQuoteCreationObj.WholeSalesTeam.wholeSalepricingTxb, 60);
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "WholeSale Team Disable Quote Commercial is not visible, Please verfiy");
				System.out.println("WholeSale Team Disable Quote Commercial is not visible, Please verfiy");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
			
//			Below condition is pricing text is visible or not
			verifyExists(CPQQuoteCreationObj.WholeSalesTeam.wholeSalepricingTxb,"WholeSale pricing Text Box");
			ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale Team Pricing Source Text box is visible");
			sendKeys(CPQQuoteCreationObj.WholeSalesTeam.wholeSalepricingTxb, charcterCount);
			sResult = saveCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale team entered 1501 characters to Verify limit for the Pricing Source id. ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
			sendKeys(CPQQuoteCreationObj.WholeSalesTeam.wholeSalepricingTxb, "12345");
			ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale Team pricing value is Entered ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			String pricingId=getAttributeFrom(CPQQuoteCreationObj.WholeSalesTeam.wholeSalepricingTxb,"value");
			DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "WholeSalePricing", pricingId); 
			sResult = saveCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			
//			To Validate in Verify the field 'Wholesale Support Member' in C4C Quotes Tab under opportunity.
			sResult = returnC4CFromCPQ();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			verifyExists(CPQQuoteCreationObj.AddProducts.quotesLnk);
			click(CPQQuoteCreationObj.AddProducts.quotesLnk);			
			Reusable.WaitforC4Cloader();
			Reusable.Waittilljquesryupdated();
			Reusable.WaitforC4Cloader();
			WebElement QuoteLink= WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']"));
			
			WebElement wsTeamId = WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//span[@title='"+WholeSaleTeamID+"']"));
			scrollIntoView(wsTeamId);
			ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale Verify the field 'Wholesale Support Member' in C4C Quotes Tab under opportunity.");
			WebElement QuoteLnk = WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']"));
			scrollIntoView(QuoteLnk);
			QuoteLnk.click();
			//click(WEB_DRIVER_THREAD_LOCAL.get().findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")));
			//WebInteractUtil.pause(1000);
			
			
//			To validate below two
//			Verify the field 'Wholesale Support Member' in C4C All Quotes section.
			sResult=navigateQuotesFromHomepage();
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			if (waitForElementToBeVisible(C4CEngagementSalesObj.salespage.searchIcon,60)) {
				Reusable.Waittilljquesryupdated();
				Reusable.WaitforC4Cloader();
				Reusable.Waittilljquesryupdated();
				//WebInteractUtil.pause(1000);
				verifyExists(C4CEngagementSalesObj.salespage.searchIcon,"Search ICon");
				ScrollIntoViewByString(C4CEngagementSalesObj.salespage.searchIcon);
				click(C4CEngagementSalesObj.salespage.searchIcon);
				waitForElementToBeVisible(C4CEngagementSalesObj.salespage.searchTxb,60);
				verifyExists(C4CEngagementSalesObj.salespage.searchTxb,"Search TextBox");
				sendKeys(C4CEngagementSalesObj.salespage.searchTxb, QuoteID);
				
				verifyExists(C4CEngagementSalesObj.salespage.searchBtn,"Search TextBox");
				click(C4CEngagementSalesObj.salespage.searchBtn);
				Reusable.WaitforC4Cloader();
				WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
				Reusable.Waittilljquesryupdated();
//					if (Environment.equalsIgnoreCase("Test2")) { WebInteractUtil.scrollIntoView(CPQ_Objects.defaultCkb); }
				driver.findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")).isEnabled();
				scrollIntoView(driver.findElement(By.xpath("//span[@title='"+WholeSaleTeamID+"']")));
				driver.findElement(By.xpath("//span[@title='"+WholeSaleTeamID+"']")).isEnabled();
				ExtentTestManager.getTest().log(LogStatus.PASS, "WholeSale Verify the field 'Wholesale Support Member' in C4C All Quotes section.");
				scrollIntoView(driver.findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")));
				driver.findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")).click();;
				//WebInteractUtil.pause(1000);
				Reusable.waitToPageLoad();
				Reusable.WaitforC4Cloader();			
			}	
			
//			To Validate in more link
//			Verify the field 'Wholesale Support Member' in C4C Quotes Overview section under opportunity.
			verifyExists(C4CEngagementSalesObj.salespage.moreLnk,"More Link");
			click(C4CEngagementSalesObj.salespage.moreLnk);
			Reusable.WaitforC4Cloader();
	    	String wholeUser=getTextFrom(CPQQuoteCreationObj.WholeSalesTeam.wholesaleC4CQuoteViewTxt);
	    	if(wholeUser.equalsIgnoreCase(WholeSaleTeamID)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Verify the field 'Wholesale Support Member' in C4C Quotes Overview section under opportunity.");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	    	}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Verify the field 'Wholesale Support Member' in C4C Quotes Overview section under opportunity. is not visible, Please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	    	}
			isVisible(CPQQuoteCreationObj.AddProducts.actionBtn);
			ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.actionBtn);
			Reusable.WaitforC4Cloader();
			click(CPQQuoteCreationObj.AddProducts.actionBtn);
			Reusable.Waittilljquesryupdated();
			click(CPQQuoteCreationObj.AddProducts.EditBtn);
			waitForElementToAppear(CPQQuoteCreationObj.AddProducts.quoteNameTxb, 180);
			Reusable.waitForpageloadmask();
			Reusable.Waittilljquesryupdated();		
			}
		break;
		
		case "Approved": case "Priced":{
			String WholeSaleTeamidSelcted = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"WholeSaleTeamSelected");
			String WholeSaleTeamdate = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"WholeSaleDate");
			String WholeSaleEmail = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"WholeSaleEmail");
			String WholeSalePrice = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"WholeSalePricing");
			
			verifyExists(CPQQuoteCreationObj.WholeSalesTeam.supportLnk,"Support Link");
			ScrollIntoViewByString(CPQQuoteCreationObj.WholeSalesTeam.supportLnk);
			click(CPQQuoteCreationObj.WholeSalesTeam.supportLnk,"Support Link");
			Reusable.waitForpageloadmask();
				
//				Below to validate the team number
			String teamNumber=getTextFrom(CPQQuoteCreationObj.WholeSalesTeam.wholesaleteamNumberTxt);
			if(teamNumber.equalsIgnoreCase(WholeSaleTeamidSelcted)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "After commerical Approve also whole sale teams user still not changed "+teamNumber);
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "After commerical Approve also whole sale teams user is changed to "+teamNumber+" ,please verfiy");

			}
				
//				Below to validate the date
	    		String date=getAttributeFrom(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleAssignedDate,"value");
	    		if(date.equalsIgnoreCase(WholeSaleTeamdate)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "After commerical Approve also whole sale teams date still not changed "+date);
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "After commerical Approve also whole sale teams date is changed to "+date+" ,please verfiy");

				}
	    		
//	    		Below to validate the email
				String email=getAttributeFrom(CPQQuoteCreationObj.WholeSalesTeam.wholeSaleEmailSubjectTxb,"value");
	    		if(email.equalsIgnoreCase(WholeSaleEmail)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "After commerical Approve also whole sale teams email still not changed "+email);
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "After commerical Approve also whole sale teams email is changed to "+email+" ,please verfiy");
				}
	    		
//	    		Below to validate the pricedid
				String pricingId=getAttributeFrom(CPQQuoteCreationObj.WholeSalesTeam.wholeSalepricingTxb,"value");
				if(pricingId.equalsIgnoreCase(WholeSalePrice)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "After commerical Approve also whole sale teams Pricing id still not changed "+pricingId);
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "After commerical Approve also whole sale teams Pricing id is changed to "+pricingId+" ,please verfiy");
				}
			}
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"General Information Link");
			ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
			click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);		
			break;
		}
		return "True";
	}
	
	public String reviseQuote() throws IOException, InterruptedException 
	{
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.reviseBtn, 60);
		verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.reviseBtn, "Revise Quote Button");
		Reusable.WaitforCPQloader();
		click(CPQQuoteCreationObj.SubmissionforCommercialApproval.reviseBtn, "Revise Quote Button");
				
		//WebInteractUtil.pause(2000);
		//WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
		//driver.switchTo().alert().accept();
		//Reusable.WaitforCPQloader();
		
		verifyDoesNotExist(CPQQuoteCreationObj.SubmissionforCommercialApproval.reviseBtn);
		Reusable.WaitforCPQloader();
		Reusable.Waittilljquesryupdated();
		
		//String sResult = Approval.validateCPQErrorMsg();
		//if (sResult.equalsIgnoreCase("False")) { return "False"; }
			
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Revise Quote is Success in CPQ");
		System.out.println("Revise Quote is Success in CPQ");

		return "True";
	}
	
	public String addQuoteInC4CForMulti(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String quoteID, sResult = null;
		String Opportunity_Currency = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Opportunity_Currency");
		String Legal_Complexity = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Legal_Complexity");
		String Technical_Complexity = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Technical_Complexity");
		String Multi_Quote_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Multi_Quote_Type");
		String OCN_Number = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"OCN_Number");

		System.out.println("Legal_Complexity :"+Legal_Complexity);
		System.out.println("Legal_Complexity :"+Technical_Complexity);

		
//		Add Quote
		/*waitForElementToAppear(CPQQuoteCreationObj.salespage.salesLst, 15);
		verifyExists(CPQQuoteCreationObj.salespage.salesLst, "Consultancy");
		click(CPQQuoteCreationObj.salespage.salesLst);
		waitForAjax();
		
		waitForElementToBeVisible(CPQQuoteCreationObj.salespage.quotesTab,75);
		verifyExists(CPQQuoteCreationObj.salespage.quotesTab,"Quotes Link");
		click(CPQQuoteCreationObj.salespage.quotesTab);
		for (int i = 1; i < 3; i++) { Reusable.WaitforC4Cloader(); }*/			
			
		verifyExists(CPQQuoteCreationObj.AddProducts.addQuoteBtn,"Add Quote Button");
		ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.addQuoteBtn);
		Reusable.WaitforC4Cloader();
		for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
		click(CPQQuoteCreationObj.AddProducts.addQuoteBtn,"Add Quote Button");
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.MultiQuote.quoteCurrencyDBtn, "Quote Currency Button");
		click(CPQQuoteCreationObj.MultiQuote.quoteCurrencyDBtn,"Quote Currency Button");
		ClickonElementByString("//li[normalize-space(.)='"+Opportunity_Currency+"']", 30);
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.MultiQuote.legalComplexityDBtn,"Legal Complexity Button");
		click(CPQQuoteCreationObj.MultiQuote.legalComplexityDBtn,"Legal Complexity Button");
		ClickonElementByString("//li[normalize-space(.)='"+ Legal_Complexity +"']", 30);
		//sendKeys(CPQQuoteCreationObj.MultiQuote.legalComplexityDBtn,Legal_Complexity);
		//keyPress(CPQQuoteCreationObj.MultiQuote.legalComplexityDBtn,Keys.ENTER);
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.MultiQuote.techicalComplexityDBtn,"Technical Complexity Button");
		click(CPQQuoteCreationObj.MultiQuote.techicalComplexityDBtn,"Technical Complexity Button");
		ClickonElementByString("//li[normalize-space(.)='"+ Technical_Complexity +"']", 30);
		//sendKeys(CPQQuoteCreationObj.MultiQuote.techicalComplexityDBtn,Technical_Complexity);
		Reusable.waitForpageloadmask();
		//click("@xpath=//span[@id='dropdownlistboxTu4gJBzaeqEGMJpCt6ciK0_7087-arrow']");
		Reusable.waitForpageloadmask();
			
		verifyExists(CPQQuoteCreationObj.MultiQuote.proceedBtn,"Proceed Button");
		click(CPQQuoteCreationObj.MultiQuote.proceedBtn,"Proceed Button");
		
		Reusable.WaitforCPQloader();
		Reusable.waitToPageLoad();
		Reusable.waitForAjax();
		
		waitForElementToAppear(CPQQuoteCreationObj.TransactionPage.QuoteId, 150);
		verifyExists(CPQQuoteCreationObj.TransactionPage.QuoteId, "Quote Id");
		
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.quoteNameTxb, 240);
		//waitForElementToAppear(CPQQuoteCreationObj.AddProducts.seRevLnk, 120);
		Reusable.WaitforCPQloader();
		
		quoteID = getAttributeFrom(CPQQuoteCreationObj.AddProducts.quoteIDElem,"value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Quote ID is generated "+quoteID);
		System.out.println("Quote ID is generated "+quoteID);
		DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Quote_ID", quoteID);			
		
//		Transcation
		if(Multi_Quote_Type.equalsIgnoreCase("Transaction")) 
		{
			waitForElementToBeVisible(CPQQuoteCreationObj.TransactionPage.adminBtn, 60);
			ScrollIntoViewByString(CPQQuoteCreationObj.TransactionPage.adminBtn);
			click(CPQQuoteCreationObj.TransactionPage.adminBtn,"Admin Button");
			Reusable.Waittilljquesryupdated();
			
			verifyExists(CPQQuoteCreationObj.TransactionPage.transactionsLnk,"Transasctions Link");
			click(CPQQuoteCreationObj.TransactionPage.transactionsLnk,"Transasctions Link");
			Reusable.Waittilljquesryupdated();
			
			verifyExists(CPQQuoteCreationObj.TransactionPage.newTransactionBtn,"New Transaction Button");
			click(CPQQuoteCreationObj.TransactionPage.newTransactionBtn,"New Transaction Button");
			
			Reusable.waitForAjax();
			waitForElementToAppear(CPQQuoteCreationObj.TransactionPage.QuoteId, 150);
			verifyExists(CPQQuoteCreationObj.TransactionPage.QuoteId, "Quote Id");
			
			waitForElementToAppear(CPQQuoteCreationObj.MultiQuote.quoteTypeBtn,75);
			quoteID = getAttributeFrom(CPQQuoteCreationObj.AddProducts.quoteIDElem,"value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Quote ID is generated "+quoteID);
			System.out.println("New Quote ID is generated "+quoteID);
			DataMiner.fnsetcolvalue(file_name, "TS_18_Multi_Quote_Sales", iScript, iSubScript, "New_Transcation_Quote_Id", quoteID);
			
			verifyExists(CPQQuoteCreationObj.MultiQuote.accountDetailsLnk,"Account Details Link");
			ScrollIntoViewByString(CPQQuoteCreationObj.MultiQuote.accountDetailsLnk);
			click(CPQQuoteCreationObj.MultiQuote.accountDetailsLnk,"Account Details Link");
        	Reusable.waitForpageloadmask();
        	
        	verifyExists(CPQQuoteCreationObj.MultiQuote.priceSegmentDrp,"Price Segment Drop");
        	click(CPQQuoteCreationObj.MultiQuote.priceSegmentDrp,"Price Segment Drop");
			ClickonElementByString("//li[normalize-space(.)='ENTS']", 30);
        	//WebInteractUtil.pause(1000);
			Reusable.waitToPageLoad();
        	sResult = saveCPQ("Main");
        	if (sResult.equalsIgnoreCase("False")){ return "False"; }				
		}
		
//		Updated Quote Type to MultiQuote
		if(waitForElementToBeVisible(CPQQuoteCreationObj.MultiQuote.quoteTypeBtn,75)) 
		{
			ScrollIntoViewByString(CPQQuoteCreationObj.MultiQuote.quoteTypeBtn);
			clickByAction(CPQQuoteCreationObj.MultiQuote.quoteTypeBtn);
			//WebInteractUtil.pause(1000);
			Reusable.waitToPageLoad();
			click(CPQQuoteCreationObj.MultiQuote.multiTypeBtn,"Multi Type Button");
			Reusable.waitForpageloadmask();
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to click MultiQuote Type, please verify");
			System.out.println("Unable to click MultiQuote Type, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
		
		//Transcation add product verify
		if(Multi_Quote_Type.equalsIgnoreCase("Transaction")) {
			verifyExists(CPQQuoteCreationObj.AddProducts.AddProductBtn,"Add Product Button");
			ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.AddProductBtn);
			click(CPQQuoteCreationObj.AddProducts.AddProductBtn,"Add Product Button");
			Reusable.waitForpageloadmask();
		if (verifyExists(CPQQuoteApprovalsObj.QuoteApprovals.CPQSummaryErrorMsgElem)) {sResult="False";}
    	if (sResult.equalsIgnoreCase("False")) {
    		verifyExists(CPQQuoteCreationObj.MultiQuote.accountDetailsLnk,"AccountDetails Link");
    		ScrollIntoViewByString(CPQQuoteCreationObj.MultiQuote.accountDetailsLnk);
        	Reusable.waitForpageloadmask();
        	verifyExists(CPQQuoteCreationObj.MultiQuote.ocnTxt);
        	sendKeys(CPQQuoteCreationObj.MultiQuote.ocnTxt, OCN_Number); 
    		sResult = saveCPQ("Main");
        	if (sResult.equalsIgnoreCase("False")){ return "False"; }
    		}else {
    			waitForElementToAppear(CPQQuoteCreationObj.AddProducts.DataLink,180);
    			verifyExists(CPQQuoteCreationObj.AddProducts.DataLink, "Data Link");
    			click(CPQQuoteCreationObj.AddProducts.DataLink, "Data Link");
    			
    			waitForElementToAppear(CPQQuoteCreationObj.AddProducts.EthernetLnk,180);
    			verifyExists(CPQQuoteCreationObj.AddProducts.EthernetLnk, "Ethernet Link");
    			click(CPQQuoteCreationObj.AddProducts.EthernetLnk, "Ethernet Link");
    			
    			waitForElementToAppear(CPQQuoteCreationObj.AddProducts.EthernetLineLnk,180);
    			verifyExists(CPQQuoteCreationObj.AddProducts.EthernetLineLnk, "Ethernet Line Link");
    			ScrollIntoViewByString(CPQQuoteCreationObj.AddProducts.EthernetLineLnk);
    			click(CPQQuoteCreationObj.AddProducts.EthernetLineLnk, "Ethernet Line Link");

				waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.siteAAddressTxb, 180);
				verifyExists(CPQQuoteCreationObj.MultiQuote.cancelConfiguration,"Cancel Configuration Button");
				click(CPQQuoteCreationObj.MultiQuote.cancelConfiguration,"Cancel Configuration Button");
				//driver.switchTo().alert().accept();
				waitForElementToBeVisible(CPQQuoteCreationObj.AddProducts.returnToC4CBtn, 90);
				for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
    		}
		}
		
		return quoteID;
	}
	
	public String multiQuoteProcess(String file_name, String Sheet_Name, String iScript, String iSubScript, String ProductName) throws IOException, InterruptedException 
	{
		
//		Initializing the variables			
		ProductName = ProductName.replaceAll("(?!^)([A-Z])", " $1");
		
		waitForElementToAppear(CPQQuoteCreationObj.MultiQuote.multiQuoteCbx, 60);
		
//		Initializing the driver
		WebDriver driver = WEB_DRIVER_THREAD_LOCAL.get();
		Reusable.waitForpageloadmask();
		
//      Setting up the instance of previous window handle
        String WindowHandle = driver.getWindowHandle();
		
		waitForElementToAppear(CPQQuoteCreationObj.AddProducts.quoteStatusElem,60);
		verifyExists(CPQQuoteCreationObj.AddProducts.quoteStatusElem,"Quote Status");
		String quoteStage = getAttributeFrom(CPQQuoteCreationObj.AddProducts.quoteStatusElem,"value");
		if (quoteStage.equals("In Process")) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Quote stage is set to "+quoteStage);
			System.out.println("Quote stage is set to "+quoteStage);
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote Stage is not expected, please verify");
					System.out.println("Quote Stage is not expected, please verify");
					ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
					return "False";
				}			
		
		
		waitForElementToAppear(CPQQuoteCreationObj.MultiQuote.multiQuoteCbx, 60);
		
		verifyExists(CPQQuoteCreationObj.MultiQuote.multiQuoteCbx,"Multi Quote Checkbox");
		ScrollIntoViewByString(CPQQuoteCreationObj.MultiQuote.multiQuoteCbx);
		click(CPQQuoteCreationObj.MultiQuote.multiQuoteCbx);
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.MultiQuote.downloadQuote);	
		for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
		//WebInteractUtil.pause(1500);
		Reusable.waitToPageLoad();
		click(CPQQuoteCreationObj.MultiQuote.downloadQuoteExternal);
		for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
        ExtentTestManager.getTest().log(LogStatus.PASS, "Downloaded External Quote");
        
//			Capturing the new window handle
	    for (String WindowHandleAfter : driver.getWindowHandles()) {
	    if(!WindowHandleAfter.equals(WindowHandle)) {
	           driver.switchTo().window(WindowHandleAfter);
	           //WebInteractUtil.pause(3000);
	           Reusable.waitToPageLoad();
	           driver.close();
	              }
	     }
	        
//	         Switch back to first browser window
	     driver.switchTo().window(WindowHandle);
	     Reusable.waitForpageloadmask(); 
	     waitForElementToAppear(CPQQuoteCreationObj.MultiQuote.procedToQuoteBtn, 120);
//		    	Select the product
		 String sResult = Reusable.WebTableCellAction("Product", ProductName, null,"Click", null);
		 //String sResult = Reusable.MultiLineWebTableCellAction("Product", ProductName, null,"Click", null, 1);
		 ScrollIntoViewByString(CPQQuoteCreationObj.MultiQuote.procedToQuoteBtn);
		 click(CPQQuoteCreationObj.MultiQuote.procedToQuoteBtn,"Proceed to Quote Button");
		 for (int i = 1; i < 3; i++) { Reusable.waitForpageloadmask(); }
		 waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn, 90);
		 waitForElementToAppear(CPQQuoteCreationObj.TransactionPage.approvedLnk, 120);
		 Reusable.WaitforCPQloader();
		 ScrollIntoViewByString(CPQQuoteCreationObj.TransactionPage.quoteIDElem);
		 String quoteID = getAttributeFrom(CPQQuoteCreationObj.TransactionPage.quoteIDElem,"value");
		 DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Multi_Quote_ID", quoteID);
		 
	     ExtentTestManager.getTest().log(LogStatus.PASS, "Capture the new Quote ID "+quoteID);
	        								
		 return "True";
	}
	
	public String clearQuoteDiscount() throws IOException, InterruptedException 
	{
		
		waitForElementToAppear(CPQQuoteCreationObj.discountingQuotePage.quoteLnk, 20);
        verifyExists(CPQQuoteCreationObj.discountingQuotePage.quoteLnk,"'QuoteLink'");
        click(CPQQuoteCreationObj.discountingQuotePage.quoteLnk,"'QuoteLink'");
		Reusable.WaitforCPQloader();
			
		waitForElementToAppear(CPQQuoteCreationObj.discountingQuotePage.basePriceNRCDiscountTxb, 20);
        verifyExists(CPQQuoteCreationObj.discountingQuotePage.quoteLnk,"'QuoteLink'");
		
		if (waitForElementToBeVisible(CPQQuoteCreationObj.discountingQuotePage.basePriceNRCDiscountTxb, 60)) 
		{
			Reusable.WaitforCPQloader();
			clearTextBox(CPQQuoteCreationObj.discountingQuotePage.basePriceNRCDiscountTxb);
			    
			verifyExists(CPQQuoteCreationObj.discountingQuotePage.basePriceMRCDiscountTxb,"Base Price MRC Discount TextBox");
			clearTextBox(CPQQuoteCreationObj.discountingQuotePage.basePriceMRCDiscountTxb);
			
			verifyExists(CPQQuoteCreationObj.discountingQuotePage.calculateDiscountBtn,"Calculate Discount Button");
			click(CPQQuoteCreationObj.discountingQuotePage.calculateDiscountBtn);
			Reusable.WaitforCPQloader();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Applied Discount has been cleared for the Quote");
			System.out.println("Applied Discount has been cleared for the Quote");
		} 
	
		return "True";
		
	}
	
	public String dealPricePLTabEntry(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
		
		String sResult = null;
		waitForElementToAppear(CPQQuoteCreationObj.PLTabInfo.plLnk, 60);
        verifyExists(CPQQuoteCreationObj.PLTabInfo.plLnk,"'P&L Tab'");
        click(CPQQuoteCreationObj.PLTabInfo.plLnk,"'P&L Tab'");
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.PLTabInfo.dealBackgroundTxb,"'P&L Tab'");
		sendKeys(CPQQuoteCreationObj.PLTabInfo.dealBackgroundTxb,"Automation","Deal Background Text Box");
        
		verifyExists(CPQQuoteCreationObj.PLTabInfo.competitorsTxb,"'P&L Tab'");
		sendKeys(CPQQuoteCreationObj.PLTabInfo.competitorsTxb,"Automation","Competitors Text Box");
		
		verifyExists(CPQQuoteCreationObj.PLTabInfo.technicalSolutionsTxb,"'P&L Tab'");
		sendKeys(CPQQuoteCreationObj.PLTabInfo.technicalSolutionsTxb,"Automation","Technical Solutions Textbox");
		
//			Calling the below method to save the details
			sResult = saveCPQ("Main");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }			
			
		return "True";
	}
	
	public String dealPricingApproval(String ActionType) throws IOException, InterruptedException 
	{
		
		scrollUp();
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink, 60);
		verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink, "Approval Link");			
		click(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink, "Approval Link");
		
		waitForAjax();

		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.dealPriceCommentTxb, 20);
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforCommercialApproval.dealPriceCommentTxb);						
		//verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.dealPriceCommentTxb, "Deal Price Comment textbox");
		sendKeys(CPQQuoteCreationObj.SubmissionforCommercialApproval.dealPriceCommentTxb,"Reject");

		//verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn, "Deal Price Reject Button");
		javaScriptclick(CPQQuoteCreationObj.SubmissionforCommercialApproval.DP_rejectDiscountBtn,"Deal Price Reject Button");	
		Reusable.Waittilljquesryupdated();

		verifyDoesNotExist(CPQQuoteCreationObj.SubmissionforCommercialApproval.DP_rejectDiscountBtn);
		Reusable.WaitforCPQloader();
		Reusable.Waittilljquesryupdated();
		
		//String sResult = Approval.validateCPQErrorMsg();
		//if (sResult.equalsIgnoreCase("False")) { return "False"; }
		
		waitForElementToAppear(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, 10);
		verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
		click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk, "General Information Link");
		Reusable.WaitforCPQloader();
		waitForAjax();
		
		return "True";
	}
		
	
	
	public String selectProductFeaturesSiteNameAlias(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
			
		
		switch (Product_Name.toUpperCase()) {
				
			    case "ETHERNETLINE": case "ETHERNETSPOKE": case "WAVE":
		
//				Entering SiteNameAlias details		
				ScrollIntoViewByString(CPQQuoteCreationObj.SiteNameAlias.ServiceFeaturesTab);
				waitForElementToAppear(CPQQuoteCreationObj.SiteNameAlias.ServiceFeaturesTab, 10);
				verifyExists(CPQQuoteCreationObj.SiteNameAlias.ServiceFeaturesTab,"'Service Features label'");	
				
				if(Product_Name.equalsIgnoreCase("WAVE"))
				{
				click(CPQQuoteCreationObj.SiteNameAlias.PerformanceRepflagWave);
				}
				else{
					click(CPQQuoteCreationObj.SiteNameAlias.PerformanceRepflag);
				}
				
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.SiteNameAlias.AEndSiteNameAlias,"'AEnd Site Name Alias Field'");
				if(Product_Name.equalsIgnoreCase("EthernetLine"))
		     	{
				verifyExists(CPQQuoteCreationObj.SiteNameAlias.BEndSiteNameAlias,"'BEnd Site Name Alias Field'");	
		     	}
		
				
	}
	
		return "True";	
	}
	
	public String selectProductFeaturesBespoke(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
	// Initializing the Variable
		String Bespoke_type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_type");
		String Bespoke_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Reference");
		String Bespoke_Comments = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Comments");
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		switch (Product_Name.toUpperCase()) {
		case "ETHERNETLINE": case "ETHERNETSPOKE":   case "ETHERNETHUB":
			
			// Entering Bespoke details
			ScrollIntoViewByString(CPQQuoteCreationObj.Bespoke.BespokeTab);
			waitForElementToAppear(CPQQuoteCreationObj.Bespoke.BespokeTab, 10);
			verifyExists(CPQQuoteCreationObj.Bespoke.BespokeTab,"'Bespoke label'");
			click(CPQQuoteCreationObj.Bespoke.Bespokeflag);
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.Bespoke.Bespoketype,"'Bespoke from type dropdown'");
			click(CPQQuoteCreationObj.Bespoke.Bespoketype);
			ClickonElementByString("//div[@data-value='"+Bespoke_type+"']", 20);
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.Bespoke.SolutionRefId,"'Diversity Reference Text Box'");
			sendKeys(CPQQuoteCreationObj.Bespoke.SolutionRefId,Bespoke_Reference,"'Ethernet Hub Reference Text Box'");
			Reusable.WaitforC4Cloader();
			Reusable.waitForpageloadmask();
			verifyExists(CPQQuoteCreationObj.Bespoke.CommentsfromSales,"'Diversity Reference Text Box'");
			sendKeys(CPQQuoteCreationObj.Bespoke.CommentsfromSales,Bespoke_Comments,"'Ethernet Hub Reference Text Box'");
			break;

		case "COLTIPACCESS":
			
				verifyExists(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
				click(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				// Entering Bespoke details
				ScrollIntoViewByString(CPQQuoteCreationObj.Bespoke.IpBespokeTab);
				waitForElementToAppear(CPQQuoteCreationObj.Bespoke.IpBespokeTab, 10);
				verifyExists(CPQQuoteCreationObj.Bespoke.IpBespokeTab,"'Bespoke label'");
				click(CPQQuoteCreationObj.Bespoke.IpBespokeflag);
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.Bespoke.IpBespoketype,"'Bespoke from type dropdown'");
				click(CPQQuoteCreationObj.Bespoke.IpBespoketype);
				ClickonElementByString("//div[@data-value='"+Bespoke_type+"']", 20);
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.Bespoke.IpSolutionRefId,"'Diversity Reference Text Box'");
				sendKeys(CPQQuoteCreationObj.Bespoke.IpSolutionRefId,Bespoke_Reference,"'Ethernet Hub Reference Text Box'");
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.Bespoke.IpCommentsfromSales,"'Diversity Reference Text Box'");
				sendKeys(CPQQuoteCreationObj.Bespoke.IpCommentsfromSales,Bespoke_Comments,"'Ethernet Hub Reference Text Box'");
				 break;
			
		case "WAVE":
					
				// Entering Bespoke details
				ScrollIntoViewByString(CPQQuoteCreationObj.Bespoke.WaveBespokeTab);
				waitForElementToAppear(CPQQuoteCreationObj.Bespoke.WaveBespokeTab, 10);
				verifyExists(CPQQuoteCreationObj.Bespoke.WaveBespokeTab,"'Bespoke label'");
				click(CPQQuoteCreationObj.Bespoke.WaveBespokeflag);
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.Bespoke.WaveBespoketype,"'Bespoke from type dropdown'");
				click(CPQQuoteCreationObj.Bespoke.WaveBespoketype);
				ClickonElementByString("//div[@data-value='"+Bespoke_type+"']", 20);
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.Bespoke.WaveSolutionRefId,"'Diversity Reference Text Box'");
				sendKeys(CPQQuoteCreationObj.Bespoke.WaveSolutionRefId,Bespoke_Reference,"'Ethernet Hub Reference Text Box'");
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				verifyExists(CPQQuoteCreationObj.Bespoke.WaveCommentsfromSales,"'Diversity Reference Text Box'");
				sendKeys(CPQQuoteCreationObj.Bespoke.WaveCommentsfromSales,Bespoke_Comments,"'Ethernet Hub Reference Text Box'");
				 break;
			
				
		}
		return "True";
	}

	public void UpdatepricingCityException(String Product_Name,String Pricing_City,String NRC, String MRC) throws InterruptedException, IOException
	{
		
		waitForElementToAppear(CPQQuoteCreationObj.engagementpf.PricingExcTab, 60);
		verifyExists(CPQQuoteCreationObj.engagementpf.PricingExcTab, "Pricing Exception Tab");			
		click(CPQQuoteCreationObj.engagementpf.PricingExcTab, "Pricing Exception Tab");
		Reusable.WaitforCPQloader();

		waitForElementToAppear(CPQQuoteCreationObj.engagementpf.PricingCityHub, 60);
		verifyExists(CPQQuoteCreationObj.engagementpf.PricingCityHub, "Pricing Exception AEnd Label");	
		
		verifyExists(CPQQuoteCreationObj.engagementpf.PricingCityLst, "Pricing Exception TextBox AEnd");
		click(CPQQuoteCreationObj.engagementpf.PricingCityLst, "Pricing Exception TextBox AEnd");
		ClickonElementByString("//div[@data-value='"+Pricing_City+"']", 20);
		
		if(!Product_Name.equalsIgnoreCase("EthernetHub")&&!Product_Name.equalsIgnoreCase("EthernetSpoke"))
		{
			verifyExists(CPQQuoteCreationObj.engagementpf.PricingCityBEnd, "Pricing Exception BEnd Label");
			verifyExists(CPQQuoteCreationObj.engagementpf.PricingCityLst1, "Pricing Exception Textbox BEnd");	
			click(CPQQuoteCreationObj.engagementpf.PricingCityLst1, "Pricing Exception Textbox BEnd");
			ClickonElementByString("//div[@data-value='"+Pricing_City+"']", 20);
		}
		
		if(Product_Name.equalsIgnoreCase("EthernetSpoke"))
        {
            verifyExists(CPQQuoteCreationObj.engagementpf.PricingCityLbl, "Pricing Exception BEnd Label");
            verifyExists(CPQQuoteCreationObj.engagementpf.PricingCityHubLst, "Pricing Exception Textbox BEnd");   
            click(CPQQuoteCreationObj.engagementpf.PricingCityHubLst, "Pricing Exception Textbox BEnd");
            ClickonElementByString("//div[@data-value='"+Pricing_City+"']", 20);
                       
            verifyExists(CPQQuoteCreationObj.engagementpf.PricingCityLbl_Spoke, "Pricing Exception Spoke Label");
            verifyExists(CPQQuoteCreationObj.engagementpf.PricingCitySpokeLst, "Spoke Pricing Exception Textbox");   
            click(CPQQuoteCreationObj.engagementpf.PricingCitySpokeLst, "Spoke Pricing Exception Textbox");
            ClickonElementByString("//div[@data-value='"+Pricing_City+"']", 20);
           
        }
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
		click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
		
		if(verifyExists(CPQQuoteCreationObj.engagementpf.POALabel)) 
		{			
			verifyExists(CPQQuoteCreationObj.engagementpf.NRCTxb, "NRC Text Box");	
			click(CPQQuoteCreationObj.engagementpf.NRCTxb, "NRC Text Box");	
			sendKeys(CPQQuoteCreationObj.engagementpf.NRCTxb,NRC, "NRC Text Box");	
			
			verifyExists(CPQQuoteCreationObj.engagementpf.MRCTxb, "MRC Text Box");	
			click(CPQQuoteCreationObj.engagementpf.MRCTxb, "MRC Text Box");	
			sendKeys(CPQQuoteCreationObj.engagementpf.MRCTxb,MRC, "MRC Text Box");
			
			Reusable.waitForpageloadmask();
		       
	        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
	        click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
	        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete, 75);
		}
		
		Reusable.waitForpageloadmask();
	       
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete, 75);
		
		updateSaveProductCPQ("SAVE");
		Reusable.WaitforCPQloader();
		
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send to Sales button");
		click(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send To Sales button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();

	}
	
	public void UpdateBasePriceException(String NRC, String MRC) throws InterruptedException, IOException
	{
		
		waitForElementToAppear(CPQQuoteCreationObj.engagementpf.PricingExcTab, 60);
		verifyExists(CPQQuoteCreationObj.engagementpf.PricingExcTab, "Pricing Exception Tab");			
		click(CPQQuoteCreationObj.engagementpf.PricingExcTab, "Pricing Exception Tab");
		Reusable.WaitforCPQloader();
		
		waitForElementToAppear(CPQQuoteCreationObj.engagementpf.POALabel, 60);
		verifyExists(CPQQuoteCreationObj.engagementpf.POALabel, "POA Exception Label");	
		
		verifyExists(CPQQuoteCreationObj.engagementpf.NRCTxb, "NRC Text Box");	
		click(CPQQuoteCreationObj.engagementpf.NRCTxb, "NRC Text Box");	
		sendKeys(CPQQuoteCreationObj.engagementpf.NRCTxb,NRC, "NRC Text Box");	
		
		verifyExists(CPQQuoteCreationObj.engagementpf.MRCTxb, "MRC Text Box");	
		click(CPQQuoteCreationObj.engagementpf.MRCTxb, "MRC Text Box");	
		sendKeys(CPQQuoteCreationObj.engagementpf.MRCTxb,MRC, "MRC Text Box");	
		
		Reusable.waitForpageloadmask();
	       
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete, 75);
		
		updateSaveProductCPQ("SAVE");
		Reusable.WaitforCPQloader();
		
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send to Sales button");
		click(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send To Sales button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();

	}
	
	public void UpdateBaseCostException(String BaseCost) throws InterruptedException, IOException
	{
		
		waitForElementToAppear(CPQQuoteCreationObj.engagementpf.PricingExcTab, 60);
		verifyExists(CPQQuoteCreationObj.engagementpf.PricingExcTab, "Pricing Exception Tab");			
		click(CPQQuoteCreationObj.engagementpf.PricingExcTab, "Pricing Exception Tab");
		Reusable.WaitforCPQloader();
		
		waitForElementToAppear(CPQQuoteCreationObj.engagementpf.BaseCostLabel, 60);
		verifyExists(CPQQuoteCreationObj.engagementpf.BaseCostLabel, "Base Cost Exception Label");	
		
		verifyExists(CPQQuoteCreationObj.engagementpf.BaseCostTxb, "Base Cost Text Box");	
		click(CPQQuoteCreationObj.engagementpf.BaseCostTxb, "Base Cost Text Box");	
		sendKeys(CPQQuoteCreationObj.engagementpf.BaseCostTxb,BaseCost, "Base Cost Text Box");	
		
		Reusable.waitForpageloadmask();
	       
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.imgLoadComplete, 75);

		updateSaveProductCPQ("SAVE");
		Reusable.WaitforCPQloader();
		
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send to Sales button");
		click(CPQQuoteCreationObj.EngportfolioPage.sendtosales,"Send To Sales button");
		
		Reusable.WaitforCPQloader();
		waitForAjax();
		Reusable.waitForpageloadmask();

	}
	
	public void clickUpdateConfigBtn() throws InterruptedException, IOException
	{
		
		WebElement select = webDriver.findElement(By.xpath("//div[contains(@class,'cpq-table-header-row')]/div[contains(@class,'cpq-table-select')]"));
		scrollIntoView(select);
		select.click();
		
		verifyExists(CPQQuoteCreationObj.CopyQuote.updateConfigBtn,"Update Configuration Button");
		ScrollIntoViewByString(CPQQuoteCreationObj.CopyQuote.updateConfigBtn);
		click(CPQQuoteCreationObj.CopyQuote.updateConfigBtn,"Update Configuration Button");
		Reusable.WaitforCPQloader();
		Reusable.waitToPageLoad();
	}
	
	public String selectProductFeaturesDiversity(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
	// Initializing the Variable
	String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
	String Diversity_Service = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Service");
	String Diversity_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Reference");
	String Diversity_Option = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Option");
	String sResult;

	switch (Product_Name.toUpperCase()) {
	
	case "COLTIPACCESS":
		
	verifyExists(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
	click(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();
	// Entering Diversity details
	ScrollIntoViewByString(CPQQuoteCreationObj.Diversity.IpDiversityLnk);
	waitForElementToAppear(CPQQuoteCreationObj.Diversity.IpDiversityLnk, 10);
	verifyExists(CPQQuoteCreationObj.Diversity.IpDiversityLnk,"'Diversity label'");
	click(CPQQuoteCreationObj.Diversity.IpDiversityflag);
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.Diversity.IpDiversefromService,"'Diverse from Service dropdown'");
	click(CPQQuoteCreationObj.Diversity.IpDiversefromService);
	ClickonElementByString("//div[@data-value='"+Diversity_Service+"']", 20);
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.Diversity.IpDiversityReference,"'Diversity Reference Text Box'");
	sendKeys(CPQQuoteCreationObj.Diversity.IpDiversityReference,Diversity_Reference,"'Ethernet Hub Reference Text Box'");
	//click(CPQQuoteCreationObj.Diversity.ValidateReference);
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();
	break;
	
	
	case "ETHERNETLINE": case "ETHERNETSPOKE":
	
	// Entering Diversity details
	ScrollIntoViewByString(CPQQuoteCreationObj.Diversity.DiversityLnk);
	waitForElementToAppear(CPQQuoteCreationObj.Diversity.DiversityLnk, 10);
	verifyExists(CPQQuoteCreationObj.Diversity.DiversityLnk,"'Diversity label'");
	click(CPQQuoteCreationObj.Diversity.Diversityflag);
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.Diversity.DiversefromService,"'Diverse from Service dropdown'");
	click(CPQQuoteCreationObj.Diversity.DiversefromService);
	ClickonElementByString("//div[@data-value='"+Diversity_Service+"']", 20);
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();
	verifyExists(CPQQuoteCreationObj.Diversity.DiversityReference,"'Diversity Reference Text Box'");
	sendKeys(CPQQuoteCreationObj.Diversity.DiversityReference,Diversity_Reference,"'Ethernet Hub Reference Text Box'");
	//click(CPQQuoteCreationObj.Diversity.ValidateReference);
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();

	verifyExists(CPQQuoteCreationObj.Diversity.Diverseoption,"'Diverse option dropdown'");
	click(CPQQuoteCreationObj.Diversity.Diverseoption);
	ClickonElementByString("//div[@data-value='"+Diversity_Option+"']", 20);
	Reusable.WaitforC4Cloader();
	Reusable.waitForpageloadmask();
	break;
	
	case "WAVE": 
	
		// Entering Diversity details
		ScrollIntoViewByString(CPQQuoteCreationObj.Diversity.DiversityLnk);
		waitForElementToAppear(CPQQuoteCreationObj.Diversity.DiversityLnk, 10);
		verifyExists(CPQQuoteCreationObj.Diversity.DiversityLnk,"'Diversity label'");
		click(CPQQuoteCreationObj.Diversity.Diversityflag);
		Reusable.WaitforC4Cloader();
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.Diversity.DiversefromService,"'Diverse from Service dropdown'");
		click(CPQQuoteCreationObj.Diversity.DiversefromService);
		ClickonElementByString("//div[@data-value='"+Diversity_Service+"']", 20);
		Reusable.WaitforC4Cloader();
		Reusable.waitForpageloadmask();
		verifyExists(CPQQuoteCreationObj.Diversity.DiversityReference,"'Diversity Reference Text Box'");
		sendKeys(CPQQuoteCreationObj.Diversity.DiversityReference,Diversity_Reference,"'Ethernet Hub Reference Text Box'");
		//click(CPQQuoteCreationObj.Diversity.ValidateReference);
		Reusable.WaitforC4Cloader();
		Reusable.waitForpageloadmask();
		break;
	}
	return "True";
	}
	
	public String reconfigureProduct(String Product_Name,String BandwidthValue,String Presentation_Interface, String Connector_Type) throws InterruptedException, IOException
	{
				
		waitForElementToAppear(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthLst, 20);
        verifyExists(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthLst,"'Service Bandwidth List'");
        //ScrollIntoViewByString(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthDropdown);
        scrollUp();
        click(CPQQuoteCreationObj.CPEConfiguration.serviceBandwidthLst,"'Service Bandwidth List'");
       
        sendKeys(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthTxb,BandwidthValue,"'Service Bandwidth List'");
        click(CPQQuoteCreationObj.CPEConfiguration.ServiceBandwidthQry,"'Service Bandwidth Query'");
        ClickonElementByString("//div[@data-value='"+BandwidthValue+"']", 20);
        Reusable.waitForpageloadmask();					
		
		//Selecing Presentation Interface
		scrollUp();
		waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
		ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
		click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst,"'Interface List'");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceALst,"Presentation Interface AList");
		ClickonElementByString("//li[normalize-space(.)='"+ Presentation_Interface +"']", 30);
		Reusable.waitForpageloadmask();

		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst,"Connector Type AList");
		click(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeALst,"Connector Type AList");
		ClickonElementByString("//li[normalize-space(.)='"+ Connector_Type +"']", 30);			
		Reusable.waitForpageloadmask();
		
		if (!Product_Name.equalsIgnoreCase("EthernetHub")&&!Product_Name.equalsIgnoreCase("EthernetSpoke"))
		{
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceBLst,"Presentation Interface BList");
			click(CPQQuoteCreationObj.AdditionalProductDataPage.presentationInterfaceBLst,"Presentation Interface BList");
			ClickonElementByString("//li[normalize-space(.)='"+ Presentation_Interface +"']", 30);
			Reusable.waitForpageloadmask();

			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeBLst,"Connector Type BList");
			click(CPQQuoteCreationObj.AdditionalProductDataPage.connectorTypeBLst,"Connector Type BList");
			ClickonElementByString("//li[normalize-space(.)='"+ Connector_Type +"']", 30);				
			Reusable.waitForpageloadmask();
		}
		
		verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
		ScrollIntoViewByString(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk);
		click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
		Reusable.waitForpageloadmask();
		
	
		return "True";	
	}
	
	
		

	public String C4CSEConfiguration(String SEEngagement_detail, String QuoteID) throws IOException, InterruptedException 
	{		
		       waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,120);
		       verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
		       Reusable.waitForpageloadmask();
		       click(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
		       
		       Reusable.WaitforC4Cloader();
		       waitForAjax();
			   Reusable.waitForpageloadmask();
			   
			   waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.PSEngagementTab, 10);	
			   verifyExists(CPQQuoteCreationObj.SEEngagement.PSEngagementTab,"'PS EngagementTab'");
			   click(CPQQuoteCreationObj.SEEngagement.PSEngagementTab);
			   
			   verifyExists(CPQQuoteCreationObj.SEEngagement.PSEngagementEditBtn,"PSEngagementEditBtn");
			   click(CPQQuoteCreationObj.SEEngagement.PSEngagementEditBtn);
			   
			   verifyExists(CPQQuoteCreationObj.SEEngagement.SERequriedflag,"'SE Requried flag'");
			   click(CPQQuoteCreationObj.SEEngagement.SERequriedflag);
			   
			    verifyExists(CPQQuoteCreationObj.SEEngagement.SEEngagementdetail,"'SE Engagement detail'");
				//click(CPQQuoteCreationObj.SEEngagement.SEEngagementdetail);
				sendKeys(CPQQuoteCreationObj.SEEngagement.SEEngagementdetail,SEEngagement_detail,"'SE Engagement detail'");
				ClickonElementByString("//li[normalize-space(.)='"+SEEngagement_detail+"']", 20);
				
				click(CPQQuoteCreationObj.SEEngagement.Savebtn);
				
				Reusable.waitForpageloadmask();
				Reusable.Waittilljquesryupdated();
/*			   
//				Calling the below method to return from cpq to c4c
				WebDriver Driver1 = WEB_DRIVER_THREAD_LOCAL.get();
				
			    waitForElementToBeVisible(CPQQuoteCreationObj.CopyQuote.quotesLnk, 40);
				click(CPQQuoteCreationObj.CopyQuote.quotesLnk,"quotesLnk");
				Reusable.waitForpageloadmask();
				Reusable.Waittilljquesryupdated();
				
				Driver1.findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")).isEnabled();
				Driver1.findElement(By.xpath("//descendant::a[text()='"+QuoteID+"']")).click();
			    Reusable.waitForpageloadmask();
			    isEnable(CPQQuoteCreationObj.CopyQuote.actionBtn);
			    ScrollIntoViewByString(CPQQuoteCreationObj.CopyQuote.actionBtn);
				Reusable.waitForpageloadmask();
		     	click(CPQQuoteCreationObj.CopyQuote.actionBtn,"actionBtn");
		     	click(CPQQuoteCreationObj.SEEngagement.EditBtn,"EditBtn");
		     	
		     	Reusable.WaitforC4Cloader();
			    waitForAjax();
				Reusable.waitForpageloadmask();
		*/

			return "True";	
	}
	
	public void SetSiebelQRId(String Siebel_QR_Id) throws InterruptedException, IOException
	{
		Reusable.waitToPageLoad();
		Reusable.WaitforCPQloader();
		Reusable.waitForAjax();
		
		verifyExists(CPQQuoteCreationObj.TransactionPage.additionalQuoteDetailsLnk,"Quotes Link");
		ScrollIntoViewByString(CPQQuoteCreationObj.TransactionPage.additionalQuoteDetailsLnk);
		click(CPQQuoteCreationObj.TransactionPage.additionalQuoteDetailsLnk,"Quotes Link");
		
		verifyExists(CPQQuoteCreationObj.TransactionPage.SiebelId,"Sieble QR ID");
		sendKeys(CPQQuoteCreationObj.TransactionPage.SiebelId,Siebel_QR_Id,"Sieble QR ID");
		click(CPQQuoteCreationObj.TransactionPage.SiebelId,"Sieble QR ID");
		Reusable.waitToPageLoad();
	}
	
	public void LinkQuoteWithOpportunity(String Opportunity_ID, String Quote_ID) throws InterruptedException, IOException
	{
		/*waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,120);
		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
		Reusable.waitForpageloadmask();
		click(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
		Reusable.WaitforC4Cloader(); */
		for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
		
		waitForElementToAppear(C4CEngagementSalesObj.salespage.salesLst, 15);
		verifyExists(C4CEngagementSalesObj.salespage.salesLst, "Consultancy");
		click(C4CEngagementSalesObj.salespage.salesLst);
		waitForAjax();
		
		verifyExists(C4CEngagementSalesObj.salespage.quotesTab, "Quotes Tab");
		click(C4CEngagementSalesObj.salespage.quotesTab);
		waitForAjax();
		
		waitForElementToAppear(C4CEngagementSalesObj.salespage.searchIcon, 15);
		verifyExists(C4CEngagementSalesObj.salespage.searchIcon, "Consultancy");
		click(C4CEngagementSalesObj.salespage.searchIcon);
		waitForAjax();

		waitForElementToAppear(C4CEngagementSalesObj.salespage.searchIcon, 15);
		verifyExists(C4CEngagementSalesObj.salespage.searchTxb, "'Quote_ID' text field");
		sendKeys(C4CEngagementSalesObj.salespage.searchTxb, Quote_ID, "Quote_ID");
		
		Reusable.waitForpageloadmask();
        Reusable.Waittilljquesryupdated();
		
		waitForAjax();
		verifyExists(C4CEngagementSalesObj.salespage.searchBtn, "Search Quote");
		click(C4CEngagementSalesObj.salespage.searchBtn);
		String quoteIdObj = CPQLoginObj.Login.QuoteID1+ Quote_ID +CPQLoginObj.Login.QuoteID2;
		
		Reusable.waitForpageloadmask();
        Reusable.Waittilljquesryupdated();
		
		waitForElementToAppear(quoteIdObj, 10);
		verifyExists(quoteIdObj,"Quote ID");
		click(quoteIdObj,"Quote ID");
		
		waitForAjax();
		waitForElementToAppear(C4CEngagementSalesObj.salespage.actionBtn, 15);
		verifyExists(C4CEngagementSalesObj.salespage.actionBtn, "Action Quote");
		click(C4CEngagementSalesObj.salespage.actionBtn);
		
		verifyExists(C4CEngagementSalesObj.salespage.LinkToOpportunity, "Link Opportunity to Quote");
		click(C4CEngagementSalesObj.salespage.LinkToOpportunity, "Link Opportunity to Quote");
		
		waitForElementToAppear(C4CEngagementSalesObj.salespage.linkQuoteLbl, 15);
		verifyExists(C4CEngagementSalesObj.salespage.OpportunitySearchTxb, "Opportunity Search TextBox");
		sendKeys(C4CEngagementSalesObj.salespage.OpportunitySearchTxb,Opportunity_ID, "Opportunity Search TextBox");
		ClickonElementByString("//span[text()='"+ Opportunity_ID +"']", 30);
		Reusable.waitForpageloadmask();
		
		verifyExists(C4CEngagementSalesObj.salespage.proceedBtn, "Proceed Button");
		click(C4CEngagementSalesObj.salespage.proceedBtn, "Proceed Button");
		
		Reusable.WaitforCPQloader();
        waitForAjax();
        Reusable.waitForpageloadmask();   
		
	}
	
	public void LIG_Feature_Verification(String ProductName,String FeatureName1, String FeatureName2) throws IOException, InterruptedException
	{
		
		switch(ProductName)
		{
		case "EthernetLine": case "EthernetHub": case "Wave": case "ColtIpAccess": case "EthernetSpoke":
			Reusable.MultiLineWebTableCellAction("Product Description",FeatureName1 ,"Product Description","LIGVerification", null, 1);
		
			if(!ProductName.equalsIgnoreCase("EthernetHub")&&!ProductName.equalsIgnoreCase("ColtIpAccess"))
			{
				Reusable.MultiLineWebTableCellAction("Product Description",FeatureName2 ,"Product Description","LIGVerification", null, 1);
			}
			
		}
						
	}
	
	public String ViewManualRequestTab(String CPQ_Common_ID) throws InterruptedException, IOException
	{
		verifyExists(CPQQuoteCreationObj.ManualRequestStatus.ManualRequestTab_CommPge, "View Manual Request Tab");
		ScrollIntoViewByString(CPQQuoteCreationObj.ManualRequestStatus.ManualRequestTab_CommPge);
		click(CPQQuoteCreationObj.ManualRequestStatus.ManualRequestTab_CommPge, "View Manual Request Tab");
		waitForElementToAppear(CPQQuoteCreationObj.ManualRequestStatus.UpdateExpDetailsBtn, 15);
		click(CPQQuoteCreationObj.ManualRequestStatus.UpdateExpDetailsBtn, "Update Explore Details Button");
		String Status = Reusable.ValidateExploreDetails("Explore Request ID",CPQ_Common_ID ,"Status","GetValue", null, 1);
		return Status;
	}
	
	public void ViewManualRequest_Status(String ProductName,String testDataFile, String dataSheet, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException, AWTException
	{
		String CPQ_Common_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPQ_Common_ID");
		String CPQ_Common_ID1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End_CPQ_Common_ID");
		String Request_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Request_ID");
		String Request_ID1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "B_End_Request_ID");
		
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Flow_Type");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Explore_Options");
		String offnetCheckBtn = null; String dslCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;	    
		String cpqErrorMsg_Line = null; String cpqErrorMsg_Spoke = null; String cpqErrorMsg_IpAccess = null; String Status = null;
		
		
		
		switch (SiteName) {
		
		case "A_End":
			if(Flow_Type.equalsIgnoreCase("ManualNearnet")) {
				Status = ViewManualRequestTab(Request_ID);
			}
			else
			{
				Status = ViewManualRequestTab(CPQ_Common_ID);
			}
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			offnetCheckBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckAEndBtn;
			dslCheckBtn= CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckAEndBtn;
			cpqErrorMsg_Line = CPQQuoteCreationObj.AdditionalProductDataPage.ManualReqRejErrorAEnd;
			cpqErrorMsg_Spoke = CPQQuoteCreationObj.AdditionalProductDataPage.ManualReqRejErrorSpokeEnd;
			cpqErrorMsg_IpAccess = CPQQuoteCreationObj.AdditionalProductDataPage.ManualReqRejErrorIpAccess;
			
			break;
			
		case "B_End":
			if(Flow_Type.equalsIgnoreCase("ManualNearnet")) {
				Status = ViewManualRequestTab(Request_ID1);
			}
			else
			{
				Status = ViewManualRequestTab(CPQ_Common_ID1);
			}			
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			offnetCheckBtn=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.OffnetCheckBEndBtn;
			dslCheckBtn= CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DslCheckBEndBtn;
			cpqErrorMsg_Line = CPQQuoteCreationObj.AdditionalProductDataPage.ManualReqRejErrorBEnd;
			break;
		}				
		
		clickProductConfigurationBtn(ProductName);
		
		switch (Status) {
		
		case "REJECTED":
			switch(ProductName) {
			
			case "EthernetSpoke":
				verifyExists(cpqErrorMsg_Spoke);
				break;
				
			case "EthernetLine":
				verifyExists(cpqErrorMsg_Line);
				break;
				
			case "ColtIpAccess":
				verifyExists(cpqErrorMsg_IpAccess);
				break;
			}
			
			if(ProductName.equalsIgnoreCase("EthernetLine")||ProductName.equalsIgnoreCase("EthernetSpoke")||ProductName.equalsIgnoreCase("ColtIpAccess")) 
			{
				if(Flow_Type.equalsIgnoreCase("ManualOffnet"))
				{
					offnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, SiteName);				
				}
				else if(Flow_Type.equalsIgnoreCase("ManualNearnet"))
				{
					nearnetEntries(testDataFile, dataSheet, scriptNo, dataSetNo, SiteName);
					
				}
				else if(Flow_Type.equalsIgnoreCase("ManualDSL"))
				{
					dslEntries_MultiRequest(testDataFile, dataSheet, scriptNo, dataSetNo, SiteName);					
				}
				else if(Flow_Type.equalsIgnoreCase("ManualDiverse"))
				{
					ManualDiverseEntries(testDataFile, dataSheet, scriptNo, dataSetNo, SiteName);
				}
			}
			else if(ProductName.equalsIgnoreCase("Wave"))
			{		
				CPQLogin.CPQLogout();		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);	
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				clickProductConfigurationBtn(ProductName);
				offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);							
			}
			break;
			
		case "inprogress": case "QUOTED":
			
			if(Flow_Type.equalsIgnoreCase("ManualOffnet"))
			{
				if(!verifyExists(offnetCheckBtn))
				{
					System.out.println("Not able to raise offnet Manual Request if the previous Request was Approved by eXplore User");
				}
			}
			else if(Flow_Type.equalsIgnoreCase("ManualDSL"))
			{
				if(!verifyExists(dslCheckBtn))
				{
					System.out.println("Not able to raise DSL Manual Request if the previous Request was Approved by eXplore User");
				}				
			}
			else if(Flow_Type.equalsIgnoreCase("ManualNearnet")||Flow_Type.equalsIgnoreCase("ManualDiverse"))
			{
				verifyExists(manualEngagementBtn,"Manual Engagement Button");
				ScrollIntoViewByString(manualEngagementBtn);
				click(manualEngagementBtn,"Manual Engagement Button");
				
				verifyExists(exploreOptionsLsb, "Explore List");
				click(exploreOptionsLsb, "A End List");
				if(!verifyExists("//li[normalize-space(.)='"+Explore_Options+"']"))
				{
					System.out.println("Not able to raise Nearnet/Diverse Manual Request if the previous Request was Approved by eXplore User");
				}
				click(manualEngagementBtn,"Manual Engagement Button");
			
			}				
			updateSaveProductCPQ("Save");
			break;
		}

	}
	
	public void ManualDiverseEntries(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException 

	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Diversity_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Type");
		String Explore_Options = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Explore_Options");	
		String Priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Priority");
		String Connection_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connection_Type");
		String offnetCheckBtn = null; String manualEngagementBtn = null; String exploreOptionsLsb = null;
		String exploreOptionsSubLsb = null;String offnetCheckRadioButton=null; String cpqErrorMsg = null;
		
		switch (SiteName) {
		
		case "A_End":
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsAEndSubLst;
			offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetAManualOffnetRdB;
			cpqErrorMsg = CPQQuoteCreationObj.AdditionalProductDataPage.ManualReqRejErrorAEnd;
			break;
			
		case "B_End":
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ManualEngagementBEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreActionsBEndSubLst;
			offnetCheckRadioButton=CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.EtherNetBManualOffnetRdB;
			cpqErrorMsg = CPQQuoteCreationObj.AdditionalProductDataPage.ManualReqRejErrorBEnd;
			break;
		}
		if(Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("EthernetHub")||
				Product_Name.equalsIgnoreCase("EthernetSpoke")||Product_Name.equalsIgnoreCase("Wave")
				&&!Product_Name.equalsIgnoreCase("ColtIpAccess")&&!Product_Name.equalsIgnoreCase("VPNNetwork")) 
		{
			verifyExists(manualEngagementBtn,"'Manual Engagement Button'");
			scrollToViewNClick(manualEngagementBtn,"Manual Engagement Button");			
			//click(manualEngagementBtn,"'Manual Engageent Button'");			
				
			waitForElementToAppear(exploreOptionsLsb, 20);
			
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			scrollToViewNClick(exploreOptionsLsb,"Explore List Box");
			//Reusable.waitForpageloadmask();
			System.out.println("//li[normalize-space(.)='"+Explore_Options+"']");
			ClickonElementByString("//li[normalize-space(.)='"+Explore_Options+"']",30);
			
			switchToFrame("exploreEngagementComponent_oob");				
		}
		
		if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
			
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpManualEngagementAEndBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndLst;
			exploreOptionsSubLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpExploreActionsAEndSubLst;
			
			verifyExists(manualEngagementBtn,"Manual Engagement Button");
			scrollToViewNClick(manualEngagementBtn,"Manual Engagement Button");	
			//click(manualEngagementBtn,"Manual Engagement Button");			
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb,75);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			ScrollIntoViewByString(exploreOptionsLsb);
			isEnable(exploreOptionsLsb);
			Reusable.waitForpageloadmask();
			
			ClickonElementByString("//li[normalize-space(.)='"+Explore_Options+"']",30);
			//selectByValueDIV(exploreOptionsLsb, exploreOptionsSubLsb, Explore_Options);
			switchToFrame("exploreEngagementComponent");			
		}
		
		if (Product_Name.equalsIgnoreCase("VPNNetwork")) {
			
			manualEngagementBtn = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnManualCheckBtn;
			exploreOptionsLsb = CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreRequestVPN;
			
			verifyExists(manualEngagementBtn,"'Manual Engageent Button'");
			click(manualEngagementBtn,"'Manual Engageent Button'");
			Reusable.waitForpageloadmask();
			
			waitForElementToAppear(exploreOptionsLsb,75);
			verifyExists(exploreOptionsLsb,"'Explore Option List box'");
			isEnable(exploreOptionsLsb);
			click(exploreOptionsLsb,"'Explore Option List box'");

			ClickonElementByString("//li[normalize-space(.)='"+Explore_Options+"']",30);

			//selectByValueDIV(exploreOptionsLsb, Explore_Options,"'Select Explore Option'");

			Reusable.WaitforProdConfigLoader();	
			switchToFrame("exploreEngagementComponent_oob");	
		}
		
		    waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,10);
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,"'Prioriry Text Box'");			
			
			/*String buliding_number=findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreBuldingNumberTxb).getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Buliding Number is "+buliding_number);
			System.out.println("Explore Buliding Number is "+buliding_number);*/
						
			sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb, Priority);
			keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.PriorityTxb,Keys.ENTER);
			Reusable.waitForpageloadmask();
			
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DiversityTypeTxb,"'Diversity Type Text Box'");
			sendKeys(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DiversityTypeTxb, Diversity_Type);
			keyPress(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DiversityTypeTxb,Keys.ENTER);
			Reusable.waitForpageloadmask();
			
			if (Connection_Type.equalsIgnoreCase("DualEntry")) 
			{ 
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.DualEntryCbx); 
			}
				
			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,20);				
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'");
			isClickable(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn);
			Thread.sleep(2000);
			click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.GetQuoteBtn,"'Quote Button'"); 
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();


            //WebInteractUtil.pause(4000);
            //Reusable.waitForAjax();
               
            //webDriver.switchTo().alert().accept();	
            //WebInteractUtil.pause(2000);
            //Reusable.waitForAjax();
               
            verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
            click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExpandArrowExploreIcn,"'Expand Explore Icon'");
				
			waitForElementToAppear(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,75);
			verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem,"'Request ID'");

			String fullVal = findWebElement(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.RequestIDElem).getText();
			String[] bits = fullVal.split(":");
			String Request_ID = bits[bits.length-1].trim();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
			System.out.println("Request ID "+Request_ID+" got Generated for the product "+Product_Name+" for the site "+sheetName);
	//				Passing this request ID to testdata sheet
			if(SiteName.equalsIgnoreCase("A_End"))
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Request_ID", Request_ID);
			}
			else
			{
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_Request_ID", Request_ID);
			}
			switchToDefaultFrame();
				
			if (Product_Name.equalsIgnoreCase("VPNNetwork")) 
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.VpnExploreOffCloseBtn,"'VPN Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			}
						
			else if (Product_Name.equalsIgnoreCase("ColtIpAccess")) 
			{ 
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"'IP Access Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.IpAccessexploreCloseBtn,"''");
				Reusable.WaitforProdConfigLoader();
			} 
			else 
			{
				verifyExists(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'"); 
				click(CPQQuoteCreationObj.Offnet_Nearnet_ManualOrder.ExploreCloseBtn,"'Explore Close Button'");
				Reusable.WaitforProdConfigLoader();
			}			
		if(Product_Name.contains("Ethernet")||Product_Name.contains("Wave")) 
		{
			waitForElementToAppear(offnetCheckRadioButton,60);
			verifyExists(offnetCheckRadioButton,"Access Type Radio Button");
			scrollIntoView(findWebElement(offnetCheckRadioButton));
			click(offnetCheckRadioButton,"Access Type Radio Button");
			Reusable.waitForpageloadmask();		
		}
		verifyExists(manualEngagementBtn,"'Manual Engagement Button'");
		scrollToViewNClick(manualEngagementBtn,"Manual Engagement Button");
	}
	
	public void diverseConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
		
		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
        String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		
		
		switch (Flow_Type.toUpperCase()) {
				
			case "MANUALDIVERSE":
				
//				Entering A-End details
				ManualDiverseEntries(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
										
//				Entering B-End details
				if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
					ManualDiverseEntries(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
					
				}
				break;
		}
										
	}
	
	public String copyQuoteWithMultiRequests(String testDataFile, String SheetName, String scriptNo, String dataSetNo, String QuoteID) throws IOException, InterruptedException, AWTException
	{
		
		String Flow_Type=DataMiner.fngetcolvalue(testDataFile, SheetName, scriptNo, dataSetNo, "Flow_Type");
		String ProductName=DataMiner.fngetcolvalue(testDataFile, SheetName, scriptNo, dataSetNo, "Product_Name");
		String Quote_ID = DataMiner.fngetcolvalue(testDataFile, SheetName, scriptNo, dataSetNo,"Quote_ID");
		String Copy_Type = DataMiner.fngetcolvalue(testDataFile, SheetName, scriptNo, dataSetNo,"Copy_Type");
//		Quote_ID = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");

		String sProduct = ProductName.replaceAll("(?!^)([A-Z])", " $1");
		String sResult;		

			
		//calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Waiting for 3rd Party");						
					
		//		calling the below method to verify quote stage and status
		sResult = logoutCPQ("Main");
		
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.EXPLORE_URL);
		ExpConfig.offnetExploreOperations(testDataFile, SheetName, scriptNo, dataSetNo);
																				
		//		Calling the Below method to perform C4C Login	
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.CPQ_URL);
		CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
		CPQLogin.SelectQuote(testDataFile, SheetName, scriptNo, dataSetNo);
		
//		Copy Quote
		sResult=copyQuote(testDataFile, SheetName, scriptNo, dataSetNo);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		
		if (!Copy_Type.equalsIgnoreCase("Lineitem")) {
			click(CPQQuoteCreationObj.CopyQuote.offnetcopyQuoteOKBtn);
			Reusable.waitForpageloadmask();
//			calling the below method to verify quote stage and status
			sResult = verifyQuoteStage("Created");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Quote_ID = DataMiner.fngetcolvalue(testDataFile, SheetName, scriptNo, dataSetNo,"Copy_Quote_ID");
			
//			calling the below method reconfigure the product
			sResult = clickProductConfigurationBtn(ProductName);
			if (sResult.equalsIgnoreCase("False")){ return "False";}
			
//			calling the below method to update and save the product in cpq
			sResult =updateSaveProductCPQ("SAVE");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
		}else {
//			Un Select the first line item grid
			sResult = Reusable.MultiLineWebTableCellAction("Product", sProduct, null,"Click", null,Integer.parseInt("1"));
//			Below method for Copy Line Item Flow
			sResult=copyQuoteLineItemEntries(testDataFile, SheetName, scriptNo, dataSetNo,"Click");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			}
		
//		calling the below method to verify quote stage and status
		sResult = verifyQuoteStage("Waiting for 3rd Party");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		if(ProductName.equalsIgnoreCase("Wave")) {			
				
			CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);	
			CPQLogin.SelectQuote(testDataFile, SheetName, scriptNo, dataSetNo);

			clickProductConfigurationBtn(ProductName);
			offnetandNearNetConfigurationWave(testDataFile, SheetName, scriptNo, dataSetNo);
			updateSaveProductCPQ("Save");				
		}
		else
		{
			offnetConfiguration(testDataFile, SheetName, scriptNo, dataSetNo);
		}
				
		//		calling the below method to Reject the Request in Explorer
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.EXPLORE_URL);
		ExpConfig.offnetExploreRejectAction(testDataFile, SheetName, scriptNo, dataSetNo);		
								
		//		Calling the Below method to perform C4C Login and navigage to the Quote in CPQ	
		close();
		openBrowser(g.getBrowser());
		openurl(Configuration.CPQ_URL);
		CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
		CPQLogin.SelectQuote(testDataFile, SheetName, scriptNo, dataSetNo);										
																			
		// 		calling the below method to verify the Status of Manual Requests in CPQ and Recreate the Manual Request												
		ViewManualRequest_Status(ProductName,testDataFile, SheetName, scriptNo, dataSetNo, "A_End");
		markPartialSave(ProductName);
		ViewManualRequest_Status(ProductName,testDataFile, SheetName, scriptNo, dataSetNo, "B_End");
		updateSaveProductCPQ("Save");
		verifyQuoteStage("Waiting for 3rd Party");
									
		//		calling the below method to configure the product
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.EXPLORE_URL);
		ExpConfig.offnetExploreOperations(testDataFile, SheetName, scriptNo, dataSetNo);
																				
		//		Calling the Below method to perform C4C Login	
		close();
		openBrowser(g.getBrowser());		
		openurl(Configuration.CPQ_URL);
		CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
		CPQLogin.SelectQuote(testDataFile, SheetName, scriptNo, dataSetNo);
										
													
		// 		calling the below method to verify the Status of Manual Requests in CPQ and Recreate the Manual Request
		ViewManualRequest_Status(ProductName,testDataFile, SheetName, scriptNo, dataSetNo, "A_End");
		updateSaveProductCPQ("Save");
		ViewManualRequest_Status(ProductName,testDataFile, SheetName, scriptNo, dataSetNo, "B_End");
		updateSaveProductCPQ("Save");

					
		return QuoteID;
	}

	public String selectProductFeaturesNetworkEncryption(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String EncryptionKey_type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EncryptionKey_type");
		String AEnd_Location = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd_Location");
		String BEnd_Location = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BEnd_Location");
		String sResult;
		
		
//		        Entering SiteNameAlias details		
		        ScrollIntoViewByString(CPQQuoteCreationObj.SiteNameAlias.ServiceFeaturesTab);
		        waitForElementToAppear(CPQQuoteCreationObj.SiteNameAlias.ServiceFeaturesTab, 10);
		        verifyExists(CPQQuoteCreationObj.SiteNameAlias.ServiceFeaturesTab,"'Service Features label'");
				
				click(CPQQuoteCreationObj.NetworkEncryption.NetworkEncryptionflag);
				
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.NetworkEncryption.SelectEncryptionKey,"'Select Encryption Key'");
				click(CPQQuoteCreationObj.NetworkEncryption.SelectEncryptionKey);
				sendKeys(CPQQuoteCreationObj.NetworkEncryption.SelectEncryptionKey,EncryptionKey_type,"'EncryptionKey List'");
				ClickonElementByString("//div[@data-value='"+EncryptionKey_type+"']", 20);
							
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.NetworkEncryption.AEndEncryptionEquipmentLocation,"'AEnd Encryption Equipment Location dropdown'");
				click(CPQQuoteCreationObj.NetworkEncryption.AEndEncryptionEquipmentLocation);
				sendKeys(CPQQuoteCreationObj.NetworkEncryption.AEndEncryptionEquipmentLocation,AEnd_Location,"'AEnd Encryption Equipment Location dropdown'");
				ClickonElementByString("//div[@data-value='"+AEnd_Location+"']", 20);
					
				Reusable.WaitforC4Cloader();
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.NetworkEncryption.BEndEncryptionEquipmentLocation,"'BEnd Encryption Equipment Location dropdown'");
				click(CPQQuoteCreationObj.NetworkEncryption.BEndEncryptionEquipmentLocation);
				sendKeys(CPQQuoteCreationObj.NetworkEncryption.BEndEncryptionEquipmentLocation,BEnd_Location,"'BEnd Encryption Equipment Location dropdown'");
				ClickonElementByString("//div[@data-value='"+BEnd_Location+"']", 20);
				
				return "True";
	}
	public String SEEngagement(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
    {        
//        Initializing the Variable
        String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
        String Complex_PriceApplicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Reference");
        String Bespoke_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Applicable");
        String JapanColtFibre_NG_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"JapanColtFibre_NG_Applicable");
        String AdditionalcostsforBespoke = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AdditionalcostsforBespoke");
        String Pick_Siebel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Pick_Siebel");
        String sResult;
        String sProduct = null;


        Reusable.WaitforCPQloader();
        Reusable.waitForpageloadmask();
        waitForAjax();

//        Calling the below method if the Bespoke are applicable
        if ((Bespoke_Applicable.equalsIgnoreCase("Yes"))||(Bespoke_Applicable.equalsIgnoreCase("Reconfigure")));
        {        
          scrollUp();
        waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,10);
        verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"Approval Link");
        click(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"Approval Link");
        Reusable.WaitforCPQloader();
        waitForAjax();

        verifyExists(CPQQuoteCreationObj.SEEngagement.AdditionalcostsforBespoke,"'AdditionalcostsforBespoke'");
        click(CPQQuoteCreationObj.SEEngagement.AdditionalcostsforBespoke);
        //sendKeys(CPQQuoteCreationObj.SEEngagement.AdditionalcostsforBespoke,AdditionalcostsforBespoke,"'AdditionalcostsforBespoke'");
        ClickonElementByString("//li[normalize-space(.)='"+AdditionalcostsforBespoke+"']", 20);

        }
 
 
        if (!Product_Name.equalsIgnoreCase("CpeSolutionsSite")) {


        	// calling the below method reconfigure the product
        	clickProductConfigurationBtn(Product_Name);
        	
         	
        	//New added for Bespoke Siebel journey
        /*	if (Pick_Siebel.equalsIgnoreCase("Yes"));
        	{
        		Reusable.waitForpageloadmask();
            	Reusable.WaitforCPQloader();
            	waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
            	ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
            	click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
            	Reusable.waitForpageloadmask();
        		
        	String BespokeDes =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Description");;
        	ScrollIntoViewByString(CPQQuoteCreationObj.SEEngagement.BespokeCustViewDescription);
        	waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.BespokeCustViewDescription, 10);
        	verifyExists(CPQQuoteCreationObj.SEEngagement.BespokeCustViewDescription,"Cutomer Niew");
        	click(CPQQuoteCreationObj.SEEngagement.BespokeCustViewDescription);
        	sendKeys(CPQQuoteCreationObj.SEEngagement.BespokeCustViewDescription,BespokeDes);
        	Reusable.waitForpageloadmask();
        	
        	ScrollIntoViewByString(CPQQuoteCreationObj.SEEngagement.BespokeColttViewNote);
        	waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.BespokeColttViewNote, 10);
        	verifyExists(CPQQuoteCreationObj.SEEngagement.BespokeColttViewNote,"Colt View");
        	click(CPQQuoteCreationObj.SEEngagement.BespokeColttViewNote);
        	sendKeys(CPQQuoteCreationObj.SEEngagement.BespokeColttViewNote,BespokeDes);
        	Reusable.waitForpageloadmask();
        	}*/
        	
        	verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn, "Validate Button");
        	click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        	Reusable.WaitforCPQloader();

        	// calling the below method to update and save the product in cpq
        	updateSaveProductCPQ("Save");

        	 }
                ScrollIntoViewByString(CPQQuoteCreationObj.SEEngagement.SEEngagementTab);
                waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.SEEngagementTab, 10);    
                verifyExists(CPQQuoteCreationObj.SEEngagement.SEEngagementTab,"'SE EngagementTab'");
                click(CPQQuoteCreationObj.SEEngagement.SEEngagementTab);

                Reusable.WaitforC4Cloader();
                Reusable.waitForpageloadmask();

                verifyExists(CPQQuoteCreationObj.SEEngagement.ViewReasonBtn,"'View Reason Btn'");
                click(CPQQuoteCreationObj.SEEngagement.ViewReasonBtn);

                Reusable.WaitforC4Cloader();
                Reusable.waitForpageloadmask();

                verifyExists(CPQQuoteCreationObj.SEEngagement.ViewReasontable,"'View Reason table'");

                Reusable.WaitforC4Cloader();
                Reusable.waitForpageloadmask();
 
 
 
                verifyExists(CPQQuoteCreationObj.SEEngagement.technicalfeacompleteBtn,"'technical feasibility complete'");
                click(CPQQuoteCreationObj.SEEngagement.technicalfeacompleteBtn);

                Reusable.WaitforC4Cloader();
                Reusable.waitForpageloadmask();

                return "True";    
    }
	public String C4CSEConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String SEEngagement_detail = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SEEngagement_detail");
		String QuoteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quote_ID");
		String sResult;
		
		
		       waitForElementToBeVisible(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,120);
		       verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
		       Reusable.waitForpageloadmask();
		       click(CPQQuoteCreationObj.AdditionalProductDataPage.returnToC4CBtn,"Return To C4C Button");
		       
		       Reusable.WaitforC4Cloader();
		       waitForAjax();
			   Reusable.waitForpageloadmask();
			   
			   waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.PSEngagementTab, 10);	
			   verifyExists(CPQQuoteCreationObj.SEEngagement.PSEngagementTab,"'PS EngagementTab'");
			   Reusable.waitForpageloadmask();
			   click(CPQQuoteCreationObj.SEEngagement.PSEngagementTab,"'PS EngagementTab'");
			   
			   Reusable.waitForpageloadmask();
               Reusable.Waittilljquesryupdated();
			   
			   waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.PSEngagementEditBtn, 20);
			   verifyExists(CPQQuoteCreationObj.SEEngagement.PSEngagementEditBtn,"'PSEngagementEditBtn'");
			   click(CPQQuoteCreationObj.SEEngagement.PSEngagementEditBtn,"'PSEngagementEditBtn'");
			   
			   Reusable.waitForpageloadmask();
               Reusable.Waittilljquesryupdated();
			   
			   verifyExists(CPQQuoteCreationObj.SEEngagement.SERequriedflag,"'SE Requried flag'");
			   click(CPQQuoteCreationObj.SEEngagement.SERequriedflag);
			   
			   Reusable.waitForpageloadmask();
               Reusable.Waittilljquesryupdated();
			   
			          
                verifyExists(CPQQuoteCreationObj.SEEngagement.SEEngagementdetail,"'SE Engagement detail'");
                //click(CPQQuoteCreationObj.SEEngagement.SEEngagementdetail);
                sendKeys(CPQQuoteCreationObj.SEEngagement.SEEngagementdetail,SEEngagement_detail,"'SE Engagement detail'");
                ClickonElementByString("//li[normalize-space(.)='"+SEEngagement_detail+"']", 20);
                
                Reusable.waitForpageloadmask();
                Reusable.Waittilljquesryupdated();
				
				click(CPQQuoteCreationObj.SEEngagement.Savebtn);
				
				Reusable.waitForpageloadmask();
				Reusable.Waittilljquesryupdated();
			   

			return "True";	
	}
	
	public String selectProductFeaturesCustomerDefRoute(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
    {       
//        Initializing the Variable
        String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
       
       
        switch (Product_Name.toUpperCase()) {
       
               case "WAVE":
                       
       
//                Entering Diversity details       
                ScrollIntoViewByString(CPQQuoteCreationObj.SEEngagement.CustomerDefinedRoutetitle);
                waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.CustomerDefinedRoutetitle, 10);
                verifyExists(CPQQuoteCreationObj.SEEngagement.CustomerDefinedRoutetitle,"'Diversity label'");   
               
                click(CPQQuoteCreationObj.SEEngagement.CustomerDefinedRouteflag);
        }
        		return "True";	
    	}
	
	public String SE_ReEngagement(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Bespoke_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Applicable");
		String NotifySEReview = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NotifySEReview");
		String Reason_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Reason_value");
		String Diversity_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Applicable");	
		String Diversity_Option_Re = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Diversity_Option_Re");
		String JapanColtFibre_NG_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"JapanColtFibre_NG_Applicable");
		String CustomerDefRoute_Applicable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CustomerDefRoute_Applicable");
		String Non_Standard_Interface_flow = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Non_Standard_Interface_flow");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");
		String ReConfigureSE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ReConfigureSE");
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();
		waitForAjax();
		
//		calling the below method reconfigure the product
		clickProductConfigurationBtn(Product_Name);
		
		
//        Calling the below method if the Customer Defined Route are applicable
        if (Flow_Type.equalsIgnoreCase("ULLFIBRE")) {
        	waitForElementToAppear(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk, 120);
        	verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
        	click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
        	Reusable.waitForpageloadmask();

        	waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.onnetAlnk, 20);
        	verifyExists(CPQQuoteCreationObj.ProductConfigration.onnetAlnk,"'Onnet Check Button'");
        	scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetAlnk);
        	click(CPQQuoteCreationObj.ProductConfigration.onnetAlnk,"'Onnet Check Button'");
        	Reusable.waitForpageloadmask();

        	scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetAendRadioBtn);
        	click(CPQQuoteCreationObj.ProductConfigration.onnetAendRadioBtn,"'Onnet Check Button'");


        	// For EthernetLine
        	if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave"))
        	{
        	waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.onnetBlnk, 20);
        	verifyExists(CPQQuoteCreationObj.ProductConfigration.onnetBlnk,"'Onnet Check Button'");
        	scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetBlnk);
        	click(CPQQuoteCreationObj.ProductConfigration.onnetBlnk,"'Onnet Check Button'");
        	Reusable.waitForpageloadmask();

        	scrollDown(CPQQuoteCreationObj.ProductConfigration.onnetBendRadioBtn);
        	click(CPQQuoteCreationObj.ProductConfigration.onnetBendRadioBtn,"'Onnet Check Button'");
        	Reusable.waitForpageloadmask();
        	}
            
        }  
		
		
     	if (Product_Name.equalsIgnoreCase("ColtIpAccess")) {
        	verifyExists(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
        	click(CPQQuoteCreationObj.ServiceFeatures.servicefeatures,"Service Features");
        	} 
        
     	else {
        	Reusable.waitForpageloadmask();
        	Reusable.WaitforCPQloader();
        	waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
        	ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
        	click(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk,"'Features Link'");
        	Reusable.waitForpageloadmask();
        	} 
		

        
//        Calling the below method if the Diversity are applicable
        if (Diversity_Applicable.equalsIgnoreCase("Yes")) 
        {
        	
        	verifyExists(CPQQuoteCreationObj.Diversity.Diverseoption,"'Diverse option dropdown'");
            click(CPQQuoteCreationObj.Diversity.Diverseoption);
           // sendKeys(CPQQuoteCreationObj.Diversity.Diverseoption,Diversity_Option,"'Diverse type List'");
            ClickonElementByString("//div[@data-value='"+Diversity_Option_Re+"']", 20);
        	
        }
        else if (Diversity_Applicable.equalsIgnoreCase("Reconfigure")) 
        {
   		  selectProductFeaturesDiversity(testDataFile, sheetName, scriptNo, dataSetNo);
        }
        
        
//      Calling the below method if the Reconfigure Bespoke are applicable
      if (Bespoke_Applicable.equalsIgnoreCase("Reconfigure")) 
      {
      	
      selectProductFeaturesBespoke(testDataFile, sheetName, scriptNo, dataSetNo);	
      
      
      }
        
        
        if (JapanColtFibre_NG_Applicable.equalsIgnoreCase("Reconfigure")) 
        {
        	
    		waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.SiteAddresslnk, 60);
    		ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.SiteAddresslnk);
    		click(CPQQuoteCreationObj.Onnet_DualEntry_Features.SiteAddresslnk,"'SiteAddress Link'");
    		Reusable.waitForpageloadmask();
        	        	
        	String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
    		String IPAccess_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPAccess_Type");
    		System.out.println("Product Name in Product_Configuration:"+ProductName);
    		
    		if(IPAccess_Type.equalsIgnoreCase("New")) {
    			ProductName="ColtIpAccess";
    		}
    				
    		Reusable.WaitforProdConfigLoader();		
    				
    		String sResult;
    		switch (ProductName) {
    		
    		case "CPESolutionsSite": 
    			
//    			Initializing the Variable
    			String SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");			

    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddTxFld, "'Site Address' text field");

    			Reusable.waitForpageloadmask();
    			sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb, "Iidabashi Grand Bloom 2-10-2 Fujimi Chiyoda-ku Japan","Site Address");
    			Reusable.waitForpageloadmask();
    			//Click on the 'Search for an Address' icon
    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
    			click(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
    			Reusable.WaitforProdConfigLoader();			
    			break;
    			
    		case "EthernetLine": case "EthernetHub": case "Wave":
    			
    			
    			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");
    			String SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
    			String Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
    			System.out.println("I am in switchcase "+SiteA_Address);
    			Reusable.waitForpageloadmask();

    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb, "'Site Address' text field");
    			sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb, ReConfigureSE,"Site Address");
    			Reusable.WaitforCPQloader();
    			//Click on the 'Search for an Address' icon
    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
    			click(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
    			Reusable.WaitforCPQloader();
    			Reusable.WaitforProdConfigLoader();
    			webDriver.switchTo().frame("siteAddressLink");			

    			AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
    			//if (sResult.equalsIgnoreCase("False")) { return "False"; }
    			
//    			Entering Site Contact Number if the flow type is Manual/Automated DSL
    			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
    				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressLnk, "'Site A Address Link'");
    				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressLnk, "'Site A Address Link'");				
    				Reusable.waitForpageloadmask();
    				
    				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb, "'Site A Phone Number Textbox'");
    				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb, Phone_Number, "'Site A Phone Number Textbox'");
    				
    				Reusable.waitForpageloadmask();
    				
    				verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn, "'Validate Address A Button'");
    				click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn, "'Validate Address A Button'");
    				Reusable.waitForpageloadmask();
    			}
    			
    			//Site B Address Entry
    			if (!ProductName.equalsIgnoreCase("EthernetHub")) {
//    				Reading the SiteAddress details
    				String SiteB_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address");
    				String SiteB_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address_Type");
    				Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
    				
    				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressTxb,"Site B Textfield");
    				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk, "'Site B Address Link'");	
    				Reusable.waitForpageloadmask();
    				
    				scrollToViewNClick(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressTxb,"'Site B Address Text Field'");
    				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressTxb, ReConfigureSE);
    				Reusable.WaitforCPQloader();
    				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteBSearchBtnImg);
    				waitForAjax();
    				Reusable.WaitforCPQloader();
    				Reusable.WaitforProdConfigLoader();
    				//By executing a java script
    				webDriver.switchTo().frame("siteAddressLink");
    				//Validate address type
    				AddressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
    				
    				//if (sResult.equalsIgnoreCase("False")) { return "False"; }
    				if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) { 
    					verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk,"Site B Address Link");
    					Reusable.waitForpageloadmask();
    					
    					verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteBPhoneNumberTxb,"Site B Phone Number Text field");
    					sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteBPhoneNumberTxb, Phone_Number,"Phone Number Text Box"); 
    					Reusable.waitForpageloadmask();
    					
    					verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressBBtn,"Site B Validat Button");
    					click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressBBtn);
    					Reusable.waitForpageloadmask();
    				}
    				
    			}
    			break;
    			
    		case "EthernetSpoke":
    			//Configuring Hub part if the network type is existing
    			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address");
    			String SiteB_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address");
    			String Hub_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hub_Type");
    			String Hub_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Hub_Reference");
    			SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
    			
    			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeLst,"'Ethernet Hub Type dropdown'");
    			click(CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeLst);
    			//selectByValueDIV(CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeLst,CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeSubLst,Hub_Type);
    			sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.HubTypeLst,Hub_Type,"'Hub type List'");
    			ClickonElementByString("//div[@data-value='"+Hub_Type+"']", 20);
    			
    			Reusable.WaitforC4Cloader();
    			Reusable.waitForpageloadmask();
    			
    			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.HubReferenceTxb,"'Ethernet Hub Reference Text Box'");					
    			sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.HubReferenceTxb,Hub_Reference,"'Ethernet Hub Reference Text Box'");
    						
    			Reusable.WaitforC4Cloader();
    			Reusable.waitForpageloadmask();
    			
    			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.HubAddressTxb,"'Ethernet Hub Address Text Box'");					
    			sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.HubAddressTxb,ReConfigureSE,"'Ethernet Hub Reference Text Box'");
    			findWebElement(CPQQuoteCreationObj.EthernetSpokeConfig.HubAddressTxb).sendKeys(Keys.ENTER);
    			click(CPQQuoteCreationObj.EthernetSpokeConfig.HubSearchImgLnk);
    			Reusable.WaitforC4Cloader();
    			Reusable.waitForpageloadmask();
    			
    			
    				//By executing a java script
    			webDriver.switchTo().frame("siteAddressLink");
    				//Validate Address
    			sResult = AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
    			if (sResult.equalsIgnoreCase("False")) 
    			{
    				ExtentTestManager.getTest().log(LogStatus.FAIL, "Site A address text box is not visible in CPQ, please verify");
    				System.out.println("Site A address text box is not visible in CPQ, please verify");
    				//ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addScreenCapture(""));				
    			}
    			
    			Reusable.WaitforC4Cloader();
    			Reusable.waitForpageloadmask();
    						
//    			Configuring Spoke Part
    						

    			String SiteB_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteB_Address_Type");
    			Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");
    			
    			verifyExists(CPQQuoteCreationObj.EthernetSpokeConfig.SopkeHubAddressTxb,"'Ethernet Hub Address Text Box'");					
    			sendKeys(CPQQuoteCreationObj.EthernetSpokeConfig.SopkeHubAddressTxb,ReConfigureSE,"'Ethernet Hub Reference Text Box'");
    			click(CPQQuoteCreationObj.EthernetSpokeConfig.SpokeSearchImgLnk,"SpokeSearchImgLnk");

    			Reusable.WaitforC4Cloader();
    			Reusable.waitForpageloadmask();
    				//By executing a java script
    			webDriver.switchTo().frame("siteAddressLink");
    				//Validate Address
    			sResult = AddressTypeConfiguration(SiteB_Address,SiteB_Address_Type);
    			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) 
    			{ 
    				
    				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk,"'Site B Address Link'");
    				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteBAddressLnk,"'Site B Address Link'");				
    				
    				Reusable.WaitforC4Cloader();
    				Reusable.waitForpageloadmask();
    				
    				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,"'Site A Phone Number Text Box'");
    				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,Phone_Number,"'Site A Phone Number Text Box'");
    				
    				verifyExists(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn,"'Validate Address Button'");
    				click(CPQQuoteCreationObj.AddresstypeConnectivity.ValidateAddressABtn,"'Validate Address Button'");
    				
    				Reusable.WaitforC4Cloader();
    				Reusable.waitForpageloadmask();
    			}
    			if (sResult.equalsIgnoreCase("False"))
    			{ 
    				ExtentTestManager.getTest().log(LogStatus.FAIL, "Site B address text box is not visible in CPQ, please verify");
    				System.out.println("Site B address text box is not visible in CPQ, please verify");
    				//ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addScreenCapture(""));
    			}
    						
    			
    			break;
    			
    		case "ColtIpAccess":
    		
    		//******************Primary Connection > PRIMARY ADDRESS DETAILS**********************************
    			waitForAjax();
    			SiteA_Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteA_Address");
    			SiteA_Address_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SiteA_Address_Type");
    			Phone_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Phone_Number");

    				
    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.PrimaryAddressHdr, "'PRIMARY ADDRESS' section header");
    				
    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddLbl, "'Site Address' field label");
    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb, "'Site Address' text field");
    			sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.SiteAAddressTxb, ReConfigureSE,"Site Address");
    				
    			//Click on the 'Search for an Address' icon
    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
    			click(CPQQuoteCreationObj.PrimaryAddressPage.SearchBtnImg, "Search Button Image");
    				
    			Reusable.WaitforC4Cloader();
    			waitForAjax();
    			
    			webDriver.switchTo().frame("siteAddressLink");
    			//Validate Address
    			AddressTypeConfiguration(SiteA_Address,SiteA_Address_Type);
    			
    			if (Flow_Type.equalsIgnoreCase("ManualDSL") || Flow_Type.equalsIgnoreCase("ManualOLODSL")) 
    			{ 
    				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.SiteDetailsEntryBtn,"'Site A Details Entry Button'");
    				click(CPQQuoteCreationObj.PrimaryAddressPage.SiteDetailsEntryBtn,"'Site A Details Entry Button'");
    				verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,"'Site A Phone Number Text Box'");
    				sendKeys(CPQQuoteCreationObj.PrimaryAddressPage.siteAPhoneNumberTxb,Phone_Number,"'Site A Phone Number Text Box'");
    				Reusable.waitForpageloadmask();
    				verifyExists(CPQQuoteCreationObj.AddProducts.update4cBtn,"Update Button");
    				click(CPQQuoteCreationObj.AddProducts.update4cBtn,"Update Button");
    				Reusable.waitForpageloadmask();
    				waitForElementToBeVisible(CPQQuoteCreationObj.AddProducts.ojUpdateEnabled,10);
    			}
    						
    			waitForElementToAppear(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, 20);
    			verifyExists(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
    			click(CPQQuoteCreationObj.PrimaryAddressPage.NextBtn, "Next Button");
    			break;
    		} 		
   		
    	}
    		
//	        Calling the below method if the Customer Defined Route are applicable
	        if (CustomerDefRoute_Applicable.equalsIgnoreCase("Reconfigure")) {
	        selectProductFeaturesCustomerDefRoute(testDataFile, sheetName, scriptNo, dataSetNo);
	            
	        } 
        
//	        Calling the below method if the Non_Standard_Interface_flow are applicable
	        if (Non_Standard_Interface_flow.equalsIgnoreCase("Reconfigure")) {
	        selectSiteDetailsNon_Standard_Interface_flow(testDataFile, sheetName, scriptNo, dataSetNo);
	            
	        }      	
	        
//	        Calling the below method if the Customer Defined Route are applicable
	      /*  if (Flow_Type.equalsIgnoreCase("ULLFIBRE")) {
	        	onnetConfiguration(testDataFile, sheetName, scriptNo, dataSetNo);
	            
	        }   */     
        
              
                 
	        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn, "Validate Button");
        	click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
        	Reusable.WaitforCPQloader();

        	// calling the below method to update and save the product in cpq
        	updateSaveProductCPQ("Save");
		
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();
		waitForAjax();
		
		verifyQuoteStage("SE Required");
		
	    ScrollIntoViewByString(CPQQuoteCreationObj.SEEngagement.SEEngagementTab);
	    waitForElementToAppear(CPQQuoteCreationObj.SEEngagement.SEEngagementTab, 10);	
	    verifyExists(CPQQuoteCreationObj.SEEngagement.SEEngagementTab,"'SE EngagementTab'");
	    click(CPQQuoteCreationObj.SEEngagement.SEEngagementTab);
			
		Reusable.WaitforC4Cloader();
		Reusable.waitForpageloadmask();
			
		verifyExists(CPQQuoteCreationObj.SEEngagement.NotifySEReviewDropdown,"'Notify SE Review Dropdown'");
		click(CPQQuoteCreationObj.SEEngagement.NotifySEReviewDropdown);
		ClickonElementByString("//li[normalize-space(.)='"+NotifySEReview+"']", 20);
			
		Reusable.WaitforC4Cloader();
		Reusable.waitForpageloadmask();
			
		verifyExists(CPQQuoteCreationObj.SEEngagement.ReasonDropdown,"'Notify SE Review Dropdown'");
		click(CPQQuoteCreationObj.SEEngagement.ReasonDropdown);
		ClickonElementByString("//li[normalize-space(.)='"+Reason_value+"']", 20);

		Reusable.WaitforC4Cloader();
		Reusable.waitForpageloadmask();
		
		verifyExists(CPQQuoteCreationObj.SEEngagement.ConfirmSelectionBtn,"'Confirm Selection Btn'");
		click(CPQQuoteCreationObj.SEEngagement.ConfirmSelectionBtn);
			                    
		Reusable.WaitforC4Cloader();
		Reusable.waitForpageloadmask();
		
        return "True";    
	}
	
	public String selectSiteDetailsNon_Standard_Interface_flow(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
    {       
//        Initializing the Variable
  
       
        ScrollIntoViewByString(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk);
        waitForElementToAppear(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk, 10);
    	verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
    	click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
		Reusable.waitForpageloadmask();	    	
	
		
				
			isEnable(CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceLst);
			verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceLst,"'Interface List'");
			selectByValueDIV(CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceLst, CPQQuoteCreationObj.PrimaryConnectionPage.InterfaceSubLst,"WAN PHY");
			Reusable.waitForpageloadmask();
      
                   
        		return "True";	
    	}			
	
	public String ULLFibreConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		
//		Initializing the Variable
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String Flow_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flow_Type");			
					
					
				waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.ULLAlnk, 20);
				verifyExists(CPQQuoteCreationObj.ProductConfigration.ULLAlnk,"'ULL Check Button'");
				scrollDown(CPQQuoteCreationObj.ProductConfigration.ULLAlnk);
				click(CPQQuoteCreationObj.ProductConfigration.ULLAlnk,"'ULL Check Button'");			
				Reusable.waitForpageloadmask();
				
				scrollDown(CPQQuoteCreationObj.ProductConfigration.ULLAendRadioBtn);
				click(CPQQuoteCreationObj.ProductConfigration.ULLAendRadioBtn,"'Onnet Check Button'");
				
									
//				For EthernetLine
				if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave"))
				{
					waitForElementToAppear(CPQQuoteCreationObj.ProductConfigration.ULLBlnk, 20);
					verifyExists(CPQQuoteCreationObj.ProductConfigration.ULLBlnk,"'ULL Check Button'");
					scrollDown(CPQQuoteCreationObj.ProductConfigration.ULLBlnk);
					click(CPQQuoteCreationObj.ProductConfigration.ULLBlnk,"'ULL Check Button'");			
					Reusable.waitForpageloadmask();
					
					scrollDown(CPQQuoteCreationObj.ProductConfigration.ULLBendRadioBtn);
					click(CPQQuoteCreationObj.ProductConfigration.ULLBendRadioBtn,"'ULL Check Button'");
					Reusable.waitForpageloadmask();
					
										
					}
			
		
		return "True";
		
	}
	
	public void optionQuoteEntry(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : optionQuoteEntry
		Purpose     : This method is to progress the option quote entry
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variables
		String No_Of_Options = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"No_Of_Copies");
		String Primary_Option = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Primary_Option");
		
		
		if(Primary_Option.contains("Option")) {
		if (waitForElementToBeVisible(CPQQuoteCreationObj.OptionQuote.optionQuoteRBtn, 60)) {
			Reusable.waitForpageloadmask();
			ScrollIntoViewByString(CPQQuoteCreationObj.OptionQuote.optionQuoteRBtn);
			click(CPQQuoteCreationObj.OptionQuote.optionQuoteRBtn, "Option Quote Button");
			Reusable.waitForpageloadmask();
			sendKeys(CPQQuoteCreationObj.OptionQuote.noOfOptionsTxb, Integer.toString(Integer.parseInt(No_Of_Options)+1), "Option Quote");
			Reusable.waitForpageloadmask();
//			Calling the below method to save the details
			saveCPQ("Main");				
			for (int i=1; i<=3; i++) {Reusable.waitForpageloadmask(); }
			selectByValueDIV(CPQQuoteCreationObj.OptionQuote.selectPrimaryOptionsLst, CPQQuoteCreationObj.OptionQuote.selectPrimaryOptionsSubLst, Primary_Option);
			Reusable.waitForpageloadmask();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Option Quote has been added successfully for further process");
			System.out.println("Option Quote has been added successfully for further process");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "optionQuoteRBtn is not Visible in CPQ Page, Please Verify");
			System.out.println("optionQuoteRBtn is not Visible in CPQ Page, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
			}
		}else {
			if (waitForElementToBeVisible(CPQQuoteCreationObj.OptionQuote.projectQuoteRBtn, 60)) {
				Reusable.waitForpageloadmask();
				click(CPQQuoteCreationObj.OptionQuote.projectQuoteRBtn, "Project Quote Button");
				Reusable.waitForpageloadmask();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Project Quote has been added successfully for further process");
				System.out.println("Project Quote has been added successfully for further process");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "ProjectQuoteRBtn is not Visible in CPQ Page, Please Verify");
				System.out.println("ProjectQuoteRBtn is not Visible in CPQ Page, Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				
				}
		}
		
//		Calling the below method to save the details
		saveCPQ("Main");
		
	}
	
	public void optionQuoteLineItemEntries(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
		/*----------------------------------------------------------------------
		Method Name : optionQuoteLineItemEntries
		Purpose : This method is to make multiple entries in line item grid table for option quote
		Designer : Vasantharaja C
		Created on : 27th July 2020
		Input : None
		Output : True/False
		----------------------------------------------------------------------*/
		// Initializing the Variables
		String ProductName = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		String Discount_Range = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Discount_Range");
		String Option_Range = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Option_Range");
		String No_Of_Copies = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"No_Of_Copies");
		String LineItem_level = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"LineItem_level");
		String Primary_Option = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Primary_Option");
		String Row_Value = null, Row_Range = null; int j = 0; int Temp_Row = 1;
		// spliting the products
		String sProduct = ProductName.replaceAll("(?!^)([A-Z])", " $1");
		waitForElementToBeVisible(CPQQuoteCreationObj.OptionQuote.discountCbx, 30);
		ScrollIntoViewByString(CPQQuoteCreationObj.OptionQuote.discountCbx);
		click(CPQQuoteCreationObj.OptionQuote.discountCbx, "Discount Checkbox");
		Reusable.waitForpageloadmask();
		String[] Discount_Split = Discount_Range.split("\\|");
		String[] Option_Split = Option_Range.split("\\|");
		String[] Line_Split = LineItem_level.split("\\|");
		String Rows[] = new String[Integer.parseInt(No_Of_Copies)+1];
		// Picking the Row Number of Each products
		for (j = 1; j <= Integer.parseInt(No_Of_Copies)+1; j++) {
		Row_Value = Reusable.MultiLineWebTableCellAction("Product", sProduct, "Product","GetRow", null, Temp_Row);
		Rows[j-1] = Row_Value;
		Temp_Row = Integer.parseInt(Row_Value)+1;
		System.out.print("Row_Value"+Row_Value);
		}
		if(!Primary_Option.contains("ProjectQuote")) {
		// Processing Discount Entries in lineitem grid table
		for (int i=0; i < Line_Split.length; i++) {
		Reusable.MultiLineWebTableCellAction("Product", sProduct, "Options","Select", Option_Split[i], Integer.parseInt(Rows[i]));
		Reusable.waitForpageloadmask();
		}
		}
		// Calling the below method to save the details
		saveCPQ("Main");
		
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		if(ProductName.equalsIgnoreCase("Colt Ip Domain")) {ProductName="IP Domain";}
		if(ProductName.equalsIgnoreCase("Colt Ip Guardian")) {ProductName="IP Guardian";}
		if(ProductName.equalsIgnoreCase("Colt Managed Virtual Firewall")) {ProductName="IP Managed Virtual Firewall";}
		if(ProductName.equalsIgnoreCase("Colt Managed Dedicated Firewall")) {ProductName="IP Managed Dedicated Firewall";}
		else { ProductName = ProductName.replaceAll("(?!^)([A-Z])", " $1"); }
		
//		Clicking on the rows from the table
		if(ProductName.equalsIgnoreCase("EthernetLine")||ProductName.equalsIgnoreCase("EthernetHub")) {
			Reusable.WebTableCellAction("Product", "Container Model", null,"Click", null);
		}else {
			Reusable.WebTableCellAction("Product", ProductName, null,"Click", null);
		}
		
		clickUpdateConfigBtn();
		
		waitForAjax();
		Reusable.waitForpageloadmask();
		
		waitForElementToBeVisible(CPQQuoteCreationObj.OptionQuote.discountCbx, 30);
		ScrollIntoViewByString(CPQQuoteCreationObj.OptionQuote.discountCbx);
		click(CPQQuoteCreationObj.OptionQuote.discountCbx, "Discount Checkbox");
		Reusable.waitForpageloadmask();
		}
		
	
	public void optionQuoteTechnicalApproval(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : optionQuoteTechnicalApproval
		Purpose     : This method is to select the option quote for technical approval
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
//		Initializing the Variable
		String Primary_Option = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Primary_Option");
		
    	    waitForAjax();
    	    Reusable.waitForpageloadmask();
    	    waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature, 10);
    	    ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature);
    	    verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature,"Customer Signature on Quote section");        
    	    click(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature,"Customer Signature");

    	    Reusable.waitToPageLoad();
			waitForElementToBeVisible(CPQQuoteCreationObj.SubmissionforTechnicalApproval.selectOptionsTechValLst, 30);
			selectByValueDIV(CPQQuoteCreationObj.SubmissionforTechnicalApproval.selectOptionsTechValLst, CPQQuoteCreationObj.SubmissionforTechnicalApproval.selectOptionsTechValSubLst, Primary_Option);
			Reusable.WaitforCPQloader();
			click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.proceedToQuoteBtn, "Proceed to Quote");
			Reusable.WaitforCPQloader();
			waitForAjax();
			Reusable.WaitforCPQloader();
			waitForElementToBeVisible(CPQQuoteCreationObj.TransactionPage.QuoteId, 120);
			Reusable.WaitforCPQloader();
			String quoteID = getAttributeFrom(CPQQuoteCreationObj.TransactionPage.QuoteId, "value");
			DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Approved_QuoteID", quoteID);
			System.out.println("Approved Quote ID "+quoteID+" got selected for further progress");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Approved Quote ID "+quoteID+" got selected for further progress");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Option Quote is successfully set for technical approval submission");
			System.out.println("Option Quote is successfully set for technical approval submission");
		
				
	}
	
	public void projectQuoteProcess(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException, AWTException {
		/*----------------------------------------------------------------------
		Method Name : projectQuoteProcess
		Purpose     : This method is to submit the Project quote for Technical approval
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		String Status_Reason = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Status_Reason");
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");

		Reusable.waitForpageloadmask();		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, 30);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink);
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		Reusable.WaitforCPQloader();
		Reusable.waitForpageloadmask();
		
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx, 30);
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx);
		verifyExists(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx, "Fast Sign Initiate Checkbox");
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignInitiateCbx, "Fast Sign Initiate Checkbox");
		Reusable.waitForpageloadmask();
		
		Submit.SendProposalToCustomer(file_name, Sheet_Name, iScript, iSubScript);
		
		String Quote_Id = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Quote_ID");
		String pdfName = Quote_Id+".pdf";	
		String Proposal_Path = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Proposal_Path");
		
		Reusable.waitForpageloadmask();
		waitForElementToAppear(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fileAttachmentLnk, 30);
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fileAttachmentLnk, "Proposal File Link");
		
//		Checking if the file is existing or not and printing the file location into the testdata sheet
		Reusable.isFileDownloaded(Proposal_Path, pdfName);
		
		Reusable.waitForpageloadmask();
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink);
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.TechnicalApprovalLink, "Technical Approval Link");
		Reusable.waitForpageloadmask();
		
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.closeFastSignBtn);
		selectByValueDIV(CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignReasonLst, CPQQuoteCreationObj.SubmissionforTechnicalApproval.fastSignReasonSubLst, Status_Reason);
		Reusable.waitForpageloadmask();	
		
		String Complete_Path = Proposal_Path+pdfName;
		sendKeys(CPQQuoteCreationObj.SubmissionforTechnicalApproval.customerProofUploadTxb, Complete_Path, "Customer Proof Upload Textbox");
		Reusable.waitForpageloadmask();		
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.closeFastSignBtn, "Close Fast Sign Btn");
		Reusable.waitForpageloadmask();	
			
		waitForInvisibilityOfElement(CPQQuoteCreationObj.SubmissionforTechnicalApproval.closeFastSignBtn, 180);
		
		//validateCPQErrorMsg();				
//			Select the First Product
		waitForElementToBeVisible(CPQQuoteCreationObj.SubmissionforTechnicalApproval.projectQuoteBtn, 60);
		if (!Product_Name.equalsIgnoreCase("CpeSolutionsSite")) {
		String sProduct = Product_Name.replaceAll("(?!^)([A-Z])", " $1");
		Reusable.WebTableCellAction("Product", sProduct, null,"Click", null);
		}
			
		Reusable.waitForpageloadmask();
		ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforTechnicalApproval.projectQuoteBtn);
		click(CPQQuoteCreationObj.SubmissionforTechnicalApproval.projectQuoteBtn);
		Reusable.WaitforCPQloader();
			
		validateCPQErrorMsg();
							
		Reusable.WaitforCPQloader();
		waitForElementToBeVisible(CPQQuoteCreationObj.TransactionPage.QuoteId, 120);
		Reusable.WaitforCPQloader();
		String quoteID = getAttributeFrom(CPQQuoteCreationObj.TransactionPage.QuoteId, "value");
		DataMiner.fnsetcolvalue(file_name, Sheet_Name, iScript, iSubScript, "Approved_QuoteID", quoteID);
			
		System.out.println("Approved Quote ID "+quoteID+" got selected for further progress");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Approved Quote ID "+quoteID+" got selected for further progress");

		Reusable.deleteFile(Proposal_Path, Complete_Path);
			
		ExtentTestManager.getTest().log(LogStatus.PASS, "Project quote is set for Fast Sign process with the text "+getTextFrom(CPQQuoteCreationObj.SubmissionforTechnicalApproval.notificationBarCPQ));
		System.out.println("Project quote set for Fast Sign process with message "+getTextFrom(CPQQuoteCreationObj.SubmissionforTechnicalApproval.notificationBarCPQ));
			
	}
	
	public void addBulkUpload(String Product_Name) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : addBulkUpload
		Purpose     : This method is to add a Bulk Upload Product
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
//		Initializing the variables
		String EtherNetLine_Path = System.getProperty("user.dir")+"\\TestData\\BulkUpload\\EthernetLine\\Bulk_Upload_Template_V2.9.xlsm";
		String EtherNetHub_Path = System.getProperty("user.dir")+"\\TestData\\BulkUpload\\EthernetHub\\Bulk_Upload_Template_V2.9.xlsm";
		String EtherNetSpoke_Path = System.getProperty("user.dir")+"\\TestData\\BulkUpload\\EthernetSpoke\\Bulk_Upload_Template_V2.9.xlsm";
		String Wave_Path = System.getProperty("user.dir")+"\\TestData\\BulkUpload\\Wave\\Bulk_Upload_Template_V2.9.xlsm";

		String ProductLenght;int ProductsSize;
		
		if(waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkUploadLnk,75)) {
			click(CPQQuoteCreationObj.BulkUpload.bulkUploadLnk, "Bulk Upload Link");
			Reusable.WaitforCPQloader();
			Reusable.waitForpageloadmask();
			switchToFrame("bulkUploadApp");
			waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkAddNewLineBtn,30);
			ScrollIntoViewByString(CPQQuoteCreationObj.BulkUpload.bulkAddNewLineBtn);
			clickByJS(CPQQuoteCreationObj.BulkUpload.bulkAddNewLineBtn);
			waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkUploadNewFlieBtn,20);
			click(CPQQuoteCreationObj.BulkUpload.bulkUploadNewFlieBtn, "Bulk Upload New File Button");
        	ExtentTestManager.getTest().log(LogStatus.PASS, "Tap on Bulk Upload new File");

		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Bulk Upload is not avalibale, Please Verify");
			System.out.println("Bulk Upload is not avalibale, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		switch(Product_Name) {
		case "EthernetLine":
			sendKeys(CPQQuoteCreationObj.BulkUpload.bulkfileInputBtn, EtherNetLine_Path, "Bulk File Input Button");
        	break;
		case "EthernetHub":
			sendKeys(CPQQuoteCreationObj.BulkUpload.bulkfileInputBtn, EtherNetHub_Path, "Bulk File Input Button");
        	break;
		case "EthernetSpoke":
			sendKeys(CPQQuoteCreationObj.BulkUpload.bulkfileInputBtn,EtherNetSpoke_Path, "Bulk File Input Button");
        	break;
		case "Wave":
			sendKeys(CPQQuoteCreationObj.BulkUpload.bulkfileInputBtn, Wave_Path, "Bulk File Input Button");
        	break;
		}		
		
		if(waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkUploadBtn, 20)) {
			clickByJS(CPQQuoteCreationObj.BulkUpload.bulkUploadBtn);
			for (int i = 1; i < 4; i++) { Reusable.waitForpageloadmask();}
			waitForInvisibilityOfElement(CPQQuoteCreationObj.BulkUpload.bulkPleaseWaitText, 90);
	        ExtentTestManager.getTest().log(LogStatus.PASS, "EtherNetLine Bulk Upload sheet is added");
	        waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkUploadedTxt, 90);
	        clickByJS(CPQQuoteCreationObj.BulkUpload.bulkDetailsTxt);
			Reusable.waitForpageloadmask();
			waitForInvisibilityOfElement(CPQQuoteCreationObj.BulkUpload.pendingConnCheckBtn, 180);
			waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkRefreshBtn,180);
			clickByJS(CPQQuoteCreationObj.BulkUpload.bulkRefreshBtn);
			
			for (int i = 1; i < 2; i++) { Reusable.waitForpageloadmask(); }
			waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkCheckConnectivityBtn,180);
			clickByJS(CPQQuoteCreationObj.BulkUpload.bulkCheckConnectivityBtn);
			for (int i = 1; i < 5; i++) { Reusable.waitForpageloadmask(); }
			if(verifyExists(CPQQuoteCreationObj.BulkUpload.connectivityStatusTxt))
			{
				waitForInvisibilityOfElement(CPQQuoteCreationObj.BulkUpload.connectivityStatusTxt,120);
			}
			if(verifyExists(CPQQuoteCreationObj.BulkUpload.bulkConnectivityTxt))
			{
				waitForInvisibilityOfElement(CPQQuoteCreationObj.BulkUpload.bulkConnectivityTxt,240);
			}
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "Tap on Bulk Connectivity Check");
			if(waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkOnnetACheckRdb, 180)) {
				Reusable.waitForpageloadmask();
				waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkRefreshBtn,180);
				clickByJS(CPQQuoteCreationObj.BulkUpload.bulkRefreshBtn);
				Reusable.waitForpageloadmask();
				clickByJS(CPQQuoteCreationObj.BulkUpload.bulkSelectAllCbx);
				Reusable.waitForpageloadmask();
				clickByJS(CPQQuoteCreationObj.BulkUpload.bulkAddToQuoteBtn);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Tap on Bulk Add to Quote");
				waitForInvisibilityOfElement(CPQQuoteCreationObj.BulkUpload.bulkPendingBtn,240);
				Reusable.waitForpageloadmask();
				waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.bulkRefreshBtn,180);
				clickByJS(CPQQuoteCreationObj.BulkUpload.bulkRefreshBtn);
				Reusable.waitForpageloadmask();
				clickByJS(CPQQuoteCreationObj.BulkUpload.bulkContinueToQuoteBtn);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Tap on Bulk Continue to Quote");
				for (int i = 1; i < 4; i++) { Reusable.waitForpageloadmask(); }
				switchToDefaultFrame();	
				Reusable.waitForpageloadmask();
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Onnet Check Failed, Please Verify");
				System.out.println("Onnet Check Failed, please verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "EtherNetLine Upload Failed, Please Verify");
			System.out.println("EtherNetLine Upload Failed, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}			
	}
	
public void bulkUploadProductsReconfigure(String file_name, String Sheet_Name, String iScript, String iSubScript, String productName) throws IOException, InterruptedException {
		
		if(waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.cpqTableOnGenInfo, 90)) {
			
			ScrollIntoViewByString(CPQQuoteCreationObj.BulkUpload.cpqTableOnGenInfo);
			
			int numOfRecords = getXPathCount(CPQQuoteCreationObj.BulkUpload.cpqTableNumOfRows);
			for(int i=2;i<=numOfRecords;i++)
			{
				String productColObj = "@xpath=" + CPQQuoteCreationObj.BulkUpload.cpqTableSelectRecord1 + i + CPQQuoteCreationObj.BulkUpload.productColRecord2;
				ScrollIntoViewByString(productColObj);
				String currentProduct = getAttributeFrom(productColObj, "title").replaceAll("\\s", "");
				
				if(currentProduct.equalsIgnoreCase(productName))
				{
				
					String stageColObj = "@xpath=" + CPQQuoteCreationObj.BulkUpload.cpqTableSelectRecord1 + i + CPQQuoteCreationObj.BulkUpload.stageColRecord2;
					ScrollIntoViewByString(stageColObj);
					String currentStage = getAttributeFrom(stageColObj, "title");
				
					if(!currentStage.trim().equalsIgnoreCase("SE Required"))
					{
						String checkboxObj =  "@xpath=" + CPQQuoteCreationObj.BulkUpload.cpqTableSelectRecord1 + i + CPQQuoteCreationObj.BulkUpload.cpqTableSelectRecord2;
						ScrollIntoViewByString(checkboxObj);
						clickByJS(checkboxObj);				
					
						//Reconfiguring Product
						productReconfigure(file_name, Sheet_Name, iScript, iSubScript, productName);
					}
				}
				
				if(i==numOfRecords)
				{
					ScrollIntoViewByString(CPQQuoteCreationObj.BulkUpload.nextPageArrow);
					String nextPageClass = getAttributeFrom(CPQQuoteCreationObj.BulkUpload.nextPageArrow, "class");
					if(nextPageClass.contains("oj-enabled"))
					{
						
						click(CPQQuoteCreationObj.BulkUpload.nextPageArrow, "Next Page Arrow");
						Reusable.waitForpageloadmask();
						Reusable.WaitforCPQloader();
						
						int thisPageRecords = getXPathCount(CPQQuoteCreationObj.BulkUpload.cpqTableNumOfRows);
						numOfRecords = numOfRecords + thisPageRecords;
						
					}else
					{
						break;
					}
				}
				
			}
		
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "CPQ table on the general information is not available");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}			
	}
	
	public void productReconfigure(String file_name, String Sheet_Name, String iScript, String iSubScript, String productName) throws IOException, InterruptedException {
	
		String Override_Reason = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Override_Reason");
		String Site_Interface = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Presentation_Interface");
		String Site_Connector_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Site_Connector_Type");
		
//		clicking on Reconfiguration link
		verifyExists(engagementpf.reConfigureBtn,"ReConfigure Button");
		Reusable.waitForpageloadmask();
    	click(engagementpf.reConfigureBtn,"ReConfigure Button");
    	Reusable.waitForpageloadmask();
    	Reusable.WaitforCPQloader();
    	waitForAjax();
    	
    	if (productName.contains("Ethernet")||productName.contains("Wave")) {
    		
    		String siteDetailsClass = getAttributeFrom(CPQQuoteCreationObj.PartialSave.SiteDetailsTab, "class");
    		if(siteDetailsClass.contains("has-error"))
    		{
    			waitForElementToAppear(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk, 120);
	    		verifyExists(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
	    		for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }				
				click(CPQQuoteCreationObj.PartialSave.SiteDetailsLnk,"Site Details Link");
				Reusable.waitForpageloadmask(); 
				
				String onnnetASwitch = getAttributeFrom(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetAEndSwitch, "class");
				
				if(!onnnetASwitch.contains("oj-disabled"))
				{
					String onnnetAChbxChecked = getAttributeFrom(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetAEndCbx, "aria-checked");
					if(onnnetAChbxChecked.trim().equalsIgnoreCase("false"))
					{
						verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetAEndCbx, "Override to Onnet");
						ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetAEndCbx);
						click(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetAEndCbx, "Override to Onnet");						
						Reusable.waitForpageloadmask(); 
					
						waitForElementToAppear(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb, 60);
						verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb, "'Override Reason' text field");
						ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb);
						sendKeys(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonAEndTxb, Override_Reason, "Override_reason");
					
						verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.AEndCorrectCbx, "Override to ULL");
						click(CPQQuoteCreationObj.PrimaryConnectionPage.AEndCorrectCbx, "A End Correct");	
						Reusable.waitForpageloadmask();
					}
				}
				
				String onnnetBSwitch = getAttributeFrom(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetBEndSwitch, "class");
				
				if(!onnnetBSwitch.contains("oj-disabled"))
				{
				
					String onnnetBChbxChecked = getAttributeFrom(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetBEndCbx, "aria-checked");
					if(onnnetBChbxChecked.trim().equalsIgnoreCase("false"))
					{
						verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetBEndCbx, "Override to Onnet");
						ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetBEndCbx);
						click(CPQQuoteCreationObj.PrimaryConnectionPage.overrideToOnnetBEndCbx, "Override to Onnet");						
						Reusable.waitForpageloadmask();
					
						waitForElementToAppear(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb, 60);
						verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb, "'Override Reason' text field");
						ScrollIntoViewByString(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb);
						sendKeys(CPQQuoteCreationObj.PrimaryConnectionPage.overrideReasonBEndTxb, Override_Reason, "Override_reason");
					
						verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.BEndCorrectCbx, "Override to ULL");
						click(CPQQuoteCreationObj.PrimaryConnectionPage.BEndCorrectCbx, "B End Correct");	
						Reusable.waitForpageloadmask();
					}
				}
				
				verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
		        click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
		        Reusable.WaitforCPQloader();
		        Reusable.waitForpageloadmask();						
    		}	    		
    		
    		String featuresClass = getAttributeFrom(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesTab, "class");
    		if(featuresClass.contains("has-error"))
    		{
    			waitForElementToAppear(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk, 60);
    			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
                ScrollIntoViewByString(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
                clickByJS(CPQQuoteCreationObj.Onnet_DualEntry_Features.FeaturesLnk);
                Reusable.waitForpageloadmask();   
                                           
                String aEndClass = getAttributeFrom(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationDropdown, "class");
        		if(aEndClass.contains("oj-invalid"))
        		{
        			//Entering A-End details
        			waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface, 15);
        			ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface);
        			clickByJS(CPQQuoteCreationObj.AdditionalProductDataPage.AendSitePresentationInterface);
        			ClickonElementByString("//div[@data-value='"+Site_Interface+"']", 20);
        			//ClickonElementByString("//li[normalize-space(.)='"+ Site_Interface +"']", 30);
        			Reusable.waitForpageloadmask();
        		}    		
        	        			
        		String bEndClass = getAttributeFrom(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationDropdown, "class");
         		if(bEndClass.contains("oj-invalid"))
         		{
         			waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterface, 15);
         			ScrollIntoViewByString(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterface);
         			clickByJS(CPQQuoteCreationObj.AdditionalProductDataPage.BendSitePresentationInterface);
         			ClickonElementByString("//div[@data-value='"+Site_Interface+"']", 20);
         			//ClickonElementByString("//li[normalize-space(.)='"+ Site_Interface +"']", 30);
         			Reusable.waitForpageloadmask();
         		}
        		
        		if(productName.equalsIgnoreCase("EthernetLine"))
        		{
        			String diversityFlagChecked = getAttributeFrom(CPQQuoteCreationObj.PrimaryConnectionPage.diversityFlagCbx, "aria-checked");
        			if(diversityFlagChecked.trim().equalsIgnoreCase("true"))
        			{
        				verifyExists(CPQQuoteCreationObj.PrimaryConnectionPage.diversityFlagCbx, "Diversity Flag");
        				MoveToElement(CPQQuoteCreationObj.PrimaryConnectionPage.diversityFlagCbx);
        				click(CPQQuoteCreationObj.PrimaryConnectionPage.diversityFlagCbx, "Diversity Flag");						
        				Reusable.waitForpageloadmask();
        			}
        		}
        		
    		}    		 
    		
    		verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
	        click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
	        Reusable.WaitforCPQloader();
	        Reusable.waitForpageloadmask();
	        
	        verifyExists(CPQQuoteCreationObj.ViewPrices.SaveButton,"Verify Save Button");
			click(CPQQuoteCreationObj.ViewPrices.SaveButton,"Click on Save Button");
			Reusable.waitForpageloadmask();
			Reusable.WaitforCPQloader();
			waitForAjax();
	    		
	    }
    	else if(productName.equals("Colt Ip Access")) {
    			for (int i = 1; i < 2; i++) {Reusable.waitForpageloadmask(); }
    			waitForElementToAppear(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, 120);
    			verifyExists(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, "IP L3 Resilience Link");
    			click(CPQQuoteCreationObj.L3RESILIENCEPage.IPL3ResilienceLnk, "IP L3 Resilience Link");
    			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask(); }
    				
    			//waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn, 50);
    			//verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn,"IPAccess Next Button");
    			//click(CPQQuoteCreationObj.AdditionalProductDataPage.cpeIPAcessNextBtn,"IPAccess Next Button");
    			//for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask();}	    		
    		}
    		else if(productName.equals("Colt Ip Domain")||productName.equals("Colt Ip Guardian")
    				||productName.equals("Colt Managed Virtual Firewall")||productName.equals("IP Managed Dedicated Firewall")) {
    			for (int i = 1; i < 3; i++) {Reusable.waitForpageloadmask();}
    		}

		
	}
	
	public String verifyDealClassStage(String Deal_Stage) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : verifyDealClassStage
		Purpose     : This method is to Verify the Deal Stage status
		Input       : None
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		int i, Iter;
		WEB_DRIVER_THREAD_LOCAL.get();
		
		if(waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.quoteStageElem, 60)) {
		ScrollIntoViewByString(CPQQuoteCreationObj.BulkUpload.opportunityLnk);
		click(CPQQuoteCreationObj.BulkUpload.opportunityLnk, "Opportunity Link");
		Reusable.waitForpageloadmask();
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to click opportunity, Please Verify");
			System.out.println("Unable to click opportunity, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
		
		if (waitForElementToBeVisible(CPQQuoteCreationObj.BulkUpload.dealTypeElem, 60)) {
			Reusable.waitForpageloadmask();
			String dealStage = getAttributeFrom(CPQQuoteCreationObj.BulkUpload.dealTypeElem, "value");
			for (i = 1; i <= 2; i++) {
				if (dealStage.equals(Deal_Stage)) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Quote stage is set to "+dealStage);
					System.out.println("Quote stage is set to "+dealStage);
					break;
				} else {
					Thread.sleep(10000);
					Reusable.waitForpageloadmask();
					click(CPQQuoteCreationObj.BulkUpload.quoteStageElem, "Quote Stage Element");
					dealStage = getAttributeFrom(CPQQuoteCreationObj.BulkUpload.quoteStageElem,"value");
					scrollIntoTop();
					continue;
				}
			}
			if (i > 2) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote stage is still set as "+dealStage+ " but the Expected status is "+Deal_Stage);
				System.out.println("Quote stage is still set as "+dealStage+ " but the Expected status is "+Deal_Stage);
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quote Type is not Standard, Please Verify");
			System.out.println("Unable to click on Quotes Link, please verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
		return "True";
	}
	
	public String VerifyPricesBookValue(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
		String Segment=DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Segment");
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		switch(Segment)
		{
		case "CLTP - Google":{
			scrollDown(CPQQuoteCreationObj.ViewPrices.PriceBookValueGoogle);
			waitForElementToAppear(CPQQuoteCreationObj.ViewPrices.PriceBookValueGoogle, 60);
			verifyExists(CPQQuoteCreationObj.ViewPrices.PriceBookValueGoogle,"Verify Google Segemnt in Price Book Column");
			ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.PriceBookValueGoogle);
			if(Product_Name.equals("EthernetLine"))
			{
				verifyExists(CPQQuoteCreationObj.ViewPrices.Bandwidth,"Verify Bandwidth in Price Book Column");
				click(CPQQuoteCreationObj.ViewPrices.Bandwidth,"Click on Bandwidth");
				
				verifyExists(CPQQuoteCreationObj.ViewPrices.BandwidthDropdownValue,"Verify Bandwidth Value");
				click(CPQQuoteCreationObj.ViewPrices.BandwidthDropdownValue,"Select Bandwidth from drop down");
				
				verifyExists(CPQQuoteCreationObj.ViewPrices.SaveButton,"Verify Save Button");
				click(CPQQuoteCreationObj.ViewPrices.SaveButton,"Click on Save Button");
				Reusable.waitForpageloadmask();
				
				scrollUp();
				verifyExists(CPQQuoteCreationObj.ViewPrices.ValidateErrorMsg,"Verify Error msg for changing Bandwidth");
				System.out.println("Error Message is displayed after changing Bandwidth");
				
				scrollDown(CPQQuoteCreationObj.ViewPrices.Bandwidth1);
				verifyExists(CPQQuoteCreationObj.ViewPrices.Bandwidth1,"Verify Bandwidth in Price Book Column");
				click(CPQQuoteCreationObj.ViewPrices.Bandwidth1,"Click on Bandwidth");
				
				verifyExists(CPQQuoteCreationObj.ViewPrices.BandwidthDropdownValuePre,"Verify Bandwidth Value");
				click(CPQQuoteCreationObj.ViewPrices.BandwidthDropdownValuePre,"Click on Bandwidth from drop down");
				Reusable.waitForpageloadmask();
				
				verifyExists(CPQQuoteCreationObj.ViewPrices.SaveButton,"Verify Save Button");
				click(CPQQuoteCreationObj.ViewPrices.SaveButton,"Click on Save Button");
				Reusable.waitForpageloadmask();
				
			}
				
		}
		break;
		case "CTLP-Fidessa":{
			scrollDown(CPQQuoteCreationObj.ViewPrices.PriceBookValueFidessa);
			waitForElementToAppear(CPQQuoteCreationObj.ViewPrices.PriceBookValueFidessa, 60);
			verifyExists(CPQQuoteCreationObj.ViewPrices.PriceBookValueFidessa,"Verify Fidessa Segemnt in Price Book Column");
			ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.PriceBookValueFidessa);
		
					scrollUp();
		}
		break;
	}
		return "True";
		
	}
	
	public String ShortTermContractMonthly(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
    {
    

 

        int i, Iter;
        
        String NRCValue=DataMiner.fngetcolvalue(file_name,Sheet_Name, iScript, iSubScript,"NRC_Value");
        String MRCValue=DataMiner.fngetcolvalue(file_name,Sheet_Name, iScript, iSubScript,"MRC_Value");
        WEB_DRIVER_THREAD_LOCAL.get();
        
        
        if (NRCValue.equals("5000")) { Iter = 50; } else { Iter = 2; };
        
         verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
         click(CPQQuoteCreationObj.AdditionalProductDataPage.validate4cBtn);
         Reusable.WaitforCPQloader();
         Reusable.waitForpageloadmask();
        
        verifyExists(CPQQuoteCreationObj.ViewPrices.ViewPriceTab,"Verify View Price Tab");
        click(CPQQuoteCreationObj.ViewPrices.ViewPriceTab, "Click on View Price Tab");
        Reusable.waitForpageloadmask();
        
        //Comparing NRC Values
        ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.ViewPriceNRCValues);
        
            if (waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.ViewPriceNRCValues, 100)) {
                Reusable.WaitforCPQloader();
                String NRCValueText= getTextFrom(CPQQuoteCreationObj.ViewPrices.ViewPriceNRCValues);
                // double d_NRCValueText=Double.parseDouble(NRCValueText);
                 System.out.println("NRC Value is"+NRCValueText);
                for (i = 1; i <= Iter; i++) {
                    if(NRCValueText.equalsIgnoreCase(NRCValue))  {
                        ExtentTestManager.getTest().log(LogStatus.PASS, "NRC Value is set to "+NRCValueText);
                        Reusable.WaitforCPQloader();
                        System.out.println("NRC Value is set to "+NRCValueText);
                        break;
                    } else {
                        Thread.sleep(10000);
                        Reusable.WaitforCPQloader();
                        click(CPQQuoteCreationObj.ViewPrices.ViewPriceNRCValues);
                        NRCValueText = findWebElement(CPQQuoteCreationObj.ViewPrices.ViewPriceNRCValues).getAttribute("value");
                        scrollIntoTop();
                        continue;
                    }
                }
                if (i > Iter) {
                    ExtentTestManager.getTest().log(LogStatus.FAIL, "NRC Value is still set as "+NRCValueText+ " but the Expected status is "+NRCValue);
                    System.out.println("Quote stage is still set as "+NRCValueText+ " but the Expected status is "+NRCValue);
                    ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
                }
            } else {
                ExtentTestManager.getTest().log(LogStatus.FAIL, "NRC Value is not visible, Please Verify");
                System.out.println("NRC Value is not visible, Please Verify");
                ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
            }
            //Comparing MRC Values
            ScrollIntoViewByString(CPQQuoteCreationObj.ViewPrices.ViewPriceMRCValues);
            if (waitForElementToBeVisible(CPQQuoteCreationObj.ViewPrices.ViewPriceMRCValues, 100)) {
                Reusable.WaitforCPQloader();
                String MRCValue_Field = getTextFrom(CPQQuoteCreationObj.ViewPrices.ViewPriceMRCValues);
                // double d_MRCValue_Field=Double.parseDouble(MRCValue_Field);
                System.out.println("NRC Value is"+MRCValue_Field);
                for (i = 1; i <= Iter; i++) {
                    if(MRCValue_Field.equalsIgnoreCase(MRCValue)) {
                        ExtentTestManager.getTest().log(LogStatus.PASS, "MRC Value is set to "+MRCValue_Field);
                        Reusable.WaitforCPQloader();
                        System.out.println("MRC Value is set to "+MRCValue_Field);
                        break;
                    } else {
                        
                        Reusable.WaitforCPQloader();
                        click(CPQQuoteCreationObj.ViewPrices.ViewPriceMRCValues);
                        MRCValue_Field = findWebElement(CPQQuoteCreationObj.ViewPrices.ViewPriceMRCValues).getAttribute("value");
                        scrollIntoTop();
                        continue;
                    }
                }
                if (i > Iter) {
                    ExtentTestManager.getTest().log(LogStatus.FAIL, "MRC Value is still set as "+MRCValue_Field+ " but the Expected status is "+MRCValue);
                    System.out.println("MRC Value is still set as "+MRCValue_Field+ " but the Expected status is "+MRCValue);
                    ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
                }
            } else {
                ExtentTestManager.getTest().log(LogStatus.FAIL, "MRC Value is not visible, Please Verify");
                System.out.println("MRC Value is not visible, Please Verify");
                ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
            }
            
            Reusable.WaitforCPQloader();
            Reusable.waitForpageloadmask();
            
            verifyExists(CPQQuoteCreationObj.ViewPrices.ClosePopup,"Verify Close Button");
            click(CPQQuoteCreationObj.ViewPrices.ClosePopup, "Click on Close Button");
            Reusable.waitForpageloadmask();
            
            Reusable.WaitforCPQloader();
            Reusable.waitForpageloadmask();
             
            updateSaveProductCPQ("SaveToQuote"); 
    
        
            
        return "True";
        
    }
    
    public String SelectMonthlyContractTerm(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
    {
    
        String Contract_Term_Monthly = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Contract_Term_Monthly");
        
            
        waitForElementToAppear(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermMonthly, 20);
        verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermMonthly, "'Contract Term Monthly' text field");
        sendKeys(CPQQuoteCreationObj.AdditionalProductDataPage.contractTermMonthly, Contract_Term_Monthly,"Enter Month");
        
        
        return "True";
    }
    
    public String ApplyCrossConnectFeature_AEnd(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
    {
    // Initializing the Variable
    WebElement demarcationCheckBox=null;
    String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
    String PR_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PR_Type");
    String Flow_Type = DataMiner.fngetcolvalue(testDataFile,sheetName, scriptNo, dataSetNo,"Flow_Type");


    waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectAEnd, 60);
    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectAEnd,"Carrier Hotel Cross Connect AEnd");
    click(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectAEnd,"Carrier Hotel Cross Connect AEnd");
    Reusable.waitForpageloadmask();

     waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDown, 60);
    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDown,"Supplier A End DropDown");
    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDown,"Supplier A End DropDown");
    Reusable.waitForpageloadmask();

     waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDownValue, 60);
    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDownValue,"Supplier A End DropDownValue");
    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierAEndDropDownValue,"Supplier A End DropDownValue");
    Reusable.waitForpageloadmask();

    return "True";
    }

    public String ApplyCrossConnectFeature_BEnd(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
    {
    // Initializing the Variable
    WebElement demarcationCheckBox=null;
    String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
    String PR_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PR_Type");
    String Flow_Type = DataMiner.fngetcolvalue(testDataFile,sheetName, scriptNo, dataSetNo,"Flow_Type");

    waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectBEnd, 60);
    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectBEnd,"Carrier Hotel Cross Connect B End");
    click(CPQQuoteCreationObj.CrossConnect_Featuers.CarrierHotelCrossConnectBEnd,"Carrier Hotel Cross Connect B End");
    Reusable.waitForpageloadmask();

     waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDown, 60);
    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDown,"Supplier B End DropDown");
    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDown,"Supplier B End DropDown");
    Reusable.waitForpageloadmask();

     waitForElementToAppear(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDownValue, 60);
    verifyExists(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDownValue,"Supplier B End DropDownValue");
    click(CPQQuoteCreationObj.CrossConnect_Featuers.SupplierBEndDropDownValue,"Supplier B End DropDownValue");
    Reusable.waitForpageloadmask();
    return "True";
    }
    
    public String Quotespawn(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
    {
    	// Initializing the Variable
    	WebElement demarcationCheckBox=null;
    	String Select_OptionQuote = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Select_OptionQuote");

    	waitForAjax();
    	Reusable.waitForpageloadmask();
    	waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature, 10);
    	ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature);
    	verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature,"Customer Signature on Quote section");        
    	click(CPQQuoteSubmissionObj.QuoteSubmit.CustomerSignature,"Customer Signature");

    	verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.SelectoptionVal,"'SelectoptionVal'");
    	click(CPQQuoteSubmissionObj.QuoteSubmit.SelectoptionVal,"'SelectoptionVal'");
    	ClickonElementByString("//div[@data-value='"+Select_OptionQuote+"']", 20);
    	Reusable.waitForpageloadmask();
    
    	waitForElementToAppear(CPQQuoteSubmissionObj.QuoteSubmit.ProceedtoQuote, 10);
    	ScrollIntoViewByString(CPQQuoteSubmissionObj.QuoteSubmit.ProceedtoQuote);
    	verifyExists(CPQQuoteSubmissionObj.QuoteSubmit.ProceedtoQuote,"ProceedtoQuote");        
    	click(CPQQuoteSubmissionObj.QuoteSubmit.ProceedtoQuote,"ProceedtoQuote");
    	Reusable.WaitforCPQloader();
    	waitForAjax();
    	Reusable.waitToPageLoad();
    
    	return "True";
    }
    
    public String quoteDiscountGovernanceReject(String require) throws IOException, InterruptedException {
    	waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink, 60);
    	Reusable.WaitforCPQloader();
    	verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"Approval page link'");
    	click(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"'Approval page link'");
    	Reusable.WaitforCPQloader();
    	Reusable.Waittilljquesryupdated();
    	if(verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn))
    	{
    	waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn, 40);
    	ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn);
    	verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn,"Approve or Reject Button");
    	javaScriptclick(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn,"Approve or Reject Button");
    	Reusable.WaitforCPQloader();
    	Reusable.Waittilljquesryupdated();
    	}else{
    	waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn_vp1, 40);
    	ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn_vp1);
    	verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn_vp1,"Approve or Reject Button");
    	javaScriptclick(CPQQuoteCreationObj.SubmissionforCommercialApproval.rejectDiscountBtn_vp1,"Approve or Reject Button");
    	Reusable.WaitforCPQloader();
    	Reusable.Waittilljquesryupdated();
    	}


    	verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"'General Information Link'");
    	ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
    	click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"General Information Link");
    	return "True";
    	}
    
    	public String quoteDiscountGovernanceApprove(String require) throws IOException, InterruptedException 
    	{
    	waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink, 60);
    	Reusable.WaitforCPQloader();
    	verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"Approval page link'");
    	click(CPQQuoteCreationObj.SubmissionforCommercialApproval.ApprovalLink,"'Approval page link'");
    	Reusable.WaitforCPQloader();
    	Reusable.Waittilljquesryupdated();
    	if(verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn))
    	{
    	waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn, 40);
    	ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn);
    	verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn,"Approve or Reject Button");
    	javaScriptclick(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn,"Approve or Reject Button");
    	Reusable.WaitforCPQloader();
    	Reusable.Waittilljquesryupdated();
    	}else
    	{
    	waitForElementToAppear(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn_vp1, 40);
    	ScrollIntoViewByString(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn_vp1);
    	verifyExists(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn_vp1,"Approve or Reject Button");
    	javaScriptclick(CPQQuoteCreationObj.SubmissionforCommercialApproval.approveDiscountBtn_vp1,"Approve or Reject Button");
    	Reusable.WaitforCPQloader();
    	Reusable.Waittilljquesryupdated();
    	}

    	verifyExists(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"'General Information Link'");
    	ScrollIntoViewByString(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk);
    	click(CPQQuoteCreationObj.LegalandTechnicalContactDetails.generalInformationLnk,"General Information Link");
    	return "True";
    	}
}




