package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_MCN_Create_Customer;
import testHarness.aptFunctions.APT_MCS_CreateFirewallDevice;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_01_APT_MCS_CustomerFunctionalityTests extends SeleniumUtils{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	APT_MCN_Create_Customer CreateCust = new APT_MCN_Create_Customer();
	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();
	APT_Login Login = new APT_Login();
	
	/*	String dataFileName = "ModifyOrder.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "CarNor";
	String scriptNo = "2";
	String dataSetNo = "12";
	String Amount = "2";*/
	
	private static GlobalVariables g;
	Properties prop = new Properties();
	
	@Parameters({ "scriptNo","dataSetNo","dataSheet","ScenarioName" })
	@Test
	public void CreateCustomer(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName) throws Exception
	{                                    
		Report.createTestCaseReportHeader(ScenarioName);	
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1+"/configuration.properties"))
		{
            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        //String botId = prop.getProperty("BotId");
        //record.startVideoRecording(botId, ScenarioName);
        
        try{
			//setup();				
			//Login.APT_Login_1(map.get("url for the Product"));
        	openurl(Configuration.APT_URL);
        	Login.APT_VoiceService();
        	CreateCust.navigateToCreateCustomerPage();
        	CreateCust.createCustomerFunctionality(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifysuccessmessage();
        	CreateCust.verifyCustomerdetails_InviewCustomerPage(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateCust.verifyEditCustomerFunction(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifysuccessmessage();
        	CreateCust.verifyCustomerdetails_InviewCustomerPage_edited(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateCust.VerifyAddUserFunctionality(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifysuccessmessage();
        	CreateCust.verifyAddedUserValuesInViewUserPage(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateCust.selectUserTOEdit(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateCust.verifyEditUserFunction(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifysuccessmessage();
        	CreateCust.deleteUser(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifysuccessmessage();
        	CreateCust.deleteCustomer();
        	CreateFirewall.verifysuccessmessage();
        }
	catch(Exception e)
	{
        Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
        ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
        ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	} 
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
	}

}
