package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSMergeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberDeactivationObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSNumberActivationFreeNumberPage extends SeleniumUtils {
	

	
	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;
	
	NMTSNumberReservationPage NumberReserv = new NMTSNumberReservationPage();
	
	public void FreeToActivateScenario(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		NumberReserv.NumberReservation(testDataFile,sheetName,scriptNo,dataSetNo);
		AllocationPerform(testDataFile,sheetName,scriptNo,dataSetNo);
		ActivatePerform(testDataFile,sheetName,scriptNo,dataSetNo);
	}
	
	
	public void AllocationPerform(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String ProcessingDecision = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Processing_Decision");
		String Contactnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contact_number");
		String ContractSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_Source");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String VATNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VAT_Number");
		String MRNNRN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MRN/NRN");
		String CustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Name");
		String BuildingName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Name");
		String BuildingNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Number");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String PostCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Post_Code");
		String Town = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Town");
	//	String AllocationRadioButton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Allocation_RadioButton");
	//	String FlatNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Flat_Number");
	//	String Extenstion = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Extenstion");
	//	String BuidingNumberletter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Buiding_Number_letter");
	//	String AllocationFirstName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Allocation_FirstName");
	//	String AllocationLastName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Allocation_LastName");
	//	String AllocationCutomerTitle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Allocation_CutomerTitle");
		String AllocationLanguage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Allocation_Customer_language");
		String RegisteredName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Registered_Name");
		String TelephoneNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number");
		String CIFvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CIF_value");
	
		
	//	Reusable.switchtoframe("fmMain");
		if(Availablity.toString().equals("COLT") || Availablity.toString().equals("DE NON-GEO") )
		{
			waitForAjax();
			
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservedRangeCheckbox,"Select Reserved  Checkbox");
			click(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservedRangeCheckbox,"Select Reserved  Checkbox");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ReservedRangeSelectMarkedLink,"Reserved Range Select Marked Link");
			click(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ReservedRangeSelectMarkedLink,"Reserved Range Select Marked Link");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectedRangeCheckbox,"Selected Range checkbox");
			click(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectedRangeCheckbox,"Selected Range checkbox");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.selectDecision,15);
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.selectDecision, ProcessingDecision.toString());
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo,"Selected Range Checkrange mark's checkbox");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo, Contactnumber.toString(),"Contact Number");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContractSource,15);
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContractSource, ContractSource.toString());
			waitForAjax();
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,"Submit Decision Button");
			click(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,"Submit Decision Button");
			
	}
	else {
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservedRangeCheckbox,"Reserved Range Checkbox");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservedRangeCheckbox,"Reserved Range Checkbox");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ReservedRangeSelectMarkedLink,"Reserved Range Select Marked Link");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ReservedRangeSelectMarkedLink,"Reserved Range Select Marked Link");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectedRangeCheckbox,"Selected Range checkbox");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectedRangeCheckbox,"Selected Range checkbox");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.selectDecision,15);
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.selectDecision, ProcessingDecision.toString());
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo,"Contact Number");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo, Contactnumber.toString(),"Contact Number");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContractSource,15);
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContractSource, ContractSource.toString());		
			if(Country.toString().equals("CH"))
			{

			}
			else {
				waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN,15);
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN,"MRN number");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN, MRNNRN.toString(),"MRN number");
					
			}
			
			if(isElementPresent(By.xpath("//input[@id='txtVatNumber']")))
			{
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,"VAT number");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,VATNumber,"VAT number");
			
			}
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.EndCustName,"End Customer Name");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.EndCustName,CustomerName.toString(),"End Customer Name");
			if(isElementPresent(By.xpath("//form[@id='frmReservationDetail_New']//input[@id='txtBuild']")))
					{
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingName,"Building Name");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingName, BuildingName.toString(),"Building Name");			
		
					}
	
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingNumber,"Building Number");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingNumber, BuildingNumber.toString(),"Building Number");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.StreetName,"Street Name");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.StreetName, StreetName.toString(),"Street Name");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Town,"Town Name");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Town, Town.toString(),"Town Name");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.PostCodeNMTS,"PostCodeNMTS");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.PostCodeNMTS, PostCode.toString(),"PostCodeNMTS");	
			
			if(Country.toString().equals("BE"))
			{
				
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.RegisteredName,"Registered Name");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.RegisteredName,RegisteredName,"Registered Name");
		
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationLanguage,"Language");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationLanguage,AllocationLanguage,"Language");
			
				/*
				if(AllocationRadioButton.toString().equals("Residential"))
				{
					waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,15);
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,"Residential Radio button");
					click(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,"Residential Radio button");
					
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,"VAT number");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,VATNumber,"VAT number");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.flatNumber,"Flat number");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.flatNumber,FlatNumber,"Flat number");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Extention,"Extenstion");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Extention,Extenstion,"Extenstion");
				
					
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.buildingnumberletter,"building number letter");
					Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.buildingnumberletter,BuidingNumberletter);

					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationFirstName,"First Name");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationFirstName,AllocationFirstName,"First Name");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationlastName,"Last Name");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationlastName,AllocationLastName,"Last Name");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.CustomerTitle,"Customer Title");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.CustomerTitle,AllocationCutomerTitle,"Customer Title");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationLanguage,"Language");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationLanguage,AllocationLanguage,"Language");
				
				}else{
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,"VAT number");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,VATNumber,"VAT number");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.flatNumber,"Flat number");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.flatNumber,FlatNumber,"Flat number");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Extention,"Extenstion");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Extention,Extenstion,"Extenstion");
			
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.buildingnumberletter,"building number letter");
					Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.buildingnumberletter,BuidingNumberletter);

					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.RegisteredName,"Registered Name");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.RegisteredName,RegisteredName,"Registered Name");
				}
				*/
				
			}
			if(Country.toString().equals("ES")){
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.TelephoneNumber,"Telephone Number");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.TelephoneNumber,TelephoneNumber,"Telephone Number");
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.CIFvalue,"CIFvalue");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.CIFvalue,CIFvalue,"CIFvalue");
			}
			if(Country.toString().equals("AT")){
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.TelephoneNumber,"Telephone Number");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.TelephoneNumber,TelephoneNumber,"Telephone Number");
			}
			if(Country.toString().equals("IE"))
			{
				/*
				if(AllocationRadioButton.toString().equals("Residential"))
				{
					
					
					waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,15);
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,"Residential Radio button");
					click(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,"Residential Radio button");
					
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationFirstName,"First Name");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationFirstName,AllocationFirstName,"First Name");
				
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationlastName,"Last Name");
					sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationlastName,AllocationLastName,"Last Name");
				
					
				}
				else {
					Report.LogInfo("Log Report",":","Selected country name is Country with Bussiness Radio button for allocation");
					
				}
				*/
			}
				
				waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,15);
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,"Submit Decision button");
				javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,"Submit Decision button");
				waitForAjax();	
				waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
				
				if(Country.toString().equals("UK"))
				{
					waitForAjax();
					isAlertPresent();
					
				}
				
				WaitForAjax();	
				if(isElementPresent(By.xpath("//*[@id='lblSuccessfulMsg']")))
				{
					String AllocationMessage = getTextFrom(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationMessage,"Allocation message");
					
					if(AllocationMessage.contains("NMTS Order Allocated")){
						ExtentTestManager.getTest().log(LogStatus.PASS, "NMTS Order Allocated successfully");
						}else{
							ExtentTestManager.getTest().log(LogStatus.FAIL, "NMTS Order not Allocated successfully");	
						}	
				}
					
		}
	
	}
	
	public void ActivatePerform(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String MADEBYNAME = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MADE_BY_NAME");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String FilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_By");
		String MatchingWithFilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Matching_With_FilterBy");
		String FilterText = NumberReserv.getvalue("NMTS_ID");

		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMenu");
		//click to activate 
		waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,15);
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,"Activate Number option");
		javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,"Activate Number option");
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMain");
		if(Country.toString().equals("IT")||Country.toString().equals("FR"))
		{
			try {
				
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Reservation of Colt");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Colt");
			}
			catch(StaleElementReferenceException r)
			{
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Reservation of Colt");
				Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Colt");
		
			}
			WaitForAjax();
		}
		
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,30);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,"Made By");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,MADEBYNAME);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateAvailability,"Availablity");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateAvailability,Availablity);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterBy,"Filter By");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterBy,FilterBy);
			WaitForAjax();
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,"Matching");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,MatchingWithFilterBy);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterText,"Filter text");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterText,FilterText,"Filter text");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitSearchbtn,"Submit Search button");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitSearchbtn,"Submit Search button");
			WaitForAjax();
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"OrderId");
			javaScriptDoubleclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"Order ID");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectAllocatedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectAllocatedRangeCheckbox,"Select Allocated Checkbox");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectAllocatedRangeCheckbox,"Select Allocated  Checkbox");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedRangeSelectMarkedLink,"Allocated Range Select Marked Link");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedRangeSelectMarkedLink,"Allocated Range Select Marked Link");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedSelectedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedSelectedRangeCheckbox,"Selected Range checkbox");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedSelectedRangeCheckbox,"Selected Range checkbox");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,"Activate Number Button");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,"Activate Number Button");
			WaitForAjax();
			
			waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
		
				if(Country.toString().equals("UK"))
					{
					//	AcceptJavaScriptMethod();
						isAlertPresent();
					}
			
			
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberMessage,15);	
			String ActivateMessage = getTextFrom(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberMessage,"Activate message");
			if(ActivateMessage.contains("Activated")){
				ExtentTestManager.getTest().log(LogStatus.PASS, "Number Activated successfully");
				}else{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Number not Activated successfully");	
				}
          	
		}
	
	public void ActivatePerform_ReservedToActivate(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String MADEBYNAME = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MADE_BY_NAME");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String FilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_By");
		String MatchingWithFilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Matching_With_FilterBy");
	//	String FilterText = NumberReserv.getvalue("NMTS_ID");
		String ReservedNumber_OR_NMTSID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ReservedNumber/NMTS_ID");

		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMenu");
		//click to activate 
		waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,15);
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,"Activate Number option");
		javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,"Activate Number option");
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMain");
		if(Country.toString().equals("IT")||Country.toString().equals("FR"))
		{
			try {
				
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Reservation of Colt");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Colt");
			}
			catch(StaleElementReferenceException r)
			{
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Reservation of Colt");
				Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Colt");
		
			}
			WaitForAjax();
		}
		
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,30);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,"Made By");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,MADEBYNAME);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateAvailability,"Availablity");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateAvailability,Availablity);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterBy,"Filter By");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterBy,FilterBy);
			WaitForAjax();
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,"Matching");
			Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,MatchingWithFilterBy);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterText,"Filter text");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterText,ReservedNumber_OR_NMTSID,"Filter text");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitSearchbtn,"Submit Search button");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitSearchbtn,"Submit Search button");
			WaitForAjax();
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"OrderId");
			javaScriptDoubleclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"Order ID");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectAllocatedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectAllocatedRangeCheckbox,"Select Allocated Checkbox");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectAllocatedRangeCheckbox,"Select Allocated  Checkbox");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedRangeSelectMarkedLink,"Allocated Range Select Marked Link");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedRangeSelectMarkedLink,"Allocated Range Select Marked Link");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedSelectedRangeCheckbox,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedSelectedRangeCheckbox,"Selected Range checkbox");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocatedSelectedRangeCheckbox,"Selected Range checkbox");
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,"Activate Number Button");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,"Activate Number Button");
			WaitForAjax();
			
			waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
		
				if(Country.toString().equals("UK"))
					{
					//	AcceptJavaScriptMethod();
						isAlertPresent();
					}
			
			
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberMessage,15);	
			String ActivateMessage = getTextFrom(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberMessage,"Activate message");
			if(ActivateMessage.contains("Activated")){
				ExtentTestManager.getTest().log(LogStatus.PASS, "Number Activated successfully");
				}else{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Number not Activated successfully");	
				}
          	
		}
		
		
}
	
	
	
	
	
	
	
	
	
	
	
	
	
 

	
	
	
	

