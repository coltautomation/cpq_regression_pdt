package testHarness.ncFunctions;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.ModifyOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import pageObjects.ncObjects.NewHubOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class IpCarNorOrder extends SeleniumUtils  
{
	
	protected static Utilities util = new Utilities();
	ReusableFunctions Reusable = new ReusableFunctions();

	public void IpAccessGetOrderDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CarOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CAR_Order_No");
//		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
//		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");

//		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		Reusable.waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");

		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CarOrdernumber);

		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
        String[] arrOfStr = CarOrdernumber.split("#", 0);
        waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
        
//        verifyExists("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","order number");
//		click("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","order number");
		
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);

		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"New IP Access Product Order");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"New IP Access Product Order");

		String NCServiceId=getTextFrom(NewHubOrderObj.NewHubOrder.NCSID);
	 	DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Retained NCS ID", NCServiceId);
	}
	
	public void ExistingOrderCarNor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No");
		String CeaseOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
			
//		 verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		 click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		 Reusable.waitForAjax();
		 
		 verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		 click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		 
		 selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		 
		 sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		 
		 verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		 click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		 
		 String[] arrOfStr = CeaseOrdernumber.split("#", 0);

		 verifyExists("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","Click order number");
		 click("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","Apply Button");
		 
		 
		 String Orderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		 DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL", Orderscreenurl);
		 
		 verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
		 click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
	}	
		
 	public void CreateCarNorOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
 		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
	 
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"New Composite Order");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"New Composite Order");
		
		waitForElementToAppear(NcOrderObj.createOrder.NewCompositeOrder, 10);
		verifyExists(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
		click(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
		Reusable.waitForAjax();
		
		String Ordernumber = getTextFrom(NcOrderObj.createOrder.OrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No", Ordernumber);
		Reusable.waitForAjax();
		String[] arrOfStr = Ordernumber.split("#", 0);
//			Log.info(arrOfStr[1]);
		
		if (ProductName.toString().contains("Line"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Line CarNor order created using CTAF Automation");
		}
		else if (ProductName.toString().contains("Hub"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Hub CarNor order created using CTAF Automation");
		}
		else if (ProductName.toString().contains("Spoke"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Spoke CarNor order created using CTAF Automation");
		}
		else if (ProductName.toString().contains("IPA"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "IP Access CarNor order created using CTAF Automation");
		}
		else
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "IPVPN CarNor order created using CTAF Automation");
		}

		verifyExists(NcOrderObj.createOrder.Update,"Update");
		click(NcOrderObj.createOrder.Update,"Update");

		String Orderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL", Orderscreenurl);
		System.out.println(Orderscreenurl);
		Reusable.waitForAjax();
		
	}
	 
	public void AddProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		
		verifyExists(NcOrderObj.addProduct.OrderTab,"Orders Tab");
		click(NcOrderObj.addProduct.OrderTab,"Orders Tab");

		verifyExists(NcOrderObj.addProduct.AddonOrderTab,"Add");
		click(NcOrderObj.addProduct.AddonOrderTab,"Add");
		Reusable.waitForAjax();

		if (ProductName.toString().contains("Line"))
		{
			verifyExists(NcOrderObj.addProduct.EthernetProductCheckBox,"Ethernet Connection Product");
			click(NcOrderObj.addProduct.EthernetProductCheckBox,"Ethernet Connection Product");
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			verifyExists(NcOrderObj.addProduct.HubProductCheckBox,"Ethernet Hub Product");
			click(NcOrderObj.addProduct.HubProductCheckBox,"Ethernet Hub Product");
		}
		else if (ProductName.toString().contains("IPA"))
		{
			verifyExists(NcOrderObj.addProduct.IpAccessCheckBox,"IP Access Product");
			click(NcOrderObj.addProduct.IpAccessCheckBox,"IP Access Product");
		}
		else
		{
			verifyExists(NcOrderObj.addProduct.IpvpnserviceCheckBox,"IP VPN Service Product");
			click(NcOrderObj.addProduct.IpvpnserviceCheckBox,"IP VPN Service Product");
		}
		
		verifyExists(NcOrderObj.addProduct.Addbutton,"Add");
		click(NcOrderObj.addProduct.Addbutton,"Add");
		Reusable.waitForAjax();
		
	}
	
	public void AddProductdetailsCarNor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String OrderRefNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Sys Ref ID");
		String OrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
		String CommercialProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Commercial Product Name");
		String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
		String RouterTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Technology");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
		String Layer3Resilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String TypeofBilling = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Typeof_Billing");
		String RelatedVoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Related_Voip_Service");
		String BespokeReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Reference");
		String NCServiceId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Retained NCS ID");
		
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"Underlying Ip order");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"Underlying Ip order");
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit");
		click(NcOrderObj.addProductDetails.Edit,"Edit");

		verifyExists(NcOrderObj.addProductDetails.OrderSystemName,"Order System Name");
		click(NcOrderObj.addProductDetails.OrderSystemName,"Order System Name");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NcOrderObj.addProductDetails.OrderSystemName,"SPARK","Order System Name");
		
		verifyExists(NcOrderObj.addProductDetails.Orderreferencenumber,"Order Reference Number");
		sendKeys(NcOrderObj.addProductDetails.Orderreferencenumber,OrderRefNumber);
		
		verifyExists(NcOrderObj.addProductDetails.Ordernumber,"Order Number");
		sendKeys(NcOrderObj.addProductDetails.Ordernumber,OrderNumber);

		verifyExists(NcOrderObj.addProductDetails.CommercialProductName,"Commercial Product Name");
		sendKeys(NcOrderObj.addProductDetails.CommercialProductName,CommercialProductName);
		
		
		if (ProductName.toString().contains("IPA"))
		{
			verifyExists(NcOrderObj.addProductDetails.RouterType,"Router Type");
			click(NcOrderObj.addProductDetails.RouterType,"Router Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addProductDetails.RouterType,RouterType,"Router Type");

			verifyExists(NcOrderObj.addProductDetails.RouterTechnology,"Router Technology");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addProductDetails.RouterTechnology,RouterTechnology,"Router Technology");

			verifyExists(NcOrderObj.addProductDetails.ServiceBandwidth,"Service Bandwidth");
			selectByVisibleText(NcOrderObj.addProductDetails.ServiceBandwidth,ServiceBandwidth,"Service Bandwidth");
//				Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBandwidth, Keys.ENTER);
			
			verifyExists(NcOrderObj.addProductDetails.Layer3Resilience,"Layer 3 Resilience");
//				Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addProductDetails.Layer3Resilience,Layer3Resilience,"Layer 3 Resilience");

			verifyExists(NcOrderObj.addProductDetails.TypeOfBilling,"Type of Billing");
			selectByVisibleText(NcOrderObj.addProductDetails.TypeOfBilling,TypeofBilling,"Type of Billing");

			
			if (RelatedVoipService.toString().contains("IPA"))
			{
				verifyExists(NcOrderObj.addProductDetails.RelatedVoipService,"Related Voip Service");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(NcOrderObj.addProductDetails.RelatedVoipService,RelatedVoipService,"Related Voip Service");
			}
			
			//confirm with Gokul
			
			/*
			 * else { if (BespokeReference.toString().equals(null)) {
			 * ExtentTestManager.getTest().log(LogStatus.PASS,
			 * " Step: Bespoke not required"); } else {
			 * verifyExists(NcOrderObj.addProductDetails.
			 * BespokeReference,"Bespoke Reference");
			 * click(NcOrderObj.addProductDetails.BespokeReference,"Bespoke Reference");
			 * Reusable.waitForSiebelLoader();
			 * sendKeys(NcOrderObj.addProductDetails.BespokeReference,
			 * BespokeReference,"Bespoke Reference"); } }
			 */
		}
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.ParallelRun,"No");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NcOrderObj.addProductDetails.ParallelRun,"No","Parallel Run Flag");
		
		verifyExists(NcOrderObj.addProductDetails.ReplacedTechSerID,"Replaced Techser ID");
		sendKeys(NcOrderObj.addProductDetails.ReplacedTechSerID,NCServiceId);
		
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update product details");
	}
	
	public void IpAddressingFeatureUpdateCarNor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String IpAddressingFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP Addressing Format");
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addIpAddressing.IpAddressingLink,"IP Addressing Link");
		click(NcOrderObj.addIpAddressing.IpAddressingLink,"IP Addressing Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addIpAddressing.IpAddressingFormat,"IP Addressing Format");
		click(NcOrderObj.addIpAddressing.IpAddressingFormat,"IP Addressing Format");
		Reusable.waitForSiebelLoader();
		
		selectByVisibleText(NcOrderObj.addIpAddressing.IpAddressingFormat,IpAddressingFormat,"IP Addressing Format");
		if (IpFormat.toString().contains("IPv4"))
		{
			verifyExists(NcOrderObj.addIpAddressing.Ipv4AddressingType,"IPv4 Addressing Type");
			click(NcOrderObj.addIpAddressing.Ipv4AddressingType,"IPv4 Addressing Type");
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv4AddressingType,AddressingType,"IPv4 Addressing Type");
		}
		else if (IpFormat.toString().contains("IPv6"))
		{
			verifyExists(NcOrderObj.addIpAddressing.Ipv6AddressingType,"IPv6 Addressing Type");
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv6AddressingType,AddressingType,"IPv6 Addressing Type");
		}
		else
		{
			verifyExists(NcOrderObj.addIpAddressing.Ipv4AddressingType,"IPv4 Addressing Type");
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv4AddressingType,AddressingType,"IPv4 Addressing Type");

			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addIpAddressing.Ipv6AddressingType,"IPv6 Addressing Type");
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv6AddressingType,AddressingType,"IPv6 Addressing Type");
		}
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();

	}
	
	public void AddProviderFeatureCarNor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String TypeofFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PA/PI Feature");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);

		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"Underlying Ip order");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"Underlying Ip order");
		
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,TypeofFeature);
		Reusable.waitForAjax();

		String handleBefore = webDriver.getWindowHandle();

		verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
		click(NcOrderObj.generalInformation.Browse,"Browse button");
		Reusable.waitForAjax();

		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		
//				switchWindow(urlContent, handles);
		
	    System.out.println("Window handel"+curent);
	    webDriver.switchTo().window(curent);

	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	    click(("@xpath=//span[text()='"+TypeofFeature+"']/parent::*/parent::*/parent::*/td//input"), "Provider Independent feature");
	    Reusable.waitForAjax();
	     
	    webDriver.switchTo().window(curent);
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
	    
	    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
		click(NcOrderObj.generalInformation.SelectButton,"Select button");
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();

		// switch back to parent window
	    webDriver.switchTo().window(handleBefore);
	    Reusable.waitForAjax();
	}

	public void ProviderAggUpdateCarNor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		Random rand = new Random();
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String NoIpv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv4 Addresses");
		String NoIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv6 Addresses");
		String PrimaryRangeReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary Range");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
			
		getUrl(Orderscreenurl);
		
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		
		if (IpFormat.toString().trim().equalsIgnoreCase("IPv4"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
				
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,"Number of IPv4 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,NoIpv4Address,"NoIpv4Address");
			
			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else if (IpFormat.toString().trim().equalsIgnoreCase("IPv6"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,"Number of IPv4 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,NoIpv4Address,"NoIpv4Address");

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();

		}
	}
	
	public void ProviderIndUpdateCarNor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		Random rand = new Random();
		String PrimaryRangeReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary Range");
		String Ipv4Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4 Prefix Inp");
		String LanIpv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN Inter IPv4 Address");
		String TypeofPiAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PI IPv4/IPv6 Addresses");		

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
		click(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();
    	
		verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
		sendKeys(NcOrderObj.addProductDetails.PrefixInput,Ipv4Prefix,"Ipv4Prefix");

		String ipAddressValue[]=LanIpv4Address.split("\\.");
		for(int i=0;i<ipAddressValue.length;i++)
		{
			verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,"Lan Interface IPv4 Address");
//					sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,["+(i+1)+"],ipAddressValue[i]);
			SendKeys(findWebElement("@xpath=//td/a[(text()='LAN Interface IPv4 Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),ipAddressValue[i]);
			Reusable.waitForAjax();
		}

		verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
		selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
		
		int randnumb = rand.nextInt(1000000000);
		verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
		sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
		
		
		verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,"Type of PI IPv4 Address");
		selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,TypeofPiAddress,"TypeofPiAddress");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
	}
	    
	public void AddDHCPFeatureCarNor(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String TypeofFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PA/PI Feature");
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,"DHCP");
		Reusable.waitForAjax();
					 
		verifyExists(NcOrderObj.generalInformation.SelectFeature,"Select Feature button");
		click(NcOrderObj.generalInformation.SelectFeature,"Select Feature button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,TypeofFeature);
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.SelectFeature,"Select Feature button");
		click(NcOrderObj.generalInformation.SelectFeature,"Select Feature button");
		Reusable.waitForAjax();
	}
	
	public void DHCPUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String DhcpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Format");
		String DomainName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Domain Name");
		String DHCPIpv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Address");
		String GatewayIpAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LegacyCpeOamLevel");
		String PrimaryWinsSerAddr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary WINS Server Add");
		String DHCPIpv4Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Prefix");
		String SecondaryWinsSerAddr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Secondary WINS Server Add");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		
	 	verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addDhcp.DhcpLink,"DHCP link");
		click(NcOrderObj.addDhcp.DhcpLink,"DHCP link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addDhcp.Format,"DHCP Format");
		selectByVisibleText(NcOrderObj.addDhcp.Format,DhcpFormat,"DhcpFormat");

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addDhcp.DhcpLink,"DHCP link");
		click(NcOrderObj.addDhcp.DhcpLink,"DHCP link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addDhcp.DomainName,"Domain Name");
		sendKeys(NcOrderObj.addDhcp.DomainName,DomainName);


		String dhcpIpAddressValue[]=DHCPIpv4Address.split("\\.");
		for(int i=0;i<dhcpIpAddressValue.length;i++)
		{
			SendKeys(findWebElement("@xpath=//td/a[text()='DHCP IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),dhcpIpAddressValue[i]);
			Reusable.waitForAjax();
		}
		
		String gatewayIpAddress[]=GatewayIpAddress.toString().split("\\.");
		for(int i=0;i<gatewayIpAddress.length;i++)
		{
//					sendKeys(NcOrderObj.addDhcp.GatewayIpv4Add,["+(i+1)+"],gatewayipAddress[i]);			
			SendKeys(findWebElement("@xpath=//td/a[text()='Gateway IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),gatewayIpAddress[i]);		
			Reusable.waitForAjax();
		}

		verifyExists(NcOrderObj.addDhcp.DhcpIpv4Prefix,"DHCP IPv4 Prefix");
		sendKeys(NcOrderObj.addDhcp.DhcpIpv4Prefix,DHCPIpv4Prefix,"Dhcp Ipv4 Prefix");
		Reusable.waitForAjax();
		
		String primaryWinsAddressValue[]=PrimaryWinsSerAddr.split("\\.");
		for(int i=0;i<primaryWinsAddressValue.length;i++)
		{
			SendKeys(findWebElement("@xpath=//td/a[text()='Primary WINS Server IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),primaryWinsAddressValue[i]);
			Reusable.waitForAjax();
		}
		
		String secondaryWinsAddressValue[]=SecondaryWinsSerAddr.split("\\.");
		for(int i=0;i<secondaryWinsAddressValue.length;i++)
		{
			SendKeys(findWebElement("@xpath=//td/a[text()='Secondary WINS Server IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),secondaryWinsAddressValue[i]);
			Reusable.waitForAjax();
		}

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
	}
	    
	public void DecomposeIpOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
        verifyExists(NcOrderObj.generalInformation.SubIpOrder,"IP Access Product Order");
        click(NcOrderObj.generalInformation.SubIpOrder,"IP Access Product Order");

        verifyExists(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        click(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        waitForAjax();
        Reusable.waitForpageloadmask();
	}
		
	public void EthernetProductDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String Bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");		
	 	String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);

		verifyExists(NcOrderObj.addProductDetails.UnderlyingEthernetOrder,"Ethernet Connection Product Order");
		click(NcOrderObj.addProductDetails.UnderlyingEthernetOrder,"Ethernet Connection Product Order");
		Reusable.waitForAjax();
	 
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();
		
		String Topology = "Point to Point";
		verifyExists(NcOrderObj.addProductDetails.Topology,"Topology");
		click(NcOrderObj.addProductDetails.Topology,"Topology");
		selectByVisibleText(NcOrderObj.addProductDetails.Topology,Topology,"Topology");

		verifyExists(NcOrderObj.addProductDetails.ServiceBand,"Service Bandwidth");		
		sendKeys(NcOrderObj.addProductDetails.ServiceBand,Bandwidth,"Service Bandwidth");
		Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBand, Keys.ENTER);
		Reusable.waitForAjax();
		Reusable.waitToPageLoad();

		sendKeys(NcOrderObj.addProductDetails.ServiceBand,Bandwidth);
		Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBand, Keys.ENTER);

		
		verifyExists(NcOrderObj.addProductDetails.CircuitCategory,"Circuit Category");
		click(NcOrderObj.addProductDetails.CircuitCategory,"Circuit Category");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NcOrderObj.addProductDetails.CircuitCategory,"IA","IA");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		clickByJS(NcOrderObj.createOrder.Update);
		
		Reusable.waitForAjax();
	}

	public void DecomposeEthernetOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
   	{
	 	String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
        verifyExists(NcOrderObj.generalInformation.Suborder,"Ethernet Connection Product Order");
        click(NcOrderObj.generalInformation.Suborder,"Ethernet Connection Product Order");
        waitForAjax();
		
        verifyExists(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        click(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        waitForAjax();
        Reusable.waitForpageloadmask();
	}

	public void ProductDeviceDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		String ResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Resilience Option");
		String accessTechnology= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access Technology");
		String accessType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access type");
		String aendSiteID= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Site ID");
		String aPortRole= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Port Role");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		
		verifyExists(NcOrderObj.addProductDetails.UnderLyingEndSiteOrder,"End Site Product Order");
		click(NcOrderObj.addProductDetails.UnderLyingEndSiteOrder,"End Site Product Order");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();
		
		if (ResilienceOption.contains("Protected"))
		{
			verifyExists(NcOrderObj.addEndSiteDetails.ResilienceOption,"Resilience Option");
			selectByVisibleText(NcOrderObj.addEndSiteDetails.ResilienceOption,ResilienceOption,"ResilienceOption");
		}
			else
			{
				verifyExists(NcOrderObj.addEndSiteDetails.ResilienceOption,"Resilience Option");
				selectByVisibleText(NcOrderObj.addEndSiteDetails.ResilienceOption,ResilienceOption,"ResilienceOption");
			}
		
		verifyExists(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Access Technology");
		click(EthernetOrderObj.CompositOrders.AccessTechnolgy,"A End Access Technology");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessTechnolgy,accessTechnology,"A End Access Technology");
		
//		verifyExists(EthernetOrderObj.CompositOrders.AccessType,"Access Type");
//		click(EthernetOrderObj.CompositOrders.AccessType,"A End Access Type");
//		Reusable.waitForSiebelLoader();

		verifyExists(NcOrderObj.addEndSiteDetails.AccessType,"Access Type");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.AccessType,accessType,"AccessType");
		
		String siteEnd= "A END";
		verifyExists(EthernetOrderObj.CompositOrders.SiteEnd,"Site End");
		click(EthernetOrderObj.CompositOrders.SiteEnd,"A End Site End");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.SiteEnd,siteEnd,"A End Site End");
		
		verifyExists(EthernetOrderObj.CompositOrders.SiteID," SiteID");  
	    sendKeys(EthernetOrderObj.CompositOrders.SiteID,aendSiteID," SiteID");
	    
	    verifyExists(EthernetOrderObj.CompositOrders.Update,"details are Updated");
		click(EthernetOrderObj.CompositOrders.Update,"details are Updated");
		
		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
		
		verifyExists(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
		click(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
		
		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
		click(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
		
		String presentConnectType= "LC/PC";
		verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
		click(EthernetOrderObj.CompositOrders.PresentConnectType,"A End Presentation Connector Type");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,presentConnectType,"A End Presentation Connector Type");
		

		verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
		click(EthernetOrderObj.CompositOrders.AccessportRole,"A End port Role");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,aPortRole,"A End Port Role");
		
		verifyExists(EthernetOrderObj.CompositOrders.Update,"details are Updated");
		click(EthernetOrderObj.CompositOrders.Update,"details are Updated");
	}

	public void ProcessIpAccessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
//	 	verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
//		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
		
		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
		Reusable.waitForAjax();
		String[] arrOfStr = Ordernumber.split("#", 0);
		
//		verifyExists("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/parent::*/td//input","order number");
//		click("//a/span[contains(text(),'\"+arrOfStr[1]+\"')]/parent::*/parent::*/parent::*/td//input","order number");
		
 		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
 		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

 		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
 		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
	}
		
	public void CompleteIpAccessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
	 	String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No");
        String WorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Work Items");
        String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL");
		
		String[] arrOfStr = Ordernumber.split("#", 0);
		
//		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
//		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
//		
//		verifyExists("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","order number");
//		click("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","order number");
//		
//		String Orderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
//		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL", Orderscreenurl);
//		System.out.println(Orderscreenurl);
		
		getUrl(Orderscreenurl);
		
		verifyExists(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
        click(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
        
        verifyExists(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        click(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        waitForAjax();
		
        verifyExists(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        click(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        waitForAjax();
        
        for (int k=1; k<=Integer.parseInt(WorkItems);k++)
        {
            waitForElementToAppear(NcOrderObj.workItems.TaskReadytoComplete,120,10000);
            click(NcOrderObj.workItems.TaskReadytoComplete,"Workitem in Ready status");
            Completworkitem(GetText(NcOrderObj.taskDetails.TaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);         
        }
       
		getUrl(Orderscreenurl);
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
		waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",180,10000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Ordernumber + EthernetOrderObj.CompositOrders.arrorder1);

	    if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+Ordernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Ordernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+Ordernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Ordernumber +" Execution InProgress");
		}
	}

	public void GotoErrors(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

	 	String Ordernumber = getTextFrom(NcOrderObj.createOrder.OrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No", Ordernumber);
		Reusable.waitForAjax();
		
		getUrl(Orderscreenurl);

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
		Reusable.waitForAjax();
		String[] arrOfStr1 = Ordernumber.split("#", 0);
		
		String	arrOfStr= getTextFrom("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/following-sibling::*[4]\")");

		if(arrOfStr.equalsIgnoreCase("Blocked by Errors"))
		{
			verifyExists("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*","Sorting of orders");
			click("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*","Sorting of orders");

			verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
			click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");

			verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			waitForAjax();
			
			verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
			click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
			waitForAjax();
		}
		else 
			{
			System.out.println("Order did not have any errors to be Captured");

			}
	}
		
	public void Completworkitem(String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Resilience Option");
		String AccessNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access NW Element");
		String AccessPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access port");
		String CPENNiPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
		String CPENNiPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
		String PeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
		String PEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 1");
		String PEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 2");
		String VcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend VCX Control");
		String Beacon = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Beacon");
		String L3cpeName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE Name");
		String L3cpeUniPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE UNI Port");
		String PrimarySR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR");
		String PrimarySRGil = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR GIL");
		String WanIpv4SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Wan IPv4 Subnet Size");
		String DuplexBfdInterval = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Duplex/Bfd interval");
		String EgressPolicyMap = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Egress Policy Map");
		String SpeedMultiplier = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Speed/Multiplier");
		String PremiumCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premium CIR%");
		String InternetCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Internet CIR%");
		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");

		
		System.out.println("In Switch case with TaskName :"+taskname);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Start completion of Task: "+taskname[0]);
		switch(taskname[0])
		{
			case "Reserve Access Resources":
			if (ResilienceOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AccessNwElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AccessPort);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,CPENNiPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,CPENNiPort2);
				waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
			}
			else
			{
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AccessNwElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//						clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AccessPort);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//						clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,CPENNiPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
//						workitemcounter.set(workitemcounter.get()+1);
			}
			break;
			
			case "Transport Circuit Design":
			if (ResilienceOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
//						clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForAjax();
				
				if (DeviceType.contains("Stub"))
				{
					if (AccessNwElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
					}			
//										if (Beacon.contains("Beacon"))
//										{
//											verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//											clearTextBox(NcOrderObj.workItems.Beacon);
//											sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//											Reusable.waitForAjax();
//											Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//											Reusable.waitForSiebelSpinnerToDisappear();
//										}
				}						
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//						clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//						clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort2);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
            	//Code for Unprotected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
//						clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//						clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

//						if (DeviceType.contains("Stub"))
//						{
//							if (AccessNwElement.contains("SC"))
//							{
//								verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//								clearTextBox(NcOrderObj.workItems.VCXController);
//								sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
//								Reusable.waitForAjax();
//								Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
//								Reusable.waitForSiebelSpinnerToDisappear();
//							}
//										if (Beacon.contains("Beacon"))
//										{
//											verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//											clearTextBox(NcOrderObj.workItems.Beacon);
//											sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//											Reusable.waitForAjax();
//											Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//											Reusable.waitForSiebelSpinnerToDisappear();
//										}
//						}						
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Reserve MPR Resources" :
			{
				verifyExists(NcOrderObj.workItems.L3cpeName,"L3CPE Name");
				clearTextBox(NcOrderObj.workItems.L3cpeName);
				sendKeys(NcOrderObj.workItems.L3cpeName,L3cpeName);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.L3cpeName, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.L3cpeUNIPort,"L3CPE UNI Port");
				clearTextBox(NcOrderObj.workItems.L3cpeUNIPort);
				sendKeys(NcOrderObj.workItems.L3cpeUNIPort,L3cpeUniPort);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.L3cpeUNIPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Verify SR Router and GIL":
			{
				verifyExists(NcOrderObj.workItems.AsrGil,"ASR GIL");
				clearTextBox(NcOrderObj.workItems.AsrGil);

				verifyExists(NcOrderObj.workItems.Asr,"ASR");
				clearTextBox(NcOrderObj.workItems.Asr);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForAjax();
				sendKeys(NcOrderObj.workItems.Asr,PrimarySR);
				Reusable.waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.Asr, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AsrGil,"ASR GIL");
				clearTextBox(NcOrderObj.workItems.AsrGil);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForAjax();
//						Reusable.waitForSiebelLoader();
				sendKeys(NcOrderObj.workItems.AsrGil,PrimarySRGil);
				Reusable.waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AsrGil, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//								verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//								click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//								verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//								click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Set Bespoke Parameters":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.WanIpSubnetSize,"WAN IPv4 Subnet Size");
				select(NcOrderObj.workItems.WanIpSubnetSize,WanIpv4SubnetSize);

				verifyExists(NcOrderObj.workItems.WanIpSubnetSizeFlag,"WAN IPv4 Subnet Size Flag");
				select(NcOrderObj.workItems.WanIpSubnetSizeFlag,"Yes");

				verifyExists(NcOrderObj.workItems.OtherWanIpSubnetSize,"Other WAN IPv4 Subnet Size");
				select(NcOrderObj.workItems.OtherWanIpSubnetSize,DuplexBfdInterval);
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Select LAN IP Range":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.RequestNewIp,"Request New IP from EIP button");
				click(NcOrderObj.workItems.RequestNewIp,"Request New IP from EIP button");
				Reusable.waitForAjax();
			}
			break;
			
			case "Waiting for RIR Approval":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Manual Design Task for MPR":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.EgressPolicyMap,"Egress Policy Map");
				clearTextBox(NcOrderObj.workItems.EgressPolicyMap);
				sendKeys(NcOrderObj.workItems.EgressPolicyMap,EgressPolicyMap);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.EgressPolicyMap, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
	        
			case "Load License to MPR":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
	        
			case "Check Order parameters":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
	    	case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
	    	case "Activation Start Confirmation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "Confirm L3 Activation Start":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "MPR Trigger Configuration Start":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Duplex Check":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "Speed Test":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "Remote Connectivity Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Enable Smart Licensing":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
					
			case "Disable Smart Licensing":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}

			case "Customer Dynamic Routing Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
	        break;

			case"Set Bandwidth Distribution Parameters":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				sendKeys(NcOrderObj.workItems.PremiumCir,PremiumCir);
				sendKeys(NcOrderObj.workItems.InternetCir,InternetCir);
				
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Bandwidth Profile Confirmation":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Check BGP":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
	        break;

			case "Service Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Legacy Activation Completed":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			default:
			// No case found that it will complete the Workitem	
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			Reusable.waitForAjax();
		}
	}	

	public void CreateIpAccessCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CarOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CAR_Order_No");
		 
//	 	verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		Reusable.waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");

		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CarOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
		String[] arrOfStr = CarOrdernumber.split("#", 0);
        waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
        
//        verifyExists("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","Apply Button");
//		click("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*","Apply Button");
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);

		String ProductInstancenumber = getTextFrom(NcOrderObj.carNor.IpProductInstNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Product_Instance_Number", ProductInstancenumber);
		
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0);

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
		
        verifyExists(NcOrderObj.carNor.ProductInstTab,"Product Instance Tab");
        click(NcOrderObj.carNor.ProductInstTab,"Product Instance Tab");
		
        verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");

		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,ProductInstancenumber);

		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		
//		verifyExists("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/parent::*/td//input","Select Instance");
//		click("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/parent::*/td//input","Apply Button");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

		verifyExists(NcOrderObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		click(NcOrderObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		waitForAjax();
		
		String CeaseOrdernumber = getTextFrom(EthernetOrderObj.CompositOrders.LinkforOrder);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No", CeaseOrdernumber);
		
		
		String CeaseOrderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL", CeaseOrderscreenurl);
		System.out.println(CeaseOrderscreenurl);
		Reusable.waitForAjax();
	}
			
	public void UpdateCeaseDescription() throws Exception
	{
	 	verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		
		verifyExists(ModifyOrderObj.ModifyOrder.EditDesc,"Edit Desc");		
		click(ModifyOrderObj.ModifyOrder.EditDesc,"Edit Desc");
		Reusable.waitForAjax();
		
		sendKeys(NcOrderObj.createOrder.OrderDescription, "Cease Order created using Automation script");
		
		verifyExists(NcOrderObj.createOrder.Update,"Update");
		click(NcOrderObj.createOrder.Update,"Update");
		Reusable.waitForAjax();
	}
		 
	public void EditProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenurl);
		
		verifyExists(NcOrderObj.carNor.DisconnectIpAccessOrder,"Disconnect IP Access Order");
		click(NcOrderObj.carNor.DisconnectIpAccessOrder,"Disconnect IP Access Order");
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.carNor.SelectDisconnectIpAccessOrder,"Disconnect IP Access Order");
		click(NcOrderObj.carNor.SelectDisconnectIpAccessOrder,"Disconnect IP Access Order");
		
		verifyExists(NcOrderObj.carNor.HardCeaseDelay,"Hard cease Delay option");
		sendKeys(NcOrderObj.carNor.HardCeaseDelay,"0");
		
		
		String modifiedDate = util.getYesterDayDatenTime("MMM-dd-yyyy");
		String[] month = modifiedDate.split("-", 0);
		String[] Date = modifiedDate.split("-", 1);
		String[] Year = modifiedDate.split("-", 2);
		
		
		verifyExists(NcOrderObj.carNor.ColtPromiseMonth,"Promise Month");
		sendKeys(NcOrderObj.carNor.ColtPromiseMonth,month[0]);
		waitForAjax();
		
		verifyExists(NcOrderObj.carNor.ColtPromiseDate,"Promise Date");
		sendKeys(NcOrderObj.carNor.ColtPromiseDate,Date[0]);
		waitForAjax();
		
		verifyExists(NcOrderObj.carNor.ColtPromiseYear,"Promise Year");
		sendKeys(NcOrderObj.carNor.ColtPromiseYear,Year[0]);
		waitForAjax();
		
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
	}
		 
	public void DecomposeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenurl);
		
	 	verifyExists(NcOrderObj.addProduct.OrderTab,"Orders Tab");
		click(NcOrderObj.addProduct.OrderTab,"Orders Tab");
		
		verifyExists(NcOrderObj.carNor.SelectDisconnectIpAccessOrder,"Disconnect IP Access Order");
		click(NcOrderObj.carNor.SelectDisconnectIpAccessOrder,"Disconnect IP Access Order");

		verifyExists(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        click(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        waitForAjax();
	}

	public void ProcessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{    	
		String CeaseOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No");
		String CeaseOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenurl);
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
		Reusable.waitForAjax();
		
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		
		click("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/parent::*/td//input","clicking order number");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
	}

	public void CompleteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No");
		String CeaseOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
		String CeaseWorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease Work Items");
	 
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0);
		Thread.sleep(50000);
		
		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
        click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
		
        click("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*","Sorting of orders");
		
        verifyExists(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
        click(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
       
        verifyExists(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        click(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        waitForAjax();
		
        verifyExists(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        click(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        waitForAjax();
		
		
        for (int k=1; k<=Integer.parseInt(CeaseWorkItems);k++)
        {
            waitForElementToAppear(NcOrderObj.workItems.TaskReadytoComplete,120,10000);
            click(NcOrderObj.workItems.TaskReadytoComplete,"Workitem in Ready status");
            CompletworkitemCease(GetText(NcOrderObj.taskDetails.TaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);         
        }
       
		getUrl(CeaseOrderscreenurl);
       
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
       
        verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
        click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
        
        waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
        waitForAjax();
	}

	public void CompletworkitemCease(String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{ 
		switch(taskname[0])
		{
		case "Legacy Deactivation Completed":
		{
			verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
			click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
			waitForAjax();
						
		}
		break;
					
		case "Waiting for Hard Cease Date for L3":
		{
			
			String modifiedDate = util.getYesterDayDatenTime("MMM-dd-yyyy");
			String[] month = modifiedDate.split("-", 0);
			String[] Date = modifiedDate.split("-", 1);
			String[] Year = modifiedDate.split("-", 2);
			
			verifyExists(NcOrderObj.carNor.ColtPromiseMonth,"Promise Month");
			sendKeys(NcOrderObj.carNor.ColtPromiseMonth,month[0]);
			waitForAjax();
			
			verifyExists(NcOrderObj.carNor.ColtPromiseDate,"Promise Date");
			sendKeys(NcOrderObj.carNor.ColtPromiseDate,Date[0]);
			waitForAjax();
			
			verifyExists(NcOrderObj.carNor.ColtPromiseYear,"Promise Year");
			sendKeys(NcOrderObj.carNor.ColtPromiseYear,Year[0]);
			waitForAjax();
			
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			waitForAjax();
		}
		break;
		
		case "Waiting for Hard Cease Date":
		{
			String modifiedDate = util.getYesterDayDatenTime("MMM-dd-yyyy");
			String[] month = modifiedDate.split("-", 0);
			String[] Date = modifiedDate.split("-", 1);
			String[] Year = modifiedDate.split("-", 2);
			
			verifyExists(NcOrderObj.carNor.ColtPromiseMonth,"Promise Month");
			sendKeys(NcOrderObj.carNor.ColtPromiseMonth,month[0]);
			waitForAjax();
			
			verifyExists(NcOrderObj.carNor.ColtPromiseDate,"Promise Date");
			sendKeys(NcOrderObj.carNor.ColtPromiseDate,Date[0]);
			waitForAjax();
			
			verifyExists(NcOrderObj.carNor.ColtPromiseYear,"Promise Year");
			sendKeys(NcOrderObj.carNor.ColtPromiseYear,Year[0]);
			waitForAjax();
						
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			waitForAjax();
		}
		break;
		
		case "Waiting for RIR Process Completion":
		{
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			waitForAjax();
		}
		break;
		
		case "Release\\Reserve LAN Range":
		{
			
			selectByVisibleText(NcOrderObj.carNor.ReleaseLanRange,"Yes","select Release Lan Range");
			
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			waitForAjax();
		}
		break;
		
		default:
			// Nothing Found it will try to complete the Task	
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			waitForAjax();
		}
	}
	
	public void NavigatebacktoAccountScreen(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		getUrl(Orderscreenurl);
	}

}