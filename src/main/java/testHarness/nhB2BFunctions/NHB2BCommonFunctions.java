package testHarness.nhB2BFunctions;

import java.util.HashMap;
import java.util.Map;
import com.relevantcodes.extentreports.LogStatus;
import baseClasses.Configuration;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.DataMiner;
import baseClasses.WebAPITestBase;
import io.restassured.response.Response;

public class NHB2BCommonFunctions extends WebAPITestBase
{
	public String path;
	public String testDataPath;
	public String endpoint;
	public String methodType;
	public String inputDataType;
	public String inputData;
	public String statusCode;
	public String apiURL;
	public Response response;
	public String sheetName;
	
	public void RunFeature(String testDataFile, String dataSheet, String scriptNo, String dataSetNo, String ScenarioName) throws Exception
	{
		//DataMiner.setTestDataToMap(testDataFile, dataSheet, scriptNo, dataSetNo, ScenarioName);
				
		String TestScenario = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Scenario_Name");
		String APIEndpoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "API_Endpoint");
		String MethodType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Method_Type");
		String InputDataType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InputData_Type");
		String InputData = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Input_Data");
		String ExpectedStatusCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Expected_StatusCode");
		String ExpectedStatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Expected_Status");
				
		apiURL = Configuration.NH_B2B_URL + APIEndpoint;
		
		Map<String, String> authhdrs = new HashMap<String, String>();
		authhdrs.put("Authorization", "Basic Y29sdDM0NTpwNTE4MlBuRA==");
	    authhdrs.put("Content-Type", "text/xml; charset=UTF-8;");		
			
		ExtentTestManager.getTest().log(LogStatus.INFO,TestScenario + "- Scenario Started");
		Report.LogInfo("Scenario Started",TestScenario + "- Scenario Started", "INFO");

		switch (MethodType) {
			case "Get":
				response = get(authhdrs, apiURL, InputDataType, InputData);
				break;
			case "Post":
				response = post(authhdrs, apiURL, InputData);
				break;
			case "Delete":
				response = delete(authhdrs, apiURL,InputDataType, InputData);
				break;
			default: 
				break;
			}	
			
		verifyStatusCode(response, Integer.parseInt(ExpectedStatusCode));
			
		compareActualWithExpected(response, ExpectedStatus);			
	        
		ExtentTestManager.endTest();			
	}			
	
}
