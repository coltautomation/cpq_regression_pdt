package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.Lanlink_DirectFiberObj;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.Lanlink_DirectFiber;

public class TS_35_Lanlink_DirectFiberTest extends SeleniumUtils {

	Lanlink_DirectFiber Fiber = new Lanlink_DirectFiber();
	APT_Login Login = new APT_Login();

	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-01", priority = 1)
	public void Lanlink_DirectFiber(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomerSelection");

			if (newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {

				Fiber.CreateCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

				String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"newCustomer");

				Fiber.selectCustomertocreateOrder(CustomerName1);

			} else if (newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {

				String CustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"existingCustomer");

				Fiber.selectCustomertocreateOrder(CustomerName);

				String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"existingCustomer");

			}

			Fiber.Verifyfields(testDataFile, dataSheet, scriptNo, dataSetNo);

			String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"existingCustomer");
			Fiber.selectCustomertocreateOrderfromleftpane(CustomerName1);

			Fiber.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			Fiber.selectServiceType(testDataFile, dataSheet, scriptNo, dataSetNo);

			Fiber.selectsubtypeunderServiceTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			Fiber.VerifyFieldsForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			Fiber.selectOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Fiber.selectServiceType(testDataFile, dataSheet, scriptNo, dataSetNo);
			Fiber.selectsubtypeunderServiceTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);
			Fiber.dataToBeEnteredOncesubservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);

			Fiber.verifysuccessmessage("Service successfully created");

			Fiber.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Fiber.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			Fiber.VerifydatenteredForServiceSubTypeSelected(testDataFile, dataSheet, scriptNo, dataSetNo);

			Fiber.EditTheservicesselected(testDataFile, dataSheet, scriptNo, dataSetNo);

			Fiber.verifysuccessmessage("Service successfully updated.");

			Fiber.syncservices();

			Fiber.manageSubnets();

			Fiber.dump_viewServicepage();

			/**
			 * Site Order
			 */

			String vpnTopology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vpnTopology");
			String circuitType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CircuitType");

			if ((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit"))) {

				ExtentTestManager.getTest().log(LogStatus.INFO,
						"Site order' Page will not display, if we select 'VPN TOpology' as 'Point-to-Point' "
								+ "and 'Circuit Type' as 'Composite Circuit' ");
				ExtentTestManager.endTest();

			} else {
				ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying fields for 'Add Site Order' page");
				Fiber.Enteraddsiteorder();
				Fiber.verifyAddsiteorderFields(testDataFile, dataSheet, scriptNo, dataSetNo);
				Fiber.Enteraddsiteorder();

				Fiber.addsiteorder(testDataFile, dataSheet, scriptNo, dataSetNo);

				Fiber.verifysuccessmessage("Site order created successfully");

				Fiber.VerifyDataEnteredForSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

				Fiber.returnbacktoviewsiteorderpage();
				Fiber.editSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				Fiber.verifysuccessmessage("Site Order successfully updated.");

			}

			// For device, Circuit creation
			String sitePreferenceType = "Null";
			String editSiteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
					dataSetNo, "editSiteOrder_sitePreferenceType");
			String siteOrder_sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"siteOrder_sitePreferenceType");

			if ((editSiteOrder_sitePreferenceType).equalsIgnoreCase("Null")) {
				sitePreferenceType = siteOrder_sitePreferenceType;
			} else {
				sitePreferenceType = editSiteOrder_sitePreferenceType;
			}

			if (((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Composite Circuit")))
					|| ((vpnTopology.equals("Point-to-Point")) && (circuitType.equals("Extended Circuit"))
							&& (sitePreferenceType.equalsIgnoreCase("Circuit")))) {

				String ServiceID = null;
				String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"Edit_serviceNumber");
				String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"serviceNumber");

				if ((Edit_serviceNumber).equalsIgnoreCase("null")) {
					ServiceID = serviceNumber;
				} else {
					ServiceID = Edit_serviceNumber;
				}
				String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"Interfacespeed");

				if ((Interfacespeed).equals("1GigE")) {

					// Overture
					Fiber.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.verifysuccessmessage("Circuit successfully created");
					Fiber.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Fiber.deleteCircuit();

					// Accedian-1G
					Fiber.addAccedianCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.verifysuccessmessage("Circuit successfully created");
					Fiber.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Fiber.deleteCircuit();

					// Atrica
					Fiber.addAtricaCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.verifysuccessmessage("Circuit successfully created");
					Fiber.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Fiber.deleteCircuit();

				}

				else if ((Interfacespeed).equals("10GigE")) {

					// Overture
					Fiber.addOvertureCircuit(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.selectInterfaceForCircuits(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.verifysuccessmessage("Circuit successfully created");
					Fiber.addOveture_PAMtest_selectRow(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.PAMtest_ForCircuitCreation(testDataFile, dataSheet, scriptNo, dataSetNo, ServiceID);
					Fiber.deleteCircuit();
				}
			} else {

				String technology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "technology");
				String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"Interfacespeed");

				String Technologyname = technology;
				if (Technologyname.equalsIgnoreCase("Actelis")) {
					boolean equipConfigurationPanel = Fiber.EquipmentCOnfigurationPanel();
					if (equipConfigurationPanel) {
						ExtentTestManager.getTest().log(LogStatus.INFO, "verify 'Add CPE Device'");
						Fiber.equipConfiguration_Actelis_AddDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
						Fiber.verifysuccessmessage("Device successfully created");

						Fiber.verifyDataEnteredFordeviceCreation_Actelis(testDataFile, dataSheet, scriptNo, dataSetNo);
						Fiber.returnbacktoviewsiteorderpage();

						Fiber.deleteDeviceFromService_EquipmentConfig_Actelis();
						Fiber.verifysuccessmessage("Actelis CPE Device successfully deleted and removed from service");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								"'Equipment Configuration' panel is not displaying");
					}

					// Actelis Configuration panel
					Fiber.verifyAddDSLAMandHSLlink();
					Fiber.AddDSLAMandHSL();
					Fiber.showInterface_ActelisConfiguuration();
					Fiber.deletInterface_ActelisConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					Fiber.successMessage_deleteInterfaceFromDevice_ActelisConfiguration();
				} else {
					ExtentTestManager.getTest().log(LogStatus.INFO,
							"Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page");
					Reporter.log(
							"Actelis panel will display only if 'Actelis' Technology is selected under 'Site order'page");
				}

				String devicename = null;
				String vendorModel = null;
				String manageAdres = null;
				String country = "";
				String siteOrderNumber = null;

				String siteOrderNumber_10G_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
						dataSetNo, "siteOrderNumber_10G_PointToPoint");
				String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
						dataSetNo, "siteOrderNumber_PointToPoint");
				String Siteordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"Siteordernumber");

				if ((vpnTopology).equals("Point-to-Point")) {
					if ((Interfacespeed).equalsIgnoreCase("1GigE")) {
						siteOrderNumber = siteOrderNumber_PointToPoint;
					} else if ((Interfacespeed).equalsIgnoreCase("10GigE")) {
						siteOrderNumber = siteOrderNumber_10G_PointToPoint;
					}

				} else {
					siteOrderNumber = Siteordernumber;
				}

				/**
				 * Equipment device creation
				 */
				
				boolean EquipmentPanel = Fiber.findPanelHeader("Equipment");
				if (EquipmentPanel) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Equipment' panel is displaying under 'view site order' page as expected");
					// Verify whether Equipment device to be created
					String deviceCreation_Equipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
							dataSetNo, "deviceCreation_Equipment");

					if ((deviceCreation_Equipment).equalsIgnoreCase("yes")) {

						ExtentTestManager.getTest().log(LogStatus.INFO,
								" Device to be created for Eqiupment as per input provided");

						ExtentTestManager.getTest().log(LogStatus.INFO,
								"Under Equipement, list of actions to be performed are: "
										+ "Verify fields for Add device" + "Add device"
										+ "Verify entered values for device" + "Edit device" + "Select Interface"
										+ "Configure Interface -- Edit Inteface"
										+ "show/Hide Interface -- Edit Interface"
										+ "Select Interface -- Add Interface to service , Remove Interface from Service"
										+ "Delete device ");

						System.out.println(
								"------For Equipment ..........Entering add cpe device, Verify CPE device, Edit CPE device----------");
						// String Interfacespeed =
						// DataMiner.fngetcolvalue(testDataFile, dataSheet,
						// scriptNo, dataSetNo, "Interfacespeed");
						String Equip_ExistingDeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet,
								scriptNo, dataSetNo, "Equip_ExistingDeviceSelection");
						String Equip_NewDeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "deviceCreation_Equipment");
						String speed = Interfacespeed;
						String existingDevice = Equip_ExistingDeviceSelection;
						String newDevice = Equip_NewDeviceSelection;

						if (speed.equals("1GigE")) {
							if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "AddExistingDevice_1G_Eqiupment");
								Fiber.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
										dataSheet, scriptNo, dataSetNo, speed);
								Fiber.verifysuccessmessage("Device successfully created");

								Fiber.verifyValuesforCPEexistingdevice_1G_Equipment();

								Fiber.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "addNewDevice_1G_Equipment");
								Fiber.verifyFieldsandAddCPEdevicefortheserviceselected_1G(testDataFile, dataSheet,
										scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully created");

								Fiber.verifydetailsEnteredforCPEdevice_1G(testDataFile, dataSheet, scriptNo, dataSetNo);

								Fiber.eDITCPEdevicedetailsentered_1G(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							}

						} else if (speed.equals("10GigE")) {

							if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "addExistingDevice_10G_Eqiupment");
								Fiber.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,
										dataSheet, scriptNo, dataSetNo, speed);
								Fiber.verifysuccessmessage("Device successfully created");

								Fiber.verifyValuesforCPEexistingdevice_10G_Equipment(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								Fiber.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							}

							else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "addNewDevice_10G_Equipment");
								Fiber.verifyFieldsandAddCPEdevicefortheserviceselected_10G(testDataFile, dataSheet,
										scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully created");

								Fiber.verifydetailsEnteredforCPEdevice_10G(testDataFile, dataSheet, scriptNo,
										dataSetNo);

								Fiber.eDITCPEdevicedetailsentered_10G(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							}
						}

						// get Device name
						String Equip_existingDevicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "Equip_existingDevicename");
						String EDIT_cpename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
								"EDIT_cpename");
						String devicename_equip = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
								"devicename_equip");

						if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							devicename = Equip_existingDevicename;
						} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
							if ((EDIT_cpename).equalsIgnoreCase("null")) {
								devicename = devicename_equip;
							} else if (!(EDIT_cpename).equalsIgnoreCase("null")) {
								devicename = EDIT_cpename;
							}
						}

						Fiber.Equip_clickonviewButton(devicename); // navigate
																	// to view
																	// device
																	// page
						devicename = Fiber.fetchdevicename_InviewPage(); // fetch
																			// Device
																			// Name
						manageAdres = Fiber.fetchManagementAddressValue_InviewPage(); // Fetch
																						// Management
																						// Address
						vendorModel = Fiber.fetchVendorModelValue(); // Fetch
																		// vendor/Model

						Fiber.testStatus();
						boolean link = Fiber.fetchDeviceInterface_viewdevicepage(devicename);
						waitForAjax();
						String CommandIPv4_Routertool = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "CommandIPv4_Routertool");

						Fiber.routerPanel(CommandIPv4_Routertool, manageAdres);

						String devicename_EquipActual = null;
						devicename = Fiber.fetchdevicename_InviewPage();
						if (devicename.contains("...")) {
							devicename_EquipActual = devicename.substring(0, 10);
						} else {
							devicename_EquipActual = devicename;
						}
						Fiber.clickOnBreadCrump(siteOrderNumber);
						Fiber.selectconfigurelinkAndverifyEditInterfacefield__Equipment(devicename_EquipActual);
						String interfaceavailability_configure = Fiber
								.EnterdataForEditInterfaceforConfigurelinkunderIntermediateEquipment(testDataFile,
										dataSheet, scriptNo, dataSetNo);
						if (interfaceavailability_configure.equalsIgnoreCase("Yes")) {
							Fiber.verifyeditedinterfacevalue(testDataFile, dataSheet, scriptNo, dataSetNo);
						}

						Fiber.clickOnBreadCrump(siteOrderNumber);
						Fiber.selectInterfacelinkforEqipment(devicename_EquipActual);

						String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "RemoveInterface_Selection");

						if ((RemoveInterface_Selection).equalsIgnoreCase("yes")) {
							Fiber.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							System.out.println("interfaces are not removed");
							ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed");
						}
						String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "AddInterface_Selection");

						if ((AddInterface_Selection).equalsIgnoreCase("yes")) {
							Fiber.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							System.out.println("Interfaces are not added");
							ExtentTestManager.getTest().log(LogStatus.PASS, "Interfaces are not removed");
						}

						String deviceName_Equip = devicename_EquipActual;
						Fiber.clickOnBreadCrump(siteOrderNumber);
						String Interfacename_forEditInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet,
								scriptNo, dataSetNo, "Interfacename_forEditInterface");

						String interfaceAvailability = Fiber.SelectShowInterfacelinkAndVerifyEditInterfacePage(
								Interfacename_forEditInterface, devicename_EquipActual);
						if (interfaceAvailability.equalsIgnoreCase("Yes")) {
							Fiber.EnterdataForEditInterfaceforShowInterfacelinkunderEquipment(testDataFile, dataSheet,
									scriptNo, dataSetNo);

						}
						String proactiveMonitorvalue = Fiber.fetchProActiveMonitoringValue();
						if (proactiveMonitorvalue.equalsIgnoreCase("Yes")) {
							Fiber.clickOnBreadCrump(siteOrderNumber);
							String csrName = Fiber.fetchCSRsiteName();
							String cityName = Fiber.fetchDeviceCityName();
							String countryName = Fiber.fetchSiteOrderCountryName();
							Fiber.clickOnAMNvalidatorLink();
							Fiber.AMNvalidator(siteOrderNumber, deviceName_Equip, csrName, cityName, countryName);
						} else {
							ExtentTestManager.getTest().log(LogStatus.INFO,
									"'AMN Validator' link do not display, as 'proactive Monitoring' checkbox is not selected ");
							ExtentTestManager.endTest();
						}

						Fiber.clickOnBreadCrump(siteOrderNumber);
						Fiber.deleteDeviceFromServiceForequipment(deviceName_Equip);
						Fiber.successMessage_deleteFromService();

					} else {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Equipment device is not created as expected");
						System.out.println("Equipment device is not created as expected");
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.INFO,
							" 'Equipment' panel is not displaying under 'view site order' page");
					System.out.println(" 'Equipment' panel is not displaying under 'view site order' page");
				}

				String devicename_intEquip = null;
				String country_intEquip = "";
				String manageAdres_intEqiup = null;
				String vendorModel_intEqiup = null;

				boolean IntermediateEquipmentPanel = Fiber.findPanelHeader("Intermediate Equipment");
				if (IntermediateEquipmentPanel) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Intermediate Equipment' panel is displaying under 'view site order' page as expected");

					String deviceCreation_IntermediateEquipment = DataMiner.fngetcolvalue(testDataFile, dataSheet,
							scriptNo, dataSetNo, "deviceCreation_IntermediateEquipment");

					if ((deviceCreation_IntermediateEquipment).equalsIgnoreCase("yes")) {
						ExtentTestManager.getTest().log(LogStatus.INFO,
								" Device to be created for Intermediate Eqiupment as per input provided");
						ExtentTestManager.getTest().log(LogStatus.INFO,
								"Under Intermediate Equipement, list of actions to be performed are: "
										+ "Verify fields for Add device" + "Add device"
										+ "Verify entered values for device" + "Edit device" + "Select Interface"
										+ "show/Hide Interface -- Edit Interface"
										+ "Select Interface -- Add Interface to service , Remove Interface from Service"
										+ "Delete device ");
						String IntEquip_existingdeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet,
								scriptNo, dataSetNo, "IntEquip_existingdeviceSelection");
						String IntEquip_newdeviceSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "IntEquip_newdeviceSelection");

						String speed = Interfacespeed;
						String existingDevice = IntEquip_existingdeviceSelection;
						String newDevice = IntEquip_newdeviceSelection;

						Fiber.clickOnBreadCrump(siteOrderNumber_PointToPoint);

						if (speed.equals("1GigE")) {
							if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								ExtentTestManager.getTest().log(LogStatus.INFO,
										"selectExistingDevice_1G_IntermediateEquipment");
								Fiber.addDevice_IntEquipment();
								Fiber.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(testDataFile,
										dataSheet, scriptNo, dataSetNo);

								Fiber.verifyValuesforCPEexistingdevice_1G_intEquipment(testDataFile, dataSheet,
										scriptNo, dataSetNo);

								Fiber.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
								Fiber.addDevice_IntEquipment();
								Fiber.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(testDataFile,
										dataSheet, scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully created");

								Fiber.verifyCPEdevicedataenteredForIntermediateEquipment_1G(testDataFile, dataSheet,
										scriptNo, dataSetNo);

								Fiber.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							}
						} else if (speed.equals("10GigE")) {
							if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
								ExtentTestManager.getTest().log(LogStatus.INFO,
										"addExistingDevice_10G_IntermediateEquipment");
								Fiber.addDevice_IntEquipment();
								Fiber.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(testDataFile,
										dataSheet, scriptNo, dataSetNo);
								Fiber.verifyValuesforCPEexistingdevice_10G_intEquipment(testDataFile, dataSheet,
										scriptNo, dataSetNo);

								Fiber.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							} else if (existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
								ExtentTestManager.getTest().log(LogStatus.INFO,
										"addNewDevice_10G_IntermediateEquipment");
								Fiber.addDevice_IntEquipment();
								Fiber.selectTechnology(testDataFile, dataSheet, scriptNo, dataSetNo);
								Fiber.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(testDataFile,
										dataSheet, scriptNo, dataSetNo);
								Fiber.verifysuccessmessage("Device successfully created");

								Fiber.verifyCPEdevicedataenteredForIntermediateEquipment_10G(testDataFile, dataSheet,
										scriptNo, dataSetNo);

								Fiber.EDITCPEdevice_IntermediateEquipment_10G(testDataFile, dataSheet, scriptNo,
										dataSetNo);
								Fiber.verifysuccessmessage("Device successfully updated");
							}
						}

						// Get device name
						if (existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							String intEquip_existingDeviceValue = DataMiner.fngetcolvalue(testDataFile, dataSheet,
									scriptNo, dataSetNo, "intEquip_existingDeviceValue");

							devicename_intEquip = intEquip_existingDeviceValue;
						} else if (existingDevice.equalsIgnoreCase("no") && newDevice.equalsIgnoreCase("Yes")) {
							String EDIT_Intequip_cpe_deviecname = DataMiner.fngetcolvalue(testDataFile, dataSheet,
									scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");

							if ((EDIT_Intequip_cpe_deviecname).equalsIgnoreCase("null")) {
								String device_intEquip_name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
										dataSetNo, "device_intEquip_name");

								devicename_intEquip = device_intEquip_name;
							} else if ((EDIT_Intequip_cpe_deviecname).equalsIgnoreCase("null")) {
								devicename_intEquip = EDIT_Intequip_cpe_deviecname;
							}
						}

						// navigate to view device page
						Fiber.IntEquip_clickonviewButton(devicename_intEquip);
						devicename_intEquip = Fiber.fetchdevicename_InviewPage(); // Device
																					// Name
						manageAdres_intEqiup = Fiber.fetchManagementAddressValue_InviewPage(); // Management
																								// Address
						vendorModel_intEqiup = Fiber.fetchVendorModelValue(); // Vendor/Model
						country_intEquip = Fiber.fetchCountryValue_InviewPage(); // Country

						// Perform fetch from Interface
						Fiber.testStatus();
						boolean link = Fiber.fetchDeviceInterface_viewdevicepage(devicename_intEquip);
						waitForAjax();
						String CommandIPv4_Routertool = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "CommandIPv4_Routertool");

						Fiber.routerPanel(CommandIPv4_Routertool, manageAdres_intEqiup);

						String devicename_intEquipActual = null;
						devicename_intEquip = Fiber.fetchdevicename_InviewPage();

						if (devicename_intEquip.contains("...")) {
							devicename_intEquipActual = devicename_intEquip.substring(0, 10);
						} else {
							devicename_intEquipActual = devicename_intEquip;
						}
						String devicename_IntEquipment = devicename_intEquipActual;

						Fiber.clickOnBreadCrump(siteOrderNumber);
						Fiber.selectInterfacelinkforIntermediateEqipment(devicename_intEquipActual);
						String RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "RemoveInterface_Selection");

						if ((RemoveInterface_Selection).equalsIgnoreCase("yes")) {
							Fiber.SelectInterfacetoremovefromservice(testDataFile, dataSheet, scriptNo, dataSetNo);
						} else {
							System.out.println("interfaces are not removed");
						}
						String AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
								dataSetNo, "AddInterface_Selection");

						if ((AddInterface_Selection).equalsIgnoreCase("yes")) {
							Fiber.SelectInterfacetoaddwithservcie(testDataFile, dataSheet, scriptNo, dataSetNo);
						}

						Fiber.clickOnBreadCrump(siteOrderNumber);
						String Interfacename_forEditInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet,
								scriptNo, dataSetNo, "Interfacename_forEditInterface");

						Fiber.SelectShowInterfacelink_IntermediateequipmentAndVerifyEditInterfacePage(
								Interfacename_forEditInterface, devicename_intEquipActual);
						Fiber.EnterdataForEditInterfaceforShowInterfacelinkunderIntermediateEquipment(testDataFile,
								dataSheet, scriptNo, dataSetNo);
						Fiber.deleteDeviceFromServiceForIntermediateequipment(testDataFile, dataSheet, scriptNo,
								dataSetNo);
						Fiber.successMessage_deleteFromService();
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Intermediate Equipment' panel not displaying under 'view site order' page");
					Reporter.log(" 'Intermediate Equipment' panel not displaying under 'view site order' page");
				}

				String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"Edit_serviceNumber");
				String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
						"serviceNumber");

				String ServiceID = null;
				if ((Edit_serviceNumber).equalsIgnoreCase("null")) {
					ServiceID = serviceNumber;
				} else {
					ServiceID = Edit_serviceNumber;
				}
				Fiber.returnbacktoviewsiteorderpage();

				Fiber.pamTest(ServiceID);

				Fiber.deleteSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			}
			String Edit_serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"Edit_serviceNumber");
			String serviceNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
					"serviceNumber");

			String ServiceID = null;
			if ((Edit_serviceNumber).equalsIgnoreCase("null")) {
				ServiceID = serviceNumber;
			} else {

				ServiceID = Edit_serviceNumber;
			}
			Fiber.clickOnBreadCrump(ServiceID);
			Fiber.deleteService();

		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}

}
