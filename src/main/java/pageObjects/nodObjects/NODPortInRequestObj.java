package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODPortInRequestObj {

	
	public static class portIn
	{ 
		//public static final String customerTypeRadioBtn ="@xpath=//input[@id='Customer_Type']";
		public static final String QuickLinkDrpDwn="@xpath=//div[contains(@class,'ng-select-container')]//div[contains(text(),'Quick Links')]"; 
		public static final String QuickLinkDrpDwnArw="@xpath=//body/app-root[1]/app-my-orders[1]/div[1]/div[1]/div[1]/div[2]/app-profile[1]/div[1]/form[1]/div[1]/ng-select[1]/div[1]/span[1]";
		public static final String portInLink ="@xpath=//label[contains(text(),'Port-in')]";
		public static final String portInPage ="@xpath=//*[contains(text(),' Port-in ')]";
		public static final String ServiceType ="@xpath=//select[@id='serviceType']";
		public static final String CustomerName ="@xpath=//input[@id='customerName']";
		public static final String BuildingNumber ="@xpath=//input[@id='cadBuildingNumber']";
		public static final String StreetName ="@xpath=//input[@id='cadstreetAddress']";
		public static final String CityOrTown ="@xpath=//input[@id='cadcitytown']";
		public static final String PostalCode ="@xpath=//input[@id='cadpostCode']";
		public static final String NewBuildingNumber ="@xpath=//input[@id='newBuildingNumber']";
		public static final String NewStreetName ="@xpath=//input[@id='newstreetAddress']";
		public static final String NewCityOrTown ="@xpath=//input[@id='newcitytown']";
		public static final String NewPostalCode ="@xpath=//input[@id='newpostCode']";
		public static final String ValidateAddressBtn ="@xpath=//button[contains(text(),'Validate Address')]";
		public static final String AddressValidationMsg ="@xpath=//h5[contains(text(),'Provided address has validated successfully')]";
		public static final String ProvideNumbersBtn ="@xpath=//button[contains(text(),'Provide Numbers')]";
		public static final String NumberForPortIn ="@xpath=//textarea[@id='null']";
		public static final String ValidateNumbers ="@xpath=//button[contains(text(),'Validate Numbers')]";
		public static final String NumberEnrichmentBtn ="@xpath=//button[contains(text(),'Number Enrichment')]";
		public static final String MainBillingNumberChkBox ="@xpath=//label[@for='headerEnrichmentCheck']";
		public static final String MainBillingNumberChkBoxNL ="@xpath=//label[@for='keepCurrentSetting']";
		public static final String SingleAndMultiLine ="@xpath=//select[@id='singleOrMulti']";
		public static final String ProvidePortingDocBtn ="@xpath=//button[contains(text(),'Provide Porting Documents')]";
		public static final String MainBillingNo ="@xpath=//input[@formcontrolname='mainBilling']";
		public static final String ChooseFileBtn ="@xpath=//input[@id='portingFormBase64']";
		public static final String currentOperator ="@xpath=//select[@id='currentProvider']";
		public static final String currentOperatorOption ="@xpath=//option[contains(text(),'operator')]";
		public static final String ProvideContactDetailsBtn ="@xpath=//button[contains(text(),'Provide your contact Details')]";
		public static final String ReviewYourOrderBtn ="@xpath=//button[contains(text(),'Review your Order')]";
		public static final String ConfirmYourOrderBtn ="@xpath=//button[contains(text(),'Confirm your Order')]";
		public static final String PortInDate ="@xpath=//input[@id='requestPortDate']";
		public static final String PortInWindow ="@xpath=//ng-select[@id='requestPortTimeDD']//input[@role='combobox']";
		public static final String PortInConfirmationMsg ="@xpath=//div[@id='portIn-l-7']//div[@class='row ng-star-inserted'][1]";
	//	public static final String SuccessMessage ="@xpath=//h6[contains(text(),'Your order was raised successfully.')]";
		public static final String SuccessMessage ="@xpath=//*[contains(text(),'Your order was raised successfully.')]";
	//	public static final String EmailNotification ="@xpath=//h3[contains(text(),'You will receive an email confirmation shortly at ')]";
		public static final String EmailNotification ="@xpath=//*[contains(text(),'You will receive an email confirmation shortly at ')]";	
	//	public static final String portInOrderId ="@xpath=//h3[contains(text(),'Order ID: ')]/a";
		public static final String portInOrderId ="@xpath=//*[contains(text(),'Order ID: ')]/a";	
		public static final String CheckMyOrder ="@xpath=//button[contains(text(),'Check My Order(s)')]";
		public static final String orderStatusVal="@xpath=(//*[@tabulator-field='currentStatus'])[2]";
		//public static final String customerTypeRadioBtn ="@xpath=//input[@id='CustomerType']";
		public static final String customerTypeRadioBtn ="@xpath=//label[contains(text(),'Customer')]";
		//public static final String customerTypeRadioBtn ="@xpath=//label[contains(text(),'residentialPortIn_NL')]";
		
		public static final String SearchTxtFld="@xpath=//*[@class='input-group']//*[@placeholder='Search']"; 
		public static final String SearchIcon="@xpath=//*[@class='input-group']//button[@type='button']"; 
	
		
	}
	
	public static class QuickNote
	{ 
		public static final String OrderRow="@xpath=//*[@class='tabulator-row tabulator-selectable tabulator-row-odd']"; 
		public static final String ActionOption="@xpath=(//div[@tabulator-field='actions'])[2]"; 
		public static final String QuickNoteAction="@xpath=//li[contains(text(),'QuickNote')]"; 
	//	public static final String AddNotePopUp="@xpath=//h4[contains(text(),'Add Notes')]"; 
		public static final String AddNotePopUp="@xpath=//*[contains(text(),'Add Notes')]"; 

		
		public static final String AddNoteTxtBox="@xpath=//textarea[@id='comment']"; 
		public static final String AddNoteBtn="@xpath=//button[contains(text(),'Add Notes')]"; 

	//	public static final String SuccessMsg="@xpath=//h6[contains(text(),'Thank you for your order. Your order is raised suc')]"; 
		public static final String SuccessMsg="@xpath=//*[contains(text(),'Thank you for your order. Your order is raised suc')]"; 	
		public static final String OrderId="@xpath=//a[@_ngcontent-mhd-c179='']"; 

		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}