package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.exploreFunctions.ExploreLoginPage;

public class TS_20_SubFunctions_Coverage extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage Login = new C4CLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	RecordVideo record = new RecordVideo();
	
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testSubFunctionsCoverage(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			Login.C4CLogin();
			
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String[] sub_function = "Reject|Accept".split("\\|");
			String Product_Name=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			String Flow_Type=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
			String sResult;
			for (int i=0; i < sub_function.length; i++) {
			
		//		Calling the below method to add the product in CPQ
				sResult = AddProduct.AddProductToQuote(Product_Name);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
		//		calling the below method to enter the site address
				sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
		//		calling the below method to configure the product
				sResult = AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
		//      Wave Product Flow for Manual Offnet
				if(Product_Name.equalsIgnoreCase("Wave")) {
					
		//			calling the below method to verify quote stage and status
					sResult = AddProduct.verifyQuoteStage("Created");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
		//			calling the below method to add Legal and Technical Contact Info					
					sResult = Approvals.SwitchCPQUser("SE_User", Quote_ID);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
		//			calling the below method reconfigure the product
					sResult = AddProduct.clickProductConfigurationBtn(Product_Name);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
		//			calling the below method to configure the product
					sResult = AddProduct.offnetandNearNetConfigurationWave(testDataFile, dataSheet, scriptNo, dataSetNo);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
						
		//			calling the below method to update and save the product in cpq
					sResult = AddProduct.updateSaveProductCPQ("Save");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }		
				}
				
				
				if (Flow_Type.equalsIgnoreCase("ManualOffnet")) {
				
			//		calling the below method to verify quote stage and status
					sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
			//		calling the below method to verify quote stage and status
					sResult = AddProduct.logoutCPQ("Main");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
			//		To Process Offnet Cost in Explore
					if (sub_function[i].equalsIgnoreCase("Reject")) {
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						sResult = ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					} else {
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.EXPLORE_URL);
						sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
						if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					}
					
				}
				
				else if (Flow_Type.equalsIgnoreCase("ManualNearnet")) {
					//		calling the below method to verify quote stage and status
							sResult = AddProduct.verifyQuoteStage("Waiting for BCP");
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							
					//		calling the below method to verify quote stage and status
							sResult = AddProduct.logoutCPQ("Main");
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							
//							To Process Offnet Cost in Explore
							if (sub_function[i].equalsIgnoreCase("Reject")) {
								close();
								openBrowser(g.getBrowser());		
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.nearnetExploreRejectOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							} else {
								close();
								openBrowser(g.getBrowser());	
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.nearnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							}
							
						}
				
				else if (Flow_Type.equalsIgnoreCase("ManualDSL")) {
					
					//		calling the below method to verify quote stage and status
							sResult = AddProduct.verifyQuoteStage("Waiting for 3rd Party");
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							
					//		calling the below method to verify quote stage and status
							sResult = AddProduct.logoutCPQ("Main");
							if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							
//							To Process Offnet Cost in Explore
							if (sub_function[i].equalsIgnoreCase("Reject")) {
								close();
								openBrowser(g.getBrowser());		
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.offnetExploreRejectAction(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							} else {
								close();
								openBrowser(g.getBrowser());		
								openurl(Configuration.EXPLORE_URL);
								sResult = ExpConfig.offnetExploreOperations(testDataFile, dataSheet, scriptNo, dataSetNo);
								if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
							}	
							
						}
				
//				Calling the Below method to perform C4C Login	
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.C4C_URL);
				Login.C4CLogin();
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below method to navigate to quotes from c4c main page
				AddProduct.navigateQuotesFromHomepage();
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below method to search the quote from c4c
				sResult = AddProduct.searchQuoteC4C(Quote_ID);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }	
				
//				calling the below method reconfigure the product
				sResult = AddProduct.clickProductConfigurationBtn(Product_Name);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				
//				calling the below method to update and save the product in cpq
				if (sub_function[i].equalsIgnoreCase("Reject")) {
					AddProduct.markPartialSave(Product_Name); }
				else
				{
					AddProduct.updateSaveProductCPQ("Save");
				 }
						
				
				if (sub_function[i].equalsIgnoreCase("Reject")) {
			//		calling the below method to verify quote stage and status
					//sResult = AddProduct.verifyQuoteStage("No Bid");
					//if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
//					calling the below method to delete the product
					sResult = Approvals.deleteProductCPQ(Product_Name);
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
					
				} else {
							
					verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
					String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
					System.out.println("quoteStage: "+quoteStage);
					if (quoteStage.equalsIgnoreCase("SE Required")) 
					{
						AddProduct.verifyQuoteStage("SE Required");
						AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo); 
						//Login with CPQ SE user
						close();
						openBrowser(g.getBrowser());		
						openurl(Configuration.CPQ_URL);
						CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
						CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
						AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
						Approvals.SwitchCPQUser("Sales_User", Quote_ID);
					}
					//calling the below method to verify quote stage and status
					sResult = AddProduct.verifyQuoteStage("Priced");
					if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}
				continue;
			}
			
			AddProduct.discountingProcess(testDataFile, dataSheet ,scriptNo, dataSetNo);
			AddProduct.dealPricePLTabEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Deal Pricing Review");					
			
			Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Quote_ID");
			Approvals.SwitchCPQUser("DP_User", Quote_ID);
			
			AddProduct.assignQuoteToDPUser(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.dealPricingApproval("Reject");	
			
			CPQLogin.CPQLogout();
				
			close();
			openBrowser(g.getBrowser());			
			openurl(Configuration.CPQ_URL);
			CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
			CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.verifyQuoteStage("Approval Denied");
			
			AddProduct.reviseQuote();	
			AddProduct.verifyQuoteStage("Priced");
			AddProduct.clearQuoteDiscount();
			
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			if(Product_Name.equalsIgnoreCase("CpeSolutionsSite")) 
			{
			String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
			Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
			} else {
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(Product_Name, BCN, "BCN NRC");
			}
			
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.logoutCPQ("Main");
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
		record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}		

}
