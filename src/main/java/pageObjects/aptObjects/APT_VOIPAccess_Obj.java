package pageObjects.aptObjects;

public class APT_VOIPAccess_Obj {
	public static class VOIPAccess {

		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";

		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";

		public static final String mcslink = "@xpath=//a[contains(text(),'Manage Customer')]";
		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";
		public static final String createorderlink = "@xpath=//li[text()='Create Order/Service']";

		// Create Customer in Customer creation page
		public static final String createcustomer_header = "@xpath=//p[text()='Customer']";
		public static final String nametextfield = "@xpath=//*[text()='Legal Customer Name']/following-sibling::input";
		public static final String maindomaintextfield = "@xpath=//*[text()='Main Domain']/following-sibling::input";
		public static final String country = "@xpath=//*[text()='Country']/following-sibling::div//input";
		public static final String ocntextfield = "@xpath=//*[text()='OCN']/following-sibling::input";
		public static final String referencetextfield = "@xpath=//*[text()='Reference']/following-sibling::input";
		public static final String technicalcontactnametextfield = "@xpath=//*[text()='Technical Contact Name']/following-sibling::input";
		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";
		public static final String emailtextfield = "@xpath=//*[text()='Email']/following-sibling::input";
		public static final String phonetextfield = "@xpath=//*[text()='Phone']/following-sibling::input";
		public static final String faxtextfield = "@xpath=//*[text()='Fax']/following-sibling::input";
		public static final String enablededicatedportalcheckbox = "@xpath=//*[text()='Enable Dedicated Portal']/following-sibling::div//input";
		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String customercreationsuccessmsg = "@xpath=//span[contains(text(),'Customer successfully created.')]";
		public static final String clearbutton = "@xpath=//span[text()='Clear']";
		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";
		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// Verify warning messages -- ="@xpath=
		public static final String customernamewarngmsg = "@xpath=//div[text()='Legal Customer Name']";
		public static final String countrywarngmsg = "@xpath=//div[text()='Country']";
		public static final String ocnwarngmsg = "@xpath=//div[text()='OCN']";
		public static final String typewarngmsg = "@xpath=//div[text()='Type']";
		public static final String emailwarngmsg = "@xpath=//div[text()='Email']";
		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";
		public static final String sidwarngmsg = "@xpath=//input[@id='serviceIdentification']/parent::div//div";
		public static final String DeleteServiceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete this item?']";

		// Verify Customer details panel info in view service page -- ="@xpath=
		public static final String CustomerDetailsHeader = "@xpath=//div[text()='Customer Details']";
		public static final String Name_Text = "@xpath=(//div//label[@for='name'])[1]";
		public static final String Name_Value = "@xpath=//div[label[text()='Legal Customer Name']]/following-sibling::div";
		public static final String MainDomain_Text = "@xpath=(//div//label[@for='name'])[2]";
		public static final String MainDomain_Value = "@xpath=//label[text()='Main Domain']/parent::div/parent::div//div[2]";
		public static final String Country_Text = "@xpath=(//div//label[@for='name'])[3]";
		public static final String Country_Value = "@xpath=//label[text()='Country']/parent::div/parent::div//div[2]";
		public static final String OCN_Text = "@xpath=(//div//label[@for='name'])[4]";
		public static final String OCN_Value = "@xpath=//label[text()='OCN']/parent::div/parent::div//div[2]";
		public static final String Reference_Text = "@xpath=(//div//label[@for='name'])[5]";
		public static final String Reference_Value = "@xpath=//label[text()='Reference']/parent::div/parent::div//div[2]";
		public static final String Type_Text = "@xpath=(//div//label[@for='name'])[6]";
		public static final String Type_Value = "@xpath=//label[text()='Type']/parent::div/parent::div//div[2]";
		public static final String TechnicalContactName_Text = "@xpath=(//div//label[@for='name'])[7]";
		public static final String TechnicalContactName_Value = "@xpath=//label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";
		public static final String Email_Text = "@xpath=(//div//label[@for='name'])[8]";
		public static final String Email_Value = "@xpath=//label[text()='Email']/parent::div/parent::div//div[2]";
		public static final String Phone_Text = "@xpath=(//div//label[@for='name'])[9]";
		public static final String Phone_Value = "@xpath=//label[text()='Phone']/parent::div/parent::div//div[2]";
		public static final String Fax_Text = "@xpath=(//div//label[@for='name'])[10]";
		public static final String Fax_Value = "@xpath=//label[text()='Fax']/parent::div/parent::div//div[2]";

		// create order/service page -- ="@xpath=
		// public static final String nametextfield
		// ="@xpath=//input[@id='customerSearch']";
		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";
		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";
		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";
		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";
		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";
		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";
		public static final String country1 = "@xpath=//div[div[label[text()='Country']]]/div[2]";
		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";
		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";
		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";
		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";
		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";
		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";
		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";
		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";
		public static final String useractionbutton = "@xpath=//button[@id='dropdown-basic-button']";
		public static final String adduserbutton = "@xpath=//a[contains(text(),'Add')]";

		// Order Creation in Order/service panel view customer page
		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";
		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";
		public static final String selectorderswitch = "@xpath=(//div[@class='react-switch-bg'])[2]";
		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";
		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";
		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";
		public static final String newordertextfield = "@xpath=//*[text()='Order/Contract Number(Parent SID)']/following-sibling::input";
		public static final String newrfireqtextfield = "@xpath=//*[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String order_contractnumber_warngmsg = "@xpath=//label[contains(text(),'Order/Contract Number(Parent SID)')]/following-sibling::span[3]";
		public static final String servicetype_warngmsg = "@xpath=//div[label[contains(text(),'Service Type')]]/following-sibling::span";

		public static final String servicetypetextfield = "@xpath=//div[div[label[text()='Service Type']]]//input";
		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";
		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String changeorder_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// Service Creation Form in Service creation page -- ="@xpath=
		public static final String createorderservice_header = "@xpath=//div//p[text()='Create Order / Service']";
		public static final String EditService_header = "@xpath=//p[contains(text(),'Edit Service')]";
		public static final String serviceidentificationtextfield = "@xpath=//input[@id='serviceIdentification']";
		public static final String ServiceIdentificationTextlabel = "@xpath=//label[contains(text(),'Service Identification')]";
		public static final String servicetypevalue = "@xpath=//div[div[label[text()='Service Type']]]//div[2]";
		public static final String servicetype_value = "@xpath=//div[label[text()='Service Type']]/following-sibling::div";
		public static final String ResellerCodetextfield = "@xpath=//input[@id='resellerCode']";
		public static final String remarktextarea = "@xpath=//textarea[contains(@name,'remark')]";
		public static final String remarktextareavalue = "@xpath=//div[label[text()='Remark']]//textarea";
		public static final String Email_header = "@xpath=//div//label[text()='Email']";
		public static final String emailtextfield1 = "@xpath=//input[@id='email']";
		public static final String emailtextfieldvalue = "@xpath=//div[label[text()='Email']]//input";
		public static final String emailarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phonecontacttextfield = "@xpath=//input[@id='phoneContact']";
		public static final String phonecontacttextfieldvalue = "@xpath=//div[label[text()='Phone Contact']]//input";
		public static final String phonearrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";

		// Management Options in Service creation page -- ="@xpath=
		public static final String PackageDropdownDisabled = "@xpath=(//input[@placeholder='Select ...'])[1]";
		public static final String ManageServicecheckbox = "@xpath=//input[@id='managedService']";
		public static final String SyslogEventViewcheckbox = "@xpath=//input[@id='syslogEventView']";
		public static final String ServiceStatusViewcheckbox = "@xpath=//input[@id='serviceStatusView']";
		public static final String RouterConfigurationViewcheckbox = "@xpath=//input[@id='routerConfigView']";
		public static final String PerformanceReportingcheckbox = "@xpath=//div[label[text()='Performance Reporting']]/div/input";
		public static final String ProactiveNotificationcheckbox = "@xpath=//input[@id='proactiveMonitoring']";
		public static final String NotificationManagementTeamDropdown = "@xpath=(//input[@placeholder='Select ...'])[2]";
		public static final String DialUserAdministrationcheckbox = "@xpath=//input[@id='dialUserAdministration']";

		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String OKbutton_ServiceCreation = "@xpath=//span[contains(text(),'OK')]";
		public static final String servicecreationmessage = "@xpath=//div[@role='alert']//span";

		// public static final String cancelbutton
		// ="@xpath=//button[@type='submit']//span[text()='Cancel']";

		// Search order page
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";
		public static final String viewLink_SearchOrderPage = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		// Customer panel in view service page
		public static final String customerheader = "@xpath=//div[text()='Customer']";
		public static final String customerpanel_customername = "@xpath=(//label[text()='Customer Name']/parent::div/div//input)[1]";
		public static final String managecustomerheader = "@xpath=//div[@class='heading-green-row row']//span[text()='Manage Customer In OSP']";
		public static final String customer_ocn = "@xpath=//div[@class='position-relative form-group']//label[text()='OCN']/following-sibling::input";
		public static final String customer_country = "@xpath=(//label[text()='Country']/parent::div//div//input)[1]";
		public static final String customername = "@xpath=(//label[text()='Customer Name']/parent::div//div//input)[1]";
		public static final String defaultcheckbox = "@xpath=//label[@class='form-label capitalize']//input[@value='defcustprof']";
		public static final String configurecheckbox = "@xpath=//label[@class='form-label capitalize']//input[@value='configcustprof']";
		public static final String CustomerpanelActionDropdown = "@xpath=//div[contains(text(),'Customer')]/following-sibling::div/div//button[text()='Action']";
		public static final String addcustomer_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String customername_columnvalue = "@xpath=//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@col-id='trailCustomerName']";
		public static final String editcustomer_successmsg = "@xpath=(//div[@role='alert']//span)[1]";
		public static final String viewpage_editcustomer = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit Customer']";
		public static final String viewpage_deletecustomer = "@xpath=//div[@class='dropdown-menu show']//a[text()='Delete Customer']";
		public static final String deletealertclose = "@xpath=//div[@class='modal-content']//button//span[text()='�']";
		public static final String customerpanel_customernamevalue = "@xpath=//label[text()='Customer Name']/parent::div/div//span";
		public static final String customerpanel_customernamecolumntext = "@xpath=//div[text()='Customer']/parent::div/following-sibling::div//span[text()='Customer Name']";
		public static final String customercountry_disabled = "@xpath=(//label[text()='Country']/parent::div//div//div)[1]";
		public static final String customername_disabled = "@xpath=(//label[text()='Customer Name']/parent::div//div//div)[1]";

		public static final String customer_countryvalue = "@xpath=//label[text()='Country']/parent::div/following-sibling::div";
		public static final String customer_customernamevalue = "@xpath=//label[text()='Customer Name']/parent::div/following-sibling::div";
		public static final String customername_selectedtext = "@xpath=(//label[text()='Customer Name']/parent::div//div//span)[1]";

		// Users panel public in view service page -- ="@xpath=

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";
		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";
		public static final String EmailColumn = "@xpath=//div[@col-id='email']";
		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";
		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";
		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";
		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		// New User creation page from view service page -- ="@xpath=
		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";
		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";
		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String UserName = "@xpath=(//div[@class='position-relative form-group'])[1]//input[@id='userName']";
		public static final String FirstName = "@xpath=(//div[@class='position-relative form-group'])[2]//input[@id='firstName']";
		public static final String SurName = "@xpath=(//div[@class='position-relative form-group'])[3]//input[@id='surname']";
		public static final String PostalAddress = "@xpath=(//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";
		public static final String Email = "@xpath=(//div[@class='position-relative form-group'])[5]//input[@id='email']";
		public static final String Phone = "@xpath=(//div[@class='position-relative form-group'])[6]//input[@id='phone']";
		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";
		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";
		public static final String OkButton = "@xpath=//button[@type='submit']//span[text()='Ok']";
		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()='Delete']";
		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String userspanel_header = "@xpath=//div[text()='Users']";
		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";
		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";
		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";
		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";
		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";
		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";

		// order panel - view service page -- ="@xpath=
		public static final String orderpanelheader = "@xpath=//div[text()='Order']";
		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath=(//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[1])[1]";
		public static final String RFIRFQIPVoiceLineNumberText = "@xpath=//label[contains(text(),'RFI/RFQ/IP Voice Line Number')]";

		public static final String changeorder_dropdownlist = "@xpath=//div[@class='sc-bxivhb kqVrwh']/div";
		public static final String changeorder_dropdownvalue = "@xpath=(//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//div/div/div/div)[1]";

		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";
		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";
		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";
		public static final String orderupdate_successmsg = "@xpath=//div[@role='alert']//span[text()='Order successfully updated']";
		public static final String ChangeOrder_successmsg = "@xpath=//div[@role='alert']//span[text()='Order successfully  changed.']";

		// service panel fields and actions list in view service page --
		// ="@xpath=
		public static final String servicepanel_header = "@xpath=//div[text()='Service']";
		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";
		public static final String servicepanel_serviceidentificationvalue = "@xpath=//div[div[label[contains(text(),'Service Identification')]]]//div[2]";
		public static final String servicepanel_servicetypevalue = "@xpath=//div[div[label[contains(text(),'Service Type')]]]//div[2]";
		public static final String servicepanel_ResselerCodevalue = "@xpath=//div[div[label[contains(text(),'Reseller Code')]]]//div[2]";
		public static final String servicepanel_Emailvalue = "@xpath=(//div[div[label[contains(text(),'Email')]]]//div)[4]";
		public static final String servicepanel_PhoneContactvalue = "@xpath=(//div[div[label[contains(text(),'Phone Contact')]]]//div)[2]";
		public static final String servicepanel_remarksvalue = "@xpath=(//div[div[label[contains(text(),'Remark')]]]//div)[2]";
		public static final String serviceupdate_successmsg = "@xpath=//div[@role='alert']//span[text()='Service successfully updated']";

		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		public static final String EditLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";
		public static final String DeleteLink = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";
		public static final String ShowInfovistaReportLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show Infovista Report']";
		public static final String ShowNewInfovistaReportLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Show New Infovista Report']";
		public static final String ManageSubnetsIpv6Link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Subnets Ipv6']";
		public static final String ManageSubnetsIpv6_header = "@xpath=//div[@class='modal-title h4']";
		public static final String ManageSubnetsIpv6_Confirmationmessage = "@xpath=//div/span[text()='There are no subnets to be managed for this service.']";
		public static final String ManageSubnetsIpv6_action = "@xpath=(//div[div[button[@id='dropdown-basic-button' and @aria-haspopup='true']]]//div)[14]";
		public static final String ManageSubnetsIpv6_ManageIPs = "@xpath=//div[a[text()='Delete']]//a[text()='Manage IPs']";
		public static final String ManageSubnetsIpv6__Delete = "@xpath=//div[a[text()='Manage IPs']]//a[text()='Delete']";
		public static final String ManageSubnetsIpv6_popupcloseX = "@xpath=//button[span[text()='Close']]";
		public static final String DumpLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Dump']";

		// Dump -- ="@xpath=
		public static final String Editservice_actiondropdown = "@xpath=//div[div[text()='Service']]//button[text()='Action']";
		public static final String Editservice_Dumplink = "@xpath=//a[text()='Dump']";
		public static final String dumpPage_header = "@xpath=//div[@class='modal-header']";
		public static final String dumpheaderName = "@xpath=//div[@class='modal-header']/div";
		public static final String dumpMessage_body = "@xpath=//div[@class='modal-body']/div";
		public static final String dump_xButton = "@xpath=//span[contains(text(),'�')]";

		public static final String manageSubnet_IPv6 = "@xpath=//a[text()='Manage Subnets Ipv6']";
		public static final String manageSubnet_successMSG = "@xpath=//div[div[@class='modal-header']]//div[@role='alert']/span";

		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";

		// Status Panel -- ="@xpath=
		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";
		public static final String status_ordername = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String status_servicename = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String status_servicetype = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String status_servicedetails = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String status_currentstatus = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='currentStatus']";
		public static final String status_modificationtime = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='mTime']";
		public static final String statuslink = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell']//a[text()='Status']";
		// Synchronization Panel -- ="@xpath=
		public static final String sync_ordername = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String sync_servicename = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String sync_servicetype = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String sync_servicedetails = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String sync_status = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='syncStatus']";
		public static final String synchronizelink = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell']//a[text()='Synchronize']";
		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";
		public static final String changestatus_dropdown = "@xpath=//label[text()='Change Status']/parent::div//input";
		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";
		public static final String servicestatushistory = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";

		public static final String portalaccess_header = "@xpath=//label[text()='Portal Access']";

		// Management Options panel text and text value in view service page --
		// ="@xpath=
		public static final String managementoptions_header = "@xpath=//div[text()='Management Options']";
		public static final String managementOptionsPanelheader = "@xpath=//div[div[contains(text(),'Management Options')]]";
		public static final String ManageService_text = "@xpath=//label[text()='Managed Service']";
		public static final String ManageService_value = "@xpath=//div[label[text()='Managed Service']]/following-sibling::div";
		public static final String SyslogEventView_text = "@xpath=//label[text()='Syslog Event View']";
		public static final String SyslogEventView_value = "@xpath=//div[label[text()='Syslog Event View']]/following-sibling::div";
		public static final String ServiceStatusView_text = "@xpath=//label[text()='Service Status View']";
		public static final String ServiceStatusView_value = "@xpath=//div[label[text()='Service Status View']]/following-sibling::div";
		public static final String RouterConfigurationView_text = "@xpath=//label[text()='Router Configuration View']";
		public static final String RouterConfigurationView_Value = "@xpath=//div[label[text()='Router Configuration View']]/following-sibling::div";
		public static final String PerformanceReporting_text = "@xpath=//div[label[text()='Pro-active Notification']]";
		public static final String PerformanceReporting_value = "@xpath=//div[label[text()='Performance Reporting']]/following-sibling::div";
		public static final String ProactiveNotification_text = "@xpath=//label[text()='Pro-active Notification']";
		public static final String ProactiveNotification_Value = "@xpath=//div[label[text()='Pro-active Notification']]/following-sibling::div";
		public static final String NotificationManagementTeam_text = "@xpath=//div[label[text()='Notification Management Team']]";
		public static final String NotificationManagementTeam_Value = "@xpath=//div[label[text()='Notification Management Team']]/following-sibling::div";
		public static final String DialUserAdministration_text = "@xpath=//label[text()='Dial User Administration']";
		public static final String DialUserAdministration_Value = "@xpath=//div[label[text()='Dial User Administration']]/following-sibling::div";

		public static final String DeleteServiceSuccessMessage = "@xpath=";

		public static final String existingMASdevicegrid = "@xpath=(//div[text()='MAS Switch']/parent::div/following-sibling::div)[2]";
		public static final String MAS_fetchAlldevice_InviewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b";

		// MAS Switch panel in view service page -- ="@xpath=
		public static final String MASswitch_header = "@xpath=//div[div[text()='MAS Switch']]";
		public static final String MAS_AddMASSwitchLink = "@xpath=//div//a[text()='Add MAS Switch']";
		public static final String MAS_ShowInterfaceLink = "@xpath=//div[a[text()='Add MAS Switch']]//div[a[text()='Show Interfaces']]";
		public static final String MAS_HideInterfaceLink = "@xpath=//div[a[text()='Add MAS Switch']]//div[a[text()='Hide Interfaces']]";
		public static final String MAS_AddMASSwitch_header = "@xpath=//div[text()='Add MAS Switch']";
		public static final String MAS_IMSPOPLocationDropdown = "@xpath=//div[input[@placeholder='Select ...']]";
		public static final String MAS_Cancelbutton = "@xpath=//button/span[text()='Cancel']";
		public static final String MAS_OKbutton = "@xpath=//button/span[text()='OK']";
		public static final String MAS_AddSwitchSuccessfulMessage = "@xpath=//div[span[text()='MAS switch added successfully']]";
		public static final String MAS_AddSwitchWarningMessage = "@xpath=//div/div/div[@style='color: red;']";
		public static final String MAS_UpdateSwitchSuccessfulMessage = "@xpath=//div[span[text()='MAS switch updated successfully']]";
		public static final String MAS_DeleteSwitchSuccessfulMessage = "@xpath=//div[span[text()='MAS switch deleted successfully']]";
		public static final String MAS_ViewService_DeleteMASDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete?']";
		public static final String MAS_ViewService_DeleteButton = "@xpath=//div/button[text()='Delete']";

		public static final String Edit_MASSwitch_Header = "@xpath=//div[contains(text(),'Edit MAS Switch')]";
		public static final String MAS_editdevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Edit'])[1]";
		public static final String MAS_editdevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Edit'])[2]";
		public static final String MAS_SelectInterfacesdevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Select Interfaces'])[1]";
		public static final String MAS_SelectInterfacesdevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Select Interfaces'])[2]";
		public static final String MAS_configuredevice1Interface = "@xpath=";
		public static final String MAS_configuredevice2Interface = "@xpath=";
		public static final String MAS_deletefromservicedevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Delete from Service'])[1]";
		public static final String MAS_deletefromservicedevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='Delete from Service'])[2]";

		public static final String MAS_ViewDevice_Header = "@xpath=//div[contains(text(),'Device')]";
		public static final String MAS_viewdevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='View'])[1]";
		public static final String MAS_viewdevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='View'])[2]";

		public static final String MAS_deleteFromService_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete from Service']";
		public static final String MAS_viewLink_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='View']";
		public static final String MAS_selectInterface_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";

		// Below are the fields values information under view MAS Switch page --
		// ="@xpath=
		public static final String MAS_View_DeviceNameValue = "@xpath=//div[label[text()='Name']]/following-sibling::div";
		public static final String MAS_View_VendorModelValue = "@xpath=//div[label[text()='Vendor/Model']]/following-sibling::div";
		public static final String MAS_View_ManagementAddressValue = "@xpath=//div[label[text()='Management Address']]/following-sibling::div";
		public static final String MAS_View_SnmproValue = "@xpath=//div[label[text()='Snmpro']]/following-sibling::div";
		public static final String MAS_View_CountryValue = "@xpath=//div[label[text()='Country']]/following-sibling::div";
		public static final String MAS_View_CityValue = "@xpath=//div[label[text()='City']]/following-sibling::div";
		public static final String MAS_View_SiteValue = "@xpath=//div[label[text()='Site']]/following-sibling::div";
		public static final String MAS_View_PremiseValue = "@xpath=//div[label[text()='Premise']]/following-sibling::div";
		public static final String MAS_View_ActionLink = "@xpath=(//button[@id='dropdown-basic-button'])[1]";
		public static final String MAS_View_TestColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Test']";
		public static final String MAS_View_StatusColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Status']";
		public static final String MAS_View_LastRefresh = "@xpath=//div/div[text()='Last Refresh:']";
		public static final String MAS_View_Action_EditLink = "@xpath=//div//a[text()='Edit']";
		public static final String MAS_Edit_PremiseLevel = "@xpath=//div//label[text()='Premise']";

		public static final String MAS_View_Action_DeleteLink = "@xpath=//div//a[text()='Delete']";
		public static final String MAS_ViewDevice_Action_DeleteMASDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete this item?']";
		public static final String MAS_ViewDevice_Action_DeleteButton = "@xpath=//button//span[text()='Delete']";

		// Edit Device Screen -- ="@xpath=
		public static final String MAS_PE_Edit_deviceName = "@xpath=//div[label[text()='Name']]//input";
		public static final String MAS_PE_Edit_vendorModel = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String MAS_PE_Edit_managementAddress = "@xpath=//div[label[text()='Management Address']]//input";
		public static final String MAS_PE_Edit_snmpro = "@xpath=//div[label[text()='Snmpro']]//input";
		public static final String countryinput = "@xpath=//select[@id='countryId']";
		public static final String citydropdowninput = "@xpath=//select[@id='cityId']";
		public static final String sitedropdowninput = "@xpath=//select[@id='siteId']";
		public static final String premisedropdowninput = "@xpath=//select[@id='premiseId']";
		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";
		public static final String citynameinputfield = "@xpath=//input[@id='cityName']";
		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode']";
		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1]";
		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1]";
		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";
		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";
		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2]";
		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2]";
		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";
		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";
		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";
		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		// Below Fetch Device Interface == ="@xpath= Manage Colt Page for MAS
		// Switch -- ="@xpath=
		public static final String MAS_View_Action_FetchDeviceInterfacesLink = "@xpath=//div//a[text()='Fetch Device Interfaces']";
		public static final String MAS_FetchDeviceInterfacesSuccessMessage = "@xpath=//span[contains(text(),' Fetch Interfaces started successfully. Please check the sync status of this device ')]";
		public static final String MAS_hereLink_UnderFetchDeviceInterfacesSuccessMessage = "@xpath=//a[text()='here']";
		public static final String MAS_ManageCOLTsNetworkManageNetwork_header = "@xpath=//div[contains(text(),'Manage COLT')]";
		public static final String MAS_Manage_Status_DeviceValue = "@xpath=(//div[div[label[text()='Device']]]/parent::div//div/a)[1]";
		public static final String MAS_Manage_Status_StatusValue = "@xpath=//div[div[label[text()='Device']]]/parent::div//div[text()='In Service']";
		public static final String MAS_Manage_Status_LastModificationValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-4'])[2]";
		public static final String MAS_Manage_Status_StatusLink = "@xpath=//a[contains(text(),'Status')]";
		public static final String MAS_Manage_Status_ViewInterfacesLink = "@xpath=//a[contains(text(),'View Interfaces')]";
		public static final String MAS_Manage_Status_Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String MAS_Device_Status_OK = "@xpath=//button/span[text()='OK']";
		public static final String MAS_servicestatus_popupclose = "@xpath=(//div[text()='Device']/following-sibling::button//span)[1]";
		public static final String MAS_servicestatushistory_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String MAS_Status_successMessage = "@xpath=//div[@role='alert']//span";
		public static final String MAS_servicestatushistoryValue = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String MAS_Manage_Synchronization_DeviceValue = "@xpath=((//div[label[text()='Device']])[2]/parent::div/following-sibling::div//a)[1]";
		public static final String MAS_Manage_Synchronization_SyncStatusValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[9]";
		public static final String MAS_Manage_Synchronization_SmartsValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[10]";
		public static final String MAS_Manage_Synchronization_FetchInterfacesValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[11]";
		public static final String MAS_Manage_Synchronization_VistaMartDeviceValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[12]";
		public static final String MAS_Manage_Synchronization_SynchronizeLink = "@xpath=//a[text()='Synchronize']";
		public static final String MAS_Manage_SynchronizeSuccessMessage = "@xpath=//div[@role='alert']//span";
		public static final String MAS_Manage_SynchronizeWarningMessage = "@xpath=//div[@role='alert']//span";
		public static final String ServiceIDonTopOfTheBredcomeNavigation = "@xpath=//li/a[text()='VOIP-21-Feb']";
		public static final String HomeLink_Bredcome = "@xpath=//li/a[text()='Home']";

		// Router Tool commands For MAS -- ="@xpath=
		public static final String RouterTool_header = "@xpath=//div/div[text()='Router Tools']";
		public static final String MAS_PE_CommandIPV4_header = "@xpath=//label[text()='Command IPV4']";
		public static final String MAS_PE_Router_IPV4CommandsDropdown = "@xpath=//div[label[text()='Command IPV4']]//input";
		public static final String MAS_PE_Router_IPV4CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String MAS_PE_Router_IPV4Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[1]";
		public static final String MAS_PE_commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String MAS_PE_commandIPv4_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[1]";

		public static final String MAS_PE_Router_IPV6CommandsDropdown = "@xpath=//div[label[text()='Command IPV6']]//input";
		public static final String MAS_PE_Router_IPV6CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String MAS_PE_Router_IPV6Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[2]";
		public static final String MAS_PE_commandIPv6_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String MAS_PE_commandIPv6_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[2]";
		public static final String MAS_PE_result_textArea = "@xpath=//div[label[text()='Result']]//textarea";

		// ADD INTERFACE PART-- ="@xpath=
		public static final String MAS_PE_InterfacesPanel_header = "@xpath=//div[text()='Interfaces']";
		public static final String MAS_PE_BackButton_viewdevicepage = "@xpath=//span[text()='Back']";
		public static final String MAS_View_InterfacesActionLink = "@xpath=(//button[@id=dropdown-basic-button'])[2]";
		public static final String MAS_PE_AddInterfaceLink = "@xpath=//a[text()='Add Interface/Link']";
		public static final String MAS_PE_Configuration_header = "@xpath=//div[8]/div[text()='Configuration']";
		public static final String MAS_PE_Configuration_label = "@xpath=//label[text()='Configuration']";
		public static final String MAS_PE_GenerateConfiguration_label = "@xpath=//div/label[text()='Generate Configuration ']";
		public static final String MAS_PE_OKButton = "@xpath=//span[text()='OK']";
		public static final String MAS_PE_InterfaceWarningMessage = "@xpath=(//div[@style='color: red;'])[1]";
		public static final String PE_InterfaceSuggestionMessage = "@xpath=//div[@style='color: red;']/following-sibling::div";
		public static final String MAS_PE_ConfigurationWarningMessage = "@xpath=(//div[@style='color: red;'])[2]";
		public static final String MAS_PE_AddInterface_header = "@xpath=//div[contains(text(),'Add Interface')]";
		public static final String MAS_PE_EditInterface_header = "@xpath=//div[contains(text(),'Edit Interface')]";
		public static final String MAS_PE_AccessMediaDropdown = "@xpath=(//div[label[text()='Access Media']]//div)[2]";
		public static final String MAS_PE_NetworkDropdown = "@xpath=(//div[label[text()='Network']]//div)[2]";
		public static final String MAS_PE_GenerateConfigurationDropdown = "@xpath=(//div[label[text()='Generate Configuration ']]//div)[2]//input";
		public static final String MAS_PE_HSRPBGPDropdown = "@xpath=(//div[label[text()='HSRP/BGP']]//div)[2]";
		public static final String MAS_PE_VRRPBGPDropdown = "@xpath=(//div[label[text()='VRRP/BGP']]//div)[2]";
		public static final String MAS_PE_InterfaceTextfield = "@xpath=//input[@name='interface']";
		public static final String MAS_PE_InterfaceAddressRangeTextfield = "@xpath=//input[@name='interfaceAddressRange']";
		public static final String MAS_PE_InterfaceAddressMaskTextfield = "@xpath=//input[@id='ipMaskField']";
		public static final String MAS_PE_HSRPIPTextfield = "@xpath=//input[@id='hsrpIP']";
		public static final String MAS_PE_VRRPIPTextfield = "@xpath=//input[@id='vrrpIP']";
		public static final String MAS_PE_InterfaceAddressRangeIPV6Textfield = "@xpath=//input[@name='interfaceAddressRangeIPV6']";
		public static final String MAS_PE_HSRPIPv6AddressTextfield = "@xpath=//input[@id='hsrpIPv6']";
		public static final String MAS_PE_VRRPIPv6AddressTextfield = "@xpath=//input[@id='vrrpIPv6']";
		public static final String MAS_PE_PrimaryIPv6onMas1Textfield = "@xpath=//input[@id='mas1IPv6']";
		public static final String MAS_PE_SecondaryIPv6onMas2Textfield = "@xpath=//input[@id='mas2IPv6']";
		public static final String MAS_PE_SecondaryIPv6onMas2header = "@xpath=//label[text()='Secondary IPv6 on Mas-2']";
		public static final String MAS_PE_GroupNumberTextfield = "@xpath=//input[@id='groupNumber']";
		public static final String MAS_PE_LinkTextfield = "@xpath=//input[@id='link']";
		public static final String MAS_PE_Linklabel = "@xpath=//label[text()='Link']";
		public static final String MAS_PE_VLANIDTextfield = "@xpath=//input[@id='vlanIdentification']";
		public static final String MAS_PE_VRRPGroupNameTextfield = "@xpath=//input[@id='vrrpGroupName']";
		public static final String MAS_PE_VRFTextfield = "@xpath=//input[@id='vrf']";
		public static final String MAS_PE_IVManagementCheckbox = "@xpath=//input[@id='ivMng']";
		public static final String MAS_PE_GenerateConfigurationButton = "@xpath=//a[text()='Generate Configuration']";
		public static final String MAS_PE_SaveConfigurationtoFileButton = "@xpath=//a[text()='Save Configuration to File']";
		public static final String MAS_PE_ConfigurationTextfield = "@xpath=//textarea[@name='configuration']";
		public static final String MAS_PE_HSRPTrackInterfaceTextfield = "@xpath=//input[@id='HSRPTrackInterface']";
		public static final String MAS_PE_VRRPTrackInterfaceTextfield = "@xpath=//input[@id='VRRPTrackInterface']";
		public static final String MAS_PE_HSRPAuthenticationTextfield = "@xpath=//input[@id='liHSRPAuthnk']";
		public static final String MAS_PE_VRRPAuthenticationTextfield = "@xpath=//input[@id='VRRPAuth']";

		public static final String MAS_PE_AddInterfaceSuccessfullMessage = "@xpath=//span[text()='Interface added successfully']";
		public static final String MAS_PE_UpdateInterfaceSuccessfullMessage = "@xpath=//span[text()='Interface updated successfully']";
		public static final String MAS_PE_DeleteInterfaceSuccessfullMessage = "@xpath=//span[text()='Interface successfully removed from this service.']";
		public static final String PE_DeleteInterfaceSuccessfullMessage = "@xpath=//span[text()='Interface deleted successfully']";

		public static final String MAS_PE_Checkbox1_ViewServicePage = "@xpath=(//span/span/span[2])[1]";
		public static final String MAS_PE_Checkbox2_ViewServicePage = "@xpath=(//span/span/span[2])[2]";
		public static final String MASSwitch_ACTION_Device1 = "@xpath=(//div[div[div[contains(text(),'MAS Switch')]]]//button[@id='dropdown-basic-button'])[1]";
		public static final String MASSwitch_ACTION_Device2 = "@xpath=(//div[div[div[contains(text(),'MAS Switch')]]]//button[@id='dropdown-basic-button'])[2]";
		// public static final String MAS_PE_ACTION_EditLink_Device1
		// ="@xpath=(//a[text()='Edit'])[1]";
		public static final String MAS_PE_ACTION_EditLink_Device2 = "@xpath=(//a[text()='Edit'])[2]";
		public static final String MAS_PE_ACTION_ResetLink_Device1 = "@xpath=(//a[text()='Reset'])[1]";
		public static final String MAS_PE_ACTION_ResetLink_Device2 = "@xpath=(//a[text()='Reset'])[2]";
		public static final String MAS_PE_ACTION_ConfigureLink_Device1 = "@xpath=(//a[text()='Configure'])[1]";
		public static final String MAS_PE_ACTION_ConfigureLink_Device2 = "@xpath=(//a[text()='Configure'])[2]";

		public static final String PE_ACTION_Device1 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//button[@id='dropdown-basic-button'])[1]";
		public static final String PE_ACTION_Device2 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//button[@id='dropdown-basic-button'])[2]";
		public static final String PE_ACTION_EditLink_Device1 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//div/a[text()='Edit'])[1]";
		public static final String PE_ACTION_EditLink_Device2 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//div/a[text()='Edit'])[2]";
		public static final String PE_ACTION_DeleteLink_Device1 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//div/a[text()='Delete'])[1]";
		public static final String PE_ACTION_DeleteLink_Device2 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//div/a[text()='delete'])[2]";
		public static final String PE_ACTION_ConfigureLink_Device1 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//div/a[text()='Configure'])[1]";
		public static final String PE_ACTION_ConfigureLink_Device2 = "@xpath=(//div[div[div[text()='Provider Equipment (PE)']]]//div/a[text()='Configure'])[2]";

		public static final String MAS_PE_Configure_ConfigureInterface_header = "@xpath=//div[text()='Configure Interface']";
		public static final String MAS_PE_DeleteInterfaceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete?']";
		public static final String MAS_PE_DeleteButton = "@xpath=//div/button[text()='Delete']";

		// Configure Interfaces for MAS_PE -- ="@xpath=
		// Below are the fields values information under view MAS Switch page --
		// ="@xpath=
		public static final String MAS_PE_Configure_DeviceNameValue = "@xpath=//div[label[text()='Device Name']]/following-sibling::div";
		public static final String MAS_PE_Configure_InterfaceValue = "@xpath=//div[label[text()='Interface']]/following-sibling::div";
		public static final String MAS_PE_Configure_LinkCircuitIdValue = "@xpath=//div[label[text()='Link/Circuit Id']]/following-sibling::div";
		public static final String MAS_PE_Configure_InterfaceAddressRangeValue = "@xpath=//div[label[text()='Interface Address Range']]/following-sibling::div";
		public static final String MAS_PE_Configure_InterfaceAddressMaskValue = "@xpath=//div[label[text()='Interface Address/Mask']]/following-sibling::div";
		public static final String MAS_PE_Configure_HSRPIPValue = "@xpath=//div[label[text()='HSRP IP']]/following-sibling::div";
		public static final String MAS_PE_Configure_VRRPIPValue = "@xpath=//div[label[text()='VRRP IP']]/following-sibling::div";
		public static final String MAS_PE_Configure_VRFValue = "@xpath=//div[label[text()='VRF']]/following-sibling::div";
		public static final String MAS_PE_Configure_GroupNumberValue = "@xpath=//div[label[text()='Group Number']]/following-sibling::div";
		public static final String MAS_PE_Configure_BearerTypeValue = "@xpath=//div[label[text()='Bearer Type']]/following-sibling::div";
		public static final String MAS_PE_Configure_BandWidthValue = "@xpath=//div[label[text()='Band Width']]/following-sibling::div";
		public static final String MAS_PE_Configure_VLANIdValue = "@xpath=//div[label[text()='VLAN Id']]/following-sibling::div";

		public static final String MAS_PE_Configure_Configuration_header = "@xpath=//div[text()='Configuration']";
		public static final String MAS_PE_Configure_Configurationlabel = "@xpath=//label[text()='Configuration']";
		public static final String MAS_PE_Configure_ConfigurationTextfield = "@xpath=//textarea[@name='configurationDetails']";
		public static final String MAS_PE_Configure_GenerateConfigurationDropdown = "@xpath=//div[label[contains(text(),'Configuration')]]/select";
		public static final String MAS_PE_Configure_GenerateConfigurationButton = "@xpath=//a[text()='Generate Configuration']";
		public static final String MAS_PE_Configure_GenerateConfigurationForAllCPERoutesButton = "@xpath=//a[text()='Generate Configuration for All CPE Routes']";
		public static final String MAS_PE_Configure_SaveConfigurationButton = "@xpath=//a[text()='Save Configuration']";
		public static final String MAS_PE_Configure_ExecuteConfigurationonDeviceButton = "@xpath=//a[text()='Execute Configuration on Device']";
		public static final String MAS_PE_Configure_BackButton = "@xpath=//span[text()='Back']";

		public static final String existingPEdevicegrid = "@xpath=(//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div)[2]";
		public static final String PE_fetchAlldevice_InviewPage = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b";

		// Provider Equipment (PE) panel in view service page -- ="@xpath=
		public static final String ProviderEquipment_header = "@xpath=//div[div[text()='Provider Equipment (PE)']]";
		public static final String PE_AddPEDeviceLink = "@xpath=//a[contains(text(),'Add PE Device')]";
		public static final String PE_showInterfacesLink = "@xpath=//div[a[text()='Add PE Device']]//div[a[text()='Show Interfaces']]";
		public static final String PE_HideInterfaceLink = "@xpath=//div[a[text()='Add PE Device']]//div[a[text()='Hide Interfaces']]";
		// public static final String PE_ACTION_Device1
		// ="@xpath=(//div[div[div[contains(text(),'Provider Equipment
		// (PE)')]]]//button[@id='dropdown-basic-button'])[1]";
		// public static final String PE_ACTION_Device1
		// ="@xpath=(//div[div[div[contains(text(),'Provider Equipment
		// (PE)')]]]//button[@id='dropdown-basic-button'])[2]";
		public static final String PE_AddPEDevice_header = "@xpath=//div[text()='Add PE Device']";
		public static final String Edit_PEDevice_Header = "@xpath=//div[contains(text(),'Edit PE Device')]";

		public static final String PE_IMSPOPLocationDropdown = "@xpath=//div[input[@placeholder='Select ...']]";
		public static final String PE_Cancelbutton = "@xpath=//button/span[text()='Cancel']";
		public static final String PE_OKbutton = "@xpath=//button//span[text()='OK']";
		public static final String PE_AddPEDeviceSuccessfulMessage = "@xpath=//div[span[text()='PE Device added successfully']]";
		public static final String PE_AddPEDeviceWarningMessage = "@xpath=//div/div/div[@style='color: red;']";
		public static final String PE_UpdatePEDeviceSuccessfulMessage = "@xpath=//div[span[text()='PE Device updated successfully']]";
		public static final String PE_DeletePEDeviceSuccessfulMessage = "@xpath=//div[span[text()='PE Device deleted successfully']]";
		public static final String PE_ViewService_DeletePEDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete?']";
		public static final String PE_ViewService_DeleteButton = "@xpath=//div/button[text()='Delete']";

		public static final String PE_editdevice1 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='Edit'])[1]";
		public static final String PE_editdevice2 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='Edit'])[2]";
		public static final String PE_SelectInterfacesdevice1 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='Select Interfaces'])[1]";
		public static final String PE_SelectInterfacesdevice2 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='Select Interfaces'])[2]";
		public static final String PE_deletefromservicedevice1 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='Delete from Service'])[1]";
		public static final String PE_deletefromservicedevice2 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='Delete from Service'])[2]";
		public static final String PE_configuredevice1Interface = "@xpath=";
		public static final String PE_configuredevice2Interface = "@xpath=";
		public static final String PE_viewdevice1 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='View'])[1]";
		public static final String PE_viewdevice2 = "@xpath=(//div[div[div[contains(text(),'Provider Equipment (PE)')]]]//a/span[text()='View'])[2]";
		public static final String PE_ViewDevice_Header = "@xpath=//div[contains(text(),'Device')]";

		// Below are the fields values information under view PE Device page --
		// ="@xpath=
		public static final String PE_View_DeviceNameValue = "@xpath=//div[label[text()='Name']]/following-sibling::div";
		public static final String PE_View_VendorModelValue = "@xpath=//div[label[text()='Vendor/Model']]/following-sibling::div";
		public static final String PE_View_ManagementAddressValue = "@xpath=//div[label[text()='Management Address']]/following-sibling::div";
		public static final String PE_View_SnmproValue = "@xpath=//div[label[text()='Snmpro']]/following-sibling::div";
		public static final String PE_View_CountryValue = "@xpath=//div[label[text()='Country']]/following-sibling::div";
		public static final String PE_View_CityValue = "@xpath=//div[label[text()='City']]/following-sibling::div";
		public static final String PE_View_SiteValue = "@xpath=//div[label[text()='Site']]/following-sibling::div";
		public static final String PE_View_PremiseValue = "@xpath=//div[label[text()='Premise']]/following-sibling::div";
		public static final String PE_View_ActionLink = "@xpath=(//button[@id='dropdown-basic-button'])[1]";
		public static final String PE_View_ActionLink2 = "@xpath=(//button[@id='dropdown-basic-button'])[1]";
		public static final String PE_View_InterfacesActionLink = "@xpath=(//button[@id='dropdown-basic-button'])[2]";
		public static final String PE_View_TestColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Test']";
		public static final String PE_View_StatusColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Status']";
		public static final String PE_View_LastRefresh = "@xpath=//div/div[text()='Last Refresh:']";
		public static final String PE_View_Action_EditLink = "@xpath=//div//a[text()='Edit']";
		public static final String PE_Edit_PremiseLevel = "@xpath=//div//label[text()='Premise']";
		public static final String PE_View_Action_DeleteLink = "@xpath=//div//a[text()='Delete']";
		public static final String PE_View_Action_FetchDeviceInterfacesLink = "@xpath=//div//a[text()='Fetch Device Interfaces']";
		public static final String PE_ViewDevice_Action_DeletePEDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete this item?']";
		public static final String PE_ViewDevice_Action_DeleteButton = "@xpath=//button//span[text()='Delete']";

		// Below Fetch Device Interface == ="@xpath= Manage Colt Page for PE
		// Device -- ="@xpath=
		public static final String PE_View_Action_FetchDeviceInterfacesLink1 = "@xpath=//div//a[text()='Fetch Device Interfaces']";
		public static final String PE_FetchDeviceInterfacesSuccessMessage = "@xpath=//span[contains(text(),' Fetch Interfaces started successfully. Please check the sync status of this device')]";
		public static final String PE_hereLink_UnderFetchDeviceInterfacesSuccessMessage = "@xpath=//span[contains(text(),' Fetch Interfaces started successfully. Please check the sync status of this device')]//following-sibling::a[text()='here']";
		public static final String PE_ManageCOLTsNetworkManageNetwork_header = "@xpath=//div[contains(text(),'Manage COLT')]";
		public static final String PE_Manage_Status_DeviceValue = "@xpath=(//div[div[label[text()='Device']]]/parent::div//div/a)[4]";
		public static final String PE_Manage_Status_StatusValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-3'])[4]";
		public static final String PE_Manage_Status_LastModificationValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-4'])[2]";
		public static final String PE_Manage_Status_StatusLink = "@xpath=//a[contains(text(),'Status')]";
		public static final String PE_Manage_Status_ViewInterfacesLink = "@xpath=//a[contains(text(),'View Interfaces')]";
		public static final String PE_Manage_Status_Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String PE_Device_Status_OK = "@xpath=//button/span[text()='OK']";
		public static final String PE_servicestatus_popupclose = "@xpath=(//div[text()='Device']/following-sibling::button//span)[1]";
		public static final String PE_servicestatushistory_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String PE_Status_successMessage = "@xpath=//div[@role='alert']//span";
		public static final String PE_servicestatushistoryValue = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String PE_Manage_Synchronization_DeviceValue = "@xpath=(//div[div[label[text()='Device']]]/parent::div//div/a)[4]";
		public static final String PE_Manage_Synchronization_SyncStatusValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[9]";
		public static final String PE_Manage_Synchronization_SmartsValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[10]";
		public static final String PE_Manage_Synchronization_FetchInterfacesValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[11]";
		public static final String PE_Manage_Synchronization_VistaMartDeviceValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[12]";
		public static final String PE_Manage_Synchronization_SynchronizeLink = "@xpath=//a[text()='Synchronize']";
		public static final String PE_Manage_SynchronizeSuccessMessage = "@xpath=//div[@role='alert']//span";
		public static final String ServiceIDonTopOfTheBredcomeNavigation_VOIP = "@xpath=//li/a[text()='VOIP-21-Feb']";

		// Trunk Group/Site Orders Panel in view service page-- ="@xpath=
		public static final String TrunkGroupSiteOrders_header = "@xpath=//div[text()='Trunk Group/Site Orders']";
		public static final String ViewService_Trunk_DeleteSiteOrderLink = "@xpath=(//div[div[span[contains(text(),'')]]]//span[text()='Delete'])[1]";
		public static final String ViewService_Trunk_DeleteSiteOrderWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete this item?']";
		public static final String ViewService_Trunk_DeleteButton = "@xpath=//button[text()='Delete']";
		public static final String ViewService_Trunk_AddSiteOrderSuccessMessage = "@xpath=//span[text()='Trunk Group added successfully']";
		public static final String ViewService_Trunk_UpdateSiteOrderSuccessMessage = "@xpath=//span[text()='Trunk Group successfully updated']";
		public static final String ViewService_Trunk_DeleteSiteOrderSuccessMessage = "@xpath=//span[text()='Trunk Group deleted successfully']";

		// public static final String ViewService_Trunk_TrunkNameText
		// ="@xpath=//div[text()='"+ExistingTrunkName+"']";
		public static final String ViewService_Trunk_ActionLink1 = "@xpath=(//div[div[div[text()='Trunk Group/Site Orders']]]//button[text()='Action'])[1]";
		public static final String ViewService_Trunk_ActionLink2 = "@xpath=(//div[div[div[text()='Trunk Group/Site Orders']]]//button[text()='Action'])[2]";
		public static final String ViewService_Trunk_EditTrunkLink = "@xpath=//a[text()='Edit']";
		public static final String ViewService_Trunk_DeleteTrunkLink = "@xpath=//a[text()='Delete']";
		public static final String ViewService_Trunk_AddCPEDeviceLink1 = "@xpath=(//a[text()='Add CPE Device'])[1]";
		public static final String ViewService_Trunk_AddCPEDeviceLink2 = "@xpath=(//a[text()='Add CPE Device'])[2]";

		public static final String ViewService_Trunk_ViewTrunkLink = "@xpath=(//a[text()='View'])[1]";
		public static final String ViewService_Trunk_ViewTrunkLink2 = "@xpath=(//a[text()='View'])[2]";

		public static final String selectTrunkName = "@xpath=//div[text()='value']";

		// New -- ="@xpath=
		public static final String viewTrunkPage_PSXconfigurationValue = "@xpath=//label[contains(text(),'PSX Manual Configuration')]";

		// DDI Range -- ="@xpath=
		public static final String DDIRange_Header = "@xpath=//h5[contains(text(),'DDI Range')]";

		// PSX Configuration -- ="@xpath=
		public static final String ViewTrunk_PSXSyncStatus = "@xpath=//font[contains(text(),'')]";
		public static final String PSXconfigurationDropdown_viewtrunk = "@xpath=(//div[label[text()='PSX Configuration']]//input)[1]";
		public static final String PSXconfig_sucessMessage = "@xpath=(//div[@role='alert']/span)[1]";
		public static final String PSXconfig_WarningMessage = "@xpath=(//div[@role='alert']/span)[1]/li";
		public static final String viewTrunk_PSX_executeButton = "@xpath=//div[div[div[label[text()='PSX Configuration']]]]//following-sibling::div//button/span[text()='Execute']";

		// GSX Configuration -- ="@xpath=
		public static final String GSXconfigurationDropdown_viewtrunk = "@xpath=(//div[label[text()='GSX Configuration']]//input)[1]";
		public static final String viewTrunk_GSX_generateConfigurationButton = "@xpath=//button/span[text()='Generate Configuration']";
		public static final String GSXcongig_textArea = "@xpath=//textarea[@name='gsxConfigNote']";
		public static final String GSX_config_executeButton = "@xpath=//span[text()='Execute']";
		public static final String GSXconfig_sucessMessage = "@xpath=(//div[@role='alert']/span)[1]";

		// SBC Configuration -- ="@xpath=
		public static final String SBCconfigurationDropdown_viewtrunk = "@xpath=(//div[label[text()='SBC Configuration']]//input)[1]";
		public static final String viewTrunk_SBC_executeButton = "@xpath=(//div[div[div[label[text()='SBC Configuration']]]]//following-sibling::div//button/span[text()='Execute'])[1]";

		public static final String selectCreatedTrunk_InViewServicePage = "@xpath=//div[div[span[b[text()='Trunks ']]]]//following-sibling::div//div[text()='value']";
		public static final String trunkActionDropdown_InviewServicePage = "@xpath=//div[div[span[b[text()='Trunks ']]]]//following-sibling::div//button[text()='Action']";

		public static final String fetchTrunkGroupName = "@xpath=(//div[@class='heading-green-row row']/div)[1]";

		// SBC Manual Execution panel -- ="@xpath=
		public static final String SBCmanualConfig_PanelHeader = "@xpath=//div[contains(text(),'SBC Manually Executed Configurations')]";
		public static final String SBCManualConfig_actionDropdown = "@xpath=//div[div[contains(text(),'SBC Manually Executed Configurations')]]//button";
		public static final String SBC_addLink = "@xpath=//a[text()='Add']";
		public static final String SBC_editLink = "@xpath=//a[text()='Edit']";
		public static final String SBC_deleteLink = "@xpath=//a[text()='Delete']";
		public static final String SBC_selectCreatedValue = "@xpath=(//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";
		public static final String SBC_columnNames = "@xpath=(//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";
		public static final String SBC_filenames = "@xpath=//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";
		public static final String SBC_dates = "@xpath=//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";
		public static final String SBC_DeleteManualConfiguration_SuccessMessage = "@xpath=//span[contains(text(),'Deleted SBC manual config successfully.')]";

		// PSX Manual Execution panel -- ="@xpath=
		public static final String PSXmanualConfig_PanelHeader = "@xpath=//div[contains(text(),'PSX Manually Executed Configurations')]";
		public static final String PSXManualConfig_actionDropdown = "@xpath=//div[div[contains(text(),'PSX Manually Executed Configurations')]]//button";
		public static final String PSXcongig_textArea = "@xpath=///textarea[@name='manualConfigNote']";
		public static final String PSX_selectCreatedValue = "@xpath=(//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";
		public static final String PSX_columnNames = "@xpath=(//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";
		public static final String PSX_fileName = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";
		public static final String PSX_dates = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";
		public static final String AddManualConfiguration_SuccessMessage = "@xpath=//span[contains(text(),'Manual Configuration added Successfully')]";
		public static final String UpdateManualConfiguration_SuccessMessage = "@xpath=//span[contains(text(),'Manual Configuration updated Successfully')]";
		public static final String PSX_DeleteManualConfiguration_SuccessMessage = "@xpath=//span[contains(text(),'Deleted PSX manual config successfully.')]";

		public static final String PSX_addLink = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[text()='Add   ']";
		public static final String PSX_editLink = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[text()='Edit ']";
		public static final String PSX_deleteLink = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[text()='Delete   ']";
		public static final String DeleteButton_PSX_GSX_SBC = "@xpath=//button[contains(text(),'Delete')]";
		public static final String DeleteMessae_PSX_GSX_SBC = "@xpath=//div[contains(text(),'Are you sure you want to delete?')]";

		// GSX Manual Execution panel -- ="@xpath=
		public static final String GSXmanualConfig_PanelHeader = "@xpath=//div[contains(text(),'GSX Manually Executed Configurations')]";
		public static final String GSXManualConfig_actionDropdown = "@xpath=//div[div[contains(text(),'GSX Manually Executed Configurations')]]//button";
		public static final String GSX_selectCreatedValue = "@xpath=(//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";
		public static final String GSX_columnNames = "@xpath=(//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";
		public static final String GSX_fileName = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";
		public static final String GSX_dates = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";
		public static final String GSX_addLink = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[text()='Add   ']";
		public static final String GSX_editLink = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[text()='Edit   ']";
		public static final String GSX_deleteLink = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[text()='Delete   ']";
		public static final String GSX_editPage_SaveButton = "@xpath=//button/span[text()='Save']";
		public static final String GSX_editPage_teaxtArea = "@xpath=//textarea[@name='manualConfigNote']";
		public static final String GSXManualConfiguration_SuccessMessage = "@xpath=//span[contains(text(),'Manual Configuration added Successfully')]";
		public static final String GSX_DeleteManualConfiguration_SuccessMessage = "@xpath=//span[contains(text(),'Deleted GSX manual config successfully.')]";

		public static final String manualConfiguration_textArea = "@xpath=//textarea[@name='manualConfigNote']";
		public static final String saveButton_manualConfiguration = "@xpath=//span[contains(text(),'Save')]";

		// ADD CPE DEVICE -- ="@xpath=
		public static final String commonDropdownValueTag = "@xpath=//div[@class='sc-htpNat AUGYd']/div";
		public static final String ViewService_Trunk_EditCPEDeviceLink = "@xpath=(//div[div[div[text()='Trunk Group/Site Orders']]]//span[text()='Edit'])[1]";
		public static final String ViewService_Trunk_ViewCPEDeviceLink = "@xpath=(//div[div[div[text()='Trunk Group/Site Orders']]]//span[text()='View'])[1]";
		public static final String ViewService_Trunk_DeleteCPEDeviceLink = "@xpath=(//div[div[div[text()='Trunk Group/Site Orders']]]//span[text()='Delete'])[1]";
		public static final String ViewService_Trunk_FetchDeviceInterfaceLink = "@xpath=(//div[div[div[text()='Trunk Group/Site Orders']]]//span[text()='Fetch device interfaces'])[1]";

		public static final String AddCPEDevice_header = "@xpath=//p[text()='Add CPE Device']";
		public static final String AddCPE_OKButton = "@xpath=//span[text()='OK']";
		public static final String AddCPE_CancelButton = "@xpath=//span[text()='Cancel']";
		public static final String AddCPE_WarningMessage_RouterID = "@xpath=//div[contains(text(),'Router Id')]";
		public static final String AddCPE_WarningMessage_DeviceName = "@xpath=//div[contains(text(),'Name')]";
		public static final String AddCPE_WarningMessage_VendorModel = "@xpath=//div[contains(text(),'Vendor/Model')]";
		public static final String AddCPE_WarningMessage_ManagementAddress = "@xpath=//div[contains(text(),'Management Address')]";
		public static final String AddCPE_WarningMessage_Snmpro = "@xpath=//div[contains(text(),'Snmpro')]";
		public static final String AddCPE_WarningMessage_Country = "@xpath=//div[contains(text(),'Country')]";
		public static final String AddCPE_WarningMessage_City = "@xpath=//div[contains(text(),'City')]";
		public static final String AddCPE_WarningMessage_Site = "@xpath=//div[contains(text(),'Site')]";

		public static final String CPE_RouterIdTextfield = "@xpath=//input[@name='routerId']";
		public static final String CPE_DeviceNameTextfield = "@xpath=//input[@id='deviceName']";
		// public static final String CPE_VendorModelDropdown
		// ="@xpath=//div[label[text()='Vendor/Model']]/div";
		public static final String CPE_ManagementAddressTextfield = "@xpath=//input[@id='ipField']";
		public static final String CPE_SnmproTextfield = "@xpath=//input[@id='snmpro']";
		public static final String CPE_SnmprwTextfield = "@xpath=//input[@id='snmprw']";
		public static final String CPE_Snmpv3ContextNameTextfield = "@xpath=//input[@id='snmpv3ContextName']";
		public static final String CPE_snmpv3ContextEngineIdTextfield = "@xpath=//input[@id='snmpv3ContextEngineId']";
		public static final String CPE_snmpv3SecurityUserNameTextfield = "@xpath=//input[@id='snmpv3SecurityUserName']";
		public static final String CPE_SnmpV3AuthProtoDropdown = "@xpath=//div[label[text()='Snmp V3 Auth Proto']]/div";
		public static final String CPE_snmpv3AuthPasswordTextfield = "@xpath=//input[@id='snmpv3AuthPassword']";
		public static final String CPE_CountryDropdown = "@xpath=//div[label[text()='Country']]/select";
		public static final String CPE_CityDropdown = "@xpath=//div[label[text()='City']]/select";
		public static final String CPE_SiteDropdown = "@xpath=//div[label[text()='Site']]/select";
		public static final String CPE_PremiseDropdown = "@xpath=//div[label[text()='Premise']]/select";

		public static final String CPE_AddCPEDeviceSuccessMessage = "@xpath=//span[text()='CPE Device added successfully']";
		public static final String CPE_UpdateCPEDeviceSuccessMessage = "@xpath=//span[contains(text(),'CPE Device updated successfully')]";
		public static final String CPE_DeleteCPEDeviceSuccessMessage = "@xpath=//span[text()='CPE Device deleted successfully']";
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

		public static final String CPE_ViewDevice_Header = "@xpath=(//div/div[2]/div[1]/div[1]/span)[1]";
		public static final String CPE_EditDevice_Header = "@xpath=//p[contains(text(),'Edit CPE Device')]";

		// Below are the fields values information under view CPE Device page --
		// ="@xpath=
		public static final String CPE_View_RouterIdValue = "@xpath=//div[label[text()='Router Id']]/following-sibling::div";
		public static final String CPE_View_DeviceNameValue = "@xpath=//div[label[text()='Device Name']]/following-sibling::div";
		public static final String CPE_View_VendorModelValue = "@xpath=//div[label[text()='Vendor/Model']]/following-sibling::div";
		public static final String CPE_View_ManagementAddressValue = "@xpath=//div[label[text()='Management Address']]/following-sibling::div";
		public static final String CPE_View_SnmproValue = "@xpath=//div[label[text()='Snmpro']]/following-sibling::div";
		public static final String CPE_View_SnmprwValue = "@xpath=//div[label[text()='Snmprw']]/following-sibling::div";
		public static final String CPE_View_SnmpV3ContextNameValue = "@xpath=//div[label[text()='Snmp V3 Context Name']]/following-sibling::div";
		public static final String CPE_View_SnmpV3ContextEngineIDValue = "@xpath=//div[label[text()='Snmp V3 Context Engine ID']]/following-sibling::div";
		public static final String CPE_View_SnmpV3SecurityUsernameValue = "@xpath=//div[label[text()='Snmp V3 Security Username']]/following-sibling::div";
		public static final String CPE_View_SnmpV3AuthProtoValue = "@xpath=//div[label[text()='Snmp V3 Auth Proto']]/following-sibling::div";
		public static final String CPE_View_SnmpV3AuthPasswordValue = "@xpath=//div[label[text()='Snmp V3 Auth Password']]/following-sibling::div";
		public static final String CPE_View_CountryValue = "@xpath=//div[label[text()='Country']]/following-sibling::div";
		public static final String CPE_View_CityValue = "@xpath=//div[label[text()='City']]/following-sibling::div";
		public static final String CPE_View_SiteValue = "@xpath=//div[label[text()='Site']]/following-sibling::div";
		public static final String CPE_View_PremiseValue = "@xpath=//div[label[text()='Premise']]/following-sibling::div";
		public static final String CPE_View_SnmpV3AuthPasswordLabel = "@xpath=//label[text()='Snmp V3 Auth Password']";

		public static final String CPE_View_ActionLink = "@xpath=//div[@style='margin-top: -1px;']/button[@id='dropdown-basic-button']";
		// public static final String CPE_View_ActionLink2
		// ="@xpath=(//button[@id="dropdown-basic-button"])[1]";
		public static final String CPE_View_TestColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Test']";
		public static final String CPE_View_StatusColumnName = "@xpath=//div/table/div/thead/tr/th[text()='Status']";
		public static final String CPE_View_LastRefresh = "@xpath=//div/div[text()='Last Refresh:']";
		public static final String CPE_View_Action_EditLink = "@xpath=//div//a[text()='Edit']";
		public static final String CPE_Edit_PremiseLevel = "@xpath=//div//label[text()='Premise']";
		public static final String CPE_View_Action_DeleteLink = "@xpath=//div//a[text()='Delete']";
		public static final String CPE_ViewDevice_Action_DeletePEDeviceWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete this item?']";
		public static final String CPE_ViewDevice_Action_DeleteButton = "@xpath=//button//span[text()='Delete']";

		// Below Fetch Device Interface == ="@xpath= Manage Colt Page for PE
		// Device -- ="@xpath=
		public static final String CPE_View_Action_FetchDeviceInterfacesLink = "@xpath=//div//a[text()='Fetch Device Interfaces']";
		public static final String CPE_FetchDeviceInterfacesSuccessMessage = "@xpath=//span[contains(text(),'Fetch interfaces started successfully.Please check the sync status of this device ')]";
		public static final String CPE_hereLink_UnderFetchDeviceInterfacesSuccessMessage = "@xpath=//a[text()='here']";
		public static final String CPE_ManageCOLTsNetworkManageNetwork_header = "@xpath=//div[contains(text(),'Manage COLT')]";
		public static final String CPE_Manage_Status_DeviceValue = "@xpath=((//div[label[text()='Device']])[1]/parent::div/following-sibling::div//a)[1]";
		public static final String CPE_Manage_Status_StatusValue = "@xpath=//div[div[label[text()='Device']]]/parent::div//div[text()='In Service']";
		public static final String CPE_Manage_Status_LastModificationValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-4'])[2]";
		public static final String CPE_Manage_Status_StatusLink = "@xpath=((//div[label[text()='Device']])[1]/parent::div/following-sibling::div//a)[2]";
		public static final String CPE_Manage_Status_ViewInterfacesLink = "@xpath=((//div[label[text()='Device']])[1]/parent::div/following-sibling::div//a)[3]";
		public static final String CPE_Manage_Status_Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String CPE_Device_Status_OK = "@xpath=//button/span[text()='OK']";
		public static final String CPE_servicestatus_popupclose = "@xpath=(//div[text()='Device']/following-sibling::button//span)[1]";
		public static final String CPE_servicestatushistory_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String CPE_Status_successMessage = "@xpath=//div[@role='alert']//span";
		public static final String CPE_servicestatushistoryValue = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String CPE_Manage_Synchronization_DeviceValue = "@xpath=((//div[label[text()='Device']])[2]/parent::div/following-sibling::div//a)[1]";
		public static final String CPE_Manage_Synchronization_SyncStatusValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[7]";
		public static final String CPE_Manage_Synchronization_FetchInterfacesValue = "@xpath=(//div[@class='col-12 col-sm-12 col-md-2'])[8]";
		public static final String CPE_Manage_Synchronization_SynchronizeLink = "@xpath=//a[text()='Synchronize']";
		public static final String CPE_Manage_SynchronizeSuccessMessage = "@xpath=//div[@role='alert']//span";
		public static final String CPE_Manage_SynchronizeWarningMessage = "@xpath=//div[@role='alert']//span";
		public static final String ServiceIDonTopOfTheBredcomeNavigation_VOIP1 = "@xpath=//li/a[text()='VOIP-21-Feb']";

		// Router Tool For CPE Device -- ="@xpath=
		// public static final String CPE_Router_IPV4CommandsDropdown
		// ="@xpath=//div[label[text()='Command IPV4']]//input";
		public static final String CPE_Router_IPV4CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";
		public static final String CPE_Router_IPV4Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[1]";
		public static final String CPE_Router_IPV6CommandTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";
		public static final String CPE_Router_IPV6Command_Executebutton = "@xpath=(//button/span[text()='Execute'])[2]";

		public static final String CPE_InterfacesPanel_header = "@xpath=//div[text()='Interfaces']";

		// trunk creation -- ="@xpath=
		public static final String TrunkGroupSiteOrderPanel_header = "@xpath=//div[div[text()='Trunk Group/Site Orders']]";
		public static final String trunkPanel = "@xpath=//div[div[text()='Trunk Group/Site Orders']]";
		public static final String addTrunkSiteOrderlink = "@xpath=//a[text()='Add Trunk Group/Site Order']";
		public static final String addtrunkSiteorderPage_panelheader = "@xpath=//div[div[p[text()='Trunk Group/Site Order']]]";
		public static final String trunkGroupOrder_checkbox = "@xpath=//div[label[text()='Trunk Group Order']]//input";
		public static final String trunkGrouporder_warningmsg = "@xpath=//div[text()='Trunk Group Order']";
		public static final String trunkGroupOrderName_textField = "@xpath=//div[label[text()='Trunk Group Order Number']]//input";
		public static final String trunkGroupOrderName_warningmsg = "@xpath=//div[text()='Trunk Group Order Number']";
		public static final String trunkGroupOrderName_errMsg = "@xpath=//div[@role='alert']";
		public static final String alertMSg_siteorderCreation = "@xpath=(//div[@role='alert']/span)[1]";
		public static final String trunk_okButton = "@xpath=//span[text()='OK']";

		// ADD TRUNK PAGE / Trunk Creation -- ="@xpath=
		public static final String AddTrunk_Header = "@xpath=//div[contains(text(),'Add Trunk')]";
		public static final String EditTrunk_Header = "@xpath=//div[contains(text(),'Edit Trunk')]";
		public static final String ViewTrunk_Header = "@xpath=//div[contains(text(),'View Trunk')]";

		public static final String trunkGroupDEscription_textField = "@xpath=//input[@id='tgDescription']";
		public static final String PrimaryTrunk_Dropdown = "@xpath=//select[@id='priTgName']";
		public static final String primaryTrunkGroup_Dropdown = "@xpath=//select[@id='priTgName']";
		public static final String voipProtocol_Dropdown = "@xpath=//select[@id='voipProtocol']";
		public static final String country_warningMessage = "@xpath=//div[text()='Billing Country']";
		public static final String billingCoutry_Dropdown = "@xpath=//select[@id='billingCountry']";
		public static final String CDRdelivery_Dropdown = "@xpath=//select[@id='cdrDelivery']";
		public static final String ResellerID_Textfield = "@xpath=//input[@id='resellerId']";
		public static final String gateway_Dropdown = "@xpath=//select[@id='gateway']";
		public static final String quality_Dropdown = "@xpath=//select[@id='quality']";
		public static final String trunkGroupName_TextField = "@xpath=//input[@id='tgName']";
		public static final String IPaddresstype_Dropdown = "@xpath=//select[@id='ipAddressType']";
		public static final String IPPBXRange_warningMessage = "@xpath=//div[text()='IP PBX Range']";
		public static final String IPPBXRange_textField = "@xpath=//input[@id='ipRangeField']";
		public static final String IPPBXRange_addButtton = "@xpath=//div[div[div[contains(text(),'IP PBX Range')]]]/following-sibling::div//span[text()='>>']";
		public static final String IPPBXRange_addedValue_selectDropdownField = "@xpath=//select[@id='selectedIpPbxRange']";
		public static final String IPPBXAddress_warningMessage = "@xpath=//div[text()='IP PBX Address']";
		public static final String IPPBXAddress_textField = "@xpath=//input[@id='ipPbxAddr']";
		public static final String internetBasedCustomer_checkbox = "@xpath=//div[label[text()='Internet Based Customer']]//input";
		public static final String vlanTag_textField = "@xpath=//input[@id='vlanTg']";
		public static final String subInterfaceSlot_Dropdown = "@xpath=//select[@id='sifSlot']";
		public static final String subInterfaceName_textField = "@xpath=//input[@id='sifName']";
		public static final String NIFgrouopp_textField = "@xpath=//input[@id='nifGroup']";
		public static final String signallingZone_textField = "@xpath=//input[@id='sigZone']";
		public static final String signallingTransportProtocol_Dropdown = "@xpath=//select[@id='transProt']";
		public static final String SIPsignallingport_textField = "@xpath=//input[@id='sipSignallingPort']";
		public static final String SIPsignallingPOrt_defaultValue_textvalue = "@xpath=//div[text()='Default Port:5060']";
		public static final String TLS_textField = "@xpath=//input[@id='tlsProfile']";
		public static final String srtp_checkbox = "@xpath=//input[@id='srtp']";
		public static final String signallingPort_textField = "@xpath=//input[@id='sigPort']";
		public static final String SignallingPortValue = "@xpath=(//div[div[label[contains(text(),'Signalling Port')]]]/div[2])[2]";
		public static final String CustomerDefaultNumber_textfield = "@xpath=//input[@id='custDefaultNumber']";
		public static final String CustomerDefaultNumber_WarningMessage = "@xpath=//div[contains(text(),'Customer Default Number')]";
		public static final String CustomerDefaultNumber_Header = "@xpath=//label[contains(text(),'Customer Default Number')]";

		// Show All, Hide All -- ="@xpath=
		public static final String ShowAllButton_ViewTrunk = "@xpath=//a[text()='Show All']";
		public static final String HideAllButton_ViewTrunk = "@xpath=//a[text()='Hide All']";
		public static final String ServiceImpacting_Header = "@xpath=//p[text()='Service Impacting']";

		// CENTREX -- ="@xpath=
		public static final String CENTREX_Header = "@xpath=//div[text()='Centrex']";
		public static final String AddTrunk_CENTREX_Header = "@xpath=//p[text()='Centrex']";
		public static final String ReuseNetworkSelectorTable_checkbox = "@xpath=//input[@id='reuseNetSelectTable']";
		public static final String reuseNIFgroup_checkbox = "@xpath=//input[@id='reuseNIFGroup']";
		public static final String reuseSigZonePart_checkbox = "@xpath=//input[@id='reuseSigZonePart']";

		// CUSTOMIZED MEDIA, SIGNALING IP AND TRANSPORT PROTOCOL -- ="@xpath=
		public static final String coltSignalingIP_textField = "@xpath=//input[@id='sigIp']";
		public static final String mediaIP_textField = "@xpath=//input[@id='mediaIp']";

		// PBX TYPE -- ="@xpath=
		public static final String PBXType_textfield = "@xpath=//input[@id='pbxType']";
		public static final String PBXProfile_Dropdown = "@xpath=//Select[@id='pbxProf']";

		// MANUAL CONFIGURATION -- ="@xpath=
		public static final String MANUALCONFIGURATION_Header = "@xpath=//div[contains(text(),'Manual Configuration')]";
		public static final String PSXManualConfigurationTrunkGroup_checkbox = "@xpath=//input[@id='psxManualConfigTG']";
		public static final String PSXManualConfigurationDDIRange_checkbox = "@xpath=//input[@id='psxManualConfigDDI']";
		public static final String ManualConfigurationonGSX_checkbox = "@xpath=//input[@id='gsxManualConfig']";

		// CAC AND CRL -- ="@xpath=
		public static final String NonServiceImpacting_Header = "@xpath=//h5[text()='Non Service Impacting']";
		public static final String callAdmissionControl_checkbox = "@xpath=//input[@id='cac']";
		public static final String callLimit_Dropdown = "@xpath=//select[@id='callLimit']";
		public static final String limitNumber_textField = "@xpath=//input[@id='limit']";
		public static final String callrateLimit_checkbox = "@xpath=//input[@id='cacrt']";
		public static final String callRateLimitt_textField = "@xpath=//input[@id='callrateLimit']";

		// SIP SERVICES -- ="@xpath=
		public static final String SIPSERVICES_Header = "@xpath=//p[text()='SIP Services']";
		public static final String sourceAddressFiltering_Dropdown = "@xpath=//select[@id='sourceAddressFiltering']";
		public static final String relsupport_Dropdown = "@xpath=//select[@id='rel100Support']";
		public static final String sipSessionKeepAliverTimer_textField = "@xpath=//input[@id='sipSessionKeepAliveRetimer']";

		// PSX TRUNK GROUP FIELDS -- ="@xpath=
		public static final String PSXTRUNKGROUPFIELDS_Header = "@xpath=//p[text()='PSX Trunk Group Fields']";
		public static final String Carrier_warningMessage = "@xpath=//div[contains(text(),'Carrier')]";
		public static final String carriers_TextField = "@xpath=//input[@id='carrierIdNew']";
		public static final String IPsignallingProfile_textField = "@xpath=//input[@id='ipsigProfId']";
		public static final String EgressipSignal_TextField = "@xpath=//input[@id='iptgipsigProfId']";

		public static final String addressReachability_Dropdown = "@xpath=//select[@id='addressReachability']";
		public static final String COSprofile_Dropdown = "@xpath=//select[@id='cosProfile']";

		public static final String defaultTextMessageUnderSIPsessionTimer = "@xpath=//div[label[text()='SIP Session Keepalive Timer(Sec)']]/div";
		public static final String ipInterface_textField = "@xpath=//input[@id='ipIinterface']";
		public static final String AddressContext_textField = "@xpath=//input[@id='addContext']";
		public static final String ipInterfaceGroup_textField = "@xpath=//input[@id='ipifgroup']";

		public static final String COSprofile_TextField = "@xpath=//input[@id='cosProfileNew']";
		public static final String COSprofile_checkbox = "@xpath=//input[@id='endiscosprof']";

		public static final String CreateTrunkSuccessMessage = "@xpath=//span[text()='Trunk created successfully']";
		public static final String UpdateTrunkSuccessMessage = "@xpath=//span[text()='Trunk updated successfully']";
		public static final String DeleteTrunkSuccessMessage = "@xpath=//span[text()='Trunk deleted successfully']";

		public static final String ViewTrunkPage_ActionButton = "@xpath=//div[div[text()='View Trunk']]//button[text()='Action']";
		public static final String ViewTrunkPage_EditLink = "@xpath=//a[contains(text(),'Edit')]";
		public static final String ViewTrunkPage_DeleteLink = "@xpath=//a[contains(text(),'Delete')]";
		public static final String ViewTrunkPage_HistoryLink = "@xpath=//a[contains(text(),'History')]";
		public static final String ViewTrunkPage_PSXQueueLink = "@xpath=//a[contains(text(),'PSX Queue')]";

		public static final String ViewTrunkPage_DeleteButton = "@xpath=//button//span[text()='Delete']";
		public static final String ViewTrunk_DeleteWarningMessage = "@xpath=//div/div[text()='Are you sure that you want to delete?']";

		// HISTORY PAGE -- ="@xpath=
		public static final String HistoryPage_Header = "@xpath=//div[contains(text(),'History')]";
		public static final String Rivision_columnName = "@xpath=//div[span[text()='Revision']]";
		public static final String RestoredForm_columnName = "@xpath=//div[span[text()='Restored Form']]";
		public static final String RevisionType_columnName = "@xpath=//div[span[text()='Revision Type']]";
		public static final String Date_columnName = "@xpath=//div[span[text()='Date']]";
		public static final String LastModifiedBy_columnName = "@xpath=//div[span[text()='Last Modified By ']]";
		public static final String TrunkGroupName_columnName = "@xpath=//div[span[text()='Trunk Group Name ']]";
		public static final String Gateway_columnName = "@xpath=//div[span[text()='Gateway ']]";
		public static final String VoIPProtocol_columnName = "@xpath=//div[span[text()='VoIP Protocol ']]";
		public static final String SignallingTransportProtocol_columnName = "@xpath=//div[span[text()='Signalling Transport Protocol ']]";
		public static final String Action_columnName = "@xpath=//div[span[text()='Action']]";
		public static final String BackButton_History = "@xpath=//span[contains(text(),'Back')]";

		// PSX QUEUE PAGE -- ="@xpath=
		public static final String PSXQueuePage_Header = "@xpath=//div[contains(text(),'PSX Queue')]";
		public static final String LastStartTime_columnName = "@xpath=//div[span[text()='Last Start Time']]";
		public static final String Customer_columnName = "@xpath=//div[span[text()='Customer']]";
		public static final String Service_columnName = "@xpath=//div[span[text()='Service']]";
		public static final String TrunkName_columnName = "@xpath=//div[span[text()='Trunk Name']]";

		// DISASTER RECOVERY -- ="@xpath=
		public static final String bulkinterfaceheader = "@xpath=//div[text()='Bulk Interface']";
		public static final String bulkjobsubmit = "@xpath=//span[text()='Submit']";
		public static final String bulkjobcancel = "@xpath=(//span[text()='Cancel'])[1]";
		public static final String bulkinterfacelink = "@xpath=//a[text()='Bulk Interface']";
		public static final String bulkinterface_archivelink = "@xpath=//a[text()='Archive']";
		public static final String bulkinterface_refreshlink = "@xpath=//a[text()='Refresh']";
		public static final String bulkinterfacepage_cancel = "@xpath=(//span[text()='Cancel'])[2]";
		public static final String successmsg = "@xpath=//div[@role='alert']//span";
		public static final String bulkinterface_actiondropdown = "@xpath=//div[contains(text(),'Bulk Interface')]/following-sibling::div/div//button[text()='Action']";

		public static final String trunkType_Dropdown = "@xpath=//select[@id='trunkType']";

		// Add DR Plans -- ="@xpath=
		public static final String customizedmedia_header = "@xpath=//div[@class='card-header'][text()='Customized Media, Signaling IP and Transport Protocol']";
		public static final String recovery_header = "@xpath=//div[@class='card-header'][text()='Disaster Recovery']";
		public static final String addDRplans_link = "@xpath=//a/span[text()='Add Dr Plan']";
		public static final String DRplansBulkInterface_link = "@xpath=//a/span[text()='DR Plans Bulk Interface ']";
		public static final String downloadDRplans_link = "@xpath=//a/span[text()='Download DR Plans']";
		public static final String addDRplan_header = "@xpath=//div[text()='Disaster Recovery Plans']";
		public static final String DRplanA_header = "@xpath=//div[text()='DR Plan A']";
		public static final String DRplanB_header = "@xpath=//div[text()='DR Plan B']";
		public static final String DRplanC_header = "@xpath=//div[text()='DR Plan C']";
		public static final String DRplanD_header = "@xpath=//div[text()='DR Plan D']";
		public static final String DRplanE_header = "@xpath=//div[text()='DR Plan E']";
		public static final String DRplanA_actiondropdown = "@xpath=(//button[text()='Action'])[1]";
		public static final String DRplanB_actiondropdown = "@xpath=(//button[text()='Action'])[2]";
		public static final String DRplanC_actiondropdown = "@xpath=(//button[text()='Action'])[3]";
		public static final String DRplanD_actiondropdown = "@xpath=(//button[text()='Action'])[4]";
		public static final String DRplanE_actiondropdown = "@xpath=(//button[text()='Action'])[5]";
		public static final String DRplan_addlink = "@xpath=//a[text()='Add']";
		public static final String DRplan_editlink = "@xpath=//a[text()='Edit']";
		public static final String DRplan_Deletelink = "@xpath=//a[text()='Delete']";
		public static final String DRplanD_E_addlink = "@xpath=(//a[text()='Add'])[2]";
		public static final String DRplanD_E_editlink = "@xpath=(//a[text()='Edit'])[2]";
		public static final String DRplanD_E_Deletelink = "@xpath=(//a[text()='Delete'])[2]";
		public static final String addDRplan_header1 = "@xpath=(//div[text()='Disaster Recovery Plans'])[2]";
		public static final String rangestart_cc_warngmsg = "@xpath=//input[@id='rangeStart_CC']/following-sibling::div";
		public static final String rangestart_lac_warngmsg = "@xpath=//input[@id='rangeStart_LAC']/following-sibling::div";
		public static final String rangestart_num_warngmsg = "@xpath=//input[@id='rangeStart_Num']/following-sibling::div";
		public static final String rangefinish_cc_warngmsg = "@xpath=//input[@id='rangeFinish_CC']/following-sibling::div";
		public static final String rangefinish_lac_warngmsg = "@xpath=//input[@id='rangeFinish_LAC']/following-sibling::div";
		public static final String rangefinish_num_warngmsg = "@xpath=//input[@id='rangeFinish_Num']/following-sibling::div";
		public static final String destinationnumber_cc_warngmsg = "@xpath=//input[@id='destinationNumberCC']/following-sibling::div";
		public static final String destinationnumber_lac_warngmsg = "@xpath=//input[@id='destinationNumberLC']/following-sibling::div";
		public static final String destinationnumber_num_warngmsg = "@xpath=//input[@id='destinationNumberNum']/following-sibling::div";
		public static final String rangestart_label = "@xpath=//label[text()='Range Start']";
		public static final String rangestart_cc = "@xpath=//input[@id='rangeStart_CC']";
		public static final String rangestart_lac = "@xpath=//input[@id='rangeStart_LAC']";
		public static final String rangestart_num = "@xpath=//input[@id='rangeStart_Num']";
		public static final String rangefinish_label = "@xpath=//label[text()='Range Finish']";
		public static final String rangefinish_cc = "@xpath=//input[@id='rangeFinish_CC']";
		public static final String rangefinish_lac = "@xpath=//input[@id='rangeFinish_LAC']";
		public static final String rangefinish_num = "@xpath=//input[@id='rangeFinish_Num']";
		public static final String destinationnumber_label = "@xpath=//label[text()='Destination Number(DN)']";
		public static final String destinationnumber_cc = "@xpath=//input[@id='destinationNumberCC']";
		public static final String destinationnumber_lac = "@xpath=//input[@id='destinationNumberLC']";
		public static final String destinationnumber_num = "@xpath=//input[@id='destinationNumberNum']";
		public static final String activate_deactivate_label = "@xpath=//label[text()='Activate/Deactivate DR Plan']";
		public static final String activate_deactivate_dropdownvalue = "@xpath=(//label[text()='Activate/Deactivate DR Plan']/parent::div/parent::div/following-sibling::div//input)[1]";
		public static final String DRplanA_rangestart_columnheader = "@xpath=(//span[text()='Range Start'])[1]";
		public static final String DRplanA_rangefinish_columnheader = "@xpath=(//span[text()='Range Finish'])[1]";
		public static final String DRplanA_destinationnumber_columnheader = "@xpath=(//span[text()='Destination Number(DN)'])[1]";
		public static final String DRplanA_activateDeactivate_columnheader = "@xpath=(//span[text()='Activate/Deactivate DR Plan'])[1]";
		public static final String DRplanB_rangestart_columnheader = "@xpath=(//span[text()='Range Start'])[2]";
		public static final String DRplanB_rangefinish_columnheader = "@xpath=(//span[text()='Range Finish'])[2]";
		public static final String DRplanB_destinationnumber_columnheader = "@xpath=(//span[text()='Destination Number(DN)'])[2]";
		public static final String DRplanB_activateDeactivate_columnheader = "@xpath=(//span[text()='Activate/Deactivate DR Plan'])[2]";
		public static final String DRplanC_rangestart_columnheader = "@xpath=(//span[text()='Range Start'])[3]";
		public static final String DRplanC_rangefinish_columnheader = "@xpath=(//span[text()='Range Finish'])[3]";
		public static final String DRplanC_destinationnumber_columnheader = "@xpath=(//span[text()='Destination Number(DN)'])[3]";
		public static final String DRplanC_activateDeactivate_columnheader = "@xpath=(//span[text()='Activate/Deactivate DR Plan'])[3]";
		public static final String DRplanD_rangestart_columnheader = "@xpath=(//span[text()='Range Start'])[4]";
		public static final String DRplanD_rangefinish_columnheader = "@xpath=(//span[text()='Range Finish'])[4]";
		public static final String DRplanD_destinationnumber_columnheader = "@xpath=(//span[text()='Destination Number(DN)'])[4]";
		public static final String DRplanD_activateDeactivate_columnheader = "@xpath=(//span[text()='Activate/Deactivate DR Plan'])[4]";
		public static final String DRplanE_rangestart_columnheader = "@xpath=(//span[text()='Range Start'])[5]";
		public static final String DRplanE_rangefinish_columnheader = "@xpath=(//span[text()='Range Finish'])[5]";
		public static final String DRplanE_destinationnumber_columnheader = "@xpath=(//span[text()='Destination Number(DN)'])[5]";
		public static final String DRplanE_activateDeactivate_columnheader = "@xpath=(//span[text()='Activate/Deactivate DR Plan'])[5]";
		public static final String addedDRplan_tablelist = "@xpath=(//div[text()='value']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']";
		public static final String addedDRplan_rangestart = "@xpath=(//div[text()='value']/parent::div/following-sibling::div[@class='ag-div-margin row'])[1]//div[@ref='eBodyContainer']//div[@role='row']//div[@col-id='rangeStart']";

		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";

		// DR Plans Bulk Interface -- ="@xpath=
		public static final String DRplans_bulkinterface_link = "@xpath=//a/span[text()='DR Plans Bulk Interface']";
		public static final String bulkinterfaceheader1 = "@xpath=//div[text()='Bulk Interface']";
		public static final String bulkjob_choosefilebutton = "@xpath=//label[text()='File:']/parent::div/parent::div/following-sibling::div//input";
		public static final String bulkjobsubmit1 = "@xpath=//span[text()='Submit']";
		public static final String bulkinterface_archivelink1 = "@xpath=//a[text()='Archive']";
		public static final String bulkinterface_refreshlink1 = "@xpath=//a[text()='Refresh']";
		public static final String bulkinterfacepage_cancel1 = "@xpath=(//span[text()='Cancel'])[2]";
		public static final String bulkinterface_actiondropdown1 = "@xpath=//div[contains(text(),'Bulk Interface')]/following-sibling::div/div//button[text()='Action']";
		public static final String trunkgroupname_columnheader = "@xpath=(//span[text()='TrunkGroup Name'])[1]";
		public static final String username_columnheader = "@xpath=(//span[text()='User Name'])[1]";
		public static final String submittime_columnheader = "@xpath=(//span[text()='Submit Time'])[1]";
		public static final String starttime_columnheader = "@xpath=(//span[text()='Start Time'])[1]";
		public static final String endtime_columnheader = "@xpath=(//span[text()='End Time'])[1]";
		public static final String status_columnheader = "@xpath=(//span[text()='Status'])[1]";
		public static final String completion_columnheader = "@xpath=(//span[text()='Completion (%)'])[1]";
		public static final String log_columnheader = "@xpath=(//span[text()='Log'])[1]";
		public static final String uploadfile_columnheader = "@xpath=(//span[text()='Upload File'])[1]";
		public static final String addedbulkinterface_tablelist = "@xpath=(//div[@ref='eBodyContainer'])[1]";
		public static final String addedbulkinterface_trunkgroupname = "@xpath=(//div[@ref='eBodyContainer'])[1]/div[@role='row']/div[text()='value']";
		public static final String addedbulkinterface_rowid = "@xpath=(//div[@ref='eBodyContainer'])[1]//div[text()='value']/parent::div[@role='row']";
		public static final String bulkinterface_trunkgroupname_value = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='tgName']";
		public static final String bulkinterface_username_value = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='user']";
		public static final String bulkinterface_submittime_value = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='submitTime']";
		public static final String bulkinterface_starttime_value = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='startTime']";
		public static final String bulkinterface_endtime_value = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='endTime']";
		public static final String bulkinterface_status_value = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='status']";
		public static final String bulkinterface_completion_value = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='completionPercentage']";
		public static final String bulkinterface_log = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='log']//div[text()='Log']";
		public static final String bulkinterface_file = "@xpath=//div[@ref='eBodyContainer']//div[@row-id='value']/div[@col-id='fileName']/div/div";
		public static final String bulkinterface_backbutton = "@xpath=(//span[text()='Back'])[1]";
		public static final String archive_backbutton = "@xpath=(//span[text()='Back'])[2]";

		// Download DR Plans
		public static final String downloadDRplans_link1 = "@xpath=//a/span[text()='Download DR Plans']";

		public static final String serivceAlert = "@xpath=//div[@role='alert']";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		public static final String ManageCustomerServiceLink = "@xpath=//span/b[contains(text(),'MANAGE CUSTOMER')]";

		public static final String MAS_ViewDevice = "@xpath=//div[contains(text(),'MAS Switch')]";

		public static final String devicefirst = "@xpath=(//tr//td//span//a[contains(text(),'View')])[1]";
		public static final String testlist1 = "@xpath=(//tbody/tr[";
		public static final String testlist2 = "]/td)[1]";
		public static final String testlist3 = "]/td)[2]/div";
		public static final String testlist = "@xpath=//tbody/tr";
		public static final String MAS_PE_ACTION_EditLink_Device1 = "@xpath=//td//a[text()='Edit']";
		public static final String existingPEdevicegrid1 = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::table";
		public static final String PE1 = "@xpath=(//tr//td//span//a[contains(text(),'View')])[1]";
		public static final String PE_Edit = "@xpath=//div//a[text()='Edit']";
		public static final String PE_Delete_Interface = "@xpath=//div//a[text()='Delete']";
		public static final String deviceDeleteFromService = "@xpath=(//tr//td//span//a[contains(text(),'Delete from Service')])[1]";
		public static final String addedDRplan_tablelist1 = "@xpath=(//div[text()='";
		public static final String addedDRplan_tablelist2 = "']/following::table)[1]";
		public static final String addedDRplan_rangestart1 = "@xpath= (//div[text()='";
		public static final String addedDRplan_rangestart2 = "']/following::table)[1]//tr";
		public static final String AddTrunkLink = "@xpath=(//a[contains(text(),'Add Trunk')])[2]";
		public static final String EditTrunkHeader = "@xpath=//p[contains(text(),'Edit')]";

		public static final String CPE_VendorModelDropdown = "@xpath=//div[label[text()='Vendor/Model']]//input";
		public static final String DeviceName = "@xpath=//input[@name='deviceName']";
		public static final String CPE_View = "@xpath=//span//a[contains(text(),'View')]";
		public static final String CPE_Device_Header = "@xpath=//div[contains(text(),'Device')]";
		public static final String DeleteTrunk = "@xpath=//a[contains(text(),'Delete')]";
		// public static final String
		// MAS_PE_OKButton1="@xpath=//button//span[contains(text(),'OK')]";
		public static final String CPE_Router_IPV6CommandsDropdown = "@xpath=//div[label[text()='Command IPV6']]//input";
		public static final String CPE_Router_IPV4CommandsDropdown = "@xpath=//div[label[text()='Command IPV4']]//input";
		public static final String EditTrunkInTable = "@xpath=//td//a[contains(text(),'Edit')]";
		public static final String PE_interface = "@xpath=//input[@name='interface']";
		public static final String MAS_PE_OKButton1 = "@xpath=//button//span[text()='OK']";
		public static final String Acess_PE = "@xpath=//div[label[text()='Access Media']]//input";

	}
}
