package testHarness.aptFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.Lanlink_InternationalObj;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import baseClasses.SeleniumUtils;

public class Lanlink_International extends SeleniumUtils{
	
	public void CreateCustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		
			//ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Customer Creation Functionality");

		
				verifyExists(Lanlink_InternationalObj.International.ManageCustomerServiceLink," Manage Customer Service Link");
				click(Lanlink_InternationalObj.International.ManageCustomerServiceLink);
				
				verifyExists(Lanlink_InternationalObj.International.createcustomerlink," create customer link");
				click(Lanlink_InternationalObj.International.createcustomerlink,"Create Customer Link");
				
				verifyExists(Lanlink_InternationalObj.International.createcustomer_header,"create customer page header");
				
				//scrolltoend();
				verifyExists(Lanlink_InternationalObj.International.okbutton,"ok");
				click(Lanlink_InternationalObj.International.okbutton,"ok");
				
				//Warning msg check
				verifyExists(Lanlink_InternationalObj.International.customernamewarngmsg,"Legal Customer Name");
				verifyExists(Lanlink_InternationalObj.International.countrywarngmsg,"Country");
				verifyExists(Lanlink_InternationalObj.International.ocnwarngmsg,"OCN");
				verifyExists(Lanlink_InternationalObj.International.typewarngmsg,"Type");
				verifyExists(Lanlink_InternationalObj.International.emailwarngmsg,"Email");
			
				//Create customer by providing all info
				
				verifyExists(Lanlink_InternationalObj.International.nametextfield,"name");
				sendKeys(Lanlink_InternationalObj.International.nametextfield,name,"name");
		
				
				verifyExists(Lanlink_InternationalObj.International.maindomaintextfield,"Main Domain");
				sendKeys(Lanlink_InternationalObj.International.maindomaintextfield,maindomain,"Main Domain");
				
				verifyExists(Lanlink_InternationalObj.International.country,"country");
			//	sendKeys(APTIPAccessNoCPE.APTNoCPE.country,country,"country");
				addDropdownValues_commonMethod("Country",Lanlink_InternationalObj.International.country,country);
			
				verifyExists(Lanlink_InternationalObj.International.ocntextfield,"ocn");
				sendKeys(Lanlink_InternationalObj.International.ocntextfield,ocn,"ocn");
				
				verifyExists(Lanlink_InternationalObj.International.referencetextfield,"reference");
				sendKeys(Lanlink_InternationalObj.International.referencetextfield,reference,"reference");
			
				verifyExists(Lanlink_InternationalObj.International.technicalcontactnametextfield,"technical contact name");
				sendKeys(Lanlink_InternationalObj.International.technicalcontactnametextfield,technicalcontactname,"technical contact name");
				
				verifyExists(Lanlink_InternationalObj.International.typedropdown,"technical contact name");
				//sendKeys(APTIPAccessNoCPE.APTNoCPE.typedropdown,type,"technical contact name");
				addDropdownValues_commonMethod("Type",Lanlink_InternationalObj.International.type,type);
				
				verifyExists(Lanlink_InternationalObj.International.emailtextfield,"email");
				sendKeys(Lanlink_InternationalObj.International.emailtextfield,email,"email");
				
				verifyExists(Lanlink_InternationalObj.International.phonetextfield,"phone text field");
				sendKeys(Lanlink_InternationalObj.International.phonetextfield,phone,"phone text field");
				
				verifyExists(Lanlink_InternationalObj.International.faxtextfield,"fax text field");
				sendKeys(Lanlink_InternationalObj.International.faxtextfield,fax,"fax text field");
				
				verifyExists(Lanlink_InternationalObj.International.okbutton,"ok");
				click(Lanlink_InternationalObj.International.okbutton,"ok");
				
				verifyExists(Lanlink_InternationalObj.International.customercreationsuccessmsg,"Customer successfully created.");
				
			}
	
	public void selectCustomertocreateOrder(String ChooseCustomerToBeSelected)
			throws InterruptedException, IOException {
		

		mouseMoveOn(Lanlink_InternationalObj.International.ManageCustomerServiceLink);

		verifyExists(Lanlink_InternationalObj.International.CreateOrderServiceLink, "Create Order Service Link");
		click(Lanlink_InternationalObj.International.CreateOrderServiceLink, "Create Order Service Link");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		// click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");

		verifyExists(Lanlink_InternationalObj.International.entercustomernamefield, "name");
		sendKeys(Lanlink_InternationalObj.International.entercustomernamefield, ChooseCustomerToBeSelected, "name");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"name");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"*","name");

		waitforPagetobeenable();

		addDropdownValues_commonMethod("Choose a customer",Lanlink_InternationalObj.International.choosecustomerdropdown, ChooseCustomerToBeSelected);
		// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

		verifyExists(Lanlink_InternationalObj.International.nextbutton, "Next button");
		click(Lanlink_InternationalObj.International.nextbutton, "Next button");

	}
	
	public void Verifyfields(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException 
			{
		
		 String ServiceTypeToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");

		
		waitToPageLoad();
							
		waitforPagetobeenable();	
			((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		
		click(Lanlink_InternationalObj.International.Next_Button,"Next");
		Reporter.log("clicked on next button to verify the mandatory fields error messages");
		
		
		//Create Order/Contract Number Error message
			verifyExists(Lanlink_InternationalObj.International.order_contractnumberErrmsg, "Order/Contract Number");
		
		//Service Type Error message
			verifyExists(Lanlink_InternationalObj.International.servicetypeerrmsg, "Service Type");
					
			
				String[] Servicetypelists = { "BM (Broadcast Media)", "Domain Management", "DSL", "FAX to Mail", "HSS",
						"IP Access (On-net/Offnet/EoS)", "IP Access Bundle", "IP Transit", "IP VPN", "IP Web/Mail", "LANLink",
						"MDF/MVF/DI", "NGIN", "Number Hosting", "Transmission Link", "Voice Line (V)", "VOIP Access",
						"Wholesale SIP Trunking" };
		
				Reporter.log("order dropdown");
				
			//check whether Order dropdown is displayed	
				boolean orderdopdown = isElementPresent(Lanlink_InternationalObj.International.orderdropdown);
			//	sa.assertTrue(orderdopdown, "Order/Contract Number dropdown is not displayed");
				Reporter.log("order dropdown field is verified");
				
				
			//Select value under 'Service Type' dropdown
				addDropdownValues_commonMethod("Service Type",Lanlink_InternationalObj.International.servicetypedropdowntoclick, ServiceTypeToBeSelected);
				
				
		   		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

				waitforPagetobeenable();
				
			//Click on next button to check mandatory messages
				click(Lanlink_InternationalObj.International.Next_Button,"Next");

				waitforPagetobeenable();
				
				Reporter.log("clicked on next button to verify the mandatory fields error messages");
		
				
			//Interface Speed Error message	
				verifyExists(Lanlink_InternationalObj.International.interfaceSpeedErrmsg, "Interface Speed");
				
				
			//Service Sub Type Error message
				verifyExists(Lanlink_InternationalObj.International.servicesubtypeErrMsg, "Service Subtype");
		
				
			//Modular msp checkbox	
				boolean modularmspCheckbox = isElementPresent(Lanlink_InternationalObj.International.modularmspcheckbox);
						
			//	sa.assertTrue(modularmspCheckbox, "modularmsp checkbox is displayed");
		
			//AutoCreate checkbox	
				boolean autocreateservicecheckbox = isElementPresent(Lanlink_InternationalObj.International.AutocreateServicecheckbox);
			//	sa.assertTrue(autocreateservicecheckbox, "Auto create check box is not displayed");
				
			
			for(int i=0; i<4; i++) 
			{
				
				if(i==0) {
					
						verifyinterfaceSpeeddropdown();
		
						verifyservicesubtypesdropdownwhenMSPandAutoCreatenotslected();
		
						verifyavailablecircuitdropdown();
		
				}
			
				
				else if(i==1) {
					
					
		
						click(Lanlink_InternationalObj.International.modularmspcheckbox, "Modular MSP checkbox");
		
						verifyservicesubtypesdropdownwhenMSPaloneselected();
		
						verifyavailablecircuitdropdown();
					
				}
		
		
				else if(i==2) {
					
					
						
						click(Lanlink_InternationalObj.International.modularmspcheckbox,"Modular MSP checkbox");
								waitforPagetobeenable();
						
						click(Lanlink_InternationalObj.International.AutocreateServicecheckbox, "AutoCreate Service");
								waitforPagetobeenable();
		
						verifyA_Endtechnologydropdown();
		
						verifyB_Endtechnologydropdowb();
		
						verifyinterfaceSpeeddropdown();
		
						verifyservicesubtypesdropdownwgenAutoCreatealoneselected();
		
						verifyavailablecircuitdropdown();
		
				}
				else if(i==3) {
					
					
					
						click(Lanlink_InternationalObj.International.modularmspcheckbox,"Modular MSP checkbox");
						waitforPagetobeenable();
						
						verifyA_Endtechnologydropdown();
		
						verifyB_Endtechnologydropdowb();
		
						verifyservicesubtypesdropdownwhenMSPandAutoCreateselected();
		
						verifyavailablecircuitdropdown();
				}
			}
			
				
			}
	
	
	public void verifyinterfaceSpeeddropdown() throws InterruptedException, IOException, IOException
	   {
			// verify the list of interfaceSpeed
			try {

			String[] interfacelist = { "1GigE", "10GigE" };

			boolean interfacespeeddropdown = isElementPresent(Lanlink_InternationalObj.International.InterfaceSpeed);
					;
		//	sa.assertTrue(interfacespeeddropdown, "Interface speed dropdown is not displayed");

			click(Lanlink_InternationalObj.International.InterfaceSpeed);

			List<String> listofinterfacespeed = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofinterfacespeed)));
			for (String interfacespeed : listofinterfacespeed) {

				boolean match = false;
				for (int i = 0; i < interfacelist.length; i++) {
					if (interfacespeed.equals(interfacelist[i])) {
						match = true;
						Reporter.log("interface speeds : " + interfacespeed);
						//sa.assertTrue(match,"");
					}
				}
				
			}
			
			} catch (AssertionError error) {
		       
				error.printStackTrace();
				
		    }catch(Exception e) {
		    	Reporter.log("dropdowns values in Interface speed are mismiatching under service type");
		    	Reporter.log("dropdowns values in Interface speed are mismiatching under service type");
			}

		}
	
	public void verifyservicesubtypesdropdownwhenMSPandAutoCreatenotslected() throws InterruptedException, IOException {

		String[] servicesubtypelist = { "Direct Fiber", "LANLink International", "LANLink Metro", "LANLink National","LANLink Outband Management", "OLO - (GCR/EU)" };

		try {
			boolean servicesubtypesdropdown = isElementPresent(Lanlink_InternationalObj.International.ServiceSubtype);
			
		//	sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

			// verify the list of service sub types
			click(Lanlink_InternationalObj.International.ServiceSubtype);

			List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofServicesubtypes)));
			for (String servicesubtypes : listofServicesubtypes) {

				boolean match = false;
				for (int i = 0; i < servicesubtypelist.length; i++) {
					if (servicesubtypes.equals(servicesubtypelist[i])) {
						match = true;
						Reporter.log("service sub types : " + servicesubtypes);
						//sa.assertTrue(match,"");
					}
				}
				

			}
		
		} catch (AssertionError error) {

			error.printStackTrace();
			
	    }catch(Exception e) {
			Reporter.log("Dropdown values in Service subtypes are mismatching");
			Reporter.log("Dropdown values in Service subtypes are mismatching");
		}

	}
	
	public void verifyavailablecircuitdropdown() throws InterruptedException, IOException {
		try {
				boolean availablecircuitsdropdown = isElementPresent(Lanlink_InternationalObj.International.AvailableCircuits);
				
			//	sa.assertTrue(availablecircuitsdropdown, "available circuit dropdown is not displayed");
				
		}catch(AssertionError e) {
			Reporter.log("Available circuit dropdown under servicetype got failed");
			Reporter.log("Available circuit dropdown under servicetype got failed");
		}
		}
	
	public void verifyservicesubtypesdropdownwhenMSPaloneselected() throws InterruptedException, IOException {

		try {
		String[] servicesubtypelist = { "LANLink International", "LANLink Metro", "LANLink National", "OLO - (GCR/EU)" };

		boolean servicesubtypesdropdown = isElementPresent(Lanlink_InternationalObj.International.ServiceSubtype);
		
		//sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

		// verify the list of service sub types
		click(Lanlink_InternationalObj.International.ServiceSubtype);

		List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofServicesubtypes)));
		for (String servicesubtypes : listofServicesubtypes) {

			boolean match = false;
			for (int i = 0; i < servicesubtypelist.length; i++) {
				if (servicesubtypes.equals(servicesubtypelist[i])) {
					match = true;
					Reporter.log("service sub types : " + servicesubtypes);
					//sa.assertTrue(match,"");
				}
			}
			

		}
		} catch (AssertionError error) {
	       
			error.printStackTrace();
			
		}catch (Exception e) {

		Reporter.log("Dropdown values inside service subtypes are mismatching");
		Reporter.log("Dropdown values inside service subtypes are mismatching");
		}

	}
	
	public void verifyA_Endtechnologydropdown() throws InterruptedException, IOException {

		try {
		String[] A_endTechnolnogylist = { "Atrica", "MMSP", "Ethernet over Fibre" };

		boolean A_EndTechnolnogy = isElementPresent(Lanlink_InternationalObj.International.A_Endtechnology);
			
	//	sa.assertTrue(A_EndTechnolnogy, "A-End technolnogy dropdown is not displayed");

		// verify the list of A-End technolnogies
		click(Lanlink_InternationalObj.International.A_Endtechnology);

		List<String> listofA_endTechnologies = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofA_endTechnologies)));

		for (String A_endTechnolnogies : listofA_endTechnologies) {

			boolean match = false;
			for (int i = 0; i < A_endTechnolnogylist.length; i++) {
				if (A_endTechnolnogies.equals(A_endTechnolnogylist[i])) {
					match = true;
					Reporter.log("A end technology values : " + A_endTechnolnogies);
					//sa.assertTrue(match,"");
				}
			}
			

		}
		} catch (AssertionError error) {

		  error.printStackTrace();
		
		}catch(Exception e) {
			Reporter.log("Dropdwon values inside A-end technology are mismatching");
			Reporter.log("Dropdwon values inside A-end technology are mismatching");
		}

	}
	
	public void verifyB_Endtechnologydropdowb() throws InterruptedException, IOException {

		try {
		String[] B_endTechnolnogylist = { "Atrica", "MMSP", "Ethernet over Fibre" };

		boolean B_Endtechnolnogy = isElementPresent(Lanlink_InternationalObj.International.B_Endtechnology);
	//	sa.assertTrue(B_Endtechnolnogy, "B-End technolnogy dropdown is not displayed");

		// verify the list of A-End technolnogies
		click(Lanlink_InternationalObj.International.B_Endtechnology);

		List<String> listofB_endTechnologies = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofB_endTechnologies)));
			
		for (String B_endTechnolnogies : listofB_endTechnologies) {

			boolean match = false;
			for (int i = 0; i < B_endTechnolnogylist.length; i++) {
				if (B_endTechnolnogies.equals(B_endTechnolnogylist[i])) {
					match = true;
					Reporter.log("B end technology values : " + B_endTechnolnogies);
					//sa.assertTrue(match,"");
				}
			}
			

		}
		} catch (AssertionError error) {

			  error.printStackTrace();
			
		}catch(Exception e) {
			Reporter.log("Dropdwon values inside B-end technology are mismatching");
			Reporter.log("Dropdwon values inside B-end technology are mismatching");
		}
	}
	
	
	public void verifyservicesubtypesdropdownwgenAutoCreatealoneselected() throws InterruptedException, IOException {

		try {
		String[] servicesubtypelist = { "Direct Fiber", "LANLink International", "LANLink Metro", "LANLink National","LANLink Outband Management", "OLO - (GCR/EU)" };

		boolean servicesubtypesdropdown = isElementPresent(Lanlink_InternationalObj.International.ServiceSubtype);
				
	//	sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

		// verify the list of service sub types
		click(Lanlink_InternationalObj.International.ServiceSubtype);

		List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofServicesubtypes)));
		for (String servicesubtypes : listofServicesubtypes) {

			boolean match = false;
			for (int i = 0; i < servicesubtypelist.length; i++) {
				if (servicesubtypes.equals(servicesubtypelist[i])) {
					match = true;
					Reporter.log("service sub types : " + servicesubtypes);
					// sa.assertTrue(match,"");
				}
			}
			

		}
		} catch (AssertionError error) {

			  error.printStackTrace();
			
		}catch(Exception e) {
			Reporter.log("Dropdown values inside service subtypes are mismatching");
			Reporter.log("Dropdwon values inside service subtypes are mismatching");
		}

	}
	
	public void verifyservicesubtypesdropdownwhenMSPandAutoCreateselected
	   () throws InterruptedException, IOException {

	   	String[] servicesubtypelist = { "LANLink International", "LANLink Metro", "LANLink National", "OLO - (GCR/EU)" };

	   	try {
	   		boolean servicesubtypesdropdown = isElementPresent(Lanlink_InternationalObj.International.ServiceSubtype);
	   		
	   		//sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

	   		// verify the list of service sub types
	   		click(Lanlink_InternationalObj.International.ServiceSubtype);

	   		List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofServicesubtypes)));
	   		for (String servicesubtypes : listofServicesubtypes) {

	   			boolean match = false;
	   			for (int i = 0; i < servicesubtypelist.length; i++) {
	   				if (servicesubtypes.equals(servicesubtypelist[i])) {
	   					match = true;
	   					Reporter.log("service sub types : " + servicesubtypes);
	   				//	sa.assertTrue(match,"");
	   				}
	   			}
	   			

	   		}
	   	
	   	} catch (AssertionError error) {

	   		error.printStackTrace();
	   		
	       }catch(Exception e) {
	   		Reporter.log("Dropdown values in Service subtypes are mismatching");
	   		Reporter.log("Dropdown values in Service subtypes are mismatching");
	   	}

	   }
	
	public void selectCustomertocreateOrderfromleftpane(String ChooseCustomerToBeSelected) throws IOException, InterruptedException
	{
		
		
	//	String CustomerName="*";
		mouseMoveOn(Lanlink_InternationalObj.International.ManageCustomerServiceLink);
		waitforPagetobeenable();
		Reporter.log("Mouser hovered on Manage Customer's Service");

		click(Lanlink_InternationalObj.International.CreateOrderServiceLink,"Create Order/Service Link");	
		Reporter.log("=== Create Order/Service navigated ===");


		//Entering Customer name
			sendKeys(Lanlink_InternationalObj.International.entercustomernamefield, ChooseCustomerToBeSelected,"Customer Name");
		
		
		
	//	sendKeys(Lanlink_InternationalObj.International.entercustomernamefield,CustomerName,"Customer Name");
		
		
		waitToPageLoad();
		waitforPagetobeenable();

		//Select Customer from dropdown
		addDropdownValues_commonMethod("Choose a customer",Lanlink_InternationalObj.International.choosecustomerdropdown, ChooseCustomerToBeSelected);
			click(Lanlink_InternationalObj.International.nextbutton,"Next");
		
	}
	
	public void createorderservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderSelection");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderSelection");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		verifyExists(Lanlink_InternationalObj.International.nextbutton, "Next button");
		click(Lanlink_InternationalObj.International.nextbutton, "Next button");

		//verifyExists(Lanlink_InternationalObj.International.order_contractnumber_warngmsg, "order contract number");
		//verifyExists(Lanlink_InternationalObj.International.servicetype_warngmsg, "service type");

		if (neworder.equalsIgnoreCase("NO")) {
			verifyExists(Lanlink_InternationalObj.International.newordertextfield, "new order textfield");
			sendKeys(Lanlink_InternationalObj.International.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(Lanlink_InternationalObj.International.newrfireqtextfield, "new rfireq textfield");
			sendKeys(Lanlink_InternationalObj.International.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(Lanlink_InternationalObj.International.createorderbutton, "create order button");
			click(Lanlink_InternationalObj.International.createorderbutton, "create order button");

			verifyExists(Lanlink_InternationalObj.International.OrderCreatedSuccessMsg, "Order Created Success Msg");
			
			waitforPagetobeenable();

		} else if (existingorderservice.equalsIgnoreCase("NO")) {
			verifyExists(Lanlink_InternationalObj.International.selectorderswitch, "select order switch");
			click(Lanlink_InternationalObj.International.selectorderswitch, "select order switch");

			verifyExists(Lanlink_InternationalObj.International.existingorderdropdown, "existing order drop down");

			click(Lanlink_InternationalObj.International.existingorderdropdown, "existing order dropdown");
			// sendKeys(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,existingordernumber,"existingordernumber");

			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
					Lanlink_InternationalObj.International.existingorderdropdown, existingordernumber);
			waitforPagetobeenable();

		} else {
			Reporter.log("Order not selected");
		}

	}
	
	public void selectServiceType(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
		String serviceTypeToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");

		addDropdownValues_commonMethod("Service Type", Lanlink_InternationalObj.International.servicetypedropdowntoclick, serviceTypeToBeSelected);


	}
	
	
	public void selectsubtypeunderServiceTypeSelected(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
		
		String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Servicesubtype");
		String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
		String autoCreateService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AutocreateService");
		String A_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A_Endtechnology");
		String B_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_Endtechnology");
		
	//	scrolltoend();
		waitforPagetobeenable();


		if (modularmsp.equalsIgnoreCase("no") && autoCreateService.equalsIgnoreCase("no")) {
			
			ExtentTestManager.getTest().log(LogStatus.PASS,"when'Modular msp' and 'Autocreate service' are not selected,   'Interface speed' value and 'Service subtype' value should be selected as mandatory ");

			// Select interface speed
				addDropdownValues_commonMethod("Interface Speed", Lanlink_InternationalObj.International.InterfaceSpeed, Interfacespeed);
	
	
	// select service sub type
	boolean serviceSubTypeAvailability=false;
	serviceSubTypeAvailability=	findWebElement(Lanlink_InternationalObj.International.ServiceSubtype).isDisplayed();

	if(serviceSubTypeAvailability) {
		ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
		Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
		
		click(Lanlink_InternationalObj.International.ServiceSubtype,"ServiceSubtype");
		waitforPagetobeenable();
		Reporter.log("clicked on srvice type");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
		
		if((SelectSubService.equals("LANLink International")) || (SelectSubService.equals("LANLink Metro")) || SelectSubService.equals("LANLink National") ||
		         SelectSubService.equals("OLO - (GCR/EU)") || (SelectSubService.equals("Direct Fiber")) || (SelectSubService.equals("LANLink Outband Management"))){

		WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
		el2.click();
		Reporter.log("=== Service sub Type selected===");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type" +SelectSubService +" has been selected");
		}
		else{
			Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
			
			ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
					+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
					+ "    1) Direct Fiber"
					+ "    2) LANLink International"
					+ "    3) LANLink Metro"
					+ "    4) LANLink National"
					+ "    5) Lanlink Outband management"
					+ "    6) OLO - (GCR/EU)");
//			driver.close();
		}

	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
	}

			
//			 clickon(getwebelement(xml.getlocator("//locators/"+application+"/AvailableCircuits")));

			click(Lanlink_InternationalObj.International.Next,"Next");
			waitforPagetobeenable();

			Report.LogInfo("Info","Page has to be selected based on service and its subtype selected","Pass");

		}

		else if (modularmsp.equalsIgnoreCase("yes") && autoCreateService.equalsIgnoreCase("no")) {
			
			ExtentTestManager.getTest().log(LogStatus.INFO,"when 'Modular msp' is selected and 'Autocreateservice' is not selected, 'Service subtype' value should be selected as it is mandatory field ");
			
          try {
			click(Lanlink_InternationalObj.International.modularmspcheckbox,"modularmspcheckbox");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Modular msp checkbox has been selected");
          }catch(Exception e) {
        	  e.printStackTrace();
        	  ExtentTestManager.getTest().log(LogStatus.FAIL, "Modular msp check box is not available");
        	  
          }

	// select service sub type
      	boolean serviceSubTypeAvailability=false;
		serviceSubTypeAvailability=	findWebElement(Lanlink_InternationalObj.International.ServiceSubtype).isDisplayed();
	
		if(serviceSubTypeAvailability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
			Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
			
			click(Lanlink_InternationalObj.International.ServiceSubtype,"ServiceSubtype");
			waitforPagetobeenable();
			Reporter.log("clicked on srvice type");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
			
			if(SelectSubService.equals("LANLink International") || SelectSubService.equals("LANLink Metro") || SelectSubService.equals("LANLink National") ||
			         SelectSubService.equals("OLO - (GCR/EU)")){

			WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
			el2.click();
			Reporter.log("=== Service sub Type selected===");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type" +SelectSubService +" has been selected");
			}
			else{
				Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
				
				ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
						+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
						+ "    1) LANLink International"
						+ "    2) LANLink Metro"
						+ "    3) LANLink National"
						+ "    4) OLO - (GCR/EU)");
//				driver.close();
			}

		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
		}

//			 SendKeys(getwebelement(xml.getlocator("//locators/"+Application+"/AvailableCircuits")),
//			 Availablecircuits);

			click(Lanlink_InternationalObj.International.Next,"Next");
			waitforPagetobeenable();

			Reporter.log("Page has to be selected based on service and its subtype selected");

		}

		if (modularmsp.equalsIgnoreCase("no") && autoCreateService.equalsIgnoreCase("yes")) {
			
			
			ExtentTestManager.getTest().log(LogStatus.INFO, " 'Service subtype' should be selected as mandatory when 'AutocreateService' is selected, 'Modular msp' not selected");
			
			Reporter.log("Only auto creta check box is selected");
			
			try {
				waitforPagetobeenable();
			click(Lanlink_InternationalObj.International.AutocreateServicecheckbox,"AutocreateServicecheckbox");
			waitforPagetobeenable();
			ExtentTestManager.getTest().log(LogStatus.PASS, " 'Autocreateservice' checkbox is selected ");
			}catch(Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Auto create service' checkbox is not available under 'Service' panel ");
			}

	//A end technology	
		addDropdownValues_commonMethod("A-End Technology",Lanlink_InternationalObj.International.A_Endtechnology, A_Endtechnologydropdown);
		
	//B end technology	
		addDropdownValues_commonMethod("B-End Technology",Lanlink_InternationalObj.International.B_Endtechnology, B_Endtechnologydropdown);
		
	//Interface speed
		addDropdownValues_commonMethod("Interface Speed",Lanlink_InternationalObj.International.InterfaceSpeed, Interfacespeed);
		

	// select service sub type
		
		boolean serviceSubTypeAvailability=false;
		serviceSubTypeAvailability=	findWebElement(Lanlink_InternationalObj.International.ServiceSubtype).isDisplayed();
	
		if(serviceSubTypeAvailability) {
			ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
			Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
			
			click(Lanlink_InternationalObj.International.ServiceSubtype,"ServiceSubtype");
			waitforPagetobeenable();
			Reporter.log("clicked on srvice type");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
			
			if((SelectSubService.equals("LANLink International")) || (SelectSubService.equals("LANLink Metro")) || SelectSubService.equals("LANLink National") ||
			         SelectSubService.equals("OLO - (GCR/EU)") || (SelectSubService.equals("Direct Fiber")) || (SelectSubService.equals("LANLink Outband Management"))){

			WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
			el2.click();
			Reporter.log("=== Service sub Type selected===");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type " +SelectSubService +" has been selected");
			}
			else{
				Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
				
				ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
						+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
						+ "    1) Direct Fiber"
						+ "    2) LANLink International"
						+ "    3) LANLink Metro"
						+ "    4) LANLink National"
						+ "    5) Lanlink Outband management"
						+ "    6) OLO - (GCR/EU)");
				
//				driver.close();
			}

		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
		}
		//scrollDown(Lanlink_InternationalObj.International.Next);
		    verifyExists(Lanlink_InternationalObj.International.nextbutton,"Next Button");
			click(Lanlink_InternationalObj.International.nextbutton,"Next Button");
			waitforPagetobeenable();

			Report.LogInfo("INFO","Page has to be selected based on service and its subtype selected","PASS");

		}

		if (modularmsp.equalsIgnoreCase("yes") && autoCreateService.equalsIgnoreCase("yes")) {
			
			ExtentTestManager.getTest().log(LogStatus.INFO, " 'Service subtype' is mandatory when 'modular msp' and 'Autocreateservices' are selected");

			
		//modular msp	
			 try {
					click(Lanlink_InternationalObj.International.modularmspcheckbox,"modularmspcheckbox");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Modular msp checkbox has been selected");
	              }catch(Exception e) {
	            	  e.printStackTrace();
	            	  ExtentTestManager.getTest().log(LogStatus.FAIL, "Modular msp check box is not available under 'Service' panel");
	            	  
	              }

		//Auto create service	 
			 try {
					click(Lanlink_InternationalObj.International.AutocreateServicecheckbox,"AutocreateServicecheckbox");
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Autocreateservice' checkbox is selected ");
					}catch(Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Auto create service' checkbox is not available under 'Service' panel ");
					}

			//A end technology	
				addDropdownValues_commonMethod(Lanlink_InternationalObj.International.A_Endtechnology, A_Endtechnologydropdown, "A-End Technology");
				
			//B end technology	
				addDropdownValues_commonMethod(Lanlink_InternationalObj.International.B_Endtechnology, B_Endtechnologydropdown, "B-End Technology");
				
			
		// select service sub type
			boolean serviceSubTypeAvailability=false;
			serviceSubTypeAvailability=	findWebElement(Lanlink_InternationalObj.International.ServiceSubtype).isDisplayed();
		
			if(serviceSubTypeAvailability) {
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
				Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
				
				click(Lanlink_InternationalObj.International.ServiceSubtype,"ServiceSubtype");
				waitforPagetobeenable();
				Reporter.log("clicked on srvice type");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
				
				if(SelectSubService.equals("LANLink International") || SelectSubService.equals("LANLink Metro") || SelectSubService.equals("LANLink National") ||
				         SelectSubService.equals("OLO - (GCR/EU)")){

				WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
				el2.click();
				Reporter.log("=== Service sub Type selected===");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type" +SelectSubService +" has been selected");
				}
				else{
					Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
					
					ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
							+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
							+ "    1) LANLink International"
							+ "    2) LANLink Metro"
							+ "    3) LANLink National"
							+ "    4) OLO - (GCR/EU)");
//					driver.close();
				}

			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
			}
			
scrollDown(Lanlink_InternationalObj.International.nextbutton);
verifyExists(Lanlink_InternationalObj.International.nextbutton,"nextbutton");
			click(Lanlink_InternationalObj.International.nextbutton,"nextbutton");
			waitforPagetobeenable();

			Report.LogInfo("INFO","Page has to be selected based on service and its subtype selected","PASS");

		}

	}
	
	public void VerifyFieldsForServiceSubTypeSelected(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
		
		String serviceType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Servicesubtype");
		String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String proActivemonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Notificationmanagement");
		String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
		String vpntopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");



		if (modularmsp.equalsIgnoreCase("no")) {
			if (Interfacespeed.equalsIgnoreCase("10GigE")) {
			Fieldvalidation_DirectFibre10G(serviceType, SelectSubService, Interfacespeed,vpntopology);

			} else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
				Fieldvalidation_DirectFibre1G( serviceType, SelectSubService, Interfacespeed ,proActivemonitoring, vpntopology);
			}
		}
		else if (modularmsp.equalsIgnoreCase("yes")) {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Direct Fiber' service will not occur when 'Modular Msp'checkbox is selected is selected");
		}

	}
	
	
	
	public void Fieldvalidation_DirectFibre1G(String serviceType, String SelectSubService,
			String Interfacespeed,String proActivemonitoring, String vpntopology) throws InterruptedException, IOException {

		
		String[] deliverychannel = { "--", "Retail", "Reseller" , "WLC", "WLEC", "CES Solutions"};
		
		String[] VPNtopology = { "Point-to-Point", "Hub&Spoke", "E-PN (Any-to-Any)" };

		String[] notifyManagement= {"DNA"}; 
		 

		boolean serviceIdentificationField, ServiceType, ServiceSubtype,interfacespeedvalue, singleendpointCPE, email,
				phone, remark, performancereoprting, deliveryChanel, proactiveMonitor, Managementorder,vpnTopology,
				intermediateTechnology, circuitref, circuitType, cancelButton;

		try {	
			verifyExists(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
			click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'OK' butto to verify mandatory warning Messages");
			Reporter.log("Clicked on 'OK' butto to verify mandatory warning Messages");
			waitforPagetobeenable();

			
			//Circuit Reference Error message
			verifyExists(Lanlink_InternationalObj.International.circuitreferenceErrmsg, "Circuit Reference");
			
			scrollUp();
			waitforPagetobeenable();
			
	//service Identification	
		serviceIdentificationField = isElementPresent(Lanlink_InternationalObj.International.ServiceIdentification);
	//	sa.assertTrue(serviceIdentificationField, "Service identification field is not displayed");
		if(serviceIdentificationField) {
			Report.LogInfo("Info",  " ' Service Identfication' mandatory field is displaynig under 'Add Service' page as expected","PASS");
		}else {
			Report.LogInfo("Info",  " ' Service Identfication' mandatory field is not available under 'Add Service' page","FAIL");
		}
		   

	 //Service type  
		ServiceType = isElementPresent(Lanlink_InternationalObj.International.ServiceType);
		waitforPagetobeenable();
	//	sa.assertTrue(ServiceType, "Service type is not displayed");
		if(ServiceType) {
			Report.LogInfo("Info",  " 'LANLink' is displying under 'Service type' as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'LANLink' is not displying under 'Service type'","FAIL");
		}
		
	//Service subtype
		ServiceSubtype = webDriver.findElement(By.xpath("//div[contains(text(),'"+SelectSubService+"')]")).isDisplayed();
	//	sa.assertTrue(ServiceSubtype, "Service subtype is not displayed");
		if(ServiceSubtype) {
			Report.LogInfo("Info",  SelectSubService + " is displying under 'Service Sub type' as expected","PASS");
		}else {
			Report.LogInfo("Info",  SelectSubService + " is not displying under 'Service Sub type'","FAIL");
		}
		

	//Interface speed 	
		interfacespeedvalue = webDriver.findElement(By.xpath("//div[contains(text(),'" + Interfacespeed + "')]")).isDisplayed();	   
//		sa.assertTrue(interfacespeedvalue, "Interface speed dropdown is not displaying as expected");
		if(interfacespeedvalue) {
			Report.LogInfo("Info",  Interfacespeed + " is displying under 'Interface Speed' as expected","PASS");
		}else {
			Report.LogInfo("Info",  Interfacespeed + " is not displying under 'Interface Speed'","FAIL");
		}

	//Single endpoint cpe	
		try {
		singleendpointCPE = isElementPresent(Lanlink_InternationalObj.International.EndpointCPE);
//		sa.assertTrue(singleendpointCPE, "single End point CPE checkbox is disabled by default");
		if(singleendpointCPE) {
			Report.LogInfo("Info",  " 'Single endpoint cpe' field is displying under 'Create Service'page as expected","PASS");
		}else {
			Report.LogInfo("Info",   " 'Single endpoint cpe' field is not available under 'Create Service' page","FAIL");
		}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "'Single endpoint cpe' checkbox is not available under 'Create Service' page","FAIL");
		}

	//Email	
		try {
		email = isElementPresent(Lanlink_InternationalObj.International.Email);
	//	sa.assertTrue(email, "email field is not displayed");
		if(email) {
		//	Report.LogInfo("Info",  " 'Email' field is displying under 'Create Service'page as expected","PASS");
		}else {
		//	Report.LogInfo("Info",  " 'Email' field is not available under 'Create Service' pag","PASS");
		}
		}catch(Exception e) {
			e.printStackTrace();
		//	Report.LogInfo("Info", "'Email' field is not available under 'Create Service' page","FAIL");
		}

		
	//phone	
		try {
		phone = isElementPresent(Lanlink_InternationalObj.International.PhoneContact);
	//	sa.assertTrue(phone, "phone contact field is not displayed");
		if(phone) {
			Report.LogInfo("Info",  " 'phone' field is displying under 'Create Service'page as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'phone' field is not available under 'Create Service' page","FAIL");
		}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  "'Phone Contact' field is not available under 'Create Service' page","FAIL");
		}
		
	//remark
		try {
		remark = isElementPresent(Lanlink_InternationalObj.International.Remark);
	//	sa.assertTrue(remark, "remark field is not displayed");
		if(remark) {
			Report.LogInfo("Info",  " 'Remark' field is displying under 'Create Service'page as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'Remark' field is not available under 'Create Service' page","FAIL");
		}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  "'Remark' field is not available under 'Create Service' page","FAIL");
		}
		
		
		scrollDown(Lanlink_InternationalObj.International.configurtoinptionpanel_webelementToScroll);
		
	//performance Reporting	
	
		performancereoprting = isElementPresent(Lanlink_InternationalObj.International.performanceReportingcheckbox);
	//	sa.assertTrue(performancereoprting,	"performance monitoring checbox is not displayed and by default not selected as expected");
		if(performancereoprting) {
			Report.LogInfo("Info",  " 'performance Reporting' checkbox is displying under 'Create Service'page as expected","PASS");
			boolean performancereoprtingselection = findWebElement(Lanlink_InternationalObj.International.performanceReportingcheckbox).isSelected();
			if(performancereoprtingselection) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'"); 
			}else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected");
		
				
			//Per CoS Performance Reporting
				verifyExists(Lanlink_InternationalObj.International.performanceReportingcheckbox,"performanceReportingcheckbox");

				click(Lanlink_InternationalObj.International.performanceReportingcheckbox,"performanceReportingcheckbox");
				waitforPagetobeenable();
				
				boolean perCosreopt=findWebElement(Lanlink_InternationalObj.International.perCoSperformncereport).isDisplayed();
			//	sa.ass//ertTrue(perCosreopt,
					//	"Per CoS Performance Reporting checbox is not displayed under 'Create Service' page, when 'Performance Reporting' checkbox is selected");
				if(perCosreopt) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Per CoS Performance Reporting checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected");
				
					boolean perCoSselection=findWebElement(Lanlink_InternationalObj.International.perCoSperformncereport).isSelected();
					if(perCoSselection) {
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Per CoS Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'"); 
					}else {
						ExtentTestManager.getTest().log(LogStatus.PASS, "Per CoS Performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected");
				
				//Actelis Based 
						verifyExists(Lanlink_InternationalObj.International.perCoSperformncereport,"perCoSperformncereport");	

						click(Lanlink_InternationalObj.International.perCoSperformncereport,"perCoSperformncereport");	
						waitforPagetobeenable();
						boolean actelisBasedcheckbox=findWebElement(Lanlink_InternationalObj.International.ActelisBasedcheckbox).isDisplayed();
					//	sa.assertTrue(actelisBasedcheckbox,
							//	" 'Atelis Based' checbox is not displayed under 'Create Service' page, when 'Per CoS Performance Reporting' checkbox is selected");
						if(actelisBasedcheckbox) {
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Actelis Based' checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected");
						
						boolean actelisBasedselection=findWebElement(Lanlink_InternationalObj.International.ActelisBasedcheckbox).isSelected();
						if(actelisBasedselection) {
							ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Actelis Based' checkbox is selected by default under 'Management Options' panel in 'Create Service page'"); 
						}else {
							ExtentTestManager.getTest().log(LogStatus.PASS, "Actelis Based' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected");
						
							
					//Bandwidth Options table
							click(Lanlink_InternationalObj.International.ActelisBasedcheckbox,"ActelisBasedcheckbox");	
							waitforPagetobeenable();
						
						//Standard CIR text field
						boolean standardCIR=findWebElement(Lanlink_InternationalObj.International.standardCIRtextfield).isDisplayed();
						//sa.assertTrue(standardCIR,
								//" 'Standard CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
						if(standardCIR) {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Standard CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}else {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Standard CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}
						
						
						//Standard EIR Text field
						boolean standardEIR=findWebElement(Lanlink_InternationalObj.International.standardEIRtextfield).isDisplayed();
					//	sa.assertTrue(standardEIR,
								//" 'Standard EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
						if(standardEIR) {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Standard EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}else {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Standard EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}
						
						
						//Premium CIR text field
						boolean premiumCIR=findWebElement(Lanlink_InternationalObj.International.premiumCIRtextfield).isDisplayed();
					//	sa.assertTrue(premiumCIR,
							//	" 'Premium CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
						if(premiumCIR) {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Premium CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}else {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Premium CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}
						
						
						//Premium EIR Text field
						boolean premiumEIR=findWebElement(Lanlink_InternationalObj.International.premiumEIRtextfield).isDisplayed();
					//	sa.assertTrue(premiumEIR,
					//			" 'Premium EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
						if(premiumEIR) {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Premium EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}else {
							ExtentTestManager.getTest().log(LogStatus.PASS," 'Premium EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected");
						}
						
				 }
				}else {
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Actelis Based' checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected");
				}
			 }
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Per CoS Performance Reporting checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected");
	    	 }
			}
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'performance Reporting' checkbox is not available under 'Create Service' page");		
			}
			
			
		//proactive monitoring			
			proactiveMonitor = findWebElement(Lanlink_InternationalObj.International.proactiveMonitoring)
					.isDisplayed();
		//	sa.assertTrue(proactiveMonitor,
				//	"pro active monitoring checkbox is not displayed");
			if(proactiveMonitor) {
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'proactive monitoring' checkbox is displying under 'Create Service'page as expected");
			
			
			boolean proactiveMonitorselection = findWebElement(Lanlink_InternationalObj.International.proactiveMonitoring).isSelected();
			if(proactiveMonitorselection) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'proactive monitoring' checkbox is selected under 'Management Options' panel in 'Create Service page'"); 
			}else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "proactive monitoring' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected");
			
		
		//Notification Management Dropdown	
			click(Lanlink_InternationalObj.International.proactiveMonitoring,"proactiveMonitoring");
			Reporter.log("Pro active monitoring check box is selected");
			waitforPagetobeenable();
		
		boolean notificationManagement=findWebElement(Lanlink_InternationalObj.International.notificationmanagement)
		.isDisplayed();
	//	sa.assertTrue(notificationManagement, "Notification management dropdown is not displayed when proactive monitoring is selected");
		Reporter.log("Notification management dropdown gets displayed when proactive monitoring is selected");
		if(notificationManagement) {
			ExtentTestManager.getTest().log(LogStatus.PASS, " 'Notification Management' dropdown is displaying under 'Management Options' panel when 'proactive Monitoring' checkbox is selected");
		
		click(Lanlink_InternationalObj.International.notificationmanagement,"notificationmanagement");
		try {
			List<WebElement> listofnotificationmanagement = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement notificationmanagementtypes : listofnotificationmanagement) {

				boolean match = false;
				for (int i = 0; i < notifyManagement.length; i++) {
					if (notificationmanagementtypes.getText().equals(notifyManagement[i])) {
						match = true;
						Reporter.log("list of notification management are : " + notificationmanagementtypes.getText());
						ExtentTestManager.getTest().log(LogStatus.PASS,"list of notification management are : " + notificationmanagementtypes.getText());
					}
				}
			//	sa.assertTrue(match);
				
			}
	      }catch(Exception e) {
	    	  Reporter.log("Notification Management dropdown values are mismatching");
	    	  e.printStackTrace();
	    	  ExtentTestManager.getTest().log(LogStatus.FAIL,"  values in Notification management dropdown under Direct Fiber service subtype is not displaying as expected");
	      }
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected");
			}
			}
		}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'proactive monitoring' checkbox is not available under 'Create Service' page");
			}
		
		
	//delivery channel
		try {	
		deliveryChanel = findWebElement(Lanlink_InternationalObj.International.deliverychannel_withclasskey).isDisplayed();
		//	sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
			if(deliveryChanel) {
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'Delivery Channel' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected");
			
			click(Lanlink_InternationalObj.International.deliverychannel_withclasskey,"deliverychannel_withclasskey");
	    try {
			List<WebElement> listofdeliverychannel = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement deliverychanneltypes : listofdeliverychannel) {

				boolean match = false;
				for (int i = 0; i < deliverychannel.length; i++) {
					if (deliverychanneltypes.getText().equals(deliverychannel[i])) {
						match = true;
						Reporter.log("list of delivery channel are : " + deliverychanneltypes.getText());
						ExtentTestManager.getTest().log(LogStatus.INFO," List of Delivery channel dropdown values under Direct Fiber service subtype are: "+deliverychanneltypes.getText());	
						
					}
				}
				//sa.assertTrue(match);
			}
		}catch(Exception e) {
	    	e.printStackTrace();
	    	Reporter.log("delivery channel dropdown values are mismatching");
	    	ExtentTestManager.getTest().log(LogStatus.FAIL,"  values in delivery channel dropdowns under Direct Fiber service subtype are not displaying as expected");
	    }
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Delivery Channel' dropdown is not avilable under 'Management options' panel in 'Create Service' page");
			}
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Delivery Channel' dropdown is not available under 'Create Service' page");
		}	

			
	    //Management Order
	try {	
			Managementorder = findWebElement(Lanlink_InternationalObj.International.managementOrder).isDisplayed();
		//	sa.assertTrue(Managementorder, "Management order field is not displayed");
			if(Managementorder) {
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'Management Order' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected");
			
			click(Lanlink_InternationalObj.International.managementOrder,"managementOrder");
			
			try {
			List<WebElement> listofmanagementOrder = findWebElements(Lanlink_InternationalObj.International.ClassNameForDropdowns);
			for (WebElement mnaagementOrdertypes : listofmanagementOrder) {
				
				//Reporter.log("Available Management Order name is : " + mnaagementOrdertypes.getText().toString());
				//ExtentTestManager.getTest().log(LogStatus.PASS,
					//	"Available Management Order name is : " + mnaagementOrdertypes.getText().toString());
				//Reporter.log("Available Management Order name is :" + mnaagementOrdertypes.getText().toString());
				
			}
			}catch(Exception e) {
				e.printStackTrace();
				//Reporter.log(" 'Management Order' dropdown values are mismatching");
			}
			}else {
				//ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Management Order' dropdown is not available under 'Management options' panel in 'Create Service' page");
			}
	}catch(Exception e) {
		e.printStackTrace();
	//	ExtentTestManager.getTest().log(LogStatus.FAIL, "'Management Order' dropdown is not available under 'Create Service' page");
	}	
			
			
			scrollDown(Lanlink_InternationalObj.International.VPNtopology);
			waitforPagetobeenable();
			
	    //VPN topology
			vpnTopology = findWebElement(Lanlink_InternationalObj.International.VPNtopology).isDisplayed();
			//sa.assertTrue(vpnTopology, "vpn topology dropdown is not displayed");
			if(vpnTopology) {
				ExtentTestManager.getTest().log(LogStatus.PASS, " VPN Topology' dropdown is displaying under 'Configuration Options' panel in 'Create Service' page as expected");
				Reporter.log("VPN topology dropdown is displaying as expected");
				
			//Check default values present inside VPN Topology dropdown		
				boolean defaultTOpologValues=webDriver.findElement(By.xpath("//span[contains(text(),'Point-to-Point')]")).isDisplayed();
				if(defaultTOpologValues) {
					ExtentTestManager.getTest().log(LogStatus.PASS, " Under 'VPN Topology' dropdown, 'Point-to-Point' is displaying by default as expected");
					Reporter.log("The default topology value is displaying as :"+defaultTOpologValues);
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Under 'VPN Topology' dropdown, 'Point-to-Point' is not displaying by default");
					Reporter.log(" Under 'VPN Topology' dropdown, 'Point-to-Point' is not displaying by default");
				}
		
				
				click(Lanlink_InternationalObj.International.IntermediateTechnology,"IntermediateTechnology");
				waitforPagetobeenable();
				
				scrollDown(Lanlink_InternationalObj.International.VPNtopology_xbutton);
				waitforPagetobeenable();
				
		//find list of values under VPN topology dropdown	
			click(Lanlink_InternationalObj.International.VPNtopology_xbutton,"VPNtopology_xbutton");
			
			try {
			List<WebElement> listofvpntopology = findWebElements(Lanlink_InternationalObj.International.ClassNameForDropdowns);
			for (WebElement vpntopologytyeps : listofvpntopology) {

				boolean match = false;
				for (int i = 0; i < VPNtopology.length; i++) {
					if (vpntopologytyeps.getText().equals(VPNtopology[i])) {
						match = true;
						Reporter.log("list of vpn topologies are : " + vpntopologytyeps.getText());
						Reporter.log("list of vpn topologies: "+vpntopologytyeps.getText());
						ExtentTestManager.getTest().log(LogStatus.PASS,"list of vpn topologies: "+vpntopologytyeps.getText());
						
					}
				}
				//sa.assertTrue(match);
			}
			}catch(Exception e) {
				e.printStackTrace();
				Reporter.log("vpn topology dropdown values are mismatching");
			}
			
			
		for(int i=0; i<VPNtopology.length;i++) {

			scrollDown(Lanlink_InternationalObj.International.VPNtopology);
			waitforPagetobeenable();
		
			Reporter.log("VPN Toplogy length is: "+ VPNtopology.length);
			Reporter.log(VPNtopology[i]+ " is the values going to pass inside vpn topology dropdown");
			
			
			if(VPNtopology[i].equals("E-PN (Any-to-Any)")) {
				click(Lanlink_InternationalObj.International.VPNtopology,"VPNtopology");
				waitforPagetobeenable();
				webDriver.findElement(By.xpath("//div[contains(text(),'"+VPNtopology[i] +"')]")).click();
				waitforPagetobeenable();
				
				Reporter.log("Under 'VPN Topology', When 'E-PN (Any-to-Any)' is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
						+ "only Circuit reference field should occur ");
				
				ExtentTestManager.getTest().log(LogStatus.INFO,"Under 'VPN Topology', When'E-PN (Any-to-Any)' is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
						+ "Only circuit reference should display ");
				
				circuitref = findWebElement(Lanlink_InternationalObj.International.circuitReference).isDisplayed();
			//	sa.assertTrue(circuitref, "circuit reference field is not displayed");
				if(circuitref) {
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'");
				
				
				click(Lanlink_InternationalObj.International.circuitReference,"circuitReference");
				
				boolean CircuitReferencepopupalertmsg=findWebElement(Lanlink_InternationalObj.International.circuitreferencealertmessage).isDisplayed();
				
			    while(CircuitReferencepopupalertmsg)	{
				String text=getTextFrom(Lanlink_InternationalObj.International.circuitreferencealertmessage);
				Reporter.log("The alert popup when circuit reference field is clicked is: " + text);
				ExtentTestManager.getTest().log(LogStatus.PASS,"on clicking circuit reference field, alert popup message displays as: "+text);
				
				CircuitReferencepopupalertmsg=false;
				click(Lanlink_InternationalObj.International.xButton,"xButton");
				waitforPagetobeenable();
			    }	
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'");
			}
					    
			}
			
			else if(VPNtopology[i].equals("Hub&Spoke")) {

				click(Lanlink_InternationalObj.International.VPNtopology,"VPNtopology");
				waitforPagetobeenable();
				webDriver.findElement(By.xpath("//div[contains(text(),'"+VPNtopology[i] +"')]")).click();
				waitforPagetobeenable();
				
				Reporter.log("Under 'VPN Topology', When 'Hub&Spoke'is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
						+ "only Circuit reference field should occur ");
				
				ExtentTestManager.getTest().log(LogStatus.INFO,"Under 'VPN Topology', When 'Hub&Spoke' is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
						+ "Only circuit reference should display ");
				
				circuitref = findWebElement(Lanlink_InternationalObj.International.circuitReference).isDisplayed();
			//	sa.assertTrue(circuitref, "circuit reference field is not displayed");
				if(circuitref) {
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected,  when 'VPN Topology' selected as 'HUb & Spoke'");
				
				
				
				click(Lanlink_InternationalObj.International.circuitReference,"circuitReference");
				
				boolean CircuitReferencepopupalertmsg=findWebElement(Lanlink_InternationalObj.International.circuitreferencealertmessage).isDisplayed();
				
			    while(CircuitReferencepopupalertmsg)	{
				String text=getTextFrom(Lanlink_InternationalObj.International.circuitreferencealertmessage);
				Reporter.log("The alert popup when circuit reference field is clicked is: " + text);
				ExtentTestManager.getTest().log(LogStatus.PASS,"on clicking circuit reference field, alert popup message displays as: "+text);
				
				CircuitReferencepopupalertmsg=false;
				click(Lanlink_InternationalObj.International.xButton,"xButton");
				waitforPagetobeenable();
			    }	 
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'HUb & Spoke'");
			}		    
			
			}
		else if(VPNtopology[i].equals("Point-to-Point")) {
			
			click(Lanlink_InternationalObj.International.VPNtopology,"VPNtopology");
			waitforPagetobeenable();
			webDriver.findElement(By.xpath("//div[text()='"+ VPNtopology[i] + "']")).click();
			waitforPagetobeenable();
			
			
			 
			ExtentTestManager.getTest().log(LogStatus.INFO,"under VPN topology dropdown for Direct Fiber service subtype"
						+ " When'Point-to-Point' is selected,'Circuit type' , 'Intermediate technology', 'Circuit Reference' should get displayed");

			
		
		//Intermediate technology field	
	      try {	
			click(Lanlink_InternationalObj.International.IntermediateTechnology,"IntermediateTechnology");
				intermediateTechnology = findWebElement(Lanlink_InternationalObj.International.IntermediateTechnology)
						.isDisplayed();
			//	sa.assertTrue(intermediateTechnology, "intermediate technology field is not displayed");
				if(intermediateTechnology) {
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Intermediate technology' field is displaying under 'Configuration Options' panel in 'Create Service' page, when 'VPN Topology' selected as 'Point-to-point'");
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'");
				}
	      }catch(Exception e) {
	    	  e.printStackTrace();
	    	  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'");
	      }
				
			//Circuit Reference 	
				circuitref = findWebElement(Lanlink_InternationalObj.International.circuitReference).isDisplayed();
			//	sa.assertTrue(circuitref, "circuit reference field is not displayed");
				if(circuitref) {
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected, when 'VPN Topology' selected as 'point-to-point'");
					
				click(Lanlink_InternationalObj.International.circuitReference,"circuitReference");
				
				boolean CircuitReferencepopupalertmsg=findWebElement(Lanlink_InternationalObj.International.circuitreferencealertmessage).isDisplayed();
				
				
			    while(CircuitReferencepopupalertmsg)	{
				String text=getTextFrom(Lanlink_InternationalObj.International.circuitreferencealertmessage);
				Reporter.log("The alert popup on clicking circuit reference field is : " + text);
				ExtentTestManager.getTest().log(LogStatus.PASS," on clicking 'Circuit reference' , alert emssage popup as : "+ text);
				
				CircuitReferencepopupalertmsg=false;
			    }
			    
				click(Lanlink_InternationalObj.International.xButton,"xButton");
				waitforPagetobeenable();
		}else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'Point-to-Point'");
		}			
				
			
			
		//Circuit type field		
			try {
			List<WebElement> listofcircuittypes = webDriver
					.findElements(By.xpath("//div[@class='div-border div-margin container']//div[@class='row'][3]//span"));
			
			for (WebElement CircuitTypes : listofcircuittypes) {
				
				Reporter.log("list of circuit types are : " + CircuitTypes.getText());
					ExtentTestManager.getTest().log(LogStatus.PASS,"list of circuit types displaying are:  "+CircuitTypes.getText());
					
					Reporter.log("list of circuit types displaying are:  "+ CircuitTypes.getText());

			}
			}catch(Exception e) {
				e.printStackTrace();
				Reporter.log("Circuit type values are mismatching");
				ExtentTestManager.getTest().log(LogStatus.FAIL,"list of circuit type values are mismatching");
				
			}
		}
			}
			
			}else {
				 ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page");
			 }
			
		//	boolean	okButton = findWebElement(Lanlink_InternationalObj.International.okbutton).isDisplayed();
			//sa.assertTrue(okButton, "OK button is not displayed");
			
			
			//	cancelButton = findWebElement(Lanlink_InternationalObj.International.cancelButton).isDisplayed();
		//	sa.assertTrue(cancelButton, "Cancel button is not displayed");
			
			scrollDown(Lanlink_InternationalObj.International.cancelButton);
			waitforPagetobeenable();
			
			click(Lanlink_InternationalObj.International.cancelButton,"cancelButton");
			waitforPagetobeenable();
			
		//	sa.assertAll();
			ExtentTestManager.getTest().log(LogStatus.PASS," Fields under Direct Fiber service subtype is verified");
	       }catch(AssertionError e) {
	    	 e.printStackTrace();
//	    	 ExtentTestManager.getTest().log(LogStatus.FAIL, "validation failure for Direct Fiber service subtype");
	       }	
			

		
		}
	
	public void Fieldvalidation_DirectFibre10G(String serviceType, String SelectSubService,
			String Interfacespeed,String vpntopology) throws InterruptedException, IOException {

		
		String[] deliverychannel = { "--", "Retail", "Reseller" , "WLC", "WLEC", "CES Solutions"};

		String[] notifyManagement= {"DNA"}; 
		 

		boolean serviceIdentificationField, ServiceType, ServiceSubtype,interfacespeedvalue, singleendpointCPE, email,
				phone, remark, performancereoprting, deliveryChanel, proactiveMonitor, Managementorder,  cancelButton;

		try {	
		
			webDriver.findElement(By.xpath("//span[contains(text(),'OK')]")).click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'OK' butto to verify mandatory warning Messages");
			Reporter.log("Clicked on 'OK' butto to verify mandatory warning Messages");
			waitforPagetobeenable();

			
		// management Connection Error Message
		//	verifyExists(Lanlink_InternationalObj.International.manageConnection_warningMesage, "Management Connection");
			
	//service Identification	
		serviceIdentificationField = isElementPresent(Lanlink_InternationalObj.International.ServiceIdentification);
	//	sa.assertTrue(serviceIdentificationField, "Service identification field is not displayed");
		if(serviceIdentificationField) {
			Report.LogInfo("Info",  " ' Service Identfication' mandatory field is displaynig under 'Add Service' page as expected","PASS");
		}else {
			Report.LogInfo("Info",  " ' Service Identfication' mandatory field is not available under 'Add Service' page","FAIL");
		}
		   

	 //Service type  
		ServiceType = isElementPresent(Lanlink_InternationalObj.International.ServiceType);
		waitforPagetobeenable();
	//	sa.assertTrue(ServiceType, "Service type is not displayed");
		if(ServiceType) {
			Report.LogInfo("Info",  " 'LANLink' is displying under 'Service type' as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'LANLink' is not displying under 'Service type'","FAIL");
		}
		
	//Service subtype
		ServiceSubtype = isElementPresent(Lanlink_InternationalObj.International.fieldValidation1 + SelectSubService + Lanlink_InternationalObj.International.fieldValidation2);

	//	sa.assertTrue(ServiceSubtype, "Service subtype is not displayed");
		if(ServiceSubtype) {
			Report.LogInfo("Info",  SelectSubService + " is displying under 'Service Sub type' as expected","PASS");
		}else {
			Report.LogInfo("Info",  SelectSubService + " is not displying under 'Service Sub type'","FAIL");
		}
		

	//Interface speed 	
	   interfacespeedvalue=isElementPresent(Lanlink_InternationalObj.International.fieldValidation1 + Interfacespeed +Lanlink_InternationalObj.International.fieldValidation2);
	   
//		sa.assertTrue(interfacespeedvalue, "Interface speed dropdown is not displaying as expected");
		if(interfacespeedvalue) {
			Report.LogInfo("Info",  Interfacespeed + " is displying under 'Interface Speed' as expected","PASS");
		}else {
			Report.LogInfo("Info",  Interfacespeed + " is not displying under 'Interface Speed'","FAIL");
		}

	//Single endpoint cpe	
		try {
		singleendpointCPE = isElementPresent(Lanlink_InternationalObj.International.EndpointCPE);
//		sa.assertTrue(singleendpointCPE, "single End point CPE checkbox is disabled by default");
		if(singleendpointCPE) {
			Report.LogInfo("Info",  " 'Single endpoint cpe' field is displying under 'Create Service'page as expected","PASS");
		}else {
			Report.LogInfo("Info",   " 'Single endpoint cpe' field is not available under 'Create Service' page","FAIL");
		}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "'Single endpoint cpe' checkbox is not available under 'Create Service' page","FAIL");
		}

	//Email	
		try {
		email = isElementPresent(Lanlink_InternationalObj.International.Email);
	//	sa.assertTrue(email, "email field is not displayed");
		if(email) {
		//	Report.LogInfo("Info",  " 'Email' field is displying under 'Create Service'page as expected","PASS");
		}else {
		//	Report.LogInfo("Info",  " 'Email' field is not available under 'Create Service' pag","PASS");
		}
		}catch(Exception e) {
			e.printStackTrace();
		//	Report.LogInfo("Info", "'Email' field is not available under 'Create Service' page","FAIL");
		}

		
	//phone	
		try {
		phone = isElementPresent(Lanlink_InternationalObj.International.PhoneContact);
	//	sa.assertTrue(phone, "phone contact field is not displayed");
		if(phone) {
			Report.LogInfo("Info",  " 'phone' field is displying under 'Create Service'page as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'phone' field is not available under 'Create Service' page","FAIL");
		}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  "'Phone Contact' field is not available under 'Create Service' page","FAIL");
		}
		
	//remark
		try {
		remark = isElementPresent(Lanlink_InternationalObj.International.Remark);
	//	sa.assertTrue(remark, "remark field is not displayed");
		if(remark) {
			Report.LogInfo("Info",  " 'Remark' field is displying under 'Create Service'page as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'Remark' field is not available under 'Create Service' page","FAIL");
		}
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  "'Remark' field is not available under 'Create Service' page","FAIL");
		}
		
		
		//scrollDown(Lanlink_InternationalObj.International.configurtoinptionpanel-webelementToScroll);
		
	//performance Reporting	
	 try {	
		performancereoprting = isElementPresent(Lanlink_InternationalObj.International.performanceReportingcheckbox);
	//	sa.assertTrue(performancereoprting,	"performance monitoring checbox is not displayed and by default not selected as expected");
		if(performancereoprting) {
			Report.LogInfo("Info",  " 'performance Reporting' checkbox is displying under 'Create Service'page as expected","PASS");
		}else {
			Report.LogInfo("Info", " 'performance Reporting' checkbox is not available under 'Create Service' page","FAIL");
		}
		
		boolean performancereoprtingselection = isSelected(Lanlink_InternationalObj.International.performanceReportingcheckbox,"Performance reporting checkbox");
		if(performancereoprtingselection) {
			Report.LogInfo("Info",  " 'Performance Reporting' checkbox is selected under 'Management Options' panel in 'Create Service page'","FAIL"); 
		}else {
			Report.LogInfo("Info",  "performance Reporting' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected","PASS");
		}
	 }catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  "'Performance reporting' checkbox is not available under 'Create Service' page","FAIL");
		}	
		
		
	//proactive monitoring	
		proactiveMonitor = isElementPresent(Lanlink_InternationalObj.International.proactiveMonitoring);
	//	sa.assertTrue(proactiveMonitor,"pro active monitoring checkbox is not displayed");
		if(proactiveMonitor) {
			Report.LogInfo("Info",  " 'proactive monitoring' checkbox is displying under 'Create Service'page as expected","PASS");
		
		
		boolean proactiveMonitorselection = isSelected(Lanlink_InternationalObj.International.proactiveMonitoring,"Proactive monitoring");
		if(proactiveMonitorselection) {
			Report.LogInfo("Info",  " 'proactive monitoring' checkbox is selected under 'Management Options' panel in 'Create Service page'","FAIL"); 
		}else {
			Report.LogInfo("Info",  "proactive monitoring' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected","PASS");
		

	//Notification Management Dropdown	
		click(Lanlink_InternationalObj.International.proactiveMonitoring,"Proactive monitoring");
		Reporter.log("Pro active monitoring check box is selected");
		waitforPagetobeenable();

	boolean notificationManagement=isElementPresent(Lanlink_InternationalObj.International.notificationmanagement);
//	sa.assertTrue(notificationManagement, "Notification management dropdown is not displayed when proactive monitoring is selected");
	Reporter.log("Notification management dropdown gets displayed when proactive monitoring is selected");

	if(notificationManagement) {
		Report.LogInfo("Info",  " 'Notification Management' dropdown is displaying under 'Management Options' panel when 'proactive Monitoring' checkbox is selected","PASS");

	click(Lanlink_InternationalObj.International.notificationmanagement);
	try {
		List<String> listofnotificationmanagement = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofnotificationmanagement)));
				
		
		for (String notificationmanagementtypes : listofnotificationmanagement) {

			boolean match = false;
			for (int i = 0; i < notifyManagement.length; i++) {
				if (notificationmanagementtypes.equals(notifyManagement[i])) {
					match = true;
					Reporter.log("list of notification management are : " + notificationmanagementtypes);
					Report.LogInfo("Info", "list of notification management are : " + notificationmanagementtypes,"PASS");
				}
			}
		//	sa.assertTrue(match,"");
			
		}
	  }catch(Exception e) {
		  Reporter.log("Notification Management dropdown values are mismatching");
		  e.printStackTrace();
		  Report.LogInfo("Info", "  values in Notification management dropdown under Direct Fiber service subtype is not displaying as expected","FAIL");
	  }
		}else {
			Report.LogInfo("Info",  " 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected","FAIL");
		}
		}
	}else {
		Report.LogInfo("Info",  " 'proactive monitoring' checkbox is not available under 'Create Service' page","FAIL");
		}

		
		scrollDown(Lanlink_InternationalObj.International.ServiceIdentification);
		
		waitforPagetobeenable();
		

	//delivery channel
		try {
	deliveryChanel = isElementPresent(Lanlink_InternationalObj.International.deliverychannel_withclasskey);
		//sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
		if(deliveryChanel) {
			Report.LogInfo("Info",  " 'Delivery Channel' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected","PASS");
		
		click(Lanlink_InternationalObj.International.deliverychannel_withclasskey);
	try {
		List<String> listofdeliverychannel = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofdeliverychannel)));
				
		for (String deliverychanneltypes : listofdeliverychannel) {

			boolean match = false;
			for (int i = 0; i < deliverychannel.length; i++) {
				if (deliverychanneltypes.equals(deliverychannel[i])) {
					match = true;
					Reporter.log("list of delivery channel are : " + deliverychanneltypes);
					
				}
			}
		//	sa.assertTrue(match,"");
		}
	}catch(Exception e) {
		e.printStackTrace();
		Reporter.log("delivery channel dropdown values are mismatching");
	//	Report.LogInfo("Info", "  values in delivery channel dropdowns under Direct Fiber service subtype are not displaying as expected","FAIL");
	}
		} else {
			//Report.LogInfo("Info",  " 'Delivery Channel' dropdown is not avilable under 'Management options' panel in 'Create Service' page","FAIL");
		}
		
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",  "'Delivery Channel' dropdown is not available under 'Create Service' page","FAIL");
		}

		
	//Management Connection
		try {
		Managementorder = isElementPresent(Lanlink_InternationalObj.International.managementConnection);
	//	sa.assertTrue(Managementorder, "Management order field is not displayed");
		if(Managementorder) {
			Report.LogInfo("Info",  " 'Management Connection' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected","PASS");
		
		click(Lanlink_InternationalObj.International.managementConnection);
		
		try {
		List<String> listofmanagementOrder =new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.listofmanagementOrder)));
				
		
		for (String mnaagementOrdertypes : listofmanagementOrder) {
			
			Reporter.log("Available Management Connection name is : " + mnaagementOrdertypes);
			Report.LogInfo("Info", 
					"Available Management Connection name is : " + mnaagementOrdertypes,"PASS");
			Reporter.log("Available Management Connection name is :" + mnaagementOrdertypes);
			
		}
		}catch(Exception e) {
			e.printStackTrace();
			Reporter.log(" 'Management Connection' dropdown values are mismatching");
		}
		}else {
			Report.LogInfo("Info",  " 'Management Connection' dropdown is not available under 'Management options' panel in 'Create Service' page","FAIL");
		}
		
		}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "'Management Connection' dropdown is not available under 'Create Service' page","FAIL");
		}	
		

		//ENNI checkbox
	//	verfyFields_ENNIcheckbox();

	//////scrolltoend();
	waitforPagetobeenable();

					
//	String	okButton = isVisible(Lanlink_InternationalObj.International.okbutton);
	//	sa.assertTrue(okButton, "OK button is not displayed");
		
		
		cancelButton = isElementPresent(Lanlink_InternationalObj.International.cancelButton);
	//	sa.assertTrue(cancelButton, "Cancel button is not displayed");
		
		//////scrolltoend();
		waitforPagetobeenable();
				
		verifyExists(Lanlink_InternationalObj.International.cancelButton,"cancelButton");

		click(Lanlink_InternationalObj.International.cancelButton,"cancelButton");
	waitforPagetobeenable();
		
	//	sa.assertAll();
		Report.LogInfo("Info", " Fields are verified","PASS");
	   }catch(AssertionError e) {
		 e.printStackTrace();
	   }	
		
	}
	
	
	public void selectOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException

	{
	String neworderSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "neworderSelection");
	String neworderno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingorderdropdown");
	String existingordernumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
	String existingorderSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingOrderSelection");


	waitToPageLoad();

	//Create New order or select Existing Order	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	if (neworderSelection.equalsIgnoreCase("YES")) {
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.selectorderswitch,"select order switch");
	click(Lanlink_InternationalObj.International.existingorderdropdown, neworderno);
	} 

	else if (existingorderSelection.equalsIgnoreCase("YES")) {

	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	click(Lanlink_InternationalObj.International.selectorderswitch,"select order switch");
	addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",Lanlink_InternationalObj.International.existingorderdropdown, existingordernumber);
	Reporter.log("=== Order Contract Number selected===");

	waitforPagetobeenable();

	} else {
	Reporter.log("Order not selected");
	}
	}
	
	
	public void dataToBeEnteredOncesubservicesselected(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
	String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modularmsp");
	String ServiceIdentificationNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
	String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
	String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
	String EndpointCPE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"endpointCPE");
	String Email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"email");
	String PhoneContact=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"phone");
	String remark=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"remark");
	String PerformanceReporting=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"performanceReportngForServices");
	String ProactiveMonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"proactiveMonitor");
	String deliveryChannel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"deliveryChannel");
	String ManagementOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManagementOrder");
	String vpnTopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vpnTopology");
	String intermediateTechnology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"intermediateTechnology");
	String CircuitReference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitReference");
	String CircuitType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitType");
	String notificationManagement=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Notificationmanagement");
	String ENNIcheckBox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ENNIcheckbox");
	String perCocPerfrmReprt=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerCoSpreformancereporting_serviceCreation");
	String actelsBased=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ActelisBased_servicecreation");
	String standrdCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardCIR_ServiceCreation");
	String standrdEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardEIR_ServiceCreation");
	String prmiumCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumCIR_ServiceCreation");
	String prmiumEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumEIR_ServiceCreation");
	String AggregateTraffic=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AggregateTraffic");
	String E_VPntechnology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"E_VPNtechnology");
	String HCoSperformreporting=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"HCoSPerformanceReporting");



	if(modularmsp.equalsIgnoreCase("no")) {	

	if (Interfacespeed.equalsIgnoreCase("10GigE")) {
		DirectFibre_10G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE,
				Email, PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
				ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference, CircuitType,
				notificationManagement,AggregateTraffic);
	}

	else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
		DirectFibre_1G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE,
				Email, PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
				ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference, CircuitType,
				notificationManagement, perCocPerfrmReprt, actelsBased, standrdCir, standrdEir, prmiumCir,
				prmiumEir);
	}
	}


	else if (modularmsp.equalsIgnoreCase("yes")) {

		metro_ModularMSpselected(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
				PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel, ManagementOrder,
				vpnTopology, intermediateTechnology, CircuitReference, CircuitType,notificationManagement,perCocPerfrmReprt,actelsBased,
				standrdCir, standrdEir, prmiumCir,prmiumEir, E_VPntechnology, HCoSperformreporting);


	}
	}    
	
	
	public void DirectFibre_1G(String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed,
			String EndpointCPE, String Email, String PhoneContact, String remark, String PerformanceReporting,
			String ProactiveMonitoring, String deliveryChannel, String ManagementOrder, String vpnTopology,
			String intermediateTechnology, String CircuitReference, String CircuitType, String notificationManagement,
			String perCocPerfrmReprt, String actelsBased, String standrdCir, String standrdEir, String prmiumCir,
			String prmiumEir)
			throws InterruptedException, IOException {

		scrollIntoTop();
		waitforPagetobeenable();

		// Service Identification
		createService_ServiceIdentification(ServiceIdentificationNumber);

		// End point CPE
		createService_singleEndPointCPE(EndpointCPE);

		// Phone Contact
		createService_phone(PhoneContact);

		// Remark
		createService_remark(remark);

		// scrollDown(Lanlink_InternationalObj.International.configurtoinptionpanel-webelementToScroll);

		// Performance Reporting
		if (!PerformanceReporting.equalsIgnoreCase("null")) {

			if (PerformanceReporting.equalsIgnoreCase("yes")) {

				boolean perfrmReprtFieldcheck = isElementPresent(
						Lanlink_InternationalObj.International.performanceReportingcheckbox);
				if (perfrmReprtFieldcheck) {
					Report.LogInfo("Info",
							" 'Performance reporting' checkbox is displaying under 'Manage ment options' panel in 'Create Service' page as exepcted",
							"PASS");
					click(Lanlink_InternationalObj.International.performanceReportingcheckbox, "performance Reporting checkbox");
					waitforPagetobeenable();

					boolean prfrmReportselection = isSelected(Lanlink_InternationalObj.International.performanceReportingcheckbox,
							"performance Reporting checkbox");
					if (prfrmReportselection) {
						Reporter.log("performance monitoring check box is selected");
						Report.LogInfo("Info", "Performance Reporting checkbox is selected as expected", "PASS");
					} else {
						Report.LogInfo("Info", "Performance Reporting checkbox is not selected", "FAIL");
					}

					
						// Per CoS Performance Reporting chekcbox
						boolean perCoSPrfrmReprtFieldcheck = isElementPresent(
								Lanlink_InternationalObj.International.perCoSperformncereport);
						if (perCoSPrfrmReprtFieldcheck) {
							Report.LogInfo("Info",
									" 'Per CoS Performance Reporting' checkbox is displaying, when 'Performance reporting' checkbox is selected",
									"PASS");
							if (perCocPerfrmReprt.equalsIgnoreCase("Yes")) {
								click(Lanlink_InternationalObj.International.perCoSperformncereport);
								waitforPagetobeenable();

								boolean perCoSSelection = isSelected(Lanlink_InternationalObj.International.perCoSperformncereport,
										"per CoS performnce report");
								if (perCoSSelection) {
									Report.LogInfo("Info",
											"Per CoS Performance Reporting checkbox is selected as expected", "PASS");
								} else {
									Report.LogInfo("Info", "Per CoS Performance Reporting checkbox is not selected",
											"FAIL");
								}

								// Actelis Based
								boolean actelisbasedFieldcheck = isElementPresent(
										Lanlink_InternationalObj.International.ActelisBasedcheckbox);
								if (actelisbasedFieldcheck) {
									Report.LogInfo("Info",
											" 'Actelis Based' checkbox is displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
											"PASS");
									if (actelsBased.equalsIgnoreCase("Yes")) {
										click(Lanlink_InternationalObj.International.ActelisBasedcheckbox,
												"Actelis Based checkbox");
										waitforPagetobeenable();

										boolean actelisSelection = isSelected(
												Lanlink_InternationalObj.International.ActelisBasedcheckbox,
												"Actelis Based checkbox");
										if (actelisSelection) {
											Report.LogInfo("Info", " 'Actelis Based' checkbox is selected as expected",
													"PASS");
										} else {
											Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected", "FAIL");
										}

										// Standard CIR Text Field
										createService_standardCIR(standrdCir);

										// Standard EIR Text Field
										createService_standardEIR(standrdEir);

										// Premium CIR Text Field
										createService_premiumCIR(prmiumCir);

										// Premium EIR Text Field
										createService_premiumEIR(prmiumEir);

									} else {
										Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected as expected",
												"PASS");
									}

								} else {
									Report.LogInfo("Info",
											" 'Actelis Based' checkbox is not displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
											"FAIL");
								}

							} else {
								Report.LogInfo("Info",
										" 'Per CoS Performance Reporting' checkbox is not selected as exepected",
										"PASS");
							}

						} else {
							Report.LogInfo("Info",
									" 'Per CoS Performance Rpeorting' checkbox is not displaying, when 'Performance reporting' checkbox is selected",
									"FAIL");
						}

					

				} else {
					Report.LogInfo("Info", " 'Performance Reporting' checkbox is not available", "FAIL");
				}
			} else {

				Reporter.log("Performance Repoting is not selected");
				Report.LogInfo("Info", "performance Reporting checkbox is not selected as expected", "PASS");

			}
		}

		// Pro active Monitoring
		createService_proactivemonitoring(ProactiveMonitoring, notificationManagement);

		 scrollDown(Lanlink_InternationalObj.International.VPNtopology);
		waitforPagetobeenable();

		// VPN topology
		boolean vpNTopology = isElementPresent(Lanlink_InternationalObj.International.VPNtopology);
		if (vpNTopology) {
			Report.LogInfo("Info", " 'VPN Topology' dropdown is dsiplaying as expected", "PASS");
			if (!vpnTopology.equalsIgnoreCase("null")) {

				click(Lanlink_InternationalObj.International.VPNtopology_xbutton, "VPNtopology_xbutton");
				waitforPagetobeenable();

				if (vpnTopology.equals("Point-to-Point")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();

					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Intermediate technology
					createService_intermediateTechnology(intermediateTechnology);

					// Circuit Reference
					createService_circuitreference(CircuitReference);

					// Circuit Type
					createSerivce_circuitType(CircuitType);
				}

				else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();

					waitforPagetobeenable();

					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Circuit Reference
					createService_circuitreference(CircuitReference);

				}

				else {

					Reporter.log(vpnTopology + " is not available under VPN topoloy dropdown");
					Reporter.log(vpnTopology + " is not available inside the VPN topoloy dropdown");
					Report.LogInfo("Info", vpnTopology + " is not available under VPN topoloy dropdown", "FAIL");
				}

			} else {
				Report.LogInfo("Info",
						" No value provided for 'VPN topology' dropdown. 'point -to -point' is selected by default",
						"PASS");

				// Intermediate technology
				createService_intermediateTechnology(intermediateTechnology);

				// Circuit Reference
				createService_circuitreference(CircuitReference);

				// Circuit Type
				createSerivce_circuitType(CircuitType);
			}
		} else {
			Report.LogInfo("Info",
					" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
					"FAIL");
			Reporter.log(
					"'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page");
		}

		click(Lanlink_InternationalObj.International.okbutton, "OK");

	}
	
	public void createService_ServiceIdentification(String ServiceIdentificationNumber)
			throws InterruptedException, IOException {
		// Service Identification

		boolean serviceIdentificationField = isElementPresent(Lanlink_InternationalObj.International.ServiceIdentification);
		if (serviceIdentificationField) {

			Report.LogInfo("Info", " 'Service Identification' text field is displaying as exepected", "Pass");
			if (!ServiceIdentificationNumber.equalsIgnoreCase("null")) {
				sendKeys((Lanlink_InternationalObj.International.ServiceIdentification), ServiceIdentificationNumber,
						"Service Id");
				waitforPagetobeenable();
				Report.LogInfo("Info", ServiceIdentificationNumber + " is entered under 'Service Identification' field",
						"Pass");
			} else {
				Report.LogInfo("Info", "It is a mandatory field. No values entered for service identification", "FAIL");

			}
		} else {
			Report.LogInfo("Info", "' Service Identfication' mandatory field is not available under 'Add Service' page",
					"FAIL");

		}

	}
	
	public void createService_singleEndPointCPE(String EndpointCPE) throws InterruptedException, IOException {

		addCheckbox_commonMethod(Lanlink_InternationalObj.International.EndpointCPE, "Single Endpoint CPE", EndpointCPE);

	}
	
	public void createService_phone(String PhoneContact) throws InterruptedException, IOException {

		boolean phone = false;
		try {
			phone = isElementPresent(Lanlink_InternationalObj.International.PhoneContact);
		//	sa.assertTrue(phone, "phone contact field is not displayed");
			if (phone) {
				Report.LogInfo("Info", " 'Phone Contact' text field is displaying as expected", "PASS");

				if (!PhoneContact.equalsIgnoreCase("null")) {

					sendKeys(Lanlink_InternationalObj.International.PhoneContact, PhoneContact, "Phone contact");
					waitforPagetobeenable();
					Report.LogInfo("Info", PhoneContact + " is entered under 'Phone contact' field", "PASS");
				} else {
					Report.LogInfo("Info", "Value not entered under 'Phone contact' field", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Phone Contact' text field is not available under 'Create Service' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Phone contact text field is not available");
			Report.LogInfo("Info", " 'Phone Contact' text field is not available under 'Create Service' page", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'hone Contact' field", "FAIL");
			Reporter.log("Not able to enter value under 'Phone Contact' field");
		}
	}
	
	public void createService_remark(String remark) throws InterruptedException, IOException {
		boolean Remark = false;
		try {
			Remark = isElementPresent(Lanlink_InternationalObj.International.Remark);
			if (Remark) {
				Report.LogInfo("Info", " 'Remark' text field is displaying as expected", "PASS");

				if (!remark.equalsIgnoreCase("null")) {

					sendKeys(Lanlink_InternationalObj.International.Remark, remark, "Remark");
					waitforPagetobeenable();
					Report.LogInfo("Info", remark + " is entered under 'Remark' field", "PASS");

				} else {

					Report.LogInfo("Info", "value not entered under 'Remark' field", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Remark' text field is not available under 'Create Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Remak text field is not availeble");
			Report.LogInfo("Info", " 'Remark' text field is not available under 'Create Service' page", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Reporter.log(" Not able t enter value in 'remark' text field");
			Reporter.log("Not able to enter value under 'Remark' field");
		}

	}
	
	public void createService_standardCIR(String standrdCir) throws InterruptedException, IOException {
		boolean standrdCiR = false;

		try {
			standrdCiR = isElementPresent(Lanlink_InternationalObj.International.standardCIRtextfield);
			if (standrdCiR) {
				Report.LogInfo("Info",
						" 'Standard CIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (standrdCir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Standard CIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_InternationalObj.International.standardCIRtextfield, standrdCir, "Standard CIR");

					String valuesForSCIR = getAttributeFrom(Lanlink_InternationalObj.International.standardCIRtextfield, "value");
					Report.LogInfo("Info", valuesForSCIR + " is enerted under 'Standard CIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'Standard CIR' field in 'create Service' page",
					"FAIL");
		}
	}
	
	
	public void createService_standardEIR(String standrdEir) throws InterruptedException, IOException {
		boolean standrdEiR = false;
		try {
			standrdEiR = isElementPresent(Lanlink_InternationalObj.International.standardEIRtextfield);
			if (standrdEiR) {
				Report.LogInfo("Info",
						" 'Standard EIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (standrdEir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Standard EIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_InternationalObj.International.standardEIRtextfield, standrdEir, "Standard EIR");

					String valuesForSEIR = getAttributeFrom(Lanlink_InternationalObj.International.standardEIRtextfield, "value");
					Report.LogInfo("Info", valuesForSEIR + " is enerted under 'Standard EIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not albe to enter value under 'Standard EIR' field in 'create Service' page",
					"FAIL");
			Reporter.log(" Not able to enter value under 'Standard EIR' field in 'create Service' page");
		}
	}
	
	public void createService_premiumCIR(String prmiumCir) throws InterruptedException, IOException {
		boolean premiumCiR = false;
		try {
			premiumCiR = isElementPresent(Lanlink_InternationalObj.International.premiumCIRtextfield);
			if (premiumCiR) {
				Report.LogInfo("Info",
						" 'Premium CIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (prmiumCir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Premium CIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_InternationalObj.International.premiumCIRtextfield, prmiumCir, "Premium CIR");

					String valuesForPCIR = getAttributeFrom(Lanlink_InternationalObj.International.premiumCIRtextfield, "value");
					Report.LogInfo("Info", valuesForPCIR + " is enerted under 'Premium CIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'Premium CIR' field in 'create Service' page",
					"FAIL");
		}
	}
	
	public void createService_premiumEIR(String prmiumEir) throws InterruptedException, IOException {
		boolean premiumEiR = false;
		try {
			premiumEiR = isElementPresent(Lanlink_InternationalObj.International.premiumEIRtextfield);
			if (premiumEiR) {
				Report.LogInfo("Info",
						" 'Standard EIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (prmiumEir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Premium EIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_InternationalObj.International.premiumEIRtextfield, prmiumEir, "Premium EIR");

					String valuesForPEIR = getAttributeFrom(Lanlink_InternationalObj.International.premiumEIRtextfield, "value");
					Report.LogInfo("Info", valuesForPEIR + " is enerted under 'Premium EIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'Premium EIR' field in 'create Service' page",
					"FAIL");
		}
	}
	
	public void createService_proactivemonitoring(String ProactiveMonitoring, String notificationManagement)
			throws InterruptedException, IOException {

		boolean proactiveMonitor = false;
		proactiveMonitor = isElementPresent(Lanlink_InternationalObj.International.proactiveMonitoring);
	//	sa.assertTrue(proactiveMonitor, "pro active monitoring checkbox is not displayed");
		if (proactiveMonitor) {
			Report.LogInfo("Info", " 'Pro active Monitoring' text field is displaying as expected", "PASS");

			if (!ProactiveMonitoring.equalsIgnoreCase("null")) {
				if (ProactiveMonitoring.equalsIgnoreCase("yes")) {

					click(Lanlink_InternationalObj.International.proactiveMonitoring, "proactive Monitoring");
					Reporter.log("Pro active monitoring check box is selected");

					boolean proactiveSelection = isSelected(Lanlink_InternationalObj.International.proactiveMonitoring,
							"proactive Monitoring");
					if (proactiveSelection) {
						Report.LogInfo("Info", " 'pro active monitoring' checkbox is selected as expected", "PASS");
					} else {
						Report.LogInfo("Info", " 'pro active monitoring' checkbox is not selected", "FAIL");
					}

					// Notification management
					try {

						if (!notificationManagement.equalsIgnoreCase("null")) {
							Reporter.log(
									"Notificationan Management dropdown displays when pro active monitoring is selected");

							click(Lanlink_InternationalObj.International.notificationmanagement, "notification management");
							waitforPagetobeenable();
							webDriver.findElement(By.xpath("//div[contains(text(),'" + notificationManagement + "')]")).click();

							waitforPagetobeenable();
							Report.LogInfo("Info",
									notificationManagement + " is selected under 'Notification management' dropdown",
									"PASS");

						} else {
							Report.LogInfo("Info", "No values selected under Notification management dropdown", "PASS");

						}

					} catch (NoSuchElementException e) {
						Reporter.log(
								" 'Notification management' dropodwn is not displaying under 'create Service' page");
						Report.LogInfo("Info",
								" 'Notification management' dropodwn is not displaying under 'create Service' page",
								"FAIL");
					} catch (Exception err) {
						err.printStackTrace();
						Report.LogInfo("Info", "Not able to select values under Notification management dropdown",
								"FAIL");
					}
				} else {
					Reporter.log("Pro active monitoring is not selected");
					Reporter.log("pro active monitoring is not selected");
					Report.LogInfo("Info", "performance monitor checkbox is not selected ", "PASS");
				}

			}
		} else {
			Report.LogInfo("Info", " 'Pro active monitoring' checkbox is not displaying", "FAIL");
		}
	}
	
	
	public void createService_deliveryChannel(String deliveryChannel) throws InterruptedException, IOException {

		boolean deliveryChanel = false;
		try {
			deliveryChanel = isElementPresent(Lanlink_InternationalObj.International.deliverychannel_withclasskey);
		//	sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
			if (deliveryChanel) {
				Report.LogInfo("Info", " 'Delivery Channel' dropdown is displaying as expected", "PASS");
				if (!deliveryChannel.equalsIgnoreCase("null")) {
					
					click(Lanlink_InternationalObj.International.deliverychannel_xbutton,"deliverychannel_xbutton");
					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.deliverychannel_withclasskey,"deliverychannel_withclasskey");
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + deliveryChannel + "')]")).click();
					Report.LogInfo("Info", deliveryChannel + " is selected under 'Delivery channel' dropdown", "PASS");

				} else {
					Report.LogInfo("Info", "No value selected under 'Delivery channel' dropdown", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Delivery channel' dropdown is not dispalying under 'create Serice' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Delivery channel' dropdown is not dispalying");
			Report.LogInfo("Info", " 'Delivery channel' dropdown is not dispalying under 'create Serice' page", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to selected value under 'Delivery channel' dropodwn", "FAIL");
		}
	}
	
	public void createService_managementOptions(String ManagementOrder) throws InterruptedException, IOException {

		boolean Managementorder = false;
		try {
			Managementorder = isElementPresent(Lanlink_InternationalObj.International.managementOrder);
		//	sa.assertTrue(Managementorder, "Management order field is not displayed");
			if (Managementorder) {
				Report.LogInfo("Info", " ' Management Order' dropdown is displaying as expected", "PASS");
				if (!ManagementOrder.equalsIgnoreCase("null")) {

					click(Lanlink_InternationalObj.International.managementOrder);
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + ManagementOrder + "')][1]")).click();


					waitforPagetobeenable();

					Report.LogInfo("Info", ManagementOrder + " is selected under 'management Order' field", "PASS");

				} else {
					Report.LogInfo("Info", "Values not entered under 'Management Order' field", "PASS");
				}
			} else {
				Report.LogInfo("Info", " ' Management Order' dropdown is not displaying under 'Create Service' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Management order' dropdown is not displaying");
			Report.LogInfo("Info", " ' Management Order' dropdown is not displaying under 'Create Service' page",
					"FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to select value under 'management Order' dropdown", "FAIL");
		}
	}
	
	public void createService_intermediateTechnology(String intermediateTechnology)
			throws InterruptedException, IOException {
		boolean intrTech = false;
		try {
			intrTech = isElementPresent(Lanlink_InternationalObj.International.IntermediateTechnology);
			if (intrTech) {
				Report.LogInfo("Info",
						" 'Intermediate Technology' text field is displying as expected under 'create service' page",
						"PASS");
				if (!intermediateTechnology.equalsIgnoreCase("null")) {
					sendKeys(Lanlink_InternationalObj.International.IntermediateTechnology, intermediateTechnology,
							"intermediateTechnology");

					String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.IntermediateTechnology, "value");
					waitforPagetobeenable();
					Report.LogInfo("Info", actualvalue + " is entered under 'Intermediate technology' field", "PASS");
				} else {
					Report.LogInfo("Info", "No value entered under 'Intermediate  Technology' field", "PASS");
				}
			} else {
				Report.LogInfo("Info",
						" 'Intermediate Technology' text field is not displying under 'create service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Intermediate Technology' text field is not displying");
			Report.LogInfo("Info", " 'Intermediate Technology' text field is not displying under 'create service' page",
					"FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info",
					" Not able to enter value under 'Intermediate Technology' text field in 'create service' page",
					"FAIL");
			Reporter.log("Not able to enter value under 'Intermediate Technology' text field in 'create service' page");
		}
	}
	
	public void createService_circuitreference(String CircuitReference) throws InterruptedException, IOException {
		boolean crctRef = isElementPresent(Lanlink_InternationalObj.International.circuitReference);
		if (crctRef) {
			Report.LogInfo("Info", " 'Circuit Reference' text field is displying as expected", "PASS");
			if (!CircuitReference.equalsIgnoreCase("null")) {
				sendKeys(Lanlink_InternationalObj.International.circuitReference, CircuitReference, "Circuit Reference");
				waitforPagetobeenable();

				String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.circuitReference, "value");

				Report.LogInfo("Info", actualvalue + " is entered under 'Circuit Reference' field", "PASS");
			} else {
				Report.LogInfo("Info", "Circuit reference field is mandatory, but no inputs has been provided", "FAIL");
			}
		} else {
			Report.LogInfo("Info", " 'Circuit Reference' text field is not displying", "FAIL");
		}
	}
	
	public void createSerivce_circuitType(String CircuitType) throws InterruptedException, IOException {

		if (!CircuitType.equalsIgnoreCase("null")) {
			webDriver.findElement(By.xpath("//div[span[contains(text(),'" + CircuitType + "')]]//input")).click();
			waitforPagetobeenable();
			Report.LogInfo("Info", CircuitType + " is selected under 'Circuit Type'", "PASS");
		} else {
			Report.LogInfo("Info", " No changes made. 'Default' is selected under'Circuit type' as no input provided",
					"PASS");

		}
	}
	
	
	public void DirectFibre_10G(String ServiceIdentificationNumber, String SelectSubService,
			String Interfacespeed, String EndpointCPE, String Email, String PhoneContact, String remark,
			String PerformanceReporting, String ProactiveMonitoring, String deliveryChannel, String ManagementOrder,
			String vpnTopology, String intermediateTechnology, String CircuitReference, String CircuitType,
			String notificationManagement,String AggregateTraffic)
			throws InterruptedException, IOException {

	    scrollIntoTop();
		waitforPagetobeenable();

		// Service Identification
		createService_ServiceIdentification(ServiceIdentificationNumber);

		// End point CPE
		createService_singleEndPointCPE(EndpointCPE);

		// Email
	//	createSerivce_email(Email);

		// Phone Contact
		createService_phone(PhoneContact);

		// Remark
		createService_remark(remark);

		
		waitforPagetobeenable();

		// Performance Reporting
		if (!PerformanceReporting.equalsIgnoreCase("null")) {

			if (PerformanceReporting.equalsIgnoreCase("yes")) {

				boolean perfrmReprtFieldcheck = findWebElement(Lanlink_InternationalObj.International.performanceReportingcheckbox).isDisplayed();
				if (perfrmReprtFieldcheck) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Performance reporting' checkbox is displaying under 'Manage ment options' panel in 'Create Service' page as exepcted");
					click(Lanlink_InternationalObj.International.performanceReportingcheckbox);
					waitforPagetobeenable();

					boolean prfrmReportselection = findWebElement(Lanlink_InternationalObj.International.performanceReportingcheckbox).isSelected();
					if (prfrmReportselection) {
						Reporter.log("performance monitoring check box is selected");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Performance Reporting checkbox is selected as expected");
						Reporter.log("Performance Reporting checkbox is selected as expected");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Performance Reporting checkbox is not selected");
						Reporter.log("Performance Reporting checkbox is not selected");
					}

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Performance Reporting' checkbox is not available");
					Reporter.log(" 'Performance Reporting' checkbox is not available");
				}
			} else {

				Reporter.log("Performance Repoting is not selected");
				ExtentTestManager.getTest().log(LogStatus.PASS, "performance Reporting checkbox is not selected as expected");
			}
		}

		// Pro active Monitoring
		createService_proactivemonitoring( ProactiveMonitoring, notificationManagement);

		// Delivery Channel
		//createService_deliveryChannel(deliveryChannel);
		// management Connection
		createService_managementOptions(ManagementOrder);

		// ENNI checkbox
	//	addCheckbox_commonMethod( Lanlink_InternationalObj.International.ENNI_checkbox, "ENNI", ENNIcheckBox);

		scrollDown(Lanlink_InternationalObj.International.okbutton);
		click(Lanlink_InternationalObj.International.okbutton,"OK");

	}
	
	public void verifysuccessmessage(String expected) throws IOException, InterruptedException {
		waitforPagetobeenable();
		scrollUp();
		verifyExists(Lanlink_InternationalObj.International.AlertForServiceCreationSuccessMessage, "Success message"+expected);

	}
	
	public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	 {
	 	String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editOrderSelection");
	 	String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_OrderNumber");
	 	String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_VoicelineNumber");

	 	if(editOrderSelection.equalsIgnoreCase("no"))
	 	{
	 		Reporter.log("Edit Order is not performed");
	     }
	 	else if(editOrderSelection.equalsIgnoreCase("Yes"))
	 	{
	 		Reporter.log("Performing Edit Order Functionality");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.orderactionbutton,"Action dropdown");
	 		click(Lanlink_InternationalObj.International.orderactionbutton,"Action dropdown");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editorderlink,"Edit Order");
	 		click(Lanlink_InternationalObj.International.editorderlink,"Edit Order");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editorderheader,"Edit Order");

	 		String EditOrderNo=getTextFrom(Lanlink_InternationalObj.International.editorderno);
	 		click(Lanlink_InternationalObj.International.editorderno,"edit order no");
	 		clearTextBox(Lanlink_InternationalObj.International.editorderno);
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editorderno,"Order Number");
	 		sendKeys(Lanlink_InternationalObj.International.editorderno,editorderno,"Order Number");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editvoicelineno,"edit voice line no");
	 		clearTextBox(Lanlink_InternationalObj.International.editvoicelineno);
	 		sendKeys(Lanlink_InternationalObj.International.editvoicelineno,editvoicelineno,"Order Number");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.cancelbutton,"cancel button");
	 		click(Lanlink_InternationalObj.International.cancelbutton,"cancel button");
	 	
	 		verifyExists(Lanlink_InternationalObj.International.orderactionbutton,"Action dropdown");
	 		click(Lanlink_InternationalObj.International.orderactionbutton,"Action dropdown");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editorderlink,"Edit Order");
	 		click(Lanlink_InternationalObj.International.editorderlink,"Edit Order");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editorderheader,"Edit Order Header");
	 		String editOrderHeader=getTextFrom(Lanlink_InternationalObj.International.editorderheader);
	 		String EditOrder= "Edit Order";
	 		editOrderHeader.equalsIgnoreCase(EditOrder);
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editorderno,"edit order no");
	 		click(Lanlink_InternationalObj.International.editorderno,"edit order no");
	 		clearTextBox(Lanlink_InternationalObj.International.editorderno);
	 		sendKeys(Lanlink_InternationalObj.International.editorderno,editorderno,"Order Number");

	 		verifyExists(Lanlink_InternationalObj.International.editvoicelineno,"edit voice line no");
	 		clearTextBox(Lanlink_InternationalObj.International.editvoicelineno);
	 		sendKeys(Lanlink_InternationalObj.International.editvoicelineno,editvoicelineno,"Order Number");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.editorder_okbutton,"OK Button");
	 		click(Lanlink_InternationalObj.International.editorder_okbutton,"OK Button");
	 		
	 		if(editorderno.equalsIgnoreCase("Null"))
	 		{
	 			Reporter.log("Order/Contract Number (Parent SID) field is not edited");
	 		}
	 		else 
	 		{
	 			verifyExists(Lanlink_InternationalObj.International.ordernumbervalue,"order number value");
	 			String editOrderno=getTextFrom(Lanlink_InternationalObj.International.ordernumbervalue);
	 			editOrderno.equalsIgnoreCase(editorderno);
	 		}
	 		
	 		if(editvoicelineno.equalsIgnoreCase("Null"))
	 		{
	 			Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
	 		}
	 		else
	 		{
	 			verifyExists(Lanlink_InternationalObj.International.ordervoicelinenumbervalue,"order voice line number value");
	 			String editVoicelineno=getTextFrom(Lanlink_InternationalObj.International.ordervoicelinenumbervalue);
	 			editVoicelineno.equalsIgnoreCase(editvoicelineno);
	 		}
	 		Reporter.log("Edit Order is successful");	

	 	}

	   }
	
	public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	 {

	 	String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
	 	String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_existingOrder");
	 	String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_newOrderNumber");
	 	String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_VoicelineNumber");
	 	String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_existingOrderNumber");

	 	if((changeOrderSelection_newOrder.equalsIgnoreCase("No")) && (changeOrderSelection_existingOrder.equalsIgnoreCase("No")))
	 	{
	 		Reporter.log("Change Order is not performed");	

	 	}
	 	else if(changeOrderSelection_newOrder.equalsIgnoreCase("Yes"))
	 	{
	 		verifyExists(Lanlink_InternationalObj.International.orderactionbutton,"Action dropdown");
	 		click(Lanlink_InternationalObj.International.orderactionbutton,"Action dropdown");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorderlink,"change order link");
	 		click(Lanlink_InternationalObj.International.changeorderlink,"change order link");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorderheader,"change order header");
	 		String changeOrderHeader=getTextFrom(Lanlink_InternationalObj.International.changeorderheader);
	 		String changeOrder = "Change Order";
	 		changeOrderHeader.equalsIgnoreCase(changeOrder);
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorder_selectorderswitch,"select order switch");
	 		click(Lanlink_InternationalObj.International.changeorder_selectorderswitch,"select order switch");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeordernumber,"change order number");
	 		click(Lanlink_InternationalObj.International.changeordernumber,"change order number");
	 		sendKeys(Lanlink_InternationalObj.International.editorderno,ChangeOrder_newOrderNumber);
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeordervoicelinenumber,"change order voiceline number");
	 		click(Lanlink_InternationalObj.International.changeordervoicelinenumber,"change order voiceline number");
	 		sendKeys(Lanlink_InternationalObj.International.changeordervoicelinenumber,changevoicelineno);
	 		
	 		verifyExists(Lanlink_InternationalObj.International.createorder_button,"create order button");
	 		click(Lanlink_InternationalObj.International.createorder_button,"create order button");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorder_okbutton,"change order ok button");
	 		click(Lanlink_InternationalObj.International.changeorder_okbutton,"change order ok button");
	 				
	 		
	 		Reporter.log("Change Order is successful");	
	 	}
	 	else if(changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) 
	 	{
	 		Reporter.log("Performing Change Order functionality");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.orderactionbutton,"order action button");
	 		click(Lanlink_InternationalObj.International.orderactionbutton,"order action button");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorderlink,"change order link");
	 		click(Lanlink_InternationalObj.International.changeorderlink,"change order link");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorderheader,"change order");
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorder_chooseorderdropdown,"Order/Contract Number (Parent SID)");
	 		addDropdownValues_commonMethod("Order/Contract Number (Parent SID)",Lanlink_InternationalObj.International.changeorder_chooseorderdropdown,ChangeOrder_existingOrderNumber);
	 		
	 		verifyExists(Lanlink_InternationalObj.International.changeorder_okbutton,"change order ok button");
	 		click(Lanlink_InternationalObj.International.changeorder_okbutton,"change order ok button");
	 		

	 		Reporter.log("Change Order is successful");

	 	}
	 	
	 }
	
	
	public void VerifydatenteredForServiceSubTypeSelected(String testDataFile,String sheetName,String scriptNo,String dataSetNo)
			throws InterruptedException, IOException {
		
		String serviceType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
		String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
		String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
		String ServiceIdentificationNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
		String EndpointCPE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"endpointCPE");
		String Email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"email");
		String PhoneContact=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"phone");
		String remark=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"remark");
		String PerformanceMonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerformMonitor");
		String ProactiveMonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"proactiveMonitor");
		String deliveryChannel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"deliveryChannel");
		String ManagementOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManagementOrder");
		String vpnTopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vpnTopology");
		String intermediateTechnology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"intermediateTechnology");
		String CircuitReference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitReference");
		String CircuitType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitType");
		String AggregateTraffic=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AggregateTraffic");
		String DeliveryChannelForselecttag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Deliverychanneforselecttag");
		String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modularmsp");
		String autoCreateService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AutocreateService");
		String ENNIcheckBox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ENNIcheckbox");

		String Manageconnectiondropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"manageConnectiondropdown");
		String A_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_Endtechnology");
		String B_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_Endtechnology");
		String Performancereporting=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"performanceReportngForServices");
		String perCocPerfrmReprt=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerCoSpreformancereporting_serviceCreation");
		String actelsBased=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ActelisBased_servicecreation");
		String standrdCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardCIR_ServiceCreation");
		String standrdEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardEIR_ServiceCreation");
		String prmiumCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumCIR_ServiceCreation");
		String prmiumEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumEIR_ServiceCreation");
		String notificationManagement=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Notificationmanagement");

		String EVPNtechnology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"E_VPNtechnology");
		String HCoSPerformanceReporting=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"HCoSPerformanceReporting");


		waitforPagetobeenable();
		try {

			if (modularmsp.equalsIgnoreCase("no")) {

				if (Interfacespeed.equalsIgnoreCase("10GigE")) {

					verifydataEntered_DirectFibre10G(serviceType, ServiceIdentificationNumber,
							SelectSubService, Interfacespeed, EndpointCPE, Email, PhoneContact, remark,
							PerformanceMonitoring, ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology,
							intermediateTechnology, CircuitReference, CircuitType, modularmsp, notificationManagement,AggregateTraffic);

				}

				else if (Interfacespeed.equalsIgnoreCase("1GigE")) {

					verifydataEntered_DirectFibre1G(serviceType, ServiceIdentificationNumber,
							SelectSubService, Interfacespeed, EndpointCPE, Email, PhoneContact, remark,
							PerformanceMonitoring, ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology,
							intermediateTechnology, CircuitReference, CircuitType, modularmsp, perCocPerfrmReprt,
							actelsBased, standrdCir, standrdEir, prmiumCir, prmiumEir, notificationManagement);

				}
			} else {
				verifydataEntered_Metro_MSPselected(serviceType, ServiceIdentificationNumber, SelectSubService,
						Interfacespeed, EndpointCPE, Email, PhoneContact, remark, PerformanceMonitoring,
						ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology, intermediateTechnology,
						CircuitReference, CircuitType, modularmsp,perCocPerfrmReprt, actelsBased, standrdCir, standrdEir, prmiumCir, prmiumEir,
						notificationManagement, EVPNtechnology, HCoSPerformanceReporting);
			}

			

		} catch (AssertionError e) {
			Reporter.log("validation failed for verify Direct Fiber service subtype page ");
			e.printStackTrace();
		}
	}

	
	public void verifydataEntered_DirectFibre1G(String serviceype, String ServiceIdentificationNumber,
			String SelectSubService, String Interfacespeed, String EndpointCPE, String Email, String PhoneContact,
			String remark, String PerformanceMonitoring, String ProactiveMonitoring, String deliveryChannel,
			String ManagementOrder, String vpnTopology, String intermediateTechnology, String CircuitReference,
			String CircuitType, String modularMSP, String perCocPerfrmReprt, String actelsBased, String standrdCir,
			String standrdEir, String prmiumCir, String prmiumEir, String notificationManagement) throws InterruptedException, IOException {

		scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);

		waitforPagetobeenable();

		/**
		 * Service Panel
		 */
		// Service Identification
		verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

		// Service type
		verifyEnteredvalues("Service Type", serviceype);

		// Service Subtype
		verifyEnteredvalues("Service Subtype", SelectSubService);

		// Interface Speed
		verifyEnteredvalues("Interface Speed", Interfacespeed);

		// Single Endpoint CPE
		if (vpnTopology.equals("Point-to-Point")) {
			verifyEnteredvalues("Single Endpoint CPE", EndpointCPE);
		} else {
			verifyEnteredvalues("Single Endpoint CPE", "No");
		}

		// Email
		verifyEnteredvalueForEmail_serviceCreationpage("Email", Email);

		
		// Phone Contact
		verifyEnteredvalues("Phone Contact", PhoneContact);

		// Modular MSP
		verifyEnteredvalues("Modular MSP", modularMSP);

		// Remark
		compareText("Remark", Lanlink_InternationalObj.International.remark_viewPage, remark);

		scrollDown(Lanlink_InternationalObj.International.viewServicepage_Servicepanel);

		waitforPagetobeenable();

		/**
		 * Management Options panel
		 */

		// Delivery Channel
	//	verifyEnteredvalues("Delivery Channel", deliveryChannel);

		// Proactive Monitoring
		verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

		if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
		//	verifyEnteredvalues("Notification Management Team", notificationManagement);
		}

		scrollDown(Lanlink_InternationalObj.International.viewServicepage_ManagementOptionsPanel);

		waitforPagetobeenable();

		//Performance Reporting
		if(PerformanceMonitoring.equalsIgnoreCase("no"))
		
			verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);
		
		
			if ((vpnTopology.equals("Point-to-Point")) || (vpnTopology.equals("E-PN (Any-to-Any)"))) {

				if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("yes"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);

					// Standard
					compareText("Header Name for 1st row", Lanlink_InternationalObj.International.standardHeaderName_viewPage,
							"Standard");

					compareText("Standard CIR", Lanlink_InternationalObj.International.standardCIRvalue_viewPage, standrdCir);

					compareText("Standard EIR", Lanlink_InternationalObj.International.standardEIRvalue_viewPage, standrdEir);

					// Premium
					compareText("Header Name for 2nd row", Lanlink_InternationalObj.International.PremisumHeaderName_viewPage,
							"Premium");

					compareText("Premium CIR", Lanlink_InternationalObj.International.premiumCIRvalue_viewPage, prmiumCir);

					compareText("Premium EIR", Lanlink_InternationalObj.International.premiumEIRvalue_viewPage, prmiumEir);

				}
			} else {

				if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("yes"))) {

					actelsBased = "No";

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

			}
		

		// WebElement managementOptionsPanel=
		// getwebelement(xml.getlocator(Lanlink_InternationalObj.International.viewServicepage_ManagementOptionsPanel"));
		// ScrolltoElement(managementOptionsPanel);
		// waitforPagetobeenable();

		// ////scrolltoend();
		waitforPagetobeenable();

		/**
		 * Configuration options panel
		 */

		// VPN Topology
		verifyEnteredvalues("VPN Topology", vpnTopology);

		if (vpnTopology.equals("Point-to-Point")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);

			// Circuit Type
			verifyEnteredvalues("Circuit Type", CircuitType);
		}

		if (vpnTopology.equals("Hub&Spoke")) {

			Reporter.log("only vpn topology displays under view Service page");
			Report.LogInfo("Info",
					" Only VPN topology field displays under 'Configuration' panel, when 'Hub&Spoke' is selected",
					"PASS");

		}

		if (vpnTopology.equals("E-PN (Any-to-Any)")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);

		}

	}
	
	public void verifyEnteredvalues(String labelname, String ExpectedText) throws InterruptedException {

		String text = null;
		WebElement element = null;

		try {
			Thread.sleep(1000);
			element = webDriver.findElement(By.xpath("//div[div[label[contains(text(),'"+ labelname + "')]]]/div[2]"));
			String emptyele = element.getText().toString();

			if(element==null)
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname+" not found");
				Reporter.log(labelname+" not found");
			}
			else if (emptyele!=null && emptyele.isEmpty()) {
//				ExtentTestManager.getTest().log(LogStatus.PASS,  labelname + "' value is empty");
				
				emptyele= "Null";
				
			//	sa.assertEquals(emptyele, ExpectedText, labelname + " value is not displaying as expected");
				
				if(emptyele.equalsIgnoreCase(ExpectedText)) {
					
					ExtentTestManager.getTest().log(LogStatus.PASS, "The Expected value for '"+ labelname +"' field  is same as the Acutal value. It is id displaying blank");
					Reporter.log("The Expected value for '\"+ labelname +\"' field  is same as the Acutal value. It is displaying blank");
					
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					Reporter.log(" The Expected value '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
				}
				

			}else 
			{   
				text = element.getText();
				if( text.contains("-")) {
					
					String[] actualTextValue=text.split(" ");
					String[] expectedValue =ExpectedText.split(" ");
					
					if(expectedValue[0].equalsIgnoreCase(actualTextValue[0])) {
						ExtentTestManager.getTest().log(LogStatus.PASS," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log(" The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					}
					else if(expectedValue[0].contains(actualTextValue[0])) {
						ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					
					}
					else
					{
						ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					}
					
				}else {
					if(ExpectedText.equalsIgnoreCase(text)) {
						ExtentTestManager.getTest().log(LogStatus.PASS," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log(" The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					}
					else if(ExpectedText.contains(text)) {
						ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					
					}
					else
					{
						ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					}
				}
				
				
			}
		}catch (Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
			Reporter.log(labelname + " field is not displaying");
		}

	}
	
	public void verifyEnteredvalueForEmail_serviceCreationpage(String label, String expectedValue)
			throws InterruptedException {

		try {

			WebElement ele = webDriver.findElement(By.xpath("(//div[div[label[contains(text(),'" + label + "')]]]/div[2])[2]"));
			String valueinfo = ele.getText().toString();
			if ((valueinfo.equals("")) || (valueinfo.equalsIgnoreCase(null))) {

				Reporter.log("value not displayed for " + label);
				valueinfo = "Null";

				//sa.assertEquals(valueinfo, expectedValue, label + " value is not displaying as expected");

//					ExtentTestManager.getTest().log(LogStatus.PASS, "No value displaying for : " + label);

			} else {

				Reporter.log("value displayed for " + label + " is : " + valueinfo);

				Reporter.log("value displayed for" + label + "is : " + valueinfo);

			//	sa.assertEquals(valueinfo, expectedValue, label + " value is not displaying as expected");

				if (valueinfo.equalsIgnoreCase(expectedValue)) {
					Reporter.log("The valus is dipslaying as expected");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" Value is displaying as expected in 'view' page for " + label);
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value displayed for" + label + "is : " + valueinfo);
				} else {
					Reporter.log("the values are not dipslaying as expected for label: " + label);
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" Value is not displaying as expected in 'view' page for " + label);
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"value displayed for " + label + "is : " + valueinfo);

				}

			}
		} catch (AssertionError err) {
			err.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, label + " value is not displaying as expected ");
		} catch (NoSuchElementException e) {
			Reporter.log("value not displayed for " + label);
			ExtentTestManager.getTest().log(LogStatus.FAIL, "" + label + " is not displaying");

		}
	}
 
	public void verifydataEntered_DirectFibre10G(String serviceype,
			String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed, String EndpointCPE,
			String email, String PhoneContact, String remark, String PerformanceMonitoring, String ProactiveMonitoring,
			String deliveryChannel, String ManagementOrder, String vpnTopology, String intermediateTechnology,
			String CircuitReference, String CircuitType, String modularMSP, String notificationManagement,String AggregateTraffic)
			throws InterruptedException, IOException {

		WebElement orderPanel = findWebElement(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
		scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
		waitforPagetobeenable();

		/**
		 * Service Panel
		 */
		// Service Identification
		verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

		// Service type
		verifyEnteredvalues("Service Type", serviceype);

		// Service Subtype
		verifyEnteredvalues("Service Subtype", SelectSubService);

		// Interface Speed
		verifyEnteredvalues("Interface Speed", Interfacespeed);

		// Single Endpoint CPE
//			verifyEnteredvalues("Single Endpoint CPE", EndpointCPE);
//			verifyEnteredvalues("Single Endpoint CPE", "No");

		// Email
		verifyEnteredvalueForEmail_serviceCreationpage("Email", email);

		// Phone Contact
		verifyEnteredvalues("Phone Contact", PhoneContact);

		// Modular MSP
		verifyEnteredvalues("Modular MSP", modularMSP);

		// Remark
		verifyExists( Lanlink_InternationalObj.International.remark_viewPage, "remark");

		WebElement servicePanel = findWebElement(Lanlink_InternationalObj.International.viewServicepage_Servicepanel);
		scrollDown(Lanlink_InternationalObj.International.viewServicepage_Servicepanel);
		waitforPagetobeenable();

		/**
		 * Management Options panel
		 */

		// Delivery Channel
		verifyEnteredvalues("Delivery Channel", deliveryChannel);

		// Management Connection
	//	verifyEnteredvalues("Management Connection", Manageconnectiondropdown);

		// Proactive Monitoring
		verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

		if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
			verifyEnteredvalues("Notification Management Team", notificationManagement);
		}

		// Performance Reporting
		verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

		// ENNI checkbox
	//	verifyEnteredvalues("ENNI", ENNIcheckBox);

	}
 

	public void EditTheservicesselected(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
					String modularmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
					String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
					String ServiceIdentificationNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_serviceNumber");
					String SelectSubService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
					String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_remark");
					String EndpointCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_endpointCPE");
					String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_email");
					String PhoneContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_phone");
					String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"performanceReportngForServices");
					String ProactiveMonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_proactiveMonitor");
					String deliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_deliveryChannel");
					String ManagementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_ManagementOrder");
					String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
					String intermediateTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_intermediateTechnology");
					String CircuitReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_CircuitReference");
					String circuitTypeUsedInServiceCreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_CircuitType");
					String notificationManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_NotificationManagement");
					String COScheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "COScheckbox");
					String Multiportcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"MultiPostcheckbox");
					String ENNIcheckBox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_ENNI");
					String perCoSperformanceReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_perCoSperformanceReport");
					String actelisBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_actelisBased");
					String standardCIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_standardCIR");
					String standardEIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_standardEIR");
					String premiumCIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_premiumCIR");
					String premiumEIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_premiumEIR");
					String EVPNtecnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_EVPNtechnology");
					String HCoSPerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_HCoSPerformanceReporting");


		String orderPanel = (Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
		scrollDown(orderPanel);
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.Editservice_actiondropdown,"Editservice Actiondropdown");
		Reporter.log("Action dropdown is working");
		waitforPagetobeenable();
		click(Lanlink_InternationalObj.International.Editservice_Editlink,"Editservice Editlink");
		waitforPagetobeenable();

		if (modularmsp.equalsIgnoreCase("no")) {

			if (Interfacespeed.equalsIgnoreCase("10GigE")) {
				Edit_DirectFibre10G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
						PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
						ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference,
						circuitTypeUsedInServiceCreation, notificationManagement);
			}

			else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
				Edit_DirectFibre1G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
						PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
						ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference,
						circuitTypeUsedInServiceCreation, notificationManagement, perCoSperformanceReport, actelisBased,
						standardCIR, standardEIR, premiumCIR, premiumEIR);
			}
		} else {

			ExtentTestManager.getTest().log(LogStatus.FAIL, "Edit Service for Direct Fiber cannot be performed as 'modular MSP' checkbox is selected");
					Report.LogInfo("INFO", "Edit Service for Direct Fiber cannot be performed as 'modular MSP' checkbox is selected","FAIL");

		}
		waitforPagetobeenable();
	}
	
	
	public void Edit_DirectFibre1G(String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed,
			String EndpointCPE, String Email, String PhoneContact, String remark, String performanceReporting,
			String ProactiveMonitoring, String deliveryChannel, String ManagementOrder, String vpnTopology,
			String intermediateTechnology, String CircuitReference, String CircuitType, String notificationManagement,
			String perCoSperformanceReport, String actelisBased, String standardCIR, String standardEIR,
			String premiumCIR, String premiumEIR) throws InterruptedException, IOException {

		// Service Identification
		editService_serviceIdentification(ServiceIdentificationNumber);

		// Endpoint CPE
		editService_singleEndPointCPE(EndpointCPE, vpnTopology);

		// Email
		//editService_Email(Email);

		// Phone contact `
		editService_phoneContact(PhoneContact);

		// Remark
		editService_remark(remark);

		// Performance Reporting
		boolean perfrmReportAvailability = false;
		try {
			perfrmReportAvailability = isElementPresent(Lanlink_InternationalObj.International.performanceReportingcheckbox);

			if (perfrmReportAvailability) {

				Report.LogInfo("Info",
						" 'Performance reporting' checkbox is displaying in 'Edit Service' page as expected", "PASS");

				if (!performanceReporting.equalsIgnoreCase("null")) {

					boolean perfReport = isSelected(Lanlink_InternationalObj.International.performanceReportingcheckbox,
							"performanceReportingcheckbox");
					waitforPagetobeenable();

					if (performanceReporting.equalsIgnoreCase("yes")) {

						if (perfReport) {

							Report.LogInfo("Info",
									"Performance Reporting checkbox is not edited and it is already Selected while creating",
									"PASS");

						} else {

							click(Lanlink_InternationalObj.International.performanceReportingcheckbox,"performanceReportingcheckbox");
							Reporter.log("Performance Reporting check box is selected");
							Report.LogInfo("Info", "Performance Reporting checkbox is selected", "PASS");
						}
					} else if (performanceReporting.equalsIgnoreCase("no")) {

						if (perfReport) {

							click(Lanlink_InternationalObj.International.performanceReportingcheckbox,"performanceReportingcheckbox");
							Reporter.log("Performance Reporting check box is unselected");
							Report.LogInfo("Info", "Performance Reporting is edited and gets unselected", "PASS");

						} else {
							Report.LogInfo("Info", "Performance Reporting is not edited and it remains unselected",
									"PASS");
						}

					}
				} else {
					Report.LogInfo("Info", "No changes made for Performance Reporting chekbox", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Performance reporting' checkbox is not available in 'Edit Service' page",
						"FAIL");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " ' Performance Reporting' checkbox is not displaying under 'Edit service' page",
					"FAIL");
			Reporter.log(" ' Performance Reporting' checkbox is not displaying under 'Edit service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to click on 'Performance Reporting' checkbox", "FAIL");
			Reporter.log(" Not able to click on 'erformance Reporting' checkbox");
		}

		// Per CoS Performance Reporting
		if (performanceReporting.equalsIgnoreCase("yes")) {
			boolean perCoSdisplay = false;
			try {
				perCoSdisplay = isElementPresent(Lanlink_InternationalObj.International.perCoSperformncereport);
				waitforPagetobeenable();

				if (perCoSdisplay) {
					Report.LogInfo("Info",
							" 'Per CoS Performance Reporting' checkbox is displaying in 'Edit Service' page", "PASS");
					if (!perCoSperformanceReport.equalsIgnoreCase("null")) {

						boolean perCoSreport = isSelected(Lanlink_InternationalObj.International.perCoSperformncereport,
								"perCoSperformncereport");
						waitforPagetobeenable();

						if (perCoSperformanceReport.equalsIgnoreCase("yes")) {

							if (perCoSreport) {

								Report.LogInfo("Info",
										"Per CoS Performance Reporting checkbox is not edited and it is already Selected while creating",
										"PASS");

							} else {

								click(Lanlink_InternationalObj.International.perCoSperformncereport,"perCoSperformncereport");
								Reporter.log("Per CoS Performance Reporting check box is selected");
								Report.LogInfo("Info", "Per CoS Performance Reporting is selected", "PASS");
							}
						}

						else if (perCoSperformanceReport.equalsIgnoreCase("no")) {

							if (perCoSreport) {

								click(Lanlink_InternationalObj.International.perCoSperformncereport,"perCoSperformncereport");
								Reporter.log("Per CoS Performance Reporting check box is unselected");
								Report.LogInfo("Info", "PPer CoS Performance Reporting is edited and gets unselected",
										"PASS");

							} else {
								Report.LogInfo("Info",
										"Per CoS Performance Reporting is not edited and it remains unselected",
										"PASS");
							}

						}
					} else {
						Report.LogInfo("Info", " 'Per CoS Performance Reporting' chekbox is not edited", "PASS");
					}
				} else {
					Report.LogInfo("Info",
							" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page",
							"FAIL");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info",
						" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page",
						"FAIL");
				Reporter.log(" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to click on 'Per CoS Performance Reporting' checkbox", "FAIL");
				Reporter.log(" Not able to click on 'Per CoS Performance Reporting' checkbox");
			}
		}

		// Actelis Based
		if (perCoSperformanceReport.equalsIgnoreCase("yes")) {
			boolean actelisbased = false;
			if (vpnTopology.equalsIgnoreCase("Point-to-Point")) {
				try {
					actelisbased = isElementPresent(Lanlink_InternationalObj.International.ActelisBasedcheckbox);
					waitforPagetobeenable();
					if (actelisbased) {
						Report.LogInfo("Info", " Actelis Based checkbox is dipslaying in 'Edit Serivce' page", "PASS");
						if (!actelisBased.equalsIgnoreCase("null")) {

							boolean actelsBased = isSelected(Lanlink_InternationalObj.International.ActelisBasedcheckbox,
									"ActelisBasedcheckbox");
							waitforPagetobeenable();

							if (actelisBased.equalsIgnoreCase("yes")) {

								if (actelsBased) {

									Report.LogInfo("Info",
											"Actelis Based checkbox is not edited and it is already Selected while creating",
											"PASS");

								} else {

									click(Lanlink_InternationalObj.International.ActelisBasedcheckbox,"ActelisBasedcheckbox");
									Reporter.log("Actelis Based check box is selected");
									Report.LogInfo("Info", "Actelis Based checkbox is selected", "PASS");
								}
							}

							else if (actelisBased.equalsIgnoreCase("no")) {

								if (actelsBased) {

									click(Lanlink_InternationalObj.International.ActelisBasedcheckbox,"ActelisBasedcheckbox");
									Reporter.log("Per CoS Performance Reporting check box is unselected");
									Report.LogInfo("Info", "Actelis Based is edited and gets unselected", "PASS");

								} else {
									Report.LogInfo("Info",
											"Actelis Based checkbox is not edited and it remains unselected", "PASS");
								}

							}
						} else {
							Report.LogInfo("Info", "No changes made for Actelis Based chekbox", "PASS");
						}
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info", " Actelis Based checkbox is not dipslaying in 'Edit Serivce' page", "FAIL");
				} catch (Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info", " Not able to click on 'Acteis' checkbox", "FAIL");
					Reporter.log(" Not able to click on 'Acteis' checkbox");
				}
			} else {
				Reporter.log(
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', 'Actelis' checkbox will not display");
				Report.LogInfo("Info",
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', 'Actelis' checkbox will not display",
						"PASS");
			}
		}

		if (actelisBased.equalsIgnoreCase("yes")) {

			if (vpnTopology.equalsIgnoreCase("Point-to-Point")) {

				// Standard CIR
				editService_standardCIR(standardCIR);

				// Standard EIR
				editService_standardEIR(standardEIR);

				// Premium CIR
				editService_premiumCIR(premiumCIR);

				// Premium EIR
				editService_premiumEIR(premiumEIR);
			} else {
				Reporter.log(
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', Standard CIR_Eir Premium CIR_EIR fields will not display");
				Report.LogInfo("Info",
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', Standard CIR_Eir Premium CIR_EIR fields will not display",
						"PASS");
			}
		}

		// Proactive monitoring
		editService_proactiveMonitoring(ProactiveMonitoring, notificationManagement);

		// Delivery channel
		editService_deliveryChannel(deliveryChannel);

		// VPN topology
		////// scrolltoend();
		waitforPagetobeenable();

		boolean vpnDisplayedValues = false;

		vpnDisplayedValues=webDriver.findElement(By.xpath("//div[contains(text(),'"+ vpnTopology +"')]")).isDisplayed();


		if (vpnDisplayedValues) {
			Report.LogInfo("Info", vpnTopology + " is displaying under 'VPN Topology' as expected", "PASS");
			if (vpnTopology.equals("Point-to-Point")) {

				// Intermediate technologies
				editService_IntermediateTechnology(intermediateTechnology);

				// Circuit reference
				editService_circuitreference(CircuitReference);

				// Circuit type
				editService_circuitType(CircuitType);

			}

			else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

				editService_circuitreference(CircuitReference);
				waitforPagetobeenable();
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'VPN Topology' value is not displaying as expected."
					+ "  Expected vlue is: "+ vpnTopology);
					
		}

		Reporter.log("going to click on OK buttto");
		////// scrolltoend();
		waitforPagetobeenable();

		// click on "Ok button to update
		click(Lanlink_InternationalObj.International.okbutton, "OK");
		waitforPagetobeenable();

	}
	
	
	public void editService_serviceIdentification(String ServiceIdentificationNumber)
			throws InterruptedException, IOException {

		boolean serviceAvailability = false;
		serviceAvailability = findWebElement(Lanlink_InternationalObj.International.ServiceIdentification)
				.isDisplayed();

		if (serviceAvailability) {

			ExtentTestManager.getTest().log(LogStatus.PASS,
					" 'Service Identification' field is displaying in 'Edit Service'page as expected");

			if (!ServiceIdentificationNumber.equalsIgnoreCase("null")) {

				clearTextBox(Lanlink_InternationalObj.International.ServiceIdentification);
				waitforPagetobeenable();
				sendKeys(Lanlink_InternationalObj.International.ServiceIdentification,ServiceIdentificationNumber,"ServiceIdentificationNumber");

				String Actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.ServiceIdentification,"value");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Edited value for 'Service Identification' field is " + Actualvalue);
				Reporter.log("Edited value for 'Service Identification' field is " + Actualvalue);

			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, " Service Identification field value is not edited");
				Reporter.log(" Service Identification field value is not edited");
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					" 'Service Identification' field is not availale in 'Edit Service'page");
		}
	}
	
	public void editService_singleEndPointCPE(String EndpointCPE, String Topology)
			throws InterruptedException, IOException {
		boolean CPEAvailability = false;

		if (Topology.equalsIgnoreCase("Point-to-Point")) {

			try {
				CPEAvailability = isElementPresent(Lanlink_InternationalObj.International.EndpointCPE);
				if (CPEAvailability) {

					Report.LogInfo("Info", "Single EndPoint CPE checkbox is displaying under 'Edit Service' page",
							"PASS");

					if (!EndpointCPE.equalsIgnoreCase("null")) {

						boolean endpoint = isSelected(Lanlink_InternationalObj.International.EndpointCPE, "EndpointCPE");
						waitforPagetobeenable();

						if (EndpointCPE.equalsIgnoreCase("yes")) {

							if (endpoint) {

								Report.LogInfo("Info",
										"Endpoint CPE is not edited. It is already Selected while creating.", "PASS");

							} else {

								click(Lanlink_InternationalObj.International.EndpointCPE,"EndpointCPE");
								Reporter.log("End point CPE check box is edited and it got selected");
								Report.LogInfo("Info", "Endpoint CE is edited and it got selected", "PASS");
							}
						} else if (EndpointCPE.equalsIgnoreCase("no")) {
							if (endpoint) {

								click(Lanlink_InternationalObj.International.EndpointCPE,"EndpointCPE");
								Reporter.log("End point CPE check box is unselected");
								Report.LogInfo("Info", "Endpoint CE is edited and it is unselected as Expected",
										"PASS");

							} else {
								Report.LogInfo("Info",
										"Endpoint CPE is not edited. It was not selected during service creation and it remains unselected as expected",
										"PASS");
							}

						}
					} else {
						Report.LogInfo("Info", "No changes made for EndPoint CPE chekbox as expected", "PASS");
					}
				} else {
					Report.LogInfo("Info", "Single EndPoint CPE checkbox is not available under 'Edit Service' page",
							"FAIL");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", "Single EndPoint CPE checkbox is not available under 'Edit Service' page",
						"FAIL");
				Reporter.log("Single EndPoint CPE checkbox is not available under 'Edit Service' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to click on 'Single endpoint CPE' checkbox", "FAIL");
				Reporter.log("Not able to click on 'Single endpoint CPE' checkbox");
			}

		} else {

			Report.LogInfo("Info", " 'Single Endpoint CPE' checkbox is not displaying as expected,"
					+ " when 'VPN Topology' selected as 'hub&spoke' or 'E-PN(Any to Any)", "PASS");

		}
	}
	
	public void editService_Email(String Email) throws InterruptedException, IOException {

		boolean emailAvailability = false;
		try {
			emailAvailability = isElementPresent(Lanlink_InternationalObj.International.Email);

			if (emailAvailability) {

				Report.LogInfo("Info", " 'Email' text field is displaying in 'Edit Service' page", "PASS");

				if (!Email.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_InternationalObj.International.Email);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.Email, Email, "Email");
					Report.LogInfo("Info", "Edited value for 'Email' field is " + Email, "PASS");

					String Actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.Email, "value");
				//	Report.LogInfo("Info", " 'Email' field is edited as: " + Actualvalue, "PASS");
				//	Reporter.log("'Email' field is edited as: " + Actualvalue);

				} else {
					Report.LogInfo("Info", "'Email' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Email' text field is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Email' text field is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'Email' text field is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " not able to edit 'Email' field", "FAIL");
		}

	}
	
	public void editService_phoneContact(String PhoneContact) throws InterruptedException, IOException {

		boolean phoneAvailability = false;
		try {
			phoneAvailability = isElementPresent(Lanlink_InternationalObj.International.PhoneContact);

			if (phoneAvailability) {

				Report.LogInfo("Info", " Phone Contact field is displaying in 'Edit Service' page as expected", "PASS");

				if (!PhoneContact.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_InternationalObj.International.PhoneContact);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.PhoneContact, PhoneContact, "Phone Contact");
					waitforPagetobeenable();
					String actualValue = getAttributeFrom(Lanlink_InternationalObj.International.PhoneContact, "value");
					Report.LogInfo("Info", "Edited value for 'Phone contact' field is " + actualValue, "PASS");
				} else {
					Report.LogInfo("Info", " 'Phone contact' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Phone Contact field is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Phone Contact field is not available in 'Edit Service' page", "FAIL");
			Reporter.log("Phone Contact field is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'phone Contact' field", "FAIL");
			Reporter.log(" Not able to edit 'phone Contact' field");
		}
	}
	
	public void editService_remark(String remark) throws InterruptedException, IOException {

		boolean remarkAvailability = false;
		try {
			remarkAvailability = isElementPresent(Lanlink_InternationalObj.International.Remark);

			if (remarkAvailability) {

				Report.LogInfo("Info", " 'Remark' field is displaying in 'Edit Service' page as expected", "PASS");

				if (!remark.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_InternationalObj.International.Remark);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.Remark, remark, "Remark");

					String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.Remark, "value");
					Report.LogInfo("Info", "Edited value for 'Remark' field is " + actualvalue, "PASS");

				} else {
					Report.LogInfo("Info", " 'Remark' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Remark' field is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Remark' field is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'remark' field is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Remark' field", "FAIL");
			Reporter.log(" Not able to edit 'Remark' field");
		}
	}
	
	public void editService_standardCIR(String standardCIR) throws InterruptedException, IOException {

		try {
			boolean stndrdCIR = isElementPresent(Lanlink_InternationalObj.International.standardCIRtextfield);
			if (stndrdCIR) {
				Report.LogInfo("Info", " 'Standard CIR' field is displaying in 'Edit Service' page ", "PASS");
				if (!standardCIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_InternationalObj.International.standardCIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.standardCIRtextfield, standardCIR, "standardCIR");
					Report.LogInfo("Info", "Edited value for 'Standard CIR' field is " + standardCIR, "PASS");

				} else {
					Report.LogInfo("Info", " 'Standard CIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Standard CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Standard CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" 'Standard CIR' field is not displaying in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Standard CIR' field", "FAIL");
			Reporter.log(" Not able to edit 'Standard CIR' field");

		}
	}
	
	public void editService_standardEIR(String standardEIR) throws InterruptedException, IOException {

		boolean stndrdEIR = false;
		try {
			stndrdEIR = isElementPresent(Lanlink_InternationalObj.International.standardEIRtextfield);
			if (stndrdEIR) {
				Report.LogInfo("Info", " Standard EIR' field is displaying in 'Edit Service' page as expected", "PASS");
				if (!standardEIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_InternationalObj.International.standardEIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.standardEIRtextfield, standardEIR, "standardEIR");

					String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.standardEIRtextfield, "value");
					Report.LogInfo("Info", "Edited value for 'Standard EIR' field is " + actualvalue, "PASS");

				} else {
					Report.LogInfo("Info", " 'Standard EIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Standard EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Standard EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" 'Standard EIR' field is not displaying in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Standard EIR' field", "FAIL");
			Reporter.log(" Not able to edit 'Standard EIR' field");

		}
	}
	
	public void editService_premiumCIR(String premiumCIR) throws InterruptedException, IOException {

		boolean premumCIR = false;
		try {
			premumCIR = isElementPresent(Lanlink_InternationalObj.International.premiumCIRtextfield);
			if (premumCIR) {
				Report.LogInfo("Info", " Premise CIR' field is displaying in 'Edit Service' page as expected", "PASS");
				if (!premiumCIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_InternationalObj.International.premiumCIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.premiumCIRtextfield, premiumCIR, "premiumCIR");

					String actualValue = getAttributeFrom(Lanlink_InternationalObj.International.premiumCIRtextfield, "value");
					Report.LogInfo("Info", "Edited value for 'Premium CIR' field is " + actualValue, "PASS");

				} else {
					Report.LogInfo("Info", " 'Premium CIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Premise CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Premise CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" Premise CIR' field is not displaying in 'Edit Service' page ");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Premise CIR' field", "FAIL");
			Reporter.log("Not able to edit 'Premise CIR' field");

		}
	}

	public void editService_proactiveMonitoring(String ProactiveMonitoring, String notificationManagement)
			throws InterruptedException, IOException {
		if (!ProactiveMonitoring.equalsIgnoreCase("null")) {

			boolean proactivemonitor = isSelected(Lanlink_InternationalObj.International.proactiveMonitoring,
					"proactive Monitoring");
			waitforPagetobeenable();

			if (ProactiveMonitoring.equalsIgnoreCase("yes")) {

				if (proactivemonitor) {

					Report.LogInfo("Info",
							"Proactive monitoring is not edited and it is already Selected while creating", "PASS");

					if (notificationManagement.equalsIgnoreCase("null")) {

						Report.LogInfo("Info", "No changes made to notification management", "PASS");

					} else {

						click(Lanlink_InternationalObj.International.notigymanagement_xbutton);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.notificationmanagement);
						waitforPagetobeenable();
						click(Lanlink_InternationalObj.International.fieldValidation1 + notificationManagement
								+ Lanlink_InternationalObj.International.fieldValidation2);
						waitforPagetobeenable();
						Report.LogInfo("Info",
								"Edited value for 'Notification Management' field is " + notificationManagement,
								"PASS");
					}

				} else {

					click(Lanlink_InternationalObj.International.proactiveMonitoring);
					Reporter.log("proactive monitoring check box is selected");
					Report.LogInfo("Info", "proactive monitoring is edited and gets selected", "PASS");

					if (notificationManagement.equalsIgnoreCase("null")) {

						Report.LogInfo("Info", "No changes made to notification management", "PASS");

					} else {

						click(Lanlink_InternationalObj.International.notigymanagement_xbutton);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.notificationmanagement);
						click(Lanlink_InternationalObj.International.fieldValidation1 + notificationManagement
								+ Lanlink_InternationalObj.International.fieldValidation2);
						Report.LogInfo("Info",
								"Edited value for 'Notification Management' field is " + notificationManagement,
								"PASS");
					}
				}
			}

			else if (ProactiveMonitoring.equalsIgnoreCase("no")) {

				if (proactivemonitor) {

					click(Lanlink_InternationalObj.International.proactiveMonitoring);
					Reporter.log("Proactive monitoring check box is unselected");
					Report.LogInfo("Info", "proactive monitoring is edited and gets unselected", "PASS");

				} else {
					Report.LogInfo("Info",
							"proactive monitoring was not selected during service creation and it remains unselected",
							"PASS");
				}
			}
		} else {
			Report.LogInfo("Info", "No changes made for 'Proactive monitoring' chekbox", "PASS");
		}
	}
	
	public void editService_premiumEIR(String premiumEIR) throws InterruptedException, IOException {

		boolean premumEIR = false;
		try {
			premumEIR = isElementPresent(Lanlink_InternationalObj.International.premiumEIRtextfield);
			if (premumEIR) {
				Report.LogInfo("Info", " Premise EIR' field is displaying in 'Edit Service' page as expected", "PASS");
				if (!premiumEIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_InternationalObj.International.premiumEIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.premiumEIRtextfield, premiumEIR, "premiumEIR");
					Report.LogInfo("Info", "Edited value for 'Premium EIR' field is " + premiumEIR, "PASS");

				} else {
					Report.LogInfo("Info", "  'Premium EIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Premise EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Premise EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" Premise EIR' field is not displaying in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Premise EIR' field", "FAIL");
			Reporter.log("Not able to edit 'Premise EIR' field");

		}
	}
	
	public void editService_deliveryChannel(String deliveryChannel) throws InterruptedException, IOException {

		boolean deliveryChannelAvailability = false;
		try {
			deliveryChannelAvailability = isElementPresent(Lanlink_InternationalObj.International.deliverychannel_withclasskey);

			if (deliveryChannelAvailability) {

				Report.LogInfo("Info", " 'Delivery channel' dropdown is displaying in 'Edit Service' page as expected",
						"PASS");

				if (!deliveryChannel.equalsIgnoreCase("null")) {

					click(Lanlink_InternationalObj.International.deliverychannel_xbutton,"deliverychannel_xbutton");
					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.deliverychannel_withclasskey,"deliverychannel_withclasskey");
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + deliveryChannel + "')]")).click();


					Report.LogInfo("Info", "Edited value for 'Delivery Channel' dropdown is " + deliveryChannel,
							"PASS");
					Reporter.log("Delivery channel dropdown value is edited as expected");

				} else {
					Report.LogInfo("Info", "Delivery channel dropdown value is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Delivery channel' dropdown is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Delivery channel' dropdown is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'Delivery channel' dropdown is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'delvery Channel' dropdown", "FAIL");
		}
	}
	
	public void editService_managementOrder(String ManagementOrder) throws InterruptedException, IOException {
		boolean managemenOrderAvailabiliy = false;
		try {
			managemenOrderAvailabiliy = isElementPresent(Lanlink_InternationalObj.International.managementOrder);

			if (managemenOrderAvailabiliy) {
				Report.LogInfo("Info", " 'Management Order' dropdown is displaying in 'Edit Service' page as expected",
						"PASS");

				if (!ManagementOrder.equalsIgnoreCase("null")) {

					click(Lanlink_InternationalObj.International.managementOrder_xButton,"managementOrder_xButton");
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.managementOrder,"managementOrder");
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + ManagementOrder + "')]")).click();

					waitforPagetobeenable();

					Report.LogInfo("Info", "Edited value for 'management Order' field is " + ManagementOrder, "PASS");
					Reporter.log("Edited value for 'management Order' field is " + ManagementOrder);

				} else {

					Report.LogInfo("Info", "No changes has been made to 'Management order' field", "PASS");
					Reporter.log("No changes has been made to 'Management order' field");
				}
			} else {
				Report.LogInfo("Info", " 'Management Order' dropdown is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Management Order' dropdown is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'Management Order' dropdown is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'management order' dropodwn", "FAIL");
			Reporter.log(" Not able to edit 'management order' dropodwn");
		}
	}
	
	public void editService_IntermediateTechnology(String intermediateTechnology)
			throws InterruptedException, IOException {
		

		boolean intTechAvilability = false;
		try {
			intTechAvilability = isElementPresent(Lanlink_InternationalObj.International.IntermediateTechnology);
			if (intTechAvilability) {
				Report.LogInfo("Info",
						"Intermediate Technologies' field is displaying under 'Edit Service' page as expected", "PASS");
				if (intermediateTechnology.equalsIgnoreCase("null")) {

					Report.LogInfo("Info", "No changes has made for 'Intermediate Technology' field", "PASS");

				} else {

					clearTextBox(Lanlink_InternationalObj.International.IntermediateTechnology);
					waitforPagetobeenable();

					sendKeys(Lanlink_InternationalObj.International.IntermediateTechnology, intermediateTechnology,
							"intermediate Technology");
					waitforPagetobeenable();
					Report.LogInfo("Info",
							"Edited value for 'Intermediate Technology' field is " + intermediateTechnology, "PASS");

				}
			} else {
				Report.LogInfo("Info", " 'Intermediate technologies' field is not displaying under 'edit Sevice' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Intermediate technologies' field is not displaying under 'edit Sevice' page",
					"FAIL");
			Reporter.log(" 'Intermediate technologies' field is not displaying under 'edit Sevice' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Intermediate technologies' field", "FAIL");
		}
	}
	
	public void editService_circuitreference(String CircuitReference) throws InterruptedException, IOException {

		boolean circuitRefAvailability = false;

		circuitRefAvailability = isElementPresent(Lanlink_InternationalObj.International.circuitReference);
		if (circuitRefAvailability) {
			if (CircuitReference.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", " 'Circuit reference' field is not edited", "PASS");
			} else {

				clearTextBox(Lanlink_InternationalObj.International.circuitReference);
				waitforPagetobeenable();

				sendKeys(Lanlink_InternationalObj.International.circuitReference, CircuitReference, "Circuit Reference");
				waitforPagetobeenable();
				String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.circuitReference, "value");
				Report.LogInfo("Info", "Edited value for 'Circuit reference' field is " + actualvalue, "PASS");
			}
		} else {
			Report.LogInfo("Info", " 'Circuit Reference' field is not displaying in 'Edit CPE device' page", "FAIL");
		}
	}
	
	public void editService_circuitType(String CircuitType) throws InterruptedException {

		try {
			if (CircuitType.equalsIgnoreCase("null")) {

			//	Report.LogInfo("Info", " 'Circuit type' field is not edited", "PASS");

			} else {

				// verify whether it is selected
				boolean circuitType=webDriver.findElement(By.xpath("//div[span[contains(text(),'" + CircuitType + "')]]//input")).isSelected();

				if (circuitType) {

					//Report.LogInfo("Info", " 'Circuit Type' value selected as : " + CircuitType + " as expected",
						//	"PASS");

				} else {
					//Report.LogInfo("Info", " 'Circuit Type' value is not selected as : " + CircuitType, "FAIL");
				}

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			//Report.LogInfo("Info", " 'Circuit type' " + CircuitType + " is not dipslaying under 'Edit Service page",
				//	"FAIL");
		//	Reporter.log(" 'Circuit type' " + CircuitType + " is not dipslaying under 'Edit Service page");
		} catch (Exception er) {
			er.printStackTrace();
		//	Report.LogInfo("Info", " 'Circuit type' " + CircuitType + " is disabled under 'Edit Service page", "PASS");
		//	Reporter.log(" 'Circuit type' " + CircuitType + " is disabled under 'Edit Service page");
		}
	}
	
	public void Edit_DirectFibre10G(String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed,
			String EndpointCPE, String Email, String PhoneContact, String remark, String performanceReporting,
			String ProactiveMonitoring, String deliveryChannel, String ManagementOrder, String vpnTopology,
			String intermediateTechnology, String CircuitReference, String CircuitType, String notificationManagement)
			throws InterruptedException, IOException {

		// Service Identification
		editService_serviceIdentification(ServiceIdentificationNumber);

		// Endpoint CPE
		editService_singleEndPointCPE(EndpointCPE, vpnTopology);

		// Email
		editService_Email(Email);

		// Phone contact
		editService_phoneContact(PhoneContact);

		// Remark
		editService_remark(remark);

		// Performance Reporting
	//	editService_performancereporting_10G(performanceReporting);

		// Proactive monitoring
		editService_proactiveMonitoring(ProactiveMonitoring, notificationManagement);

		scrollDown(Lanlink_InternationalObj.International.ServiceIdentification);

		waitforPagetobeenable();

		// Delivery channel
		editService_deliveryChannel(deliveryChannel);

		// Management order
		editService_managementOrder(ManagementOrder);

		// Scroll to botton
		////// scrolltoend();
		waitforPagetobeenable();

		// VPN topology
		boolean vpnDisplayedValues=	webDriver.findElement(By.xpath("//div[contains(text(),'"+ vpnTopology +"')]")).isDisplayed();
		if(vpnDisplayedValues)
		if (vpnDisplayedValues) {
			Report.LogInfo("Info", vpnTopology + " is displaying under 'VPN Topology' as expected", "PASS");

			if (vpnTopology.equals("Point-to-Point")) {

				// Intermediate Technologies
				editService_IntermediateTechnology(intermediateTechnology);

				// Circuit reference
				editService_circuitreference(CircuitReference);

				// Circuit Type
				editService_circuitType(CircuitType);

			}

			else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

				// Circuit reference
				editService_circuitreference(CircuitReference);

			}

		} else {
			Report.LogInfo("Info", vpnTopology + " value is not displaying as expected.", "FAIL");
		}

		Report.LogInfo("Info", "Values has been Edited for Direct Fiber subtype under lanlink Service", "PASS");

		// click on "Ok button to update
		click(Lanlink_InternationalObj.International.okbutton, "OK");
		waitforPagetobeenable();

	}
	
	public void syncservices() throws IOException, InterruptedException 
	{

	//String orderPanel= getTextFrom(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.Editservice_actiondropdown,"Action");
	waitforPagetobeenable();
	click(Lanlink_InternationalObj.International.Editservice_sysnchronizelink,"Synchronize link");
	waitforPagetobeenable();

	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	 verifysuccessmessage("Sync started successfully. Please check the sync status of this service.");
	}
	
	
	public void shownewInfovista() throws IOException, InterruptedException 
	{

	///String orderPanel= getTextFrom(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.Editservice_actiondropdown,"Action");
	waitforPagetobeenable();
	click(Lanlink_InternationalObj.International.Editservice_infovistareport,"Show infovista link");
	waitforPagetobeenable();

	String expectedPageName= "SSO login Page";

	//Switch to new tab
	List<String> browserTabs = new ArrayList<String> (webDriver.getWindowHandles());
	webDriver.switchTo().window(browserTabs .get(1));
	waitforPagetobeenable();

	try { 
	// Get Tab name
	String pageTitle=webDriver.switchTo().window(browserTabs .get(1)).getTitle();
	Reporter.log("page title displays as: "+pageTitle);


	waitforPagetobeenable();
	webDriver.switchTo().window(browserTabs.get(0)); 

	//sa.assertEquals(pageTitle, expectedPageName, " on clicking 'Show Infovista link', it got naviagted to "+pageTitle);

	//sa.assertAll();

	waitforPagetobeenable();

	}catch(AssertionError e) {

	e.printStackTrace();

	waitforPagetobeenable();
	webDriver.switchTo().window(browserTabs.get(0));


	}

	}
	
	public void manageSubnets() throws IOException, InterruptedException  {

		//String orderPanel= getTextFrom(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
		scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.Editservice_actiondropdown,"Edit service action drop down");
		waitforPagetobeenable();
		click(Lanlink_InternationalObj.International.Editservice_managesubnets,"Manage Subnet");
		waitforPagetobeenable();

		boolean manageSubnetPage=false;
		try { 
		manageSubnetPage=isElementPresent(getTextFrom(Lanlink_InternationalObj.International.manageSubnetPage_header));
		if(manageSubnetPage) {
		Reporter.log("'Manage Subnet' page is displaying");

		String errMsg=getTextFrom(Lanlink_InternationalObj.International.manageSubnet_errMsg);
		if(errMsg.isEmpty()) {

		Reporter.log("No messages displays under 'manage Subnet' page");
		}
		else
		{
		Reporter.log(" Message in 'Manage Subnet' page displays as "+errMsg);
		}

		}else
		{
		Reporter.log("'Manage Subnet' page is not displaying");
		}
		}
		catch(Exception e)
		{
		e.printStackTrace();
		Reporter.log("'Manage Subnet' page is not displaying");
		}

		click(Lanlink_InternationalObj.International.cancelButton,"Cancel"); 
		waitforPagetobeenable();

		}
	
	public void dump_viewServicepage() throws IOException, InterruptedException 
	{

	//String orderPanel= getTextFrom(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.Editservice_actiondropdown,"Action");
	waitforPagetobeenable();
	click(Lanlink_InternationalObj.International.Editservice_Dumplink, "Dump");
	waitforPagetobeenable();	   

	waitToPageLoad();

	String dumpelement= getTextFrom(Lanlink_InternationalObj.International.dump_container);

	String dumpvalue=dumpelement;

	if(dumpvalue.isEmpty()) {

	Reporter.log("NO values dipslaying under 'Dump' page");

	}else{
	Reporter.log("Dump value is displaying as:   "+ dumpvalue);
	}

	webDriver.navigate().back();
	waitforPagetobeenable();

	}
	
	public void Enteraddsiteorder() throws IOException, InterruptedException 
	{

		waitforPagetobeenable();
	
		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	//waitforPagetobeenable();
	//verifyExists(Lanlink_InternationalObj.International.Actiondropdown_siteorder,"Action Dropdown Siteorder");
	//click(Lanlink_InternationalObj.International.Actiondropdown_siteorder,"Action Dropdown Siteorder");
	waitforPagetobeenable();
	verifyExists(Lanlink_InternationalObj.International.Addsiteorderlink,"Addsite Orderlink");

	click(Lanlink_InternationalObj.International.Addsiteorderlink,"Addsite Orderlink");
}
	
	
	public void verifyAddsiteorderFields(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
		
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String offnetSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_Offnet");
		String EPNoffnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNoffnet");
		String EPNEOSDHselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNEOSDH");
		String modularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
		String TrafficAgregatorcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AggregateTraffic");
		String editTrafficAggregatorcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_AggregateTraffic");

		
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'add Site Order' fields");
		
String TrafficAgregator = "Null";
		
		if(editTrafficAggregatorcheckbox.equalsIgnoreCase("Null")) {
			TrafficAgregator = TrafficAgregatorcheckbox;
		}else {
			TrafficAgregator = editTrafficAggregatorcheckbox;
		}
		
		if(TrafficAgregator.equalsIgnoreCase("Yes")) {
			
			if(VPNtopology.equalsIgnoreCase("Hub&Spoke")) {
				verifySiteOrderForHubAndSpoke_trafficAggregatorSelected();
			}
			else if(VPNtopology.equalsIgnoreCase("E-PN (Any-to-Any)")) {
				verifySiteOrderForE_PN_trafficAggregatorSelected();
			}
			
		}
		else if(TrafficAgregator.equalsIgnoreCase("No")) {
			
			if( (VPNtopology.equals("Point-to-Point")) & (circuitType.equals("Default"))) {
				
				verifySiteOrderForPoint_to_point(interfaceSpeed, modularMSP);
				
			}
			else if(VPNtopology.equals("Point-to-Point") &&  (circuitType.equals("Extended Circuit"))) {
				 
				verifySiteOrderForPoint_to_point_extendedCircuit(interfaceSpeed, modularMSP);
				
			 }
			else if(VPNtopology.equals("Hub&Spoke")) {
				
				if(offnetSelection.equalsIgnoreCase("No")) {
				
				verifySiteOrderForHubAndSpoke( interfaceSpeed, modularMSP);

			}
				if(offnetSelection.equalsIgnoreCase("Yes")) {
					if(modularMSP.equalsIgnoreCase("yes")) {
						
						ExtentTestManager.getTest().log(LogStatus.INFO, " 'offnet' checkbox does not display for 'Modular MSP' service");
						
						verifySiteOrderForHubAndSpoke_MSPselectedAndoffnet( interfaceSpeed, modularMSP);
						
					}else {
						verifySiteOrderForHubAndSpoke_offnetSelected( interfaceSpeed, modularMSP);
					}
					
				}
				
			}else if(VPNtopology.equals("E-PN (Any-to-Any)")) {
				
				if(EPNEOSDHselection.equalsIgnoreCase("No")) {
				
				verifySiteOrderForE_PN(interfaceSpeed, modularMSP);
				
				}
				else if(EPNEOSDHselection.equalsIgnoreCase("yes")) {
					
					verifySiteOrderForEPN_EOSDHselected(interfaceSpeed, modularMSP);
					
					}
			}
			
		}
	}
	
	public void verifySiteOrderForPoint_to_point(String interfaceSpeed)
			throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Country Error message
						verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

						// City Error message
						verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

						// CSR name Error message
						verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg, "CSR Name");

						// Technology Error message
						verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");

		/*	// Circuit Reference Error Message
			verifyExists(Lanlink_InternationalObj.International.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_InternationalObj.International.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");
*/
			
			// Validate site order Number Field
			// -- validatesiteOrderNumber_AddSiteOrder();

			// Validate Country dropdown
			validateCountry_AddSiteorder();

			// Validate City Fields
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			validateSite_AddSiteOrder();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			validateSmartsMOnitoring_AddSiteOrder();

			// Validate Site Alias field
			Reporter.log("validate Site Alias fields");
			validateSiteAlias_AddSiteOrder();

			// Validate VLAN Id field
			Reporter.log("validate VLAn Id fields");
			validateVlanID_AddSiteOrder();

			// Validate DCA Enabled Site and Cloud Service Provider dropdown
			Reporter.log("validate DCA enabled site checkbox");
			valiadateDCAEnabledsite_AddSieOrder();

			// Verify Remark field
			Reporter.log("validate Remark fields");
			validateRemark_AddSiteOrder();
			
				if (interfaceSpeed.equals("1GigE")) {

					technologyDropdownFor1GigE();

				} else if (interfaceSpeed.equals("10GigE")) {

					technologyDropdownFor10GigE();
				}
			

			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_InternationalObj.International.Addsiteorder_cancel);
			waitforPagetobeenable();

		//	sa.assertAll();

		} catch (AssertionError e) {
			e.printStackTrace();
		}
	
	

}
	
	public void validateCountry_AddSiteorder() throws InterruptedException, IOException {
		
		boolean COuntry=false;
		
		COuntry = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Country);
	//	sa.assertTrue(COuntry, "Country dropdown is not displayed");
		if(COuntry) {
			Report.LogInfo("Info"," 'Country' mandatory dropdown is displaying under 'Add Site Order' page as expected","PASS");	
			Reporter.log("Country dropdown is displaying");
		scrollUp();
		click(Lanlink_InternationalObj.International.Addsiteorder_Country,"Country");
		
		List<WebElement> listofcountry = findWebElements(Lanlink_InternationalObj.International.ClassNameForDropdowns);

		if(listofcountry.size()>=1) {
		for (WebElement countrytypes : listofcountry) {
			
			Report.LogInfo("Info","The list of country inside dropdown is: "+countrytypes,"PASS");
			
		}
	}else {
		Reporter.log("no values are available inside Country dropdown for Add site order");
		Report.LogInfo("Info","no values are available inside Country dropdown for Add site order","FAIL");
	}

	//click on Blank page	
		clickOnBankPage();
		Thread.sleep(3000);
		
	}else {
		Report.LogInfo("Info", " 'Country' mandatory dropdown is not available under 'Add Site Order' page","FAIL");
	}
		
	}
	
	public void validateCity_AddSiteOrder() throws InterruptedException, IOException {
		
		
		// City dropdown
		boolean CIty = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_City);
	//	sa.assertTrue(CIty, "City dropdown is not displayed");
		if(CIty) {
			Report.LogInfo("Info",  " 'City' mandatory dropdown is displaying under 'Add Site Order' page as expected","PASS");
		
	}else {
		Report.LogInfo("Info",  " 'City' mandatory dropdown is not available under 'Add Site Order' page","FAIL");
	}

			
		//select city toggle button
		boolean selectcitytoggle=isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Citytogglebutton);
		//sa.assertTrue(selectcitytoggle, "Select city toggle button for Add Site is not available");
		if(selectcitytoggle) {
			Report.LogInfo("Info",  " 'Select City' toggle button is displaying under 'Add Site Order' page as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'Select City' toggle button is not avilable under 'Add Site Order' page ","FAIL");
		}
		
		
		click(Lanlink_InternationalObj.International.Addsiteorder_Citytogglebutton);
		waitforPagetobeenable();
		
		scrollDown(Lanlink_InternationalObj.International.obutton_spanTag);
		waitforPagetobeenable();
		
		Reporter.log("Scrolling down to validate error messgae for City name and city code");
		//Click on Next button to get warning message for XNG City name and XNG City Code text fields
		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();
		
		scrollDown(Lanlink_InternationalObj.International.Addsiteorder_Country);
		
		waitforPagetobeenable();
		
		Reporter.log("scrolling above till device country for validating error message for 'city name ' and 'city code'");
		//XNG City Name Error message	
		boolean xngCitynameErr = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_xngcitynameerrmsg);
	//	sa.assertTrue(xngCitynameErr, " 'XNG City Name' warning message is not displayed ");
		if(xngCitynameErr) {
			Report.LogInfo("Info",  " Warning message for 'XNG City Name' dropdown is displying under 'Add Site order' page as expected ","PASS");
			String citynameErrMsg = getTextFrom(Lanlink_InternationalObj.International.Addsiteorder_xngcitynameerrmsg);
			Reporter.log(
					"XNG City Name  message displayed as : " + citynameErrMsg);
			Report.LogInfo("Info", 
					" validation message for 'XNG City Name' text field displayed as : " + citynameErrMsg,"PASS");
			Reporter.log("XNG City Name warning message displayed as : " + citynameErrMsg);
			
		}else {
			Report.LogInfo("Info",  " Warning message for 'XNG City Name' dropdown is not displaying under 'Add Site order' page ","FAIL");
		}


		
		//XNG City Code Error message	
		boolean xngCitycodeErr = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_xngcityCodeerrmsg);
	//	sa.assertTrue(xngCitycodeErr, " 'XNG City Code' warning message is not displayed ");
		if(xngCitycodeErr) {
			Report.LogInfo("Info",  " Warning message for 'XNG City Code' dropdown is displying under 'Add Site order' page as expected ","PASS");
			String cityCodeErrMsg = getTextFrom(Lanlink_InternationalObj.International.Addsiteorder_xngcityCodeerrmsg);
			Reporter.log(
					"XNG City Name  message displayed as : " + cityCodeErrMsg);
			Report.LogInfo("Info", 
					" validation message for 'XNG City Code' text field displayed as : " + cityCodeErrMsg,"PASS");
			Reporter.log("XNG City Code warning message displayed as : " + cityCodeErrMsg);
			
		}else {
			Report.LogInfo("Info",  " Warning message for 'XNG City Code' dropdown is not displaying under 'Add Site order' page ","FAIL");
		}


	//xng city name
	boolean XNGcityname=isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_xngcityname);
	//sa.assertTrue(XNGcityname, "XNG city name field for Add Site is not available");
	if(XNGcityname) {
		Report.LogInfo("Info",  " 'XNG City name field is displaying under 'Add Site order' as expected","PASS");
	}else {
		Report.LogInfo("Info",  " 'XNG City name field is not available under 'Add Site order'","FAIL");
	}

	//xng city code
	boolean XNGcitycode=isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_XNGcitycode);
//	sa.assertTrue(XNGcitycode, "XNG city code field for Add Site is not available");
	if(XNGcitycode) {
		Report.LogInfo("Info",  " 'XNG City code field is displaying under 'Add Site order' as expected","PASS");
	}else {
		Report.LogInfo("Info",  " 'XNG City code field is not available under 'Add Site order'","FAIL");
	}
	
	}
	
	public void validateSite_AddSiteOrder() throws InterruptedException, IOException 
	   {
			 
			 
			// CSR name field
			 boolean csr_name=false;
				csr_name = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_CSRname);
			//	sa.assertTrue(csr_name, "CSR_Name field is not displayed");
				if(csr_name) {
					Report.LogInfo("Info", " 'CSR Name' text field is displaying under 'Add Site order' page as expected","PASS");
					Reporter.log("CSR name field is dipslaying as expected");
				}else {
					Report.LogInfo("Info", " 'CSR Name' text field is not available under 'Add Site order' page","FAIL");
				}

			// click on site toggle button to check Physical site dropdown
				boolean sitetogglebutton=false;
				sitetogglebutton = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Sitetogglebutton);
			//	sa.assertTrue(sitetogglebutton, "select Site toggle button is not displayed");
				if(sitetogglebutton) {
					Reporter.log("site order toggle button is displaying as expected");
					Report.LogInfo("Info"," 'Select Site' toggle button is displaying under 'Add Site Order' page as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'Select Site' toggle button is not avilable under 'Add Site Order' page","FAIL");
				}

				
				click(Lanlink_InternationalObj.International.Addsiteorder_Sitetogglebutton);
				waitforPagetobeenable();

		//Check for Error message for physical Site
				scrollDown(Lanlink_InternationalObj.International.obutton_spanTag);
				waitforPagetobeenable();
				Reporter.log("scrolling down to click n OK button to find eror message for site Dropdown");
				click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
				waitforPagetobeenable();
				
				scrollDown(Lanlink_InternationalObj.International.Addsiteorder_Country);
				
				waitforPagetobeenable();
				
				Reporter.log("scrolling up back till device country dropodwn to find error message validation for physical site");
				boolean physicalsiteErr=false;
				physicalsiteErr = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_physicalsiteErrmsg);
		//		sa.assertTrue(physicalsiteErr, "Physical Site dropdown warning is not displayed ");
				if(physicalsiteErr) {
					Reporter.log("Physical Site Error message is displaying as expected");
					Report.LogInfo("Info", " 'Physite Site' dropdown warning message is displaying under 'Add Site Order' page as expected","PASS");
					String physicalsiteErrMsg = getTextFrom(Lanlink_InternationalObj.International.Addsiteorder_physicalsiteErrmsg);
					Reporter.log(
							"Physical Site  message displayed as : " + physicalsiteErrMsg);
					Report.LogInfo("Info",
							" validation message for Physical Site dropdown displayed as : " + physicalsiteErrMsg,"PASS");
					Reporter.log("Physical Site validation message displayed as : " + physicalsiteErrMsg);	
				}else {
					Report.LogInfo("Info", " 'Physical Site' dropdown warning message is not displaying under 'Add Site Order' page","FAIL");
					Reporter.log("Physical site warning message is not displaying");
				}
				
			

		//Physical Site dropdown
				boolean SIte=false;
				SIte = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_sitedropdown);
			//	sa.assertTrue(SIte, "PhysicalSite dropdown is not displayed");
				if(SIte) {
					Reporter.log("Physical Site dropdown is displaying as expected");
					Report.LogInfo("Info", " 'physical Site' dropdown is displaying under 'Add Site order' page as expected","PASS");

				}else {
					Report.LogInfo("Info", " 'Physical Site' dropdown is not available under 'Add Site Order' page","FAIL");
		}
		 }
	
	public void validatePerformancereporting_AddSiteOrder() throws InterruptedException, IOException {
		 
		 
		 String[] Performancereporting = { "Follow Service", "no" };

		// Performance reporting dropdown
		 boolean performancereport=false;
			performancereport = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_performancereporting);
		//	sa.assertTrue(performancereport, "performance reporting dropdown is not displayed");
			if(performancereport) {
				Report.LogInfo("Info", " 'Performance reporting' dropdown is displaying under 'Add Site order' as expected","PASS");
				waitforPagetobeenable();
				
				//check default value
				String performanceRprtDefaultValues=getTextFrom(Lanlink_InternationalObj.International.Addsiteorder_performancereportingdefaultvalue);
						Report.LogInfo("Info", performanceRprtDefaultValues+ " is displaying under 'Performance reporting' dropdown by default","PASS");

				//check list of values inside Performance Reporting drodpown		
			click(Lanlink_InternationalObj.International.Addsiteorder_performancereporting,"performancereporting");
			List<WebElement> listofperformancereporting = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));					
			
		if(listofperformancereporting.size()>=1) {	
			for (WebElement perfoemancereportingtypes : listofperformancereporting) {
				boolean match = false;
				for (int i = 0; i < Performancereporting.length; i++) {
					if (perfoemancereportingtypes.equals(Performancereporting[i])) {
						match = true;
						Reporter.log("list of performance reporting : " + perfoemancereportingtypes);
						Report.LogInfo("Info", "list of performance reporting for AddSite order : " + perfoemancereportingtypes,"PASS");
					}
					
				}
				
			//	sa.assertTrue(match);

			}
		}else {
			Reporter.log("no values are available inside performance reporting dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside performance reporting dropdown for Add site order","FAIL");
		}
	}else {
		Report.LogInfo("Info",  " 'Performance reporting' dropdown is not availble under 'Add Site order' ","FAIL");
	}
	}
	
	public void validateProactiveMonitoring_AddSiteOrder() throws InterruptedException, IOException {
		 
		 String[] Proactivemonitoring = { "Follow Service", "no" };

		// pro active monitoring
		 boolean proactivemonitoring=false;
			proactivemonitoring = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_proactivemonitoring);
		//	sa.assertTrue(proactivemonitoring, "pro active monitoring dropdown is not displayed");
			if(proactivemonitoring) {
				Report.LogInfo("Info"," 'Proactie Monitoring' dropdown is displaying under 'Add Site Order' page as Expected","PASS");
				
				//check default value
				String proactiveMonitorDefaultValues=getTextFrom(Lanlink_InternationalObj.International.Addsiteorder_proactivemonitoringdefaultvalue);
						Report.LogInfo("Info", proactiveMonitorDefaultValues+ " is displaying under 'roactive Monitoring' dropdown by default","PASS");

			click(Lanlink_InternationalObj.International.Addsiteorder_proactivemonitoring,"proactivemonitoring");
			List<WebElement> listofproactivemonitoring = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));			
		if(listofproactivemonitoring.size()>=1) {	
			for (WebElement proactivemonitoringtypes : listofproactivemonitoring) {

				boolean match = false;
				for (int i = 0; i < Proactivemonitoring.length; i++) {
					if (proactivemonitoringtypes.equals(Proactivemonitoring[i])) {
						match = true;
						Reporter.log("list of pro active monitoring : " + proactivemonitoringtypes);
						
						Report.LogInfo("Info","The list of proactive monitoring inside dropdown while  adding site order is: "+proactivemonitoringtypes,"PASS");
					}
			}
			//	sa.assertTrue(match);

			}
		}else {
			
			Reporter.log("no values are available inside pro active monitoring dropdown for Add site order");
			Report.LogInfo("Info","no values are available inside pro active monitoring dropdown for Add site order","FAIL");
		}
	}else {
		Report.LogInfo("Info", " 'Proactie Monitoring' dropdown is not available under 'Add Site Order' page ","FAIL");
	}
	}
	
	public void validateSmartsMOnitoring_AddSiteOrder() throws InterruptedException, IOException {
		 
		 String[] Smartmonitoring = { "Follow Service", "no" };

		// smarts monitoring
		 boolean smartmonitoring=false;
			smartmonitoring = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoring);
		//	sa.assertTrue(smartmonitoring, "Smart monitoring dropdown is not displayed");
			if(smartmonitoring) {
				Report.LogInfo("Info", " 'Smart Monitoring' dropdown is displaying under 'Add Site Order' page as expected","PASS");
				//check default value
				String smartmonitorDefaultValues=getTextFrom(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoringdefaultvalue);
				Report.LogInfo("Info", smartmonitorDefaultValues+ " is displaying under 'Smart Monitoring' dropdown by default","PASS");

			click(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoring,"smartmonitoring");
			List<WebElement> listofsmartmonitoring = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
		if(listofsmartmonitoring.size()>=1) {	
			for (WebElement smartmonitoringtypes : listofsmartmonitoring) {

				boolean match = false;
				for (int i = 0; i < Smartmonitoring.length; i++) {
					if (smartmonitoringtypes.equals(Smartmonitoring[i])) {
						match = true;
						Reporter.log("list of smart monitoring are : " + smartmonitoringtypes);
						Report.LogInfo("Info","The list of smart monitoring  inside dropdown while  adding site order is: "+smartmonitoringtypes,"PASS");
					}
				}
				
				//sa.assertTrue(match);
			}
		}else {

			Reporter.log("no values are available inside smart monitoring dropdown for Add site order");
			Report.LogInfo("Info","no values are available inside smart monitoring dropdown for Add site order","FAIL");
		}
			}else {
				Report.LogInfo("Info", " 'Smart Monitoring' dropdown is not avilable under 'Add Site Order' page","FAIL");
			}
	}
	
	public void validateSiteAlias_AddSiteOrder() throws InterruptedException, IOException {
		
		// Site alias Field
	 boolean sitealias=false; 
try { 				
sitealias = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_sitealias);
				//sa.assertTrue(sitealias, "Site alias field is not displayed");
				if(sitealias) {
					 Report.LogInfo("Info", " 'Site Alias' text field is displaying under 'Add Site order' page as expected","PASS");
				}else {
					 Report.LogInfo("Info"," 'Site Alias' text field is not displaying under 'Add Site order' page","FAIL");
				}
 }catch	(Exception e) {
	 e.printStackTrace();
	 Report.LogInfo("Info", " 'Site Alias' text field is not displaying under 'Add Site order' page","FAIL");
 }
 }
	
	public void validateVlanID_AddSiteOrder() throws InterruptedException, IOException {
		 boolean vlanid=false;
		 try {
					vlanid = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Vlanid);
				//	sa.assertTrue(vlanid, "VLAN id field is not displayed");
					if(vlanid) {
						 Report.LogInfo("Info",  "'VLAN ID' text field is displaying under 'Add Site order' page as expected","PASS");
					}else {
						 Report.LogInfo("Info",  " 'VLAN ID' text field is not displaying under 'Add Site order' page","FAIL");
					}
		 }catch(Exception e) {
			 e.printStackTrace();
			 Report.LogInfo("Info", " 'VLAN ID' text field is not displaying under 'Add Site order' page","FAIL");
		 }
	}
	
	public void valiadateDCAEnabledsite_AddSieOrder() throws InterruptedException,IOException {
	  	 
		   String[] cloudServiceprovider = { "Amazon Web Service", "Microsoft Azure" };
		   boolean DCAEnabledsite=false;

		   // DCA Enabled site
		   			DCAEnabledsite = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox);
		   		//	sa.assertTrue(DCAEnabledsite, "DCA enabled site is not displayed ");
		   			if(DCAEnabledsite) {
		   				
		   			Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is displaying under 'Add Site order' page as expected","PASS");	
		   			boolean DCAselection=isSelected(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox,"DCA enabled sitecheckbox");
		   		//	sa.assertFalse(DCAselection,"DCA checkbox under Addsite order is selected by default");
		   			if(DCAselection) {
		   				Report.LogInfo("Info", " ' DCA Enabled Site' checkbox should not be selected by default","FAIL");
		   			}else {
		   				Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not selected by default as expected","PASS");
		   						
		   			
		   			waitforPagetobeenable();
		   		// For Cloud service provider
		   			click(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox);
		   			Reporter.log("DCA site is enabled to add cloud service provider details");
		   			waitforPagetobeenable();
		   			
		   			boolean DCAafterSelection=isSelected(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox,"DCA enabled site checkbox");
		   			if(DCAafterSelection) {
		   			Report.LogInfo("Info","DCA site is selected to add cloud service provider details","PASS");
		   			waitforPagetobeenable();

		   			boolean cloudservice = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_cloudserviceProvider);
		   		//	sa.assertTrue(cloudservice, "cloud service provider dropdown is not displayed");
		   			if(cloudservice) {
		   				Report.LogInfo("Info", " 'Cloud Service Provider' dropdown is displaying when 'DCA Enabled Site' checkbox is selected as expected","PASS");

		   			click(Lanlink_InternationalObj.International.Addsiteorder_cloudserviceProvider);
		   			List<WebElement> listofcloudservices = webDriver
    						.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));		   					
		   			
		   		if(listofcloudservices.size()>0) {	
		   			for (WebElement cloudserviceprovidertypes : listofcloudservices) {

		   				boolean match = false;
		   				for (int i = 0; i < cloudServiceprovider.length; i++) {
		   					if (cloudserviceprovidertypes.equals(cloudServiceprovider[i])) {
		   						match = true;
		   						Reporter.log("list of cloud service providers are : " + cloudserviceprovidertypes);
		   						Report.LogInfo("Info","The list of cloudservice provider inside dropdown while  adding site order is: "+cloudserviceprovidertypes,"");
		   					}
		   				}
		   				//sa.assertTrue(match,"");
		   			}
		   		}else {
		   			Reporter.log("no values are available inside cloudservice provider dropdown for Add site order");
		   			Report.LogInfo("Info","no values are available inside cloudservice provider dropdown for Add site order","FAIL");
		   			
		   		}
		   			}else {
		   				Report.LogInfo("Info", " 'Cloud Service Provider' dropdown is not available when 'DCA Enabled Site' checkbox is selected","PASS");
		   			}
		   			
		   			}else {
		   				Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not getting selected","FAIL");
		   			}
		   	  }
		   		
		   			click(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox);
		   			waitforPagetobeenable();
		   			
		   	}else {
		   		Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not available under 'Add Site order' page","FAIL");
		   	}
		   }
	
	public void validateRemark_AddSiteOrder() throws InterruptedException, IOException {
		 boolean REmark=false;
		 try {
			REmark = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_remark);
		//	sa.assertTrue(REmark, " Remark field is not displayed");
			if(REmark) {
				 Report.LogInfo("Info", " 'Remak' field is displaying under 'Add Site order' page as expected","PASS");
			}else {
				 Report.LogInfo("Info", " 'Remak' field is not displaying under 'Add Site order' page","FAIL");
			}
		 }catch(Exception e) {
			 e.printStackTrace();
			 Report.LogInfo("Info", " 'Remak' field is not displaying under 'Add Site order' page","FAIL");
			 
		 }

	}
	
public void technologyDropdownFor1GigE() throws InterruptedException, IOException {
		
		String[] Technology = { "Actelis", "Atrica", "Overture","Alu", "Accedian-1G", "Cyan" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox;
		
		// Technology dropdown
				technology = findWebElement(Lanlink_InternationalObj.International.Addsiteorder_Technology)
						.isDisplayed();
			//	sa.assertTrue(technology, "Technology dropdown is not displayed");

				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				List<WebElement> listoftechnology = findWebElements(Lanlink_InternationalObj.International.ClassNameForDropdowns);
				
			if(listoftechnology.size()>=1) {	
				for (WebElement technologytypes : listoftechnology) {

							Reporter.log("list of technology are : " + technologytypes.getText());
							ExtentTestManager.getTest().log(LogStatus.PASS,"The list of technology  inside dropdown while  adding site order is: "+technologytypes.getText());
							Reporter.log("The list of technology  inside dropdown while  adding site order is: "+technologytypes.getText());
							String technologyValue=technologytypes.getText();
				}
				
		
				for(int k=0;k<Technology.length;k++) {	
					//Actelis	
							if(Technology[k].equalsIgnoreCase("Actelis")) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Actelis' is selected under Technology"
										+ "no additional fields displays");
								
								click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
								
								
								WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);
									
							}
							
						//Atrica	
							else if(Technology[k].equalsIgnoreCase("Atrica")) {
								
								ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Atrica' is selected under Technology"
										+ "list of fields should occur: "
										+ "Device name - Mandatory field"
										+ "Non Termination point checkbox"
										+ "Protected checkbox");
								
								click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
								WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);
								
								
						//Device Name	
							verifySiteOrderField_deviceName();
							
						//Non Termination Point	
							verifySiteOrderFields_NonterminationField();
							
						//Protected checkbox	
							verifySiteOrderFields_protected();
						}
						
						//Overture	
							else if(Technology[k].equalsIgnoreCase("Overture")) {
								ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Overture' is selected under Technology"
										+ "list of fields should occur: "
										+ "Non Termination point checkbox"
										+ "Protected checkbox");
								
								click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
								WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);
								
							//Non Termination Point	
								verifySiteOrderFields_NonterminationField();
								
							//Protected checkbox	
								verifySiteOrderFields_protected();
				
							}
					
						//Alu	
							else if(Technology[k].equalsIgnoreCase("Alu")) {
								
								ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Alu' is selected under Technology"
										+ "list of fields should occur: "
										+ "Device name - Mandatory field");
								
								click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
								WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);
								
							//Device Name	
								verifySiteOrderField_deviceName();
							}
							
							
					//Accedian
							else if((Technology[k].equalsIgnoreCase("Accedian-1G"))) {
								
								ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Accedian' is selected under Technology list of field should occur: "
										+ "Non Termination point checkbox"
										+ "Protected checkbox");
								
								addDropdownValues_commonMethod("Technology", Lanlink_InternationalObj.International.Addsiteorder_Technology, Technology[k]);
//								click_commonMethod(application, "Technology dropdown", "Addsiteorder_Technology", xml);
//								WebElement technologySelected = getwebelement(xml.getlocator("//locators/" + application + "/selectValueUnderTechnologyDropdown").replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

							//Non Termination Point	
								verifySiteOrderFields_NonterminationField();

								
							//Protected checkbox	
								verifySiteOrderFields_protected();			
									
							}	
						
					//Cyan		
							else if(Technology[k].equalsIgnoreCase("Cyan")) {
								
								ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Cyan' is selected under Technology"
										+ "list of fields should occur: "
										+ "Non Termination point checkbox");
								
								
								click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
								WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

							//Non Termination Point	
								verifySiteOrderFields_NonterminationField();
								
							}	
				}
			}else {
				
				Reporter.log("no values are available inside technology dropdown for Add site order");
				ExtentTestManager.getTest().log(LogStatus.FAIL,"no values are available inside technology dropdown for Add site order");
			}
		}
	
	public void verifySiteOrderFields_NonterminationField() throws InterruptedException, IOException {
		boolean Nonterminationpointcheckbox=false;
		try {		
			Nonterminationpointcheckbox=isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint);
		//	sa.assertTrue(Nonterminationpointcheckbox, "On selecting 'Overture' under Technology, Non termination point checkbox is not available");
			if(Nonterminationpointcheckbox) {
				 Report.LogInfo("Info", " 'Non Termination Point' checkbox is displayed under 'Add Site order' page as expected","PASS");
			}else {
				 Report.LogInfo("Info", " 'Non Termination Point' checkbox is not Available under 'Add Site order' page","FAIL");
			}
			
			boolean nonTerminaionpointselection=isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint);
		//	sa.assertFalse(nonTerminaionpointselection,"Non-termination point checbox under Add site is selected by default");
			if(nonTerminaionpointselection) {
				// Report.LogInfo("Info", " 'Non-Termination Point' checkbox is selected by default","FAIL");
			}else {
				// Report.LogInfo("Info", " 'Non-Termination Point' checkbox is not selected by default as expected","PASS");
			}
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			 Report.LogInfo("Info", " 'Non Termination Point' checkbox is not Available under 'Add Site order' page","FAIL");
			Reporter.log(" 'Non Termination Point' checkbox is not Available under 'Add Site order' page");
		}catch(Exception ee) {
			ee.printStackTrace();
			// Report.LogInfo("Info", " 'Non-Termination Point' checkbox is selected by default","FAIL");
		//	Reporter.log( " 'Non-Termination Point' checkbox is selected by default");
		}
	}
	
	public void clickonTechnology(WebElement el, String technology) throws InterruptedException {
		//Thread.sleep(3000);
		
		try {
		el.click();
		ExtentTestManager.getTest().log(LogStatus.INFO, "Under 'technology' dropdown, '"+ technology + "' is selected");
		Reporter.log("Under 'technology' dropdown, '"+ technology + "' is selected");
		}
		catch(Exception e)
		//Thread.sleep(3000);
		{
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, technology + " is not available under 'Technology' dropdown");
			Reporter.log( technology + " is not available under 'Technology' dropdown");
		}
	}
	
	public void verifySiteOrderField_deviceName() throws InterruptedException, IOException {
		boolean devicename = false;
		try {
			devicename = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Devicenamefield);
		//	sa.assertTrue(devicename, "On selecting Atrica under Technology, Device name is not available");
			if (devicename) {
				Report.LogInfo("Info", " 'Device Name' field is displaying under 'Add Site Order' page as expected",
						"PASS");
			} else {
				Report.LogInfo("Info", " 'Device Name' field is not displaying under 'Add Site Order' page", "FAIL");

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Device Name' field is not displaying under 'Add Site Order' page", "FAIL");
			Reporter.log(" 'Device Name' field is not displaying under 'Add Site Order' page");
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Device Name' field is not displaying under 'Add Site Order' page", "FAIL");
			Reporter.log(" 'Device Name' field is not displaying under 'Add Site Order' page");
		}
	}
	
	public void verifySiteOrderFields_protected() throws InterruptedException {
		boolean portectedcheckbox=false;
		try {	
			portectedcheckbox=findWebElement(Lanlink_InternationalObj.International.Addsiteorder_protected).isDisplayed();
		
		//	sa.assertTrue(portectedcheckbox, "On selecting Atrica under Technology, protected checkbox is not available");
			if(portectedcheckbox) {
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'Protected' checkbox is displayed under 'Add Site order' page as expected");
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Protected' checkbox is not available under 'Add Site order' page");
			}
		
			boolean protectedtselection=findWebElement(Lanlink_InternationalObj.International.Addsiteorder_protected).isSelected();
		//	sa.assertFalse(protectedtselection,"Protected checbox under Add site is selected by default");
			if(protectedtselection) {
			//	ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Protected' checkbox is selected by default");
			}else {
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'Protected' checkbox is not selected by default as expected");
			}
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Protected' checkbox is not Available under 'Add Site order' page");
			Reporter.log(" 'Protected' checkbox is not Available under 'Add Site order' page");
		}catch(Exception ee) {
			ee.printStackTrace();
			//ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Protected' checkbox is selected by default");
			//Reporter.log( " 'Protected' checkbox is selected by default");
		}
	}
	
	
	public void technologyDropdownFor10GigE() throws InterruptedException, IOException {
		
		String Technology = "Accedian";

		boolean technology, Nonterminationpointcheckbox, portectedcheckbox;
		
		// Technology dropdown
				technology = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Technology);
				//sa.assertTrue(technology, "Technology dropdown is not displayed");

				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				List<String> listoftechnology = new ArrayList<>(Arrays.asList((Lanlink_InternationalObj.International.ClassNameForDropdowns)));
				
			if(listoftechnology.size()>=1) {	
				for (String technologytypes : listoftechnology) {

							Reporter.log("list of technology are : " + technologytypes);
							Reporter.log("list of technology are : " + technologytypes);
							 Report.LogInfo("Info","The list of technology  inside dropdown while  adding site order is: "+technologytypes,"PASS");
				}
			}else {
				
				Reporter.log("no values are available inside technology dropdown for Add site order");
				 Report.LogInfo("Info","no values are available inside technology dropdown for Add site order","FAIL");
			}
			
			    verifyExists(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				webDriver.findElement(By.xpath("//div[text()='" + Technology + "']")).click();
				
				
			//Non Termination Point	
				verifySiteOrderFields_NonterminationField();

			
			}	
	
	public void OKbutton_AddSiteOrder() throws InterruptedException, IOException {

		 boolean ok=false;
		try { 
		ok = isElementPresent(Lanlink_InternationalObj.International.okbutton);
		//	sa.assertTrue(ok, "OK button is not displayed");
			if(ok) {
				Report.LogInfo("Info", " 'OK' button is displaying as expected","PASS");
			}else {
				Report.LogInfo("Info", " 'OK' button is not displaying","FAIL");
			}
			
		}catch(Exception e) {
			Report.LogInfo("Info", " 'OK' button is not displaying","FAIL");
			Reporter.log(" 'OK' button is not displaying");
		}
	}
	
	public void cancelbutton_AddSiteOrder() throws InterruptedException, IOException {
		 
		 boolean cancel=false;
		 try {
			 cancel = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_cancel);
		//	sa.assertTrue(cancel, "Cancel button is not "
					
			if(cancel) {
				Report.LogInfo("Info", " 'Cancel' button is displaying as expected","PASS");
			}else {
				Report.LogInfo("Info", " 'Cancel' button is not displaying","FAIL");
			}
		 }catch(Exception e) {
			 Report.LogInfo("Info", " 'Cancel' button is not displaying","FAIL");
			 Reporter.log(" 'Cancel' button is not displaying");
		}
	}
	
	public void verifySiteOrderForPoint_to_point(String interfaceSpeed,String modularMSP)throws InterruptedException, IOException 
	{

		try {


			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
			waitforPagetobeenable();

			
			//Technology Error message	
			verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");
			
		//CSR name Error message	
			verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg , "Physical Site: CSR Name");
			// Country Error message
			verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

			// City Error message
			verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

			// Validate Country dropdown
			Reporter.log("validate Country dropdown");
			validateCountry_AddSiteorder();

			// Validate City Fields
			Reporter.log("Validate city fields");
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			Reporter.log("validate Site Fields");
			validateSite_AddSiteOrder();

			//// scrolltoend();
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			Reporter.log("validate performance reporting checkbox");
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			Reporter.log("validate proactive monitoring checkbox");
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			Reporter.log("validate Smarts monitoring checkbox");
			validateSmartsMOnitoring_AddSiteOrder();

			//// scrolltoend();
			waitforPagetobeenable();

			// Validate Site Alias field
			Reporter.log("validate Site Alias fields");
			validateSiteAlias_AddSiteOrder();

			// Validate VLAN Id field
			Reporter.log("validate VLAn Id fields");
			validateVlanID_AddSiteOrder();

			// Validate DCA Enabled Site and Cloud Service Provider dropdown
			Reporter.log("validate DCA enabled site checkbox");
			valiadateDCAEnabledsite_AddSieOrder();

			// Verify Remark field
			Reporter.log("validate Remark fields");
			validateRemark_AddSiteOrder();

			if(modularMSP.equalsIgnoreCase("yes")) {
				
				technologyDropdown_p2p_mspselected();
				
			}else {
				
			if (interfaceSpeed.equals("1GigE")) {

				technologyDropdownFor1GigE();
			}

			else if (interfaceSpeed.equals("10GigE")) {

				technologyDropdownFor10GigE();
			}
		}
			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_InternationalObj.International.Addsiteorder_cancel,"Addsiteorder Cancel");
			waitforPagetobeenable();

			waitforPagetobeenable();

		} catch (AssertionError e) {
			e.printStackTrace();
		}
	}
	
	
	public void addsiteorder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
		
		String interfaceSpeed= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String country= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "country");
		String city= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "city");
		String CSR_Name= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSR_Name");
		String site= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitevalue");
		String performReport= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "performReport");
		String ProactiveMonitor= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Proactivemonitor");
		String smartmonitor= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "smartmonitor");
		String technology= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String siteallias= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteallias");
		String VLANid= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
		String DCAenabledsite= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DCAenabledsite");
		String cloudserviceprovider= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cloudserviceprovider");
		String sitevalue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existing_SiteOrdervalue");
		String remark= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteorder_Remark");
		String xngcityname= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xngcityname");
		String xngcitycode= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xngciycode");
		String devicename= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicenameForaddsiteorder");
		String nonterminatepoinr= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "nonterminationpoint");
		String Protected= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "protectforaddsiteorder");
		String newcityselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcity");
		String existingcityselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingcity");
		String existingsiteselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingsite");
		String newsiteselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newsite");

		String VPNtopology= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String circuitType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String sitePreferenceType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrder_sitePreferenceType");
		String offnetSelection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_Offnet");
		String siteOrderNumber= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siteordernumber");
		String circuitref= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrder_CircuitReference");
		String IVReference= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_Ivrefrence");
		String GCRolo= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrer_GCROloType");
		String Vlan= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_Vlan");
		String Vlanether= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_Vlanethertype");
		String primaryVlan= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrder_PrimaryVlan");
		String primaryVlanether= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_PrimaryVlanEtherType");
		String EPNoffnet= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNoffnet");
		String EPNEOSDH= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNEOSDH");
		String mappingmode= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_mappingMode");
		String portBased= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_portBased");
		String vlanBased= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");

		String modularMSP= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");

		
		 
		ExtentTestManager.getTest().log(LogStatus.INFO, "create Site Order");
		Reporter.log("create Site Order");

		
		scrollUp();
		Thread.sleep(5000);
		 
		if( (VPNtopology.equals("Point-to-Point")) && (circuitType.equals("Default"))) {
			
			addSiteOrderValues_point2point(interfaceSpeed, country,  city,  CSR_Name,  site,
					 performReport,  ProactiveMonitor,  smartmonitor,  technology,  siteallias,
					 VLANid,  DCAenabledsite,  cloudserviceprovider,  sitevalue,  remark,
					 xngcityname,  xngcitycode, devicename,  nonterminatepoinr,  Protected,  newcityselection,  existingcityselection,
					 existingsiteselection,  newsiteselection, modularMSP);
			
		}
		else if( (VPNtopology.equals("Point-to-Point")) && (circuitType.equals("Extended Circuit"))) {
			
			addSiteOrderValues_point2point_extendedCircuit(interfaceSpeed, country,  city,  CSR_Name,  site,
					 performReport,  ProactiveMonitor,  smartmonitor,  technology,  siteallias,
					 VLANid,  DCAenabledsite,  cloudserviceprovider,  sitevalue,  remark,
					 xngcityname,  xngcitycode, devicename,  nonterminatepoinr,  Protected,  newcityselection,  existingcityselection,
					 existingsiteselection,  newsiteselection, sitePreferenceType);
			
		}
		else if(VPNtopology.equals("Hub&Spoke")) {
			
			if(offnetSelection.equalsIgnoreCase("No")) {
			
			addSiteOrderValues_HubAndSPoke( interfaceSpeed, country, city, CSR_Name, newsiteselection, 
					performReport, ProactiveMonitor, smartmonitor, technology, siteallias, VLANid, DCAenabledsite,cloudserviceprovider, sitevalue, 
					remark, xngcityname, xngcitycode, devicename, nonterminatepoinr, Protected, newcityselection,existingcityselection, existingsiteselection, 
					newsiteselection, siteOrderNumber, circuitref, offnetSelection, IVReference,
		  			GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether);
			
			}
			
			if(offnetSelection.equalsIgnoreCase("Yes")) {
				
				addSiteOrderValues_HubAndSPoke_OffnetSelected( interfaceSpeed, country, city, CSR_Name, newsiteselection, 
						performReport, ProactiveMonitor, smartmonitor, technology, siteallias, VLANid, DCAenabledsite,cloudserviceprovider, sitevalue, 
						remark, xngcityname, xngcitycode, devicename, nonterminatepoinr, Protected, newcityselection,existingcityselection, existingsiteselection, 
						newsiteselection, siteOrderNumber, circuitref, offnetSelection, IVReference,
			  			GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether);
				}
			
			
		}else if(VPNtopology.equals("E-PN (Any-to-Any)")) {
			
			addSiteOrderValues_EPN(interfaceSpeed, country, city, CSR_Name, newsiteselection, 
					performReport, ProactiveMonitor, smartmonitor, technology, siteallias, VLANid, DCAenabledsite,cloudserviceprovider, sitevalue, 
					remark, xngcityname, xngcitycode, devicename, nonterminatepoinr, Protected, newcityselection,existingcityselection, existingsiteselection, 
					newsiteselection, siteOrderNumber, circuitref, offnetSelection, IVReference,
		  			GCRolo, Vlan, Vlanether, EPNoffnet, EPNEOSDH, mappingmode, portBased, vlanBased);
			
		}
		
	}
	
	public void addSiteOrderValues_point2point_extendedCircuit(String interfaceSpeed,
  			String country, String city, String CSR_Name, String site,
  			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
  			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
  			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
  			String existingsiteselection, String newsiteselection, String siteOrder_sitePreferenceType) throws InterruptedException, IOException {
     	
     	 scrollUp();
     	waitforPagetobeenable();
     	 
     	 Countyr_AddSiteOrder(country);
     	 
     	 City_AddSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode, sitevalue, CSR_Name, existingsiteselection, newsiteselection);

//     	 Site_AddSiteOrder(application, existingsiteselection, sitevalue, newsiteselection, CSR_Name);

     //	 scrolltoend();
     	waitforPagetobeenable();
     	 
     	 performancereporting_AddSiteOrder( performReport);

     	 proactiveMonitoring_AddSiteOrder( ProactiveMonitor);

     	 smartsMonitoring_AddSiteOrder( smartmonitor);

   //  scrolltoend();
     	waitforPagetobeenable();
     
     	 SiteAlias_AddSiteOrder( siteallias);
     	 
     	 VLANid_AddSiteOrder( VLANid);	
     	 
     	 addDropdownValues_commonMethod( "Site Preference Type" , Lanlink_InternationalObj.International.AddSiteOrder_sitePreferenceType_Dropodwn , siteOrder_sitePreferenceType);
     	 
     	 
     	 DCAEnabledSite_AddSiteOrder( DCAenabledsite, cloudserviceprovider);
     	 
     	 remark_AddSiteOrder (remark);
     	 
     	 technologyP2P_AddSiteOrder( technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);
     	 
     	waitforPagetobeenable();
     	// scrolltoend();
  		verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");

  		click(Lanlink_InternationalObj.International.okbutton, "OK");
     	 

      }
	
	public void Countyr_AddSiteOrder(String country)
			throws InterruptedException, IOException
	{

		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_Country,"country");
		addDropdownValues_commonMethod( "Device Country",Lanlink_InternationalObj.International.Addsiteorder_Country,country);
		
	}
	
	
	public void City_AddSiteorder( String existingcityselection, String city,
			String newcityselection, String xngcityname, String xngcitycode, String sitevalue, String CSR_Name,
			String existingsiteselection, String newsiteselection)
			throws InterruptedException, IOException {
		
		//Existing City
				if((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

//					Clickon(getwebelement(xml.getlocator("//locators/" + application + "/Addsiteorder_disabledCitytogglebutton")));
//					 Thread.sleep(5000);
					 waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.Addsiteorder_City);
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[text()='" + city + "']")).click();
					waitforPagetobeenable();
					
					ExtentTestManager.getTest().log(LogStatus.PASS, city+ " is selected under Device Xng City dropdown");
					Reporter.log(city+ " is selected under Device Xng City dropdown");

					
				//Existing Site	
					if((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
		    			
						click(Lanlink_InternationalObj.International.Addsiteorder_Sitetogglebutton);
						waitforPagetobeenable();
						
						click(Lanlink_InternationalObj.International.Addsiteorder_sitedropdown);
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//span[text()='" + sitevalue + "']")).click();
						waitforPagetobeenable();
						ExtentTestManager.getTest().log(LogStatus.PASS, sitevalue+  " is selected under Physical Site dropdown");
						Reporter.log(sitevalue+" is selected under Physical Site dropdown");

		    		}
					
				//New site
					if((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
						
//						Clickon(getwebelement(xml.getlocator("//locators/" + application + "/AddsiteOrdr_disabledSitetogglebutton")));
//						Thread.sleep(3000);
						
						if(CSR_Name.equalsIgnoreCase("null")){
							ExtentTestManager.getTest().log(LogStatus.FAIL, "CSR name field is mandatory and no values are provided");
							Reporter.log("No values provided for mandatory field 'CSR Name'");
							
						}else {
							
						sendKeys(Lanlink_InternationalObj.International.Addsiteorder_CSRname, CSR_Name,"CSR Name");
						waitforPagetobeenable();

						ExtentTestManager.getTest().log(LogStatus.PASS, CSR_Name+ " is entered under CSR Name field");
						Reporter.log(CSR_Name+ " is entered under CSR Name field");

						}
					}
				
					
					
				}
				else if((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {
					
					click(Lanlink_InternationalObj.International.Addsiteorder_Citytogglebutton);
					waitforPagetobeenable();
					
					//City name 
					 if(xngcityname.equalsIgnoreCase("null")) {
						 ExtentTestManager.getTest().log(LogStatus.FAIL, "City name field is a mandatory field and the value is not provided");
						 Reporter.log("City name field is a mandatory field and the value is not provided");
					 }else {
					 sendKeys(Lanlink_InternationalObj.International.Addsiteorder_xngcityname, xngcityname,"xngcityname");
					 waitforPagetobeenable();
					 ExtentTestManager.getTest().log(LogStatus.PASS, xngcityname+ " is entered in City name field");
					 Reporter.log(xngcityname+ " is entered in City name field");
					 waitforPagetobeenable();
					 }
					 
					 //City code
					 if(xngcitycode.equalsIgnoreCase("null")) {
						 ExtentTestManager.getTest().log(LogStatus.FAIL, "City Code field is a mandatory field and the value is not provided");
						 Reporter.log("no values provided for city code text field");
					 }else {
					 sendKeys(Lanlink_InternationalObj.International.Addsiteorder_XNGcitycode, xngcitycode,"xngcitycode");
					 waitforPagetobeenable();
					 ExtentTestManager.getTest().log(LogStatus.PASS, xngcitycode+" is entered in City Code field" );
					 Reporter.log(xngcitycode+" is entered in City Code field");

					 }
					 waitforPagetobeenable();
					 
					
			//add new Site 		 
					try {
						if(CSR_Name.equalsIgnoreCase("null")){
							ExtentTestManager.getTest().log(LogStatus.FAIL, "CSR name field is mandatory and no values are provided");
							 Reporter.log(" no values provided for 'CSR Name' text field");
							
						}else {
							
						sendKeys(Lanlink_InternationalObj.International.Addsiteorder_CSRname, CSR_Name,"CSR_Name");
						 waitforPagetobeenable();
						ExtentTestManager.getTest().log(LogStatus.PASS, CSR_Name+ " is entered under CSR Name field");
						Reporter.log(CSR_Name+ " is entered under CSR Name field");
						}
						
					}catch(Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " CSR NAme not performed");
					}
				
				}

   }
	
	public void performancereporting_AddSiteOrder(String performReport)throws InterruptedException, IOException {
	  	 
	   	//Perfomance Reporting	
			if(performReport.equalsIgnoreCase("Null")) {
				
				Reporter.log("Performance reporting value is not provided. 'Follow Service' is selected by default PASS");
			}else {
			verifyExists(Lanlink_InternationalObj.International.Addsiteorder_performancereporting_xbutton,"Add siteorder Performancereporting xbutton");
			click(Lanlink_InternationalObj.International.Addsiteorder_performancereporting_xbutton,"Add siteorder Performancereporting xbutton");
			
			waitforPagetobeenable();
			//Clickon(getwebelement("//div[label[text()='Performance Reporting']]//div[text()='"+ performReport +"']"));
			
			webDriver.findElement(By.xpath("//div[label[text()='Performance Reporting']]//div[text()='"+ performReport +"']")).click();
			waitforPagetobeenable();
			
			//String actualvalue=webDriver.findElement(By.xpath("(//div[label[text()='Performance Reporting']]//span)[2]")).getText();
			
		//	Reporter.log(actualvalue + " is selected under Performance reporting dropdown PASS");
		
			}
	    }
	
	public void proactiveMonitoring_AddSiteOrder(String ProactiveMonitor)throws InterruptedException, IOException{
	  	 
	   	//Pro active monitoring	
	 		if(ProactiveMonitor.equalsIgnoreCase("Null")) {
	 			
	 			Reporter.log("Pro active monitoring value is not provided. 'Follow Service' is selected by default PASS");
	 		}else {	
	 		
	 		verifyExists(Lanlink_InternationalObj.International.Addsitorder_proactivemonitoring_xbutton,"Add sitorder proactive monitoring xbutton");
			click(Lanlink_InternationalObj.International.Addsitorder_proactivemonitoring_xbutton,"Add sitorder_proactive monitoring xbutton");
	 		waitforPagetobeenable();
	 		webDriver.findElement(By.xpath("//div[label[text()='Proactive Monitoring']]//div[text()='"+ ProactiveMonitor +"']")).click(); 		//Clickon(getwebelement("//div[label[text()='Proactive Monitoring']]//div[text()='"+ ProactiveMonitor +"']"));
	 		waitforPagetobeenable();
	 		
	 		//String actualvalue=getTextFrom(Lanlink_InternationalObj.International.ProactiveMonitor);
	 		
	 		//Reporter.log(actualvalue+  "is selected under proactive monitoring dropdown PASS");
	 		
	 		} 
	    }
	
	public void smartsMonitoring_AddSiteOrder(String smartmonitor)throws InterruptedException, IOException {
	  	 
		if(smartmonitor.equalsIgnoreCase("null")) {
			
			Reporter.log("INFO Smart monitoring value is not provided Follow Service' is selected by default PASS");
		}else {
			
		
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoring_xbutton,"Add siteorder smart monitoring xbutton");
		click(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoring_xbutton,"Add siteorder smart monitoring xbutton");
		waitforPagetobeenable();
		
		waitforPagetobeenable();
		webDriver.findElement(By.xpath("//div[label[text()='Smarts Monitoring']]//div[text()='"+ smartmonitor +"']")).click();
		waitforPagetobeenable();
			
		//	String actualvalue=getTextFrom(Lanlink_InternationalObj.International.smartmonitor);
		
		//	Reporter.log(actualvalue+ " is selected under Smart monitoring dropdown PASS");

		}

	}
	
	public void SiteAlias_AddSiteOrder(String siteallias)throws InterruptedException, IOException {
	 	 
		if(siteallias.equalsIgnoreCase("null")) {
			
			Reporter.log("No values entered for 'Site Alias' field");
		}else {
			try {
				sendKeys(Lanlink_InternationalObj.International.Addsiteorder_sitealias,
						siteallias);
				waitforPagetobeenable();

				String actualvalue =  getAttributeFrom(Lanlink_InternationalObj.International.Addsiteorder_sitealias,"value");
				Reporter.log(actualvalue + " is entered under 'Site Alias' field");
			}  catch (Exception err) {
				err.printStackTrace();
				Reporter.log(" Not able to enter value under 'Site Alias' field");
			}
		}

	}
	
	public void VLANid_AddSiteOrder( String VLANid)
			throws InterruptedException, IOException {

		if (VLANid.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "No values entered for 'Vlan id' field");
			Reporter.log("No values entered for 'Vlan id' field");
		} else {
			try {
				sendKeys(Lanlink_InternationalObj.International.Addsiteorder_Vlanid, VLANid,"VLANid");
				waitforPagetobeenable();

				String actualvalue = findWebElement(Lanlink_InternationalObj.International.Addsiteorder_Vlanid)
						.getAttribute("value");
				ExtentTestManager.getTest().log(LogStatus.PASS, actualvalue + " is entered under Vlan id field");
				Reporter.log(actualvalue + " is entered under Vlan id field");

			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" 'Vlan Id' field is not displating under 'Add Site order' page");
				Reporter.log(" 'Vlan Id' field is not displating under 'Add Site order' page");
			} catch (Exception err) {
				err.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value under 'Vlan Id' field");
				Reporter.log(" Not able to enter value under 'Vlan Id' field");
			}
		}

	}
	
	public void DCAEnabledSite_AddSiteOrder( String DCAenabledsite, String cloudserviceprovider)
			throws InterruptedException, IOException {

		// DCA Enabled Site
		if (DCAenabledsite.equalsIgnoreCase("yes")) {

			click(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox);
			ExtentTestManager.getTest().log(LogStatus.PASS, "DCA enabled checkbox is selected");
			Reporter.log("DCA enabled checkbox is selected");
			
			// Cloud Service provider
			if (cloudserviceprovider.equalsIgnoreCase("null")) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"DCA cloud service provider dropdown is mandatory. No values are provided");
				Reporter.log("DCA cloud service provider dropdown is mandatory. No values are provided");
			} else {
				click(Lanlink_InternationalObj.International.Addsiteorder_cloudserviceProvider);
				waitforPagetobeenable();
				webDriver.findElement(By.xpath("//div[text()='" + cloudserviceprovider + "']")).click();;
				waitforPagetobeenable();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						cloudserviceprovider + " is selected under 'cloud service provider' dropdown");
				Reporter.log(cloudserviceprovider + " is selected under 'cloud service provider' dropdown");
			}

		} else {
			Reporter.log("DCA site is not selected");
			ExtentTestManager.getTest().log(LogStatus.PASS, "DCA enabled checkbox is not selected");
		}

	}
	
	public void remark_AddSiteOrder(String remark)
			throws InterruptedException, IOException {

		if (remark.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "No values entered under remark ");
			Reporter.log("No values entered under remark ");
		} else {
			try {
				sendKeys(Lanlink_InternationalObj.International.Addsiteorder_remark, remark,"remark");
				waitforPagetobeenable();

				String actualvalue = findWebElement(Lanlink_InternationalObj.International.Addsiteorder_remark)
						.getAttribute("value");
				ExtentTestManager.getTest().log(LogStatus.PASS, actualvalue + " is entered under 'remark' field");
				
				Reporter.log(actualvalue + " is entered under 'remark' field");
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" 'Remark' field is not displating under 'Add Site order' page");
				Reporter.log(" 'Remark' field is not displating under 'Add Site order' page");
			} catch (Exception err) {
				err.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value under 'Remark' field");
				Reporter.log(" Not able to enter value under 'Remark' field");
			}
		}

	}
	
	public void technologyP2P_AddSiteOrder(String technology, String interfaceSpeed,
			String devicename, String nonterminatepoinr, String Protected)
			throws InterruptedException, IOException {

		// Technology
		if (technology.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Technology dropdown is a mandatory field and no values are provided");
			Reporter.log("Technology dropdown is a mandatory field and no values are provided");
		} else {

			if (interfaceSpeed.equals("1GigE")) {

				if (technology.equals("Actelis") || technology.equals("Atrica") || technology.equals("Overture")
						|| technology.equals("Accedian-1G") || technology.equals("Cyan") || technology.equals("Alu")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_Technology,  "Technology dropdown");
					waitforPagetobeenable();
					WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", technology)); 
					clickonTechnology(technologySelected, technology);
					waitforPagetobeenable();
					ExtentTestManager.getTest().log(LogStatus.PASS, technology + " is selected under technology dropdown");
					Reporter.log(technology + " is selected under technology dropdown");

					if (technology.equals("Actelis")) {

						Reporter.log("No additional fields displays");
					}

					else if (technology.equals("Atrica")) {

						// Device name
						devicename_AddSiteOrder( devicename);

						// Non- termination point
						nontermination_AddSiteorder( nonterminatepoinr);

					}

					else if (technology.equals("Overture") || technology.equals("Accedian-1G")) {

						// Non- termination point
						nontermination_AddSiteorder( nonterminatepoinr);

					}

					else if (technology.equals("Cyan")) {

						// Non- termination point
						nontermination_AddSiteorder( nonterminatepoinr);

					}

					else if (technology.equals("Alu")) {

						// Device name
						devicename_AddSiteOrder( devicename);

					}
				}
			}

			if (interfaceSpeed.equals("10GigE")) {

				if (technology.equals("Accedian")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
					waitforPagetobeenable();
					WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", technology)); 
					clickonTechnology(technologySelected, technology);
					waitforPagetobeenable();
					ExtentTestManager.getTest().log(LogStatus.PASS, technology + " is selected under technology dropdown");
					Reporter.log(technology + " is selected under technology dropdown");
					// Non- termination point
					nontermination_AddSiteorder( nonterminatepoinr);

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							technology + "is not available under 'Technology' dropdown for '10GigE' interface speed");
					Reporter.log(technology + "is not available under 'Technology' dropdown for '10GigE' interface speed");
				}

			}
		}

		ExtentTestManager.getTest().log(LogStatus.PASS, "Data has been entered for add site order");
		Reporter.log("Data has been entered for add site order");

	}
	
	public void devicename_AddSiteOrder(String devicename)
			throws InterruptedException, IOException {

		// Device name
		if (devicename.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"device name field is mandatory. No values entered under 'device name' field");
			Reporter.log("device name field is mandatory. No values entered under 'device name' field");
		} else {
			try {
				sendKeys(Lanlink_InternationalObj.International.Addsiteorder_Devicenamefield,devicename,"devicename");
				waitforPagetobeenable();

				String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.Addsiteorder_Devicenamefield,"value");

				ExtentTestManager.getTest().log(LogStatus.PASS, actualvalue + " is entered under 'device name' field");
				Reporter.log(actualvalue + " is entered under 'device name' field");
				
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" 'Device name' field is not displaying under 'Add Site Order' page");
				Reporter.log(" 'Device name' field is not displaying under 'Add Site Order' page");
			} catch (Exception err) {
				err.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value in 'Device name' field");
				Reporter.log(" Not able to enter value in 'Device name' field");
			}
		}

	}
	
	
	public void nontermination_AddSiteorder(String nonterminatepoinr)
			throws InterruptedException {

		// Non- termination point
		if (nonterminatepoinr.equalsIgnoreCase("yes")) {
			try {
				click(Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint);
				waitforPagetobeenable();

				boolean nonTerminationSelection = findWebElement(
						Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint).isSelected();
				if (nonTerminationSelection) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Non-Termination point' checkbox is selected as expected");
					Reporter.log(" 'Non-Termination point' checkbox is selected as expected");
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Non-Termination point' checkbox is not selected");
					Reporter.log(" 'Non-Termination point' checkbox is not selected");
				}

				ExtentTestManager.getTest().log(LogStatus.PASS, "Non-termination point checkbox is selected");
				Reporter.log("Non-termination point checkbox is selected");
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" Non-Termination point' checkbox is not dipslaying under 'Add Site order' page");
				Reporter.log(" Non-Termination point' checkbox is not dipslaying under 'Add Site order' page");
			} catch (Exception err) {
				err.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to click on 'non-Termination point' checkbox");
				Reporter.log(" Not able to click on 'non-Termination point' checkbox");
			}
		} else {
			Reporter.log("Non termination point checkbox is not selected as expected");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Non-termination point chekbox is not selected");
		}

	}
	
	
	 
	 
	 
	 public void addSiteOrderValues_HubAndSPoke(String interfaceSpeed,
	  			String country, String city, String CSR_Name, String site,
	  			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
	  			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
	  			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
	  			String existingsiteselection, String newsiteselection,
	  			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
	  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether) throws InterruptedException, IOException { 
		 
		 scrollUp();
	     	waitforPagetobeenable();
	     	 
	     	 Countyr_AddSiteOrder(country);
	     	 
	     	 City_AddSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode, sitevalue, CSR_Name, existingsiteselection, newsiteselection);

//	     	 Site_AddSiteOrder(application, existingsiteselection, sitevalue, newsiteselection, CSR_Name);

	     //	 scrolltoend();
	     	waitforPagetobeenable();
	     	 
	     	 performancereporting_AddSiteOrder( performReport);

	     	 proactiveMonitoring_AddSiteOrder( ProactiveMonitor);

	     	 smartsMonitoring_AddSiteOrder( smartmonitor);

	   //  scrolltoend();
	     	waitforPagetobeenable();
	     
	     	 SiteAlias_AddSiteOrder( siteallias);
	     	 
	     	 VLANid_AddSiteOrder( VLANid);	
	     	 
	     //	 addDropdownValues_commonMethod( "Site Preference Type" , Lanlink_InternationalObj.International.AddSiteOrder_sitePreferenceType_Dropodwn , siteOrder_sitePreferenceType);
	     	 
	     	 
	     	 DCAEnabledSite_AddSiteOrder( DCAenabledsite, cloudserviceprovider);
	     	 
	     	 remark_AddSiteOrder (remark);
	     	 
	     	 technologyP2P_AddSiteOrder( technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);
	     	 
	     	waitforPagetobeenable();
	     	// scrolltoend();
	  		verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");

	  		click(Lanlink_InternationalObj.International.okbutton, "OK");
		 
		 
	 }
	 
	 public void addSiteOrderValues_HubAndSPoke_OffnetSelected(String interfaceSpeed,
	   			String country, String city, String CSR_Name, String site,
	   			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
	   			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
	   			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
	   			String existingsiteselection, String newsiteselection,
	   			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
	   			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether) throws InterruptedException, IOException {
		 scrollUp();
	     	waitforPagetobeenable();
	     	 
	     	 Countyr_AddSiteOrder(country);
	     	 
	     	 City_AddSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode, sitevalue, CSR_Name, existingsiteselection, newsiteselection);

//	     	 Site_AddSiteOrder(application, existingsiteselection, sitevalue, newsiteselection, CSR_Name);

	     //	 scrolltoend();
	     	waitforPagetobeenable();
	     	 
	     	 performancereporting_AddSiteOrder( performReport);

	     	 proactiveMonitoring_AddSiteOrder( ProactiveMonitor);

	     	 smartsMonitoring_AddSiteOrder( smartmonitor);

	   //  scrolltoend();
	     	waitforPagetobeenable();
	     
	     	 SiteAlias_AddSiteOrder( siteallias);
	     	 
	     	 VLANid_AddSiteOrder( VLANid);	
	     	 
	     //	 addDropdownValues_commonMethod( "Site Preference Type" , Lanlink_InternationalObj.International.AddSiteOrder_sitePreferenceType_Dropodwn , siteOrder_sitePreferenceType);
	     	 
	     	 
	     	 DCAEnabledSite_AddSiteOrder( DCAenabledsite, cloudserviceprovider);
	     	 
	     	 remark_AddSiteOrder (remark);
	     	 
	     	 technologyP2P_AddSiteOrder( technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);
	     	 
	     	waitforPagetobeenable();
	     	// scrolltoend();
	  		verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");

	  		click(Lanlink_InternationalObj.International.okbutton, "OK");
	 }
	 
	 public void addSiteOrderValues_EPN(String interfaceSpeed,
	   			String country, String city, String CSR_Name, String site,
	   			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
	   			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
	   			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
	   			String existingsiteselection, String newsiteselection,String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
	   			String GCRolo, String Vlan, String Vlanether, String EPNoffnetSelection, String EPNEOSDHSelection,
	   			String mappingmode, String portBased, String vlanBased) throws InterruptedException, IOException {
		 scrollUp();
	     	waitforPagetobeenable();
	     	 
	     	 Countyr_AddSiteOrder(country);
	     	 
	     	 City_AddSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode, sitevalue, CSR_Name, existingsiteselection, newsiteselection);

//	     	 Site_AddSiteOrder(application, existingsiteselection, sitevalue, newsiteselection, CSR_Name);

	     //	 scrolltoend();
	     	waitforPagetobeenable();
	     	 
	     	 performancereporting_AddSiteOrder( performReport);

	     	 proactiveMonitoring_AddSiteOrder( ProactiveMonitor);

	     	 smartsMonitoring_AddSiteOrder( smartmonitor);

	   //  scrolltoend();
	     	waitforPagetobeenable();
	     
	     	 SiteAlias_AddSiteOrder( siteallias);
	     	 
	     	 VLANid_AddSiteOrder( VLANid);	
	     	 
	     //	 addDropdownValues_commonMethod( "Site Preference Type" , Lanlink_InternationalObj.International.AddSiteOrder_sitePreferenceType_Dropodwn , siteOrder_sitePreferenceType);
	     	 
	     	 
	     	 DCAEnabledSite_AddSiteOrder( DCAenabledsite, cloudserviceprovider);
	     	 
	     	 remark_AddSiteOrder (remark);
	     	 
	     	 technologyP2P_AddSiteOrder( technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);
	     	 
	     	waitforPagetobeenable();
	     	// scrolltoend();
	  		verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");

	  		click(Lanlink_InternationalObj.International.okbutton, "OK");
		 
	 }
	      	
	 
	 public void VerifyDataEnteredForSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException {
			
			String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
			String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
			String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
			String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "country");
			String city = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "city");
			String CSR_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSR_Name");
			String site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitevalue");
			String performReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "performReport");
			String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Proactivemonitor");
			String smartmonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "smartmonitor");
			String technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
			String siteallias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteallias");
			String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
			String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DCAenabledsite");
			String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"cloudserviceprovider");
			String sitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"existing_SiteOrdervalue");
			String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteorder_Remark");
			String xngcityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng city name");
			String xngcitycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng ciy code");
			String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"devicenameForaddsiteorder");
			String nonterminatepoinr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"nonterminationpoint");
			String Protected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"protectforaddsiteorder");
			String newcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcity");
			String existingcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"existingcity");
			String existingsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"existingsite");
			String newsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newsite");
			String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Siteordernumber");
			String circuitref = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"siteOrder_CircuitReference");
			String offnetSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SiteOrder_Offnet");
			String IVReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SiteOrder_Ivrefrence");
			String GCRolo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrer_GCROloType");
			String Vlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
			String Vlanether = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SiteOrder_Vlanethertype");
			String primaryVlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"siteOrder_PrimaryVlan");
			String primaryVlanether = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SiteOrder_PrimaryVlanEtherType");
			String EPNoffnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNoffnet");
			String EPNEOSDH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNEOSDH");
			String mappingmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SiteOrder_mappingMode");
			String portBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_portBased");
			String vlanBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
			String modularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");

			if (VPNtopology.equals("Point-to-Point")) {

				
					VerifyDataEnteredForSiteOrder_viewSiteOrder_P2P(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
	    	    			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
	    	    			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection);

			}

				else if (IVReference.equals("Primary")) {

				if (VPNtopology.equals("Hub&Spoke")) {

						VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Primay(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
	    	        			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
	    	        			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
	    	        			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);

					}

				} else if (VPNtopology.equals("E-PN (Any-to-Any)")) {

						VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Primay(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
	                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
	                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
	                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);
					
				}

				else if (IVReference.equals("Access")) {

				if (VPNtopology.equals("Hub&Spoke")) {

					
						VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Access(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
	                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
	                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
	                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);
					}

				} else if (VPNtopology.equals("E-PN (Any-to-Any)")) {

		
						VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Access(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
	                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
	                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
	                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);
					
				     }
				}
	 
	 public String splitPhysicalSitealue(String siteValue)
				throws IOException, InterruptedException {
			
			String value = null;
			int len = siteValue.length();

			if (siteValue.startsWith("(")) {
				value = siteValue.substring(1, len - 1);
			} else if (siteValue.startsWith(" (")) {
				value = siteValue.substring(2, len - 1);
			} else {
				value = siteValue;
			}
			return value;
		}
	 
	 
	 public void VerifyDataEnteredForSiteOrder_viewSiteOrder_P2P(String country, String city, String CSR_Name, String site,
				String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
				String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
				String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
				String existingsiteselection, String newsiteselection)
				throws InterruptedException, IOException {

			
			 //Country	
	    	verifyEnteredvalues("Device Country", country);
	    	
	    //City
	    	
	    	if((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {
	    		
	    	//City	
	    		verifyEnteredvalues("Device Xng City", city);
	    		
	    	//Site
	    		if((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
	    			String siteValue=splitPhysicalSitealue(site);
	    			//verifyEnteredvalues("Physical Site: CSR Name", siteValue);
	    		}
	    		
	    		else if((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
	    			//verifyEnteredvalues("Physical Site: CSR Name", CSR_Name);
	    		}
	    		
	    	}
	    	else if((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {
	    		
	    		//New City
	    		verifyEnteredvalues("Device Xng City", xngcitycode);
	    		
	    		//New Site
	    		verifyEnteredvalues("Physical Site: CSR Name", CSR_Name);
	    		
	    	}
	    	
	    	
	    //Performance Reporting
	    	verifyEnteredvalues("Performance Reporting", performReport);
	    	
	    //Proactive Monitoring
	    	verifyEnteredvalues("Proactive Monitoring", ProactiveMonitor);
	    	
	    //Smarts Monitoring
	    	verifyEnteredvalues("Smarts Monitoring", smartmonitor);
	    	
	    //Technology
	    	verifyEnteredvalues("Technology", technology);
	    	
	    	if(technology.equals("Belgacom VDSL")) {
	    		
	    	Reporter.log("Site alias and Vlan Id text field will not display for 'BElgacom' technology");
	    		
	    	}else {
	    		//Site Alias
	        	verifyEnteredvalues("Site Alias", siteallias);
	        	
	        //VLAN Id
//	        	verifyEnteredvalues("VLAN Id", VLANid);
	    	}
	    	
	    
	    	
	    //DCA Enabled Site
	    	verifyEnteredvalues("DCA Enabled Site", DCAenabledsite);
	    	
	    	if(DCAenabledsite.equalsIgnoreCase("Yes")) {
	    		
	    		//Cloud Service Provider
	    		     verifyEnteredvalues("Cloud Service Provider", cloudserviceprovider);
	    		
	    	}
	    	
	    //Remark
	    	compareText("Remark", Lanlink_InternationalObj.International.remark_viewPage, remark);
	    	
	    	
	    	if(technology.equals("Atrica")) {
	    		//Non_termination Point checkbox
	    			verifyEnteredvalues("Non Termination Point", nonterminatepoinr);
	    		
	    		//Device Name text field
	    			verifyEnteredvalues("Device Name", devicename);
	    			//Protected checkbox
	    		//	verifyEnteredvalues("Protected", Protected);
	    		
	    	}
	    	
	    	
	    	if(technology.equals("Overture")) {
	    		//Non_termination Point checkbox
	    			verifyEnteredvalues("Non Termination Point", nonterminatepoinr);
	    			
	    			//Protected checkbox
	    		//	verifyEnteredvalues("Protected", Protected);
	    		
	    	}
	    	
	    	if(technology.equals("Alu")) {
	    		//Device Name text field
				verifyEnteredvalues("Device Name", devicename);
	    	}
	    	
	    	if(technology.equals("Accedian-1G")) {
	    		//Non_termination Point checkbox
				verifyEnteredvalues("Non Termination Point", nonterminatepoinr);
				//Protected checkbox
    			verifyEnteredvalues("Protected", Protected);

	    	}
	    	
	    	if(technology.equals("Belgacom VDSL")) {
	    		Reporter.log("No additonal fields displays");
	    	}
	    	}
	 
	 
	 public void VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Primay(String country, String city, String CSR_Name, String site,
				String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
				String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
				String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
				String existingsiteselection, String newsiteselection,
				String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
	  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {

		
			// Site order Number
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteOrderNumber + Lanlink_InternationalObj.International.viewPage2);

			// Country
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + country + Lanlink_InternationalObj.International.viewPage2);
			// City

			if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

				// City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + city + Lanlink_InternationalObj.International.viewPage2);

				// Site
				if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
					String siteV = splitPhysicalSitealue(site);
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteV + Lanlink_InternationalObj.International.viewPage2);
				}

				else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);
				}

			} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

				// New City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + xngcitycode + Lanlink_InternationalObj.International.viewPage2);

				// New Site
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);

			}

			// Performance Reporting
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + performReport + Lanlink_InternationalObj.International.viewPage2);

			// Proactive Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + ProactiveMonitor + Lanlink_InternationalObj.International.viewPage2);
			waitforPagetobeenable();

			// Smarts Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + smartmonitor + Lanlink_InternationalObj.International.viewPage2);

			// Technology
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + technology + Lanlink_InternationalObj.International.viewPage2);
			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			} else {
				// Site Alias
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteallias + Lanlink_InternationalObj.International.viewPage2);
			}
			// IV Reference
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + IVReference + Lanlink_InternationalObj.International.viewPage2);

			// Remark

			(getTextFrom(Lanlink_InternationalObj.International.viewPage1 + remark + Lanlink_InternationalObj.International.viewPage2))
					.equals(remark);

			if (technology.equals("Atrica")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Overture")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Alu")) {
				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Accedian-1G")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				// verifyExists(Lanlink_InternationalObj.International.viewPage1+devicename+Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			}
		}
	 
	 
	 public void VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Primay(String country, String city, String CSR_Name, String site,
				String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
				String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
				String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
				String existingsiteselection, String newsiteselection,
				String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
	  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {

			// Site order Number
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteOrderNumber + Lanlink_InternationalObj.International.viewPage2);

			// Country
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + country + Lanlink_InternationalObj.International.viewPage2);
			// City

			if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

				// City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + city + Lanlink_InternationalObj.International.viewPage2);

				// Site
				if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
					String siteV = splitPhysicalSitealue(site);
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteV + Lanlink_InternationalObj.International.viewPage2);
				}

				else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);
				}

			} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

				// New City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + xngcitycode + Lanlink_InternationalObj.International.viewPage2);

				// New Site
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);

			}

			// Performance Reporting
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + performReport + Lanlink_InternationalObj.International.viewPage2);

			// Proactive Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + ProactiveMonitor + Lanlink_InternationalObj.International.viewPage2);
			waitforPagetobeenable();

			// Smarts Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + smartmonitor + Lanlink_InternationalObj.International.viewPage2);

			// Technology
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + technology + Lanlink_InternationalObj.International.viewPage2);
			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			} else {
				// Site Alias
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteallias + Lanlink_InternationalObj.International.viewPage2);
			}
			// IV Reference
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + IVReference + Lanlink_InternationalObj.International.viewPage2);

			// Remark

			(getTextFrom(Lanlink_InternationalObj.International.viewPage1 + remark + Lanlink_InternationalObj.International.viewPage2))
					.equals(remark);

			if (technology.equals("Atrica")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Overture")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

				// EPN Offnet
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + EPNoffnet + Lanlink_InternationalObj.International.viewPage2);

				// EPN EOSDH
				if (EPNEOSDH.equalsIgnoreCase("Yes")) {
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + EPNEOSDH + Lanlink_InternationalObj.International.viewPage2);

				} else {
					Reporter.log("EPN EOSDH will not display, if it is not selected");
				}
			}

			if (technology.equals("Alu")) {
				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Accedian-1G")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				// verifyExists(Lanlink_InternationalObj.International.viewPage1+devicename+Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			}
		}
	 
	 public void VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Access(String country, String city, String CSR_Name, String site,
				String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
				String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
				String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
				String existingsiteselection, String newsiteselection,
				String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
	  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH)
				throws InterruptedException, IOException {

		
			// Site order Number
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteOrderNumber + Lanlink_InternationalObj.International.viewPage2);

			// Country
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + country + Lanlink_InternationalObj.International.viewPage2);
			// City

			if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

				// City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + city + Lanlink_InternationalObj.International.viewPage2);

				// Site
				if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
					String siteV = splitPhysicalSitealue(site);
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteV + Lanlink_InternationalObj.International.viewPage2);
				}

				else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);
				}

			} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

				// New City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + xngcitycode + Lanlink_InternationalObj.International.viewPage2);

				// New Site
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);

			}

			// Performance Reporting
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + performReport + Lanlink_InternationalObj.International.viewPage2);

			// Proactive Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + ProactiveMonitor + Lanlink_InternationalObj.International.viewPage2);
			waitforPagetobeenable();

			// Smarts Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + smartmonitor + Lanlink_InternationalObj.International.viewPage2);

			// Technology
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + technology + Lanlink_InternationalObj.International.viewPage2);
			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			} else {
				// Site Alias
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteallias + Lanlink_InternationalObj.International.viewPage2);
			}
			// IV Reference
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + IVReference + Lanlink_InternationalObj.International.viewPage2);

			// Remark

			(getTextFrom(Lanlink_InternationalObj.International.viewPage1 + remark + Lanlink_InternationalObj.International.viewPage2))
					.equals(remark);

			if (technology.equals("Atrica")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);
			}

			
			if (technology.equals("Overture")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// GCR OLO Type dropdown
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + GCRolo + Lanlink_InternationalObj.International.viewPage2);

				// VLAN tetx field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vlan + Lanlink_InternationalObj.International.viewPage2);

				// VLAN Ether type dropdown
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vlanether + Lanlink_InternationalObj.International.viewPage2);

				// Primary VLAN Text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + primaryVlan + Lanlink_InternationalObj.International.viewPage2);

				// Primary VLAN Ether Type
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + primaryVlanether + Lanlink_InternationalObj.International.viewPage2);
			}

			if (technology.equals("Alu")) {
				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Accedian-1G")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				// verifyExists(Lanlink_InternationalObj.International.viewPage1+devicename+Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			}
		}
		
	 public void VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Access(String country, String city, String CSR_Name, String site,
				String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
				String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
				String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
				String existingsiteselection, String newsiteselection,
				String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
	  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {

			
			// Site order Number
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteOrderNumber + Lanlink_InternationalObj.International.viewPage2);

			// Country
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + country + Lanlink_InternationalObj.International.viewPage2);
			// City

			if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

				// City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + city + Lanlink_InternationalObj.International.viewPage2);

				// Site
				if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
					String siteV = splitPhysicalSitealue(site);
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteV + Lanlink_InternationalObj.International.viewPage2);
				}

				else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
					verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);
				}

			} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

				// New City
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + xngcitycode + Lanlink_InternationalObj.International.viewPage2);

				// New Site
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + CSR_Name + Lanlink_InternationalObj.International.viewPage2);

			}

			// Performance Reporting
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + performReport + Lanlink_InternationalObj.International.viewPage2);

			// Proactive Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + ProactiveMonitor + Lanlink_InternationalObj.International.viewPage2);
			waitforPagetobeenable();

			// Smarts Monitoring
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + smartmonitor + Lanlink_InternationalObj.International.viewPage2);

			// Technology
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + technology + Lanlink_InternationalObj.International.viewPage2);
			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			} else {
				// Site Alias
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + siteallias + Lanlink_InternationalObj.International.viewPage2);
			}
			// IV Reference
			verifyExists(Lanlink_InternationalObj.International.viewPage1 + IVReference + Lanlink_InternationalObj.International.viewPage2);

			// Remark

			(getTextFrom(Lanlink_InternationalObj.International.viewPage1 + remark + Lanlink_InternationalObj.International.viewPage2))
					.equals(remark);

			if (technology.equals("Atrica")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Overture")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

				// EPN Offnet
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + EPNoffnet + Lanlink_InternationalObj.International.viewPage2);

				// EPN EOSDH

			}

			if (technology.equals("Alu")) {
				// Device Name text field
				verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Accedian-1G")) {
				// Non_termination Point checkbox
				verifyExists(
						Lanlink_InternationalObj.International.viewPage1 + nonterminatepoinr + Lanlink_InternationalObj.International.viewPage2);

				// Device Name text field
				// verifyExists(Lanlink_InternationalObj.International.viewPage1+devicename+Lanlink_InternationalObj.International.viewPage2);

			}

			if (technology.equals("Belgacom VDSL")) {
				Reporter.log("No additonal fields displays");
			}
		}
	 
	 
	 public void returnbacktoviewsiteorderpage() throws IOException, InterruptedException 
	 {
	 	

	 ((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	 waitforPagetobeenable();
	 verifyExists(Lanlink_InternationalObj.International.Backbutton,"Back Button");
	 click(Lanlink_InternationalObj.International.Backbutton,"Back Button");
	 waitforPagetobeenable();
	 }
	 
	 
	 public void selectRowForsiteorder(String testDataFile,String sheetName,String scriptNo, String dataSetNo)
				throws InterruptedException, IOException {
			
			String siteordernumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siteordernumber");
			String siteOrdernumber_P2P=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");
			String topology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
			String interfaceSpeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
			String siteOrderNumber_10G_p2p=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber_10G_PointToPoint");
			


			 
			waitforPagetobeenable();
			
			Reporter.log("-----------------------------" + siteordernumber + "---------------------");
			int TotalPages;

			//scrolltoend();
			waitforPagetobeenable();
				List<WebElement> results = null;
				
				if(topology.equalsIgnoreCase("Point-to-Point")) {
					if(interfaceSpeed.equals("10GigE")) {
						results=webDriver.findElements(By.xpath("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteOrderNumber_10G_p2p + "']"));
					}else if(interfaceSpeed.equals("1GigE")) {
						results=webDriver.findElements(By.xpath("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteOrdernumber_P2P + "']"));
					}
						
				}else {
					results=webDriver.findElements(By.xpath("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteordernumber + "']"));
				}
			
				int numofrows = results.size();
				Reporter.log("no of results: " + numofrows);
				boolean resultflag;

							resultflag = results.get(0).isDisplayed();
							Reporter.log("status of result: " + resultflag);
							if (resultflag) {
								Reporter.log(results.get(0).getText());
								results.get(0).click();
								Thread.sleep(5000);
								click(Lanlink_InternationalObj.International.Actiondropdown_siteorder, "Action");

								waitforPagetobeenable();
							}
				}
	 
	 
	 public void editSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
				throws InterruptedException, IOException {
			String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
			String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
			String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
			String performReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_performReport");
			String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"EditSiteOrder_ProactiveMonitor");
			String smartmonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_smartmonitor");
			String technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
			String siteallias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_siteallias");
			String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_VLANid");
			String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_DCAenabledsite");
			String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"EditSiteOrder_cloudserviceprovider");
			String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_remark");
			String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"editsiteorder_devicename");
			String Protected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"editsiteorder_protected");
					
			String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"Siteordernumber");
			String IVreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"SiteOrder_Ivrefrence");
					
			String portBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_portBased");
			String vlanBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
			String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");
			String nontermination = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_nonterminationpoint");
			String modularMSp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
			String sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
			String Circuitreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"editsiteorder_circuitReference");
			String editEPNoffnetvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_EPNOffnet");
			String editEpNEosDH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editEpNEosDH");
			String GCRoloType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
					"editsiteorder_GCRoloType");
			String VLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_VLAN");
			String VlanEtherType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_VlanEtherType");
			String primaryVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_primaryVLAN");
			String primaryVlanEtherType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_primaryVlanEtherType");
			String mappingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
			String maapingModeAddedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_nonterminationpoint");
			
			String editOffnetvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_Offnet");

			waitforPagetobeenable();
			//click(Lanlink_InternationalObj.International.EditsideOrderlink);
			webDriver.findElement(By.xpath("//tr//*[contains(.,'"+siteOrderNumber_PointToPoint+"')]//following-sibling::td//a[text()='Edit']")).click();
			waitforPagetobeenable();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			// Point to Point
			if (VPNtopology.equals("Point-to-Point") && (circuitType.equals("Default"))) {

			
					if (interfaceSpeed.equals("1GigE")) {
						editSiteOrder_P2P_1G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
								DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
								remark);
					}

					else if (interfaceSpeed.equals("10GigE")) {
						editSiteOrder_P2P_10G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
								DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, remark);
					}
				}
			
			 if (VPNtopology.equals("Point-to-Point") && (circuitType.equals("Extended Circuit"))) {


					if (interfaceSpeed.equals("1GigE")) {
						editSiteOrder_P2P_1G_extendedCircuit(performReport, ProactiveMonitor, smartmonitor, siteallias,
								VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected,
								devicename, remark, sitePreferenceType);
					}

					else if (interfaceSpeed.equals("10GigE")) {
						editSiteOrder_P2P_10G_extendedCircuit(performReport, ProactiveMonitor, smartmonitor, siteallias,
								VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, remark,
								sitePreferenceType);
					}
				}
			

			// HUb & Spoke
			if (VPNtopology.equals("Hub&Spoke")) {

					if (interfaceSpeed.equals("1GigE")) {

						if (IVreference.equals("Primary")) {

							editSiteOrder_HubAndSpoke_1G_IVRefPrimary(performReport, ProactiveMonitor, smartmonitor,
									siteallias, VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination,
									Protected, devicename, remark, siteOrderNumber, IVreference, Circuitreference,
									editOffnetvalue);

						}

						else if (IVreference.equals("Access")) {

							editSiteOrder_HubAndSpoke_1G_IVRefAccess(performReport, ProactiveMonitor, smartmonitor,
									siteallias, VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination,
									Protected, devicename, remark, siteOrderNumber, IVreference, Circuitreference,
									GCRoloType, VLAN, VlanEtherType, primaryVLAN, primaryVlanEtherType, editOffnetvalue);

						}
					}

					else if (interfaceSpeed.equals("10GigE")) {

						editSiteOrder_HubAndSpoke_10G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
								DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
								remark, siteOrderNumber, IVreference, Circuitreference, editEPNoffnetvalue);
					}
				}


			// E-PN (Any to Any)
			if (VPNtopology.equals("E-PN (Any-to-Any)")) {

			

					if (interfaceSpeed.equals("1GigE")) {

						if (IVreference.equals("Primary")) {

							editSiteOrder_EPN_1G_IVRefPrimary(performReport, ProactiveMonitor, smartmonitor, siteallias,
									VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected,
									devicename, remark, siteOrderNumber, IVreference, Circuitreference, editEPNoffnetvalue,
									editEpNEosDH);

						}

						else if (IVreference.equals("Access")) {

							editSiteOrder_EPN_1G_IVRefAccess(performReport, ProactiveMonitor, smartmonitor, siteallias,
									VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected,
									devicename, remark, siteOrderNumber, IVreference, Circuitreference, GCRoloType, VLAN,
									VlanEtherType, primaryVLAN, primaryVlanEtherType, editEPNoffnetvalue, mappingMode,
									portBased, vlanBased, maapingModeAddedValue);

						}
					}

					else if (interfaceSpeed.equals("10GigE")) {

						editSiteOrder_HubAndSpoke_10G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
								DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
								remark, siteOrderNumber, IVreference, Circuitreference, editEPNoffnetvalue);
					}
				}

			
		}
	 
	 
	 
	 public void editSiteOrder_P2P_1G_extendedCircuit(String performReport, String ProactiveMonitor, String smartmonitor,
				String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology, String nontermination, 
				String Protected, String devicename, String remark, String siteOrder_sitePreferenceType)
				throws InterruptedException, IOException {
			
		//Performance Reporting
			editSiteOrder_Performancereporting(performReport);
			
			 
		//Pro active Monitoring 
			editSiteOrder_proactiveMonitoring( ProactiveMonitor);

			 
		//Smart Monitoring
			editSiteOrder_smartMonitoring( smartmonitor);

		//Vlan id
			editsiteOrder_vlanid( VLANid);
			
		//Site alias
			editsiteorder_sitealias( siteallias);
			
			//();
			waitforPagetobeenable();
			
		//DCA Enabled Site 
			editesiteOrder_DcaEnabled( DCAenabledsite, cloudserviceprovider);
		
			
		//Remark
			editsiteOrder_remark( remark);
			
		//Technology
			editSiteOrder_technology(technology);
			 
			if(technology.equalsIgnoreCase("Actelis")) {
				Reporter.log(" NO additional fields display for technology Actelis");
			}
			
			
			if(technology.equalsIgnoreCase("Atrica")) {
				 
				//Non-termination point
				 editsiteorder_NonterminationPoint( nontermination); 
				
				//Protected 
			//	 editsiteOrder_protected( Protected);
				 
				 //Device Name
			//	 editSiteOrder_deviceName( devicename);
				
			 }
			
			if(technology.equalsIgnoreCase("Overture")) {
				
				//Non-termination point
				 editsiteorder_NonterminationPoint( nontermination); 
				
				//Protected 
			//	 editsiteOrder_protected( Protected);
				 
			}
			
			
			if(technology.equalsIgnoreCase("Alu")) {
				
				 //Device Name
				 editSiteOrder_deviceName( devicename);
				
			}
			
			if(technology.equalsIgnoreCase("Accedian-1G")) {
				
				//Non-termination point
				 editsiteorder_NonterminationPoint( nontermination); 
				
				//Protected 
			//	 editsiteOrder_protected( Protected);
				
			}
			
			if(technology.equalsIgnoreCase("Cyan")) {
				
				//Non-termination point
				 editsiteorder_NonterminationPoint( nontermination); 
				
			}
			
			scrollDown(Lanlink_InternationalObj.International.okbutton);
		waitforPagetobeenable();
			verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");
			click(Lanlink_InternationalObj.International.okbutton, "OK");
			
			waitforPagetobeenable();
			
			
		}
	 
	 
	 public void editSiteOrder_Performancereporting(String performReport)throws InterruptedException, IOException
		{

			waitforPagetobeenable();
			
			if(isClickable(Lanlink_InternationalObj.International.Addsiteorder_performancereporting))
			{
				if (performReport.equalsIgnoreCase("null"))
				{
			   		Reporter.log("Performance reporting dropdown is not edited");
		
				}
				else
				{
					verifyExists(Lanlink_InternationalObj.International.Addsiteorder_performancereporting_xbutton,"Addsiteorder performancereporting");
					click(Lanlink_InternationalObj.International.Addsiteorder_performancereporting_xbutton,"Addsiteorder performancereporting");
					
					waitforPagetobeenable();
					
					webDriver.findElement(By.xpath("//div[label[text()='Performance Reporting']]//div[text()='" + performReport + "']")).click();
					waitforPagetobeenable();

			   		Reporter.log("Edited value for 'Performance reporting' dropdown is: " + performReport);
				}
		    }
			else
			{
		   		Reporter.log("Performance Reporting' dropdown is not available under 'Edit Site Order' page");
			}
		}
		
		public void editSiteOrder_proactiveMonitoring(String ProactiveMonitor)throws InterruptedException, IOException
		{

			waitforPagetobeenable();
			
			if(isClickable(Lanlink_InternationalObj.International.Addsiteorder_proactivemonitoring))
			{
				if (ProactiveMonitor.equalsIgnoreCase("null"))
				{
			   		Reporter.log("Proactive monitoring' dropdown value is not edited");
		
				}
				else
				{
					verifyExists(Lanlink_InternationalObj.International.Addsitorder_proactivemonitoring_xbutton,"Addsitorder Proactivemonitoring");
					click(Lanlink_InternationalObj.International.Addsitorder_proactivemonitoring_xbutton,"Addsitorder Proactivemonitoring");
					
					waitforPagetobeenable();
					
					webDriver.findElement(By.xpath("//div[label[text()='Proactive Monitoring']]//div[text()='" + ProactiveMonitor + "']")).click();
					waitforPagetobeenable();

			   		Reporter.log("Edited value for 'Pro active Monitoring' dropdown is: " + ProactiveMonitor);
				}
		    }
			else
			{
		   		Reporter.log(" Pro active Monitoring' dropdown is not available under 'Edit Site Order' page");
			}
		}
		
		
		public void editSiteOrder_smartMonitoring(String smartmonitor)throws InterruptedException, IOException
		{

			waitforPagetobeenable();
			
			if(isClickable(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoring))
			{
				if (smartmonitor.equalsIgnoreCase("null"))
				{
			   		Reporter.log("Smarts Monitoring dropdown value is not edited");
		
				}
				else
				{
					verifyExists(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoring_xbutton,"Addsiteorder smartmonitoring");
					click(Lanlink_InternationalObj.International.Addsiteorder_smartmonitoring_xbutton,"Addsiteorder smartmonitoring");
					
					waitforPagetobeenable();
					
					webDriver.findElement(By.xpath("//div[label[text()='Smarts Monitoring']]//div[text()='" + smartmonitor + "']")).click();
					waitforPagetobeenable();

			   		Reporter.log("Edited value for 'Smarts Monitoring' dropdown is: " + smartmonitor);
				}
		    }
			else
			{
		   		Reporter.log("Smart Monitoring dropdown is not available under 'Edit Site Order' page");
			}
		}
		
		public void editsiteOrder_vlanid(String VLANid) throws InterruptedException, IOException {
					
					boolean vlanAvailability = false;
					
				try {	
					vlanAvailability=isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Vlanid);

					if(vlanAvailability) {
					 if(VLANid.equalsIgnoreCase("null")) {
						 Report.LogInfo("Info",  "Vlanid field value is not edited","PASS");
						 Reporter.log("Vlanid field value is not edited");
					 }else {
						 clearTextBox(Lanlink_InternationalObj.International.Addsiteorder_Vlanid);
						 waitforPagetobeenable();
						 
						 sendKeys(Lanlink_InternationalObj.International.Addsiteorder_Vlanid, VLANid, "VLAN Id");
						 waitforPagetobeenable();
						 
						String VLANidValue= getAttributeFrom(Lanlink_InternationalObj.International.Addsiteorder_Vlanid,"value");
						Report.LogInfo("Info",  "Edited value for 'Vlan id' field is: "+ VLANidValue,"PASS");
						 Reporter.log( "Edited value for 'Vlan id' field is: "+ VLANidValue);
					 }
					}else {
						Report.LogInfo("Info",  "VLAN Id field is not available under 'Edit Site Order' page","FAIL");
						Reporter.log("VLAN Id field is not available under 'Edit Site Order' page");
					}
				}catch(NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info",  "VLAN Id field is not available under 'Edit Site Order' page","FAIL");
					Reporter.log("VLAN Id field is not available under 'Edit Site Order' page");
				}catch(Exception ee) {
					ee.printStackTrace();
					Report.LogInfo("Info",  " Not able to edit 'VLAn Id' text field","FAIL");
					Reporter.log(" not able to edit 'VLAN ID' text field");
				}
				}
				
				public void editsiteorder_sitealias(String siteallias)throws InterruptedException, IOException
		{

			waitforPagetobeenable();
			
			if(isClickable(Lanlink_InternationalObj.International.Addsiteorder_sitealias))
			{
				if (siteallias.equalsIgnoreCase("null"))
				{
			   		Reporter.log("Site Alias field value is not edited");
		
				}
				else
				{
					verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitealias,"Addsiteorder");
					clearTextBox(Lanlink_InternationalObj.International.Addsiteorder_sitealias);
					sendKeys(Lanlink_InternationalObj.International.Addsiteorder_sitealias,siteallias,"Addsiteorder");
					
					waitforPagetobeenable();
					
					String siteAliasvalue= getAttributeFrom(Lanlink_InternationalObj.International.Addsiteorder_sitealias,"value");


			   		Reporter.log("Edited value for 'Site Alias' field is: " + siteAliasvalue);
				}
		    }
			else
			{
		   		Reporter.log(" Site Alias field is not available under 'Edit Site Order' page");
			}
		}
		
		
		public void editesiteOrder_DcaEnabled(String DCAenabledsite, String cloudserviceprovider) 
						throws InterruptedException, IOException {
					


					boolean DCAavailability = false;

					try {
						DCAavailability = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox);
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (DCAavailability) {

						if (!DCAenabledsite.equalsIgnoreCase("null")) {

							boolean dcaenabled = isSelected(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox,"DCAenabledsitecheckbox");
							waitforPagetobeenable();

							if (DCAenabledsite.equalsIgnoreCase("yes")) {

								if (dcaenabled) {

									Report.LogInfo("Info",
											"DCA Enabled Site is already Selected while creating","PASS");

									if (cloudserviceprovider.equalsIgnoreCase("null")) {

										Report.LogInfo("Info", "No changes made to Cloud Service Provider","PASS");

									} else {

										addDropdownValues_commonMethod("Cloud Service Provider", Lanlink_InternationalObj.International.Addsiteorder_cloudserviceProvider, cloudserviceprovider);
									}

								} else {

									click(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox,"DCAenabledsitecheckbox");
									Reporter.log("DCA Enabled Site check box is selected");
									Report.LogInfo("Info", "DCA Enabled Site checkbox is selected","PASS");

									if (cloudserviceprovider.equalsIgnoreCase("null")) {

										Report.LogInfo("Info", "No changes made to Cloud Service Provider","PASS");

									} else {

										addDropdownValues_commonMethod("Cloud Service Provider", Lanlink_InternationalObj.International.Addsiteorder_cloudserviceProvider, cloudserviceprovider);
									}
								}

							}

							else if (DCAenabledsite.equalsIgnoreCase("no")) {

								if (dcaenabled) {

									click(Lanlink_InternationalObj.International.Addsiteorder_DCAenabledsitecheckbox,"DCAenabledsitecheckbox");
									Reporter.log("DCA Enabled Site check box is unselected");
									Report.LogInfo("Info", "DCA Enabled Site is unselected as Expected","PASS");

								} else {
									Report.LogInfo("Info",
											"DCA Enabled Site was not selected during service creation and it remains unselected as expected","PASS");
								}

							}
						} else {
							Report.LogInfo("Info", "No changes made for DCAenabled site chekbox as expected","PASS");
						}
					} else {
						Report.LogInfo("Info",
								"DCA Enabled Site checkbox is not displaying under 'Edit Site Order' page","FAIL");
					}

				}
				
				
				public void editsiteOrder_remark(String remark)throws InterruptedException, IOException
		{

			waitforPagetobeenable();
			
			if(isClickable(Lanlink_InternationalObj.International.Addsiteorder_remark))
			{
				if (remark.equalsIgnoreCase("null"))
				{
			   		Reporter.log("Remark text field value is not edited");
		
				}
				else
				{
					verifyExists(Lanlink_InternationalObj.International.Addsiteorder_remark,"Addsiteorder");
					clearTextBox(Lanlink_InternationalObj.International.Addsiteorder_remark);
					sendKeys(Lanlink_InternationalObj.International.Addsiteorder_remark,remark,"Addsiteorder");

					
					String remarkValue= getAttributeFrom(Lanlink_InternationalObj.International.Addsiteorder_remark,"value");
					
			   		Reporter.log("Edited value for 'Remark' field is: " + remarkValue);
				}
		    }
			else
			{
		   		Reporter.log("Remark text field is not available under 'Edit Site Order' page");
			}
		}
		
		public void editSiteOrder_technology(String technology) throws InterruptedException {

					boolean techValue = false;

					try {
						techValue=webDriver.findElement(By.xpath("//div[contains(text(),'"+ technology + "')]")).isDisplayed();

						if (techValue) {

							ExtentTestManager.getTest().log(LogStatus.PASS,
									" Technology value is displaying as: " + technology + " as expected");

						} else {
							String actualValue = webDriver.findElement(By.xpath("//div[div[label[contains(text(),'Technology')]]]/div[2]")).getText();
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									" Technology value is not displaying as expected" + "   Actual value displaying is: "
											+ actualValue + "  Expected value for Technology is: " + technology);
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " Technology value is not displaying as expected");
						Reporter.log(" Technology value is not displaying as expected");
					}
				}
				
				
				public void editsiteorder_NonterminationPoint(String nontermination) throws InterruptedException, IOException {
					
					boolean NonTerminationPointAvailability=false;
					
					try {
						NonTerminationPointAvailability=isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint);
					
					if(NonTerminationPointAvailability) {
					
				  if(!nontermination.equalsIgnoreCase("null")) {
						
					  boolean nonterminatepoint=false;
					 try { 
					  nonterminatepoint=isSelected(Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint,"Non termination point");
					 }catch(Exception e) {
						 e.printStackTrace();
					 }
						waitforPagetobeenable();
						
						if (nontermination.equalsIgnoreCase("yes")) {

							if(nonterminatepoint) {
								
							//	Report.LogInfo("Info", " 'Non-Termination point' checkbox is already Selected while creating","PASS");
								
							}else {
								
								click(Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint,"Addsiteorder Nonterminationpoint");
								//Reporter.log("'Non-Termination point' check box is selected");
							//	Report.LogInfo("Info","'Non-Termination point' is edited and gets selected","PASS");
							}
							
							
						}

						else if (nontermination.equalsIgnoreCase("no")) {
							
							if(nonterminatepoint) {
								
								click(Lanlink_InternationalObj.International.Addsiteorder_nonterminationpoint,"Addsiteorder Nonterminationpoint");
							//	Reporter.log("'Non-Termination point' check box is unselected");
							//	Report.LogInfo("Info","'Non-Termination point' is edited and gets unselected","PASS");
								
							}else {
								//Report.LogInfo("Info", "'Non-Termination point' was not selected during service creation and it remains unselected as expected","PASS");
							}
							
						}
					}else {
					//	Report.LogInfo("Info","No changes made for 'Non-Termination point' chekbox as expected","PASS");
					}
				}else {
				//	Report.LogInfo("Info", " Non-Termination Point checkbox is not available under 'Edit Site order' page","FAIL");
				}
					}catch(NoSuchElementException e) {
						e.printStackTrace();
					//	Report.LogInfo("Info"," Non-Termination Point checkbox is not available under 'Edit Site order' page","FAIL");
					//	Reporter.log(" Non-Termination Point checkbox is not available under 'Edit Site order' page");
					}catch(Exception err) {
						err.printStackTrace();
						//Report.LogInfo("Info", " Not able to click on 'Non-termination point' checkbox ","FAIL");
						//Reporter.log(" Not able to click on 'Non-termination point' checkbox ");
					}
				}
				
				
				
				public void editsiteOrder_protected(String application, String Protected) throws InterruptedException {
					
					boolean prtctedAvailability= false;
					
					try {
						prtctedAvailability=findWebElement(Lanlink_InternationalObj.International.Addsiteorder_protected).isDisplayed();
					
					if(prtctedAvailability) {
			          if(!Protected.equalsIgnoreCase("null")) {
							
			        	  boolean prtcted=false;
			        	 try { 
			        	  prtcted=findWebElement(Lanlink_InternationalObj.International.Addsiteorder_protected).isSelected();
			        	 }catch(Exception e) {
			        		 e.printStackTrace();
			        	 }
							Thread.sleep(2000);
							
							if (Protected.equalsIgnoreCase("yes")) {
								if(prtcted) {
									ExtentTestManager.getTest().log(LogStatus.PASS, " 'Protected' checkbox is already Selected while creating");
								}else {
									
									click(Lanlink_InternationalObj.International.Addsiteorder_protected,"Addsiteorder_protected");
									Reporter.log("'Non-Termination point' check box is selected");
									ExtentTestManager.getTest().log(LogStatus.PASS,"'Protected' is edited and gets selected");
								}
							}

							else if (Protected.equalsIgnoreCase("no")) {
								if(prtcted) {
									
									click(Lanlink_InternationalObj.International.Addsiteorder_protected,"Addsiteorder_protected");
									Reporter.log("'Non-Termination point' check box is unselected");
									ExtentTestManager.getTest().log(LogStatus.PASS,"'Protected' is edited and gets unselected");
									
								}else {
									ExtentTestManager.getTest().log(LogStatus.PASS, "'Protected' was not selected during service creation and it remains unselected as expected");
								}
							}
						}else {
							ExtentTestManager.getTest().log(LogStatus.PASS,"No changes made for 'Protected' chekbox as expected");
						}
					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, " Protected checkbox is not displaying under 'Edit Site order' page");
					}
					}catch(NoSuchElementException e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " Protected checkbox is not displaying under 'Edit Site order' page");
						Reporter.log(" Protected checkbox is not displaying under 'Edit Site order' page");
					}catch(Exception err) {
						err.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to select 'protected' checkbox");
						Reporter.log(" Not able to select 'protected' checkbox");
					
					}
					
				}
				
				
				
				
				public void editSiteOrder_P2P_10G(String performReport, String ProactiveMonitor, String smartmonitor,
						String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology, String nontermination, String Protected, String remark) throws InterruptedException, IOException {
					
					//Performance Reporting
					editSiteOrder_Performancereporting(performReport);
					
					 
				//Pro active Monitoring 
					editSiteOrder_proactiveMonitoring( ProactiveMonitor);

					 
				//Smart Monitoring
					editSiteOrder_smartMonitoring( smartmonitor);

				//Vlan id
					editsiteOrder_vlanid( VLANid);
					
				//Site Preference Type	
					//addDropdownValues_commonMethod("Site Preference Type" , Lanlink_InternationalObj.International.AddSiteOrder_sitePreferenceType_Dropodwn , siteOrder_sitePreferenceType);
					 
				//Site alias
					editsiteorder_sitealias( siteallias);
					
					//();
					waitforPagetobeenable();
					
				//DCA Enabled Site 
					editesiteOrder_DcaEnabled( DCAenabledsite, cloudserviceprovider);
				
					
				//Remark
					editsiteOrder_remark( remark);
					
				//Technology
					editSiteOrder_technology(technology);
					 
					if(technology.equalsIgnoreCase("Actelis")) {
						Reporter.log(" NO additional fields display for technology Actelis");
					}
					
					
					if(technology.equalsIgnoreCase("Atrica")) {
						 
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
						//Protected 
					//	 editsiteOrder_protected( Protected);
						 
						 //Device Name
					//	 editSiteOrder_deviceName( devicename);
						
					 }
					
					if(technology.equalsIgnoreCase("Overture")) {
						
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
						//Protected 
						// editsiteOrder_protected( Protected);
						 
					}
					
					
					if(technology.equalsIgnoreCase("Alu")) {
						
						 //Device Name
						// editSiteOrder_deviceName( devicename);
						
					}
					
					if(technology.equalsIgnoreCase("Accedian-1G")) {
						
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
						//Protected 
						// editsiteOrder_protected( Protected);
						
					}
					
					if(technology.equalsIgnoreCase("Cyan")) {
						
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
					}
					
					scrollDown(Lanlink_InternationalObj.International.okbutton);
				waitforPagetobeenable();
					verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");
					click(Lanlink_InternationalObj.International.okbutton, "OK");
					
					waitforPagetobeenable();
					
				}
				
				
				public void editSiteOrder_P2P_10G_extendedCircuits(String application, String performReport, String ProactiveMonitor, String smartmonitor,
						String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology, String nontermination,
						String Protected, String remark, String siteOrder_sitePreferenceType) throws InterruptedException, IOException {
					//Performance Reporting
					editSiteOrder_Performancereporting(performReport);
					
					 
				//Pro active Monitoring 
					editSiteOrder_proactiveMonitoring( ProactiveMonitor);

					 
				//Smart Monitoring
					editSiteOrder_smartMonitoring( smartmonitor);

				//Vlan id
					editsiteOrder_vlanid( VLANid);
					
				//Site Preference Type	
					//addDropdownValues_commonMethod("Site Preference Type" , Lanlink_InternationalObj.International.AddSiteOrder_sitePreferenceType_Dropodwn , siteOrder_sitePreferenceType);
					 
				//Site alias
					editsiteorder_sitealias( siteallias);
					
					//();
					waitforPagetobeenable();
					
				//DCA Enabled Site 
					editesiteOrder_DcaEnabled( DCAenabledsite, cloudserviceprovider);
				
					
				//Remark
					editsiteOrder_remark( remark);
					
				//Technology
					editSiteOrder_technology(technology);
					 
					if(technology.equalsIgnoreCase("Actelis")) {
						Reporter.log(" NO additional fields display for technology Actelis");
					}
					
					
					if(technology.equalsIgnoreCase("Atrica")) {
						 
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
						//Protected 
					//	 editsiteOrder_protected( Protected);
						 
						 //Device Name
					//	 editSiteOrder_deviceName( devicename);
						
					 }
					
					if(technology.equalsIgnoreCase("Overture")) {
						
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
						//Protected 
						// editsiteOrder_protected( Protected);
						 
					}
					
					
					if(technology.equalsIgnoreCase("Alu")) {
						
						 //Device Name
						// editSiteOrder_deviceName( devicename);
						
					}
					
					if(technology.equalsIgnoreCase("Accedian-1G")) {
						
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
						//Protected 
						// editsiteOrder_protected( Protected);
						
					}
					
					if(technology.equalsIgnoreCase("Cyan")) {
						
						//Non-termination point
						 editsiteorder_NonterminationPoint( nontermination); 
						
					}
					
					scrollDown(Lanlink_InternationalObj.International.okbutton);
				waitforPagetobeenable();
					verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");
					click(Lanlink_InternationalObj.International.okbutton, "OK");
					
					waitforPagetobeenable();
					
				}
					
				
				public void editSiteOrder_P2P_10G_extendedCircuit(String performReport, String ProactiveMonitor,
						String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
						String technology, String nontermination, String Protected, String remark,
						String siteOrder_sitePreferenceType) throws InterruptedException, IOException, IOException {

					// Performance Reporting
					editSiteOrder_Performancereporting(performReport);

					// Pro active Monitoring
					editSiteOrder_proactiveMonitoring(ProactiveMonitor);

					// Smart Monitoring
					editSiteOrder_smartMonitoring(smartmonitor);

					// Vlan id
					editsiteOrder_vlanid(VLANid);

					// Site alias
					editsiteorder_sitealias(siteallias);

					// Site Preference Type
					addDropdownValues_commonMethod("Site Preference Type",
							Lanlink_InternationalObj.International.AddSiteOrder_sitePreferenceType_Dropodwn, siteOrder_sitePreferenceType);

					//// scrolltoend();
					waitforPagetobeenable();

					// DCA Enabled Site
					editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

					// Remark
					editsiteOrder_remark(remark);

					// Technology
					editSiteOrder_technology(technology);

					if (technology.equalsIgnoreCase("Accedian")) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

					}
					//// scrolltoend();
					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.okbutton, "OK");

				}
				
				public void editSiteOrder_HubAndSpoke_1G_IVRefPrimary(String performReport, String ProactiveMonitor,
						String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
						String technology, String nontermination, String Protected, String devicename, String remark,
						String siteOrderNumber, String IVReference, String CircuitReference, String offnet)
						throws InterruptedException, IOException {

					// Site Order Number (Siebel Service ID)
					

					// Performance Reporting
					editSiteOrder_Performancereporting(performReport);

					// Pro active Monitoring
					editSiteOrder_proactiveMonitoring(ProactiveMonitor);

					// Smart Monitoring
					editSiteOrder_smartMonitoring(smartmonitor);

					if (technology.equalsIgnoreCase("Belgacom VDSL")) {
						Reporter.log("Site Alias field will not display for Belgacom technology");
					} else {
						// Site alias

						editsiteorder_sitealias(siteallias);

					}
				

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();

					// Remark
					editsiteOrder_remark(remark);

					// Circuit Reference
					
					// Technology
					editSiteOrder_technology(technology);

					if (technology.equalsIgnoreCase("Actelis")) {
						Reporter.log(" NO additional fields display for technology Actelis");
					}

					if (technology.equalsIgnoreCase("Atrica")) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

						// Device Name
						editSiteOrder_deviceName(devicename);

					}

					if (technology.equalsIgnoreCase("Overture")) {
						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);
					}

					if (technology.equalsIgnoreCase("Alu")) {

						// Device Name
						editSiteOrder_deviceName(devicename);

					}

					if ((technology.equals("Accedian-1G"))) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

					}

					if (technology.equalsIgnoreCase("Belgacom VDSL")) {

						Reporter.log("NO additonal fields displays");
					}
					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.okbutton, "OK");
				}
					
				
				public void editSiteOrder_deviceName(String devicename) throws InterruptedException, IOException {

					boolean devicenameAvailability = false;
					try {

						devicenameAvailability = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Devicenamefield);

						if (devicenameAvailability) {

							if (devicename.equalsIgnoreCase("Null")) {

								Report.LogInfo("Info", " NO changes made for 'Device Name' field", "PASS");
							} else {

								clearTextBox(Lanlink_InternationalObj.International.Addsiteorder_Devicenamefield);

								sendKeys(Lanlink_InternationalObj.International.Addsiteorder_Devicenamefield, devicename, "Device name");
								waitforPagetobeenable();

								String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.Addsiteorder_Devicenamefield,
										"value");
								Report.LogInfo("Info", " Device Name value has been edited and the edited value is: " + actualvalue,
										"PASS");
							}
						} else {
							Report.LogInfo("Info", " Device name text field is not displaying under 'Edit Site Order' page",
									"FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " Device name text field is not displaying under 'Edit Site Order' page", "FAIL");
						Reporter.log(" Device name text field is not displaying under 'Edit Site Order' page");
					} catch (Exception ee) {
						ee.printStackTrace();
						Report.LogInfo("Info", " Not able to edit 'device name' field", "FAIL");
						Reporter.log(" Not able to edit 'device name' field");
					}
				}
	 
				public void editSiteOrder_HubAndSpoke_10G(String performReport, String ProactiveMonitor, String smartmonitor,
						String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
						String nontermination, String Protected, String devicename, String remark, String siteOrderNumber,
						String IVReference, String CircuitReference, String offnet) throws InterruptedException, IOException {

					// Site Order Number (Siebel Service ID)
					editSiteOrder_siteOrderNumber(siteOrderNumber);

					// IV Reference
					editSiteOrder_IVReference(IVReference);

					// Performance Reporting
					editSiteOrder_Performancereporting(performReport);

					// Pro active Monitoring
					editSiteOrder_proactiveMonitoring(ProactiveMonitor);

					// Smart Monitoring
					editSiteOrder_smartMonitoring(smartmonitor);

					// Site alias

					editsiteorder_sitealias(siteallias);

					// offnet
				//	editSiteorder_Offnet(offnet);

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();

					// Remark
					editsiteOrder_remark(remark);

					// Circuit Reference
				//	editsiteorder_circuitReference(CircuitReference);

					// Technology
					editSiteOrder_technology(technology);

					// Non-termination point
					editsiteorder_NonterminationPoint(nontermination);

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.okbutton, "OK");
				}
	 
				public void editSiteOrder_siteOrderNumber(String siteOrderNumber) throws InterruptedException {

					boolean siteOrderValue = false;
					try {
					//	siteOrderValue = isElementPresent(Lanlink_InternationalObj.International.siteOrderNumber1 + siteOrderNumber
						//		+ Lanlink_InternationalObj.International.siteOrderNumber2);

						if (siteOrderValue) {

							Report.LogInfo("Info", " Site Order Number (Siebel Service ID) value is displaying as: "
									+ siteOrderNumber + " as expected", "PASS");
							Reporter.log(" Site Order Number (Siebel Service ID) value is displaying as: " + siteOrderNumber
									+ " as expected");
						} else {
						//	String actualValue = getTextFrom(Lanlink_InternationalObj.International.siteOrderNumber);
							
							
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'Site Order NUmber' value is not displaying as expected", "FAIL");
						Reporter.log(" 'Site Order NUmber' value is not displaying as expected");
					} catch (Exception err) {
						err.printStackTrace();
						Report.LogInfo("Info", " 'Site Order NUmber' value is not displaying as expected", "FAIL");
						Reporter.log(" 'Site Order NUmber' value is not displaying as expected");
					}

				}
				
				
				public void editSiteOrder_IVReference(String IVReference) throws InterruptedException, IOException {

					boolean IVrefValue = false;

					try {
					//	IVrefValue = isElementPresent(
					//			Lanlink_InternationalObj.International.IVReference1 + IVReference + Lanlink_InternationalObj.International.IVReference2);

						if (IVrefValue) {

							Report.LogInfo("Info", " IV reference value is displaying as: " + IVReference + " as expected", "PASS");
							Reporter.log(" IV reference value is displaying as: " + IVReference + " as expected");

						} else {
						//	String actualValue = getTextFrom(Lanlink_InternationalObj.International.IVReference);
							
							
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'IV Reference' value is not dispaying as expected", "FAIL");
						Reporter.log("IV Reference value is not dispaying as expected");
					}
				}
				
				public void editSiteOrder_EPN_1G_IVRefPrimary(String performReport, String ProactiveMonitor, String smartmonitor,
						String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
						String nontermination, String Protected, String devicename, String remark, String siteOrderNumber,
						String IVReference, String CircuitReference, String EPNoffnet, String EPnEosDH)
						throws InterruptedException, IOException {

					// Site Order Number (Siebel Service ID)
					editSiteOrder_siteOrderNumber(siteOrderNumber);

					// IV Reference
					editSiteOrder_IVReference(IVReference);

					// Performance Reporting
					editSiteOrder_Performancereporting(performReport);

					// Pro active Monitoring
					editSiteOrder_proactiveMonitoring(ProactiveMonitor);

					// Smart Monitoring
					editSiteOrder_smartMonitoring(smartmonitor);

					// Site alias
					if (technology.equalsIgnoreCase("Belgacom VDSL")) {

						Reporter.log("Site alias field will not display for 'Belgacom VDSL' technology");
					} else {
						editsiteorder_sitealias(siteallias);
					}

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();

					// Remark
					editsiteOrder_remark(remark);

					// Circuit Reference
		//		editsiteorder_circuitReference(CircuitReference);

					// Technology
					editSiteOrder_technology(technology);

					if (technology.equalsIgnoreCase("Actelis")) {
						Reporter.log(" NO additional fields display for technology Actelis");
					}

					if (technology.equalsIgnoreCase("Atrica")) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

						// Device Name
						editSiteOrder_deviceName(devicename);

					}

					if (technology.equalsIgnoreCase("Overture")) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

						// Device Name
						editSiteOrder_deviceName(devicename);

					}

					if (technology.equalsIgnoreCase("Alu")) {

						// Device Name
						editSiteOrder_deviceName(devicename);

					}

					if ((technology.equalsIgnoreCase("Accedian-1G")) || (technology.equalsIgnoreCase("Accedian-1G"))) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

						// Device Name
						editSiteOrder_deviceName(devicename);

					}

					if (technology.equalsIgnoreCase("Belgacom VDSL")) {

						Reporter.log("No additonal fields displays");
					}

					// EPN Offnet
				//	editSiteorder_Offnet(EPNoffnet);

					// EPN EOSDH
					sendKeys(Lanlink_InternationalObj.International.Addsiteorder_EPNEOSDHCheckbox, EPnEosDH, "EPN EOSDH");

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.okbutton, "OK");
				}
				
				public void editSiteOrder_HubAndSpoke_1G_IVRefAccess(String performReport, String ProactiveMonitor,
						String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
						String technology, String nontermination, String Protected, String devicename, String remark,
						String siteOrderNumber, String IVReference, String circuitReference, String GCRoloType, String VLAN,
						String VlanEtherType, String primaryVLAN, String primaryVlanEtherType, String offnet)
						throws InterruptedException, IOException {

					// Site Order Number (Siebel Service ID)
					editSiteOrder_siteOrderNumber(siteOrderNumber);

					// IV Reference
					editSiteOrder_IVReference(IVReference);

					// Performance Reporting
					editSiteOrder_Performancereporting(performReport);

					// Pro active Monitoring
					editSiteOrder_proactiveMonitoring(ProactiveMonitor);

					// Smart Monitoring
					editSiteOrder_smartMonitoring(smartmonitor);

					if (technology.equalsIgnoreCase("Belgacom VDSL")) {
						Reporter.log("No additional fields display, when 'Belgacom VDSL' technology is selected");

					} else {
						// Site alias

						editsiteorder_sitealias(siteallias);

					}

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();

					// Offnet
				//	editSiteorder_Offnet(offnet);

					// Remark
					editsiteOrder_remark(remark);

					// Circuit Reference
				//	editsiteorder_circuitReference(circuitReference);

					// Technology
					editSiteOrder_technology(technology);

					if (technology.equalsIgnoreCase("Actelis")) {
						Reporter.log(" NO additional fields display for technology Actelis");
					}

					if (technology.equalsIgnoreCase("Atrica")) {

						// Non-termination point

					}

					if (technology.equalsIgnoreCase("Overture")) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

				

					}

					if (technology.equalsIgnoreCase("Alu")) {

						Reporter.log("No additional fields display for Alu Technology");
					}

					if ((technology.equals("Accedian")) || (technology.equals("Accedian-1G"))) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

					}

					if (technology.equalsIgnoreCase("Belgacom VDSL")) {

						Reporter.log("NO additonal fields display");
					}
					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.okbutton, "OK");
				}
				
				public void editSiteOrder_EPN_1G_IVRefAccess(String performReport, String ProactiveMonitor, String smartmonitor,
						String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
						String nontermination, String Protected, String devicename, String remark, String siteOrderNumber,
						String IVReference, String circuitReference, String GCRoloType, String VLAN, String VlanEtherType,
						String primaryVLAN, String primaryVlanEtherType, String offnet, String mappingMode, String portBased,
						String vlanBased, String maapingModeAddedValue) throws InterruptedException, IOException {

					// String deviceName=DataMiner.fngetcolvalue(testDataFile, sheetName,
					// scriptNo, dataSetNo, "proactiveMonitoring");
					// String mappingMode=DataMiner.fngetcolvalue(testDataFile, sheetName,
					// scriptNo, dataSetNo, "smartMonitoring");

					// Site Order Number (Siebel Service ID)
					editSiteOrder_siteOrderNumber(siteOrderNumber);

					// IV Reference
					editSiteOrder_IVReference(IVReference);

					// Performance Reporting
					editSiteOrder_Performancereporting(performReport);

					// Pro active Monitoring
					editSiteOrder_proactiveMonitoring(ProactiveMonitor);

					// Smart Monitoring
					editSiteOrder_smartMonitoring(smartmonitor);

					// Site alias
					if (technology.equalsIgnoreCase("Belgacom VDSL")) {

						Reporter.log("Site alias field will not display for 'Belgacom VDSL' technology");
					} else {
						editsiteorder_sitealias(siteallias);
					}

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();

					// Remark
					editsiteOrder_remark(remark);

					// Technology
					editSiteOrder_technology(technology);

					if (technology.equalsIgnoreCase("Actelis")) {
						Reporter.log(" NO additional fields display for technology Actelis");
					}

					if (technology.equalsIgnoreCase("Atrica")) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

						// Device Name
						editSiteOrder_deviceName(devicename);

						// Mapping Mode
						// --- editSiteOrder_mappingMode(mappingMode);

						// validation --> sometime mapping mode dropdown will not edited,
						// port or Vlan fields will be edited
						if ((maapingModeAddedValue.equalsIgnoreCase("Port Based")) && (mappingMode.equalsIgnoreCase("null"))) {

							sendKeys(Lanlink_InternationalObj.International.portname_textField, portBased, "Port Name");

						} else if ((maapingModeAddedValue.equalsIgnoreCase("Vlan Based"))
								&& (mappingMode.equalsIgnoreCase("null"))) {

							sendKeys(Lanlink_InternationalObj.International.vlanid_textfield, vlanBased, "VLAN Id");
						}
					}

					if (technology.equalsIgnoreCase("Overture")) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

						// Device Name
						editSiteOrder_deviceName(devicename);

						// GCR OLO Type
						// -- editSiteOrder_GCRoloType(GCRoloType);

						// VLAN
						// -- editsiteorder_VLAN(VLAN);

						// VLAN Ether Type
						// -- editSiteOrder_VLANEtherType(VLANEtherType);

					}

					if (technology.equalsIgnoreCase("Alu")) {

						// Device Name
						editSiteOrder_deviceName(devicename);

						// Mapping Mode
						// -- editSiteOrder_mappingMode(mappingMode);

						// validation --> sometime mapping mode dropdown will not edited,
						// port or Vlan fields will be edited
						if ((maapingModeAddedValue.equalsIgnoreCase("Port Based")) && (mappingMode.equalsIgnoreCase("null"))) {

							sendKeys(Lanlink_InternationalObj.International.portname_textField, portBased, "Port Name");

						} else if ((maapingModeAddedValue.equalsIgnoreCase("Vlan Based"))
								&& (mappingMode.equalsIgnoreCase("null"))) {

							sendKeys(Lanlink_InternationalObj.International.vlanid_textfield, "VLAN Id");
						}

					}

					if ((technology.equalsIgnoreCase("Accedian")) || (technology.equalsIgnoreCase("Accedian-1G"))) {

						// Non-termination point
						editsiteorder_NonterminationPoint(nontermination);

						// Device Name
						editSiteOrder_deviceName(devicename);

					}

					if (technology.equalsIgnoreCase("Belgacom VDSL")) {

						Reporter.log("No additonal fields displays");
					}

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.okbutton, "OK");
				}
				
				
				public void verifyFieldsandAddCPEdevicefortheserviceselected_1G(String testDataFile, String sheetName,
						String scriptNo, String dataSetNo) throws InterruptedException, IOException {
					
					String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
					String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
					String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
					String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_managementAddress");
					String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
					String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_1G");
					String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Macaddress");
					String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
					String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_hexaSerialnumber");
					String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_linkLostForwarding");
					String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
					String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");
					String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_1G");
					String mediaSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
					String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpe_manageaddressdropdownvalue");
					String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
					String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpe_existingmanagementAddressSelection");
					

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.CPEdevice_adddevicelink,"CPEdevice Adddevicelink");
					waitforPagetobeenable();

					if (technologySelected.equalsIgnoreCase("Accedian-1G")) {
						equip_adddevi_Accedian1G(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm,
								mediaSelection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
								newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue);

					} else {

						if (technologySelected.equalsIgnoreCase("Atrica") && vpnTopology.equals("Hub&Spoke")) {
							selectTechnology_HubAndSpoke();
						}

						equip_addDevice_1G(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm,
								mediaSelection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
								newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue);
					}

				}
				
				
				public void verifydetailsEnteredforCPEdevice_1G(String testDataFile, String sheetName, String scriptNo,
						String dataSetNo) throws InterruptedException, IOException {
					String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
					String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
					String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
					String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_managementAddress");
					String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
					String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_1G");
					String MACAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Macaddress");
					String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
					String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_hexaSerialnumber");
					String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_linkLostForwarding");
					String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
					String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");
					String vender_model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_1G");
					String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
					String MediaselectionActualValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
					String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpe_manageaddressdropdownvalue");
					String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
					String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpe_existingmanagementAddressSelection");
					
					
					Report.LogInfo("Info", "verify the details entered for creating device","Info");
					
					clickOnBankPage();
					waitforPagetobeenable();
					
					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					
					webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();
					waitforPagetobeenable();
				
					String[] RouterId=new String[2];
					RouterId=cpename.split(".lanlink");
					
					String RouterIdValue=RouterId[0];
					
					
					String mediaSelectionValueInViewDevicePage="no";
					if(Mediaselection.equalsIgnoreCase("null")) {
						Mediaselection=mediaSelectionValueInViewDevicePage;
					}else {
						Mediaselection=mediaSelectionValueInViewDevicePage;
					}
				  
				// verifyEnteredvalues_deviceName("Router Id",RouterIdValue,cpename );
				  
				  verifyEnteredvalues("Router Id", RouterIdValue);
				  
				  verifyEnteredvalues("Vendor/Model", vender_model);
				  
				  verifyEnteredvalues("Snmpro", Snmpro);
				  
				  
					//Management Address  
					  	if((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
						  verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
						 }
						 else if((existingmanagementAddress.equalsIgnoreCase("no")) && (newmanagementAddress.equalsIgnoreCase("Yes"))) {
							 verifyEnteredvalues("Management Address", ManagementAddress);
						 } 
				  
//				  verifyEnteredvalues("MEP Id", Mepid);
				  
				  verifyEnteredvalues("Power Alarm", PowerAlarm);
				  
				if(technologySelected.equalsIgnoreCase("Accedian-1G")) {  
					
				  verifyEnteredvalues("Serial Number", serialNumber);
				  
				}else {
				  
				  verifyEnteredvalues("Media Selection", Mediaselection);
				  
				  verifyEnteredvalues("MAC Address", MACAddress);
				  
				}
			}


			public void eDITCPEdevicedetailsentered_1G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
						throws InterruptedException, IOException {
					String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
					String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
					String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
					String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_managementAddress");
					String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mepid");
					String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_poweralarm_1G");
					String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Macaddress");
					String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_serialNumber");
					String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_hexaSerialnumber");
					String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_linkLostForwarding");
					String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
					String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpename");
					String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_vender_1G");
					String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mediaselection");
					String MediaselectionActualValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
					String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpe_manageaddressdropdownvalue");
					String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
					String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"cpe_existingmanagementAddressSelection");

					Reporter.log("Entered edit functionalitty");

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.viewPCEdevice_Actiondropdown, "Action");
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.EditCPEdevicelinkunderviewpage,"EditCPEdevice");
					Reporter.log("edit functionality worked");

					waitToPageLoad();

					// Name field
					device_editnamefield(cpename);

					// vendor/model
					device_editVendorModelField(vender);

					// Snmpro
					device_editSnmproField();

					// Management address
					device_editManagementAddressField(managementAddress);

					// Mepid
					device_editMEPIdField(Mepid);

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();

					// power alarm
					device_editPowerAlarm(poweralarm);

					if (technologySelected.equalsIgnoreCase("Accedian-1G")) {

						// Serial Number
						device_editserialnumber(serialNumber);

					} else {

						// Media Selection
						device_editMediaselection(Mediaselection);

						waitforPagetobeenable();

						// Mac address
						device_editMACaddress(Macaddress);

						// linklost forwarding
						device_editlinkLostforwarding(linkLostForwarding);
					}

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.okbutton, "OK");
					waitforPagetobeenable();

				}
			
			
			public void equip_addDevice_1G(String interfaceSpeed, String cpename, String vender, String snmpro,
					String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,
					String serialNumber, String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress,
					String existingmanagementAddress, String manageaddressdropdownvalue)
					throws InterruptedException, java.io.IOException {

				try {

					String linklostForwardingcheckboxstate = "enabled";

					String[] Vender = { "Overture ISG-26", "Overture ISG-26R", "Overture ISG-26S", "Overture ISG140",
							"Overture ISG180", "Overture ISG6000" };

					String[] powerAlarm = { "AC", "DC" };

					String[] Mediaselection = { "SFP-A with SFP-B", "RJ45-A with SFP-B" };

					String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

					String mepValue = "null";

					//// scrolltoend();
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
					waitforPagetobeenable();

					// Vendor/Model Error Message
					device_vendorModelWarningMessage();

					// Management Address Error Message
					device_managementAddressWarningMessage();

					// Power Alarm Error Message
					device_powerAlarmWarningMessage();

					// Media Selection Error Message
					// device_mediaSelectionWarningMessage();

					// MAC Address Error Message
					device_macAddressWarningMessage();

					// Vendor/Model
					device_vendorModel(Vender, vender);

					// Snmpro
					device_snmPro(snmpro);

					// Management Address dropdown
					device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

					// MEP Id
					device_mepID(Mepid);

					// Power Alarm
					device_powerAlarm(poweralarm);

					//// scrolltoend();
					waitforPagetobeenable();

					// Media Selection
					device_mediaSelection(Mediaselection, mediaSelection);

					// MAC Address
					device_MAcaddress(Macaddress);

					// Link lost Forwarding
					device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

					//// scrolltoend();
					waitforPagetobeenable();

					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();

				//	verifyExists(Lanlink_InternationalObj.International.warningmEssage1_devicename, "Device Name");
				//	verifyExists(Lanlink_InternationalObj.International.warningmEssage2_devicename, "Device Name");
				//	verifyExists(Lanlink_InternationalObj.International.warningmEssage3_devicename, "Device Name");

					// Name
					device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

					((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
					waitforPagetobeenable();
					click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

				//	sa.assertAll();

				} catch (AssertionError e) {

					e.printStackTrace();
					// Report.LogInfo("Info", "FAiled while verify the fields for Add
					// CPE device");

				}

			}
			
			
			public void device_nameField(String cpename, String expectedDeviceNameFieldAutopopulatedValue) {
				boolean name = false;
				try {
					name = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_Name);
				//	sa.assertTrue(name, "Name text field is not available under create device for Equipment");

					if (name) {
						if (cpename.equalsIgnoreCase("null")) {
							Reporter.log("No values has been assed for 'Name' text field");
							Report.LogInfo("Info",
									"No values has been passed for Mandatory 'Name' field under 'Add CPE Device' page", "FAIL");
						}

						else {
							String deviceNameActualPopulatedvalue = getAttributeFrom(
									Lanlink_InternationalObj.International.AddCPEdevice_Name, "value");
					//		sa.assertEquals(deviceNameActualPopulatedvalue, expectedDeviceNameFieldAutopopulatedValue,
								//	"Device Name field Auto Populated value is not displaying as expected");
							Report.LogInfo("Info", " Under 'Name' text field, value displaying by default is: "
									+ deviceNameActualPopulatedvalue, "PASS");
							clearTextBox(Lanlink_InternationalObj.International.AddCPEdevice_Name);
							waitforPagetobeenable();
							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_Name, cpename, "CPE name");
							waitforPagetobeenable();
							Report.LogInfo("Info", cpename + " is the value passed for Mandatory 'Cpe name' text field",
									"PASS");
						}
					} else {
						Report.LogInfo("Info", " Name text field is not displaying under 'Add CPE Device' page", "FAIL");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'Name' text field is not available", "FAIL");
				} catch (Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info", "Not able to enter value inside the 'Name' field", "FAIL");
				}

			}
			
			
			
			public void device_linklostForwarding(String linkLostForwarding, String state)
					throws InterruptedException, IOException {

				boolean linklostenable = false;
				boolean linklost = false;
				try {
					linklost = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding);
				//	sa.assertTrue(linklost, "Link lost Forwarding checkbox under add device is not available");
					if (linklost) {
						// Find whether it enabled or disabled
						linklostenable = isEnable(Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding);
						if (state.equalsIgnoreCase("disabled")) {
							if (linklostenable) {
							} else {
							}
						} else if (state.equalsIgnoreCase("enabled")) {
							if (linklostenable) {

								// select the checkbox as per input
								boolean linklostSelection = isSelected(
										Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding, "Link lost");
								if (linklostSelection) {

									// click on link lost checkbox
									if (linkLostForwarding.equalsIgnoreCase("Yes")) {

									} else {
										click(Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding);
										waitforPagetobeenable();
										Reporter.log(" Link Lost Forwarding is unselected as expected");
									}
								} else {
									Reporter.log(" 'link lostforwarding' is not selected by default");
								}

							} else {
								// Link lost Forwarding' checkbox is disabled under 'Add
								// CPE device' page
							}

						}
					} else {
						Reporter.log(" 'Link Lost Forwarding' checkbox is not displaying under 'Add CPE device' page");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				}
			}
			
			public void device_MAcaddress(String macAdressInput) {

				boolean macAdres = false;
				try {
					macAdres = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_macaddress);
				//	sa.assertTrue(macAdres, "Mepid field under 'Add CPE device' page is not available");

					if (macAdres) {
						Report.LogInfo("Info", " ' MAC Address' field is displaying in 'Add CPE Device' page as expected",
								"PASS");
						Reporter.log(" 'MAC Address'  text field is displaying as expected");

						if (macAdressInput.equalsIgnoreCase("null")) {
							Report.LogInfo("Info", "No values has been passed for 'MAC Address' text field for adding device",
									"FAIL");
							Reporter.log("No values has been passed for 'MAC Address' mandaotyr field");

						} else {

							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_macaddress, macAdressInput, "macAdressInput");
							waitforPagetobeenable();

							Report.LogInfo("Info", macAdressInput + " is entered under 'MAc Address' text field", "PASS");
							Reporter.log(macAdressInput + " is entered under 'MAc Address' text field");

						}
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'MAC Address' text field is not available", "FAIL");
				} catch (Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info", "Not able to enter value inside the 'MAC Address' text field", "FAIL");
				}
			}
			
			
			public void device_mediaSelection(String Mediaselection[], String mediaSelection)
					throws InterruptedException, IOException {

				addDropdownValues_commonMethod("Media Selection", Lanlink_InternationalObj.International.AddCPEdevice_mediaselection,
						mediaSelection);

			}
			
			public void device_powerAlarm(String poweralarm) throws InterruptedException, IOException {

				addDropdownValues_commonMethod("Power Alarm",Lanlink_InternationalObj.International.AddCPEdevice_poweralarm, poweralarm);

			}
			
			public void device_mepID(String Mepid) {

				String mepValue = "null";
				boolean mepid = false;
				try {
					mepid = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_mepid);

				//	sa.assertTrue(mepid, "Mepid field under 'Add CPE device' page is not available");

					if (mepid) {

						Reporter.log("MEP Id  text field is displaying as expected");

						mepValue = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEdevice_mepid, "value");
						if (mepValue.equalsIgnoreCase("null")) {
							Reporter.log(
									" No values are displaying under 'MEP ID' field. It should be auto populated by default");
						} else {
							Reporter.log(" MEP ID field is auto populated and it is displaying as : " + mepValue);
						}

					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}


		public void device_managementAddress(String existingmanagementAddress, String newmanagementAddress,
					String managementAddress) throws InterruptedException, IOException {

				boolean managementaddresdropdown = false;
				boolean manageAddresstextField = false;
				String manageAddresspopulatedValue = "null";

				if ((existingmanagementAddress.equalsIgnoreCase("Yes")) & (newmanagementAddress.equalsIgnoreCase("No"))) {
					try {
						managementaddresdropdown = isElementPresent(
								Lanlink_InternationalObj.International.AddCPEdevice_manageaddressdropdown);
				//		sa.assertTrue(managementaddresdropdown,
								//"Management Address dropdown under add device is not available");

						click(Lanlink_InternationalObj.International.AddCPEdevice_getSubnetbutton);
						waitforPagetobeenable();
						manageAddresspopulatedValue = getAttributeFrom(
								Lanlink_InternationalObj.International.AddCPEdevice_manageaddressdropdown, "value");
						if (manageAddresspopulatedValue.equalsIgnoreCase("null")) {
							// " No values gets populates in 'Manage Address' dropdown,
							// on clicking 'get Subnets' button
						} else {
							// Values displaying under 'Manage Addres' dropodwn is : "+
							// manageAddresspopulatedValue);
						}
						click(Lanlink_InternationalObj.International.AddCPEdevice_manageaddressdropdown);
						waitforPagetobeenable();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
					else if((existingmanagementAddress.equalsIgnoreCase("No")) & (newmanagementAddress.equalsIgnoreCase("Yes"))) {
			   			
			   			click(Lanlink_InternationalObj.International.changeButton_managementAddress, "change");	//click on change button for getting management address text field
			   			waitforPagetobeenable();
						
			   		try {	
			   			manageAddresstextField=isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_manageaddress);
			   		//	sa.assertTrue(manageAddresstextField, "Manage Address text Field is not displaying in 'Add CPE Device' page");
			   			if(manageAddresstextField) {
			   					if(managementAddress.equalsIgnoreCase("null")) {
			   						Report.LogInfo("Info", "No values has been passed for Mandatory field 'Manage Address' for adding device","FAIL");
			   					}else {
			   						
			   						
			   				sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_manageaddress,managementAddress,"management address"); 
			   				waitforPagetobeenable();
			   				Report.LogInfo("Info", managementAddress + " is the value passed for Mandatory 'Management Address' field in 'Add CPE Device' page","PASS");
			   					}
			   			}else {
			   				Report.LogInfo("Info", " 'Management Address' text field is not dipslaying under 'Add CPE device' page","FAIL");
			   			}
			   				
			   		}catch(NoSuchElementException e) {
			   			e.printStackTrace();
			   			Report.LogInfo("Info", " 'Manage Address' text field is not available in 'Add CPE Device' page","FAIL");
			   		}catch(Exception err) {
			   			err.printStackTrace();
			   			Report.LogInfo("Info", "Not able to enter value inside the 'Manage Address' field","FAIL");
			   		}
				}
			}
			
			public void device_snmPro(String snmproValueToBeChanged) throws IOException, InterruptedException {

				boolean sNmpro = false;
				try {

					sNmpro = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_snmpro);

					if (sNmpro) {
						Reporter.log("Smpro text field is displaying as expected");

						boolean actualValue_snmpro = isElementPresent(
								Lanlink_InternationalObj.International.CPEdevice_snmpro_autoPopulatedValue);

						if (snmproValueToBeChanged.equalsIgnoreCase("null")) {
							Reporter.log("No changes has been made to 'Snmpro' field");
							Reporter.log(" 'Snmpro' field is displaying as: "
									+ getAttributeFrom(Lanlink_InternationalObj.International.AddCPEdevice_snmpro, "value"));
						} else {
							clearTextBox(Lanlink_InternationalObj.International.AddCPEdevice_snmpro);
							waitforPagetobeenable();

							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_snmpro, snmproValueToBeChanged, "snmpro");
							waitforPagetobeenable();

						}

					} else {
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}

			}
			
			public void device_vendorModel(String[] Vender, String vender) throws InterruptedException, IOException {
				boolean vendor = false;
				// String Vender[]=(String) DataMiner.fngetcolvalue(testDataFile,
				// sheetName, scriptNo, dataSetNo, "Vender");
scrollUp();
				try {
					vendor = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_vender);
				//	sa.assertTrue(vendor, "Vender/Model dropdown is not available");

					click(Lanlink_InternationalObj.International.AddCPEdevice_vender,"AddCPEdevice");

					try {
						List<String> listofvender = new ArrayList<>(
								Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.ClassNameForDropdowns)));

						if (listofvender.size() > 0) {

							for (String vendertypes : listofvender) {

								boolean match = false;
								for (int i = 0; i < Vender.length; i++) {
									if (vendertypes.equals(Vender[i])) {
										match = true;
										Reporter.log("list of vendor under add devices are : " + vendertypes);
										Reporter.log("list of vendor under add devices are : " + vendertypes);

									}
								}
							//	sa.assertTrue(match, "");
							}

						} else {
							Reporter.log("dropdown value inside Vender/Model is empty");
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					// Entering value inside Vendor/Model dropdown
					try {
						if (vender.equalsIgnoreCase("null")) {

							Reporter.log("No values has been passed for Mandatory 'Vendor/Model' dropdown for adding device");

						} else {
							addDropdownValues_commonMethod("Vendor/Model",Lanlink_InternationalObj.International.AddCPEdevice_vender,vender);
							Reporter.log(
									vender + " is the value passed for Mandatory 'Vendor/Model' dropdown for adding device");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Reporter.log("vendor/Model dropdown is not dipslaying under 'Add CPE device' page");
				}
			}
			
			public void device_macAddressWarningMessage() throws InterruptedException, IOException {
				boolean macErr = false;
				try {
					macErr = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_macAdressErrmsg);
				//	sa.assertTrue(macErr, "MAC Address warning message is not displayed ");
					if (macErr) {
						String macadresErrMsg = getTextFrom(Lanlink_InternationalObj.International.AddCPEdevice_macAdressErrmsg);
						Reporter.log("MAC Address  message displayed as : " + macadresErrMsg);
						Report.LogInfo("Info", " validation message for MAC Address field displayed as : " + macadresErrMsg,
								"PASS");
						Reporter.log("MAC Address warning message displayed as : " + macadresErrMsg);
					} else {
					//	Report.LogInfo("Info", " 'MAC Address' warning message is not displaying under 'Add cpe device' page",
							//	"FAIL");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Reporter.log("Mac Adress warning message is not dipslaying");
					Report.LogInfo("Info", " MAC Address warning message is not displaying", "FAIL");
				} catch (Exception er) {
					er.printStackTrace();
				}
			}
			
			public void device_powerAlarmWarningMessage() throws InterruptedException {
				boolean pwralrmErr = false;
				try {
					pwralrmErr = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_powerAlarmerrmsg);
				//	sa.assertTrue(pwralrmErr, "Power Alarm warning message is not displayed ");
					if (pwralrmErr) {
						String pwralarmErrMsg = getTextFrom(Lanlink_InternationalObj.International.AddCPEdevice_powerAlarmerrmsg);
						Reporter.log("Power Alarm  message displayed as : " + pwralarmErrMsg);
						Reporter.log("Power Alarm warning message displayed as : " + pwralarmErrMsg);
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Reporter.log("Power Alarm warning message is not dipslaying");
				} catch (Exception er) {
					er.printStackTrace();
				}
			}
			
			public void device_managementAddressWarningMessage() throws InterruptedException, IOException {

				boolean mangadrsErr = false;
				try {
					mangadrsErr = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_managementAddresserrmsg);
				//	sa.assertTrue(mangadrsErr, "Management Addres warning message is not displayed ");
					if (mangadrsErr) {
						String mngadresErrMsg = getTextFrom(Lanlink_InternationalObj.International.AddCPEdevice_managementAddresserrmsg);
						Reporter.log("Management Addres  message displayed as : " + mngadresErrMsg);

						Reporter.log("Management Addres warning message displayed as : " + mngadresErrMsg);
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Reporter.log("management Address warning message is not found");
				} catch (Exception ed) {
					ed.printStackTrace();
				}
			}
			
			public void device_vendorModelWarningMessage() throws InterruptedException, IOException {
				boolean vendorErr = false;
				// Vendor/Model Error Message
				try {
					vendorErr = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_vendorErrmsg);
				//	sa.assertTrue(vendorErr, "Vendor/Model warning message is not displayed ");
					if (vendorErr) {
						String vendorErrMsg = getTextFrom(Lanlink_InternationalObj.International.AddCPEdevice_vendorErrmsg);
						Reporter.log("Vendor/Model  message displayed as : " + vendorErrMsg);
						Reporter.log("Vendor/Model warning message displayed as : " + vendorErrMsg);
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Reporter.log("Vendor/Mdel warning message is not dipslaying");
				} catch (Exception ed) {
					ed.printStackTrace();
				}
			}
			
			
			public void device_editnamefield(String cpename) {

				boolean name = false;
				try {

					name = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_Name);
					waitforPagetobeenable();

					if (name) {

						if (cpename.equalsIgnoreCase("null")) {

						} else {
							clearTextBox(Lanlink_InternationalObj.International.AddCPEdevice_Name);
							waitforPagetobeenable();

							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_Name, cpename, "CPE Name");
							waitforPagetobeenable();

							String actualValue_Name = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEdevice_Name, "value");
						}

					} else {
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
			
			public void device_editVendorModelField(String vender) {

				boolean vendor = false;
				try {

					vendor = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_vender);
					waitforPagetobeenable();
					if (vendor) {

						// 'Vendor/Model' dropdown is displaying in 'Edit CPE Device'
						// page

						if (vender.equalsIgnoreCase("null")) {
							// "No changes made for 'Vender/Model' dropdown while
							// editing

						} else {
							click(Lanlink_InternationalObj.International.EditCPEdevice_vendoModel_xbutton);
							waitforPagetobeenable();

							addDropdownValues_commonMethod("Vendor/Model",Lanlink_InternationalObj.International.AddCPEdevice_vender,vender);

						}
					} else {
						// 'Vendor/Model' dropdown is not available in 'Edit CPE Device'
						// page
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void device_editSnmproField() {

				boolean sNmpro = false;
				try {

					sNmpro = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_snmpro);

					if (sNmpro) {
						// ' Snmpro' field is displaying in 'Edit CPE Device' page as
						// expected

						boolean actualValue_snmpro = isElementPresent(
								Lanlink_InternationalObj.International.CPEdevice_snmpro_autoPopulatedValue);
						if (actualValue_snmpro) {
							// 'Snmpro' field value is displaying as expected

						} else {
							// 'Snmpro' value is not displaying as expected."
						}

					} else {
						// 'Snmpro' field is not available in 'Edit CPE Device' page
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
			
			public void device_editManagementAddressField(String managementAddress) {

				boolean manageAddressAvailability = false;
				try {

					// click(Lanlink_InternationalObj.International.AddCPEdevice_selectSubnettogglebutton);
					// waitforPagetobeenable();

					manageAddressAvailability = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_manageaddress);
					waitforPagetobeenable();

					if (manageAddressAvailability) {

						if (managementAddress.equalsIgnoreCase("null")) {

						} else {

							click(Lanlink_InternationalObj.International.changeButton_managementAddress);
							waitforPagetobeenable();

							clearTextBox(Lanlink_InternationalObj.International.AddCPEdevice_manageaddress);
							waitforPagetobeenable();
							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_manageaddress, managementAddress);
							waitforPagetobeenable();
						}

					}

				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
			
			public void device_editMEPIdField(String Mepid) {

				boolean mepIdavailability = false;
				try {
					mepIdavailability = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_mepid);
					if (mepIdavailability) {

						if (Mepid.equalsIgnoreCase("null")) {
						} else {
							clearTextBox(Lanlink_InternationalObj.International.AddCPEdevice_mepid);
							waitforPagetobeenable();
							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_mepid, Mepid, "MEP Id");
							waitforPagetobeenable();
						}
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
			
			public void device_editPowerAlarm(String poweralarm) throws InterruptedException, IOException {

				addDropdownValues_commonMethod("Power Alarm",Lanlink_InternationalObj.International.AddCPEdevice_poweralarm, poweralarm);

			}
			
			public void device_editserialnumber(String serialNumber) {

				boolean serialunmberAvailability = false;
				try {

					serialunmberAvailability = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_serialnumber);
					waitforPagetobeenable();

					if (serialunmberAvailability) {
						// 'Serial Number' field is displaying under 'Edit CPE device'
						// page

						if (serialNumber.equalsIgnoreCase("null")) {

							// "No changes made for 'Serial Number' field while editing

						} else {
							clearTextBox(Lanlink_InternationalObj.International.AddCPEdevice_serialnumber);
							waitforPagetobeenable();

							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_serialnumber, serialNumber, "Serial Number");
							waitforPagetobeenable();

							String actualValue_Name = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEdevice_serialnumber,
									"value");
						}

					} else {
						// 'Serial Number' field is not available under 'Edit CPE
						// device' page
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}
			
			public void device_editMediaselection(String Mediaselection) throws InterruptedException, IOException {

				addDropdownValues_commonMethod("Media Selection",Lanlink_InternationalObj.International.AddCPEdevice_mediaselection, Mediaselection);

			}
			
			
			public void device_editMACaddress(String Macaddress) throws IOException, InterruptedException {

				boolean macAddress = false;
				try {
					macAddress = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_macaddress);

					if (macAddress) {
						if (Macaddress.equalsIgnoreCase("null")) {
						} else {
							clearTextBox(Lanlink_InternationalObj.International.AddCPEdevice_macaddress);
							waitforPagetobeenable();
							sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_macaddress, Macaddress, "MAC Address");
							String actualValue_MacAddress = getAttributeFrom(
									Lanlink_InternationalObj.International.AddCPEdevice_macaddress, "value");
						}
					} else {
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception err) {
					err.printStackTrace();
				}
			}


		public void device_editlinkLostforwarding(String linkLostForwarding) {

				boolean linklostcheckbox = false;
				try {
					linklostcheckbox = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding);

					if (linklostcheckbox) {

						if (linkLostForwarding.equalsIgnoreCase("null")) {

							// No changes made for linklost forwarding while editing cpe
							// device under equipment
						} else {

							boolean linklost = isSelected(Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding,
									"linklostforowarding");

							if (linkLostForwarding.equalsIgnoreCase("yes")) {

								if (linklost) {
									// linklost forwarding has been edited for cpe
									// device under Equipment

								} else {

									click(Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding);
								}

							} else if (linkLostForwarding.equalsIgnoreCase("no")) {

								if (linklost) {
									click(Lanlink_InternationalObj.International.AddCPEdevice_linklostforowarding);

								} else {

									// linklost forwarding has been edited for cpe
									// device under Equipment
								}

							}
						}
					} else {
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
				} catch (Exception er) {
					er.printStackTrace();
				}
			}
			
		
		public void equip_adddevi_Accedian1G(String interfaceSpeed, String cpename, String vender, String snmpro,
				String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,
				String serialNumber, String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress,
				String existingmanagementAddress, String manageaddressdropdownvalue)
				throws InterruptedException, IOException {

			//// scrolltoend();
			waitforPagetobeenable();

			click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");

			try {

				String linklostForwardingcheckboxstate = "disabled";

				String[] Vender = { "Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX" };

				String[] powerAlarm = { "DC Single Power Supply - Feed A", "DC Dual Power Supply - Feed-A+B",
						"AC Single Power Supply - Feed A", "AC Dual Power Supply -Feed A+B" };

				String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

				String expectedValueForSnmpro = "JdhquA5";

				// Vendor/Model Error Message
				device_vendorModelWarningMessage();

				// Management Address Error Message
				device_managementAddressWarningMessage();

				// Power Alarm Error Message
				device_powerAlarmWarningMessage();

				// serial number Eror Message
				device_serialNumberWarningMessage();

				// Hexa serial Number
				device_hexaSerialNumberWarningMessage();

				// Vendor/Model
				device_vendorModel(Vender, vender);

				// Snmpro
				device_snmPro(snmpro);

				// Management Address dropdown
				device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

				// MEP Id
				device_mepID(Mepid);

				// Power Alarm
				device_powerAlarm(poweralarm);

				// Serial Number
				device_serialNumber(serialNumber);

				// Link lost Forwarding
				device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

				waitforPagetobeenable();

				click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

				((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

				waitforPagetobeenable();

				verifyExists(Lanlink_InternationalObj.International.warningmEssage1_devicename, "Device Name");
				verifyExists(Lanlink_InternationalObj.International.warningmEssage2_devicename, "Device Name");
				verifyExists(Lanlink_InternationalObj.International.warningmEssage3_devicename, "Device Name");

				// Name
				device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

				((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
				waitforPagetobeenable();
				click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

			//	sa.assertAll();

			} catch (AssertionError e) {

				e.printStackTrace();
				// Report.LogInfo("Info", "FAiled while verify the fields for Add
				// CPE device");

			}

		}
			
			
			
		public void device_serialNumberWarningMessage() throws InterruptedException, IOException {
			// Serial Number Error Message
			boolean serialNumberErr = false;

			try {
				serialNumberErr = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_serialNumberErrmsg);
			//	sa.assertTrue(serialNumberErr, "Serial Number warning message is not displayed ");
				if (serialNumberErr) {
					Reporter.log(" 'Serial number; warning message is dipslaying as expected");
					String serialnumberErrMsg = getTextFrom(Lanlink_InternationalObj.International.AddCPEdevice_serialNumberErrmsg);
					Reporter.log("Serial Number  message displayed as : " + serialnumberErrMsg);
					Reporter.log("Serial Number warning message displayed as : " + serialnumberErrMsg);
				} else {
					Reporter.log("Serial Number warning message is not dipslaying");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Reporter.log("Serial Number Warning message is not diplsying");
			}
		}
		
		
		public void device_hexaSerialNumberWarningMessage() throws InterruptedException, IOException {
			// Serial Number Error Message
			boolean HexaserialNumberErr = false;

			try {
				HexaserialNumberErr = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_hexaSerialNumnerErrmsg);
		//		sa.assertTrue(HexaserialNumberErr, "Hexa Serial Number warning message is not displayed ");
				if (HexaserialNumberErr) {
					Reporter.log(" 'Hexa Serial number' warning message is dipslaying as expected");
					String hexaserialnumberErrMsg = getTextFrom(
							Lanlink_InternationalObj.International.AddCPEdevice_hexaSerialNumnerErrmsg);
					Reporter.log("Hexa Serial Number  message displayed as : " + hexaserialnumberErrMsg);
					Reporter.log("Hexa Serial Number warning message displayed as : " + hexaserialnumberErrMsg);
				} else {
					Reporter.log("Hexa Serial Number warning message is not dipslaying");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Reporter.log("Serial Number Warning message is not diplsying");
			}
		}

		public void device_serialNumber(String serialNumber) throws InterruptedException, IOException, IOException {

			// Serial Number
			boolean serialNmber = false;
			try {
				serialNmber = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_serialnumber);
			//	sa.assertTrue(serialNmber, "Serial Number is not available in 'Add CPE Device' page");
				if (serialNmber) {
					if (serialNumber.equalsIgnoreCase("null")) {
					} else {

						sendKeys(Lanlink_InternationalObj.International.AddCPEdevice_serialnumber, serialNumber, "Serial Number");
						waitforPagetobeenable();
						String SNactualValue = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEdevice_serialnumber,
								"value");
					}
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Reporter.log(" 'Serial Number' text field is not displaying");
			} catch (Exception er) {
				er.printStackTrace();
				Reporter.log("not able to enter value under 'Serial number' textfield");
			}
		}

			
		public void selectTechnology_HubAndSpoke() throws InterruptedException, IOException {

			// verify Technology popup
			boolean technologypopup = false;
			technologypopup = isElementPresent(Lanlink_InternationalObj.International.technologyPopup);
			if (technologypopup) {
				Reporter.log("Technology popup is displaying as expected");
			} else {
				Reporter.log("Technology popup is not displaying");
			}

			// Dropdown values inside popup
			boolean technologyDropdown = false;
			technologyDropdown = isElementPresent(Lanlink_InternationalObj.International.technologypopup_dropdown);
			if (technologyDropdown) {
				Reporter.log("Technology dropdown is displaying as expected");

				click(Lanlink_InternationalObj.International.technologypopup_dropdown);
				waitforPagetobeenable();

				// verify list of values inside technology dropdown
				List<String> listofTechnololgy = new ArrayList<>(
						Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.ClassNameForDropdowns)));

				if (listofTechnololgy.size() > 0) {

					for (String technoloyTypes : listofTechnololgy) {
						Reporter.log("List of values available under 'Technology' dropdown are: " + technoloyTypes);
					}
				}

				// Select the Technology
				webDriver.findElement(By.xpath("//div[@class='modal-body']//div[contains(text(),'Overture')]")).click();
				waitforPagetobeenable();
				String actualValue = getTextFrom(Lanlink_InternationalObj.International.tchnologyPopup_dropdownValues);
				Reporter.log(" 'Technology' selected is: " + actualValue);

			} else {
				Reporter.log("Technology dropdown is not displaying");
			}

			click(Lanlink_InternationalObj.International.IntermediateEquipment_OKbuttonforpopup);
			waitforPagetobeenable();

		}
			
		
		public void Equip_clickonviewButton(String devicename) throws InterruptedException, IOException {

			clickOnBankPage();
			scrollIntoTop();
			waitforPagetobeenable();
			boolean viewpage = false;
			try {
				viewpage = isElementPresent(Lanlink_InternationalObj.International.viewCPEdevicepage_devices);

				if (viewpage) {
					Reporter.log("In view page");
				} else {

					////// scrolltoend();
					waitforPagetobeenable();

					webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='View']")).click();

					waitforPagetobeenable();
				}
			} catch (Exception e) {
				e.printStackTrace();

				////// scrolltoend();
				waitforPagetobeenable();

				webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='View']")).click();
				waitforPagetobeenable();
			}

		}
		
		
		
		public String fetchdevicename_InviewPage()throws InterruptedException, IOException {

			scrollUp();
			waitforPagetobeenable();

			WebElement devicename = findWebElement(Lanlink_InternationalObj.International.fetchDeviceValue);
			String devieName = getTextFrom(Lanlink_InternationalObj.International.fetchDeviceValue);
			Reporter.log("device name  "+ devicename);

			return devieName;
		}
		
		
		public String fetchManagementAddressValue_InviewPage()throws InterruptedException, IOException {

			scrollUp();
			waitforPagetobeenable();

			WebElement manageAdres = findWebElement(Lanlink_InternationalObj.International.fetchmanagementAddressvalue);
			String managementAddress = getTextFrom(Lanlink_InternationalObj.International.fetchmanagementAddressvalue);

			return managementAddress;
		}

public String fetchVendorModelValue(String application) throws InterruptedException, IOException {
			
	         scrollUp();
			waitforPagetobeenable();
			WebElement vendorModel=findWebElement(Lanlink_InternationalObj.International.fetchVendorModelvalue);
			String vendorValue=getTextFrom(Lanlink_InternationalObj.International.fetchVendorModelvalue);
			
			Reporter.log("vendor value "+vendorValue);
			return vendorValue;
		}

public void testStatus() throws IOException {

	String element = null;
	String status = null;

	List<WebElement> testlist=webDriver.findElements(By.xpath("//tbody/tr"));
	int listSize = testlist.size();

	for (int i = 1; i <= listSize; i++) {
		try {
			element = getTextFrom(Lanlink_InternationalObj.International.testlist1 + i + Lanlink_InternationalObj.International.testlist2);

			if (element.isEmpty()) {

			} else {
				Reporter.log("Test Name is displaying as: " + element);

				status = getAttributeFrom(Lanlink_InternationalObj.International.testlist1 + i + Lanlink_InternationalObj.International.testlist3, "class");
				Reporter.log("status displays as: " + status);

				if (status.contains("red")) {
					Reporter.log(element + " status colour dipslays as: red");
				} else if (status.contains("green")) {
					Reporter.log(element + " status colour dipslays as: green");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
	      	 
public boolean fetchDeviceInterface_viewdevicepage(String devicename) throws IOException, InterruptedException {

	boolean clickLink = false;

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.viewPCEdevice_Actiondropdown, "Action");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.fetchDeviceinterfacelink_viewDevicePage, "fetchDeviceInterfaceLink");

	waitforPagetobeenable();
	waitToPageLoad();

	// verify success Message
	String expectedValue = "Fetch interfaces started successfully. Please check the sync status of this device";
	boolean successMessage = false;
	try {
		successMessage = isElementPresent(Lanlink_InternationalObj.International.fetchDeviceInterace_SuccessMessage);
		String actualMessage = getTextFrom(Lanlink_InternationalObj.International.fetchdeviceInterface_successtextMessage);
		if (successMessage) {

			if (actualMessage.isEmpty()) {
				Reporter.log("No messages displays");
			}
			if (actualMessage.contains(expectedValue)) {

				Reporter.log(
						" After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected");

				Reporter.log(" Success Message displays as: " + actualMessage);

				// click on the 'click here' link
				// click((Lanlink_InternationalObj.International.ClickhereLink_fetchInterface);
				// waitforPagetobeenable();

				clickLink = true;

			} else if (actualMessage.equalsIgnoreCase(expectedValue)) {

				Reporter.log(
						" After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected");

				Reporter.log(" Success Message displays as: " + actualMessage);

				// click on the 'click here' link
				// click(Lanlink_InternationalObj.International.ClickhereLink_fetchInterface);
				// waitforPagetobeenable();

				clickLink = true;
			} else {
				Reporter.log(
						"After clicking on 'Fetch Device Interface' link, message displays as " + actualMessage);
			}

		}
	} catch (Exception e) {
		e.printStackTrace();
		Reporter.log("After clicking on 'Fetch Device Interface' link, success message is not displaying");

	}
	return clickLink;
}


public void routerPanel(String commandIPv4,String ipAddress)
		throws InterruptedException, IOException {
	

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	String vendorModel = getTextFrom(Lanlink_InternationalObj.International.fetchVendorModelvalue);
	String vendorValue = vendorModel;

	if (vendorValue.startsWith("Overture")) {

		scrollDown(Lanlink_InternationalObj.International.commandIPV4_dropdown);
		waitforPagetobeenable();

		// Command IPV4
		addDropdownValues_commonMethod("Command IPV4",Lanlink_InternationalObj.International.commandIPV4_dropdown, commandIPv4 );

		hostnametextField_IPV4(commandIPv4, ipAddress);

		executeCommandAndFetchTheValue(Lanlink_InternationalObj.International.executebutton_Ipv4);
	} else {
		// 'Router' panel displays only for Overture device
	}
}


public void hostnametextField_IPV4(String command_ipv4, String ipAddress) throws IOException, InterruptedException {

	boolean IPV4availability = false;
	try {
		IPV4availability = isElementPresent(Lanlink_InternationalObj.International.commandIPv4_hostnameTextfield);

		if (IPV4availability) {

			sendKeys(Lanlink_InternationalObj.International.commandIPv4_hostnameTextfield, ipAddress,
					"IP Address or Hostname");

		} else {
			System.out
					.println("'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4);
		}
	} catch (Exception e) {
		e.printStackTrace();
		Reporter.log("'Hostname or IpAddress' for 'Ipv4' text field is not displaying for " + command_ipv4);
	}
}


public void executeCommandAndFetchTheValue(String executeButton) throws IOException {

	click(Lanlink_InternationalObj.International.executebutton_Ipv4, "Execute");

	boolean remarkField = false;
	try {
		remarkField = isElementPresent(Lanlink_InternationalObj.International.result_textArea);
		if (remarkField) {
			Reporter.log("'Remark' text field is displaying");

			String remarkvalue = getTextFrom(Lanlink_InternationalObj.International.result_textArea);
			Reporter.log("value under 'Remark' field displaying as " + remarkvalue);

		} else {
			Reporter.log("'Remark' text field is not displaying");
		}
	} catch (Exception e) {
		e.printStackTrace();
		Reporter.log("'Remark' text field is not displaying");
	}
}

public void hideInterfaceLink_Equipment() throws IOException, InterruptedException {

	click(Lanlink_InternationalObj.International.CPEdevice_hideInterfaeLink_eqiupment,"HideInterfaeLink");
	waitforPagetobeenable();
}

public void clickOnBreadCrump(String breadCrumpLink) throws IOException, InterruptedException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	String breadcrumb = null;

	try {
		breadcrumb = (Lanlink_InternationalObj.International.breadcrump).replace("value", breadCrumpLink);

		if (isElementPresent(breadcrumb)) {
			click(breadcrumb,"breadcrumb");
			waitforPagetobeenable();
			waitToPageLoad();
		} else {
			Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
		}
	} catch (Exception e) {
		e.printStackTrace();
		Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
	}
}


public void selectconfigurelinkAndverifyEditInterfacefield__Equipment(String devicename) throws IOException, InterruptedException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='Configure']")).click();

	waitforPagetobeenable();

	scrollDown(Lanlink_InternationalObj.International.Equipment_configureActiondropdown);
	
	waitforPagetobeenable();

	// Try to edit without selecting the interface
	click(Lanlink_InternationalObj.International.Equipment_configureActiondropdown,"Equipment ConfigureActiondropdown");
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.Equipment_configureEditlink,"Equipment ConfigureEditlink");
	waitforPagetobeenable();

	// check whether Alert message displays
	boolean alertMessage = false;
	try {
		alertMessage = isElementPresent(Lanlink_InternationalObj.International.configure_alertPopup);
		if (alertMessage) {
			Reporter.log("Alert popup displays, if we click on 'Edit' without selected the interface");

			String alertMsg = getTextFrom(Lanlink_InternationalObj.International.configure_alertMessage);
			Reporter.log(" Alert message displays as: " + alertMsg);

			click(Lanlink_InternationalObj.International.configure_alertPopup_xbutton,"configure_alertPopup_xbutton");
			waitforPagetobeenable();
		} else {
			Reporter.log("'Alert' popup is not displaying when we click on 'edit' without selecting an interface");

			boolean editInterface_popuptitleName = false;
			try {
				editInterface_popuptitleName = isElementPresent(
						Lanlink_InternationalObj.International.Editinterface_popupTitlename);
				if (editInterface_popuptitleName) {
					Reporter.log("Edit interface popup is displaying without selected an interface");

					click(Lanlink_InternationalObj.International.EditInterfacepopup_xbutton,"EditInterfacepopup_xbutton");
					waitForAjax();
					waitforPagetobeenable();
				}
			} catch (Exception e) {
				e.printStackTrace();
				Reporter.log(" 'Edit interface' popup is not displaying, without selecting an Interface");
			}

		}
	} catch (NoSuchElementException e) {
		Reporter.log("'Alert' popup is not displaying when we click on 'edit' without selecting an interface");

	} catch (Exception ee) {
		ee.printStackTrace();
		Reporter.log("'Alert' popup is not displaying when we click on 'edit' without selecting an interface");
	}

}

public void verifyeditedinterfacevalue(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String interfaceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

	List<String> interfacedetails = new ArrayList<>(
			Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.interfacedetails1 + interfaceName
					+ Lanlink_InternationalObj.International.interfacedetails2)));

	for (String listOfInterfacenames : interfacedetails) {

		String ColumnNames = getAttributeFrom(Lanlink_InternationalObj.International.interfacedetails1 + interfaceName
				+ Lanlink_InternationalObj.International.interfacedetails2, "col-id");
		String values = listOfInterfacenames;

		Reporter.log("After Editing interface, list of values displaying in interface table are: ");
		Reporter.log("value displaying for " + ColumnNames + " is: " + values);
		
		

	}
}


public void selectInterfacelinkforEqipment(String devicename) throws IOException, InterruptedException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//div//span[text()='Select Interfaces']")).click();
	Reporter.log("SelectInterface link for Equipment is selected");
	Reporter.log("Select an inertface to add with the service under equipment");

	waitforPagetobeenable();
}

public void SelectInterfacetoremovefromservice(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws IOException, InterruptedException {
	String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Interfaceinservice_Interfacenumber");

	scrollDown(Lanlink_InternationalObj.International.labelname_managementAddresss);
	
	waitforPagetobeenable();

	selectRowforInterfaceInService(interfacename);

}

public void SelectInterfacetoaddwithservcie(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo)

		throws InterruptedException, IOException {

	String interfacenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Interfacetoselect_Interfacenumber");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitforPagetobeenable();

	selectrowforInterfaceToselecttable(interfacenumber);

}

public void EnterdataForEditInterfaceforShowInterfacelinkunderEquipment(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerType");
	String bearerspeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerSpeed");
	String totalbandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"editInterfacepage_bandwidth");
	String vlantype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_vlantype");
	String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_Vlanid");
	String circuitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_circuitId");
	String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

	
	// verify Edit Interface page
	boolean editInterface_popuptitleName = false;
	try {
		editInterface_popuptitleName = isElementPresent(Lanlink_InternationalObj.International.Editinterface_popupTitlename);
		if (editInterface_popuptitleName) {
			Reporter.log("Edit interface popup is displaying as expected");

			
			// Interface name
			verifyenteredvaluesForEditPage_noneditableFields("Interface", interfacename);

			// Circuit ID
			configure_circuitId(circuitId);

			// Bearer type
			addDropdownValues_commonMethod("Bearer Type",Lanlink_InternationalObj.International.Equipment_configureBearerType, bearertype_value);

			// Bearerspeed
			addDropdownValues_commonMethod("Bearer Speed",Lanlink_InternationalObj.International.Equipment_configureBearerSpeed, bearerspeed);

			// Total Circuit bandwidth
			addDropdownValues_commonMethod("Total Circuit Bandwidth",Lanlink_InternationalObj.International.Equipment_configureTotalcircuitbandwidth, totalbandwidth);

			// VLAN type
			addDropdownValues_commonMethod("VLAN Type",Lanlink_InternationalObj.International.Equipment_configureVlanType, vlantype);

			// vlan
			sendKeys(Lanlink_InternationalObj.International.Equipment_configureVLANid, vlanid, "VLAN Id");
			waitforPagetobeenable();

		}
	} catch (NoSuchElementException e) {
		e.printStackTrace();
		Reporter.log(" 'Edit interface' popup is not displaying");
	} catch (Exception ee) {
		ee.printStackTrace();
		Reporter.log(" 'Edit interface' popup is not displaying");
	}

	click(Lanlink_InternationalObj.International.okbutton, "OK");
	waitforPagetobeenable();

}

public String fetchCSRsiteName() throws IOException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	String CSrname = getTextFrom(Lanlink_InternationalObj.International.physicalSiteCSRName_viewSiteOrder);
	String CSRname = CSrname;

	return CSRname;
}

public String fetchDeviceCityName() throws IOException, InterruptedException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	String cityName = getTextFrom(Lanlink_InternationalObj.International.deviceCityName_viewSiteOrder);
	String dveiceCityName = cityName;

	return dveiceCityName;
}

public String fetchSiteOrderCountryName() throws IOException {

	String countryName = getTextFrom(Lanlink_InternationalObj.International.countryName_viewSiteOrder);
	String siteOrder_countryName = countryName;

	return siteOrder_countryName;
}


public void clickOnAMNvalidatorLink() throws InterruptedException, IOException {
	
	scrollUp();
	waitforPagetobeenable();
	
	click(Lanlink_InternationalObj.International.siteOrder_ActionButton, "Action");   //click on Action button
	
	click(Lanlink_InternationalObj.International.AMNvalidatorlink, "AMNvalidator");
}

public void AMNvalidator(String siteOrderNumber ,String  devicename_Equip,String  csrName,String  cityName,String  countryName)
		throws InterruptedException, IOException {
		boolean AMNvalidatorPanelHeader = false;

	waitforPagetobeenable();

	waitToPageLoad();

	waitforPagetobeenable();

	waitforPagetobeenable();

	String[] statusColumnNamesExpected = { "Site Order", "CSR Site Name", "Device Country", "Device Xing City",
			"Smarts" };
	String[] deviceColumnNamesExpected = { "Device", "Smarts", "Fetch Interfaces" };

	List<String> ls = new ArrayList<String>();
	List<String> ls1 = new ArrayList<String>();

	try {
		AMNvalidatorPanelHeader = isElementPresent(Lanlink_InternationalObj.International.AMNvalidator_panelHeader);

		if (AMNvalidatorPanelHeader) {
			Report.LogInfo("Info", "'AMN Validator' panel is displaying", "PASS");
			Reporter.log("'AMN Validator' panel is displaying");

			// check Status Panel column Header
			List<String> status_PanelHeaders = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.AMNvalidator_status_columnHeader)));
			for (String panels : status_PanelHeaders) {

				boolean match = false;
				for (int i = 0; i < statusColumnNamesExpected.length; i++) {
					if (panels.equals(statusColumnNamesExpected[i])) {
						match = true;
						ls.add(panels);
					} else {
						// Report.LogInfo("Info", "Under 'Site Orders'
						// column, Column name for " + panels.getText() + "
						// is not displaying","FAIL");
						Reporter.log(
								"Under 'Site Orders' column, Column name for " + panels + " is not displaying");
					}
				}
			}

			Report.LogInfo("Info", "Under 'Site Orders' Panel, list of column names dipslaying are: " + ls, "PASS");
			Reporter.log("Under 'Site Orders' Panel, list of column names dipslaying are: " + ls);

			// verify values under "Status" panel
			compareText("Site Order", Lanlink_InternationalObj.International.AMNvalidator_statusPanel_siteOrderColumnValue,
					siteOrderNumber); // Site Order Column Name
			compareText("CSR Site Name",
					Lanlink_InternationalObj.International.AMNvalidator_statusPanel_CSRsiteNameColumnValue, csrName); // CSR
																												// Site
																												// Name
			compareText("Device Xng City", Lanlink_InternationalObj.International.AMNvalidator_statusPanel_deviceXngCityName,
					cityName); // Device XN City Name
			compareText("Device Country", Lanlink_InternationalObj.International.AMNvalidator_statusPanel_countryName,
					countryName); // CountryName

			// scrolltoend();
			waitforPagetobeenable();
			// check Device panel column Header
			List<String> device_PanelHeaders = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.AMNvalidator_device_columnHeader)));
			for (String devicepanels : device_PanelHeaders) {

				boolean match = false;
				for (int i = 0; i < deviceColumnNamesExpected.length; i++) {
					if (devicepanels.equals(deviceColumnNamesExpected[i])) {
						match = true;
						ls1.add(devicepanels);

					} else {
						Reporter.log("Under 'Devices For Services' panel, Column name for " + devicepanels
								+ " is not displaying");
					}
				}
			}

			Report.LogInfo("Info", "Under 'Devices For Service' Panel, List Of column Names dispaying are: " + ls1,
					"PASS");
			Reporter.log("Under 'Devices For Service' Panel, List Of column Names dispaying are: " + ls1);

			// verify vales under 'device' panel
	//		compareText("DeviceValue", Lanlink_InternationalObj.International.AMNvalidator_devicePanel_deviceName, devicename_Equip);

		} else {
			Report.LogInfo("Info", "'AMN Validator' panel is not displaying", "PASS");
			Reporter.log("'AMN Validator' panel is not displaying");
		}
	} catch (Exception e) {
		e.printStackTrace();
		Report.LogInfo("Info", "'AMN Validator' panel is not displaying", "PASS");
		Reporter.log("'AMN Validator' panel is not displaying");
	}
}


public void deleteDeviceFromServiceForequipment(String devicename) throws IOException, InterruptedException {
	

	webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='Delete']")).click();;

	Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Equipment");
	Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Equipment");

	boolean deletemessage = isElementPresent(getTextFrom(Lanlink_InternationalObj.International.deleteMessage_equipment));
	while (deletemessage) {
		Reporter.log(
				"Are you sure want to delete - message is getting displayed on clicking DeletefromService link");
		Reporter.log("Delete popup message is getting displayed");
		String actualMessage = getTextFrom(Lanlink_InternationalObj.International.deleteMessage_equipment);
		Reporter.log("Delete device popup is displaying and popup message displays as: " + actualMessage);
		break;
	}

		click(Lanlink_InternationalObj.International.deletebutton_deletefromsrviceforequipment,"deletebutton");
		waitforPagetobeenable();
		Reporter.log("Device got deleted from service as expected");

	
}

public void successMessage_deleteFromService() throws IOException, InterruptedException {

	verifysuccessmessage("Device successfully deleted");
}

public void selectrowforInterfaceToselecttable(String interfacenumber) throws IOException, InterruptedException {

	int TotalPages;

	String TextKeyword = getTextFrom(Lanlink_InternationalObj.International.InterfaceToselect_totalpage,
			"Interface Toselect totalpage");

	TotalPages = Integer.parseInt(TextKeyword);

	Reporter.log("Total number of pages in Interface to select table is: " + TotalPages);

	ab:

	if (TotalPages != 0) {
		for (int k = 1; k <= TotalPages; k++) {

			// Current page
			String CurrentPage = getTextFrom(Lanlink_InternationalObj.International.InterfaceToselect_currentpage,
					"Interface Toselect currentpage");
			int Current_page = Integer.parseInt(CurrentPage);

		//	sa.assertEquals(k, Current_page);

			Reporter.log("Currently we are in page number: " + Current_page);

			List<WebElement> results = webDriver.findElements(By.xpath("//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='"+ interfacenumber  +"']]//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
			
			int numofrows = results.size();
			Reporter.log("no of results: " + numofrows);
			boolean resultflag;

			if (numofrows == 0) {

				PageNavigation_NextPageForInterfaceToselect();

			}

			else {

				for (int i = 0; i < numofrows; i++) {

					try {

						resultflag = results.get(i).isDisplayed();
						Reporter.log("status of result: " + resultflag);
						if (resultflag) {
							Reporter.log(results.get(i).getText());
							results.get(i).click();
									Report.LogInfo("Info",
									interfacenumber + " is selected under 'Interface to select' table", "PASS");
							waitforPagetobeenable();
							click(Lanlink_InternationalObj.International.InterfaceToselect_Actiondropdown,
									"Interface Toselect Actiondropdown");

							waitforPagetobeenable();

							click(Lanlink_InternationalObj.International.InterfaceToselect_addbuton,
									"Interface Toselect addbuton");
							waitforPagetobeenable();
							Report.LogInfo("Info", interfacenumber + " is added to service", "PASS");

						}

					} catch (StaleElementReferenceException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						results = webDriver.findElements(By.xpath("(//div[@class='row'])[8]//div[div[contains(text(),'"
								+ interfacenumber + "')]]//input"));
						numofrows = results.size();
						// results.get(i).click();
						Reporter.log("selected row is : " + i);
						Report.LogInfo("Info", "Failure on selecting an Interface to ad with service ", "FAIL");

					}

				}

				break ab;

			}

		}

	} else {

		Reporter.log("No values found inside the table");
		Reporter.log("No values available inside the Interfacetoselect table");
	}

}

public void PageNavigation_NextPageForInterfaceToselect() throws InterruptedException, IOException {

	click(Lanlink_InternationalObj.International.InterfaceToselect_nextpage, "Interface Toselect nextpage");
	waitforPagetobeenable();

}

public void configure_circuitId(String circuitId) throws InterruptedException {

	try {
		if (circuitId.equalsIgnoreCase("null")) {

			Reporter.log(" No input provided for 'Circuit ID' field");

		} else {
			click(Lanlink_InternationalObj.International.selectCircuit_togglebutton);
			waitforPagetobeenable();

			sendKeys(Lanlink_InternationalObj.International.Equipment_configureXNGCircuitID, circuitId, "Circuit Id");

			String actualvalue = getAttributeFrom(Lanlink_InternationalObj.International.Equipment_configureXNGCircuitID,
					"value");
		}
	} catch (NoSuchElementException e) {
		e.printStackTrace();
	} catch (Exception err) {
		err.printStackTrace();
	}
	waitforPagetobeenable();
}


public void verifyenteredvaluesForEditPage_noneditableFields(String labelname, String ExpectedValue) throws InterruptedException, IOException {
	boolean actualvalue = false;
	try {

		actualvalue=webDriver.findElement(By.xpath("//div[div[label[text()='" + labelname + "']]]//div[text()='"+ ExpectedValue +"']")).isDisplayed();
		if (actualvalue) {

			Reporter.log("Value is displaying as expected for the field: " + labelname);

		} else {

			Reporter.log("Value is not displaying as expected for the text field: " + labelname);
		}
	} catch (NoSuchElementException e) {
		e.printStackTrace();
	}

}
public void selectRowforInterfaceInService(String interfacenumber) throws IOException, InterruptedException {

	int TotalPages;

	String TextKeyword = getTextFrom(Lanlink_InternationalObj.International.InterfaceInselect_totalpage);

	TotalPages = Integer.parseInt(TextKeyword);

	Reporter.log("Total number of pages in table is: " + TotalPages);

	ab:

	if (TotalPages != 0) {
		for (int k = 1; k <= TotalPages; k++) {

			// Current page
			String CurrentPage = getTextFrom(Lanlink_InternationalObj.International.InterfaceInselect_currentpage);
			int Current_page = Integer.parseInt(CurrentPage);

	//		sa.assertEquals(k, Current_page);

			Reporter.log("Currently we are in page number: " + Current_page);
			List<WebElement> results = webDriver.findElements(By.xpath("//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='" + interfacenumber +"']]//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
			
			int numofrows = results.size();
			Reporter.log("no of results: " + numofrows);
			boolean resultflag;

			if (numofrows == 0) {

				PageNavigation_NextPageForInterfaceInService();

			}

			else {

				for (int i = 0; i < numofrows; i++) {

					try {

						resultflag = results.get(i).isDisplayed();
						Reporter.log("status of result: " + resultflag);
						if (resultflag) {
							Reporter.log(results.get(i).getText());
							results.get(i).click();
							Report.LogInfo("Info", interfacenumber + " is selected under 'Interface in Service' table","PASS");
								click(Lanlink_InternationalObj.International.InterfaceInselect_Actiondropdown,
									"Interface Inselect Actiondropdown");

							waitforPagetobeenable();

							click(Lanlink_InternationalObj.International.InterfaceInselect_removebuton,
									"Interface Inselect removebuton");
							Report.LogInfo("Info",
									interfacenumber + " has been selected to get removed from service", "PASS");

						}

					} catch (StaleElementReferenceException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						results = webDriver.findElements(By.xpath("//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='"+ interfacenumber+"']]//input"));
						numofrows = results.size();
						// results.get(i).click();
					
						Reporter.log("selected row is : " + i);
						Report.LogInfo("Info", "failure while selecting interface to remove from service", "FAIL");

					}

				}

				break ab;

			}

		}

	} else {

		Reporter.log("No values available in table");
		Reporter.log("No values available inside the InterfaceInService table");
	}

}


public void PageNavigation_NextPageForInterfaceInService() throws InterruptedException, IOException {

	click(Lanlink_InternationalObj.International.InterfaceInselect_nextpage, "Interface Inselect nextpage");
	waitforPagetobeenable();

}

public void addDevice_IntEquipment() throws IOException, InterruptedException
{

//click on Add device link	
	verifyExists(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_adddevicelink,"AddCPEforIntermediateEquip_adddevicelink");

click(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_adddevicelink,"AddCPEforIntermediateEquip_adddevicelink");
waitforPagetobeenable();
}

public void selectTechnology(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
String technologyToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");


//verify Technology popup
boolean technologypopup=false;
technologypopup=isElementPresent(Lanlink_InternationalObj.International.technologyPopup);
if(technologypopup) 
{
Reporter.log("Technology popup is displaying as expected");
}
else 
{
Reporter.log("Technology popup is not displaying");
}

//Dropdown values inside popup
boolean technologyDropdown=false;
technologyDropdown=isElementPresent(Lanlink_InternationalObj.International.technologypopup_dropdown);
if(technologyDropdown) {
Reporter.log("Technology dropdown is displaying as expected");

click(Lanlink_InternationalObj.International.technologypopup_dropdown);
waitforPagetobeenable();

//verify list of values inside technology dropdown
List<String> listofTechnololgy = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.ClassNameForDropdowns)));

if(listofTechnololgy.size()>0) {

for (String technoloyTypes : listofTechnololgy) {
	Reporter.log("List of values available under 'Technology' dropdown are: "+technoloyTypes);
}
}


//Select the Technology
webDriver.findElement(By.xpath("//div[@class='modal-body']//div[contains(text(),'"+ technologyToBeSelected +"')]")).click();
waitforPagetobeenable();
String actualValue=getTextFrom(Lanlink_InternationalObj.International.tchnologyPopup_dropdownValues);
Reporter.log( " 'Technology' selected is: "+actualValue);

}else {
Reporter.log("Technology dropdown is not displaying");
}

verifyExists(Lanlink_InternationalObj.International.IntermediateEquipment_OKbuttonforpopup,"IntermediateEquipment_OKbuttonforpopup");

click(Lanlink_InternationalObj.International.IntermediateEquipment_OKbuttonforpopup,"IntermediateEquipment_OKbuttonforpopup");
waitforPagetobeenable();		
}


public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(String testDataFile,
		String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {

	String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
	String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
	String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
	String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
	String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
	String technologySelectedfordevicecreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
	String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
	String poweralarm_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_overture");
	String powerAlarm_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_Accedian");
	String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
	String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
	String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
	String vender_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Overture");
	String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Accedian");
	String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"device_intequip_manageaddress_dropdownvalue");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"device_intequip_existingmanagementAddress_selection");
	String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
	String citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
	String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
	String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
	String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
	String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
	String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
	String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
	String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
	String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
	String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
	String technology_siteorder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");


		
		if(technologySelectedfordevicecreation.equalsIgnoreCase("Overture")) {
			deviceCreatoin_Overture(cpename, vender_Overture, snmpro, managementAddress, Mepid, poweralarm_Overture, MediaselectionActualValue,
					Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country, City, Site, Premise, newmanagementAddress, 
					existingmanagementAddress, manageaddressdropdownvalue, existingcityselectionmode, newcityselectionmode, cityname, 
					citycode, existingsiteselectionmode, newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, 
					newpremiseselectionmode, premisename, premisecode);
			
		}
		
		if((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian")) || (technologySelectedfordevicecreation.equalsIgnoreCase("Accedian-1G"))) {
		deviceCreatoin_Accedian(cpename, vender_Accedian, snmpro, managementAddress, Mepid, powerAlarm_Accedian, MediaselectionActualValue,
				Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country, City, Site, Premise, newmanagementAddress, 
				existingmanagementAddress, manageaddressdropdownvalue, existingcityselectionmode, newcityselectionmode, cityname, 
				citycode, existingsiteselectionmode, newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, 
				newpremiseselectionmode, premisename, premisecode);
	}
		
}

public void deviceCreatoin_Overture(String cpename, String vender, String snmpro, String managementAddress,
		String Mepid, String poweralarm, String MediaselectionActualValue, String Macaddress, String serialNumber,
		String hexaSerialnumber, String linkLostForwarding, String country, String City, String Site,
		String Premise, String newmanagementAddress, String existingmanagementAddress,
		String manageaddressdropdownvalue, String existingcityselectionmode, String newcityselectionmode,
		String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
		String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode,
		String premisename, String premisecode)
		throws InterruptedException, IOException, IOException {

	try {
		String linklostForwardingcheckboxstate = "enabled";

		String[] Vender = { "Overture ISG-26", "Overture ISG-26R", "Overture ISG-26S", "Overture ISG140",
				"Overture ISG180", "Overture ISG6000" };

		String[] powerAlarm = { "AC", "DC" };

		String[] MediaSelectionExpectedValue = { "SFP-A with SFP-B", "RJ45-A with SFP-B" };

		String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

		//// scrolltoend();
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Country Error Message
		device_countrywarningMessage();

		// Media Selection Error Message
		// device_mediaSelectionWarningMessage(application);

	//	scrollDown("//label[text()='Name']");
		waitforPagetobeenable();

		// Vendor/Model Error Message
		device_vendorModelWarningMessage();

		// Management Address Error Message
		device_managementAddressWarningMessage();

		// Power Alarm Error Message
		device_powerAlarmWarningMessage();

		// MAC Address Error Message
		device_macAddressWarningMessage();

		// Vendor/Model
		device_vendorModel(Vender, vender);

		// Snmpro
		device_snmPro(snmpro);

		// Management Address dropdown
		device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

		// MEP Id
		device_mepID(Mepid);

		// Power Alarm
		device_powerAlarm(poweralarm);

		// MAC Address
		device_MAcaddress(Macaddress);

		// Media Selection
		
			device_mediaSelection(MediaSelectionExpectedValue, MediaselectionActualValue);
	

		//// scrolltoend();
		waitforPagetobeenable();

		// Link lost Forwarding
		device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

		// Country
		device_country(country);

		// City
		if (existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
			addCityToggleButton();
			// New City
			newcity(newcityselectionmode, cityname, citycode);
			// New Site
			newSite(newsiteselectionmode, sitename, sitecode);
			// New Premise
			newPremise(newpremiseselectionmode, premisename, premisecode);

		}

		else if (existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
			// Existing City
			existingCity(City);

			// Site

			if (existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
				// Existing Site
				existingSite(Site);

				// Premise
				if (existingpremiseselectionmode.equalsIgnoreCase("yes")
						& newpremiseselectionmode.equalsIgnoreCase("no")) {
					existingPremise(Premise);

				} else if (existingpremiseselectionmode.equalsIgnoreCase("no")
						& newpremiseselectionmode.equalsIgnoreCase("yes")) {
					// New Premise
					addPremiseTogglebutton();
					newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
				}

			}

			else if (existingsiteselectionmode.equalsIgnoreCase("no")
					& newsiteselectionmode.equalsIgnoreCase("yes")) {
				// New Site
				addSiteToggleButton();
				newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode);

				// New Premise
				newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
			}
		}

		// OK button

		
		
		
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		scrollIntoTop();
		waitforPagetobeenable();

	//	verifyExists(Lanlink_InternationalObj.International.warningmEssage1_devicename, "Device Name");
	//	verifyExists(Lanlink_InternationalObj.International.warningmEssage2_devicename, "Device Name");
	//	verifyExists(Lanlink_InternationalObj.International.warningmEssage3_devicename, "Device Name");

		// Name
		device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

		// scrolltoend();
		waitforPagetobeenable();
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

	//	sa.assertAll();

	} catch (AssertionError e) {

		e.printStackTrace();
		// Report.LogInfo("Info", "FAiled while verify the fields for Add
		// CPE device","FAIL");

	}
	waitforPagetobeenable();
	waitforPagetobeenable();

}


public void device_countrywarningMessage() throws InterruptedException, IOException {

	// Country Error Message
	boolean countryErr = false;
	countryErr = isElementPresent(Lanlink_InternationalObj.International.AddCPEdevice_countryErrmsg);
//	sa.assertTrue(countryErr, "Country warning message is not displayed ");

	if (countryErr) {
		Reporter.log("country warning message is displaying as expected");
		String countryErrMsg = getTextFrom(Lanlink_InternationalObj.International.AddCPEdevice_countryErrmsg);
		Reporter.log("Country  message displayed as : " + countryErrMsg);
		Report.LogInfo("Info", " validation message for Country field displayed as : " + countryErrMsg, "PASS");
		Reporter.log("Country warning message displayed as : " + countryErrMsg);
	} else {
	//	Reporter.log("Country warning message is not displaying");
		//Report.LogInfo("Info", " Validation message for Country dropdown is not displaying", "FAIL");
	}
}

public void device_country(String country) throws InterruptedException, IOException {

	selectByVisibleText(Lanlink_InternationalObj.International.countryDropdown_selectTag, country,"Country");
}

public void addCityToggleButton() throws InterruptedException, IOException {

	// Add city Toggle button
	click(Lanlink_InternationalObj.International.addsiteswitch);
	waitforPagetobeenable();
}

public void newcity(String newcityselectionmode, String cityname, String citycode)
		throws InterruptedException, IOException, IOException {

	// New City
	if (newcityselectionmode.equalsIgnoreCase("yes")) {

		if (cityname.equalsIgnoreCase("null")) {
		} else {
			// City Name Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_citynamefield, cityname, "City Name");
			waitforPagetobeenable();
			String citynme = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_citynamefield,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered City Name is : " + citynme);
		}

		if (citycode.equalsIgnoreCase("null")) {
			// value for City Code field is not added
		} else {

			// City Code Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_citycodefield, citycode, "City Code");
			waitforPagetobeenable();
			String citycde = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_citycodefield,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered City Code is : " + citycde);

		}

	} else {
	}

}

public void newSite(String newsiteselectionmode, String sitename, String sitecode)
		throws InterruptedException, IOException, IOException {

	// New site
	if (newsiteselectionmode.equalsIgnoreCase("yes")) {

		if (sitename.equalsIgnoreCase("null")) {
		} else {
			// Site Name Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitenamefield, sitename, "Site Name");
			waitforPagetobeenable();
			String sitenme = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitenamefield,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered Site Name is : " + sitenme);

		}

		if (sitecode.equalsIgnoreCase("null")) {
		} else {

			// Site Code Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitecodefield, sitecode, "Site Code");
			waitforPagetobeenable();
			String sitecde = getAttributeFrom(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitecodefield,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered Site Code is : " + sitecde);

		}

	}

}

public void newPremise(String newpremiseselectionmode, String premisecode, String premisename)
		throws InterruptedException, IOException, IOException {

	// New premise
	if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

		if (premisename.equalsIgnoreCase("null")) {
		} else {
			// Premise Name Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisenamefield, premisename,
					"Premise Name");
			waitforPagetobeenable();
			String prmsenme = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisenamefield, "value");
			waitforPagetobeenable();
			Reporter.log("Entered Premise Name is : " + prmsenme);
		}

		if (premisecode.equalsIgnoreCase("null")) {
		} else {
			// Premise Code Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisecodefield, premisecode,
					"Premise Code");
			waitforPagetobeenable();
			String premisecde = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisecodefield, "value");
			waitforPagetobeenable();
			Reporter.log("Entered Premise Code is : " + premisecde);
		}

	} else {
	}

}


public void existingCity(String city) throws InterruptedException, IOException {

	selectByVisibleText(Lanlink_InternationalObj.International.cityDropdown_selectTag,city,"City");

}

public void existingSite(String site) throws InterruptedException, IOException {

	selectByVisibleText(Lanlink_InternationalObj.International.siteDropdown_selectTag, site,"Site");
	//selectValueInsideDropdown(Lanlink_InternationalObj.International.siteDropdown_selectTag, "Site", site);
	//addDropdownValues_commonMethod("Site",Lanlink_InternationalObj.International.siteDropdown_selectTag,site);

}

public void existingPremise(String premise) throws InterruptedException, IOException {

	selectByVisibleText(Lanlink_InternationalObj.International.premiseDropdown_selectTag, premise,"Premise");

}

public void addPremiseTogglebutton() throws InterruptedException, IOException {

	// Add Premise Toggle button
	click(Lanlink_InternationalObj.International.addpremiseswitch,"addpremiseswitch");
	waitforPagetobeenable();
}

public void newPremise_clickOnPremisetoggleButton(String newpremiseselectionmode, String premisecode,
		String premisename) throws InterruptedException, IOException { // New
																		// premise
	if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

		if (premisename.equalsIgnoreCase("null")) {
		} else {
			// Premise Name Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton,
					premisename, "Premise name");
			waitforPagetobeenable();
			String prmsenme = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered Premise Name is : " + prmsenme);

		}

		if (premisecode.equalsIgnoreCase("null")) {
			// value for Premise code field is not entered
		} else {
			// Premise Code Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton,
					premisecode, "Premise code");
			waitforPagetobeenable();
			String premisecde = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered Premise Code is : " + premisecde);

		}

	} else {
		// Add new Premise is not selected
	}
}

public void addSiteToggleButton() throws InterruptedException, IOException {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	// Add Site Toggle button
	click(Lanlink_InternationalObj.International.addsiteswitch,"addsiteswitch");
	waitforPagetobeenable();
}

public void newSite_ClickOnSiteTogglebutton(String newsiteselectionmode, String sitename, String sitecode)
		throws InterruptedException, IOException {

	// New site
	if (newsiteselectionmode.equalsIgnoreCase("yes")) {

		if (sitename.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", " value for Site name field  is not entered", "FAIL");
		} else {
			// Site Name Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton,
					sitename, "Site name");
			waitforPagetobeenable();
			String sitenme = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton, "value");
			waitforPagetobeenable();
			Reporter.log("Entered Site Name is : " + sitenme);
			Report.LogInfo("Info", "Entered Site Name is : " + sitenme, "PASS");
		}

		if (sitecode.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", " value for Site code field  is not entered", "FAIL");
		} else {

			// Site Code Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton,
					sitecode, "Site code");
			waitforPagetobeenable();
			String sitecde = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton, "value");
			waitforPagetobeenable();
			Reporter.log("Entered Site Code is : " + sitecde);
			Report.LogInfo("Info", "Entered Site Code is : " + sitecde, "PASS");

		}

	} else {
		Report.LogInfo("Info", " Add new city is not selected", "FAIL");
	}
}

public void newPremise_clickonSiteToggleButton(String newpremiseselectionmode, String premisename,
		String premisecode) throws InterruptedException, IOException {

	// New premise
	if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

		if (premisename.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", " value for Premise Name field  is not entered", "PASS");
		} else {
			// Premise Name Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton,
					premisename, "premisename");
			waitforPagetobeenable();
			String prmsenme = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered Premise Name is : " + prmsenme);
			Report.LogInfo("Info", "Entered Premise Name is : " + prmsenme, "PASS");
		}

		if (premisecode.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", " value for Premise code field  is not entered", "FAIL");
		} else {
			// Premise Code Field
			sendKeys(Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton,
					premisecode, "premisename");
			waitforPagetobeenable();
			String premisecde = getAttributeFrom(
					Lanlink_InternationalObj.International.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton,
					"value");
			waitforPagetobeenable();
			Reporter.log("Entered Premise Code is : " + premisecde);
			Report.LogInfo("Info", "Entered Premise Code is : " + premisecde, "PASS");
		}

	} else {
	//	Report.LogInfo("Info", " Add new Premise is not selected", "FAIL");
	}
}


public void verifyCPEdevicedataenteredForIntermediateEquipment_1G(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
	String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
	String technologySelectedfordevicecreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
	String poweralarm_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_overture");
	String powerAlarm_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_Accedian");
	String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
	String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
	String vender_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Overture");
	String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Accedian");
	String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"cpe_manageaddressdropdownvalue");
	String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
	String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
	String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
	String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
	String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
	String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
	String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
	String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
	String existingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
	String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
	String newmanagementAddressSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
	String existingmanagementAddressSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
	
	waitToPageLoad();

	waitforPagetobeenable();
	clickOnBankPage();
	waitforPagetobeenable();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	 webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();

	waitforPagetobeenable();

	if (technologySelectedfordevicecreation.equalsIgnoreCase("Overture")) {

		viewdevice_Overture(cpename, vender_Overture, snmpro, managementAddress, Mepid, poweralarm_Overture, MediaselectionActualValue,
				Macaddress,linkLostForwarding, country, existingCity, cityname, existingSite, sitename, existingPremise, premisename,existingcityselectionmode,
				newcityselectionmode, existingsiteselectionmode, newsiteselectionmode, newmanagementAddressSelection, existingmanagementAddressSelection,
				manageaddressdropdownvalue, existingpremiseselectionmode, newpremiseselectionmode);

	}

	if ((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian"))
			|| (technologySelectedfordevicecreation.equalsIgnoreCase("Accedian-1G"))) {

		viewdevice_Accedian(cpename, vender_Accedian, snmpro, managementAddress, Mepid, powerAlarm_Accedian, MediaselectionActualValue,
				Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,country, existingCity, cityname, existingSite, sitename, existingPremise, premisename,existingcityselectionmode,
				newcityselectionmode, existingsiteselectionmode, newsiteselectionmode, newmanagementAddressSelection, existingmanagementAddressSelection,
				manageaddressdropdownvalue, existingpremiseselectionmode, newpremiseselectionmode);
	}

}

public void viewdevice_Overture(String cpename, String vender,
		String snmpro, String managementAddress, String Mepid, String poweralarm, String Mediaselection,
		String Macaddress, String linkLostForwarding, String existingcountry, String existingCity,
		String newCity, String existingSite, String newSite, String existingPremise, String newPremise, 
		String existingcityselectionmode, String newcityselectionmode, String existingsiteselectionmode, 
		String newsiteselectionmode, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
		String existingpremiseselectionmode, String newpremiseselectionmode)
		throws InterruptedException, IOException {

	
	String[] RouterId = new String[2];
	RouterId = cpename.split(".lanlink");

	String RouterIdValue = RouterId[0];

	String mediaSelectionValueInViewDevicePage = "no";
	if (Mediaselection.equalsIgnoreCase("null")) {
		Mediaselection = mediaSelectionValueInViewDevicePage;
	} else {
		Mediaselection = mediaSelectionValueInViewDevicePage;
	}

	//  verifyEnteredvalues_deviceName("Name", RouterIdValue,cpename );
	  
	  verifyEnteredvalues("Router Id", RouterIdValue);
	  
	  verifyEnteredvalues("Vendor/Model", vender);
	  
	  verifyEnteredvalues("Snmpro", snmpro);
	// Management Address
	if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
		 verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
	} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
			&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
		 verifyEnteredvalues("Management Address", managementAddress);

	}

//	  verifyEnteredvalues("MEP Id", Mepid);
	  
	  verifyEnteredvalues("Power Alarm", poweralarm);
	  
	  verifyEnteredvalues("Media Selection", Mediaselection);
	  
	  verifyEnteredvalues("MAC Address", Macaddress);
	  
	  verifyEnteredvalues("Link Lost Forwarding", linkLostForwarding);
	  
	  verifyEnteredvalues("Country", existingcountry);

	//City  
		 if((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {
			// verifyEnteredvalues("City", existingCity);
		 }
		 else if((existingcityselectionmode.equalsIgnoreCase("no")) && (newcityselectionmode.equalsIgnoreCase("Yes"))) {
			// verifyEnteredvalues("City", newCity);
		 } 
		 
		 
		//Site
		 if((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {
			// verifyEnteredvalues("Site", existingSite);
		 }
		 else if((existingsiteselectionmode.equalsIgnoreCase("no")) && (newsiteselectionmode.equalsIgnoreCase("Yes"))) {
			// verifyEnteredvalues("Site", newSite);
		 } 
		 
		 
		//Premise
		 if((existingpremiseselectionmode.equalsIgnoreCase("Yes")) && (newpremiseselectionmode.equalsIgnoreCase("no"))) {
			 verifyEnteredvalues("Premise", existingPremise);
		 }
		 else if((existingpremiseselectionmode.equalsIgnoreCase("no")) && (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
			 verifyEnteredvalues("Premise", newPremise);
		 } 
		  
		
		}

public void deviceCreatoin_Accedian(String cpename, String vender, String snmpro, String managementAddress,
		String Mepid, String poweralarm, String MediaselectionActualValue, String Macaddress, String serialNumber,
		String hexaSerialnumber, String linkLostForwarding, String country, String City, String Site,
		String Premise, String newmanagementAddress, String existingmanagementAddress,
		String manageaddressdropdownvalue, String existingcityselectionmode, String newcityselectionmode,
		String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
		String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode,
		String premisename, String premisecode) throws InterruptedException, IOException, IOException {

	try {
		String linklostForwardingcheckboxstate = "disabled";

		String[] Vender = { "Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX" };

		String[] powerAlarm = { "AC", "DC" };

		String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

		// scrolltoend();
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Country Error Message
		device_countrywarningMessage();

		// serial Number Error Message
		device_serialNumberWarningMessage();

		// Hexa Serial Number Error Message
		device_hexaSerialNumberWarningMessage();

		scrollDown("//label[text()='Name']");
		waitforPagetobeenable();

		// Vendor/Model Error Message
		device_vendorModelWarningMessage();

		// Management Address Error Message
		device_managementAddressWarningMessage();

		// Power Alarm Error Message
		device_powerAlarmWarningMessage();

		// Vendor/Model
		device_vendorModel(Vender, vender);

		// Snmpro
		device_snmPro(snmpro);

		// Management Address dropdown
		device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

		// MEP Id
		device_mepID(Mepid);

		// Power Alarm
		device_powerAlarm(poweralarm);

		// serial number
		device_serialNumber(serialNumber);

		// scrolltoend();
		waitforPagetobeenable();

		// Link lost Forwarding
		// device_linklostForwarding(linkLostForwarding,
		// linklostForwardingcheckboxstate);

		// Country
		device_country(country);

		// City
		if (existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
			addCityToggleButton();
			// New City
			newcity(newcityselectionmode, cityname, citycode);
			// New Site
			newSite(newsiteselectionmode, sitename, sitecode);
			// New Premise
			newPremise(newpremiseselectionmode, premisename, premisecode);

		}

		else if (existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
			// Existing City
			existingCity(City);

			// Site

			if (existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
				// Existing Site
				existingSite(Site);

				// Premise
				if (existingpremiseselectionmode.equalsIgnoreCase("yes")
						& newpremiseselectionmode.equalsIgnoreCase("no")) {
					existingPremise(Premise);

				} else if (existingpremiseselectionmode.equalsIgnoreCase("no")
						& newpremiseselectionmode.equalsIgnoreCase("yes")) {
					// New Premise
					addPremiseTogglebutton();
					newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
				}

			}

			else if (existingsiteselectionmode.equalsIgnoreCase("no")
					& newsiteselectionmode.equalsIgnoreCase("yes")) {
				// New Site
				addSiteToggleButton();
				newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode);

				// New Premise
				newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
			}
		}

		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		scrollIntoTop();
		waitforPagetobeenable();

		verifyExists(Lanlink_InternationalObj.International.warningmEssage1_devicename, "Device Name");
		verifyExists(Lanlink_InternationalObj.International.warningmEssage2_devicename, "Device Name");
		verifyExists(Lanlink_InternationalObj.International.warningmEssage3_devicename, "Device Name");

		// Name
		device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

		// scrolltoend();
		waitforPagetobeenable();
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

	//	sa.assertAll();

	} catch (AssertionError e) {

		e.printStackTrace();
		// Report.LogInfo("Info", "FAiled while verify the fields for Add
		// CPE device","FAIL");

	}
	waitforPagetobeenable();

}

public void viewdevice_Accedian(String cpename, String vender,
		String snmpro, String managementAddress, String Mepid, String poweralarm, String Mediaselection,
		String Macaddress, String serialNumber, String hexaSerialnumber, String linkLostForwarding, String existingcountry, String existingCity,
		String newCity, String existingSite, String newSite, String existingPremise, String newPremise, 
		String existingcityselectionmode, String newcityselectionmode, String existingsiteselectionmode, 
		String newsiteselectionmode, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
		String existingpremiseselectionmode, String newpremiseselectionmode)
		throws InterruptedException, IOException {

	
	String[] RouterId = new String[2];
	RouterId = cpename.split(".lanlink");

	String RouterIdValue = RouterId[0];

	String mediaSelectionValueInViewDevicePage = "no";
	if (Mediaselection.equalsIgnoreCase("null")) {
		Mediaselection = mediaSelectionValueInViewDevicePage;
	} else {
		Mediaselection = mediaSelectionValueInViewDevicePage;
	}
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + cpename + Lanlink_InternationalObj.International.deviceview2);
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + RouterIdValue + Lanlink_InternationalObj.International.deviceview2);
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + vender + Lanlink_InternationalObj.International.deviceview2);
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + snmpro + Lanlink_InternationalObj.International.deviceview2);

	// Management Address
	if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {

		verifyExists(Lanlink_InternationalObj.International.deviceview1 + manageaddressdropdownvalue
				+ Lanlink_InternationalObj.International.deviceview2);

	} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
			&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {

		verifyExists(Lanlink_InternationalObj.International.deviceview1 + managementAddress
				+ Lanlink_InternationalObj.International.deviceview2);

	}

	verifyExists(Lanlink_InternationalObj.International.deviceview1 + poweralarm + Lanlink_InternationalObj.International.deviceview2);
	verifyExists(
			Lanlink_InternationalObj.International.deviceview1 + Mediaselection + Lanlink_InternationalObj.International.deviceview2);
	// verifyExists(Lanlink_InternationalObj.International.deviceview1+Macaddress+Lanlink_InternationalObj.International.deviceview2);
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + serialNumber + Lanlink_InternationalObj.International.deviceview2);
	// verifyExists(Lanlink_InternationalObj.International.deviceview1+linkLostForwarding+Lanlink_InternationalObj.International.deviceview2);

	// City
	if ((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {

		verifyExists(
				Lanlink_InternationalObj.International.deviceview1 + existingCity + Lanlink_InternationalObj.International.deviceview2);

	} else if ((existingcityselectionmode.equalsIgnoreCase("no"))
			&& (newcityselectionmode.equalsIgnoreCase("Yes"))) {

		verifyExists(Lanlink_InternationalObj.International.deviceview1 + newCity + Lanlink_InternationalObj.International.deviceview2);

	}

	// Site
	if ((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {

		verifyExists(
				Lanlink_InternationalObj.International.deviceview1 + existingSite + Lanlink_InternationalObj.International.deviceview2);

	} else if ((existingsiteselectionmode.equalsIgnoreCase("no"))
			&& (newsiteselectionmode.equalsIgnoreCase("Yes"))) {

		verifyExists(Lanlink_InternationalObj.International.deviceview1 + newSite + Lanlink_InternationalObj.International.deviceview2);

	}

	// Premise
	if ((existingpremiseselectionmode.equalsIgnoreCase("Yes"))
			&& (newpremiseselectionmode.equalsIgnoreCase("no"))) {

		verifyExists(Lanlink_InternationalObj.International.deviceview1 + existingPremise
				+ Lanlink_InternationalObj.International.deviceview2);

	} else if ((existingpremiseselectionmode.equalsIgnoreCase("no"))
			&& (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
		verifyExists(
				Lanlink_InternationalObj.International.deviceview1 + newPremise + Lanlink_InternationalObj.International.deviceview2);
	}
}

public void EDITCPEdevicedforIntermediateEquipment_1G(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {
	String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
	String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_hexaSerialnumber");
	String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseName");
	String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseCode");
	String NewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteCode");
	String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_country");
	String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_devic_snmpro");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_managementAddress");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mepid");
	String poweralarm_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_1G_Overture");
	String ExistingSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSiteSelection");
	String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Macaddress");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_serialNumber");
	String ExistingPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremiseSelection");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_linkLostForwarding");
	String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
	String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");
	String vender_1G_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_1G_Overtue");
	String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityCode");
	String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mediaselection");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"device_intequip_manageaddress_dropdownvalue");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"device_intequip_existingmanagementAddress_selection");
	String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
	String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityName");
	String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
	String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
	String NewSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteSelection");
	String ExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
	String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
	String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
	String newPremiseselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseSelection");
	String NewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteName");
	String newCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcitySelection");
	String ExistingCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingcitySelection");
	String technologySelected_SiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String vender_1G_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_1G_Accedian");
	String poweralarm_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_1G_Accedian");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	if (technologySelected.equalsIgnoreCase("Overture")) {

		eDITCPEdevicedetailsentered_1G_Overture(Cpename, vender_1G_Overture, snmpro, managementAddress, Mepid,
				poweralarm_Overture, Mediaselection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
				Country, ExistingCitySelection, newCitySelection, existingCity, newCityName, newCityCode,
				ExistingSiteSelection, NewSiteSelection, ExistingSite, NewSiteName, NewSiteCode,
				ExistingPremiseSelection, newPremiseselection, existingPremise, newPremiseName, newPremiseCode);
	} else if ((technologySelected.equalsIgnoreCase("Accedian"))
			|| (technologySelected.equalsIgnoreCase("Accedian-1G"))) {

		eDITCPEdevicedetailsentered_1G_Accedian(Cpename, vender_1G_Accedian, snmpro, managementAddress, Mepid,
				poweralarm_Accedian, Mediaselection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
				Country, ExistingCitySelection, newCitySelection, existingCity, newCityName, newCityCode,
				ExistingSiteSelection, NewSiteSelection, ExistingSite, NewSiteName, NewSiteCode,
				ExistingPremiseSelection, newPremiseselection, existingPremise, newPremiseName, newPremiseCode);
	}

}

public void eDITCPEdevicedetailsentered_1G_Overture(String cpedevicename, String vender, String snmpro,
		String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
		String serialNumber, String hexaSerialnumber, String linkLostForwarding, String Country,
		String ExistingCitySelection, String NewCitySelection, String existingCity, String newCityName,
		String newCityCode, String ExistingSiteSelection, String NewSiteSelection, String ExistingSite,
		String NewSiteName, String NewSiteCode, String ExistingPremiseSelection, String newPremiseselection,
		String existingPremise, String newPremiseName, String newPremiseCode)
		throws InterruptedException, IOException {

	Reporter.log("Entered edit functionalitty");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	click(Lanlink_InternationalObj.International.viewPCEdevice_Actiondropdown, "Action");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.EditCPEdevicelinkunderviewpage,"EditCPEdevicelinkunderviewpage");
	waitforPagetobeenable();
	Reporter.log("edit functionality worked");

	// Name field
	device_editnamefield(cpedevicename);

	// vendor/model
	device_editVendorModelField(vender);

	// Snmpro
	device_editSnmproField();

	// Management address
	device_editManagementAddressField(managementAddress);

	// Mepid
	device_editMEPIdField(Mepid);

	// power alarm
	device_editPowerAlarm(poweralarm);

	// Serial Number
	device_editserialnumber(serialNumber);

	// Media Selection
	


	// Mac address
	device_editMACaddress(Macaddress);

	// linklost forwarding
	device_editlinkLostforwarding(linkLostForwarding);

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	// Country
	if (!Country.equalsIgnoreCase("Null")) {

		click(Lanlink_InternationalObj.International.countryDropdown_selectTag, Country);

		// New City
		if (ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
			click_addToggleButton(Lanlink_InternationalObj.International.addcityswitch);
			click(Lanlink_InternationalObj.International.addcityswitch);
			// City name
			sendKeys(Lanlink_InternationalObj.International.citynameinputfield, newCityName, "City Name");
			// City Code
			sendKeys(Lanlink_InternationalObj.International.citycodeinputfield, newCityCode, "City Code");
			// Site name
			sendKeys(Lanlink_InternationalObj.International.sitenameinputfield_addCityToggleSelected, NewSiteName,
					"Site Name");
			// Site Code
			sendKeys(Lanlink_InternationalObj.International.sitecodeinputfield_addCityToggleSelected, NewSiteCode,
					"Site Code");
			// Premise name
			sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addCityToggleSelected, newPremiseName,
					"Premise Name");
			// Premise Code
			sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addCityToggleSelected, newPremiseCode,
					"Premise Code");

		}

		// Existing City
		else if (ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {

			click(Lanlink_InternationalObj.International.cityDropdown_selectTag, existingCity);

			// Existing Site
			if (ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
				click(Lanlink_InternationalObj.International.siteDropdown_selectTag, ExistingSite);

				// Existing Premise
				if (ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
					click(Lanlink_InternationalObj.International.premiseDropdown_selectTag, existingPremise);

				}

				// New Premise
				else if (ExistingPremiseSelection.equalsIgnoreCase("no")
						& newPremiseselection.equalsIgnoreCase("yes")) {

					click(Lanlink_InternationalObj.International.addpremiseswitch);
					// Premise name
					sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addPremiseToggleSelected,
							newPremiseName, "Premise Name");
					// Premise Code
					sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addPremiseToggleSelected,
							newPremiseCode, "Premise Code");
				}
			}

			else if (ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {

				click(Lanlink_InternationalObj.International.addsiteswitch);
				// Site name
				sendKeys(Lanlink_InternationalObj.International.sitenameinputfield_addSiteToggleSelected, NewSiteName,
						"Site Name");
				// Site Code
				sendKeys(Lanlink_InternationalObj.International.sitecodeinputfield_addSiteToggleSelected, NewSiteCode,
						"Site Code");

				// Premise name
				sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addSiteToggleSelected, newPremiseName,
						"Premise Name");
				// Premise Code
				sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addSiteToggleSelected, newPremiseCode,
						"Premise Code");
			}
		}

	} else if (Country.equalsIgnoreCase("Null")) {

		// City
		//City	
				editCity(ExistingCitySelection, NewCitySelection, Lanlink_InternationalObj.International.cityDropdown_selectTag, "selectcityswitch", "addcityswitch",
						existingCity, newCityName, newCityCode, "City");
				
				
			//Site	
				editSite(ExistingSiteSelection, NewSiteSelection, Lanlink_InternationalObj.International.siteDropdown_selectTag, "selectsiteswitch",
						"addsiteswitch", ExistingSite , NewSiteName, NewSiteCode, "Site");
				
			//Premise
				editPremise(ExistingPremiseSelection, newPremiseselection, Lanlink_InternationalObj.International.premiseDropdown_selectTag, "selectpremiseswitch",
						"addpremiseswitch", existingPremise, newPremiseName, newPremiseCode, "Premise");

	}

	click(Lanlink_InternationalObj.International.okbutton, "OK");
	waitforPagetobeenable();
	waitforPagetobeenable();

}

public void click_addToggleButton(String xpath) throws InterruptedException, IOException {

	// Add Toggle button
	click(xpath);
	waitforPagetobeenable();
}

public void editCity(String editExistingCity, String editNewCity, String dropdown_xpath,
		String selectCityToggleButton, String addCityToggleButton, String dropdownValue, String editNewCityName,
		String editNewCityCode, String labelname) throws InterruptedException, IOException {
	if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
		existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
		existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
		newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
	} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
		newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
	}

	else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
		Report.LogInfo("EditCity", "No chnges made under 'City' field", "PASS");
	}

}

public void editSite(String editExistingCity, String editNewCity, String dropdown_xpath,
		String selectSiteToggleButton, String addSitetoggleButton, String dropdownValue, String editNewSiteName,
		String editNewSiteCode, String labelname) throws InterruptedException, IOException {

	if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
		existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
		existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
	} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
		newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
	} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
		newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
	} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
		Report.LogInfo("EditSite", "No changes made under 'Site' field", "PASS");
	}

}

public void editPremise(String editExistingPremise, String editNewPremise, String dropdown_xpath,
		String selectPremiseToggleButton, String addPremisetoggleButton, String dropdownValue,
		String editNewPremiseName, String editNewPremiseCode, String labelname)
		throws InterruptedException, IOException {

	if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
		existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
	} else if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("null")) {
		existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
	} else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("Yes")) {
		newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
	} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("Yes")) {
		newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
	} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("null")) {
		Report.LogInfo("EditPremise", "No changes made under 'Premise' field", "PASS");

	}

}

public void existingPremise(String dropdown_xpath, String dropdownValue, String selectPremiseToggleButton,
		String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {
		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);
	} else {
		click(selectPremiseToggleButton);
		waitforPagetobeenable();
		// click_commonMethod(application, "Select Premise toggle button",
		// selectPremiseToggleButton, xml);
		// Thread.sleep(1000);
		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);
	}
}

public void newPremise(String dropdown_xpath, String addPremisetoggleButton, String editNewPremiseName,
		String editNewPremiseCode) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {
		click(addPremisetoggleButton);
		waitforPagetobeenable();
		// click_commonMethod(application, "Select Premise toggle button",
		// addPremisetoggleButton, xml);
		// Thread.sleep(1000);
		// Premise name
		edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
				editNewPremiseName);

		// Premise Code
		edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
				editNewPremiseCode);
	} else {
		// Premise name
		edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
				editNewPremiseName);
		// Premise Code
		edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
				editNewPremiseCode);
	}
}

public void existingSite(String dropdown_xpath, String dropdownValue, String selectSiteToggleButton,
		String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {

		selectByValue(dropdown_xpath, dropdownValue, labelname);
	} else {
		click(selectSiteToggleButton);
		waitforPagetobeenable();
		// Thread.sleep(1000);
		selectByValue(dropdown_xpath, dropdownValue, labelname);

	}
}


public void newSite(String dropdown_xpath, String addSitetoggleButton, String editNewSiteName,
		String editNewSiteCode, String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {
		click(addSitetoggleButton);
		waitforPagetobeenable();
		// click_commonMethod(application, "Select City toggle button",
		// addSitetoggleButton, xml);
		// Thread.sleep(1000);
		// Site name
		edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
		// Site Code
		edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
	} else {
		// Site name
		edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
		// Site Code
		edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
	}
}

public void existingCity(String dropdown_xpath, String dropdownValue, String selectCityToggleButton,
		String labelname) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {

		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);

	} else {

		click(selectCityToggleButton);
		waitforPagetobeenable();
		// click_commonMethod(application, "Select City toggle button",
		// selectCityToggleButton, xml);
		// Thread.sleep(1000);

		selectByValue(dropdown_xpath, dropdownValue, labelname);
		// addDropdownValues_commonMethod(application, labelname,
		// dropdown_xpath, dropdownValue, xml);

	}
}

public void newCity(String dropdown_xpath, String addCitytoggleButton, String editNewCityName,
		String editNewCityCode) throws InterruptedException, IOException {

	if (isVisible(dropdown_xpath)) {

		click(addCitytoggleButton);
		waitforPagetobeenable();
		// click_commonMethod(application, "Select City toggle button",
		// addCitytoggleButton, xml);
		// Thread.sleep(1000);
		// City name
		edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
		// City Code
		edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
	} else {
		// City name
		edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
		// City Code
		edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
	}
}

public void eDITCPEdevicedetailsentered_1G_Accedian(String cpedevicename, String vender, String snmpro,
		String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
		String serialNumber, String hexaSerialnumber, String linkLostForwarding, String Country,
		String ExistingCitySelection, String NewCitySelection, String existingCity, String newCityName, String newCityCode,
		String ExistingSiteSelection, String NewSiteSelection, String ExistingSite, String NewSiteName, String NewSiteCode,
		String ExistingPremiseSelection, String newPremiseselection, String existingPremise, String newPremiseName, String newPremiseCode)
		throws InterruptedException, IOException {
   
   

   Reporter.log("Entered edit functionalitty");

		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		
		click(Lanlink_InternationalObj.International.viewPCEdevice_Actiondropdown, "Action");
		
		waitforPagetobeenable();
		
		click(Lanlink_InternationalObj.International.EditCPEdevicelinkunderviewpage);
		waitforPagetobeenable();
		Reporter.log("edit functionality worked");
		
	     
	//Name field
		device_editnamefield(cpedevicename);
		
	//vendor/model
		device_editVendorModelField(vender);
		
	//Snmpro
		device_editSnmproField();

	//Management address
		device_editManagementAddressField(managementAddress);
		
	//Mepid	
		device_editMEPIdField(Mepid);
		
	//power alarm	
		device_editPowerAlarm(poweralarm);
	
	//Serial Number
		device_editserialnumber(serialNumber);
	    
	    
	//linklost forwarding	
//		device_editlinkLostforwarding(linkLostForwarding);
	    
	
 		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		
		//Country
		if(!Country.equalsIgnoreCase("Null")) {
			
			//click(Lanlink_InternationalObj.International.countryDropdown_selectTag,Country);
			selectValueInsideDropdown(Lanlink_InternationalObj.International.countryDropdown_selectTag, "Country", Country);

			
			//New City		
			if(ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
			
				click(Lanlink_InternationalObj.International.addcityswitch);
			   //City name
				sendKeys(Lanlink_InternationalObj.International.citynameinputfield, newCityName, "City Name");
			   //City Code	
				sendKeys(Lanlink_InternationalObj.International.citycodeinputfield, newCityCode, "City Code");
			   //Site name
				sendKeys(Lanlink_InternationalObj.International.sitenameinputfield_addCityToggleSelected, NewSiteName, "Site Name");
			   //Site Code
				sendKeys(Lanlink_InternationalObj.International.sitecodeinputfield_addCityToggleSelected, NewSiteCode, "Site Code");
			   //Premise name	
				sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addCityToggleSelected, newPremiseName, "Premise Name");
			   //Premise Code	
				sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addCityToggleSelected, newPremiseCode, "Premise Code");
					
			  	 }	
		
		//Existing City	
		else if(ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {
			
			   selectValueInsideDropdown(Lanlink_InternationalObj.International.cityDropdown_selectTag, "City", existingCity);

			
		 //Existing Site
			  if(ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
				  selectValueInsideDropdown(Lanlink_InternationalObj.International.siteDropdown_selectTag, "Site", ExistingSite);
				  
				  
			 //Existing Premise
				  if(ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
					 selectValueInsideDropdown(Lanlink_InternationalObj.International.premiseDropdown_selectTag, "Premise", existingPremise);
					  
		          	 }
				  
				//New Premise  
				  else if(ExistingPremiseSelection.equalsIgnoreCase("no") & newPremiseselection.equalsIgnoreCase("yes")) {
					  
					  click(Lanlink_InternationalObj.International.addpremiseswitch);
					  //Premise name	
					  sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addPremiseToggleSelected, newPremiseName, "Premise Name");
					   //Premise Code	
					  sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addPremiseToggleSelected, newPremiseCode, "Premise Code");
				  } 
	         	}
  		
		  else if(ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {
			  	
			  click(Lanlink_InternationalObj.International.addsiteswitch);
			  //Site name
				sendKeys(Lanlink_InternationalObj.International.sitenameinputfield_addSiteToggleSelected, NewSiteName,"Site Name");
			   //Site Code
				sendKeys(Lanlink_InternationalObj.International.sitecodeinputfield_addSiteToggleSelected, NewSiteCode, "Site Code");
				
			   //Premise name	
				sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addSiteToggleSelected, newPremiseName, "Premise Name");
			   //Premise Code	
				sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addSiteToggleSelected, newPremiseCode, "Premise Code");
			  }
		}
		
	}
	else if(Country.equalsIgnoreCase("Null")) {
		
	
	//City	
		editCity(ExistingCitySelection, NewCitySelection, Lanlink_InternationalObj.International.cityDropdown_selectTag, "selectcityswitch", "addcityswitch",
				existingCity, newCityName, newCityCode, "City");  //xpath  -  cityDropdown_selectTag
		
		
	//Site	
		editSite(ExistingSiteSelection, NewSiteSelection,  Lanlink_InternationalObj.International.siteDropdown_selectTag, "selectsiteswitch",
				"addsiteswitch", ExistingSite , NewSiteName, NewSiteCode, "Site");  //xpath  -  siteDropdown_selectTag
		
	//Premise
		editPremise(ExistingPremiseSelection, newPremiseselection,  Lanlink_InternationalObj.International.premiseDropdown_selectTag, "selectpremiseswitch",
				"addpremiseswitch", existingPremise, newPremiseName, newPremiseCode, "Premise");    //xpath  -  premiseDropdown_selectTag
		
	}
   scrollDown( Lanlink_InternationalObj.International.okbutton);
	
	click(Lanlink_InternationalObj.International.okbutton,"OK");
	waitforPagetobeenable();
	


}

public void IntEquip_clickonviewButton(String devicename) throws InterruptedException, IOException {

	waitToPageLoad();

	clickOnBankPage();

	waitforPagetobeenable();

	boolean viewpage = false;
	try {
		viewpage = isElementPresent(Lanlink_InternationalObj.International.viewCPEdevicepage_devices);

		if (viewpage) {
			Reporter.log("In view page");
		} else {

			((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();

			webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+devicename+"')]]]//span[text()='View']")).click();
			waitforPagetobeenable();
		}
	} catch (Exception e) {
		e.printStackTrace();

		// scrolltoend();
		waitforPagetobeenable();

		webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'" + devicename+ "')]]]//span[text()='View']")).click();
		waitforPagetobeenable();
	}
}




private void scrollIntoEnd() {
	// TODO Auto-generated method stub
	
}

public String fetchVendorModelValue() throws IOException, InterruptedException {
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	String vendorModel = getTextFrom(Lanlink_InternationalObj.International.fetchVendorModelvalue);
	String vendorValue = vendorModel;

	return vendorValue;
}

public String fetchCountryValue_InviewPage() throws IOException, InterruptedException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	String manageAdres = getTextFrom(Lanlink_InternationalObj.International.fetchCountryValue);
	String managementAddress = manageAdres;

	return managementAddress;
}

public void selectInterfacelinkforIntermediateEqipment(String devicename) throws IOException, InterruptedException {

	try {
		webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='Select Interfaces']")).click();

		Reporter.log("SelectInterface link for Intermediate Equipment is selected");
		Reporter.log("Select an inertface to add with the service under Intermediate equipment");
	} catch (Exception e) {
		e.printStackTrace();
	}

}


public void SelectShowInterfacelink_IntermediateequipmentAndVerifyEditInterfacePage(
		String Interfacename_forEditInterface, String devicename_IntEquipment)
		throws InterruptedException, IOException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.CPEdevice_showinterfaceslink_intEquip);
	waitforPagetobeenable();

	selectRowForEditingInterface_showInterface(Interfacename_forEditInterface, devicename_IntEquipment);

	click(Lanlink_InternationalObj.International.Equipment_showintefaceedit);
	waitforPagetobeenable();

}

public void EnterdataForEditInterfaceforShowInterfacelinkunderIntermediateEquipment(String testDataFile,
		String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
	String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerType");
	String bearerspeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerSpeed");
	String totalbandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"editInterfacepage_bandwidth");
	String vlantype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_vlantype");
	String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_Vlanid");
	String circuitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_circuitId");
	String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

	// verify Edit Interface page
	boolean editInterface_popuptitleName = false;
	try {
		editInterface_popuptitleName = isElementPresent(Lanlink_InternationalObj.International.Editinterface_popupTitlename);
		if (editInterface_popuptitleName) {
			Reporter.log("Edit interface popup is displaying as expected");

			// Interface name
			verifyenteredvaluesForEditPage_noneditableFields("Interface",interfacename);

			// Circuit ID
			configure_circuitId(circuitId);

			// Bearer type
			addDropdownValues_commonMethod("Bearer Type",Lanlink_InternationalObj.International.Equipment_configureBearerType, bearertype_value);

			// Bearerspeed
			addDropdownValues_commonMethod("Bearer Speed",Lanlink_InternationalObj.International.Equipment_configureBearerSpeed, bearerspeed);

			// Total Circuit bandwidth
			addDropdownValues_commonMethod("Total Circuit Bandwidth",Lanlink_InternationalObj.International.Equipment_configureTotalcircuitbandwidth, totalbandwidth);

			// VLAN type
			addDropdownValues_commonMethod("VLAN Type",Lanlink_InternationalObj.International.Equipment_configureVlanType, vlantype);

			// vlan
			sendKeys(Lanlink_InternationalObj.International.Equipment_configureVLANid, vlanid, "VLAN Id");
			waitforPagetobeenable();

		}
	} catch (NoSuchElementException e) {
		e.printStackTrace();
		Reporter.log(" 'Edit interface' popup is not displaying");
	} catch (Exception ee) {
		ee.printStackTrace();
		Reporter.log(" 'Edit interface' popup is not displaying");
	}
	click(Lanlink_InternationalObj.International.okbutton, "OK");
	waitforPagetobeenable();
}
public String selectRowForEditingInterface_showInterface(String interfacename, String devicename)
		throws InterruptedException, IOException {

	int TotalPages;

	String TextKeyword = webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//span[@ref='lbTotal']")).getText();

	TotalPages = Integer.parseInt(TextKeyword);

	Reporter.log("Total number of pages in table is: " + TotalPages);
	String interfaceAvailability = "yes";
	ab:
	if (TotalPages != 0) {

		for (int k = 1; k <= TotalPages; k++) {

		// Current page
		String CurrentPage = webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//span[@ref='lbCurrent']")).getText();
		int Current_page = Integer.parseInt(CurrentPage);
		Reporter.log("The current page is: " + Current_page);

		assertEquals(k, Current_page,"");

		Reporter.log("Currently we are in page number: " + Current_page);

		List<WebElement> results = webDriver.findElements(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//div[text()='"+ interfacename +"']"));
		
			
		int numofrows = results.size();
		Reporter.log("no of results: " + numofrows);
		boolean resultflag;

	
		if ((numofrows == 0)) {
			
			webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//button[text()='Next']"));
			Thread.sleep(3000);

		}

		else {
			for (int i = 0; i < numofrows; i++) {
				try {

					resultflag = results.get(i).isDisplayed();
					Reporter.log("status of result: " + resultflag);
					if (resultflag) {
						Reporter.log(results.get(i).getText());
						results.get(i).click();
							Report.LogInfo("Info", interfacename + " is selected under 'show Interface' ", "PASS");
							waitforPagetobeenable();
							webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//button[text()='Action']")).click();;

							break ab;
						}

					} catch (StaleElementReferenceException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						Report.LogInfo("Info", interfacename + " Interface name is not displaying", "FAIL");

						results = webDriver.findElements(
								By.xpath("(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//div[text()='"+interfacename +"']"));
						numofrows = results.size();
						// results.get(i).click();
						Reporter.log("selected row is : " + i);

					}

				}
			}
		}
	} else {
		Report.LogInfo("Info", interfacename + " Interface name is not displaying", "FAIL");
		Reporter.log("No values available in table");
		Reporter.log("No values available inside the InterfaceInService table");
		Report.LogInfo("Info", "No value available inside 'show Interface' panel", "FAIL");
		interfaceAvailability = "No";
	}

	return interfaceAvailability;

}

public void hideInterface_IntEquipment() throws InterruptedException, IOException {
	verifyExists(Lanlink_InternationalObj.International.CPEdevoce_hideInterfacelink_intEquip,"CPEdevoce_hideInterfacelink_intEquip");

	click(Lanlink_InternationalObj.International.CPEdevoce_hideInterfacelink_intEquip,"CPEdevoce_hideInterfacelink_intEquip");
	waitforPagetobeenable();
	
}

public void deleteDeviceFromServiceForIntermediateequipment(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
	webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='Delete']")).click();	
	Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Intermediate Equipment");
	Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Intermediate Equipment");
	ExtentTestManager.getTest().log(LogStatus.PASS, " 'Delete From Service' link has been clicked for cpe device under Intermediate Equipment");


	boolean deletemessage=findWebElement(Lanlink_InternationalObj.International.deleteMessage_equipment).isDisplayed();
	while(deletemessage) {
		Reporter.log("Are you sure want to delete - message is getting displayed on clicking DeletefromService link");
		Reporter.log("Delete popup message is getting displayed");
		String actualMessage=findWebElement(Lanlink_InternationalObj.International.deleteMessage_equipment).getText();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Delete device popup is displaying and popup message displays as: "+ actualMessage);
		Reporter.log("Delete device popup is displaying and popup message displays as: "+ actualMessage);
		break;
	} 
	
	
		click(Lanlink_InternationalObj.International.deletebutton_deletefromsrviceforequipment,"deletebutton_deletefromsrviceforequipment");
		ExtentTestManager.getTest().log(LogStatus.PASS, "clicked on 'delete' button");
		waitforPagetobeenable();
		ExtentTestManager.getTest().log(LogStatus.PASS, cpename + " device has got deleted from service");
		Reporter.log("Device got deleted from service as expected");

	
}

public void successMessage_deleteFromService(String application) throws InterruptedException {
	
	boolean successMessage=false;
try {	
	successMessage=findWebElement(Lanlink_InternationalObj.International.deleteDevice_successmEssage).isDisplayed();
	
	if(successMessage) {
		
		String actualmessage=findWebElement(Lanlink_InternationalObj.International.deleteDevice_successmEssage).getText();
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message for device deletion is dipslaying as expected");
		Reporter.log( " Success Message for device deletion is dipslaying as expected");
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Message displays as: "+actualmessage);
		Reporter.log("Message displays as: "+actualmessage);
		
	}else {
		ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message for device deletion is not dipslaying");
		Reporter.log( " Success Message for device deletion is not dipslaying");
	}
	
}catch(Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message for device deletion is not dipslaying");
		Reporter.log( " Success Message for device deletion is not dipslaying");
	}
}

public void pamTest(String serviceID) throws InterruptedException, IOException {
	
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	verifyExists(Lanlink_InternationalObj.International.PAMtest_Link, "PAMtest_Link" );

	click(Lanlink_InternationalObj.International.PAMtest_Link, "PAMtest_Link" );
	
	waitforPagetobeenable();
	waitforPagetobeenable();
	
	boolean pamTestPage=false;
	try {	
	pamTestPage=findWebElement(Lanlink_InternationalObj.International.PAMtest_popupPage).isDisplayed();
	if(pamTestPage) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "'PAM Test' popup page is displaying");
		Reporter.log("'PAM Test' popup page is displaying");
		
	//Type Value	
		String typeValue=getTextFrom(Lanlink_InternationalObj.International.PAMTest_TypeFieldValue);
		if(typeValue.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Type' field");
			Reporter.log("No values displaying under 'Type' field");
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Under 'Type' field, value is dispaying as: "+typeValue);
			Reporter.log("Under 'Type' field, value is dispaying as: "+typeValue);
		}
		
	//Service
		String serviceValue=getTextFrom(Lanlink_InternationalObj.International.PAMTest_ServiceValue);
		if(serviceValue.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Service' field");
			Reporter.log("No values displaying under 'Service' field");
		}else {
			verifyExists(Lanlink_InternationalObj.International.PAMTest_ServiceValue , "Service");
		}
		
	//Tool Response
		String toolResponse=getTextFrom(Lanlink_InternationalObj.International.PAMTest_ToolResponse);
		if(toolResponse.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Tool Response' field");
			Reporter.log("No values displaying under 'Tool Response' field");
		}else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Under 'Tool Response' field, value is dispaying as: "+toolResponse);
			Reporter.log("Under 'Tool Response' field, value is dispaying as: "+toolResponse);
		}
		
		
	//click on "X"button to close the popup
		click(Lanlink_InternationalObj.International.configure_alertPopup_xbutton,  "X");
		
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'PAM Test' popup page is not displaying");
		Reporter.log("'PAM Test' popup page is not displaying");
	}
}catch(Exception e) {
	e.printStackTrace();
	ExtentTestManager.getTest().log(LogStatus.FAIL, "'PAM Test' popup page is not displaying");
	Reporter.log("'PAM Test' popup page is not displaying");
	}
}

public void deleteSiteOrder(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitforPagetobeenable();
	webDriver.findElement(By.xpath("//tr//*[contains(.,'"+siteOrderNumber_PointToPoint+"')]//following-sibling::td//a[text()='Delete']")).click();

	
	WebElement DeleteAlertPopup= findWebElement(Lanlink_InternationalObj.International.delete_alertpopup);
    if(DeleteAlertPopup.isDisplayed())
    {
  	 String deletPopUpMessage= getTextFrom(Lanlink_InternationalObj.International.deleteMessages_textMessage);
  	 ExtentTestManager.getTest().log(LogStatus.PASS, "Delete Pop up message displays as: "+ deletPopUpMessage);
  	 
       click(Lanlink_InternationalObj.International.deletebutton, "Delete");
       
       scrollUp();
      waitforPagetobeenable();
       
       verifysuccessmessage("Site Order successfully deleted");
    }
    else
    {
         Reporter.log("Delete alert popup is not displayed");
          ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete alert popup is not displayed");
    }
}

public void deleteService() throws InterruptedException, IOException {
	
	WebElement orderPanel= findWebElement(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);
	waitforPagetobeenable();
	
	click(Lanlink_InternationalObj.International.Editservice_actiondropdown, "Action");
	Reporter.log("Action dropdown is working");
	waitforPagetobeenable();
	
		//click on delete link
			click(Lanlink_InternationalObj.International.deleteLink_common, "Delete");
			
			 WebElement DeleteAlertPopup= findWebElement(Lanlink_InternationalObj.International.delete_alertpopup);
	         if(DeleteAlertPopup.isDisplayed())
	         {
	       	 String deletPopUpMessage= getTextFrom(Lanlink_InternationalObj.International.deleteMessages_textMessage);
	       	 ExtentTestManager.getTest().log(LogStatus.PASS, "Delete Pop up message displays as: "+ deletPopUpMessage);
	       	 
	            click(Lanlink_InternationalObj.International.deletebutton, "Delete");
	            
	            scrollUp();
	           waitforPagetobeenable();
	            
	           
	         }
	         else
	         {
	              Reporter.log("Delete alert popup is not displayed");
	               ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete alert popup is not displayed");
	         }
}

public String fetchProActiveMonitoringValue() throws InterruptedException, IOException {
	
	String proactiveMonitor = "No";
	
	WebElement servicePanel= findWebElement(Lanlink_InternationalObj.International.viewServicepage_Servicepanel);
	scrollDown(Lanlink_InternationalObj.International.viewServicepage_Servicepanel);
	waitforPagetobeenable();
	
	proactiveMonitor = getTextFrom(Lanlink_InternationalObj.International.fetchProActiveMonitoringValue);
	
	return proactiveMonitor;
}

public boolean findPanelHeader(String devicePanelName) throws InterruptedException {

	//  scrolltoend();
	  WebElement el=null;
	 boolean panelheader=false;
	try { 
		
		el=findWebElement((Lanlink_InternationalObj.International.devicePanelHeaders_InViewSiteOrderPage).replace("value", devicePanelName));
		panelheader=el.isDisplayed();
		
	 if(panelheader) {
		 ExtentTestManager.getTest().log(LogStatus.INFO, devicePanelName +" panel is displaying under 'view site order' page");
		Reporter.log(" 'Equipment' panel is displaying under 'view site order' page");
		 panelheader=true;
		 
	 }else {
		 ExtentTestManager.getTest().log(LogStatus.INFO, devicePanelName + "  panel is not displaying under 'view site order' page");
		 Reporter.log(" 'Equipment' panel is not displaying under 'view site order' page");
		 panelheader=false;
		 
	 }}catch(NoSuchElementException e) {
		 e.printStackTrace();
		 ExtentTestManager.getTest().log(LogStatus.INFO, devicePanelName + " panel is not displaying under 'view site order' page");
		 Reporter.log(" 'Equipment' panel is not displaying under 'view site order' page");
		 panelheader=false;
		 
	 }
	 
	  return panelheader;
}

public WebElement getwebelementNoWait(final String locator) throws InterruptedException
{   
	final String[] finalval;
	if(locator.startsWith("name"))
	{
		finalval=locator.split("=");	
		return webDriver.findElement(By.name(finalval[1]));     
		 
	}
	else if(locator.startsWith("id"))
	{
		finalval=locator.split("=");
		return webDriver.findElement(By.id(finalval[1]));     
		
	}
	else if(locator.startsWith("//")|| locator.startsWith("(//"))
	{
		return webDriver.findElement(By.xpath(locator));     
	}
	else
	{
		return webDriver.findElement(By.xpath(locator)); 
	}

}
public void verifyValuesforCPEexistingdevice_1G_Equipment() throws InterruptedException	{
	   
	   fetchValueFromViewPage("Name");
	   fetchValueFromViewPage( "Router Id");
	   fetchValueFromViewPage( "Vendor/Model");
	   fetchValueFromViewPage( "Snmpro");
	   fetchValueFromViewPage( "Management Address");
	   fetchValueFromViewPage( "MEP Id");
	   fetchValueFromViewPage( "Power Alarm");
	   fetchValueFromViewPage( "Media Selection");
	   fetchValueFromViewPage( "Link Lost Forwarding");
	   fetchValueFromViewPage( "MAC Address");
//	   fetchValueFromViewPage(application, "Serial Number");
}

public void verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(String testDataFile,
		String sheetName, String scriptNo, String dataSetNo, String speed)
		throws InterruptedException, IOException {
	String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String vpntopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpntopology");
	String interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interfacespeed");
	String existingDeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingDeviceName");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.CPEdevice_adddevicelink, "Add Device");
	waitforPagetobeenable();
	waitToPageLoad();

	if ((technologySelected.equalsIgnoreCase("Atrica")) && (vpntopology.equals("Hub&Spoke"))
			&& (interfacespeed.equals("1GigE"))) {
		selectTechnology_HubAndSpoke();
	}
	waitToPageLoad();

	click(Lanlink_InternationalObj.International.existingDevice_SelectDeviceToggleButton, "Select Device");
	waitforPagetobeenable();
	waitToPageLoad();

	click(Lanlink_InternationalObj.International.chooseAdeviceDropdown, existingDeviceName);
	waitToPageLoad();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
}

public void fetchValueFromViewPage(String labelname) throws InterruptedException { 

	String text = null;
	WebElement element = null;

	try {
		element = getwebelementNoWait("//div[div[label[contains(text(),'"+ labelname + "')]]]/div[2]");
		String emptyele = element.getText().toString();

		if(element==null)
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname+" not found");
			Reporter.log(labelname+" not found");
		}
		else if (emptyele!=null && emptyele.isEmpty()) {
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "No value displaying under "+ labelname);
			Reporter.log("No value displaying under "+ labelname);
		}
//			
		else {
			element.getText();
			ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " value is displaying as: "+ emptyele);
			Reporter.log(labelname + " value is displaying as: "+ emptyele);
			
		} 
		
	}catch (Exception e) {
		e.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
		Reporter.log(labelname + " field is not displaying");
	}
}

public String EnterdataForEditInterfaceforConfigurelinkunderIntermediateEquipment(String testDataFile,
		String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerType");
	String bearerspeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerSpeed");
	String totalbandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"editInterfacepage_bandwidth");
	String vlantype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_vlantype");
	String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_Vlanid");
	String circuitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_circuitId");
	String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

	
	
	String interfaceavailability = selectRowForconfiglinkunderIntermediateEquipment(interfacename);
	
	if(interfaceavailability.equalsIgnoreCase("Yes")) {	

	// Perform edit Interface
	click(Lanlink_InternationalObj.International.Equipment_configureActiondropdown,"Equipment_configureActiondropdown");
	//waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.Equipment_configureEditlink,"Equipment_configureEditlink");
	waitForAjax();
	waitforPagetobeenable();

	// verify Edit Interface page
	boolean editInterface_popuptitleName = false;
	try {
		editInterface_popuptitleName = isElementPresent(Lanlink_InternationalObj.International.Editinterface_popupTitlename);
		if (editInterface_popuptitleName) {
			Reporter.log("Edit interface popup is displaying as expected");

			// Interface name
			verifyenteredvaluesForEditPage_noneditableFields("Interface", interfacename);

			// Circuit ID
			configure_circuitId(circuitId);

			// Bearer type
			addDropdownValues_commonMethod("Bearer Type",Lanlink_InternationalObj.International.Equipment_configureBearerType, bearertype_value);

			// Bearerspeed
			addDropdownValues_commonMethod("Bearer Speed",Lanlink_InternationalObj.International.Equipment_configureBearerSpeed, bearerspeed);

			// Total Circuit bandwidth
			addDropdownValues_commonMethod("Total Circuit Bandwidth",Lanlink_InternationalObj.International.Equipment_configureTotalcircuitbandwidth, totalbandwidth);

			// VLAN type
			addDropdownValues_commonMethod("VLAN Type",Lanlink_InternationalObj.International.Equipment_configureVlanType, vlantype);

			// vlan
			sendKeys(Lanlink_InternationalObj.International.Equipment_configureVLANid, vlanid, "VLAN Id");
			waitforPagetobeenable();

		}
	} catch (NoSuchElementException e) {
		e.printStackTrace();
		Reporter.log(" 'Edit interface' popup is not displaying");
	} catch (Exception ee) {
		ee.printStackTrace();
		Reporter.log(" 'Edit interface' popup is not displaying");
	}

	click(Lanlink_InternationalObj.International.okbutton, "OK");
	waitforPagetobeenable();
	}
	return interfaceavailability;
	

}

public String selectRowForconfiglinkunderIntermediateEquipment(String interfacename) throws InterruptedException, IOException {

	int TotalPages;

	// scroll down the page
	// JavascriptExecutor js = ((JavascriptExecutor) driver);
	// js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	// waitforPagetobeenable();
	String interfaceAvailability = "Yes";
	scrollDown(Lanlink_InternationalObj.International.configure_totalPage);
	String TextKeyword = getTextFrom(Lanlink_InternationalObj.International.configure_totalPage);

	TotalPages = Integer.parseInt(TextKeyword);

	Reporter.log("Total number of pages in table is: " + TotalPages);

	ab: if (TotalPages != 0) {
		for (int k = 1; k <= TotalPages; k++) {

			// Current page
			String CurrentPage = getTextFrom(Lanlink_InternationalObj.International.configure_currentpage);
			int Current_page = Integer.parseInt(CurrentPage);
			Reporter.log("The current page is: " + Current_page);

			assertEquals(k, Current_page, "");

			Reporter.log("Currently we are in page number: " + Current_page);

			List<WebElement> results = webDriver.findElements(By.xpath("//div[@role='row']//div[text()='"+ interfacename +"']"));
			int numofrows = results.size();
			Reporter.log("no of results: " + numofrows);
			boolean resultflag;

			if (numofrows == 0) {

				click(Lanlink_InternationalObj.International.intermediateequip_nextpage);

			}

			else {

				for (int i = 0; i < numofrows; i++) {

					try {

						resultflag = results.get(i).isDisplayed();
						Reporter.log("status of result: " + resultflag);
						if (resultflag) {
							Reporter.log(results.get(i).getText());
							results.get(i).click();
							Report.LogInfo("Info", interfacename + " is selected","PASS");
							waitforPagetobeenable();
							break ab;
						}

					} catch (StaleElementReferenceException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						results = webDriver.findElements(
								By.xpath("//div[@role='row']//div[text()='"+ interfacename +"']"));
						numofrows = results.size();
						// results.get(i).click();
						Reporter.log("selected row is : " + i);

					}
				}
			}

		}
	} else {
		Reporter.log("No values present inside the table");
	}
	return interfaceAvailability;
}
	
public String SelectShowInterfacelinkAndVerifyEditInterfacePage(String interfacename, String devicename) throws InterruptedException, IOException {
	
	waitforPagetobeenable();
	scrollDown(Lanlink_InternationalObj.International.CPEdevice_showinterfaceslink);
	waitforPagetobeenable();
	
	click(Lanlink_InternationalObj.International.CPEdevice_showinterfaceslink, "showInterface_Link");
	waitforPagetobeenable();
	
	WebElement devinameRow=webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]"));
	scrollIntoView(devinameRow);
	waitforPagetobeenable();
	
	String interfaceAvailability = selectRowForEditingInterface_showInterface(interfacename, devicename);
	
	if(interfaceAvailability.equalsIgnoreCase("Yes")) {
		click(Lanlink_InternationalObj.International.Equipment_showintefaceedit, "Edit");
		waitforPagetobeenable();
	}
	
	return interfaceAvailability;	
	
}

public void verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(String testDataFile,
		String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	
	String existingDeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "intEquip_existingDeviceValue");

	ExtentTestManager.getTest().log(LogStatus.INFO,"Select existing CPE device");
	  
	 
	waitforPagetobeenable();
	
	scrollUp();
	
	click(Lanlink_InternationalObj.International.existingDevice_SelectDeviceToggleButton,"Select Device" );
	waitforPagetobeenable();
	 
	waitforPagetobeenable();
	
	addDropdownValues_commonMethod("Choose a Device", Lanlink_InternationalObj.International.chooseAdeviceDropdown, existingDeviceName);
	waitforPagetobeenable();
	scrollDown(Lanlink_InternationalObj.International.obutton_spanTag);
	click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
	}

public void verifyValuesforCPEexistingdevice_1G_intEquipment(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {
	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
	String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
	String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Management Address");
	String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Link Lost Forwarding");
	String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
	String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String MACAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Name + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + RouterId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vendor_Model + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Snmpro + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + ManagementAddress + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MEPId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + PowerAlarm + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MediaselectionActualValue
			+ Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + serialNumber + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(
			Lanlink_InternationalObj.International.viewPage1 + linkLostForwarding + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + country + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + cityname + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MACAddress + Lanlink_InternationalObj.International.viewPage2);

}



public void EDITCPEdevice_IntermediateEquipment_10G(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {

	String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCityName");
	String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCityCode");
	String NewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteName");
	String NewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewSiteCode");
	String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingmanagementAddress");
	String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newmanagementAddress");
	String vender_10G_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vender");
	String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpename");
	String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vender");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmpro");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingmanagementAddress");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newmanagementAddress");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"manageaddressdropdownvalue");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"managementAddress");
	String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Link Lost Forwarding");
	String existingcountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
	String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String existingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");
	String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");
	String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingcityselectionmode");
	String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newcityselectionmode");
	String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingsiteselectionmode");
	String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newsiteselectionmode");
	String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingpremiseselectionmode");
	String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newpremiseselectionmode");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
	String ExistingCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"ExistingCitySelection");
	String ExistingSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"ExistingSiteSelection");
	String NewSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"NewSiteSelection");
	String ExistingPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"ExistingPremiseSelection");
	String newPremiseselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newPremiseselection");

	String NewCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"NewCitySelection");
	String ExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCitySelection");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	Reporter.log("Entered edit functionalitty");

	click(Lanlink_InternationalObj.International.viewPCEdevice_Actiondropdown, "Action");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.EditCPEdevicelinkunderviewpage);
	waitforPagetobeenable();
	Reporter.log("edit functionality worked");

	// Name field
	device_editnamefield(Cpename);

	// vendor/model
	device_editVendorModelField(vender_10G_Accedian);

	// Snmpro
	device_editSnmproField();

	// Management address
	device_editManagementAddressField(managementAddress);

	// Mepid
	device_editMEPIdField(Mepid);

	// power alarm
	device_editPowerAlarm(poweralarm);

	// Serial Number
	device_editserialnumber(serialNumber);

	// linklost forwarding
	device_editlinkLostforwarding(linkLostForwarding);

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitforPagetobeenable();

	// Country
	if (!Country.equalsIgnoreCase("Null")) {

		click(Lanlink_InternationalObj.International.countryDropdown_selectTag, Country);

		// New City
		if (ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
			click(Lanlink_InternationalObj.International.addcityswitch);
			// City name
			sendKeys(Lanlink_InternationalObj.International.citynameinputfield, newCityName, "City Name");
			// City Code
			sendKeys(Lanlink_InternationalObj.International.citycodeinputfield, newCityCode, "City Code");
			// Site name
			sendKeys(Lanlink_InternationalObj.International.sitenameinputfield_addCityToggleSelected, NewSiteName,
					"Site Name");
			// Site Code
			sendKeys(Lanlink_InternationalObj.International.sitecodeinputfield_addCityToggleSelected, NewSiteCode,
					"Site Code");
			// Premise name
			sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addCityToggleSelected, newPremiseName,
					"Premise Name");
			// Premise Code
			sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addCityToggleSelected, newPremiseCode,
					"Premise Code");

		}

		// Existing City
		else if (ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {

			click(Lanlink_InternationalObj.International.cityDropdown_selectTag, existingCity);

			// Existing Site
			if (ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
				click(Lanlink_InternationalObj.International.siteDropdown_selectTag, existingSite);

				// Existing Premise
				if (ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
					click(Lanlink_InternationalObj.International.premiseDropdown_selectTag, existingPremise);

				}

				// New Premise
				else if (ExistingPremiseSelection.equalsIgnoreCase("no")
						& newPremiseselection.equalsIgnoreCase("yes")) {

					click(Lanlink_InternationalObj.International.addpremiseswitch);
					// Premise name
					sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addPremiseToggleSelected,
							newPremiseName, "Premise Name");
					// Premise Code
					sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addPremiseToggleSelected,
							newPremiseCode, "Premise Code");
				}
			}

			else if (ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {

				click(Lanlink_InternationalObj.International.addsiteswitch);
				// Site name
				sendKeys(Lanlink_InternationalObj.International.sitenameinputfield_addSiteToggleSelected, NewSiteName,
						"Site Name");
				// Site Code
				sendKeys(Lanlink_InternationalObj.International.sitecodeinputfield_addSiteToggleSelected, NewSiteCode,
						"Site Code");

				// Premise name
				sendKeys(Lanlink_InternationalObj.International.premisenameinputfield_addSiteToggleSelected, newPremiseName,
						"Premise Name");
				// Premise Code
				sendKeys(Lanlink_InternationalObj.International.premisecodeinputfield_addSiteToggleSelected, newPremiseCode,
						"Premise Code");
			}
		}

	} else if (Country.equalsIgnoreCase("Null")) {

		// City
		editCity(ExistingCitySelection, NewCitySelection, "cityDropdown_selectTag", "selectcityswitch",
				"addcityswitch", existingCity, newCityName, newCityCode, "City");

		// Site
		editSite(ExistingSiteSelection, NewSiteSelection, "siteDropdown_selectTag", "selectsiteswitch",
				"addsiteswitch", ExistingSite, NewSiteName, NewSiteCode, "Site");

		// Premise
		editPremise(ExistingPremiseSelection, newPremiseselection, "premiseDropdown_selectTag",
				"selectpremiseswitch", "addpremiseswitch", existingPremise, newPremiseName, newPremiseCode,
				"Premise");

	}

	click(Lanlink_InternationalObj.International.okbutton, "OK");
	waitforPagetobeenable();

}

public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpename");
	String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vender");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmpro");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingmanagementAddress");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newmanagementAddress");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"manageaddressdropdownvalue");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"managementAddress");
	String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Link Lost Forwarding");
	String existingcountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
	String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String existingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");
	String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");
	String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingcityselectionmode");
	String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newcityselectionmode");
	String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingsiteselectionmode");
	String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newsiteselectionmode");
	String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingpremiseselectionmode");
	String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newpremiseselectionmode");
	// String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName,
	// scriptNo, dataSetNo, "MEP Id");
	String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingcityselectionmode");
	String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcityselectionmode");
	String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingsiteselectionmode");
	String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newsiteselectionmode");
	String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingpremiseselectionmode");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingcityselectionmode");
	String anewsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newcityselectionmode");

	try {

		String linklostForwardingcheckboxstate = "disabled";

		String[] Vender = { "Accedian 10GigE-MetroNode-CE-2Port", "Accedian 10GigE-MetroNode-CE-4Port" };

		String[] powerAlarm = { "DC Single Power Supply - PSU A", "DC Dual Power Supply - PSU-A+B" };

		String expectedDeviceNameFieldAutopopulatedValue = "<Device>-10G.lanlink.dcn.colt.net";

		String MEPid = "5555";

		String expectedValueForSnmpro = "JdhquA5";

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

	//	scrollDown(Lanlink_InternationalObj.International.name);
		waitforPagetobeenable();

		// Vendor/Model Error Message
		device_vendorModelWarningMessage();

		// Management Address Error Message
		device_managementAddressWarningMessage();

		// Power Alarm Error Message
		device_powerAlarmWarningMessage();

		// serial number Eror Message
		device_serialNumberWarningMessage();

		// Hexa serial Number
		device_hexaSerialNumberWarningMessage();

		// Vendor/Model
		device_vendorModel(Vender, vender);

		// Snmpro
		device_snmPro(snmpro);

		// Management Address dropdown
		device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

		// MEP Id
		device_mepID(Mepid);

		// Power Alarm
		device_powerAlarm(poweralarm);

		// Serial Number
		device_serialNumber(serialNumber);

		// Link lost Forwarding
		device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

		// Country
		device_country(country);

		// City
		if (existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
			addCityToggleButton();
			// New City
			newcity(newcityselectionmode, cityname, citycode);
			// New Site
			newSite(newsiteselectionmode, sitename, sitecode);
			// New Premise
			newPremise(newpremiseselectionmode, premisename, premisecode);

		}

		else if (existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
			// Existing City
			existingCity(City);

			// Site

			if (existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
				// Existing Site
				existingSite(Site);

				// Premise
				if (existingpremiseselectionmode.equalsIgnoreCase("yes")
						& newpremiseselectionmode.equalsIgnoreCase("no")) {
					existingPremise(Premise);

				} else if (existingpremiseselectionmode.equalsIgnoreCase("no")
						& newpremiseselectionmode.equalsIgnoreCase("yes")) {
					// New Premise
					addPremiseTogglebutton();
					newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
				}
			}

			else if (existingsiteselectionmode.equalsIgnoreCase("no")
					& newsiteselectionmode.equalsIgnoreCase("yes")) {
				// New Site
				addSiteToggleButton();
				newSite_ClickOnSiteTogglebutton(anewsiteselectionmode, sitename, sitecode);

				// New Premise
				newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
			}
		}

		// OK button

		// OK button
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		// cancel button
		verifyExists(Lanlink_InternationalObj.International.AddCPEdevice_cancelbutton, "Cancel");

		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		verifyExists(Lanlink_InternationalObj.International.warningmEssage1_devicename, "Device Name");
		verifyExists(Lanlink_InternationalObj.International.warningmEssage2_devicename, "Device Name");
		verifyExists(Lanlink_InternationalObj.International.warningmEssage3_devicename, "Device Name");

		// Name
		device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		//sa.assertAll();

	} catch (AssertionError e) {

		e.printStackTrace();

	}

}

public void verifyValuesforCPEexistingdevice_10G_intEquipment(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
	String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
	String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Management Address");
	String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Link Lost Forwarding");
	String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
	String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Name + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + RouterId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vendor_Model + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Snmpro + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + ManagementAddress + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MEPId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + PowerAlarm + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MediaselectionActualValue
			+ Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + serialNumber + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(
			Lanlink_InternationalObj.International.viewPage1 + linkLostForwarding + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + country + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + cityname + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + site + Lanlink_InternationalObj.International.viewPage2);
}

public void verifyCPEdevicedataenteredForIntermediateEquipment_10G(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpename");
	String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vender");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "snmpro");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingmanagementAddress");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newmanagementAddress");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"manageaddressdropdownvalue");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"managementAddress");
	String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Link Lost Forwarding");
	String existingcountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
	String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
	String existingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");
	String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");
	String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
	String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingcityselectionmode");
	String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newcityselectionmode");
	String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingsiteselectionmode");
	String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newsiteselectionmode");
	String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingpremiseselectionmode");
	String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newpremiseselectionmode");

	waitforPagetobeenable();
	clickOnBankPage();
	waitforPagetobeenable();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.deviceview1 + cpename + Lanlink_InternationalObj.International.deviceview2);
	waitforPagetobeenable();

	String[] RouterId = new String[2];
	RouterId = cpename.split(".lanlink");

	String RouterIdValue = RouterId[0];

	String mediaSelectionValueInViewDevicePage = "no";
	if (MediaselectionActualValue.equalsIgnoreCase("null")) {
		MediaselectionActualValue = mediaSelectionValueInViewDevicePage;
	} else {
		MediaselectionActualValue = mediaSelectionValueInViewDevicePage;
	}

	verifyExists(Lanlink_InternationalObj.International.deviceview1 + cpename + Lanlink_InternationalObj.International.deviceview2);
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + RouterIdValue + Lanlink_InternationalObj.International.deviceview2);
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + vender + Lanlink_InternationalObj.International.deviceview2);
	verifyExists(Lanlink_InternationalObj.International.deviceview1 + snmpro + Lanlink_InternationalObj.International.deviceview2);

	// Management Address
	if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
		verifyExists(manageaddressdropdownvalue);
	} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
			&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
		verifyExists(managementAddress);
	}

	verifyExists(poweralarm);

	verifyExists(MediaselectionActualValue);

	verifyExists(serialNumber);

	verifyExists(linkLostForwarding);

	verifyExists(existingcountry);

	// City
	if ((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {
		verifyExists(existingCity);
	} else if ((existingcityselectionmode.equalsIgnoreCase("no"))
			&& (newcityselectionmode.equalsIgnoreCase("Yes"))) {
		verifyExists(cityname);
	}

	// Site
	if ((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {
		verifyExists(existingSite);
	} else if ((existingsiteselectionmode.equalsIgnoreCase("no"))
			&& (newsiteselectionmode.equalsIgnoreCase("Yes"))) {
		verifyExists(sitename);
	}

	// Premise
	if ((existingpremiseselectionmode.equalsIgnoreCase("Yes"))
			&& (newpremiseselectionmode.equalsIgnoreCase("no"))) {
		verifyExists(existingPremise);
	} else if ((existingpremiseselectionmode.equalsIgnoreCase("no"))
			&& (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
		verifyExists(premisename);
	}

}
public void eDITCPEdevicedetailsentered_10G(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {

	String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String vender_10G_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;

	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;
	String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	;

	Reporter.log("Entered edit functionalitty");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.viewPCEdevice_Actiondropdown, "Action");
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.EditCPEdevicelinkunderviewpage);
	Reporter.log("edit functionality worked");

	waitToPageLoad();

	// Name field
	device_editnamefield(Cpename);

	// vendor/model
	device_editVendorModelField(vender_10G_Accedian);

	// Snmpro
	device_editSnmproField();

	// Management address
	device_editManagementAddressField(managementAddress);

	// Mepid
	device_editMEPIdField(Mepid);

	// power alarm
	device_editPowerAlarm(poweralarm);

	// serial Number
	device_editserialnumber(serialNumber);

	// linklost forwarding
//	device_editlinklostforwarding_10G(linkLostForwarding);

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	click(Lanlink_InternationalObj.International.okbutton, "OK");
	waitforPagetobeenable();

}


public void verifydetailsEnteredforCPEdevice_10G(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpename");

	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
	String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Management Address");
	// String MEPId=DataMiner.fngetcolvalue(testDataFile, sheetName,
	// scriptNo, dataSetNo, "MEP Id");
	String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
	String MACAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"manageaddressdropdownvalue");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Link Lost Forwarding");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"manageaddressdropdownvalue");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"manageaddressdropdownvalue");

	clickOnBankPage();
	waitforPagetobeenable();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	//click(Lanlink_InternationalObj.International.deviceView1 + cpename + Lanlink_InternationalObj.International.deviceView2);
	waitforPagetobeenable();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	// Splitting device name
	String[] RouterId = new String[2];
	RouterId = cpename.split(".lanlink");

	String RouterIdValue = RouterId[0];

	String Mediaselection = "no";
	String LinkLostForwarding = "yes";

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Name + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + RouterId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vendor_Model + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Snmpro + Lanlink_InternationalObj.International.viewPage2);

	// Management Address
	if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
		verifyExists(Lanlink_InternationalObj.International.viewPage1 + manageaddressdropdownvalue
				+ Lanlink_InternationalObj.International.viewPage2);
	} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
			&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
		verifyExists(
				Lanlink_InternationalObj.International.viewPage1 + managementAddress + Lanlink_InternationalObj.International.viewPage2);
	}

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + PowerAlarm + Lanlink_InternationalObj.International.viewPage2);

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MediaselectionActualValue
			+ Lanlink_InternationalObj.International.viewPage2);

	verifyExists(
			Lanlink_InternationalObj.International.viewPage1 + linkLostForwarding + Lanlink_InternationalObj.International.viewPage2);

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + serialNumber + Lanlink_InternationalObj.International.viewPage2);

}

public void verifyFieldsandAddCPEdevicefortheserviceselected_10G(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"MAC Address");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"MAC Address");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");

	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"MAC Address");
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.CPEdevice_adddevicelink);
	waitforPagetobeenable();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
	waitforPagetobeenable();

	try {

		String linklostForwardingcheckboxstate = "disabled";

		String[] Vender = { "Accedian 10GigE-MetroNode-CE-2Port" };

		String[] powerAlarm = { "DC Single Power Supply - PSU A", "DC Dual Power Supply - PSU-A+B" };

		String expectedDeviceNameFieldAutopopulatedValue = "<Device>-10G.lanlink.dcn.colt.net";

		String MEPid = "5555";

		String expectedValueForSnmpro = "JdhquA5";

		// Vendor/Model Error Message
		device_vendorModelWarningMessage();

		// Management Address Error Message
		device_managementAddressWarningMessage();

		// Power Alarm Error Message
		device_powerAlarmWarningMessage();

		// serial number Eror Message
		device_serialNumberWarningMessage();

		// Hexa serial Number
		device_hexaSerialNumberWarningMessage();

		// Vendor/Model
		device_vendorModel(Vender, vender);

		// Snmpro
		device_snmPro(snmpro);

		// Management Address dropdown
		device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

		// MEP Id
		device_mepID(Mepid);

		// Power Alarm
		device_powerAlarm(poweralarm);

		// Serial Number
		device_serialNumber(serialNumber);

		// Link lost Forwarding
		device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		// OK button
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		// cancel button
		verifyExists(Lanlink_InternationalObj.International.AddCPEdevice_cancelbutton, "Cancel");

		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		// CAncel button

		verifyExists(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		verifyExists(Lanlink_InternationalObj.International.warningmEssage1_devicename, "Device Name");
		verifyExists(Lanlink_InternationalObj.International.warningmEssage2_devicename, "Device Name");
		verifyExists(Lanlink_InternationalObj.International.warningmEssage3_devicename, "Device Name");

		// Name
		device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_InternationalObj.International.AddCPEdevice_OKbutton, "OK");

	//	sa.assertAll();

	} catch (AssertionError e) {

		e.printStackTrace();
		//

	}

}
public void verifyValuesforCPEexistingdevice_10G_Equipment(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {
	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
	String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
	String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Management Address");
	String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Link Lost Forwarding");

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Name + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + RouterId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vendor_Model + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Snmpro + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + ManagementAddress + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MEPId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + PowerAlarm + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MediaselectionActualValue
			+ Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + serialNumber + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(
			Lanlink_InternationalObj.International.viewPage1 + linkLostForwarding + Lanlink_InternationalObj.International.viewPage2);

}

public boolean EquipmentCOnfigurationPanel() throws IOException {

	boolean EquipConfigPanel = false;
	EquipConfigPanel = isElementPresent(Lanlink_InternationalObj.International.EquipementConfigurationPanel);
	if (EquipConfigPanel) {
		Reporter.log(
				"In 'view Site Order' page, 'Equipment Configuration' panel is displaying as expected for 'Actelis' Technology");
	} else {
		Reporter.log(
				"In 'view Site Order' page, 'Equipment Configuration' panel is not displaying for 'Actelis' Technology");

	}
	return EquipConfigPanel;
}

public void equipConfiguration_Actelis_AddDevice(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws IOException, InterruptedException {
	String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");
	String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
	String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Management Address");
	String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String ETH_Port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ETH Port");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	click(Lanlink_InternationalObj.International.equipConfig_addCPEdevice);
	waitforPagetobeenable();

	boolean addActelisHeader = false;
	addActelisHeader = isElementPresent(Lanlink_InternationalObj.International.addActelisCPEpage_headerName);
	if (addActelisHeader) {
		Reporter.log(" 'Add Actelis CPE device' page is displaying as expected");

		verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");
		click(Lanlink_InternationalObj.International.okbutton, "OK");
		waitforPagetobeenable();

		// Validate Warning Messages_Name Field
		verifyExists(Lanlink_InternationalObj.International.devicenameFieldErrMSg_addCPE_Actelis, "Name");

		// Validate Warning Messages_Router ID Field
		verifyExists(Lanlink_InternationalObj.International.RouterIDFieldErrMSg_addCPE_Actelis, "Router Id");

		// Validate Warning Messages_Management Address Field
		verifyExists(Lanlink_InternationalObj.International.manageAddressFieldErrMSg_addCPE_Actelis, "Management Address");

		// Name
		sendKeys(Lanlink_InternationalObj.International.nameField_addCPE_Actelis, devicename, "Name");

		// vendor/Model
		sendKeys(Lanlink_InternationalObj.International.vendorField_addCPE_Actelis, Vendor_Model, "Vendor/Model");

		// Router Id
		sendKeys(Lanlink_InternationalObj.International.RouterIdField_addCPE_Actelis, RouterId, "Router Id");

		// Management Address
		sendKeys(Lanlink_InternationalObj.International.managementAddressField_addCPE_Actelis, ManagementAddress,
				"Management Address");

		// MEP Id
		sendKeys(Lanlink_InternationalObj.International.MEPidField_addCPE_Actelis, MEPId, "MEP Id");

		// ETH Port
		sendKeys(Lanlink_InternationalObj.International.ETHportField_addCPE_Actelis, ETH_Port, "ETH Port");

		// CAncel button
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_cancel, "Cancel");

		verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");
		click(Lanlink_InternationalObj.International.okbutton, "OK");
		waitforPagetobeenable();

	} else {
		// Add Actelis CPE device' page is not displaying
	}
}

public void verifyDataEnteredFordeviceCreation_Actelis(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {
	String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");
	String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
	// String Vendor_Model=DataMiner.fngetcolvalue(testDataFile, sheetName,
	// scriptNo, dataSetNo, "Vendor/Model");
	String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Management Address");
	String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String ETH_Port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ETHPort");

	waitToPageLoad();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.actelis_EquipConfig_viewLink, "view_Link");
	waitforPagetobeenable();

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + devicename + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + RouterId + Lanlink_InternationalObj.International.viewPage2);
	// verifyExists(Lanlink_InternationalObj.International.viewPage1+Vendor_Model+Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + ManagementAddress + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MEPId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + ETH_Port + Lanlink_InternationalObj.International.viewPage2);

}

public void deleteDeviceFromService_EquipmentConfig_Actelis() throws InterruptedException, IOException {
	waitToPageLoad();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	click(Lanlink_InternationalObj.International.actelis_EquipConfig_deleteLink, "delete_link");

	Reporter.log(
			" 'Delete From Service' link has been clicked for cpe device under 'Equipment Configuration' panel");

	boolean deletemessage = isElementPresent(Lanlink_InternationalObj.International.deleteMessage_equipment);
	while (deletemessage) {
		Reporter.log(
				"Are you sure want to delete - message is getting displayed on clicking DeletefromService link");
		Reporter.log("Delete popup message is getting displayed");
		String actualMessage = getTextFrom(Lanlink_InternationalObj.International.deleteMessage_equipment);
		Reporter.log("Delete device popup is displaying and popup message displays as: " + actualMessage);
		break;
	}

	click(Lanlink_InternationalObj.International.deletebutton_deletefromsrviceforequipment);
	waitforPagetobeenable();
	Reporter.log("Device got deleted from service as expected");

}
public void addOvertureCircuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws IOException, InterruptedException {
	String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceName");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	// Click on Add Overture Link
	click(Lanlink_InternationalObj.International.addOvertureLink, "Add Overture");
	waitforPagetobeenable();
	waitToPageLoad();

	boolean overturePanelHeader = isElementPresent(Lanlink_InternationalObj.International.addOverture_overturePanel);
	if (overturePanelHeader) {
		Reporter.log("'Overture' page is displaying");

		sendKeys(Lanlink_InternationalObj.International.addOverture_serviceNameTextField, serviceName, "Service Name"); // Search
																												// ame
																												// text
																												// Field

		click(Lanlink_InternationalObj.International.addOverture_searchButton, "Search"); // Search
																				// Button
		waitforPagetobeenable();
		waitToPageLoad();

		String selectValueInTable = getTextFrom(Lanlink_InternationalObj.International.selectValueUnderAddOverturePage)
				.replace("value", serviceName);
		try {
			isElementPresent(selectValueInTable);
			Reporter.log("Records displays for the Service " + serviceName);

			click(selectValueInTable);
			waitforPagetobeenable();

			click(Lanlink_InternationalObj.International.addOverture_OKbutton, "OK");
			waitforPagetobeenable();
			waitToPageLoad();

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("No record displays for the Service " + serviceName);

			click(Lanlink_InternationalObj.International.addOverture_cancelButton, "cancel");
		}
	} else {
		Reporter.log("'Overture' page is not displaying");
	}
}

public void selectInterfaceForCircuits(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws IOException, InterruptedException {
	String interface1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interface1");
	String interface2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interface2");
	String selectEdgePointForInterface1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"selectEdgePointForInterface1");
	String selectEdgePointForInterface2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"selectEdgePointForInterface2");

	if (isElementPresent(Lanlink_InternationalObj.International.addOverture_interfaceInServicePanel)) {
		Reporter.log("'Interface in Service' page displays");

		// Select row for 1st interface
		click(Lanlink_InternationalObj.International.interfaceFilterButton, "interface_filter");

		sendKeys(Lanlink_InternationalObj.International.interfacePage_filterText, interface1, "filter");

		String selectInterface = getTextFrom(Lanlink_InternationalObj.International.interfaceInService_selectValueUnderTable)
				.replace("value", interface1);
		click(selectInterface);
		Reporter.log(interface1 + " is selected under 'Interface In Service' page");

		if (selectEdgePointForInterface1.equalsIgnoreCase("Yes")) {
			String selectEdgePoint = getTextFrom(
					Lanlink_InternationalObj.International.interfaceinService_selectEdgePointforInterface).replace("value",
							interface1);
			click(selectEdgePoint);
			Reporter.log(interface1 + " is selected under 'Interface In Service' page");

		} else {
			Reporter.log("'Edge End Point' radio button is not selected for " + interface1);
		}

		// select row for 2nd interface
		click(Lanlink_InternationalObj.International.interfaceFilterButton, "interface_filter");

		sendKeys(Lanlink_InternationalObj.International.interfacePage_filterText, interface2, "filter");
		waitforPagetobeenable();

		String selectInterface2 = getTextFrom(Lanlink_InternationalObj.International.interfaceInService_selectValueUnderTable)
				.replace("value", interface2);
		click(selectInterface2);
		Reporter.log(interface2 + " is selected under 'Interface In Service' page");

		if (selectEdgePointForInterface2.equalsIgnoreCase("Yes")) {
			String selectEdgePoint2 = getTextFrom(
					Lanlink_InternationalObj.International.interfaceinService_selectEdgePointforInterface).replace("value",
							interface2);
			click(selectEdgePoint2);
			Reporter.log(interface2 + " is selected under 'Interface In Service' page");

		} else {

			Reporter.log("'Edge End Point' radio button is not selected for " + interface2);
		}

		waitforPagetobeenable();
		click(Lanlink_InternationalObj.International.addOverture_OKbutton, "OK");

	} else {
		Reporter.log("'Interface in Service' page not displays");
	}
}

public void addOveture_PAMtest_selectRow(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws IOException, InterruptedException {
	String interface1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interface1");

	waitToPageLoad();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	String selectInterface = getTextFrom(Lanlink_InternationalObj.International.PAMtest_selectinterface).replace("value",
			interface1);
	click(Lanlink_InternationalObj.International.PAMtest_selectinterface, selectInterface);
	Reporter.log(interface1 + " is selected for performing 'PAM Test'");

	click(Lanlink_InternationalObj.International.PAMtest_actionDropdown, "Action");
	click(Lanlink_InternationalObj.International.PAMtest_Link, "PAMTest");

	waitforPagetobeenable();
	waitToPageLoad();

}

public void PAMtest_ForCircuitCreation(String testDataFile, String sheetName, String scriptNo, String dataSetNo,
		String ServiceID) throws InterruptedException, IOException {
	String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceName");
	String siteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteName");

	boolean pamTestPage = false;
	try {
		pamTestPage = isElementPresent(getTextFrom(Lanlink_InternationalObj.International.PAMtest_popupPage));
		if (pamTestPage) {
			Reporter.log("'PAM Test' popup page is displaying");

			// Type Value
			String typeValue = getTextFrom(Lanlink_InternationalObj.International.PAMTest_TypeFieldValue);
			if (typeValue.isEmpty()) {
				Reporter.log("No values displaying under 'Type' field");
			} else {
				Reporter.log("Under 'Type' field, value is dispaying as: " + typeValue);
			}

			// Service
			String serviceValue = getTextFrom(Lanlink_InternationalObj.International.PAMTest_ServiceValue);
			if (serviceValue.isEmpty()) {
				Reporter.log("No values displaying under 'Service' field");
			} else {
				serviceName.equals(getTextFrom(Lanlink_InternationalObj.International.PAMTest_ServiceValue));
			}

			// site
			String siteValue = getTextFrom(Lanlink_InternationalObj.International.PAMTest_siteValue);
			if (siteValue.isEmpty()) {
				Reporter.log("No values displaying under 'Site' field");
			} else {
				siteName.equals(getTextFrom(Lanlink_InternationalObj.International.PAMTest_siteValue));
			}

			// Tool Response
			String toolResponse = getTextFrom(Lanlink_InternationalObj.International.PAMTest_ToolResponse);
			if (toolResponse.isEmpty()) {
				Reporter.log("No values displaying under 'Tool Response' field");
			} else {
				Reporter.log("Under 'Tool Response' field, value is dispaying as: " + toolResponse);
			}

			// click on "X"button to close the popup
			click(Lanlink_InternationalObj.International.configure_alertPopup_xbutton, "X");

		} else {
			Reporter.log("'PAM Test' popup page is not displaying");
		}
	} catch (Exception e) {
		e.printStackTrace();
		Reporter.log("'PAM Test' popup page is not displaying");
	}
}

public void deleteCircuit() throws IOException, InterruptedException {

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	click(Lanlink_InternationalObj.International.PAMTest_deleteButton, "deleteLink");

	click(Lanlink_InternationalObj.International.deleteCircuit_deleteButton, "deleteCircuitpopup_deleteLink");
	waitforPagetobeenable();

	waitToPageLoad();

	verifysuccessmessage("Circuit deleted successfully");

}

public void verifyAddDSLAMandHSLlink() throws InterruptedException {

	boolean actelisConfigurationPanel = false;

	waitToPageLoad();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	actelisConfigurationPanel = isElementPresent(Lanlink_InternationalObj.International.ActelisConfigurationPanel);

	if (actelisConfigurationPanel) {
		Reporter.log(" 'Actelis Configuration' panel is displaying as expected");

		boolean actelisLink = false;
		try {
			actelisLink = isElementPresent(Lanlink_InternationalObj.International.Actelisconfig_addDSLAM);
			if (actelisLink) {

				click(Lanlink_InternationalObj.International.Actelisconfig_addDSLAM);
				waitforPagetobeenable();

			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	} else {
		// Actelis Configuration' panel is not displaying
	}

}

public void AddDSLAMandHSL() throws InterruptedException, IOException {

	waitToPageLoad();

	click(Lanlink_InternationalObj.International.addDSLAMandHSL_xButton);
	Reporter.log("Clicked on 'X' button under 'DSLAM device' dropdown");

///	sendKeys(Lanlink_InternationalObj.International.DSLM_Device_Select, DSLMdevice, "DSLMdevice");

	//String valueToSElect = getTextFrom(Lanlink_InternationalObj.International.selectDSLAMdeviceValue).replace("value",
	//		DSLMdevice);

	try {
	//	if (isElementPresent(valueToSElect)) {
		//	Reporter.log(DSLMdevice + " is displaying under 'DSLAM device' dropdown");

		//	click(valueToSElect);
			//Reporter.log(DSLMdevice + " is selected under 'DSLAM device' dropdown");

			waitToPageLoad();

			click(Lanlink_InternationalObj.International.List_HSL_Link, "List_HSL"); // click
																			// on
																			// "List
																			// HSL"
																			// button

		//	selectRowForAddingInterface_Actelis(HSlname); // select the
															// Interface

	//	}// else {
			//Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
	//	}

	} catch (Exception e) {
		e.printStackTrace();
		//Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
	}
}

public void showInterface_ActelisConfiguuration() throws IOException, InterruptedException {

	click(Lanlink_InternationalObj.International.showInterface_ActelisCnfiguration);
	waitforPagetobeenable();
}

public void deletInterface_ActelisConfiguration(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws IOException, InterruptedException {
	String interfaceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interfaceName");

	// select the interface
	//click(Lanlink_InternationalObj.International.deleteInterafce1 + interfaceName
	//		+ Lanlink_InternationalObj.International.deleteInterafce2);

	// click on Action button
	click(Lanlink_InternationalObj.International.AcionButton_ActelisConfiguration);

	// Remove Button
	click(Lanlink_InternationalObj.International.removeButton_ActelisConfiguration);

	boolean popupMessage = false;
	popupMessage = isElementPresent(Lanlink_InternationalObj.International.popupMessage_forRemove_ActelisConfiguration);

	if (popupMessage) {
		String actualmsg = getTextFrom(Lanlink_InternationalObj.International.popupMessage_forRemove_ActelisConfiguration);
		Reporter.log(" On clicking remoe button, popup message displays as: " + actualmsg);

		//click(Lanlink_InternationalObj.International.deleteInterfaceX);
		waitforPagetobeenable();
	} else {

		// popup message does not display after clicking on 'Remove' button
	}
}

public void successMessage_deleteInterfaceFromDevice_ActelisConfiguration() throws IOException {

	boolean successMessage = isElementPresent(
			Lanlink_InternationalObj.International.successMessage_ActelisConfiguration_removeInterface);
	String actualmessage = getTextFrom(
			Lanlink_InternationalObj.International.successMessage_ActelisConfiguration_removeInterface);
	if (successMessage) {

		Reporter.log(" Success Message for removing interface is dipslaying as expected");

		Reporter.log("Message displays as: " + actualmessage);

	} else {
		Reporter.log(" Success Message for removing Interface is not dipslaying");
	}
}

public void searchorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException

{
	waitforPagetobeenable();
	waitforPagetobeenable();
	String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "serviceNumber");

	verifyExists(Lanlink_InternationalObj.International.ManageCustomerServiceLink, " Manage Customer Service Link");
	mouseMoveOn(Lanlink_InternationalObj.International.ManageCustomerServiceLink);

	verifyExists(Lanlink_InternationalObj.International.searchorderlink, "search order link");
	click(Lanlink_InternationalObj.International.searchorderlink, "search order link");

	verifyExists(Lanlink_InternationalObj.International.servicefield, "service field");
	sendKeys(Lanlink_InternationalObj.International.servicefield, sid);

	click(Lanlink_InternationalObj.International.searchbutton, "searchbutton");
	// click(searchbutton,"searchbutton");

	verifyExists(Lanlink_InternationalObj.International.serviceradiobutton, "service radio button");
	click(Lanlink_InternationalObj.International.serviceradiobutton, "service radio button");

	verifyExists(Lanlink_InternationalObj.International.searchorder_actiondropdown, "search order actiodropdown");
	click(Lanlink_InternationalObj.International.searchorder_actiondropdown, "search order linksearch order actiodropdown");

	verifyExists(Lanlink_InternationalObj.International.view, "view");
	click(Lanlink_InternationalObj.International.view, "view");

}



public void addAccedianCircuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service Name");

	// Click on Add Accedian-1G Link
	click(Lanlink_InternationalObj.International.addCircuit_AccedianLink, "Add Accedian-1G");
	waitforPagetobeenable();
	waitToPageLoad();

	boolean overturePanelHeader = isElementPresent(Lanlink_InternationalObj.International.addCircuit_Accedian1Gpage_panel);
	if (overturePanelHeader) {
		Reporter.log("'Accedian-1G' page is displaying");

		sendKeys(Lanlink_InternationalObj.International.addOverture_serviceNameTextField, serviceName, "Service Name"); // Search
																												// ame
																												// text
																												// Field

		click(Lanlink_InternationalObj.International.addOverture_searchButton, "Search"); // Search
																				// Button
		waitforPagetobeenable();
		waitToPageLoad();

		String selectValueInTable = getTextFrom(Lanlink_InternationalObj.International.selectValueUnderAddOverturePage)
				.replace("value", serviceName);
		try {
			isElementPresent(selectValueInTable);
			Reporter.log("Records displays for the Service " + serviceName);

			click(selectValueInTable);
			waitforPagetobeenable();

			click(Lanlink_InternationalObj.International.addOverture_OKbutton, "OK");
			waitforPagetobeenable();
			waitToPageLoad();

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("No record displays for the Service " + serviceName);

			click(Lanlink_InternationalObj.International.addOverture_cancelButton, "cancel");
		}

	} else {
		Reporter.log("'Overture' page is not displaying");
	}
}

public void addAtricaCircuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service Name");
	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	// Click on Add Circuit Link
	click(Lanlink_InternationalObj.International.addCircuit_AtricaLink, "Add Circuit");
	waitforPagetobeenable();
	waitToPageLoad();

	boolean overturePanelHeader = isElementPresent(Lanlink_InternationalObj.International.addCircuit_Atricapage_Panel);
	if (overturePanelHeader) {
		Reporter.log("'Accedian-1G' page is displaying");

		sendKeys(Lanlink_InternationalObj.International.addOverture_serviceNameTextField, serviceName, "Service Name"); // Search
																												// ame
																												// text
																												// Field

		click(Lanlink_InternationalObj.International.addOverture_searchButton, "Search"); // Search
																				// Button
		waitforPagetobeenable();

		String selectValueInTable = getTextFrom(Lanlink_InternationalObj.International.selectValueUnderAddOverturePage)
				.replace("value", serviceName);
		try {
			isElementPresent(selectValueInTable);
			Reporter.log("Records displays for the Service " + serviceName);

			click(selectValueInTable);
			waitforPagetobeenable();

			click(Lanlink_InternationalObj.International.addOverture_OKbutton, "OK");
			waitforPagetobeenable();

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("No record displays for the Service " + serviceName);

			click(Lanlink_InternationalObj.International.addOverture_cancelButton, "cancel");
		}

	} else {
		Reporter.log("'Overture' page is not displaying");
	}
}

public void metro_ModularMSpselected(String ServiceIdentificationNumber,String SelectSubService,String Interfacespeed,String EndpointCPE,String Email,
		String PhoneContact,String remark,String PerformanceReporting,String ProactiveMonitoring,String deliveryChannel,String ManagementOrder,
		String	vpnTopology,String intermediateTechnology,String CircuitReference,String CircuitType,String notificationManagement,String perCocPerfrmReprt,String actelsBased,
		String standrdCir,String  standrdEir,String prmiumCir,String prmiumEir,String E_VPntechnology, String HCoSperformreporting)
		throws InterruptedException, IOException {

	scrollIntoTop();
	waitforPagetobeenable();

	// Service Identification
	createService_ServiceIdentification(ServiceIdentificationNumber);

	

	// Phone Contact
	createService_phone(PhoneContact);

	// COS checkbox
	//addCheckbox_commonMethod(Lanlink_InternationalObj.International.cosCheckbox_createService, "COS", COScheckbox);

	

	// Remark
	createService_remark(remark);

	scrollDown(Lanlink_InternationalObj.International.configurtoinptionpanel_webelementToScroll);

	// Performance Reporting
	if (!PerformanceReporting.equalsIgnoreCase("null")) {

		if (PerformanceReporting.equalsIgnoreCase("yes")) {

			boolean perfrmReprtFieldcheck = isElementPresent(
					Lanlink_InternationalObj.International.performanceReportingcheckbox);
			if (perfrmReprtFieldcheck) {
				Report.LogInfo("Info",
						" 'Performance reporting' checkbox is displaying under 'Manage ment options' panel in 'Create Service' page as exepcted",
						"PASS");

				boolean prfrmReportselection = isSelected(Lanlink_InternationalObj.International.performanceReportingcheckbox,
						"performance Reporting checkbox");
				if (prfrmReportselection) {
					Reporter.log("performance monitoring check box is selected");
					Report.LogInfo("Info", "Performance Reporting checkbox is selected as expected", "PASS");
				} else {
					Report.LogInfo("Info", "Performance Reporting checkbox is not selected", "FAIL");
				}

				// HCoS Performance reporting checkbox
				boolean HCoSperformancereportingFieldCheck = false;
				try {
					HCoSperformancereportingFieldCheck = isElementPresent(
							Lanlink_InternationalObj.International.HCoSperformanceReport);
					if (HCoSperformancereportingFieldCheck) {
						Report.LogInfo("Info",
								" 'HCoS Performance Reporting' checkbox is displaying, when 'Performance reporting' checkbox is selected",
								"PASS");
						if (HCoSperformreporting.equalsIgnoreCase("Yes")) {
							click(Lanlink_InternationalObj.International.HCoSperformanceReport);
							waitforPagetobeenable();

							boolean HCoSSelection = isSelected(Lanlink_InternationalObj.International.HCoSperformanceReport,
									"HCoSperformanceReport");
							if (HCoSSelection) {
								Report.LogInfo("Info",
										"'HCoS Performance Reporting checkbox is selected as expected", "PASS");
							} else {
								Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not selected",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not selected", "PASS");
						}
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not displaying", "PASS");
					Reporter.log(" 'HCoS Performance Reporting checkbox is not displaying");
				} catch (Exception ee) {
					ee.printStackTrace();
					Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not selected", "PASS");
					Reporter.log(" 'HCoS Performance Reporting checkbox is not seleted");
				}

				// Per CoS Performance Reporting chekcbox
				boolean perCoSPrfrmReprtFieldcheck = isElementPresent(
						Lanlink_InternationalObj.International.perCoSperformncereport);
				if (perCoSPrfrmReprtFieldcheck) {
					Report.LogInfo("Info",
							" 'Per CoS Performance Reporting' checkbox is displaying, when 'Performance reporting' checkbox is selected",
							"PASS");
					if (perCocPerfrmReprt.equalsIgnoreCase("Yes")) {
						click(Lanlink_InternationalObj.International.perCoSperformncereport);
						waitforPagetobeenable();

						boolean perCoSSelection = isSelected(Lanlink_InternationalObj.International.perCoSperformncereport,
								"perCoSperformncereport");
						if (perCoSSelection) {
							Report.LogInfo("Info", "Per CoS Performance Reporting checkbox is selected as expected",
									"PASS");
						} else {
							Report.LogInfo("Info", "Per CoS Performance Reporting checkbox is not selected",
									"FAIL");
						}

						// Actelis Based
						boolean actelisbasedFieldcheck = isElementPresent(
								Lanlink_InternationalObj.International.ActelisBasedcheckbox);
						if (actelisbasedFieldcheck) {
							Report.LogInfo("Info",
									" 'Actelis Based' checkbox is displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
									"PASS");
							if (actelsBased.equalsIgnoreCase("Yes")) {
								click(Lanlink_InternationalObj.International.ActelisBasedcheckbox);
								waitforPagetobeenable();

								boolean actelisSelection = isSelected(
										Lanlink_InternationalObj.International.ActelisBasedcheckbox, "ActelisBasedcheckbox");
								if (actelisSelection) {
									Report.LogInfo("Info", " 'Actelis Based' checkbox is selected as expected",
											"PASS");
								} else {
									Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected", "FAIL");
								}

								// Standard CIR Text Field
								createService_standardCIR(standrdCir);

								// Standard EIR Text Field
								createService_standardEIR(standrdEir);

								// Premium CIR Text Field
								createService_premiumCIR(prmiumCir);

								// Premium EIR Text Field
								createService_premiumEIR(prmiumEir);

							} else {
								Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected as expected",
										"PASS");
							}

						} else {
							Report.LogInfo("Info",
									" 'Actelis Based' checkbox is not displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
									"FAIL");
						}

					} else {
						Report.LogInfo("Info",
								" 'Per CoS Performance Reporting' checkbox is not selected as exepected", "PASS");
					}

				} else {
					Report.LogInfo("Info",
							" 'Per CoS Performance Rpeorting' checkbox is not displaying, when 'Performance reporting' checkbox is selected",
							"FAIL");
				}

			} else {
				Report.LogInfo("Info", " 'Performance Reporting' checkbox is not available", "FAIL");
			}

		} else {

			Reporter.log("Performance Repoting is not selected");
			Report.LogInfo("Info", "performance Reporting checkbox is not selected as expected", "PASS");

		}

	}

	// Pro active Monitoring
	createService_proactivemonitoring(ProactiveMonitoring, notificationManagement);

	// Scroll Up till phone contact field
	scrollDown(Lanlink_InternationalObj.International.ServiceIdentification);

	waitforPagetobeenable();

	// Delivery Channel
	//createService_deliveryChannel(deliveryChannel);

	// Management Order
	createService_managementOptions(ManagementOrder);

	

	////// scrolltoend();
	waitforPagetobeenable();

	// VPN topology
	boolean vpNTopology = isElementPresent(Lanlink_InternationalObj.International.VPNtopology);
	if (vpNTopology) {
		Report.LogInfo("Info", " 'VPN Topology' dropdown is dsiplaying as expected", "PASS");
		if (!vpnTopology.equalsIgnoreCase("null")) {

			click(Lanlink_InternationalObj.International.VPNtopology_xbutton);
			waitforPagetobeenable();

			if (vpnTopology.equals("Point-to-Point")) {

				
				Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

				// Intermediate technology
				createService_intermediateTechnology(intermediateTechnology);

				// Circuit Reference
				createService_circuitreference(CircuitReference);

				// Circuit Type
				createSerivce_circuitType(CircuitType);
			}

			else if (vpnTopology.equals("E-PN (Any-to-Any)")) {

				click(Lanlink_InternationalObj.International.fieldValidation1 + vpnTopology
						+ Lanlink_InternationalObj.International.fieldValidation2);
				waitforPagetobeenable();

				Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

				// Circuit Reference
				createService_circuitreference(CircuitReference);

			} else if (vpnTopology.equals("Hub&Spoke")) {

				click(Lanlink_InternationalObj.International.fieldValidation1 + vpnTopology
						+ Lanlink_InternationalObj.International.fieldValidation2);
				waitforPagetobeenable();

				Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

				Reporter.log("NO additional fields display");

			}

			else {

				Reporter.log(vpnTopology + " is not available under VPN topoloy dropdown");
				Reporter.log(vpnTopology + " is not available inside the VPN topoloy dropdown");
				Report.LogInfo("Info", vpnTopology + " is not available under VPN topoloy dropdown", "FAIL");
			}

		} else {
			Report.LogInfo("Info",
					" No value provided for 'VPN topology' dropdown. 'point -to -point' is selected by default",
					"PASS");

			// Intermediate technology
			createService_intermediateTechnology(intermediateTechnology);

			// Circuit Reference
			createService_circuitreference(CircuitReference);

			// Circuit Type
			createSerivce_circuitType(CircuitType);
		}
	} else {
		Report.LogInfo("Info",
				" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
				"FAIL");
		Reporter.log(
				"'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page");
	}

	click(Lanlink_InternationalObj.International.okbutton, "OK");

}

public void verifydataEntered_Metro_MSPselected(String serviceype, String ServiceIdentificationNumber,
		String SelectSubService, String Interfacespeed, String EndpointCPE, String Email, String PhoneContact,
		String remark, String PerformanceMonitoring, String ProactiveMonitoring, String deliveryChannel,
		String ManagementOrder, String vpnTopology, String intermediateTechnology, String CircuitReference,
		String CircuitType, String modularMSP, String perCocPerfrmReprt, String actelsBased, String standrdCir,
		String standrdEir, String prmiumCir, String prmiumEir, String notificationManagement,String EVPNtechnology,String HCoSPerformanceReporting)
		throws InterruptedException, IOException {

	scrollDown(Lanlink_InternationalObj.International.viewServicepage_OrderPanel);

	waitforPagetobeenable();

	/**
	 * Service Panel
	 */
	// Service Identification
	verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

	// Service type
	verifyEnteredvalues("Service Type", serviceype);

	// Service Subtype
	verifyEnteredvalues("Service Subtype", SelectSubService);

	// E_VPN technology
	verifyEnteredvalues("E-VPN Technology", EVPNtechnology);

	// Email
	verifyEnteredvalueForEmail_serviceCreationpage("Email", Email);

	// COS checkbox
	

	

	// Phone Contact
	
	// Modular MSP
	verifyEnteredvalues("Modular MSP", modularMSP);

	// Remark
	compareText("Remark", Lanlink_InternationalObj.International.remark_viewPage, remark);

	scrollDown(Lanlink_InternationalObj.International.viewServicepage_Servicepanel);

	waitforPagetobeenable();

	/**
	 * Management Options panel
	 */

	// Delivery Channel
	verifyEnteredvalues("Delivery Channel", deliveryChannel);

	// Proactive Monitoring
	verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

	if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
		verifyEnteredvalues("Notification Management Team", notificationManagement);
	}

	scrollDown(Lanlink_InternationalObj.International.viewServicepage_ManagementOptionsPanel);

	waitforPagetobeenable();

	// Performance Reporting
	verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);
String COScheckbox=null;
	if (COScheckbox.equalsIgnoreCase("Yes")) {

		if (vpnTopology.equals("Point-to-Point")) {

			if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
					& (actelsBased.equalsIgnoreCase("no"))) {

				verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

				verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

			}

			else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
					& (actelsBased.equalsIgnoreCase("no"))) {

				verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

				verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				verifyEnteredvalues("Actelis Based", actelsBased);
			}

			else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
					& (actelsBased.equalsIgnoreCase("yes"))) {

				verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

				verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				verifyEnteredvalues("Actelis Based", actelsBased);

				// Standard
				compareText("Header Name for 1st row", Lanlink_InternationalObj.International.standardHeaderName_viewPage,
						"Standard");

				compareText("Standard CIR", Lanlink_InternationalObj.International.standardCIRvalue_viewPage, standrdCir);

				compareText("Standard EIR", Lanlink_InternationalObj.International.standardEIRvalue_viewPage, standrdEir);

				// Premium
				compareText("Header Name for 2nd row", Lanlink_InternationalObj.International.PremisumHeaderName_viewPage,
						"Premium");

				compareText("Premium CIR", Lanlink_InternationalObj.International.premiumCIRvalue_viewPage, prmiumCir);

				compareText("Premium EIR", Lanlink_InternationalObj.International.premiumEIRvalue_viewPage, prmiumEir);

			}
		} else {

			if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
					& (actelsBased.equalsIgnoreCase("no"))) {

				verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

				verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

			}

			else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
					& (actelsBased.equalsIgnoreCase("no"))) {

				verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

				verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				verifyEnteredvalues("Actelis Based", actelsBased);
			}

			else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
					& (actelsBased.equalsIgnoreCase("yes"))) {

				actelsBased = "No";

				verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

				verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				verifyEnteredvalues("Actelis Based", actelsBased);
			}

		}
	}

	// WebElement managementOptionsPanel=
	// getwebelement(xml.getlocator(Lanlink_InternationalObj.International.viewServicepage_ManagementOptionsPanel"));
	// ScrolltoElement(managementOptionsPanel);
	// waitforPagetobeenable();

	// ////scrolltoend();
	waitforPagetobeenable();

	/**
	 * Configuration options panel
	 */

	// VPN Topology
	verifyEnteredvalues("VPN Topology", vpnTopology);

	if (vpnTopology.equals("Point-to-Point")) {

		// Circuit Reference
		verifyEnteredvalues("Circuit Reference", CircuitReference);

		// Circuit Type
		verifyEnteredvalues("Circuit Type", CircuitType);
	}

	if (vpnTopology.equals("Hub&Spoke")) {

		Reporter.log("only vpn topology displays under view Service page");
		Report.LogInfo("Info",
				" Only VPN topology field displays under 'Configuration' panel, when 'Hub&Spoke' is selected",
				"PASS");

	}

	if (vpnTopology.equals("E-PN (Any-to-Any)")) {

		// Circuit Reference
		verifyEnteredvalues("Circuit Reference", CircuitReference);
	}
}


public void technologyDropdown_p2p_mspselected() throws InterruptedException, IOException {

	String[] Technology = { "Actelis", "Atrica", "Overture", "Accedian", "Cyan" };

	boolean technology;

	// Technology dropdown
	technology = isElementPresent(Lanlink_InternationalObj.International.Addsiteorder_Technology);
//	sa.assertTrue(technology, "Technology dropdown is not displayed");

	click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
	List<String> listoftechnology = new ArrayList<>(
			Arrays.asList(getTextFrom(Lanlink_InternationalObj.International.ClassNameForDropdowns)));

	if (listoftechnology.size() >= 1) {
		for (String technologytypes : listoftechnology) {

			Reporter.log("list of technology are : " + technologytypes);
			Report.LogInfo("Info",
					"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
					"PASS");
			Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
			String technologyValue = technologytypes;
		}

		for (int k = 0; k < Technology.length; k++) {
			// Actelis
			if (Technology[k].equalsIgnoreCase("Actelis")) {

				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				click("//div[text()='" + Technology[k] + "']");

			}

			// Atrica
			else if (Technology[k].equalsIgnoreCase("Atrica")) {

				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
			//	clickonTechnology(technologySelected, Technology[k]);

				// Device Name
				verifySiteOrderField_deviceName();
			}

			// Overture
			else if (Technology[k].equalsIgnoreCase("Overture")) {

				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
			//	clickonTechnology(technologySelected, Technology[k]);
			}

			// Accedian
			else if ((Technology[k].equalsIgnoreCase("Accedian"))) {

				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
			//	clickonTechnology(technologySelected, Technology[k]);

			}

			// Cyan
			else if (Technology[k].equalsIgnoreCase("Cyan")) {

				click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
			//	clickonTechnology(technologySelected, Technology[k]);

			}
		}
	} else {

		Reporter.log("no values are available inside technology dropdown for Add site order");
		Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
	}
}

public void verifySiteOrderForHubAndSpoke_trafficAggregatorSelected() throws InterruptedException, IOException{
	click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
	
	
	
}


public void verifySiteOrderForE_PN_trafficAggregatorSelected() throws InterruptedException, IOException{
	
	ExtentTestManager.getTest().log(LogStatus.INFO, " Site order functions will be performed for 'VPN Topology' --> E-PN (Any to Any)");
	
}

public void verifySiteOrderForPoint_to_point_extendedCircuit

(String interfaceSpeed, String modularMSP) throws InterruptedException, IOException {

	try {

		String[] IVrefrence = { "Primary", "Access" };

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Circuit Reference Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

		// IV Reference Error Messages
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_IVReferenceErrmsg, "IV Reference");

		// CSR name Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg, "CSR Name");

		// Technology Error message
		verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		waitforPagetobeenable();

		// Site Order Number Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

		// Country Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

		// City Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

		// Validate Country dropdown
		Reporter.log("validate Country dropdown");
		validateCountry_AddSiteorder();

		// Validate City Fields
		Reporter.log("Validate city fields");
		validateCity_AddSiteOrder();

		// Validate Site/CSR field
		Reporter.log("validate Site Fields");
		validateSite_AddSiteOrder();

		//// scrolltoend();
		waitforPagetobeenable();

		// Validate performance reporting dropdown
		Reporter.log("validate performance reporting checkbox");
		validatePerformancereporting_AddSiteOrder();

		// validate proactive Monitoring dropdown
		Reporter.log("validate proactive monitoring checkbox");
		validateProactiveMonitoring_AddSiteOrder();

		// Validate Smarts monitoring dropdown
		Reporter.log("validate Smarts monitoring checkbox");
		validateSmartsMOnitoring_AddSiteOrder();

		//// scrolltoend();
		waitforPagetobeenable();

		// Validate Site Alias field
		Reporter.log("validate Site Alias fields");
		validateSiteAlias_AddSiteOrder();

		// Validate VLAN Id field
		Reporter.log("validate VLAn Id fields");
		validateVlanID_AddSiteOrder();

		// Validate DCA Enabled Site and Cloud Service Provider dropdown
		Reporter.log("validate DCA enabled site checkbox");
		valiadateDCAEnabledsite_AddSieOrder();

		// Verify Remark field
		Reporter.log("validate Remark fields");
		validateRemark_AddSiteOrder();

		if (interfaceSpeed.equals("1GigE")) {

			technologyDropdownFor1GigE();
		}

		else if (interfaceSpeed.equals("10GigE")) {

			technologyDropdownFor10GigE();
		}
		// Validate OK button
		OKbutton_AddSiteOrder();

		// validate cancel button
		cancelbutton_AddSiteOrder();

		waitforPagetobeenable();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_InternationalObj.International.Addsiteorder_cancel);
		waitforPagetobeenable();

	//	sa.assertAll();

	} catch (AssertionError e) {
		e.printStackTrace();
	}
}

public void verifySiteOrderForHubAndSpoke(String interfaceSpeed, String modularMSP)
		throws InterruptedException, IOException {

	try {

		String[] IVrefrence = { "Primary", "Access" };

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Circuit Reference Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

		// IV Reference Error Messages
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_IVReferenceErrmsg, "IV Reference");

		// CSR name Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg, "CSR Name");

		// Technology Error message
		verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		waitforPagetobeenable();

		// Site Order Number Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

		// Country Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

		// City Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

		// Validate site order Number Field
	//	validatesiteOrderNumber_AddSiteOrder();

		// Validate Country dropdown
		validateCountry_AddSiteorder();

		// Validate City Fields
		validateCity_AddSiteOrder();

		// Validate Site/CSR field
		validateSite_AddSiteOrder();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		// Validate performance reporting dropdown
		validatePerformancereporting_AddSiteOrder();

		// validate proactive Monitoring dropdown
		validateProactiveMonitoring_AddSiteOrder();

		// Validate Smarts monitoring dropdown
		validateSmartsMOnitoring_AddSiteOrder();

		
		// validate EPN EOSDH checkbox
		if (modularMSP.equalsIgnoreCase("yes")) {


		} else {
			Reporter.log("Spoke id is not displaying, if modular msp is selected");
		}

		// Validate Site Alias field
		validateSiteAlias_AddSiteOrder();

		// Verify Remark field
		validateRemark_AddSiteOrder();

		//// scrolltoend();
		waitforPagetobeenable();
		if (modularMSP.equalsIgnoreCase("yes")) {

			for (int i = 0; i < IVrefrence.length; i++) {
				if (IVrefrence[i].equals("Primary")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
					waitforPagetobeenable();

				//	technologyDropdown_mspSelected_HubAndSpoke_Primary();
				}

				else if (IVrefrence[i].equals("Access")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
					waitforPagetobeenable();

				//	technologyDropdown_MSPselected_HubAndSpoke_Access();
				}
			}

		} else {
			if ((interfaceSpeed.equals("1GigE"))) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

						
					}

					else if (IVrefrence[i].equals("Access")) {
					
						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

						
					}
				}
			}

			else if (interfaceSpeed.equals("10GigE")) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

					//	technologyDropdownFor10GigE_HubAndSpoke_primary();

					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

						
					}
				}
			}

		}

		// Validate OK button
		OKbutton_AddSiteOrder();

		// validate cancel button
		cancelbutton_AddSiteOrder();

		waitforPagetobeenable();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_InternationalObj.International.Addsiteorder_cancel);
		waitforPagetobeenable();

		// sa.assertAll();
	}

	catch (AssertionError e) {
		e.printStackTrace();
	}
}



public void verifySiteOrderForHubAndSpoke_MSPselectedAndoffnet(String interfaceSpeed, String modularMSP)
		throws InterruptedException, IOException {

	try {

		String[] IVrefrence = { "Primary", "Access" };

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Circuit Reference Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

		// IV Reference Error Messages
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_IVReferenceErrmsg, "IV Reference");

		// CSR name Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg, "CSR Name");

		// Technology Error message
		verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		waitforPagetobeenable();

		// Site Order Number Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

		// Country Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

		// City Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

		// Validate site order Number Field
	//	validatesiteOrderNumber_AddSiteOrder();

		// Validate Country dropdown
		validateCountry_AddSiteorder();

		// Validate City Fields
		validateCity_AddSiteOrder();

		// Validate Site/CSR field
		validateSite_AddSiteOrder();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		// Validate performance reporting dropdown
		validatePerformancereporting_AddSiteOrder();

		// validate proactive Monitoring dropdown
		validateProactiveMonitoring_AddSiteOrder();

		// Validate Smarts monitoring dropdown
		validateSmartsMOnitoring_AddSiteOrder();

	


		// Validate Site Alias field
		validateSiteAlias_AddSiteOrder();

		// Verify Remark field
		validateRemark_AddSiteOrder();

		if (modularMSP.equalsIgnoreCase("yes")) {

			for (int i = 0; i < IVrefrence.length; i++) {
				if (IVrefrence[i].equals("Primary")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
					waitforPagetobeenable();

				//	technologyDropdown_mspSelected_HubAndSpoke_Primary();
				}

				else if (IVrefrence[i].equals("Access")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
					waitforPagetobeenable();

					// Access Circuit ID
				//	validateAccessCircuitID_AddSiteOrder();

					// Circuit Reference
				//	circuitreferenceDisabled_EPN();

				//	technologyDropdown_MSPselected_HubAndSpoke_Access();
				}
			}

		}

		// Validate OK button
		OKbutton_AddSiteOrder();

		// validate cancel button
		cancelbutton_AddSiteOrder();

		waitforPagetobeenable();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_InternationalObj.International.Addsiteorder_cancel);
		waitforPagetobeenable();


	} catch (AssertionError e) {
		e.printStackTrace();
	}
}


public void verifySiteOrderForHubAndSpoke_offnetSelected(String interfaceSpeed, String modularMSP)
		throws InterruptedException, IOException {

	try {

		String[] IVrefrence = { "Primary", "Access" };

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Circuit Reference Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

		// IV Reference Error Messages
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_IVReferenceErrmsg, "IV Reference");

		// CSR name Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg, "CSR Name");

		// Technology Error message
		verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		waitforPagetobeenable();

		// Site Order Number Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

		// Country Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

		// City Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

		// Validate site order Number Field
	//	validatesiteOrderNumber_AddSiteOrder();

		// Validate Country dropdown
		validateCountry_AddSiteorder();

		// Validate City Fields
		validateCity_AddSiteOrder();

		// Validate Site/CSR field
		validateSite_AddSiteOrder();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		// Validate performance reporting dropdown
		validatePerformancereporting_AddSiteOrder();

		// validate proactive Monitoring dropdown
		validateProactiveMonitoring_AddSiteOrder();

		// Validate Smarts monitoring dropdown
		validateSmartsMOnitoring_AddSiteOrder();

		



		// Validate Site Alias field
		validateSiteAlias_AddSiteOrder();

		// Verify Remark field
		validateRemark_AddSiteOrder();


		if (interfaceSpeed.equals("1GigE")) {


		} else {
			Reporter.log(" 'EPN EOSDH' checkbox does not display for 10G interface speed");
		}

		// Validate Site Alias field
		validateSiteAlias_AddSiteOrder();

		// Verify Remark field
		validateRemark_AddSiteOrder();

		

		if ((interfaceSpeed.equals("1GigE"))) {

			for (int i = 0; i < IVrefrence.length; i++) {
				if (IVrefrence[i].equals("Primary")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_offnetCheckbox);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
					waitforPagetobeenable();
					//technologyDropdownFor1GigE_HubAndSpoke_Primary_offnetselected("Primary");
				}

				else if (IVrefrence[i].equals("Access")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_offnetCheckbox);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
					waitforPagetobeenable();
					//technologyDropdownFor1GigE_HubAndSpoke_Access_offnetselected("Access");
				}
			}
		}

		else if (interfaceSpeed.equals("10GigE")) {

			click(Lanlink_InternationalObj.International.Addsiteorder_offnetCheckbox);

		}

		// Validate OK button
		OKbutton_AddSiteOrder();

		// validate cancel button
		cancelbutton_AddSiteOrder();

		waitforPagetobeenable();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_InternationalObj.International.Addsiteorder_cancel);
		waitforPagetobeenable();


	} catch (AssertionError e) {
		e.printStackTrace();
	}
}
public void verifySiteOrderForE_PN(String interfaceSpeed, String modularMSP)
		throws InterruptedException, IOException {

	try {

		String[] IVrefrence = { "Primary", "Access" };

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Circuit Reference Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

		// IV Reference Error Messages
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_IVReferenceErrmsg, "IV Reference");

		// CSR name Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg, "CSR Name");

		// Technology Error message
		verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		waitforPagetobeenable();

		// Site Order Number Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

		// Country Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

		// City Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

		
		// Validate Country dropdown
		validateCountry_AddSiteorder();

		// Validate City Fields
		validateCity_AddSiteOrder();

		// Validate Site/CSR field
		validateSite_AddSiteOrder();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		// Validate performance reporting dropdown
		validatePerformancereporting_AddSiteOrder();

		// validate proactive Monitoring dropdown
		validateProactiveMonitoring_AddSiteOrder();

		// Validate Smarts monitoring dropdown
		validateSmartsMOnitoring_AddSiteOrder();

		// Validate IV Reference dropdown
	//	validateIVReference_AddSiteorder();

		// Validate Circuit reference Field
	//	validateCircuitreference_AddSiteOrder();

		// Validate EPN Offnet checkbox
	//	validateEPNoffnet_AddSiteOrder();

		// validate EPN EOSDH checkbox
		if (modularMSP.equalsIgnoreCase("yes")) {

		//	validateEPNEOSDH_AddSiteOrder();

		} else {
			if (interfaceSpeed.equals("1GigE")) {

			//	validateEPNEOSDH_AddSiteOrder();

			} else {
				Reporter.log(" 'EPN EOSDH' checkbox does not display for 10G interface speed");
			}
		}

		// Validate Site Alias field
		validateSiteAlias_AddSiteOrder();

		// Verify Remark field
		validateRemark_AddSiteOrder();

		if (modularMSP.equalsIgnoreCase("yes")) {

			for (int i = 0; i < IVrefrence.length; i++) {
				if (IVrefrence[i].equals("Primary")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
					waitforPagetobeenable();

				//	technologyDropdown_MSPselected_Primary();
				}

				else if (IVrefrence[i].equals("Access")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
					waitforPagetobeenable();

					// Access Circuit ID
				//	validateAccessCircuitID_AddSiteOrder();

					// Circuit Reference
				//	circuitreferenceDisabled_EPN();

					//technologyDropdown_MSPselected_EPN_Access();
				}
			}

		} else {
			if ((interfaceSpeed.equals("1GigE"))) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

					//	technologyDropdownFor1GigE_EPN_Primary();
					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

						//technologyDropdownFor1GigE_EPN_Access();
					}
				}
			}

			else if (interfaceSpeed.equals("10GigE")) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

						//technologyDropdownFor10GigE_HubAndSpoke_primary();

					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

					//	technologyDropdownFor10GigE_HubAndSpoke_Access();
					}
				}
			}

		}

		// Validate OK button
		OKbutton_AddSiteOrder();

		// validate cancel button
		cancelbutton_AddSiteOrder();

		waitforPagetobeenable();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_InternationalObj.International.Addsiteorder_cancel);
		waitforPagetobeenable();

	

	} catch (AssertionError e) {
		e.printStackTrace();
	}
}



public void verifySiteOrderForEPN_EOSDHselected(String interfaceSpeed, String modularMSP)
		throws InterruptedException, IOException {

	try {

		String[] IVrefrence = { "Primary", "Access" };

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		click(Lanlink_InternationalObj.International.obutton_spanTag, "OK");
		waitforPagetobeenable();

		// Circuit Reference Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

		// IV Reference Error Messages
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_IVReferenceErrmsg, "IV Reference");

		// CSR name Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_csrnameErrmsg, "CSR Name");

		// Technology Error message
		verifyExists(Lanlink_InternationalObj.International.Addsitieorder_technologyErrmsg, "Technology");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		waitforPagetobeenable();

		// Site Order Number Error Message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

		// Country Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_countryerrmsg, "Country");

		// City Error message
		verifyExists(Lanlink_InternationalObj.International.Addsiteorder_devciexngCityErrmsg, "City");

		
		// Validate Country dropdown
		validateCountry_AddSiteorder();

		// Validate City Fields
		validateCity_AddSiteOrder();

		// Validate Site/CSR field
		validateSite_AddSiteOrder();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		// Validate performance reporting dropdown
		validatePerformancereporting_AddSiteOrder();

		// validate proactive Monitoring dropdown
		validateProactiveMonitoring_AddSiteOrder();

		// Validate Smarts monitoring dropdown
		validateSmartsMOnitoring_AddSiteOrder();

		// Validate IV Reference dropdown
	//	validateIVReference_AddSiteorder();

		// Validate Circuit reference Field
	//	validateCircuitreference_AddSiteOrder();

		// Validate EPN Offnet checkbox
	//	validateEPNoffnet_AddSiteOrder();

		// validate EPN EOSDH checkbox
		if (modularMSP.equalsIgnoreCase("yes")) {

		//	validateEPNEOSDH_AddSiteOrder();

		} else {
			if (interfaceSpeed.equals("1GigE")) {

				//validateEPNEOSDH_AddSiteOrder();

			} else {
				Reporter.log(" 'EPN EOSDH' checkbox does not display for 10G interface speed");
			}
		}

		// Validate Site Alias field
		validateSiteAlias_AddSiteOrder();

		// Verify Remark field
		validateRemark_AddSiteOrder();

		if (modularMSP.equalsIgnoreCase("yes")) {

			for (int i = 0; i < IVrefrence.length; i++) {
				if (IVrefrence[i].equals("Primary")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
					waitforPagetobeenable();

				//	technologyDropdown_MSPselected_Primary();
				}

				else if (IVrefrence[i].equals("Access")) {

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
					waitforPagetobeenable();

					click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Access);
					waitforPagetobeenable();

					// Access Circuit ID
				//	validateAccessCircuitID_AddSiteOrder();

					// Circuit Reference
				//	circuitreferenceDisabled_EPN();

					//technologyDropdown_MSPselected_EPN_Access();
				}
			}

		} else {
			if ((interfaceSpeed.equals("1GigE"))) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_EPNEOSDHCheckbox);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();
					//	technologyDropdownFor1GigE_EPNEOSDHselected_Primary();
					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_InternationalObj.International.Addsiteorder_EPNEOSDHCheckbox);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_InternationalObj.International.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();
					//	technologyDropdownFor1GigE_EPNEOSDHselected_Access();
					}
				}
			}

			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_InternationalObj.International.Addsiteorder_cancel);
			waitforPagetobeenable();

		//	sa.assertAll();

		}
	} catch (AssertionError e) {
		e.printStackTrace();
	}
}

public void addSiteOrderValues_point2point(String interfaceSpeed,
			String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection, String modularMSP) throws InterruptedException, IOException {
 	
 	 scrollUp();
 	waitforPagetobeenable();
 	 
 	 Countyr_AddSiteOrder(country);
 	 
 	 City_AddSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode, sitevalue, CSR_Name, existingsiteselection, newsiteselection);

// 	 Site_AddSiteOrder(application, existingsiteselection, sitevalue, newsiteselection, CSR_Name);

 //	 scrolltoend();
 	waitforPagetobeenable();
 	 
 	 performancereporting_AddSiteOrder( performReport);

 	 proactiveMonitoring_AddSiteOrder( ProactiveMonitor);

 	 smartsMonitoring_AddSiteOrder( smartmonitor);

//  scrolltoend();
 	waitforPagetobeenable();
 
 	 SiteAlias_AddSiteOrder( siteallias);
 	 
 	 VLANid_AddSiteOrder( VLANid);	 	 
 	 
 	 DCAEnabledSite_AddSiteOrder( DCAenabledsite, cloudserviceprovider);
 	 
 	 remark_AddSiteOrder (remark);
 	 if(modularMSP.equalsIgnoreCase("yes")) {
		 
		 technologyP2P_MSPselected_AddSiteOrder(technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);
		 
	 }else {
 	 technologyP2P_AddSiteOrder( technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);
	 }
 	waitforPagetobeenable();
 	// scrolltoend();
		verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");

		click(Lanlink_InternationalObj.International.okbutton, "OK");
 	 

  }

public void technologyP2P_MSPselected_AddSiteOrder(String technology, String interfaceSpeed, String devicename, String nonterminatepoinr, String Protected) throws InterruptedException, IOException {
	 
 	//Technology
		if(technology.equalsIgnoreCase("null")) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Technology dropdown is a mandatory field and no values are provided"); 
		}else {
		
			
		if(technology.equals("Actelis") || technology.equals("Atrica") || technology.equals("Overture") || technology.equals("Accedian") || technology.equals("Cyan" ))	{
			
			click(Lanlink_InternationalObj.International.Addsiteorder_Technology, "Technology dropdown");
			waitforPagetobeenable();
			WebElement technologySelected = findWebElement((Lanlink_InternationalObj.International.selectValueUnderTechnologyDropdown).replace("value", technology)); 
			clickonTechnology(technologySelected, technology);
			waitforPagetobeenable();
			ExtentTestManager.getTest().log(LogStatus.PASS, technology + " is selected under technology dropdown");
			
			if(technology.equals("Actelis")) {	
				
			     Reporter.log("No additional fields displays");
			}
			

			else if(technology.equals("Atrica")) {
				
				//Device name
					devicename_AddSiteOrder(devicename);

			}
			
			

			else if(technology.equals("Overture") || technology.equals("Accedian-1G")) {	

				Reporter.log("No additional fields displays");	
			}

			else if(technology.equals("Cyan")) {	

				Reporter.log("No additional fields displays");	
				nontermination_AddSiteorder(nonterminatepoinr);
			}
			
			
		}
	}
		}


public void editSiteOrder_P2P_1G(String performReport, String ProactiveMonitor, String smartmonitor,
		String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology, String nontermination, 
		String Protected, String devicename, String remark)
		throws InterruptedException, IOException {
	
//Performance Reporting
	editSiteOrder_Performancereporting(performReport);
	
	 
//Pro active Monitoring 
	editSiteOrder_proactiveMonitoring( ProactiveMonitor);

	 
//Smart Monitoring
	editSiteOrder_smartMonitoring( smartmonitor);

//Vlan id
	editsiteOrder_vlanid( VLANid);
	
//Site alias
	editsiteorder_sitealias( siteallias);
	
	//();
	waitforPagetobeenable();
	
//DCA Enabled Site 
	editesiteOrder_DcaEnabled( DCAenabledsite, cloudserviceprovider);

	
//Remark
	editsiteOrder_remark( remark);
	
//Technology
	editSiteOrder_technology(technology);
	 
	if(technology.equalsIgnoreCase("Actelis")) {
		Reporter.log(" NO additional fields display for technology Actelis");
	}
	
	
	if(technology.equalsIgnoreCase("Atrica")) {
		 
		//Non-termination point
		 editsiteorder_NonterminationPoint( nontermination); 
		
		
		 
	//	 Device Name
	//	 editSiteOrder_deviceName( devicename);
		
	 }
	
	if(technology.equalsIgnoreCase("Overture")) {
		
		//Non-termination point
		 editsiteorder_NonterminationPoint( nontermination); 
		
		 
	}
	
	
	if(technology.equalsIgnoreCase("Alu")) {
		
		 //Device Name
		 editSiteOrder_deviceName( devicename);
		
	}
	
	if(technology.equalsIgnoreCase("Accedian-1G")) {
		
		//Non-termination point
		 editsiteorder_NonterminationPoint( nontermination); 
		
		
		
	}
	
	if(technology.equalsIgnoreCase("Cyan")) {
		
		//Non-termination point
		 editsiteorder_NonterminationPoint( nontermination); 
		
	}
	
	scrollDown(Lanlink_InternationalObj.International.okbutton);
waitforPagetobeenable();
	verifyExists(Lanlink_InternationalObj.International.okbutton, "OK");
	click(Lanlink_InternationalObj.International.okbutton, "OK");
	
	waitforPagetobeenable();
	
	
}


public void verifyValuesforCPEexistingdevice_MSPselected(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws IOException, InterruptedException {
	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
	String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
	String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Management Address");
	String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String VLANId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Name + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + RouterId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vendor_Model + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Snmpro + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + ManagementAddress + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + MEPId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + VLANId + Lanlink_InternationalObj.International.viewPage2);
}


public void eDITCPEdevicedetailsentered_MSPselected(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	String managementAddres = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");
	String VLANId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");

	Reporter.log("Entered edit functionalitty");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.viewPCEdevice_Actiondropdown, "Action");
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.EditCPEdevicelinkunderviewpage);
	Reporter.log("edit functionality worked");

	waitToPageLoad();

	// Name field
	device_editnamefield(cpename);

	// vendor/model
	device_editVendorModelField(vender);

	// Snmpro
	device_editSnmproField();

	

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.okbutton, "OK");
	waitforPagetobeenable();

}


public void verifyFieldsandAddCPEdevicefortheserviceselected_MSPselected(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String mediaSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Topology");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Topology");
	String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	waitforPagetobeenable();

	click(Lanlink_InternationalObj.International.CPEdevice_adddevicelink);
	waitforPagetobeenable();

	if (technologySelected.equalsIgnoreCase("Accedian")) {
		

	//equip_addDevice_MSPselected(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm,
				//mediaSelection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
			//	newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue, VLANid);
	}

}

public void verifydetailsEnteredforCPEdevice_MSPselected(String testDataFile, String sheetName, String scriptNo,
		String dataSetNo) throws InterruptedException, IOException {
	String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpename");
	String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Media selection");
	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"existingmanagementAddress");
	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"newmanagementAddress");
	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"manageaddressdropdownvalue");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"managementAddress");
	String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
	String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String VLANId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");

	

	String[] RouterId = new String[2];
	RouterId = cpename.split(".lanlink");

	String RouterIdValue = RouterId[0];

	String mediaSelectionValueInViewDevicePage = "no";
	if (Mediaselection.equalsIgnoreCase("null")) {
		Mediaselection = mediaSelectionValueInViewDevicePage;
	} else {
		Mediaselection = mediaSelectionValueInViewDevicePage;
	}

	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Name + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + RouterIdValue + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Vendor_Model + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + Snmpro + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(Lanlink_InternationalObj.International.viewPage1 + VLANId + Lanlink_InternationalObj.International.viewPage2);
	verifyExists(
			Lanlink_InternationalObj.International.viewPage1 + newmanagementAddress + Lanlink_InternationalObj.International.viewPage2);

	// Management Address
	if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
		verifyExists(Lanlink_InternationalObj.International.viewPage1 + manageaddressdropdownvalue
				+ Lanlink_InternationalObj.International.viewPage2);
	} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
			&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
		verifyExists(
				Lanlink_InternationalObj.International.viewPage1 + managementAddress + Lanlink_InternationalObj.International.viewPage2);
	}

	// verifyExists(Lanlink_InternationalObj.International.viewPage1+MEPId+Lanlink_InternationalObj.International.viewPage2);

}





public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_MSPselected(String testDataFile,
		String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String technologySelectedfordevicecreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
			dataSetNo, "Topology");

	if ((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian"))) {
	}

}

public void EDITCPEdevicedforIntermediateEquipment_MSPselected(String testDataFile, String sheetName,
		String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String VLAnid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String newPremiseselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String ExistingPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Topology");
	String NewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String NewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String ExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String NewSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String ExistingSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Topology");
	String ExistingCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"Topology");
	String newCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");

	((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();

	if ((technologySelected.equalsIgnoreCase("Accedian"))) {

	}

}


}
