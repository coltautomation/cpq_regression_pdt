package testHarness.siebelFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelAccounts extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void NavigateToAccounts() throws InterruptedException, IOException 
	{
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelAccountsObj.Accounts.accountsTab, 10);
		verifyExists(SiebelAccountsObj.Accounts.accountsTab,"Account Tab");
		click(SiebelAccountsObj.Accounts.accountsTab,"Account Tab");
		waitForAjax();
		verifyExists(SiebelAccountsObj.Accounts.OCNTxb,"Account Number");	
	}
	
	public void SearchAccount(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Account_Number");
		verifyExists(SiebelAccountsObj.Accounts.OCNTxb,"Account Number");	
			sendKeys(SiebelAccountsObj.Accounts.OCNTxb, OCN, "Account Number Textbox");
			verifyExists(SiebelAccountsObj.Accounts.GoBtn,"Go Button");
			click(SiebelAccountsObj.Accounts.GoBtn,"Go Button");
			Reusable.waitForSiebelLoader();
			ClickonElementByString("//td[text()='"+OCN+"']//following-sibling::*//a", 30);
			Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelAccountsObj.Accounts.InstalledAssets_NewBtn, 10);
			verifyExists(SiebelAccountsObj.Accounts.InstalledAssets_NewBtn,"Installed Assets New Button");
			click(SiebelAccountsObj.Accounts.InstalledAssets_NewBtn,"Installed Assets New Button");
			Reusable.waitForSiebelLoader();
		
		
	}	

	

}
