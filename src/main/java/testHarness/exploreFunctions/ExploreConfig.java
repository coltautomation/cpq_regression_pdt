package testHarness.exploreFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;
import pageObjects.exploreObjects.ExploreConfigObj;
import pageObjects.exploreObjects.ExploreLoginObj;
import testHarness.commonFunctions.ReusableFunctions;


public class ExploreConfig extends SeleniumUtils
{
	
	ReusableFunctions Reusable = new ReusableFunctions();
	GlobalVariables g = new GlobalVariables();

	ExploreLoginPage ExpLogin = new ExploreLoginPage();
	
	public String SearchFibrePlanningWork_Record(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws InterruptedException, AWTException, IOException 
	{
		String Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Request_ID");
		String B_End_Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Request_ID");
		
		waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.FibrePlanningQueueBtn,30);
		Reusable.Waittilljquesryupdated();
		verifyExists(ExploreConfigObj.ExploreConfiguration.FibrePlanningQueueBtn,"Fibre Planning Queue Button");
		click(ExploreConfigObj.ExploreConfiguration.FibrePlanningQueueBtn,"Fibre Planning Queue Button");
		Reusable.Waittilljquesryupdated();
		Reusable.Waittilljquesryupdated();
		Reusable.waitToPageLoad();

		if(SiteName.equalsIgnoreCase("A_End"))
		{
			ClickonElementByString("//a[text()='"+Request_ID+"']",120);	
		}
		else
		{
			ClickonElementByString("//a[text()='"+B_End_Request_ID+"']",120);	
		}		
		Reusable.Waittilljquesryupdated();
		
		String ExploreID = webDriver.findElement(By.xpath("//span[@data-dojo-attach-point='requestIdNode']")).getAttribute("value");
		System.out.println("ExploreID: "+ExploreID);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPQ_Common_ID", ExploreID);
		
		System.out.println("Record "+Request_ID+" Search in Fibre Planning Work Queue was successfull");
				
		return "True";
	}
	
	public String selectActionsFromExplore(String ActionType) throws IOException, InterruptedException 
	{
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.ActionsBtn,"'Action Button'");	
		Reusable.Waittilljquesryupdated();
		click(ExploreConfigObj.ExploreConfiguration.ActionsBtn,"'Action Button'");
		Reusable.waitForAjax();
		Reusable.Waittilljquesryupdated();
			
		switch (ActionType.toUpperCase()) 
		{
				case "REQUESTASSIGNTOME":
					waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.AssignRequestToMeLnk,30);
					verifyExists(ExploreConfigObj.ExploreConfiguration.AssignRequestToMeLnk,"'Assign Request To Me Link'");
					click(ExploreConfigObj.ExploreConfiguration.AssignRequestToMeLnk,"'Assign Request To Me Link'");
					break;
				case "CREATECOST":
					waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.CreateCostLnk,30);
					verifyExists(ExploreConfigObj.ExploreConfiguration.CreateCostLnk, "'Create Cost Link'");
					Reusable.waitForpageloadmask();
					click(ExploreConfigObj.ExploreConfiguration.CreateCostLnk,"'Create Cost Link'");
					Reusable.waitForpageloadmask();
					break;
				case "ASSIGNTOME":
					verifyExists(ExploreConfigObj.ExploreConfiguration.AssignToMeLnk, "'Assign To Me Link'");
                    Reusable.waitForpageloadmask();
                    Reusable.Waittilljquesryupdated();
                    click(ExploreConfigObj.ExploreConfiguration.AssignToMeLnk,"'Assign To Me Link'");
                    Reusable.waitForpageloadmask();
					break;
				case "REJECT":
					verifyExists(ExploreConfigObj.ExploreConfiguration.RejectLnk, "'Reject Link'");
					click(ExploreConfigObj.ExploreConfiguration.RejectLnk,"'Reject Link'");
					Reusable.Waittilljquesryupdated();
					
					verifyDoesNotExist(ExploreConfigObj.ExploreConfiguration.AssignSuccess);
					verifyExists(ExploreConfigObj.ExploreConfiguration.CommentExplore, "'Comment Explore'");
					sendKeys(ExploreConfigObj.ExploreConfiguration.CommentExplore, "Rejected","'Enter Rejected value'");
					
					click(ExploreConfigObj.ExploreConfiguration.SubmitExplorePopUpBtn,"'Submit Explore PupUp Button'");
					Reusable.Waittilljquesryupdated();
					click(ExploreLoginObj.Login.mapViewBtn,"'Map View Button'");
					break;
					
				case "REJECTNEARNET":
					verifyExists(ExploreConfigObj.ExploreConfiguration.AssignToMeLnk, "'Assign To Me Link'");
					click(ExploreConfigObj.ExploreConfiguration.AssignToMeLnk,"'Assign To Me Link'");
					Reusable.Waittilljquesryupdated();
					Reusable.Waittilljquesryupdated();
					//Thread.sleep(3000);
										
					waitForElementToBeVisible(ExploreConfigObj.ExploreConfiguration.DigRejectCheckBox, 60);
					click(ExploreConfigObj.ExploreConfiguration.DigRejectCheckBox);					
					Reusable.Waittilljquesryupdated();
					
					verifyExists(ExploreConfigObj.ExploreConfiguration.NearNetcommentExplore, "'Nearnet Comment Explore'");
					sendKeys(ExploreConfigObj.ExploreConfiguration.NearNetcommentExplore, "Rejected","'Enter Rejected value'");
					
					verifyExists(ExploreConfigObj.ExploreConfiguration.ExploreSendToSalesBtn, "'Explore Send to Sales Button'");
					click(ExploreConfigObj.ExploreConfiguration.ExploreSendToSalesBtn,"'Explore Send to Sales Button'");
					Reusable.Waittilljquesryupdated();
					
					click(ExploreLoginObj.Login.mapViewBtn,"'Map View Button'");
					Reusable.Waittilljquesryupdated();
					break;
			}
			Reusable.Waittilljquesryupdated();			
			System.out.println("Initializing "+ActionType+" Under Actions in Explore window is was successfull");
		
		return "True";
	}
	
	public String createNearnetCostExplore(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
	{		
//		Initializing the Variable
		String Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Request_ID");
		String Distance_From_Colt = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Distance_From_Colt");
		String Terms = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Terms");
		String Currency = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Currency");
		String Cost_Category = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cost_Category");
		String Cost_SubCategory = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cost_SubCategory");
		String Unit_Cost = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Unit_Cost");
		String Quantity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Quantity");
		String Connection_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connection_Type");
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		String sResult;

		if (!Connection_Type.equalsIgnoreCase("OnnetDualEntry")) 
		{
			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.DistanceFromColtTxb,60);
			verifyExists(ExploreConfigObj.ExploreConfiguration.DistanceFromColtTxb, "'DistanceFromColt Text box'");
			Reusable.Waittilljquesryupdated();			
			sendKeys(ExploreConfigObj.ExploreConfiguration.DistanceFromColtTxb, Distance_From_Colt, "'DistanceFromColt Text box'");
			Reusable.Waittilljquesryupdated();
			
			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.QuoteValidityUpToMonthTxb,60);
			verifyExists(ExploreConfigObj.ExploreConfiguration.QuoteValidityUpToMonthTxb,"'quoteValidityUpToMonth Text box'");
			sendKeys(ExploreConfigObj.ExploreConfiguration.QuoteValidityUpToMonthTxb, Terms,"'quoteValidityUpToMonth Text box'");
			Reusable.Waittilljquesryupdated();
			
			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.NearnetCurrencyTxb,60);
			verifyExists(ExploreConfigObj.ExploreConfiguration.NearnetCurrencyTxb,"'Nearnet Currency Text box'");
			sendKeys(ExploreConfigObj.ExploreConfiguration.NearnetCurrencyTxb, Currency,"'Nearnet Currency Text box'");
			Reusable.Waittilljquesryupdated();

			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.AddCostBtn,60);
			verifyExists(ExploreConfigObj.ExploreConfiguration.AddCostBtn,"'Add Cost Button'");
			click(ExploreConfigObj.ExploreConfiguration.AddCostBtn,"'Add Cost Button'");
				
			Reusable.waitForpageloadmask();
            Reusable.Waittilljquesryupdated();
            waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.NearnetCostTable, 60);
			verifyExists(ExploreConfigObj.ExploreConfiguration.NearnetCostTable, "'Nearnet Cost Table'");
			
			Reusable.waitForpageloadmask();
            Reusable.Waittilljquesryupdated();
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderTable, ExploreConfigObj.ExploreConfiguration.NearnetCostTable, "Cost Category", "1", "Select", Cost_Category);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
			waitToPageLoad();
			Reusable.waitForpageloadmask();
            Reusable.Waittilljquesryupdated();
			
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderTable, ExploreConfigObj.ExploreConfiguration.NearnetCostTable, "Cost Subcategory", "1", "Select", Cost_SubCategory);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
			waitToPageLoad();
			Reusable.waitForpageloadmask();
            Reusable.Waittilljquesryupdated();
			
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderTable, ExploreConfigObj.ExploreConfiguration.NearnetCostTable, "Unit Cost", "1", "Edit", Unit_Cost);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
			waitToPageLoad();
			Reusable.waitForpageloadmask();
            Reusable.Waittilljquesryupdated();
			
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderTable, ExploreConfigObj.ExploreConfiguration.NearnetCostTable, "Quantity", "1", "Edit", Quantity);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
			waitToPageLoad();
			Reusable.waitForpageloadmask();
            Reusable.Waittilljquesryupdated();

		}
				
//		Secondary Cost Details Entry
		if (Connection_Type.equalsIgnoreCase("OnnetDualEntry") || Connection_Type.equalsIgnoreCase("New Building Dual Entry")) 
		{
			switch (Connection_Type) {
				case "New Building Dual Entry":
					verifyExists(ExploreConfigObj.ExploreConfiguration.DualEntryDigCostTab,"'Dual Entry Dig Cost Tab'");
					click(ExploreConfigObj.ExploreConfiguration.DualEntryDigCostTab,"'Dual Entry Dig Cost Tab'");
					Reusable.Waittilljquesryupdated();
					
					verifyExists(ExploreConfigObj.ExploreConfiguration.DistanceFromColtSecondaryTxb,"'Disatance From Colt Secondary Text Box'");
					sendKeys(ExploreConfigObj.ExploreConfiguration.DistanceFromColtSecondaryTxb, Distance_From_Colt,"'Disatance From Colt Secondary Text Box'");
					
					verifyExists(ExploreConfigObj.ExploreConfiguration.AddSecondaryCostBtn,"'Add Secondary Cost Button'");
					click(ExploreConfigObj.ExploreConfiguration.AddSecondaryCostBtn,"'Add Secondary Cost Button'");
					break;
				case "OnnetDualEntry":
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.DistanceFromColtSecondaryTxb, 60);
					verifyExists(ExploreConfigObj.ExploreConfiguration.DistanceFromColtSecondaryTxb,"'Disatance From Colt Secondary Text Box'");
					sendKeys(ExploreConfigObj.ExploreConfiguration.DistanceFromColtSecondaryTxb, Distance_From_Colt,"'Disatance From Colt Secondary Text Box'");
					
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					verifyExists(ExploreConfigObj.ExploreConfiguration.QuoteValidityUpToMonthSecondaryTxb,"'Quote Validity Upto Month Secondary Text Box'");
					sendKeys(ExploreConfigObj.ExploreConfiguration.QuoteValidityUpToMonthSecondaryTxb, Terms,"'Quote Validity Upto Month Secondary Text Box'");
					
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					verifyExists(ExploreConfigObj.ExploreConfiguration.NearnetCurrencySecondaryTxb,"'Nearnet Currency Secondary Text Box'");
					sendKeys(ExploreConfigObj.ExploreConfiguration.NearnetCurrencySecondaryTxb, Terms,"'Nearnet Currency Secondary Text Box'");
					//findWebElement(ExploreConfigObj.ExploreConfiguration.NearnetCurrencySecondaryTxb).sendKeys("ENTEr");
					
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					verifyExists(ExploreConfigObj.ExploreConfiguration.AddSecondaryCostBtn,"'Add Socondary Cost Button'");
					click(ExploreConfigObj.ExploreConfiguration.AddSecondaryCostBtn,"'Add Socondary Cost Button'");
			}
			Reusable.Waittilljquesryupdated();
			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderSecondaryTable, 60);
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderSecondaryTable, ExploreConfigObj.ExploreConfiguration.NearnetCostSecondaryTable, "Cost Category", "1", "Select", Cost_Category);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
			
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderSecondaryTable, ExploreConfigObj.ExploreConfiguration.NearnetCostSecondaryTable, "Cost Subcategory", "1", "Select", Cost_SubCategory);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderSecondaryTable, ExploreConfigObj.ExploreConfiguration.NearnetCostSecondaryTable, "Unit Cost", "1", "Edit", Unit_Cost);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
			sResult = WebTableCellAction(ExploreConfigObj.ExploreConfiguration.NearnetCostHeaderSecondaryTable, ExploreConfigObj.ExploreConfiguration.NearnetCostSecondaryTable, "Quantity", "1", "Edit", Quantity);
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			Reusable.Waittilljquesryupdated();
		}
		
//		Saving the Details
		waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.ExploreSaveBtn, 60);		
		verifyExists(ExploreConfigObj.ExploreConfiguration.ExploreSaveBtn,"Save Button");
		isClickable(ExploreConfigObj.ExploreConfiguration.ExploreSaveBtn);
		click(ExploreConfigObj.ExploreConfiguration.ExploreSaveBtn,"Save Button");
		
		waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.ExploreSendToSalesBtn, 60);		
		verifyExists(ExploreConfigObj.ExploreConfiguration.ExploreSendToSalesBtn,"'ExploreSend To Sale Button'");
		isClickable(ExploreConfigObj.ExploreConfiguration.ExploreSendToSalesBtn);
		click(ExploreConfigObj.ExploreConfiguration.ExploreSendToSalesBtn,"'ExploreSend To Sale Button'");
		Reusable.Waittilljquesryupdated();
		Reusable.WaitforCPQloader();
		if ((findWebElement(ExploreConfigObj.ExploreConfiguration.NearnetStatusElem).getText().trim().equalsIgnoreCase("Completed"))) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Nearnet Request with ID "+Request_ID+" for Site "+sheetName+" processed succesfully");
			System.out.println("Nearnet Request with ID "+Request_ID+" for Site "+sheetName+" processed succesfully");
			click(ExploreLoginObj.Login.mapViewBtn);
			Reusable.Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Process Nearnet Request with ID "+Request_ID+" for Site "+sheetName+" succesfully, Please Verify");
			System.out.println("Unable to Process Nearnet Request with ID "+Request_ID+" for Site "+sheetName+" succesfully, Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
			
		return "True";
	}
	
	public String logOutExplore() throws IOException, InterruptedException 
	{

		verifyExists(ExploreConfigObj.ExploreConfiguration.MenuLnk,"'Menu Link'");
		click(ExploreConfigObj.ExploreConfiguration.MenuLnk,"'Menu Link'");
		Reusable.Waittilljquesryupdated();
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.ExploreLogoutLnk,"'Menu Link'");
		click(ExploreConfigObj.ExploreConfiguration.ExploreLogoutLnk,"'Menu Link'");
		Reusable.Waittilljquesryupdated();
		
		System.out.println("Explore logout was done successfully");
		
		return "True";
	}
	
	public String WebTableCellAction(String hTable, String sTable, String actColumn, String row_number, String ActionType, String ActionValue) throws IOException, InterruptedException 
	{
		String tXpath, sXpath;
		
		waitForElementToAppear(hTable, 75);
		verifyExists(hTable);
		scrollIntoView(findWebElement(hTable));
		
		WebElement sTable_web = findWebElement(sTable);
		
		sXpath = sTable_web.toString();
			
		sXpath = sXpath.substring(sXpath.indexOf("xpath:")+"xpath:".length(), sXpath.indexOf("]]")+1).trim();
		
//		getting the row and column number
		List<WebElement> rows = findWebElement(hTable).findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(0).findElements(By.tagName("th"));
		int tot_col = columns.size();
		int iCol, aColumn_number = 0;
		
		//Reading the actual column number ff table and set the column number with use of reference
		for(iCol = 1; iCol <= tot_col-1; iCol++){
			String Col_Val = columns.get(iCol).getText().trim();
			if (Col_Val.contains(actColumn)){ 
				aColumn_number = iCol+1; 
				break; 
			}
		}
//			Returns the function of column names are not matched
		if (iCol >= tot_col) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Column Name "+actColumn+" is not found in the webtable, Please Verify ");
			System.out.println("Column Name "+actColumn+" is not found in the webtable, Please Verify ");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
		
		Reusable.waitForpageloadmask();
		
		WebElement Cell; String sOut = null;
		Actions actions = new Actions(webDriver);
		switch (ActionType) {
			case "Edit":
				Cell = webDriver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]"));
				scrollIntoView(Cell);
				actions.doubleClick(Cell).perform(); 
				Reusable.Waittilljquesryupdated();
				Reusable.waitForpageloadmask();
				WebElement edit_Box = webDriver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]//input"));
//				actions.sendKeys(edit_Box, ActionValue).perform();
				actions.sendKeys(ActionValue).perform();
				Thread.sleep(4000);				
				click(hTable,"Table");
				break;
			case "Select":
				Cell = webDriver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]"));
				scrollIntoView(Cell);
				actions.doubleClick(Cell).perform(); 
				WebElement dropdown = webDriver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]//input"));
				click(dropdown.getText());
				actions.sendKeys(ActionValue).perform();
				waitToPageLoad();
				Reusable.waitForpageloadmask();
				actions.sendKeys(Keys.ARROW_DOWN).perform();
				waitForAjax();
				actions.sendKeys(Keys.ENTER).perform();
				waitToPageLoad();
				Reusable.waitForpageloadmask();
				click(hTable,"'Table'");
				break;
			case "Store":
				Cell = webDriver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]//child::span"));
				scrollIntoView(Cell);
				sOut = Cell.getAttribute("title");
				break;
				
			case "Click":
				Cell = webDriver.findElement(By.xpath(sXpath+"/tr["+row_number+"]/td["+aColumn_number+"]"));
				scrollIntoView(Cell);
				click(Cell.getText());
				//javaScriptClickWE(Cell,"'Cell'");
				break;
		}
		
		return "True";
	}
	
	public String SearchONQTQueue_Record(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException {
	
		
		switch(SiteName) {
		case "A_End":
//			Initializing the Variable
			String Request_ID = null;
			String Additional_Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Additional_Request_ID");
			
			if (Additional_Request_ID.equals("")) {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Request_ID");
				System.out.println("Request_ID : "+Request_ID);
			} else {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Additional_Request_ID");
			}
//			Verify Login Successsfull or not
			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.OnqtWorkQueueBtn, 60);
			click(ExploreConfigObj.ExploreConfiguration.OnqtWorkQueueBtn);
			Reusable.Waittilljquesryupdated();
			Reusable.Waittilljquesryupdated();
			Reusable.waitToPageLoad();
			
			ClickonElementByString("//a[text()='"+Request_ID+"']",120);		
			Reusable.Waittilljquesryupdated();
			
			String ExploreID = webDriver.findElement(By.xpath("//*[@id='dijit_form_TextBox_15']")).getAttribute("value");
			System.out.println("ExploreID: "+ExploreID);
			DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPQ_Common_ID", ExploreID);
			
			System.out.println("Record "+Request_ID+" Search in Fibre Planning Work Queue was successfull");
			
			break;
			
		case "B_End":
//			Initializing the Variable
			String B_End_Request_ID = null;
			String B_End_Additional_Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Additional_Request_ID");
			
			if (B_End_Additional_Request_ID.equals("")) {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Request_ID");	
			} else {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Additional_Request_ID");
			}
//			Verify Login Successsfull or not
			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.OnqtWorkQueueBtn, 60);
			click(ExploreConfigObj.ExploreConfiguration.OnqtWorkQueueBtn);
			Reusable.Waittilljquesryupdated();
			Reusable.Waittilljquesryupdated();
			Reusable.waitToPageLoad();
			
			ClickonElementByString("//a[text()='"+Request_ID+"']",120);		
			Reusable.Waittilljquesryupdated();
			
			ExploreID = webDriver.findElement(By.xpath("//*[@id='dijit_form_TextBox_15']")).getAttribute("value");
			System.out.println("ExploreID: "+ExploreID);
			DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_End_CPQ_Common_ID", ExploreID);
			
			System.out.println("Record "+Request_ID+" Search in Fibre Planning Work Queue was successfull");
			
			break;
		}
		return "True";
						
	}
	
	public String createCostExploreOffnet(String file_name, String Sheet_Name, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
		
		
//		Initializing the Variable
		String Carrier = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Carrier");
		String Node = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Node");
		String MRC = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"MRC");
		String NRC = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"NRC");
		String MRC_New = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"MRC_New");
		String NRC_New = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"NRC_New");
		String Terms = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Terms");
		String Currency = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Currency");
		String Price_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Price_Type");
		String Connector_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Connector_Type");
		String Connection_Type = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Connection_Type");
		String Dual_Entry_From_OLO = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Dual_Entry_From_OLO");
		String Request_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Request_ID");
		String Explore_Cost_Update = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Explore_Cost_Update");
		
		waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.CarrierTxb, 60);
		Reusable.Waittilljquesryupdated();			
					
		verifyExists(ExploreConfigObj.ExploreConfiguration.CarrierTxb,"'Carrier Txb'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.CarrierTxb, Carrier,"'CarrierTxb'");
		//findWebElement(ExploreConfigObj.ExploreConfiguration.CarrierTxb).sendKeys("ENTER");
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.NodeTxb,"'Node Txb'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.NodeTxb, Node,"'Node Txb'");
		//findWebElement(ExploreConfigObj.ExploreConfiguration.NodeTxb).sendKeys("ENTER");
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.NrcTxb,"'NRC Text box'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.NrcTxb, NRC,"'NRC Text box'");
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.MrcTxb,"'MRC Text box'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.MrcTxb, MRC,"'MRC Text box'");
			
		verifyExists(ExploreConfigObj.ExploreConfiguration.TermsTxb,"'Terms Txb'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.TermsTxb, Terms,"'Terms Txb'");
		//findWebElement(ExploreConfigObj.ExploreConfiguration.TermsTxb).sendKeys("ENTER");
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.CurrenctTxb,"'Currenct Txb'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.CurrenctTxb, Currency,"'Currenct Txb'");
		//findWebElement(ExploreConfigObj.ExploreConfiguration.CurrenctTxb).sendKeys("ENTER");
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.PriceTypeTxb,"'PriceType Txb'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.PriceTypeTxb, Price_Type,"'PriceType Txb");
		//findWebElement(ExploreConfigObj.ExploreConfiguration.PriceTypeTxb).sendKeys("ENTER");
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.ConnectorTxb,"'Connector Txb'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.ConnectorTxb, Connector_Type,"'Connector Txb'");
		//findWebElement(ExploreConfigObj.ExploreConfiguration.ConnectorTxb).sendKeys("ENTER");
			
		
		/*	if (Connection_Type.equalsIgnoreCase("DualEntry")) 
			{ 
				verifyExists(ExploreConfigObj.ExploreConfiguration.DualEntryFromOLOTxb,"'dualEntryFromOLOTxb'");
				sendKeys(ExploreConfigObj.ExploreConfiguration.DualEntryFromOLOTxb, Dual_Entry_From_OLO,"'dualEntryFromOLOTxb'");
				//findWebElement(ExploreConfigObj.ExploreConfiguration.DualEntryFromOLOTxb).sendKeys("ENTER");
					
				}*/
			click(ExploreConfigObj.ExploreConfiguration.CreateCostBtn);
			Reusable.Waittilljquesryupdated();	
			
			waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.SaveCostBtn, 60);
			verifyExists(ExploreConfigObj.ExploreConfiguration.SaveCostBtn,"Save Cost Button");
			Reusable.Waittilljquesryupdated();	
			click(ExploreConfigObj.ExploreConfiguration.ApproveQuote);
			Reusable.Waittilljquesryupdated();
				
			
		return "True";
		}
	
	public String nearnetExploreOperations(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
		
//		Initializing the Variable
		String sResult = null;
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
//		Login to Explore
		sResult = ExpLogin.ExploreLogin(Configuration.EXPLORE_Nearnet_User,Configuration.EXPLORE_Password);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

//		To Process Offnet Cost in Explore for A Site
		sResult = processNearnetCostExplore(file_name, Sheet_Name, iScript, iSubScript,"A_End");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		To Process Offnet Cost in Explore for B Site
		if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
			sResult = processNearnetCostExplore(file_name, Sheet_Name, iScript, iSubScript,"B_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Logout of Explore Operations
		sResult = logOutExplore();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String processNearnetCostExplore(String file_name, String Sheet_Name, String iScript, String iSubScript, String SiteName) throws IOException, InterruptedException {
		/*----------------------------------------------------------------------
		Method Name : processOffnetCostExplore
		Purpose     : This method is to process the offnet cost in explore for the respective ends
		Designer    : Vasantharaja C
		Created on  : 24th June 2020 
		Input       : String file_name, String Sheet_Name, String iScript, String iSubScript
		Output      : True/False
		 ----------------------------------------------------------------------*/ 
		
		//Initialize the Varibles
		String sResult = null;
		
//		Search record in fibreplanning work queue
		try {
			sResult = SearchFibrePlanningWork_Record(file_name, Sheet_Name, iScript, iSubScript,SiteName);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("AssignToMe");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//to enter cost for nearnet
		sResult = createNearnetCostExplore(file_name, Sheet_Name, iScript, iSubScript);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }		
		
		return "True";
	}
	
	public String offnetExploreOperations(String file_name, String Sheet_Name, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
			
//		Initializing the Variable
		String sResult = null;
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Product_Name");
		String MRC_New = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"MRC_New");
		String NRC_New = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"NRC_New");
		String Request_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"B_End_Request_ID");
		String B_End_Request_ID = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Request_ID");
		String Explore_Cost_Update = DataMiner.fngetcolvalue(file_name, Sheet_Name, scriptNo, dataSetNo,"Explore_Cost_Update");
//		Login to Explore

		sResult = ExpLogin.ExploreLogin(Configuration.EXPLORE_Offnet_User,Configuration.EXPLORE_Password);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

//		To Process Offnet Cost in Explore for A Site
		sResult = processOffnetCostExplore(file_name, Sheet_Name, scriptNo, dataSetNo, "A_End");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		if(Explore_Cost_Update.equalsIgnoreCase("Yes"))
		{
			Reusable.Waittilljquesryupdated();	
			Reusable.waitForpageloadmask();
			
			ClickonElementByString("//a[contains(text(),'"+Request_ID+-1+"')]",120);	
			
			verifyExists(ExploreConfigObj.ExploreConfiguration.EditBtn,"'EditBtn'");
			click(ExploreConfigObj.ExploreConfiguration.EditBtn);
			
			Reusable.Waittilljquesryupdated();	
			Reusable.waitForpageloadmask();
			
			Reusable.Waittilljquesryupdated();	
			Reusable.waitForpageloadmask();
			
			verifyExists(ExploreConfigObj.ExploreConfiguration.NrcTxb,"'NRC Text box'");
			sendKeys(ExploreConfigObj.ExploreConfiguration.NrcTxb, NRC_New,"'NRC Text box'");
			
			verifyExists(ExploreConfigObj.ExploreConfiguration.MrcTxb,"'MRC Text box'");
			sendKeys(ExploreConfigObj.ExploreConfiguration.MrcTxb, MRC_New,"'MRC Text box'");
			
			Reusable.Waittilljquesryupdated();	
			click(ExploreConfigObj.ExploreConfiguration.ApproveQuote);
			Reusable.Waittilljquesryupdated();
			
		}
		
		if (waitForInvisibilityOfElement(ExploreConfigObj.ExploreConfiguration.ApproveQuote,20))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Offnet Cost have been Created for the request "+Request_ID+" successfully");
			System.out.println("Explore Offnet Cost have been Created for the request "+Request_ID+" successfully");
			click(ExploreLoginObj.Login.mapViewBtn);
			Reusable.Waittilljquesryupdated();
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Approve Explore offnet Cost for the request "+Request_ID+" , Please Verify");
			System.out.println("Unable to Approve Explore offnet Cost for the request "+Request_ID+" , Please Verify");
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			return "False";
		}
		
		
//		To Process Offnet Cost in Explore for B Site
		if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
			sResult = processOffnetCostExplore(file_name,Sheet_Name, scriptNo, dataSetNo,"B_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			if(Explore_Cost_Update.equalsIgnoreCase("Yes"))
			{
				Reusable.Waittilljquesryupdated();	
				Reusable.waitForpageloadmask();
				
				ClickonElementByString("//a[contains(text(),'"+B_End_Request_ID+-1+"')]",120);
				
				Reusable.Waittilljquesryupdated();	
				Reusable.waitForpageloadmask();
				
				
				verifyExists(ExploreConfigObj.ExploreConfiguration.EditBtn,"'EditBtn'");
				click(ExploreConfigObj.ExploreConfiguration.EditBtn);
				
				Reusable.Waittilljquesryupdated();	
				Reusable.waitForpageloadmask();
				
				verifyExists(ExploreConfigObj.ExploreConfiguration.NrcTxb,"'NRC Text box'");
				sendKeys(ExploreConfigObj.ExploreConfiguration.NrcTxb, NRC_New,"'NRC Text box'");
				
				verifyExists(ExploreConfigObj.ExploreConfiguration.MrcTxb,"'MRC Text box'");
				sendKeys(ExploreConfigObj.ExploreConfiguration.MrcTxb, MRC_New,"'MRC Text box'");
				
				Reusable.Waittilljquesryupdated();	
				click(ExploreConfigObj.ExploreConfiguration.ApproveQuote);
				Reusable.Waittilljquesryupdated();
				
			}
			
			if (waitForInvisibilityOfElement(ExploreConfigObj.ExploreConfiguration.ApproveQuote,20))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Explore Offnet Cost have been Created for the request "+Request_ID+" successfully");
				System.out.println("Explore Offnet Cost have been Created for the request "+Request_ID+" successfully");
				click(ExploreLoginObj.Login.mapViewBtn);
				Reusable.Waittilljquesryupdated();
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Unable to Approve Explore offnet Cost for the request "+Request_ID+" , Please Verify");
				System.out.println("Unable to Approve Explore offnet Cost for the request "+Request_ID+" , Please Verify");
				ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
				return "False";
			}
		}
		
		
		
//		Logout of Explore Operations
		sResult = logOutExplore();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String processOffnetCostExplore(String file_name, String Sheet_Name, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException {
		
		//Initialize the Varibles
		String sResult = null;
//		String Re_Explore_Value = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Request_ID");
		
//		Search record in Explore
		sResult = SearchONQTQueue_Record(file_name, Sheet_Name, scriptNo, dataSetNo, SiteName);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("RequestAssignToMe");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("CreateCost");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Creating a cost for explore offnet record
		sResult = createCostExploreOffnet(file_name, Sheet_Name, scriptNo, dataSetNo);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String offnetExploreRejectAction(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String sResult = null;
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
//		Login to Explore
		sResult = ExpLogin.ExploreLogin(Configuration.EXPLORE_Offnet_User,Configuration.EXPLORE_Password);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

//		To Process Offnet Cost in Explore for A Site
		sResult = rejectOffnetCostExplore(file_name, Sheet_Name, iScript, iSubScript, "A_End");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		To Process Offnet Cost in Explore for B Site
		if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
			sResult = rejectOffnetCostExplore(file_name, Sheet_Name, iScript, iSubScript, "B_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Logout of Explore Operations
		sResult = logOutExplore();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String rejectOffnetCostExplore(String file_name, String Sheet_Name, String iScript, String iSubScript, String SiteName) throws IOException, InterruptedException 
	{
		//Initialize the Varibles
		String sResult = null;
//		String Re_Explore_Value = dataminer.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Request_ID");
		
//		Search record in Explore
		sResult = SearchONQTQueue_Record(file_name, Sheet_Name, iScript, iSubScript, SiteName);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("RequestAssignToMe");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("Reject");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		//Creating a cost for explore offnet record
//		sResult = createCostExploreOffnet(file_name, Sheet_Name, iScript, iSubScript);
//		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String nearnetExploreRejectOperations(String file_name, String Sheet_Name, String iScript, String iSubScript) throws IOException, InterruptedException 
	{
//		Initializing the Variable
		String sResult = null;
		String Product_Name = DataMiner.fngetcolvalue(file_name, Sheet_Name, iScript, iSubScript,"Product_Name");
		
//		Login to Explore
		sResult = ExpLogin.ExploreLogin(Configuration.EXPLORE_Nearnet_User,Configuration.EXPLORE_Password);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

//		To Process Offnet Cost in Explore for A Site
		sResult = rejectNearnetCostExplore(file_name,Sheet_Name , iScript, iSubScript, "A_End");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		To Process Offnet Cost in Explore for B Site
		if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) {
			sResult = rejectNearnetCostExplore(file_name, Sheet_Name, iScript, iSubScript,"B_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}
		
//		Logout of Explore Operations
		sResult = logOutExplore();
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		return "True";
	}
	
	public String rejectNearnetCostExplore(String file_name, String Sheet_Name, String iScript, String iSubScript, String SiteName) throws IOException, InterruptedException 
	{
		//Initialize the Varibles
		String sResult = null;
		
//		Search record in fibreplanning work queue
		try {
			sResult = SearchFibrePlanningWork_Record(file_name, Sheet_Name, iScript, iSubScript, SiteName);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		//Assigning the explore request to the logged in user
		sResult = selectActionsFromExplore("REJECTNEARNET");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		//to enter cost for nearnet
//		sResult = createNearnetCostExplore(file_name, Sheet_Name, iScript, iSubScript);
//		if (sResult.equalsIgnoreCase("False")){ return "False"; }		
		
		return "True";
	}		
	
	public String UpdateONQTApprovedStatus(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
	{
				
//		Initializing the Variable
		String sResult = null;
		String Product_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Name");
		
//		Login to Explore

		sResult = ExpLogin.ExploreLogin(Configuration.EXPLORE_Offnet_User,Configuration.EXPLORE_Password);
		if (sResult.equalsIgnoreCase("False")){ return "False"; }

//		To Process Offnet Cost in Explore for A Site
		sResult = processOffnetCostExplore(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
		SearchONQTQueue_Record(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
		
		sResult = UpdateONQTCostInfor(testDataFile, sheetName, scriptNo, dataSetNo, "A_End");
		if (sResult.equalsIgnoreCase("False")){ return "False"; }
		
//		To Process Offnet Cost in Explore for B Site
		if (Product_Name.equalsIgnoreCase("EthernetLine")||Product_Name.equalsIgnoreCase("Wave")) 
		{
			sResult = processOffnetCostExplore(testDataFile,sheetName, scriptNo, dataSetNo,"B_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
			
			SearchONQTQueue_Record(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
			
			sResult = UpdateONQTCostInfor(testDataFile, sheetName, scriptNo, dataSetNo, "B_End");
			if (sResult.equalsIgnoreCase("False")){ return "False"; }
		}									
		
		return "True";
		
	}
	
	public String UpdateONQTCostInfor(String testDataFile, String sheetName, String scriptNo, String dataSetNo, String SiteName) throws IOException, InterruptedException
	{
		
		String MRC_New = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MRC_New");
		String NRC_New = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NRC_New");
			
		switch(SiteName) {
		case "A_End":
//			Initializing the Variable
			String Request_ID = null;
			String Additional_Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Additional_Request_ID");
			
			if (Additional_Request_ID.equals("")) {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Request_ID");
				System.out.println("Request_ID : "+Request_ID);
			} else {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Additional_Request_ID");
			}
			
			ClickonElementByString("//a[text()='"+Request_ID+-2+"']",120);		
			Reusable.Waittilljquesryupdated();	
			Reusable.waitForpageloadmask();
			
			break;
			
		case "B_End":
//			Initializing the Variable
			String B_End_Request_ID = null;
			String B_End_Additional_Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Additional_Request_ID");
			
			if (B_End_Additional_Request_ID.equals("")) {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Request_ID");	
			} else {
				Request_ID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_End_Additional_Request_ID");
			}

			ClickonElementByString("//a[text()='"+Request_ID+-2+"']",120);		
			Reusable.Waittilljquesryupdated();
			Reusable.waitForpageloadmask();
			
			break;
		}
		
		//Update Cost Information
		
		waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.CarrierTxb, 60);
		Reusable.Waittilljquesryupdated();	
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.EditBtn,"Edit Button");
		ScrollIntoViewByString(ExploreConfigObj.ExploreConfiguration.EditBtn);
		click(ExploreConfigObj.ExploreConfiguration.EditBtn,"Edit Button");
		Reusable.Waittilljquesryupdated();
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.NrcTxb,"'NRC Text box'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.NrcTxb, NRC_New,"'NRC Text box'");
		
		verifyExists(ExploreConfigObj.ExploreConfiguration.MrcTxb,"'MRC Text box'");
		sendKeys(ExploreConfigObj.ExploreConfiguration.MrcTxb, MRC_New,"'MRC Text box'");			
			
		waitForElementToAppear(ExploreConfigObj.ExploreConfiguration.SaveCostBtn, 60);
		//verifyExists(ExploreConfigObj.ExploreConfiguration.SaveCostBtn,"Save Cost Button");
		//Reusable.Waittilljquesryupdated();	
		click(ExploreConfigObj.ExploreConfiguration.ApproveQuote);
		Reusable.Waittilljquesryupdated();
					
		Reusable.Waittilljquesryupdated();	
		
		return "True";
	}

}
