package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSMergeNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSMergeNumberPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSAddNumberPage AddNumber = new NMTSAddNumberPage();
	NMTSSplitFreeNumberPage SpitNumber = new NMTSSplitFreeNumberPage();
	
	public void MergerdNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		
	//	String CheckboxesStartRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Checkboxes_Start_Range");
	//	String CheckboxEndRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Checkbox_End_Range");
		String NubmerRadioButtonName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NubmerRadioButtonName");
		String TargetStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Target_Status");
		String Targetavailabilty = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Target_availabilty");
		String ColtCategory = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Category");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String NumberStatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number_Status/Status");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String AreaName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Area_Code");
		String NMTSAreaCodeExtension = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NMTS_Area_Code_Extension");
		String RangeStart = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeStart");
		String RangeEnd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"RangeEnd");
		String GeoNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Geo_Number");
		String BlockSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Block_Size");
		String PageSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Page_Size");
		
		
		AddNumber.UserGroupforSuperUser(testDataFile, sheetName, scriptNo, dataSetNo);
		waitForAjax();
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMenu");
		if(Country.toString().equals("CH")) {
			waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.mergebtnCH,15);
			verifyExists(NMTSMergeNumberObj.MergeNumber.mergebtnCH,"Merge and move Number button");
			javaScriptclick(NMTSMergeNumberObj.MergeNumber.mergebtnCH,"Merge and move Number button");
		} else {
			waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.MergeButtonforsuper,15);
			verifyExists(NMTSMergeNumberObj.MergeNumber.MergeButtonforsuper,"Merge and move Number button");
			javaScriptclick(NMTSMergeNumberObj.MergeNumber.MergeButtonforsuper,"Merge and move Number button");
		}
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMain");
		if (NubmerRadioButtonName.toString().equals("Merge Number")) {
			waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.MergeRadioBtn,15);
			verifyExists(NMTSMergeNumberObj.MergeNumber.MergeRadioBtn,"Merge Radio button");
			click(NMTSMergeNumberObj.MergeNumber.MergeRadioBtn,"Merge Radio button");
		} else {
			waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.poolandstatus,15);
		verifyExists(NMTSMergeNumberObj.MergeNumber.poolandstatus,"Change pool radio button");
		javaScriptclick(NMTSMergeNumberObj.MergeNumber.poolandstatus,"Change pool radio button");
		}
		
		Reusable.waitForAjax();
	//	waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.IEStatus,15);
		verifyExists(NMTSMergeNumberObj.MergeNumber.Status,"Number Status");
		Reusable.Select(NMTSMergeNumberObj.MergeNumber.Status, NumberStatus);
		
		verifyExists(NMTSMergeNumberObj.MergeNumber.Availability,"Number Availablity");
		Reusable.Select(NMTSMergeNumberObj.MergeNumber.Availability,Availablity);
		
		verifyExists(NMTSMergeNumberObj.MergeNumber.AreaCode,"Area Code");
		sendKeys(NMTSMergeNumberObj.MergeNumber.AreaCode, AreaName, "Area Code");
		
		
		verifyExists(NMTSMergeNumberObj.MergeNumber.AreaCodeEXtension,"Area Code Extension");
		sendKeys(NMTSMergeNumberObj.MergeNumber.AreaCodeEXtension, NMTSAreaCodeExtension, "Area Code Extension");
		
		verifyExists(NMTSMergeNumberObj.MergeNumber.MergeRangeStart,"Range Start");
		sendKeys(NMTSMergeNumberObj.MergeNumber.MergeRangeStart, RangeStart, "Range Start");
			
		verifyExists(NMTSMergeNumberObj.MergeNumber.MergeRangeEnd,"Range End");
		sendKeys(NMTSMergeNumberObj.MergeNumber.MergeRangeEnd, RangeEnd, "Range End");
			
		verifyExists(NMTSMergeNumberObj.MergeNumber.GeoNumber,"Geo Number");
		Reusable.Select(NMTSMergeNumberObj.MergeNumber.GeoNumber,GeoNumber);
		if (NubmerRadioButtonName.toString().equals("Merge Number")) {
			if (Availablity.toString().contains("Wholesale")) {
				verifyExists(NMTSMergeNumberObj.MergeNumber.DropBlockSize,"Drop Block Size");
				Reusable.Select(NMTSMergeNumberObj.MergeNumber.DropBlockSize,BlockSize);
				

			} else {
				System.out.println("Current Availability is Colt so block size is not required.");
			}
		}else if (NubmerRadioButtonName.toString().equals("Change pool/Status")) {
			verifyExists(NMTSMergeNumberObj.MergeNumber.DropBlockSize,"Drop Block Size");
			Reusable.Select(NMTSMergeNumberObj.MergeNumber.DropBlockSize,BlockSize);
		}
		verifyExists(NMTSMergeNumberObj.MergeNumber.PageSize,"Drop Block Size");
		Reusable.Select(NMTSMergeNumberObj.MergeNumber.PageSize,PageSize);
		waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.SubmitButton,15);
		verifyExists(NMTSMergeNumberObj.MergeNumber.SubmitButton,"Submit button");
	//	click(NMTSMergeNumberObj.MergeNumber.IESubmitButton,"Submit button");
		javaScriptclick(NMTSMergeNumberObj.MergeNumber.SubmitButton,"Submit button");
		waitForAjax();
		
		if (isElementPresent(By.xpath("//table[@id='grdView']//tbody/tr[2]/td[contains(text(),'No Record Found')]"))) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Numbers Status/Pool Not changed successfully");	
	
		} else {
			
			waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.AllCheckbox,15);
			verifyExists(NMTSMergeNumberObj.MergeNumber.AllCheckbox,"Submit button");
			javaScriptclick(NMTSMergeNumberObj.MergeNumber.AllCheckbox,"Submit button");
			
			/*
			if (CheckboxesStartRange.toString().contains("All")) {
				waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.IEAllCheckbox,15);
				verifyExists(NMTSMergeNumberObj.MergeNumber.IEAllCheckbox,"Submit button");
			//	click(NMTSMergeNumberObj.MergeNumber.IEAllCheckbox,"Submit button");
				javaScriptclick(NMTSMergeNumberObj.MergeNumber.IEAllCheckbox,"Submit button");
			}

			else {
			Report.LogInfo("enter the value of range start:-",CheckboxesStartRange,"CheckboxesStartRange");
				
				int rangeStart = Integer.parseInt(CheckboxesStartRange);
				
				Report.LogInfo("enter the value of range end:-", CheckboxEndRange,"CheckboxEndRange");
				
				int rangeEnd = Integer.parseInt(CheckboxEndRange);
				
				if(isElementPresent(By.xpath("//table[@id='grdView']//tbody/tr[3]"))){
			
					for (int k = rangeStart; k <= rangeEnd; k++) {
					//	if (isElementPresent(xml.getlocator("//locators/IERandomCheckbox").replace("Rowindex", String.valueOf(k + 1)).replace("index", String.valueOf(k)))) {
				
						if (isElementPresent(By.xpath("//table//td[11])[index]//input").replace("Rowindex", String.valueOf(k + 1)).replace("index", String.valueOf(k)))) {
							
							verifyExists(NMTSMergeNumberObj.MergeNumber.IERandomCheckbox,"IE Random Check box");
							click(NMTSMergeNumberObj.MergeNumber.IERandomCheckbox.replace("Rowindex", String.valueOf(k + 1)).replace("index", String.valueOf(k)));
						} else {
						}
					}
				} else {
					
					
				}
				
				
			
			}
		
			Reusable.waitForAjax();

		
		
			}
		
			*/
			
			Reusable.waitForAjax();
			if (NubmerRadioButtonName.toString().equals("Merge Number")) {
				
				waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.mergeButton,15);
				verifyExists(NMTSMergeNumberObj.MergeNumber.mergeButton,"merge Button");
				javaScriptclick(NMTSMergeNumberObj.MergeNumber.mergeButton,"merge Button");
				Reusable.waitForAjax();
				String MergedMessage = getTextFrom(NMTSMergeNumberObj.MergeNumber.MergedMessage,"Merged Message");
				
				if(MergedMessage.contains("successfully")){
				ExtentTestManager.getTest().log(LogStatus.PASS, "Numbers Merged successfully");
				}else{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Numbers not Merged successfully");	
				}
				
			}
			 else { 
				 	verifyExists(NMTSMergeNumberObj.MergeNumber.targetStatus," TargetStatus");
					Reusable.Select(NMTSMergeNumberObj.MergeNumber.targetStatus, TargetStatus);
					Reusable.waitForAjax();
					verifyExists(NMTSMergeNumberObj.MergeNumber.tgtavail,"Targetavailabilty");
					Reusable.Select(NMTSMergeNumberObj.MergeNumber.tgtavail, Targetavailabilty);

					Reusable.waitForAjax();
					verifyExists(NMTSMergeNumberObj.MergeNumber.SelectColtcategory,"ColtCategory");
					Reusable.Select(NMTSMergeNumberObj.MergeNumber.SelectColtcategory, ColtCategory);
					
					waitForElementToBeVisible(NMTSMergeNumberObj.MergeNumber.poolandstatusbtn,15);
					verifyExists(NMTSMergeNumberObj.MergeNumber.poolandstatusbtn,"poolandstatus Button");
					javaScriptclick(NMTSMergeNumberObj.MergeNumber.poolandstatusbtn,"poolandstatus Button");
					Reusable.waitForAjax();
					verifyExists(NMTSMergeNumberObj.MergeNumber.MergedMessage,"Success message verified");
					String MergedMessage = getTextFrom(NMTSMergeNumberObj.MergeNumber.MergedMessage,"Status/Pool message");
					
					if(MergedMessage.contains("successfully")){
					ExtentTestManager.getTest().log(LogStatus.PASS, "Number Status/Pool changed successfully");
					}else{
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Numbers Status/Pool Not changed successfully");	
					}
			 	}
	
		}
	}
	
	

}
	
	
	
	

