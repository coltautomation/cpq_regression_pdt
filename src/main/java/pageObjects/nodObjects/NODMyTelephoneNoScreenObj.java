package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODMyTelephoneNoScreenObj {

	
	public static class TelephoneNoScreen
	{ 
		public static final String ActionDrpDwn="@xpath=//*[@class='actionLinks ml-4 ng-star-inserted']";
//		public static final String AddressUpdateOption="@xpath=//*[@class='actionLinks ml-4 ng-star-inserted']//*[text()='Address Update']"; 
		
		public static final String Update_CustomerName="@xpath=//input[@id='emerBusinessNameUpdated']"; 
		public static final String Update_BuildingNumber="@xpath=//input[@id='emerBuildingNameUpdated']"; 
		public static final String Update_StreetName="@xpath=//input[@id='emerStreetUpdated']"; 
		public static final String Update_CityOrTown="@xpath=//input[@id='emerTownUpdated']"; 
		public static final String Update_PostalCode="@xpath=//input[@id='emerPostCodeUpdated']"; 

		public static final String AddressSuggestion="@xpath=(//*[@class='card rounded-0'])[1]"; 
		public static final String AddressUpdateMessage="@xpath=//*[contains(text(),'Provided address has validated successfully')]"; 
		public static final String AddressUpdatebtn="@xpath=//button[contains(text(),'Update Address')]"; 
		public static final String AddressUpdate_SuccessMsg="@xpath=//*[contains(text(),'Thank you for your order. Your order is raised suc')]"; 



		
		public static final String ActionOption="@xpath=(//div[@tabulator-field='actions'])[2]";
		public static final String AddressUpdateOption="@xpath=//*[@id='actionModal']//*[text()='Address Update']"; 
		public static final String AddressUpdatePage ="@xpath=(//*[contains(text(),'Update Address')])[1]";
		public static final String ActiationPage ="@xpath=//*[contains(text(),'Activation')]";
		public static final String ServiceType ="@xpath=//select[@id='serviceType']";
		public static final String CustomerName ="@xpath=//input[@id='emerBusinessName']";
		public static final String CustomerNameUpdated ="@xpath=//input[@id='emerBusinessNameUpdated']";
		public static final String BuildingNumberUpdated ="@xpath=//input[@id='emerBuildingNumberUpdated']";
		public static final String StreetNameUpdated ="@xpath=//input[@id='emerStreetUpdated']";
		public static final String CityOrTownUpdated ="@xpath=//input[@id='emerTownUpdated']";
		public static final String PostalCodeUpdated ="@xpath=//input[@id='emerPostCodeUpdated']";
		public static final String ValidateAddressBtn ="@xpath=//button[contains(text(),'Validate Address')]";
		public static final String AddressValidationMsg ="@xpath=//h5[contains(text(),'Provided address has validated successfully')]";
		public static final String UpdateAddressBtn="@xpath=//button[contains(text(),'Update Address')]";
		public static final String SuccessMsg="@xpath=//*[contains(text(),'Thank you for your order. Your order is raised successfully.')]"; 
		
		public static final String FirstName="@xpath=//input[@id='emerFirstname']";
		public static final String LastName="@xpath=//input[@id='emerLastname']"; 
		public static final String Language="@xpath=//*[@name='emerLanguage']"; 
		public static final String customerTypeRadioBtn ="@xpath=//label[contains(text(),'Customer')]";
		
		public static final String PostalCodeUpdatedFR ="@xpath=//select[@id='emerPostCodeUpdated']";
		public static final String CityOrTownUpdatedFR ="@xpath=//select[@id='emerTownUpdated']";
		public static final String DepartmentTypeUpdated ="@xpath=//select[@id='emerSerUpdDepartment']";
		public static final String SubcriberId="@xpath=//input[@id='companyRegNumber']";
		
		public static final String DirServiceUpdateOption="@xpath=//*[@id='actionModal']//*[text()='Directory Service Update']"; 
		public static final String DirServiceUpdatePage="@xpath=//*[contains(text(),'DSU Update')]"; 
		
		public static final String CustomerName1="@xpath=//input[@id='btName']"; 
		public static final String BuildingNumber="@xpath=//input[@id='bNumber']"; 
		public static final String StreetName="@xpath=//input[@id='acStreet']"; 
		public static final String CityOrTown="@xpath=//input[@id='acCitytown']"; 
		public static final String PostalCode="@xpath=//input[@id='acPostCode']"; 

		public static final String OrderTypeDrpDwn="@xpath=//select[@id='ostatusDD']"; 
		public static final String OrderTypeOption="@xpath=//option[contains(text(),'option')]"; 

		public static final String LineTypeDrpDwn="@xpath=//select[@id='lineTypeDD']"; 

		public static final String TraffDrpDwn="@xpath=//select[@id='tariffDD']"; 
		public static final String EntryTypeDrpDwn="@xpath=//select[@id='entryTypeDD']"; 
		public static final String TypeFaceDrpDwn="@xpath=//select[@id='typefaceDD']"; 
		public static final String ListingCategoryDrpDwn="@xpath=//select[@id='lCategoryDD']"; 
		public static final String listingTypeDrpDwn="@xpath=//select[@id='listingTypeDD']"; 
		public static final String PriorityDrpDwn="@xpath=//select[@id='priorityDD']"; 
		public static final String telphoneNoDrpDwn="@xpath=//input[@id='telphoneNo']"; 
		public static final String UpdateDSUBtn="@xpath=//button[contains(text(),'Update DSU')]"; 


	
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}