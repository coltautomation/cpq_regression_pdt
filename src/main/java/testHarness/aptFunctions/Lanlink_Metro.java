
package testHarness.aptFunctions;




import java.awt.AWTException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;


import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.Lanlink_Metro_Obj;
import pageObjects.aptObjects.Lanlink_OLO_Obj;


public class Lanlink_Metro extends SeleniumUtils {
	SoftAssert sa = new SoftAssert();

	

	boolean orderdopdown, serviceTypedropdown, modularmspCheckbox, autocreateservicecheckbox, interfacespeeddropdown,
	servicesubtypesdropdown, availablecircuitsdropdown, nextbutton, A_EndTechnolnogy, B_Endtechnolnogy;
	
	public void deleteSiteOrder(String siteOrderNumber_PointToPoint) throws IOException, InterruptedException {

		webDriver.findElement(By.xpath("//tr//*[contains(.,'"+siteOrderNumber_PointToPoint+"')]//following-sibling::td//a[text()='Delete']")).click();
		waitforPagetobeenable();

		String DeleteAlertPopup = (Lanlink_Metro_Obj.Lanlink_Metro.delete_alertpopup);
		if (isElementPresent(DeleteAlertPopup)) {
			String deletPopUpMessage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.deleteMessages_textMessage);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deleteButton, "Delete");
			click(Lanlink_Metro_Obj.Lanlink_Metro.deleteButton, "Delete");
			
			scrollIntoTop();

			waitforPagetobeenable();

	//		verifysuccessmessage("Site Order successfully deleted");
		} else {
			Reporter.log("Delete alert popup is not displayed");
		}
	}

	public void deleteDeviceFromServiceForIntermediateequipment(String devicename_IntEquipment)
			throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+ devicename_IntEquipment +"')]]]//span[text()='Delete']")).click();

		Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Intermediate Equipment");
		Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Intermediate Equipment");

		boolean deletemessage = isElementPresent(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.deleteMessage_equipment));
		while (deletemessage) {
			Reporter.log(
					"Are you sure want to delete - message is getting displayed on clicking DeletefromService link");
			Reporter.log("Delete popup message is getting displayed");
			String actualMessage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.deleteMessage_equipment);
			Reporter.log("Delete device popup is displaying and popup message displays as: " + actualMessage);
			break;
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.deletebutton_deletefromsrviceforequipment,"Delete");
		waitforPagetobeenable();
		Reporter.log("Device got deleted from service as expected");

	}

	public void hideInterface_IntEquipment() throws IOException, InterruptedException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevoce_hideInterfacelink_intEquip);
		waitforPagetobeenable();

	}

	public String fetchCountryValue_InviewPage() throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		String manageAdres = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.fetchCountryValue);
		String managementAddress = manageAdres;

		return managementAddress;
	}

	public void IntEquip_clickviewButton(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");

		waitToPageLoad();

		clickOnBankPage();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		boolean viewpage = false;
		try {
			viewpage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.viewCPEdevicepage_devices);

			if (viewpage) {
				Reporter.log("In view page");
			} else {

				((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

				waitforPagetobeenable();

				click(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
				waitforPagetobeenable();
			}
		} catch (Exception e) {
			e.printStackTrace();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
			waitforPagetobeenable();
		}
	}

	public void verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(String existingDeviceName)
			throws IOException, InterruptedException {

		waitToPageLoad();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.existingDevice_SelectDeviceToggleButton, "Select Device");
		waitforPagetobeenable();

		waitToPageLoad();

		click(Lanlink_Metro_Obj.Lanlink_Metro.chooseAdeviceDropdown, existingDeviceName);
		waitToPageLoad();

		click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");

	}

	public void Enteraddsiteorder() throws IOException, InterruptedException {

		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		//click(Lanlink_Metro_Obj.Lanlink_Metro.Actiondropdown_siteorder,"Action option");
		//waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorderlink,"Add site order");
	}

	public void PageNavigation_NextPage() throws IOException, InterruptedException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.Pagenavigationfornextpage);
		waitforPagetobeenable();

	}

	public void selectInterfacelinkforEqipment(String devicename) throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//div//span[text()='Select Interfaces']")).click();
		Reporter.log("SelectInterface link for Equipment is selected");
		Reporter.log("Select an inertface to add with the service under equipment");

		waitforPagetobeenable();
	}

	public void selectCustomertocreateOrderfromleftpane(String ChooseCustomerToBeSelected)
			throws IOException, InterruptedException {

		mouseMoveOn(Lanlink_Metro_Obj.Lanlink_Metro.ManageCustomerServiceLink);
		waitforPagetobeenable();
		Reporter.log("Mouser hovered on Manage Customer's Service");

		click(Lanlink_Metro_Obj.Lanlink_Metro.CreateOrderServiceLink, "Create Order/Service Link");
		Reporter.log("=== Create Order/Service navigated ===");

		// Entering Customer name
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.entercustomernamefield, ChooseCustomerToBeSelected, "Customer Name");

		// Select Customer from dropdown
		addDropdownValues_commonMethod("Choose a customer",Lanlink_Metro_Obj.Lanlink_Metro.chooseCustomerdropdown, ChooseCustomerToBeSelected);
		click(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton, "Next");

	}

	public void selectServiceType(String serviceTypeToBeSelected) throws IOException, InterruptedException {

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.servicetypetextfield, "service type textfield");
		addDropdownValues_commonMethod("Service Type", Lanlink_Metro_Obj.Lanlink_Metro.servicetypetextfield, serviceTypeToBeSelected);

	}

	public void selectOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException

	{
		String neworderSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"new order Selection");
		String neworderno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existing order dropdown");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingOrderNumber");
		String existingorderSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingOrderSelection");

		waitToPageLoad();

		// Create New order or select Existing Order
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		if (neworderSelection.equalsIgnoreCase("YES")) {
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.selectorderswitch, "select order switch");
			click(Lanlink_Metro_Obj.Lanlink_Metro.existingorderdropdown, neworderno);
		}

		else if (existingorderSelection.equalsIgnoreCase("YES")) {

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_Metro_Obj.Lanlink_Metro.selectorderswitch, "select order switch");
			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",Lanlink_Metro_Obj.Lanlink_Metro.existingorderdropdown, existingordernumber);
			Reporter.log("=== Order Contract Number selected===");

			waitforPagetobeenable();

		} else {
			Reporter.log("Order not selected");
		}
	}

	public String fetchProActiveMonitoringValue() throws IOException {

		String proactiveMonitor = "No";

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_Servicepanel);
		

		proactiveMonitor = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.fetchProActiveMonitoringValue);

		return proactiveMonitor;
	}

	public void syncservices() throws IOException, InterruptedException {

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);
		
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_actiondropdown, "Action");
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_sysnchronizelink, "Synchronize link");
		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		boolean syncSuccessMessage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.alertForSynchronize);
		
		if (syncSuccessMessage) {

			String actualmsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.alertMSG_synchronize);
			Reporter.log(" success message for synchronize displays as: " + actualmsg);

		} else {
			Reporter.log(" Success message did not display after clicking on 'Synchronize' button");
		}
	}

	public void shownewInfovista() throws IOException, InterruptedException {

		String orderPanel = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);
		scrollDown(orderPanel);
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_actiondropdown, "Action");
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_infovistareport, "Show infovista link");
		waitforPagetobeenable();

		String expectedPageName = "SSO login Page";

		// Switch to new tab
		List<String> browserTabs = new ArrayList<String>(webDriver.getWindowHandles());
		webDriver.switchTo().window(browserTabs.get(1));
		waitforPagetobeenable();

		try {
			// Get Tab name
			String pageTitle = webDriver.switchTo().window(browserTabs.get(1)).getTitle();
			Reporter.log("page title displays as: " + pageTitle);

			waitforPagetobeenable();
			webDriver.switchTo().window(browserTabs.get(0));

			sa.assertEquals(pageTitle, expectedPageName,
					" on clicking 'Show Infovista link', it got naviagted to " + pageTitle);

			sa.assertAll();

			waitforPagetobeenable();

		} catch (AssertionError e) {

			e.printStackTrace();

			waitforPagetobeenable();
			webDriver.switchTo().window(browserTabs.get(0));

		}

	}

	public void manageSubnets() throws IOException, InterruptedException {

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_actiondropdown, "Edit service action drop down");
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_managesubnets, "Manage Subnet");
		waitforPagetobeenable();

		boolean manageSubnetPage = false;
		try {
			manageSubnetPage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.manageSubnetPage_header);
			if (manageSubnetPage) {
				Reporter.log("'Manage Subnet' page is displaying");

				String errMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.manageSubnet_errMsg);
				if (errMsg.isEmpty()) {

					Reporter.log("No messages displays under 'manage Subnet' page");
				} else {
					Reporter.log(" Message in 'Manage Subnet' page displays as " + errMsg);
				}

			} else {
				Reporter.log("'Manage Subnet' page is not displaying");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("'Manage Subnet' page is not displaying");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.cancelButton, "Cancel");
		waitforPagetobeenable();

	}

	public void dump_viewServicepage() throws IOException, InterruptedException {

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_actiondropdown,"Action");
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_Dumplink,"Dump");
		waitforPagetobeenable();

		waitToPageLoad();

		String dumpelement = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.dump_container);

		String dumpvalue = dumpelement;

		if (dumpvalue.isEmpty()) {

			Reporter.log("NO values dipslaying under 'Dump' page");

		} else {
			Reporter.log("Dump value is displaying as:   " + dumpvalue);
		}

		webDriver.navigate().back();
		waitforPagetobeenable();
		waitToPageLoad();

	}

	public void returnbacktoviewsiteorderpage() throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Backbutton,"Back");
		waitforPagetobeenable();
	}

	public void selectRowForsiteorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String siteordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Siteordernumber");
		String topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String modularMSp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String siteordernumber_p2p_mspSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrderNumber_p2p_mspSelected");
		String siteOrderNumber_10G_p2p = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrderNumber_10G_PointToPoint");
		String siteOrdernumber_P2P = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrderNumber_PointToPoint");

		Reporter.log("-----------------------------" + siteordernumber + "---------------------");
		int TotalPages;

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		// WebElement
		// web=getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.TotalPagesforsiteorder"));
		// Reporter.log(" webelement name for total page: "+ web);
		// String TextKeyword =
		// getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.TotalPagesforsiteorder")).);
		//
		// TotalPages = Integer.parseInt(TextKeyword);
		//
		// Reporter.log("Total number of pages in table is: " + TotalPages);
		//
		// ab:
		//
		// for (int k = 1; k <= TotalPages; k++) {
		//
		// // Current page
		// String CurrentPage =
		// getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.Currentpageforsiteorderfunc")));
		// int Current_page = Integer.parseInt(CurrentPage);
		// Reporter.log("The current page is: " + Current_page);
		//
		// assertEquals(k, Current_page);
		//
		// Reporter.log("Currently we are in page number: " + Current_page);

		// div[div[contains(text(),'" + siteOrdernumber_P2P +
		// "')]]//span[@class='ag-icon ag-icon-checkbox-unchecked'] --> needs to
		// be updated
		List<WebElement> results = null;
		
		if(topology.equalsIgnoreCase("Point-to-Point")) {
			if(modularMSp.equalsIgnoreCase("yes")) {
				results=findWebElements("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteordernumber_p2p_mspSelected + "']");
			}
			else if(modularMSp.equalsIgnoreCase("No")) {
				if(interfaceSpeed.equals("10GigE")) {
					results=findWebElements("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteOrderNumber_10G_p2p + "']");
				}else if(interfaceSpeed.equals("1GigE")) {
					results=findWebElements("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteOrdernumber_P2P + "']");
				}	
			}
			
		}else {
			results=findWebElements("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteordernumber + "']");
		}
	
		int numofrows = results.size();
		Reporter.log("no of results: " + numofrows);
		boolean resultflag;
//
		// if (numofrows == 0) {
		//
		//// PageNavigation_NextPage();
		// click(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.pagenavigationfornextpage_underviewserviceforsiteorder")));
		//
		//
		// }
		//
		// else {
		//
		// for (int i = 0; i < numofrows; i++) {
		//
		// try {

		resultflag = results.get(0).isDisplayed();
		Reporter.log("status of result: " + resultflag);
		if (resultflag) {
			Reporter.log(results.get(0).getText());
			results.get(0).click();
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.Actiondropdown_siteorder,"action drop down");

			waitforPagetobeenable();

		}
		//
		// } catch (StaleElementReferenceException e) {
		// // TODO Auto-generated catch block
		// // e.printStackTrace();
		// results = driver.findElements(
		// By.xpath("//div[div[contains(text(),'" + siteordernumber +
		// "')]]//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
		// numofrows = results.size();
		// // results.get(i).click();
		// Reporter.log("selected row is : " + i);
		// }
		// }
		// break ab;
		// }
		// }
	}

	public void addOvertureCircuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addOverture_serviceNameForCreatingCircuit");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		// Click on Add Overture Link
		click(Lanlink_Metro_Obj.Lanlink_Metro.addOvertureLink, "Add Overture");
		waitforPagetobeenable();
		waitToPageLoad();

		boolean overturePanelHeader = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_overturePanel);
		if (overturePanelHeader) {
			Reporter.log("'Overture' page is displaying");

			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_serviceNameTextField, serviceName, "Service Name"); // Search
																													// ame
																													// text
																													// Field

			click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_searchButton, "Search"); // Search
																					// Button
			waitforPagetobeenable();
			waitToPageLoad();

			String selectValueInTable = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderAddOverturePage)
					.replace("value", serviceName);
			try {
				isElementPresent(selectValueInTable);
				Reporter.log("Records displays for the Service " + serviceName);

				click(selectValueInTable);
				waitforPagetobeenable();

				click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_OKbutton, "OK");
				waitforPagetobeenable();
				waitToPageLoad();

			} catch (Exception e) {
				e.printStackTrace();
				Reporter.log("No record displays for the Service " + serviceName);

				click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_cancelButton, "cancel");
			}
		} else {
			Reporter.log("'Overture' page is not displaying");
		}
	}

	public void addOveture_PAMtest_selectRow(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String interface1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addOverture_interfaceName1");

		waitToPageLoad();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		String selectInterface = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMtest_selectinterface).replace("value",
				interface1);
		click(Lanlink_Metro_Obj.Lanlink_Metro.PAMtest_selectinterface, selectInterface);
		Reporter.log(interface1 + " is selected for performing 'PAM Test'");

		click(Lanlink_Metro_Obj.Lanlink_Metro.PAMtest_actionDropdown, "Action");
		click(Lanlink_Metro_Obj.Lanlink_Metro.PAMtest_Link, "PAMTest");

		waitforPagetobeenable();
		waitToPageLoad();

	}

	public void deleteCircuit() throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_deleteButton, "deleteLink");

		click(Lanlink_Metro_Obj.Lanlink_Metro.deleteCircuit_deleteButton, "deleteCircuitpopup_deleteLink");
		waitforPagetobeenable();

		waitToPageLoad();

		verifysuccessmessage("Circuit deleted successfully");

	}

	public boolean EquipmentCOnfigurationPanel() throws IOException {

		boolean EquipConfigPanel = false;
		EquipConfigPanel = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.EquipementConfigurationPanel);
		if (EquipConfigPanel) {
			Reporter.log(
					"In 'view Site Order' page, 'Equipment Configuration' panel is displaying as expected for 'Actelis' Technology");
		} else {
			Reporter.log(
					"In 'view Site Order' page, 'Equipment Configuration' panel is not displaying for 'Actelis' Technology");

		}
		return EquipConfigPanel;
	}

	public String SelectShowInterfacelinkAndVerifyEditInterfacePage(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo,String devicename_EquipActual) throws IOException, InterruptedException {
		
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_showinterfaceslink);
		waitforPagetobeenable();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		//scrollDown("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename_EquipActual +"')]]]");
		
		waitforPagetobeenable();

		String interfaceAvailability = selectRowForEditingInterface_showInterface(interfacename, devicename_EquipActual);

		if(interfaceAvailability.equalsIgnoreCase("Yes")) {
		click(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_showintefaceedit);
		waitforPagetobeenable();
		}
		return interfaceAvailability;

	}

	public void hideInterfaceLink_Equipment() throws IOException, InterruptedException {

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_hideInterfaeLink_eqiupment);
		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_hideInterfaeLink_eqiupment,"Hide interfaces link");
		waitforPagetobeenable();
	}

	public String fetchCSRsiteName() throws IOException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		String CSrname = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.physicalSiteCSRName_viewSiteOrder);
		String CSRname = CSrname;

		return CSRname;
	}

	public String fetchDeviceCityName() throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		String cityName = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.deviceCityName_viewSiteOrder);
		String dveiceCityName = cityName;

		return dveiceCityName;
	}

	public String fetchSiteOrderCountryName() throws IOException {

		String countryName = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.countryName_viewSiteOrder);
		String siteOrder_countryName = countryName;

		return siteOrder_countryName;
	}

	public void clickAMNvalidatorLink() throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.siteOrder_ActionButton, "Action"); // click
																				// on
																				// Action
																				// button

		click(Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidatorlink, "AMNvalidator");
	}

	public void amnvalidator_viewServicepage() throws IOException, InterruptedException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_actiondropdown, "Action");
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_AMNvalidator, "Manage Subnet");
		waitforPagetobeenable();
	}

	public void deleteDeviceFromServiceForequipment(String devicename) throws IOException, InterruptedException {
		

		webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='Delete']")).click();;

		Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Equipment");
		Reporter.log(" 'Delete From Service' link has been clicked for cpe device under Equipment");

		boolean deletemessage = isElementPresent(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.deleteMessage_equipment));
		while (deletemessage) {
			Reporter.log(
					"Are you sure want to delete - message is getting displayed on clicking DeletefromService link");
			Reporter.log("Delete popup message is getting displayed");
			String actualMessage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.deleteMessage_equipment);
			Reporter.log("Delete device popup is displaying and popup message displays as: " + actualMessage);
			break;
		}

			click(Lanlink_Metro_Obj.Lanlink_Metro.deletebutton_deletefromsrviceforequipment,"Delete button");
			waitforPagetobeenable();
			Reporter.log("Device got deleted from service as expected");

		
	}

	public void successMessage_deleteFromService() throws IOException, InterruptedException {

		verifysuccessmessage("Device successfully deleted");
	}

	public void addDevice_IntEquipment() throws IOException, InterruptedException {

		// click on Add device link
		click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_adddevicelink,"Add device link");
		waitforPagetobeenable();
	}

	public void selectTechnology(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String technologyToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechToBeselected_underTechpopup_device");

		// verify Technology popup
		boolean technologypopup = false;
		technologypopup = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.technologyPopup);
		if (technologypopup) {
			Reporter.log("Technology popup is displaying as expected");
		} else {
			Reporter.log("Technology popup is not displaying");
		}

		// Dropdown values inside popup
		boolean technologyDropdown = false;
		technologyDropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.technologypopup_dropdown);
		if (technologyDropdown) {
			Reporter.log("Technology dropdown is displaying as expected");

			click(Lanlink_Metro_Obj.Lanlink_Metro.technologypopup_dropdown);
			waitforPagetobeenable();

			// verify list of values inside technology dropdown
			List<String> listofTechnololgy = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listOfTechnology)));

			if (listofTechnololgy.size() > 0) {

				for (String technoloyTypes : listofTechnololgy) {
					Reporter.log("List of values available under 'Technology' dropdown are: " + technoloyTypes);
				}
			}

			// Select the Technology
			webDriver.findElement(By.xpath("//div[@class='modal-body']//div[contains(text(),'"+ technologyToBeSelected +"')]")).click();
			waitforPagetobeenable();
			String actualValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.tchnologyPopup_dropdownValues);
			Reporter.log(" 'Technology' selected is: " + actualValue);

		} else {
			Reporter.log("Technology dropdown is not displaying");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateEquipment_OKbuttonforpopup,"Ok button");
		waitforPagetobeenable();
	}

	public void verifyeditedinterfacevalue(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String interfaceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

		List<String> interfacedetails = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.interfacedetails1 + interfaceName
						+ Lanlink_Metro_Obj.Lanlink_Metro.interfacedetails2)));

		for (String listOfInterfacenames : interfacedetails) {

			String ColumnNames = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.interfacedetails1 + interfaceName
					+ Lanlink_Metro_Obj.Lanlink_Metro.interfacedetails2, "col-id");
			String values = listOfInterfacenames;

			Reporter.log("After Editing interface, list of values displaying in interface table are: ");
			Reporter.log("value displaying for " + ColumnNames + " is: " + values);
			
			

		}
	}

	public void pamTest(String ServiceID)
			throws IOException, InterruptedException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.PAMtest_Link, "PAMtest_Link");
		waitforPagetobeenable();
		waitForAjax();
		waitforPagetobeenable();

		boolean pamTestPage = false;
		try {
			pamTestPage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.PAMtest_popupPage);
			if (pamTestPage) {
				Reporter.log("'PAM Test' popup page is displaying");

				// Type Value
				String typeValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_TypeFieldValue);
				if (typeValue.isEmpty()) {
					Reporter.log("No values displaying under 'Type' field");
				} else {
					Reporter.log("Under 'Type' field, value is dispaying as: " + typeValue);
				}

				// Service
				String serviceValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_ServiceValue);
				if (serviceValue.isEmpty()) {
					Reporter.log("No values displaying under 'Service' field");
				} else {
					compareText("Service", Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_ServiceValue , ServiceID);
				}

				// Tool Response
				String toolResponse = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_ToolResponse);
				if (toolResponse.isEmpty()) {
					Reporter.log("No values displaying under 'Tool Response' field");
				} else {
					Reporter.log("Under 'Tool Response' field, value is dispaying as: " + toolResponse);
				}

				// click on "X"button to close the popup
				click(Lanlink_Metro_Obj.Lanlink_Metro.configure_alertPopup_xbutton, "X");

			} else {
				Reporter.log("'PAM Test' popup page is not displaying");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("'PAM Test' popup page is not displaying");
		}
	}

	public void selectInterfacelinkforIntermediateEqipment(String devicename) throws IOException, InterruptedException {

		try {
			click(Lanlink_Metro_Obj.Lanlink_Metro.selectInterface1 + devicename
					+ Lanlink_Metro_Obj.Lanlink_Metro.selectInterface2);
			Reporter.log("SelectInterface link for Intermediate Equipment is selected");
			Reporter.log("Select an inertface to add with the service under Intermediate equipment");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void Equip_clickviewButton(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");

		clickOnBankPage();
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		boolean viewpage = false;
		try {
			viewpage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.viewCPEdevicepage_devices);

			if (viewpage) {
				Reporter.log("In view page");
			} else {

				((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

				waitforPagetobeenable();

				click(Lanlink_Metro_Obj.Lanlink_Metro.deviceView1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.deviceView2);
				waitforPagetobeenable();
			}
		} catch (Exception e) {
			e.printStackTrace();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.deviceView1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.deviceView2);
			waitforPagetobeenable();
		}

	}

	public String fetchdevicename_InviewPage() throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		String devicename = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.fetchDeviceValue);
		String devieName = devicename;

		return devieName;
	}

	public String fetchManagementAddressValue_InviewPage() throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
		waitforPagetobeenable();

		String manageAdres = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.fetchmanagementAddressvalue);
		String managementAddress = manageAdres;

		return managementAddress;
	}

	public String fetchVendorModelValue() throws IOException, InterruptedException {
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		String vendorModel = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.fetchVendorModelvalue);
		String vendorValue = vendorModel;

		return vendorValue;
	}

	public void clickOnBreadCrump(String breadCrumpLink) throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		String breadcrumb = null;

		try {
			breadcrumb = (Lanlink_Metro_Obj.Lanlink_Metro.breadcrump).replace("value", breadCrumpLink);

			if (isElementPresent(breadcrumb)) {
				click(breadcrumb,"breadcrumb");
				waitforPagetobeenable();
				waitToPageLoad();
			} else {
				Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
		}
	}

	public boolean findPanelHeader(String Equipment)
			throws IOException, InterruptedException {
		

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		String el = null;
		boolean panelheader = false;
		try {
			panelheader = isElementPresent((Lanlink_Metro_Obj.Lanlink_Metro.devicePanelHeaders_InViewSiteOrderPage).replace("value",Equipment));
			//el = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.devicePanelHeaders_InViewSiteOrderPage).replace("value",Equipment);
			

			if (panelheader) {
				Reporter.log(" 'Equipment' panel is displaying under 'view site order' page");
				panelheader = true;

			} else {
				Reporter.log(" 'Equipment' panel is not displaying under 'view site order' page");
				panelheader = false;

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Equipment' panel is not displaying under 'view site order' page");
			panelheader = false;

		}

		return panelheader;
	}
	
	

	public void verifyValuesforCPEexistingdevice_MSPselected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management Address");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
		String VLANId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLAN Id");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vendor_Model + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Snmpro + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ManagementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MEPId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + VLANId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
	}

	public void verifyCPEdevicedataenteredForIntermediateEquipment_10G(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_10G_Accedian");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
		String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_10G_Accedian");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
		String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
		String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
		String citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
		String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
		String existingcountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
		String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
		String existingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
		String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_manageaddress_dropdownvalue");
		
		

		waitforPagetobeenable();
		clickOnBankPage();
		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();
		waitforPagetobeenable();

		String[] RouterId = new String[2];
		RouterId = cpename.split(".lanlink");

		String RouterIdValue = RouterId[0];

		String mediaSelectionValueInViewDevicePage = "no";
		if (MediaselectionActualValue.equalsIgnoreCase("null")) {
			MediaselectionActualValue = mediaSelectionValueInViewDevicePage;
		} else {
			MediaselectionActualValue = mediaSelectionValueInViewDevicePage;
		}

	//	 verifyEnteredvalues_deviceName("Name", RouterIdValue,cpename );
		  
		  verifyEnteredvalues("Router Id", RouterIdValue);
		  
		  verifyEnteredvalues("Vendor/Model", vender);
		  
		  verifyEnteredvalues("Snmpro", snmpro);
		  
		// Management Address
		if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
			verifyExists(manageaddressdropdownvalue);
		} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
				&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
			verifyExists(managementAddress);
		}

		verifyEnteredvalues("Power Alarm", poweralarm);
		  
		  verifyEnteredvalues("Media Selection", MediaselectionActualValue);
		  
		  verifyEnteredvalues("Serial Number", serialNumber);
		  
		  verifyEnteredvalues("Link Lost Forwarding", linkLostForwarding);
		  
		  verifyEnteredvalues("Country", existingcountry);

		//City  
			 if((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {
				 verifyEnteredvalues("City", existingCity);
			 }
			 else if((existingcityselectionmode.equalsIgnoreCase("no")) && (newcityselectionmode.equalsIgnoreCase("Yes"))) {
				 verifyEnteredvalues("City", cityname);
			 } 
			 
			 
			//Site
			 if((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {
				 verifyEnteredvalues("Site", existingSite);
			 }
			 else if((existingsiteselectionmode.equalsIgnoreCase("no")) && (newsiteselectionmode.equalsIgnoreCase("Yes"))) {
				 verifyEnteredvalues("Site", sitename);
			 } 
			 
			 
			//Premise
			 if((existingpremiseselectionmode.equalsIgnoreCase("Yes")) && (newpremiseselectionmode.equalsIgnoreCase("no"))) {
				 verifyEnteredvalues("Premise", existingPremise);
			 }
			 else if((existingpremiseselectionmode.equalsIgnoreCase("no")) && (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
				 verifyEnteredvalues("Premise", premisename);
			 } 

		}
	public void selectInterfaceForCircuits(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String interface1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addOverture_interfaceName1");
		String interface2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addOverture_interfaceName2");
		String selectEdgePointForInterface1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addOverture_edgePointSelectForInterface1");
		String selectEdgePointForInterface2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addOverture_edgePointSelectForInterface2");

		if (isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_interfaceInServicePanel)) {
			Reporter.log("'Interface in Service' page displays");

			// Select row for 1st interface
			click(Lanlink_Metro_Obj.Lanlink_Metro.interfaceFilterButton, "interface_filter");

			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.interfacePage_filterText, interface1, "filter");

			String selectInterface = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.interfaceInService_selectValueUnderTable)
					.replace("value", interface1);
			click(selectInterface);
			Reporter.log(interface1 + " is selected under 'Interface In Service' page");

			if (selectEdgePointForInterface1.equalsIgnoreCase("Yes")) {
				String selectEdgePoint = getTextFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.interfaceinService_selectEdgePointforInterface).replace("value",
								interface1);
				click(selectEdgePoint);
				Reporter.log(interface1 + " is selected under 'Interface In Service' page");

			} else {
				Reporter.log("'Edge End Point' radio button is not selected for " + interface1);
			}

			// select row for 2nd interface
			click(Lanlink_Metro_Obj.Lanlink_Metro.interfaceFilterButton, "interface_filter");

			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.interfacePage_filterText, interface2, "filter");
			waitforPagetobeenable();

			String selectInterface2 = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.interfaceInService_selectValueUnderTable)
					.replace("value", interface2);
			click(selectInterface2);
			Reporter.log(interface2 + " is selected under 'Interface In Service' page");

			if (selectEdgePointForInterface2.equalsIgnoreCase("Yes")) {
				String selectEdgePoint2 = getTextFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.interfaceinService_selectEdgePointforInterface).replace("value",
								interface2);
				click(selectEdgePoint2);
				Reporter.log(interface2 + " is selected under 'Interface In Service' page");

			} else {

				Reporter.log("'Edge End Point' radio button is not selected for " + interface2);
			}

			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_OKbutton, "OK");

		} else {
			Reporter.log("'Interface in Service' page not displays");
		}
	}

	public void PAMtest_ForCircuitCreation(String testDataFile, String sheetName, String scriptNo, String dataSetNo,
			String siteName) throws InterruptedException, IOException {
		String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addOverture_serviceNameForCreatingCircuit");

		boolean pamTestPage = false;
		try {
			pamTestPage = isElementPresent(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMtest_popupPage));
			if (pamTestPage) {
				Reporter.log("'PAM Test' popup page is displaying");

				// Type Value
				String typeValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_TypeFieldValue);
				if (typeValue.isEmpty()) {
					Reporter.log("No values displaying under 'Type' field");
				} else {
					Reporter.log("Under 'Type' field, value is dispaying as: " + typeValue);
				}

				// Service
				String serviceValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_ServiceValue);
				if (serviceValue.isEmpty()) {
					Reporter.log("No values displaying under 'Service' field");
				} else {
					serviceName.equals(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_ServiceValue));
				}

				// site
				String siteValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_siteValue);
				if (siteValue.isEmpty()) {
					Reporter.log("No values displaying under 'Site' field");
				} else {
					siteName.equals(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_siteValue));
				}

				// Tool Response
				String toolResponse = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.PAMTest_ToolResponse);
				if (toolResponse.isEmpty()) {
					Reporter.log("No values displaying under 'Tool Response' field");
				} else {
					Reporter.log("Under 'Tool Response' field, value is dispaying as: " + toolResponse);
				}

				// click on "X"button to close the popup
				click(Lanlink_Metro_Obj.Lanlink_Metro.configure_alertPopup_xbutton, "X");

			} else {
				Reporter.log("'PAM Test' popup page is not displaying");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("'PAM Test' popup page is not displaying");
		}
	}

	public void showInterface_ActelisConfiguuration() throws IOException, InterruptedException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.showInterface_ActelisCnfiguration);
		waitforPagetobeenable();
	}

	public void deletInterface_ActelisConfiguration(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		String interfaceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interfaceName");

		// select the interface
		click(Lanlink_Metro_Obj.Lanlink_Metro.deleteInterafce1 + interfaceName
				+ Lanlink_Metro_Obj.Lanlink_Metro.deleteInterafce2);

		// click on Action button
		click(Lanlink_Metro_Obj.Lanlink_Metro.AcionButton_ActelisConfiguration);

		// Remove Button
		click(Lanlink_Metro_Obj.Lanlink_Metro.removeButton_ActelisConfiguration);

		boolean popupMessage = false;
		popupMessage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.popupMessage_forRemove_ActelisConfiguration);

		if (popupMessage) {
			String actualmsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.popupMessage_forRemove_ActelisConfiguration);
			Reporter.log(" On clicking remoe button, popup message displays as: " + actualmsg);

			click(Lanlink_Metro_Obj.Lanlink_Metro.deleteInterfaceX);
			waitforPagetobeenable();
		} else {

			// popup message does not display after clicking on 'Remove' button
		}
	}

	public void successMessage_deleteInterfaceFromDevice_ActelisConfiguration() throws IOException {

		boolean successMessage = isElementPresent(
				Lanlink_Metro_Obj.Lanlink_Metro.successMessage_ActelisConfiguration_removeInterface);
		String actualmessage = getTextFrom(
				Lanlink_Metro_Obj.Lanlink_Metro.successMessage_ActelisConfiguration_removeInterface);
		if (successMessage) {

			Reporter.log(" Success Message for removing interface is dipslaying as expected");

			Reporter.log("Message displays as: " + actualmessage);

		} else {
			Reporter.log(" Success Message for removing Interface is not dipslaying");
		}
	}

	public void equipConfiguration_Actelis_AddDevice(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management Address");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
		String ETH_Port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ETH Port");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.equipConfig_addCPEdevice);
		waitforPagetobeenable();

		boolean addActelisHeader = false;
		addActelisHeader = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.addActelisCPEpage_headerName);
		if (addActelisHeader) {
			Reporter.log(" 'Add Actelis CPE device' page is displaying as expected");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
			click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
			waitforPagetobeenable();

			// Validate Warning Messages_Name Field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.devicenameFieldErrMSg_addCPE_Actelis, "Name");

			// Validate Warning Messages_Router ID Field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.RouterIDFieldErrMSg_addCPE_Actelis, "Router Id");

			// Validate Warning Messages_Management Address Field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.manageAddressFieldErrMSg_addCPE_Actelis, "Management Address");

			// Name
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.nameField_addCPE_Actelis, devicename, "Name");

			// vendor/Model
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.vendorField_addCPE_Actelis, Vendor_Model, "Vendor/Model");

			// Router Id
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.RouterIdField_addCPE_Actelis, RouterId, "Router Id");

			// Management Address
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.managementAddressField_addCPE_Actelis, ManagementAddress,
					"Management Address");

			// MEP Id
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.MEPidField_addCPE_Actelis, MEPId, "MEP Id");

			// ETH Port
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.ETHportField_addCPE_Actelis, ETH_Port, "ETH Port");

			// CAncel button
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel, "Cancel");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
			click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
			waitforPagetobeenable();

		} else {
			// Add Actelis CPE device' page is not displaying
		}
	}

	public void verifyDataEnteredFordeviceCreation_Actelis(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
		// String Vendor_Model=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "Vendor/Model");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management Address");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
		String ETH_Port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ETHPort");

		waitToPageLoad();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.actelis_EquipConfig_viewLink, "view_Link");
		waitforPagetobeenable();

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1+Vendor_Model+Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ManagementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MEPId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ETH_Port + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

	}

	public void deleteDeviceFromService_EquipmentConfig_Actelis() throws InterruptedException, IOException {
		waitToPageLoad();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.actelis_EquipConfig_deleteLink, "delete_link");

		Reporter.log(
				" 'Delete From Service' link has been clicked for cpe device under 'Equipment Configuration' panel");

		boolean deletemessage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.deleteMessage_equipment);
		while (deletemessage) {
			Reporter.log(
					"Are you sure want to delete - message is getting displayed on clicking DeletefromService link");
			Reporter.log("Delete popup message is getting displayed");
			String actualMessage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.deleteMessage_equipment);
			Reporter.log("Delete device popup is displaying and popup message displays as: " + actualMessage);
			break;
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.deletebutton_deletefromsrviceforequipment);
		waitforPagetobeenable();
		Reporter.log("Device got deleted from service as expected");

	}

	public void verifyAddDSLAMandHSLlink() throws InterruptedException {

		boolean actelisConfigurationPanel = false;

		waitToPageLoad();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		actelisConfigurationPanel = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ActelisConfigurationPanel);

		if (actelisConfigurationPanel) {
			Reporter.log(" 'Actelis Configuration' panel is displaying as expected");

			boolean actelisLink = false;
			try {
				actelisLink = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Actelisconfig_addDSLAM);
				if (actelisLink) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Actelisconfig_addDSLAM);
					waitforPagetobeenable();

				}

			} catch (NoSuchElementException e) {
				e.printStackTrace();
			} catch (Exception err) {
				err.printStackTrace();
			}
		} else {
			// Actelis Configuration' panel is not displaying
		}

	}

	public void AddDSLAMandHSL(String DSLMdevice, String HSlname) throws InterruptedException, IOException {

		waitToPageLoad();

		click(Lanlink_Metro_Obj.Lanlink_Metro.addDSLAMandHSL_xButton);
		Reporter.log("Clicked on 'X' button under 'DSLAM device' dropdown");

		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.DSLM_Device_Select, DSLMdevice, "DSLMdevice");

		String valueToSElect = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.selectDSLAMdeviceValue).replace("value",
				DSLMdevice);

		try {
			if (isElementPresent(valueToSElect)) {
				Reporter.log(DSLMdevice + " is displaying under 'DSLAM device' dropdown");

				click(valueToSElect);
				Reporter.log(DSLMdevice + " is selected under 'DSLAM device' dropdown");

				waitToPageLoad();

				click(Lanlink_Metro_Obj.Lanlink_Metro.List_HSL_Link, "List_HSL"); // click
																				// on
																				// "List
																				// HSL"
																				// button

				selectRowForAddingInterface_Actelis(HSlname); // select the
																// Interface

			} else {
				Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
		}
	}

	public void selectconfigurelinkAndverifyEditInterfacefield__Equipment(String devicename) throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='Configure']")).click();

		waitforPagetobeenable();

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureActiondropdown);
		
		waitforPagetobeenable();

		// Try to edit without selecting the interface
		click(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureActiondropdown);
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureEditlink);
		waitforPagetobeenable();

		// check whether Alert message displays
		boolean alertMessage = false;
		try {
			alertMessage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.configure_alertPopup);
			if (alertMessage) {
				Reporter.log("Alert popup displays, if we click on 'Edit' without selected the interface");

				String alertMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.configure_alertMessage);
				Reporter.log(" Alert message displays as: " + alertMsg);

				click(Lanlink_Metro_Obj.Lanlink_Metro.configure_alertPopup_xbutton,"X button");
				waitforPagetobeenable();
			} else {
				Reporter.log("'Alert' popup is not displaying when we click on 'edit' without selecting an interface");

				boolean editInterface_popuptitleName = false;
				try {
					editInterface_popuptitleName = isElementPresent(
							Lanlink_Metro_Obj.Lanlink_Metro.Editinterface_popupTitlename);
					if (editInterface_popuptitleName) {
						Reporter.log("Edit interface popup is displaying without selected an interface");

						click(Lanlink_Metro_Obj.Lanlink_Metro.EditInterfacepopup_xbutton);
						waitforPagetobeenable();
					}
				} catch (Exception e) {
					e.printStackTrace();
					Reporter.log(" 'Edit interface' popup is not displaying, without selecting an Interface");
				}

			}
		} catch (NoSuchElementException e) {
			Reporter.log("'Alert' popup is not displaying when we click on 'edit' without selecting an interface");

		} catch (Exception ee) {
			ee.printStackTrace();
			Reporter.log("'Alert' popup is not displaying when we click on 'edit' without selecting an interface");
		}

	}

	public void testStatus() throws IOException {

		String element = null;
		String status = null;

		List<String> testlist = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.testlist)));

		int listSize = testlist.size();

		for (int i = 1; i <= listSize; i++) {
			try {
				element = getTextFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.testlist1 + i + Lanlink_Metro_Obj.Lanlink_Metro.testlist2);

				if (element.isEmpty()) {

				} else {
					Reporter.log("Test Name is displaying as: " + element);

					status = getAttributeFrom(
							Lanlink_Metro_Obj.Lanlink_Metro.testlist1 + i + Lanlink_Metro_Obj.Lanlink_Metro.testlist3, "class");
					Reporter.log("status displays as: " + status);

					if (status.contains("red")) {
						Reporter.log(element + " status colour dipslays as: red");
					} else if (status.contains("green")) {
						Reporter.log(element + " status colour dipslays as: green");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean fetchDeviceInterface_viewdevicepage() throws IOException, InterruptedException {

		boolean clickLink = false;

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.fetchDeviceinterfacelink_viewDevicePage, "fetchDeviceInterfaceLink");

		waitforPagetobeenable();
		waitToPageLoad();

		// verify success Message
		String expectedValue = "Fetch interfaces started successfully. Please check the sync status of this device";
		boolean successMessage = false;
		try {
			successMessage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fetchDeviceInterace_SuccessMessage);
			String actualMessage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.fetchdeviceInterface_successtextMessage);
			if (successMessage) {

				if (actualMessage.isEmpty()) {
					Reporter.log("No messages displays");
				}
				if (actualMessage.contains(expectedValue)) {

					Reporter.log(
							" After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected");

					Reporter.log(" Success Message displays as: " + actualMessage);

					// click on the 'click here' link
					// click((Lanlink_Metro_Obj.Lanlink_Metro.ClickhereLink_fetchInterface);
					// waitforPagetobeenable();

					clickLink = true;

				} else if (actualMessage.equalsIgnoreCase(expectedValue)) {

					Reporter.log(
							" After clicking on 'Fetch Device Interface' link, success Message is displaiyng as expected");

					Reporter.log(" Success Message displays as: " + actualMessage);

					// click on the 'click here' link
					// click(Lanlink_Metro_Obj.Lanlink_Metro.ClickhereLink_fetchInterface);
					// waitforPagetobeenable();

					clickLink = true;
				} else {
					Reporter.log(
							"After clicking on 'Fetch Device Interface' link, message displays as " + actualMessage);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("After clicking on 'Fetch Device Interface' link, success message is not displaying");

		}
		return clickLink;
	}

	public void verifyValuesforCPEexistingdevice_1G_intEquipment(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management Address");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
		String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Media selection");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Link Lost Forwarding");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
		String MACAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vendor_Model + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Snmpro + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ManagementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MEPId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + PowerAlarm + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MediaselectionActualValue
				+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + serialNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(
				Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + linkLostForwarding + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + cityname + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MACAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

	}

	public void verifyValuesforCPEexistingdevice_1G_Equipment(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management Address");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
		String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Media selection");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Link Lost Forwarding");
		String MACAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vendor_Model + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Snmpro + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ManagementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MEPId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + PowerAlarm + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MediaselectionActualValue
				+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + serialNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(
				Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + linkLostForwarding + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MACAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

	}

	public void verifyValuesforCPEexistingdevice_10G_Equipment(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management Address");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
		String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Media selection");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Link Lost Forwarding");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vendor_Model + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Snmpro + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ManagementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MEPId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + PowerAlarm + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MediaselectionActualValue
				+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + serialNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(
				Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + linkLostForwarding + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

	}

	public void verifyValuesforCPEexistingdevice_10G_intEquipment(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String RouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Management Address");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
		String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Media selection");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Link Lost Forwarding");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
		String site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vendor_Model + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Snmpro + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ManagementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MEPId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + PowerAlarm + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MediaselectionActualValue
				+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + serialNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(
				Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + linkLostForwarding + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + cityname + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + site + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
	}

	public void EnterdataForEditInterfaceforShowInterfacelinkunderIntermediateEquipment(String testDataFile,
			String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException {
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerType");
		String bearerspeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerSpeed");
		String totalbandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editInterfacepage_bandwidth");
		String vlantype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_vlantype");
		String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_Vlanid");
		String circuitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_circuitId");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

		// verify Edit Interface page
		boolean editInterface_popuptitleName = false;
		try {
			editInterface_popuptitleName = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Editinterface_popupTitlename);
			if (editInterface_popuptitleName) {
				Reporter.log("Edit interface popup is displaying as expected");

				// Interface name
				verifyenteredvaluesForEditPage_noneditableFields("Interface",interfacename);

				// Circuit ID
				configure_circuitId(circuitId);

				// Bearer type
				addDropdownValues_commonMethod("Bearer Type",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureBearerType, bearertype_value);

				// Bearerspeed
				addDropdownValues_commonMethod("Bearer Speed",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureBearerSpeed, bearerspeed);

				// Total Circuit bandwidth
				addDropdownValues_commonMethod("Total Circuit Bandwidth",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureTotalcircuitbandwidth, totalbandwidth);

				// VLAN type
				addDropdownValues_commonMethod("VLAN Type",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureVlanType, vlantype);

				// vlan
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureVLANid, vlanid, "VLAN Id");
				waitforPagetobeenable();

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Edit interface' popup is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			Reporter.log(" 'Edit interface' popup is not displaying");
		}
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();
	}

	public void SelectShowInterfacelink_IntermediateequipmentAndVerifyEditInterfacePage(
			String Interfacename_forEditInterface, String devicename_IntEquipment)
			throws InterruptedException, IOException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_showinterfaceslink_intEquip,"Show Interfaces");
		waitforPagetobeenable();

		selectRowForEditingInterface_showInterface(Interfacename_forEditInterface, devicename_IntEquipment);

		click(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_showintefaceedit,"Edit");
		waitforPagetobeenable();

	}

	public void verifyCPEdevicedataenteredForIntermediateEquipment_1G(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
		String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
		String technologySelectedfordevicecreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
		String poweralarm_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_overture");
		String powerAlarm_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_Accedian");
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
		String vender_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Overture");
		String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Accedian");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_manageaddressdropdownvalue");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
		String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
		String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
		String existingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
		String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
		String newmanagementAddressSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
		String existingmanagementAddressSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
		
		waitToPageLoad();

		waitforPagetobeenable();
		clickOnBankPage();
		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		 webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();

		waitforPagetobeenable();

		if (technologySelectedfordevicecreation.equalsIgnoreCase("Overture")) {

			viewdevice_Overture(cpename, vender_Overture, snmpro, managementAddress, Mepid, poweralarm_Overture, MediaselectionActualValue,
					Macaddress,linkLostForwarding, country, existingCity, cityname, existingSite, sitename, existingPremise, premisename,existingcityselectionmode,
					newcityselectionmode, existingsiteselectionmode, newsiteselectionmode, newmanagementAddressSelection, existingmanagementAddressSelection,
					manageaddressdropdownvalue, existingpremiseselectionmode, newpremiseselectionmode);

		}

		if ((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian"))
				|| (technologySelectedfordevicecreation.equalsIgnoreCase("Accedian-1G"))) {

			viewdevice_Accedian(cpename, vender_Accedian, snmpro, managementAddress, Mepid, powerAlarm_Accedian, MediaselectionActualValue,
					Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,country, existingCity, cityname, existingSite, sitename, existingPremise, premisename,existingcityselectionmode,
					newcityselectionmode, existingsiteselectionmode, newsiteselectionmode, newmanagementAddressSelection, existingmanagementAddressSelection,
					manageaddressdropdownvalue, existingpremiseselectionmode, newpremiseselectionmode);
		}

	}

	public void VerifyFieldsForServiceSubTypeSelected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String modularmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
		String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
		String serviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		String SelectSubService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
		String vpntopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String proActivemonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"proactiveMonitor");

		if (modularmsp.equalsIgnoreCase("no")) {
			if (Interfacespeed.equalsIgnoreCase("10GigE")) {

				Fieldvalidation_DirectFibre10G(serviceType, SelectSubService, Interfacespeed, vpntopology);

			} else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
				Fieldvalidation_DirectFibre1G(serviceType, SelectSubService, Interfacespeed, proActivemonitoring,
						vpntopology);
			}

		}

		else if (modularmsp.equalsIgnoreCase("yes")) {

			Fieldvalidation_DirectFibreMSPselected(serviceType, SelectSubService, Interfacespeed, proActivemonitoring,
					vpntopology);

		}
	}

	public void dataToBeEnteredOncesubservicesselected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String modularmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
		String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
		String ServiceIdentificationNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
		String SelectSubService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
		String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "remark");
		String EndpointCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "endpointCPE");
		String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "email");
		String PhoneContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "phone");
		String moduPerformanceReportinglarmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"modular msp");
		String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"performanceReportngForServices");
		String ProactiveMonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PerformMonitor");
		String deliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"deliveryChannel");
		String ManagementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ManagementOrder");
		String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String intermediateTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"intermediateTechnology");
		String CircuitReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CircuitReference");
		String CircuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String notificationManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Notification management");
		String COScheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "COScheckbox");
		String Multiportcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"MultiPostcheckbox");
		String ENNIcheckBox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ENNI checkbox");
		String perCocPerfrmReprt = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PerCoS preformance reporting_serviceCreation");
		String actelsBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Actelis Based_service creation");
		String standrdCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StandardCIR_ServiceCreation");
		String standrdEir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StandardEIR_ServiceCreation");
		String prmiumCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "premiumCIR_ServiceCreation");
		String prmiumEir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "premiumEIR_ServiceCreation");
		String E_VPntechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"E_VPNtechnology");
		String HCoSperformreporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"HCoSPerformanceReporting");

		

		if (modularmsp.equalsIgnoreCase("no")) {

			if (Interfacespeed.equalsIgnoreCase("10GigE")) {
				DirectFibre_10G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
						PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
						ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference, CircuitType,
						notificationManagement, COScheckbox, Multiportcheckbox, ENNIcheckBox);
			}

			else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
				DirectFibre_1G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
						PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
						ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference, CircuitType,
						notificationManagement, perCocPerfrmReprt, actelsBased, standrdCir, standrdEir, prmiumCir,
						prmiumEir, COScheckbox, Multiportcheckbox, ENNIcheckBox);
			}
		}

		else if (modularmsp.equalsIgnoreCase("yes")) {

			metro_ModularMSpselected(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
					PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel, ManagementOrder,
					vpnTopology, intermediateTechnology, CircuitReference, CircuitType, notificationManagement,
					perCocPerfrmReprt, actelsBased, standrdCir, standrdEir, prmiumCir, prmiumEir, E_VPntechnology,
					HCoSperformreporting, COScheckbox, Multiportcheckbox, ENNIcheckBox);

		}
	}

	public void VerifydatenteredForServiceSubTypeSelected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String serviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "serviceType");
		String PerformanceMonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PerformMonitor");
					String MultiPostcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Interface Speed");
					String modularmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
					String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
					String ServiceIdentificationNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
					String SelectSubService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
					String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "remark");
					String EndpointCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "endpointCPE");
					String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "email");
					String PhoneContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "phone");
					String moduPerformanceReportinglarmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"modular msp");
					String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"performanceReportngForServices");
					String ProactiveMonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"proactiveMonitor");
					String deliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"deliveryChannel");
					String ManagementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ManagementOrder");
					String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
					String intermediateTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"intermediateTechnology");
					String CircuitReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"CircuitReference");
					String CircuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
					String notificationManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"Notification management");
					String COScheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "COScheckbox");
					String Multiportcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"MultiPostcheckbox");
					String ENNIcheckBox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ENNI checkbox");
					String perCocPerfrmReprt = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"PerCoS preformance reporting_serviceCreation");
					String actelsBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Actelis Based_service creation");
					String standrdCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StandardCIR_ServiceCreation");
					String standrdEir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StandardEIR_ServiceCreation");
					String prmiumCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "premiumCIR_ServiceCreation");
					String prmiumEir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "premiumEIR_ServiceCreation");
					String EVPNtechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"E_VPNtechnology");
					String HCoSPerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"HCoSPerformanceReporting");

					
	
		waitforPagetobeenable();
		try {

			if (modularmsp.equalsIgnoreCase("no")) {

				if (Interfacespeed.equalsIgnoreCase("10GigE")) {

					verifydataEntered_DirectFibre10G(serviceType, ServiceIdentificationNumber, SelectSubService,
							Interfacespeed, EndpointCPE, Email, PhoneContact, remark, PerformanceMonitoring,
							ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology, intermediateTechnology,
							CircuitReference, CircuitType, modularmsp, notificationManagement, ENNIcheckBox,
							COScheckbox, MultiPostcheckbox);

				}

				else if (Interfacespeed.equalsIgnoreCase("1GigE")) {

					verifydataEntered_DirectFibre1G(serviceType, ServiceIdentificationNumber, SelectSubService,
							Interfacespeed, EndpointCPE, Email, PhoneContact, remark, PerformanceMonitoring,
							ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology, intermediateTechnology,
							CircuitReference, CircuitType, modularmsp, perCocPerfrmReprt, actelsBased, standrdCir,
							standrdEir, prmiumCir, prmiumEir, notificationManagement, ENNIcheckBox, COScheckbox,
							MultiPostcheckbox);

				}
			} else {
				verifydataEntered_Metro_MSPselected(serviceType, ServiceIdentificationNumber, SelectSubService,
						Interfacespeed, EndpointCPE, Email, PhoneContact, remark, PerformanceMonitoring,
						ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology, intermediateTechnology,
						CircuitReference, CircuitType, modularmsp, perCocPerfrmReprt, actelsBased, standrdCir,
						standrdEir, prmiumCir, prmiumEir, notificationManagement, EVPNtechnology,
						HCoSPerformanceReporting, ENNIcheckBox, COScheckbox, MultiPostcheckbox);
			}

			sa.assertAll();

		} catch (AssertionError e) {
			Reporter.log("validation failed for verify service subtype page ");
			e.printStackTrace();
		}
	}

	public void EditTheservicesselected(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
					String modularmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
					String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
					String ServiceIdentificationNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_serviceNumber");
					String SelectSubService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
					String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_remark");
					String EndpointCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Edit_endpointCPE");
					String Email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_email");
					String PhoneContact = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_phone");
					String PerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"performanceReportngForServices");
					String ProactiveMonitoring = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_proactiveMonitor");
					String deliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_deliveryChannel");
					String ManagementOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_ManagementOrder");
					String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
					String intermediateTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_intermediateTechnology");
					String CircuitReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_CircuitReference");
					String circuitTypeUsedInServiceCreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_CircuitType");
					String notificationManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_NotificationManagement");
					String COScheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "COScheckbox");
					String Multiportcheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"MultiPostcheckbox");
					String ENNIcheckBox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_ENNI");
					String perCoSperformanceReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_perCoSperformanceReport");
					String actelisBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_actelisBased");
					String standardCIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_standardCIR");
					String standardEIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_standardEIR");
					String premiumCIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_premiumCIR");
					String premiumEIR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditService_premiumEIR");
					String EVPNtecnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_EVPNtechnology");
					String HCoSPerformanceReporting = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
							"EditService_HCoSPerformanceReporting");


		String orderPanel = (Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);
		scrollDown(orderPanel);
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_actiondropdown,"Action option");
		Reporter.log("Action dropdown is working");
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.Editservice_Editlink,"Edit link");
		waitforPagetobeenable();

		if (modularmsp.equalsIgnoreCase("no")) {

			if (Interfacespeed.equalsIgnoreCase("10GigE")) {
				Edit_DirectFibre10G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
						PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
						ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference,
						circuitTypeUsedInServiceCreation, notificationManagement);
			}

			else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
				Edit_DirectFibre1G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
						PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
						ManagementOrder, vpnTopology, intermediateTechnology, CircuitReference,
						circuitTypeUsedInServiceCreation, notificationManagement, perCoSperformanceReport, actelisBased,
						standardCIR, standardEIR, premiumCIR, premiumEIR);
			}
		} else {

			Edit_Metro_MSPselected(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE, Email,
					PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel, ManagementOrder,
					vpnTopology, intermediateTechnology, CircuitReference, circuitTypeUsedInServiceCreation,
					notificationManagement, perCoSperformanceReport, actelisBased, standardCIR, standardEIR, premiumCIR,
					premiumEIR, EVPNtecnology, HCoSPerformanceReporting);

		}
		waitforPagetobeenable();
	}

	public void verifyAddsiteorderFields(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String modularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
			String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String offnetSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Offnet");
		String EPNEOSDHselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_EPNEOSDH");
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Interfacespeed");

		if ((VPNtopology.equals("Point-to-Point")) & (circuitType.equals("Default"))) {

			verifySiteOrderForPoint_to_point(interfaceSpeed, modularMSP);

		} else if (VPNtopology.equals("Point-to-Point") && (circuitType.equals("Extended Circuit"))) {

			verifySiteOrderForPoint_to_point_extendedCircuit(interfaceSpeed, modularMSP);

		} else if (VPNtopology.equals("Hub&Spoke")) {

			if (offnetSelection.equalsIgnoreCase("No")) {

				verifySiteOrderForHubAndSpoke(interfaceSpeed, modularMSP);

			}

			if (offnetSelection.equalsIgnoreCase("Yes")) {
				if (modularMSP.equalsIgnoreCase("yes")) {

					verifySiteOrderForHubAndSpoke_MSPselectedAndoffnet(interfaceSpeed, modularMSP);

				} else {
					verifySiteOrderForHubAndSpoke_offnetSelected(interfaceSpeed, modularMSP);
				}

			}

		} else if (VPNtopology.equals("E-PN (Any-to-Any)")) {

			if (EPNEOSDHselection.equalsIgnoreCase("No")) {

				verifySiteOrderForE_PN(interfaceSpeed, modularMSP);

			} else if (EPNEOSDHselection.equalsIgnoreCase("yes")) {

				verifySiteOrderForEPN_EOSDHselected(interfaceSpeed, modularMSP);

			}
		}
	}

	public void VerifyDataEnteredForSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "country");
		String city = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "city");
		String CSR_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSR_Name");
		String site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitevalue");
		String performReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "performReport");
		String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Proactivemonitor");
		String smartmonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "smartmonitor");
		String technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String siteallias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteallias");
		String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
		String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DCAenabledsite");
		String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cloudserviceprovider");
		String sitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existing_SiteOrdervalue");
		String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteorder_Remark");
		String xngcityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng city name");
		String xngcitycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng ciy code");
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"devicenameForaddsiteorder");
		String nonterminatepoinr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"nonterminationpoint");
		String Protected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"protectforaddsiteorder");
		String newcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcity");
		String existingcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingcity");
		String existingsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingsite");
		String newsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newsite");
		String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Siteordernumber");
		String circuitref = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrder_CircuitReference");
		String offnetSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Offnet");
		String IVReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Ivrefrence");
		String GCRolo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrer_GCROloType");
		String Vlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
		String Vlanether = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Vlanethertype");
		String primaryVlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrder_PrimaryVlan");
		String primaryVlanether = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_PrimaryVlanEtherType");
		String EPNoffnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNoffnet");
		String EPNEOSDH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNEOSDH");
		String mappingmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_mappingMode");
		String portBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_portBased");
		String vlanBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
		String modularMSP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");

		if (VPNtopology.equals("Point-to-Point")) {

			if (modularMSP.equalsIgnoreCase("yes")) {

				VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_P2P(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
    	    			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
    	    			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection);

			} else {

				VerifyDataEnteredForSiteOrder_viewSiteOrder_P2P(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
    	    			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
    	    			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection);

			}

		}

		if (IVReference.equals("Primary")) {

			if (VPNtopology.equals("Hub&Spoke")) {

				if (modularMSP.equalsIgnoreCase("yes")) {
					VerifyDataEnteredForSiteOrder_viewSiteOrder__MSPselectedhubAndSpoke_Primay(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
    	        			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
    	        			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
    	        			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);

				} else {

					VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Primay(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
    	        			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
    	        			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
    	        			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);

				}

			} else if (VPNtopology.equals("E-PN (Any-to-Any)")) {

				if (modularMSP.equalsIgnoreCase("yes")) {
					VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_EPN_Primay(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);

				} else {

					VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Primay(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);
				}

			}

		}

		if (IVReference.equals("Access")) {

			if (VPNtopology.equals("Hub&Spoke")) {

				if (modularMSP.equalsIgnoreCase("yes")) {

					VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_hubAndSpoke_Access(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);

				} else {

					VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Access(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);
				}

			} else if (VPNtopology.equals("E-PN (Any-to-Any)")) {

				if (modularMSP.equalsIgnoreCase("yes")) {

					VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_EPN_Access(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);

				} else {

					VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Access(country, city, CSR_Name, site, performReport, ProactiveMonitor, smartmonitor,
                			technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename,
                			nonterminatepoinr, Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection, siteOrderNumber, 
                			circuitref, offnetSelection, IVReference, GCRolo, Vlan, Vlanether, primaryVlan, primaryVlanether, EPNoffnet, EPNEOSDH);
				}
			}
		}
	}

	public void editSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String performReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_performReport");
		String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditSiteOrder_ProactiveMonitor");
		String smartmonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_smartmonitor");
		String technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String siteallias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_siteallias");
		String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_VLANid");
		String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_DCAenabledsite");
		String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditSiteOrder_cloudserviceprovider");
		String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_remark");
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editsiteorder_devicename");
		String Protected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editsiteorder_protected");
				
		String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Siteordernumber");
		String IVreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Ivrefrence");
				
		String portBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_portBased");
		String vlanBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
		String siteOrderNumber_PointToPoint = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");
		String nontermination = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_nonterminationpoint");
		String modularMSp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
		String sitePreferenceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
		String Circuitreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editsiteorder_circuitReference");
		String editEPNoffnetvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_EPNOffnet");
		String editEpNEosDH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editEpNEosDH");
		String GCRoloType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editsiteorder_GCRoloType");
		String VLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_VLAN");
		String VlanEtherType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_VlanEtherType");
		String primaryVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_primaryVLAN");
		String primaryVlanEtherType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_primaryVlanEtherType");
		String mappingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");
		String maapingModeAddedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_nonterminationpoint");
		
		String editOffnetvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_Offnet");

			
		//click(Lanlink_Metro_Obj.Lanlink_Metro.EditsideOrderlink);
		webDriver.findElement(By.xpath("//tr//*[contains(.,'"+siteOrderNumber_PointToPoint+"')]//following-sibling::td//a[text()='Edit']")).click();
		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		// Point to Point
		if (VPNtopology.equals("Point-to-Point") && (circuitType.equals("Default"))) {

			if (modularMSp.equalsIgnoreCase("Yes")) {

				editSiteOrder_P2P_MSPselected(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
						DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
						remark);

			} else {

				if (interfaceSpeed.equals("1GigE")) {
					editSiteOrder_P2P_1G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
							DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
							remark);
				}

				else if (interfaceSpeed.equals("10GigE")) {
					editSiteOrder_P2P_10G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
							DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, remark);
				}
			}
		}

		else if (VPNtopology.equals("Point-to-Point") && (circuitType.equals("Extended Circuit"))) {

			if (modularMSp.equalsIgnoreCase("Yes")) {

				editSiteOrder_P2P_MSPselected(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
						DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
						remark);

			} else {

				if (interfaceSpeed.equals("1GigE")) {
					editSiteOrder_P2P_1G_extendedCircuit(performReport, ProactiveMonitor, smartmonitor, siteallias,
							VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected,
							devicename, remark, sitePreferenceType);
				}

				else if (interfaceSpeed.equals("10GigE")) {
					editSiteOrder_P2P_10G_extendedCircuit(performReport, ProactiveMonitor, smartmonitor, siteallias,
							VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, remark,
							sitePreferenceType);
				}
			}
		}

		// HUb & Spoke
		if (VPNtopology.equals("Hub&Spoke")) {

			if (modularMSp.equalsIgnoreCase("Yes")) {

				if (IVreference.equals("Primary")) {

					editSiteOrder_HubAndSpoke_MSPselected_IVRefPrimary(performReport, ProactiveMonitor, smartmonitor,
							siteallias, VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination,
							Protected, devicename, remark, siteOrderNumber, IVreference, Circuitreference,
							editOffnetvalue);

				}

				else if (IVreference.equals("Access")) {

					editSiteOrder_HubAndSpoke_MSPselected_IVRefAccess(performReport, ProactiveMonitor, smartmonitor,
							siteallias, VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination,
							Protected, devicename, remark, siteOrderNumber, IVreference, Circuitreference, GCRoloType,
							VLAN, VlanEtherType, primaryVLAN, primaryVlanEtherType, editOffnetvalue);

				}

			} else {

				if (interfaceSpeed.equals("1GigE")) {

					if (IVreference.equals("Primary")) {

						editSiteOrder_HubAndSpoke_1G_IVRefPrimary(performReport, ProactiveMonitor, smartmonitor,
								siteallias, VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination,
								Protected, devicename, remark, siteOrderNumber, IVreference, Circuitreference,
								editOffnetvalue);

					}

					else if (IVreference.equals("Access")) {

						editSiteOrder_HubAndSpoke_1G_IVRefAccess(performReport, ProactiveMonitor, smartmonitor,
								siteallias, VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination,
								Protected, devicename, remark, siteOrderNumber, IVreference, Circuitreference,
								GCRoloType, VLAN, VlanEtherType, primaryVLAN, primaryVlanEtherType, editOffnetvalue);

					}
				}

				else if (interfaceSpeed.equals("10GigE")) {

					editSiteOrder_HubAndSpoke_10G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
							DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
							remark, siteOrderNumber, IVreference, Circuitreference, editEPNoffnetvalue);
				}
			}

		}

		// E-PN (Any to Any)
		if (VPNtopology.equals("E-PN (Any-to-Any)")) {

			if (modularMSp.equalsIgnoreCase("Yes")) {

				if (IVreference.equals("Primary")) {

					editSiteOrder_EPN_MSPselected_IVRefPrimary(performReport, ProactiveMonitor, smartmonitor,
							siteallias, VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination,
							Protected, devicename, remark, siteOrderNumber, IVreference, Circuitreference,
							editEPNoffnetvalue, editEpNEosDH);

				}
				
				else if (IVreference.equals("Access")) {

					editSiteOrder_EPN_MSPselected_IVRefAccess(performReport, ProactiveMonitor, smartmonitor, siteallias,
							VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected,
							devicename, remark, siteOrderNumber, IVreference, Circuitreference, GCRoloType, VLAN,
							VlanEtherType, primaryVLAN, primaryVlanEtherType, editEPNoffnetvalue, mappingMode,
							portBased, vlanBased, maapingModeAddedValue);

				}

			} else {

				if (interfaceSpeed.equals("1GigE")) {

					if (IVreference.equals("Primary")) {

						editSiteOrder_EPN_1G_IVRefPrimary(performReport, ProactiveMonitor, smartmonitor, siteallias,
								VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected,
								devicename, remark, siteOrderNumber, IVreference, Circuitreference, editEPNoffnetvalue,
								editEpNEosDH);

					}

					else if (IVreference.equals("Access")) {

						editSiteOrder_EPN_1G_IVRefAccess(performReport, ProactiveMonitor, smartmonitor, siteallias,
								VLANid, DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected,
								devicename, remark, siteOrderNumber, IVreference, Circuitreference, GCRoloType, VLAN,
								VlanEtherType, primaryVLAN, primaryVlanEtherType, editEPNoffnetvalue, mappingMode,
								portBased, vlanBased, maapingModeAddedValue);

					}
				}

				else if (interfaceSpeed.equals("10GigE")) {

					editSiteOrder_HubAndSpoke_10G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
							DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
							remark, siteOrderNumber, IVreference, Circuitreference, editEPNoffnetvalue);
				}
			}

		}

		// waitforPagetobeenable();
		// click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void addAccedianCircuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service Name");

		// Click on Add Accedian-1G Link
		click(Lanlink_Metro_Obj.Lanlink_Metro.addCircuit_AccedianLink, "Add Accedian-1G");
		waitforPagetobeenable();
		waitToPageLoad();

		boolean overturePanelHeader = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.addCircuit_Accedian1Gpage_panel);
		if (overturePanelHeader) {
			Reporter.log("'Accedian-1G' page is displaying");

			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_serviceNameTextField, serviceName, "Service Name"); // Search
																													// ame
																													// text
																													// Field

			click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_searchButton, "Search"); // Search
																					// Button
			waitforPagetobeenable();
			waitToPageLoad();

			String selectValueInTable = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderAddOverturePage)
					.replace("value", serviceName);
			try {
				isElementPresent(selectValueInTable);
				Reporter.log("Records displays for the Service " + serviceName);

				click(selectValueInTable);
				waitforPagetobeenable();

				click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_OKbutton, "OK");
				waitforPagetobeenable();
				waitToPageLoad();

			} catch (Exception e) {
				e.printStackTrace();
				Reporter.log("No record displays for the Service " + serviceName);

				click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_cancelButton, "cancel");
			}

		} else {
			Reporter.log("'Overture' page is not displaying");
		}
	}

	public void addAtricaCircuit(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String serviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service Name");
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		// Click on Add Circuit Link
		click(Lanlink_Metro_Obj.Lanlink_Metro.addCircuit_AtricaLink, "Add Circuit");
		waitforPagetobeenable();
		waitToPageLoad();

		boolean overturePanelHeader = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.addCircuit_Atricapage_Panel);
		if (overturePanelHeader) {
			Reporter.log("'Accedian-1G' page is displaying");

			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_serviceNameTextField, serviceName, "Service Name"); // Search
																													// ame
																													// text
																													// Field

			click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_searchButton, "Search"); // Search
																					// Button
			waitforPagetobeenable();

			String selectValueInTable = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderAddOverturePage)
					.replace("value", serviceName);
			try {
				isElementPresent(selectValueInTable);
				Reporter.log("Records displays for the Service " + serviceName);

				click(selectValueInTable);
				waitforPagetobeenable();

				click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_OKbutton, "OK");
				waitforPagetobeenable();

			} catch (Exception e) {
				e.printStackTrace();
				Reporter.log("No record displays for the Service " + serviceName);

				click(Lanlink_Metro_Obj.Lanlink_Metro.addOverture_cancelButton, "cancel");
			}

		} else {
			Reporter.log("'Overture' page is not displaying");
		}
	}

	public void AddDSLAMandHSL(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String DSLMdevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DSLM device");
		String interfacenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DSLM device");

		waitToPageLoad();

		click(Lanlink_Metro_Obj.Lanlink_Metro.addDSLAMandHSL_xButton);
		Reporter.log("Clicked on 'X' button under 'DSLAM device' dropdown");

		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.DSLM_Device_Select, DSLMdevice, "DSLAM device");

		String valueToSElect = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.selectDSLAMdeviceValue).replace("value",
				DSLMdevice);

		try {
			if (isElementPresent(valueToSElect)) {
				Reporter.log(DSLMdevice + " is displaying under 'DSLAM device' dropdown");

				click(valueToSElect);
				Reporter.log(DSLMdevice + " is selected under 'DSLAM device' dropdown");

				waitToPageLoad();

				click(Lanlink_Metro_Obj.Lanlink_Metro.List_HSL_Link, "List_HSL"); // click
																				// on
																				// "List
																				// HSL"
																				// button

				selectRowForAddingInterface_Actelis(interfacenumber); // select
																		// the
																		// Interface

			} else {
				Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
			}

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
		}

	}

	public void verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(String testDataFile,
			String sheetName, String scriptNo, String dataSetNo, String speed)
			throws InterruptedException, IOException {
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
		String vpntopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpntopology");
		String interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interfacespeed");
		String existingDeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingDeviceName");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_adddevicelink, "Add Device");
		waitforPagetobeenable();
		waitToPageLoad();

		if ((technologySelected.equalsIgnoreCase("Atrica")) && (vpntopology.equals("Hub&Spoke"))
				&& (interfacespeed.equals("1GigE"))) {
			selectTechnology_HubAndSpoke();
		}
		waitToPageLoad();

		click(Lanlink_Metro_Obj.Lanlink_Metro.existingDevice_SelectDeviceToggleButton, "Select Device");
		waitforPagetobeenable();
		waitToPageLoad();

		click(Lanlink_Metro_Obj.Lanlink_Metro.chooseAdeviceDropdown, existingDeviceName);
		waitToPageLoad();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
	}

	public void verifyFieldsandAddCPEdevicefortheserviceselected_MSPselected(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_modularMSpselected");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_managementAddress");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_1G");
		String mediaSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Macaddress");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_hexaSerialnumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_linkLostForwarding");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_existingmanagementAddressSelection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_manageaddressdropdownvalue");
		String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mspselected_VLANid");
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_adddevicelink);
		waitforPagetobeenable();

		if (technologySelected.equalsIgnoreCase("Accedian")) {
			equip_adddevi_Accedian_MSPselected(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid,
					poweralarm, mediaSelection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
					newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue, VLANid);

		} else {

			equip_addDevice_MSPselected(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm,
					mediaSelection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
					newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue, VLANid);
		}

	}

	public void verifydetailsEnteredforCPEdevice_MSPselected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");
		String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_Mediaselection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_existingmanagementAddressSelection");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_newmanagementAddressSelection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_manageaddressdropdownvalue");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_managementAddress");
		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_modularMSpselected");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String MEPId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
		String VLANId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mspselected_VLANid");

		click(Lanlink_Metro_Obj.Lanlink_Metro.deviceView1 + cpename + Lanlink_Metro_Obj.Lanlink_Metro.deviceView2);
		waitforPagetobeenable();

		String[] RouterId = new String[2];
		RouterId = cpename.split(".lanlink");

		String RouterIdValue = RouterId[0];

		String mediaSelectionValueInViewDevicePage = "no";
		if (Mediaselection.equalsIgnoreCase("null")) {
			Mediaselection = mediaSelectionValueInViewDevicePage;
		} else {
			Mediaselection = mediaSelectionValueInViewDevicePage;
		}

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterIdValue + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vendor_Model + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Snmpro + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + VLANId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(
				Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + newmanagementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Management Address
		if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + manageaddressdropdownvalue
					+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
				&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + managementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}

		// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1+MEPId+Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

	}

	public void verifyFieldsandAddCPEdevicefortheserviceselected_1G(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_managementAddress");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_1G");
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Macaddress");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_hexaSerialnumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_linkLostForwarding");
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_1G");
		String mediaSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_manageaddressdropdownvalue");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_existingmanagementAddressSelection");
		

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_adddevicelink,"Add device");
		waitforPagetobeenable();

		if (technologySelected.equalsIgnoreCase("Accedian-1G")) {
			equip_adddevi_Accedian1G(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm,
					mediaSelection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
					newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue);

		} else {

			if (technologySelected.equalsIgnoreCase("Atrica") && vpnTopology.equals("Hub&Spoke")) {
				selectTechnology_HubAndSpoke();
			}

			equip_addDevice_1G(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm,
					mediaSelection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
					newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue);
		}

	}

	public void EDITCPEdevicedforIntermediateEquipment_1G(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_hexaSerialnumber");
		String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseName");
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseCode");
		String NewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteCode");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_country");
		String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_devic_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_managementAddress");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mepid");
		String poweralarm_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_1G_Overture");
		String ExistingSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSiteSelection");
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Macaddress");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_serialNumber");
		String ExistingPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremiseSelection");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_linkLostForwarding");
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");
		String vender_1G_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_1G_Overtue");
		String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityCode");
		String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mediaselection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"device_intequip_manageaddress_dropdownvalue");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"device_intequip_existingmanagementAddress_selection");
		String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
		String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityName");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
		String NewSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteSelection");
		String ExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
		String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
		String newPremiseselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseSelection");
		String NewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteName");
		String newCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcitySelection");
		String ExistingCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingcitySelection");
		String technologySelected_SiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
		String vender_1G_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_1G_Accedian");
		String poweralarm_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_1G_Accedian");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		if (technologySelected.equalsIgnoreCase("Overture")) {

			eDITCPEdevicedetailsentered_1G_Overture(Cpename, vender_1G_Overture, snmpro, managementAddress, Mepid,
					poweralarm_Overture, Mediaselection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
					Country, ExistingCitySelection, newCitySelection, existingCity, newCityName, newCityCode,
					ExistingSiteSelection, NewSiteSelection, ExistingSite, NewSiteName, NewSiteCode,
					ExistingPremiseSelection, newPremiseselection, existingPremise, newPremiseName, newPremiseCode,
					technologySelected_SiteOrder);
		} else if ((technologySelected.equalsIgnoreCase("Accedian"))
				|| (technologySelected.equalsIgnoreCase("Accedian-1G"))) {

			eDITCPEdevicedetailsentered_1G_Accedian(Cpename, vender_1G_Accedian, snmpro, managementAddress, Mepid,
					poweralarm_Accedian, Mediaselection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding,
					Country, ExistingCitySelection, newCitySelection, existingCity, newCityName, newCityCode,
					ExistingSiteSelection, NewSiteSelection, ExistingSite, NewSiteName, NewSiteCode,
					ExistingPremiseSelection, newPremiseselection, existingPremise, newPremiseName, newPremiseCode);
		}

	}

	public void EDITCPEdevicedforIntermediateEquipment_MSPselected(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");
		String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_MSPselected");
		String VLAnid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_VLANid_MSPselected");
		String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseName");
		String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseCode");
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
		String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
		String newPremiseselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseSelection");
		String ExistingPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_existingPremiseSelection");
		String NewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteCode");
		String NewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteName");
		String ExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
		String NewSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteSelection");
		String ExistingSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_ExistingSiteSelection");
		String ExistingCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_ExistingcitySelection");
		String newCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcitySelection");
		String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
		String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityName");
		String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityCode");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_devic_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_managementAddress");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mepid");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_country");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		if ((technologySelected.equalsIgnoreCase("Accedian"))) {

			eDITCPEdevicedetailsentered_MSPselected_Accedian(Cpename, vender_Accedian, snmpro, managementAddress, Mepid,
					Country, ExistingCitySelection, newCitySelection, existingCity, newCityName, newCityCode,
					ExistingSiteSelection, NewSiteSelection, ExistingSite, NewSiteName, NewSiteCode,
					ExistingPremiseSelection, newPremiseselection, existingPremise, newPremiseName, newPremiseCode,
					VLAnid);
		}

	}

	public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_MSPselected(String testDataFile,
			String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intEquip_name");
		String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intEquip_vender_MSPSelected_Accedian");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_managementAddress_textfield");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_Mepid");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_country");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingcity_dropodwnvalue");
		String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_site_dropdownvalue");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_premisedropdownvalue");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newmanagementAddress_selection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingmanagementAddress_selection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_manageaddress_dropdownvalue");
		String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingcityselectionmode");
		String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newcityselectionmode");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_cityname");
		String citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_citycode");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingsiteselectionmode");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newsiteselectionmode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_sitename");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_sitecode");
		String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingpremiseselectionmode");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newpremiseselectionmode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "deivce_intequip_premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_premisecode");
		String technologySelectedfordevicecreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "TechToBeselected_underTechpopup_device");
		String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_VLANid");


		if ((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian"))) {
			deviceCreatoin_Accedian_MSPselected(cpename, vender_Accedian, snmpro, managementAddress, Mepid, country, City, Site, Premise, newmanagementAddress, 
					existingmanagementAddress, manageaddressdropdownvalue, existingcityselectionmode, newcityselectionmode, cityname, 
					citycode, existingsiteselectionmode, newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, 
					newpremiseselectionmode, premisename, premisecode, VLANid);
		}

	}

	public void verifyCPEdevicedataenteredForIntermediateEquipment_MSPselected(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intEquip_name");
		String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intEquip_vender_MSPSelected_Accedian");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_managementAddress_textfield");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_Mepid");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_country");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingcity_dropodwnvalue");
		String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_site_dropdownvalue");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_premisedropdownvalue");
		String newmanagementAddressSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newmanagementAddress_selection");
		String existingmanagementAddressSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingmanagementAddress_selection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_manageaddress_dropdownvalue");
		String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingcityselectionmode");
		String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newcityselectionmode");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_cityname");
		String citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_citycode");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingsiteselectionmode");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newsiteselectionmode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_sitename");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_sitecode");
		String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_existingpremiseselectionmode");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_newpremiseselectionmode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "deivce_intequip_premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_premisecode");
		String technologySelectedfordevicecreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "TechToBeselected_underTechpopup_device");
		String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,dataSetNo, "device_intequip_VLANid");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		clickOnBankPage();
		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + cpename + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		waitforPagetobeenable();

		if ((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian"))) {

			viewdevice_Accedian_MSPselected(cpename, vender_Accedian, snmpro, managementAddress, Mepid,country, City, cityname, Site, sitename, Premise, premisename,existingcityselectionmode,
					newcityselectionmode, existingsiteselectionmode, newsiteselectionmode, newmanagementAddressSelection, existingmanagementAddressSelection,
					manageaddressdropdownvalue, existingpremiseselectionmode, newpremiseselectionmode, VLANid);
		}

	}

	public String EnterdataForEditInterfaceforConfigurelinkunderIntermediateEquipment(String testDataFile,
			String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerType");
		String bearerspeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerSpeed");
		String totalbandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editInterfacepage_bandwidth");
		String vlantype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_vlantype");
		String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_Vlanid");
		String circuitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_circuitId");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

		
		
		String interfaceavailability = selectRowForconfiglinkunderIntermediateEquipment(interfacename);
		
		if(interfaceavailability.equalsIgnoreCase("Yes")) {	

		// Perform edit Interface
		click(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureActiondropdown);
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureEditlink);
		waitforPagetobeenable();

		// verify Edit Interface page
		boolean editInterface_popuptitleName = false;
		try {
			editInterface_popuptitleName = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Editinterface_popupTitlename);
			if (editInterface_popuptitleName) {
				Reporter.log("Edit interface popup is displaying as expected");

				// Interface name
				verifyenteredvaluesForEditPage_noneditableFields("Interface", interfacename);

				// Circuit ID
				configure_circuitId(circuitId);

				// Bearer type
				addDropdownValues_commonMethod("Bearer Type",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureBearerType, bearertype_value);

				// Bearerspeed
				addDropdownValues_commonMethod("Bearer Speed",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureBearerSpeed, bearerspeed);

				// Total Circuit bandwidth
				addDropdownValues_commonMethod("Total Circuit Bandwidth",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureTotalcircuitbandwidth, totalbandwidth);

				// VLAN type
				addDropdownValues_commonMethod("VLAN Type",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureVlanType, vlantype);

				// vlan
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureVLANid, vlanid, "VLAN Id");
				waitforPagetobeenable();

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Edit interface' popup is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			Reporter.log(" 'Edit interface' popup is not displaying");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();
		}
		return interfaceavailability;
		

	}

	public void EnterdataForEditInterfaceforShowInterfacelinkunderEquipment(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerType");
		String bearerspeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_BearerSpeed");
		String totalbandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"editInterfacepage_bandwidth");
		String vlantype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_vlantype");
		String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_Vlanid");
		String circuitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editInterfacepage_circuitId");
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacename_forEditInterface");

		
		// verify Edit Interface page
		boolean editInterface_popuptitleName = false;
		try {
			editInterface_popuptitleName = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Editinterface_popupTitlename);
			if (editInterface_popuptitleName) {
				Reporter.log("Edit interface popup is displaying as expected");

				
				// Interface name
				verifyenteredvaluesForEditPage_noneditableFields("Interface", interfacename);

				// Circuit ID
				configure_circuitId(circuitId);

				// Bearer type
				addDropdownValues_commonMethod("Bearer Type",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureBearerType, bearertype_value);

				// Bearerspeed
				addDropdownValues_commonMethod("Bearer Speed",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureBearerSpeed, bearerspeed);

				// Total Circuit bandwidth
				addDropdownValues_commonMethod("Total Circuit Bandwidth",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureTotalcircuitbandwidth, totalbandwidth);

				// VLAN type
				addDropdownValues_commonMethod("VLAN Type",Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureVlanType, vlantype);

				// vlan
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureVLANid, vlanid, "VLAN Id");
				waitforPagetobeenable();

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Edit interface' popup is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			Reporter.log(" 'Edit interface' popup is not displaying");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void routerPanel(String commandIPv4,String ipAddress)
			throws InterruptedException, IOException {
		

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		String vendorModel = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.fetchVendorModelvalue);
		String vendorValue = vendorModel;

		if (vendorValue.startsWith("Overture")) {

			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.commandIPV4_dropdown);
			waitforPagetobeenable();

			// Command IPV4
			addDropdownValues_commonMethod("Command IPV4",Lanlink_Metro_Obj.Lanlink_Metro.commandIPV4_dropdown, commandIPv4 );

			hostnametextField_IPV4(commandIPv4, ipAddress);

			executeCommandAndFetchTheValue(Lanlink_Metro_Obj.Lanlink_Metro.executebutton_Ipv4);
		} else {
			// 'Router' panel displays only for Overture device
		}
	}

	public void eDITCPEdevicedetailsentered_10G(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpename");
		;
		String vender_10G_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_vender_10G");
		;
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_snmpro");
		;
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_managementAddress");
		;
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mepid");
		;
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_poweralarm_10G");
		;
		String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mediaselection");
		;
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Macaddress");
		;

		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_serialNumber");
		;
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_hexaSerialnumber");
		;
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_linkLostForwarding");
		;
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicenameforEquipment");
		;

		Reporter.log("Entered edit functionalitty");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevicelinkunderviewpage,"Edit");
		Reporter.log("edit functionality worked");

		waitToPageLoad();

		// Name field
		device_editnamefield(Cpename);

		// vendor/model
		device_editVendorModelField(vender_10G_Accedian);

		// Snmpro
		device_editSnmproField();

		// Management address
		device_editManagementAddressField(managementAddress);

		// Mepid
		device_editMEPIdField(Mepid);

		// power alarm
		device_editPowerAlarm(poweralarm);

		// serial Number
		device_editserialnumber(serialNumber);

		// linklost forwarding
		device_editlinklostforwarding_10G(linkLostForwarding);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void eDITCPEdevicedetailsentered_MSPselected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpename");
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_vender_MSPselected");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String managementAddres = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_managementAddress");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mepid");
		String VLANId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_vender_MSPselected_VLANid");

		Reporter.log("Entered edit functionalitty");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevicelinkunderviewpage);
		Reporter.log("edit functionality worked");

		waitToPageLoad();

		// Name field
		device_editnamefield(cpename);

		// vendor/model
		device_editVendorModelField(vender);

		// Snmpro
		device_editSnmproField();

		// Management address
		device_editManagementAddressField_MSPselected(managementAddres);

		// Mepid
		device_editMEPIdField(Mepid);

		// VLAN Id
		device_editVLANIdField(VLANId);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void verifyFieldsandAddCPEdevicefortheserviceselected_10G(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_10G");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_existingmanagementAddressSelection");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_newmanagementAddressSelection");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_managementAddress");

		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_10G");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_linkLostForwarding");
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_adddevicelink,"Add device");
		

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
		waitforPagetobeenable();

		try {

			String linklostForwardingcheckboxstate = "disabled";

			String[] Vender = { "Accedian 10GigE-MetroNode-CE-2Port" };

			String[] powerAlarm = { "DC Single Power Supply - PSU A", "DC Dual Power Supply - PSU-A+B" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>-10G.lanlink.dcn.colt.net";

			String MEPid = "5555";

			String expectedValueForSnmpro = "JdhquA5";

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Power Alarm Error Message
			device_powerAlarmWarningMessage();

			// serial number Eror Message
			device_serialNumberWarningMessage();

			// Hexa serial Number
			device_hexaSerialNumberWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address dropdown
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// Power Alarm
			device_powerAlarm(poweralarm);

			// Serial Number
			device_serialNumber(serialNumber);

			// Link lost Forwarding
			device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			// OK button
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			// cancel button
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_cancelbutton, "Cancel");

			//click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			// CAncel button

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

			waitforPagetobeenable();

			

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			//

		}

	}

	public void Verifyfields(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String neworder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingOrderSelection");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingOrderNumber");
		String InterfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String ServiceTypeToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceType");
		String ServiceSubtype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Servicesubtype");
		
		waitToPageLoad();

		
	
		
		clickOnBankPage();
		
		waitforPagetobeenable();
		//scrolltoend();
		
		click(Lanlink_Metro_Obj.Lanlink_Metro.Next_Button, "Next");
		Reporter.log("clicked on next button to verify the mandatory fields error messages");
		
		
		//Create Order/Contract Number Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.order_contractnumber_warngmsg, "Order/Contract Number");
		
		//Service Type Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.servicetype_warngmsg, "Service Type");
					
			
				String[] Servicetypelists = { "BM (Broadcast Media)", "Domain Management", "DSL", "FAX to Mail", "HSS",
						"IP Access (On-net/Offnet/EoS)", "IP Access Bundle", "IP Transit", "IP VPN", "IP Web/Mail", "LANLink",
						"MDF/MVF/DI", "NGIN", "Number Hosting", "Transmission Link", "Voice Line (V)", "VOIP Access",
						"Wholesale SIP Trunking" };
		
				Reporter.log("order dropdown");
				
			//check whether Order dropdown is displayed	
				orderdopdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.orderdropdown);
				sa.assertTrue(orderdopdown, "Order/Contract Number dropdown is not displayed");
				Reporter.log("order dropdown field is verified");
				
				
			//Select value under 'Service Type' dropdown
				addDropdownValues_commonMethod("Service Type", Lanlink_Metro_Obj.Lanlink_Metro.servicetypedropdowntoclick, ServiceTypeToBeSelected);
				
				
			//	scrolltoend();
				waitforPagetobeenable();
				
			//Click on next button to check mandatory messages
				click(Lanlink_Metro_Obj.Lanlink_Metro.Next_Button, "Next");
				waitforPagetobeenable();
				Report.LogInfo("Info","clicked on next button to verify the mandatory fields error messages","PASS");
				Reporter.log("clicked on next button to verify the mandatory fields error messages");
		
				
			//Interface Speed Error message	
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.interfaceSpeedErrmsg, "Interface Speed");
				
				
			//Service Sub Type Error message
						verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.servicesubtypeErrMsg, "Service Subtype");
		
				
			//Modular msp checkbox	
				modularmspCheckbox = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.modularmspcheckbox);
		
			//AutoCreate checkbox	
				autocreateservicecheckbox = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AutocreateServicecheckbox);

		for (int i = 0; i < 4; i++) {

			if (i == 0) {

				verifyinterfaceSpeeddropdown();

				verifyservicesubtypesdropdownwhenMSPandAutoCreatenotslected();

				verifyavailablecircuitdropdown();

			}

			else if (i == 1) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.modularmspcheckbox, "Modular MSP checkbox");

				verifyservicesubtypesdropdownwhenMSPaloneselected();

				verifyavailablecircuitdropdown();

			}

			else if (i == 2) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.modularmspcheckbox, "Modular MSP checkbox");
				waitforPagetobeenable();

				click(Lanlink_Metro_Obj.Lanlink_Metro.AutocreateServicecheckbox, "AutoCreate Service");
				waitforPagetobeenable();

				verifyA_Endtechnologydropdown();

				verifyB_Endtechnologydropdowb();

				verifyinterfaceSpeeddropdown();

				verifyservicesubtypesdropdownwgenAutoCreatealoneselected();

				verifyavailablecircuitdropdown();

			} else if (i == 3) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.modularmspcheckbox, "Modular MSP checkbox");
				waitforPagetobeenable();

				verifyA_Endtechnologydropdown();

				verifyB_Endtechnologydropdowb();

				verifyservicesubtypesdropdownwhenMSPandAutoCreateselected();

				verifyavailablecircuitdropdown();
			}
		}

	}

	public void verifysuccessmessage(String expected) throws InterruptedException, IOException {

		waitToPageLoad();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		try {

			boolean successMsg = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.serivceAlert);

			if (successMsg) {

				String alrtmsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AlertForServiceCreationSuccessMessage);

				if (expected.contains(alrtmsg)) {

					Reporter.log("Message is verified. It is displaying as: " + alrtmsg);
					// successScreenshot();

				} else {

					Reporter.log("Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg);
					// failureScreenshot();
				}

			} else {
				Reporter.log(" Success Message is not displaying");
				// failureScreenshot();
			}

			waitforPagetobeenable();

		} catch (Exception e) {
			Reporter.log("failure in fetching success message - 'Service created Successfully'  ");
			Reporter.log(expected + " message is not getting dislpayed");
			// failureScreenshot();
		}
	}

	public void eDITCPEdevicedetailsentered_1G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_managementAddress");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mepid");
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_poweralarm_1G");
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Macaddress");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_serialNumber");
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_hexaSerialnumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_linkLostForwarding");
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpename");
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_vender_1G");
		String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mediaselection");
		String MediaselectionActualValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_manageaddressdropdownvalue");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_existingmanagementAddressSelection");

		Reporter.log("Entered edit functionalitty");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevicelinkunderviewpage,"Edit");
		Reporter.log("edit functionality worked");

		waitToPageLoad();

		// Name field
		device_editnamefield(cpename);

		// vendor/model
		device_editVendorModelField(vender);

		// Snmpro
		device_editSnmproField();

		// Management address
		device_editManagementAddressField(managementAddress);

		// Mepid
		device_editMEPIdField(Mepid);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// power alarm
		device_editPowerAlarm(poweralarm);

		if (technologySelected.equalsIgnoreCase("Accedian-1G")) {

			// Serial Number
			device_editserialnumber(serialNumber);

		} else {

			// Media Selection
			device_editMediaselection(Mediaselection);

			waitforPagetobeenable();

			// Mac address
			device_editMACaddress(Macaddress);

			// linklost forwarding
			device_editlinkLostforwarding(linkLostForwarding);
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void verifydetailsEnteredforCPEdevice_1G(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String ManagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_managementAddress");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
		String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_1G");
		String MACAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Macaddress");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_hexaSerialnumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_linkLostForwarding");
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");
		String vender_model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_1G");
		String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
		String MediaselectionActualValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_manageaddressdropdownvalue");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_existingmanagementAddressSelection");
		
		
		Report.LogInfo("Info", "verify the details entered for creating device","Info");
		
		clickOnBankPage();
		waitforPagetobeenable();
		
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		
		webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();
		waitforPagetobeenable();
	
		String[] RouterId=new String[2];
		RouterId=cpename.split(".lanlink");
		
		String RouterIdValue=RouterId[0];
		
		
		String mediaSelectionValueInViewDevicePage="no";
		if(Mediaselection.equalsIgnoreCase("null")) {
			Mediaselection=mediaSelectionValueInViewDevicePage;
		}else {
			Mediaselection=mediaSelectionValueInViewDevicePage;
		}
	  
	// verifyEnteredvalues_deviceName("Router Id",RouterIdValue,cpename );
	  
	  verifyEnteredvalues("Router Id", RouterIdValue);
	  
	  verifyEnteredvalues("Vendor/Model", vender_model);
	  
	  verifyEnteredvalues("Snmpro", Snmpro);
	  
	  
		//Management Address  
		  	if((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
			  verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
			 }
			 else if((existingmanagementAddress.equalsIgnoreCase("no")) && (newmanagementAddress.equalsIgnoreCase("Yes"))) {
				 verifyEnteredvalues("Management Address", ManagementAddress);
			 } 
	  
//	  verifyEnteredvalues("MEP Id", Mepid);
	  
	  verifyEnteredvalues("Power Alarm", PowerAlarm);
	  
	if(technologySelected.equalsIgnoreCase("Accedian-1G")) {  
		
	  verifyEnteredvalues("Serial Number", serialNumber);
	  
	}else {
	  
	  verifyEnteredvalues("Media Selection", Mediaselection);
	  
	  verifyEnteredvalues("MAC Address", MACAddress);
	  
	}
}

	public void verifydetailsEnteredforCPEdevice_10G(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");

		String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String Vendor_Model = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_10G");
		String Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_managementAddress");
		// String MEPId=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "cpe_Mepid");
		String PowerAlarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_10G");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_Mediaselection");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
		String MACAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Macaddress");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_manageaddressdropdownvalue");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_linkLostForwarding");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_existingmanagementAddressSelection");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cpe_newmanagementAddressSelection");

		clickOnBankPage();
		waitforPagetobeenable();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();
		

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Splitting device name
		String[] RouterId = new String[2];
		RouterId = cpename.split(".lanlink");

		String RouterIdValue = RouterId[0];

		String Mediaselection = "no";
		String LinkLostForwarding = "yes";

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + RouterId + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vendor_Model + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Snmpro + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Management Address
		if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + manageaddressdropdownvalue
					+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
				&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + managementAddress + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + PowerAlarm + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + MediaselectionActualValue
				+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		verifyExists(
				Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + linkLostForwarding + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + serialNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

	}

	public void EDITCPEdevice_IntermediateEquipment_10G(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String newCityName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityName");
		String newCityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityCode");
		String NewSiteName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteName");
		String NewSiteCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteCode");
		String newPremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_newPremiseName");
		String newPremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_newPremiseCode");
		String vender_10G_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_10g_Accedian");
		String Cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");
		String Mediaselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_Mediaselection");
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_10g_Accedian");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_devic_snmpro");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingmanagementAddress");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newmanagementAddress");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"manageaddressdropdownvalue");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_managementAddress");
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_10G_Accedian");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_serialNumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_linkLostForwarding");
		String existingcountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Country");
		String existingCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
		String newCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "City");
		String existingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
		String newSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site");
		String existingPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
		String newPremise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premise");
		String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingcityselectionmode");
		String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newcityselectionmode");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_ExistingSiteSelection");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_newSiteSelection");
		String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_existingPremiseSelection");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_newPremiseSelection");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mepid");
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Macaddress");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_country");
		String ExistingCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_ExistingcitySelection");
		String ExistingSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingSiteSelection");
		String NewSiteSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"NewSiteSelection");
		String ExistingPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPremiseSelection");
		String newPremiseselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newPremiseselection");

		String NewCitySelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EDIT_Intequip_device_newcitySelection");
		String ExistingSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewCitySelection");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		Reporter.log("Entered edit functionalitty");

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevicelinkunderviewpage,"Edit");
		waitforPagetobeenable();
		Reporter.log("edit functionality worked");

		// Name field
		device_editnamefield(Cpename);

		// vendor/model
		device_editVendorModelField(vender_10G_Accedian);

		// Snmpro
		device_editSnmproField();

		// Management address
		device_editManagementAddressField(managementAddress);

		// Mepid
		device_editMEPIdField(Mepid);

		// power alarm
		device_editPowerAlarm(poweralarm);

		// Serial Number
		device_editserialnumber(serialNumber);

		// linklost forwarding
		device_editlinkLostforwarding(linkLostForwarding);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		// Country
		if (!Country.equalsIgnoreCase("Null")) {

			click(Lanlink_Metro_Obj.Lanlink_Metro.countryDropdown_selectTag, Country);

			// New City
			if (ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
				click(Lanlink_Metro_Obj.Lanlink_Metro.addcityswitch);
				// City name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citynameinputfield, newCityName, "City Name");
				// City Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citycodeinputfield, newCityCode, "City Code");
				// Site name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addCityToggleSelected, NewSiteName,
						"Site Name");
				// Site Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addCityToggleSelected, NewSiteCode,
						"Site Code");
				// Premise name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addCityToggleSelected, newPremiseName,
						"Premise Name");
				// Premise Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addCityToggleSelected, newPremiseCode,
						"Premise Code");

			}

			// Existing City
			else if (ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.cityDropdown_selectTag, existingCity);

				// Existing Site
				if (ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
					click(Lanlink_Metro_Obj.Lanlink_Metro.siteDropdown_selectTag, existingSite);

					// Existing Premise
					if (ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
						click(Lanlink_Metro_Obj.Lanlink_Metro.premiseDropdown_selectTag, existingPremise);

					}

					// New Premise
					else if (ExistingPremiseSelection.equalsIgnoreCase("no")
							& newPremiseselection.equalsIgnoreCase("yes")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.addpremiseswitch);
						// Premise name
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addPremiseToggleSelected,
								newPremiseName, "Premise Name");
						// Premise Code
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addPremiseToggleSelected,
								newPremiseCode, "Premise Code");
					}
				}

				else if (ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.addsiteswitch);
					// Site name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addSiteToggleSelected, NewSiteName,
							"Site Name");
					// Site Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addSiteToggleSelected, NewSiteCode,
							"Site Code");

					// Premise name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addSiteToggleSelected, newPremiseName,
							"Premise Name");
					// Premise Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addSiteToggleSelected, newPremiseCode,
							"Premise Code");
				}
			}

		} else if (Country.equalsIgnoreCase("Null")) {

			// City
			editCity(ExistingCitySelection, NewCitySelection, "cityDropdown_selectTag", "selectcityswitch",
					"addcityswitch", existingCity, newCityName, newCityCode, "City");

			// Site
			editSite(ExistingSiteSelection, NewSiteSelection, "siteDropdown_selectTag", "selectsiteswitch",
					"addsiteswitch", ExistingSite, NewSiteName, NewSiteCode, "Site");

			// Premise
			editPremise(ExistingPremiseSelection, newPremiseselection, "premiseDropdown_selectTag",
					"selectpremiseswitch", "addpremiseswitch", existingPremise, newPremiseName, newPremiseCode,
					"Premise");

		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
		String vender = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_10G_Accedian");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
		String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
		String poweralarm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_10G_Accedian");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
		String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
		String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
		String citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
		String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
		

		
		

		try {

			String linklostForwardingcheckboxstate = "disabled";

			String[] Vender = { "Accedian 10GigE-MetroNode-CE-2Port", "Accedian 10GigE-MetroNode-CE-4Port" };

			String[] powerAlarm = { "DC Single Power Supply - PSU A", "DC Dual Power Supply - PSU-A+B" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>-10G.lanlink.dcn.colt.net";

			String MEPid = "5555";

			String expectedValueForSnmpro = "JdhquA5";

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.name);
			waitforPagetobeenable();

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Power Alarm Error Message
			device_powerAlarmWarningMessage();

			// serial number Eror Message
			device_serialNumberWarningMessage();

			// Hexa serial Number
			device_hexaSerialNumberWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			
			// Management Address dropdown
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// Power Alarm
			device_powerAlarm(poweralarm);

			// Serial Number
			device_serialNumber(serialNumber);

			// Link lost Forwarding
			device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

			// Country
			device_country(country);

			// City
			if (existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
				addCityToggleButton();
				// New City
				newcity(newcityselectionmode, cityname, citycode);
				// New Site
				newSite(newsiteselectionmode, sitename, sitecode);
				// New Premise
				newPremise(newpremiseselectionmode, premisename, premisecode);

			}
			
			else if (existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
				// Existing City
				existingCity(City);

				// Site
				
				if (existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
					// Existing Site
					existingSite(Site);

					// Premise
					if (existingpremiseselectionmode.equalsIgnoreCase("yes")
							& newpremiseselectionmode.equalsIgnoreCase("no")) {
						existingPremise(Premise);

					} else if (existingpremiseselectionmode.equalsIgnoreCase("no")
							& newpremiseselectionmode.equalsIgnoreCase("yes")) {
						// New Premise
						addPremiseTogglebutton();
						newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
					}
				}

				else if (existingsiteselectionmode.equalsIgnoreCase("no")
						& newsiteselectionmode.equalsIgnoreCase("yes")) {
					// New Site
					addSiteToggleButton();
					newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode);

					// New Premise
					newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
				}
			}

			// OK button

			// OK button
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			// cancel button
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_cancelbutton, "Cancel");

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

			waitforPagetobeenable();

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage1_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage2_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage3_devicename, "Device Name");

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();

		}

	}

	public void addCityToggleButton() throws InterruptedException, IOException {

		// Add city Toggle button
		click(Lanlink_Metro_Obj.Lanlink_Metro.addsiteswitch);
		waitforPagetobeenable();
	}

	public void addPremiseTogglebutton() throws InterruptedException, IOException {

		// Add Premise Toggle button
		click(Lanlink_Metro_Obj.Lanlink_Metro.addpremiseswitch);
		waitforPagetobeenable();
	}

	public void addSiteToggleButton() throws InterruptedException, IOException {
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// Add Site Toggle button
		click(Lanlink_Metro_Obj.Lanlink_Metro.addsiteswitch);
		waitforPagetobeenable();
	}

	public void click_addToggleButton(String xpath) throws InterruptedException, IOException {

		// Add Toggle button
		click(xpath);
		waitforPagetobeenable();
	}

	public void viewdevice_Overture(String cpename, String vender,
			String snmpro, String managementAddress, String Mepid, String poweralarm, String Mediaselection,
			String Macaddress, String linkLostForwarding, String existingcountry, String existingCity,
			String newCity, String existingSite, String newSite, String existingPremise, String newPremise, 
			String existingcityselectionmode, String newcityselectionmode, String existingsiteselectionmode, 
			String newsiteselectionmode, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
			String existingpremiseselectionmode, String newpremiseselectionmode)
			throws InterruptedException, IOException {

		
		String[] RouterId = new String[2];
		RouterId = cpename.split(".lanlink");

		String RouterIdValue = RouterId[0];

		String mediaSelectionValueInViewDevicePage = "no";
		if (Mediaselection.equalsIgnoreCase("null")) {
			Mediaselection = mediaSelectionValueInViewDevicePage;
		} else {
			Mediaselection = mediaSelectionValueInViewDevicePage;
		}

		//  verifyEnteredvalues_deviceName("Name", RouterIdValue,cpename );
		  
		  verifyEnteredvalues("Router Id", RouterIdValue);
		  
		  verifyEnteredvalues("Vendor/Model", vender);
		  
		  verifyEnteredvalues("Snmpro", snmpro);
		// Management Address
		if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
			 verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
		} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
				&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
			 verifyEnteredvalues("Management Address", managementAddress);

		}

//		  verifyEnteredvalues("MEP Id", Mepid);
		  
		  verifyEnteredvalues("Power Alarm", poweralarm);
		  
		  verifyEnteredvalues("Media Selection", Mediaselection);
		  
		  verifyEnteredvalues("MAC Address", Macaddress);
		  
		  verifyEnteredvalues("Link Lost Forwarding", linkLostForwarding);
		  
		  verifyEnteredvalues("Country", existingcountry);

		//City  
			 if((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {
				 verifyEnteredvalues("City", existingCity);
			 }
			 else if((existingcityselectionmode.equalsIgnoreCase("no")) && (newcityselectionmode.equalsIgnoreCase("Yes"))) {
				 verifyEnteredvalues("City", newCity);
			 } 
			 
			 
			//Site
			 if((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {
				 verifyEnteredvalues("Site", existingSite);
			 }
			 else if((existingsiteselectionmode.equalsIgnoreCase("no")) && (newsiteselectionmode.equalsIgnoreCase("Yes"))) {
				 verifyEnteredvalues("Site", newSite);
			 } 
			 
			 
			//Premise
			 if((existingpremiseselectionmode.equalsIgnoreCase("Yes")) && (newpremiseselectionmode.equalsIgnoreCase("no"))) {
				 verifyEnteredvalues("Premise", existingPremise);
			 }
			 else if((existingpremiseselectionmode.equalsIgnoreCase("no")) && (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
				 verifyEnteredvalues("Premise", newPremise);
			 } 
			  
			  
			  

			}

	public void viewdevice_Accedian_MSPselected(String cpename, String vender,
			String snmpro, String managementAddress, String Mepid, String existingcountry, String existingCity,
			String newCity, String existingSite, String newSite, String existingPremise, String newPremise, 
			String existingcityselectionmode, String newcityselectionmode, String existingsiteselectionmode, 
			String newsiteselectionmode, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
			String existingpremiseselectionmode, String newpremiseselectionmode, String VLANid) throws InterruptedException, IOException {

		
		String[] RouterId = new String[2];
		RouterId = cpename.split(".lanlink");

		String RouterIdValue = RouterId[0];

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + cpename + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + RouterIdValue + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + vender + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + snmpro + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + VLANid + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		// Management Address
		if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + manageaddressdropdownvalue
					+ Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
				&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + managementAddress
					+ Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		}

		// City
		if ((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {

			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + existingCity + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingcityselectionmode.equalsIgnoreCase("no"))
				&& (newcityselectionmode.equalsIgnoreCase("Yes"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + newCity + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		}

		// Site
		if ((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {

			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + existingSite + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingsiteselectionmode.equalsIgnoreCase("no"))
				&& (newsiteselectionmode.equalsIgnoreCase("Yes"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + newSite + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		}

		// Premise
		if ((existingpremiseselectionmode.equalsIgnoreCase("Yes"))
				&& (newpremiseselectionmode.equalsIgnoreCase("no"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + existingPremise
					+ Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingpremiseselectionmode.equalsIgnoreCase("no"))
				&& (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + newPremise + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		}
	}

	public void viewdevice_Accedian(String cpename, String vender,
			String snmpro, String managementAddress, String Mepid, String poweralarm, String Mediaselection,
			String Macaddress, String serialNumber, String hexaSerialnumber, String linkLostForwarding, String existingcountry, String existingCity,
			String newCity, String existingSite, String newSite, String existingPremise, String newPremise, 
			String existingcityselectionmode, String newcityselectionmode, String existingsiteselectionmode, 
			String newsiteselectionmode, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
			String existingpremiseselectionmode, String newpremiseselectionmode)
			throws InterruptedException, IOException {

		
		String[] RouterId = new String[2];
		RouterId = cpename.split(".lanlink");

		String RouterIdValue = RouterId[0];

		String mediaSelectionValueInViewDevicePage = "no";
		if (Mediaselection.equalsIgnoreCase("null")) {
			Mediaselection = mediaSelectionValueInViewDevicePage;
		} else {
			Mediaselection = mediaSelectionValueInViewDevicePage;
		}
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + cpename + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + RouterIdValue + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + vender + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + snmpro + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		// Management Address
		if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + manageaddressdropdownvalue
					+ Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
				&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + managementAddress
					+ Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		}

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + poweralarm + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(
				Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + Mediaselection + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1+Macaddress+Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + serialNumber + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1+linkLostForwarding+Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		// City
		if ((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {

			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + existingCity + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingcityselectionmode.equalsIgnoreCase("no"))
				&& (newcityselectionmode.equalsIgnoreCase("Yes"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + newCity + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		}

		// Site
		if ((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {

			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + existingSite + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingsiteselectionmode.equalsIgnoreCase("no"))
				&& (newsiteselectionmode.equalsIgnoreCase("Yes"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + newSite + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		}

		// Premise
		if ((existingpremiseselectionmode.equalsIgnoreCase("Yes"))
				&& (newpremiseselectionmode.equalsIgnoreCase("no"))) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + existingPremise
					+ Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);

		} else if ((existingpremiseselectionmode.equalsIgnoreCase("no"))
				&& (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.deviceview1 + newPremise + Lanlink_Metro_Obj.Lanlink_Metro.deviceview2);
		}
	}

	public void newSite_clickSiteTogglebutton(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitecode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitename");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitename");

		// New site
		if (newsiteselectionmode.equalsIgnoreCase("yes")) {

			if (sitename.equalsIgnoreCase("null")) {
			} else {
				// Site Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton,
						sitename, "Site name");
				waitforPagetobeenable();
				String sitenme = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton, "value");
				waitforPagetobeenable();
				Reporter.log("Entered Site Name is : " + sitenme);

			}

			if (sitecode.equalsIgnoreCase("null")) {
				// value for Site code field is not entered
			} else {

				// Site Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton,
						sitecode, "Site code");
				waitforPagetobeenable();
				String sitecde = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton, "value");
				waitforPagetobeenable();
				Reporter.log("Entered Site Code is : " + sitecde);

			}

		} else {
			// Add new city is not selected
		}
	}

	public void newPremise_clickSiteToggleButton(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "premisecode");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newpremiseselectionmode");
		// New premise
		if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

			if (premisename.equalsIgnoreCase("null")) {
			} else {
				// Premise Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton,
						premisename, "Premise name");
				waitforPagetobeenable();
				String prmsenme = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Name is : " + prmsenme);

			}

			if (premisecode.equalsIgnoreCase("null")) {
				// value for Premise code field is not entered
			} else {
				// Premise Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton,
						premisecode, "Premise code");
				waitforPagetobeenable();
				String premisecde = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Code is : " + premisecde);

			}

		} else {
			// Add new Premise is not selected
		}
	}

	public void newPremise_clickOnPremisetoggleButton(String newpremiseselectionmode, String premisecode,
			String premisename) throws InterruptedException, IOException { // New
																			// premise
		if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

			if (premisename.equalsIgnoreCase("null")) {
			} else {
				// Premise Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton,
						premisename, "Premise name");
				waitforPagetobeenable();
				String prmsenme = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Name is : " + prmsenme);

			}

			if (premisecode.equalsIgnoreCase("null")) {
				// value for Premise code field is not entered
			} else {
				// Premise Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton,
						premisecode, "Premise code");
				waitforPagetobeenable();
				String premisecde = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Code is : " + premisecde);

			}

		} else {
			// Add new Premise is not selected
		}
	}

	public void device_powerAlarmWarningMessage() throws InterruptedException {
		boolean pwralrmErr = false;
		try {
			pwralrmErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_powerAlarmerrmsg);
			sa.assertTrue(pwralrmErr, "Power Alarm warning message is not displayed ");
			if (pwralrmErr) {
				String pwralarmErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_powerAlarmerrmsg);
				Reporter.log("Power Alarm  message displayed as : " + pwralarmErrMsg);
				Reporter.log("Power Alarm warning message displayed as : " + pwralarmErrMsg);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Power Alarm warning message is not dipslaying");
		} catch (Exception er) {
			er.printStackTrace();
		}
	}

	public void device_hexaSerialNumberWarningMessage() throws InterruptedException, IOException {
		// Serial Number Error Message
		boolean HexaserialNumberErr = false;

		try {
			HexaserialNumberErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_hexaSerialNumnerErrmsg);
			sa.assertTrue(HexaserialNumberErr, "Hexa Serial Number warning message is not displayed ");
			if (HexaserialNumberErr) {
				Reporter.log(" 'Hexa Serial number' warning message is dipslaying as expected");
				String hexaserialnumberErrMsg = getTextFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_hexaSerialNumnerErrmsg);
				Reporter.log("Hexa Serial Number  message displayed as : " + hexaserialnumberErrMsg);
				Reporter.log("Hexa Serial Number warning message displayed as : " + hexaserialnumberErrMsg);
			} else {
				Reporter.log("Hexa Serial Number warning message is not dipslaying");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Serial Number Warning message is not diplsying");
		}
	}

	public void device_managementAddressWarningMessage() throws InterruptedException, IOException {

		boolean mangadrsErr = false;
		try {
			mangadrsErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_managementAddresserrmsg);
			sa.assertTrue(mangadrsErr, "Management Addres warning message is not displayed ");
			if (mangadrsErr) {
				String mngadresErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_managementAddresserrmsg);
				Reporter.log("Management Addres  message displayed as : " + mngadresErrMsg);

				Reporter.log("Management Addres warning message displayed as : " + mngadresErrMsg);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("management Address warning message is not found");
		} catch (Exception ed) {
			ed.printStackTrace();
		}
	}

	public void device_serialNumberWarningMessage() throws InterruptedException, IOException {
		// Serial Number Error Message
		boolean serialNumberErr = false;

		try {
			serialNumberErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialNumberErrmsg);
			sa.assertTrue(serialNumberErr, "Serial Number warning message is not displayed ");
			if (serialNumberErr) {
				Reporter.log(" 'Serial number; warning message is dipslaying as expected");
				String serialnumberErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialNumberErrmsg);
				Reporter.log("Serial Number  message displayed as : " + serialnumberErrMsg);
				Reporter.log("Serial Number warning message displayed as : " + serialnumberErrMsg);
			} else {
				Reporter.log("Serial Number warning message is not dipslaying");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Serial Number Warning message is not diplsying");
		}
	}

	public void device_vendorModelWarningMessage() throws InterruptedException, IOException {
		boolean vendorErr = false;
		// Vendor/Model Error Message
		try {
			vendorErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vendorErrmsg);
			sa.assertTrue(vendorErr, "Vendor/Model warning message is not displayed ");
			if (vendorErr) {
				String vendorErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vendorErrmsg);
				Reporter.log("Vendor/Model  message displayed as : " + vendorErrMsg);
				Reporter.log("Vendor/Model warning message displayed as : " + vendorErrMsg);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Vendor/Mdel warning message is not dipslaying");
		} catch (Exception ed) {
			ed.printStackTrace();
		}
	}

	public void editSiteOrder_EPN_1G_IVRefAccess(String performReport, String ProactiveMonitor, String smartmonitor,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
			String nontermination, String Protected, String devicename, String remark, String siteOrderNumber,
			String IVReference, String circuitReference, String GCRoloType, String VLAN, String VlanEtherType,
			String primaryVLAN, String primaryVlanEtherType, String offnet, String mappingMode, String portBased,
			String vlanBased, String maapingModeAddedValue) throws InterruptedException, IOException {

		// String deviceName=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "proactiveMonitoring");
		// String mappingMode=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "smartMonitoring");

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Site alias
		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("Site alias field will not display for 'Belgacom VDSL' technology");
		} else {
			editsiteorder_sitealias(siteallias);
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(circuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

			// Mapping Mode
			// --- editSiteOrder_mappingMode(mappingMode);

			// validation --> sometime mapping mode dropdown will not edited,
			// port or Vlan fields will be edited
			if ((maapingModeAddedValue.equalsIgnoreCase("Port Based")) && (mappingMode.equalsIgnoreCase("null"))) {

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.portname_textField, portBased, "Port Name");

			} else if ((maapingModeAddedValue.equalsIgnoreCase("Vlan Based"))
					&& (mappingMode.equalsIgnoreCase("null"))) {

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.vlanid_textfield, vlanBased, "VLAN Id");
			}
		}

		if (technology.equalsIgnoreCase("Overture")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

			// GCR OLO Type
			// -- editSiteOrder_GCRoloType(GCRoloType);

			// VLAN
			// -- editsiteorder_VLAN(VLAN);

			// VLAN Ether Type
			// -- editSiteOrder_VLANEtherType(VLANEtherType);

		}

		if (technology.equalsIgnoreCase("Alu")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

			// Mapping Mode
			// -- editSiteOrder_mappingMode(mappingMode);

			// validation --> sometime mapping mode dropdown will not edited,
			// port or Vlan fields will be edited
			if ((maapingModeAddedValue.equalsIgnoreCase("Port Based")) && (mappingMode.equalsIgnoreCase("null"))) {

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.portname_textField, portBased, "Port Name");

			} else if ((maapingModeAddedValue.equalsIgnoreCase("Vlan Based"))
					&& (mappingMode.equalsIgnoreCase("null"))) {

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.vlanid_textfield, "VLAN Id");
			}

		}

		if ((technology.equalsIgnoreCase("Accedian")) || (technology.equalsIgnoreCase("Accedian-1G"))) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("No additonal fields displays");
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder__MSPselectedhubAndSpoke_Primay(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {
	
		// Site order Number

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Site Alias
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {
			Reporter.log("NO additoinal fields");

		}

		if (technology.equals("Overture")) {
			Reporter.log("NO additoinal fields");
		}

		if (technology.equals("Accedian")) {

			Reporter.log("NO additoinal fields");

		}

		if (technology.equals("Cyan")) {
			Reporter.log("NO additoinal fields");
		}

	}

	public String splitPhysicalSitealue(String siteValue)
			throws IOException, InterruptedException {
		
		String value = null;
		int len = siteValue.length();

		if (siteValue.startsWith("(")) {
			value = siteValue.substring(1, len - 1);
		} else if (siteValue.startsWith(" (")) {
			value = siteValue.substring(2, len - 1);
		} else {
			value = siteValue;
		}
		return value;
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Access(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {

		
		// Site order Number
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		} else {
			// Site Alias
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}
		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Overture")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// EPN Offnet
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + EPNoffnet + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// EPN EOSDH

		}

		if (technology.equals("Alu")) {
			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Accedian-1G")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1+devicename+Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		}
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_EPN_Primay(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {

		// Site order Number
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		} else {
			// Site Alias
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}
		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Overture")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// EPN Offnet
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + EPNoffnet + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// EPN EOSDH
			if (EPNEOSDH.equalsIgnoreCase("Yes")) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + EPNEOSDH + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			} else {
				Reporter.log("EPN EOSDH will not display, if it is not selected");
			}
		}

		if (technology.equals("Alu")) {
			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Accedian-1G")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1+devicename+Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		}
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Access(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH)
			throws InterruptedException, IOException {

	
		// Site order Number
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		} else {
			// Site Alias
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}
		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}

		
		if (technology.equals("Overture")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// GCR OLO Type dropdown
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + GCRolo + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// VLAN tetx field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vlan + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// VLAN Ether type dropdown
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vlanether + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Primary VLAN Text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + primaryVlan + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Primary VLAN Ether Type
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + primaryVlanether + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}

		if (technology.equals("Alu")) {
			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Accedian-1G")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1+devicename+Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		}
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_hubAndSpoke_Primay(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {

	
		// Site order Number
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		} else {
			// Site Alias
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}
		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Overture")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Alu")) {
			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Accedian-1G")) {
			// Non_termination Point checkbox
			verifyExists(
					Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Device Name text field
			// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1+devicename+Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		}
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_EPN_Access(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH) throws InterruptedException, IOException {

	
		// Site order Number
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		} else {
			// Site Alias
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}
		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Overture")) {

			// EPN Offnet
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + EPNoffnet + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// EPN EOSDH

		}

		if (technology.equals("Accedian")) {
			Reporter.log("NO additional fields");
		}

		if (technology.equals("Cyan")) {
			Reporter.log("NO additional fields");
		}

	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_EPN_Primay(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH)
			throws InterruptedException, IOException {

	
		// Site order Number
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		} else {
			// Site Alias
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}
		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Overture")) {

			// EPN Offnet
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + EPNoffnet + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// EPN EOSDH

		}

		if (technology.equals("Accedian")) {
			Reporter.log("NO additional fields");
		}

		if (technology.equals("Cyan")) {
			Reporter.log("NO additional fields");
		}

	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_hubAndSpoke_Access(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection,
			String siteOrderNumber, String circuitref, String offnetSelection, String IVReference,
  			String GCRolo, String Vlan, String Vlanether, String primaryVlan, String primaryVlanether,String EPNoffnet,String EPNEOSDH)
			throws InterruptedException, IOException {

		
		// Site order Number
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteOrderNumber + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		if (technology.equals("Belgacom VDSL")) {
			Reporter.log("No additonal fields displays");
		} else {
			// Site Alias
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		}
		// IV Reference
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {

			Reporter.log("NO additional fields");
		}

	if (technology.equals("Overture")) {

			if (technology.equals("Overture")) {
				// Non_termination Point checkbox
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + nonterminatepoinr
						+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

				// GCR OLO Type dropdown
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + GCRolo + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

				// VLAN tetx field
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vlan + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

				// VLAN Ether type dropdown
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + Vlanether + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

				// Primary VLAN Text field
				verifyExists(
						Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + primaryVlan + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

				// Primary VLAN Ether Type
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + primaryVlanether
						+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			}

			if (technology.equals("Accedian")) {
				Reporter.log("NO additional fields required");
			}

			if (technology.equals("Cyan")) {
				Reporter.log("NO additional fields required");
			}
		}
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_MSPselected_P2P(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection) throws InterruptedException, IOException {

		


		// Country
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + country + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		// City

		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + city + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
				String siteV = splitPhysicalSitealue(site);
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteV + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

			else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			// New City
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + xngcitycode + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

			// New Site
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + CSR_Name + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Performance Reporting
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + performReport + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Proactive Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + ProactiveMonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);
		waitforPagetobeenable();

		// Smarts Monitoring
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + smartmonitor + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Technology
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// Site Alias
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + siteallias + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		// VLAN Id
		// verifyEnteredvalues("VLAN Id", VLANid);

		// DCA Enabled Site
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + DCAenabledsite + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		if (DCAenabledsite.equalsIgnoreCase("Yes")) {

			// Cloud Service Provider
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + cloudserviceprovider
					+ Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		// Remark

		(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + remark + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2))
				.equals(remark);

		if (technology.equals("Atrica")) {
			// Device Name text field
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.viewPage1 + devicename + Lanlink_Metro_Obj.Lanlink_Metro.viewPage2);

		}

		if (technology.equals("Overture")) {
			Reporter.log("NO additional fields");
		}

		if (technology.equals("Accedian")) {
			Reporter.log("NO additional fields");
		}

		if (technology.equals("Cyan")) {
			Reporter.log("NO additional fields");
		}
	}

	public void VerifyDataEnteredForSiteOrder_viewSiteOrder_P2P(String country, String city, String CSR_Name, String site,
			String performReport, String ProactiveMonitor, String smartmonitor, String technology, String siteallias,
			String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue, String remark,
			String xngcityname, String xngcitycode,String devicename, String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
			String existingsiteselection, String newsiteselection)
			throws InterruptedException, IOException {

		
		 //Country	
    	verifyEnteredvalues("Device Country", country);
    	
    //City
    	
    	if((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {
    		
    	//City	
    		verifyEnteredvalues("Device Xng City", city);
    		
    	//Site
    		if((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
    			String siteValue=splitPhysicalSitealue(site);
    			verifyEnteredvalues("Physical Site: CSR Name", siteValue);
    		}
    		
    		else if((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
    			verifyEnteredvalues("Physical Site: CSR Name", CSR_Name);
    		}
    		
    	}
    	else if((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {
    		
    		//New City
    		verifyEnteredvalues("Device Xng City", xngcitycode);
    		
    		//New Site
    		verifyEnteredvalues("Physical Site: CSR Name", CSR_Name);
    		
    	}
    	
    	
    //Performance Reporting
    	verifyEnteredvalues("Performance Reporting", performReport);
    	
    //Proactive Monitoring
    	verifyEnteredvalues("Proactive Monitoring", ProactiveMonitor);
    	
    //Smarts Monitoring
    	verifyEnteredvalues("Smarts Monitoring", smartmonitor);
    	
    //Technology
    	verifyEnteredvalues("Technology", technology);
    	
    	if(technology.equals("Belgacom VDSL")) {
    		
    	Reporter.log("Site alias and Vlan Id text field will not display for 'BElgacom' technology");
    		
    	}else {
    		//Site Alias
        	verifyEnteredvalues("Site Alias", siteallias);
        	
        //VLAN Id
//        	verifyEnteredvalues("VLAN Id", VLANid);
    	}
    	
    
    	
    //DCA Enabled Site
    	verifyEnteredvalues("DCA Enabled Site", DCAenabledsite);
    	
    	if(DCAenabledsite.equalsIgnoreCase("Yes")) {
    		
    		//Cloud Service Provider
    		     verifyEnteredvalues("Cloud Service Provider", cloudserviceprovider);
    		
    	}
    	
    //Remark
    	compareText("Remark", Lanlink_Metro_Obj.Lanlink_Metro.remark_viewPage, remark);
    	
    	
    	if(technology.equals("Atrica")) {
    		//Non_termination Point checkbox
    			verifyEnteredvalues("Non Termination Point", nonterminatepoinr);
    		
    		//Device Name text field
    			verifyEnteredvalues("Device Name", devicename);
    		
    	}
    	
    	
    	if(technology.equals("Overture")) {
    		//Non_termination Point checkbox
    			verifyEnteredvalues("Non Termination Point", nonterminatepoinr);
    		
    	}
    	
    	if(technology.equals("Alu")) {
    		//Device Name text field
			verifyEnteredvalues("Device Name", devicename);
    	}
    	
    	if(technology.equals("Accedian-1G")) {
    		//Non_termination Point checkbox
			verifyEnteredvalues("Non Termination Point", nonterminatepoinr);

    	}
    	
    	if(technology.equals("Belgacom VDSL")) {
    		Reporter.log("No additonal fields displays");
    	}
    	}
    

	public void selectRowForAddingInterface_Actelis(String interfacenumber) throws IOException, InterruptedException {
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		Reporter.log("check second time");
		int TotalPages;

		String TextKeyword = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceToSelect_actelis_totalpage);

		TotalPages = Integer.parseInt(TextKeyword);

		Reporter.log("Total number of pages in table is: " + TotalPages);

		ab:

		if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceToSelect_actelis_currentpage);
				int Current_page = Integer.parseInt(CurrentPage);

				Reporter.log("Currently we are in page number: " + Current_page);

				List<String> results = new ArrayList<>(
						Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.interfacenumber1 + interfacenumber
								+ Lanlink_Metro_Obj.Lanlink_Metro.interfacenumber2)));

				int numofrows = results.size();
				Reporter.log("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.nextBtn);
					waitforPagetobeenable();

				}

				else {

					for (int i = 0; i < numofrows; i++) {

						try {

							resultflag = isElementPresent(results.get(i));
							Reporter.log("status of result: " + resultflag);
							if (resultflag) {
								Reporter.log(getTextFrom(results.get(i)));
								click(getTextFrom(results.get(i)));

								click(Lanlink_Metro_Obj.Lanlink_Metro.Next,"Next button");
								waitforPagetobeenable();
							}

						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = new ArrayList<>(Arrays.asList(Lanlink_Metro_Obj.Lanlink_Metro.interfacenumber1
									+ interfacenumber + Lanlink_Metro_Obj.Lanlink_Metro.interfacenumber2));
							numofrows = results.size();
							// results.get(i).click();
							Reporter.log("selected row is : " + i);
						}
					}
					break ab;
				}
			}
		} else {

			Reporter.log("No values available in table");
			Reporter.log("No Interfaces got fetched");
		}

	}

	public void verifyenteredvaluesForEditPage_noneditableFields(String labelname, String ExpectedValue) throws InterruptedException, IOException {
		boolean actualvalue = false;
		try {

			actualvalue=webDriver.findElement(By.xpath("//div[div[label[text()='" + labelname + "']]]//div[text()='"+ ExpectedValue +"']")).isDisplayed();
			if (actualvalue) {

				Reporter.log("Value is displaying as expected for the field: " + labelname);

			} else {

				Reporter.log("Value is not displaying as expected for the text field: " + labelname);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}

	}

	public void verifyinterfaceSpeeddropdown() throws InterruptedException, IOException {
		// verify the list of interfaceSpeed
		try {

			String[] interfacelist = { "1GigE", "10GigE" };

			interfacespeeddropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceSpeed);

			click(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceSpeed);

			List<WebElement> listofinterfacespeed = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement interfacespeed : listofinterfacespeed) {

				boolean match = false;
				for (int i = 0; i < interfacelist.length; i++) {
					if (interfacespeed.getText().equals(interfacelist[i])) {
						match = true;
						Reporter.log("interface speeds : " + interfacespeed.getText());
						Report.LogInfo("Info","interface speeds : " + interfacespeed.getText(),"PASS");
						sa.assertTrue(match,"");
					}
				}
				
			}
			

		} catch (AssertionError error) {

			error.printStackTrace();

		} catch (Exception e) {
			Reporter.log("dropdowns values in Interface speed are mismiatching under service type");
			Reporter.log("dropdowns values in Interface speed are mismiatching under service type");
		}

	}

	public void verifyservicesubtypesdropdownwgenAutoCreatealoneselected() throws InterruptedException, IOException {

		try {
			String[] servicesubtypelist = { "Direct Fiber", "LANLink International", "LANLink Metro",
					"LANLink National", "LANLink Outband Management", "OLO - (GCR/EU)" };

			boolean servicesubtypesdropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

			// verify the list of service sub types
			click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			List<WebElement> listofServicesubtypes = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement servicesubtypes : listofServicesubtypes) {

				boolean match = false;
				for (int i = 0; i < servicesubtypelist.length; i++) {
					if (servicesubtypes.getText().equals(servicesubtypelist[i])) {
						match = true;
						Reporter.log("service sub types : " + servicesubtypes.getText());
						Report.LogInfo("Info","service sub types : " + servicesubtypes.getText(),"PASS");
						sa.assertTrue(match,"");
					}
				}
				

			}

		} catch (AssertionError error) {

			error.printStackTrace();

		} catch (Exception e) {
			Reporter.log("Dropdown values inside service subtypes are mismatching");
			Reporter.log("Dropdwon values inside service subtypes are mismatching");
		}

	}

	public void verifyservicesubtypesdropdownwhenMSPaloneselected() throws InterruptedException, IOException {

		try {
			String[] servicesubtypelist = { "LANLink International", "LANLink Metro", "LANLink National",
					"OLO - (GCR/EU)" };

			boolean servicesubtypesdropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

			// verify the list of service sub types
			click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			List<WebElement> listofServicesubtypes = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement servicesubtypes : listofServicesubtypes) {

				boolean match = false;
				for (int i = 0; i < servicesubtypelist.length; i++) {
					if (servicesubtypes.getText().equals(servicesubtypelist[i])) {
						match = true;
						Reporter.log("service sub types : " + servicesubtypes.getText());
						Report.LogInfo("Info","service sub types : " + servicesubtypes.getText(),"PASS");
						sa.assertTrue(match,"");
					}
				}
				

			}
		} catch (AssertionError error) {

			error.printStackTrace();

		} catch (Exception e) {

			Reporter.log("Dropdown values inside service subtypes are mismatching");
			Reporter.log("Dropdown values inside service subtypes are mismatching");
		}

	}

	public void verifyservicesubtypesdropdownwhenMSPandAutoCreatenotslected()
			throws InterruptedException, IOException {

		String[] servicesubtypelist = { "Direct Fiber", "LANLink International", "LANLink Metro", "LANLink National",
				"LANLink Outband Management", "OLO - (GCR/EU)" };

		try {
			boolean servicesubtypesdropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			// sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown
			// is not displayed");

			// verify the list of service sub types
			click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);
			List<WebElement> listofServicesubtypes = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement servicesubtypes : listofServicesubtypes) {

				boolean match = false;
				for (int i = 0; i < servicesubtypelist.length; i++) {
					if (servicesubtypes.getText().equals(servicesubtypelist[i])) {
						match = true;
						Reporter.log("service sub types : " + servicesubtypes.getText());
						Report.LogInfo("Info","service sub types : " + servicesubtypes.getText(),"PASS");
						sa.assertTrue(match,"");
					}
				}
				

			}

		} catch (AssertionError error) {

			error.printStackTrace();

		} catch (Exception e) {
			Reporter.log("Dropdown values in Service subtypes are mismatching");
			Reporter.log("Dropdown values in Service subtypes are mismatching");
		}

	}

	public void verifyservicesubtypesdropdownwhenMSPandAutoCreateselected() throws InterruptedException, IOException {

		String[] servicesubtypelist = { "LANLink International", "LANLink Metro", "LANLink National",
				"OLO - (GCR/EU)" };

		try {
			boolean servicesubtypesdropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

			// verify the list of service sub types
			click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			List<String> listofServicesubtypes = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofServicesubtypes)));
			for (String servicesubtypes : listofServicesubtypes) {

				boolean match = false;
				for (int i = 0; i < servicesubtypelist.length; i++) {
					if (servicesubtypes.equals(servicesubtypelist[i])) {
						match = true;
						Reporter.log("service sub types : " + servicesubtypes);
						sa.assertTrue(match, "");
					}
				}

			}

		} catch (AssertionError error) {

			error.printStackTrace();

		} catch (Exception e) {
			Reporter.log("Dropdown values in Service subtypes are mismatching");
			Reporter.log("Dropdown values in Service subtypes are mismatching");
		}

	}

	public String selectRowForconfiglinkunderIntermediateEquipment(String interfacename) throws InterruptedException, IOException {

		int TotalPages;

		// scroll down the page
		// JavascriptExecutor js = ((JavascriptExecutor) driver);
		// js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		// waitforPagetobeenable();
		String interfaceAvailability = "Yes";
		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.interfaces_header);
		String TextKeyword = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.configure_totalPage);

		TotalPages = Integer.parseInt(TextKeyword);

		Reporter.log("Total number of pages in table is: " + TotalPages);

		ab: if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.configure_currentpage);
				int Current_page = Integer.parseInt(CurrentPage);
				Reporter.log("The current page is: " + Current_page);

				assertEquals(k, Current_page, "");

				Reporter.log("Currently we are in page number: " + Current_page);

				List<WebElement> results = webDriver.findElements(By.xpath("//div[@role='row']//div[text()='"+ interfacename +"']"));
				int numofrows = results.size();
				Reporter.log("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.intermediateequip_nextpage);

				}

				else {

					for (int i = 0; i < numofrows; i++) {

						try {

							resultflag = results.get(i).isDisplayed();
							Reporter.log("status of result: " + resultflag);
							if (resultflag) {
								Reporter.log(results.get(i).getText());
								results.get(i).click();
								Report.LogInfo("Info", interfacename + " is selected","PASS");
								waitforPagetobeenable();
								break ab;
							}

						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = webDriver.findElements(
									By.xpath("//div[@role='row']//div[text()='"+ interfacename +"']"));
							numofrows = results.size();
							// results.get(i).click();
							Reporter.log("selected row is : " + i);

						}
					}
				}

			}
		} else {
			Reporter.log("No values present inside the table");
		}
		return interfaceAvailability;
	}

	public void verifyA_Endtechnologydropdown() throws InterruptedException, IOException {

		try {
			String[] A_endTechnolnogylist = { "Atrica", "MMSP", "Ethernet over Fibre" };

			boolean A_EndTechnolnogy = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.A_Endtechnology);

			sa.assertTrue(A_EndTechnolnogy, "A-End technolnogy dropdown is not displayed");

			// verify the list of A-End technolnogies
			click(Lanlink_Metro_Obj.Lanlink_Metro.A_Endtechnology);

			List<WebElement> listofA_endTechnologies = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement A_endTechnolnogies : listofA_endTechnologies) {

				boolean match = false;
				for (int i = 0; i < A_endTechnolnogylist.length; i++) {
					if (A_endTechnolnogies.getText().equals(A_endTechnolnogylist[i])) {
						match = true;
						Reporter.log("A end technology values : " + A_endTechnolnogies.getText());
						Report.LogInfo("Info","A end technology are : " + A_endTechnolnogies.getText(),"PASS");
						sa.assertTrue(match,"");
					}
				}
				

			}
		} catch (AssertionError error) {

			error.printStackTrace();

		} catch (Exception e) {
			Reporter.log("Dropdwon values inside A-end technology are mismatching");
			Reporter.log("Dropdwon values inside A-end technology are mismatching");
		}

	}

	public void verifyB_Endtechnologydropdowb() throws InterruptedException, IOException {

		try {
			String[] B_endTechnolnogylist = { "Atrica", "MMSP", "Ethernet over Fibre" };

			boolean B_Endtechnolnogy = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.B_Endtechnology);
			sa.assertTrue(B_Endtechnolnogy, "B-End technolnogy dropdown is not displayed");

			// verify the list of A-End technolnogies
			click(Lanlink_Metro_Obj.Lanlink_Metro.B_Endtechnology);

			List<WebElement> listofB_endTechnologies = webDriver
					.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
			for (WebElement B_endTechnolnogies : listofB_endTechnologies) {

				boolean match = false;
				for (int i = 0; i < B_endTechnolnogylist.length; i++) {
					if (B_endTechnolnogies.getText().equals(B_endTechnolnogylist[i])) {
						match = true;
						Reporter.log("B end technology values : " + B_endTechnolnogies.getText());
						Report.LogInfo("Info", "B end technologies are : " + B_endTechnolnogies.getText(),"PASS");
						sa.assertTrue(match,"");
					}
				}
				

			}
		} catch (AssertionError error) {

			error.printStackTrace();

		} catch (Exception e) {
			Reporter.log("Dropdwon values inside B-end technology are mismatching");
			Reporter.log("Dropdwon values inside B-end technology are mismatching");
		}
	}

	public void device_linklostForwarding(String linkLostForwarding, String state)
			throws InterruptedException, IOException {

		boolean linklostenable = false;
		boolean linklost = false;
		try {
			linklost = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);
			sa.assertTrue(linklost, "Link lost Forwarding checkbox under add device is not available");
			if (linklost) {
				// Find whether it enabled or disabled
				linklostenable = isEnable(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);
				if (state.equalsIgnoreCase("disabled")) {
					if (linklostenable) {
					} else {
					}
				} else if (state.equalsIgnoreCase("enabled")) {
					if (linklostenable) {

						// select the checkbox as per input
						boolean linklostSelection = isSelected(
								Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding, "Link lost");
						if (linklostSelection) {

							// click on link lost checkbox
							if (linkLostForwarding.equalsIgnoreCase("Yes")) {

							} else {
								click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);
								waitforPagetobeenable();
								Reporter.log(" Link Lost Forwarding is unselected as expected");
							}
						} else {
							Reporter.log(" 'link lostforwarding' is not selected by default");
						}

					} else {
						// Link lost Forwarding' checkbox is disabled under 'Add
						// CPE device' page
					}

				}
			} else {
				Reporter.log(" 'Link Lost Forwarding' checkbox is not displaying under 'Add CPE device' page");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}

	public void device_mepID(String Mepid) {

		String mepValue = "null";
		boolean mepid = false;
		try {
			mepid = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_mepid);

			sa.assertTrue(mepid, "Mepid field under 'Add CPE device' page is not available");

			if (mepid) {

				Reporter.log("MEP Id  text field is displaying as expected");

				mepValue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_mepid, "value");
				if (mepValue.equalsIgnoreCase("null")) {
					Reporter.log(
							" No values are displaying under 'MEP ID' field. It should be auto populated by default");
				} else {
					Reporter.log(" MEP ID field is auto populated and it is displaying as : " + mepValue);
				}

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void device_powerAlarm(String poweralarm) throws InterruptedException, IOException {

		addDropdownValues_commonMethod("Power Alarm",Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_poweralarm, poweralarm);

	}

	public void device_serialNumber(String serialNumber) throws InterruptedException, IOException, IOException {

		// Serial Number
		boolean serialNmber = false;
		try {
			serialNmber = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialnumber);
			sa.assertTrue(serialNmber, "Serial Number is not available in 'Add CPE Device' page");
			if (serialNmber) {
				if (serialNumber.equalsIgnoreCase("null")) {
				} else {

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialnumber, serialNumber, "Serial Number");
					waitforPagetobeenable();
					String SNactualValue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialnumber,
							"value");
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Serial Number' text field is not displaying");
		} catch (Exception er) {
			er.printStackTrace();
			Reporter.log("not able to enter value under 'Serial number' textfield");
		}
	}

	public void device_snmPro(String snmproValueToBeChanged) throws IOException, InterruptedException {

		boolean sNmpro = false;
		try {

			sNmpro = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_snmpro);

			if (sNmpro) {
				Reporter.log("Smpro text field is displaying as expected");

				boolean actualValue_snmpro = isElementPresent(
						Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_snmpro_autoPopulatedValue);

				if (snmproValueToBeChanged.equalsIgnoreCase("null")) {
					Reporter.log("No changes has been made to 'Snmpro' field");
					Reporter.log(" 'Snmpro' field is displaying as: "
							+ getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_snmpro, "value"));
				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_snmpro);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_snmpro, snmproValueToBeChanged, "snmpro");
					waitforPagetobeenable();

				}

			} else {
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}

	}

	public void device_vendorModel(String[] Vender, String vender) throws InterruptedException, IOException {
		boolean vendor = false;
		// String Vender[]=(String) DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "Vender");

		try {
			vendor = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender);
			sa.assertTrue(vendor, "Vender/Model dropdown is not available");

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender);

			try {
				List<String> listofvender = new ArrayList<>(
						Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofvender)));

				if (listofvender.size() > 0) {

					for (String vendertypes : listofvender) {

						boolean match = false;
						for (int i = 0; i < Vender.length; i++) {
							if (vendertypes.equals(Vender[i])) {
								match = true;
								Reporter.log("list of vendor under add devices are : " + vendertypes);
								Reporter.log("list of vendor under add devices are : " + vendertypes);

							}
						}
						sa.assertTrue(match, "");
					}

				} else {
					Reporter.log("dropdown value inside Vender/Model is empty");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			// Entering value inside Vendor/Model dropdown
			try {
				if (vender.equalsIgnoreCase("null")) {

					Reporter.log("No values has been passed for Mandatory 'Vendor/Model' dropdown for adding device");

				} else {
					//sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender,"Vendor/Model",vender);
					addDropdownValues_commonMethod("Vendor/Model",Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender,vender);
					//webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//div[text()='"+vender +"']")).click();					
					Reporter.log(
							vender + " is the value passed for Mandatory 'Vendor/Model' dropdown for adding device");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("vendor/Model dropdown is not dipslaying under 'Add CPE device' page");
		}
	}

	public void selectTechnology_HubAndSpoke() throws InterruptedException, IOException {

		// verify Technology popup
		boolean technologypopup = false;
		technologypopup = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.technologyPopup);
		if (technologypopup) {
			Reporter.log("Technology popup is displaying as expected");
		} else {
			Reporter.log("Technology popup is not displaying");
		}

		// Dropdown values inside popup
		boolean technologyDropdown = false;
		technologyDropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.technologypopup_dropdown);
		if (technologyDropdown) {
			Reporter.log("Technology dropdown is displaying as expected");

			click(Lanlink_Metro_Obj.Lanlink_Metro.technologypopup_dropdown);
			waitforPagetobeenable();

			// verify list of values inside technology dropdown
			List<String> listofTechnololgy = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofTechnology)));

			if (listofTechnololgy.size() > 0) {

				for (String technoloyTypes : listofTechnololgy) {
					Reporter.log("List of values available under 'Technology' dropdown are: " + technoloyTypes);
				}
			}

			// Select the Technology
			webDriver.findElement(By.xpath("//div[@class='modal-body']//div[contains(text(),'Overture')]")).click();;

			waitforPagetobeenable();
			String actualValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.tchnologyPopup_dropdownValues);
			Reporter.log(" 'Technology' selected is: " + actualValue);

		} else {
			Reporter.log("Technology dropdown is not displaying");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateEquipment_OKbuttonforpopup,"Ok button");
		waitforPagetobeenable();

	}

	public void verifyavailablecircuitdropdown() throws InterruptedException, IOException {
		try {
			boolean availablecircuitsdropdown = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AvailableCircuits);

			// sa.assertTrue(availablecircuitsdropdown, "available circuit
			// dropdown is not displayed");

		} catch (AssertionError e) {
			Reporter.log("Available circuit dropdown under servicetype got failed");
		}
	}

	public void newSite(String newsiteselectionmode, String sitename, String sitecode)
			throws InterruptedException, IOException, IOException {

		// New site
		if (newsiteselectionmode.equalsIgnoreCase("yes")) {

			if (sitename.equalsIgnoreCase("null")) {
			} else {
				// Site Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitenamefield, sitename, "Site Name");
				waitforPagetobeenable();
				String sitenme = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitenamefield,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Site Name is : " + sitenme);

			}

			if (sitecode.equalsIgnoreCase("null")) {
			} else {

				// Site Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitecodefield, sitecode, "Site Code");
				waitforPagetobeenable();
				String sitecde = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitecodefield,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Site Code is : " + sitecde);

			}

		}

	}

	public void editCity(String editExistingCity, String editNewCity, String dropdown_xpath,
			String selectCityToggleButton, String addCityToggleButton, String dropdownValue, String editNewCityName,
			String editNewCityCode, String labelname) throws InterruptedException, IOException, IOException {

		if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {

			existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
		}

		else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {

			existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
		}

		else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {

			newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode, labelname);
		}

		else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {

			newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode, labelname);
		}

		else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {

			Reporter.log("No chnges made under 'City' field");
		}

	}

	public void newPremise(String newpremiseselectionmode, String premisecode, String premisename)
			throws InterruptedException, IOException, IOException {

		// New premise
		if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

			if (premisename.equalsIgnoreCase("null")) {
			} else {
				// Premise Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield, premisename,
						"Premise Name");
				waitforPagetobeenable();
				String prmsenme = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield, "value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Name is : " + prmsenme);
			}

			if (premisecode.equalsIgnoreCase("null")) {
			} else {
				// Premise Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield, premisecode,
						"Premise Code");
				waitforPagetobeenable();
				String premisecde = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield, "value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Code is : " + premisecde);
			}

		} else {
		}

	}

	public void newcity(String newcityselectionmode, String cityname, String citycode)
			throws InterruptedException, IOException, IOException {

		// New City
		if (newcityselectionmode.equalsIgnoreCase("yes")) {

			if (cityname.equalsIgnoreCase("null")) {
			} else {
				// City Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_citynamefield, cityname, "City Name");
				waitforPagetobeenable();
				String citynme = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_citynamefield,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered City Name is : " + citynme);
			}

			if (citycode.equalsIgnoreCase("null")) {
				// value for City Code field is not added
			} else {

				// City Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_citycodefield, citycode, "City Code");
				waitforPagetobeenable();
				String citycde = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_citycodefield,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered City Code is : " + citycde);

			}

		} else {
		}

	}

	public void device_editMEPIdField(String Mepid) {

		boolean mepIdavailability = false;
		try {
			mepIdavailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_mepid);
			if (mepIdavailability) {

				if (Mepid.equalsIgnoreCase("null")) {
				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_mepid);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_mepid, Mepid, "MEP Id");
					waitforPagetobeenable();
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void device_editnamefield(String cpename) {

		boolean name = false;
		try {

			name = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name);
			waitforPagetobeenable();

			if (name) {

				if (cpename.equalsIgnoreCase("null")) {

				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name, cpename, "CPE Name");
					waitforPagetobeenable();

					String actualValue_Name = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name, "value");
				}

			} else {
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void editPremise(String editExistingPremise, String editNewPremise, String dropdown_xpath,
			String selectPremiseToggleButton, String addPremisetoggleButton, String dropdownValue,
			String editNewPremiseName, String editNewPremiseCode, String labelname)
			throws InterruptedException, IOException {

		if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {

			existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
		}

		else if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("null")) {

			existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
		}

		else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("Yes")) {

			newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode, labelname);
		}

		else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("Yes")) {

			newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode, labelname);
		}

		else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("null")) {

			Reporter.log("No changes made under 'Premise' field");

		}

	}

	public void configure_circuitId(String circuitId) throws InterruptedException {

		try {
			if (circuitId.equalsIgnoreCase("null")) {

				Reporter.log(" No input provided for 'Circuit ID' field");

			} else {
				click(Lanlink_Metro_Obj.Lanlink_Metro.selectCircuit_togglebutton);
				waitforPagetobeenable();

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureXNGCircuitID, circuitId, "Circuit Id");

				String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Equipment_configureXNGCircuitID,
						"value");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
		waitforPagetobeenable();
	}

	public void device_country(String country) throws InterruptedException, IOException {

		selectByVisibleText(Lanlink_Metro_Obj.Lanlink_Metro.countryDropdown_selectTag, country,"Country");
	}

	public void device_editMACaddress(String Macaddress) throws IOException, InterruptedException {

		boolean macAddress = false;
		try {
			macAddress = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macaddress);

			if (macAddress) {
				if (Macaddress.equalsIgnoreCase("null")) {
				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macaddress);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macaddress, Macaddress, "MAC Address");
					String actualValue_MacAddress = getAttributeFrom(
							Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macaddress, "value");
				}
			} else {
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void device_editManagementAddressField(String managementAddress) {

		boolean manageAddressAvailability = false;
		try {

			// click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_selectSubnettogglebutton);
			// waitforPagetobeenable();

			manageAddressAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress);
			waitforPagetobeenable();

			if (manageAddressAvailability) {

				if (managementAddress.equalsIgnoreCase("null")) {

				} else {

					click(Lanlink_Metro_Obj.Lanlink_Metro.changeButton_managementAddress);
					waitforPagetobeenable();

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress, managementAddress);
					waitforPagetobeenable();
				}

			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void device_editPowerAlarm(String poweralarm) throws InterruptedException, IOException {

		addDropdownValues_commonMethod("Power Alarm",Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_poweralarm, poweralarm);

	}

	public void device_editMediaselection(String Mediaselection) throws InterruptedException, IOException {

		addDropdownValues_commonMethod("Media Selection",Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_mediaselection, Mediaselection);

	}

	public void device_editlinkLostforwarding(String linkLostForwarding) {

		boolean linklostcheckbox = false;
		try {
			linklostcheckbox = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);

			if (linklostcheckbox) {

				if (linkLostForwarding.equalsIgnoreCase("null")) {

					// No changes made for linklost forwarding while editing cpe
					// device under equipment
				} else {

					boolean linklost = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding,
							"linklostforowarding");

					if (linkLostForwarding.equalsIgnoreCase("yes")) {

						if (linklost) {
							// linklost forwarding has been edited for cpe
							// device under Equipment

						} else {

							click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);
						}

					} else if (linkLostForwarding.equalsIgnoreCase("no")) {

						if (linklost) {
							click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);

						} else {

							// linklost forwarding has been edited for cpe
							// device under Equipment
						}

					}
				}
			} else {
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception er) {
			er.printStackTrace();
		}
	}

	public void device_editlinklostforwarding_10G(String linklostforwarding) {

		boolean linklostcheckboxAvailability = false;
		boolean linklostcheckboxEnabled = false;
		try {
			linklostcheckboxAvailability = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);

			if (linklostcheckboxAvailability) {
				// 'Link lost Forwarding' checkbox is displaying in 'Edit CPE
				// device' page as expected

				linklostcheckboxEnabled = isEnable(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_linklostforowarding);
				if (linklostcheckboxEnabled) {
					Reporter.log(" 'link lostforwarding is enabled for 10G");

				} else {
					Reporter.log("link lost checkbox is disabled as expected");
				}

			} else {
				// 'Link Lost Forwarding' checkbox is not available in 'Edit CPE
				// device' page
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception er) {
			er.printStackTrace();
		}

	}

	public void device_editManagementAddressField_MSPselected(String managementAddress)
			throws InterruptedException, IOException {

		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress, managementAddress, "Management Address");

	}

	public void device_editserialnumber(String serialNumber) {

		boolean serialunmberAvailability = false;
		try {

			serialunmberAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialnumber);
			waitforPagetobeenable();

			if (serialunmberAvailability) {
				// 'Serial Number' field is displaying under 'Edit CPE device'
				// page

				if (serialNumber.equalsIgnoreCase("null")) {

					// "No changes made for 'Serial Number' field while editing

				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialnumber);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialnumber, serialNumber, "Serial Number");
					waitforPagetobeenable();

					String actualValue_Name = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_serialnumber,
							"value");
				}

			} else {
				// 'Serial Number' field is not available under 'Edit CPE
				// device' page
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void device_editSnmproField() {

		boolean sNmpro = false;
		try {

			sNmpro = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_snmpro);

			if (sNmpro) {
				// ' Snmpro' field is displaying in 'Edit CPE Device' page as
				// expected

				boolean actualValue_snmpro = isElementPresent(
						Lanlink_Metro_Obj.Lanlink_Metro.CPEdevice_snmpro_autoPopulatedValue);
				if (actualValue_snmpro) {
					// 'Snmpro' field value is displaying as expected

				} else {
					// 'Snmpro' value is not displaying as expected."
				}

			} else {
				// 'Snmpro' field is not available in 'Edit CPE Device' page
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void device_editVendorModelField(String vender) {

		boolean vendor = false;
		try {

			vendor = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender);
			waitforPagetobeenable();
			if (vendor) {

				// 'Vendor/Model' dropdown is displaying in 'Edit CPE Device'
				// page

				if (vender.equalsIgnoreCase("null")) {
					// "No changes made for 'Vender/Model' dropdown while
					// editing

				} else {
					click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevice_vendoModel_xbutton,"X button");
					waitforPagetobeenable();

					addDropdownValues_commonMethod("Vendor/Model",Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender,vender);
				}
			} else {
				// 'Vendor/Model' dropdown is not available in 'Edit CPE Device'
				// page
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void device_editVLANIdField(String VLANId) throws IOException, InterruptedException {

		boolean VLANIDavailability = false;
		try {
			VLANIDavailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vlanId);
			if (VLANIDavailability) {

				if (VLANId.equalsIgnoreCase("null")) {
					// "No changes made for 'VLAN Id' field while editing cpe
					// device under Equipment
				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vlanId);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vlanId, VLANId, "VLan Id");
					waitforPagetobeenable();

					String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vlanId, "value");
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'VLAN id' field is not available ");
		} catch (Exception err) {
			err.printStackTrace();
			Reporter.log(" Not able to enter value in 'VLAN Id' field");
		}
	}

	public void device_managementAddress(String existingmanagementAddress, String newmanagementAddress,
			String managementAddress) throws InterruptedException, IOException {

		boolean managementaddresdropdown = false;
		boolean manageAddresstextField = false;
		String manageAddresspopulatedValue = "null";

		if ((existingmanagementAddress.equalsIgnoreCase("Yes")) & (newmanagementAddress.equalsIgnoreCase("No"))) {
			try {
				managementaddresdropdown = isElementPresent(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddressdropdown);
				sa.assertTrue(managementaddresdropdown,
						"Management Address dropdown under add device is not available");

				click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_getSubnetbutton);
				waitforPagetobeenable();
				manageAddresspopulatedValue = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddressdropdown, "value");
				if (manageAddresspopulatedValue.equalsIgnoreCase("null")) {
					// " No values gets populates in 'Manage Address' dropdown,
					// on clicking 'get Subnets' button
				} else {
					// Values displaying under 'Manage Addres' dropodwn is : "+
					// manageAddresspopulatedValue);
				}
				click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddressdropdown);
				waitforPagetobeenable();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			else if((existingmanagementAddress.equalsIgnoreCase("No")) & (newmanagementAddress.equalsIgnoreCase("Yes"))) {
	   			
	   			click(Lanlink_Metro_Obj.Lanlink_Metro.changeButton_managementAddress, "change");	//click on change button for getting management address text field
	   			waitforPagetobeenable();
				
	   		try {	
	   			manageAddresstextField=isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress);
	   			sa.assertTrue(manageAddresstextField, "Manage Address text Field is not displaying in 'Add CPE Device' page");
	   			if(manageAddresstextField) {
	   					if(managementAddress.equalsIgnoreCase("null")) {
	   						Report.LogInfo("Info", "No values has been passed for Mandatory field 'Manage Address' for adding device","FAIL");
	   					}else {
	   						
	   						
	   				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress,managementAddress,"management address"); 
	   				waitforPagetobeenable();
	   				Report.LogInfo("Info", managementAddress + " is the value passed for Mandatory 'Management Address' field in 'Add CPE Device' page","PASS");
	   					}
	   			}else {
	   				Report.LogInfo("Info", " 'Management Address' text field is not dipslaying under 'Add CPE device' page","FAIL");
	   			}
	   				
	   		}catch(NoSuchElementException e) {
	   			e.printStackTrace();
	   			Report.LogInfo("Info", " 'Manage Address' text field is not available in 'Add CPE Device' page","FAIL");
	   		}catch(Exception err) {
	   			err.printStackTrace();
	   			Report.LogInfo("Info", "Not able to enter value inside the 'Manage Address' field","FAIL");
	   		}
		}
	}

	public void eDITCPEdevicedetailsentered_1G_Accedian(String cpedevicename, String vender, String snmpro,
			String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
			String serialNumber, String hexaSerialnumber, String linkLostForwarding, String Country,
			String ExistingCitySelection, String NewCitySelection, String existingCity, String newCityName,
			String newCityCode, String ExistingSiteSelection, String NewSiteSelection, String ExistingSite,
			String NewSiteName, String NewSiteCode, String ExistingPremiseSelection, String newPremiseselection,
			String existingPremise, String newPremiseName, String newPremiseCode)
			throws InterruptedException, IOException {

		Reporter.log("Entered edit functionalitty");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevicelinkunderviewpage);
		waitforPagetobeenable();
		Reporter.log("edit functionality worked");

		// Name field
		device_editnamefield(cpedevicename);

		// vendor/model
		device_editVendorModelField(vender);

		// Snmpro
		device_editSnmproField();

		// Management address
		device_editManagementAddressField(managementAddress);

		// Mepid
		device_editMEPIdField(Mepid);

		// power alarm
		device_editPowerAlarm(poweralarm);

		// Serial Number
		device_editserialnumber(serialNumber);

		// linklost forwarding
		// device_editlinkLostforwarding(linkLostForwarding);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// Country
		if (!Country.equalsIgnoreCase("Null")) {

			click(Lanlink_Metro_Obj.Lanlink_Metro.countryDropdown_selectTag, Country);

			// New City
			if (ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
				click_addToggleButton(Lanlink_Metro_Obj.Lanlink_Metro.addcityswitch);
				click(Lanlink_Metro_Obj.Lanlink_Metro.addcityswitch);
				// City name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citynameinputfield, newCityName, "City Name");
				// City Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citycodeinputfield, newCityCode, "City Code");
				// Site name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addCityToggleSelected, NewSiteName,
						"Site Name");
				// Site Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addCityToggleSelected, NewSiteCode,
						"Site Code");
				// Premise name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addCityToggleSelected, newPremiseName,
						"Premise Name");
				// Premise Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addCityToggleSelected, newPremiseCode,
						"Premise Code");

			}

			// Existing City
			else if (ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.cityDropdown_selectTag, existingCity);

				// Existing Site
				if (ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
					click(Lanlink_Metro_Obj.Lanlink_Metro.siteDropdown_selectTag, ExistingSite);

					// Existing Premise
					if (ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
						click(Lanlink_Metro_Obj.Lanlink_Metro.premiseDropdown_selectTag, existingPremise);

					}

					// New Premise
					else if (ExistingPremiseSelection.equalsIgnoreCase("no")
							& newPremiseselection.equalsIgnoreCase("yes")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.addpremiseswitch);
						// Premise name
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addPremiseToggleSelected,
								newPremiseName, "Premise Name");
						// Premise Code
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addPremiseToggleSelected,
								newPremiseCode, "Premise Code");
					}
				}

				else if (ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.addsiteswitch);
					// Site name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addSiteToggleSelected, NewSiteName,
							"Site Name");
					// Site Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addSiteToggleSelected, NewSiteCode,
							"Site Code");

					// Premise name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addSiteToggleSelected, newPremiseName,
							"Premise Name");
					// Premise Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addSiteToggleSelected, newPremiseCode,
							"Premise Code");
				}
			}

		} else if (Country.equalsIgnoreCase("Null")) {

			// City
			editCity(ExistingCitySelection, NewCitySelection, "cityDropdown_selectTag", "selectcityswitch",
					"addcityswitch", existingCity, newCityName, newCityCode, "City"); // xpath
																						// -
																						// cityDropdown_selectTag

			// Site
			editSite(ExistingSiteSelection, NewSiteSelection, "siteDropdown_selectTag", "selectsiteswitch",
					"addsiteswitch", ExistingSite, NewSiteName, NewSiteCode, "Site"); // xpath
																						// -
																						// siteDropdown_selectTag

			// Premise
			editPremise(ExistingPremiseSelection, newPremiseselection, "premiseDropdown_selectTag",
					"selectpremiseswitch", "addpremiseswitch", existingPremise, newPremiseName, newPremiseCode,
					"Premise"); // xpath - premiseDropdown_selectTag

		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void eDITCPEdevicedetailsentered_1G_Overture(String cpedevicename, String vender, String snmpro,
			String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
			String serialNumber, String hexaSerialnumber, String linkLostForwarding, String Country,
			String ExistingCitySelection, String NewCitySelection, String existingCity, String newCityName,
			String newCityCode, String ExistingSiteSelection, String NewSiteSelection, String ExistingSite,
			String NewSiteName, String NewSiteCode, String ExistingPremiseSelection, String newPremiseselection,
			String existingPremise, String newPremiseName, String newPremiseCode, String technologySelected_SiteOrder)
			throws InterruptedException, IOException {

		Reporter.log("Entered edit functionalitty");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevicelinkunderviewpage);
		waitforPagetobeenable();
		Reporter.log("edit functionality worked");

		// Name field
		device_editnamefield(cpedevicename);

		// vendor/model
		device_editVendorModelField(vender);

		// Snmpro
		device_editSnmproField();

		// Management address
		device_editManagementAddressField(managementAddress);

		// Mepid
		device_editMEPIdField(Mepid);

		// power alarm
		device_editPowerAlarm(poweralarm);

		// Serial Number
		device_editserialnumber(serialNumber);

		// Media Selection
		if (technologySelected_SiteOrder.equals("Accedian-1G")) {
			Reporter.log("Media Selection field will not display if Site Order technology selected as 'Accedian-1G'");
		} else {
			device_editMediaselection(Mediaselection);
		}

		// Mac address
		device_editMACaddress(Macaddress);

		// linklost forwarding
		// device_editlinkLostforwarding(testDataFile, sheetName, scriptNo,
		// dataSetNo, linkLostForwarding);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// Country
		if (!Country.equalsIgnoreCase("Null")) {

			click(Lanlink_Metro_Obj.Lanlink_Metro.countryDropdown_selectTag, Country);

			// New City
			if (ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
				click_addToggleButton(Lanlink_Metro_Obj.Lanlink_Metro.addcityswitch);
				click(Lanlink_Metro_Obj.Lanlink_Metro.addcityswitch);
				// City name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citynameinputfield, newCityName, "City Name");
				// City Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citycodeinputfield, newCityCode, "City Code");
				// Site name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addCityToggleSelected, NewSiteName,
						"Site Name");
				// Site Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addCityToggleSelected, NewSiteCode,
						"Site Code");
				// Premise name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addCityToggleSelected, newPremiseName,
						"Premise Name");
				// Premise Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addCityToggleSelected, newPremiseCode,
						"Premise Code");

			}

			// Existing City
			else if (ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.cityDropdown_selectTag, existingCity);

				// Existing Site
				if (ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
					click(Lanlink_Metro_Obj.Lanlink_Metro.siteDropdown_selectTag, ExistingSite);

					// Existing Premise
					if (ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
						click(Lanlink_Metro_Obj.Lanlink_Metro.premiseDropdown_selectTag, existingPremise);

					}

					// New Premise
					else if (ExistingPremiseSelection.equalsIgnoreCase("no")
							& newPremiseselection.equalsIgnoreCase("yes")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.addpremiseswitch);
						// Premise name
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addPremiseToggleSelected,
								newPremiseName, "Premise Name");
						// Premise Code
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addPremiseToggleSelected,
								newPremiseCode, "Premise Code");
					}
				}

				else if (ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.addsiteswitch);
					// Site name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addSiteToggleSelected, NewSiteName,
							"Site Name");
					// Site Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addSiteToggleSelected, NewSiteCode,
							"Site Code");

					// Premise name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addSiteToggleSelected, newPremiseName,
							"Premise Name");
					// Premise Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addSiteToggleSelected, newPremiseCode,
							"Premise Code");
				}
			}

		} else if (Country.equalsIgnoreCase("Null")) {

			// City
			editCity(ExistingCitySelection, NewCitySelection, "cityDropdown_selectTag", "selectcityswitch",
					"addcityswitch", existingCity, newCityName, newCityCode, "City"); // xpath
																						// -
																						// cityDropdown_selectTag

			// Site
			editSite(ExistingSiteSelection, NewSiteSelection, "siteDropdown_selectTag", "selectsiteswitch",
					"addsiteswitch", ExistingSite, NewSiteName, NewSiteCode, "Site"); // xpath
																						// -
																						// siteDropdown_selectTag

			// Premise
			editPremise(ExistingPremiseSelection, newPremiseselection, "premiseDropdown_selectTag",
					"selectpremiseswitch", "addpremiseswitch", existingPremise, newPremiseName, newPremiseCode,
					"Premise"); // xpath - premiseDropdown_selectTag

		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void eDITCPEdevicedetailsentered_MSPselected_Accedian(String cpedevicename, String vender, String snmpro,
			String managementAddress, String Mepid, String Country, String ExistingCitySelection,
			String NewCitySelection, String existingCity, String newCityName, String newCityCode,
			String ExistingSiteSelection, String NewSiteSelection, String ExistingSite, String NewSiteName,
			String NewSiteCode, String ExistingPremiseSelection, String newPremiseselection, String existingPremise,
			String newPremiseName, String newPremiseCode, String VLANId) throws InterruptedException, IOException {

		Reporter.log("Entered edit functionalitty");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		click(Lanlink_Metro_Obj.Lanlink_Metro.viewPCEdevice_Actiondropdown, "Action");

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.EditCPEdevicelinkunderviewpage);
		waitforPagetobeenable();
		Reporter.log("edit functionality worked");

		// Name field
		device_editnamefield(cpedevicename);

		// vendor/model
		device_editVendorModelField(vender);

		// Snmpro
		device_editSnmproField();

		// Management address
		device_editManagementAddressField(managementAddress);

		// Mepid
		device_editMEPIdField(Mepid);

		// VLAN Id
		device_editVLANIdField(VLANId);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// Country
		if (!Country.equalsIgnoreCase("Null")) {

			click(Lanlink_Metro_Obj.Lanlink_Metro.countryDropdown_selectTag, Country);

			// New City
			if (ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
				click_addToggleButton(Lanlink_Metro_Obj.Lanlink_Metro.addcityswitch);
				click(Lanlink_Metro_Obj.Lanlink_Metro.addcityswitch);
				// City name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citynameinputfield, newCityName, "City Name");
				// City Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citycodeinputfield, newCityCode, "City Code");
				// Site name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addCityToggleSelected, NewSiteName,
						"Site Name");
				// Site Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addCityToggleSelected, NewSiteCode,
						"Site Code");
				// Premise name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addCityToggleSelected, newPremiseName,
						"Premise Name");
				// Premise Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addCityToggleSelected, newPremiseCode,
						"Premise Code");

			}

			// Existing City
			else if (ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.cityDropdown_selectTag, existingCity);

				// Existing Site
				if (ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
					click(Lanlink_Metro_Obj.Lanlink_Metro.siteDropdown_selectTag, ExistingSite);

					// Existing Premise
					if (ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
						click(Lanlink_Metro_Obj.Lanlink_Metro.premiseDropdown_selectTag, existingPremise);

					}

					// New Premise
					else if (ExistingPremiseSelection.equalsIgnoreCase("no")
							& newPremiseselection.equalsIgnoreCase("yes")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.addpremiseswitch);
						// Premise name
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addPremiseToggleSelected,
								newPremiseName, "Premise Name");
						// Premise Code
						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addPremiseToggleSelected,
								newPremiseCode, "Premise Code");
					}
				}

				else if (ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.addsiteswitch);
					// Site name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addSiteToggleSelected, NewSiteName,
							"Site Name");
					// Site Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addSiteToggleSelected, NewSiteCode,
							"Site Code");

					// Premise name
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addSiteToggleSelected, newPremiseName,
							"Premise Name");
					// Premise Code
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addSiteToggleSelected, newPremiseCode,
							"Premise Code");
				}
			}

		} else if (Country.equalsIgnoreCase("Null")) {

			// City
			editCity(ExistingCitySelection, NewCitySelection, "cityDropdown_selectTag", "selectcityswitch",
					"addcityswitch", existingCity, newCityName, newCityCode, "City"); // xpath
																						// -
																						// cityDropdown_selectTag

			// Site
			editSite(ExistingSiteSelection, NewSiteSelection, "siteDropdown_selectTag", "selectsiteswitch",
					"addsiteswitch", ExistingSite, NewSiteName, NewSiteCode, "Site"); // xpath
																						// -
																						// siteDropdown_selectTag

			// Premise
			editPremise(ExistingPremiseSelection, newPremiseselection, "premiseDropdown_selectTag",
					"selectpremiseswitch", "addpremiseswitch", existingPremise, newPremiseName, newPremiseCode,
					"Premise"); // xpath - premiseDropdown_selectTag

		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void verifySiteOrderForE_PN(String interfaceSpeed, String modularMSP)
			throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			// CSR name Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_csrnameErrmsg, "CSR Name");

			// Technology Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitieorder_technologyErrmsg, "Technology");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

			// Country Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_countryerrmsg, "Country");

			// City Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_devciexngCityErrmsg, "City");

			// Validate site order Number Field
			validatesiteOrderNumber_AddSiteOrder();

			// Validate Country dropdown
			validateCountry_AddSiteorder();

			// Validate City Fields
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			validateSite_AddSiteOrder();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			validateSmartsMOnitoring_AddSiteOrder();

			// Validate IV Reference dropdown
			validateIVReference_AddSiteorder();

			// Validate Circuit reference Field
			validateCircuitreference_AddSiteOrder();

			// Validate EPN Offnet checkbox
			validateEPNoffnet_AddSiteOrder();

			// validate EPN EOSDH checkbox
			if (modularMSP.equalsIgnoreCase("yes")) {

				validateEPNEOSDH_AddSiteOrder();

			} else {
				if (interfaceSpeed.equals("1GigE")) {

					validateEPNEOSDH_AddSiteOrder();

				} else {
					Reporter.log(" 'EPN EOSDH' checkbox does not display for 10G interface speed");
				}
			}

			// Validate Site Alias field
			validateSiteAlias_AddSiteOrder();

			// Verify Remark field
			validateRemark_AddSiteOrder();

			if (modularMSP.equalsIgnoreCase("yes")) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

						technologyDropdown_MSPselected_Primary();
					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

						// Access Circuit ID
						validateAccessCircuitID_AddSiteOrder();

						// Circuit Reference
						circuitreferenceDisabled_EPN();

						technologyDropdown_MSPselected_EPN_Access();
					}
				}

			} else {
				if ((interfaceSpeed.equals("1GigE"))) {

					for (int i = 0; i < IVrefrence.length; i++) {
						if (IVrefrence[i].equals("Primary")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
							waitforPagetobeenable();

							technologyDropdownFor1GigE_EPN_Primary();
						}

						else if (IVrefrence[i].equals("Access")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
							waitforPagetobeenable();

							technologyDropdownFor1GigE_EPN_Access();
						}
					}
				}

				else if (interfaceSpeed.equals("10GigE")) {

					for (int i = 0; i < IVrefrence.length; i++) {
						if (IVrefrence[i].equals("Primary")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
							waitforPagetobeenable();

							technologyDropdownFor10GigE_HubAndSpoke_primary();

						}

						else if (IVrefrence[i].equals("Access")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
							waitforPagetobeenable();

							technologyDropdownFor10GigE_HubAndSpoke_Access();
						}
					}
				}

			}

			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
			waitforPagetobeenable();

			sa.assertAll();

		} catch (AssertionError e) {
			e.printStackTrace();
		}
	}

	public void verifySiteOrderForEPN_EOSDHselected(String interfaceSpeed, String modularMSP)
			throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			// CSR name Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_csrnameErrmsg, "CSR Name");

			// Technology Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitieorder_technologyErrmsg, "Technology");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

			// Country Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_countryerrmsg, "Country");

			// City Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_devciexngCityErrmsg, "City");

			// Validate site order Number Field
			validatesiteOrderNumber_AddSiteOrder();

			// Validate Country dropdown
			validateCountry_AddSiteorder();

			// Validate City Fields
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			validateSite_AddSiteOrder();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			validateSmartsMOnitoring_AddSiteOrder();

			// Validate IV Reference dropdown
			validateIVReference_AddSiteorder();

			// Validate Circuit reference Field
			validateCircuitreference_AddSiteOrder();

			// Validate EPN Offnet checkbox
			validateEPNoffnet_AddSiteOrder();

			// validate EPN EOSDH checkbox
			if (modularMSP.equalsIgnoreCase("yes")) {

				validateEPNEOSDH_AddSiteOrder();

			} else {
				if (interfaceSpeed.equals("1GigE")) {

					validateEPNEOSDH_AddSiteOrder();

				} else {
					Reporter.log(" 'EPN EOSDH' checkbox does not display for 10G interface speed");
				}
			}

			// Validate Site Alias field
			validateSiteAlias_AddSiteOrder();

			// Verify Remark field
			validateRemark_AddSiteOrder();

			if (modularMSP.equalsIgnoreCase("yes")) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

						technologyDropdown_MSPselected_Primary();
					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

						// Access Circuit ID
						validateAccessCircuitID_AddSiteOrder();

						// Circuit Reference
						circuitreferenceDisabled_EPN();

						technologyDropdown_MSPselected_EPN_Access();
					}
				}

			} else {
				if ((interfaceSpeed.equals("1GigE"))) {

					for (int i = 0; i < IVrefrence.length; i++) {
						if (IVrefrence[i].equals("Primary")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNEOSDHCheckbox);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
							waitforPagetobeenable();
							technologyDropdownFor1GigE_EPNEOSDHselected_Primary();
						}

						else if (IVrefrence[i].equals("Access")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNEOSDHCheckbox);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
							waitforPagetobeenable();
							technologyDropdownFor1GigE_EPNEOSDHselected_Access();
						}
					}
				}

				// Validate OK button
				OKbutton_AddSiteOrder();

				// validate cancel button
				cancelbutton_AddSiteOrder();

				waitforPagetobeenable();
				((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
				waitforPagetobeenable();

				sa.assertAll();

			}
		} catch (AssertionError e) {
			e.printStackTrace();
		}
	}

	public void verifySiteOrderForHubAndSpoke(String interfaceSpeed, String modularMSP)
			throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			// CSR name Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_csrnameErrmsg, "CSR Name");

			// Technology Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitieorder_technologyErrmsg, "Technology");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

			// Country Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_countryerrmsg, "Country");

			// City Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_devciexngCityErrmsg, "City");

			// Validate site order Number Field
			validatesiteOrderNumber_AddSiteOrder();

			// Validate Country dropdown
			validateCountry_AddSiteorder();

			// Validate City Fields
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			validateSite_AddSiteOrder();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			validateSmartsMOnitoring_AddSiteOrder();

			// Validate IV Reference dropdown
			validateIVReference_AddSiteorder();

			// Validate Circuit reference Field
			validateCircuitreference_AddSiteOrder();

			// Validate EPN Offnet checkbox
			validateEPNoffnet_AddSiteOrder();

			// validate EPN EOSDH checkbox
			if (modularMSP.equalsIgnoreCase("yes")) {

				validateEPNEOSDH_AddSiteOrder();

			} else {
				Reporter.log("Spoke id is not displaying, if modular msp is selected");
			}

			// Validate Site Alias field
			validateSiteAlias_AddSiteOrder();

			// Verify Remark field
			validateRemark_AddSiteOrder();

			//// scrolltoend();
			waitforPagetobeenable();
			if (modularMSP.equalsIgnoreCase("yes")) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

						technologyDropdown_mspSelected_HubAndSpoke_Primary();
					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

						technologyDropdown_MSPselected_HubAndSpoke_Access();
					}
				}

			} else {
				if ((interfaceSpeed.equals("1GigE"))) {

					for (int i = 0; i < IVrefrence.length; i++) {
						if (IVrefrence[i].equals("Primary")) {

							// Validate Offnet checkbox
							validateoffnet_AddSiteOrder();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
							waitforPagetobeenable();

							technologyDropdownFor1GigE_HubAndSpoke_Primary();
						}

						else if (IVrefrence[i].equals("Access")) {
							// Validate Offnet checkbox
							validateoffnet_AddSiteOrder();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
							waitforPagetobeenable();

							technologyDropdownFor1GigE_HubAndSpoke_Access();
						}
					}
				}

				else if (interfaceSpeed.equals("10GigE")) {

					for (int i = 0; i < IVrefrence.length; i++) {
						if (IVrefrence[i].equals("Primary")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
							waitforPagetobeenable();

							technologyDropdownFor10GigE_HubAndSpoke_primary();

						}

						else if (IVrefrence[i].equals("Access")) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
							waitforPagetobeenable();

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
							waitforPagetobeenable();

							technologyDropdownFor10GigE_HubAndSpoke_Access();
						}
					}
				}

			}

			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
			waitforPagetobeenable();

			// sa.assertAll();
		}

		catch (AssertionError e) {
			e.printStackTrace();
		}
	}

	public void verifySiteOrderForHubAndSpoke_MSPselectedAndoffnet(String interfaceSpeed, String modularMSP)
			throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			// CSR name Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_csrnameErrmsg, "CSR Name");

			// Technology Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitieorder_technologyErrmsg, "Technology");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

			// Country Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_countryerrmsg, "Country");

			// City Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_devciexngCityErrmsg, "City");

			// Validate site order Number Field
			validatesiteOrderNumber_AddSiteOrder();

			// Validate Country dropdown
			validateCountry_AddSiteorder();

			// Validate City Fields
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			validateSite_AddSiteOrder();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			validateSmartsMOnitoring_AddSiteOrder();

			// Validate IV Reference dropdown
			validateIVReference_AddSiteorder();

			// Validate Circuit reference Field
			validateCircuitreference_AddSiteOrder();

			// Validate Site Alias field
			validateSiteAlias_AddSiteOrder();

			// Verify Remark field
			validateRemark_AddSiteOrder();

			if (modularMSP.equalsIgnoreCase("yes")) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Primary);
						waitforPagetobeenable();

						technologyDropdown_mspSelected_HubAndSpoke_Primary();
					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();

						// Access Circuit ID
						validateAccessCircuitID_AddSiteOrder();

						// Circuit Reference
						circuitreferenceDisabled_EPN();

						technologyDropdown_MSPselected_HubAndSpoke_Access();
					}
				}

			}

			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
			waitforPagetobeenable();

			sa.assertAll();

		} catch (AssertionError e) {
			e.printStackTrace();
		}
	}

	public void verifySiteOrderForHubAndSpoke_offnetSelected(String interfaceSpeed, String modularMSP)
			throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			// CSR name Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_csrnameErrmsg, "CSR Name");

			// Technology Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitieorder_technologyErrmsg, "Technology");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

			// Country Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_countryerrmsg, "Country");

			// City Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_devciexngCityErrmsg, "City");

			// Validate site order Number Field
			validatesiteOrderNumber_AddSiteOrder();

			// Validate Country dropdown
			validateCountry_AddSiteorder();

			// Validate City Fields
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			validateSite_AddSiteOrder();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			validateSmartsMOnitoring_AddSiteOrder();

			// Validate IV Reference dropdown
			validateIVReference_AddSiteorder();

			// Validate Circuit reference Field
			validateCircuitreference_AddSiteOrder();

			// Spoke ID
			validatespokeId_AddSiteOrder();

			// Validate Site Alias field
			validateSiteAlias_AddSiteOrder();

			// Verify Remark field
			validateRemark_AddSiteOrder();

			// Validate Offnet checkbox
			validateoffnet_AddSiteOrder();

			if (interfaceSpeed.equals("1GigE")) {

				validateEPNEOSDH_AddSiteOrder();

			} else {
				Reporter.log(" 'EPN EOSDH' checkbox does not display for 10G interface speed");
			}

			// Validate Site Alias field
			validateSiteAlias_AddSiteOrder();

			// Verify Remark field
			validateRemark_AddSiteOrder();

			// Validate Offnet checkbox
			validateoffnet_AddSiteOrder();

			if ((interfaceSpeed.equals("1GigE"))) {

				for (int i = 0; i < IVrefrence.length; i++) {
					if (IVrefrence[i].equals("Primary")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();
						technologyDropdownFor1GigE_HubAndSpoke_Primary_offnetselected("Primary");
					}

					else if (IVrefrence[i].equals("Access")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreference_Access);
						waitforPagetobeenable();
						technologyDropdownFor1GigE_HubAndSpoke_Access_offnetselected("Access");
					}
				}
			}

			else if (interfaceSpeed.equals("10GigE")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox);

			}

			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
			waitforPagetobeenable();

			sa.assertAll();

		} catch (AssertionError e) {
			e.printStackTrace();
		}
	}

	public void verifySiteOrderForPoint_to_point(String interfaceSpeed, String modularMSP)
			throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Country Error message
						verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_countryerrmsg, "Country");

						// City Error message
						verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_devciexngCityErrmsg, "City");

						// CSR name Error message
						verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_csrnameErrmsg, "CSR Name");

						// Technology Error message
						verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitieorder_technologyErrmsg, "Technology");

		/*	// Circuit Reference Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");
*/
			
			// Validate site order Number Field
			// -- validatesiteOrderNumber_AddSiteOrder();

			// Validate Country dropdown
			validateCountry_AddSiteorder();

			// Validate City Fields
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			validateSite_AddSiteOrder();

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			validateSmartsMOnitoring_AddSiteOrder();

			// Validate Site Alias field
			Reporter.log("validate Site Alias fields");
			validateSiteAlias_AddSiteOrder();

			// Validate VLAN Id field
			Reporter.log("validate VLAn Id fields");
			validateVlanID_AddSiteOrder();

			// Validate DCA Enabled Site and Cloud Service Provider dropdown
			Reporter.log("validate DCA enabled site checkbox");
			valiadateDCAEnabledsite_AddSieOrder();

			// Verify Remark field
			Reporter.log("validate Remark fields");
			validateRemark_AddSiteOrder();
			if (modularMSP.equalsIgnoreCase("yes")) {

				technologyDropdown_p2p_mspselected();

			} else {
				if (interfaceSpeed.equals("1GigE")) {

					technologyDropdownFor1GigE();

				} else if (interfaceSpeed.equals("10GigE")) {

					technologyDropdownFor10GigE();
				}
			}

			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			//cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
			

			sa.assertAll();

		} catch (AssertionError e) {
			e.printStackTrace();
		}
	}

	public void verifySiteOrderForPoint_to_point_extendedCircuit

	(String interfaceSpeed, String modularMSP) throws InterruptedException, IOException {

		try {

			String[] IVrefrence = { "Primary", "Access" };

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceErrmsg, "Site Order Number");

			// IV Reference Error Messages
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVReferenceErrmsg, "IV Reference");

			// CSR name Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_csrnameErrmsg, "CSR Name");

			// Technology Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitieorder_technologyErrmsg, "Technology");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
			waitforPagetobeenable();

			// Site Order Number Error Message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitOrderNumberErrmsg, "Site Order Number");

			// Country Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_countryerrmsg, "Country");

			// City Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_devciexngCityErrmsg, "City");

			// Validate Country dropdown
			Reporter.log("validate Country dropdown");
			validateCountry_AddSiteorder();

			// Validate City Fields
			Reporter.log("Validate city fields");
			validateCity_AddSiteOrder();

			// Validate Site/CSR field
			Reporter.log("validate Site Fields");
			validateSite_AddSiteOrder();

			//// scrolltoend();
			waitforPagetobeenable();

			// Validate performance reporting dropdown
			Reporter.log("validate performance reporting checkbox");
			validatePerformancereporting_AddSiteOrder();

			// validate proactive Monitoring dropdown
			Reporter.log("validate proactive monitoring checkbox");
			validateProactiveMonitoring_AddSiteOrder();

			// Validate Smarts monitoring dropdown
			Reporter.log("validate Smarts monitoring checkbox");
			validateSmartsMOnitoring_AddSiteOrder();

			//// scrolltoend();
			waitforPagetobeenable();

			// Validate Site Alias field
			Reporter.log("validate Site Alias fields");
			validateSiteAlias_AddSiteOrder();

			// Validate VLAN Id field
			Reporter.log("validate VLAn Id fields");
			validateVlanID_AddSiteOrder();

			// Validate DCA Enabled Site and Cloud Service Provider dropdown
			Reporter.log("validate DCA enabled site checkbox");
			valiadateDCAEnabledsite_AddSieOrder();

			// Verify Remark field
			Reporter.log("validate Remark fields");
			validateRemark_AddSiteOrder();

			if (interfaceSpeed.equals("1GigE")) {

				technologyDropdownFor1GigE();
			}

			else if (interfaceSpeed.equals("10GigE")) {

				technologyDropdownFor10GigE();
			}
			// Validate OK button
			OKbutton_AddSiteOrder();

			// validate cancel button
			cancelbutton_AddSiteOrder();

			waitforPagetobeenable();
			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel,"Cancel");
			waitforPagetobeenable();

			sa.assertAll();

		} catch (AssertionError e) {
			e.printStackTrace();
		}
	}

	public void editSiteOrder_EPN_1G_IVRefPrimary(String performReport, String ProactiveMonitor, String smartmonitor,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
			String nontermination, String Protected, String devicename, String remark, String siteOrderNumber,
			String IVReference, String CircuitReference, String EPNoffnet, String EPnEosDH)
			throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Site alias
		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("Site alias field will not display for 'Belgacom VDSL' technology");
		} else {
			editsiteorder_sitealias(siteallias);
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(CircuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Overture")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Alu")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if ((technology.equalsIgnoreCase("Accedian-1G")) || (technology.equalsIgnoreCase("Accedian-1G"))) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("No additonal fields displays");
		}

		// EPN Offnet
		editSiteorder_Offnet(EPNoffnet);

		// EPN EOSDH
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNEOSDHCheckbox, EPnEosDH, "EPN EOSDH");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void editSiteOrder_EPN_MSPselected_IVRefAccess(String performReport, String ProactiveMonitor,
			String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
			String technology, String nontermination, String Protected, String devicename, String remark,
			String siteOrderNumber, String IVReference, String circuitReference, String GCRoloType, String VLAN,
			String VlanEtherType, String primaryVLAN, String primaryVlanEtherType, String offnet, String mappingMode,
			String portBased, String vlanBased, String maapingModeAddedValue) throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Site alias

		editsiteorder_sitealias(siteallias);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(circuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

			// Mapping Mode
			editSiteOrder_mappingMode(mappingMode, portBased, vlanBased);

			// Mapping Mode
			editSiteOrder_mappingMode(mappingMode, portBased, vlanBased);

			// validation --> sometime mapping mode dropdown will not edited,
			// port or Vlan fields will be edited
			if ((maapingModeAddedValue.equalsIgnoreCase("Port Based")) && (mappingMode.equalsIgnoreCase("null"))) {

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.portname_textField, portBased, "Port Name");

			} else if ((maapingModeAddedValue.equalsIgnoreCase("Vlan Based"))
					&& (mappingMode.equalsIgnoreCase("null"))) {

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.vlanid_textfield, vlanBased, "VLAN Id");
			}
		}

		if (technology.equalsIgnoreCase("Overture")) {

			// GCR OLO Type
			editSiteOrder_GCRoloType(GCRoloType);

			// VLAN
			editsiteorder_VLAN(VLAN);

			// VLAN Ether Type
			editSiteOrder_VLANEtherType(VlanEtherType);

		}

		if (technology.equalsIgnoreCase("Accedian")) {

			Reporter.log("No additonal fields displays");
		}

		if (technology.equalsIgnoreCase("Cyan")) {

			Reporter.log("NO additonal field display");
		}
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void editSiteOrder_EPN_MSPselected_IVRefPrimary(String performReport, String ProactiveMonitor,
			String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
			String technology, String nontermination, String Protected, String devicename, String remark,
			String siteOrderNumber, String IVReference, String CircuitReference, String EPNoffnet, String EPnEosDH)
			throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Site alias

		editsiteorder_sitealias(siteallias);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(CircuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Overture")) {

			Reporter.log("No additonal fields display");
		}

		if (technology.equalsIgnoreCase("Accedian")) {

			Reporter.log("No additonal fields displays");
		}

		if (technology.equalsIgnoreCase("Cyan")) {

			Reporter.log("NO additonal field display");
		}

		// EPN Offnet
		editSiteorder_Offnet(EPNoffnet);

		// EPN EOSDH
		editcheckbox_commonMethod(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNEOSDHCheckbox, "EPN EOSDH", EPnEosDH);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void editSiteOrder_HubAndSpoke_10G(String performReport, String ProactiveMonitor, String smartmonitor,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
			String nontermination, String Protected, String devicename, String remark, String siteOrderNumber,
			String IVReference, String CircuitReference, String offnet) throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Site alias

		editsiteorder_sitealias(siteallias);

		// offnet
		editSiteorder_Offnet(offnet);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(CircuitReference);

		// Technology
		editSiteOrder_technology(technology);

		// Non-termination point
		editsiteorder_NonterminationPoint(nontermination);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void editSiteOrder_HubAndSpoke_1G_IVRefAccess(String performReport, String ProactiveMonitor,
			String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
			String technology, String nontermination, String Protected, String devicename, String remark,
			String siteOrderNumber, String IVReference, String circuitReference, String GCRoloType, String VLAN,
			String VlanEtherType, String primaryVLAN, String primaryVlanEtherType, String offnet)
			throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {
			Reporter.log("No additional fields display, when 'Belgacom VDSL' technology is selected");

		} else {
			// Site alias

			editsiteorder_sitealias(siteallias);

		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Offnet
		editSiteorder_Offnet(offnet);

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(circuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Non-termination point

		}

		if (technology.equalsIgnoreCase("Overture")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// GCR OLO Type
			editSiteOrder_GCRoloType(GCRoloType);

			// VLAN
			editsiteorder_VLAN(VLAN);

			// VLAN Ether Type
			editSiteOrder_VLANEtherType(VlanEtherType);

			// Primary VLAN
			editsiteorder_primaryVLAN(primaryVLAN);

			// Primary VLAN Ether type
			editSiteOrder_primaryVLANEtherType(primaryVlanEtherType);

		}

		if (technology.equalsIgnoreCase("Alu")) {

			Reporter.log("No additional fields display for Alu Technology");
		}

		if ((technology.equals("Accedian")) || (technology.equals("Accedian-1G"))) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("NO additonal fields display");
		}
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void editSiteOrder_HubAndSpoke_1G_IVRefPrimary(String performReport, String ProactiveMonitor,
			String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
			String technology, String nontermination, String Protected, String devicename, String remark,
			String siteOrderNumber, String IVReference, String CircuitReference, String offnet)
			throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {
			Reporter.log("Site Alias field will not display for Belgacom technology");
		} else {
			// Site alias

			editsiteorder_sitealias(siteallias);

		}
		// Offnet
		editSiteorder_Offnet(offnet);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(CircuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Overture")) {
			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);
		}

		if (technology.equalsIgnoreCase("Alu")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if ((technology.equals("Accedian-1G"))) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("NO additonal fields displays");
		}
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void editSiteOrder_HubAndSpoke_MSPselected_IVRefAccess(String performReport, String ProactiveMonitor,
			String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
			String technology, String nontermination, String Protected, String devicename, String remark,
			String siteOrderNumber, String IVReference, String circuitReference, String GCRoloType, String VLAN,
			String VlanEtherType, String primaryVLAN, String primaryVlanEtherType, String offnet)
			throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Site alias

		editsiteorder_sitealias(siteallias);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		// Offnet
		editSiteorder_Offnet(offnet);

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(circuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			Reporter.log("NO additonal fields display");
		}

		if (technology.equalsIgnoreCase("Overture")) {

			// GCR OLO Type
			editSiteOrder_GCRoloType(GCRoloType);

			// VLAN
			editsiteorder_VLAN(VLAN);

			// VLAN Ether Type
			editSiteOrder_VLANEtherType(VlanEtherType);

			// Primary VLAN
			editsiteorder_primaryVLAN(primaryVLAN);

			// Primary VLAN Ether type
			editSiteOrder_primaryVLANEtherType(primaryVlanEtherType);

		}

		if ((technology.equals("Accedian"))) {

			Reporter.log("No additonal fields display");
		}

		if (technology.equalsIgnoreCase("Cyan")) {

			Reporter.log("No additonal fields display");
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
	}

	public void editSiteOrder_HubAndSpoke_MSPselected_IVRefPrimary(String performReport, String ProactiveMonitor,
			String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
			String technology, String nontermination, String Protected, String devicename, String remark,
			String siteOrderNumber, String IVReference, String CircuitReference, String offnet)
			throws InterruptedException, IOException {

		// Site Order Number (Siebel Service ID)
		editSiteOrder_siteOrderNumber(siteOrderNumber);

		// IV Reference
		editSiteOrder_IVReference(IVReference);

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Site alias
		editsiteorder_sitealias(siteallias);

		//// scrolltoend();
		waitforPagetobeenable();

		// Offnet
		editSiteorder_Offnet(offnet);

		// Remark
		editsiteOrder_remark(remark);

		// Circuit Reference
		editsiteorder_circuitReference(CircuitReference);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Overture")) {

			Reporter.log("NO additonal fields displays");
		}

		if ((technology.equals("Accedian"))) {

			Reporter.log("no additonal fields display");
		}

		if (technology.equalsIgnoreCase("Cyan")) {

			Reporter.log("No additonal fields display");
		}

		//// scrolltoend();
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void editSiteOrder_P2P_10G(String performReport, String ProactiveMonitor, String smartmonitor,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
			String nontermination, String Protected, String remark)
			throws InterruptedException, IOException, IOException {

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Vlan id
		editsiteOrder_vlanid(VLANid);

		// Site alias
		editsiteorder_sitealias(siteallias);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// DCA Enabled Site
		editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

		// Remark
		editsiteOrder_remark(remark);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Accedian")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}
		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void editSiteOrder_P2P_10G_extendedCircuit(String performReport, String ProactiveMonitor,
			String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
			String technology, String nontermination, String Protected, String remark,
			String siteOrder_sitePreferenceType) throws InterruptedException, IOException, IOException {

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Vlan id
		editsiteOrder_vlanid(VLANid);

		// Site alias
		editsiteorder_sitealias(siteallias);

		// Site Preference Type
		addDropdownValues_commonMethod("Site Preference Type",
				Lanlink_Metro_Obj.Lanlink_Metro.AddSiteOrder_sitePreferenceType_Dropodwn, siteOrder_sitePreferenceType);

		//// scrolltoend();
		waitforPagetobeenable();

		// DCA Enabled Site
		editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

		// Remark
		editsiteOrder_remark(remark);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Accedian")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}
		//// scrolltoend();
		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void editSiteOrder_P2P_1G(String performReport, String ProactiveMonitor, String smartmonitor,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
			String nontermination, String Protected, String devicename, String remark)
			throws InterruptedException, IOException {

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {
			Reporter.log("Site alias and VLAN id text field does not display for 'belgacom VDSL' technology");
		} else {

			// Vlan id
			editsiteOrder_vlanid(VLANid);

			// Site alias
			editsiteorder_sitealias(siteallias);
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// DCA Enabled Site
		editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

		// Remark
		editsiteOrder_remark(remark);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Overture")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}

		if (technology.equalsIgnoreCase("Alu")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Accedian-1G")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("NO additonal fields display");
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void editSiteOrder_P2P_1G_extendedCircuit(String performReport, String ProactiveMonitor, String smartmonitor,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
			String nontermination, String Protected, String devicename, String remark,
			String siteOrder_sitePreferenceType) throws InterruptedException, IOException {

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {
			Reporter.log("Site alias and VLAN id text field does not display for 'belgacom VDSL' technology");
		} else {

			// Vlan id
			editsiteOrder_vlanid(VLANid);

			// Site alias
			editsiteorder_sitealias(siteallias);
		}

		// Site Preference Type
		addDropdownValues_commonMethod("Site Preference Type",
				Lanlink_Metro_Obj.Lanlink_Metro.AddSiteOrder_sitePreferenceType_Dropodwn, siteOrder_sitePreferenceType);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// DCA Enabled Site
		editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

		// Remark
		editsiteOrder_remark(remark);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Overture")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}

		if (technology.equalsIgnoreCase("Alu")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Accedian-1G")) {

			// Non-termination point
			editsiteorder_NonterminationPoint(nontermination);

		}

		if (technology.equalsIgnoreCase("Belgacom VDSL")) {

			Reporter.log("NO additonal fields display");
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void editSiteOrder_P2P_MSPselected(String performReport, String ProactiveMonitor, String smartmonitor,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String technology,
			String nontermination, String Protected, String devicename, String remark)
			throws InterruptedException, IOException {

		// Performance Reporting
		editSiteOrder_Performancereporting(performReport);

		// Pro active Monitoring
		editSiteOrder_proactiveMonitoring(ProactiveMonitor);

		// Smart Monitoring
		editSiteOrder_smartMonitoring(smartmonitor);

		// Vlan id
		editsiteOrder_vlanid(VLANid);

		// Site alias
		editsiteorder_sitealias(siteallias);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();

		// DCA Enabled Site
		editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

		// Remark
		editsiteOrder_remark(remark);

		// Technology
		editSiteOrder_technology(technology);

		if (technology.equalsIgnoreCase("Actelis")) {
			Reporter.log(" NO additional fields display for technology Actelis");
		}

		if (technology.equalsIgnoreCase("Atrica")) {

			// Device Name
			editSiteOrder_deviceName(devicename);

		}

		if (technology.equalsIgnoreCase("Overture")) {

			Reporter.log("NO additonal fields required");
		}

		if (technology.equalsIgnoreCase("Accedian")) {

			Reporter.log("NO additonal fields displays");
		}

		if (technology.equalsIgnoreCase("Cyan")) {

			Reporter.log("No additonal fields displays");
		}

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void equip_adddevi_Accedian_MSPselected(String interfaceSpeed, String cpename, String vender, String snmpro,
			String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,
			String serialNumber, String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress,
			String existingmanagementAddress, String manageaddressdropdownvalue, String VLANid)
			throws InterruptedException, IOException, IOException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
		waitforPagetobeenable();
		try {

			String[] Vender = { "Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX",
					"Accedian 10GigE-MetroNode-CE-2Port", "Accedian 10GigE-MetroNode-CE-4Port", "Accedian Clipper LT",
					"Accedian Clipper LT-S", "Accedian MetroNID GT", "Accedian MetroNID GT-S",
					"Accedian MetroNID GX-S" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address dropdown
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress, managementAddress, "Management Address");
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// VLAN ID
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vlanId, VLANid, "VLAN Id");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			// OK button

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

			waitforPagetobeenable();

			

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			// Report.LogInfo("Info", "FAiled while verify the fields for Add
			// CPE device");

		}

	}

	public void equip_adddevi_Accedian1G(String interfaceSpeed, String cpename, String vender, String snmpro,
			String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,
			String serialNumber, String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress,
			String existingmanagementAddress, String manageaddressdropdownvalue)
			throws InterruptedException, IOException {

		//// scrolltoend();
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");

		try {

			String linklostForwardingcheckboxstate = "disabled";

			String[] Vender = { "Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX" };

			String[] powerAlarm = { "DC Single Power Supply - Feed A", "DC Dual Power Supply - Feed-A+B",
					"AC Single Power Supply - Feed A", "AC Dual Power Supply -Feed A+B" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

			String expectedValueForSnmpro = "JdhquA5";

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Power Alarm Error Message
			device_powerAlarmWarningMessage();

			// serial number Eror Message
			device_serialNumberWarningMessage();

			// Hexa serial Number
			device_hexaSerialNumberWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address dropdown
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// Power Alarm
			device_powerAlarm(poweralarm);

			// Serial Number
			device_serialNumber(serialNumber);

			// Link lost Forwarding
			device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

			waitforPagetobeenable();

			

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			// Report.LogInfo("Info", "FAiled while verify the fields for Add
			// CPE device");

		}

	}

	public void equip_addDevice_1G(String interfaceSpeed, String cpename, String vender, String snmpro,
			String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,
			String serialNumber, String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress,
			String existingmanagementAddress, String manageaddressdropdownvalue)
			throws InterruptedException, java.io.IOException {

		try {

			String linklostForwardingcheckboxstate = "enabled";

			String[] Vender = { "Overture ISG-26", "Overture ISG-26R", "Overture ISG-26S", "Overture ISG140",
					"Overture ISG180", "Overture ISG6000" };

			String[] powerAlarm = { "AC", "DC" };

			String[] Mediaselection = { "SFP-A with SFP-B", "RJ45-A with SFP-B" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

			String mepValue = "null";

			//// scrolltoend();
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Power Alarm Error Message
			device_powerAlarmWarningMessage();

			// Media Selection Error Message
			// device_mediaSelectionWarningMessage();

			// MAC Address Error Message
			device_macAddressWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address dropdown
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// Power Alarm
			device_powerAlarm(poweralarm);

			//// scrolltoend();
			waitforPagetobeenable();

			// Media Selection
			device_mediaSelection(Mediaselection, mediaSelection);

			// MAC Address
			device_MAcaddress(Macaddress);

			// Link lost Forwarding
			device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

			//// scrolltoend();
			waitforPagetobeenable();

			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

			waitforPagetobeenable();

		//	verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage1_devicename, "Device Name");
		//	verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage2_devicename, "Device Name");
		//	verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage3_devicename, "Device Name");

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			// Report.LogInfo("Info", "FAiled while verify the fields for Add
			// CPE device");

		}

	}

	public void equip_addDevice_MSPselected(String interfaceSpeed, String cpename, String vender, String snmpro,
			String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,
			String serialNumber, String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress,
			String existingmanagementAddress, String manageaddressdropdownvalue, String VLANid)
			throws InterruptedException, IOException {

		try {

			String[] Vender = { "Overture ISG-26", "Overture ISG-26R", "Overture ISG-26S", "Overture ISG140",
					"Overture ISG180", "Overture ISG6000" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address textfield
			sendKeys("Management Address", "AddCPEdevice_manageaddress", managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// VLAN ID
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vlanId, VLANid, "VLAN Id");

			waitforPagetobeenable();
			// OK button

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

			waitforPagetobeenable();

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage1_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage2_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage3_devicename, "Device Name");

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			// Report.LogInfo("Info", "FAiled while verify the fields for Add
			// CPE device");

		}

	}

	public void SelectInterfacetoaddwithservcie(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo)

			throws InterruptedException, IOException {

		String interfacenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Interfacetoselect_Interfacenumber");

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		selectrowforInterfaceToselecttable(interfacenumber);

	}

	public void SelectInterfacetoremovefromservice(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		String interfacename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Interfaceinservice_Interfacenumber");

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.labelname_managementAddresss);
		
		waitforPagetobeenable();

		selectRowforInterfaceInService(interfacename);

	}

	public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		// Report.LogInfo(LogStatus.INFO, "'Verifying Customer Creation
		// Functionality");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.ManageCustomerServiceLink, " Manage Customer Service Link");
		click(Lanlink_Metro_Obj.Lanlink_Metro.ManageCustomerServiceLink);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.createcustomerlink, " create customer link");
		click(Lanlink_Metro_Obj.Lanlink_Metro.createcustomerlink, "Create Customer Link");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.createcustomer_header, "create customer page header");

		////// scrolltoend();
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "ok");
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "ok");

		// Warning msg check
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.customernamewarngmsg, "Legal Customer Name");
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.countrywarngmsg, "Country");
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.ocnwarngmsg, "OCN");
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.typewarngmsg, "Type");
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.emailwarngmsg, "Email");

		// Create customer by providing all info

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.nametextfield, "name");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.nametextfield, name, "name");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.maindomaintextfield, "Main Domain");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.maindomaintextfield, maindomain, "Main Domain");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.country, "country");
		// sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.country,country,"country");
		addDropdownValues_commonMethod("Country", Lanlink_Metro_Obj.Lanlink_Metro.country, country);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.ocntextfield, "ocn");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.ocntextfield, ocn, "ocn");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.referencetextfield, "reference");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.referencetextfield, reference, "reference");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.technicalcontactnametextfield, "technical contact name");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.technicalcontactnametextfield, technicalcontactname,
				"technical contact name");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.typedropdown, "technical contact name");
		// sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.typedropdown,type,"technical
		// contact name");
		addDropdownValues_commonMethod("Type", Lanlink_Metro_Obj.Lanlink_Metro.type, type);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.emailtextfield, "email");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.emailtextfield, email, "email");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.phonetextfield, "phone text field");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.phonetextfield, phone, "phone text field");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.faxtextfield, "fax text field");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.faxtextfield, fax, "fax text field");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "ok");
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "ok");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.customercreationsuccessmsg, "Customer successfully created.");

	}

	public void selectCustomertocreateOrder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

		mouseMoveOn(Lanlink_Metro_Obj.Lanlink_Metro.ManageCustomerServiceLink);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.CreateOrderServiceLink, "Create Order Service Link");
		click(Lanlink_Metro_Obj.Lanlink_Metro.CreateOrderServiceLink, "Create Order Service Link");

		// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton,"Next button");
		// click(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton,"Next button");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.entercustomernamefield, "name");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.entercustomernamefield, name, "name");

		// verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.entercustomernamefield,"name");
		// sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.entercustomernamefield,"*","name");

		waitforPagetobeenable();

		addDropdownValues_commonMethod("Choose a customer", Lanlink_Metro_Obj.Lanlink_Metro.chooseCustomerdropdown, name);
		// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton, "Next button");
		click(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton, "Next button");

	}

	public void createorderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ExistingOrderSelection");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton, "Next button");
		click(Lanlink_Metro_Obj.Lanlink_Metro.nextbutton, "Next button");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.order_contractnumber_warngmsg, "order contract number");
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.servicetype_warngmsg, "service type");

		if (neworder.equalsIgnoreCase("YES")) {
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.newordertextfield, "new order textfield");
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.newrfireqtextfield, "new rfireq textfield");
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.createorderbutton, "create order button");
			click(Lanlink_Metro_Obj.Lanlink_Metro.createorderbutton, "create order button");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.OrderCreatedSuccessMsg, "Order Created Success Msg");

		} else if (existingorderselection.equalsIgnoreCase("YES")) {
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.selectorderswitch, "select order switch");
			click(Lanlink_Metro_Obj.Lanlink_Metro.selectorderswitch, "select order switch");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.existingorderdropdown, "existing order drop down");
			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",Lanlink_Metro_Obj.Lanlink_Metro.existingorderdropdown, existingordernumber);
		} else {
			Reporter.log("Order not selected");
		}

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void createService_ServiceIdentification(String ServiceIdentificationNumber)
			throws InterruptedException, IOException {
		// Service Identification

		boolean serviceIdentificationField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);
		if (serviceIdentificationField) {

			Report.LogInfo("Info", " 'Service Identification' text field is displaying as exepected", "Pass");
			if (!ServiceIdentificationNumber.equalsIgnoreCase("null")) {
				sendKeys((Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification), ServiceIdentificationNumber,
						"Service Id");
				waitforPagetobeenable();
				Report.LogInfo("Info", ServiceIdentificationNumber + " is entered under 'Service Identification' field",
						"Pass");
			} else {
				Report.LogInfo("Info", "It is a mandatory field. No values entered for service identification", "FAIL");

			}
		} else {
			Report.LogInfo("Info", "' Service Identfication' mandatory field is not available under 'Add Service' page",
					"FAIL");

		}

	}

	public void createService_singleEndPointCPE(String EndpointCPE) throws InterruptedException, IOException {

		addCheckbox_commonMethod(Lanlink_Metro_Obj.Lanlink_Metro.EndpointCPE, "Single Endpoint CPE", EndpointCPE);

	}

	public void createService_phone(String PhoneContact) throws InterruptedException, IOException {

		boolean phone = false;
		try {
			phone = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact);
			sa.assertTrue(phone, "phone contact field is not displayed");
			if (phone) {
				Report.LogInfo("Info", " 'Phone Contact' text field is displaying as expected", "PASS");

				if (!PhoneContact.equalsIgnoreCase("null")) {

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact, PhoneContact, "Phone contact");
					waitforPagetobeenable();
					Report.LogInfo("Info", PhoneContact + " is entered under 'Phone contact' field", "PASS");
				} else {
					Report.LogInfo("Info", "Value not entered under 'Phone contact' field", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Phone Contact' text field is not available under 'Create Service' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Phone contact text field is not available");
			Report.LogInfo("Info", " 'Phone Contact' text field is not available under 'Create Service' page", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'hone Contact' field", "FAIL");
			Reporter.log("Not able to enter value under 'Phone Contact' field");
		}
	}

	public void createSerivce_email(String Email) throws InterruptedException, IOException {

		boolean email = false;
		try {
			email = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Email);
			if (email) {
				Report.LogInfo("Info", " 'Email' text field is displaying as expected", "PASS");

				if (!Email.equalsIgnoreCase("null")) {

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Email, Email, "Email");
					waitforPagetobeenable();
					Report.LogInfo("Info", Email + " is entered under 'Email' field", "PASS");

				} else {
					Report.LogInfo("Info", "value not entered under 'Email' field", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Email' field is not available under 'Create Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Email field is not available");
			Report.LogInfo("Info", " 'Email' field is not available under 'create Service' page", "PASS");
		} catch (Exception er) {
			er.printStackTrace();
			Reporter.log("Not able to enter value in 'Email' field");
			Report.LogInfo("Info", "Not able to enter value in 'Email' field", "FAIL");
		}
	}

	public void DirectFibre_10G(String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed,
			String EndpointCPE, String Email, String PhoneContact, String remark, String PerformanceReporting,
			String ProactiveMonitoring, String deliveryChannel, String ManagementOrder, String vpnTopology,
			String intermediateTechnology, String CircuitReference, String CircuitType, String notificationManagement,
			String COScheckbox, String Multiportcheckbox, String ENNIcheckbox)
			throws InterruptedException, IOException {

		scrollIntoTop();
		waitforPagetobeenable();

		// Service Identification Number
		createService_ServiceIdentification(ServiceIdentificationNumber);

		// End point CPE
		createService_singleEndPointCPE(EndpointCPE);

		// Email
		createSerivce_email(Email);

		// COS checkbox
		

		// Phone Contact
		createService_phone(PhoneContact);

		// Remark
		createService_remark(remark);

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.configurtoinptionpanel_webelementToScroll);

		// Performance Reporting
		createService_performancereporting_10G(PerformanceReporting);

		// Pro active Monitoring
		createService_proactivemonitoring(ProactiveMonitoring, notificationManagement);

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

		waitforPagetobeenable();

		// Delivery Channel
		createService_deliveryChannel(deliveryChannel);

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
		waitforPagetobeenable();

		// VPN topology
		boolean vpNTopology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
		if (vpNTopology) {
			Report.LogInfo("Info", " 'VPN Topology' dropdown is displaying as expected", "PASS");
			if (!vpnTopology.equalsIgnoreCase("null")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology_xbutton);
				waitforPagetobeenable();

				if (vpnTopology.equals("Point-to-Point")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();
					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Intermediate technology
					createService_intermediateTechnology(intermediateTechnology);

					// Circuit Reference
					createService_circuitreference(CircuitReference);

					// Circuit Type
					createSerivce_circuitType(CircuitType);
				}

				else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();
					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Circuit Reference
					createService_circuitreference(CircuitReference);
				} else {

					Reporter.log(vpnTopology + " is not available under VPN topoloy dropdown");
					Reporter.log(vpnTopology + " is not available inside the VPN topoloy dropdown");
					Report.LogInfo("Info", vpnTopology + " is not available under VPN topoloy dropdown", "FAIL");

				}
			} else {
				Report.LogInfo("Info",
						" No value provided for 'VPN topology' dropdown. 'point -to -point' is selected by default",
						"PASS");

				// Intermediate technology
				createService_intermediateTechnology(intermediateTechnology);

				// Circuit Reference
				createService_circuitreference(CircuitReference);

				// Circuit Type
				createSerivce_circuitType(CircuitType);
			}
		} else {
			Report.LogInfo("Info",
					" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
					"FAIL");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void createService_remark(String remark) throws InterruptedException, IOException {
		boolean Remark = false;
		try {
			Remark = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Remark);
			if (Remark) {
				Report.LogInfo("Info", " 'Remark' text field is displaying as expected", "PASS");

				if (!remark.equalsIgnoreCase("null")) {

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Remark, remark, "Remark");
					waitforPagetobeenable();
					Report.LogInfo("Info", remark + " is entered under 'Remark' field", "PASS");

				} else {

					Report.LogInfo("Info", "value not entered under 'Remark' field", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Remark' text field is not available under 'Create Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Remak text field is not availeble");
			Report.LogInfo("Info", " 'Remark' text field is not available under 'Create Service' page", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Reporter.log(" Not able t enter value in 'remark' text field");
			Reporter.log("Not able to enter value under 'Remark' field");
		}

	}

	public void createService_performancereporting_10G(String PerformanceReporting)
			throws InterruptedException, IOException {

		boolean performancereoprting = false;
		try {
			performancereoprting = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);

			if (performancereoprting) {
				Report.LogInfo("Info", " 'Performance Reporting' checkbox is displaying as expected", "PASS");

				if (!PerformanceReporting.equalsIgnoreCase("null")) {

					if (PerformanceReporting.equalsIgnoreCase("yes")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox, "select checkbox");
						Reporter.log("performance Reporting check box is selected");

						boolean prfrmselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox,
								"checkbox");
						if (prfrmselection) {
							Report.LogInfo("Info", "Performance Reporting checkbox is selected as expected", "PASS");
						} else {
							Report.LogInfo("Info", "Performance Reporting checkbox is not selected", "FAIL");
						}
					} else {

						Reporter.log("Performance Reporting is not selected");
						Report.LogInfo("Info", "performance Reporting checkbox is not selected", "PASS");
					}
				} else {
					Report.LogInfo("Info", " 'Performance Reporting' checkbox is not selected", "PASS");
				}
			} else {
				Report.LogInfo("Info",
						" 'Performance Reporting' checkbox is not displaying under 'create service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Perormance reporting' checkbox is not selected");
			Report.LogInfo("Info", " 'Performance Reporting' checkbox is not displaying under 'create service' page",
					"FAIL");
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	public void createService_deliveryChannel(String deliveryChannel) throws InterruptedException, IOException {

		boolean deliveryChanel = false;
		try {
			deliveryChanel = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
			sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
			if (deliveryChanel) {
				Report.LogInfo("Info", " 'Delivery Channel' dropdown is displaying as expected", "PASS");
				if (!deliveryChannel.equalsIgnoreCase("null")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + deliveryChannel + "')]")).click();
					waitforPagetobeenable();
					Report.LogInfo("Info", deliveryChannel + " is selected under 'Delivery channel' dropdown", "PASS");

				} else {
					Report.LogInfo("Info", "No value selected under 'Delivery channel' dropdown", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Delivery channel' dropdown is not dispalying under 'create Serice' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Delivery channel' dropdown is not dispalying");
			Report.LogInfo("Info", " 'Delivery channel' dropdown is not dispalying under 'create Serice' page", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to selected value under 'Delivery channel' dropodwn", "FAIL");
		}
	}

	public void createService_managementOptions(String ManagementOrder) throws InterruptedException, IOException {

		boolean Managementorder = false;
		try {
			Managementorder = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.managementOrder);
			sa.assertTrue(Managementorder, "Management order field is not displayed");
			if (Managementorder) {
				Report.LogInfo("Info", " ' Management Order' dropdown is displaying as expected", "PASS");
				if (!ManagementOrder.equalsIgnoreCase("null")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.managementOrder,"Management order");
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + ManagementOrder + "')][1]")).click();

					waitforPagetobeenable();

					Report.LogInfo("Info", ManagementOrder + " is selected under 'management Order' field", "PASS");

				} else {
					Report.LogInfo("Info", "Values not entered under 'Management Order' field", "PASS");
				}
			} else {
				Report.LogInfo("Info", " ' Management Order' dropdown is not displaying under 'Create Service' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'Management order' dropdown is not displaying");
			Report.LogInfo("Info", " ' Management Order' dropdown is not displaying under 'Create Service' page",
					"FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to select value under 'management Order' dropdown", "FAIL");
		}
	}

	public void createService_intermediateTechnology(String intermediateTechnology)
			throws InterruptedException, IOException {
		boolean intrTech = false;
		try {
			intrTech = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology);
			if (intrTech) {
				Report.LogInfo("Info",
						" 'Intermediate Technology' text field is displying as expected under 'create service' page",
						"PASS");
				if (!intermediateTechnology.equalsIgnoreCase("null")) {
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology, intermediateTechnology,
							"intermediateTechnology");

					String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology, "value");
					waitforPagetobeenable();
					Report.LogInfo("Info", actualvalue + " is entered under 'Intermediate technology' field", "PASS");
				} else {
					Report.LogInfo("Info", "No value entered under 'Intermediate  Technology' field", "PASS");
				}
			} else {
				Report.LogInfo("Info",
						" 'Intermediate Technology' text field is not displying under 'create service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Intermediate Technology' text field is not displying");
			Report.LogInfo("Info", " 'Intermediate Technology' text field is not displying under 'create service' page",
					"FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info",
					" Not able to enter value under 'Intermediate Technology' text field in 'create service' page",
					"FAIL");
			Reporter.log("Not able to enter value under 'Intermediate Technology' text field in 'create service' page");
		}
	}

	public void createService_circuitreference(String CircuitReference) throws InterruptedException, IOException {
		boolean crctRef = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
		if (crctRef) {
			Report.LogInfo("Info", " 'Circuit Reference' text field is displying as expected", "PASS");
			if (!CircuitReference.equalsIgnoreCase("null")) {
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference, CircuitReference, "Circuit Reference");
				waitforPagetobeenable();

				String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference, "value");

				Report.LogInfo("Info", actualvalue + " is entered under 'Circuit Reference' field", "PASS");
			} else {
				Report.LogInfo("Info", "Circuit reference field is mandatory, but no inputs has been provided", "FAIL");
			}
		} else {
			Report.LogInfo("Info", " 'Circuit Reference' text field is not displying", "FAIL");
		}
	}

	public void createSerivce_circuitType(String CircuitType) throws InterruptedException, IOException {

		if (!CircuitType.equalsIgnoreCase("null")) {
			click(Lanlink_Metro_Obj.Lanlink_Metro.CircuitType1 + CircuitType + Lanlink_Metro_Obj.Lanlink_Metro.CircuitType2);
			waitforPagetobeenable();
			Report.LogInfo("Info", CircuitType + " is selected under 'Circuit Type'", "PASS");
		} else {
			Report.LogInfo("Info", " No changes made. 'Default' is selected under'Circuit type' as no input provided",
					"PASS");

		}
	}

	/*
	 * public void device_vendorModel(String[] Vender, String vender) throws
	 * InterruptedException, IOException { boolean vendor=false;
	 * 
	 * 
	 * if(isClickable(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender));
	 * 
	 * click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vender);
	 * 
	 * 
	 * try { List<WebElement> listofvender =
	 * findWebElements(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns);
	 * 
	 * if(listofvender.size()>0) {
	 * 
	 * for (WebElement vendertypes : listofvender) {
	 * 
	 * boolean match = false; for (int i = 0; i < Vender.length; i++) { if
	 * (vendertypes.getText().equals(Vender[i])) { match = true;
	 * 
	 * Reporter.log("list of vendor under add devices are : " +
	 * vendertypes.getText());
	 * 
	 * } }
	 * 
	 * }
	 * 
	 * }else { Reporter.log("dropdown value inside Vender/Model is empty"); } }
	 * catch(Exception e) { e.printStackTrace(); } //Entering value inside
	 * Vendor/Model dropdown if(vender.equalsIgnoreCase("null")) {
	 * 
	 * Reporter.
	 * log("No values has been passed for Mandatory 'Vendor/Model' dropdown for adding device"
	 * );
	 * 
	 * } else { click(Lanlink_Metro_Obj.Lanlink_Metro.vender1+vender
	 * +Lanlink_Metro_Obj.Lanlink_Metro.vender2); }
	 * 
	 * 
	 * }
	 */

	public void editSiteOrder_Performancereporting(String performReport) throws InterruptedException, IOException {

		waitforPagetobeenable();

		if (isClickable(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereporting)) {
			if (performReport.equalsIgnoreCase("null")) {
				Reporter.log("Performance reporting dropdown is not edited");

			} else {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereporting_xbutton,
						"Addsiteorder performancereporting");
				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereporting_xbutton,
						"Addsiteorder performancereporting");

				waitforPagetobeenable();

				addDropdownValues_commonMethod("Performance Reporting",Lanlink_Metro_Obj.Lanlink_Metro.performancereport,performReport);


				waitforPagetobeenable();

				Reporter.log("Edited value for 'Performance reporting' dropdown is: " + performReport);
			}
		} else {
			Reporter.log("Performance Reporting' dropdown is not available under 'Edit Site Order' page");
		}
	}

	public void editSiteOrder_proactiveMonitoring(String ProactiveMonitor) throws InterruptedException, IOException {

		waitforPagetobeenable();

		if (isClickable(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_proactivemonitoring)) {
			if (ProactiveMonitor.equalsIgnoreCase("null")) {
				Reporter.log("Proactive monitoring' dropdown value is not edited");

			} else {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitorder_proactivemonitoring_xbutton,
						"Addsitorder Proactivemonitoring");
				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsitorder_proactivemonitoring_xbutton,
						"Addsitorder Proactivemonitoring");

				
				
				addDropdownValues_commonMethod("Proactive Monitoring",Lanlink_Metro_Obj.Lanlink_Metro.ProactiveMonitor,ProactiveMonitor);

				waitforPagetobeenable();

				Reporter.log("Edited value for 'Pro active Monitoring' dropdown is: " + ProactiveMonitor);
			}
		} else {
			Reporter.log(" Pro active Monitoring' dropdown is not available under 'Edit Site Order' page");
		}
	}

	public void editSiteOrder_smartMonitoring(String smartmonitor) throws InterruptedException, IOException {

		waitforPagetobeenable();

		if (isClickable(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoring)) {
			if (smartmonitor.equalsIgnoreCase("null")) {
				Reporter.log("Smarts Monitoring dropdown value is not edited");

			} else {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoring_xbutton,
						"Addsiteorder smartmonitoring");
				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoring_xbutton, "Addsiteorder smartmonitoring");

				

				
				addDropdownValues_commonMethod("Smarts Monitoring",Lanlink_Metro_Obj.Lanlink_Metro.smartmonitor,smartmonitor);


				waitforPagetobeenable();

				Reporter.log("Edited value for 'Smarts Monitoring' dropdown is: " + smartmonitor);
			}
		} else {
			Reporter.log("Smart Monitoring dropdown is not available under 'Edit Site Order' page");
		}
	}

	public void editsiteorder_sitealias(String siteallias) throws InterruptedException, IOException {

		waitforPagetobeenable();

		if (isClickable(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias)) {
			if (siteallias.equalsIgnoreCase("null")) {
				Reporter.log("Site Alias field value is not edited");

			} else {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias, "Addsiteorder");
				clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias);
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias, siteallias, "Addsiteorder");

				waitforPagetobeenable();

				String siteAliasvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias, "value");

				Reporter.log("Edited value for 'Site Alias' field is: " + siteAliasvalue);
			}
		} else {
			Reporter.log(" Site Alias field is not available under 'Edit Site Order' page");
		}
	}

	public void hostnametextField_IPV4(String command_ipv4, String ipAddress) throws IOException, InterruptedException {

		boolean IPV4availability = false;
		try {
			IPV4availability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.commandIPv4_hostnameTextfield);

			if (IPV4availability) {

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.commandIPv4_hostnameTextfield, ipAddress,
						"IP Address or Hostname");

			} else {
				System.out
						.println("'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("'Hostname or IpAddress' for 'Ipv4' text field is not displaying for " + command_ipv4);
		}
	}

	public void executeCommandAndFetchTheValue(String executeButton) throws IOException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.executebutton_Ipv4, "Execute");

		boolean remarkField = false;
		try {
			remarkField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.result_textArea);
			if (remarkField) {
				Reporter.log("'Remark' text field is displaying");

				String remarkvalue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.result_textArea);
				Reporter.log("value under 'Remark' field displaying as " + remarkvalue);

			} else {
				Reporter.log("'Remark' text field is not displaying");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("'Remark' text field is not displaying");
		}
	}

	public void editsiteOrder_remark(String remark) throws InterruptedException, IOException {

		waitforPagetobeenable();

		if (isClickable(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark)) {
			if (remark.equalsIgnoreCase("null")) {
				Reporter.log("Remark text field value is not edited");

			} else {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark, "Addsiteorder");
				clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark);
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark, remark, "Addsiteorder");

				String remarkValue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark, "value");

				Reporter.log("Edited value for 'Remark' field is: " + remarkValue);
			}
		} else {
			Reporter.log("Remark text field is not available under 'Edit Site Order' page");
		}
	}

	public void OKbutton_AddSiteOrder() throws InterruptedException, IOException {

		boolean ok = false;
		try {
			ok = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.okbutton);
			sa.assertTrue(ok, "OK button is not displayed");
			if (ok) {
				Report.LogInfo("Info", " 'OK' button is displaying as expected", "PASS");
			} else {
				Report.LogInfo("Info", " 'OK' button is not displaying", "FAIL");
			}

		} catch (Exception e) {
			Report.LogInfo("Info", " 'OK' button is not displaying", "FAIL");
			Reporter.log(" 'OK' button is not displaying");
		}
	}

	public void cancelbutton_AddSiteOrder() throws InterruptedException, IOException {

		boolean cancel = false;
		try {
			cancel = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cancel);
			sa.assertTrue(cancel, "Cancel button is not " + "displayed");
			if (cancel) {
				Report.LogInfo("Info", " 'Cancel' button is displaying as expected", "PASS");
			} else {
				Report.LogInfo("Info", " 'Cancel' button is not displaying", "FAIL");
			}
		} catch (Exception e) {
			Report.LogInfo("Info", " 'Cancel' button is not displaying", "FAIL");
			Reporter.log(" 'Cancel' button is not displaying");
		}
	}

	public void editSiteOrder_deviceName(String devicename) throws InterruptedException, IOException {

		boolean devicenameAvailability = false;
		try {

			devicenameAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Devicenamefield);

			if (devicenameAvailability) {

				if (devicename.equalsIgnoreCase("Null")) {

					Report.LogInfo("Info", " NO changes made for 'Device Name' field", "PASS");
				} else {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Devicenamefield);

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Devicenamefield, devicename, "Device name");
					waitforPagetobeenable();

					String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Devicenamefield,
							"value");
					Report.LogInfo("Info", " Device Name value has been edited and the edited value is: " + actualvalue,
							"PASS");
				}
			} else {
				Report.LogInfo("Info", " Device name text field is not displaying under 'Edit Site Order' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Device name text field is not displaying under 'Edit Site Order' page", "FAIL");
			Reporter.log(" Device name text field is not displaying under 'Edit Site Order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'device name' field", "FAIL");
			Reporter.log(" Not able to edit 'device name' field");
		}
	}

	public void validateRemark_AddSiteOrder() throws InterruptedException, IOException {
		boolean REmark = false;
		try {
			REmark = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark);
			sa.assertTrue(REmark, " Remark field is not displayed");
			if (REmark) {
				Report.LogInfo("Info", " 'Remak' field is displaying under 'Add Site order' page as expected", "PASS");
			} else {
				Report.LogInfo("Info", " 'Remak' field is not displaying under 'Add Site order' page", "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Remak' field is not displaying under 'Add Site order' page", "FAIL");

		}

	}

	public void validateVlanID_AddSiteOrder() throws InterruptedException, IOException {
		boolean vlanid = false;
		try {
			vlanid = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Vlanid);
			sa.assertTrue(vlanid, "VLAN id field is not displayed");
			if (vlanid) {
				Report.LogInfo("Info", "'VLAN ID' text field is displaying under 'Add Site order' page as expected",
						"PASS");
			} else {
				Report.LogInfo("Info", " 'VLAN ID' text field is not displaying under 'Add Site order' page", "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'VLAN ID' text field is not displaying under 'Add Site order' page", "FAIL");
		}
	}

	public void technologyDropdownFor10GigE() throws InterruptedException, IOException {

		String Technology = "Accedian";

		boolean technology, Nonterminationpointcheckbox, portectedcheckbox;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList((Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown).replace("value", Technology); 
				 clickonTechnology(technologySelected, Technology);
		// Non Termination Point
		verifySiteOrderFields_NonterminationField();

	}

	public void verifySiteOrderFields_NonterminationField() throws InterruptedException, IOException {
		boolean Nonterminationpointcheckbox = false;
		try {
			Nonterminationpointcheckbox = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint);
			sa.assertTrue(Nonterminationpointcheckbox,
					"On selecting 'Overture' under Technology, Non termination point checkbox is not available");
			if (Nonterminationpointcheckbox) {
				Report.LogInfo("Info",
						" 'Non Termination Point' checkbox is displayed under 'Add Site order' page as expected",
						"PASS");
			} else {
				Report.LogInfo("Info", " 'Non Termination Point' checkbox is not Available under 'Add Site order' page",
						"FAIL");
			}

			boolean nonTerminaionpointselection = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint);
			sa.assertFalse(nonTerminaionpointselection,
					"Non-termination point checbox under Add site is selected by default");
			if (nonTerminaionpointselection) {
				Report.LogInfo("Info", " 'Non-Termination Point' checkbox is selected by default", "PASS");
			} else {
				Report.LogInfo("Info", " 'Non-Termination Point' checkbox is not selected by default as expected",
						"PASS");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Non Termination Point' checkbox is not Available under 'Add Site order' page",
					"FAIL");
			Reporter.log(" 'Non Termination Point' checkbox is not Available under 'Add Site order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			//Report.LogInfo("Info", " 'Non-Termination Point' checkbox is selected by default", "FAIL");
			Reporter.log(" 'Non-Termination Point' checkbox is selected by default");
		}
	}

	public void validateSiteAlias_AddSiteOrder() throws InterruptedException, IOException {

		// Site alias Field
		boolean sitealias = false;
		try {
			sitealias = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias);
			sa.assertTrue(sitealias, "Site alias field is not displayed");
			if (sitealias) {
				Report.LogInfo("Info", " 'Site Alias' text field is displaying under 'Add Site order' page as expected",
						"PASS");
			} else {
				Report.LogInfo("Info", " 'Site Alias' text field is not displaying under 'Add Site order' page",
						"FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Site Alias' text field is not displaying under 'Add Site order' page", "FAIL");
		}
	}

	public void validateCountry_AddSiteorder() throws InterruptedException, IOException {

		boolean COuntry = false;

		COuntry = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Country);
		sa.assertTrue(COuntry, "Country dropdown is not displayed");
		if (COuntry) {
			Report.LogInfo("Info",
					" 'Country' mandatory dropdown is displaying under 'Add Site Order' page as expected", "PASS");
			Reporter.log("Country dropdown is displaying");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Country, "Country");
			List<String> listofcountry = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofcountry)));

			if (listofcountry.size() >= 1) {
				for (String countrytypes : listofcountry) {

					Report.LogInfo("Info", "The list of country inside dropdown is: " + countrytypes, "PASS");

				}
			} else {
				Reporter.log("no values are available inside Country dropdown for Add site order");
				Report.LogInfo("Info", "no values are available inside Country dropdown for Add site order", "FAIL");
			}

			// click on Blank page
			clickOnBankPage();
			waitforPagetobeenable();

		} else {
			Report.LogInfo("Info", " 'Country' mandatory dropdown is not available under 'Add Site Order' page",
					"FAIL");
		}

	}

	public void editsiteorder_circuitReference(String circuitRef) throws InterruptedException, IOException {

		boolean circuitRefAvilability = false;
		try {
			circuitRefAvilability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceField);

			if (circuitRefAvilability) {
				if (circuitRef.equalsIgnoreCase("null")) {

					Report.LogInfo("Info", "Circuit Reference field value is not edited", "PASS");

				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceField);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceField, circuitRef,
							"Circuit reference");
					waitforPagetobeenable();
					String circuitRefvalue = getAttributeFrom(
							Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceField, "value");
					Report.LogInfo("Info", "Edited value for 'Circuit Reference' field is: " + circuitRefvalue, "PASS");
				}
			} else {
				Report.LogInfo("Info", " Circuit Reference field is not available under 'Edit Site Order' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Circuit Reference field is not available under 'Edit Site Order' page", "FAIL");
			Reporter.log(" Circuit Reference field is not available under 'Edit Site Order' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'Circuit reference' field", "FAIL");
			Reporter.log(" Not able to enter value under 'Circuit reference' field");
		}
	}

	public void editSiteOrder_IVReference(String IVReference) throws InterruptedException, IOException {

		boolean IVrefValue = false;

		try {
			IVrefValue = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.IVReference1 + IVReference + Lanlink_Metro_Obj.Lanlink_Metro.IVReference2);

			if (IVrefValue) {

				Report.LogInfo("Info", " IV reference value is displaying as: " + IVReference + " as expected", "PASS");
				Reporter.log(" IV reference value is displaying as: " + IVReference + " as expected");

			} else {
				String actualValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.IVReference);
				Report.LogInfo("Info",
						" IV Reference) value is not displaying as expected" + "   Actual value displaying is: "
								+ actualValue + "  Expected value for 'IV Reference' is: " + IVReference,
						"FAIL");

				Reporter.log(" IV Reference) value is not displaying as expected" + "   Actual value displaying is: "
						+ actualValue + "  Expected value for 'IV Reference' is: " + IVReference);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'IV Reference' value is not dispaying as expected", "FAIL");
			Reporter.log("IV Reference value is not dispaying as expected");
		}
	}

	public void editsiteorder_NonterminationPoint(String nontermination) throws InterruptedException, IOException {

		boolean NonTerminationPointAvailability = false;

		try {
			NonTerminationPointAvailability = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint);

			if (NonTerminationPointAvailability) {

				if (!nontermination.equalsIgnoreCase("null")) {

					boolean nonterminatepoint = false;
					try {
						nonterminatepoint = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint,
								"Non termination point");
					} catch (Exception e) {
						e.printStackTrace();
					}
					waitforPagetobeenable();

					if (nontermination.equalsIgnoreCase("yes")) {

						if (nonterminatepoint) {

							Report.LogInfo("Info",
									" 'Non-Termination point' checkbox is already Selected while creating", "PASS");

						} else {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint);
							Reporter.log("'Non-Termination point' check box is selected");
							Report.LogInfo("Info", "'Non-Termination point' is edited and gets selected", "PASS");
						}

					}

					else if (nontermination.equalsIgnoreCase("no")) {

						if (nonterminatepoint) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint);
							Reporter.log("'Non-Termination point' check box is unselected");
							Report.LogInfo("Info", "'Non-Termination point' is edited and gets unselected", "PASS");

						} else {
							Report.LogInfo("Info",
									"'Non-Termination point' was not selected during service creation and it remains unselected as expected",
									"PASS");
						}

					}
				} else {
					Report.LogInfo("Info", "No changes made for 'Non-Termination point' chekbox as expected", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Non-Termination Point checkbox is not available under 'Edit Site order' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Non-Termination Point checkbox is not available under 'Edit Site order' page",
					"FAIL");
			Reporter.log(" Non-Termination Point checkbox is not available under 'Edit Site order' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to click on 'Non-termination point' checkbox ", "FAIL");
			Reporter.log(" Not able to click on 'Non-termination point' checkbox ");
		}
	}

	public void editsiteOrder_vlanid(String VLANid) throws InterruptedException, IOException {

		boolean vlanAvailability = false;

		try {
			vlanAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Vlanid);

			if (vlanAvailability) {
				if (VLANid.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "Vlanid field value is not edited", "PASS");
					Reporter.log("Vlanid field value is not edited");
				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Vlanid);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Vlanid, VLANid, "VLAN Id");
					waitforPagetobeenable();

					String VLANidValue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Vlanid, "value");
					Report.LogInfo("Info", "Edited value for 'Vlan id' field is: " + VLANidValue, "PASS");
					Reporter.log("Edited value for 'Vlan id' field is: " + VLANidValue);
				}
			} else {
				Report.LogInfo("Info", "VLAN Id field is not available under 'Edit Site Order' page", "FAIL");
				Reporter.log("VLAN Id field is not available under 'Edit Site Order' page");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", "VLAN Id field is not available under 'Edit Site Order' page", "FAIL");
			Reporter.log("VLAN Id field is not available under 'Edit Site Order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'VLAn Id' text field", "FAIL");
			Reporter.log(" not able to edit 'VLAN ID' text field");
		}
	}

	public void editSiteOrder_technology(String technology) throws InterruptedException {

		boolean techValue = false;

		try {
			techValue = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.technology1 + technology + Lanlink_Metro_Obj.Lanlink_Metro.technology2);

			if (techValue) {

				Report.LogInfo("Info", " Technology value is displaying as: " + technology + " as expected", "PASS");

			} else {
				String actualValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.tech);
				Report.LogInfo("Info",
						" Technology value is not displaying as expected" + "   Actual value displaying is: "
								+ actualValue + "  Expected value for Technology is: " + technology,
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Technology value is not displaying as expected", "FAIL");
			Reporter.log(" Technology value is not displaying as expected");
		} catch (Exception ew) {
			ew.printStackTrace();
			Report.LogInfo("Info", " Technology value is not displaying as expected", "FAIL");
			Reporter.log(" Technology value is not displaying as expected");
		}
	}

	public void editSiteorder_Offnet(String offnet) throws InterruptedException, IOException {

		boolean offnetAvailability = false;

		try {
			offnetAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (offnetAvailability) {

			Report.LogInfo("Info", " 'Offnet' checkbox is displaying under 'Edit Site ordeer' page as exepected",
					"PASS");
			if (!offnet.equalsIgnoreCase("null")) {

				boolean offnetselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox,
						"offnet Checkbox");
				waitforPagetobeenable();

				if (offnet.equalsIgnoreCase("yes")) {

					if (offnetselection) {

						Report.LogInfo("Info",
								"Offnet checkbox is not edited and it is already Selected while creating", "PASS");

					} else {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox, "offnet Checkbox");
						Reporter.log("Offnet check box is selected");
						Report.LogInfo("Info", "Offnet is edited and gets selected as expected", "PASS");
					}

				}

				else if (offnet.equalsIgnoreCase("no")) {

					if (offnetselection) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox, "offnet Checkbox");
						Reporter.log("Offnet check box is unselected");
						Report.LogInfo("Info", "offnet is edited and gets unselected", "PASS");

					} else {
						Report.LogInfo("Info", "Offnet is not edited and it remains unselected", "PASS");
					}

				}
			} else {
				Report.LogInfo("Info", "No changes made for Offnet chekbox", "PASS");
			}
		} else {
			Report.LogInfo("Info", " 'Offnet' checkbox is not available under 'Edit Site Order' page", "FAIL");
		}
	}

	public void validateSmartsMOnitoring_AddSiteOrder() throws InterruptedException, IOException {

		String[] Smartmonitoring = { "Follow Service", "no" };

		// smarts monitoring
		boolean smartmonitoring = false;
		smartmonitoring = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoring);
		sa.assertTrue(smartmonitoring, "Smart monitoring dropdown is not displayed");
		if (smartmonitoring) {
			Report.LogInfo("Info", " 'Smart Monitoring' dropdown is displaying under 'Add Site Order' page as expected",
					"PASS");
			// check default value
			String smartmonitorDefaultValues = getTextFrom(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoringdefaultvalue);
			Report.LogInfo("Info",
					smartmonitorDefaultValues + " is displaying under 'Smart Monitoring' dropdown by default", "PASS");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoring, "smartmonitoring");
			List<String> listofsmartmonitoring = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofsmartmonitoring)));

			if (listofsmartmonitoring.size() >= 1) {
				for (String smartmonitoringtypes : listofsmartmonitoring) {

					boolean match = false;
					for (int i = 0; i < Smartmonitoring.length; i++) {
						if (smartmonitoringtypes.equals(Smartmonitoring[i])) {
							match = true;
							Reporter.log("list of smart monitoring are : " + smartmonitoringtypes);
							Report.LogInfo("Info",
									"The list of smart monitoring  inside dropdown while  adding site order is: "
											+ smartmonitoringtypes,
									"PASS");
						}
					}

					sa.assertTrue(match,"");
				}
			} else {

				Reporter.log("no values are available inside smart monitoring dropdown for Add site order");
				Report.LogInfo("Info", "no values are available inside smart monitoring dropdown for Add site order",
						"FAIL");
			}
		} else {
			Report.LogInfo("Info", " 'Smart Monitoring' dropdown is not avilable under 'Add Site Order' page", "FAIL");
		}
	}

	public void validateProactiveMonitoring_AddSiteOrder() throws InterruptedException, IOException {

		String[] Proactivemonitoring = { "Follow Service", "no" };

		// pro active monitoring
		boolean proactivemonitoring = false;
		proactivemonitoring = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_proactivemonitoring);
		sa.assertTrue(proactivemonitoring, "pro active monitoring dropdown is not displayed");
		if (proactivemonitoring) {
			Report.LogInfo("Info",
					" 'Proactie Monitoring' dropdown is displaying under 'Add Site Order' page as Expected", "PASS");

			// check default value
			String proactiveMonitorDefaultValues = getTextFrom(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_proactivemonitoringdefaultvalue);
			Report.LogInfo("Info",
					proactiveMonitorDefaultValues + " is displaying under 'roactive Monitoring' dropdown by default",
					"PASS");

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_proactivemonitoring, "proactivemonitoring");
			List<String> listofproactivemonitoring = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofproactivemonitoring)));

			if (listofproactivemonitoring.size() >= 1) {
				for (String proactivemonitoringtypes : listofproactivemonitoring) {

					boolean match = false;
					for (int i = 0; i < Proactivemonitoring.length; i++) {
						if (proactivemonitoringtypes.equals(Proactivemonitoring[i])) {
							match = true;
							Reporter.log("list of pro active monitoring : " + proactivemonitoringtypes);

							Report.LogInfo("Info",
									"The list of proactive monitoring inside dropdown while  adding site order is: "
											+ proactivemonitoringtypes,
									"PASS");
						}
					}
					sa.assertTrue(match,"");

				}
			} else {

				Reporter.log("no values are available inside pro active monitoring dropdown for Add site order");
				Report.LogInfo("Info",
						"no values are available inside pro active monitoring dropdown for Add site order", "FAIL");
			}
		} else {
			Report.LogInfo("Info", " 'Proactie Monitoring' dropdown is not available under 'Add Site Order' page ",
					"FAIL");
		}
	}

	public void validatePerformancereporting_AddSiteOrder() throws InterruptedException, IOException {

		String[] Performancereporting = { "Follow Service", "no" };

		// Performance reporting dropdown
		boolean performancereport = false;
		performancereport = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereporting);
		sa.assertTrue(performancereport, "performance reporting dropdown is not displayed");
		if (performancereport) {
			Report.LogInfo("Info", " 'Performance reporting' dropdown is displaying under 'Add Site order' as expected",
					"PASS");
			waitforPagetobeenable();

			// check default value
			String performanceRprtDefaultValues = getTextFrom(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereportingdefaultvalue);
			Report.LogInfo("Info",
					performanceRprtDefaultValues + " is displaying under 'Performance reporting' dropdown by default",
					"PASS");

			// check list of values inside Performance Reporting drodpown
			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereporting, "performancereporting");
			List<String> listofperformancereporting = new ArrayList<>(
					Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofperformancereporting)));

			if (listofperformancereporting.size() >= 1) {
				for (String perfoemancereportingtypes : listofperformancereporting) {
					boolean match = false;
					for (int i = 0; i < Performancereporting.length; i++) {
						if (perfoemancereportingtypes.equals(Performancereporting[i])) {
							match = true;
							Reporter.log("list of performance reporting : " + perfoemancereportingtypes);
							Report.LogInfo("Info",
									"list of performance reporting for AddSite order : " + perfoemancereportingtypes,
									"PASS");
						}

					}

					sa.assertTrue(match,"");

				}
			} else {
				Reporter.log("no values are available inside performance reporting dropdown for Add site order");
				Report.LogInfo("Info",
						"no values are available inside performance reporting dropdown for Add site order", "FAIL");
			}
		} else {
			Report.LogInfo("Info", " 'Performance reporting' dropdown is not availble under 'Add Site order' ", "FAIL");
		}
	}

	public void editSiteOrder_siteOrderNumber(String siteOrderNumber) throws InterruptedException {

		boolean siteOrderValue = false;
		try {
			siteOrderValue = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.siteOrderNumber1 + siteOrderNumber
					+ Lanlink_Metro_Obj.Lanlink_Metro.siteOrderNumber2);

			if (siteOrderValue) {

				Report.LogInfo("Info", " Site Order Number (Siebel Service ID) value is displaying as: "
						+ siteOrderNumber + " as expected", "PASS");
				Reporter.log(" Site Order Number (Siebel Service ID) value is displaying as: " + siteOrderNumber
						+ " as expected");
			} else {
				String actualValue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.siteOrderNumber);
				Report.LogInfo("Info",
						" Site Order Number (Siebel Service ID) value is not displaying"
								+ "   Actual value displaying is: " + actualValue
								+ "  Expected value for 'Site Order Number (Siebel Service ID)' is: " + siteOrderNumber,
						"FAIL");

				Reporter.log(" Site Order Number (Siebel Service ID) value is not displaying"
						+ "   Actual value displaying is: " + actualValue
						+ "  Expected value for 'Site Order Number (Siebel Service ID)' is: " + siteOrderNumber);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Site Order NUmber' value is not displaying as expected", "FAIL");
			Reporter.log(" 'Site Order NUmber' value is not displaying as expected");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " 'Site Order NUmber' value is not displaying as expected", "FAIL");
			Reporter.log(" 'Site Order NUmber' value is not displaying as expected");
		}

	}

	public void existingCity(String city) throws InterruptedException, IOException {

		selectByVisibleText(Lanlink_Metro_Obj.Lanlink_Metro.cityDropdown_selectTag,city,"City");

	}

	public void existingPremise(String premise) throws InterruptedException, IOException {

		selectByVisibleText(Lanlink_Metro_Obj.Lanlink_Metro.premiseDropdown_selectTag, premise,"Premise");

	}

	public void deviceCreatoin_Accedian_MSPselected(String cpename, String vender, String snmpro,
			String managementAddress, String Mepid,  String country, String City,
			String Site, String Premise, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
			String existingcityselectionmode, String newcityselectionmode, String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
			String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode, String premisename, String premisecode, String VLANid) throws InterruptedException, IOException {

		

		try {

			String[] Vender = { "Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX",
					"Accedian 10GigE-MetroNode-CE-2Port", "Accedian 10GigE-MetroNode-CE-4Port", "Accedian Clipper LT",
					"Accedian Clipper LT-S", "Accedian MetroNID GT", "Accedian MetroNID GT-S",
					"Accedian MetroNID GX-S" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

			waitforPagetobeenable();
			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag);
			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Country Error Message
			device_countrywarningMessage();

		//	scrollDown("//label[text()='Name']");
			waitforPagetobeenable();

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address dropdown
			// device_managementAddress(existingmanagementAddress,
			// newmanagementAddress, managementAddress);
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_manageaddress, managementAddress, "Management Address");

			// MEP Id
			device_mepID(Mepid);

			// VLAN Id
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_vlanId, VLANid, "VLAN Id");

			////// scrolltoend();
			waitforPagetobeenable();

			// Country
			device_country(country);

			// City
			if (existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
				addCityToggleButton();
				// New City
				newcity(newcityselectionmode, cityname, citycode);
				// New Site
				newSite(newsiteselectionmode, sitename, sitecode);
				// New Premise
				newPremise(newpremiseselectionmode, premisename, premisecode);

			}

			else if (existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
				// Existing City
				existingCity(City);

				// Site

				if (existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
					// Existing Site
					existingSite(Site);

					// Premise
					if (existingpremiseselectionmode.equalsIgnoreCase("yes")
							& newpremiseselectionmode.equalsIgnoreCase("no")) {
						existingPremise(Premise);

					} else if (existingpremiseselectionmode.equalsIgnoreCase("no")
							& newpremiseselectionmode.equalsIgnoreCase("yes")) {
						// New Premise
						addPremiseTogglebutton();
						newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
					}

				}

				else if (existingsiteselectionmode.equalsIgnoreCase("no")
						& newsiteselectionmode.equalsIgnoreCase("yes")) {
					// New Site
					addSiteToggleButton();
					newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode);

					// New Premise
					newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
				}
			}

			// OK button

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			// cancel button

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_cancelbutton, "Cancel");

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			scrollIntoTop();
			waitforPagetobeenable();

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage1_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage2_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage3_devicename, "Device Name");

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton);
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			// Report.LogInfo("Info", "FAiled while verify the fields for Add
			// CPE device");

		}
		Report.LogInfo("Info", "Input data has been passed for creating device", "PASS");
		waitforPagetobeenable();

	}

	public void device_countrywarningMessage() throws InterruptedException, IOException {

		// Country Error Message
		boolean countryErr = false;
		countryErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_countryErrmsg);
		sa.assertTrue(countryErr, "Country warning message is not displayed ");

		if (countryErr) {
			Reporter.log("country warning message is displaying as expected");
			String countryErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_countryErrmsg);
			Reporter.log("Country  message displayed as : " + countryErrMsg);
			Report.LogInfo("Info", " validation message for Country field displayed as : " + countryErrMsg, "PASS");
			Reporter.log("Country warning message displayed as : " + countryErrMsg);
		} else {
			Reporter.log("Country warning message is not displaying");
			Report.LogInfo("Info", " Validation message for Country dropdown is not displaying", "FAIL");
		}
	}

	public void existingSite(String site) throws InterruptedException, IOException {
		
		//selectByValue(Lanlink_Metro_Obj.Lanlink_Metro.siteDropdown_selectTag, "Site", site);
		selectByVisibleText(Lanlink_Metro_Obj.Lanlink_Metro.siteDropdown_selectTag,site,"Site");
		//selectValueInsideDropdown(Lanlink_Metro_Obj.Lanlink_Metro.siteDropdown_selectTag, "Site", site);
	}

	public void newSite_ClickOnSiteTogglebutton(String newsiteselectionmode, String sitename, String sitecode)
			throws InterruptedException, IOException {

		// New site
		if (newsiteselectionmode.equalsIgnoreCase("yes")) {

			if (sitename.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", " value for Site name field  is not entered", "FAIL");
			} else {
				// Site Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton,
						sitename, "Site name");
				waitforPagetobeenable();
				String sitenme = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton, "value");
				waitforPagetobeenable();
				Reporter.log("Entered Site Name is : " + sitenme);
				Report.LogInfo("Info", "Entered Site Name is : " + sitenme, "PASS");
			}

			if (sitecode.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", " value for Site code field  is not entered", "FAIL");
			} else {

				// Site Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton,
						sitecode, "Site code");
				waitforPagetobeenable();
				String sitecde = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton, "value");
				waitforPagetobeenable();
				Reporter.log("Entered Site Code is : " + sitecde);
				Report.LogInfo("Info", "Entered Site Code is : " + sitecde, "PASS");

			}

		} else {
			Report.LogInfo("Info", " Add new city is not selected", "FAIL");
		}
	}

	public void newPremise_clickonSiteToggleButton(String newpremiseselectionmode, String premisename,
			String premisecode) throws InterruptedException, IOException {

		// New premise
		if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

			if (premisename.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", " value for Premise Name field  is not entered", "PASS");
			} else {
				// Premise Name Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton,
						premisename, "premisename");
				waitforPagetobeenable();
				String prmsenme = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Name is : " + prmsenme);
				Report.LogInfo("Info", "Entered Premise Name is : " + prmsenme, "PASS");
			}

			if (premisecode.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", " value for Premise code field  is not entered", "FAIL");
			} else {
				// Premise Code Field
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton,
						premisecode, "premisename");
				waitforPagetobeenable();
				String premisecde = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton,
						"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Code is : " + premisecde);
				Report.LogInfo("Info", "Entered Premise Code is : " + premisecde, "PASS");
			}

		} else {
			Report.LogInfo("Info", " Add new Premise is not selected", "FAIL");
		}
	}

	public void device_nameField(String cpename, String expectedDeviceNameFieldAutopopulatedValue) {
		boolean name = false;
		try {
			name = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name);
			sa.assertTrue(name, "Name text field is not available under create device for Equipment");

			if (name) {
				if (cpename.equalsIgnoreCase("null")) {
					Reporter.log("No values has been assed for 'Name' text field");
					Report.LogInfo("Info",
							"No values has been passed for Mandatory 'Name' field under 'Add CPE Device' page", "FAIL");
				}

				else {
					String deviceNameActualPopulatedvalue = getAttributeFrom(
							Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name, "value");
					sa.assertEquals(deviceNameActualPopulatedvalue, expectedDeviceNameFieldAutopopulatedValue,
							"Device Name field Auto Populated value is not displaying as expected");
					Report.LogInfo("Info", " Under 'Name' text field, value displaying by default is: "
							+ deviceNameActualPopulatedvalue, "PASS");
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_Name, cpename, "CPE name");
					waitforPagetobeenable();
					Report.LogInfo("Info", cpename + " is the value passed for Mandatory 'Cpe name' text field",
							"PASS");
				}
			} else {
				Report.LogInfo("Info", " Name text field is not displaying under 'Add CPE Device' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Name' text field is not available", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", "Not able to enter value inside the 'Name' field", "FAIL");
		}

	}

	public void editesiteOrder_DcaEnabled(String DCAenabledsite, String cloudserviceprovider)
			throws InterruptedException, IOException {

		boolean DCAavailability = false;

		try {
			DCAavailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (DCAavailability) {

			if (!DCAenabledsite.equalsIgnoreCase("null")) {

				boolean dcaenabled = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox,
						"DCAenabledsitecheckbox");
				waitforPagetobeenable();

				if (DCAenabledsite.equalsIgnoreCase("yes")) {

					if (dcaenabled) {

						Report.LogInfo("Info", "DCA Enabled Site is already Selected while creating", "PASS");

						if (cloudserviceprovider.equalsIgnoreCase("null")) {

							Report.LogInfo("Info", "No changes made to Cloud Service Provider", "PASS");

						} else {

							addDropdownValues_commonMethod("Cloud Service Provider",Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cloudserviceProvider, cloudserviceprovider);
						}

					} else {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox,
								"DCAenabledsitecheckbox");
						Reporter.log("DCA Enabled Site check box is selected");
						Report.LogInfo("Info", "DCA Enabled Site checkbox is selected", "PASS");

						if (cloudserviceprovider.equalsIgnoreCase("null")) {

							Report.LogInfo("Info", "No changes made to Cloud Service Provider", "PASS");

						} else {

							addDropdownValues_commonMethod("Cloud Service Provider",Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cloudserviceProvider, cloudserviceprovider);

						}
					}

				}

				else if (DCAenabledsite.equalsIgnoreCase("no")) {

					if (dcaenabled) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox,
								"DCAenabledsitecheckbox");
						Reporter.log("DCA Enabled Site check box is unselected");
						Report.LogInfo("Info", "DCA Enabled Site is unselected as Expected", "PASS");

					} else {
						Report.LogInfo("Info",
								"DCA Enabled Site was not selected during service creation and it remains unselected as expected",
								"PASS");
					}

				}
			} else {
				Report.LogInfo("Info", "No changes made for DCAenabled site chekbox as expected", "PASS");
			}
		} else {
			Report.LogInfo("Info", "DCA Enabled Site checkbox is not displaying under 'Edit Site Order' page", "FAIL");
		}

	}

	public void validateSite_AddSiteOrder() throws InterruptedException, IOException {

		// CSR name field
		boolean csr_name = false;
		csr_name = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_CSRname);
		sa.assertTrue(csr_name, "CSR_Name field is not displayed");
		if (csr_name) {
			Report.LogInfo("Info", " 'CSR Name' text field is displaying under 'Add Site order' page as expected",
					"PASS");
			Reporter.log("CSR name field is dipslaying as expected");
		} else {
			Report.LogInfo("Info", " 'CSR Name' text field is not available under 'Add Site order' page", "FAIL");
		}

		// click on site toggle button to check Physical site dropdown
		boolean sitetogglebutton = false;
		sitetogglebutton = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Sitetogglebutton);
		sa.assertTrue(sitetogglebutton, "select Site toggle button is not displayed");
		if (sitetogglebutton) {
			Reporter.log("site order toggle button is displaying as expected");
			Report.LogInfo("Info", " 'Select Site' toggle button is displaying under 'Add Site Order' page as expected",
					"PASS");
		} else {
			Report.LogInfo("Info", " 'Select Site' toggle button is not avilable under 'Add Site Order' page", "FAIL");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Sitetogglebutton);
		waitforPagetobeenable();

		// Check for Error message for physical Site
		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag);
		waitforPagetobeenable();
		Reporter.log("scrolling down to click n OK button to find eror message for site Dropdown");
		click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
		waitforPagetobeenable();

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Country);

		waitforPagetobeenable();

		Reporter.log(
				"scrolling up back till device country dropodwn to find error message validation for physical site");
		boolean physicalsiteErr = false;
		physicalsiteErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_physicalsiteErrmsg);
		sa.assertTrue(physicalsiteErr, "Physical Site dropdown warning is not displayed ");
		if (physicalsiteErr) {
			Reporter.log("Physical Site Error message is displaying as expected");
			Report.LogInfo("Info",
					" 'Physite Site' dropdown warning message is displaying under 'Add Site Order' page as expected",
					"PASS");
			String physicalsiteErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_physicalsiteErrmsg);
			Reporter.log("Physical Site  message displayed as : " + physicalsiteErrMsg);
			Report.LogInfo("Info",
					" validation message for Physical Site dropdown displayed as : " + physicalsiteErrMsg, "PASS");
			Reporter.log("Physical Site validation message displayed as : " + physicalsiteErrMsg);
		} else {
			Report.LogInfo("Info",
					" 'Physical Site' dropdown warning message is not displaying under 'Add Site Order' page", "FAIL");
			Reporter.log("Physical site warning message is not displaying");
		}

		// Physical Site dropdown
		boolean SIte = false;
		SIte = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitedropdown);
		sa.assertTrue(SIte, "PhysicalSite dropdown is not displayed");
		if (SIte) {
			Reporter.log("Physical Site dropdown is displaying as expected");
			Report.LogInfo("Info", " 'physical Site' dropdown is displaying under 'Add Site order' page as expected",
					"PASS");

		} else {
			Report.LogInfo("Info", " 'Physical Site' dropdown is not available under 'Add Site Order' page", "FAIL");
		}
	}

	public void validateCity_AddSiteOrder() throws InterruptedException, IOException {

		// City dropdown
		boolean CIty = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_City);
		sa.assertTrue(CIty, "City dropdown is not displayed");
		if (CIty) {
			Report.LogInfo("Info", " 'City' mandatory dropdown is displaying under 'Add Site Order' page as expected",
					"PASS");

		} else {
			Report.LogInfo("Info", " 'City' mandatory dropdown is not available under 'Add Site Order' page", "FAIL");
		}

		// select city toggle button
		boolean selectcitytoggle = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Citytogglebutton);
		sa.assertTrue(selectcitytoggle, "Select city toggle button for Add Site is not available");
		if (selectcitytoggle) {
			Report.LogInfo("Info", " 'Select City' toggle button is displaying under 'Add Site Order' page as expected",
					"PASS");
		} else {
			Report.LogInfo("Info", " 'Select City' toggle button is not avilable under 'Add Site Order' page ", "FAIL");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Citytogglebutton);
		waitforPagetobeenable();

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag);
		waitforPagetobeenable();

		Reporter.log("Scrolling down to validate error messgae for City name and city code");
		// Click on Next button to get warning message for XNG City name and XNG
		// City Code text fields
		click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
		waitforPagetobeenable();

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Country);

		waitforPagetobeenable();

		Reporter.log(
				"scrolling above till device country for validating error message for 'city name ' and 'city code'");
		// XNG City Name Error message
		boolean xngCitynameErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_xngcitynameerrmsg);
		sa.assertTrue(xngCitynameErr, " 'XNG City Name' warning message is not displayed ");
		if (xngCitynameErr) {
			Report.LogInfo("Info",
					" Warning message for 'XNG City Name' dropdown is displying under 'Add Site order' page as expected ",
					"PASS");
			String citynameErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_xngcitynameerrmsg);
			Reporter.log("XNG City Name  message displayed as : " + citynameErrMsg);
			Report.LogInfo("Info",
					" validation message for 'XNG City Name' text field displayed as : " + citynameErrMsg, "PASS");
			Reporter.log("XNG City Name warning message displayed as : " + citynameErrMsg);

		} else {
			Report.LogInfo("Info",
					" Warning message for 'XNG City Name' dropdown is not displaying under 'Add Site order' page ",
					"FAIL");
		}

		// XNG City Code Error message
		boolean xngCitycodeErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_xngcityCodeerrmsg);
		sa.assertTrue(xngCitycodeErr, " 'XNG City Code' warning message is not displayed ");
		if (xngCitycodeErr) {
			Report.LogInfo("Info",
					" Warning message for 'XNG City Code' dropdown is displying under 'Add Site order' page as expected ",
					"PASS");
			String cityCodeErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_xngcityCodeerrmsg);
			Reporter.log("XNG City Name  message displayed as : " + cityCodeErrMsg);
			Report.LogInfo("Info",
					" validation message for 'XNG City Code' text field displayed as : " + cityCodeErrMsg, "PASS");
			Reporter.log("XNG City Code warning message displayed as : " + cityCodeErrMsg);

		} else {
			Report.LogInfo("Info",
					" Warning message for 'XNG City Code' dropdown is not displaying under 'Add Site order' page ",
					"FAIL");
		}

		// xng city name
		boolean XNGcityname = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_xngcityname);
		sa.assertTrue(XNGcityname, "XNG city name field for Add Site is not available");
		if (XNGcityname) {
			Report.LogInfo("Info", " 'XNG City name field is displaying under 'Add Site order' as expected", "PASS");
		} else {
			Report.LogInfo("Info", " 'XNG City name field is not available under 'Add Site order'", "FAIL");
		}

		// xng city code
		boolean XNGcitycode = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_XNGcitycode);
		sa.assertTrue(XNGcitycode, "XNG city code field for Add Site is not available");
		if (XNGcitycode) {
			Report.LogInfo("Info", " 'XNG City code field is displaying under 'Add Site order' as expected", "PASS");
		} else {
			Report.LogInfo("Info", " 'XNG City code field is not available under 'Add Site order'", "FAIL");
		}

	}

	public void DirectFibre_1G(String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed,
			String EndpointCPE, String Email, String PhoneContact, String remark, String PerformanceReporting,
			String ProactiveMonitoring, String deliveryChannel, String ManagementOrder, String vpnTopology,
			String intermediateTechnology, String CircuitReference, String CircuitType, String notificationManagement,
			String perCocPerfrmReprt, String actelsBased, String standrdCir, String standrdEir, String prmiumCir,
			String prmiumEir, String COScheckbox, String Multiportcheckbox, String ENNIcheckbox)
			throws InterruptedException, IOException {

		scrollIntoTop();
		waitforPagetobeenable();

		// Service Identification
		createService_ServiceIdentification(ServiceIdentificationNumber);

		// End point CPE
		createService_singleEndPointCPE(EndpointCPE);

		// Email
		createSerivce_email(Email);

		// COS checkbox
		addCheckbox_commonMethod(Lanlink_Metro_Obj.Lanlink_Metro.cosCheckbox_createService, "COS", COScheckbox);

		// Multiport
		if (COScheckbox.equalsIgnoreCase("Yes")) {
			addCheckbox_commonMethod(Lanlink_Metro_Obj.Lanlink_Metro.multiport_checkbox, "Multiport", Multiportcheckbox);
		}

		// Phone Contact
		createService_phone(PhoneContact);

		// Remark
		createService_remark(remark);

		// scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.configurtoinptionpanel-webelementToScroll);

		// Performance Reporting
		if (!PerformanceReporting.equalsIgnoreCase("null")) {

			if (PerformanceReporting.equalsIgnoreCase("yes")) {

				boolean perfrmReprtFieldcheck = isElementPresent(
						Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
				if (perfrmReprtFieldcheck) {
					Report.LogInfo("Info",
							" 'Performance reporting' checkbox is displaying under 'Manage ment options' panel in 'Create Service' page as exepcted",
							"PASS");
					click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox, "performance Reporting checkbox");
					waitforPagetobeenable();

					boolean prfrmReportselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox,
							"performance Reporting checkbox");
					if (prfrmReportselection) {
						Reporter.log("performance monitoring check box is selected");
						Report.LogInfo("Info", "Performance Reporting checkbox is selected as expected", "PASS");
					} else {
						Report.LogInfo("Info", "Performance Reporting checkbox is not selected", "FAIL");
					}

					if (COScheckbox.equalsIgnoreCase("Yes")) {
						// Per CoS Performance Reporting chekcbox
						boolean perCoSPrfrmReprtFieldcheck = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
						if (perCoSPrfrmReprtFieldcheck) {
							Report.LogInfo("Info",
									" 'Per CoS Performance Reporting' checkbox is displaying, when 'Performance reporting' checkbox is selected",
									"PASS");
							if (perCocPerfrmReprt.equalsIgnoreCase("Yes")) {
								click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
								waitforPagetobeenable();

								boolean perCoSSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport,
										"per CoS performnce report");
								if (perCoSSelection) {
									Report.LogInfo("Info",
											"Per CoS Performance Reporting checkbox is selected as expected", "PASS");
								} else {
									Report.LogInfo("Info", "Per CoS Performance Reporting checkbox is not selected",
											"FAIL");
								}

								// Actelis Based
								boolean actelisbasedFieldcheck = isElementPresent(
										Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
								if (actelisbasedFieldcheck) {
									Report.LogInfo("Info",
											" 'Actelis Based' checkbox is displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
											"PASS");
									if (actelsBased.equalsIgnoreCase("Yes")) {
										click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox,
												"Actelis Based checkbox");
										waitforPagetobeenable();

										boolean actelisSelection = isSelected(
												Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox,
												"Actelis Based checkbox");
										if (actelisSelection) {
											Report.LogInfo("Info", " 'Actelis Based' checkbox is selected as expected",
													"PASS");
										} else {
											Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected", "FAIL");
										}

										// Standard CIR Text Field
										createService_standardCIR(standrdCir);

										// Standard EIR Text Field
										createService_standardEIR(standrdEir);

										// Premium CIR Text Field
										createService_premiumCIR(prmiumCir);

										// Premium EIR Text Field
										createService_premiumEIR(prmiumEir);

									} else {
										Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected as expected",
												"PASS");
									}

								} else {
									Report.LogInfo("Info",
											" 'Actelis Based' checkbox is not displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
											"FAIL");
								}

							} else {
								Report.LogInfo("Info",
										" 'Per CoS Performance Reporting' checkbox is not selected as exepected",
										"PASS");
							}

						} else {
							Report.LogInfo("Info",
									" 'Per CoS Performance Rpeorting' checkbox is not displaying, when 'Performance reporting' checkbox is selected",
									"FAIL");
						}

					}

				} else {
					Report.LogInfo("Info", " 'Performance Reporting' checkbox is not available", "FAIL");
				}
			} else {

				Reporter.log("Performance Repoting is not selected");
				Report.LogInfo("Info", "performance Reporting checkbox is not selected as expected", "PASS");

			}
		}

		// Pro active Monitoring
		createService_proactivemonitoring(ProactiveMonitoring, notificationManagement);

		// Scroll Up
		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact);

		waitforPagetobeenable();

		// Delivery Channel
		createService_deliveryChannel(deliveryChannel);

		// management order
		createService_managementOptions(ManagementOrder);

		// ENNI checkbox
		addCheckbox_commonMethod("ENNI",Lanlink_Metro_Obj.Lanlink_Metro.ENNI_checkbox,ENNIcheckbox);

		////// scrolltoend();
		waitforPagetobeenable();

		// VPN topology
		boolean vpNTopology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
		if (vpNTopology) {
			Report.LogInfo("Info", " 'VPN Topology' dropdown is dsiplaying as expected", "PASS");
			if (!vpnTopology.equalsIgnoreCase("null")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology_xbutton, "VPNtopology_xbutton");
				waitforPagetobeenable();

				if (vpnTopology.equals("Point-to-Point")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();

					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Intermediate technology
					createService_intermediateTechnology(intermediateTechnology);

					// Circuit Reference
					createService_circuitreference(CircuitReference);

					// Circuit Type
					createSerivce_circuitType(CircuitType);
				}

				else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();

					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Circuit Reference
					createService_circuitreference(CircuitReference);

				}

				else {

					Reporter.log(vpnTopology + " is not available under VPN topoloy dropdown");
					Reporter.log(vpnTopology + " is not available inside the VPN topoloy dropdown");
					Report.LogInfo("Info", vpnTopology + " is not available under VPN topoloy dropdown", "FAIL");
				}

			} else {
				Report.LogInfo("Info",
						" No value provided for 'VPN topology' dropdown. 'point -to -point' is selected by default",
						"PASS");

				// Intermediate technology
				createService_intermediateTechnology(intermediateTechnology);

				// Circuit Reference
				createService_circuitreference(CircuitReference);

				// Circuit Type
				createSerivce_circuitType(CircuitType);
			}
		} else {
			Report.LogInfo("Info",
					" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
					"FAIL");
			Reporter.log(
					"'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void createService_standardCIR(String standrdCir) throws InterruptedException, IOException {
		boolean standrdCiR = false;

		try {
			standrdCiR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield);
			if (standrdCiR) {
				Report.LogInfo("Info",
						" 'Standard CIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (standrdCir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Standard CIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield, standrdCir, "Standard CIR");

					String valuesForSCIR = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield, "value");
					Report.LogInfo("Info", valuesForSCIR + " is enerted under 'Standard CIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'Standard CIR' field in 'create Service' page",
					"FAIL");
		}
	}

	public void createService_standardEIR(String standrdEir) throws InterruptedException, IOException {
		boolean standrdEiR = false;
		try {
			standrdEiR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield);
			if (standrdEiR) {
				Report.LogInfo("Info",
						" 'Standard EIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (standrdEir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Standard EIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield, standrdEir, "Standard EIR");

					String valuesForSEIR = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield, "value");
					Report.LogInfo("Info", valuesForSEIR + " is enerted under 'Standard EIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not albe to enter value under 'Standard EIR' field in 'create Service' page",
					"FAIL");
			Reporter.log(" Not able to enter value under 'Standard EIR' field in 'create Service' page");
		}
	}

	public void createService_premiumCIR(String prmiumCir) throws InterruptedException, IOException {
		boolean premiumCiR = false;
		try {
			premiumCiR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield);
			if (premiumCiR) {
				Report.LogInfo("Info",
						" 'Premium CIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (prmiumCir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Premium CIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield, prmiumCir, "Premium CIR");

					String valuesForPCIR = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield, "value");
					Report.LogInfo("Info", valuesForPCIR + " is enerted under 'Premium CIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'Premium CIR' field in 'create Service' page",
					"FAIL");
		}
	}

	public void createService_premiumEIR(String prmiumEir) throws InterruptedException, IOException {
		boolean premiumEiR = false;
		try {
			premiumEiR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield);
			if (premiumEiR) {
				Report.LogInfo("Info",
						" 'Standard EIR' text field displaying, when 'Actelis Based' checkbox is selected", "PASS");
				if (prmiumEir.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No Values entered in 'Premium EIR' text field", "PASS");
				} else {
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield, prmiumEir, "Premium EIR");

					String valuesForPEIR = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield, "value");
					Report.LogInfo("Info", valuesForPEIR + " is enerted under 'Premium EIR' text field", "PASS");
				}

			} else {
				Report.LogInfo("Info",
						" 'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
			Report.LogInfo("Info",
					" 'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to enter value under 'Premium EIR' field in 'create Service' page",
					"FAIL");
		}
	}

	public void createService_proactivemonitoring(String ProactiveMonitoring, String notificationManagement)
			throws InterruptedException, IOException {

		boolean proactiveMonitor = false;
		proactiveMonitor = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
		sa.assertTrue(proactiveMonitor, "pro active monitoring checkbox is not displayed");
		if (proactiveMonitor) {
			Report.LogInfo("Info", " 'Pro active Monitoring' text field is displaying as expected", "PASS");

			if (!ProactiveMonitoring.equalsIgnoreCase("null")) {
				if (ProactiveMonitoring.equalsIgnoreCase("yes")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring, "proactive Monitoring");
					Reporter.log("Pro active monitoring check box is selected");

					boolean proactiveSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring,
							"proactive Monitoring");
					if (proactiveSelection) {
						Report.LogInfo("Info", " 'pro active monitoring' checkbox is selected as expected", "PASS");
					} else {
						Report.LogInfo("Info", " 'pro active monitoring' checkbox is not selected", "FAIL");
					}

					// Notification management
					try {

						if (!notificationManagement.equalsIgnoreCase("null")) {
							Reporter.log(
									"Notificationan Management dropdown displays when pro active monitoring is selected");

							click(Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement, "notification management");
							waitforPagetobeenable();
							webDriver.findElement(By.xpath("//div[contains(text(),'" + notificationManagement + "')]")).click();
							waitforPagetobeenable();
							Report.LogInfo("Info",
									notificationManagement + " is selected under 'Notification management' dropdown",
									"PASS");

						} else {
							Report.LogInfo("Info", "No values selected under Notification management dropdown", "PASS");

						}

					} catch (NoSuchElementException e) {
						Reporter.log(
								" 'Notification management' dropodwn is not displaying under 'create Service' page");
						Report.LogInfo("Info",
								" 'Notification management' dropodwn is not displaying under 'create Service' page",
								"FAIL");
					} catch (Exception err) {
						err.printStackTrace();
						Report.LogInfo("Info", "Not able to select values under Notification management dropdown",
								"FAIL");
					}
				} else {
					Reporter.log("Pro active monitoring is not selected");
					Reporter.log("pro active monitoring is not selected");
					Report.LogInfo("Info", "performance monitor checkbox is not selected ", "PASS");
				}

			}
		} else {
			Report.LogInfo("Info", " 'Pro active monitoring' checkbox is not displaying", "FAIL");
		}
	}

	public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editOrderSelection");
		String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_OrderNumber");
		String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_VoicelineNumber");

		if (editOrderSelection.equalsIgnoreCase("no")) {
			Reporter.log("Edit Order is not performed");
		} else if (editOrderSelection.equalsIgnoreCase("Yes")) {
			Reporter.log("Performing Edit Order Functionality");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "Action dropdown");
			click(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "Action dropdown");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editorderlink, "Edit Order");
			click(Lanlink_Metro_Obj.Lanlink_Metro.editorderlink, "Edit Order");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editorderheader, "Edit Order");

			String EditOrderNo = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.editorderno);
			click(Lanlink_Metro_Obj.Lanlink_Metro.editorderno, "edit order no");
			clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.editorderno);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editorderno, "Order Number");
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.editorderno, editorderno, "Order Number");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editvoicelineno, "edit voice line no");
			clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.editvoicelineno);
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.cancelbutton, "cancel button");
			click(Lanlink_Metro_Obj.Lanlink_Metro.cancelbutton, "cancel button");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "Action dropdown");
			click(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "Action dropdown");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editorderlink, "Edit Order");
			click(Lanlink_Metro_Obj.Lanlink_Metro.editorderlink, "Edit Order");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editorderheader, "Edit Order Header");
			String editOrderHeader = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.editorderheader);
			String EditOrder = "Edit Order";
			editOrderHeader.equalsIgnoreCase(EditOrder);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editorderno, "edit order no");
			click(Lanlink_Metro_Obj.Lanlink_Metro.editorderno, "edit order no");
			clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.editorderno);
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.editorderno, editorderno, "Order Number");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editvoicelineno, "edit voice line no");
			clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.editvoicelineno);
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.editorder_okbutton, "OK Button");
			click(Lanlink_Metro_Obj.Lanlink_Metro.editorder_okbutton, "OK Button");

			if (editorderno.equalsIgnoreCase("Null")) {
				Reporter.log("Order/Contract Number (Parent SID) field is not edited");
			} else {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.ordernumbervalue, "order number value");
				String editOrderno = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ordernumbervalue);
				editOrderno.equalsIgnoreCase(editorderno);
			}

			if (editvoicelineno.equalsIgnoreCase("Null")) {
				Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
			} else {
				verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.ordervoicelinenumbervalue, "order voice line number value");
				String editVoicelineno = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ordervoicelinenumbervalue);
				editVoicelineno.equalsIgnoreCase(editvoicelineno);
			}
			Reporter.log("Edit Order is successful");

		}

	}

	public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{

		String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"changeOrderSelection_newOrder");
		String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "changeOrderSelection_existingOrder");
		String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_newOrderNumber");
		String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_VoicelineNumber");
		String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_existingOrderNumber");

		if ((changeOrderSelection_newOrder.equalsIgnoreCase("No"))
				&& (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
			Reporter.log("Change Order is not performed");

		} else if (changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "Action dropdown");
			click(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "Action dropdown");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorderlink, "change order link");
			click(Lanlink_Metro_Obj.Lanlink_Metro.changeorderlink, "change order link");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorderheader, "change order header");
			String changeOrderHeader = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.changeorderheader);
			String changeOrder = "Change Order";
			changeOrderHeader.equalsIgnoreCase(changeOrder);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_selectorderswitch, "select order switch");
			click(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_selectorderswitch, "select order switch");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeordernumber, "change order number");
			click(Lanlink_Metro_Obj.Lanlink_Metro.changeordernumber, "change order number");
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.editorderno, ChangeOrder_newOrderNumber);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeordervoicelinenumber, "change order voiceline number");
			click(Lanlink_Metro_Obj.Lanlink_Metro.changeordervoicelinenumber, "change order voiceline number");
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.changeordervoicelinenumber, changevoicelineno);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.createorder_button, "create order button");
			click(Lanlink_Metro_Obj.Lanlink_Metro.createorder_button, "create order button");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_okbutton, "change order ok button");
			click(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_okbutton, "change order ok button");

			Reporter.log("Change Order is successful");
		} else if (changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) {
			Reporter.log("Performing Change Order functionality");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "order action button");
			click(Lanlink_Metro_Obj.Lanlink_Metro.orderactionbutton, "order action button");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorderlink, "change order link");
			click(Lanlink_Metro_Obj.Lanlink_Metro.changeorderlink, "change order link");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorderheader, "change order");

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_chooseorderdropdown,
					"Order/Contract Number (Parent SID)");
			select(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_chooseorderdropdown, ChangeOrder_existingOrderNumber);

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_okbutton, "change order ok button");
			click(Lanlink_Metro_Obj.Lanlink_Metro.changeorder_okbutton, "change order ok button");

			Reporter.log("Change Order is successful");

		}

	}

	public void addsiteorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "country");
		String city = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "city");
		String CSR_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSR_Name");
		String site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitevalue");
		String performReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "performReport");
		String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Proactivemonitor");
		String smartmonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "smartmonitor");
		String technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String siteallias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteallias");
		String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
		String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DCAenabledsite");
		String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"cloudserviceprovider");
		String sitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existing_SiteOrdervalue");
		String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteorder_Remark");
		String xngcityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng city name");
		String xngcitycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng ciy code");
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"devicenameForaddsiteorder");
		String nonterminatepoinr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"nonterminationpoint");
		String Protected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"protectforaddsiteorder");
		String newcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcity");
		String existingcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingcity");
		String existingsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingsite");
		String newsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newsite");
		String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Siteordernumber");
		String circuitref = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrder_CircuitReference");
		String offnetSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Offnet");
		String IVReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Ivrefrence");
		String GCRolo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrer_GCROloType");
		String Vlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
		String Vlanether = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_Vlanethertype");
		String primaryVlan = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrder_PrimaryVlan");
		String primaryVlanether = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_PrimaryVlanEtherType");
		String EPNoffnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNoffnet");
		String EPNEOSDH = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_EPNEOSDH");
		String mappingmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SiteOrder_mappingMode");
		String portBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_portBased");
		String vlanBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_vlanBased");

		waitforPagetobeenable();

		Reporter.log("addSiteOrder");

		addSiteOrderValues_OnnetOffnet(interfaceSpeed, country, city, CSR_Name, site, performReport, ProactiveMonitor,
				smartmonitor, technology, siteallias, VLANid, DCAenabledsite, cloudserviceprovider, sitevalue, remark,
				xngcityname, xngcitycode, devicename, nonterminatepoinr, Protected, newcityselection,
				existingcityselection, existingsiteselection, newsiteselection);

	}

	public void Equip_clickonviewButton(String devicename) throws InterruptedException, IOException {

		clickOnBankPage();
		scrollIntoTop();
		waitforPagetobeenable();
		boolean viewpage = false;
		try {
			viewpage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.viewCPEdevicepage_devices);

			if (viewpage) {
				Reporter.log("In view page");
			} else {

				////// scrolltoend();
				waitforPagetobeenable();

				webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='View']")).click();

				waitforPagetobeenable();
			}
		} catch (Exception e) {
			e.printStackTrace();

			////// scrolltoend();
			waitforPagetobeenable();

			webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ devicename +"')]]]//span[text()='View']")).click();
			waitforPagetobeenable();
		}

	}

	public void selectRowforInterfaceInService(String interfacenumber) throws IOException, InterruptedException {

		int TotalPages;

		String TextKeyword = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceInselect_totalpage);

		TotalPages = Integer.parseInt(TextKeyword);

		Reporter.log("Total number of pages in table is: " + TotalPages);

		ab:

		if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceInselect_currentpage);
				int Current_page = Integer.parseInt(CurrentPage);

				sa.assertEquals(k, Current_page);

				Reporter.log("Currently we are in page number: " + Current_page);
				List<WebElement> results = webDriver.findElements(By.xpath("//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='" + interfacenumber +"']]//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
				
				int numofrows = results.size();
				Reporter.log("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {

					PageNavigation_NextPageForInterfaceInService();

				}

				else {

					for (int i = 0; i < numofrows; i++) {

						try {

							resultflag = results.get(i).isDisplayed();
							Reporter.log("status of result: " + resultflag);
							if (resultflag) {
								Reporter.log(results.get(i).getText());
								results.get(i).click();
								Report.LogInfo("Info", interfacenumber + " is selected under 'Interface in Service' table","PASS");
									click(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceInselect_Actiondropdown,
										"Interface Inselect Actiondropdown");

								waitforPagetobeenable();

								click(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceInselect_removebuton,
										"Interface Inselect removebuton");
								Report.LogInfo("Info",
										interfacenumber + " has been selected to get removed from service", "PASS");

							}

						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = webDriver.findElements(By.xpath("//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//div[div[text()='"+ interfacenumber+"']]//input"));
							numofrows = results.size();
							// results.get(i).click();
						
							Reporter.log("selected row is : " + i);
							Report.LogInfo("Info", "failure while selecting interface to remove from service", "FAIL");

						}

					}

					break ab;

				}

			}

		} else {

			Reporter.log("No values available in table");
			Reporter.log("No values available inside the InterfaceInService table");
		}

	}

	public void PageNavigation_NextPageForInterfaceInService() throws InterruptedException, IOException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceInselect_nextpage, "Interface Inselect nextpage");
		waitforPagetobeenable();

	}

	public void selectrowforInterfaceToselecttable(String interfacenumber) throws IOException, InterruptedException {

		int TotalPages;

		String TextKeyword = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceToselect_totalpage,
				"Interface Toselect totalpage");

		TotalPages = Integer.parseInt(TextKeyword);

		Reporter.log("Total number of pages in Interface to select table is: " + TotalPages);

		ab:

		if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceToselect_currentpage,
						"Interface Toselect currentpage");
				int Current_page = Integer.parseInt(CurrentPage);

				sa.assertEquals(k, Current_page);

				Reporter.log("Currently we are in page number: " + Current_page);

				List<WebElement> results = webDriver.findElements(By.xpath("//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='"+ interfacenumber  +"']]//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
				
				int numofrows = results.size();
				Reporter.log("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {

					PageNavigation_NextPageForInterfaceToselect();

				}

				else {

					for (int i = 0; i < numofrows; i++) {

						try {

							resultflag = results.get(i).isDisplayed();
							Reporter.log("status of result: " + resultflag);
							if (resultflag) {
								Reporter.log(results.get(i).getText());
								results.get(i).click();
										Report.LogInfo("Info",
										interfacenumber + " is selected under 'Interface to select' table", "PASS");
								waitforPagetobeenable();
								click(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceToselect_Actiondropdown,
										"Interface Toselect Actiondropdown");

								waitforPagetobeenable();

								click(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceToselect_addbuton,
										"Interface Toselect addbuton");
								waitforPagetobeenable();
								Report.LogInfo("Info", interfacenumber + " is added to service", "PASS");

							}

						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = webDriver.findElements(By.xpath("(//div[@class='row'])[8]//div[div[contains(text(),'"
									+ interfacenumber + "')]]//input"));
							numofrows = results.size();
							// results.get(i).click();
							Reporter.log("selected row is : " + i);
							Report.LogInfo("Info", "Failure on selecting an Interface to ad with service ", "FAIL");

						}

					}

					break ab;

				}

			}

		} else {

			Reporter.log("No values found inside the table");
			Reporter.log("No values available inside the Interfacetoselect table");
		}

	}

	public void PageNavigation_NextPageForInterfaceToselect() throws InterruptedException, IOException {

		click(Lanlink_Metro_Obj.Lanlink_Metro.InterfaceToselect_nextpage, "Interface Toselect nextpage");
		waitforPagetobeenable();

	}

	public void remark_AddSiteOrder(String remark) throws InterruptedException, IOException {

		if (remark.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", "No values entered under remark ", "PASS");
		} else {
			try {
				scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark);
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark, remark, "Remark");
				waitforPagetobeenable();

				String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_remark, "value");
				Report.LogInfo("Info", actualvalue + " is entered under 'remark' field", "PASS");
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", " 'Remark' field is not displating under 'Add Site order' page", "FAIL");
				Reporter.log(" 'Remark' field is not displating under 'Add Site order' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to enter value under 'Remark' field", "FAIL");
				Reporter.log(" Not able to enter value under 'Remark' field");
			}
		}

	}

	public void Edit_DirectFibre10G(String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed,
			String EndpointCPE, String Email, String PhoneContact, String remark, String performanceReporting,
			String ProactiveMonitoring, String deliveryChannel, String ManagementOrder, String vpnTopology,
			String intermediateTechnology, String CircuitReference, String CircuitType, String notificationManagement)
			throws InterruptedException, IOException {

		// Service Identification
		editService_serviceIdentification(ServiceIdentificationNumber);

		// Endpoint CPE
		editService_singleEndPointCPE(EndpointCPE, vpnTopology);

		// Email
		editService_Email(Email);

		// Phone contact
		editService_phoneContact(PhoneContact);

		// Remark
		editService_remark(remark);

		// Performance Reporting
		editService_performancereporting_10G(performanceReporting);

		// Proactive monitoring
		editService_proactiveMonitoring(ProactiveMonitoring, notificationManagement);

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

		waitforPagetobeenable();

		// Delivery channel
		editService_deliveryChannel(deliveryChannel);

		

		// Scroll to botton
		////// scrolltoend();
		waitforPagetobeenable();

		// VPN topology
		boolean vpnDisplayedValues = isElementPresent(
				Lanlink_Metro_Obj.Lanlink_Metro.vpnTopology1 + vpnTopology + Lanlink_Metro_Obj.Lanlink_Metro.vpnTopology2);
		if (vpnDisplayedValues) {
			Report.LogInfo("Info", vpnTopology + " is displaying under 'VPN Topology' as expected", "PASS");

			if (vpnTopology.equals("Point-to-Point")) {

				// Intermediate Technologies
				editService_IntermediateTechnology(intermediateTechnology);

				// Circuit reference
				editService_circuitreference(CircuitReference);

				// Circuit Type
				editService_circuitType(CircuitType);

			}

			else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

				// Circuit reference
				editService_circuitreference(CircuitReference);

			}

		} else {
			Report.LogInfo("Info", vpnTopology + " value is not displaying as expected.", "FAIL");
		}

		Report.LogInfo("Info", "Values has been Edited for Direct Fiber subtype under lanlink Service", "PASS");

		// click on "Ok button to update
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void verifyEnteredvalues(String labelname, String ExpectedText) throws InterruptedException {

		String text = null;
		String element = null;

		try {
			waitforPagetobeenable();
			String emptyele = getTextFrom(
					Lanlink_Metro_Obj.Lanlink_Metro.labelname1 + labelname + Lanlink_Metro_Obj.Lanlink_Metro.labelname2);

			if (element == null) {
			//	Report.LogInfo("Info", labelname + " not found", "FAIL");
				Reporter.log(labelname + " not found");
			} else if (emptyele != null && emptyele.isEmpty()) {
				// Report.LogInfo("Info", labelname + "' value is
				// empty","PASS");

				emptyele = "Null";

				sa.assertEquals(emptyele, ExpectedText, labelname + " value is not displaying as expected");

				if (emptyele.equalsIgnoreCase(ExpectedText)) {

					Report.LogInfo("Info", "The Expected value for '" + labelname
							+ "' field  is same as the Acutal value. It is id displaying blank", "PASS");
					Reporter.log(
							"The Expected value for '\"+ labelname +\"' field  is same as the Acutal value. It is displaying blank");

				} else {
					Report.LogInfo("Info",
							"The Expected value '" + ExpectedText + "' is not same as the Acutal value '" + text + "'",
							"FAIL");
					Reporter.log(" The Expected value '" + ExpectedText + "' is not same as the Acutal value '" + text
							+ "'");
				}

			} else {
				text = element;
				if (text.contains("-")) {

					String[] actualTextValue = text.split(" ");
					String[] expectedValue = ExpectedText.split(" ");

					if (expectedValue[0].equalsIgnoreCase(actualTextValue[0])) {
						Report.LogInfo("Info", " The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
						Reporter.log(" The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'");
					} else if (expectedValue[0].contains(actualTextValue[0])) {
						Report.LogInfo("Info", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
						Reporter.log("The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'");

					} else {
						Report.LogInfo("Info", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is not same as the Acutal value '" + text + "'", "FAIL");
						Reporter.log("The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is not same as the Acutal value '" + text + "'");
					}

				} else {
					if (ExpectedText.equalsIgnoreCase(text)) {
						Report.LogInfo("Info", " The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
						Reporter.log(" The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'");
					} else if (ExpectedText.contains(text)) {
						Report.LogInfo("Info", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'", "PASS");
						Reporter.log("The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is same as the Acutal value '" + text + "'");

					} else {
						Report.LogInfo("Info", "The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is not same as the Acutal value '" + text + "'", "FAIL");
						Reporter.log("The Expected value for '" + labelname + "' field '" + ExpectedText
								+ "' is not same as the Acutal value '" + text + "'");
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		//	Report.LogInfo("Info", labelname + " field is not displaying", "FAIL");
			Reporter.log(labelname + " field is not displaying");
		}

	}

	public void verifydataEntered_DirectFibre10G(String serviceype, String ServiceIdentificationNumber,
			String SelectSubService, String Interfacespeed, String EndpointCPE, String email, String PhoneContact,
			String remark, String PerformanceMonitoring, String ProactiveMonitoring, String deliveryChannel,
			String ManagementOrder, String vpnTopology, String intermediateTechnology, String CircuitReference,
			String CircuitType, String modularMSP, String notificationManagement, String ENNIcheckBox,
			String COScheckbox, String MultiPostcheckbox) throws InterruptedException, IOException {

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);
		waitforPagetobeenable();
		try {

			/**
			 * Service Panel
			 */
			// Service Identification
			verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

			// Service type
			verifyEnteredvalues("Service Type", serviceype);

			// Service Subtype
			verifyEnteredvalues("Service Subtype", SelectSubService);

			// Interface Speed
			verifyEnteredvalues("Interface Speed", Interfacespeed);

			// Single Endpoint CPE
			if (vpnTopology.equals("Point-to-Point")) {
				verifyEnteredvalues("Single Endpoint CPE", EndpointCPE);
			} else {
				verifyEnteredvalues("Single Endpoint CPE", "No");
			}

			// Email
			verifyEnteredvalueForEmail_serviceCreationpage("Email", email);

			// COS checkbox
			verifyEnteredvalues("COS", COScheckbox);

			if (COScheckbox.equalsIgnoreCase("Yes")) {
				verifyEnteredvalues("Multiport", MultiPostcheckbox);
			}

			// Phone Contact
			verifyEnteredvalues("Phone Contact", PhoneContact);

			// Modular MSP
			verifyEnteredvalues("Modular MSP", modularMSP);

			// Remark
			compareText("Remark", Lanlink_Metro_Obj.Lanlink_Metro.remark_viewPage, remark);

			sa.assertAll();
		} catch (AssertionError err) {
			err.printStackTrace();
			// Report.LogInfo("Info", label + " value is not displaying as
			// expected ");
		}

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_Servicepanel);

		waitforPagetobeenable();
		waitforPagetobeenable();

		/**
		 * Management Options panel
		 */

		// Delivery Channel
		verifyEnteredvalues("Delivery Channel", deliveryChannel);

		// Management Order
		verifyEnteredvalues("Management Order", ManagementOrder);

		// Proactive Monitoring
		verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

		if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
			verifyEnteredvalues("Notification Management Team", notificationManagement);
		}

		// Performance Reporting
		verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

		// WebElement managementOptionsPanel=
		// getwebelement(xml.getlocator(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_ManagementOptionsPanel"));
		// ScrolltoElement(managementOptionsPanel);
		// waitforPagetobeenable();

		// ////scrolltoend();
		waitforPagetobeenable();

		/**
		 * Configuration options panel
		 */

		// VPN Topology
		verifyEnteredvalues("VPN Topology", vpnTopology);

		if (vpnTopology.equals("Point-to-Point")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);

			// Circuit Type
			verifyEnteredvalues("Circuit Type", CircuitType);
		}

		if (vpnTopology.equals("Hub&Spoke")) {

			Reporter.log("Only vpn topology vaue displays under 'hub&Spoke'");
			Report.LogInfo("Info",
					" Only VPN topology field displays under 'Configuration' panel, when 'Hub&Spoke' is selected",
					"PASS");
		}

		if (vpnTopology.equals("E-PN (Any-to-Any)")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);
		}

	}

	public void verifyEnteredvalueForEmail_serviceCreationpage(String label, String expectedValue)
			throws InterruptedException, IOException {

		try {

			String valueinfo = getTextFrom(
					Lanlink_Metro_Obj.Lanlink_Metro.label1 + label + Lanlink_Metro_Obj.Lanlink_Metro.label2);
			if ((valueinfo.equals("")) || (valueinfo.equalsIgnoreCase(null))) {

				Reporter.log("value not displayed for " + label);
				valueinfo = "Null";

				sa.assertEquals(valueinfo, expectedValue, label + " value is not displaying as expected");

				// Report.LogInfo("Info", "No value displaying for : " +
				// label,"PASS");

			} else {

				Reporter.log("value displayed for " + label + " is : " + valueinfo);

				Reporter.log("value displayed for" + label + "is : " + valueinfo);

				sa.assertEquals(valueinfo, expectedValue, label + " value is not displaying as expected");

				if (valueinfo.equalsIgnoreCase(expectedValue)) {
					Reporter.log("The valus is dipslaying as expected");
					Report.LogInfo("Info", " Value is displaying as expected in 'view' page for " + label, "PASS");
					Report.LogInfo("Info", "value displayed for" + label + "is : " + valueinfo, "PASS");
				} else {
					Reporter.log("the values are not dipslaying as expected for label: " + label);
					Report.LogInfo("Info", " Value is not displaying as expected in 'view' page for " + label, "FAIL");
					Report.LogInfo("Info", "value displayed for " + label + "is : " + valueinfo, "FAIL");

				}

			}
		} catch (AssertionError err) {
			err.printStackTrace();
			Report.LogInfo("Info", label + " value is not displaying as expected ", "FAIL");
		} catch (NoSuchElementException e) {
			Reporter.log("value not displayed for " + label);
			Report.LogInfo("Info", "" + label + " is not displaying", "FAIL");

		}
	}

	public void verifydataEntered_DirectFibre1G(String serviceype, String ServiceIdentificationNumber,
			String SelectSubService, String Interfacespeed, String EndpointCPE, String Email, String PhoneContact,
			String remark, String PerformanceMonitoring, String ProactiveMonitoring, String deliveryChannel,
			String ManagementOrder, String vpnTopology, String intermediateTechnology, String CircuitReference,
			String CircuitType, String modularMSP, String perCocPerfrmReprt, String actelsBased, String standrdCir,
			String standrdEir, String prmiumCir, String prmiumEir, String notificationManagement, String ENNIcheckBox,
			String COScheckbox, String MultiPostcheckbox) throws InterruptedException, IOException {

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);

		waitforPagetobeenable();

		/**
		 * Service Panel
		 */
		// Service Identification
		verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

		// Service type
		verifyEnteredvalues("Service Type", serviceype);

		// Service Subtype
		verifyEnteredvalues("Service Subtype", SelectSubService);

		// Interface Speed
		verifyEnteredvalues("Interface Speed", Interfacespeed);

		// Single Endpoint CPE
		if (vpnTopology.equals("Point-to-Point")) {
			verifyEnteredvalues("Single Endpoint CPE", EndpointCPE);
		} else {
			verifyEnteredvalues("Single Endpoint CPE", "No");
		}

		// Email
		verifyEnteredvalueForEmail_serviceCreationpage("Email", Email);

		// COS checkbox
		verifyEnteredvalues("COS", COScheckbox);

		if (COScheckbox.equalsIgnoreCase("Yes")) {
			verifyEnteredvalues("Multiport", MultiPostcheckbox);
		}

		// Phone Contact
		verifyEnteredvalues("Phone Contact", PhoneContact);

		// Modular MSP
		verifyEnteredvalues("Modular MSP", modularMSP);

		// Remark
		compareText("Remark", Lanlink_Metro_Obj.Lanlink_Metro.remark_viewPage, remark);

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_Servicepanel);

		waitforPagetobeenable();

		/**
		 * Management Options panel
		 */

		// Delivery Channel
		verifyEnteredvalues("Delivery Channel", deliveryChannel);

		// Management Order
		verifyEnteredvalues("Management Order", ManagementOrder);

		// Proactive Monitoring
		verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

		if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
			verifyEnteredvalues("Notification Management Team", notificationManagement);
		}

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_ManagementOptionsPanel);

		waitforPagetobeenable();

		// Performance Reporting
		verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

		if (COScheckbox.equalsIgnoreCase("Yes")) {

			if ((vpnTopology.equals("Point-to-Point")) || (vpnTopology.equals("E-PN (Any-to-Any)"))) {

				if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("yes"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);

					// Standard
					compareText("Header Name for 1st row", Lanlink_Metro_Obj.Lanlink_Metro.standardHeaderName_viewPage,
							"Standard");

					compareText("Standard CIR", Lanlink_Metro_Obj.Lanlink_Metro.standardCIRvalue_viewPage, standrdCir);

					compareText("Standard EIR", Lanlink_Metro_Obj.Lanlink_Metro.standardEIRvalue_viewPage, standrdEir);

					// Premium
					compareText("Header Name for 2nd row", Lanlink_Metro_Obj.Lanlink_Metro.PremisumHeaderName_viewPage,
							"Premium");

					compareText("Premium CIR", Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRvalue_viewPage, prmiumCir);

					compareText("Premium EIR", Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRvalue_viewPage, prmiumEir);

				}
			} else {

				if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("yes"))) {

					actelsBased = "No";

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

			}
		} else if (COScheckbox.equalsIgnoreCase("Yes")) {

			verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);
		}

		// WebElement managementOptionsPanel=
		// getwebelement(xml.getlocator(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_ManagementOptionsPanel"));
		// ScrolltoElement(managementOptionsPanel);
		// waitforPagetobeenable();

		// ////scrolltoend();
		waitforPagetobeenable();

		/**
		 * Configuration options panel
		 */

		// VPN Topology
		verifyEnteredvalues("VPN Topology", vpnTopology);

		if (vpnTopology.equals("Point-to-Point")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);

			// Circuit Type
			verifyEnteredvalues("Circuit Type", CircuitType);
		}

		if (vpnTopology.equals("Hub&Spoke")) {

			Reporter.log("only vpn topology displays under view Service page");
			Report.LogInfo("Info",
					" Only VPN topology field displays under 'Configuration' panel, when 'Hub&Spoke' is selected",
					"PASS");

		}

		if (vpnTopology.equals("E-PN (Any-to-Any)")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);

		}

	}

	public void verifydataEntered_Metro_MSPselected(String serviceype, String ServiceIdentificationNumber,
			String SelectSubService, String Interfacespeed, String EndpointCPE, String Email, String PhoneContact,
			String remark, String PerformanceMonitoring, String ProactiveMonitoring, String deliveryChannel,
			String ManagementOrder, String vpnTopology, String intermediateTechnology, String CircuitReference,
			String CircuitType, String modularMSP, String perCocPerfrmReprt, String actelsBased, String standrdCir,
			String standrdEir, String prmiumCir, String prmiumEir, String notificationManagement, String EVPNtechnology,
			String HCoSPerformanceReporting, String ENNIcheckBox, String COScheckbox, String MultiPostcheckbox)
			throws InterruptedException, IOException {

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_OrderPanel);

		waitforPagetobeenable();

		/**
		 * Service Panel
		 */
		// Service Identification
		verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

		// Service type
		verifyEnteredvalues("Service Type", serviceype);

		// Service Subtype
		verifyEnteredvalues("Service Subtype", SelectSubService);

		// E_VPN technology
		verifyEnteredvalues("E-VPN Technology", EVPNtechnology);

		// Email
		verifyEnteredvalueForEmail_serviceCreationpage("Email", Email);

		// COS checkbox
		verifyEnteredvalues("COS", COScheckbox);

		if (COScheckbox.equalsIgnoreCase("Yes")) {
			verifyEnteredvalues("Multiport", MultiPostcheckbox);
		}

		// Phone Contact
		verifyEnteredvalues("Phone Contact", PhoneContact);

		// Modular MSP
		verifyEnteredvalues("Modular MSP", modularMSP);

		// Remark
		compareText("Remark", Lanlink_Metro_Obj.Lanlink_Metro.remark_viewPage, remark);

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_Servicepanel);

		waitforPagetobeenable();

		/**
		 * Management Options panel
		 */

		// Delivery Channel
		verifyEnteredvalues("Delivery Channel", deliveryChannel);

		// Proactive Monitoring
		verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

		if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
			verifyEnteredvalues("Notification Management Team", notificationManagement);
		}

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_ManagementOptionsPanel);

		waitforPagetobeenable();

		// Performance Reporting
		verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

		if (COScheckbox.equalsIgnoreCase("Yes")) {

			if (vpnTopology.equals("Point-to-Point")) {

				if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("yes"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);

					// Standard
					compareText("Header Name for 1st row", Lanlink_Metro_Obj.Lanlink_Metro.standardHeaderName_viewPage,
							"Standard");

					compareText("Standard CIR", Lanlink_Metro_Obj.Lanlink_Metro.standardCIRvalue_viewPage, standrdCir);

					compareText("Standard EIR", Lanlink_Metro_Obj.Lanlink_Metro.standardEIRvalue_viewPage, standrdEir);

					// Premium
					compareText("Header Name for 2nd row", Lanlink_Metro_Obj.Lanlink_Metro.PremisumHeaderName_viewPage,
							"Premium");

					compareText("Premium CIR", Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRvalue_viewPage, prmiumCir);

					compareText("Premium EIR", Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRvalue_viewPage, prmiumEir);

				}
			} else {

				if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("no"))) {

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

				else if ((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes"))
						& (actelsBased.equalsIgnoreCase("yes"))) {

					actelsBased = "No";

					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

					verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);

					verifyEnteredvalues("Actelis Based", actelsBased);
				}

			}
		}

		// WebElement managementOptionsPanel=
		// getwebelement(xml.getlocator(Lanlink_Metro_Obj.Lanlink_Metro.viewServicepage_ManagementOptionsPanel"));
		// ScrolltoElement(managementOptionsPanel);
		// waitforPagetobeenable();

		// ////scrolltoend();
		waitforPagetobeenable();

		/**
		 * Configuration options panel
		 */

		// VPN Topology
		verifyEnteredvalues("VPN Topology", vpnTopology);

		if (vpnTopology.equals("Point-to-Point")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);

			// Circuit Type
			verifyEnteredvalues("Circuit Type", CircuitType);
		}

		if (vpnTopology.equals("Hub&Spoke")) {

			Reporter.log("only vpn topology displays under view Service page");
			Report.LogInfo("Info",
					" Only VPN topology field displays under 'Configuration' panel, when 'Hub&Spoke' is selected",
					"PASS");

		}

		if (vpnTopology.equals("E-PN (Any-to-Any)")) {

			// Circuit Reference
			verifyEnteredvalues("Circuit Reference", CircuitReference);
		}
	}

	public void Fieldvalidation_DirectFibre10G(String serviceType, String SelectSubService, String Interfacespeed,
			String vpntopology) throws InterruptedException, IOException {

		String[] deliverychannel = { "--", "Retail", "Reseller", "WLC", "WLEC", "CES Solutions" };

		String[] VPNtopology = { "Point-to-Point", "Hub&Spoke", "E-PN (Any-to-Any)" };

		String[] notifyManagement = { "DNA" };

		boolean serviceIdentificationField, ServiceType, ServiceSubtype, interfacespeedvalue, singleendpointCPE, email,
				phone, remark, performancereoprting, deliveryChanel, proactiveMonitor, Managementorder, vpnTopology,
				intermediateTechnology, circuitref, circuitType, okButton, cancelButton;

		try {

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferenceErrmsg, "Circuit Reference");

			scrollIntoTop();
			waitforPagetobeenable();

			// Service Identification Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.serviceIdentificationerrmsg, "Service Identification");

			// service Identification
			serviceIdentificationField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);
			sa.assertTrue(serviceIdentificationField, "Service identification field is not displayed");
			if (serviceIdentificationField) {
				Report.LogInfo("Info",
						" ' Service Identfication' mandatory field is displaynig under 'Add Service' page as expected",
						"PASS");
			} else {
				Report.LogInfo("Info",
						" ' Service Identfication' mandatory field is not available under 'Add Service' page", "FAIL");
			}

			// Service type
			ServiceType = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceType);
			waitforPagetobeenable();
			sa.assertTrue(ServiceType, "Service type is not displayed");
			if (ServiceType) {
				Report.LogInfo("Info", " 'LANLink' is displying under 'Service type' as expected", "PASS");
			} else {
				Report.LogInfo("Info", " 'LANLink' is not displying under 'Service type'", "FAIL");
			}

			// Service subtype
			ServiceSubtype = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + SelectSubService
					+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);

			sa.assertTrue(ServiceSubtype, "Service subtype is not displayed");
			if (ServiceSubtype) {
				Report.LogInfo("Info", SelectSubService + " is displying under 'Service Sub type' as expected", "PASS");
			} else {
				Report.LogInfo("Info", SelectSubService + " is not displying under 'Service Sub type'", "FAIL");
			}

			// Interface speed
			interfacespeedvalue = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + Interfacespeed
					+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);

			sa.assertTrue(interfacespeedvalue, "Interface speed dropdown is not displaying as expected");
			if (interfacespeedvalue) {
				Report.LogInfo("Info", Interfacespeed + " is displying under 'Interface Speed' as expected", "PASS");
			} else {
				Report.LogInfo("Info", Interfacespeed + " is not displying under 'Interface Speed'", "FAIL");
			}

			// Single endpoint cpe
			try {
				singleendpointCPE = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.EndpointCPE);
				sa.assertTrue(singleendpointCPE, "single End point CPE checkbox is disabled by default");
				if (singleendpointCPE) {
					Report.LogInfo("Info",
							" 'Single endpoint cpe' checkbox is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info",
							" 'Single endpoint cpe' checkbox is not available under 'Create Service' page", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Single endpoint cpe' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

		
			// Email
			try {
				email = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Email);
				sa.assertTrue(email, "email field is not displayed");
				if (email) {
					Report.LogInfo("Info", " 'Email' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'Email' field is not available under 'Create Service' pag", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Email' field is not available under 'Create Service' page", "FAIL");
			}

			// phone
			try {
				phone = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact);
				sa.assertTrue(phone, "phone contact field is not displayed");
				if (phone) {
					Report.LogInfo("Info", " 'phone' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'phone' field is not available under 'Create Service' page", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Phone Contact' field is not available under 'Create Service' page", "FAIL");
			}

			// remark
			try {
				remark = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Remark);
				sa.assertTrue(remark, "remark field is not displayed");
				if (remark) {
					Report.LogInfo("Info", " 'Remark' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'Remark' field is not available under 'Create Service' page", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Remark' field is not available under 'Create Service' page", "FAIL");
			}

			// scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.configurtoinptionpanel-webelementToScroll);

			// performance Reporting
			try {
				performancereoprting = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
				sa.assertTrue(performancereoprting,
						"performance monitoring checbox is not displayed and by default not selected as expected");
				if (performancereoprting) {
					Report.LogInfo("Info",
							" 'performance Reporting' checkbox is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info",
							" 'performance Reporting' checkbox is not available under 'Create Service' page", "FAIL");
				}

				boolean performancereoprtingselection = isSelected(
						Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox, "Performance reporting checkbox");
				if (performancereoprtingselection) {
					Report.LogInfo("Info",
							" 'Performance Reporting' checkbox is selected under 'Management Options' panel in 'Create Service page'",
							"FAIL");
				} else {
					Report.LogInfo("Info",
							"performance Reporting' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected",
							"PASS");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Performance reporting' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

			// proactive monitoring
			proactiveMonitor = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
			sa.assertTrue(proactiveMonitor, "pro active monitoring checkbox is not displayed");
			if (proactiveMonitor) {
				Report.LogInfo("Info",
						" 'proactive monitoring' checkbox is displying under 'Create Service'page as expected", "PASS");

				boolean proactiveMonitorselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring,
						"Proactive monitoring");
				if (proactiveMonitorselection) {
					Report.LogInfo("Info",
							" 'proactive monitoring' checkbox is selected under 'Management Options' panel in 'Create Service page'",
							"FAIL");
				} else {
					Report.LogInfo("Info",
							"proactive monitoring' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected",
							"PASS");

					// Notification Management Dropdown
					click(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring, "Proactive monitoring");
					Reporter.log("Pro active monitoring check box is selected");
					waitforPagetobeenable();

					boolean notificationManagement = isElementPresent(
							Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
					sa.assertTrue(notificationManagement,
							"Notification management dropdown is not displayed when proactive monitoring is selected");
					Reporter.log(
							"Notification management dropdown gets displayed when proactive monitoring is selected");

					if (notificationManagement) {
						Report.LogInfo("Info",
								" 'Notification Management' dropdown is displaying under 'Management Options' panel when 'proactive Monitoring' checkbox is selected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
						try {
							List<String> listofnotificationmanagement = new ArrayList<>(Arrays
									.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofnotificationmanagement)));

							for (String notificationmanagementtypes : listofnotificationmanagement) {

								boolean match = false;
								for (int i = 0; i < notifyManagement.length; i++) {
									if (notificationmanagementtypes.equals(notifyManagement[i])) {
										match = true;
										Reporter.log(
												"list of notification management are : " + notificationmanagementtypes);
										Report.LogInfo("Info",
												"list of notification management are : " + notificationmanagementtypes,
												"PASS");
									}
								}
								sa.assertTrue(match, "");

							}
						} catch (Exception e) {
							Reporter.log("Notification Management dropdown values are mismatching");
							e.printStackTrace();
							Report.LogInfo("Info",
									"  values in Notification management dropdown under Direct Fiber service subtype is not displaying as expected",
									"FAIL");
						}
					} else {
						Report.LogInfo("Info",
								" 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected",
								"FAIL");
					}
				}
			} else {
				Report.LogInfo("Info", " 'proactive monitoring' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

			waitforPagetobeenable();

			// delivery channel
			try {
				deliveryChanel = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
				sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
				if (deliveryChanel) {
					Report.LogInfo("Info",
							" 'Delivery Channel' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected",
							"PASS");

					click(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
					try {
						List<WebElement> listofdeliverychannel = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
						for (WebElement deliverychanneltypes : listofdeliverychannel) {

							boolean match = false;
							for (int i = 0; i < deliverychannel.length; i++) {
								if (deliverychanneltypes.getText().equals(deliverychannel[i])) {
									match = true;
									Reporter.log("list of delivery channel are : " + deliverychanneltypes.getText());
									

								}
							}
							sa.assertTrue(match, "");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Reporter.log("delivery channel dropdown values are mismatching");
						Report.LogInfo("Info",
								"  values in delivery channel dropdowns under Direct Fiber service subtype are not displaying as expected",
								"FAIL");
					}
				} else {
					Report.LogInfo("Info",
							" 'Delivery Channel' dropdown is not avilable under 'Management options' panel in 'Create Service' page",
							"FAIL");
				}

			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Delivery Channel' dropdown is not available under 'Create Service' page",
						"FAIL");
			}

			

			////// scrolltoend();
			waitforPagetobeenable();

			// VPN topology
			vpnTopology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
			sa.assertTrue(vpnTopology, "vpn topology dropdown is not displayed");
			if (vpnTopology) {
				Report.LogInfo("Info",
						" VPN Topology' dropdown is displaying under 'Configuration Options' panel in 'Create Service' page as expected",
						"PASS");

				// Check default values present inside VPN Topology dropdown
				boolean defaultTOpologValues = webDriver.findElement(By.xpath("//span[contains(text(),'Point-to-Point')]")).isDisplayed();
				if (defaultTOpologValues) {
					Report.LogInfo("Info",
							" Under 'VPN Topology' dropdown, 'Point-to-Point' is displaying by default as expected",
							"PASS");
				} else {
					Report.LogInfo("Info",
							" Under 'VPN Topology' dropdown, 'Point-to-Point' is not displaying by default", "FAIL");
				}

				// fine list of values under VPN topology dropdown

				click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology,"Intermediate technology");
				waitforPagetobeenable();

				////// scrolltoend();
				waitforPagetobeenable();

				click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
				waitforPagetobeenable();

				try {
					List<String> listofvpntopology = new ArrayList<>(
							Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofvpntopology)));

					for (String vpntopologytyeps : listofvpntopology) {

						boolean match = false;
						for (int i = 0; i < VPNtopology.length; i++) {
							if (vpntopologytyeps.equals(VPNtopology[i])) {
								match = true;
								Reporter.log("list of vpn topologies are : " + vpntopologytyeps);
								Reporter.log("list of vpn topologies: " + vpntopologytyeps);
								Report.LogInfo("Info", "list of vpn topologies: " + vpntopologytyeps, "PASS");

							}
						}
						sa.assertTrue(match, "");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Reporter.log("vpn topology dropdown values are mismatching");
				}

				for (int i = 0; i < VPNtopology.length; i++) {

					if (VPNtopology[i].equals("E-PN (Any-to-Any)")) {
						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology,"topology");
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//div[contains(text(),'"+VPNtopology[i] +"')]")).click();

						waitforPagetobeenable();

						Reporter.log(
								"Under 'VPN Topology', When 'E-PN (Any-to-Any)' is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
										+ "only Circuit reference field should occur ");

						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup when circuit reference field is clicked is: " + text);
								Report.LogInfo("Info",
										"on clicking circuit reference field, alert popup message displays as: " + text,
										"PASS");

								CircuitReferencepopupalertmsg = false;
								click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
								waitforPagetobeenable();
							}
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'",
									"FAIL");
						}

					}

					else if (VPNtopology[i].equals("Hub&Spoke")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology,"Topology");
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//div[contains(text(),'"+VPNtopology[i] +"')]")).click();

						waitforPagetobeenable();

						Reporter.log(
								"Under 'VPN Topology', When 'Hub&Spoke'is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
										+ "only Circuit reference field should occur ");

						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected,  when 'VPN Topology' selected as 'HUb & Spoke'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup when circuit reference field is clicked is: " + text);
								Report.LogInfo("Info",
										"on clicking circuit reference field, alert popup message displays as: " + text,
										"PASS");

								CircuitReferencepopupalertmsg = false;
								click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
								waitforPagetobeenable();
							}
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'HUb & Spoke'",
									"FAIL");
						}

					} else if (VPNtopology[i].equals("Point-to-Point")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology,"topology");
						waitforPagetobeenable();

						webDriver.findElement(By.xpath("//div[contains(text(),'"+VPNtopology[i] +"')]")).click();;

						waitforPagetobeenable();

						// Intermediate technology field
						try {
							click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology,"Intermediate technology");
							intermediateTechnology = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology);
							sa.assertTrue(intermediateTechnology, "intermediate technology field is not displayed");
							if (intermediateTechnology) {
								Report.LogInfo("Info",
										" 'Intermediate technology' field is displaying under 'Configuration Options' panel in 'Create Service' page, when 'VPN Topology' selected as 'Point-to-point'",
										"PASS");
							} else {
								Report.LogInfo("Info",
										" 'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'",
										"FAIL");
							}
						} catch (Exception e) {
							e.printStackTrace();
							Report.LogInfo("Info",
									"'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'",
									"FAIL");
						}

						// Circuit Reference
						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected, when 'VPN Topology' selected as 'point-to-point'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup on clicking circuit reference field is : " + text);
								Report.LogInfo("Info",
										" on clicking 'Circuit reference' , alert emssage popup as : " + text, "PASS");

								CircuitReferencepopupalertmsg = false;
							}

							click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
							waitforPagetobeenable();
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'Point-to-Point'",
									"FAIL");
						}

						// Circuit type field
						try {
							List<String> listofcircuittypes = new ArrayList<>(
									Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofcircuittypes1)));

							for (String CircuitTypes : listofcircuittypes) {

								Reporter.log("list of circuit types are : " + CircuitTypes);
								Report.LogInfo("Info", "list of circuit types displaying are:  " + CircuitTypes,
										"PASS");

							}
						} catch (Exception e) {
							e.printStackTrace();
							Reporter.log("Circuit type values are mismatching");
							//Report.LogInfo("Info", "list of circuit type values are mismatching", "FAIL");

						}
					}
				}

			} else {
				Report.LogInfo("Info",
						" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
						"FAIL");
			}

			okButton = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.okbutton);
			sa.assertTrue(okButton, "OK button is not displayed");

			cancelButton = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.cancelButton);
			sa.assertTrue(cancelButton, "Cancel button is not displayed");

			////// scrolltoend();
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.cancelButton,"Cancel");
			waitforPagetobeenable();

			sa.assertAll();
			Report.LogInfo("Info", " Fields are verified", "PASS");
		} catch (AssertionError e) {
			e.printStackTrace();
		}

	}

	public void verifyFields_COScheckbox() {

		boolean cosAvailability = false;
		boolean CosSelection = false;
		try {
			cosAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.cosCheckbox_createService);

			if (cosAvailability) {
				Report.LogInfo("Info", " 'COS' checkbox is displaying under 'Create Service'page as expected", "PASS");

				CosSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.cosCheckbox_createService,
						"create service checkbox");
				if (CosSelection) {
					Report.LogInfo("Info", " 'COS' checkbox is selected by default", "FAIL");
					Reporter.log(" 'COS' checkbox is selected by default");
				} else {
					Report.LogInfo("Info", " 'COS' checkbox is not selected by default", "PASS");
					Reporter.log(" 'COS' checkbox is not selected by default");
				}

			} else {
				Report.LogInfo("Info", " 'COS' checkbox is not available under 'Create Service' page", "FAIL");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'COS' chekcbox is not displaying", "FAIL");
			Reporter.log(" 'COS' chekcbox is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " 'COS' checkbox is not avaiable", "PASS");
			Reporter.log(" 'COS' checkbox is not avaiable");
		}
	}

	public void verifyFields_multiPort() throws InterruptedException, IOException {

		boolean multiportAvailability = false;
		boolean multiPOrtSelection = false;

		scrollIntoTop();
		waitforPagetobeenable();

		boolean COScheckbox = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.cosCheckbox_createService,
				"Create service checkbox");
		if (COScheckbox) {
			Report.LogInfo("Info", "CoS checkbox is selected", "PASS");
			Reporter.log("CoS checkbox is selected");
			try {
				multiportAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.multiport_checkbox);
				if (multiportAvailability) {
					Report.LogInfo("Info", " 'Multiport' checkbox is displaying under 'Create Service'page as expected",
							"PASS");

					multiPOrtSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.multiport_checkbox,
							"multiport checkbox");
					if (multiPOrtSelection) {
						Report.LogInfo("Info", " 'Multiport' checkbox is selected by default", "FAIL");
						Reporter.log(" 'Multiport' checkbox is selected by default");
					} else {
						Report.LogInfo("Info", " 'Multiport' checkbox is not selected by default", "PASS");
						Reporter.log(" 'Multiport' checkbox is not selected by default");
					}

				} else {
					Report.LogInfo("Info", " 'Multiport' checkbox is not available under 'Create Service' page",
							"FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", " 'Multiport' checkbox is not available under 'Create Service' page", "FAIL");
				Reporter.log(" 'Multiport' checkbox is not available under 'Create Service' page");
			}

		} else {
			Report.LogInfo("Info", "CoS checkbox is not selected", "FAIL");
			Reporter.log("CoS checkbox is not selected");
		}
	}

	public void verfyFields_ENNIcheckbox() {
		boolean ENNIAvailability = false;
		boolean ENNISelection = false;
		try {
			ENNIAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ENNI_checkbox);

			if (ENNIAvailability) {
				Report.LogInfo("Info", " 'ENNI' checkbox is displaying under 'Create Service'page as expected", "PASS");

				ENNISelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.ENNI_checkbox, "ENNI checkbox");
				if (ENNISelection) {
					Report.LogInfo("Info", " 'ENNI' checkbox is selected by default", "FAIL");
					Reporter.log(" 'ENNI' checkbox is selected by default");
				} else {
					Report.LogInfo("Info", " 'ENNI' checkbox is not selected by default", "PASS");
					Reporter.log(" 'ENNI' checkbox is not selected by default");
				}

			} else {
				Report.LogInfo("Info", " 'ENNI' checkbox is not available under 'Create Service' page", "FAIL");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'ENNI' chekcbox is not displaying", "FAIL");
			Reporter.log(" 'ENNI' chekcbox is not displaying");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " 'ENNI' checkbox is not selected", "PASS");
			Reporter.log(" 'ENNI' checkbox is not selected");
		}
	}

	public void Fieldvalidation_DirectFibre1G(String serviceType, String SelectSubService, String Interfacespeed,
			String proActivemonitoring, String vpntopology) throws InterruptedException, IOException {

		String[] deliverychannel = { "--", "Retail", "Reseller", "WLC", "WLEC", "CES Solutions" };

		String[] VPNtopology = { "Point-to-Point", "Hub&Spoke", "E-PN (Any-to-Any)" };

		String[] notifyManagement = { "DNA" };

		boolean serviceIdentificationField, ServiceType, ServiceSubtype, interfacespeedvalue, singleendpointCPE, email,
				phone, remark, performancereoprting, deliveryChanel, proactiveMonitor, Managementorder, vpnTopology,
				intermediateTechnology, circuitref, circuitType, okButton, cancelButton;

		try {

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferenceErrmsg, "Circuit Reference");

			// Service Identification Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.serviceIdentificationerrmsg, "Service Identification");

			// service Identification
			serviceIdentificationField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);
			sa.assertTrue(serviceIdentificationField, "Service identification field is not displayed");
			if (serviceIdentificationField) {
				Report.LogInfo("Info",
						" ' Service Identfication' mandatory field is displaynig under 'Add Service' page as expected",
						"PASS");
			} else {
				Report.LogInfo("Info",
						" ' Service Identfication' mandatory field is not available under 'Add Service' page", "FAIL");
			}

			// Service type
			ServiceType = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceType);
			sa.assertTrue(ServiceType, "Service type is not displayed");
			if (ServiceType) {
				Report.LogInfo("Info", " 'LANLink' is displying under 'Service type' as expected", "PASS");
			} else {
				Report.LogInfo("Info", " 'LANKLink' is not displying under 'Service type'", "FAIL");
			}

			// Service subtype
			ServiceSubtype = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + SelectSubService
					+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
			sa.assertTrue(ServiceSubtype, "Service subtype is not displayed");
			if (ServiceSubtype) {
				Report.LogInfo("Info", SelectSubService + " is displying under 'Service Sub type' as expected", "PASS");
			} else {
				Report.LogInfo("Info", SelectSubService + " is not displying under 'Service Sub type'", "FAIL");
			}

			// Interface speed
			interfacespeedvalue = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + Interfacespeed
					+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
			sa.assertTrue(interfacespeedvalue, "Interface speed dropdown is not displaying as expected");
			if (interfacespeedvalue) {
				Report.LogInfo("Info", Interfacespeed + " is displying under 'Interface Speed' as expected", "PASS");
			} else {
				Report.LogInfo("Info", Interfacespeed + " is not displying under 'Interface Speed'", "FAIL");
			}

			// Single endpoint cpe
			try {
				singleendpointCPE = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.EndpointCPE);
				sa.assertTrue(singleendpointCPE, "single End point CPE checkbox is disabled by default");
				if (singleendpointCPE) {
					Report.LogInfo("Info",
							" 'Single endpoint cpe' field is displying under 'Create Service'page as expected", "PASS");
				} else {
					Report.LogInfo("Info", " 'Single endpoint cpe' field is not available under 'Create Service' pag",
							"FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Single Endpoint CPE' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

			

			// Email
			try {
				email = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Email);
				sa.assertTrue(email, "email field is not displayed");
				if (email) {
					Report.LogInfo("Info", " 'Email' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'Email' field is not available under 'Create Service' pag", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Email' field is not available under 'Create Service' page", "FAIL");
			}

			// phone
			try {
				phone = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact);
				sa.assertTrue(phone, "phone contact field is not displayed");
				if (phone) {
					Report.LogInfo("Info", " 'phone' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'phone' field is not available under 'Create Service' page", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Phone' field is not available under 'Create Service' page", "FAIL");
			}

			// remark
			try {
				remark = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Remark);
				sa.assertTrue(remark, "remark field is not displayed");
				if (remark) {
					Report.LogInfo("Info", " 'Remark' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'Remark' field is not available under 'Create Service' page", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Remark' field is not available under 'Create Service' page", "FAIL");
			}

			// scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.configurtoinptionpanel-webelementToScroll);

			waitforPagetobeenable();
			// performance Reporting
			performancereoprting = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
			sa.assertTrue(performancereoprting,
					"performance reporting checbox is not displayed under 'Create Service' page");
			if (performancereoprting) {
				Report.LogInfo("Info",
						" 'performance Reporting' checkbox is displying under 'Create Service'page as expected",
						"PASS");

				boolean performancereoprtingselection = isSelected(
						Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox, "Performance reporting checkbox");
				if (performancereoprtingselection) {
					Report.LogInfo("Info",
							" 'Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'",
							"FAIL");
				} else {
					Report.LogInfo("Info",
							"performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected",
							"PASS");

					// Per CoS Performance Reporting
					click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
					waitforPagetobeenable();

					boolean perCosreopt = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
					sa.assertTrue(perCosreopt,
							"Per CoS Performance Reporting checbox is not displayed under 'Create Service' page, when 'Performance Reporting' checkbox is selected");
					if (perCosreopt) {
						Report.LogInfo("Info",
								"Per CoS Performance Reporting checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected",
								"PASS");

						boolean perCoSselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport,
								"Per CoS Performance Reporting");
						if (perCoSselection) {
							Report.LogInfo("Info",
									" 'Per CoS Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'",
									"FAIL");
						} else {
							Report.LogInfo("Info",
									"Per CoS Performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected",
									"PASS");

							// Actelis Based
							click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
							waitforPagetobeenable();
							boolean actelisBasedcheckbox = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
							sa.assertTrue(actelisBasedcheckbox,
									" 'Atelis Based' checbox is not displayed under 'Create Service' page, when 'Per CoS Performance Reporting' checkbox is selected");
							if (actelisBasedcheckbox) {
								Report.LogInfo("Info",
										" 'Actelis Based' checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected",
										"PASS");

								boolean actelisBasedselection = isSelected(
										Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox, "Actelis Based checkbox");
								if (actelisBasedselection) {
									Report.LogInfo("Info",
											" 'Actelis Based' checkbox is selected by default under 'Management Options' panel in 'Create Service page'",
											"FAIL");
								} else {
									Report.LogInfo("Info",
											"Actelis Based' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected",
											"PASS");

									// Bandwidth Options table
									click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
									waitforPagetobeenable();

									// Standard CIR text field
									boolean standardCIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield);
									sa.assertTrue(standardCIR,
											" 'Standard CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (standardCIR) {
										Report.LogInfo("Info",
												" 'Standard CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Standard CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

									// Standard EIR Text field
									boolean standardEIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield);
									sa.assertTrue(standardEIR,
											" 'Standard EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (standardEIR) {
										Report.LogInfo("Info",
												" 'Standard EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Standard EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

									// Premium CIR text field
									boolean premiumCIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield);
									sa.assertTrue(premiumCIR,
											" 'Premium CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (premiumCIR) {
										Report.LogInfo("Info",
												" 'Premium CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Premium CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

									// Premium EIR Text field
									boolean premiumEIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield);
									sa.assertTrue(premiumEIR,
											" 'Premium EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (premiumEIR) {
										Report.LogInfo("Info",
												" 'Premium EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Premium EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

								}
							} else {
								Report.LogInfo("Info",
										" 'Actelis Based' checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected",
										"PASS");
							}
						}
					} else {
						Report.LogInfo("Info",
								"Per CoS Performance Reporting checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected",
								"FAIL");
					}
				}
			} else {
				Report.LogInfo("Info", " 'performance Reporting' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

			// proactive monitoring
			proactiveMonitor = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
			sa.assertTrue(proactiveMonitor, "pro active monitoring checkbox is not displayed");
			if (proactiveMonitor) {
				Report.LogInfo("Info",
						" 'proactive monitoring' checkbox is displying under 'Create Service'page as expected", "PASS");

				boolean proactiveMonitorselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring,
						"proactive Monitoring");
				if (proactiveMonitorselection) {
					Report.LogInfo("Info",
							" 'proactive monitoring' checkbox is selected under 'Management Options' panel in 'Create Service page'",
							"FAIL");
				} else {
					Report.LogInfo("Info",
							"proactive monitoring' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected",
							"PASS");

					// Notification Management Dropdown
					click(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
					Reporter.log("Pro active monitoring check box is selected");
					waitforPagetobeenable();

					boolean notificationManagement = isElementPresent(
							Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
					sa.assertTrue(notificationManagement,
							"Notification management dropdown is not displayed when proactive monitoring is selected");
					Reporter.log(
							"Notification management dropdown gets displayed when proactive monitoring is selected");
					if (notificationManagement) {
						Report.LogInfo("Info",
								" 'Notification Management' dropdown is displaying under 'Management Options' panel when 'proactive Monitoring' checkbox is selected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
						try {
							List<String> listofnotificationmanagement = new ArrayList<>(Arrays
									.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofnotificationmanagement)));

							for (String notificationmanagementtypes : listofnotificationmanagement) {

								boolean match = false;
								for (int i = 0; i < notifyManagement.length; i++) {
									if (notificationmanagementtypes.equals(notifyManagement[i])) {
										match = true;
										Reporter.log(
												"list of notification management are : " + notificationmanagementtypes);
										Report.LogInfo("Info",
												"list of notification management are : " + notificationmanagementtypes,
												"PASS");
									}
								}
								sa.assertTrue(match, "");

							}
						} catch (Exception e) {
							Reporter.log("Notification Management dropdown values are mismatching");
							e.printStackTrace();
							Report.LogInfo("Info",
									"  values in Notification management dropdown under Direct Fiber service subtype is not displaying as expected",
									"FAIL");
						}
					} else {
						Report.LogInfo("Info",
								" 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected",
								"FAIL");
					}
				}
			} else {
				Report.LogInfo("Info", " 'proactive monitoring' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

			waitforPagetobeenable();

			// delivery channel
			try {
				deliveryChanel = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
				sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
				if (deliveryChanel) {
					Report.LogInfo("Info",
							" 'Delivery Channel' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected",
							"PASS");

					click(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey1);
					try {
						List<String> listofdeliverychannel = new ArrayList<>(
								Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofdeliverychannel)));

						for (String deliverychanneltypes : listofdeliverychannel) {

							boolean match = false;
							for (int i = 0; i < deliverychannel.length; i++) {
								if (deliverychanneltypes.equals(deliverychannel[i])) {
									match = true;
									Reporter.log("list of delivery channel are : " + deliverychanneltypes);

								}
							}
							sa.assertTrue(match, "");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Reporter.log("delivery channel dropdown values are mismatching");
					//	Report.LogInfo("Info","  values in delivery channel dropdowns under Direct Fiber service subtype are not displaying as expected","FAIL");
					}
				} else {
					Report.LogInfo("Info",
							" 'Delivery Channel' dropdown is not avilable under 'Management options' panel in 'Create Service' page",
							"FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Delivery Channel' dropdown is not available under 'Create Service' page",
						"FAIL");
			}

			// Management Order
			try {
				Managementorder = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.managementOrder);
				sa.assertTrue(Managementorder, "Management order field is not displayed");
				if (Managementorder) {
					Report.LogInfo("Info",
							" 'Management Order' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected",
							"PASS");

					click(Lanlink_Metro_Obj.Lanlink_Metro.managementOrder);

					try {
						List<String> listofmanagementOrder = new ArrayList<>(
								Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofmanagementOrder)));

						for (String mnaagementOrdertypes : listofmanagementOrder) {

							Reporter.log("Available Management Order name is : " + mnaagementOrdertypes);
							Report.LogInfo("Info", "Available Management Order name is : " + mnaagementOrdertypes,
									"PASS");
							Reporter.log("Available Management Order name is :" + mnaagementOrdertypes);

						}
					} catch (Exception e) {
						e.printStackTrace();
						Reporter.log(" 'Management Order' dropdown values are mismatching");
					}
				} else {
					Report.LogInfo("Info",
							" 'Management Order' dropdown is not available under 'Management options' panel in 'Create Service' page",
							"FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Management Order' dropdown is not available under 'Create Service' page",
						"FAIL");
			}

			// ENNI checkbox
			verfyFields_ENNIcheckbox();

			////// scrolltoend();
			waitforPagetobeenable();

			// VPN topology
			vpnTopology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
			sa.assertTrue(vpnTopology, "vpn topology dropdown is not displayed");
			if (vpnTopology) {
				Report.LogInfo("Info",
						" VPN Topology' dropdown is displaying under 'Configuration Options' panel in 'Create Service' page as expected",
						"PASS");
				Reporter.log("VPN topology dropdown is displaying as expected");

				// Check default values present inside VPN Topology dropdown
				boolean defaultTOpologValues = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.defaultTOpologValues);
				if (defaultTOpologValues) {
					Report.LogInfo("Info",
							" Under 'VPN Topology' dropdown, 'Point-to-Point' is displaying by default as expected",
							"PASS");
					Reporter.log("The default topology value is displaying as :" + defaultTOpologValues);
				} else {
					Report.LogInfo("Info",
							" Under 'VPN Topology' dropdown, 'Point-to-Point' is not displaying by default", "FAIL");
				}

				click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology,"Intermediate technology");
				waitforPagetobeenable();

				////// scrolltoend();
				waitforPagetobeenable();

				// find list of values under VPN topology dropdown
				click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);

				try {
					List<String> listofvpntopology = new ArrayList<>(
							Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofvpntopology)));
					for (String vpntopologytyeps : listofvpntopology) {

						boolean match = false;
						for (int i = 0; i < VPNtopology.length; i++) {
							if (vpntopologytyeps.equals(VPNtopology[i])) {
								match = true;
								Reporter.log("list of vpn topologies are : " + vpntopologytyeps);
								Reporter.log("list of vpn topologies: " + vpntopologytyeps);
								Report.LogInfo("Info", "list of vpn topologies: " + vpntopologytyeps, "PASS");

							}
						}
						sa.assertTrue(match, "");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Reporter.log("vpn topology dropdown values are mismatching");
				}

				for (int i = 0; i < VPNtopology.length; i++) {

					////// scrolltoend();
					waitforPagetobeenable();

					Reporter.log("VPN Toplogy length is: " + VPNtopology.length);
					Reporter.log(VPNtopology[i] + " is the values going to pass inside vpn topology dropdown");

					if (VPNtopology[i].equals("E-PN (Any-to-Any)")) {
						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
						waitforPagetobeenable();
						click(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + VPNtopology[i]
								+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
						waitforPagetobeenable();

						Reporter.log(
								"Under 'VPN Topology', When 'E-PN (Any-to-Any)' is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
										+ "only Circuit reference field should occur ");

						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup when circuit reference field is clicked is: " + text);
								Report.LogInfo("Info",
										"on clicking circuit reference field, alert popup message displays as: " + text,
										"PASS");

								CircuitReferencepopupalertmsg = false;
								click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
								waitforPagetobeenable();
							}
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'",
									"FAIL");
						}

					}

					else if (VPNtopology[i].equals("Hub&Spoke")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
						waitforPagetobeenable();
						click(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + VPNtopology[i]
								+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
						waitforPagetobeenable();

						Reporter.log(
								"Under 'VPN Topology', When 'Hub&Spoke'is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
										+ "only Circuit reference field should occur ");

						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected,  when 'VPN Topology' selected as 'HUb & Spoke'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup when circuit reference field is clicked is: " + text);
								Report.LogInfo("Info",
										"on clicking circuit reference field, alert popup message displays as: " + text,
										"PASS");

								CircuitReferencepopupalertmsg = false;
								click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
								waitforPagetobeenable();
							}
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'HUb & Spoke'",
									"FAIL");
						}

					} else if (VPNtopology[i].equals("Point-to-Point")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//div[contains(text(),'"+VPNtopology[i] +"')]")).click();
						waitforPagetobeenable();

						// Intermediate technology field
						try {
							click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology,"Intermediate technology");
							intermediateTechnology = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology);
							sa.assertTrue(intermediateTechnology, "intermediate technology field is not displayed");
							if (intermediateTechnology) {
								Report.LogInfo("Info",
										" 'Intermediate technology' field is displaying under 'Configuration Options' panel in 'Create Service' page, when 'VPN Topology' selected as 'Point-to-point'",
										"PASS");
							} else {
								Report.LogInfo("Info",
										" 'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'",
										"FAIL");
							}
						} catch (Exception e) {
							e.printStackTrace();
							Report.LogInfo("Info",
									" 'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'",
									"FAIL");
						}

						// Circuit Reference
						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected, when 'VPN Topology' selected as 'point-to-point'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup on clicking circuit reference field is : " + text);
								Report.LogInfo("Info",
										" on clicking 'Circuit reference' , alert emssage popup as : " + text, "PASS");

								CircuitReferencepopupalertmsg = false;
							}

							click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
							waitforPagetobeenable();
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'Point-to-Point'",
									"FAIL");
						}

						// Circuit type field
						try {
							List<String> listofcircuittypes = new ArrayList<>(
									Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofcircuittypes)));

							for (String CircuitTypes : listofcircuittypes) {

								Reporter.log("list of circuit types are : " + CircuitTypes);
								Report.LogInfo("Info", "list of circuit types displaying are:  " + CircuitTypes,
										"PASS");

								Reporter.log("list of circuit types displaying are:  " + CircuitTypes);

							}
						} catch (Exception e) {
							e.printStackTrace();
							Reporter.log("Circuit type values are mismatching");
						//	//Report.LogInfo("Info", "list of circuit type values are mismatching", "FAIL");

						}
					}
				}

			} else {
				Report.LogInfo("Info",
						" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
						"FAIL");
			}

			okButton = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.okbutton);
			sa.assertTrue(okButton, "OK button is not displayed");

			cancelButton = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.cancelButton);
			sa.assertTrue(cancelButton, "Cancel button is not displayed");

			////// scrolltoend();
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.cancelButton,"Cancel");
			

			sa.assertAll();
			Report.LogInfo("Info", " Fields are verified", "PASS");
		} catch (AssertionError e) {
			e.printStackTrace();
		}

	}

	public void valiadateDCAEnabledsite_AddSieOrder() throws InterruptedException, IOException {

		String[] cloudServiceprovider = { "Amazon Web Service", "Microsoft Azure" };
		boolean DCAEnabledsite = false;

		// DCA Enabled site
		DCAEnabledsite = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox);
		sa.assertTrue(DCAEnabledsite, "DCA enabled site is not displayed ");
		if (DCAEnabledsite) {

			Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is displaying under 'Add Site order' page as expected",
					"PASS");
			boolean DCAselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox,
					"DCA enabled sitecheckbox");
			sa.assertFalse(DCAselection, "DCA checkbox under Addsite order is selected by default");
			if (DCAselection) {
				Report.LogInfo("Info", " ' DCA Enabled Site' checkbox should not be selected by default", "FAIL");
			} else {
				Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not selected by default as expected", "PASS");

				waitforPagetobeenable();
				// For Cloud service provider
				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox);
				Reporter.log("DCA site is enabled to add cloud service provider details");
				waitforPagetobeenable();

				boolean DCAafterSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox,
						"DCA enabled site checkbox");
				if (DCAafterSelection) {
					Report.LogInfo("Info", "DCA site is selected to add cloud service provider details", "PASS");
					waitforPagetobeenable();

					boolean cloudservice = isElementPresent(
							Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cloudserviceProvider);
					sa.assertTrue(cloudservice, "cloud service provider dropdown is not displayed");
					if (cloudservice) {
						Report.LogInfo("Info",
								" 'Cloud Service Provider' dropdown is displaying when 'DCA Enabled Site' checkbox is selected as expected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cloudserviceProvider);
						List<String> listofcloudservices = new ArrayList<>(
								Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.listofcloudservices)));

						if (listofcloudservices.size() > 0) {
							for (String cloudserviceprovidertypes : listofcloudservices) {

								boolean match = false;
								for (int i = 0; i < cloudServiceprovider.length; i++) {
									if (cloudserviceprovidertypes.equals(cloudServiceprovider[i])) {
										match = true;
										Reporter.log(
												"list of cloud service providers are : " + cloudserviceprovidertypes);
										Report.LogInfo("Info",
												"The list of cloudservice provider inside dropdown while  adding site order is: "
														+ cloudserviceprovidertypes,
												"");
									}
								}
								sa.assertTrue(match, "");
							}
						} else {
							Reporter.log(
									"no values are available inside cloudservice provider dropdown for Add site order");
							Report.LogInfo("Info",
									"no values are available inside cloudservice provider dropdown for Add site order",
									"FAIL");

						}
					} else {
						Report.LogInfo("Info",
								" 'Cloud Service Provider' dropdown is not available when 'DCA Enabled Site' checkbox is selected",
								"PASS");
					}

				} else {
					Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not getting selected", "FAIL");
				}
			}

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox);
			waitforPagetobeenable();

		} else {
			Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not available under 'Add Site order' page", "FAIL");
		}
	}

	public void VLANid_AddSiteOrder(String VLANid) throws InterruptedException, IOException {

		if (VLANid.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", "No values entered for 'Vlan id' field", "PASS");
			Reporter.log("No values entered for 'Vlan id' field");
		} else {
			try {
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Vlanid, VLANid, "VLANid");
				waitforPagetobeenable();

				String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Vlanid, "value");
				Report.LogInfo("Info", actualvalue + " is entered under Vlan id field", "PASS");
				Reporter.log(actualvalue + " is entered under Vlan id field");

			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", " 'Vlan Id' field is not displating under 'Add Site order' page", "FAIL");
				Reporter.log(" 'Vlan Id' field is not displating under 'Add Site order' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to enter value under 'Vlan Id' field", "FAIL");
				Reporter.log(" Not able to enter value under 'Vlan Id' field");
			}
		}

	}

	public void metro_ModularMSpselected(String ServiceIdentificationNumber, String SelectSubService,
			String Interfacespeed, String EndpointCPE, String Email, String PhoneContact, String remark,
			String PerformanceReporting, String ProactiveMonitoring, String deliveryChannel, String ManagementOrder,
			String vpnTopology, String intermediateTechnology, String CircuitReference, String CircuitType,
			String notificationManagement, String perCocPerfrmReprt, String actelsBased, String standrdCir,
			String standrdEir, String prmiumCir, String prmiumEir, String E_VPntechnology, String HCoSperformreporting,
			String COScheckbox, String Multiportcheckbox, String ENNIcheckbox)
			throws InterruptedException, IOException {

		scrollIntoTop();
		waitforPagetobeenable();

		// Service Identification
		createService_ServiceIdentification(ServiceIdentificationNumber);

		// E_VPN technology dropdown
		createService_EVPNtechnology(E_VPntechnology);

		// Email
		createSerivce_email(Email);

		// Phone Contact
		createService_phone(PhoneContact);

		// COS checkbox
		addCheckbox_commonMethod(Lanlink_Metro_Obj.Lanlink_Metro.cosCheckbox_createService, "COS", COScheckbox);

		// Multiport
		if (COScheckbox.equalsIgnoreCase("Yes")) {
			addCheckbox_commonMethod(Lanlink_Metro_Obj.Lanlink_Metro.multiport_checkbox, "Multiport", Multiportcheckbox);
		}

		// Remark
		createService_remark(remark);

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.configurtoinptionpanel_webelementToScroll);

		// Performance Reporting
		if (!PerformanceReporting.equalsIgnoreCase("null")) {

			if (PerformanceReporting.equalsIgnoreCase("yes")) {

				boolean perfrmReprtFieldcheck = isElementPresent(
						Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
				if (perfrmReprtFieldcheck) {
					Report.LogInfo("Info",
							" 'Performance reporting' checkbox is displaying under 'Manage ment options' panel in 'Create Service' page as exepcted",
							"PASS");

					boolean prfrmReportselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox,
							"performance Reporting checkbox");
					if (prfrmReportselection) {
						Reporter.log("performance monitoring check box is selected");
						Report.LogInfo("Info", "Performance Reporting checkbox is selected as expected", "PASS");
					} else {
						Report.LogInfo("Info", "Performance Reporting checkbox is not selected", "FAIL");
					}

					// HCoS Performance reporting checkbox
					boolean HCoSperformancereportingFieldCheck = false;
					try {
						HCoSperformancereportingFieldCheck = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport);
						if (HCoSperformancereportingFieldCheck) {
							Report.LogInfo("Info",
									" 'HCoS Performance Reporting' checkbox is displaying, when 'Performance reporting' checkbox is selected",
									"PASS");
							if (HCoSperformreporting.equalsIgnoreCase("Yes")) {
								click(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport);
								waitforPagetobeenable();

								boolean HCoSSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport,
										"HCoSperformanceReport");
								if (HCoSSelection) {
									Report.LogInfo("Info",
											"'HCoS Performance Reporting checkbox is selected as expected", "PASS");
								} else {
									Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not selected",
											"FAIL");
								}
							} else {
								Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not selected", "PASS");
							}
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not displaying", "PASS");
						Reporter.log(" 'HCoS Performance Reporting checkbox is not displaying");
					} catch (Exception ee) {
						ee.printStackTrace();
						Report.LogInfo("Info", " 'HCoS Performance Reporting checkbox is not selected", "PASS");
						Reporter.log(" 'HCoS Performance Reporting checkbox is not seleted");
					}

					// Per CoS Performance Reporting chekcbox
					boolean perCoSPrfrmReprtFieldcheck = isElementPresent(
							Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
					if (perCoSPrfrmReprtFieldcheck) {
						Report.LogInfo("Info",
								" 'Per CoS Performance Reporting' checkbox is displaying, when 'Performance reporting' checkbox is selected",
								"PASS");
						if (perCocPerfrmReprt.equalsIgnoreCase("Yes")) {
							click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
							waitforPagetobeenable();

							boolean perCoSSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport,
									"perCoSperformncereport");
							if (perCoSSelection) {
								Report.LogInfo("Info", "Per CoS Performance Reporting checkbox is selected as expected",
										"PASS");
							} else {
								Report.LogInfo("Info", "Per CoS Performance Reporting checkbox is not selected",
										"FAIL");
							}

							// Actelis Based
							boolean actelisbasedFieldcheck = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
							if (actelisbasedFieldcheck) {
								Report.LogInfo("Info",
										" 'Actelis Based' checkbox is displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
										"PASS");
								if (actelsBased.equalsIgnoreCase("Yes")) {
									click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
									waitforPagetobeenable();

									boolean actelisSelection = isSelected(
											Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox, "ActelisBasedcheckbox");
									if (actelisSelection) {
										Report.LogInfo("Info", " 'Actelis Based' checkbox is selected as expected",
												"PASS");
									} else {
										Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected", "FAIL");
									}

									// Standard CIR Text Field
									createService_standardCIR(standrdCir);

									// Standard EIR Text Field
									createService_standardEIR(standrdEir);

									// Premium CIR Text Field
									createService_premiumCIR(prmiumCir);

									// Premium EIR Text Field
									createService_premiumEIR(prmiumEir);

								} else {
									Report.LogInfo("Info", " 'Actelis Based' checkbox is not selected as expected",
											"PASS");
								}

							} else {
								Report.LogInfo("Info",
										" 'Actelis Based' checkbox is not displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected",
										"FAIL");
							}

						} else {
							Report.LogInfo("Info",
									" 'Per CoS Performance Reporting' checkbox is not selected as exepected", "PASS");
						}

					} else {
						Report.LogInfo("Info",
								" 'Per CoS Performance Rpeorting' checkbox is not displaying, when 'Performance reporting' checkbox is selected",
								"FAIL");
					}

				} else {
					Report.LogInfo("Info", " 'Performance Reporting' checkbox is not available", "FAIL");
				}

			} else {

				Reporter.log("Performance Repoting is not selected");
				Report.LogInfo("Info", "performance Reporting checkbox is not selected as expected", "PASS");

			}

		}

		// Pro active Monitoring
		createService_proactivemonitoring(ProactiveMonitoring, notificationManagement);

		// Scroll Up till phone contact field
		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

		waitforPagetobeenable();

		// Delivery Channel
		createService_deliveryChannel(deliveryChannel);

		// Management Order
		createService_managementOptions(ManagementOrder);

		// ENNI checkbox
		//addCheckbox_commonMethod(Lanlink_Metro_Obj.Lanlink_Metro.ENNI_checkbox, "ENNI", ENNIcheckbox);

		////// scrolltoend();
		waitforPagetobeenable();

		// VPN topology
		boolean vpNTopology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
		if (vpNTopology) {
			Report.LogInfo("Info", " 'VPN Topology' dropdown is dsiplaying as expected", "PASS");
			if (!vpnTopology.equalsIgnoreCase("null")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology_xbutton);
				waitforPagetobeenable();

				if (vpnTopology.equals("Point-to-Point")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();

					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Intermediate technology
					createService_intermediateTechnology(intermediateTechnology);

					// Circuit Reference
					createService_circuitreference(CircuitReference);

					// Circuit Type
					createSerivce_circuitType(CircuitType);
				}

				else if (vpnTopology.equals("E-PN (Any-to-Any)")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();

					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					// Circuit Reference
					createService_circuitreference(CircuitReference);

				} else if (vpnTopology.equals("Hub&Spoke")) {

					webDriver.findElement(By.xpath("//div[text()='"+ vpnTopology + "']")).click();
					waitforPagetobeenable();

					Report.LogInfo("Info", vpnTopology + " is selected under 'VPN topology' dropdown", "PASS");

					Reporter.log("NO additional fields display");

				}

				else {

					Reporter.log(vpnTopology + " is not available under VPN topoloy dropdown");
					Reporter.log(vpnTopology + " is not available inside the VPN topoloy dropdown");
					Report.LogInfo("Info", vpnTopology + " is not available under VPN topoloy dropdown", "FAIL");
				}

			} else {
				Report.LogInfo("Info",
						" No value provided for 'VPN topology' dropdown. 'point -to -point' is selected by default",
						"PASS");

				// Intermediate technology
				createService_intermediateTechnology(intermediateTechnology);

				// Circuit Reference
				createService_circuitreference(CircuitReference);

				// Circuit Type
				createSerivce_circuitType(CircuitType);
			}
		} else {
			Report.LogInfo("Info",
					" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
					"FAIL");
			Reporter.log(
					"'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");

	}

	public void createService_EVPNtechnology(String E_VPNtechnology) throws InterruptedException, IOException {

		boolean EVPNavaiability = false;
		try {
			EVPNavaiability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.E_VPNtechnologyDropdown);
			sa.assertTrue(EVPNavaiability, " 'E_VPN tehnology' dropdown is not displayed");
			if (EVPNavaiability) {
				Report.LogInfo("Info", " 'E_VPN tehnology' dropdown is displaying as expected", "PASS");
				Reporter.log(" 'E_VPN tehnology' dropdown is displaying as expected");
				if (!E_VPNtechnology.equalsIgnoreCase("null")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.E_VPNtechnologyDropdown);
					waitforPagetobeenable();
					click(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + E_VPNtechnology
							+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
					waitforPagetobeenable();
					Report.LogInfo("Info", E_VPNtechnology + " is selected under 'E_VPN tehnology' dropdown", "PASS");
					Reporter.log(E_VPNtechnology + " is selected under 'E_VPN tehnology' dropdown");

				} else {
					Report.LogInfo("Info", "No value selected under 'E_VPN tehnology' dropdown", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'E_VPN tehnology' dropdown is not dispalying under 'create Serice' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log(" 'E_VPN tehnology' dropdown is not dispalying");
			Report.LogInfo("Info", " 'E_VPN tehnology' dropdown is not dispalying under 'create Serice' page", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to select value under 'E_VPN tehnology' dropodwn", "FAIL");
			Reporter.log(" Not able to select value under 'E_VPN tehnology' dropodwn");
		}
	}

	public void editService_serviceIdentification(String ServiceIdentificationNumber)
			throws InterruptedException, IOException {

		boolean serviceAvailability = false;
		serviceAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

		if (serviceAvailability) {

			Report.LogInfo("Info", " 'Service Identification' field is displaying in 'Edit Service'page as expected",
					"PASS");

			if (!ServiceIdentificationNumber.equalsIgnoreCase("null")) {

				clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);
				waitforPagetobeenable();
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification, ServiceIdentificationNumber,
						"Service Identification Number");

				String Actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification, "value");
				Report.LogInfo("Info", "Edited value for 'Service Identification' field is " + Actualvalue, "PASS");

			} else {
				Report.LogInfo("Info", " Service Identification field value is not edited", "PASS");
				Reporter.log(" Service Identification field value is not edited");
			}
		} else {
			Report.LogInfo("Info", " 'Service Identification' field is not availale in 'Edit Service'page", "FAIL");
		}
	}

	public void editService_singleEndPointCPE(String EndpointCPE, String Topology)
			throws InterruptedException, IOException {
		boolean CPEAvailability = false;

		if (Topology.equalsIgnoreCase("Point-to-Point")) {

			try {
				CPEAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.EndpointCPE);
				if (CPEAvailability) {

					Report.LogInfo("Info", "Single EndPoint CPE checkbox is displaying under 'Edit Service' page",
							"PASS");

					if (!EndpointCPE.equalsIgnoreCase("null")) {

						boolean endpoint = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.EndpointCPE, "EndpointCPE");
						waitforPagetobeenable();

						if (EndpointCPE.equalsIgnoreCase("yes")) {

							if (endpoint) {

								Report.LogInfo("Info",
										"Endpoint CPE is not edited. It is already Selected while creating.", "PASS");

							} else {

								click(Lanlink_Metro_Obj.Lanlink_Metro.EndpointCPE);
								Reporter.log("End point CPE check box is edited and it got selected");
								Report.LogInfo("Info", "Endpoint CE is edited and it got selected", "PASS");
							}
						} else if (EndpointCPE.equalsIgnoreCase("no")) {
							if (endpoint) {

								click(Lanlink_Metro_Obj.Lanlink_Metro.EndpointCPE);
								Reporter.log("End point CPE check box is unselected");
								Report.LogInfo("Info", "Endpoint CE is edited and it is unselected as Expected",
										"PASS");

							} else {
								Report.LogInfo("Info",
										"Endpoint CPE is not edited. It was not selected during service creation and it remains unselected as expected",
										"PASS");
							}

						}
					} else {
						Report.LogInfo("Info", "No changes made for EndPoint CPE chekbox as expected", "PASS");
					}
				} else {
					Report.LogInfo("Info", "Single EndPoint CPE checkbox is not available under 'Edit Service' page",
							"FAIL");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", "Single EndPoint CPE checkbox is not available under 'Edit Service' page",
						"FAIL");
				Reporter.log("Single EndPoint CPE checkbox is not available under 'Edit Service' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to click on 'Single endpoint CPE' checkbox", "FAIL");
				Reporter.log("Not able to click on 'Single endpoint CPE' checkbox");
			}

		} else {

			Report.LogInfo("Info", " 'Single Endpoint CPE' checkbox is not displaying as expected,"
					+ " when 'VPN Topology' selected as 'hub&spoke' or 'E-PN(Any to Any)", "PASS");

		}
	}

	public void editService_Email(String Email) throws InterruptedException, IOException {

		boolean emailAvailability = false;
		try {
			emailAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Email);

			if (emailAvailability) {

				Report.LogInfo("Info", " 'Email' text field is displaying in 'Edit Service' page", "PASS");

				if (!Email.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Email);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Email, Email, "Email");
					Report.LogInfo("Info", "Edited value for 'Email' field is " + Email, "PASS");

					String Actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Email, "value");
					Report.LogInfo("Info", " 'Email' field is edited as: " + Actualvalue, "PASS");
					Reporter.log("'Email' field is edited as: " + Actualvalue);

				} else {
					Report.LogInfo("Info", "'Email' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Email' text field is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Email' text field is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'Email' text field is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " not able to edit 'Email' field", "FAIL");
		}

	}

	public void editService_phoneContact(String PhoneContact) throws InterruptedException, IOException {

		boolean phoneAvailability = false;
		try {
			phoneAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact);

			if (phoneAvailability) {

				Report.LogInfo("Info", " Phone Contact field is displaying in 'Edit Service' page as expected", "PASS");

				if (!PhoneContact.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact, PhoneContact, "Phone Contact");
					waitforPagetobeenable();
					String actualValue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact, "value");
					Report.LogInfo("Info", "Edited value for 'Phone contact' field is " + actualValue, "PASS");
				} else {
					Report.LogInfo("Info", " 'Phone contact' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Phone Contact field is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Phone Contact field is not available in 'Edit Service' page", "FAIL");
			Reporter.log("Phone Contact field is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'phone Contact' field", "FAIL");
			Reporter.log(" Not able to edit 'phone Contact' field");
		}
	}

	public void editService_remark(String remark) throws InterruptedException, IOException {

		boolean remarkAvailability = false;
		try {
			remarkAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Remark);

			if (remarkAvailability) {

				Report.LogInfo("Info", " 'Remark' field is displaying in 'Edit Service' page as expected", "PASS");

				if (!remark.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Remark);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Remark, remark, "Remark");

					String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Remark, "value");
					Report.LogInfo("Info", "Edited value for 'Remark' field is " + actualvalue, "PASS");

				} else {
					Report.LogInfo("Info", " 'Remark' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Remark' field is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Remark' field is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'remark' field is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Remark' field", "FAIL");
			Reporter.log(" Not able to edit 'Remark' field");
		}
	}

	public void editService_performancereporting_10G(String performanceReporting)
			throws InterruptedException, IOException {

		if (!performanceReporting.equalsIgnoreCase("null")) {

			boolean perfmmonitr = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox,
					"performanceReportingcheckbox");
			waitforPagetobeenable();

			if (performanceReporting.equalsIgnoreCase("yes")) {

				if (perfmmonitr) {

					Report.LogInfo("Info",
							"Performance Reporting checkbox is not edited and it is already Selected while creating",
							"PASS");

				} else {

					click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
					Reporter.log("Performance monitor check box is selected");
					Report.LogInfo("Info", "Performance Reporting is edited and it is selected", "PASS");
				}
			} else if (performanceReporting.equalsIgnoreCase("no")) {
				if (perfmmonitr) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.performancemonitoring);
					Reporter.log("Performance Reporting check box is unselected");
					Report.LogInfo("Info", "Performance Reporting is edited and gets unselected", "PASS");

				} else {
					Report.LogInfo("Info", "Performance Reporting is not edited and it remains unselected", "PASS");
				}
			}
		} else {
			Report.LogInfo("Info", " 'Performance reporting' checkbox is not edited", "PASS");
		}
	}

	public void editService_proactiveMonitoring(String ProactiveMonitoring, String notificationManagement)
			throws InterruptedException, IOException {
		if (!ProactiveMonitoring.equalsIgnoreCase("null")) {

			boolean proactivemonitor = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring,
					"proactive Monitoring");
			waitforPagetobeenable();

			if (ProactiveMonitoring.equalsIgnoreCase("yes")) {

				if (proactivemonitor) {

					Report.LogInfo("Info",
							"Proactive monitoring is not edited and it is already Selected while creating", "PASS");

					if (notificationManagement.equalsIgnoreCase("null")) {

						Report.LogInfo("Info", "No changes made to notification management", "PASS");

					} else {

						click(Lanlink_Metro_Obj.Lanlink_Metro.notigymanagement_xbutton);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
						waitforPagetobeenable();
						click(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + notificationManagement
								+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
						waitforPagetobeenable();
						Report.LogInfo("Info",
								"Edited value for 'Notification Management' field is " + notificationManagement,
								"PASS");
					}

				} else {

					click(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
					Reporter.log("proactive monitoring check box is selected");
					Report.LogInfo("Info", "proactive monitoring is edited and gets selected", "PASS");

					if (notificationManagement.equalsIgnoreCase("null")) {

						Report.LogInfo("Info", "No changes made to notification management", "PASS");

					} else {

						click(Lanlink_Metro_Obj.Lanlink_Metro.notigymanagement_xbutton);
						waitforPagetobeenable();

						click(Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
						click(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + notificationManagement
								+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
						Report.LogInfo("Info",
								"Edited value for 'Notification Management' field is " + notificationManagement,
								"PASS");
					}
				}
			}

			else if (ProactiveMonitoring.equalsIgnoreCase("no")) {

				if (proactivemonitor) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
					Reporter.log("Proactive monitoring check box is unselected");
					Report.LogInfo("Info", "proactive monitoring is edited and gets unselected", "PASS");

				} else {
					Report.LogInfo("Info",
							"proactive monitoring was not selected during service creation and it remains unselected",
							"PASS");
				}
			}
		} else {
			Report.LogInfo("Info", "No changes made for 'Proactive monitoring' chekbox", "PASS");
		}
	}

	public void editService_deliveryChannel(String deliveryChannel) throws InterruptedException, IOException {

		boolean deliveryChannelAvailability = false;
		try {
			deliveryChannelAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);

			if (deliveryChannelAvailability) {

				Report.LogInfo("Info", " 'Delivery channel' dropdown is displaying in 'Edit Service' page as expected",
						"PASS");

				if (!deliveryChannel.equalsIgnoreCase("null")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_xbutton);
					waitforPagetobeenable();
					click(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + deliveryChannel + "')]")).click();;

					Report.LogInfo("Info", "Edited value for 'Delivery Channel' dropdown is " + deliveryChannel,
							"PASS");
					Reporter.log("Delivery channel dropdown value is edited as expected");

				} else {
					Report.LogInfo("Info", "Delivery channel dropdown value is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Delivery channel' dropdown is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Delivery channel' dropdown is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'Delivery channel' dropdown is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'delvery Channel' dropdown", "FAIL");
		}
	}

	public void editService_managementOrder(String ManagementOrder) throws InterruptedException, IOException {
		boolean managemenOrderAvailabiliy = false;
		try {
			managemenOrderAvailabiliy = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.managementOrder);

			if (managemenOrderAvailabiliy) {
				Report.LogInfo("Info", " 'Management Order' dropdown is displaying in 'Edit Service' page as expected",
						"PASS");

				if (!ManagementOrder.equalsIgnoreCase("null")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.managementOrder_xButton,"X button");
					waitforPagetobeenable();

					click(Lanlink_Metro_Obj.Lanlink_Metro.managementOrder,"Management order");
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + ManagementOrder + "')]")).click();

					waitforPagetobeenable();

					Report.LogInfo("Info", "Edited value for 'management Order' field is " + ManagementOrder, "PASS");
					Reporter.log("Edited value for 'management Order' field is " + ManagementOrder);

				} else {

					Report.LogInfo("Info", "No changes has been made to 'Management order' field", "PASS");
					Reporter.log("No changes has been made to 'Management order' field");
				}
			} else {
				Report.LogInfo("Info", " 'Management Order' dropdown is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Management Order' dropdown is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'Management Order' dropdown is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'management order' dropodwn", "FAIL");
			Reporter.log(" Not able to edit 'management order' dropodwn");
		}
	}

	public void editService_IntermediateTechnology(String intermediateTechnology)
			throws InterruptedException, IOException {

		boolean intTechAvilability = false;
		try {
			intTechAvilability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology);
			if (intTechAvilability) {
				Report.LogInfo("Info",
						"Intermediate Technologies' field is displaying under 'Edit Service' page as expected", "PASS");
				if (intermediateTechnology.equalsIgnoreCase("null")) {

					Report.LogInfo("Info", "No changes has made for 'Intermediate Technology' field", "PASS");

				} else {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology, intermediateTechnology,
							"intermediate Technology");
					waitforPagetobeenable();
					Report.LogInfo("Info",
							"Edited value for 'Intermediate Technology' field is " + intermediateTechnology, "PASS");

				}
			} else {
				Report.LogInfo("Info", " 'Intermediate technologies' field is not displaying under 'edit Sevice' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Intermediate technologies' field is not displaying under 'edit Sevice' page",
					"FAIL");
			Reporter.log(" 'Intermediate technologies' field is not displaying under 'edit Sevice' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Intermediate technologies' field", "FAIL");
		}
	}

	public void editService_circuitreference(String CircuitReference) throws InterruptedException, IOException {

		boolean circuitRefAvailability = false;

		circuitRefAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
		if (circuitRefAvailability) {
			if (CircuitReference.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", " 'Circuit reference' field is not edited", "PASS");
			} else {

				clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
				waitforPagetobeenable();

				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference, CircuitReference, "Circuit Reference");
				waitforPagetobeenable();
				String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference, "value");
				Report.LogInfo("Info", "Edited value for 'Circuit reference' field is " + actualvalue, "PASS");
			}
		} else {
			Report.LogInfo("Info", " 'Circuit Reference' field is not displaying in 'Edit CPE device' page", "FAIL");
		}
	}

	public void editService_circuitType(String CircuitType) throws InterruptedException {

		try {
			if (CircuitType.equalsIgnoreCase("null")) {

				Report.LogInfo("Info", " 'Circuit type' field is not edited", "PASS");

			} else {

				// verify whether it is selected
				boolean circuitType = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.CircuitType_1 + CircuitType
						+ Lanlink_Metro_Obj.Lanlink_Metro.CircuitType_2, "Circuit type");
				if (circuitType) {

					Report.LogInfo("Info", " 'Circuit Type' value selected as : " + CircuitType + " as expected",
							"PASS");

				} else {
					Report.LogInfo("Info", " 'Circuit Type' value is not selected as : " + CircuitType, "FAIL");
				}

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Circuit type' " + CircuitType + " is not dipslaying under 'Edit Service page",
					"FAIL");
			Reporter.log(" 'Circuit type' " + CircuitType + " is not dipslaying under 'Edit Service page");
		} catch (Exception er) {
			er.printStackTrace();
			Report.LogInfo("Info", " 'Circuit type' " + CircuitType + " is disabled under 'Edit Service page", "PASS");
			Reporter.log(" 'Circuit type' " + CircuitType + " is disabled under 'Edit Service page");
		}
	}

	public void Edit_DirectFibre1G(String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed,
			String EndpointCPE, String Email, String PhoneContact, String remark, String performanceReporting,
			String ProactiveMonitoring, String deliveryChannel, String ManagementOrder, String vpnTopology,
			String intermediateTechnology, String CircuitReference, String CircuitType, String notificationManagement,
			String perCoSperformanceReport, String actelisBased, String standardCIR, String standardEIR,
			String premiumCIR, String premiumEIR) throws InterruptedException, IOException {

		// Service Identification
		editService_serviceIdentification(ServiceIdentificationNumber);

		// Endpoint CPE
		editService_singleEndPointCPE(EndpointCPE, vpnTopology);

		// Email
		editService_Email(Email);

		// Phone contact `
		editService_phoneContact(PhoneContact);

		// Remark
		editService_remark(remark);

		// Performance Reporting
		boolean perfrmReportAvailability = false;
		try {
			perfrmReportAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);

			if (perfrmReportAvailability) {

				Report.LogInfo("Info",
						" 'Performance reporting' checkbox is displaying in 'Edit Service' page as expected", "PASS");

				if (!performanceReporting.equalsIgnoreCase("null")) {

					boolean perfReport = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox,
							"performanceReportingcheckbox");
					waitforPagetobeenable();

					if (performanceReporting.equalsIgnoreCase("yes")) {

						if (perfReport) {

							Report.LogInfo("Info",
									"Performance Reporting checkbox is not edited and it is already Selected while creating",
									"PASS");

						} else {

							click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
							Reporter.log("Performance Reporting check box is selected");
							Report.LogInfo("Info", "Performance Reporting checkbox is selected", "PASS");
						}
					} else if (performanceReporting.equalsIgnoreCase("no")) {

						if (perfReport) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
							Reporter.log("Performance Reporting check box is unselected");
							Report.LogInfo("Info", "Performance Reporting is edited and gets unselected", "PASS");

						} else {
							Report.LogInfo("Info", "Performance Reporting is not edited and it remains unselected",
									"PASS");
						}

					}
				} else {
					Report.LogInfo("Info", "No changes made for Performance Reporting chekbox", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Performance reporting' checkbox is not available in 'Edit Service' page",
						"FAIL");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " ' Performance Reporting' checkbox is not displaying under 'Edit service' page",
					"FAIL");
			Reporter.log(" ' Performance Reporting' checkbox is not displaying under 'Edit service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to click on 'Performance Reporting' checkbox", "FAIL");
			Reporter.log(" Not able to click on 'erformance Reporting' checkbox");
		}

		// Per CoS Performance Reporting
		if (performanceReporting.equalsIgnoreCase("yes")) {
			boolean perCoSdisplay = false;
			try {
				perCoSdisplay = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
				waitforPagetobeenable();

				if (perCoSdisplay) {
					Report.LogInfo("Info",
							" 'Per CoS Performance Reporting' checkbox is displaying in 'Edit Service' page", "PASS");
					if (!perCoSperformanceReport.equalsIgnoreCase("null")) {

						boolean perCoSreport = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport,
								"perCoSperformncereport");
						waitforPagetobeenable();

						if (perCoSperformanceReport.equalsIgnoreCase("yes")) {

							if (perCoSreport) {

								Report.LogInfo("Info",
										"Per CoS Performance Reporting checkbox is not edited and it is already Selected while creating",
										"PASS");

							} else {

								click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
								Reporter.log("Per CoS Performance Reporting check box is selected");
								Report.LogInfo("Info", "Per CoS Performance Reporting is selected", "PASS");
							}
						}

						else if (perCoSperformanceReport.equalsIgnoreCase("no")) {

							if (perCoSreport) {

								click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
								Reporter.log("Per CoS Performance Reporting check box is unselected");
								Report.LogInfo("Info", "PPer CoS Performance Reporting is edited and gets unselected",
										"PASS");

							} else {
								Report.LogInfo("Info",
										"Per CoS Performance Reporting is not edited and it remains unselected",
										"PASS");
							}

						}
					} else {
						Report.LogInfo("Info", " 'Per CoS Performance Reporting' chekbox is not edited", "PASS");
					}
				} else {
					Report.LogInfo("Info",
							" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page",
							"FAIL");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info",
						" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page",
						"FAIL");
				Reporter.log(" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to click on 'Per CoS Performance Reporting' checkbox", "FAIL");
				Reporter.log(" Not able to click on 'Per CoS Performance Reporting' checkbox");
			}
		}

		// Actelis Based
		if (perCoSperformanceReport.equalsIgnoreCase("yes")) {
			boolean actelisbased = false;
			if (vpnTopology.equalsIgnoreCase("Point-to-Point")) {
				try {
					actelisbased = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
					waitforPagetobeenable();
					if (actelisbased) {
						Report.LogInfo("Info", " Actelis Based checkbox is dipslaying in 'Edit Serivce' page", "PASS");
						if (!actelisBased.equalsIgnoreCase("null")) {

							boolean actelsBased = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox,
									"ActelisBasedcheckbox");
							waitforPagetobeenable();

							if (actelisBased.equalsIgnoreCase("yes")) {

								if (actelsBased) {

									Report.LogInfo("Info",
											"Actelis Based checkbox is not edited and it is already Selected while creating",
											"PASS");

								} else {

									click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
									Reporter.log("Actelis Based check box is selected");
									Report.LogInfo("Info", "Actelis Based checkbox is selected", "PASS");
								}
							}

							else if (actelisBased.equalsIgnoreCase("no")) {

								if (actelsBased) {

									click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
									Reporter.log("Per CoS Performance Reporting check box is unselected");
									Report.LogInfo("Info", "Actelis Based is edited and gets unselected", "PASS");

								} else {
									Report.LogInfo("Info",
											"Actelis Based checkbox is not edited and it remains unselected", "PASS");
								}

							}
						} else {
							Report.LogInfo("Info", "No changes made for Actelis Based chekbox", "PASS");
						}
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info", " Actelis Based checkbox is not dipslaying in 'Edit Serivce' page", "FAIL");
				} catch (Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info", " Not able to click on 'Acteis' checkbox", "FAIL");
					Reporter.log(" Not able to click on 'Acteis' checkbox");
				}
			} else {
				Reporter.log(
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', 'Actelis' checkbox will not display");
				Report.LogInfo("Info",
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', 'Actelis' checkbox will not display",
						"PASS");
			}
		}

		if (actelisBased.equalsIgnoreCase("yes")) {

			if (vpnTopology.equalsIgnoreCase("Point-to-Point")) {

				// Standard CIR
				editService_standardCIR(standardCIR);

				// Standard EIR
				editService_standardEIR(standardEIR);

				// Premium CIR
				editService_premiumCIR(premiumCIR);

				// Premium EIR
				editService_premiumEIR(premiumEIR);
			} else {
				Reporter.log(
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', Standard CIR_Eir Premium CIR_EIR fields will not display");
				Report.LogInfo("Info",
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', Standard CIR_Eir Premium CIR_EIR fields will not display",
						"PASS");
			}
		}

		// Proactive monitoring
		editService_proactiveMonitoring(ProactiveMonitoring, notificationManagement);

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

		waitforPagetobeenable();

		// Delivery channel
		editService_deliveryChannel(deliveryChannel);

		// management order
		editService_managementOrder(ManagementOrder);

		// VPN topology
		////// scrolltoend();
		waitforPagetobeenable();

		boolean vpnDisplayedValues = false;

		vpnDisplayedValues = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + vpnTopology
				+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);

		if (vpnDisplayedValues) {
			Report.LogInfo("Info", vpnTopology + " is displaying under 'VPN Topology' as expected", "PASS");
			if (vpnTopology.equals("Point-to-Point")) {

				// Intermediate technologies
				editService_IntermediateTechnology(intermediateTechnology);

				// Circuit reference
				editService_circuitreference(CircuitReference);

				// Circuit type
				editService_circuitType(CircuitType);

			}

			else if (vpnTopology.equals("Hub&Spoke") || vpnTopology.equals("E-PN (Any-to-Any)")) {

				editService_circuitreference(CircuitReference);
				waitforPagetobeenable();
			}

		} else {
			Report.LogInfo(
					"Info", " 'VPN Topology' value is not displaying as expected." + "  Expected vlue is: "
							+ vpnTopology + "  Actual values displaying is: " + Lanlink_Metro_Obj.Lanlink_Metro.vpnTopology,
					"FAIL");
		}

		Reporter.log("going to click on OK buttto");
		////// scrolltoend();
		waitforPagetobeenable();

		// click on "Ok button to update
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void editService_standardCIR(String standardCIR) throws InterruptedException, IOException {

		try {
			boolean stndrdCIR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield);
			if (stndrdCIR) {
				Report.LogInfo("Info", " 'Standard CIR' field is displaying in 'Edit Service' page ", "PASS");
				if (!standardCIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield, standardCIR, "standardCIR");
					Report.LogInfo("Info", "Edited value for 'Standard CIR' field is " + standardCIR, "PASS");

				} else {
					Report.LogInfo("Info", " 'Standard CIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Standard CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Standard CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" 'Standard CIR' field is not displaying in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Standard CIR' field", "FAIL");
			Reporter.log(" Not able to edit 'Standard CIR' field");

		}
	}

	public void editService_standardEIR(String standardEIR) throws InterruptedException, IOException {

		boolean stndrdEIR = false;
		try {
			stndrdEIR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield);
			if (stndrdEIR) {
				Report.LogInfo("Info", " Standard EIR' field is displaying in 'Edit Service' page as expected", "PASS");
				if (!standardEIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield, standardEIR, "standardEIR");

					String actualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield, "value");
					Report.LogInfo("Info", "Edited value for 'Standard EIR' field is " + actualvalue, "PASS");

				} else {
					Report.LogInfo("Info", " 'Standard EIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Standard EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Standard EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" 'Standard EIR' field is not displaying in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Standard EIR' field", "FAIL");
			Reporter.log(" Not able to edit 'Standard EIR' field");

		}
	}

	public void editService_premiumCIR(String premiumCIR) throws InterruptedException, IOException {

		boolean premumCIR = false;
		try {
			premumCIR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield);
			if (premumCIR) {
				Report.LogInfo("Info", " Premise CIR' field is displaying in 'Edit Service' page as expected", "PASS");
				if (!premiumCIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield, premiumCIR, "premiumCIR");

					String actualValue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield, "value");
					Report.LogInfo("Info", "Edited value for 'Premium CIR' field is " + actualValue, "PASS");

				} else {
					Report.LogInfo("Info", " 'Premium CIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Premise CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Premise CIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" Premise CIR' field is not displaying in 'Edit Service' page ");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Premise CIR' field", "FAIL");
			Reporter.log("Not able to edit 'Premise CIR' field");

		}
	}

	public void editService_premiumEIR(String premiumEIR) throws InterruptedException, IOException {

		boolean premumEIR = false;
		try {
			premumEIR = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield);
			if (premumEIR) {
				Report.LogInfo("Info", " Premise EIR' field is displaying in 'Edit Service' page as expected", "PASS");
				if (!premiumEIR.equalsIgnoreCase("null")) {

					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield);
					waitforPagetobeenable();

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield, premiumEIR, "premiumEIR");
					Report.LogInfo("Info", "Edited value for 'Premium EIR' field is " + premiumEIR, "PASS");

				} else {
					Report.LogInfo("Info", "  'Premium EIR' field is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " Premise EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Premise EIR' field is not displaying in 'Edit Service' page ", "FAIL");
			Reporter.log(" Premise EIR' field is not displaying in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Premise EIR' field", "FAIL");
			Reporter.log("Not able to edit 'Premise EIR' field");

		}
	}

	public void Edit_Metro_MSPselected(String ServiceIdentificationNumber, String SelectSubService,
			String Interfacespeed, String EndpointCPE, String Email, String PhoneContact, String remark,
			String performanceReporting, String ProactiveMonitoring, String deliveryChannel, String ManagementOrder,
			String vpnTopology, String intermediateTechnology, String CircuitReference, String CircuitType,
			String notificationManagement, String perCoSperformanceReport, String actelisBased, String standardCIR,
			String standardEIR, String premiumCIR, String premiumEIR, String EVPNtchnology,
			String HCoSperformanceRpeorting) throws InterruptedException, IOException {

		// Service Identification
		editService_serviceIdentification(ServiceIdentificationNumber);

		// EVPN Technology
		editService_EVPNtechnology(EVPNtchnology);

		// Email
		editService_Email(Email);

		// Phone contact `
		editService_phoneContact(PhoneContact);

		// Remark
		editService_remark(remark);

		// Performance Reporting
		boolean perfrmReportAvailability = false;
		try {
			perfrmReportAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);

			if (perfrmReportAvailability) {

				Report.LogInfo("Info",
						" 'Performance reporting' checkbox is displaying in 'Edit Service' page as expected", "PASS");

				if (!performanceReporting.equalsIgnoreCase("null")) {

					boolean perfReport = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox,
							"performanceReportingcheckbox");
					waitforPagetobeenable();

					if (performanceReporting.equalsIgnoreCase("yes")) {

						if (perfReport) {

							Report.LogInfo("Info",
									"Performance Reporting checkbox is not edited and it is already Selected while creating",
									"PASS");

						} else {

							click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
							Reporter.log("Performance Reporting check box is selected");
							Report.LogInfo("Info", "Performance Reporting checkbox is selected", "PASS");
						}
					} else if (performanceReporting.equalsIgnoreCase("no")) {

						if (perfReport) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
							Reporter.log("Performance Reporting check box is unselected");
							Report.LogInfo("Info", "Performance Reporting is edited and gets unselected", "PASS");

						} else {
							Report.LogInfo("Info", "Performance Reporting is not edited and it remains unselected",
									"PASS");
						}

					}
				} else {
					Report.LogInfo("Info", "No changes made for Performance Reporting chekbox", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'Performance reporting' checkbox is not available in 'Edit Service' page",
						"FAIL");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " ' Performance Reporting' checkbox is not displaying under 'Edit service' page",
					"FAIL");
			Reporter.log(" ' Performance Reporting' checkbox is not displaying under 'Edit service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to click on 'Performance Reporting' checkbox", "FAIL");
			Reporter.log(" Not able to click on 'erformance Reporting' checkbox");
		}

		// HCoS performance Reporting
		boolean HCoSperfrmReportAvailability = false;
		try {
			HCoSperfrmReportAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport);

			if (HCoSperfrmReportAvailability) {

				Report.LogInfo("Info",
						" 'HCoS Performance Reporting (Modular MSP)' checkbox is displaying in 'Edit Service' page as expected",
						"PASS");

				if (!HCoSperformanceRpeorting.equalsIgnoreCase("null")) {

					boolean perfReport = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport,
							"HCoSperformanceReport");
					waitforPagetobeenable();

					if (HCoSperformanceRpeorting.equalsIgnoreCase("yes")) {

						if (perfReport) {

							Report.LogInfo("Info",
									" 'HCoS Performance Reporting (Modular MSP)' checkbox is not edited and it is already Selected while creating",
									"PASS");

						} else {

							click(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport);
							Reporter.log("Performance Reporting check box is selected");
							Report.LogInfo("Info", "Performance Reporting checkbox is selected", "PASS");
						}
					} else if (HCoSperformanceRpeorting.equalsIgnoreCase("no")) {

						if (perfReport) {

							click(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport);
							Reporter.log(" 'HCoS Performance Reporting (Modular MSP)' check box is unselected");
							Report.LogInfo("Info",
									" 'HCoS Performance Reporting (Modular MSP)' is edited and gets unselected",
									"PASS");

						} else {
							Report.LogInfo("Info",
									" 'HCoS Performance Reporting (Modular MSP)' is not edited and it remains unselected",
									"PASS");
						}

					}
				} else {
					Report.LogInfo("Info", "No changes made for 'HCoS Performance Reporting (Modular MSP)' chekbox",
							"PASS");
				}
			} else {
				Report.LogInfo("Info",
						" 'HCoS Performance Reporting (Modular MSP)' checkbox is not available in 'Edit Service' page",
						"FAIL");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info",
					" 'HCoS Performance Reporting (Modular MSP)' checkbox is not displaying under 'Edit service' page",
					"FAIL");
			Reporter.log(
					" 'HCoS Performance Reporting (Modular MSP)' checkbox is not displaying under 'Edit service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to click on 'HCoS Performance Reporting (Modular MSP)' checkbox", "FAIL");
			Reporter.log(" Not able to click on 'HCoS Performance Reporting (Modular MSP)' checkbox");
		}

		// Per CoS Performance Reporting
		if (performanceReporting.equalsIgnoreCase("yes")) {
			boolean perCoSdisplay = false;
			try {
				perCoSdisplay = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
				waitforPagetobeenable();

				if (perCoSdisplay) {
					Report.LogInfo("Info",
							" 'Per CoS Performance Reporting' checkbox is displaying in 'Edit Service' page", "PASSA");
					if (!perCoSperformanceReport.equalsIgnoreCase("null")) {

						boolean perCoSreport = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport,
								"perCoSperformncereport");
						waitforPagetobeenable();

						if (perCoSperformanceReport.equalsIgnoreCase("yes")) {

							if (perCoSreport) {

								Report.LogInfo("Info",
										"Per CoS Performance Reporting checkbox is not edited and it is already Selected while creating",
										"PASS");

							} else {

								click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
								Reporter.log("Per CoS Performance Reporting check box is selected");
								Report.LogInfo("Info", "Per CoS Performance Reporting is selected", "PASS");
							}
						}

						else if (perCoSperformanceReport.equalsIgnoreCase("no")) {

							if (perCoSreport) {

								click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
								Reporter.log("Per CoS Performance Reporting check box is unselected");
								Report.LogInfo("Info", "PPer CoS Performance Reporting is edited and gets unselected",
										"PASS");

							} else {
								Report.LogInfo("Info",
										"Per CoS Performance Reporting is not edited and it remains unselected",
										"PASS");
							}

						}
					} else {
						Report.LogInfo("Info", " 'Per CoS Performance Reporting' chekbox is not edited", "PASS");
					}
				} else {
					Report.LogInfo("Info",
							" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page",
							"FAIL");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info",
						" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page",
						"FAIL");
				Reporter.log(" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to click on 'Per CoS Performance Reporting' checkbox", "FAIL");
				Reporter.log(" Not able to click on 'Per CoS Performance Reporting' checkbox");
			}
		}

		// Actelis Based
		if (perCoSperformanceReport.equalsIgnoreCase("yes")) {
			boolean actelisbased = false;
			if (vpnTopology.equalsIgnoreCase("Point-to-Point")) {
				try {
					actelisbased = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
					waitforPagetobeenable();
					if (actelisbased) {
						Report.LogInfo("Info", " Actelis Based checkbox is dipslaying in 'Edit Serivce' page", "PASS");
						if (!actelisBased.equalsIgnoreCase("null")) {

							boolean actelsBased = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox,
									"ActelisBasedcheckbox");
							waitforPagetobeenable();

							if (actelisBased.equalsIgnoreCase("yes")) {

								if (actelsBased) {

									Report.LogInfo("Info",
											"Actelis Based checkbox is not edited and it is already Selected while creating",
											"PASS");

								} else {

									click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
									Reporter.log("Actelis Based check box is selected");
									Report.LogInfo("Info", "Actelis Based checkbox is selected", "PASS");
								}
							}

							else if (actelisBased.equalsIgnoreCase("no")) {

								if (actelsBased) {

									click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
									Reporter.log("Per CoS Performance Reporting check box is unselected");
									Report.LogInfo("Info", "Actelis Based is edited and gets unselected", "PASS");

								} else {
									Report.LogInfo("Info",
											"Actelis Based checkbox is not edited and it remains unselected", "PASS");
								}

							}
						} else {
							Report.LogInfo("Info", "No changes made for Actelis Based chekbox", "PASS");
						}
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info", " Actelis Based checkbox is not dipslaying in 'Edit Serivce' page", "FAIL");
				} catch (Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info", " Not able to click on 'Acteis' checkbox", "FAIL");
					Reporter.log(" Not able to click on 'Acteis' checkbox");
				}
			} else {
				Reporter.log(
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', 'Actelis' checkbox will not display");
				Report.LogInfo("Info",
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', 'Actelis' checkbox will not display",
						"PASS");
			}
		}

		if (actelisBased.equalsIgnoreCase("yes")) {

			if (vpnTopology.equalsIgnoreCase("Point-to-Point")) {

				// Standard CIR
				editService_standardCIR(standardCIR);

				// Standard EIR
				editService_standardEIR(standardEIR);

				// Premium CIR
				editService_premiumCIR(premiumCIR);

				// Premium EIR
				editService_premiumEIR(premiumEIR);
			} else {
				Reporter.log(
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', Standard CIR_Eir Premium CIR_EIR fields will not display");
				Report.LogInfo("Info",
						" when topology selected as 'Hub&Spoke' or 'E-PN (Any to Any)', Standard CIR_Eir Premium CIR_EIR fields will not display",
						"PASS");
			}
		}

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

		waitforPagetobeenable();

		// Proactive monitoring
		editService_proactiveMonitoring(ProactiveMonitoring, notificationManagement);

		// Delivery channel
		editService_deliveryChannel(deliveryChannel);

		// VPN topology
		////// scrolltoend();
		waitforPagetobeenable();

		boolean vpnDisplayedValues = false;

		vpnDisplayedValues = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + vpnTopology
				+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);

		if (vpnDisplayedValues) {
			Report.LogInfo("Info", vpnTopology + " is displaying under 'VPN Topology' as expected", "PASS");
			if (vpnTopology.equals("Point-to-Point")) {

				// Intermediate technologies
				editService_IntermediateTechnology(intermediateTechnology);

				// Circuit reference
				editService_circuitreference(CircuitReference);

				// Circuit type
				editService_circuitType(CircuitType);

			}

			else if (vpnTopology.equals("E-PN (Any-to-Any)")) {

				editService_circuitreference(CircuitReference);
				waitforPagetobeenable();
			}

			else if (vpnTopology.equals("Hub&Spoke")) {

				Reporter.log("No additional fields");
			}

		} else {
			Report.LogInfo("Info",
					" 'VPN Topology' value is not displaying as expected." + "  Expected vlue is: " + vpnTopology
							+ "  Actual values displaying is: " + Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1
							+ vpnTopology + Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2,
					"FAIL");
		}

		Reporter.log("going to click on OK buttto");
		////// scrolltoend();
		waitforPagetobeenable();

		// click on "Ok button to update
		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton, "OK");
		waitforPagetobeenable();

	}

	public void editService_EVPNtechnology(String EVPNtecnology) throws InterruptedException, IOException {

		boolean EVPNtehcnologyAvailability = false;
		try {
			EVPNtehcnologyAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.E_VPNtechnologyDropdown);

			if (EVPNtehcnologyAvailability) {

				Report.LogInfo("Info", " 'E-VPN Technology' dropdown is displaying in 'Edit Service' page as expected",
						"PASS");

				if (!EVPNtecnology.equalsIgnoreCase("null")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.E_VPNtechnologyDropdown_xButton);
					waitforPagetobeenable();

					click(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + EVPNtecnology
							+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);

					Report.LogInfo("Info", "Edited value for 'E-VPN Technology' dropdown is " + EVPNtecnology, "PASS");
					Reporter.log("E-VPN Technology dropdown value is edited as expected");

				} else {
					Report.LogInfo("Info", "E-VPN Technology dropdown value is not edited", "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'E-VPN Technology' dropdown is not available in 'Edit Service' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'E-VPN Technology' dropdown is not available in 'Edit Service' page", "FAIL");
			Reporter.log(" 'E-VPN Technology' dropdown is not available in 'Edit Service' page");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'E-VPN Technology' dropdown", "FAIL");
		}
	}

	public void technologyP2P_AddSiteOrder(String technology, String interfaceSpeed, String devicename,
			String nonterminatepoinr, String Protected) throws InterruptedException, IOException {

		// Technology
		if (technology.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", "Technology dropdown is a mandatory field and no values are provided", "FAIL");
			Reporter.log("Technology dropdown is a mandatory field and no values are provided");
		} else {

			if (interfaceSpeed.equals("1GigE")) {

				if (technology.equals("Actelis") || technology.equals("Atrica") || technology.equals("Overture")
						|| technology.equals("Accedian-1G") || technology.equals("Cyan") || technology.equals("Alu")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					waitforPagetobeenable();
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", technology);
					clickonTechnology(technologySelected, technology);
					waitforPagetobeenable();
					Report.LogInfo("Info", technology + " is selected under technology dropdown", "PASS");
					Reporter.log(technology + " is selected under technology dropdown");

					if (technology.equals("Actelis")) {

						Reporter.log("No additional fields displays");
					}

					else if (technology.equals("Atrica")) {

						// Device name
						devicename_AddSiteOrder(devicename);

						// Non- termination point
						nontermination_AddSiteorder(nonterminatepoinr);

					}

					else if (technology.equals("Overture") || technology.equals("Accedian-1G")) {

						// Non- termination point
						nontermination_AddSiteorder(nonterminatepoinr);

					}

					else if (technology.equals("Cyan")) {

						// Non- termination point
						nontermination_AddSiteorder(nonterminatepoinr);

					}

					else if (technology.equals("Alu")) {

						// Device name
						devicename_AddSiteOrder(devicename);

					}
				}
			}

			if (interfaceSpeed.equals("10GigE")) {

				if (technology.equals("Accedian")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					waitforPagetobeenable();
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", technology);
					clickonTechnology(technologySelected, technology);
					waitforPagetobeenable();
					Report.LogInfo("Info", technology + " is selected under technology dropdown", "PASS");
					Reporter.log(technology + " is selected under technology dropdown");
					// Non- termination point
					nontermination_AddSiteorder(nonterminatepoinr);

				} else {
					Report.LogInfo("Info",
							technology + "is not available under 'Technology' dropdown for '10GigE' interface speed",
							"FAIL");
					Reporter.log(
							technology + "is not available under 'Technology' dropdown for '10GigE' interface speed");
				}

			}
		}

		Report.LogInfo("Info", "Data has been entered for add site order", "PASS");
		Reporter.log("Data has been entered for add site order");

	}

	public void DCAEnabledSite_AddSiteOrder(String DCAenabledsite, String cloudserviceprovider)
			throws InterruptedException, IOException {

		// DCA Enabled Site
		if (DCAenabledsite.equalsIgnoreCase("yes")) {

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_DCAenabledsitecheckbox);
			Report.LogInfo("Info", "DCA enabled checkbox is selected", "PASS");
			Reporter.log("DCA enabled checkbox is selected");

			// Cloud Service provider
			if (cloudserviceprovider.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", "DCA cloud service provider dropdown is mandatory. No values are provided",
						"FAIL");
				Reporter.log("DCA cloud service provider dropdown is mandatory. No values are provided");
			} else {
				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cloudserviceProvider);
				waitforPagetobeenable();
				addDropdownValues_commonMethod("Cloud Service Provider",Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_cloudserviceProvider, cloudserviceprovider);
				waitforPagetobeenable();
				Report.LogInfo("Info", cloudserviceprovider + " is selected under 'cloud service provider' dropdown",
						"PASS");
				Reporter.log(cloudserviceprovider + " is selected under 'cloud service provider' dropdown");
			}

		} else {
			Reporter.log("DCA site is not selected");
			Report.LogInfo("Info", "DCA enabled checkbox is not selected", "PASS");
		}

	}

	public void clickonTechnology(String technologySelected, String technology) throws InterruptedException {
		waitforPagetobeenable();

		try {
			addDropdownValues_commonMethod("Technology",Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, technology);

			
			Reporter.log("Under 'technology' dropdown, '" + technology + "' is selected");
		} catch (Exception e)

		{
			e.printStackTrace();
			Report.LogInfo("Info", technology + " is not available under 'Technology' dropdown", "FAIL");
			Reporter.log(technology + " is not available under 'Technology' dropdown");
		}
	}

	public void devicename_AddSiteOrder(String devicename) throws InterruptedException, IOException {

		// Device name
		if (devicename.equalsIgnoreCase("null")) {
			Report.LogInfo("Info", "device name field is mandatory. No values entered under 'device name' field",
					"FAIL");
			Reporter.log("device name field is mandatory. No values entered under 'device name' field");
		} else {
			try {
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Devicenamefield, devicename,
						"devicename");
				waitforPagetobeenable();

				String actualvalue = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Devicenamefield, "value");

				Report.LogInfo("Info", actualvalue + " is entered under 'device name' field", "PASS");
				Reporter.log(actualvalue + " is entered under 'device name' field");

			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", " 'Device name' field is not displaying under 'Add Site Order' page", "FAIL");
				Reporter.log(" 'Device name' field is not displaying under 'Add Site Order' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to enter value in 'Device name' field", "FAIL");
				Reporter.log(" Not able to enter value in 'Device name' field");
			}
		}

	}

	public void nontermination_AddSiteorder(String nonterminatepoinr) throws InterruptedException {

		// Non- termination point
		if (nonterminatepoinr.equalsIgnoreCase("yes")) {
			try {
				scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint);
				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint);
				waitforPagetobeenable();

				boolean nonTerminationSelection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_nonterminationpoint,"non termination point checkbox");
				if (nonTerminationSelection) {
					Report.LogInfo("Info", " 'Non-Termination point' checkbox is selected as expected", "PASS");
					Reporter.log(" 'Non-Termination point' checkbox is selected as expected");
				} else {
					Report.LogInfo("Info", " 'Non-Termination point' checkbox is not selected", "FAIL");
					Reporter.log(" 'Non-Termination point' checkbox is not selected");
				}

				Report.LogInfo("Info", "Non-termination point checkbox is selected", "PASS");
				Reporter.log("Non-termination point checkbox is selected");
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", " Non-Termination point' checkbox is not dipslaying under 'Add Site order' page",
						"FAIL");
				Reporter.log(" Non-Termination point' checkbox is not dipslaying under 'Add Site order' page");
			} catch (Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to click on 'non-Termination point' checkbox", "FAIL");
				Reporter.log(" Not able to click on 'non-Termination point' checkbox");
			}
		} else {
			Reporter.log("Non termination point checkbox is not selected as expected");
			Report.LogInfo("Info", "Non-termination point chekbox is not selected", "PASS");
		}

	}

	public void technologyDropdownFor1GigE() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Belgacom VDSL" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {
				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Device Name
					verifySiteOrderField_deviceName();

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

				}

				// Alu
				else if (Technology[k].equalsIgnoreCase("Alu")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Device Name
					verifySiteOrderField_deviceName();
				}

				// Accedian-1G
				else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();
				}

				// Belgacom VDSL
				else if (Technology[k].equalsIgnoreCase("Belgacom VDSL")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}
	}

	public void verifySiteOrderField_deviceName() throws InterruptedException, IOException {
		boolean devicename = false;
		try {
			devicename = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Devicenamefield);
			sa.assertTrue(devicename, "On selecting Atrica under Technology, Device name is not available");
			if (devicename) {
				Report.LogInfo("Info", " 'Device Name' field is displaying under 'Add Site Order' page as expected",
						"PASS");
			} else {
				Report.LogInfo("Info", " 'Device Name' field is not displaying under 'Add Site Order' page", "FAIL");

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Device Name' field is not displaying under 'Add Site Order' page", "FAIL");
			Reporter.log(" 'Device Name' field is not displaying under 'Add Site Order' page");
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Device Name' field is not displaying under 'Add Site Order' page", "FAIL");
			Reporter.log(" 'Device Name' field is not displaying under 'Add Site Order' page");
		}
	}

	public String selectRowForEditingInterface_showInterface(String interfacename, String devicename)
			throws InterruptedException, IOException {

		int TotalPages;

		String TextKeyword = webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//span[@ref='lbTotal']")).getText();

		TotalPages = Integer.parseInt(TextKeyword);

		Reporter.log("Total number of pages in table is: " + TotalPages);
		String interfaceAvailability = "yes";
		ab:
		if (TotalPages != 0) {

			for (int k = 1; k <= TotalPages; k++) {

			// Current page
			String CurrentPage = webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//span[@ref='lbCurrent']")).getText();
			int Current_page = Integer.parseInt(CurrentPage);
			Reporter.log("The current page is: " + Current_page);

			assertEquals(k, Current_page,"");

			Reporter.log("Currently we are in page number: " + Current_page);

			List<WebElement> results = webDriver.findElements(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//div[text()='"+ interfacename +"']"));
			
				
			int numofrows = results.size();
			Reporter.log("no of results: " + numofrows);
			boolean resultflag;

		
			if ((numofrows == 0)) {
				
				webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//button[text()='Next']"));
				Thread.sleep(3000);

			}

			else {
				for (int i = 0; i < numofrows; i++) {
					try {

						resultflag = results.get(i).isDisplayed();
						Reporter.log("status of result: " + resultflag);
						if (resultflag) {
							Reporter.log(results.get(i).getText());
							results.get(i).click();
								Report.LogInfo("Info", interfacename + " is selected under 'show Interface' ", "PASS");
								waitforPagetobeenable();
								webDriver.findElement(By.xpath("(//div[div[div[contains(@title,'"+ devicename +"')]]]/following-sibling::div)[1]//button[text()='Action']")).click();;

								break ab;
							}

						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							Report.LogInfo("Info", interfacename + " Interface name is not displaying", "FAIL");

							results = webDriver.findElements(
									By.xpath("(//div[@class='ag-root-wrapper ag-layout-auto-height ag-ltr'])[2]//div[text()='"+interfacename +"']"));
							numofrows = results.size();
							// results.get(i).click();
							Reporter.log("selected row is : " + i);

						}

					}
				}
			}
		} else {
			Report.LogInfo("Info", interfacename + " Interface name is not displaying", "FAIL");
			Reporter.log("No values available in table");
			Reporter.log("No values available inside the InterfaceInService table");
			Report.LogInfo("Info", "No value available inside 'show Interface' panel", "FAIL");
			interfaceAvailability = "No";
		}

		return interfaceAvailability;

	}

	public void Fieldvalidation_DirectFibreMSPselected(String serviceType, String SelectSubService,
			String Interfacespeed, String proActivemonitoring, String vpntopology)
			throws InterruptedException, IOException {

		String[] deliverychannel = { "--", "Retail", "Reseller", "WLC", "WLEC", "CES Solutions" };

		String[] VPNtopology = { "Point-to-Point", "Hub&Spoke", "E-PN (Any-to-Any)" };

		String[] notifyManagement = { "DNA" };

		boolean serviceIdentificationField, ServiceType, ServiceSubtype, interfacespeedvalue, singleendpointCPE, email,
				phone, remark, performancereoprting, deliveryChanel, proactiveMonitor, Managementorder, vpnTopology,
				intermediateTechnology, circuitref, circuitType, okButton, cancelButton;

		try {

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Circuit Reference Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferenceErrmsg, "Circuit Reference");

			// Service Identification Error message
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.serviceIdentificationerrmsg, "Service Identification");

			// service Identification
			serviceIdentificationField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);
			sa.assertTrue(serviceIdentificationField, "Service identification field is not displayed");
			if (serviceIdentificationField) {
				Report.LogInfo("Info",
						" ' Service Identfication' mandatory field is displaynig under 'Add Service' page as expected",
						"PASS");
				click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);
				waitforPagetobeenable();
			} else {
				Report.LogInfo("Info",
						" ' Service Identfication' mandatory field is not available under 'Add Service' page", "FAIL");
			}

			// Service type
			ServiceType = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceType);
			sa.assertTrue(ServiceType, "Service type is not displayed");
			if (ServiceType) {
				Report.LogInfo("Info", " 'LANLink' is displying under 'Service type' as expected", "PASS");
				Reporter.log(" 'LANLink' is displying under 'Service type' as expected");
			} else {
				Report.LogInfo("Info", " 'LANKLink' is not displying under 'Service type'", "FAIL");
				Reporter.log(" 'LANKLink' is not displying under 'Service type'");
			}

			// Service subtype
			ServiceSubtype = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation1 + SelectSubService
					+ Lanlink_Metro_Obj.Lanlink_Metro.fieldValidation2);
			sa.assertTrue(ServiceSubtype, "Service subtype is not displayed");
			if (ServiceSubtype) {
				Report.LogInfo("Info", SelectSubService + " is displying under 'Service Sub type' as expected", "PASS");
				Reporter.log(SelectSubService + " is displying under 'Service Sub type' as expected");
			} else {
				Report.LogInfo("Info", SelectSubService + " is not displying under 'Service Sub type'", "FAIL");
			}

			// E_VPN Technology
			boolean Evpntechnology = false;
			try {
				Evpntechnology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.E_VPNtechnologyDropdown);
				Reporter.log("E VPn topology  ");
				sa.assertTrue(Evpntechnology, " 'E-VPN Technology' dropdown is not displayed");
				Reporter.log(" 'E-VPN Technology'  dropdown gets displayed when proactive monitoring is selected");
				if (Evpntechnology) {
					Report.LogInfo("Info", "  'E-VPN Technology'  dropdown is displaying as expected", "PASS");

					click(Lanlink_Metro_Obj.Lanlink_Metro.E_VPNtechnologyDropdown);
					try {
						List<WebElement> listofEVPNtechnology = webDriver
								.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
						for (WebElement EVPNtechnologyTypes : listofEVPNtechnology) {

							Reporter.log("list of 'E-VPN Technology' are : " + EVPNtechnologyTypes.getText());
							Report.LogInfo("Info", "list of 'E-VPN Technology' are : " + EVPNtechnologyTypes, "PASS");
							Reporter.log("list of 'E-VPN Technology' are : " + EVPNtechnologyTypes.getText());
						}
					} catch (Exception e) {
						Reporter.log(" 'E-VPN' technology dropdown values are mismatching");
						e.printStackTrace();
						Report.LogInfo("Info",
								"  values in 'E-VPN Technology' dropdown under 'Metro' service subtype is not displaying",
								"FAIL");
						Reporter.log(
								"  values in 'E-VPN Technology' dropdown under 'Metro' service subtype is not displaying");
					}
				} else {
					Report.LogInfo("Info", "  'E-VPN Technology'  dropdown is not displaying", "PASS");
					Reporter.log("  'E-VPN Technology'  dropdown is not displaying");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info", "  'E-VPN Technology'  dropdown is not displaying", "PASS");
				Reporter.log("  'E-VPN Technology'  dropdown is not displaying");
			} catch (Exception ee) {
				ee.printStackTrace();
			}

			// COS checkbox
			verifyFields_COScheckbox();

			// clickon COS checkbox to get Multiport checkbox
			click(Lanlink_Metro_Obj.Lanlink_Metro.cosCheckbox_createService);
			waitforPagetobeenable();
			// Multiport text field
			verifyFields_multiPort();

			// Email
			try {
				email = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Email);
				sa.assertTrue(email, "email field is not displayed");
				if (email) {
					Report.LogInfo("Info", " 'Email' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'Email' field is not available under 'Create Service' pag", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Email' field is not available under 'Create Service' page", "FAIL");
			}

			// phone
			try {
				phone = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.PhoneContact);
				sa.assertTrue(phone, "phone contact field is not displayed");
				if (phone) {
					Report.LogInfo("Info", " 'phone' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'phone' field is not available under 'Create Service' page", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Phone' field is not available under 'Create Service' page", "FAIL");
			}

			// remark
			try {
				remark = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Remark);
				sa.assertTrue(remark, "remark field is not displayed");
				if (remark) {
					Report.LogInfo("Info", " 'Remark' field is displying under 'Create Service'page as expected",
							"PASS");
				} else {
					Report.LogInfo("Info", " 'Remark' field is not available under 'Create Service' page", "FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Remark' field is not available under 'Create Service' page", "FAIL");
			}

			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.configurtoinptionpanel_webelementToScroll);

			waitforPagetobeenable();
			// performance Reporting
			performancereoprting = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox);
			sa.assertTrue(performancereoprting,
					"performance reporting checbox is not displayed under 'Create Service' page");
			if (performancereoprting) {
				Report.LogInfo("Info",
						" 'performance Reporting' checkbox is displying under 'Create Service'page as expected",
						"PASS");

				boolean performancereoprtingselection = isSelected(
						Lanlink_Metro_Obj.Lanlink_Metro.performanceReportingcheckbox, "performance reporting checkbox");
				if (performancereoprtingselection) {
					Report.LogInfo("Info",
							" 'Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'",
							"PASS");
					Reporter.log(
							" 'Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'");

					// HCoS Performance Rpeorting
					boolean HCoSperformReport = false;
					try {
						HCoSperformReport = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport);
						sa.assertTrue(HCoSperformReport,
								"HCoS Performance Reporting checbox is not displayed under 'Create Service' page");
						if (HCoSperformReport) {
							Report.LogInfo("Info", "HCoS Performance Reporting checbox is displayed", "PASS");

							boolean perCoSselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.HCoSperformanceReport,
									"HCOS performance report");
							if (perCoSselection) {
								Report.LogInfo("Info",
										" 'HCoS Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'",
										"FAIL");
							} else {
								Report.LogInfo("Info",
										" 'HCoS Performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected",
										"PASS");
							}
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'HCoS Performance Reporting' checkbox is not displaying", "FAIL");
						Reporter.log(" 'HCoS Performance Reporting' checkbox is not displaying");
					} catch (Exception ee) {
						ee.printStackTrace();
						Report.LogInfo("Info", " 'HCoS Performance Reporting' checkbox is selected by default", "FAIL");
						Reporter.log(" 'HCoS Performance Reporting' checkbox is selected by default");
					}

					// Per CoS Performance Reporting
					boolean perCosreopt = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
					sa.assertTrue(perCosreopt,
							"Per CoS Performance Reporting checbox is not displayed under 'Create Service' page, when 'Performance Reporting' checkbox is selected");
					if (perCosreopt) {
						Report.LogInfo("Info",
								"Per CoS Performance Reporting checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected",
								"PASS");
						boolean perCoSselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport,
								"per CoS performance report");
						if (perCoSselection) {
							Report.LogInfo("Info",
									" 'Per CoS Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'",
									"FAIL");
						} else {
							Report.LogInfo("Info",
									"Per CoS Performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected",
									"PASS");

							// Actelis Based
							waitforPagetobeenable();
							click(Lanlink_Metro_Obj.Lanlink_Metro.perCoSperformncereport);
							waitforPagetobeenable();
							boolean actelisBasedcheckbox = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
							sa.assertTrue(actelisBasedcheckbox,
									" 'Atelis Based' checbox is not displayed under 'Create Service' page, when 'Per CoS Performance Reporting' checkbox is selected");
							if (actelisBasedcheckbox) {
								Report.LogInfo("Info",
										" 'Actelis Based' checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected",
										"PASS");

								boolean actelisBasedselection = isSelected(
										Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox, "acetelis based checkbox");
								if (actelisBasedselection) {
									Report.LogInfo("Info",
											" 'Actelis Based' checkbox is selected by default under 'Management Options' panel in 'Create Service page'",
											"FAIL");
								} else {
									Report.LogInfo("Info",
											"Actelis Based' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected",
											"PASS");

									// Bandwidth Options table
									click(Lanlink_Metro_Obj.Lanlink_Metro.ActelisBasedcheckbox);
									waitforPagetobeenable();

									// Standard CIR text field
									boolean standardCIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.standardCIRtextfield);
									sa.assertTrue(standardCIR,
											" 'Standard CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (standardCIR) {
										Report.LogInfo("Info",
												" 'Standard CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Standard CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

									// Standard EIR Text field
									boolean standardEIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.standardEIRtextfield);
									sa.assertTrue(standardEIR,
											" 'Standard EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (standardEIR) {
										Report.LogInfo("Info",
												" 'Standard EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Standard EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

									// Premium CIR text field
									boolean premiumCIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.premiumCIRtextfield);
									sa.assertTrue(premiumCIR,
											" 'Premium CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (premiumCIR) {
										Report.LogInfo("Info",
												" 'Premium CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Premium CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

									// Premium EIR Text field
									boolean premiumEIR = isElementPresent(
											Lanlink_Metro_Obj.Lanlink_Metro.premiumEIRtextfield);
									sa.assertTrue(premiumEIR,
											" 'Premium EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");
									if (premiumEIR) {
										Report.LogInfo("Info",
												" 'Premium EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									} else {
										Report.LogInfo("Info",
												" 'Premium EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected",
												"PASS");
									}

								}
							} else {
								Report.LogInfo("Info",
										" 'Actelis Based' checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected",
										"PASS");
							}
						}
					} else {
						Report.LogInfo("Info",
								"Per CoS Performance Reporting checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected",
								"FAIL");
					}
				} else {
					Report.LogInfo("Info",
							"performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected",
							"FAIL");
					Reporter.log(
							"performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected");
				}
			} else {
				Report.LogInfo("Info", " 'performance Reporting' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

			// proactive monitoring
			proactiveMonitor = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
			sa.assertTrue(proactiveMonitor, "pro active monitoring checkbox is not displayed");
			if (proactiveMonitor) {
				Report.LogInfo("Info",
						" 'proactive monitoring' checkbox is displying under 'Create Service'page as expected", "PASS");

				boolean proactiveMonitorselection = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring,
						"practive monitoring");
				if (proactiveMonitorselection) {
					Report.LogInfo("Info",
							" 'proactive monitoring' checkbox is selected under 'Management Options' panel in 'Create Service page'",
							"FAIL");
				} else {
					Report.LogInfo("Info",
							"proactive monitoring' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected",
							"PASS");

					// Notification Management Dropdown
					click(Lanlink_Metro_Obj.Lanlink_Metro.proactiveMonitoring);
					Reporter.log("Pro active monitoring check box is selected");
					waitforPagetobeenable();

					boolean notificationManagement = false;
					try {
						notificationManagement = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
						sa.assertTrue(notificationManagement,
								"Notification management dropdown is not displayed when proactive monitoring is selected");
						Reporter.log(
								"Notification management dropdown gets displayed when proactive monitoring is selected");
						if (notificationManagement) {
							Report.LogInfo("Info",
									" 'Notification Management' dropdown is displaying under 'Management Options' panel when 'proactive Monitoring' checkbox is selected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.notificationmanagement);
							try {
								List<WebElement> listofnotificationmanagement = webDriver
										.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
								for (WebElement notificationmanagementtypes : listofnotificationmanagement) {

									boolean match = false;
									for (int i = 0; i < notifyManagement.length; i++) {
										if (notificationmanagementtypes.getText().equals(notifyManagement[i])) {
											match = true;
											Reporter.log("list of notification management are : "
													+ notificationmanagementtypes.getText());
											Report.LogInfo("Info", "list of notification management are : "
													+ notificationmanagementtypes, "PASS");
										}
									}
									sa.assertTrue(match, "");

								}
							} catch (Exception e) {
								Reporter.log("Notification Management dropdown values are mismatching");
								e.printStackTrace();
								Report.LogInfo("Info",
										"  values in Notification management dropdown under Direct Fiber service subtype is not displaying as expected",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected",
									"FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected",
								"FAIL");
						Reporter.log(
								" 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected");
					} catch (Exception ee) {
						ee.printStackTrace();
					}

				}
			} else {
				Report.LogInfo("Info", " 'proactive monitoring' checkbox is not available under 'Create Service' page",
						"FAIL");
			}

			// delivery channel
			scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.ServiceIdentification);

			waitforPagetobeenable();

			try {
				deliveryChanel = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
				sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
				if (deliveryChanel) {
					Report.LogInfo("Info",
							" 'Delivery Channel' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected",
							"PASS");

					click(Lanlink_Metro_Obj.Lanlink_Metro.deliverychannel_withclasskey);
					try {
						List<WebElement> listofdeliverychannel = webDriver
								.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
						for (WebElement deliverychanneltypes : listofdeliverychannel) {

							boolean match = false;
							for (int i = 0; i < deliverychannel.length; i++) {
								if (deliverychanneltypes.getText().equals(deliverychannel[i])) {
									match = true;
									Reporter.log("list of delivery channel are : " + deliverychanneltypes.getText());

								}
							}
							sa.assertTrue(match, "");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Reporter.log("delivery channel dropdown values are mismatching");
						Report.LogInfo("Info",
								"  values in delivery channel dropdowns under Direct Fiber service subtype are not displaying as expected",
								"FAIL");
					}
				} else {
					Report.LogInfo("Info",
							" 'Delivery Channel' dropdown is not avilable under 'Management options' panel in 'Create Service' page",
							"FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Delivery Channel' dropdown is not available under 'Create Service' page",
						"FAIL");
			}

			//// scrolltoend();
			waitforPagetobeenable();

			// VPN topology
			vpnTopology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
			sa.assertTrue(vpnTopology, "vpn topology dropdown is not displayed");
			if (vpnTopology) {
				Report.LogInfo("Info",
						" VPN Topology' dropdown is displaying under 'Configuration Options' panel in 'Create Service' page as expected",
						"PASS");
				Reporter.log("VPN topology dropdown is displaying as expected");

				// Check default values present inside VPN Topology dropdown
				boolean defaultTOpologValues = (webDriver.findElement(By.xpath("//span[contains(text(),'Point-to-Point')]"))).isDisplayed();
				if (defaultTOpologValues) {
					Report.LogInfo("Info",
							" Under 'VPN Topology' dropdown, 'Point-to-Point' is displaying by default as expected",
							"PASS");
					Reporter.log("The default topology value is displaying as :" + defaultTOpologValues);
				} else {
					Report.LogInfo("Info",
							" Under 'VPN Topology' dropdown, 'Point-to-Point' is not displaying by default", "FAIL");
				}

				click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology,"Intermediate technology");
				waitforPagetobeenable();

				//// scrolltoend();
				waitforPagetobeenable();

				// find list of values under VPN topology dropdown
				click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);

				try {
					List<WebElement> listofvpntopology = webDriver
							.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
					for (WebElement vpntopologytyeps : listofvpntopology) {

						boolean match = false;
						for (int i = 0; i < VPNtopology.length; i++) {
							if (vpntopologytyeps.getText().equals(VPNtopology[i])) {
								match = true;
								Reporter.log("list of vpn topologies are : " + vpntopologytyeps.getText());
								Reporter.log("list of vpn topologies: " + vpntopologytyeps.getText());
								Report.LogInfo("Info", "list of vpn topologies: " + vpntopologytyeps, "PASS");

							}
						}
						sa.assertTrue(match, "");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Reporter.log("vpn topology dropdown values are mismatching");
				}

				for (int i = 0; i < VPNtopology.length; i++) {

					//// scrolltoend();
					waitforPagetobeenable();

					Reporter.log("VPN Toplogy length is: " + VPNtopology.length);
					Reporter.log(VPNtopology[i] + " is the values going to pass inside vpn topology dropdown");

					if (VPNtopology[i].equals("E-PN (Any-to-Any)")) {
						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNtopology[i] + "')]")).click();;
						waitforPagetobeenable();

						Reporter.log(
								"Under 'VPN Topology', When 'E-PN (Any-to-Any)' is selected, 'Circuit type' and 'Intermediate technology' should get disapper"
										+ "only Circuit reference field should occur ");

						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup when circuit reference field is clicked is: " + text);
								Report.LogInfo("Info",
										"on clicking circuit reference field, alert popup message displays as: " + text,
										"PASS");

								CircuitReferencepopupalertmsg = false;
								click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
								waitforPagetobeenable();
							}
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'E-PN (Any-to-Any)'",
									"FAIL");
						}

					}

					else if (VPNtopology[i].equals("Hub&Spoke")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNtopology[i] + "')]")).click();
						waitforPagetobeenable();

						Reporter.log(
								"Under 'VPN Topology', When 'Hub&Spoke'is selected, 'Circuit type', 'Circuit reference' and 'Intermediate technology' should get disapper"
										+ "only Circuit reference field should occur ");

					} else if (VPNtopology[i].equals("Point-to-Point")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.VPNtopology);
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNtopology[i] + "')]")).click();;
						waitforPagetobeenable();

						// Intermediate technology field
						try {
							click(Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology,"Intermediate technology");
							intermediateTechnology = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.IntermediateTechnology);

							sa.assertTrue(intermediateTechnology, "intermediate technology field is not displayed");
							if (intermediateTechnology) {
								Report.LogInfo("Info",
										" 'Intermediate technology' field is displaying under 'Configuration Options' panel in 'Create Service' page, when 'VPN Topology' selected as 'Point-to-point'",
										"PASS");
							} else {
								Report.LogInfo("Info",
										" 'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'",
										"FAIL");
							}
						} catch (Exception e) {
							e.printStackTrace();
							Report.LogInfo("Info",
									" 'Intermediate technology' field is not available under 'Configuration Options' panel in 'Create Service' page when 'VPN Topology' selected as 'Point-to-point'",
									"FAIL");
						}

						// Circuit Reference
						circuitref = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);
						sa.assertTrue(circuitref, "circuit reference field is not displayed");
						if (circuitref) {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is displaying under 'Configuration Options' panel in 'Create Service' page as expected, when 'VPN Topology' selected as 'point-to-point'",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.circuitReference);

							boolean CircuitReferencepopupalertmsg = isElementPresent(
									Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);

							while (CircuitReferencepopupalertmsg) {
								String text = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.circuitreferencealertmessage);
								Reporter.log("The alert popup on clicking circuit reference field is : " + text);
								Report.LogInfo("Info",
										" on clicking 'Circuit reference' , alert emssage popup as : " + text, "PASS");

								CircuitReferencepopupalertmsg = false;
							}

							click(Lanlink_Metro_Obj.Lanlink_Metro.xButton,"X button");
							waitforPagetobeenable();
						} else {
							Report.LogInfo("Info",
									" 'Circuit Reference' field is not available under 'Configuration Options' panel in 'Create Service' page,  when 'VPN Topology' selected as 'Point-to-Point'",
									"FAIL");
						}

						// Circuit type
						boolean circuitTypedefaultvalue = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.circuitType_MSPselected_Default);
						if (circuitTypedefaultvalue) {
							Report.LogInfo("Info", " 'Circuit type' value is displaying as 'Default' as expected",
									"PASS");
							Reporter.log(" 'Circuit type' value is displaying as 'Default' as expected");
						} else {
							Report.LogInfo("Info", " 'Circuit type' value is not displaying as 'Default'", "PASS");
							Reporter.log(" 'Circuit type' value is not displaying as 'Default'");
						}
					}
				}

			} else {
				Report.LogInfo("Info",
						" 'VPN topology' dropdown is not available under 'Configuration Options' panel in 'Create Service' page",
						"FAIL");
			}

			okButton = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.okbutton);
			sa.assertTrue(okButton, "OK button is not displayed");

			cancelButton = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.cancelButton);
			sa.assertTrue(cancelButton, "Cancel button is not displayed");

			//// scrolltoend();
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.cancelButton,"Cancel");
			waitforPagetobeenable();

			sa.assertAll();
			Report.LogInfo("Info", " Fields are verified", "PASS");
		} catch (AssertionError e) {
			e.printStackTrace();
		}

	}

	public void clickOnAMNvalidatorLink() throws InterruptedException, IOException {

		scrollIntoTop();
		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.siteOrder_ActionButton, "Action"); // click
																				// on
																				// Action
																				// button

		click(Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidatorlink, "AMNvalidator");
	}
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////

	public void AMNvalidator(String siteOrderNumber ,String  devicename_Equip,String  csrName,String  cityName,String  countryName)
			throws InterruptedException, IOException {
			boolean AMNvalidatorPanelHeader = false;

		waitforPagetobeenable();

		waitToPageLoad();

		waitforPagetobeenable();

		waitforPagetobeenable();

		String[] statusColumnNamesExpected = { "Site Order", "CSR Site Name", "Device Country", "Device Xing City",
				"Smarts" };
		String[] deviceColumnNamesExpected = { "Device", "Smarts", "Fetch Interfaces" };

		List<String> ls = new ArrayList<String>();
		List<String> ls1 = new ArrayList<String>();

		try {
			AMNvalidatorPanelHeader = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_panelHeader);

			if (AMNvalidatorPanelHeader) {
				Report.LogInfo("Info", "'AMN Validator' panel is displaying", "PASS");
				Reporter.log("'AMN Validator' panel is displaying");

				// check Status Panel column Header
				List<String> status_PanelHeaders = new ArrayList<>(
						Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_status_columnHeader)));
				for (String panels : status_PanelHeaders) {

					boolean match = false;
					for (int i = 0; i < statusColumnNamesExpected.length; i++) {
						if (panels.equals(statusColumnNamesExpected[i])) {
							match = true;
							ls.add(panels);
						} else {
							// Report.LogInfo("Info", "Under 'Site Orders'
							// column, Column name for " + panels.getText() + "
							// is not displaying","FAIL");
							Reporter.log(
									"Under 'Site Orders' column, Column name for " + panels + " is not displaying");
						}
					}
				}

				Report.LogInfo("Info", "Under 'Site Orders' Panel, list of column names dipslaying are: " + ls, "PASS");
				Reporter.log("Under 'Site Orders' Panel, list of column names dipslaying are: " + ls);

				// verify values under "Status" panel
				compareText("Site Order", Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_statusPanel_siteOrderColumnValue,
						siteOrderNumber); // Site Order Column Name
				compareText("CSR Site Name",
						Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_statusPanel_CSRsiteNameColumnValue, csrName); // CSR
																													// Site
																													// Name
				compareText("Device Xng City", Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_statusPanel_deviceXngCityName,
						cityName); // Device XN City Name
				compareText("Device Country", Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_statusPanel_countryName,
						countryName); // CountryName

				// scrolltoend();
				waitforPagetobeenable();
				// check Device panel column Header
				List<String> device_PanelHeaders = new ArrayList<>(
						Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_device_columnHeader)));
				for (String devicepanels : device_PanelHeaders) {

					boolean match = false;
					for (int i = 0; i < deviceColumnNamesExpected.length; i++) {
						if (devicepanels.equals(deviceColumnNamesExpected[i])) {
							match = true;
							ls1.add(devicepanels);

						} else {
							Reporter.log("Under 'Devices For Services' panel, Column name for " + devicepanels
									+ " is not displaying");
						}
					}
				}

				Report.LogInfo("Info", "Under 'Devices For Service' Panel, List Of column Names dispaying are: " + ls1,
						"PASS");
				Reporter.log("Under 'Devices For Service' Panel, List Of column Names dispaying are: " + ls1);

				// verify vales under 'device' panel
		//		compareText("DeviceValue", Lanlink_Metro_Obj.Lanlink_Metro.AMNvalidator_devicePanel_deviceName, devicename_Equip);

			} else {
				Report.LogInfo("Info", "'AMN Validator' panel is not displaying", "PASS");
				Reporter.log("'AMN Validator' panel is not displaying");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "'AMN Validator' panel is not displaying", "PASS");
			Reporter.log("'AMN Validator' panel is not displaying");
		}
	}

	public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(String cpename, String vender_Overture,
			String vender_Accedian, String snmpro, String managementAddress, String Mepid, String poweralarm_Overture,
			String powerAlarm_Accedian, String MediaselectionActualValue, String Macaddress, String serialNumber,
			String hexaSerialnumber, String linkLostForwarding, String country, String City, String Site,
			String Premise, String newmanagementAddress, String existingmanagementAddress,
			String manageaddressdropdownvalue, String existingcityselectionmode, String newcityselectionmode,
			String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
			String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode,
			String premisename, String premisecode, String technologySelectedfordevicecreation,
			String technology_siteorder) throws InterruptedException, IOException, IOException {

		if (technologySelectedfordevicecreation.equalsIgnoreCase("Overture")) {
			deviceCreatoin_Overture(cpename, vender_Overture, snmpro, managementAddress, Mepid, poweralarm_Overture,
					MediaselectionActualValue, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country,
					City, Site, Premise, newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue,
					existingcityselectionmode, newcityselectionmode, cityname, citycode, existingsiteselectionmode,
					newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, newpremiseselectionmode,
					premisename, premisecode, technology_siteorder);

		}

		if ((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian"))
				|| (technologySelectedfordevicecreation.equalsIgnoreCase("Accedian-1G"))) {
			deviceCreatoin_Accedian(cpename, vender_Accedian, snmpro, managementAddress, Mepid, powerAlarm_Accedian,
					MediaselectionActualValue, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country,
					City, Site, Premise, newmanagementAddress, existingmanagementAddress, manageaddressdropdownvalue,
					existingcityselectionmode, newcityselectionmode, cityname, citycode, existingsiteselectionmode,
					newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, newpremiseselectionmode,
					premisename, premisecode);
		}

	}

	public void IntEquip_clickonviewButton(String devicename) throws InterruptedException, IOException {

		waitToPageLoad();

		clickOnBankPage();

		scrollIntoTop();
		waitforPagetobeenable();

		boolean viewpage = false;
		try {
			viewpage = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.viewCPEdevicepage_devices);

			if (viewpage) {
				Reporter.log("In view page");
			} else {

				// scrolltoend();
				waitforPagetobeenable();

				webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'" + devicename
						+ "')]]]//span[text()='View']")).click();
				waitforPagetobeenable();
			}
		} catch (Exception e) {
			e.printStackTrace();

			// scrolltoend();
			waitforPagetobeenable();

			webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'" + devicename
					+ "')]]]//span[text()='View']")).click();
			waitforPagetobeenable();
		}
	}

	public void device_macAddressWarningMessage() throws InterruptedException, IOException {
		boolean macErr = false;
		try {
			macErr = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macAdressErrmsg);
			sa.assertTrue(macErr, "MAC Address warning message is not displayed ");
			if (macErr) {
				String macadresErrMsg = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macAdressErrmsg);
				Reporter.log("MAC Address  message displayed as : " + macadresErrMsg);
				Report.LogInfo("Info", " validation message for MAC Address field displayed as : " + macadresErrMsg,
						"PASS");
				Reporter.log("MAC Address warning message displayed as : " + macadresErrMsg);
			} else {
				Report.LogInfo("Info", " 'MAC Address' warning message is not displaying under 'Add cpe device' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Mac Adress warning message is not dipslaying");
			Report.LogInfo("Info", " MAC Address warning message is not displaying", "FAIL");
		} catch (Exception er) {
			er.printStackTrace();
		}
	}

	public void editSiteOrder_VLANEtherType(String VlanEtherType) throws InterruptedException, IOException {

		boolean VLANEtherTypeAvailability = false;
		try {
			VLANEtherTypeAvailability = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);

			if (VLANEtherTypeAvailability) {
				if (VlanEtherType.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "'VLAN Ether Type' dropdown value is not edited", "PASS");
				} else {
					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAnEtheryTypeDropdown_xbutton);
					waitforPagetobeenable();
					click("//div[label[text()='VLAN Ether Type']]//div[text()='" + VlanEtherType + "']");
					waitforPagetobeenable();
					Reporter.log("'VLAN Ether Type' dropdown selected");

					String actualValue = getTextFrom("//div[label[text()='VLAN Ether Type']]//span");
					Report.LogInfo("Info", "Edited value for 'VLAN Ether Type' dropdown is: " + actualValue, "PASS");

				}
			} else {
				Report.LogInfo("Info", " 'VLAN Ether Type' dropdown is not available under 'Edit Site Order' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'VLAN Ether Type' dropdown is not available under 'Edit Site Order' page", "FAIL");
			Reporter.log(" 'VLAN Ether Type' dropdown is not available under 'Edit Site Order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", VlanEtherType + " is not selected under 'VLAN Ether type' dropdown", "FAIL");
			Reporter.log(VlanEtherType + " is not edited under 'VLAN Ether type' dropdown");
		}

	}

	public void editsiteorder_VLAN(String VLAN) throws InterruptedException, IOException {

		boolean VLANAvilability = false;
		try {
			VLANAvilability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);

			if (VLANAvilability) {
				if (VLAN.equalsIgnoreCase("null")) {

					Report.LogInfo("Info", "VLAN text field value is not edited", "PASS");

				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN, VLAN, "VLAN");
					waitforPagetobeenable();

					String VLANactualvalue = getAttributeFrom(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN, "value");
					Report.LogInfo("Info", "Edited value for 'VLAN' text field is: " + VLANactualvalue, "PASS");
				}
			} else {
				Report.LogInfo("Info", " VLAN text field is not available under 'Edit Site Order' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " VLAN text field is not available under 'Edit Site Order' page", "FAIL");
			Reporter.log(" VLAN text field is not available under 'Edit Site Order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " Not able to edit value under 'VLAN' text field", "FAIL");
			Reporter.log(" Not able to edit value under 'VLAN' text field");
		}

	}

	public void editSiteOrder_GCRoloType(String GCRoloType) throws InterruptedException, IOException {

		boolean GCRoloTypeAvailability = false;
		try {
			GCRoloTypeAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);

			if (GCRoloTypeAvailability) {
				if (GCRoloType.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "'GCR OLO Type' dropdown is not edited", "PASS");
				} else {
					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown_xbutton);
					waitforPagetobeenable();
					click("//div[text()='" + GCRoloType + "']");
					waitforPagetobeenable();
					Reporter.log("'GCR OLO Type' dropdown selected");

					String actualvalue = getTextFrom("//div[label[text()='GCR OLO Type']]//span");
					Report.LogInfo("Info", "Edited value for 'GCR OLO Type' dropdown is: " + actualvalue, "PASS");
				}
			} else {
				Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not available under 'Edit Site Order' page",
						"FAIL");
			}

		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not available under 'Edit Site Order' page", "FAIL");
			Reporter.log(" 'GCR OLO Type' dropdown is not available under 'Edit Site Order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", GCRoloType + " not available under 'GCR OLO type' dropdown", "FAIL");
			Reporter.log(GCRoloType + " not available under 'GCR OLO type' dropdown");
		}

	}

	public void editSiteOrder_mappingMode(String mappingmode, String portBased, String vlanBased)
			throws InterruptedException, IOException {

		boolean mappingModeAvailability = false;
		try {
			mappingModeAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_mappingModedropdown);
			if (mappingModeAvailability) {
				Report.LogInfo("Info", " 'Mapping Mode' dropdown is displaying in 'Edit Site Order' page as expected",
						"PASS");
				Reporter.log(" 'Mapping Mode' dropdown is displaying in 'Edit Site Order' page as expected");

				if (mappingmode.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", " 'mapping Mode' dropdown is not edited", "PASS");
					Reporter.log(" 'mapping Mode' dropdown is not edited");

				} else {
					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_mappingModedropdown_xbutton);
					waitforPagetobeenable();
					click("//div[text()='" + mappingmode + "']");
					waitforPagetobeenable();

					String actualValue = getTextFrom("//div[label[text()='Mapping Mode']]//span");
					Report.LogInfo("Info", " 'Mapping mode' dropdown is edited as: " + actualValue, "PASS");

					if (actualValue.equalsIgnoreCase("Port Based")) {

						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.portname_textField, portBased, "Port Name");

					} else if (actualValue.equalsIgnoreCase("Vlan Based")) {

						sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.vlanid_textfield, vlanBased, "VLAN Id");
					}

				}
			} else {
				Report.LogInfo("Info", " 'Mapping mode' dropdown is not displaying under 'Edit Site order' page",
						"PASS");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Mapping mode' dropdown is not displaying under 'Edit Site order' page", "FAIL");
			Reporter.log(" 'Mapping mode' dropdown is not displaying under 'Edit Site order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " Not able to edit value under 'Mapping Mode' dropdown", "FAIL");
			Reporter.log(" Not able to edit value under 'Mapping Mode' dropdown");
		}
	}

	public void deviceCreatoin_Overture(String cpename, String vender, String snmpro, String managementAddress,
			String Mepid, String poweralarm, String MediaselectionActualValue, String Macaddress, String serialNumber,
			String hexaSerialnumber, String linkLostForwarding, String country, String City, String Site,
			String Premise, String newmanagementAddress, String existingmanagementAddress,
			String manageaddressdropdownvalue, String existingcityselectionmode, String newcityselectionmode,
			String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
			String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode,
			String premisename, String premisecode, String technology_siteOrder)
			throws InterruptedException, IOException, IOException {

		try {
			String linklostForwardingcheckboxstate = "enabled";

			String[] Vender = { "Overture ISG-26", "Overture ISG-26R", "Overture ISG-26S", "Overture ISG140",
					"Overture ISG180", "Overture ISG6000" };

			String[] powerAlarm = { "AC", "DC" };

			String[] MediaSelectionExpectedValue = { "SFP-A with SFP-B", "RJ45-A with SFP-B" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

			//// scrolltoend();
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Country Error Message
			device_countrywarningMessage();

			// Media Selection Error Message
			// device_mediaSelectionWarningMessage(application);

		//	scrollDown("//label[text()='Name']");
			waitforPagetobeenable();

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Power Alarm Error Message
			device_powerAlarmWarningMessage();

			// MAC Address Error Message
			device_macAddressWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address dropdown
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// Power Alarm
			device_powerAlarm(poweralarm);

			// MAC Address
			device_MAcaddress(Macaddress);

			// Media Selection
			if(technology_siteOrder.equals("Accedian-1G")) {
				Reporter.log("media Selection field will not display for 'Accedian-1G' technology");
			}else {
				device_mediaSelection(MediaSelectionExpectedValue, MediaselectionActualValue);
			}

			//// scrolltoend();
			waitforPagetobeenable();

			// Link lost Forwarding
			device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);

			// Country
			device_country(country);

			// City
			if (existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
				addCityToggleButton();
				// New City
				newcity(newcityselectionmode, cityname, citycode);
				// New Site
				newSite(newsiteselectionmode, sitename, sitecode);
				// New Premise
				newPremise(newpremiseselectionmode, premisename, premisecode);

			}

			else if (existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
				// Existing City
				existingCity(City);

				// Site

				if (existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
					// Existing Site
					existingSite(Site);

					// Premise
					if (existingpremiseselectionmode.equalsIgnoreCase("yes")
							& newpremiseselectionmode.equalsIgnoreCase("no")) {
						existingPremise(Premise);

					} else if (existingpremiseselectionmode.equalsIgnoreCase("no")
							& newpremiseselectionmode.equalsIgnoreCase("yes")) {
						// New Premise
						addPremiseTogglebutton();
						newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
					}

				}

				else if (existingsiteselectionmode.equalsIgnoreCase("no")
						& newsiteselectionmode.equalsIgnoreCase("yes")) {
					// New Site
					addSiteToggleButton();
					newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode);

					// New Premise
					newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
				}
			}

			// OK button

			
			
			
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			scrollIntoTop();
			waitforPagetobeenable();

		//	verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage1_devicename, "Device Name");
		//	verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage2_devicename, "Device Name");
		//	verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage3_devicename, "Device Name");

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			// scrolltoend();
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			// Report.LogInfo("Info", "FAiled while verify the fields for Add
			// CPE device","FAIL");

		}
		waitforPagetobeenable();

	}

	public void deviceCreatoin_Accedian(String cpename, String vender, String snmpro, String managementAddress,
			String Mepid, String poweralarm, String MediaselectionActualValue, String Macaddress, String serialNumber,
			String hexaSerialnumber, String linkLostForwarding, String country, String City, String Site,
			String Premise, String newmanagementAddress, String existingmanagementAddress,
			String manageaddressdropdownvalue, String existingcityselectionmode, String newcityselectionmode,
			String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
			String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode,
			String premisename, String premisecode) throws InterruptedException, IOException, IOException {

		try {
			String linklostForwardingcheckboxstate = "disabled";

			String[] Vender = { "Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX" };

			String[] powerAlarm = { "AC", "DC" };

			String expectedDeviceNameFieldAutopopulatedValue = "<Device>.lanlink.dcn.colt.net";

			// scrolltoend();
			waitforPagetobeenable();

			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			// Country Error Message
			device_countrywarningMessage();

			// serial Number Error Message
			device_serialNumberWarningMessage();

			// Hexa Serial Number Error Message
			device_hexaSerialNumberWarningMessage();

			scrollDown("//label[text()='Name']");
			waitforPagetobeenable();

			// Vendor/Model Error Message
			device_vendorModelWarningMessage();

			// Management Address Error Message
			device_managementAddressWarningMessage();

			// Power Alarm Error Message
			device_powerAlarmWarningMessage();

			// Vendor/Model
			device_vendorModel(Vender, vender);

			// Snmpro
			device_snmPro(snmpro);

			// Management Address dropdown
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);

			// MEP Id
			device_mepID(Mepid);

			// Power Alarm
			device_powerAlarm(poweralarm);

			// serial number
			device_serialNumber(serialNumber);

			// scrolltoend();
			waitforPagetobeenable();

			// Link lost Forwarding
			// device_linklostForwarding(linkLostForwarding,
			// linklostForwardingcheckboxstate);

			// Country
			device_country(country);

			// City
			if (existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
				addCityToggleButton();
				// New City
				newcity(newcityselectionmode, cityname, citycode);
				// New Site
				newSite(newsiteselectionmode, sitename, sitecode);
				// New Premise
				newPremise(newpremiseselectionmode, premisename, premisecode);

			}

			else if (existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
				// Existing City
				existingCity(City);

				// Site

				if (existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
					// Existing Site
					existingSite(Site);

					// Premise
					if (existingpremiseselectionmode.equalsIgnoreCase("yes")
							& newpremiseselectionmode.equalsIgnoreCase("no")) {
						existingPremise(Premise);

					} else if (existingpremiseselectionmode.equalsIgnoreCase("no")
							& newpremiseselectionmode.equalsIgnoreCase("yes")) {
						// New Premise
						addPremiseTogglebutton();
						newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
					}

				}

				else if (existingsiteselectionmode.equalsIgnoreCase("no")
						& newsiteselectionmode.equalsIgnoreCase("yes")) {
					// New Site
					addSiteToggleButton();
					newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode);

					// New Premise
					newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
				}
			}

			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			scrollIntoTop();
			waitforPagetobeenable();

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage1_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage2_devicename, "Device Name");
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.warningmEssage3_devicename, "Device Name");

			// Name
			device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);

			// scrolltoend();
			waitforPagetobeenable();
			click(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_OKbutton, "OK");

			sa.assertAll();

		} catch (AssertionError e) {

			e.printStackTrace();
			// Report.LogInfo("Info", "FAiled while verify the fields for Add
			// CPE device","FAIL");

		}
		waitforPagetobeenable();

	}

	public void selectsubtypeunderServiceTypeSelected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {

		String A_Endtechnologydropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_Endtechnology");
		String B_Endtechnologydropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_Endtechnology");
		String Interfacespeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String modularmsp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
		String autoCreateService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AutocreateService");
		String SelectSubService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Servicesubtype");

		// scrolltoend();
		waitforPagetobeenable();

		if (modularmsp.equalsIgnoreCase("no") && autoCreateService.equalsIgnoreCase("no")) {

			Report.LogInfo("Info",
					"when'Modular msp' and 'Autocreate service' are not selected,   'Interface speed' value and 'Service subtype' value should be selected as mandatory ",
					"PASS");

			// Select interface speed
			addDropdownValues_commonMethod("Interface Speed", Lanlink_Metro_Obj.Lanlink_Metro.InterfaceSpeed,
					Interfacespeed);

			// select service sub type
			boolean serviceSubTypeAvailability = false;
			serviceSubTypeAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			if (serviceSubTypeAvailability) {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is displaying as expected", "PASS");
				Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");

				click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);
				waitforPagetobeenable();
				Reporter.log("clicked on srvice type");
				Report.LogInfo("Info", "Service subtype dropdown has been selected", "PASS");

				if ((SelectSubService.equals("LANLink International")) || (SelectSubService.equals("LANLink Metro"))
						|| SelectSubService.equals("LANLink National") || SelectSubService.equals("OLO - (GCR/EU)")
						|| (SelectSubService.equals("Direct Fiber"))
						|| (SelectSubService.equals("LANLink Outband Management"))) {

					
					addDropdownValues_commonMethod("Service Subtype:",Lanlink_Metro_Obj.Lanlink_Metro.servicesubtypeddwn,SelectSubService);
					Reporter.log("=== Service sub Type selected===");
					Report.LogInfo("Info", "Service sub type" + SelectSubService + " has been selected", "PASS");
				} else {
					Reporter.log(SelectSubService
							+ " is not available under Service subtype dropdown when Modular msp is selected");

					Report.LogInfo("Info", SelectSubService + " is not available when Modular msp is selected."
							+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
							+ "    1) Direct Fiber" + "    2) LANLink International" + "    3) LANLink Metro"
							+ "    4) LANLink National" + "    5) Lanlink Outband management" + "    6) OLO - (GCR/EU)",
							"FAIL");
					// driver.close();
				}

			} else {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is not displaying", "FAIL");
			}

			// clickon(getwebelement(xml.getlocator("//locators/"+application+"/AvailableCircuits")));

			click(Lanlink_Metro_Obj.Lanlink_Metro.Next,"Next button");
			waitforPagetobeenable();

			Reporter.log("Page has to be selected based on service and its subtype selected");

		}

		else if (modularmsp.equalsIgnoreCase("yes") && autoCreateService.equalsIgnoreCase("no")) {

			try {
				click(Lanlink_Metro_Obj.Lanlink_Metro.modularmspcheckbox);
				Report.LogInfo("Info", "Modular msp checkbox has been selected", "PASS");
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "Modular msp check box is not available", "FAIL");

			}

			// select service sub type
			boolean serviceSubTypeAvailability = false;
			serviceSubTypeAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			if (serviceSubTypeAvailability) {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is displaying as expected", "PASS");
				Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");

				click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);
				waitforPagetobeenable();
				Reporter.log("clicked on srvice type");
				Report.LogInfo("Info", "Service subtype dropdown has been selected", "PASS");

				if (SelectSubService.equals("LANLink International") || SelectSubService.equals("LANLink Metro")
						|| SelectSubService.equals("LANLink National") || SelectSubService.equals("OLO - (GCR/EU)")) {

					String el2 = ("//div[contains(text(),'" + SelectSubService + "')]");
					click(el2);
					Reporter.log("=== Service sub Type selected===");
					Report.LogInfo("Info", "Service sub type" + SelectSubService + " has been selected", "PASS");
				} else {
					Reporter.log(SelectSubService
							+ " is not available under Service subtype dropdown when Modular msp is selected");

					Report.LogInfo("Info",
							SelectSubService + " is not available when Modular msp is selected."
									+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
									+ "    1) LANLink International" + "    2) LANLink Metro"
									+ "    3) LANLink National" + "    4) OLO - (GCR/EU)",
							"FAIL");
					// driver.close();
				}

			} else {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is not displaying", "FAIL");
			}

			// SendKeys(getwebelement(xml.getlocator("//locators/"+Application+"/AvailableCircuits")),
			// Availablecircuits);

			click(Lanlink_Metro_Obj.Lanlink_Metro.Next,"Next button");
			waitforPagetobeenable();

			Reporter.log("Page has to be selected based on service and its subtype selected");

		}

		if (modularmsp.equalsIgnoreCase("no") && autoCreateService.equalsIgnoreCase("yes")) {

			Reporter.log("Only auto creta check box is selected");

			try {
				click(Lanlink_Metro_Obj.Lanlink_Metro.AutocreateServicecheckbox);
				Report.LogInfo("Info", " 'Autocreateservice' checkbox is selected ", "PASS");
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", " 'Auto create service' checkbox is not available under 'Service' panel ",
						"FAIL");
			}

			// A end technology
			addDropdownValues_commonMethod("A-End Technology", Lanlink_Metro_Obj.Lanlink_Metro.A_Endtechnology,
					A_Endtechnologydropdown);

			// B end technology
			addDropdownValues_commonMethod("B-End Technology", Lanlink_Metro_Obj.Lanlink_Metro.B_Endtechnology,
					B_Endtechnologydropdown);

			// Interface speed
			addDropdownValues_commonMethod("Interface Speed", Lanlink_Metro_Obj.Lanlink_Metro.InterfaceSpeed,
					Interfacespeed);

			// select service sub type

			boolean serviceSubTypeAvailability = false;
			serviceSubTypeAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			if (serviceSubTypeAvailability) {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is displaying as expected", "PASS");
				Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");

				click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);
				waitforPagetobeenable();
				Reporter.log("clicked on srvice type");
				Report.LogInfo("Info", "Service subtype dropdown has been selected", "PASS");

				if ((SelectSubService.equals("LANLink International")) || (SelectSubService.equals("LANLink Metro"))
						|| SelectSubService.equals("LANLink National") || SelectSubService.equals("OLO - (GCR/EU)")
						|| (SelectSubService.equals("Direct Fiber"))
						|| (SelectSubService.equals("LANLink Outband Management"))) {

					String el2 = ("//div[contains(text(),'" + SelectSubService + "')]");
					click(el2);
					Reporter.log("=== Service sub Type selected===");
					Report.LogInfo("Info", "Service sub type " + SelectSubService + " has been selected", "PASS");
				} else {
					Reporter.log(SelectSubService
							+ " is not available under Service subtype dropdown when Modular msp is selected");

					Report.LogInfo("Info", SelectSubService + " is not available when Modular msp is selected."
							+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
							+ "    1) Direct Fiber" + "    2) LANLink International" + "    3) LANLink Metro"
							+ "    4) LANLink National" + "    5) Lanlink Outband management" + "    6) OLO - (GCR/EU)",
							"FAIL");

					// driver.close();
				}

			} else {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is not displaying", "FAIL");
			}
			// scrolltoend();

			click(Lanlink_Metro_Obj.Lanlink_Metro.Next,"Next button");
			waitforPagetobeenable();

			Reporter.log("Page has to be selected based on service and its subtype selected");

		}

		if (modularmsp.equalsIgnoreCase("yes") && autoCreateService.equalsIgnoreCase("yes")) {

			// modular msp
			try {
				click(Lanlink_Metro_Obj.Lanlink_Metro.modularmspcheckbox);
				Report.LogInfo("Info", "Modular msp checkbox has been selected", "PASS");
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "Modular msp check box is not available under 'Service' panel", "FAIL");

			}

			// Auto create service
			try {
				click(Lanlink_Metro_Obj.Lanlink_Metro.AutocreateServicecheckbox);
				Report.LogInfo("Info", " 'Autocreateservice' checkbox is selected ", "PASS");
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", " 'Auto create service' checkbox is not available under 'Service' panel ",
						"FAIL");
			}

			// A end technology
			addDropdownValues_commonMethod("A-End Technology", Lanlink_Metro_Obj.Lanlink_Metro.A_Endtechnology,
					A_Endtechnologydropdown);

			// B end technology
			addDropdownValues_commonMethod("B-End Technology", Lanlink_Metro_Obj.Lanlink_Metro.B_Endtechnology,
					B_Endtechnologydropdown);

			// select service sub type
			boolean serviceSubTypeAvailability = false;
			serviceSubTypeAvailability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);

			if (serviceSubTypeAvailability) {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is displaying as expected", "PASS");
				Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");

				click(Lanlink_Metro_Obj.Lanlink_Metro.ServiceSubtype);
				waitforPagetobeenable();
				Reporter.log("clicked on srvice type");
				Report.LogInfo("Info", "Service subtype dropdown has been selected", "PASS");

				if (SelectSubService.equals("LANLink International") || SelectSubService.equals("LANLink Metro")
						|| SelectSubService.equals("LANLink National") || SelectSubService.equals("OLO - (GCR/EU)")) {

					String el2 = ("//div[contains(text(),'" + SelectSubService + "')]");
					click(el2);
					Reporter.log("=== Service sub Type selected===");
					Report.LogInfo("Info", "Service sub type" + SelectSubService + " has been selected", "PASS");
				} else {
					Reporter.log(SelectSubService
							+ " is not available under Service subtype dropdown when Modular msp is selected");

					Report.LogInfo("Info",
							SelectSubService + " is not available when Modular msp is selected."
									+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
									+ "    1) LANLink International" + "    2) LANLink Metro"
									+ "    3) LANLink National" + "    4) OLO - (GCR/EU)",
							"FAIL");
					// driver.close();
				}

			} else {
				Report.LogInfo("Info", " 'Service subtype mandatory dropdown is not displaying", "FAIL");
			}

			//// scrolltoend();
			click(Lanlink_Metro_Obj.Lanlink_Metro.Next,"Next button");
			waitforPagetobeenable();
			

			Reporter.log("Page has to be selected based on service and its subtype selected");

		}

	}

	public void device_MAcaddress(String macAdressInput) {

		boolean macAdres = false;
		try {
			macAdres = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macaddress);
			sa.assertTrue(macAdres, "Mepid field under 'Add CPE device' page is not available");

			if (macAdres) {
				Report.LogInfo("Info", " ' MAC Address' field is displaying in 'Add CPE Device' page as expected",
						"PASS");
				Reporter.log(" 'MAC Address'  text field is displaying as expected");

				if (macAdressInput.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "No values has been passed for 'MAC Address' text field for adding device",
							"FAIL");
					Reporter.log("No values has been passed for 'MAC Address' mandaotyr field");

				} else {

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_macaddress, macAdressInput, "macAdressInput");
					waitforPagetobeenable();

					Report.LogInfo("Info", macAdressInput + " is entered under 'MAc Address' text field", "PASS");
					Reporter.log(macAdressInput + " is entered under 'MAc Address' text field");

				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'MAC Address' text field is not available", "FAIL");
		} catch (Exception err) {
			err.printStackTrace();
			Report.LogInfo("Info", "Not able to enter value inside the 'MAC Address' text field", "FAIL");
		}
	}

	public void device_mediaSelection(String Mediaselection[], String mediaSelection)
			throws InterruptedException, IOException {

		addDropdownValues_commonMethod("Media Selection", Lanlink_Metro_Obj.Lanlink_Metro.AddCPEdevice_mediaselection,
				mediaSelection);

	}

	public void addSiteOrderValues_OnnetOffnet(String interfaceSpeed, String country, String city, String CSR_Name,
			String site, String performReport, String ProactiveMonitor, String smartmonitor, String technology,
			String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider, String sitevalue,
			String remark, String xngcityname, String xngcitycode, String devicename, String nonterminatepoinr,
			String Protected, String newcityselection, String existingcityselection, String existingsiteselection,
			String newsiteselection) throws InterruptedException, IOException {

		Countyr_AddSiteOrder(country);

		City_AddSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode, sitevalue, CSR_Name,
				existingsiteselection, newsiteselection);

		// scroll to bottom

		waitforPagetobeenable();

		performancereporting_AddSiteOrder(performReport);

		proactiveMonitoring_AddSiteOrder(ProactiveMonitor);

		smartsMonitoring_AddSiteOrder(smartmonitor);

		SiteAlias_AddSiteOrder(siteallias);

		VLANid_AddSiteOrder(VLANid);

		DCAEnabledSite_AddSiteOrder(DCAenabledsite, cloudserviceprovider);

		remark_AddSiteOrder(remark);

		technologyP2P_AddSiteOrder(technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);

		waitforPagetobeenable();

		click(Lanlink_Metro_Obj.Lanlink_Metro.okbutton);

	}

	public void Countyr_AddSiteOrder(String country) throws InterruptedException, IOException {

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Country,"Country");
		addDropdownValues_commonMethod("Device Country",Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Country, country);
		//webDriver.findElement(By.xpath("//span[text()='" + country + "']")).click();

	}

	public void City_AddSiteorder(String existingcityselection, String city, String newcityselection,
			String xngcityname, String xngcitycode, String sitevalue, String CSR_Name, String existingsiteselection,
			String newsiteselection) throws InterruptedException, IOException {

		// Existing City
		if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

			// Clickon(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_disabledCitytogglebutton")));
			// waitforPagetobeenable();
			//verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_City, "City");
			click(Lanlink_OLO_Obj.Lanlink_OLO.Addsiteorder_City);
			addDropdownValues_commonMethod("Device Xng City",Lanlink_OLO_Obj.Lanlink_OLO.Addsiteorder_City, city);


			Report.LogInfo("Info", city + " is selected under Device Xng City dropdown", "PASS");
			Reporter.log(city + " is selected under Device Xng City dropdown");

			// Existing Site
			if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {

				click(Lanlink_OLO_Obj.Lanlink_OLO.Addsiteorder_Sitetogglebutton);
				waitforPagetobeenable();
				click(Lanlink_OLO_Obj.Lanlink_OLO.Addsiteorder_sitedropdown);
				webDriver.findElement(By.xpath("//span[text()='" + sitevalue + "']")).click();
				Report.LogInfo("Info", sitevalue + " is selected under Physical Site dropdown", "PASS");
				Reporter.log(sitevalue + " is selected under Physical Site dropdown");

			}

			// New site
			if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {

				// Clickon(Lanlink_Metro_Obj.Lanlink_Metro.AddsiteOrdr_disabledSitetogglebutton")));
				// waitforPagetobeenable();

				if (CSR_Name.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "CSR name field is mandatory and no values are provided", "FAIL");
					Reporter.log("No values provided for mandatory field 'CSR Name'");

				} else {

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_CSRname, CSR_Name, "CSR Name");
					waitforPagetobeenable();

					Report.LogInfo("Info", CSR_Name + " is entered under CSR Name field", "PASS");
					Reporter.log(CSR_Name + " is entered under CSR Name field");

				}
			}

		} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Citytogglebutton);
			waitforPagetobeenable();

			// City name
			if (xngcityname.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", "City name field is a mandatory field and the value is not provided", "FAIL");
				Reporter.log("City name field is a mandatory field and the value is not provided");
			} else {
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_xngcityname, xngcityname,
						"xngcityname");
				waitforPagetobeenable();
				Report.LogInfo("Info", xngcityname + " is entered in City name field", "PASS");
				Reporter.log(xngcityname + " is entered in City name field");
				waitforPagetobeenable();
			}

			// City code
			if (xngcitycode.equalsIgnoreCase("null")) {
				Report.LogInfo("Info", "City Code field is a mandatory field and the value is not provided", "FAIL");
				Reporter.log("no values provided for city code text field");
			} else {
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_XNGcitycode, xngcitycode,
						"xngcitycode");
				waitforPagetobeenable();
				Report.LogInfo("Info", xngcitycode + " is entered in City Code field", "PASS");
				Reporter.log(xngcitycode + " is entered in City Code field");

			}
			waitforPagetobeenable();

			// add new Site
			try {
				if (CSR_Name.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "CSR name field is mandatory and no values are provided", "FAIL");
					Reporter.log(" no values provided for 'CSR Name' text field");

				} else {

					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_CSRname, CSR_Name, "CSR_Name");
					waitforPagetobeenable();
					Report.LogInfo("Info", CSR_Name + " is entered under CSR Name field", "PASS");
					Reporter.log(CSR_Name + " is entered under CSR Name field");
				}

			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", " CSR NAme not performed", "FAIL");
			}

		}

	}

	public void performancereporting_AddSiteOrder(String performReport) throws InterruptedException, IOException {

		// Perfomance Reporting
		if (performReport.equalsIgnoreCase("Null")) {

			Reporter.log("Performance reporting value is not provided. 'Follow Service' is selected by default PASS");
		} else {
			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereporting_xbutton,
					"Add siteorder Performancereporting xbutton");
			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_performancereporting_xbutton,
					"Add siteorder Performancereporting xbutton");

			waitforPagetobeenable();
			// Clickon(getwebelement("//div[label[text()='Performance
			// Reporting']]//div[text()='"+ performReport +"']"));

			addDropdownValues_commonMethod("Performance Reporting",Lanlink_Metro_Obj.Lanlink_Metro.performancereport,performReport);
			waitforPagetobeenable();

			// String
			// actualvalue=getwebelement("(//div[label[text()='Performance
			// Reporting']]//span)[2]").getText();
			String actualvalue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.performancereport);

			Reporter.log(actualvalue + " is selected under Performance reporting dropdown PASS");

		}
	}

	public void proactiveMonitoring_AddSiteOrder(String ProactiveMonitor) throws InterruptedException, IOException {

		// Pro active monitoring
		if (ProactiveMonitor.equalsIgnoreCase("Null")) {

			Reporter.log("Pro active monitoring value is not provided. 'Follow Service' is selected by default PASS");
		} else {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsitorder_proactivemonitoring_xbutton,
					"Add sitorder proactive monitoring xbutton");
			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsitorder_proactivemonitoring_xbutton,
					"Add sitorder_proactive monitoring xbutton");
			waitforPagetobeenable();
			addDropdownValues_commonMethod("Proactive Monitoring",Lanlink_Metro_Obj.Lanlink_Metro.ProactiveMonitor,ProactiveMonitor);
			// Clickon(getwebelement("//div[label[text()='Proactive
			// Monitoring']]//div[text()='"+ ProactiveMonitor +"']"));
			waitforPagetobeenable();

			String actualvalue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ProactiveMonitor);

			Reporter.log(actualvalue + "is selected under proactive monitoring dropdown PASS");

		}
	}

	public void smartsMonitoring_AddSiteOrder(String smartmonitor) throws InterruptedException, IOException {

		if (smartmonitor.equalsIgnoreCase("null")) {

			Reporter.log("INFO Smart monitoring value is not provided Follow Service' is selected by default PASS");
		} else {

			verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoring_xbutton,
					"Add siteorder smart monitoring xbutton");
			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_smartmonitoring_xbutton,
					"Add siteorder smart monitoring xbutton");
			waitforPagetobeenable();

			
			addDropdownValues_commonMethod("Smarts Monitoring",Lanlink_Metro_Obj.Lanlink_Metro.smartmonitor,smartmonitor);

			
			// Clickon(getwebelement("//div[label[text()='Proactive
			// Monitoring']]//div[text()='"+ ProactiveMonitor +"']"));
			waitforPagetobeenable();

			String actualvalue = getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.smartmonitor);

			Reporter.log(actualvalue + " is selected under Smart monitoring dropdown PASS");

		}

	}

	public void SiteAlias_AddSiteOrder(String siteallias) throws InterruptedException, IOException {

		if (siteallias.equalsIgnoreCase("null")) {

			Reporter.log("No values entered for 'Site Alias' field");
		} else {
			try {
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias, siteallias);
				waitforPagetobeenable();

				String actualvalue = getAttributeFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_sitealias, "value");
				Reporter.log(actualvalue + " is entered under 'Site Alias' field");
			} catch (Exception err) {
				err.printStackTrace();
				Reporter.log(" Not able to enter value under 'Site Alias' field");
			}
		}

	}

	public void editsiteorder_primaryVLAN(String primaryVLAN) throws InterruptedException, IOException, IOException {

		boolean primaryVLANAvilability = false;
		try {
			primaryVLANAvilability = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_PrimaryVLAN);

			if (primaryVLANAvilability) {
				if (primaryVLAN.equalsIgnoreCase("null")) {

					Report.LogInfo("Info", " Primary VLAN text field value is not edited", "PASS");

				} else {
					clearTextBox(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_PrimaryVLAN);
					waitforPagetobeenable();
					sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_PrimaryVLAN, primaryVLAN, "Primary VLAN");
					waitforPagetobeenable();
					String primaryVLANactualvalue = getAttributeFrom(
							Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_PrimaryVLAN, "value");
					Report.LogInfo("Info", "Edited value for 'Primary VLAN' text field is: " + primaryVLANactualvalue,
							"PASS");
				}
			} else {
				Report.LogInfo("Info", " Primary VLAN text field is not available under 'Edit Site Order' page",
						"FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " Primary VLAN text field is not available under 'Edit Site Order' page", "FAIL");
			Reporter.log(" Primary VLAN text field is not available under 'Edit Site Order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " Not able to edit 'Primary VLAn Type' text field", "FAIL");
			Reporter.log(" Not able to edit 'Primary VLAn Type' text field");
		}

	}

	public void editSiteOrder_primaryVLANEtherType(String primaryVlanEtherType)
			throws InterruptedException, IOException {

		boolean primaryVLANEtherTypeAvailability = false;
		try {
			primaryVLANEtherTypeAvailability = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown);

			if (primaryVLANEtherTypeAvailability) {
				if (primaryVlanEtherType.equalsIgnoreCase("null")) {
					Report.LogInfo("Info", "'Primary VLAN Ether Type' dropdown value is not edited", "PASS");
				} else {
					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown_xbutton);
					waitforPagetobeenable();
					click("//div[label[text()='Primary VLAN Ether Type']]//div[text()='" + primaryVlanEtherType + "']");
					waitforPagetobeenable();
					Reporter.log("'Primary VLAN Ether Type' dropdown selected");

					String actualValue = getTextFrom("//div[label[text()='Primary VLAN Ether Type']]//span");
					Report.LogInfo("Info",
							"Edited value for 'Primary VLAN Ether Type' dropdown is: " + primaryVlanEtherType, "PASS");

				}
			} else {
				Report.LogInfo("Info",
						" 'Primary VLAN Ether Type' dropdown is not available under 'Edit Site Order' page", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Primary VLAN Ether Type' dropdown is not available under 'Edit Site Order' page",
					"FAIL");
			Reporter.log(" 'Primary VLAN Ether Type' dropdown is not available under 'Edit Site Order' page");
		} catch (Exception ee) {
			ee.printStackTrace();
			Report.LogInfo("Info", " Not able to edit value under 'Primary Vlan Ether type' dropdown", "FAIL");
			Reporter.log(" Not able to edit value under 'Primary Vlan Ether type' dropdown");
		}
	}

	public void validatesiteOrderNumber_AddSiteOrder() throws InterruptedException, IOException {

		// Site Order Number Field
		boolean siteorderNmber = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_siteordernumbertextfield);
		sa.assertTrue(siteorderNmber, " 'site order number' field is not displayed");
		if (siteorderNmber) {
			Report.LogInfo("Info",
					" 'Site Order Number (Siebel Service ID)' text field is displaying under 'Add Site order' page as expected",
					"PASS");
		} else {
			Report.LogInfo("Info",
					" 'site order number (Siebel Service ID)' text field is not displaying under 'Add Site order' page",
					"FAIL");
		}

	}

	public void validateIVReference_AddSiteorder() throws InterruptedException, IOException {

		boolean IVReference = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
		sa.assertTrue(IVReference, " 'IV reference' dropdown is not displayed");
		if (IVReference) {

			click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
			List<String> listofIVreference = new ArrayList<>(
					Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

			if (listofIVreference.size() >= 1) {
				for (String IVreferencetypes : listofIVreference) {

					Reporter.log("list of IV References : " + IVreferencetypes);
					Reporter.log("list of IV References for AddSite order are: " + IVreferencetypes);
					Report.LogInfo("Info", "list of IV References for AddSite order are: " + IVreferencetypes, "PASS");

				}

			} else {
				Reporter.log("no values are available inside 'IV reference' dropdown for Add site order");
				Report.LogInfo("Info", "no values are available inside 'IV reference' dropdown for Add site order",
						"FAIL");
			}

		} else {
			Report.LogInfo("Info",
					"Mandatory field 'IV Reference' dropdown is not available under 'Add Site ORder' page", "FAIL");
		}
	}

	public void editSite(String editExistingCity, String editNewCity, String dropdown_xpath,
			String selectSiteToggleButton, String addSitetoggleButton, String dropdownValue, String editNewSiteName,
			String editNewSiteCode, String labelname) throws InterruptedException, IOException {

		if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {

			existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);

		}

		else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {

			existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);

		}

		else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {

			newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);

		}

		else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {

			newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);

		}

		else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {

			Report.LogInfo("Info", "No changes made under 'Site' field", "PASS");
			Reporter.log("No changes made under 'Site' field");

		}

	}

	public void technologyDropdown_MSPselected_Primary() throws InterruptedException, IOException {

		String Technology = "Accedian";

		String technology;
		boolean devicename, Nonterminationpointcheckbox, portectedcheckbox, GCRoloField, Vlan, VlanetherType;

		// Technology dropdown
		technology = (Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);

		// sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}
		}

		for (int k = 0; k < Technology.length(); k++) {

			// Accedian
			if ((Technology.equalsIgnoreCase("Accedian"))) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology);
				clickonTechnology(technologySelected, Technology);

			}
		}
	}

	public void technologyDropdown_p2p_mspselected() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Accedian", "Cyan" };

		boolean technology;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {
				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Device Name
					verifySiteOrderField_deviceName();
				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Accedian
				else if ((Technology[k].equalsIgnoreCase("Accedian"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}

				// Cyan
				else if (Technology[k].equalsIgnoreCase("Cyan")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}
	}

	public void existingCity(String dropdown_xpath, String dropdownValue, String selectCityToggleButton,
			String labelname) throws InterruptedException, IOException, IOException {

		boolean cityDisplayed = false;
		try {
			cityDisplayed = isElementPresent(dropdown_xpath);

			if (cityDisplayed) {

				selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);

			} else {

				click(Lanlink_Metro_Obj.Lanlink_Metro.selectCityToggleButton, "Select City toggle button");
				Thread.sleep(1000);

				selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);

			}
		} catch (Exception e) {
			e.printStackTrace();

			click(Lanlink_Metro_Obj.Lanlink_Metro.selectCityToggleButton, "Select City toggle button");

			selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);
		}

	}

	public void existingPremise(String dropdown_xpath, String dropdownValue, String selectPremiseToggleButton,
			String labelname) throws InterruptedException, IOException, IOException {

		boolean premiseDisplayed = false;
		try {
			premiseDisplayed = isElementPresent(dropdown_xpath);

			if (premiseDisplayed) {

				selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);

			} else {
				click(Lanlink_Metro_Obj.Lanlink_Metro.selectPremiseToggleButton, "Select Premise toggle button");
				Thread.sleep(1000);

				selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);

			}
		} catch (Exception e) {
			e.printStackTrace();

			click(Lanlink_Metro_Obj.Lanlink_Metro.selectPremiseToggleButton, "Select Premise toggle button");

			selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);
		}

	}

	public void existingSite(String dropdown_xpath, String dropdownValue, String selectSiteToggleButton,
			String labelname) throws InterruptedException, IOException {

		boolean siteDisplayed = false;
		try {
			siteDisplayed = isElementPresent(dropdown_xpath);

			if (siteDisplayed) {

				selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);

			} else {
				click(Lanlink_Metro_Obj.Lanlink_Metro.selectSiteToggleButton, "Select Site toggle button");
				Thread.sleep(1000);

				selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);

			}
		} catch (Exception e) {
			e.printStackTrace();

			click(Lanlink_Metro_Obj.Lanlink_Metro.selectSiteToggleButton, "Select Site toggle button");

			selectValueInsideDropdown(dropdown_xpath, labelname, dropdownValue);
		}

	}

	public void newSite(String dropdown_xpath, String addSitetoggleButton, String editNewSiteName,
			String editNewSiteCode, String labelname) throws InterruptedException, IOException {

		boolean siteDisplayed = false;
		try {
			siteDisplayed = isElementPresent(dropdown_xpath);

			if (siteDisplayed) {

				click(addSitetoggleButton, "Select City toggle button");
				Thread.sleep(1000);

				// Site name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addSiteToggleSelected, editNewSiteName,
						"Site Name");

				// Site Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addSiteToggleSelected, editNewSiteCode,
						"Site Code");

			} else {

				// Site name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addCityToggleSelected, editNewSiteName,
						"Site Name");

				// Site Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addCityToggleSelected, editNewSiteCode,
						"Site Code");

			}
		} catch (Exception e) {
			e.printStackTrace();

			// Site name
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitenameinputfield_addCityToggleSelected, editNewSiteName,
					"Site Name");

			// Site Code
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.sitecodeinputfield_addCityToggleSelected, editNewSiteCode,
					"Site Code");

		}

	}

	public void newCity(String dropdown_xpath, String addCitytoggleButton, String editNewCityName,
			String editNewCityCode, String labelname) throws InterruptedException, IOException {

		boolean cityDisplayed = false;
		try {
			cityDisplayed = isElementPresent(dropdown_xpath);

			if (cityDisplayed) {

				click(addCitytoggleButton, "Select City toggle button");
				Thread.sleep(1000);

				// City name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citynameinputfield, editNewCityName, "City Name");

				// City Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citycodeinputfield, editNewCityCode, "City Code");

			} else {

				// City name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citynameinputfield, editNewCityName, "City Name");

				// City Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citycodeinputfield, editNewCityCode, "City Code");
			}
		} catch (Exception e) {
			e.printStackTrace();

			// City name
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citynameinputfield, editNewCityName, "City Name");

			// City Code
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.citycodeinputfield, editNewCityCode, "City Code");

		}

	}

	public void newPremise(String dropdown_xpath, String addPremisetoggleButton, String editNewPremiseName,
			String editNewPremiseCode, String labelname) throws InterruptedException, IOException {

		boolean premiseDisplayed = false;
		try {
			premiseDisplayed = isElementPresent(dropdown_xpath);

			if (premiseDisplayed) {

				click(addPremisetoggleButton, "Select Premise toggle button");
				Thread.sleep(1000);

				// Premise name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addPremiseToggleSelected, editNewPremiseName,
						"Premise Name");

				// Premise Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addPremiseToggleSelected, editNewPremiseCode,
						"Premise Code");

			} else {

				// Premise name
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addCityToggleSelected, editNewPremiseName,
						"Premise Name");

				// Premise Code
				sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode,
						"Premise Code");

			}
		} catch (Exception e) {
			e.printStackTrace();

			// Premise name
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisenameinputfield_addCityToggleSelected, editNewPremiseName,
					"Premise Name");

			// Premise Code
			sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.premisecodeinputfield_addCityToggleSelected, editNewPremiseCode,
					"Premise Code");

		}

	}

	public void validateCircuitreference_AddSiteOrder() throws InterruptedException, IOException {

		boolean circuitReference = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceField);

		sa.assertTrue(circuitReference, "Circuit Reference field is not displayed");
		if (circuitReference) {
			Report.LogInfo("Info",
					"Mandatory field 'Circuit Reference' text field is displaying under 'Add Site order' page as expected",
					"PASS");
		} else {
			Report.LogInfo("Info",
					"Mandatory field 'Circuit Reference' text field is not displaying under 'Add Site order' page",
					"FAIL");
		}
	}

	public void validateEPNEOSDH_AddSiteOrder() throws InterruptedException, IOException {

		// EPN EOSDH checkbox
		boolean EPNEOSDH = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNEOSDHCheckbox);

		sa.assertTrue(EPNEOSDH, " EPN EOSDH checkbox is not displayed");
		boolean EPNEOSDHcheckbox = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNEOSDHCheckbox,
				"EPNEOSDHCheckbox");
		sa.assertFalse(EPNEOSDHcheckbox, "  EPN EOSDH checkbox is selected");

		if (EPNEOSDH) {
			Report.LogInfo("Info", "'EPN EOSDH' text field is displaying under 'Add Site order' page as expected",
					"PASS");
		} else {
			Report.LogInfo("Info", "'EPN EOSDH' text field is not displaying under 'Add Site order' page", "FAIL");
		}

		if (EPNEOSDHcheckbox) {
			Report.LogInfo("Info", " 'EPN EOSDH' checkbox is selected by default", "FAIL");
		} else {
			Report.LogInfo("Info", " 'EPN EOSDH' checkbox is not selected by default", "PASS");
		}
	}

	public void validateAccessCircuitID_AddSiteOrder() throws InterruptedException, IOException {

		boolean accessCircuitID = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_AccessCircuitID);
		sa.assertTrue(accessCircuitID, " 'Access Circuit ID' text field is not displayed");
		if (accessCircuitID) {
			Report.LogInfo("Info",
					"Mandatory field 'Access Circuit ID' text field is displaying when 'IV Reference' selected as 'Access' under 'Add Site order' page as expected",
					"PASS");

			// verify mandatory message
			click(Lanlink_Metro_Obj.Lanlink_Metro.obutton_spanTag, "OK");
			waitforPagetobeenable();

			boolean circuitIDwarningMesage = isElementPresent(
					Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_AccessCircuitIDwarningMessage);
			if (circuitIDwarningMesage) {

				String warningMessage = getTextFrom(
						Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_AccessCircuitIDwarningMessage);
				Report.LogInfo("Info", " Warning message for 'Circuit ID' field is displaying as: " + warningMessage,
						"PASS");
				Reporter.log(" Warning message for 'Circuit ID' field is displaying as: " + warningMessage);

			} else {
				Report.LogInfo("Info", " Warning message for 'Access Circuit ID' field is not displaying", "FAIL");
				Reporter.log(" Warning message for 'Access Circuit ID' field is not displaying");
			}

		} else {
			Report.LogInfo("Info",
					"Mandatory field 'Access Circuit' text field is not displaying when 'IV Reference' selected as 'Access' under 'Add Site order' page as expected",
					"FAIL");
		}
	}

	public void validateEPNoffnet_AddSiteOrder() throws InterruptedException, IOException {

		// EPN Offnet checkbox
		boolean EPNoffnet = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNoffnetCheckbox);
		sa.assertTrue(EPNoffnet, " EPN Offnet checkbox is not displayed");
		boolean EPNoffnetcheckbox = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_EPNoffnetCheckbox,
				"EPNoffnetCheckbox");
		sa.assertFalse(EPNoffnetcheckbox, "  EPN Offnet checkbox is selected");

		if (EPNoffnet) {
			Report.LogInfo("Info", "'EPN Offnet' text field is displaying under 'Add Site order' page as expected",
					"PASS");
		} else {
			Report.LogInfo("Info", "'EPN Offnet' text field is not displaying under 'Add Site order' page", "FAIL");
		}

		if (EPNoffnetcheckbox) {
			Report.LogInfo("Info", " 'EPN Offnet' checkbox is selected by default", "FAIL");
		} else {
			Report.LogInfo("Info", " 'EPN Offnet' checkbox is not selected by default as expected", "PASS");
		}
	}

	public void technologyDropdownFor1GigE_EPN_Primary() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Belgacom VDSL" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox, GCRoloField, Vlan,
				VlanetherType;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {

				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Device Name Text Field
					verifySiteOrderField_deviceName();

				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Device Name
					verifySiteOrderField_deviceName();
				}

				// Alu
				else if (Technology[k].equals("Alu")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Device Name
					verifySiteOrderField_deviceName();

				}

				// Accedian-1G
				else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Device Name
					verifySiteOrderField_deviceName();
				}

				// Belgacom VDSL
				else if (Technology[k].equalsIgnoreCase("Belgacom VDSL")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

	}

	public void circuitreferenceDisabled_EPN() throws InterruptedException, IOException {
		boolean circuitReference = false;
		try {
			circuitReference = isEnable(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_circuitReferenceField);
			if (circuitReference) {
				Report.LogInfo("Info",
						" 'Circuit reference' text field is enabled, when 'Iv Reference' selected as 'Access' for modular msp service",
						"FAIL");
			} else {
				Report.LogInfo("Info",
						" 'Circuit reference' text field is disabled as expected, when 'Iv Reference' selected as 'Access'",
						"PASS");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info",
					" 'Circuit reference' text field is enabled, when 'Iv Reference' selected as 'Access' for modular msp service",
					"FAIL");
		}

	}

	public void technologyDropdown_MSPselected_EPN_Access() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Accedian", "Cyan" };

		String[] GCROLOType = { "2A3", "1A4", "1A3", "2A4/1A4" };

		String[] VLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox, GCRoloField, Vlan,
				VlanetherType;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {
				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Mapping Mode
					// -- verifySiteorderFields_mappingMode();

					// Device Name Text Field
					verifySiteOrderField_deviceName();
				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// GCR OLO Type dropdown
					try {
						GCRoloField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
						sa.assertTrue(GCRoloField, "GCR OLO Type dropdown is not Available");
						if (GCRoloField) {
							Report.LogInfo("Info",
									" 'GCR OLO Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
							List<String> listofGcrOLOtype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofGcrOLOtype.size() >= 1) {
								for (String GCROlotypes : listofGcrOLOtype) {
									boolean match = false;
									for (int i = 0; i < GCROLOType.length; i++) {
										if (GCROlotypes.equals(GCROLOType[i])) {
											match = true;
											Reporter.log("list of 'GCR OLO Type' are : " + GCROlotypes);
											Report.LogInfo("Info",
													"The list of 'GCR OLO Type' inside dropdown is: " + GCROlotypes,
													"PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'GCR OLO Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'GCR OLO Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'GCR OLO Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not Available under 'Add Site order' page",
								"FAIL");
					}

					// VLAN Text field
					try {
						Vlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
						sa.assertTrue(Vlan, " 'VLAN' text field is not available");
						if (Vlan) {
							Report.LogInfo("Info",
									" 'VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"PASS");
						} else {
							Report.LogInfo("Info",
									" 'VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page",
								"FAIL");
					}

					// VLAN Ether Type
					try {
						VlanetherType = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
						sa.assertTrue(VlanetherType, " 'VLAN Ether Type' dropdown is not Available");
						if (VlanetherType) {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
							List<String> listofVLANethertype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofVLANethertype.size() >= 1) {
								for (String VLANEthertypes : listofVLANethertype) {
									boolean match = false;
									for (int i = 0; i < VLANEtherType.length; i++) {
										if (VLANEthertypes.equals(VLANEtherType[i])) {
											match = true;
											Reporter.log("list of 'VLAN Ether Type' are : " + VLANEthertypes);
											Report.LogInfo("Info", "The list of 'VLAN Ether Type' inside dropdown is: "
													+ VLANEthertypes, "PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'VLAN Ether type' dropdown is not Available under 'Add Site order' page", "FAIL");
					}
				}

				// Accedian
				else if ((Technology[k].equalsIgnoreCase("Accedian"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Cyan
				else if (Technology[k].equalsIgnoreCase("Cyan")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

	}

	public void technologyDropdownFor1GigE_EPN_Access() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Belgacom VDSL" };

		String[] GCROLOType = { "2A3", "1A4", "1A3", "2A4/1A4" };

		String[] VLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox, GCRoloField, Vlan,
				VlanetherType;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);

		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {
				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Mapping Mode
					// -- verifySiteorderFields_mappingMode();

					// Device Name Text Field
					verifySiteOrderField_deviceName();
				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Device Name
					verifySiteOrderField_deviceName();

					// GCR OLO Type dropdown
					try {
						GCRoloField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
						sa.assertTrue(GCRoloField, "GCR OLO Type dropdown is not Available");
						if (GCRoloField) {
							Report.LogInfo("Info",
									" 'GCR OLO Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
							List<String> listofGcrOLOtype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofGcrOLOtype.size() >= 1) {
								for (String GCROlotypes : listofGcrOLOtype) {
									boolean match = false;
									for (int i = 0; i < GCROLOType.length; i++) {
										if (GCROlotypes.equals(GCROLOType[i])) {
											match = true;
											Reporter.log("list of 'GCR OLO Type' are : " + GCROlotypes);
											Report.LogInfo("Info",
													"The list of 'GCR OLO Type' inside dropdown is: " + GCROlotypes,
													"PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'GCR OLO Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'GCR OLO Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'GCR OLO Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not Available under 'Add Site order' page",
								"FAIL");
					}

					// VLAN Text field
					try {
						Vlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
						sa.assertTrue(Vlan, " 'VLAN' text field is not available");
						if (Vlan) {
							Report.LogInfo("Info",
									" 'VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"PASS");
						} else {
							Report.LogInfo("Info",
									" 'VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page",
								"FAIL");
					}

					// VLAN Ether Type
					try {
						VlanetherType = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
						sa.assertTrue(VlanetherType, " 'VLAN Ether Type' dropdown is not Available");
						if (VlanetherType) {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
							List<String> listofVLANethertype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofVLANethertype.size() >= 1) {
								for (String VLANEthertypes : listofVLANethertype) {
									boolean match = false;
									for (int i = 0; i < VLANEtherType.length; i++) {
										if (VLANEthertypes.equals(VLANEtherType[i])) {
											match = true;
											Reporter.log("list of 'VLAN Ether Type' are : " + VLANEthertypes);
											Report.LogInfo("Info", "The list of 'VLAN Ether Type' inside dropdown is: "
													+ VLANEthertypes, "PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'VLAN Ether type' dropdown is not Available under 'Add Site order' page", "FAIL");
					}
				}

				// Alu
				else if (Technology[k].equalsIgnoreCase("Alu")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Mapping Mode
					// -- verifySiteorderFields_mappingMode();

					// Device Name
					verifySiteOrderField_deviceName();

				}

				// Accedian
				else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Device Name
					verifySiteOrderField_deviceName();
				}

				// Cyan
				else if (Technology[k].equalsIgnoreCase("Belgacom VDSL")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}
	}

	public void technologyDropdownFor10GigE_HubAndSpoke_primary() throws InterruptedException, IOException {

		String Technology = "Accedian";

		boolean technology;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		Reporter.log("site order to be selected is: " + Technology);
		click("//div[text()='" + Technology + "']");

		// Non Termination Point
		verifySiteOrderFields_NonterminationField();

	}

	public void technologyDropdownFor10GigE_HubAndSpoke_Access() throws InterruptedException, IOException {

		String Technology = "Accedian";

		boolean technology;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		Reporter.log("site order to be selected is: " + Technology);
		click("//div[text()='" + Technology + "']");

		// Non Termination Point
		verifySiteOrderFields_NonterminationField();

	}

	public void validatespokeId_AddSiteOrder() throws InterruptedException, IOException {
		try {
			boolean spokeId = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_spokeIdField);
			if (spokeId) {
				boolean spokeIDvalue = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_spokeId);

				if (spokeIDvalue) {
					Report.LogInfo("Info", " 'Spoke Id' value is displaying as '0' by default as expected", "PASS");
				} else {
					Report.LogInfo("Info", " 'Spoke Id' is not displaying as expected", "FAIL");
				}
			} else {
				Report.LogInfo("Info", " 'Spoke Id' field is not displaying under 'Add Site Order' page", "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", " 'Spoke Id' field is not displaying under 'Add Site Order' page", "FAIL");
		}

	}

	public void validateoffnet_AddSiteOrder() throws InterruptedException, IOException {

		// Offnet checkbox

		boolean offnet = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox);

		sa.assertTrue(offnet, "Offnet field is not displayed");
		boolean offnetcheckbox = isSelected(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox, "offnetCheckbox");
		sa.assertFalse(offnetcheckbox, " Offnet checkbox is selected");

		if (offnet) {
			Report.LogInfo("Info", "'Offnet' text field is displaying under 'Add Site order' page as expected", "PASS");
		} else {
			Report.LogInfo("Info", "'Offent' text field is not displaying under 'Add Site order' page", "FAIL");
		}

		if (offnetcheckbox) {
			Report.LogInfo("Info", " 'Offnet' checkbox is selected by default", "FAIL");
		} else {
			Report.LogInfo("Info", " 'Offnet' checkbox is not selected by default as expected", "PASS");
		}
	}

	public void technologyDropdownFor1GigE_EPNEOSDHselected_Primary() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Belgacom VDSL" };

		boolean technology;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);

		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {

				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Device Name Text Field
					verifySiteOrderField_deviceName();

				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// VLAN Text field
					boolean Vlan = false;
					try {
						Vlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
						sa.assertTrue(Vlan, " 'VLAN' text field is not available");
						if (Vlan) {
							Report.LogInfo("Info",
									" 'VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"PASS");
						} else {
							Report.LogInfo("Info",
									" 'VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page",
								"FAIL");
					}

					// VLAN Ether Type
					boolean VlanetherType = false;
					try {
						VlanetherType = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
						sa.assertTrue(VlanetherType, " 'VLAN Ether Type' dropdown is not Available");
						if (VlanetherType) {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
							List<String> listofVLANethertype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofVLANethertype.size() >= 1) {
								for (String VLANEthertypes : listofVLANethertype) {
									Reporter.log("list of 'VLAN Ether Type' are : " + VLANEthertypes);
									Report.LogInfo("Info",
											"The list of 'VLAN Ether Type' inside dropdown is: " + VLANEthertypes,
											"PASS");

								}
							} else {
								Reporter.log(
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'VLAN Ether type' dropdown is not Available under 'Add Site order' page", "FAIL");
					}
				}

				// Alu
				else if (Technology[k].equals("Alu")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Device Name
					verifySiteOrderField_deviceName();

				}

				// Accedian
				else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))
						|| (Technology[k].equalsIgnoreCase("Accedian"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// Device Name
					// verifySiteOrderField_deviceName();
				}

				// Belgacom VDSL
				else if (Technology[k].equalsIgnoreCase("Belgacom VDSL")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();
				}
			}
		} else {
			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}
	}

	public void technologyDropdownFor1GigE_EPNEOSDHselected_Access() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Belgacom VDSL" };

		String[] GCROLOType = { "2A3", "1A4", "1A3", "2A4/1A4" };

		String[] VLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox, GCRoloField, Vlan,
				VlanetherType;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				String technologyValue = technologytypes;
			}
		}

		for (int k = 0; k < Technology.length; k++) {
			// Actelis
			if (Technology[k].equalsIgnoreCase("Actelis")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);
			}

			// Atrica
			else if (Technology[k].equalsIgnoreCase("Atrica")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();

				// Mapping Mode
				// -- verifySiteorderFields_mappingMode();

				// Device Name Text Field
				verifySiteOrderField_deviceName();
			}

			// Overture
			else if (Technology[k].equalsIgnoreCase("Overture")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();

				// Device Name
				verifySiteOrderField_deviceName();

				// GCR OLO Type dropdown
				try {
					GCRoloField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
					sa.assertTrue(GCRoloField, "GCR OLO Type dropdown is not Available");
					if (GCRoloField) {
						Report.LogInfo("Info",
								" 'GCR OLO Type' drodpown field is displaying under 'Add Site Order' page as expected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
						List<String> listofGcrOLOtype = new ArrayList<>(
								Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

						if (listofGcrOLOtype.size() >= 1) {
							for (String GCROlotypes : listofGcrOLOtype) {
								boolean match = false;
								for (int i = 0; i < GCROLOType.length; i++) {
									if (GCROlotypes.equals(GCROLOType[i])) {
										match = true;
										Reporter.log("list of 'GCR OLO Type' are : " + GCROlotypes);
										Report.LogInfo("Info",
												"The list of 'GCR OLO Type' inside dropdown is: " + GCROlotypes,
												"PASS");

									}
								}
								sa.assertTrue(match,"");

							}
						} else {
							Reporter.log("no values are available inside 'GCR OLO Type' dropdown for Add site order");
							Report.LogInfo("Info",
									"no values are available inside 'GCR OLO Type' dropdown for Add site order",
									"FAIL");
						}
					} else {
						Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not available under 'Add Site order' page",
								"FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not Available under 'Add Site order' page",
							"FAIL");
				}

				// VLAN Text field
				try {
					Vlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
					sa.assertTrue(Vlan, " 'VLAN' text field is not available");
					if (Vlan) {
						Report.LogInfo("Info",
								" 'VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
								"PASS");
					} else {
						Report.LogInfo("Info",
								" 'VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
								"FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page", "FAIL");
				}

				// VLAN Ether Type
				try {
					VlanetherType = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
					sa.assertTrue(VlanetherType, " 'VLAN Ether Type' dropdown is not Available");
					if (VlanetherType) {
						Report.LogInfo("Info",
								" 'VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
						List<String> listofVLANethertype = new ArrayList<>(
								Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

						if (listofVLANethertype.size() >= 1) {
							for (String VLANEthertypes : listofVLANethertype) {
								boolean match = false;
								for (int i = 0; i < VLANEtherType.length; i++) {
									if (VLANEthertypes.equals(VLANEtherType[i])) {
										match = true;
										Reporter.log("list of 'VLAN Ether Type' are : " + VLANEthertypes);
										Report.LogInfo("Info",
												"The list of 'VLAN Ether Type' inside dropdown is: " + VLANEthertypes,
												"PASS");

									}
								}
								sa.assertTrue(match,"");

							}
						} else {
							Reporter.log(
									"no values are available inside 'VLAN Ether Type' dropdown for Add site order");
							Report.LogInfo("Info",
									"no values are available inside 'VLAN Ether Type' dropdown for Add site order",
									"FAIL");
						}
					} else {
						Report.LogInfo("Info",
								" 'VLAN Ether Type' dropdown is not available under 'Add Site order' page", "FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'VLAN Ether type' dropdown is not Available under 'Add Site order' page",
							"FAIL");
				}
			}

			// Alu
			else if (Technology[k].equalsIgnoreCase("Alu")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Mapping Mode
				// -- verifySiteorderFields_mappingMode();

				// Device Name
				verifySiteOrderField_deviceName();

			}

			// Accedian
			else if ((Technology[k].equalsIgnoreCase("Accedian-1G")) || (Technology[k].equalsIgnoreCase("Accedian"))) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();

				// Device Name
				verifySiteOrderField_deviceName();
			}

			// Belgacom VDSL
			else if (Technology[k].equalsIgnoreCase("Belgacom VDSL")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();
			}
		}
	}

	public void technologyDropdownFor1GigE_HubAndSpoke_Primary() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Belgacom VDSL" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));
		Reporter.log("Number of technology sizes: " + listoftechnology.size());

		for (String technologytypesSample : listoftechnology) {
			Reporter.log(" list of technologies are: " + technologytypesSample);
		}

		try {
			if (listoftechnology.size() >= 1) {
				for (String technologytypes : listoftechnology) {

					webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);

					Reporter.log("tech value to be found: " + technologytypes);
					Report.LogInfo("Info",
							"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
							"PASS");
					Reporter.log(
							"The list of technology  inside dropdown while  adding site order is: " + technologytypes);
					String technologyValue = technologytypes;
				}

				for (int k = 0; k < Technology.length; k++) {

					// Actelis
					if (Technology[k].equalsIgnoreCase("Actelis")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);
					}

					// Atrica
					else if (Technology[k].equalsIgnoreCase("Atrica")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);

						// Non Termination Point
						verifySiteOrderFields_NonterminationField();

						// Device Name
						verifySiteOrderField_deviceName();
					}

					// Overture
					else if (Technology[k].equalsIgnoreCase("Overture")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);

						// Non Termination Point
						verifySiteOrderFields_NonterminationField();

					}

					// Alu
					else if (Technology[k].equalsIgnoreCase("Alu")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);

						// Device Name
						verifySiteOrderField_deviceName();

					}

					// Accedian-1G
					else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);

						// Non Termination Point
						verifySiteOrderFields_NonterminationField();

					}

					// Belgacom VDSL
					else if (Technology[k].equalsIgnoreCase("Belgacom VDSL")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);

					}

				}
			} else {

				Reporter.log("no values are available inside technology dropdown for Add site order");
				Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
			}
		} catch (StaleElementReferenceException e) {

			e.printStackTrace();

		}

	}

	public void technologyDropdownFor1GigE_HubAndSpoke_Access() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Belgacom VDSL" };

		String[] GCROLOType = { "2A3", "1A4", "1A3", "2A4/1A4" };

		String[] VLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		String[] primaryVLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox, GCRoloField, Vlan,
				VlanetherType, PrimaryVlan, primaryVlanetherType;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {

				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

					// GCR OLO Type dropdown
					try {
						GCRoloField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
						sa.assertTrue(GCRoloField, "GCR OLO Type dropdown is not Available");
						if (GCRoloField) {
							Report.LogInfo("Info",
									" 'GCR OLO Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
							List<String> listofGcrOLOtype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofGcrOLOtype.size() >= 1) {
								for (String GCROlotypes : listofGcrOLOtype) {
									boolean match = false;
									for (int i = 0; i < GCROLOType.length; i++) {
										if (GCROlotypes.equals(GCROLOType[i])) {
											match = true;
											Reporter.log("list of 'GCR OLO Type' are : " + GCROlotypes);
											Report.LogInfo("Info",
													"The list of 'GCR OLO Type' inside dropdown is: " + GCROlotypes,
													"PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'GCR OLO Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'GCR OLO Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'GCR OLO Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not Available under 'Add Site order' page",
								"FAIL");
						Reporter.log(" 'GCR OLO Type' dropdown is not Available under 'Add Site order' page");
					} catch (Exception ee) {
						ee.printStackTrace();
						Report.LogInfo("Info", " Not able to enter value under 'GCR OLO type' dropdown", "FAIL");
						Reporter.log(" Not able to enter value under 'GCR OLO type' dropdown");
					}

					// VLAN Text field
					try {
						Vlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
						sa.assertTrue(Vlan, " 'VLAN' text field is not available");
						if (Vlan) {
							Report.LogInfo("Info",
									" 'VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"PASS");
						} else {
							Report.LogInfo("Info",
									" 'VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page",
								"FAIL");
						Reporter.log(" 'VLAN' text field is not Available under 'Add Site order' page");
					}

					// VLAN Ether Type
					try {
						VlanetherType = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
						sa.assertTrue(VlanetherType, " 'VLAN Ether Type' dropdown is not Available");
						if (VlanetherType) {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
							List<String> listofVLANethertype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofVLANethertype.size() >= 1) {
								for (String VLANEthertypes : listofVLANethertype) {
									boolean match = false;
									for (int i = 0; i < VLANEtherType.length; i++) {
										if (VLANEthertypes.equals(VLANEtherType[i])) {
											match = true;
											Reporter.log("list of 'VLAN Ether Type' are : " + VLANEthertypes);
											Report.LogInfo("Info", "The list of 'VLAN Ether Type' inside dropdown is: "
													+ VLANEthertypes, "PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'VLAN Ether type' dropdown is not Available under 'Add Site order' page", "FAIL");
					}

					// Primary VLAN Text field
					try {
						PrimaryVlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_PrimaryVLAN);
						sa.assertTrue(PrimaryVlan, " 'Primary VLAN' text field is not available");
						if (PrimaryVlan) {
							Report.LogInfo("Info",
									" 'Primary VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"PASS");
						} else {
							Report.LogInfo("Info",
									" 'Primary VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'Primary VLAN' text field is not Available under 'Add Site order' page", "FAIL");
						Reporter.log(" 'Primary VLAN' text field is not Available under 'Add Site order' page");
					}

					// Primary VLAN Ether Type
					try {
						primaryVlanetherType = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown);
						sa.assertTrue(primaryVlanetherType, " 'Primary VLAN Ether Type' dropdown is not Available");
						if (primaryVlanetherType) {
							Report.LogInfo("Info",
									" 'Primary VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown);
							List<String> listofprimaryVLANethertype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofprimaryVLANethertype.size() >= 1) {
								for (String primaryVLANEthertypes : listofprimaryVLANethertype) {
									boolean match = false;
									for (int i = 0; i < primaryVLANEtherType.length; i++) {
										if (primaryVLANEthertypes.equals(primaryVLANEtherType[i])) {
											match = true;
											Reporter.log(
													"list of 'Primary VLAN Ether Type' are : " + primaryVLANEthertypes);
											Report.LogInfo("Info",
													"The list of 'Primary VLAN Ether Type' inside dropdown is: "
															+ primaryVLANEthertypes,
													"PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'Primary VLAN Ether Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'Primary VLAN Ether Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'Primary VLAN Ether Type' dropdown is not available under 'Add Site order' page",
									"FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'Primary VLAN Ether type' dropdown is not Available under 'Add Site order' page",
								"FAIL");
					}
				}

				// Alu
				else if (Technology[k].equalsIgnoreCase("Alu")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Accedian-1G
				else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// Non Termination Point
					verifySiteOrderFields_NonterminationField();

				}

				// Belgacom VDSL
				else if (Technology[k].equalsIgnoreCase("Belgacom VDSL")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}

			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

	}

	public void technologyDropdown_MSPselected_HubAndSpoke_Access() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Accedian", "Cyan" };

		String[] GCROLOType = { "2A3", "1A4", "1A3", "2A4/1A4" };

		String[] VLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		String[] primaryVLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		boolean technology, GCRoloField, Vlan, VlanetherType, PrimaryVlan, primaryVlanetherType;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);
		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
				String technologyValue = technologytypes;
			}

			for (int k = 0; k < Technology.length; k++) {

				// Actelis
				if (Technology[k].equalsIgnoreCase("Actelis")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}

				// Atrica
				else if (Technology[k].equalsIgnoreCase("Atrica")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);
				}

				// Overture
				else if (Technology[k].equalsIgnoreCase("Overture")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

					// GCR OLO Type dropdown
					try {
						GCRoloField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
						sa.assertTrue(GCRoloField, "GCR OLO Type dropdown is not Available");
						if (GCRoloField) {
							Report.LogInfo("Info",
									" 'GCR OLO Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
							List<String> listofGcrOLOtype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofGcrOLOtype.size() >= 1) {
								for (String GCROlotypes : listofGcrOLOtype) {
									boolean match = false;
									for (int i = 0; i < GCROLOType.length; i++) {
										if (GCROlotypes.equals(GCROLOType[i])) {
											match = true;
											Reporter.log("list of 'GCR OLO Type' are : " + GCROlotypes);
											Report.LogInfo("Info",
													"The list of 'GCR OLO Type' inside dropdown is: " + GCROlotypes,
													"PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'GCR OLO Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'GCR OLO Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'GCR OLO Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not Available under 'Add Site order' page",
								"FAIL");
						Reporter.log(" 'GCR OLO Type' dropdown is not Available under 'Add Site order' page");
					} catch (Exception ee) {
						ee.printStackTrace();
						Report.LogInfo("Info", " Not able to enter value under 'GCR OLO type' dropdown", "FAIL");
						Reporter.log(" Not able to enter value under 'GCR OLO type' dropdown");
					}

					// VLAN Text field
					try {
						Vlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
						sa.assertTrue(Vlan, " 'VLAN' text field is not available");
						if (Vlan) {
							Report.LogInfo("Info",
									" 'VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"PASS");
						} else {
							Report.LogInfo("Info",
									" 'VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page",
								"FAIL");
						Reporter.log(" 'VLAN' text field is not Available under 'Add Site order' page");
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page",
								"FAIL");
						Reporter.log(" 'VLAN' text field is not Available under 'Add Site order' page");
					}

					// VLAN Ether Type
					try {
						VlanetherType = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
						sa.assertTrue(VlanetherType, " 'VLAN Ether Type' dropdown is not Available");
						if (VlanetherType) {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
							List<String> listofVLANethertype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofVLANethertype.size() >= 1) {
								for (String VLANEthertypes : listofVLANethertype) {
									boolean match = false;
									for (int i = 0; i < VLANEtherType.length; i++) {
										if (VLANEthertypes.equals(VLANEtherType[i])) {
											match = true;
											Reporter.log("list of 'VLAN Ether Type' are : " + VLANEthertypes);
											Report.LogInfo("Info", "The list of 'VLAN Ether Type' inside dropdown is: "
													+ VLANEthertypes, "PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'VLAN Ether Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'VLAN Ether Type' dropdown is not available under 'Add Site order' page", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'VLAN Ether type' dropdown is not Available under 'Add Site order' page", "FAIL");
					}

					// Primary VLAN Text field
					try {
						PrimaryVlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_PrimaryVLAN);
						sa.assertTrue(PrimaryVlan, " 'Primary VLAN' text field is not available");
						if (PrimaryVlan) {
							Report.LogInfo("Info",
									" 'Primary VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"PASS");
						} else {
							Report.LogInfo("Info",
									" 'Primary VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
									"FAIL");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'Primary VLAN' text field is not Available under 'Add Site order' page", "FAIL");
						Reporter.log(" 'Primary VLAN' text field is not Available under 'Add Site order' page");
					}

					// Primary VLAN Ether Type
					try {
						primaryVlanetherType = isElementPresent(
								Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown);
						sa.assertTrue(primaryVlanetherType, " 'Primary VLAN Ether Type' dropdown is not Available");
						if (primaryVlanetherType) {
							Report.LogInfo("Info",
									" 'Primary VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
									"PASS");

							click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown);
							List<String> listofprimaryVLANethertype = new ArrayList<>(
									Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

							if (listofprimaryVLANethertype.size() >= 1) {
								for (String primaryVLANEthertypes : listofprimaryVLANethertype) {
									boolean match = false;
									for (int i = 0; i < primaryVLANEtherType.length; i++) {
										if (primaryVLANEthertypes.equals(primaryVLANEtherType[i])) {
											match = true;
											Reporter.log(
													"list of 'Primary VLAN Ether Type' are : " + primaryVLANEthertypes);
											Report.LogInfo("Info",
													"The list of 'Primary VLAN Ether Type' inside dropdown is: "
															+ primaryVLANEthertypes,
													"PASS");

										}
									}
									sa.assertTrue(match,"");

								}
							} else {
								Reporter.log(
										"no values are available inside 'Primary VLAN Ether Type' dropdown for Add site order");
								Report.LogInfo("Info",
										"no values are available inside 'Primary VLAN Ether Type' dropdown for Add site order",
										"FAIL");
							}
						} else {
							Report.LogInfo("Info",
									" 'Primary VLAN Ether Type' dropdown is not available under 'Add Site order' page",
									"FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						Report.LogInfo("Info",
								" 'Primary VLAN Ether type' dropdown is not Available under 'Add Site order' page",
								"FAIL");
					}
				}

				// Accedian
				else if ((Technology[k].equalsIgnoreCase("Accedian"))) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}

				// Cyan
				else if (Technology[k].equalsIgnoreCase("Cyan")) {

					click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
					String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
							.replace("value", Technology[k]);
					clickonTechnology(technologySelected, Technology[k]);

				}
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

	}

	public void technologyDropdown_mspSelected_HubAndSpoke_Primary() throws InterruptedException, IOException {

		String[] Technology = { "Actelis", "Atrica", "Overture", "Accedian", "Cyan" };

		boolean technology;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);

		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));
		Reporter.log("Number of technology sizes: " + listoftechnology.size());

		for (String technologytypesSample : listoftechnology) {
			Reporter.log(" list of technologies are: " + technologytypesSample);
		}

		try {
			if (listoftechnology.size() >= 1) {
				for (String technologytypes : listoftechnology) {

					webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
					Reporter.log("tech value to be found: " + technologytypes);
					Report.LogInfo("Info",
							"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
							"PASS");
					Reporter.log(
							"The list of technology  inside dropdown while  adding site order is: " + technologytypes);
					String technologyValue = technologytypes;
				}

				for (int k = 0; k < Technology.length; k++) {

					// Actelis
					if (Technology[k].equalsIgnoreCase("Actelis")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);
					}

					// Atrica
					else if (Technology[k].equalsIgnoreCase("Atrica")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);

						// Device Name
						verifySiteOrderField_deviceName();
					}

					// Overture
					else if (Technology[k].equalsIgnoreCase("Overture")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);
					}

					// Accedian
					else if ((Technology[k].equalsIgnoreCase("Accedian"))) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);
					}

					// Cyan
					else if (Technology[k].equalsIgnoreCase("Cyan")) {

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
						String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
								.replace("value", Technology[k]);
						clickonTechnology(technologySelected, Technology[k]);

					}

				}
			} else {

				Reporter.log("no values are available inside technology dropdown for Add site order");
				Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
			}
		} catch (StaleElementReferenceException e) {

			e.printStackTrace();

		}

	}

	public void technologyDropdownFor1GigE_HubAndSpoke_Access_offnetselected(String IVReferenceSelection)
			throws InterruptedException, IOException {

		String[] Technology = { "Overture", "Accedian-1G" };

		String[] GCROLOType = { "2A3", "1A4", "1A3", "2A4/1A4" };

		String[] VLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		String[] primaryVLANEtherType = { "S-VLAN(88A8)", "C-VLAN(8100)" };

		boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox, GCRoloField, Vlan,
				VlanetherType, PrimaryVlan, primaryVlanetherType;

		// Select "IV Reference" as "Primary
		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
		waitforPagetobeenable();

		click("//div[contains(text(),'" + IVReferenceSelection + "')]");
		waitforPagetobeenable();

		// Select "Offnet" checkbox
		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox);
		waitforPagetobeenable();

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);

		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");

			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

		for (int k = 0; k < Technology.length; k++) {

			if (Technology[k].equalsIgnoreCase("Overture")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();

				// GCR OLO Type dropdown
				try {
					GCRoloField = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
					sa.assertTrue(GCRoloField, "GCR OLO Type dropdown is not Available");
					if (GCRoloField) {
						Report.LogInfo("Info",
								" 'GCR OLO Type' drodpown field is displaying under 'Add Site Order' page as expected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_GCRoloTypeDropdown);
						List<String> listofGcrOLOtype = new ArrayList<>(
								Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

						if (listofGcrOLOtype.size() >= 1) {
							for (String GCROlotypes : listofGcrOLOtype) {
								boolean match = false;
								for (int i = 0; i < GCROLOType.length; i++) {
									if (GCROlotypes.equals(GCROLOType[i])) {
										match = true;
										Reporter.log("list of 'GCR OLO Type' are : " + GCROlotypes);
										Report.LogInfo("Info",
												"The list of 'GCR OLO Type' inside dropdown is: " + GCROlotypes,
												"PASS");

									}
								}
								sa.assertTrue(match,"");

							}
						} else {
							Reporter.log("no values are available inside 'GCR OLO Type' dropdown for Add site order");
							Report.LogInfo("Info",
									"no values are available inside 'GCR OLO Type' dropdown for Add site order",
									"FAIL");
						}
					} else {
						Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not available under 'Add Site order' page",
								"FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'GCR OLO Type' dropdown is not Available under 'Add Site order' page",
							"FAIL");
				}

				// VLAN Text field
				try {
					Vlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLAN);
					sa.assertTrue(Vlan, " 'VLAN' text field is not available");
					if (Vlan) {
						Report.LogInfo("Info",
								" 'VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
								"PASS");
					} else {
						Report.LogInfo("Info",
								" 'VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
								"FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'VLAN' text field is not Available under 'Add Site order' page", "FAIL");
				}

				// VLAN Ether Type
				try {
					VlanetherType = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
					sa.assertTrue(VlanetherType, " 'VLAN Ether Type' dropdown is not Available");
					if (VlanetherType) {
						Report.LogInfo("Info",
								" 'VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_VLANEtherTypeDropdown);
						List<String> listofVLANethertype = new ArrayList<>(
								Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

						if (listofVLANethertype.size() >= 1) {
							for (String VLANEthertypes : listofVLANethertype) {
								boolean match = false;
								for (int i = 0; i < VLANEtherType.length; i++) {
									if (VLANEthertypes.equals(VLANEtherType[i])) {
										match = true;
										Reporter.log("list of 'VLAN Ether Type' are : " + VLANEthertypes);
										Report.LogInfo("Info",
												"The list of 'VLAN Ether Type' inside dropdown is: " + VLANEthertypes,
												"PASS");

									}
								}
								sa.assertTrue(match,"");

							}
						} else {
							Reporter.log(
									"no values are available inside 'VLAN Ether Type' dropdown for Add site order");
							Report.LogInfo("Info",
									"no values are available inside 'VLAN Ether Type' dropdown for Add site order",
									"FAIL");
						}
					} else {
						Report.LogInfo("Info",
								" 'VLAN Ether Type' dropdown is not available under 'Add Site order' page", "FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'VLAN Ether type' dropdown is not Available under 'Add Site order' page",
							"FAIL");
				}

				// Primary VLAN Text field
				try {
					PrimaryVlan = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_PrimaryVLAN);
					sa.assertTrue(PrimaryVlan, " 'Primary VLAN' text field is not available");
					if (PrimaryVlan) {
						Report.LogInfo("Info",
								" 'Primary VLAN' field is displaying as expected, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
								"PASS");
					} else {
						Report.LogInfo("Info",
								" 'Primary VLAN' field is not displaying, when 'Technology' is selected as 'Overture' and 'IV Reference' selected as 'Access'",
								"FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", " 'Primary VLAN' text field is not Available under 'Add Site order' page",
							"FAIL");
				}

				// Primary VLAN Ether Type
				try {
					primaryVlanetherType = isElementPresent(
							Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown);
					sa.assertTrue(primaryVlanetherType, " 'Primary VLAN Ether Type' dropdown is not Available");
					if (primaryVlanetherType) {
						Report.LogInfo("Info",
								" 'Primary VLAN Ether Type' drodpown field is displaying under 'Add Site Order' page as expected",
								"PASS");

						click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_primaryVLANEtherTypeDropdown);
						List<String> listofprimaryVLANethertype = new ArrayList<>(
								Arrays.asList(getTextFrom("//div[@class='sc-ifAKCX oLlzc']")));

						if (listofprimaryVLANethertype.size() >= 1) {
							for (String primaryVLANEthertypes : listofprimaryVLANethertype) {
								boolean match = false;
								for (int i = 0; i < primaryVLANEtherType.length; i++) {
									if (primaryVLANEthertypes.equals(primaryVLANEtherType[i])) {
										match = true;
										Reporter.log(
												"list of 'Primary VLAN Ether Type' are : " + primaryVLANEthertypes);
										Report.LogInfo("Info",
												"The list of 'Primary VLAN Ether Type' inside dropdown is: "
														+ primaryVLANEthertypes,
												"PASS");

									}
								}
								sa.assertTrue(match,"");

							}
						} else {
							Reporter.log(
									"no values are available inside 'Primary VLAN Ether Type' dropdown for Add site order");
							Report.LogInfo("Info",
									"no values are available inside 'Primary VLAN Ether Type' dropdown for Add site order",
									"FAIL");
						}
					} else {
						Report.LogInfo("Info",
								" 'Primary VLAN Ether Type' dropdown is not available under 'Add Site order' page",
								"FAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info",
							" 'Primary VLAN Ether type' dropdown is not Available under 'Add Site order' page", "FAIL");
				}

			}

			else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();

			}

		}
	}

	public void technologyDropdownFor1GigE_HubAndSpoke_Primary_offnetselected(String IVReferenceSelection)
			throws InterruptedException, IOException {

		String[] Technology = { "Overture", "Accedian-1G" };

		// Select "IV Reference" as "Primary
		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_IVreferencedropdown);
		waitforPagetobeenable();

		click("//div[contains(text(),'" + IVReferenceSelection + "')]");
		waitforPagetobeenable();

		// Select "Offnet" checkbox
		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_offnetCheckbox);
		waitforPagetobeenable();

		boolean technology, Nonterminationpointcheckbox, portectedcheckbox;

		// Technology dropdown
		technology = isElementPresent(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology);

		sa.assertTrue(technology, "Technology dropdown is not displayed");

		click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
		List<String> listoftechnology = new ArrayList<>(
				Arrays.asList(getTextFrom(Lanlink_Metro_Obj.Lanlink_Metro.ClassNameForDropdowns)));

		if (listoftechnology.size() >= 1) {
			for (String technologytypes : listoftechnology) {

				Reporter.log("list of technology are : " + technologytypes);
				Reporter.log("The list of technology  inside dropdown while  adding site order is: " + technologytypes);
				Report.LogInfo("Info",
						"The list of technology  inside dropdown while  adding site order is: " + technologytypes,
						"PASS");
			}
		} else {

			Reporter.log("no values are available inside technology dropdown for Add site order");
			Report.LogInfo("Info", "no values are available inside technology dropdown for Add site order", "FAIL");
		}

		for (int k = 0; k < Technology.length; k++) {

			if (Technology[k].equalsIgnoreCase("Overture")) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();

			}

			else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

				click(Lanlink_Metro_Obj.Lanlink_Metro.Addsiteorder_Technology, "Technology dropdown");
				String technologySelected = (Lanlink_Metro_Obj.Lanlink_Metro.selectValueUnderTechnologyDropdown)
						.replace("value", Technology[k]);
				clickonTechnology(technologySelected, Technology[k]);

				// Non Termination Point
				verifySiteOrderFields_NonterminationField();

			}

		}
	}

	public void deleteService() throws InterruptedException, IOException, AWTException {
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.orderpanelheader, "Order panel header");
		// ScrollIntoViewByString(Lanlink_Metro_Obj.Lanlink_Metro.orderpanelheader);
		// Delete Service

		scrollDown(Lanlink_Metro_Obj.Lanlink_Metro.serviceactiondropdown);
		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.serviceactiondropdown, "service creation drop down");
		click(Lanlink_Metro_Obj.Lanlink_Metro.serviceactiondropdown);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.delete, "delete option");
		click(Lanlink_Metro_Obj.Lanlink_Metro.delete,"Delete option");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.deleteButton, "Delete");
		click(Lanlink_Metro_Obj.Lanlink_Metro.deleteButton, "Delete");
		
		//waitforPagetobeenable();
		verifysuccessmessage("Service deleted successfully");

	}
	
	public void searchorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		waitforPagetobeenable();
		waitforPagetobeenable();
		String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "serviceNumber");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.ManageCustomerServiceLink, " Manage Customer Service Link");
		mouseMoveOn(Lanlink_Metro_Obj.Lanlink_Metro.ManageCustomerServiceLink);

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.searchorderlink, "search order link");
		click(Lanlink_Metro_Obj.Lanlink_Metro.searchorderlink, "search order link");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.servicefield, "service field");
		sendKeys(Lanlink_Metro_Obj.Lanlink_Metro.servicefield, sid);

		click(Lanlink_Metro_Obj.Lanlink_Metro.searchbutton, "searchbutton");
		// click(searchbutton,"searchbutton");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.serviceradiobutton, "service radio button");
		click(Lanlink_Metro_Obj.Lanlink_Metro.serviceradiobutton, "service radio button");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.searchorder_actiondropdown, "search order actiodropdown");
		click(Lanlink_Metro_Obj.Lanlink_Metro.searchorder_actiondropdown, "search order linksearch order actiodropdown");

		verifyExists(Lanlink_Metro_Obj.Lanlink_Metro.view, "view");
		click(Lanlink_Metro_Obj.Lanlink_Metro.view, "view");

	}
	
	public void verifyEnteredvalues_deviceName(String label, String expectedValue, String devicename) throws InterruptedException {

		try {
			
			boolean deviceName = isElementPresent("//div[div[label[text()='Name']]]//div[contains(text(),'"+ expectedValue +"')]");
			
			if(deviceName) {
				Reporter.log("device name is displaying as expected");
				
				Report.LogInfo("Info", "Device name is displaying as:  "+ devicename + "as expected","PASS");
			}else {
				
				WebElement Actualvalue=findWebElement("//div[div[label[text()='Name']]]//div[2]");
				Reporter.log("Device name is not displaying as expected");
				Report.LogInfo("Info", "Device name is displaying as:  "+ Actualvalue,"FAIL");
			}
		} catch(AssertionError err) {
			err.printStackTrace();
			Report.LogInfo("Info", label + " value is not displaying as expected ","FAIL");
		} catch (NoSuchElementException e) {
			Reporter.log("value not displayed for " + label);
			
			Report.LogInfo("Info", "value not displayed for : " + label,"FAIL");
			
		}
	}
	
	
	public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(String testDataFile,
			String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {

		String Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
		String existingcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
		String technologySelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String newcityselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
		String technologySelectedfordevicecreation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
		String vpnTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
		String Mepid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
		String poweralarm_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_overture");
		String powerAlarm_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_Accedian");
		String Macaddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
		String serialNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
		String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
		String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String cpename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
		String vender_Overture = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Overture");
		String vender_Accedian = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Accedian");
		String MediaselectionActualValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
		String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"device_intequip_manageaddress_dropdownvalue");
		String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
		String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"device_intequip_existingmanagementAddress_selection");
		String cityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
		String citycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
		String existingsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
		String newsiteselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
		String sitename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
		String sitecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
		String City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
		String existingpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
		String newpremiseselectionmode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
		String technology_siteorder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");

	
			
			if(technologySelectedfordevicecreation.equalsIgnoreCase("Overture")) {
				deviceCreatoin_Overture(cpename, vender_Overture, snmpro, managementAddress, Mepid, poweralarm_Overture, MediaselectionActualValue,
						Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country, City, Site, Premise, newmanagementAddress, 
						existingmanagementAddress, manageaddressdropdownvalue, existingcityselectionmode, newcityselectionmode, cityname, 
						citycode, existingsiteselectionmode, newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, 
						newpremiseselectionmode, premisename, premisecode, technology_siteorder);
				
			}
			
			if((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian")) || (technologySelectedfordevicecreation.equalsIgnoreCase("Accedian-1G"))) {
			deviceCreatoin_Accedian(cpename, vender_Accedian, snmpro, managementAddress, Mepid, powerAlarm_Accedian, MediaselectionActualValue,
					Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country, City, Site, Premise, newmanagementAddress, 
					existingmanagementAddress, manageaddressdropdownvalue, existingcityselectionmode, newcityselectionmode, cityname, 
					citycode, existingsiteselectionmode, newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, 
					newpremiseselectionmode, premisename, premisecode);
		}
			
	}
	
	
	
	
	
	
}
