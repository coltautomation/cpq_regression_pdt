package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.By;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPTransit;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_wholeSale;

public class TS_48_APT_wholeSale_Test extends SeleniumUtils {

	APT_Login Login = new APT_Login();
	Properties prop = new Properties();
	APT_wholeSale wholeSale = new APT_wholeSale();
	public String serviceIdentification=null;

	public String TrunkName=null;
	public String CustomerName=null; 
	
	

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-01", priority = 1)
	public void wholeSale(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomerSelection");

			
			
			if(newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
				
				//CreateNewCustomer_wholesaleSIPTrunking");
				wholeSale.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
								
				String CustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
				wholeSale.selectCustomertocreateOrder(CustomerName);
				
			}
			else if(newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
				String CustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");
				
				wholeSale.selectCustomertocreateOrder(CustomerName);
								
			}
			 
		
			//verifyCreateOrderFunctionality_wholesaleSIPtrunking
  		    wholeSale.createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			wholeSale.serviceSelection(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.serviceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.verifysuccessmessage("Service successfully created.");
				
		
			wholeSale.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			 
			wholeSale.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			wholeSale.verifyEnteredValuesForServiceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);		
	
			wholeSale.navigateToEditPage();
			wholeSale.editService(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.verifysuccessmessage("Service successfully updated");
			wholeSale.verifyEnteredValuesForServiceUpdation(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String edit_serviceId = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "edit_serviceId");
			String ServiceIdentification = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

			if((edit_serviceId).equalsIgnoreCase("null")) {
				serviceIdentification = ServiceIdentification;
			}else {
				serviceIdentification = edit_serviceId;
			}
			
			String orderNumber = wholeSale.fetchOrderNumber();
			wholeSale.verifyManageService(testDataFile, dataSheet, scriptNo, dataSetNo,serviceIdentification , orderNumber);
			wholeSale.clickOnBreadCrump(serviceIdentification);
			
			wholeSale.manageSubnet_viewServicepage();
			wholeSale.dump_viewServicepage();
			
		 // MAS switch	
			wholeSale.verifyAddMASswitch(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.verifysuccessmessage("MAS switch added successfully");
			wholeSale.MASswitch_clickOnViewPage(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.verifyAddedMASswitchInformation_View();
			          
			wholeSale.editMASdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.verifysuccessmessage("MAS switch updated successfully");
			wholeSale.verifyAddInterfaceFunction_MAS(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.verifysuccessmessage("Interface added successfully");
			
			String MAS_Interface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Interface");

			wholeSale.verifyinterfaceTableColumnNames(MAS_Interface);
			           
		
			wholeSale.routerPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
	
			wholeSale.clickOnBreadCrump( serviceIdentification);
			
			String deviceName_masswitch=null;
			String MAS_editName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editName");
			String MAS_deviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_deviceName");

			if((MAS_editName).equalsIgnoreCase("null")) {
				deviceName_masswitch=MAS_deviceName;
			}else {
				deviceName_masswitch=MAS_editName;
			}
			
			     //String deviceSerialNumberunderMASPanel=wholeSale.MASswitch_getDeviceSerialNumber(deviceName_masswitch);
			
			wholeSale.selectInterfaceAndClickonConfigureLInk_MASswitch(deviceName_masswitch,testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.viewInterface_MASswitch(deviceName_masswitch , testDataFile, dataSheet, scriptNo, dataSetNo);
	
			wholeSale.clickOnEditInterfaceLink();
			wholeSale.editInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
	
			String interfaceName=null;
			String MAS_editInterface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_editInterface");
			//String MAS_Interface = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_Interface");

			if((MAS_editInterface).equalsIgnoreCase("null")) {
				interfaceName=MAS_Interface;
			}else {
				interfaceName=MAS_editInterface;
			}
	
			
			wholeSale.clickOnBreadCrump(serviceIdentification);
			wholeSale.MSAdevice_clickOnselectInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String MAS_RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_RemoveInterface_Selection");

			if((MAS_RemoveInterface_Selection).equalsIgnoreCase("yes")) {
				wholeSale.SelectInterfacetoremovefromservice(interfaceName);
			}else {
				Report.LogInfo("INFO","interfaces are not removed","PASS");
			}
			
			String MAS_AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_AddInterface_Selection");

			if((MAS_AddInterface_Selection).equalsIgnoreCase("yes")) {
				    
			}
			
			wholeSale.clickOnBreadCrump( serviceIdentification);
			          
	
			String deviceName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_deviceName");

			wholeSale.MASswitch__DeleteFromServiceFunctionality(deviceName1);
			
			String deviceName2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MAS_devicename2");

			wholeSale.MASswitch__DeleteFromServiceFunctionality(deviceName2);
			
			
			 ///Provider Equipment (PE)
			
			wholeSale.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);

			 
			wholeSale.verifyPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.verifysuccessmessage("PE Device added successfully");
									
				wholeSale.PEdevice_clickOnViewPage(testDataFile, dataSheet, scriptNo, dataSetNo);
				wholeSale.verifyAddedMASswitchInformation_View();
				wholeSale.editPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
				wholeSale.verifyAddInterfaceFunction_PE(testDataFile, dataSheet, scriptNo, dataSetNo);
				wholeSale.verifysuccessmessage("Interface added successfully");
				
				String PE_InterfaceDefaultValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_InterfaceDefaultValue");
				String PE_VLANID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_VLANID");
				String interfaceDefaultValue=PE_InterfaceDefaultValue;
				String vlanIdValue=PE_VLANID;
				String interfacename1=interfaceDefaultValue+vlanIdValue;
				wholeSale.verifyinterfaceTableColumnNames( interfacename1);
				
				wholeSale.routerPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
			
				wholeSale.clickOnBreadCrump(serviceIdentification);
				
					String deviceName_PEdevice=null;
					String PE_editName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_editName");
					String PE_deviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_deviceName");

					
					if((PE_editName).equalsIgnoreCase("null")) {
						deviceName_PEdevice=PE_deviceName;
					}else {
						deviceName_PEdevice=PE_editName;
					}
				
				String interfaceDefaultValue1=PE_InterfaceDefaultValue;
				String vlanIdValue1=PE_VLANID;
				String interfacename2=interfaceDefaultValue1+vlanIdValue1;
				    
				String PE_RemoveInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_RemoveInterface_Selection");

				String PE_AddInterface_Selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PE_AddInterface_Selection");

				String existingdevicename1= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_deviceName");
				wholeSale.PEdevice__DeleteFromServiceFunctionality(existingdevicename1);
				
				String existingdevicename2= DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PE_deviceName2");

				wholeSale.PEdevice__DeleteFromServiceFunctionality(existingdevicename2);
			
		
			
	
	 // Trunk		
			
			
			String siteOrderNumber=null;
			wholeSale.clickOnBreadCrump(serviceIdentification);
			wholeSale.addTrunkSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			wholeSale.editSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String edit_TrunkGroupOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "edit_TrunkGroupOrderNumber");
			String TrunkGroupOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TrunkGroupOrderNumber");

			if((edit_TrunkGroupOrderNumber).equalsIgnoreCase("null")) {
				
				siteOrderNumber= TrunkGroupOrderNumber;
						
			}else{
				siteOrderNumber=edit_TrunkGroupOrderNumber;
			}
			
			wholeSale.verifyAddedSiteOrderAndTrunkLinkUnderTrunkPanel(siteOrderNumber);
			wholeSale.addTrunk(CustomerName, serviceIdentification ,testDataFile, dataSheet, scriptNo, dataSetNo);
     		wholeSale.verifysuccessmessage("Trunk created successfully");
			
			wholeSale.viewTrunk_Primary(CustomerName, serviceIdentification ,testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String trunkGroupName = wholeSale.fetchTrunkName();
					
					
			wholeSale.history( "insert_trunk",  testDataFile, dataSheet, scriptNo, dataSetNo);
			

			            String trunkName=wholeSale.fetchTrunkName();
			            TrunkName=trunkName;
			
			
				    String Gateway = wholeSale.fetchgatewayValue();
			
				     wholeSale.viewTrunk_PSX_executeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo,trunkName);
				
				String gatewayUpdated = wholeSale.fetchgatewayValue();
				
			
				if(gatewayUpdated.contains("SBC")) {
					wholeSale.viewTrunk_SBC_executeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				else
				{
					wholeSale.viewTrunk_GSX_executeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				
			 try {
				if(gatewayUpdated.contains("SBC")) {
					wholeSale.addSBC_manualExecutionConfig(testDataFile, dataSheet, scriptNo, dataSetNo);
					  
				}else {
					ExtentTestManager.getTest().log(LogStatus.INFO, "'SBC Manual Execution Configuration' panel will not display, if 'SBC' gateway not selected ");
				Report.LogInfo("INFO", "'SBC Manual Execution Configuration' panel will not display, if 'SBC' gateway not selected ", "PASS");
				}
				}catch(Exception e) {
					 ExtentTestManager.getTest().log(LogStatus.PASS,"Failure at 'SBC manual COnfiguration'");
					 Report.LogInfo("INFO","Failure at 'SBC manual COnfiguration'","PASS");
				}
			
			 try {
				wholeSale.addPSX_manualExecutionConfig(testDataFile, dataSheet, scriptNo, dataSetNo);
			 }catch(Exception e) {
				 ExtentTestManager.getTest().log(LogStatus.PASS,"Failure at 'PSX manual COnfiguration'"); 
				 Report.LogInfo("INFO","Failure at 'PSX manual COnfiguration'","PASS"); 

			 }
				
				
			 try {
				if(gatewayUpdated.contains("SBC")) {
					ExtentTestManager.getTest().log(LogStatus.INFO, "'GSX Manual Execution Configuration' panel will not display, if 'SBC' gateway is selected ");
					 Report.LogInfo("INFO", "'GSX Manual Execution Configuration' panel will not display, if 'SBC' gateway is selected ","PASS");

				}
				else {
					wholeSale.addGSX_manualExecutionConfig(testDataFile, dataSheet, scriptNo, dataSetNo);
					wholeSale.verifyGSXfileAdded();
					wholeSale.editGSX_manualExecutionConfig(testDataFile, dataSheet, scriptNo, dataSetNo);
					wholeSale.deleteGSX_manualExecutionConfig();
				}
			 }catch(Exception e) {
				 ExtentTestManager.getTest().log(LogStatus.PASS,"Failure at 'GSX manual COnfiguration'"); 
				 Report.LogInfo("INFO","Failure at 'GSX manual COnfiguration'","PASS"); 

			 }
				
				
			
			 // CPE device	
			 
			String resilTrunkName=wholeSale.fetchTrunkName();
				wholeSale.clickOnBreadCrump(serviceIdentification);
  			wholeSale.clickOnCPEdeviceLink(trunkName, siteOrderNumber);
     			wholeSale.addCPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
 				wholeSale.verifysuccessmessage("CPE Device added successfully");
			
				wholeSale.CPEdevice_clickOnViewLink(testDataFile, dataSheet, scriptNo, dataSetNo);
 				wholeSale.viewCPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
				
				wholeSale.editCPEdeviceLink();
				wholeSale.editCPEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
				wholeSale.verifysuccessmessage( "CPE Device updated successfully");
			
				wholeSale.routerPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
				

				String routerId=null;
				String editCPEdevice_routerID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCPEdevice_routerID");
				String CPEdevice_routerID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPEdevice_routerID");

				if((editCPEdevice_routerID).equalsIgnoreCase("null")) {
					routerId=CPEdevice_routerID;
				}
				else {
					routerId=editCPEdevice_routerID;
				}
				
				wholeSale.clickOnBreadCrump(serviceIdentification);
				wholeSale.CPEdevice_clickOnDeleteLink(routerId );
				wholeSale.verifysuccessmessage("CPE Device deleted successfully");
			
				wholeSale.clickOnBreadCrump( serviceIdentification);
				String viewtrunk_PSXconfiguration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "viewtrunk_PSXconfiguration");

				if((viewtrunk_PSXconfiguration).equals("Add Destination IP Address")) {
					wholeSale.deleteTrunk( resilTrunkName, siteOrderNumber);
				}
				
				 
				
				wholeSale.deleteService();
			
				 
		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}

	}



