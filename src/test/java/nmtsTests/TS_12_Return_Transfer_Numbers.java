package nmtsTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.nmtsFunctions.NMTSAddNumberPage;
import testHarness.nmtsFunctions.NMTSLoginPage;
import testHarness.nmtsFunctions.NMTSMergeNumberPage;
import testHarness.nmtsFunctions.NMTSNumberActivationFreeNumberPage;
import testHarness.nmtsFunctions.NMTSNumberReservationPage;
import testHarness.nmtsFunctions.NMTSPort_IN_Number;
import testHarness.nmtsFunctions.NMTSReturn_Transfer_Numbers;


public class TS_12_Return_Transfer_Numbers extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	NMTSLoginPage Login = new NMTSLoginPage();
	NMTSAddNumberPage AddNumber = new NMTSAddNumberPage();
	GlobalVariables g = new GlobalVariables();
	

	NMTSPort_IN_Number PortIn = new NMTSPort_IN_Number();
	NMTSReturn_Transfer_Numbers TransferNumber= new NMTSReturn_Transfer_Numbers();

	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testReturnTransferNumber(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{

		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		
		try
		{
			openNMTSUrl(Configuration.NMTS_URL);
			TransferNumber.ReturnTransferNumberNMTS(testDataFile, dataSheet, scriptNo, dataSetNo);
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		} 
//		Terminating the Execution once it gets ended
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        WEB_DRIVER_THREAD_LOCAL.get().close();
		
	}	

}
