package siebelTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelCEOS;
import testHarness.siebelFunctions.SiebelCreateCustomerOrder;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelFunctions.SiebelManualValidation;
import testHarness.siebelFunctions.SiebelMode;
import testHarness.siebelFunctions.SiebelNumberManagement;

public class TS_03_New_off_net extends SeleniumUtils
{
	/*ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	String dataFileName = "Siebel_testData.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "NewOrderOffnet";
	String scriptNo = "3";
	String dataSetNo = "13";
	String Amount = "2";
	
	*/
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCreateCustomerOrder AddProduct = new SiebelCreateCustomerOrder();
	SiebelAccounts Accounts = new SiebelAccounts();
	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelMode siebelmod = new SiebelMode();
	SiebelNumberManagement NumMana = new SiebelNumberManagement();
	SiebelManualValidation Validation = new SiebelManualValidation();
	SiebelCEOS CEOS = new SiebelCEOS();
	Properties prop = new Properties();
	
	
	
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testNewOrderONNET(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
				
		try
		{
			
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.createCustomerOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Product.productSelection(testDataFile, dataSheet, scriptNo, dataSetNo);
			Product.openServiceOrderNumber(testDataFile, dataSheet, scriptNo, dataSetNo);
			siebelmod.enterMandatoryFieldsInHeader(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String username = Configuration.CEOS_Username;
			String password = Configuration.CEOS_Password;
			siebelmod.addProductConfigurationDetailsAndCompleteOrderStatusOffNet(testDataFile, dataSheet, scriptNo, dataSetNo,username,password);
		}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	    }
	        Report.createTestCaseReportFooter();
	        Report.SummaryReportlog(ScenarioName);
			
	}
	
	
	
	
	
	/*
	@Test(priority = 1, groups={"testSiebelLogin"})
	public void testSiebelLogin()
	{
		Report.createTestCaseReportHeader();	
				
		try
		{
			Login.SiebelLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSiebelLogin "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSiebelLogin "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Siebel Login page functionality");
		
	}	
	
	@Test(priority = 2, groups={"testNavigateToAccounts"}, dependsOnGroups = {"testSiebelLogin"})
	public void testNavigateToAccounts()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Accounts.NavigateToAccounts();

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testNavigateToAccounts "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testNavigateToAccounts "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify navigation to accounts Page");
		
	}
	
	@Test(priority = 3, groups={"testSearchAccount"}, dependsOnGroups = { "testNavigateToAccounts" })
	public void testSearchAccount()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Accounts.SearchAccount(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSearchAccount "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSearchAccount "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Search and Open existing account");
	}	
	
	@Test(priority = 4, groups={"testSiebelCreateCustomerOrder"}, dependsOnGroups = { "testSearchAccount" })
	public void testSiebelCreateCustomerOrder()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			AddProduct.createCustomerOrder(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSiebelCreateCustomerOrder "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSiebelCreateCustomerOrder "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Siebel Create Customer Order");
		
	}	
	@Test(priority = 5, groups={"testproductSelection"}, dependsOnGroups = { "testSiebelCreateCustomerOrder" })
	public void testproductSelection()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.productSelection(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testproductSelection "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testproductSelection "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to add product");
	}	
	
	
	@Test(priority = 6, groups={"testopenServiceOrderNumber"}, dependsOnGroups = { "testproductSelection" } )
	public void testopenServiceOrderNumber()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			
			Product.openServiceOrderNumber(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testopenServiceOrderNumber "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testopenServiceOrderNumber "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to open serive Order ref number");
	}	
	
	
	@Test(priority = 7, groups={"testenterMandatoryFieldsInHeader"}, dependsOnGroups = { "testopenServiceOrderNumber" })
	public void testenterMandatoryFieldsInHeader()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
		//	Thread.sleep(20000);
			siebelmod.enterMandatoryFieldsInHeader(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Mandatory Fields In Header "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in enter Mandatory Fields InHeader "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Mandatory Details In Middle Applet.");
	}	
	
	@Test(priority = 8, groups={"testaddProductConfigurationDetailsAndCompleteOrderStatusOffNet"}, dependsOnGroups = { "testenterMandatoryFieldsInHeader" })
	public void testaddProductConfigurationDetailsAndCompleteOrderStatusOffNet()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
		//	Thread.sleep(15000);
			String username = Configuration.CEOS_Username;
			String password = Configuration.CEOS_Password;
			siebelmod.addProductConfigurationDetailsAndCompleteOrderStatusOffNet(testDataFile, tsSheetName, scriptNo, dataSetNo,username,password);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testaddProductConfigurationDetailsAndCompleteOrderStatusOffNet "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testaddProductConfigurationDetailsAndCompleteOrderStatusOffNet "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to add Product Configuration Details And Complete Order Status");
	}


	

	
	
	
	/*
	
	
	
	
	@Test(priority = 8, groups={"testenterMandatoryDetailsInMiddleApplet"}, dependsOnGroups = { "testenterMandatoryFieldsInHeader" })
	public void testenterMandatoryDetailsInMiddleApplet()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			siebelmod.enterMandatoryDetailsInMiddleApplet(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Mandatory Fields In Header "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in enter Mandatory Details In MiddleApplet"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Mandatory Details In Header.");
	}
	
	@Test(priority = 9, groups={"testVoiceConfigTab"}, dependsOnGroups = { "testenterMandatoryDetailsInMiddleApplet" })
	public void testVoiceConfigTab()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			siebelmod.VoiceConfigTab(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Voice Config Tab "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in Voice Config Tab"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Voice Config Tab");
	}	
	
	
	
	@Test(priority = 10, groups={"testVoiceFeatureTab"}, dependsOnGroups = { "testVoiceConfigTab" })
	public void testVoiceFeatureTab()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			siebelmod.VoiceFeatureTab(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Voice Feature Tab "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in Voice Feature Tab"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Voice Feature Tab");
	}
	
	

	@Test(priority = 11, groups={"testNumberManagementTab"}, dependsOnGroups = { "testVoiceFeatureTab" })
	public void testNumberManagementTab()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			NumMana.AddDataUnderNumberManagement(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Number Management Tab "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in Number Management Tab"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Number Management Tab");
	}
	
	
	@Test(priority = 12, groups={"testEnterDateInFooter"}, dependsOnGroups = { "testNumberManagementTab" })
	public void testEnterDateInFooter()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			siebelmod.EnterDateInFooter(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Enter Date In Footer "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in Enter Date In Footer"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Enter Date In Footer");
	}
	
	@Test(priority = 13, groups={"testEnterBillingDateInFooter"}, dependsOnGroups = { "testEnterDateInFooter" })
	public void testEnterBillingDateInFooter()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			siebelmod.EnterBillingDateInFooter(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in Enter Date In Footer "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in Enter Date In Footer"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Enter Date In Footer");
	}
	
	@Test(priority = 14, groups={"testEnterServiceChargeInFooter"}, dependsOnGroups = { "testEnterBillingDateInFooter" })
	public void testEnterServiceChargeInFooter()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			siebelmod.EnterServiceChargeInFooter(testDataFile, tsSheetName, scriptNo, dataSetNo, Amount);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in EnterService Charge In Footer "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in Enter Service Charge In Footer"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Enter Service Charge In Footer");
	}
	
	
	@Test(priority = 15, groups={"testSelectAttachmentTab"}, dependsOnGroups = { "testEnterServiceChargeInFooter" } )
	public void testSelectAttachmentTab()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.SelectAttachmentTab(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSelectAttachmentTab "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSelectAttachmentTab "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to select attachment Tab");
	}	
	
	@Test(priority = 16, groups={"testUploadDocument"}, dependsOnGroups = { "testSelectAttachmentTab" } )
	public void testUploadDocument()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.UploadDocument(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testUploadDocument "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testUploadDocument "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to upload document");
	}	
	
	@Test(priority = 17, groups={"testSelectServiceGroupTab"}, dependsOnGroups = { "testUploadDocument" } )
	public void testSelectServiceGroupTab()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.SelectServiceGroupTab(testDataFile, tsSheetName, scriptNo, dataSetNo);
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSelectServiceGroupTab "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSelectServiceGroupTab "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to select ServiceGroupTab");
	}	
	
	

	
	
	@Test(priority = 18, groups={"testOperationAttribute"}, dependsOnGroups = { "testSelectServiceGroupTab" } )
	public void testOperationAttribute()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			
			Product.OperationAttribute(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testOperationAttribute "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testOperationAttribute "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that Operation Attribute");
	}
	
	
	
	@Test(priority = 19, groups={"testEnterInstallationChargeInFooter"}, dependsOnGroups = { "testOperationAttribute" } )
	public void testEnterInstallationChargeInFooter()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.EnterInstallationChargeInFooter(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testEnterInstallationChargeInFooter "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testEnterInstallationChargeInFooter "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able  add data in InstallationChargeInFooter");
	}	
	
	
	

	@Test(priority = 20, groups={"testMandatoryFields"}, dependsOnGroups = { "testEnterInstallationChargeInFooter" } )
	public void testMandatoryFields()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			NumMana.MandatoryFields(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testMandatoryFields "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testMandatoryFields "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able  add data in MandatoryFields");
	}	
	
	
	@Test(priority = 21, groups={"testCommercialValidation"}, dependsOnGroups = { "testMandatoryFields" } )
	public void testCommercialValidation()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.CommercialValidation(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testCommercialValidation "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testCommercialValidation "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to select Commercial Validation");
	}	
	
	
	
	@Test(priority = 22, groups={"testTechnicalValidation"}, dependsOnGroups = { "testCommercialValidation" } )
	public void testTechnicalValidation()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.TechnicalValidation(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testTechnicalValidation "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testTechnicalValidation "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to select Techanical Validation");
	}	
	

	
	@Test(priority = 23, groups={"testclickOnManualValidationB"}, dependsOnGroups = { "testTechnicalValidation" } )
	public void testclickOnManualValidationB()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Validation.clickOnManualValidationB();
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testclickOnManualValidationB "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testclickOnManualValidationB "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to select Manual Validation B");
	}	
	
	
	@Test(priority = 24, groups={"testDeliveryValidation"}, dependsOnGroups = { "testclickOnManualValidationB" } )
	public void testDeliveryValidation()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.DeliveryValidation(testDataFile, tsSheetName, scriptNo, dataSetNo);
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testDeliveryValidation "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testDeliveryValidation "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to select Delivery Validation ");
	}
	
	
	@Test(priority = 25, groups={"testclickOnManualValidationA"}, dependsOnGroups = { "testDeliveryValidation" } )
	public void testclickOnManualValidationA()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Validation.clickOnManualValidationA();
			
			
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testclickOnManualValidationA "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testclickOnManualValidationA "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to select manual Validation A");
	}
	
	@Test(priority = 26, groups={"testOffnetAndRequiredData"}, dependsOnGroups = { "testclickOnManualValidationA" } )
	public void testOffnetAndRequiredData()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
		String Product_Type = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Product_Type");
		String Offnet = DataMiner.fngetcolvalue(testDataFile, tsSheetName, scriptNo, dataSetNo,"Offnet");
		String username = Configuration.CEOS_Username;
		String password = Configuration.CEOS_Password;
		
			if (!Product_Type.equals("IP VPN Service")) {
				if (Offnet.toString().equals("Offnet")) 
				{
				CEOS.CEOS_Offnet(testDataFile, tsSheetName, scriptNo, dataSetNo);
				Product.LaunchingCEOSApplication(testDataFile, tsSheetName, scriptNo, dataSetNo, username, password);
				Product.getReferenceNo(testDataFile, tsSheetName, scriptNo, dataSetNo);
				Product.CompletedValidation(testDataFile, tsSheetName, scriptNo, dataSetNo);	
			}

			else
			{
				Product.CompletedValidation_offnet(testDataFile, tsSheetName, scriptNo, dataSetNo);

			}
		}
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testOffnetAndRequiredData "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testOffnetAndRequiredData "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to add CEOS, Launching CEOS Application, Get reference number, Completed Validation and CompletedValidation offnet");
	}
	
	@Test(priority = 27, groups={"testgetReferenceNo"}, dependsOnGroups = { "testOffnetAndRequiredData" } )
	public void testgetReferenceNo()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.getReferenceNo(testDataFile, tsSheetName, scriptNo, dataSetNo);
				
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testgetReferenceNo "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testgetReferenceNo "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("get Reference No");
	}
	
	@Test(priority = 28, groups={"testCompletedValidation"}, dependsOnGroups = { "testgetReferenceNo" } )
	public void testCompletedValidation()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			Product.CompletedValidation(testDataFile, tsSheetName, scriptNo, dataSetNo);
				
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testCompletedValidation "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testCompletedValidation"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Completed Validation");
	}

	@Test(priority = 29, groups={"testValidateSlaMatrix"}, dependsOnGroups = { "testCompletedValidation" } )
	public void testValidateSlaMatrix()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
			
			Product.validateSlaMatrix(testDataFile, tsSheetName, scriptNo, dataSetNo);
				
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testValidateSlaMatrix"+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testValidateSlaMatrix"+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that SLA matrix is displayed");
	}
*/
	
}
