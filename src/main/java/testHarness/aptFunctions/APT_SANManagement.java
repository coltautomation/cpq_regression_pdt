package testHarness.aptFunctions;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.openqa.selenium.WebElement;
import org.testng.Reporter;


import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_SANManagement_Obj;

public class APT_SANManagement extends SeleniumUtils
{
	Lanlink_NewTab NewTab = new Lanlink_NewTab();
	public void verifySearchSAN(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String searchSANfilename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SearchSANfilename");
		String browserfiles_downloadspath = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Browserfiles_Downloadspath");
		String search_sannumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Search_SANNumber");
		String SANSearch = "SAN Search";
		String SearchForOrderService = "Search For Order / Service";
		String ViewSanHeader = "View San";
		
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.managecoltnetworklink, "Manage colt network link");
		mouseMoveOn(APT_SANManagement_Obj.APT_SANManagement.managecoltnetworklink);
		
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchsanlink, "Search for SANs");
		click(APT_SANManagement_Obj.APT_SANManagement.searchsanlink, "Search for SANs");
		getTextFrom(APT_SANManagement_Obj.APT_SANManagement.sansearchheader,"san search header").equals(SANSearch);
		
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.santextfield, "SAN text field");
		sendKeys(APT_SANManagement_Obj.APT_SANManagement.santextfield, search_sannumber, "SAN");
		
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		click(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		//waitToPageLoad();
		waitforPagetobeenable();
		waitForAjax();
		if(isVisible(APT_SANManagement_Obj.APT_SANManagement.downloadtoexcellink)){
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.downloadtoexcellink, "Download To Excel");
		click(APT_SANManagement_Obj.APT_SANManagement.downloadtoexcellink, "Download To Excel");
		isFileDownloaded(searchSANfilename, browserfiles_downloadspath);
		waitToPageLoad();
		//Rework done
		
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.santextfield, "SAN text field");
		sendKeys(APT_SANManagement_Obj.APT_SANManagement.santextfield, search_sannumber, "SAN");
		waitToPageLoad();
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		click(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		waitToPageLoad();

		String SelectSAN= (APT_SANManagement_Obj.APT_SANManagement.SelectSAN1+search_sannumber+APT_SANManagement_Obj.APT_SANManagement.SelectSAN2);
		try {
		if(isElementPresent(SelectSAN))
		{
			String FRCNumber = "FRC Number";
			String ServiceProfile = "Service Profile";
			String CustomerName = "Customer Name";
			String BeginDate = "Begin Date";
			String OrderName = "Order Name";
			//Verify SearchSAN columns headers
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.frcnumber_column,"frcnumber_column").equals(FRCNumber);
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.serviceprofilecolumn,"serviceprofilecolumn").equals(ServiceProfile);
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.customernamecolumn,"customernamecolumn").equals(CustomerName);
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.begindatecolumn,"begindatecolumn").equals(BeginDate);
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.ordernamecolumn,"ordernamecolumn").equals(OrderName);
			
			verifyExists(SelectSAN, "Select SAN");
			click(SelectSAN, "Select SAN");
			
			verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
			click(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
			//View link displayed verify
			if(isElementPresent(APT_SANManagement_Obj.APT_SANManagement.view))
			{
				Reporter.log("View link is displaying in Search SAN page");
			}
			else
			{
				Reporter.log("View link is not displaying in Search SAN page");
			}

			//Delete link displayed verify
			if(isElementPresent(APT_SANManagement_Obj.APT_SANManagement.delete))
			{
				Reporter.log("Delete link is displaying in Search SAN page");
			}
			else
			{
				Reporter.log("Delete link is not displaying in Search SAN page");
			}
		}
		else
		{
			Reporter.log("No existing SAN to display");
		}
		}
		catch (Exception e) {
				e.printStackTrace();
		}
		
			verifyExists(APT_SANManagement_Obj.APT_SANManagement.view, "View");
			click(APT_SANManagement_Obj.APT_SANManagement.view, "View");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.searchsan_viewpageheader,"searchsan_viewpageheader").equals(ViewSanHeader);

			//Verify view San details

			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.viewsan_customername,"Customer Name");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.viewsan_sanmgmt,"SAN/FRC Number");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.serviceprofilevalue,"Service Profile");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.internationaloutgoingcallsvalue,"International outgoing calls");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.searchsan_internationalincomingcallsvalue,"International incoming calls");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.mobilecallsallowedvalue,"Mobile calls allowed");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.view_noreplytimervalue,"No reply timer");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.maxcalldurationvalue,"Max call duration");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.chargebandnamevalue,"Charge band name");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.payphoneblockingenabledvalue,"Payphone blocking enabled");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.webaccessblockedvalue,"Web access blocked");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.searchsan_predestinationnumbervalue,"Predestination number");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.CPSvalue,"CPS Free Format");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.ringtonumbervalue,"Ring To Number");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.searchsan_announcementtoplayvalue,"Announcement to play");
			scrollDown(APT_SANManagement_Obj.APT_SANManagement.viewpage_backbutton);
			waitToPageLoad();
			verifyExists(APT_SANManagement_Obj.APT_SANManagement.viewpage_backbutton, "Back");
			click(APT_SANManagement_Obj.APT_SANManagement.viewpage_backbutton, "Back");
			waitToPageLoad();
		if(isElementPresent(APT_SANManagement_Obj.APT_SANManagement.sansearchheader))
		{
			Reporter.log("Navigated to SAN Search page");
		}
		else
		{
			Reporter.log("Didn't navigate to SAN Search page");
		}

		//Verify Search order/service page
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.santextfield,"SAN text field");
		sendKeys(APT_SANManagement_Obj.APT_SANManagement.santextfield, search_sannumber,"SAN");
		waitToPageLoad();
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		click(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		waitToPageLoad();
		String SelectSAN1= APT_SANManagement_Obj.APT_SANManagement.search_sannumber1+search_sannumber+APT_SANManagement_Obj.APT_SANManagement.search_sannumber2;
		try {
		if(isElementPresent(SelectSAN1))
		{	
			String OrderNumber= getTextFrom(APT_SANManagement_Obj.APT_SANManagement.ordername_link,"ordername_link");
			verifyExists(APT_SANManagement_Obj.APT_SANManagement.ordername_link, "Order Name");
			click(APT_SANManagement_Obj.APT_SANManagement.ordername_link, "Order Name");
			
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.searchfororder_header,"search for order_header").equals(SearchForOrderService);
			waitToPageLoad();
			String SelectExistingOrder= APT_SANManagement_Obj.APT_SANManagement.OrderNumber1+OrderNumber+APT_SANManagement_Obj.APT_SANManagement.OrderNumber2;

			if(isElementPresent(SelectExistingOrder))
			{
				verifyExists(SelectExistingOrder,"Select existing order");
				click(SelectExistingOrder,"Select existing order");
				verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
				click(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
				waitToPageLoad();
				verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchfororder_viewlink, "View");
				click(APT_SANManagement_Obj.APT_SANManagement.searchfororder_viewlink, "View");
				waitToPageLoad();
				if(isElementPresent(APT_SANManagement_Obj.APT_SANManagement.customerdetailsheader))
				{
					Reporter.log("Navigated to view service page");
				}
				else
				{
					Reporter.log("Didn't navigate to view service page");
				}

			}
			else
			{
				Reporter.log("Existing order is not available");
			}
		}
		else
		{
			Reporter.log(" No existing SAN to display");
		}
		}
		catch (Exception e) {
				e.printStackTrace();
		}
		webDriver.navigate().back();
		waitToPageLoad();

		//Verify manage link in search order page
	
		getTextFrom(APT_SANManagement_Obj.APT_SANManagement.sansearchheader,"san search header").equals(SANSearch);
		if(isElementPresent(APT_SANManagement_Obj.APT_SANManagement.sansearchheader))
		{
			Reporter.log("Navigated to 'SAN Search' page");
			}
		else
		{
			Reporter.log("Didn't navigate to 'SAN Search' page");
		}

		verifyExists(APT_SANManagement_Obj.APT_SANManagement.santextfield, "SAN");
		sendKeys(APT_SANManagement_Obj.APT_SANManagement.santextfield, search_sannumber, "SAN");
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		click(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton, "Search");
		waitToPageLoad();
		String SelectSAN2= APT_SANManagement_Obj.APT_SANManagement.search_sannumber1+search_sannumber+APT_SANManagement_Obj.APT_SANManagement.search_sannumber2;
		try {
		if(isElementPresent(SelectSAN2))
		{
			
			String OrderNumber= getTextFrom(APT_SANManagement_Obj.APT_SANManagement.ordername_link,"order name_link");
			verifyExists(APT_SANManagement_Obj.APT_SANManagement.ordername_link, "Order Name");
			click(APT_SANManagement_Obj.APT_SANManagement.ordername_link, "Order Name");
			getTextFrom(APT_SANManagement_Obj.APT_SANManagement.searchfororder_header,"search for order_header").equals(SearchForOrderService);
			String SelectExistingOrder= APT_SANManagement_Obj.APT_SANManagement.OrderNumber1+OrderNumber+APT_SANManagement_Obj.APT_SANManagement.OrderNumber2;
			if(isElementPresent(SelectExistingOrder))
			{
				verifyExists(SelectExistingOrder, "Select existing order");
				click(SelectExistingOrder, "Select existing order");
				verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
				click(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
				verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchfororder_managelink, "Manage");
				click(APT_SANManagement_Obj.APT_SANManagement.searchfororder_managelink, "Manage");
				waitToPageLoad();
				
				if(isElementPresent(APT_SANManagement_Obj.APT_SANManagement.manageservice_header))
				{
					Reporter.log("Navigated to Manage service page");
				}
				else
				{
					Reporter.log("Didn't navigated to Manage service page");
				}

			}
			else
			{
				Reporter.log("Existing order is not available");
			}
		}
		else
		{
			Reporter.log("No existing SAN to display");
		}
		}
		catch (Exception e) {
				e.printStackTrace();
		}
		
		webDriver.navigate().back();
		waitToPageLoad();

		//Verify Delete SAN details
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.santextfield, "SAN");
		sendKeys(APT_SANManagement_Obj.APT_SANManagement.santextfield, search_sannumber, "SAN");
		waitForAjax();
		verifyExists(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton,"Search");
		click(APT_SANManagement_Obj.APT_SANManagement.san_searchbutton,"Search");
		waitToPageLoad();
		String SelectSAN3= (APT_SANManagement_Obj.APT_SANManagement.search_sannumber1+search_sannumber+APT_SANManagement_Obj.APT_SANManagement.search_sannumber2);
		
		if(isElementPresent(SelectSAN3))
		{
			verifyExists(SelectSAN3,"SAN 3");
			click(SelectSAN3,"SAN 3");
			waitToPageLoad();
			verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
			click(APT_SANManagement_Obj.APT_SANManagement.searchsan_actiondropdown, "Action dropdown");
			waitForAjax();
			verifyExists(APT_SANManagement_Obj.APT_SANManagement.delete, "Delete SAN");
			click(APT_SANManagement_Obj.APT_SANManagement.delete, "Delete SAN");
			waitToPageLoad();
			String DeleteAlertPopup = APT_SANManagement_Obj.APT_SANManagement.delete_alertpopup;
			if(isElementPresent(DeleteAlertPopup))
			{
				verifyExists(APT_SANManagement_Obj.APT_SANManagement.searchsan_deletebutton, "Delete");
				click(APT_SANManagement_Obj.APT_SANManagement.searchsan_deletebutton, "Delete");
				waitToPageLoad();
				NewTab.verifysuccessmessage("SAN successfully deleted.");
			}
			else
			{
				Reporter.log("Delete alert popup is not displayed");
			}			
		}
		else
		{
			Reporter.log("No existing SAN to display");
		}
		}else{
			Report.LogInfo("INFO", "No Data Found with Respective SAN number", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "No Data Found with Respective SAN number");
		}
		
	}
	
	public static Boolean isFileDownloaded(String fileName, String downloadspath) {
		boolean flag = false;
		//paste your directory path below
		//eg: C:\\Users\\username\\Downloads
				
		String dirPath = downloadspath; 
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		try {
		if (files.length == 0 || files == null) {
			Reporter.log("The directory is empty");
			Reporter.log("Step : Downloads folder is empty");
			flag = false;
		} else {
			for (File listFile : files) {
				if (listFile.getName().contains(fileName)) {
					Reporter.log(fileName + " is present");
					Reporter.log("Step : '"+fileName+"' excel file is downloaded successfully");
					break;
				}
				flag = true;
			}
		}
		}
		catch (Exception e) {
			Reporter.log("Step : Downloads folder path '"+dirPath+"' is not found");
		}
		return flag;
	}
}
