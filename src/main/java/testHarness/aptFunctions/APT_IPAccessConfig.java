package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.aptObjects.APT_IPAccessConfigObj;
import pageObjects.aptObjects.APT_IPAccessResilientConfigObj;
import pageObjects.aptObjects.APT_IPAccess_VCPEConfigObj;

public class APT_IPAccessConfig extends SeleniumUtils {

	public static String InterfaceName=null;
	public static String Edit_InterfaceName=null;
	public static String Multilink_InterfaceName=null;
	public static String DeviceName=null;
	public static String VendorModel=null;
	
	APT_IPAccessResilientConfig IPConfig = new APT_IPAccessResilientConfig();
	
	public void verifyUserDetailsInformation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String Login = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "LoginColumn");
		String Name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NameColumn");
		String Email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EmailColumn");
		String Roles = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RolesColumn");
		String Address = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddressColumn");
		String Resource = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResourceColumn");

		// verify table column names

		scrollDown(APT_IPAccessConfigObj.ipa_config.customerdetailsheader);
		compareText("Login column", APT_IPAccessConfigObj.ipa_config.LoginColumn, Login);
		compareText("Name column", APT_IPAccessConfigObj.ipa_config.NameColumn, Name);
		compareText("Email column", APT_IPAccessConfigObj.ipa_config.EmailColumn, Email);
		compareText("Roles column", APT_IPAccessConfigObj.ipa_config.RolesColumn, Roles);
		compareText("Address column", APT_IPAccessConfigObj.ipa_config.AddressColumn, Address);
		compareText("Resource column", APT_IPAccessConfigObj.ipa_config.ResourcesColumn, Resource);

	}

	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
		String networkConfig_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NetworkConfiguration_DrodpwonValue");
		String billingtypevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BillingTypevalue");
		String phonecontact = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PhoneContact");
		String NoOfCircuits_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NoOfCircuits_DropdownValue");
		String OrderType_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"OrderType_DropdownValue");
		String Speedysurf_checkboxvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"SpeedySurf_CheckboxValue");
		String CPEWANTechnology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CPEWANTechnology_DropdownValue");
		String PEWANTechnology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PEWANTechnology_DropdownValue");
		String DSLProvider_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DSLProvider_DropdownValue");
		String PPPBased_checkboxvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PPPBased_CheckboxValue");
		String VOIP_checkboxvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VOIP_CheckboxValue");
		String downstream_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Downstream_DropdownValue");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");

		waitforPagetobeenable();
		// waitforPagetobeenable();
		scrollDown(APT_IPAccessConfigObj.ipa_config.orderpanelheader);
		// Thread.sleep(1000);
		compareText("Service panel Header", APT_IPAccessConfigObj.ipa_config.servicepanel_header, "Service");
		compareText("Service Identification", APT_IPAccessConfigObj.ipa_config.servicepanel_serviceidentificationvalue,
				sid);
		compareText("Service Type", APT_IPAccessConfigObj.ipa_config.servicepanel_servicetypevalue, servicetype);
		compareText("Network Configuration", APT_IPAccessConfigObj.ipa_config.servicepanel_networkconfigvalue,
				networkConfig_dropdownvalue);
		compareText("Billing Type", APT_IPAccessConfigObj.ipa_config.servicepanel_billingtype, billingtypevalue);
		
		compareText("Phone Contact", APT_IPAccessConfigObj.ipa_config.servicepanel_phone, phonecontact);
		scrollDown(APT_IPAccessConfigObj.ipa_config.servicepanel_email);
		
		compareText("No. of circuits", APT_IPAccessConfigObj.ipa_config.servicepanel_Noofcircuits,
				NoOfCircuits_dropdownvalue);
		compareText("Order Type", APT_IPAccessConfigObj.ipa_config.servicepanel_ordertype, OrderType_dropdownvalue);
		compareText("Speedy Surf", APT_IPAccessConfigObj.ipa_config.servicepanel_speedysurf, Speedysurf_checkboxvalue);

		compareText("CPE WAN Technology", APT_IPAccessConfigObj.ipa_config.servicepanel_cpewantechnology,
				CPEWANTechnology_dropdownvalue);

		if (CPEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet")) {

			compareText("PE WAN Technology Type", "servicepanel_pewantechnology", PEWANTechnology_dropdownvalue);
			if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("ATM")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
				compareText("PPP Based", APT_IPAccessConfigObj.ipa_config.servicepanel_pppbased,
						PPPBased_checkboxvalue);
			} else if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet-L2TP")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
				compareText("VOIP", APT_IPAccessConfigObj.ipa_config.servicepanel_voip, VOIP_checkboxvalue);
			}
		} else if (CPEWANTechnology_dropdownvalue.equalsIgnoreCase("ADSL")) {
			compareText("PE WAN Technology Type", APT_IPAccessConfigObj.ipa_config.servicepanel_pewantechnology,
					PEWANTechnology_dropdownvalue);
			if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("ATM")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
				compareText("Downstream", APT_IPAccessConfigObj.ipa_config.servicepanel_downstream,
						downstream_dropdownvalue);
				compareText("PPP Based", APT_IPAccessConfigObj.ipa_config.servicepanel_pppbased,
						PPPBased_checkboxvalue);
				if (PPPBased_checkboxvalue.equalsIgnoreCase("yes")) {
					compareText("VOIP", APT_IPAccessConfigObj.ipa_config.servicepanel_voip, VOIP_checkboxvalue);
				}
			} else if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("L2TP")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
				compareText("Downstream", APT_IPAccessConfigObj.ipa_config.servicepanel_downstream,
						downstream_dropdownvalue);
				compareText("VOIP", APT_IPAccessConfigObj.ipa_config.servicepanel_voip, VOIP_checkboxvalue);
			} else if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
				compareText("Downstream", APT_IPAccessConfigObj.ipa_config.servicepanel_downstream,
						downstream_dropdownvalue);
			}
		} else if (CPEWANTechnology_dropdownvalue.equalsIgnoreCase("SDSL")) {
			compareText("PE WAN Technology Type", APT_IPAccessConfigObj.ipa_config.servicepanel_pewantechnology,
					PEWANTechnology_dropdownvalue);
			if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("ATM")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
				compareText("PPP Based", APT_IPAccessConfigObj.ipa_config.servicepanel_pppbased,
						PPPBased_checkboxvalue);
				if (PPPBased_checkboxvalue.equalsIgnoreCase("yes")) {
					compareText("VOIP", APT_IPAccessConfigObj.ipa_config.servicepanel_voip, VOIP_checkboxvalue);
				}
			} else if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("L2TP")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
				compareText("VOIP", APT_IPAccessConfigObj.ipa_config.servicepanel_voip, VOIP_checkboxvalue);
			} else if (PEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet")) {
				compareText("DSL Provider", APT_IPAccessConfigObj.ipa_config.servicepanel_dslprovider,
						DSLProvider_dropdownvalue);
			}
		}

		compareText("Remarks", APT_IPAccessConfigObj.ipa_config.servicepanel_remarksvalue, Remarks);

		Thread.sleep(1000);
	}

	public void verifyManagementConfigpanels(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String package_dropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Package_DropdownValue");
		String managedservice_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ManagedService_Checkbox");
		String ipguardian_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IPGuardian_Checkbox");
		String routerconfigview_ipv4_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigView_IPv4_Checkbox");
		String routerconfigview_ipv6_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterConfigView_IPv6_Checkbox");
		String BGPCheck_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGPCheck_Checkbox");
		String ManInMiddle_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ManInMiddle_Checkbox");
		String snmpnotification_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"SnmpNotification_Checkbox");
		String PA_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PA_Checkbox");
		String PI_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PI_Checkbox");
		String deliverychannel_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DeliveryChannel_DropdownValue");
		String traptargetaddress_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TrapTargetAddress_Value");
		String routerbasedfirewall_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"RouterbasedFirewall_Checkbox");
		String qos_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "QOS_Checkbox");
		String actelisbased_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ActelisBased_Checkbox");

		scrollDown(APT_IPAccessConfigObj.ipa_config.managementoptions_header);
		Thread.sleep(1000);

		// management options panel
		compareText("Management Options", APT_IPAccessConfigObj.ipa_config.managementoptions_header,
				"Management Options");
		compareText("Performance Reporting", APT_IPAccessConfigObj.ipa_config.managementoptions_performancereporting,
				"yes");
		if (package_dropdownValue.equalsIgnoreCase("Gold")) {
			// verify Managed service
			if (managedservice_checkbox.equalsIgnoreCase("Null")) {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice, "no");
			} else if (managedservice_checkbox.equalsIgnoreCase("yes")) {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice,
						"yes");
			} else {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice, "no");
			}
			// verify IP Guardian checkbox
			if (ipguardian_checkbox.equalsIgnoreCase("Null")) {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "no");
			} else if (ipguardian_checkbox.equalsIgnoreCase("yes")) {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "yes");
			} else {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "no");
			}
			// verify Router Configuration View IPv4 checkbox
			if (routerconfigview_ipv4_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "yes");
			} else if (routerconfigview_ipv4_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "yes");
			} else {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "no");
			}
			// verify Router Configuration View IPv6 checkbox
			if (routerconfigview_ipv6_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "no");
			} else if (routerconfigview_ipv6_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "yes");
			} else {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "no");
			}
			// verify BGP Check checkbox
			if (BGPCheck_checkbox.equalsIgnoreCase("Null")) {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "no");
			} else if (BGPCheck_checkbox.equalsIgnoreCase("yes")) {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "yes");
			} else {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "no");
			}
			// verify Man In Middle checkbox
			if (ManInMiddle_checkbox.equalsIgnoreCase("Null")) {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "no");
			} else if (ManInMiddle_checkbox.equalsIgnoreCase("yes")) {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "yes");
			} else {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "no");
			}
			// verify SNMP Notification checkbox
			if (snmpnotification_checkbox.equalsIgnoreCase("Null")) {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"no");
			} else if (snmpnotification_checkbox.equalsIgnoreCase("yes")) {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"yes");
			} else {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"no");
			}
			if (BGPCheck_checkbox.equalsIgnoreCase("Yes")) {
				// verify PA checkbox
				if (PA_checkbox.equalsIgnoreCase("Null")) {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "no");
				} else if (PA_checkbox.equalsIgnoreCase("yes")) {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "yes");
				} else {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "no");
				}
			}

			if (BGPCheck_checkbox.equalsIgnoreCase("Yes")) {
				// verify PI checkbox
				if (PI_checkbox.equalsIgnoreCase("Null")) {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "no");
				} else if (PI_checkbox.equalsIgnoreCase("yes")) {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "yes");
				} else {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "no");
				}
			}
			// verify Delivery Channel checkbox
			compareText("Delivery Channel", APT_IPAccessConfigObj.ipa_config.managementoptions_deliverychannel,
					deliverychannel_dropdownvalue);
			if (snmpnotification_checkbox.equalsIgnoreCase("Yes")) {
				compareText("Trap Target Address", APT_IPAccessConfigObj.ipa_config.managementoptions_traptargetaddress,
						traptargetaddress_value);
			}

			// Configurations Options panel
			scrollDown(APT_IPAccessConfigObj.ipa_config.configurationoptions_panelheader);
			((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-150)");
			compareText("Configuration Options", APT_IPAccessConfigObj.ipa_config.configurationoptions_panelheader,
					"Configuration Options");
			// verify Router Based Firewall checkbox
			if (routerbasedfirewall_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"yes");
			} else if (routerbasedfirewall_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"yes");
			} else {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"no");
			}
			// verify QoS checkbox
			if (qos_checkbox.equalsIgnoreCase("Null")) {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "no");
			} else if (qos_checkbox.equalsIgnoreCase("yes")) {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "yes");
			} else {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "no");
			}
			// verify Actelis Based checkbox
			if (actelisbased_checkbox.equalsIgnoreCase("Null")) {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "no");
			} else if (actelisbased_checkbox.equalsIgnoreCase("yes")) {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "yes");
			} else {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "no");
			}

		} else if (package_dropdownValue.equalsIgnoreCase("Silver")) {

			// verify Managed service
			if (managedservice_checkbox.equalsIgnoreCase("Null")) {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice,
						"yes");
			} else if (managedservice_checkbox.equalsIgnoreCase("yes")) {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice,
						"yes");
			} else {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice, "no");
			}
			// verify IP Guardian checkbox
			if (ipguardian_checkbox.equalsIgnoreCase("Null")) {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "no");
			} else if (ipguardian_checkbox.equalsIgnoreCase("yes")) {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "yes");
			} else {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "no");
			}
			// verify Router Configuration View IPv4 checkbox
			if (routerconfigview_ipv4_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "no");
			} else if (routerconfigview_ipv4_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "yes");
			} else {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "no");
			}
			// verify Router Configuration View IPv6 checkbox
			if (routerconfigview_ipv6_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "no");
			} else if (routerconfigview_ipv6_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "yes");
			} else {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "no");
			}
			// verify BGP Check checkbox
			if (BGPCheck_checkbox.equalsIgnoreCase("Null")) {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "no");
			} else if (BGPCheck_checkbox.equalsIgnoreCase("yes")) {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "yes");
			} else {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "no");
			}
			// verify Man In Middle checkbox
			if (ManInMiddle_checkbox.equalsIgnoreCase("Null")) {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "no");
			} else if (ManInMiddle_checkbox.equalsIgnoreCase("yes")) {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "yes");
			} else {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "no");
			}
			// verify SNMP Notification checkbox
			if (snmpnotification_checkbox.equalsIgnoreCase("Null")) {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"no");
			} else if (snmpnotification_checkbox.equalsIgnoreCase("yes")) {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"yes");
			} else {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"no");
			}
			if (BGPCheck_checkbox.equalsIgnoreCase("Yes")) {
				// verify PA checkbox
				if (PA_checkbox.equalsIgnoreCase("Null")) {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "no");
				} else if (PA_checkbox.equalsIgnoreCase("yes")) {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "yes");
				} else {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "no");
				}
			}

			if (BGPCheck_checkbox.equalsIgnoreCase("Yes")) {
				// verify PI checkbox
				if (PI_checkbox.equalsIgnoreCase("Null")) {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "no");
				} else if (PI_checkbox.equalsIgnoreCase("yes")) {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "yes");
				} else {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "no");
				}
			}
			// verify Delivery Channel checkbox
			compareText("Delivery Channel", APT_IPAccessConfigObj.ipa_config.managementoptions_deliverychannel,
					deliverychannel_dropdownvalue);
			if (snmpnotification_checkbox.equalsIgnoreCase("Yes")) {
				compareText("Trap Target Address", APT_IPAccessConfigObj.ipa_config.managementoptions_traptargetaddress,
						traptargetaddress_value);
			}

			// Configurations Options panel
			scrollDown(APT_IPAccessConfigObj.ipa_config.configurationoptions_panelheader);
			((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-150)");
			compareText("Configuration Options", APT_IPAccessConfigObj.ipa_config.configurationoptions_panelheader,
					"Configuration Options");
			// verify Router Based Firewall checkbox
			if (routerbasedfirewall_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"no");
			} else if (routerbasedfirewall_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"yes");
			} else {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"no");
			}
			// verify QoS checkbox
			if (qos_checkbox.equalsIgnoreCase("Null")) {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "no");
			} else if (qos_checkbox.equalsIgnoreCase("yes")) {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "yes");
			} else {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "no");
			}
			// verify Actelis Based checkbox
			if (actelisbased_checkbox.equalsIgnoreCase("Null")) {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "no");
			} else if (actelisbased_checkbox.equalsIgnoreCase("yes")) {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "yes");
			} else {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "no");
			}

		} else {
			// ExtentTestManager.getTest().log(LogStatus.PASS, "Step : package
			// dropdown value is not selected");

			// verify Managed service
			if (managedservice_checkbox.equalsIgnoreCase("Null")) {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice, "no");
			} else if (managedservice_checkbox.equalsIgnoreCase("yes")) {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice,
						"yes");
			} else {
				compareText("Managed Service", APT_IPAccessConfigObj.ipa_config.managementoptions_managedservice, "no");
			}
			// verify IP Guardian checkbox
			if (ipguardian_checkbox.equalsIgnoreCase("Null")) {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "no");
			} else if (ipguardian_checkbox.equalsIgnoreCase("yes")) {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "yes");
			} else {
				compareText("IP Guardian", APT_IPAccessConfigObj.ipa_config.managementoptions_ipguardian, "no");
			}
			// verify Router Configuration View IPv4 checkbox
			if (routerconfigview_ipv4_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "no");
			} else if (routerconfigview_ipv4_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "yes");
			} else {
				compareText("Router Configuration View IPv4",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv4, "no");
			}
			// verify Router Configuration View IPv6 checkbox
			if (routerconfigview_ipv6_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "no");
			} else if (routerconfigview_ipv6_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "yes");
			} else {
				compareText("Router Configuration View IPv6",
						APT_IPAccessConfigObj.ipa_config.managementoptions_routerconfigIPv6, "no");
			}
			// verify BGP Check checkbox
			if (BGPCheck_checkbox.equalsIgnoreCase("Null")) {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "no");
			} else if (BGPCheck_checkbox.equalsIgnoreCase("yes")) {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "yes");
			} else {
				compareText("BGP Check", APT_IPAccessConfigObj.ipa_config.managementoptions_bgpcheck, "no");
			}
			// verify Man In Middle checkbox
			if (ManInMiddle_checkbox.equalsIgnoreCase("Null")) {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "no");
			} else if (ManInMiddle_checkbox.equalsIgnoreCase("yes")) {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "yes");
			} else {
				compareText("Man In Middle", APT_IPAccessConfigObj.ipa_config.managementoptions_maninmiddle, "no");
			}
			// verify SNMP Notification checkbox
			if (snmpnotification_checkbox.equalsIgnoreCase("Null")) {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"no");
			} else if (snmpnotification_checkbox.equalsIgnoreCase("yes")) {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"yes");
			} else {
				compareText("SNMP Notification", APT_IPAccessConfigObj.ipa_config.managementoptions_snmpnotification,
						"no");
			}
			if (BGPCheck_checkbox.equalsIgnoreCase("Yes")) {
				// verify PA checkbox
				if (PA_checkbox.equalsIgnoreCase("Null")) {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "no");
				} else if (PA_checkbox.equalsIgnoreCase("yes")) {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "yes");
				} else {
					compareText("PA", APT_IPAccessConfigObj.ipa_config.managementoptions_PAcheckbox, "no");
				}
			}

			if (BGPCheck_checkbox.equalsIgnoreCase("Yes")) {
				// verify PI checkbox
				if (PI_checkbox.equalsIgnoreCase("Null")) {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "no");
				} else if (PI_checkbox.equalsIgnoreCase("yes")) {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "yes");
				} else {
					compareText("PI", APT_IPAccessConfigObj.ipa_config.managementoptions_PIcheckbox, "no");
				}
			}
			// verify Delivery Channel checkbox
			compareText("Delivery Channel", APT_IPAccessConfigObj.ipa_config.managementoptions_deliverychannel,
					deliverychannel_dropdownvalue);
			if (snmpnotification_checkbox.equalsIgnoreCase("Yes")) {
				compareText("Trap Target Address", APT_IPAccessConfigObj.ipa_config.managementoptions_traptargetaddress,
						traptargetaddress_value);
			}

			// Configurations Options panel
			scrollDown(APT_IPAccessConfigObj.ipa_config.configurationoptions_panelheader);
			((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-150)");
			compareText("Configuration Options", APT_IPAccessConfigObj.ipa_config.configurationoptions_panelheader,
					"Configuration Options");
			// verify Router Based Firewall checkbox
			if (routerbasedfirewall_checkbox.equalsIgnoreCase("Null")) {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"no");
			} else if (routerbasedfirewall_checkbox.equalsIgnoreCase("yes")) {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"yes");
			} else {
				compareText("Router Based Firewall", APT_IPAccessConfigObj.ipa_config.configoptions_routerbasedfirewall,
						"no");
			}
			// verify QoS checkbox
			if (qos_checkbox.equalsIgnoreCase("Null")) {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "no");
			} else if (qos_checkbox.equalsIgnoreCase("yes")) {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "yes");
			} else {
				compareText("QoS", APT_IPAccessConfigObj.ipa_config.configoptions_qos, "no");
			}
			// verify Actelis Based checkbox
			if (actelisbased_checkbox.equalsIgnoreCase("Null")) {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "no");
			} else if (actelisbased_checkbox.equalsIgnoreCase("yes")) {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "yes");
			} else {
				compareText("Actelis Based", APT_IPAccessConfigObj.ipa_config.configoptions_actelisbased, "no");
			}
		}
	}

	public void verifyViewDevicepage_Links() throws InterruptedException, IOException {

		// Thread.sleep(1000);
		waitToPageLoad();
		verifyExists(APT_IPAccessConfigObj.ipa_config.viewdevice_Actiondropdown, "Action");
		click(APT_IPAccessConfigObj.ipa_config.viewdevice_Actiondropdown, "Action");

		compareText("Edit", APT_IPAccessConfigObj.ipa_config.viewdevice_Edit, "Edit");
		compareText("Delete", APT_IPAccessConfigObj.ipa_config.viewdevice_delete, "Delete");
		compareText("Fetch Interface", APT_IPAccessConfigObj.ipa_config.viewdevice_fetchinterfacelink,
				"Fetch Device Interfaces");

		// Edit in view device page
		verifyExists(APT_IPAccessConfigObj.ipa_config.viewdevice_Edit, "Edit");
		click(APT_IPAccessConfigObj.ipa_config.viewdevice_Edit, "Edit");
		waitToPageLoad();
		// Thread.sleep(2000);
		scrollUp();
		if (isVisible(APT_IPAccessConfigObj.ipa_config.editdeviceheader)) {
			Report.LogInfo("INFO", "Navigated to 'Edit Ethernet PE Device' page successfully", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Navigated to 'Edit Ethernet PE Device' page successfully");

			scrollDown(APT_IPAccessConfigObj.ipa_config.cancelbutton);
			waitForAjax();
			verifyExists(APT_IPAccessConfigObj.ipa_config.cancelbutton, "cancelbutton");
			click(APT_IPAccessConfigObj.ipa_config.cancelbutton, "cancelbutton");
			waitForAjax();
			waitforPagetobeenable();
		} else {
			Report.LogInfo("INFO", "Not navigated to 'Edit PE Device' page", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Not navigated to 'Edit PE Device' page");
		}

		scrollUp();
		// verify delete device in view device page
		verifyExists(APT_IPAccessConfigObj.ipa_config.viewdevice_Actiondropdown, "viewdevice_Actiondropdown");
		click(APT_IPAccessConfigObj.ipa_config.viewdevice_Actiondropdown, "viewdevice_Actiondropdown");

		verifyExists(APT_IPAccessConfigObj.ipa_config.viewdevice_delete, "viewdevice_delete");
		click(APT_IPAccessConfigObj.ipa_config.viewdevice_delete, "viewdevice_delete");

		waitForAjax();

		Alert alert = webDriver.switchTo().alert();

		// Capturing alert message
		String alertMessage = webDriver.switchTo().alert().getText();
		if (alertMessage.isEmpty()) {
			Report.LogInfo("INFO", "No message displays", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No message displays");
		} else {
			Report.LogInfo("INFO", "Alert message displays as: " + alertMessage, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Alert message displays as: " + alertMessage);
		}

		try {
			alert.dismiss();
			Thread.sleep(2000);

		} catch (Exception e) {
			e.printStackTrace();
		}

		scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
		verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
		click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
		waitForAjax();
	}

	public String verifyDeviceName() throws InterruptedException, IOException {

		scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_devicename);
		
		DeviceName = getTextFrom(APT_IPAccessConfigObj.ipa_config.viewpage_devicename,"view page_device name");
		return DeviceName;
	}

	public String verifyVendorModel(String application) throws InterruptedException, IOException {
		scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_vendormodel);
		
		
		Thread.sleep(1000);
		// Vendor Model
		VendorModel = getTextFrom(APT_IPAccessConfigObj.ipa_config.viewpage_vendormodel,"view page_vendor model");
		//VendorModel = GetText(application, "Vendor/Model", "viewpage_vendormodel");
		return VendorModel;
	}
	
	public void verify_Cisco_Ethernet_EthernetStandard_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceName");
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");
		/*String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"LinkValue");
		*/
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		/*String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerType_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLANID_Value");
		*/
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");
		/*String vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Checkbox");
		String vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Converged_Radiobutton");
		String vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Standalone_Radiobutton");
		*/
		
		waitforPagetobeenable();
		scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
		//Thread.sleep(1000);
		compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
		click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
		click(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
		
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
		scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
		//Thread.sleep(1000);
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
		click(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
		//verify warning messages in add interface page
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg, "interface_warngmsg");
		//verifyExists(APT_IPAccessConfigObj.ipa_config.encapsulation_warngmsg, "encapsulation_warngmsg");


		//Add Interface
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
		verifyExists(APT_IPAccessConfigObj.ipa_config.interfacename_textfield, "interfacename_textfield");
		sendKeys(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,interfacename);
		
		edittextFields_commonMethod("Interface Address Range",
				APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrange_textfield1,
				newinterfaceAddressrange);
		//addtextFields_commonMethod("Interface", "interfacename_textfield", interfacename, xml);
		
		InterfaceName= interfacename;
		
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
		
		//scrolltoend();
		generateConfiguration();
		
		waitForAjax();
		waitforPagetobeenable();
		verifysuccessmessage("Interface successfully created.");
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
		click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");

		
		waitforPagetobeenable();
	}
	
	public void generateConfiguration() throws InterruptedException, IOException {

		// perform Generate configuration
		boolean configurationpanel = false;

		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configuration_header)) {
			Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.generateconfiguration_button,
					"Generate Configuration");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.generateconfiguration_button,
					"Generate Configuration");

			waitforPagetobeenable();

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configuration_textarea);

			String configurationvalues = getTextFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.configuration_textarea,"configuration_text area");
			if (configurationvalues.isEmpty()) {
				Report.LogInfo("INFO",
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
						"FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
			} else {
				Report.LogInfo("INFO", "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
			}

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.saveconfiguration_button,
					"Save Configuration to File");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.saveconfiguration_button,
					"Save Configuration to File");

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.executeandsave_button,
					"Execute and Save");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.executeandsave_button, "Execute and Save");

		} else {
			Report.LogInfo("INFO", "'Configuration' panel is not displaying", "PASS");
			ExtentTestManager.getTest().log(LogStatus.INFO, "'Configuration' panel is not displaying");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "Ok");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.okbutton, "Ok");

		}
	}
	
	public void verifysuccessmessage(String expected) throws InterruptedException {

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.alertMsg);

		try {

			// boolean successMsg=getwebelement(xml.getlocator("//locators/" +
			// application + "/alertMsg")).isDisplayed();

			if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.alertMsg)) {

				// String alrtmsg=getwebelement(xml.getlocator("//locators/" +
				// application +
				// "/AlertForServiceCreationSuccessMessage")).getText();
				String alrtmsg = getTextFrom(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AlertForServiceCreationSuccessMessage,"Alert For Service Creation Success Message");
				if (expected.contains(alrtmsg)) {
					Report.LogInfo("INFO", "Message is verified. It is displaying as: " + alrtmsg, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Message is verified. It is displaying as: " + alrtmsg);

				} else {
					Report.LogInfo("INFO", "Message is displaying and it gets mismatches. It is displaying as: "
							+ alrtmsg + " .The Expected value is: " + expected, "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg
									+ " .The Expected value is: " + expected);
				}

			} else {
				Report.LogInfo("INFO", " Success Message is not displaying", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");

			}

		} catch (Exception e) {
			Report.LogInfo("INFO", expected + " Message is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, expected + " Message is not displaying");

		}
	}
	
	public void interfaceAddressRangeIPv4(String existingAddressRangeIPv4selection, String newAddressRangeIpv4selection,
			String subnetSizeValue_IPv4, String eipallocation_city, String existingAddressIPv4DropdownValue,
			String newinterfaceAddressrange) throws InterruptedException, IOException {

		boolean addressValue = false;

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			// verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_button,"EIP
			// Allocation button")
			// click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_button,"EIP
			// Allocation button");

			compareText("EIP Subnet Allocation",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipsubnetallocation_header,
					"EIP Subnet Allocation");

			addDropdownValues_commonMethod("City",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_citydropdown,
					eipallocation_city);
			addDropdownValues_commonMethod("Sub Net Size",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_subnetsize,
					subnetSizeValue_IPv4);

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");

			// waitForpageload();
			waitforPagetobeenable();
			// click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.getaddress_button,"Get
			// Address");

			// WebElement el =getwebelement(xml.getlocator("//locators/" +
			// application + "/interfacerange_Address_dropdown"));
			WebElement el = findWebElement(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown);
			Select sel = new Select(el);
			String interfaceAddressRange = sel.getFirstSelectedOption().getText();
			if (interfaceAddressRange.isEmpty()) {
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range' dropdown", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range' dropdown");

			} else {
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);

				waitforPagetobeenable();
				waitForAjax();

				// addressValue = getwebelement(xml.getlocator("//locators/" +
				// application +
				// "/interfacerange_Address_dropdown")).isDisplayed();
				if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_Address_dropdown)) {
					// selectEnableValueUnderAddressDropdown("Address",
					// "interfacerange_Address_dropdown" ,
					// existingAddressIPv4DropdownValue);
				} else {
					Report.LogInfo("INFO", "'Address' dropdown is not displaying", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address' dropdown is not displaying");
				}
			}
		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			edittextFields_commonMethod("Interface Address Range",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrange_textfield,
					newinterfaceAddressrange);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow, ">>");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddress_Addarrow, ">>");

			// String addressValueIntextField =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/address_textfield")).getAttribute("value");
			String addressValueIntextField = getAttributeFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.address_textfield, "value");
			if (addressValueIntextField.isEmpty()) {
				Report.LogInfo("INFO", "No values dipslaying under 'Address' text field", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address' text field");
			} else {
				Report.LogInfo("INFO", "value in 'Address' text field is displaying as: " + addressValueIntextField,
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address' text field is displaying as: " + addressValueIntextField);
			}

		}
	}

	public void interfaceAddressRangeIPv6(String existingAddressRangeIPv6selection, String newAddressRangeIpv6selection,
			String subnetSizeValue_IPv6, String availableBlocksValue_IPv6, String newinterfaceAddressrangeIPv6)
			throws InterruptedException, IOException {

		scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.network_fieldvalue);
		// IPv6 Configuration
		if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

		
			selectValueInsideDropdown(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.eipallocation_ipv6_subnetsize,
					"Sub Net Size", subnetSizeValue_IPv6);
			selectValueInsideDropdown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.availableblocks_dropdown,
					"Available Blocks", availableBlocksValue_IPv6);

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.allocatesubnet_button,
					"Allocate subnet");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.allocatesubnet_button, "Allocate subnet");

			// EIPallocationSuccessMessage(application, "successfully
			// allocated");
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.closesymbol, "Close");

			
			String interfaceAddressRange = getTextFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfacerange_AddressIpv6_dropdown,"interface range_Address Ipv6_dropdown");
			if (interfaceAddressRange.isEmpty()) {
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv6' dropdown");

			} else {
				Report.LogInfo("INFO",
						"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);

				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow,
						">>");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow, ">>");

				// click_commonMethod(application, ">>" ,
				// "interfaceaddressIPv6_Addarrow", xml);

				waitForAjax();
				waitforPagetobeenable();

				// String AddressValueIntextField =
				// getwebelement(xml.getlocator("//locators/" + application +
				// "/addressIPv6_textfield")).getAttribute("value");
				String AddressValueIntextField = getAttributeFrom(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addressIPv6_textfield, "value");
				if (AddressValueIntextField.isEmpty()) {
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
				} else {
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + AddressValueIntextField, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + AddressValueIntextField);
				}
			}
		} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

			edittextFields_commonMethod("Interface Address Range",
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressrangeIPv6_textfield,
					newinterfaceAddressrangeIPv6);

			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow, ">>");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.interfaceaddressIPv6_Addarrow, ">>");

			
			String addressValueIntextField = getAttributeFrom(
					APT_IPAccessResilientConfigObj.IPAccessResilientConfig.addressIPv6_textfield, "value");
			if (addressValueIntextField.isEmpty()) {
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv6' text field");
				// System.out.println("No values dipslaying under 'Address_IPv6'
				// text field");
			} else {
				Report.LogInfo("INFO",
						"value in 'Address_IPv6' text field is displaying as: " + addressValueIntextField, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv6' text field is displaying as: " + addressValueIntextField);
			}
		}
	}
	
	public void verify_Cisco_Ethernet_EthernetStandard_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException, AWTException {
		
		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
	/*	String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String edit_link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_LinkValue");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_Value");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Encapsulation_Value");
		String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLANID_Value");
		String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IVManagement_Checkbox");
		String edit_vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Checkbox");
		String edit_vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Converged_Radiobutton");	
		String edit_vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Standalone_Radiobutton");	
		*/
		
		//edit Interface
	
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		if(isVisible(APT_IPAccessConfigObj.ipa_config.createdInterfaceEditName+InterfaceName+APT_IPAccessConfigObj.ipa_config.createdInterfaceEditName1))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.createdInterfaceEditName+InterfaceName+APT_IPAccessConfigObj.ipa_config.createdInterfaceEditName1, "Edit Link");
			click(APT_IPAccessConfigObj.ipa_config.createdInterfaceEditName+InterfaceName+APT_IPAccessConfigObj.ipa_config.createdInterfaceEditName1, "Edit Link");

			Report.LogInfo("INFO", "Clicked on existing Interface radio button", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
						
			waitForAjax();
			waitforPagetobeenable();
			//compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit Interface/Link");
			scrollUp();
			
			//Add Interface
			editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
			Edit_InterfaceName= edit_interfacename;
			
			if(isAlertPresent()==true){
				Robot r = new Robot();
				Thread.sleep(1000);
				r.keyPress(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
				r.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
			}
			
			//scrolltoend();
			generateConfiguration();
			Thread.sleep(2000);
			verifysuccessmessage("Interface successfully updated.");
		}
		else
		{
			Report.LogInfo("INFO", "Interface is not added", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
		}

	}
	
	public void verify_Juniper_Ethernet_EthernetStandard_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
			
		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceName");
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"LinkValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerType_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLANID_Value");
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");
		String vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Checkbox");
		String vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Converged_Radiobutton");
		String vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Standalone_Radiobutton");
		
		String PEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PEWANTechnology");
		String Edit_PEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_PEWANTechnology");
		String cardtype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CardType_DropdownValue");
		String framingtype_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"FramingType_Value");
		String STM1number_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"STM1number_Value");
		String bearernumber_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerNumber_Value");
		String clocksource_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ClockSource_DropdownValue");
		String unitID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"UnitID_Value");
		String slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Slot_Value");
		String pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Pic_Value");
		String port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Port_Value");
		String DlciNumber_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DLCINumber_Value");
		String atricaconnected_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"AtricaConnected_checkbox");
		String coltAs_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ColtAs_Checkbox");
		String bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGP_Checkbox");
		
			scrollUp();
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
			//Thread.sleep(1000);
			compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
			click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
			click(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");

			scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
			
			//Thread.sleep(1000);
			//GetText(application, "Add Interface header", "addinterface_header");
			//ScrolltoElement(application, "executeandsave_button", xml);
			scrollDown(APT_IPAccessConfigObj.ipa_config.executeandsave_button);
			//Thread.sleep(1000);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.executeandsave_button, "executeandsave_button");
			click(APT_IPAccessConfigObj.ipa_config.executeandsave_button, "executeandsave_button");

			//Thread.sleep(1000);
			scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);

			//verify warning messages in add interface page
			verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg, "interface_warngmsg");
			verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg, "interface_warngmsg");
			

			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
			//GetText(application, "Network", "network_fieldvalue");
			
			if(PEWANTechnology.equalsIgnoreCase("ATM") || Edit_PEWANTechnology.equalsIgnoreCase("ATM")) {
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			}
			else
			{
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
			}
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield, "link_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);
			//addtextFields_commonMethod(application, "Link", "link_textfield", link_value, xml);
			
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", bearertype_value);
			//Thread.sleep(2000);
			//GetText(application, "Bandwidth", "interface_bandwidthvalue");
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);

			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.cardtype_dropdown, "Card Type", cardtype_value);
			
			if(bearertype_value.equalsIgnoreCase("E1")) {
				selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.framingtype_dropdown, "Framing Type", framingtype_dropdownvalue);
				selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.clocksource_dropdown, "Clock Source", clocksource_dropdownvalue);
				
				verifyExists(APT_IPAccessConfigObj.ipa_config.STM1Number_textfield, "STM1Number_textfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.STM1Number_textfield,STM1number_value);
				
				verifyExists(APT_IPAccessConfigObj.ipa_config.bearerno_textfield, "bearerno_textfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.bearerno_textfield,bearernumber_value);
			}
			if(bearertype_value.equalsIgnoreCase("E3") && bearertype_value.equalsIgnoreCase("T3")) {
				verifyExists(APT_IPAccessConfigObj.ipa_config.STM1Number_textfield, "STM1Number_textfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.STM1Number_textfield,STM1number_value);
				
				verifyExists(APT_IPAccessConfigObj.ipa_config.bearerno_textfield, "bearerno_textfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.bearerno_textfield,bearernumber_value);
				
			}
			if(!framingtype_dropdownvalue.equalsIgnoreCase("unframed")) {
				//String Timeslot= getwebelement(xml.getlocator("//locators/" + application + "/timeslot_field")).getAttribute("value");
				String Timeslot= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.timeslot_field,"value");

				Report.LogInfo("INFO", "Time slot value is displayed as: "+Timeslot, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Time slot value is displayed as: "+Timeslot);
			}
			edittextFields_commonMethod("Unit ID", APT_IPAccessConfigObj.ipa_config.unitid_textfield, unitID_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.slot_textfield, "slot_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.slot_textfield,slot_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.pic_textfield, "pic_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.pic_textfield,pic_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.port_textfield, "port_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.port_textfield,port_value);
		
			
			if(encapsulation_value.equalsIgnoreCase("frame-relay")) {
				verifyExists(APT_IPAccessConfigObj.ipa_config.dlcinumber_field, "dlcinumber_field");
				sendKeys(APT_IPAccessConfigObj.ipa_config.dlcinumber_field,DlciNumber_value);
			}
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.atricaconnected_checkbox, "Atrica Connected", atricaconnected_checkbox);
			//Thread.sleep(1000);
			if(bgp_checkbox.equalsIgnoreCase("Yes"))
			{
			//if(getwebelement(xml.getlocator("//locators/" + application + "/bgp_checkbox")).getAttribute("disabled")!=null)
			if(getAttributeFrom(APT_IPAccessConfigObj.ipa_config.bgp_checkbox,"disabled")!=null)
			{
				Report.LogInfo("INFO", "BGP checkbox is disabled as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "BGP checkbox is disabled as expected");
			}
			}
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfacename_textfield);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.coltAs_checkbox, "Colt As(15404)", coltAs_checkbox);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV", vlv_checkbox);
			Thread.sleep(2000);
			if(vlv_checkbox.equalsIgnoreCase("Yes")) {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", vlv_converged_radiobutton);
				if(vlv_converged_radiobutton.equalsIgnoreCase("yes")) {
					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
				}
				else {
					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
				}
				
			}
			
			//InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
			InterfaceName = getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
			Report.LogInfo("INFO", "Interface value is displayed as:"+InterfaceName, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+InterfaceName);
			//scrolltoend();
			generateConfiguration();
			waitForAjax();
			verifysuccessmessage("Interface successfully created.");
			waitForAjax();

			//Verify added interface
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
			//WebElement AddedInterfaces= getwebelement(xml.getlocator("//locators/" + application + "/addedinterfaces"));
			//String addedinterfacecheck= AddedInterfaces.getAttribute("style");
			String addedinterfacecheck= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.addedinterfaces,"style");
			if(!addedinterfacecheck.contains("height: 1px"))
			{
				compareText("Interfaces", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
				
			}
			else
			{
				Report.LogInfo("INFO", "No Interfaces added under Interfaces panel", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No Interfaces added under Interfaces panel");
			}
			scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
			verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
			click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
			
			waitForAjax();
			waitforPagetobeenable();		

		}
	
	public void verify_Juniper_Ethernet_EthernetStandard_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");
	
		
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String edit_link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_LinkValue");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_Value");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Encapsulation_Value");
		String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLANID_Value");
		String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IVManagement_Checkbox");
		String edit_vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Checkbox");
		String edit_vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Converged_Radiobutton");	
		String edit_vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Standalone_Radiobutton");	
		
		String PEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PEWANTechnology");
		String Edit_PEWANTechnology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_PEWANTechnology");
		String edit_unitID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_UnitID_Value");
		String edit_slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Slot_Value");
		String edit_atricaconnected_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_AtricaConnected_checkbox");
		String edit_pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Pic_Value");
		String edit_port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Port_Value");
		String edit_STM1number_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_STM1number_Value");
		String bgp_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BGP_Checkbox");
		String edit_coltAs_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ColtAs_Checkbox");
		
		//edit Interface
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		//if(getwebelement(xml.getlocator("//locators/" + application + "/showinterfaces_link")).isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.showinterfaces_link))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
			click(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
			
		}
		//Thread.sleep(1000);
		//WebElement SelectInterface= getwebelement(xml.getlocator("//locators/" + application + "/selectinterface").replace("value", InterfaceName));
		//if(SelectInterface.isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.selectinterface))
		{
			//Clickon(SelectInterface);
			verifyExists(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			click(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			Report.LogInfo("INFO", "Clicked on existing Interface radio button", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
			//Thread.sleep(1000);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2, "selectinterface");
			click(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2, "selectinterface");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.edit, "edit");
			click(APT_IPAccessConfigObj.ipa_config.edit, "edit");
			
			compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit");
			scrollUp();
			//Add Interface
			editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
			compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER CUST");
			
			if(PEWANTechnology.equalsIgnoreCase("ATM") || Edit_PEWANTechnology.equalsIgnoreCase("ATM")) {
				interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			}
			else
			{
				interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
				interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
			}
			
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
			edittextFields_commonMethod("Link", APT_IPAccessConfigObj.ipa_config.link_textfield, edit_link_value);
			
			if(edit_bearertype_value.equalsIgnoreCase("Yes")) {
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", edit_bearertype_value);
			}
			else
			{
				//String BearerType_Value= getwebelement(xml.getlocator("//locators/" + application + "/bearertype_dropdown")).getText();
				String BearerType_Value= getTextFrom(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown,"bearer type dropdown");
				Report.LogInfo("INFO", "Bearer type dropdown value is displayed as:"+BearerType_Value, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Bearer type dropdown value is displayed as:"+BearerType_Value);
			}
			//Thread.sleep(2000);
			//GetText("Bandwidth", "interface_bandwidthvalue");
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", edit_encapsulation_value);
			edittextFields_commonMethod("Unit ID", APT_IPAccessConfigObj.ipa_config.unitid_textfield, edit_unitID_value);
			edittextFields_commonMethod("Slot", APT_IPAccessConfigObj.ipa_config.slot_textfield, edit_slot_value);
			edittextFields_commonMethod("Pic", APT_IPAccessConfigObj.ipa_config.pic_textfield, edit_pic_value);
			edittextFields_commonMethod("Port", APT_IPAccessConfigObj.ipa_config.port_textfield, edit_port_value);
			edittextFields_commonMethod("STM1 Number", APT_IPAccessConfigObj.ipa_config.STM1Number_textfield, edit_STM1number_value);
			edittextFields_commonMethod("VLAN Id", APT_IPAccessConfigObj.ipa_config.vlanid_textfield, edit_vlanID_value);
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
			Edit_InterfaceName= edit_interfacename;
			editcheckbox_commonMethod(edit_atricaconnected_checkbox, "atricaconnected_checkbox", "Atrica Connected");
			Thread.sleep(1000);
			if(bgp_checkbox.equalsIgnoreCase("Yes"))
			{
			//if(getwebelement(xml.getlocator("//locators/" + application + "/bgp_checkbox")).getAttribute("disabled")!=null)
			if(getAttributeFrom(APT_IPAccessConfigObj.ipa_config.bgp_checkbox,"disabled")!=null)
			{
				Report.LogInfo("INFO", "BGP checkbox is disabled as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "BGP checkbox is disabled as expected");
			}
			}
			editcheckbox_commonMethod(edit_coltAs_checkbox, APT_IPAccessConfigObj.ipa_config.coltAs_checkbox, "Colt As(15404)");
			editcheckbox_commonMethod(edit_IVManagement_checkbox, APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management");
			editcheckbox_commonMethod(edit_vlv_checkbox, APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV");
			if(edit_vlv_checkbox.equalsIgnoreCase("Yes")) {
				editcheckbox_commonMethod(edit_vlv_converged_radiobutton, APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged");
				editcheckbox_commonMethod(edit_vlv_standalone_radiobutton, APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone");
			}
			
			//scrolltoend();
			generateConfiguration();
			//Thread.sleep(2000);
			verifysuccessmessage("Interface successfully updated.");
		}
		else
		{
			Report.LogInfo("INFO", "Interface is not added", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
		}

	}
	
	public void verify_Ethernet_ATM_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceName");
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");		
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"LinkValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");		
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerType_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");		
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");
		
		String PPPencapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"PPPEncapsulation_Value");
		String MBS_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"MBS_Value");
		String DSLdownstreamspeed_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DSLDownstreamSpeed_Value");
		String DSLupstreamspeed_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DSLUpstreamSpeed_Value");
		String VPI_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VPI_Value");
		String VCI_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VCI_Value");
		String slot_dropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Slot_DropdownValue");
		String port_dropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Port_DropdownValue");
		
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
			//Thread.sleep(1000);
			compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
			click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
			click(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
			
			//Thread.sleep(2000);
			scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
			//Thread.sleep(1000);
			//GetText("Add Interface header", "addinterface_header");
			scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
			//Thread.sleep(1000);
			verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
			click(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
			
			//Thread.sleep(1000);
			scrollDown("addinterface_header");
			//verify warning messages in add interface page
			verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg, "interface_warngmsg");
			verifyExists(APT_IPAccessConfigObj.ipa_config.encapsulation_warngmsg, "encapsulation_warngmsg");
		

			//Add Interface
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, interfacename);
			//Thread.sleep(1000);
			//InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
			InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
			
			Report.LogInfo("INFO", "Interface value is displayed as:"+InterfaceName, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+InterfaceName);
			
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
			verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield, "link_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);
			//addtextFields_commonMethod("Link", "link_textfield", link_value);
			
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", bearertype_value);
			
			//Thread.sleep(2000);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.pppEncapsulation_dropdown, "PPP Encapsulation", PPPencapsulation_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.DSLdownstreamspped_dropdown, "DSL Downstream Speed(PCR)", DSLdownstreamspeed_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.DSLupstreamspeed_dropdown, "DSL Upstream Speed(SCR)", DSLupstreamspeed_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.MBS_dropdown, "MBS", MBS_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.VPI_textfield, "VPI_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.VPI_textfield,VPI_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.VCI_textfield, "VCI_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.VCI_textfield,VCI_value);
			
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.slot_dropdown, "Slot", slot_dropdownValue);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.port_dropdown, "Port", port_dropdownValue);
			
			
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
			
			//scrolltoend();
			generateConfiguration();
			
			//Thread.sleep(5000);
			verifysuccessmessage("Interface added successfully");
			//Thread.sleep(2000);

			//Verify added interface
			scrollDown("interfaces_header");
			//WebElement AddedInterfaces= getwebelement(xml.getlocator("//locators/" + application + "/addedinterfaces"));
			//String addedinterfacecheck= AddedInterfaces.getAttribute("style");
			if(getAttributeFrom(APT_IPAccessConfigObj.ipa_config.addedinterfaces,"style").contains("height: 1px"))
			{
				compareText("Interfaces", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
		
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No Interfaces added under Interfaces panel");
			}
			scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
			verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
			click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");

			//Thread.sleep(2000);
		}
	
	public void verify_Ethernet_ATM_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
		
		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");	
		
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String edit_link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_LinkValue");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_Value");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Encapsulation_Value");
		
		String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IVManagement_Checkbox");		
		
		String edit_PPPencapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_PPPEncapsulation_Value");
		String edit_DSLdownstreamspeed_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_DSLDownstreamSpeed_Value");
		String edit_MBS_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_MBS_Value");
		String edit_DSLupstreamspeed_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_DSLUpstreamSpeed_Value");
		String edit_VPI_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VPI_Value");
		String edit_VCI_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VCI_Value");
		String edit_slot_dropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Slot_DropdownValue");
		String edit_port_dropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Port_DropdownValue");
		
		//edit Interface
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		//if(getwebelement(xml.getlocator("//locators/" + application + "/showinterfaces_link")).isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.showinterfaces_link))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
			click(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
		}
		//Thread.sleep(1000);
		//WebElement SelectInterface= getwebelement(xml.getlocator("//locators/" + application + "/selectinterface").replace("value", InterfaceName));
		//if(SelectInterface.isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.selectinterface))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			click(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			//Clickon(SelectInterface);
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
			//Thread.sleep(1000);
			//WebElement AddedDevice_Interface_Actiondropdown= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_interface_actiondropdown").replace("value", DeviceName));
			//Clickon(AddedDevice_Interface_Actiondropdown);
			verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1, "Interface Acton dropdown");
			click(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1, "Interface Acton dropdown");


			verifyExists(APT_IPAccessConfigObj.ipa_config.edit, "edit");
			click(APT_IPAccessConfigObj.ipa_config.edit, "edit");
		
			//Thread.sleep(2000);
			compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit");
			scrollUp();
			
			//Add Interface
			editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
			//Thread.sleep(1000);
			//Edit_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
			Edit_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
			Report.LogInfo("INFO", "Interface value is displayed as:"+Edit_InterfaceName, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Edit_InterfaceName);
			
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
			
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
			edittextFields_commonMethod("Link", APT_IPAccessConfigObj.ipa_config.link_textfield, edit_link_value);
			
			if(edit_bearertype_value.equalsIgnoreCase("Yes")) {
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", edit_bearertype_value);
			}
			else
			{
				//String BearerType_Value= getwebelement(xml.getlocator("//locators/" + application + "/bearertype_dropdown")).getText();
				String BearerType_Value= getTextFrom(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown,"bearer type_dropdown");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Bearer type dropdown value is displayed as:"+BearerType_Value);
				Report.LogInfo("INFO", "Bearer type dropdown value is displayed as:"+BearerType_Value, "PASS");
			}
			//Thread.sleep(2000);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", edit_encapsulation_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.pppEncapsulation_dropdown, "PPP Encapsulation", edit_PPPencapsulation_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.DSLdownstreamspped_dropdown, "DSL Downstream Speed(PCR)", edit_DSLdownstreamspeed_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.DSLupstreamspeed_dropdown, "DSL Upstream Speed(SCR)", edit_DSLupstreamspeed_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.MBS_dropdown, "MBS", edit_MBS_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.VPI_textfield, "VPI_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.VPI_textfield,edit_VPI_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.VCI_textfield, "VCI_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.VCI_textfield,edit_VCI_value);
			
			
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.slot_dropdown, "Slot", edit_slot_dropdownValue);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.port_dropdown, "Port", edit_port_dropdownValue);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", edit_IVManagement_checkbox);
			
			//scrolltoend();
			generateConfiguration();
			//Thread.sleep(2000);
			verifysuccessmessage("Interface successfully updated.");
		}
		else
		{
			Report.LogInfo("INFO", "Interface is not added", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
		}

	}
	
	public void verify_Ethernet_EthernetL2TP_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceName");
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");		
		
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");		
					
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");
				
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
		//Thread.sleep(1000);
		compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
		click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
		click(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
				
		//Thread.sleep(2000);
		scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
		//Thread.sleep(1000);
		//GetText("Add Interface header", "addinterface_header");
		scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
		//Thread.sleep(1000);
		verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
		click(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
		//verify warning messages in add interface page
		verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg, "interface_warngmsg");
		//warningMessage_commonMethod("interface_warngmsg", "Interface");

		//Add Interface
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
		edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, interfacename);
		//Thread.sleep(1000);
		//InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
		InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
		Report.LogInfo("INFO", "Interface value is displayed as:"+InterfaceName, "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+InterfaceName);
		
		interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
		
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
		//Thread.sleep(1000);
		
		//scrolltoend();
		generateConfiguration();
		
		//Thread.sleep(3000);
		verifysuccessmessage("Interface added successfully");
		//Thread.sleep(2000);

		//Verify added interface
		scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
		//WebElement AddedInterfaces= getwebelement(xml.getlocator("//locators/" + application + "/addedinterfaces"));
		//String addedinterfacecheck= AddedInterfaces.getAttribute("style");
		String addedinterfacecheck = getAttributeFrom(APT_IPAccessConfigObj.ipa_config.addedinterfaces,"style");

		if(!addedinterfacecheck.contains("height: 1px"))
		{
			compareText("Interfaces", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");			

		}
		else
		{
			Report.LogInfo("INFO", "No Interfaces added under Interfaces panel", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Interfaces added under Interfaces panel");
		}
		scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);

		verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
		click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
	
	}
	
	public void verify_Ethernet_EthernetL2TP_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
		
		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");	
		
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");		
		String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IVManagement_Checkbox");		
				
		
		//edit Interface
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		//if(getwebelement(xml.getlocator("//locators/" + application + "/showinterfaces_link")).isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.showinterfaces_link))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
			click(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
		}
		//Thread.sleep(1000);
		//WebElement SelectInterface= getwebelement(xml.getlocator("//locators/" + application + "/selectinterface").replace("value", InterfaceName));
		//if(SelectInterface.isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.selectinterface))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			click(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			//Clickon(SelectInterface);
			
			Report.LogInfo("INFO", "Clicked on existing Interface radio button", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
			//Thread.sleep(1000);
			//WebElement AddedDevice_Interface_Actiondropdown= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_interface_actiondropdown").replace("value", DeviceName));
			verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2, "Action dropdown");
			click(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2, "Action dropdown");

			verifyExists(APT_IPAccessConfigObj.ipa_config.edit, "edit");
			click(APT_IPAccessConfigObj.ipa_config.edit, "edit");
			//Thread.sleep(2000);
			compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit");
			scrollUp();
			//Add Interface
			editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
			//Thread.sleep(1000);
			
			//Edit_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
			Edit_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
			Report.LogInfo("PASS", "Interface value is displayed as:"+Edit_InterfaceName, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Edit_InterfaceName);
			
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			
			editcheckbox_commonMethod(edit_IVManagement_checkbox, APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management");
			//Thread.sleep(1000);
			
			//scrolltoend();
			generateConfiguration();
			//Thread.sleep(2000);
			verifysuccessmessage("Interface successfully updated.");
		}
		else
		{
			Report.LogInfo("INFO", "Interface is not added", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
		}

	}
	
	public void verify_Juniper_SDH_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"LinkValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerType_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLANID_Value");
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");
		String vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Checkbox");
		String vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Converged_Radiobutton");
		String vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Standalone_Radiobutton");
	
		String unitID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"UnitID_Value");
		String slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Slot_Value");
		String pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Pic_Value");
		String port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Port_Value");
		
		String atricaconnected_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"AtricaConnected_checkbox");
		String coltAs_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ColtAs_Checkbox");
	
		
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
			//Thread.sleep(1000);
			compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
			click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
			click(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
			
		
			//Thread.sleep(2000);
			scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
			//Thread.sleep(1000);
			//GetText("Add Interface header", "addinterface_header");
			scrollDown(APT_IPAccessConfigObj.ipa_config.executeandsave_button);
			//Thread.sleep(1000);
			verifyExists(APT_IPAccessConfigObj.ipa_config.executeandsave_button, "executeandsave_button");
			click(APT_IPAccessConfigObj.ipa_config.executeandsave_button, "executeandsave_button");
			//Thread.sleep(1000);
			scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
			//verify warning messages in add interface page
			verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg, "interface_warngmsg");
			verifyExists(APT_IPAccessConfigObj.ipa_config.encapsulation_warngmsg, "encapsulation_warngmsg");


			//Add Interface
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
			compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER");
			
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
			
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
			verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield, "link_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);
			//addtextFields_commonMethod("Link", "link_textfield", link_value);
			selectValueInsideDropdown("bearertype_dropdown", "Bearer Type", bearertype_value);
			
			//Thread.sleep(2000);
			//GetText("Bandwidth", "interface_bandwidthvalue");
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);
			verifyExists(APT_IPAccessConfigObj.ipa_config.unitid_textfield, "unitid_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.unitid_textfield,unitID_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.slot_textfield, "slot_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.slot_textfield,slot_value);

			verifyExists(APT_IPAccessConfigObj.ipa_config.pic_textfield, "pic_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.pic_textfield,pic_value);

			verifyExists(APT_IPAccessConfigObj.ipa_config.port_textfield, "port_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.port_textfield,port_value);

			verifyExists(APT_IPAccessConfigObj.ipa_config.vlanid_textfield, "vlanid_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.vlanid_textfield,vlanID_value);

			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.atricaconnected_checkbox, "Atrica Connected", atricaconnected_checkbox);
			//Thread.sleep(1000);
			//if(getwebelement(xml.getlocator("//locators/" + application + "/bgp_checkbox")).getAttribute("disabled")!=null)
			if(getAttributeFrom(APT_IPAccessConfigObj.ipa_config.bgp_checkbox,"disabled")!=null)
			{
				Report.LogInfo("INFO", "BGP checkbox is disabled as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "BGP checkbox is disabled as expected");
			}
	

			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.coltAs_checkbox, "Colt As(15404)", coltAs_checkbox);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV", vlv_checkbox);
			if(vlv_checkbox.equalsIgnoreCase("Yes")) {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", vlv_converged_radiobutton);
				if(vlv_converged_radiobutton.equalsIgnoreCase("yes")) {
					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
				}
				else {
					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
				}
				
			}

			InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");

			//InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+InterfaceName);
			//scrolltoend();
			generateConfiguration();
			
			//Thread.sleep(5000);
			waitForAjax();
			verifysuccessmessage("Interface added successfully");
			//Thread.sleep(2000);

			//Verify added interface
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
			//WebElement AddedInterfaces= getwebelement(xml.getlocator("//locators/" + application + "/addedinterfaces"));
			//String addedinterfacecheck= AddedInterfaces.getAttribute("style");
			if(getAttributeFrom(APT_IPAccessConfigObj.ipa_config.addedinterfaces,"style").contains("height: 1px"))
			{
				compareText("Interfaces", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
			}
			else
			{
				Report.LogInfo("INFO", "No Interfaces added under Interfaces panel", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No Interfaces added under Interfaces panel");
			}
			scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
			//Thread.sleep(1000);
			verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
			click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");

			//Thread.sleep(1000);
			waitforPagetobeenable();

		}
	
	public void verify_Juniper_SDH_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");	
		
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String edit_link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_LinkValue");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_Value");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Encapsulation_Value");
		String edit_vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLANID_Value");
		String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IVManagement_Checkbox");
		String edit_vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Checkbox");
		String edit_vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Converged_Radiobutton");	
		String edit_vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Standalone_Radiobutton");			
		
		String edit_unitID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_UnitID_Value");
		String edit_slot_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Slot_Value");
		String edit_atricaconnected_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_AtricaConnected_checkbox");
		String edit_pic_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Pic_Value");
		String edit_port_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Port_Value");		
		String edit_coltAs_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ColtAs_Checkbox");
		
		//edit Interface
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		//Thread.sleep(1000);
		//WebElement SelectInterface= getwebelement(xml.getlocator("//locators/" + application + "/selectinterface").replace("value", InterfaceName));
		//if(SelectInterface.isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.selectinterface))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			click(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
			//Clickon(SelectInterface);
			Report.LogInfo("INFO", "Clicked on existing Interface radio button", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
			//Thread.sleep(1000);
			
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2, "Action dropdown");
			click(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2, "Action dropdown");

			verifyExists(APT_IPAccessConfigObj.ipa_config.edit, "edit");
			click(APT_IPAccessConfigObj.ipa_config.edit, "edit");
			//Thread.sleep(2000);
			waitforPagetobeenable();
			
			compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit");
			scrollUp();
			//Add Interface
			editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
			//Thread.sleep(1000);
			
				
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
			
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
			edittextFields_commonMethod("Link", APT_IPAccessConfigObj.ipa_config.link_textfield, edit_link_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", edit_bearertype_value);
			
			//Thread.sleep(2000);
			//GetText("Bandwidth", "interface_bandwidthvalue");
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", edit_encapsulation_value);
			edittextFields_commonMethod("Unit ID", APT_IPAccessConfigObj.ipa_config.unitid_textfield, edit_unitID_value);
			edittextFields_commonMethod("Slot", APT_IPAccessConfigObj.ipa_config.slot_textfield, edit_slot_value);
			edittextFields_commonMethod("Pic", APT_IPAccessConfigObj.ipa_config.pic_textfield, edit_pic_value);
			edittextFields_commonMethod("Port", APT_IPAccessConfigObj.ipa_config.port_textfield, edit_port_value);
			edittextFields_commonMethod("VLAN Id", APT_IPAccessConfigObj.ipa_config.vlanid_textfield, edit_vlanID_value);
			
			//Edit_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
			Edit_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
			Report.LogInfo("PASS", "Interface value is displayed as:"+Edit_InterfaceName, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Edit_InterfaceName);
	
			editcheckbox_commonMethod(edit_atricaconnected_checkbox, APT_IPAccessConfigObj.ipa_config.atricaconnected_checkbox, "Atrica Connected");
			//Thread.sleep(1000);
			//if(getwebelement(xml.getlocator("//locators/" + application + "/bgp_checkbox")).getAttribute("disabled")!=null)
			if(getAttributeFrom(APT_IPAccessConfigObj.ipa_config.bgp_checkbox,"disabled")!=null)
			{
				Report.LogInfo("INFO", "BGP checkbox is disabled as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "BGP checkbox is disabled as expected");
			}

			editcheckbox_commonMethod(edit_coltAs_checkbox, APT_IPAccessConfigObj.ipa_config.coltAs_checkbox, "Colt As(15404)");
			editcheckbox_commonMethod(edit_IVManagement_checkbox, APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management");
			editcheckbox_commonMethod(edit_vlv_checkbox, APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV");
			if(edit_vlv_checkbox.equalsIgnoreCase("Yes")) {
				editcheckbox_commonMethod(edit_vlv_converged_radiobutton, APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged");
				editcheckbox_commonMethod(edit_vlv_standalone_radiobutton, APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone");
			}
			
			//scrolltoend();
			generateConfiguration();
			//Thread.sleep(2000);
			verifysuccessmessage("Interface successfully updated.");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
		}

	}
	
	public void verify_SDH_AddMultilink(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DeviceName");	
		String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");	
		String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_InterfaceName");	
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"LinkValue");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");
		String vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Checkbox");
		String vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Converged_Radiobutton");
		String vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Standalone_Radiobutton");
		String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CheckToAddInterface_Checkbox");
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		//if(getwebelement(xml.getlocator("//locators/" + application + "/existingdevicegrid")).isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.existingdevicegrid))
		{
			//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addeddevices_list"));
			List<WebElement> addeddevicesList= findWebElements(APT_IPAccessConfigObj.ipa_config.addeddevices_list);
			//System.out.println(addeddevicesList);
			int AddedDevicesCount= addeddevicesList.size();
			for(int i=0;i<AddedDevicesCount;i++) {
				String AddedDeviceNameText= addeddevicesList.get(i).getText();
				String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
				if(AddedDeviceNameText.contains(name))
				{
					//WebElement AddedDevice_ViewLink= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_viewlink").replace("value", AddedDevice_SNo));
					verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
					click(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
					
					waitForAjax();
					//Clickon(AddedDevice_ViewLink);
					//Thread.sleep(5000);
					compareText("View device header", APT_IPAccessConfigObj.ipa_config.viewdevicepage_header, "Device Details");

					scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
					//Thread.sleep(1000);
					compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
					verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
					click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");

					verifyExists(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");
					click(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");

					//Thread.sleep(2000);
					scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", multilink_configureinterface_checkbox);
					scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
					//Thread.sleep(1000);
					verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
					click(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
					//click_commonMethod("OK", "okbutton");
					//Thread.sleep(1000);
					scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

					//Add Multilink
					verifyExists(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"interfacename_textfield");
					sendKeys(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,multilink_interfacename);

					compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER");
					
					interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
					interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
					
					scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
					verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield,"link_textfield");
					sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);

					selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);

					editcheckbox_commonMethod(IVManagement_checkbox, APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management");
					editcheckbox_commonMethod(vlv_checkbox, APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV");
					if(vlv_checkbox.equalsIgnoreCase("Yes")) {
						addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", vlv_converged_radiobutton);
						if(vlv_converged_radiobutton.equalsIgnoreCase("yes")) {
							addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
						}
						else {
							addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
						}
					}
					
					//Multilink_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
					Multilink_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
					Report.LogInfo("INFO", "Interface value is displayed as:"+Multilink_InterfaceName, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Multilink_InterfaceName);
					
					//Multilinked Bearers table
					//scrolltoend();
					compareText("Multilinked Bearers", APT_IPAccessConfigObj.ipa_config.multilinkedbearers_header, "Multilinked Bearers");

					//table columns
					compareText("Check to add Interface", APT_IPAccessConfigObj.ipa_config.checktoaddinterface_column, "Check to add Interface");
					compareText("Interface", APT_IPAccessConfigObj.ipa_config.multilink_interface_column, "Interface");
					compareText("Link/Circuit", APT_IPAccessConfigObj.ipa_config.multilink_link_column, "Link/Circuit");
					compareText("Bearer Type", APT_IPAccessConfigObj.ipa_config.multilink_BearerType_column, "Bearer Type");
					compareText("VLAN Id", APT_IPAccessConfigObj.ipa_config.multilink_vlanid_column, "VLAN Id");
					
					//String MultilinkBearer_ExistingInterface= getwebelement(xml.getlocator("//locators/" + application + "/multilinkbearer_tabledata")).getAttribute("style");
					String MultilinkBearer_ExistingInterface= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilinkbearer_tabledata,"style");
			
					if(!MultilinkBearer_ExistingInterface.contains("height: 1px"))
					{
						ExtentTestManager.getTest().log(LogStatus.PASS, "Existing interface details are displaying in multilink bearer table");
						if(checktoaddinterface_checkbox.equalsIgnoreCase("yes"))
						{
							//WebElement CheckToAddInterface= getwebelement(xml.getlocator("//locators/" + application + "/checktoaddinterface").replace("value", InterfaceName));
							verifyExists(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");
							click(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");

							//Clickon(CheckToAddInterface);
						}
					}
					else
					{
						Report.LogInfo("INFO", "No existing interfaces to display", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "No existing interfaces to display");
					}
					//scrolltoend();
					//configuration panel in add interface page
					generateConfiguration();
					
					//Thread.sleep(2000);
					waitforPagetobeenable();
					verifysuccessmessage("Multilink Interface successfully created");
					scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
					//Thread.sleep(1000);
					compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");

					//Multilink table values under interfaces panel
					String Multilink_Name= "Multilink"+Multilink_InterfaceName;
					//String Multilink_RowID= getwebelement(xml.getlocator("//locators/" + application + "/multilink_rowid").replace("value", Multilink_Name)).getAttribute("row-id");
					String Multilink_RowID=getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilink_rowid1+Multilink_Name+APT_IPAccessConfigObj.ipa_config.multilink_rowid2,"row-id");
					
					String Bandwidth_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"Band width table value");
					Report.LogInfo("INFO", "Bandwidth value is displayed as:"+Bandwidth_value, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Bandwidth value is displayed as:"+Bandwidth_value);
					click(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"BandWidth Table");
					//Clickon(Bandwidth);
					keyPress(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,Keys.TAB);
					//Bandwidth.sendKeys(Keys.TAB);
					
					
					String vlanID_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,"VLan ID value");
					Report.LogInfo("INFO", "VLAN Id  value is displayed as:"+vlanID_value, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "VLAN Id value is displayed as:"+vlanID_value);
					click(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,"Vlan Table");
					keyPress(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,Keys.TAB);
								
					
					scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
					verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
					click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
				
					//Multilink_InterfaceName= Multilink_Name;
				}
				else
				{
					Report.LogInfo("INFO", "Invalid device name", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
				}
			}
		}
		else
		{
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}

	}
	
	public void verify_OneAccess_SDH_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceName");
		String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"LinkValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"InterfaceAddressRange_Value");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"BearerType_Value");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");		
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");
		String vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Checkbox");
		String vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Converged_Radiobutton");
		String vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"VLV_Standalone_Radiobutton");		
			
		
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
			//Thread.sleep(1000);
			compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
			verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
			click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link,"addinterface_link");
			click(APT_IPAccessConfigObj.ipa_config.addinterface_link,"addinterface_link");
			
			//Thread.sleep(2000);
			scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
			//Thread.sleep(1000);
			//GetText("Add Interface header", "addinterface_header");
			scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
			//Thread.sleep(1000);
			verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
			click(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");

			//Thread.sleep(1000);
			scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
			//verify warning messages in add interface page
			verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg,"interface_warngmsg");
			verifyExists(APT_IPAccessConfigObj.ipa_config.encapsulation_warngmsg,"encapsulation_warngmsg");

			//Add Interface
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, interfacename);
			//Thread.sleep(1000);
			
			InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
			Report.LogInfo("INFO", "Interface value is displayed as:"+InterfaceName, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+InterfaceName);
			compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER");
			
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
			
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
			verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield,"link_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);

			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", bearertype_value);
			
			//Thread.sleep(2000);
			//GetText("Bandwidth", "interface_bandwidthvalue");
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);
			
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV", vlv_checkbox);
			if(vlv_checkbox.equalsIgnoreCase("Yes")) {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", vlv_converged_radiobutton);
				if(vlv_converged_radiobutton.equalsIgnoreCase("yes")) {
					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
				}
				else {
					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
				}
				
			}
			
			//scrolltoend();
			generateConfiguration();
			
			//Thread.sleep(5000);
			verifysuccessmessage("Interface added successfully");
			//Thread.sleep(2000);

			//Verify added interface
			scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
			//WebElement AddedInterfaces= getwebelement(xml.getlocator("//locators/" + application + "/addedinterfaces"));
			//String addedinterfacecheck= AddedInterfaces.getAttribute("style");
			String addedinterfacecheck= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.addedinterfaces,"style");
			if(!addedinterfacecheck.contains("height: 1px"))
			{
				compareText("Interfaces", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");				

			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No Interfaces added under Interfaces panel");
			}
			scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
			verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"viewpage_backbutton");
			click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"viewpage_backbutton");
			
			//Thread.sleep(2000);

		}
	
	public void verify_OneAccess_SDH_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		

		String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_ConfigureInterface_Checkbox");
	
		
		String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_InterfaceName");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv6_SubnetSize");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_AvailableBlocksValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv6");
		String edit_link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_LinkValue");
		String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_BearerType_Value");
		String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_Encapsulation_Value");	
		String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_IVManagement_Checkbox");
		String edit_vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Checkbox");
		String edit_vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Converged_Radiobutton");	
		String edit_vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Edit_VLV_Standalone_Radiobutton");	
		
		
		//edit Interface
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		//Thread.sleep(1000);
		//WebElement SelectInterface= getwebelement(xml.getlocator("//locators/" + application + "/selectinterface").replace("value", InterfaceName));
		if(isVisible(APT_IPAccessConfigObj.ipa_config.selectinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.selectinterface2))
		{
			verifyExists(APT_IPAccessConfigObj.ipa_config.selectinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.selectinterface2,"interface");
			click(APT_IPAccessConfigObj.ipa_config.selectinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.selectinterface2,"interface");

			Report.LogInfo("INFO", "Clicked on existing Interface radio button", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
			//Thread.sleep(1000);
			//WebElement AddedDevice_Interface_Actiondropdown= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_interface_actiondropdown").replace("value", DeviceName));
			verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2,"DeviceName");
			click(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2,"DeviceName");

			verifyExists(APT_IPAccessConfigObj.ipa_config.edit,"edit");
			click(APT_IPAccessConfigObj.ipa_config.edit,"edit");
	
			//Thread.sleep(2000);
			compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit");
			scrollUp();
			//Add Interface
			editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
			edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
			//Thread.sleep(1000);
			//Edit_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
			//ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Edit_InterfaceName);
			
			interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
			interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
			
			scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
			edittextFields_commonMethod("Link", APT_IPAccessConfigObj.ipa_config.link_textfield, edit_link_value);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", edit_bearertype_value);
			
			//Thread.sleep(2000);
			//GetText("Bandwidth", "interface_bandwidthvalue");
			selectValueInsideDropdown("encapsulation_dropdown", "Encapsulation", edit_encapsulation_value);
			
			verifyExists(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"interfacename_textfield");
			sendKeys(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,edit_interfacename);
			//addtextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
			Edit_InterfaceName= edit_interfacename;
			
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", edit_IVManagement_checkbox);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV", edit_vlv_checkbox);
			if(edit_vlv_checkbox.equalsIgnoreCase("Yes")) {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", edit_vlv_converged_radiobutton);
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", edit_vlv_standalone_radiobutton);
			}
			
			//scrolltoend();
			generateConfiguration();
			//Thread.sleep(2000);
			verifysuccessmessage("Interface successfully updated.");
		}
		else
		{
			Report.LogInfo("INFO", "Interface is not added", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
		}

	}
	
public void verify_ADSL_ATM_AddMultilink(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
		
		String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DeviceName");	
		String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ConfigureInterface_Checkbox");	
		String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Multilink_InterfaceName");	
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_IPv4_SubnetSize");
		String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EIPAllocation_City");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"NewInterfaceAddressRangeIPv4");		
		String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"LinkValue");
		String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"Encapsulation_Value");
		String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"IVManagement_Checkbox");		
		String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"CheckToAddInterface_Checkbox");
		String DSLdownstreamspeed_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DSLDownstreamSpeed_Value");
		String DSLupstreamspeed_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"DSLUpstreamSpeed_Value");
		
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
		//if(getwebelement(xml.getlocator("//locators/" + application + "/existingdevicegrid")).isDisplayed())
		if(isVisible(APT_IPAccessConfigObj.ipa_config.existingdevicegrid))
		{
			//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addeddevices_list"));
			List<WebElement> addeddevicesList= findWebElements(APT_IPAccessConfigObj.ipa_config.addeddevices_list);
			//System.out.println(addeddevicesList);
			int AddedDevicesCount= addeddevicesList.size();
			for(int i=0;i<AddedDevicesCount;i++) {
				String AddedDeviceNameText= addeddevicesList.get(i).getText();
				String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
				if(AddedDeviceNameText.contains(name))
				{
					//WebElement AddedDevice_ViewLink= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_viewlink").replace("value", AddedDevice_SNo));
					verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
					click(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
					
					waitForAjax();
					//Clickon(AddedDevice_ViewLink);
					//Thread.sleep(5000);
					compareText("View device header", APT_IPAccessConfigObj.ipa_config.viewdevicepage_header, "Device Details");

					scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
					//Thread.sleep(1000);
					compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
					verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
					click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");

					verifyExists(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");
					click(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");

					//Thread.sleep(2000);
					scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", multilink_configureinterface_checkbox);
					scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
					//Thread.sleep(1000);
					verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
					click(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
					//click_commonMethod("OK", "okbutton");
					//Thread.sleep(1000);
					scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

					//Add Multilink
					verifyExists(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"interfacename_textfield");
					sendKeys(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,multilink_interfacename);

					compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER");
					
					interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
					//interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
					
					editcheckbox_commonMethod(IVManagement_checkbox, APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management");
					scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
					verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield,"link_textfield");
					sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);

					selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);
					
					selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.DSLdownstreamspped_dropdown, "DSL Downstream Speed(PCR)", DSLdownstreamspeed_value);
					selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.DSLupstreamspeed_dropdown, "DSL Upstream Speed(SCR)", DSLupstreamspeed_value);
					
					
					//Multilink_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
					Multilink_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
					Report.LogInfo("INFO", "Interface value is displayed as:"+Multilink_InterfaceName, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Multilink_InterfaceName);
					
					//Multilinked Bearers table
					//scrolltoend();
					compareText("Multilinked Bearers", APT_IPAccessConfigObj.ipa_config.multilinkedbearers_header, "Multilinked Bearers");

					//table columns
					compareText("Check to add Interface", APT_IPAccessConfigObj.ipa_config.checktoaddinterface_column, "Check to add Interface");
					compareText("Interface", APT_IPAccessConfigObj.ipa_config.multilink_interface_column, "Interface");
					compareText("Link/Circuit", APT_IPAccessConfigObj.ipa_config.multilink_link_column, "Link/Circuit");
					compareText("Bearer Type", APT_IPAccessConfigObj.ipa_config.multilink_BearerType_column, "Bearer Type");
					compareText("VLAN Id", APT_IPAccessConfigObj.ipa_config.multilink_vlanid_column, "VLAN Id");
					
					//String MultilinkBearer_ExistingInterface= getwebelement(xml.getlocator("//locators/" + application + "/multilinkbearer_tabledata")).getAttribute("style");
					String MultilinkBearer_ExistingInterface= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilinkbearer_tabledata,"style");
			
					if(!MultilinkBearer_ExistingInterface.contains("height: 1px"))
					{
						ExtentTestManager.getTest().log(LogStatus.PASS, "Existing interface details are displaying in multilink bearer table");
						if(checktoaddinterface_checkbox.equalsIgnoreCase("yes"))
						{
							//WebElement CheckToAddInterface= getwebelement(xml.getlocator("//locators/" + application + "/checktoaddinterface").replace("value", InterfaceName));
							verifyExists(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");
							click(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");

							//Clickon(CheckToAddInterface);
						}
					}
					else
					{
						Report.LogInfo("INFO", "No existing interfaces to display", "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS, "No existing interfaces to display");
					}
					//scrolltoend();
					//configuration panel in add interface page
					generateConfiguration();
					
					//Thread.sleep(2000);
					waitforPagetobeenable();
					verifysuccessmessage("Multilink Interface successfully created");
					scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
					//Thread.sleep(1000);
					compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");

					//Multilink table values under interfaces panel
					String Multilink_Name= "Multilink"+Multilink_InterfaceName;
					//String Multilink_RowID= getwebelement(xml.getlocator("//locators/" + application + "/multilink_rowid").replace("value", Multilink_Name)).getAttribute("row-id");
					String Multilink_RowID=getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilink_rowid1+Multilink_Name+APT_IPAccessConfigObj.ipa_config.multilink_rowid2,"row-id");
					
					String Bandwidth_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"Band width table value");
					Report.LogInfo("INFO", "Bandwidth value is displayed as:"+Bandwidth_value, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Bandwidth value is displayed as:"+Bandwidth_value);
					click(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"BandWidth Table");
					//Clickon(Bandwidth);
					keyPress(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,Keys.TAB);
					//Bandwidth.sendKeys(Keys.TAB);
					
					
					String vlanID_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,"vlan ID table value");
					Report.LogInfo("INFO", "VLAN Id  value is displayed as:"+vlanID_value, "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "VLAN Id value is displayed as:"+vlanID_value);
					click(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,"Vlan Table");
					keyPress(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,Keys.TAB);
								
					
					scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
					verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
					click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
				
					//Multilink_InterfaceName= Multilink_Name;
				}
				else
				{
					Report.LogInfo("INFO", "Invalid device name", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
				}
			}
		}
		else
		{
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}

	}

public void verify_ADSL_L2TP_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"InterfaceName");
	String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ConfigureInterface_Checkbox");
	String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressRangeIPv4selection");
	String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv4selection");
	String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv4_SubnetSize");
	String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_City");
	String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressIPv4DropdownValue");		
	String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"LinkValue");
	String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"InterfaceAddressRange_Value");		
	String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"BearerType_Value");
	String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Encapsulation_Value");		
	String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"IVManagement_Checkbox");
	

		scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
		//Thread.sleep(1000);
		compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
		click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown, "interfacepanel_actiondropdown");
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
		click(APT_IPAccessConfigObj.ipa_config.addinterface_link, "addinterface_link");
		
		//Thread.sleep(2000);
		scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
		//Thread.sleep(1000);
		//GetText("Add Interface header", "addinterface_header");
		scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
		//Thread.sleep(1000);
		verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
		click(APT_IPAccessConfigObj.ipa_config.okbutton, "okbutton");
		
		//Thread.sleep(1000);
		scrollDown("addinterface_header");
		//verify warning messages in add interface page
		verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg, "interface_warngmsg");
		verifyExists(APT_IPAccessConfigObj.ipa_config.encapsulation_warngmsg, "encapsulation_warngmsg");
	

		//Add Interface
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
		edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, interfacename);
		//Thread.sleep(1000);
		//InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
		InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
		
		Report.LogInfo("INFO", "Interface value is displayed as:"+InterfaceName, "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+InterfaceName);
		
		interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
		
		
		
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
		
		//scrolltoend();
		generateConfiguration();
		
		//Thread.sleep(5000);
		verifysuccessmessage("Interface added successfully");
		//Thread.sleep(2000);

		//Verify added interface
		scrollDown("interfaces_header");
		//WebElement AddedInterfaces= getwebelement(xml.getlocator("//locators/" + application + "/addedinterfaces"));
		//String addedinterfacecheck= AddedInterfaces.getAttribute("style");
		if(getAttributeFrom(APT_IPAccessConfigObj.ipa_config.addedinterfaces,"style").contains("height: 1px"))
		{
			compareText("Interfaces", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
	
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Interfaces added under Interfaces panel");
		}
		scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
		verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");
		click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton, "viewpage_backbutton");

		//Thread.sleep(2000);
	}

public void verify_ADSL_L2TP_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
	
	String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_ConfigureInterface_Checkbox");	
	
	String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_InterfaceName");
	String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"existingAddressRangeIPv4selection");
	String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv4selection");
	String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv4_SubnetSize");
	String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_City");
	String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressIPv4DropdownValue");
	String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewInterfaceAddressRangeIPv4");
	
	String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_IVManagement_Checkbox");		
	
	
	//edit Interface
	scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
	//if(getwebelement(xml.getlocator("//locators/" + application + "/showinterfaces_link")).isDisplayed())
	if(isVisible(APT_IPAccessConfigObj.ipa_config.showinterfaces_link))
	{
		verifyExists(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
		click(APT_IPAccessConfigObj.ipa_config.showinterfaces_link, "showinterfaces_link");
	}
	//Thread.sleep(1000);
	//WebElement SelectInterface= getwebelement(xml.getlocator("//locators/" + application + "/selectinterface").replace("value", InterfaceName));
	//if(SelectInterface.isDisplayed())
	if(isVisible(APT_IPAccessConfigObj.ipa_config.selectinterface))
	{
		verifyExists(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
		click(APT_IPAccessConfigObj.ipa_config.selectinterface, "selectinterface");
		//Clickon(SelectInterface);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
		verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1, "Interface Acton dropdown");
		click(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1, "Interface Acton dropdown");


		verifyExists(APT_IPAccessConfigObj.ipa_config.edit, "edit");
		click(APT_IPAccessConfigObj.ipa_config.edit, "edit");
	
		//Thread.sleep(2000);
		compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit");
		scrollUp();
		
		//Add Interface
		editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
		edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
		//Thread.sleep(1000);
		//Edit_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
		Edit_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
		Report.LogInfo("INFO", "Interface value is displayed as:"+Edit_InterfaceName, "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Edit_InterfaceName);
		
		interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", edit_IVManagement_checkbox);
		
		//scrolltoend();
		generateConfiguration();
		//Thread.sleep(2000);
		verifysuccessmessage("Interface successfully updated.");
	}
	else
	{
		Report.LogInfo("INFO", "Interface is not added", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
	}

}

public void verify_ADSL_L2TP_AddMultilink(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"DeviceName");	
	String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ConfigureInterface_Checkbox");	
	String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Multilink_InterfaceName");	
	String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressRangeIPv4selection");
	String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv4selection");
	String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv4_SubnetSize");
	String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_City");
	String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressIPv4DropdownValue");
	String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewInterfaceAddressRangeIPv4");		
	String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"IVManagement_Checkbox");
	String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"CheckToAddInterface_Checkbox");
	
	
	scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
	//if(getwebelement(xml.getlocator("//locators/" + application + "/existingdevicegrid")).isDisplayed())
	if(isVisible(APT_IPAccessConfigObj.ipa_config.existingdevicegrid))
	{
		//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addeddevices_list"));
		List<WebElement> addeddevicesList= findWebElements(APT_IPAccessConfigObj.ipa_config.addeddevices_list);
		//System.out.println(addeddevicesList);
		int AddedDevicesCount= addeddevicesList.size();
		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			if(AddedDeviceNameText.contains(name))
			{
				//WebElement AddedDevice_ViewLink= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_viewlink").replace("value", AddedDevice_SNo));
				verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
				click(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
				
				waitForAjax();
				//Clickon(AddedDevice_ViewLink);
				//Thread.sleep(5000);
				compareText("View device header", APT_IPAccessConfigObj.ipa_config.viewdevicepage_header, "Device Details");

				scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
				//Thread.sleep(1000);
				compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
				verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
				click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");

				verifyExists(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");
				click(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");

				//Thread.sleep(2000);
				scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", multilink_configureinterface_checkbox);
				scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
				//Thread.sleep(1000);
				verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
				click(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
				//click_commonMethod("OK", "okbutton");
				//Thread.sleep(1000);
				scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

				//Add Multilink
				verifyExists(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"interfacename_textfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,multilink_interfacename);

				compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER");
				
				interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
				//interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
				
				editcheckbox_commonMethod(IVManagement_checkbox, APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management");
				Multilink_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
				Report.LogInfo("INFO", "Interface value is displayed as:"+Multilink_InterfaceName, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Multilink_InterfaceName);
				
				//Multilinked Bearers table
				//scrolltoend();
				compareText("Multilinked Bearers", APT_IPAccessConfigObj.ipa_config.multilinkedbearers_header, "Multilinked Bearers");

				//table columns
				compareText("Check to add Interface", APT_IPAccessConfigObj.ipa_config.checktoaddinterface_column, "Check to add Interface");
				compareText("Interface", APT_IPAccessConfigObj.ipa_config.multilink_interface_column, "Interface");
				compareText("Link/Circuit", APT_IPAccessConfigObj.ipa_config.multilink_link_column, "Link/Circuit");
				compareText("Bearer Type", APT_IPAccessConfigObj.ipa_config.multilink_BearerType_column, "Bearer Type");
				compareText("VLAN Id", APT_IPAccessConfigObj.ipa_config.multilink_vlanid_column, "VLAN Id");
				
				//String MultilinkBearer_ExistingInterface= getwebelement(xml.getlocator("//locators/" + application + "/multilinkbearer_tabledata")).getAttribute("style");
				String MultilinkBearer_ExistingInterface= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilinkbearer_tabledata,"style");
		
				if(!MultilinkBearer_ExistingInterface.contains("height: 1px"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Existing interface details are displaying in multilink bearer table");
					if(checktoaddinterface_checkbox.equalsIgnoreCase("yes"))
					{
						//WebElement CheckToAddInterface= getwebelement(xml.getlocator("//locators/" + application + "/checktoaddinterface").replace("value", InterfaceName));
						verifyExists(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");
						click(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");

						//Clickon(CheckToAddInterface);
					}
				}
				else
				{
					Report.LogInfo("INFO", "No existing interfaces to display", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "No existing interfaces to display");
				}
				//scrolltoend();
				//configuration panel in add interface page
				generateConfiguration();
				
				//Thread.sleep(2000);
				waitforPagetobeenable();
				verifysuccessmessage("Multilink Interface successfully created");
				scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
				//Thread.sleep(1000);
				compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");

				//Multilink table values under interfaces panel
				String Multilink_Name= "Multilink"+Multilink_InterfaceName;
				//String Multilink_RowID= getwebelement(xml.getlocator("//locators/" + application + "/multilink_rowid").replace("value", Multilink_Name)).getAttribute("row-id");
				String Multilink_RowID=getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilink_rowid1+Multilink_Name+APT_IPAccessConfigObj.ipa_config.multilink_rowid2,"row-id");
				
				String Bandwidth_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"Band Width table value");
				Report.LogInfo("INFO", "Bandwidth value is displayed as:"+Bandwidth_value, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Bandwidth value is displayed as:"+Bandwidth_value);
				click(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"BandWidth Table");
				//Clickon(Bandwidth);
				keyPress(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,Keys.TAB);
				//Bandwidth.sendKeys(Keys.TAB);
				
				
				String vlanID_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2," v lan ID table value");
				Report.LogInfo("INFO", "VLAN Id  value is displayed as:"+vlanID_value, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "VLAN Id value is displayed as:"+vlanID_value);
				click(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,"Vlan Table");
				keyPress(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,Keys.TAB);
							
				
				scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
				verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
				click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
			
				//Multilink_InterfaceName= Multilink_Name;
			}
			else
			{
				Report.LogInfo("INFO", "Invalid device name", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
			}
		}
	}
	else
	{
		Report.LogInfo("INFO", "No Device added in grid", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
	}

}

public void verify_ADSL_Ethernet_AddInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"InterfaceName");
	String configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ConfigureInterface_Checkbox");
	String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressRangeIPv4selection");
	String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv4selection");
	String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv4_SubnetSize");
	String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_City");
	String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressIPv4DropdownValue");
	String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressRangeIPv6selection");
	String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv6selection");
	String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv6_SubnetSize");
	String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"LinkValue");
	String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"InterfaceAddressRange_Value");
	String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_AvailableBlocksValue");
	String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewInterfaceAddressRangeIPv6");
	String bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"BearerType_Value");
	String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Encapsulation_Value");		
	String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"IVManagement_Checkbox");
	String vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"VLV_Checkbox");
	String vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"VLV_Converged_Radiobutton");
	String vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"VLV_Standalone_Radiobutton");	
	String vlanID_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"VLANID_Value");
		
	
		scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
		//Thread.sleep(1000);
		compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
		verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
		click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.addinterface_link,"addinterface_link");
		click(APT_IPAccessConfigObj.ipa_config.addinterface_link,"addinterface_link");
		
		//Thread.sleep(2000);
		scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
		//Thread.sleep(1000);
		//GetText("Add Interface header", "addinterface_header");
		scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
		//Thread.sleep(1000);
		verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
		click(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");

		//Thread.sleep(1000);
		scrollDown(APT_IPAccessConfigObj.ipa_config.addinterface_header);
		//verify warning messages in add interface page
		verifyExists(APT_IPAccessConfigObj.ipa_config.interface_warngmsg,"interface_warngmsg");
		verifyExists(APT_IPAccessConfigObj.ipa_config.encapsulation_warngmsg,"encapsulation_warngmsg");

		//Add Interface
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", configureinterface_checkbox);
		edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, interfacename);
		//Thread.sleep(1000);
		
		InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
		Report.LogInfo("INFO", "Interface value is displayed as:"+InterfaceName, "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+InterfaceName);
		compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER");
		
		interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
		interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
		verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield,"link_textfield");
		sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.vlanid_textfield,"vlanid_textfield");
		sendKeys(APT_IPAccessConfigObj.ipa_config.vlanid_textfield,vlanID_value);
		if(bearertype_value.equalsIgnoreCase("Yes")) {
		selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", bearertype_value);
		}
		else
		{
			String BearerType_Value= getTextFrom(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown,"Bearer type drop down");
			Report.LogInfo("INFO", "Bearer type dropdown value is displayed as:"+BearerType_Value, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Bearer type dropdown value is displayed as:"+BearerType_Value);
		}
		//Thread.sleep(2000);
		//GetText("Bandwidth", "interface_bandwidthvalue");
		selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);
		
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", IVManagement_checkbox);
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV", vlv_checkbox);
		if(vlv_checkbox.equalsIgnoreCase("Yes")) {
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", vlv_converged_radiobutton);
			if(vlv_converged_radiobutton.equalsIgnoreCase("yes")) {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
			}
			else {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
			}
			
		}
		
		//scrolltoend();
		generateConfiguration();
		
		//Thread.sleep(5000);
		verifysuccessmessage("Interface added successfully");
		//Thread.sleep(2000);

		//Verify added interface
		scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
		//WebElement AddedInterfaces= getwebelement(xml.getlocator("//locators/" + application + "/addedinterfaces"));
		//String addedinterfacecheck= AddedInterfaces.getAttribute("style");
		String addedinterfacecheck= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.addedinterfaces,"style");
		if(!addedinterfacecheck.contains("height: 1px"))
		{
			compareText("Interfaces", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");				

		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Interfaces added under Interfaces panel");
		}
		scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
		verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"viewpage_backbutton");
		click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"viewpage_backbutton");
		
		//Thread.sleep(2000);

	}
	
public void verify_ADSL_Ethernet_EditInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	

	String edit_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_ConfigureInterface_Checkbox");

	
	String edit_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_InterfaceName");
	String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"existingAddressRangeIPv4selection");
	String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv4selection");
	String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv4_SubnetSize");
	String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_City");
	String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressIPv4DropdownValue");
	String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewInterfaceAddressRangeIPv4");
	String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressRangeIPv6selection");
	String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv6selection");
	String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv6_SubnetSize");
	String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_AvailableBlocksValue");
	String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewInterfaceAddressRangeIPv6");
	String edit_link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_LinkValue");
	String edit_bearertype_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_BearerType_Value");
	String edit_encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_Encapsulation_Value");	
	String edit_IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_IVManagement_Checkbox");
	String edit_vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_VLV_Checkbox");
	String edit_vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_VLV_Converged_Radiobutton");	
	String edit_vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Edit_VLV_Standalone_Radiobutton");	
	
	
	//edit Interface
	scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
	//Thread.sleep(1000);
	//WebElement SelectInterface= getwebelement(xml.getlocator("//locators/" + application + "/selectinterface").replace("value", InterfaceName));
	if(isVisible(APT_IPAccessConfigObj.ipa_config.selectinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.selectinterface2))
	{
		verifyExists(APT_IPAccessConfigObj.ipa_config.selectinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.selectinterface2,"interface");
		click(APT_IPAccessConfigObj.ipa_config.selectinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.selectinterface2,"interface");

		Report.LogInfo("INFO", "Clicked on existing Interface radio button", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on existing Interface radio button");
		//Thread.sleep(1000);
		//WebElement AddedDevice_Interface_Actiondropdown= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_interface_actiondropdown").replace("value", DeviceName));
		verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2,"DeviceName");
		click(APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown1+DeviceName+APT_IPAccessConfigObj.ipa_config.addeddevice_interface_actiondropdown2,"DeviceName");

		verifyExists(APT_IPAccessConfigObj.ipa_config.edit,"edit");
		click(APT_IPAccessConfigObj.ipa_config.edit,"edit");

		//Thread.sleep(2000);
		compareText("Edit Interface/Link", APT_IPAccessConfigObj.ipa_config.editinterface_header, "Edit");
		scrollUp();
		//Add Interface
		editcheckbox_commonMethod(edit_configureinterface_checkbox, APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device");
		edittextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
		//Thread.sleep(1000);
		//Edit_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
		//ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Edit_InterfaceName);
		
		interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
		interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
		
		scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
		edittextFields_commonMethod("Link", APT_IPAccessConfigObj.ipa_config.link_textfield, edit_link_value);
		if(edit_bearertype_value.equalsIgnoreCase("Yes")) {
		selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown, "Bearer Type", edit_bearertype_value);
		}else
		{
			String BearerType_Value= getTextFrom(APT_IPAccessConfigObj.ipa_config.bearertype_dropdown,"Bearer type dropdown");
			Report.LogInfo("INFO", "Bearer type dropdown value is displayed as:"+BearerType_Value, "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Bearer type dropdown value is displayed as:"+BearerType_Value);
		}
		//Thread.sleep(2000);
		//GetText("Bandwidth", "interface_bandwidthvalue");
		selectValueInsideDropdown("encapsulation_dropdown", "Encapsulation", edit_encapsulation_value);
		
		verifyExists(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"interfacename_textfield");
		sendKeys(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,edit_interfacename);
		//addtextFields_commonMethod("Interface", APT_IPAccessConfigObj.ipa_config.interfacename_textfield, edit_interfacename);
		Edit_InterfaceName= edit_interfacename;
		
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management", edit_IVManagement_checkbox);
		addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV", edit_vlv_checkbox);
		if(edit_vlv_checkbox.equalsIgnoreCase("Yes")) {
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", edit_vlv_converged_radiobutton);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", edit_vlv_standalone_radiobutton);
		}
		
		//scrolltoend();
		generateConfiguration();
		//Thread.sleep(2000);
		verifysuccessmessage("Interface successfully updated.");
	}
	else
	{
		Report.LogInfo("INFO", "Interface is not added", "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Interface is not added");
	}

}

public void verify_ADSL_Ethernet_AddMultilink(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"DeviceName");	
	String multilink_configureinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ConfigureInterface_Checkbox");	
	String multilink_interfacename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Multilink_InterfaceName");	
	String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressRangeIPv4selection");
	String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv4selection");
	String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv4_SubnetSize");
	String eipallocation_city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_City");
	String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressIPv4DropdownValue");
	String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewInterfaceAddressRangeIPv4");
	String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewInterfaceAddressRangeIPv6");
	String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_AvailableBlocksValue");
	String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"EIPAllocation_IPv6_SubnetSize");
	String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"NewAddressRangeIpv6selection");
	String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"ExistingAddressRangeIPv6selection");
	String link_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"LinkValue");
	String encapsulation_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"Encapsulation_Value");
	String IVManagement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"IVManagement_Checkbox");
	String vlv_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"VLV_Checkbox");
	String vlv_converged_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"VLV_Converged_Radiobutton");
	String vlv_standalone_radiobutton = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"VLV_Standalone_Radiobutton");
	String checktoaddinterface_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
			"CheckToAddInterface_Checkbox");
	
	scrollDown(APT_IPAccessConfigObj.ipa_config.providerequipment_header);
	//if(getwebelement(xml.getlocator("//locators/" + application + "/existingdevicegrid")).isDisplayed())
	if(isVisible(APT_IPAccessConfigObj.ipa_config.existingdevicegrid))
	{
		//List<WebElement> addeddevicesList= getwebelements(xml.getlocator("//locators/" + application + "/addeddevices_list"));
		List<WebElement> addeddevicesList= findWebElements(APT_IPAccessConfigObj.ipa_config.addeddevices_list);
		//System.out.println(addeddevicesList);
		int AddedDevicesCount= addeddevicesList.size();
		for(int i=0;i<AddedDevicesCount;i++) {
			String AddedDeviceNameText= addeddevicesList.get(i).getText();
			String AddedDevice_SNo= AddedDeviceNameText.substring(0, 1);
			if(AddedDeviceNameText.contains(name))
			{
				//WebElement AddedDevice_ViewLink= getwebelement(xml.getlocator("//locators/" + application + "/addeddevice_viewlink").replace("value", AddedDevice_SNo));
				verifyExists(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
				click(APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1+AddedDevice_SNo+APT_IPAccessConfigObj.ipa_config.addeddevice_viewlink1, "Add device view link");
				
				waitForAjax();
				//Clickon(AddedDevice_ViewLink);
				//Thread.sleep(5000);
				compareText("View device header", APT_IPAccessConfigObj.ipa_config.viewdevicepage_header, "Device Details");

				scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
				//Thread.sleep(1000);
				compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");
				verifyExists(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");
				click(APT_IPAccessConfigObj.ipa_config.interfacepanel_actiondropdown,"interfacepanel_actiondropdown");

				verifyExists(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");
				click(APT_IPAccessConfigObj.ipa_config.addmultilink_link,"addmultilink_link");

				//Thread.sleep(2000);
				scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.configureinterface_checkbox, "Configure Interface on Device", multilink_configureinterface_checkbox);
				scrollDown(APT_IPAccessConfigObj.ipa_config.okbutton);
				//Thread.sleep(1000);
				verifyExists(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
				click(APT_IPAccessConfigObj.ipa_config.okbutton,"okbutton");
				//click_commonMethod("OK", "okbutton");
				//Thread.sleep(1000);
				scrollDown(APT_IPAccessConfigObj.ipa_config.addmultilink_header);

				//Add Multilink
				verifyExists(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"interfacename_textfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,multilink_interfacename);

				compareText("Network", APT_IPAccessConfigObj.ipa_config.network_fieldvalue, "XFER");
				
				interfaceAddressRangeIPv4(existingAddressRangeIPv4selection, newAddressRangeIpv4selection, subnetSizeValue_IPv4, eipallocation_city, existingAddressIPv4DropdownValue, newinterfaceAddressrange);
				interfaceAddressRangeIPv6(existingAddressRangeIPv6selection, newAddressRangeIpv6selection, subnetSizeValue_IPv6, availableBlocksValue_IPv6, newinterfaceAddressrangeIPv6);
				
				scrollDown(APT_IPAccessConfigObj.ipa_config.link_textfield);
				verifyExists(APT_IPAccessConfigObj.ipa_config.link_textfield,"link_textfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.link_textfield,link_value);

				selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.encapsulation_dropdown, "Encapsulation", encapsulation_value);

				editcheckbox_commonMethod(IVManagement_checkbox, APT_IPAccessConfigObj.ipa_config.ivmanagement_checkbox, "IV Management");
				editcheckbox_commonMethod(vlv_checkbox, APT_IPAccessConfigObj.ipa_config.vlvcheckbox, "VLV");
				if(vlv_checkbox.equalsIgnoreCase("Yes")) {
					addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.converged_radiobutton, "Converged", vlv_converged_radiobutton);
					if(vlv_converged_radiobutton.equalsIgnoreCase("yes")) {
						addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
					}
					else {
						addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.standalone_radiobutton, "Standalone", vlv_standalone_radiobutton);
					}
				}
				
				//Multilink_InterfaceName= getwebelement(xml.getlocator("//locators/" + application + "/interfacename_textfield")).getAttribute("value");
				Multilink_InterfaceName= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.interfacename_textfield,"value");
				Report.LogInfo("INFO", "Interface value is displayed as:"+Multilink_InterfaceName, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Interface value is displayed as:"+Multilink_InterfaceName);
				
				//Multilinked Bearers table
				//scrolltoend();
				compareText("Multilinked Bearers", APT_IPAccessConfigObj.ipa_config.multilinkedbearers_header, "Multilinked Bearers");

				//table columns
				compareText("Check to add Interface", APT_IPAccessConfigObj.ipa_config.checktoaddinterface_column, "Check to add Interface");
				compareText("Interface", APT_IPAccessConfigObj.ipa_config.multilink_interface_column, "Interface");
				compareText("Link/Circuit", APT_IPAccessConfigObj.ipa_config.multilink_link_column, "Link/Circuit");
				compareText("Bearer Type", APT_IPAccessConfigObj.ipa_config.multilink_BearerType_column, "Bearer Type");
				compareText("VLAN Id", APT_IPAccessConfigObj.ipa_config.multilink_vlanid_column, "VLAN Id");
				
				//String MultilinkBearer_ExistingInterface= getwebelement(xml.getlocator("//locators/" + application + "/multilinkbearer_tabledata")).getAttribute("style");
				String MultilinkBearer_ExistingInterface= getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilinkbearer_tabledata,"style");
		
				if(!MultilinkBearer_ExistingInterface.contains("height: 1px"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Existing interface details are displaying in multilink bearer table");
					if(checktoaddinterface_checkbox.equalsIgnoreCase("yes"))
					{
						//WebElement CheckToAddInterface= getwebelement(xml.getlocator("//locators/" + application + "/checktoaddinterface").replace("value", InterfaceName));
						verifyExists(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");
						click(APT_IPAccessConfigObj.ipa_config.checktoaddinterface1+InterfaceName+APT_IPAccessConfigObj.ipa_config.checktoaddinterface2,"Add intrface");

						//Clickon(CheckToAddInterface);
					}
				}
				else
				{
					Report.LogInfo("INFO", "No existing interfaces to display", "PASS");
					ExtentTestManager.getTest().log(LogStatus.PASS, "No existing interfaces to display");
				}
				//scrolltoend();
				//configuration panel in add interface page
				generateConfiguration();
				
				//Thread.sleep(2000);
				waitforPagetobeenable();
				verifysuccessmessage("Multilink Interface successfully created");
				scrollDown(APT_IPAccessConfigObj.ipa_config.interfaces_header);
				//Thread.sleep(1000);
				compareText("Interfaces header", APT_IPAccessConfigObj.ipa_config.interfaces_header, "Interfaces");

				//Multilink table values under interfaces panel
				String Multilink_Name= "Multilink"+Multilink_InterfaceName;
				//String Multilink_RowID= getwebelement(xml.getlocator("//locators/" + application + "/multilink_rowid").replace("value", Multilink_Name)).getAttribute("row-id");
				String Multilink_RowID=getAttributeFrom(APT_IPAccessConfigObj.ipa_config.multilink_rowid1+Multilink_Name+APT_IPAccessConfigObj.ipa_config.multilink_rowid2,"row-id");
				
				String Bandwidth_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"Band Width table value");
				Report.LogInfo("INFO", "Bandwidth value is displayed as:"+Bandwidth_value, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Bandwidth value is displayed as:"+Bandwidth_value);
				click(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,"BandWidth Table");
				//Clickon(Bandwidth);
				keyPress(APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.bandwidth_tablevalue2,Keys.TAB);
				//Bandwidth.sendKeys(Keys.TAB);
				
				
				String vlanID_value = getTextFrom(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,"vlan ID table value");
				Report.LogInfo("INFO", "VLAN Id  value is displayed as:"+vlanID_value, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "VLAN Id value is displayed as:"+vlanID_value);
				click(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,"Vlan Table");
				keyPress(APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue1+Multilink_RowID+APT_IPAccessConfigObj.ipa_config.vlanid_tablevalue2,Keys.TAB);
							
				
				scrollDown(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton);
				verifyExists(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
				click(APT_IPAccessConfigObj.ipa_config.viewpage_backbutton,"Back Button");
			
				//Multilink_InterfaceName= Multilink_Name;
			}
			else
			{
				Report.LogInfo("INFO", "Invalid device name", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
			}
		}
	}
	else
	{
		Report.LogInfo("INFO", "No Device added in grid", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
	}

}

public void verifyservicecreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException  {

	String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"ServiceIdentification");	
	String orderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"NewOrderNumber");	
	String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"ServiceType");	
	String networkConfig_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"NetworkConfiguration");	
	String billingtypevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BillingType");	
	String terminationdate = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"TerminationDate");	
	String phonecontact = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PhoneContact");	
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Email");	
	String CPEWANTechnology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPEWANTechnology_DropdownValue");	
	String PEWANTechnology_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PEWANTechnology_DropdownValue");	
	String DSLProvider_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"DSLProvider_DropdownValue");	
	String PPPBased_checkboxvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"PPPBased_CheckboxValue");	
	String VOIP_checkboxvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"VOIP_CheckboxValue");	
	String downstream_dropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Downstream_DropdownValue");	
	

	// verify warning messages
	waitforPagetobeenable();
	scrollDown(APT_IPAccessConfigObj.ipa_config.nextbutton);
	click(APT_IPAccessConfigObj.ipa_config.nextbutton,"nextbutton");
	//Thread.sleep(1000);
	scrollIntoTop();
	verifyExists(APT_IPAccessConfigObj.ipa_config.sidwarngmsg, "Service Identification");
	verifyExists(APT_IPAccessConfigObj.ipa_config.billingtype_warngmsg, "Billing Type");
	verifyExists(APT_IPAccessConfigObj.ipa_config.CPEWANTechnology_warngmsg, "CPE WAN Technology");
	
	//Create service
	//addtextFields_commonMethod("Service Identification", "serviceidentificationtextfield", sid);
	verifyExists(APT_IPAccessConfigObj.ipa_config.serviceidentificationtextfield, "serviceidentificationtextfield");
	sendKeys(APT_IPAccessConfigObj.ipa_config.serviceidentificationtextfield,sid);
	
	compareText("Service Type", APT_IPAccessConfigObj.ipa_config.servicetypevalue, servicetype);
	compareText("Network Configuration", APT_IPAccessConfigObj.ipa_config.networkconfig_value, networkConfig_dropdownvalue);
	
	selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.billingtype_dropdown, "Billing Type", billingtypevalue);
	
	verifyExists(APT_IPAccessConfigObj.ipa_config.terminationdate_field, "terminationdate_field");
	sendKeys(APT_IPAccessConfigObj.ipa_config.terminationdate_field,terminationdate);
	
	verifyExists(APT_IPAccessConfigObj.ipa_config.emailtextfieldvalue, "emailtextfieldvalue");
	sendKeys(APT_IPAccessConfigObj.ipa_config.emailtextfieldvalue,email);
	
	verifyExists(APT_IPAccessConfigObj.ipa_config.phonecontacttextfieldvalue, "phonecontacttextfieldvalue");
	sendKeys(APT_IPAccessConfigObj.ipa_config.phonecontacttextfieldvalue,phonecontact);

	scrollDown(APT_IPAccessConfigObj.ipa_config.emailtextfieldvalue);
	
	selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.cpewantechnology_Dropdown, "CPE WAN Technology", CPEWANTechnology_dropdownvalue);
	
	if(CPEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet")) {
		//Thread.sleep(1000);
		selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.pewantechnology_dropdown, "PE WAN Technology Type", PEWANTechnology_dropdownvalue);
		if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("ATM"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.pppbased_checkbox, "PPP Based", PPPBased_checkboxvalue);
		}
		else if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet-L2TP"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.VOIP_checkbox, "VOIP", VOIP_checkboxvalue);
		}
	}
	else if(CPEWANTechnology_dropdownvalue.equalsIgnoreCase("ADSL")) {
		selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.pewantechnology_dropdown, "PE WAN Technology Type", PEWANTechnology_dropdownvalue);
		if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("ATM"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.downstream_dropdown, "Downstream", downstream_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.pppbased_checkbox, "PPP Based", PPPBased_checkboxvalue);
			if(PPPBased_checkboxvalue.equalsIgnoreCase("yes")) {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.VOIP_checkbox, "VOIP", VOIP_checkboxvalue);
			}
		}
		else if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("L2TP"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.downstream_dropdown, "Downstream", downstream_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.VOIP_checkbox, "VOIP", VOIP_checkboxvalue);
		}
		else if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.downstream_dropdown, "Downstream", downstream_dropdownvalue);
		}
	}
	else if(CPEWANTechnology_dropdownvalue.equalsIgnoreCase("SDSL")) {
		selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.pewantechnology_dropdown, "PE WAN Technology Type", PEWANTechnology_dropdownvalue);
		if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("ATM"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.pppbased_checkbox, "PPP Based", PPPBased_checkboxvalue);
			if(PPPBased_checkboxvalue.equalsIgnoreCase("yes")) {
				addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.VOIP_checkbox, "VOIP", VOIP_checkboxvalue);
			}
		}
		else if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("L2TP"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
			addCheckbox_commonMethod(APT_IPAccessConfigObj.ipa_config.VOIP_checkbox, "VOIP", VOIP_checkboxvalue);
		}
		else if(PEWANTechnology_dropdownvalue.equalsIgnoreCase("Ethernet"))
		{
			selectValueInsideDropdown(APT_IPAccessConfigObj.ipa_config.dslprovider_dropdown, "DSL Provider", DSLProvider_dropdownvalue);
		}
	}
	
	scrollDown(APT_IPAccessConfigObj.ipa_config.nextbutton);
	click(APT_IPAccessConfigObj.ipa_config.nextbutton,"Next Button");
	//Thread.sleep(2000);
	verifysuccessmessage("Service successfully created");
	
}

public void verifyAddCPEDeviceFunction(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {

	String CPE_DeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_DeviceName");
	String CPE_VendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
			"CPE_VendorModel");
	String CPE_Snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Snmpro");
	String CPE_Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Country");
	String CPE_City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_City");
	String CPE_Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Site");
	String CPE_RouterID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_RouterID");

	// ScrolltoElement(application,
	// "CustomerPremiseEquipment_CPE_Panelheader",
	// xml);//CustomerPremiseEquipment_CPE_Panelheader
	scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerPremiseEquipment_CPE_Panelheader);
	if (isVisible(
			APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CustomerPremiseEquipment_CPE_Panelheader)) {
		verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPEDeviceLink,
				"Add CPE Device Link");
		click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPEDeviceLink, "Add CPE Device Link");
		if (isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPEDevicePageHeader)) {
			Report.LogInfo("INFO", "'Add CPE Device' Page navigated as expected", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Add CPE Device' Page navigated as expected");

			scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton);
			verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton, "Next Button");
			click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton, "Next Button");

			try {
				if(isVisible(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_WarningMessage_Country)){
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_WarningMessage_Country,
						"Warning Country");
				}
				verifyExists(
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_WarningMessage_VendorModel,
						"Warning Vendor");

				//clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_DeviceNameTextfield);
				//sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_DeviceNameTextfield,
						//CPE_DeviceName);
				verifyExists(
						APT_IPAccessConfigObj.ipa_config.CPE_RouterIdTextfield,"CPE_RouterIdTextfield");
				sendKeys(APT_IPAccessConfigObj.ipa_config.CPE_RouterIdTextfield, CPE_RouterID);

				
				clearTextBox(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SnmproTextfield);
				sendKeys(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SnmproTextfield, CPE_Snmpro,
						"Snmpro");

				// ClearAndEnterTextValue(application, "Snmpro",
				// "CPE_SnmproTextfield", CPE_Snmpro, xml);

				SelectDropdownValueUnderSelectTag("Country", CPE_Country,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_CountryDropdown);
				SelectDropdownValueUnderSelectTag("City", CPE_City,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_CityDropdown);
				scrollDown(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SiteDropdown);
				SelectDropdownValueUnderSelectTag("Site", CPE_Site,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_SiteDropdown);
				// **SelectDropdownValueUnderSelectTag(application,
				// "Premise", CPE_Premise, "CPE_PremiseDropdown", xml);
				
				SelectDropdownValueUnderSelectTag("Vendor/Model", CPE_VendorModel,
						APT_IPAccessResilientConfigObj.IPAccessResilientConfig.CPE_VendorModelDropdown);
				
				
				verifyExists(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton,
						"Next Button");
				click(APT_IPAccessResilientConfigObj.IPAccessResilientConfig.AddCPE_NextButton, "Next Button");

				waitForAjax();
				waitforPagetobeenable();
				// verifysuccessmessage(application, "CPE Device added
				// successfully");
				verifysuccessmessage("Device successfully created.");

			} catch (NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("INFO", "Field is not displayed in Add CPE Device page", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						e + " : Field is not displayed in Add CPE Device page");
				// System.out.println( e+ " : Field is not displayed in Add
				// CPE Device page");
			} catch (Exception e) {
				e.printStackTrace();
				Report.LogInfo("INFO", "Field is not displayed in Add CPE Device page", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						e + " : Field is not displayed in Add CPE Device page ");
			}

		} else {
			Report.LogInfo("INFO", "'Add CPE Device' page not navigated", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add CPE Device' page not navigated");
		}
	} else {
		Report.LogInfo("INFO", "'Customer Premise Equipment (CPE)' panel not displayed", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Customer Premise Equipment (CPE)' panel not displayed");
	}
}

public void navigateToViewCEPDevicePage(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String CPEDeviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_RouterID");

	if (isVisible(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2)) {
		scrollDown(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2, "View Link");
		webDriver.findElement(By.xpath("//td[contains(.,'" + CPEDeviceName + "')]//span//a[text()='View']"))
				.click();

		waitforPagetobeenable();
		waitForAjax();
		// waitForAjax();

		if (isVisible(APTIPAccessNoCPE.APTNoCPE.viewdevicepage_header)) {
			Report.LogInfo("INFO", "View PE Device' page navigated as expected", "PASS");
			Reporter.log("View PE Device' page navigated as expected");
		} else {
			Report.LogInfo("INFO", "View PE Device' page not navigated", "PASS");
			Reporter.log("View PE Device' page not navigated");
		}
	} else {
		Report.LogInfo("INFO", "No Device added in grid", "PASS");
		Reporter.log("No Device added in grid");
	}

}

public void deleteCEPDevicePage(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException, AWTException {
	String CPEDeviceName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPE_RouterID");

	if (isVisible(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2)) {
		scrollDown(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.viewservicepage_viewdevicelink2, "View Link");
		webDriver.findElement(By.xpath("//td[contains(.,'" + CPEDeviceName + "')]//span//a[text()='Delete']"))
				.click();
		
		if(isAlertPresent()==true){
			Robot r = new Robot();
			Thread.sleep(1000);
			r.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
		}
		
		waitforPagetobeenable();
		verifysuccessmessage("Device successfully deleted.");
		
	} else {
		Report.LogInfo("INFO", "No Device added in grid", "PASS");
		Reporter.log("No Device added in grid");
	}

}

}
