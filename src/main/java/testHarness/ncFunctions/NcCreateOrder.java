package testHarness.ncFunctions;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.DocumentFormatException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

//import baseClasses.PropertyReader;
import baseClasses.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class NcCreateOrder extends SeleniumUtils 
{
	public static ThreadLocal<String> Orderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> NetworkReferenceIPVPN = new ThreadLocal<>();
	public static ThreadLocal<String> Circuitreferencenumber = new ThreadLocal<>();
	
	GlobalVariables g = new GlobalVariables();
	ReusableFunctions Reusable = new ReusableFunctions();
	EthernetOrder Ethernet = new EthernetOrder();
	HubOrder HubAndSpoke = new HubOrder();
	IpVpnOrder IpVpnOrder = new IpVpnOrder();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();

	public void NcNewOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		String BgpFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BGP Feature");
		String DhcpFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Feature");
		String SNMPFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Feature");
		String NATFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT Feature");
		String StaticFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Static Route");
		
		CreateOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		AddProduct(testDataFile, sheetName, scriptNo, dataSetNo);
		if (ProductName.toString().contains("Line"))
		{
			Ethernet.AddProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.AddFeatureDetails();
			DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.ProductDeviceDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.GotoErrors(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			HubAndSpoke.AddHubProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.DecomposeHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.HubProductDeviceDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.CompleteHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.CreateSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.AddSpokeProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.AddSpokeProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.AddSpokeFeature(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.DecomposeSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.AddHub(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.SpokeProductDeviceDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.ProcessSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.CompleteSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("IPA"))
		{
			AddProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			IpAddressingFeatureUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			if (AddressingType.toString().contains("Provider Aggregated"))
			{
				AddProviderAggregateFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				ProviderAggregateUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			else if (AddressingType.toString().contains("Provider Independent"))
			{
				AddProviderIndependentFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				ProviderIndependentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			else if (AddressingType.toString().contains("PA and PI"))
			{
				AddProviderAggregateFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				ProviderAggregateUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				AddProviderIndependentFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				ProviderIndependentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			if (BgpFeature.toString().contains("Yes"))
			{
				AddBgpFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				BgpFeedUpdate(testDataFile, sheetName, scriptNo, dataSetNo);			
			}
			if (DhcpFeature.toString().contains("Yes"))
			{
				AddDhcpFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				DhcpUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				IpDhcpUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			if (SNMPFeature.toString().contains("Yes"))
			{
				AddIpSnmpFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				IpSnmpUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				AddSnmpDeviceFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				SnmpDeviceUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				AddSnmpReadAccess(testDataFile, sheetName, scriptNo, dataSetNo);
				SnmpReadAccessUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			if (NATFeature.toString().contains("Yes"))
			{
				AddNATFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				NatUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				NatUpdateStaticandDynamic(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			if (StaticFeature.toString().contains("Yes"))
			{
				AddStaticRouteFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				StaticRouteUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			DecomposeIpOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			EthernetProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			EndSiteProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
        else if(ProductName.equalsIgnoreCase("IPVPN"))
        {
      	  //IpVpnOrder.CreateEthernetLink(testDataFile, sheetName, scriptNo, dataSetNo);
      	  IpVpnOrder.IpvpnCreateAndProcess(testDataFile, sheetName, scriptNo, dataSetNo);
        }

	}
	
	public void NcMigrationOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		String BgpFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BGP Feature");
		String DhcpFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Feature");
		String SNMPFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Feature");
		String NATFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT Feature");
		String StaticFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Static Route");
		
		if (ProductName.toString().contains("Line"))
		{
			Ethernet.CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			Ethernet.GotoErrors(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		
		else if (ProductName.toString().contains("IPA"))
		{
			CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			HubAndSpoke.CompleteHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			HubAndSpoke.CompleteSpokeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
	}
	
	
	public void CreateOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
				
		waitForElementToAppear(NcOrderObj.createOrder.NewCompositeOrder, 10);
		verifyExists(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
		click(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
		Reusable.waitForAjax();
		
		String Ordernumber = getTextFrom(NcOrderObj.createOrder.OrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No", Ordernumber);
		Reusable.waitForAjax();
		String[] arrOfStr = Ordernumber.split("#", 0);
//		Log.info(arrOfStr[1]);
		
		if (ProductName.toString().contains("Line"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Line order created using CTAF Automation");
		}
		else if (ProductName.toString().contains("Hub"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Hub order created using CTAF Automation");
		}
		else if (ProductName.toString().contains("Spoke"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Spoke order created using CTAF Automation");
		}
		else if (ProductName.toString().contains("IPA"))
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "IP Access order created using CTAF Automation");
		}
		else
		{
			sendKeys(NcOrderObj.createOrder.OrderDescription, "IPVPN order created using CTAF Automation");
		}
		verifyExists(NcOrderObj.createOrder.Update,"Update");
		click(NcOrderObj.createOrder.Update,"Update");
//		Orderscreenurl.set(getAttributeFrom(NcOrderObj.createOrder.LinkforOrder,"href"));
		String Orderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL", Orderscreenurl);
		System.out.println(Orderscreenurl);
		Reusable.waitForAjax();
	}
	
	public void AddProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		
		verifyExists(NcOrderObj.addProduct.OrderTab,"Orders Tab");
		click(NcOrderObj.addProduct.OrderTab,"Orders Tab");

		verifyExists(NcOrderObj.addProduct.AddonOrderTab,"Add");
		click(NcOrderObj.addProduct.AddonOrderTab,"Add");
		Reusable.waitForAjax();
		
		if (ProductName.toString().contains("Line"))
		{
			verifyExists(NcOrderObj.addProduct.EthernetProductCheckBox,"Ethernet Connection Product");
			click(NcOrderObj.addProduct.EthernetProductCheckBox,"Ethernet Connection Product");
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			verifyExists(NcOrderObj.addProduct.HubProductCheckBox,"Ethernet Hub Product");
			click(NcOrderObj.addProduct.HubProductCheckBox,"Ethernet Hub Product");
		}
		else if (ProductName.toString().contains("IPA"))
		{
			verifyExists(NcOrderObj.addProduct.IpAccessCheckBox,"IP Access Product");
			click(NcOrderObj.addProduct.IpAccessCheckBox,"IP Access Product");
		}
		else
		{
			verifyExists(NcOrderObj.addProduct.IpvpnserviceCheckBox,"IP VPN Service Product");
			click(NcOrderObj.addProduct.IpvpnserviceCheckBox,"IP VPN Service Product");
		}
		verifyExists(NcOrderObj.addProduct.Addbutton,"Add");
		click(NcOrderObj.addProduct.Addbutton,"Add");
		Reusable.waitForAjax();
	}
	
	public void AddProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		String OrderRefNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Sys Ref ID");
		String OrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
		String CommercialProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Commercial Product Name");
		String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
		String RouterTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Technology");
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
		String Layer3Resilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String TypeofBilling = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Typeof_Billing");
		String RelatedVoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Related_Voip_Service");
		String BespokeReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Bespoke_Reference");
		
//		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
//		getUrl(Orderscreenurl);
		
		if (ProductName.toString().contains("IPA"))
		{
			verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Product Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Product Order Link");
			Reusable.waitForAjax();
		}
		else if (ProductName.toString().contains("Line") || (ProductName.toString().contains("Spoke")))
		{
			verifyExists(NcOrderObj.addProductDetails.UnderlyingEthernetOrder,"Ethernet Connection Product Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingEthernetOrder,"Ethernet Connection Product Order Link");
			Reusable.waitForAjax();
		}
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.OrderSystemName,"Order System Name");
		click(NcOrderObj.addProductDetails.OrderSystemName,"Order System Name");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NcOrderObj.addProductDetails.OrderSystemName,"SPARK","Order System Name");
//		click(NcOrderObj.addProductDetails.OrderSystemName,"Order System Name");
		
		verifyExists(NcOrderObj.addProductDetails.Orderreferencenumber,"Order Reference Number");
		sendKeys(NcOrderObj.addProductDetails.Orderreferencenumber,OrderRefNumber);

		verifyExists(NcOrderObj.addProductDetails.Ordernumber,"Order Number");
		sendKeys(NcOrderObj.addProductDetails.Ordernumber,OrderNumber);

		verifyExists(NcOrderObj.addProductDetails.CommercialProductName,"Commercial Product Name");
		sendKeys(NcOrderObj.addProductDetails.CommercialProductName,CommercialProductName);

		if (ProductName.toString().contains("IPA"))
		{
			verifyExists(NcOrderObj.addProductDetails.RouterType,"Router Type");
			click(NcOrderObj.addProductDetails.RouterType,"Router Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addProductDetails.RouterType,RouterType,"Router Type");

			verifyExists(NcOrderObj.addProductDetails.RouterTechnology,"Router Technology");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addProductDetails.RouterTechnology,RouterTechnology,"Router Technology");

			verifyExists(NcOrderObj.addProductDetails.ServiceBandwidth,"Service Bandwidth");
			selectByVisibleText(NcOrderObj.addProductDetails.ServiceBandwidth,ServiceBandwidth,"Service Bandwidth");
//			Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBandwidth, Keys.ENTER);
			
			verifyExists(NcOrderObj.addProductDetails.Layer3Resilience,"Layer 3 Resilience");
//			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addProductDetails.Layer3Resilience,Layer3Resilience,"Layer 3 Resilience");

			verifyExists(NcOrderObj.addProductDetails.TypeOfBilling,"Type of Billing");
//			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addProductDetails.TypeOfBilling,TypeofBilling,"Type of Billing");
			if (RelatedVoipService.toString().contains("IPA"))
			{
				verifyExists(NcOrderObj.addProductDetails.RelatedVoipService,"Related Voip Service");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(NcOrderObj.addProductDetails.RelatedVoipService,RelatedVoipService,"Related Voip Service");
			}
			else
			{
				if (BespokeReference.toString().equals(null))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Bespoke not required");
				}
				else
				{
					verifyExists(NcOrderObj.addProductDetails.BespokeReference,"Bespoke Reference");
					click(NcOrderObj.addProductDetails.BespokeReference,"Bespoke Reference");
					Reusable.waitForSiebelLoader();
					sendKeys(NcOrderObj.addProductDetails.BespokeReference,BespokeReference,"Bespoke Reference");
				}
			}
		}
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update product details");
		
		Reusable.waitForAjax();
	}
	
	public void IpAddressingFeatureUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String IpAddressingFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP Addressing Format");
		String AddressingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4/IPv6 Addressing Type");
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		
		
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.addIpAddressing.IpAddressingLink,"IP Addressing Link");
		click(NcOrderObj.addIpAddressing.IpAddressingLink,"IP Addressing Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.addIpAddressing.IpAddressingFormat,"IP Addressing Format");
		click(NcOrderObj.addIpAddressing.IpAddressingFormat,"IP Addressing Format");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NcOrderObj.addIpAddressing.IpAddressingFormat,IpAddressingFormat,"IP Addressing Format");
		if (IpFormat.toString().contains("IPv4"))
		{
			verifyExists(NcOrderObj.addIpAddressing.Ipv4AddressingType,"IPv4 Addressing Type");
			click(NcOrderObj.addIpAddressing.Ipv4AddressingType,"IPv4 Addressing Type");
//			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv4AddressingType,AddressingType,"IPv4 Addressing Type");
		}
		else if (IpFormat.toString().contains("IPv6"))
		{
			verifyExists(NcOrderObj.addIpAddressing.Ipv6AddressingType,"IPv6 Addressing Type");
//			click(NcOrderObj.addIpAddressing.Ipv6AddressingType,"IPv6 Addressing Type");
//			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv6AddressingType,AddressingType,"IPv6 Addressing Type");
		}
		else
		{
			verifyExists(NcOrderObj.addIpAddressing.Ipv4AddressingType,"IPv4 Addressing Type");
//			click(NcOrderObj.addIpAddressing.Ipv4AddressingType,"IPv4 Addressing Type");
//			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv4AddressingType,AddressingType,"IPv4 Addressing Type");

			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addIpAddressing.Ipv6AddressingType,"IPv6 Addressing Type");
//			click(NcOrderObj.addIpAddressing.Ipv6AddressingType,"IPv6 Addressing Type");
//			Reusable.waitForSiebelLoader();
			selectByVisibleText(NcOrderObj.addIpAddressing.Ipv6AddressingType,AddressingType,"IPv6 Addressing Type");
		}
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
	}
	
	public void AddProviderAggregateFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String TypeofFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PA/PI Feature");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		if (IpFormat.toString().trim().equalsIgnoreCase("IPv4") || IpFormat.toString().trim().equalsIgnoreCase("IPv6"))
		{
			getUrl(Orderscreenurl);
			verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,TypeofFeature);
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//			switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		    click(("@xpath=//span[text()='"+TypeofFeature+"']/parent::*/parent::*/parent::*/td//input"), "Provider Aggregated feature");
		    Reusable.waitForAjax();
		     
		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		    Reusable.waitForAjax();
		}
		else
        {
			getUrl(Orderscreenurl);
			verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();
			
            verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
            click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
            Reusable.waitForAjax();
            verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
            click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
            sendKeys(NcOrderObj.generalInformation.TypeofFeature,"Provider Aggregated IPv4");
            Reusable.waitForAjax();

            String handleBefore = webDriver.getWindowHandle();

            verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
            click(NcOrderObj.generalInformation.Browse,"Browse button");
            Reusable.waitForAjax();

            Set<String> handles = webDriver.getWindowHandles();
            Iterator<String> iterator = handles.iterator();
            iterator.next();
            String curent=iterator.next();
            
 //           switchWindow(urlContent, handles);
            
            System.out.println("Window handel"+curent);
            webDriver.switchTo().window(curent);

            // Perform the actions on new window
            webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
            verifyExists("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input","Provider Aggregated IPv4");
            //ScrollIntoViewByString("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input");
            click("@xpath=//span[text()='Provider Aggregated IPv4']/parent::*/parent::*/parent::*/td//input", "Provider Aggregated IPv4");
            Reusable.waitForAjax();
            
            verifyExists("@xpath=//span[text()='Provider Aggregated IPv6']/parent::*/parent::*/parent::*/td//input","Provider Aggregated IPv6");
            ScrollIntoViewByString("@xpath=//span[text()='Provider Aggregated IPv6']/parent::*/parent::*/parent::*/td//input");
            click("@xpath=//span[text()='Provider Aggregated IPv6']/parent::*/parent::*/parent::*/td//input", "Provider Aggregated IPv6");
            Reusable.waitForAjax();

            webDriver.switchTo().window(curent);
            webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

            verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
            click(NcOrderObj.generalInformation.SelectButton,"Select button");
            Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
            Reusable.waitForAjax();

            // switch back to parent window
            //switchToDefaultFrame();
            //verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
            webDriver.switchTo().window(handleBefore);
            Reusable.waitForAjax();
        }
	}
	
	public void ProviderAggregateUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String NoIpv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv4 Addresses");
		String NoIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv6 Addresses");
		String PrimaryRangeReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary Range");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (IpFormat.toString().trim().equalsIgnoreCase("IPv4"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,"Number of IPv4 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,NoIpv4Address,"NoIpv4Address");

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else if (IpFormat.toString().trim().equalsIgnoreCase("IPv6"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv4Link,"Provide Aggregated IPv4 link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,"Number of IPv4 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv4Addresses,NoIpv4Address,"NoIpv4Address");

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderAggIpv6Link,"Provide Aggregated IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
	}
	
	public void AddProviderIndependentFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
		String TypeofFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PA/PI Feature");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		if (IpFormat.toString().trim().equalsIgnoreCase("IPv4") || IpFormat.toString().trim().equalsIgnoreCase("IPv6"))
		{
			getUrl(Orderscreenurl);
			verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,TypeofFeature);
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//			switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
//		    verifyExists("@xpath=//span[text()='Provider Independent IPv4']/parent::*/parent::*/parent::*/td//input","Provider Independent IPv4");
//		    ScrollIntoViewByString("@xpath=//span[text()='Provider Independent IPv4']/parent::*/parent::*/parent::*/td//input");
		    click(("@xpath=//span[text()='"+TypeofFeature+"']/parent::*/parent::*/parent::*/td//input"), "Provider Independent feature");
		    Reusable.waitForAjax();
		     
		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		    Reusable.waitForAjax();
		}
	}

	public void ProviderIndependentUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
		String TypeofFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PA/PI Feature");
		String Ipv4Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4 Prefix Inp");
		String Ipv6Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv6 Prefix Inp");
		String LanIpv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN Inter IPv4 Address");
		String LanIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN Inter IPv6 Address");
		String NoIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Number of IPv6 Addresses");
		String PrimaryRangeReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary Range");
		String TypeofPiAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PI IPv4/IPv6 Addresses");		
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (TypeofFeature.toString().trim().equalsIgnoreCase("Provider Independent IPv4"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			sendKeys(NcOrderObj.addProductDetails.PrefixInput,Ipv4Prefix,"Ipv4Prefix");

			String ipAddressValue[]=LanIpv4Address.split("\\.");
			for(int i=0;i<ipAddressValue.length;i++)
			{
				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,"Lan Interface IPv4 Address");
//				sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,["+(i+1)+"],ipAddressValue[i]);
				SendKeys(findWebElement("@xpath=//td/a[(text()='LAN Interface IPv4 Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),ipAddressValue[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,"Type of PI IPv4 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else if (TypeofFeature.toString().trim().equalsIgnoreCase("Provider Independent IPv6"))
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			sendKeys(NcOrderObj.addProductDetails.PrefixInput,Ipv6Prefix,"Ipv6Prefix");
			
			verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,"Lan Interface IPv6 Address");
			sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,LanIpv6Address,"LAN Interface IPv6 Address");

//			String ipAddressValue[]=LanIpv6Address.split("\\.");
//			for(int i=0;i<ipAddressValue.length;i++)
//			{
//				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,"Lan Interface IPv6 Address");
//				SendKeys(findWebElement("@xpath=(//td/a[(text()='LAN Interface IPv6 Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),ipAddressValue[i]);
//				Reusable.waitForAjax();
//			}

			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");
			
			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv6Addresses,"Type of PI IPv6 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv6Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv4Link,"Provide Independent IPv4 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			selectByVisibleText(NcOrderObj.addProductDetails.PrefixInput,Ipv4Prefix,"Ipv4Prefix");

			String ipAddressValue[]=LanIpv4Address.split("\\.");
			for(int i=0;i<ipAddressValue.length;i++)
			{
				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,"Lan Interface IPv4 Address");
//				sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv4Address,["+(i+1)+"],ipAddressValue[i]);
				SendKeys(findWebElement("(//td/a[(text()='LAN Interface IPv4 Address')]/parent::*/following-sibling::*//input)["+(i+1)+"]"),ipAddressValue[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,"Type of PI IPv4 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			click(NcOrderObj.addIpAddressing.ProviderIndepIpv6Link,"Provide Independent IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.PrefixInput,"IPv4/IPv6 Prefix");
			selectByVisibleText(NcOrderObj.addProductDetails.PrefixInput,Ipv6Prefix,"Ipv6Prefix");

			String ipAddressValue1[]=LanIpv6Address.split("\\.");
			for(int i=0;i<ipAddressValue1.length;i++)
			{
				verifyExists(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,"Lan Interface IPv4 Address");
//				sendKeys(NcOrderObj.addIpAddressing.LanInterfaceIpv6Address,["+(i+1)+"],ipAddressValue[i]);
				SendKeys(findWebElement("(//td/a[(text()='LAN Interface IPv6 Address')]/parent::*/following-sibling::*//input)["+(i+1)+"]"),ipAddressValue1[i]);
				Reusable.waitForAjax();
			}
			verifyExists(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,"Number of IPv6 Addresses");
			selectByVisibleText(NcOrderObj.addIpAddressing.NoOfIpv6Addresses,NoIpv6Address,"NoIpv6Address");
			
			verifyExists(NcOrderObj.addIpAddressing.PrimaryRange,"Primary Range");
			selectByVisibleText(NcOrderObj.addIpAddressing.PrimaryRange,PrimaryRangeReq,"PrimaryRangeReq");
			
//			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

			verifyExists(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,"Type of PI IPv4 Address");
			selectByVisibleText(NcOrderObj.addIpAddressing.TypeofPIIpv4Addresses,TypeofPiAddress,"TypeofPiAddress");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
	}
	
	public void AddBgpFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String IpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
//		String TypeofFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of PA/PI Feature");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,"BGP4 Feed");
		Reusable.waitForAjax();

		String handleBefore = webDriver.getWindowHandle();

		verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
		click(NcOrderObj.generalInformation.Browse,"Browse button");
		Reusable.waitForAjax();

		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		
//			switchWindow(urlContent, handles);
		
	    System.out.println("Window handel"+curent);
	    webDriver.switchTo().window(curent);

	    // Perform the actions on new window
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	    click(("@xpath=//span[text()='BGP4 Feed']/parent::*/parent::*/parent::*/td//input"), "BGP4 Feed");
	    Reusable.waitForAjax();
	     
	    webDriver.switchTo().window(curent);
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

	    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
		click(NcOrderObj.generalInformation.SelectButton,"Select button");
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();

		// switch back to parent window
	    webDriver.switchTo().window(handleBefore);
	    Reusable.waitForAjax();
	}

	public void BgpFeedUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
		String ASType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Type of AS");
		String ASNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AS Number");
		String BgpFeedType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BGP4 Feed Type");
		String BgpFeedReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"BGP4 Feed Required");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addBgpFeed.Bgp4FeedLink,"BGP4 Feed link");
		click(NcOrderObj.addBgpFeed.Bgp4FeedLink,"BGP4 Feed link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addBgpFeed.TypeofAS,"Type of AS");
		selectByVisibleText(NcOrderObj.addBgpFeed.TypeofAS,ASType,"ASType");
		
		verifyExists(NcOrderObj.addBgpFeed.ASNumber,"AS Number");
		sendKeys(NcOrderObj.addBgpFeed.ASNumber,ASNumber);
	
		verifyExists(NcOrderObj.addBgpFeed.BGP4FeedType,"BGP4 Feed Type");
		selectByVisibleText(NcOrderObj.addBgpFeed.BGP4FeedType,BgpFeedType,"BgpFeedType");
		
		verifyExists(NcOrderObj.addBgpFeed.BGP4FeedReq,"BGP4 Feed Required");
		selectByVisibleText(NcOrderObj.addBgpFeed.BGP4FeedReq,BgpFeedReq,"BgpFeedReq");
		Reusable.waitForAjax();
		
		int randnumb = rand.nextInt(1000000000);
		verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
		sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
	}

	public void AddDhcpFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String FeatureName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Feature Name");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,"DHCP");
		Reusable.waitForAjax();

		String handleBefore = webDriver.getWindowHandle();

		verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
		click(NcOrderObj.generalInformation.Browse,"Browse button");
		Reusable.waitForAjax();

		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		
//			switchWindow(urlContent, handles);
		
	    System.out.println("Window handel"+curent);
	    webDriver.switchTo().window(curent);

	    // Perform the actions on new window
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	    click(("@xpath=//span[text()='DHCP']/parent::*/parent::*/parent::*/td//input"), "DHCP Feature");

	    if (FeatureFormat.toString().contains("IPv4"))
	    {
		    click(("@xpath=//span[text()='"+FeatureName+"']/parent::*/parent::*/parent::*/td//input"), "IPv4 DHCP Feature");
	    	Reusable.waitForAjax();
	    }
	    else
	    {
		    click(("@xpath=//span[text()='IPv4 DHCP']/parent::*/parent::*/parent::*/td//input"), "IPv4 Feature");
	    	Reusable.waitForAjax();
		    click(("@xpath=//span[text()='IPv6 DHCP']/parent::*/parent::*/parent::*/td//input"), "IPv6 Feature");
	    	Reusable.waitForAjax();
	    }
	    	
	    webDriver.switchTo().window(curent);
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

	    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
		click(NcOrderObj.generalInformation.SelectButton,"Select button");
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();

		// switch back to parent window
	    webDriver.switchTo().window(handleBefore);
	    Reusable.waitForAjax();
	}

	public void DhcpUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
		String DhcpFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addDhcp.DhcpLink,"DHCP link");
		click(NcOrderObj.addDhcp.DhcpLink,"DHCP link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addDhcp.Format,"DHCP Format");
		selectByVisibleText(NcOrderObj.addDhcp.Format,DhcpFormat,"DhcpFormat");

		int randnumb = rand.nextInt(1000000000);
		verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
		sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
	}
	
	public void IpDhcpUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String DomainName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Domain Name");
		String GatewayIpAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Gateway IP Add");
		String DHCPIpv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Address");
		String DHCPIpv4Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Prefix");
		String PrimaryWinsSerAddr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Primary WINS Server Add");
		String SecondaryWinsSerAddr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Secondary WINS Server Add");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		Random rand = new Random();

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addDhcp.DhcpIpv4Link,"IPv4 DHCP link");
		click(NcOrderObj.addDhcp.DhcpIpv4Link,"IPv4 DHCP link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addDhcp.DomainName,"Domain Name");
		sendKeys(NcOrderObj.addDhcp.DomainName,DomainName);

		String gatewayIpAddress[]=GatewayIpAddress.toString().split("\\.");
		for(int i=0;i<gatewayIpAddress.length;i++)
		{
//			sendKeys(NcOrderObj.addDhcp.GatewayIpv4Add,["+(i+1)+"],gatewayipAddress[i]);			
			SendKeys(findWebElement("@xpath=//td/a[text()='Gateway IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),gatewayIpAddress[i]);		
			Reusable.waitForAjax();
		}
		
		String dhcpIpAddressValue[]=DHCPIpv4Address.split("\\.");
		for(int i=0;i<dhcpIpAddressValue.length;i++)
		{
			SendKeys(findWebElement("@xpath=//td/a[text()='DHCP IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),dhcpIpAddressValue[i]);
			Reusable.waitForAjax();
		}

		verifyExists(NcOrderObj.addDhcp.DhcpIpv4Prefix,"DHCP IPv4 Prefix");
		sendKeys(NcOrderObj.addDhcp.DhcpIpv4Prefix,DHCPIpv4Prefix,"Dhcp Ipv4 Prefix");
		Reusable.waitForAjax();

		String primaryWinsAddressValue[]=PrimaryWinsSerAddr.split("\\.");
		for(int i=0;i<primaryWinsAddressValue.length;i++)
		{
			SendKeys(findWebElement("@xpath=//td/a[text()='Primary WINS Server IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),primaryWinsAddressValue[i]);
			Reusable.waitForAjax();
		}
		
		String secondaryWinsAddressValue[]=SecondaryWinsSerAddr.split("\\.");
		for(int i=0;i<secondaryWinsAddressValue.length;i++)
		{
			SendKeys(findWebElement("@xpath=//td/a[text()='Secondary WINS Server IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),secondaryWinsAddressValue[i]);
			Reusable.waitForAjax();
		}
		
		int randnumb = rand.nextInt(1000000000);
		verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
		sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
	}
	
	public void AddIpSnmpFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String SNMPFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Feature Name");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (FeatureFormat.toString().equalsIgnoreCase("IPv4") || FeatureFormat.toString().equalsIgnoreCase("IPv6"))
		{
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,SNMPFeature);
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		    click(("@xpath=//span[text()='"+SNMPFeature+"']/parent::*/parent::*/parent::*/td//input"), "SNMP Feature");
		    Reusable.waitForAjax();

		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		    Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,SNMPFeature);
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		    click(("@xpath=//span[text()='IPv4 SNMP']/parent::*/parent::*/parent::*/td//input"), "IPv4 SNMP");
		    Reusable.waitForAjax();

		    click(("@xpath=//span[text()='IPv6 SNMP']/parent::*/parent::*/parent::*/td//input"), "IPv6 SNMP");
		    Reusable.waitForAjax();

		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		    Reusable.waitForAjax();
		}
	}

	public void IpSnmpUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String CommunityString = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Community String");
		String TrapCommunityStringIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Trap Community String IPv4");
		String TrapCommunityStringIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Trap Community String IPv6");
		String TrapsReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Traps Required");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (FeatureFormat.toString().equalsIgnoreCase("IPv4"))
		{
			verifyExists(NcOrderObj.addSnmp.IpSnmp4Link,"IPv4 SNMP link");
			click(NcOrderObj.addSnmp.IpSnmp4Link,"IPv4 SNMP link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.addSnmp.CommunityString,"Community String");
			sendKeys(NcOrderObj.addSnmp.CommunityString,CommunityString);
			
			verifyExists(NcOrderObj.addSnmp.TrapCommunityString,"Trap Community String IPv4");
			sendKeys(NcOrderObj.addSnmp.TrapCommunityString,TrapCommunityStringIPv4);

			verifyExists(NcOrderObj.addSnmp.TrapsRequired,"Traps Required");
			selectByVisibleText(NcOrderObj.addSnmp.TrapsRequired,TrapsReq,"TrapsReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		
		else if (FeatureFormat.toString().equalsIgnoreCase("IPv6"))
		{
			verifyExists(NcOrderObj.addSnmp.IpSnmp6Link,"IPv6 SNMP link");
			click(NcOrderObj.addSnmp.IpSnmp6Link,"IPv6 SNMP link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.addSnmp.CommunityString,"Community String");
			sendKeys(NcOrderObj.addSnmp.CommunityString,CommunityString);
			
			verifyExists(NcOrderObj.addSnmp.TrapCommunityString,"Trap Community String IPv6");
			sendKeys(NcOrderObj.addSnmp.TrapCommunityString,TrapCommunityStringIPv6);

			verifyExists(NcOrderObj.addSnmp.TrapsRequired,"Traps Required");
			selectByVisibleText(NcOrderObj.addSnmp.TrapsRequired,TrapsReq,"TrapsReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addSnmp.IpSnmp4Link,"IPv4 SNMP link");
			click(NcOrderObj.addSnmp.IpSnmp4Link,"IPv4 SNMP link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.addSnmp.CommunityString,"Community String");
			sendKeys(NcOrderObj.addSnmp.CommunityString,CommunityString);
			
			verifyExists(NcOrderObj.addSnmp.TrapCommunityString,"Trap Community String IPv4");
			sendKeys(NcOrderObj.addSnmp.TrapCommunityString,TrapCommunityStringIPv4);

			verifyExists(NcOrderObj.addSnmp.TrapsRequired,"Traps Required");
			selectByVisibleText(NcOrderObj.addSnmp.TrapsRequired,TrapsReq,"TrapsReq");
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addSnmp.IpSnmp6Link,"IPv6 SNMP link");
			click(NcOrderObj.addSnmp.IpSnmp6Link,"IPv6 SNMP link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.addSnmp.CommunityString,"Community String");
			sendKeys(NcOrderObj.addSnmp.CommunityString,CommunityString);
			
			verifyExists(NcOrderObj.addSnmp.TrapCommunityString,"Trap Community String IPv6");
			sendKeys(NcOrderObj.addSnmp.TrapCommunityString,TrapCommunityStringIPv6);

			verifyExists(NcOrderObj.addSnmp.TrapsRequired,"Traps Required");
			selectByVisibleText(NcOrderObj.addSnmp.TrapsRequired,TrapsReq,"TrapsReq");
			
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
	}
	
	public void AddSnmpDeviceFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String SNMPDeviceFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Device Feature Name");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
		
		if (FeatureFormat.toString().equalsIgnoreCase("IPv4") || FeatureFormat.toString().equalsIgnoreCase("IPv6"))
		{			
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,SNMPDeviceFeature);
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		    click(("@xpath=//span[text()='"+SNMPDeviceFeature+"']/parent::*/parent::*/parent::*/td//input"), "SNMP Device Feature");
		    Reusable.waitForAjax();

		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		}
		else
		{
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,"SNMP Device IPv4 Address");
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		    click(("@xpath=//span[text()='SNMP Device IPv4 Address']/parent::*/parent::*/parent::*/td//input"), "SNMP Device IPv4 Address");
		    Reusable.waitForAjax();

		    click(("@xpath=//span[text()='SNMP Device IPv6 Address']/parent::*/parent::*/parent::*/td//input"), "SNMP Device IPv6 Address");
		    Reusable.waitForAjax();

		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		    Reusable.waitForAjax();
		}
	}

	public void SnmpDeviceUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		Random rand = new Random();
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String SnmpDeviceAddressIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Device Address IPv4");
		String SnmpDeviceAddressIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Device Address IPv6");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (FeatureFormat.toString().equalsIgnoreCase("IPv4"))
		{
			verifyExists(NcOrderObj.addSnmp.SnmpDeviceIpv4Link,"SNMP Device IPv4 link");
			click(NcOrderObj.addSnmp.SnmpDeviceIpv4Link,"SNMP Device IPv4 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			String snmpDeviceAddressValue[]=SnmpDeviceAddressIPv4.split("\\."); 
			for(int i=0;i<snmpDeviceAddressValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'SNMP Device')]/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),snmpDeviceAddressValue[i]);
				Reusable.waitForAjax();
			}
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else if (FeatureFormat.toString().equalsIgnoreCase("IPv6"))
		{
			verifyExists(NcOrderObj.addSnmp.SnmpDeviceIpv6Link,"SNMP Device IPv6 link");
			click(NcOrderObj.addSnmp.SnmpDeviceIpv6Link,"SNMP Device IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			String snmpDeviceAddressValue[]=SnmpDeviceAddressIPv6.split("\\."); 
			for(int i=0;i<snmpDeviceAddressValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'SNMP Device')]/parent::*/following-sibling::*//input[@maxlength='4']["+(i+1)+"]"),snmpDeviceAddressValue[i]);
				Reusable.waitForAjax();
			}
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addSnmp.SnmpDeviceIpv4Link,"SNMP Device IPv4 link");
			click(NcOrderObj.addSnmp.SnmpDeviceIpv4Link,"SNMP Device IPv4 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			String snmpDeviceAddressValue[]=SnmpDeviceAddressIPv4.split("\\."); 
			for(int i=0;i<snmpDeviceAddressValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'SNMP Device')]/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),snmpDeviceAddressValue[i]);
				Reusable.waitForAjax();
			}
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addSnmp.SnmpDeviceIpv6Link,"SNMP Device IPv6 link");
			click(NcOrderObj.addSnmp.SnmpDeviceIpv6Link,"SNMP Device IPv6 link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();
			
			String snmpDeviceAddressValue6[]=SnmpDeviceAddressIPv6.split("\\."); 
			for(int i=0;i<snmpDeviceAddressValue6.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'SNMP Device')]/parent::*/following-sibling::*//input[@maxlength='4']["+(i+1)+"]"),snmpDeviceAddressValue6[i]);
				Thread.sleep(5000);
			}
			
//			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
	}

	public void AddSnmpReadAccess(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,"SNMP Read-Only Access");
		Reusable.waitForAjax();

		String handleBefore = webDriver.getWindowHandle();

		verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
		click(NcOrderObj.generalInformation.Browse,"Browse button");
		Reusable.waitForAjax();

		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		
//				switchWindow(urlContent, handles);
		
	    System.out.println("Window handel"+curent);
	    webDriver.switchTo().window(curent);

	    // Perform the actions on new window
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	    click(("@xpath=//span[text()='SNMP Read-Only Access']/parent::*/parent::*/parent::*/td//input"), "SNMP Read-Only Access");
	    Reusable.waitForAjax();

	    webDriver.switchTo().window(curent);
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

	    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
		click(NcOrderObj.generalInformation.SelectButton,"Select button");
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();

		// switch back to parent window
	    webDriver.switchTo().window(handleBefore);
	    Reusable.waitForAjax();
	}

	public void SnmpReadAccessUpdate (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException	
	{
		Random rand = new Random();
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String AccessReq = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access Required");
		String SnmpVersion = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Version");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addSnmp.SnmpReadLink,"SNMP Read Access link");
		click(NcOrderObj.addSnmp.SnmpReadLink,"SNMP Read Access link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		if (FeatureFormat.toString().equalsIgnoreCase("IPv4"))
		{
			verifyExists(NcOrderObj.addDhcp.Format,"Addressing Format");
			selectByVisibleText(NcOrderObj.addDhcp.Format,FeatureFormat,"FeatureFormat");
		}
		else if (FeatureFormat.toString().equalsIgnoreCase("IPv6"))
		{
			verifyExists(NcOrderObj.addDhcp.Format,"Addressing Format");
			selectByVisibleText(NcOrderObj.addDhcp.Format,FeatureFormat,"FeatureFormat");
		}
		else
		{
			verifyExists(NcOrderObj.addDhcp.Format,"Addressing Format");
			selectByVisibleText(NcOrderObj.addDhcp.Format,FeatureFormat,"IPv4 and IPv6");
		}
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addSnmp.Acessrequired,"Access Required");
		selectByVisibleText(NcOrderObj.addSnmp.Acessrequired,AccessReq,"AccessReq");
		
		verifyExists(NcOrderObj.addSnmp.SnmpVersion,"SnmpVersion");
		selectByVisibleText(NcOrderObj.addSnmp.SnmpVersion,SnmpVersion,"SnmpVersion");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
	}
	
	public void AddNATFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String FeatureName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT Feature Name");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
		Reusable.waitForAjax();
		verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
		sendKeys(NcOrderObj.generalInformation.TypeofFeature,"NAT");
		Reusable.waitForAjax();

		String handleBefore = webDriver.getWindowHandle();

		verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
		click(NcOrderObj.generalInformation.Browse,"Browse button");
		Reusable.waitForAjax();

		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		
//				switchWindow(urlContent, handles);
		
	    System.out.println("Window handel"+curent);
	    webDriver.switchTo().window(curent);

	    // Perform the actions on new window
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
	    click(("@xpath=//span[text()='NAT']/parent::*/parent::*/parent::*/td//input"), "NAT");
	    Reusable.waitForAjax();
	    
	    click(("@xpath=//span[text()='"+FeatureName+"']/parent::*/parent::*/parent::*/td//input"), "NAT Feature");
	    Reusable.waitForAjax();

	    webDriver.switchTo().window(curent);
	    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

	    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
		click(NcOrderObj.generalInformation.SelectButton,"Select button");
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();

		// switch back to parent window
	    webDriver.switchTo().window(handleBefore);
	    Reusable.waitForAjax();
	}
	
	public void NatUpdate (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException	
	{
		Random rand = new Random();
		String FeatureName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT Feature Name");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addNat.NatLink,"NAT link");
		click(NcOrderObj.addNat.NatLink,"NAT link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		if (FeatureName.toString().equalsIgnoreCase("Static NAT Route"))
		{
			verifyExists(NcOrderObj.addNat.NatType,"Nat Type");
			selectByVisibleText(NcOrderObj.addNat.NatType,"Static","Static");
		}
		else if (FeatureName.toString().equalsIgnoreCase("Dynamic NAT"))
		{
			verifyExists(NcOrderObj.addNat.NatType,"Nat Type");
			selectByVisibleText(NcOrderObj.addNat.NatType,"Dynamic","Dynamic");
		}
		else
		{
			verifyExists(NcOrderObj.addNat.NatType,"Nat Type");
			selectByVisibleText(NcOrderObj.addNat.NatType,"Staic and Dynamic","Staic and Dynamic");
		}
		Reusable.waitForAjax();
		
		int randnumb = rand.nextInt(1000000000);
		verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
		sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		click(NcOrderObj.createOrder.Update,"Update button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();
	}
	
	public void NatUpdateStaticandDynamic (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException	
	{
		Random rand = new Random();
		String FeatureName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT Feature Name");
		String PrivatePort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Private Port");
		String PrivateIpAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Private IP Address");
		String PublicPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Public Port");
		String PublicIpAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Public IP Address");
		String NatfromMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT from Mask");
		String NatIpv4RangeFrom = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT IPv4 Range From");
		String NatIpv4RangeTo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT IPv4 Range To");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (FeatureName.toString().equalsIgnoreCase("Static NAT Route"))
		{
			verifyExists(NcOrderObj.addNat.StaticNatLink,"Static NAT link");
			click(NcOrderObj.addNat.StaticNatLink,"Static Nat Link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addNat.NatPrivateport,"Private Port");
			sendKeys(NcOrderObj.addNat.NatPrivateport,PrivatePort);
			
			verifyExists(NcOrderObj.addNat.NatPublicport,"Public Port");
			sendKeys(NcOrderObj.addNat.NatPublicport,PublicPort);
			Reusable.waitForAjax();
			
			String natPrivateIpAddressValue[]=PrivateIpAdd.split("\\."); 
			for(int i=0;i<natPrivateIpAddressValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'Private IP Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),natPrivateIpAddressValue[i]);
				Reusable.waitForAjax();
			}
			
			String natPublicIpAddressValue[]=PublicIpAdd.split("\\."); 
			for(int i=0;i<natPublicIpAddressValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'Public IP Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),natPublicIpAddressValue[i]);
				Reusable.waitForAjax();
			}
			
			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addNat.DynamicNatLink,"Dynamic NAT link");
			click(NcOrderObj.addNat.DynamicNatLink,"Dynamic NAT Link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			String natFromMaskValue[]=NatfromMask.split("\\.");
			for(int i=0;i<natFromMaskValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'NAT From Mask')]/parent::*/following-sibling::*//input["+(i+1)+"]"),natFromMaskValue[i]);
				Reusable.waitForAjax();
			}
			
			String natRangeFromValue[]=NatIpv4RangeFrom.split("\\.");
			for(int i=0;i<natRangeFromValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'NAT IPv4 Address Range From')]/parent::*/following-sibling::*//input["+(i+1)+"]"),natRangeFromValue[i]);
				Reusable.waitForAjax();
			}

			String natRangeToValue[]=NatIpv4RangeTo.split("\\.");
			for(int i=0;i<natRangeToValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'NAT IPv4 Address Range To')]/parent::*/following-sibling::*//input["+(i+1)+"]"),natRangeToValue[i]);
				Reusable.waitForAjax();
			}

			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
	}
	
	public void AddStaticRouteFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
		String FeatureName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Route Feature Name");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		if (FeatureFormat.toString().equalsIgnoreCase("IPv4") || FeatureFormat.toString().equalsIgnoreCase("IPv6"))
		{
			getUrl(Orderscreenurl);
			
			verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
			Reusable.waitForAjax();
	
			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,FeatureName);
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		    click(("@xpath=//span[text()='"+FeatureName+"']/parent::*/parent::*/parent::*/td//input"), "Static Route Feature");
		    Reusable.waitForAjax();

		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		    Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature Link");
			Reusable.waitForAjax();
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			click(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,"Static IPv4 Route");
			Reusable.waitForAjax();

			String handleBefore = webDriver.getWindowHandle();

			verifyExists(NcOrderObj.generalInformation.Browse,"Browse button");
			click(NcOrderObj.generalInformation.Browse,"Browse button");
			Reusable.waitForAjax();

			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			
//				switchWindow(urlContent, handles);
			
		    System.out.println("Window handel"+curent);
		    webDriver.switchTo().window(curent);

		    // Perform the actions on new window
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		    click(("@xpath=//span[text()='Static IPv4 Route']/parent::*/parent::*/parent::*/td//input"),"Static IPv4 Route");
		    Reusable.waitForAjax();

		    click(("@xpath=//span[text()='Static IPv6 Route']/parent::*/parent::*/parent::*/td//input"), "Static IPv6 Route");
		    Reusable.waitForAjax();

		    webDriver.switchTo().window(curent);
		    webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));

		    verifyExists(NcOrderObj.generalInformation.SelectButton,"Select button");
			click(NcOrderObj.generalInformation.SelectButton,"Select button");
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForpageloadmask();
			Reusable.waitForAjax();

			// switch back to parent window
		    webDriver.switchTo().window(handleBefore);
		    Reusable.waitForAjax();
		}
	}
	
	public void StaticRouteUpdate (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException	
	{
		Random rand = new Random();
		String FeatureFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Format");
//		String FeatureName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Feature Name");
		String NextHopIpv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Next Hop IPv4");
		String NextHopIpv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Next Hop IPv6");
		String TargetNetworkIpv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Target Network IPv4");
		String TargetNetworkIpv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Target Network IPv6");
		String SubnetMaskIpv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Subnet Mask IPv4");
		String SubnetMaskIpv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Subnet Mask IPv6");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		click(NcOrderObj.addProductDetails.UnderlyingIpOrder,"IP Access Order Link");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (FeatureFormat.toString().equalsIgnoreCase("IPv4"))
		{
			verifyExists(NcOrderObj.addStaticRoute.StaticRoute4Link,"Static Route IPv4 link");
			click(NcOrderObj.addStaticRoute.StaticRoute4Link,"Static Route IPv4 Link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			String nextHopIpv4Value[]=NextHopIpv4.split("\\.");
			for(int i=0;i<nextHopIpv4Value.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[(text()='Next Hop IP')]/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),nextHopIpv4Value[i]);
				Reusable.waitForAjax();
			}

			String targetNetworkValue[]=TargetNetworkIpv4.split("\\.");
			for(int i=0;i<targetNetworkValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'Target Network')]/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),targetNetworkValue[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addStaticRoute.SubnetMask,"Subnet Mask IPv4");
			sendKeys(NcOrderObj.addStaticRoute.SubnetMask,SubnetMaskIpv4);
			Reusable.waitForAjax();

			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else if (FeatureFormat.toString().equalsIgnoreCase("IPv6"))
		{
			verifyExists(NcOrderObj.addStaticRoute.StaticRoute6Link,"Static Route IPv6 link");
			click(NcOrderObj.addStaticRoute.StaticRoute6Link,"Static Route IPv6 Link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			String nextHopIpv6Value[]=NextHopIpv6.split("\\.");
			for(int i=0;i<nextHopIpv6Value.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[text()='Next Hop IP']/parent::*/following-sibling::*//input["+(i+1)+"]"),nextHopIpv6Value[i]);
//				SendKeys(findWebElement("@xpath=//td/a[(text()='LAN Interface IPv4 Address')]/parent::*/following-sibling::*//input["+(i+1)+"]"),ipAddressValue[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addStaticRoute.TargetNetwork,"Target Network IPv6");
			sendKeys(NcOrderObj.addStaticRoute.TargetNetwork,TargetNetworkIpv6);
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addStaticRoute.SubnetMask,"Subnet Mask IPv6");
			sendKeys(NcOrderObj.addStaticRoute.SubnetMask,SubnetMaskIpv6);
			Reusable.waitForAjax();

			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
		else
		{
			verifyExists(NcOrderObj.addStaticRoute.StaticRoute4Link,"Static Route IPv4 link");
			click(NcOrderObj.addStaticRoute.StaticRoute4Link,"Static Route IPv4 Link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			String nextHopIpv4Value[]=NextHopIpv4.split("\\.");
			for(int i=0;i<nextHopIpv4Value.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'Next Hop IP')]/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),nextHopIpv4Value[i]);
				Reusable.waitForAjax();
			}

			String targetNetworkValue[]=TargetNetworkIpv4.split("\\.");
			for(int i=0;i<targetNetworkValue.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[contains(text(),'Target Network')]/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]"),targetNetworkValue[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addStaticRoute.SubnetMask,"Subnet Mask IPv4");
			sendKeys(NcOrderObj.addStaticRoute.SubnetMask,SubnetMaskIpv4);
			Reusable.waitForAjax();

			int randnumb = rand.nextInt(1000000000);
			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addStaticRoute.StaticRoute6Link,"Static Route IPv6 link");
			click(NcOrderObj.addStaticRoute.StaticRoute6Link,"Static Route IPv6 Link");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			String nextHopIpv6Value[]=NextHopIpv6.split("\\.");
			for(int i=0;i<nextHopIpv6Value.length;i++)
			{
				SendKeys(findWebElement("@xpath=//td/a[text()='Next Hop IP']/parent::*/following-sibling::*//input["+(i+1)+"]"),nextHopIpv6Value[i]);
				Reusable.waitForAjax();
			}

			verifyExists(NcOrderObj.addStaticRoute.TargetNetwork,"Target Network IPv6");
			sendKeys(NcOrderObj.addStaticRoute.TargetNetwork,TargetNetworkIpv6);
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addStaticRoute.SubnetMask,"Subnet Mask IPv6");
			sendKeys(NcOrderObj.addStaticRoute.SubnetMask,SubnetMaskIpv6);
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addIpAddressing.SiebelCompId,"Siebel Component ID");
			sendKeys(NcOrderObj.addIpAddressing.SiebelCompId,Integer.toString(randnumb));
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
			Reusable.waitForAjax();
		}
	}
	
	public void DecomposeIpOrder (String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException	
	{
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
        verifyExists(NcOrderObj.addProduct.OrderTab,"Orders Tab");
        click(NcOrderObj.addProduct.OrderTab,"Orders Tab");
       
        verifyExists(NcOrderObj.generalInformation.SubIpOrder,"IP Access Product Order");
        click(NcOrderObj.generalInformation.SubIpOrder,"IP Access Product Order");
       
        verifyExists(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        click(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        waitForAjax();
	}
	
	public void EthernetProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");		
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.addProductDetails.UnderlyingEthernetOrder,"Ethernet Connection Product Order");
		click(NcOrderObj.addProductDetails.UnderlyingEthernetOrder,"Ethernet Connection Product Order");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		String Topology = "Point to Point";
		verifyExists(NcOrderObj.addProductDetails.Topology,"Topology");
		click(NcOrderObj.addProductDetails.Topology,"Topology");
		//Reusable.waitForSiebelLoader();
		selectByVisibleText(NcOrderObj.addProductDetails.Topology,Topology,"Topology");
		//Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.Topology, Keys.ENTER);

		verifyExists(NcOrderObj.addProductDetails.ServiceBand,"Service Bandwidth");		
		sendKeys(NcOrderObj.addProductDetails.ServiceBand,ServiceBandwidth,"ServiceBandwidth");
		Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBand, Keys.ENTER);
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.CircuitCategory,"Circuit Category");
		click(NcOrderObj.addProductDetails.CircuitCategory,"Circuit Category");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NcOrderObj.addProductDetails.CircuitCategory,"IA","IA");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		clickByJS(NcOrderObj.createOrder.Update);
		
		Reusable.waitForAjax();
	}
	
	public void DecomposeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
    {
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		
        verifyExists(NcOrderObj.addProduct.OrderTab,"Orders Tab");
        click(NcOrderObj.addProduct.OrderTab,"Orders Tab");
       
        verifyExists(NcOrderObj.generalInformation.Suborder,"Ethernet Connection Product Order");
        click(NcOrderObj.generalInformation.Suborder,"Ethernet Connection Product Order");
       
        verifyExists(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        click(NcOrderObj.generalInformation.Decompose,"Click on Decompose button");
        waitForAjax();
        Reusable.waitForpageloadmask();
    }
	
	public void EndSiteProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String ResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Resilience Option");
		String AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access Technology");
		String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access type");
		String SiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Site ID");
//		String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation Interface");
		String PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Port Role");
		String VlanTaggingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Vlan Tagging Mode");
		String VlanTagId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Vlan Tag Id");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
//		String ServiceBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth");
		
		getUrl(Orderscreenurl);

		verifyExists(NcOrderObj.addProductDetails.UnderLyingEndSiteOrder,"End Site Product Order");
		click(NcOrderObj.addProductDetails.UnderLyingEndSiteOrder,"End Site Product Order");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addEndSiteDetails.ResilienceOption,"Resilience Option");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.ResilienceOption,ResilienceOption,"ResilienceOption");
		
		verifyExists(NcOrderObj.addEndSiteDetails.AccessTechnology,"Access Technology");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.AccessTechnology,AccessTechnology,"AccessTechnology");

		verifyExists(NcOrderObj.addEndSiteDetails.AccessType,"Access Type");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.AccessType,AccessType,"AccessType");

		verifyExists(NcOrderObj.addEndSiteDetails.SiteEnd,"Site End");
		selectByVisibleText(NcOrderObj.addEndSiteDetails.SiteEnd,"A END","A End");

		verifyExists(NcOrderObj.addEndSiteDetails.SiteID,"Site ID");
		sendKeys(NcOrderObj.addEndSiteDetails.SiteID,SiteID);
		
		/*verifyExists(NcOrderObj.addProductDetails.ServiceBand,"Service Bandwidth");		
		sendKeys(NcOrderObj.addProductDetails.ServiceBand,ServiceBandwidth,"ServiceBandwidth");
		Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBand, Keys.ENTER);
		Reusable.waitForAjax();*/
		
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		clickByJS(NcOrderObj.createOrder.Update);
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addAccessPort.AccessPortLink,"Access Port Link");
		click(NcOrderObj.addAccessPort.AccessPortLink,"Access Port Link");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addAccessPort.PresentConnectType,"Presentation Connector Type");
		selectByVisibleText(NcOrderObj.addAccessPort.PresentConnectType,"LC/PC","PresentationInterface");

		verifyExists(NcOrderObj.addAccessPort.AccessportRole,"Port Role");
		selectByVisibleText(NcOrderObj.addAccessPort.AccessportRole,PortRole,"PortRole");
		
		if (PortRole.toString().contains("VLAN"))
		{
			verifyExists(NcOrderObj.addAccessPort.VlanTaggingMode,"VLAN Tagging Mode");
			selectByVisibleText(NcOrderObj.addAccessPort.VlanTaggingMode,VlanTaggingMode,"VlanTaggingMode");
		}

		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		clickByJS(NcOrderObj.createOrder.Update);
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"IP Access Order Screen");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.addAccessPort.CPElink,"CPE Information Link");
		click(NcOrderObj.addAccessPort.CPElink,"CPE Information Link");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		click(NcOrderObj.addProductDetails.Edit,"Edit button");
		Reusable.waitForAjax();
		
		verifyExists(NcOrderObj.addAccessPort.CabinetType,"Cabinet Type");
		selectByVisibleText(NcOrderObj.addAccessPort.CabinetType,"Existing Colt Cabinet","Existing Colt Cabinet");

		verifyExists(NcOrderObj.addAccessPort.CabinetID,"Cabinet ID");
		sendKeys(NcOrderObj.addAccessPort.CabinetID,"43");
		
		verifyExists(NcOrderObj.createOrder.Update,"Update button");
		clickByJS(NcOrderObj.createOrder.Update);
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"End Site Order Screen");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"End Site Order Screen");
		Reusable.waitForAjax();

		verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();

		if (PortRole.toString().contains("VLAN"))
		{
			verifyExists(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature link");
			click(NcOrderObj.generalInformation.AddFeaturelink,"Add Feature link");
			
			verifyExists(NcOrderObj.generalInformation.TypeofFeature,"Type of Feature");
			sendKeys(NcOrderObj.generalInformation.TypeofFeature,"VLAN");
			Reusable.SendkeaboardKeys(NcOrderObj.generalInformation.TypeofFeature, Keys.ENTER);
			
			verifyExists(NcOrderObj.generalInformation.SelectFeature,"Select Feature button");
			click(NcOrderObj.generalInformation.SelectFeature,"Select Feature button");
			Reusable.waitForAjax();
			
			verifyExists(NcOrderObj.addAccessPort.VLANLink,"VLAN link");
			click(NcOrderObj.addAccessPort.VLANLink,"VLAN link");
			
			verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
			click(NcOrderObj.addProductDetails.Edit,"Edit button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.addAccessPort.Ethertype,"Ether Type");
			clearTextBox(NcOrderObj.addAccessPort.Ethertype);
			sendKeys(NcOrderObj.addAccessPort.Ethertype,"VLAN (0x8100)");

			verifyExists(NcOrderObj.addAccessPort.VLANLink,"VLAN Tag ID");
			sendKeys(NcOrderObj.addAccessPort.VLANLink,VlanTagId);
			
			verifyExists(NcOrderObj.createOrder.Update,"Update button");
			click(NcOrderObj.createOrder.Update,"Update button");
			Reusable.waitForAjax();

			verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"End Site Order Screen");
			click(NcOrderObj.generalInformation.Accountbredcrumb,"End Site Order Screen");
			Reusable.waitForAjax();
		}
	}
	
	public void ProcessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		
		getUrl(Orderscreenurl);
		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
//		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//		Reusable.waitForAjax();
//		String[] arrOfStr = Ordernumber.split("#", 0);
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

//		verifyExists("//span[text()='"+Ordernumber+"']/parent::*/parent::*/parent::*/td//input","Created Order");
//	    checkbox(("//span[text()='"+Ordernumber+"']/parent::*/parent::*/parent::*/td//input"),Keys);
//		WebElement checkbox = webDriver.findElement(By.xpath("//span[text()='"+Ordernumber+"']/parent::*/parent::*/parent::*/td//input"));
//		checkbox.click();

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
	}
	
	public void CompleteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    {
        String Ordernumber = null;
        Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_No");
        String WorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Work Items");
		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

        String[] arrOfStr = Ordernumber.split("#", 0);

//		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of Orders");
//		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of Orders");
//		   
//		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);

        getUrl(Orderscreenurl);
        
        verifyExists(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
        click(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");
       
        verifyExists(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        click(NcOrderObj.taskDetails.ExecutionFlowlink,"Execution Flow Link");
        waitForAjax();
   
        verifyExists(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        click(NcOrderObj.taskDetails.Workitems,"Workitems Tab");
        waitForAjax();
       
        for (int k=1; k<=Integer.parseInt(WorkItems);k++)
        {
            waitForElementToAppear(NcOrderObj.workItems.TaskReadytoComplete,120,10000);
            click(NcOrderObj.workItems.TaskReadytoComplete,"Workitem in Ready status");
            //need to verify
            Completeworkitem(GetText(NcOrderObj.taskDetails.TaskTitle), testDataFile, sheetName, scriptNo, dataSetNo);         
//            Completworkitem(GetText2(getwebelement(xml.getlocator("//locators/Tasks/TaskTitle"))),Inputdata);
        }
       
		getUrl(Orderscreenurl);
       
        verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
        click(NcOrderObj.generalInformation.Accountbredcrumb,"Accounts Composite Orders Tab");
       
//        verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
//        click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");

		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Ordernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ Ordernumber + EthernetOrderObj.CompositOrders.arrorder1,200,10000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Ordernumber + EthernetOrderObj.CompositOrders.arrorder1);

	    if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+Ordernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Ordernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+Ordernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, Ordernumber +" Execution InProgress");
		}
    }
	
	public void Completeworkitem(String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String ResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Resilience Option");
		String AccessNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access NW Element");
		String AccessPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access port");
		String CPENNiPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
		String CPENNiPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
		String PeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
		String PEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 1");
		String PEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 2");
		String VcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend VCX Control");
		String Beacon = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Beacon");
		String L3cpeName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE Name");
		String L3cpeUniPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE UNI Port");
		String PrimarySR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR");
		String PrimarySRGil = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR GIL");
		String WanIpv4SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Wan IPv4 Subnet Size");
		String DuplexBfdInterval = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Duplex/Bfd interval");
		String EgressPolicyMap = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Egress Policy Map");
		String SpeedMultiplier = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Speed/Multiplier");
		String PremiumCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premium CIR%");
		String InternetCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Internet CIR%");
		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");

		
		System.out.println("In Switch case with TaskName :"+taskname);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Start completion of Task: "+taskname[0]);
		switch(taskname[0])
		{
			case "Reserve Access Resources":
			if (ResilienceOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AccessNwElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AccessPort);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,CPENNiPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,CPENNiPort2);
				waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
			}
			else
			{
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AccessNwElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AccessPort);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,CPENNiPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
//				workitemcounter.set(workitemcounter.get()+1);
			}
			break;
			
			case "Transport Circuit Design":
			if (ResilienceOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
//				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForAjax();
				
				if (DeviceType.contains("Stub"))
				{
					if (AccessNwElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
					}			
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort2);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
            	//Code for Unprotected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
//				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,PeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
//				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,PEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();

//				if (DeviceType.contains("Stub"))
//				{
//					if (AccessNwElement.contains("SC"))
//					{
//						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//						clearTextBox(NcOrderObj.workItems.VCXController);
//						sendKeys(NcOrderObj.workItems.VCXController,VcxControl);
//						Reusable.waitForAjax();
//						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
//						Reusable.waitForSiebelSpinnerToDisappear();
//					}
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
//				}						
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Reserve MPR Resources" :
			{
				verifyExists(NcOrderObj.workItems.L3cpeName,"L3CPE Name");
				clearTextBox(NcOrderObj.workItems.L3cpeName);
				sendKeys(NcOrderObj.workItems.L3cpeName,L3cpeName);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.L3cpeName, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.L3cpeUNIPort,"L3CPE UNI Port");
				clearTextBox(NcOrderObj.workItems.L3cpeUNIPort);
				sendKeys(NcOrderObj.workItems.L3cpeUNIPort,L3cpeUniPort);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.L3cpeUNIPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Verify SR Router and GIL":
			{
				verifyExists(NcOrderObj.workItems.AsrGil,"ASR GIL");
				clearTextBox(NcOrderObj.workItems.AsrGil);

				verifyExists(NcOrderObj.workItems.Asr,"ASR");
				clearTextBox(NcOrderObj.workItems.Asr);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForAjax();
				sendKeys(NcOrderObj.workItems.Asr,PrimarySR);
				Reusable.waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.Asr, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.workItems.AsrGil,"ASR GIL");
				clearTextBox(NcOrderObj.workItems.AsrGil);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForAjax();
//				Reusable.waitForSiebelLoader();
				sendKeys(NcOrderObj.workItems.AsrGil,PrimarySRGil);
				Reusable.waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.AsrGil, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					Reusable.waitForAjax();
                    verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
                    click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
                    Reusable.waitForAjax();
                    
					for(int i=1;i<=3;i++)
    				{
						ErrorWorkItems.errorWorkitems("Beacon: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
    				}
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Set Bespoke Parameters":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.WanIpSubnetSize,"WAN IPv4 Subnet Size");
				select(NcOrderObj.workItems.WanIpSubnetSize,WanIpv4SubnetSize);

				verifyExists(NcOrderObj.workItems.WanIpSubnetSizeFlag,"WAN IPv4 Subnet Size Flag");
				select(NcOrderObj.workItems.WanIpSubnetSizeFlag,"Yes");

				verifyExists(NcOrderObj.workItems.OtherWanIpSubnetSize,"Other WAN IPv4 Subnet Size");
				select(NcOrderObj.workItems.OtherWanIpSubnetSize,DuplexBfdInterval);
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Select LAN IP Range":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.RequestNewIp,"Request New IP from EIP button");
				click(NcOrderObj.workItems.RequestNewIp,"Request New IP from EIP button");
				Reusable.waitForAjax();
			}
			break;
			
			case "Waiting for RIR Approval":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Manual Design Task for MPR":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.EgressPolicyMap,"Egress Policy Map");
				clearTextBox(NcOrderObj.workItems.EgressPolicyMap);
				sendKeys(NcOrderObj.workItems.EgressPolicyMap,EgressPolicyMap);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.EgressPolicyMap, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
	        
			case "Load License to MPR":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
	        
			case "Check Order parameters":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
	    	case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				if(DeviceType.contains("Stub"))
				{
					ErrorWorkItems.errorWorkitems("EIP: Get IP",testDataFile, sheetName, scriptNo, dataSetNo);
					ErrorWorkItems.checkipaddress("Check IP address in DNS",testDataFile, sheetName, scriptNo, dataSetNo);
				}
	    	}
	    	break;
	    	
	    	case "Activation Start Confirmation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				if(DeviceType.contains("Stub"))
				{
					ErrorWorkItems.errorWorkitems("Check GX/LTS Smarts Status in NC OSS",testDataFile, sheetName, scriptNo, dataSetNo);
					for(int i=1;i<=2;i++)
					{
						ErrorWorkItems.errorWorkitems("Create Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
					}
				}
	    	}
	    	break;
	    	
			case "Confirm L3 Activation Start":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "MPR Trigger Configuration Start":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Duplex Check":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "Speed Test":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
			break;

			case "Remote Connectivity Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;

			case "Enable Smart Licensing":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
					
			case "Disable Smart Licensing":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}

			case "Customer Dynamic Routing Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
	        break;

			case"Set Bandwidth Distribution Parameters":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				sendKeys(NcOrderObj.workItems.PremiumCir,PremiumCir);
				sendKeys(NcOrderObj.workItems.InternetCir,InternetCir);
				
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Bandwidth Profile Confirmation":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Check BGP":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
	        break;

			case "Service Test":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Legacy Activation Completed":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			default:
			// No case found that it will complete the Workitem	
			verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
			click(NcOrderObj.workItems.Complete,"Complete Workitem");
			Reusable.waitForAjax();
		}
	}
	
	public void searchCompOrderinNC(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
    { 		
		String TechnicalServiceId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalServiceId");
		
		verifyExists(NcOrderObj.searchCompOrder.Fastsearch,"Fastsearch");
		click(NcOrderObj.searchCompOrder.Fastsearch,"Fastsearch");
		Reusable.waitForSiebelLoader();
		
		verifyExists(NcOrderObj.searchCompOrder.ServiceInventorySearch,"ServiceInventorySearch");
		mouseMoveOn(NcOrderObj.searchCompOrder.ServiceInventorySearch);
		Reusable.waitForSiebelLoader();
		
		verifyExists(NcOrderObj.searchCompOrder.ProductOrderSearch,"ProductOrderSearch");
		click(NcOrderObj.searchCompOrder.ProductOrderSearch,"ProductOrderSearch");
		Reusable.waitForSiebelLoader();
		
		verifyExists(NcOrderObj.searchCompOrder.ProductOrderSearch,"ServiceIDsearchcriterion");
		sendKeys(NcOrderObj.searchCompOrder.ProductOrderSearch,TechnicalServiceId,"ProductOrderSearch");
		Reusable.SendkeaboardKeys(NcOrderObj.addProductDetails.ServiceBand, Keys.TAB);
		Reusable.waitForSiebelLoader();
		
		verifyExists(NcOrderObj.searchCompOrder.searchButton,"searchButton");
		click(NcOrderObj.searchCompOrder.searchButton,"searchButton");
		Reusable.waitForSiebelLoader();
		
		String Ordernumber = getTextFrom(NcOrderObj.searchCompOrder.OrderNolink);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Ordernumber", Ordernumber);
		Reusable.waitForSiebelLoader();
		Reusable.waitForSiebelSpinnerToDisappear();
		
		verifyExists(NcOrderObj.searchCompOrder.OrderNolink,"OrderNolink");
		click(NcOrderObj.searchCompOrder.OrderNolink,"OrderNolink");
		Reusable.waitForSiebelLoader();
		
		String OrderscreenURL=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OrderscreenURL", OrderscreenURL);
		System.out.println(Orderscreenurl);
		Reusable.waitForAjax();
	}
}

class Error_WorkItems extends SeleniumUtils 
{
	
	GlobalVariables g = new GlobalVariables();
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void errorWorkitems(String Taksname1, String testDataFile, String sheetName, String scriptNo,String dataSetNo)throws Exception
	{
		Reusable.waitForpageloadmask();
		click(NcOrderObj.taskDetails.TaskTab,"Tasks Tab");

		Reusable.waitForpageloadmask();
		String taskTabURL = getURLFromPage();
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Tasktab_URL", taskTabURL);
		waitForElementToAppear("@xpath=//td[text()='Error']/preceding-sibling::*//a/span[text()='"+Taksname1+"']",200,20000);
		//click("@xpath=//td[text()='Error']/preceding-sibling::*//a/span[text()='"+Taksname1+"']","Click on Task");
		click("@xpath=//td[text()='Error']/preceding-sibling::*//a/span[text()='"+Taksname1+"']/parent::*/parent::*/preceding-sibling::*//input","Click");
		click(NcOrderObj.workItems.Cancelbutton,"Click on Cancel Button");
		Reusable.waitForAjax();
		click(NcOrderObj.taskDetails.Workitems,"Click on Workitems Tab");
		Reusable.waitForAjax();
	}

	public void checkipaddress(String Taksname1,String testDataFile, String sheetName, String scriptNo,String dataSetNo)throws Exception
	{
		String taskTabURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Tasktab_URL");
		String Management_IP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management IP");	
		
		getUrl(taskTabURL);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to current url");
		Reusable.waitForAjax();
		waitForElementToAppear("@xpath=//td[text()='Error']/preceding-sibling::*//a/span[text()='"+Taksname1+"']",120,20000);
		click("@xpath=//a/span[text()='"+Taksname1+"']/parent::*/parent::*/preceding-sibling::*//span[contains(text(),'New GX/LTS CPE RFS Order')]","Click error task");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click New GX/LTS CPE RFS Order");
		click(NcOrderObj.addProductDetails.Edit,"Click Edit button");
		//SendKeys(getwebelement(xml.getlocator("//locators/Ipadress")),"10.91.140.158/31");
		sendKeys(NcOrderObj.workItems.Ipadress,Management_IP,"ManagementIP value");
		click(NcOrderObj.createOrder.Update,"Update");
		Reusable.waitForAjax();
		getUrl(taskTabURL);

		Reusable.waitForAjax();
		click("@xpath=//td[text()='Error']/preceding-sibling::*//a/span[text()='"+Taksname1+"']/parent::*/parent::*/preceding-sibling::*//input","Click on Task Name");
		click(NcOrderObj.workItems.RetryTask); ;

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		//Retry Store Initial Config Files Error
		waitForElementToAppear("@xpath=//td[text()='Error']/preceding-sibling::*//a/span[text()='Store Initial Config Files']",120,20000);
		click("@xpath=//td[text()='Error']/preceding-sibling::*//a/span[text()='Store Initial Config Files']/parent::*/parent::*/preceding-sibling::*//input","Click on Task Name");
		click(NcOrderObj.workItems.RetryTask,"Click on Retry Button");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		click(NcOrderObj.taskDetails.Workitems,"Click on workitems tab");
	}
}