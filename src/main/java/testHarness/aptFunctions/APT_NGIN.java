package testHarness.aptFunctions;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import baseClasses.ExtentTestManager;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_DDIRangeObj;
import pageObjects.aptObjects.APT_IPTransitObj;
import pageObjects.aptObjects.APT_NGINObj;

public class APT_NGIN extends SeleniumUtils{
	
	public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		
			//ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Customer Creation Functionality");

		
				verifyExists(APT_NGINObj.NGIN.ManageCustomerServiceLink," Manage Customer Service Link");
				click(APT_NGINObj.NGIN.ManageCustomerServiceLink," Manage Customer Service Link");
				
				verifyExists(APT_NGINObj.NGIN.createcustomerlink," create customer link");
				click(APT_NGINObj.NGIN.createcustomerlink,"Create Customer Link");
				
				verifyExists(APT_NGINObj.NGIN.createcustomer_header,"create customer page header");
				
				//scrolltoend();
				verifyExists(APT_NGINObj.NGIN.okbutton,"ok");
				click(APT_NGINObj.NGIN.okbutton,"ok");
				
				//Warning msg check
				verifyExists(APT_NGINObj.NGIN.customernamewarngmsg,"Legal Customer Name");
				verifyExists(APT_NGINObj.NGIN.countrywarngmsg,"Country");
				verifyExists(APT_NGINObj.NGIN.ocnwarngmsg,"OCN");
				verifyExists(APT_NGINObj.NGIN.typewarngmsg,"Type");
				verifyExists(APT_NGINObj.NGIN.emailwarngmsg,"Email");
			
				//Create customer by providing all info
				
				verifyExists(APT_NGINObj.NGIN.nametextfield,"name");
				sendKeys(APT_NGINObj.NGIN.nametextfield,name,"name");
		
				
				verifyExists(APT_NGINObj.NGIN.maindomaintextfield,"Main Domain");
				sendKeys(APT_NGINObj.NGIN.maindomaintextfield,maindomain,"Main Domain");
				
				verifyExists(APT_NGINObj.NGIN.country,"country");
			//	sendKeys(APTIPAccessNoCPE.APTNoCPE.country,country,"country");
				addDropdownValues_commonMethod("Country",APT_NGINObj.NGIN.country,country);
			
				verifyExists(APT_NGINObj.NGIN.ocntextfield,"ocn");
				sendKeys(APT_NGINObj.NGIN.ocntextfield,ocn,"ocn");
				
				verifyExists(APT_NGINObj.NGIN.referencetextfield,"reference");
				sendKeys(APT_NGINObj.NGIN.referencetextfield,reference,"reference");
			
				verifyExists(APT_NGINObj.NGIN.technicalcontactnametextfield,"technical contact name");
				sendKeys(APT_NGINObj.NGIN.technicalcontactnametextfield,technicalcontactname,"technical contact name");
				
				verifyExists(APT_NGINObj.NGIN.typedropdown,"technical contact name");
				//sendKeys(APTIPAccessNoCPE.APTNoCPE.typedropdown,type,"technical contact name");
				addDropdownValues_commonMethod("Type",APT_NGINObj.NGIN.type,type);
				
				verifyExists(APT_NGINObj.NGIN.emailtextfield,"email");
				sendKeys(APT_NGINObj.NGIN.emailtextfield,email,"email");
				
				verifyExists(APT_NGINObj.NGIN.phonetextfield,"phone text field");
				sendKeys(APT_NGINObj.NGIN.phonetextfield,phone,"phone text field");
				
				verifyExists(APT_NGINObj.NGIN.faxtextfield,"fax text field");
				sendKeys(APT_NGINObj.NGIN.faxtextfield,fax,"fax text field");
				
				verifyExists(APT_NGINObj.NGIN.okbutton,"ok");
				click(APT_NGINObj.NGIN.okbutton,"ok");
				
				verifyExists(APT_NGINObj.NGIN.customercreationsuccessmsg,"Customer successfully created.");
				
			}
	
	public void selectCustomertocreateOrder(String ChooseCustomerToBeSelected)
			throws InterruptedException, IOException {
		

		mouseMoveOn(APT_NGINObj.NGIN.ManageCustomerServiceLink);

		verifyExists(APT_NGINObj.NGIN.CreateOrderServiceLink, "Create Order Service Link");
		click(APT_NGINObj.NGIN.CreateOrderServiceLink, "Create Order Service Link");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		// click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");

		verifyExists(APT_NGINObj.NGIN.entercustomernamefield, "name");
		sendKeys(APT_NGINObj.NGIN.entercustomernamefield, ChooseCustomerToBeSelected, "name");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"name");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"*","name");

		waitForAjax();

		addDropdownValues_commonMethod("Choose a customer",APT_NGINObj.NGIN.chooseCustomerdropdown, ChooseCustomerToBeSelected);
		// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

		verifyExists(APT_NGINObj.NGIN.nextbutton, "Next button");
		click(APT_NGINObj.NGIN.nextbutton, "Next button");

	}
	
	public void createorderservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderService");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderService");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		verifyExists(APT_NGINObj.NGIN.nextbutton, "Next button");
		click(APT_NGINObj.NGIN.nextbutton, "Next button");

		verifyExists(APT_NGINObj.NGIN.order_contractnumber_warngmsg,"order contract number");
		verifyExists(APT_NGINObj.NGIN.servicetype_warngmsg,"service type");

		if (neworder.equalsIgnoreCase("YES")) {
			verifyExists(APT_NGINObj.NGIN.newordertextfield, "new order textfield");
			sendKeys(APT_NGINObj.NGIN.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(APT_NGINObj.NGIN.newrfireqtextfield, "new rfireq textfield");
			sendKeys(APT_NGINObj.NGIN.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(APT_NGINObj.NGIN.createorderbutton, "create order button");
			click(APT_NGINObj.NGIN.createorderbutton, "create order button");
			
			waitForAjax();
			verifyExists(APT_NGINObj.NGIN.OrderCreatedSuccessMsg, "Order Created Success Msg");

		} else if (existingorderservice.equalsIgnoreCase("YES")) {
			verifyExists(APT_NGINObj.NGIN.selectorderswitch, "select order switch");
			click(APT_NGINObj.NGIN.selectorderswitch, "select order switch");

			verifyExists(APT_NGINObj.NGIN.existingorderdropdown, "existing order drop down");

			click(APT_NGINObj.NGIN.existingorderdropdown, "existing order dropdown");
			// sendKeys(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,existingordernumber,"existingordernumber");

			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
					APT_NGINObj.NGIN.existingorderdropdown, existingordernumber);
			waitForAjax();

		} else {
			Reporter.log("Order not selected");
		}

	}
	
	
	public void verifyselectservicetype(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		
		String CreateOrderServiceHeader = "Create Order / Service";
	//	String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
		// String
		// actualValue=getTextFrom(APT_IPTransitObj.APT_IPTransit.service);

		// select service type
		ScrollIntoViewByString(APT_NGINObj.NGIN.servicetypetextfield);

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		// click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.ServiceTypeWarningMessage,"Service
		// Type Warning Message");

		verifyExists(APT_NGINObj.NGIN.servicetypetextfield, "service type textfield");
		addDropdownValues_commonMethod("Service Type",APT_NGINObj.NGIN.servicetypetextfield, servicetype);

		verifyExists(APT_NGINObj.NGIN.nextbutton, "Next button");
		click(APT_NGINObj.NGIN.nextbutton, "Next button");

		(APT_NGINObj.NGIN.createorderservice_header).equals(CreateOrderServiceHeader);
		
		
		
	}
	
	
	public void verifyservicecreation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Remarks");
		String customadm = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerAdministration");
		String sanadm = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANAdministration");
		String reselladm = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerAdministration");
		String orderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String rfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
		String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		
		//compareText("Create Order / Service Header", "createorderservice_header", "Create Order / Service");
		
		verifyExists(APT_NGINObj.NGIN.cancelbutton,  "Cancel");
		click(APT_NGINObj.NGIN.cancelbutton,  "Cancel");
		
		waitforPagetobeenable();
		waitForAjax();
		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			Reporter.log("Navigated to create order page");
			System.out.println("Navigated to create order page");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Navigated to create order page");
			Report.LogInfo("INFO", "Navigated to create order page", "PASS");
		}

		//Create service
		scrollDown(APT_NGINObj.NGIN.createorderservice_header);
		waitForAjax();
		
		click(APT_NGINObj.NGIN.selectorderswitch, "select order switch");
		waitForAjax();
		
		addDropdownValues_commonMethod("Order/Contract Number(Parent SID)", APT_NGINObj.NGIN.existingorderdropdown, orderno);
		
		boolean availability1=false;
		try {  
			if(isVisible(APT_NGINObj.NGIN.servicetypetextfield)) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service Type dropdown is displaying");
				System.out.println("Service Type dropdown is displaying");
				Reporter.log("Service Type dropdown is displaying");

				if(servicetype.equalsIgnoreCase("null")) {

					ExtentTestManager.getTest().log(LogStatus.PASS, "No values selected under Service Type dropdown");
					System.out.println("No values selected under Service Type dropdown");
					Reporter.log("No values selected under Service Type dropdown");
				}else {

					webDriver.findElement(By.xpath("//div[label[text()='Service Type']]//div[text()='�']")).click();
					waitForAjax();

					//verify list of values inside dropdown
					List<WebElement> listofvalues = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));

					ExtentTestManager.getTest().log(LogStatus.PASS, "List of values inside Service Type dropdown is:  ");
					System.out.println("List of values inside Service Type dropdown is:  ");
					Reporter.log("List of values inside Service Type dropdown is:  ");

					for (WebElement valuetypes : listofvalues) {
						Reporter.log("service sub types : " + valuetypes.getText());
						ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
						System.out.println(" " + valuetypes.getText());
					}

					waitForAjax();
					
					sendKeys(APT_NGINObj.NGIN.servicetype, servicetype,"servicetype");	
					waitForAjax();

					//scrolltoend();
					scrollDown(APT_NGINObj.NGIN.nextbutton);
					waitForAjax();
					webDriver.findElement(By.xpath("(//div[text()='"+ servicetype +"'])[1]")).click();
					Thread.sleep(3000);

					String actualValue= webDriver.findElement(By.xpath(("//label[text()='Service Type']/following-sibling::div//span"))).getText();
					ExtentTestManager.getTest().log(LogStatus.PASS, "Service Type dropdown value selected as: "+ actualValue );
					System.out.println("Service Type dropdown value selected as: "+ actualValue);
					Reporter.log("Service Type dropdown value selected as: "+ actualValue);
				}
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Service Type is not displaying");
				System.out.println("Service Type is not displaying");
				Reporter.log("Service Type is not displaying");
			}
		}catch(NoSuchElementException e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Service Type is not displaying");
			System.out.println("Service Type is not displaying");
			Reporter.log("Service Type is not displaying");
		}catch(Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, "NOt able to perform selection under Service Type dropdown");
			System.out.println("NO value selected under Service Type dropdown");
			Reporter.log("NO value selected under Service Type dropdown");
		}

		// click on next button
		click(APT_NGINObj.NGIN.nextbutton, "Next");
		waitforPagetobeenable();
	//	compareText("Create Order / Service Header", "createorderservice_header", "Create Order / Service");

		// verify warning messages
		click(APT_NGINObj.NGIN.nextbutton, "Next");
		
		waitForAjax();
		
		verifyExists(APT_NGINObj.NGIN.sidwarngmsg, "Service Identification");
		// service identification
		sendKeys(APT_NGINObj.NGIN.serviceidentificationtextfield, sid, "Service Identification");
		// remarks
		sendKeys(APT_NGINObj.NGIN.remarktextarea,Remarks,"Remarks");
		
		// management options- Customer Administration Selection
		//addCheckbox_commonMethod(APT_NGINObj.NGIN.customeradministrationcheckbox, "customer administration checkbox", customadm);
		
//		if (customadm.equalsIgnoreCase("YES")) {
//			click_commonMethod(application, "customer administration checkbox", "customeradministrationcheckbox", xml);
//		} else {
//			System.out.println("customer administration checkbox is not selected");
//			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : customer administration checkbox is not selected");
//			Log.info("customer administration checkbox is not selected");
//		}
//
//		if (customadm.equalsIgnoreCase("NO")) {
//			System.out.println("customer administration checkbox is already selected");
//			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : customer administration checkbox is already selected");
//			Log.info("customer administration checkbox is already selected");
//		} else {
//			click_commonMethod(application, "customer administration checkbox", "customeradministrationcheckbox", xml);
//		}

		// management options- SAN Administration Selection
	//	click(APT_NGINObj.NGIN.sanadministrationcheckbox, "SAN administration checkbox");

//		if (sanadm.equalsIgnoreCase("YES")) {
//			click_commonMethod(application, "SAN administration checkbox", "sanadministrationcheckbox", xml);
//		} else {
//			System.out.println("SAN administration checkbox is not selected");
//			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : SAN administration checkbox is not selected");
//			Log.info("SAN administration checkbox is not selected");
//		}
//
//		if (sanadm.equalsIgnoreCase("NO")) {
//			System.out.println("SAN administration checkbox is already selected");
//			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : SAN administration checkbox is already selected");
//			Log.info("SAN administration checkbox is already selected");
//		} else {
//			click_commonMethod(application, "SAN administration checkbox", "sanadministrationcheckbox", xml);
//		}

		// management options- Reseller Administration Selection
		//click(APT_NGINObj.NGIN.reselleradministrationcheckbox, "Reseller Administration checkbox");
		
//		if (reselladm.equalsIgnoreCase("YES")) {
//			click_commonMethod(application, "Reseller Administration checkbox", "reselleradministrationcheckbox", xml);
//		} else {
//			System.out.println("Reseller Administration checkbox is not selected");
//			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : Reseller Administration checkbox is not selected");
//			Log.info("Reseller Administration checkbox is not selected");
//		}
//
//		if (reselladm.equalsIgnoreCase("NO")) {
//			System.out.println("Reseller Administration checkbox is already selected");
//			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : Reseller Administration checkbox is already selected");
//			Log.info("Reseller Administration checkbox is already selected");
//		} else {
//			click_commonMethod(application, "Reseller Administration checkbox", "reselleradministrationcheckbox", xml);
//		}

		
		click(APT_NGINObj.NGIN.nextbutton, "Next");
		//WebElement Nextbutton= findWebElement(APT_NGINObj.NGIN.nextbutton);
		//((JavascriptExecutor)WebDriver). executeScript("arguments[0]. click();", Nextbutton);
		waitforPagetobeenable();
		//verifysuccessmessage( "Service successfully created");
	
	}
	
	public void verifyCustomerDetailsInformation(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"newCustomerCreation");
		String existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomerSelection");
		String newCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
		String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MainDomain");

		if (newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) {
			verifyExists(APT_NGINObj.NGIN.Name_Value, "Name Value");
			String nameValue = getTextFrom(APT_NGINObj.NGIN.Name_Value, "Name Value");
			nameValue.equalsIgnoreCase(newCustomer);

			verifyExists(APT_NGINObj.NGIN.Country_Value, "Country");
			String countryValue = getTextFrom(APT_NGINObj.NGIN.Country_Value, "Country");
			countryValue.equalsIgnoreCase(country);

			verifyExists(APT_NGINObj.NGIN.OCN_Value, "OCN");
			String OCN = getTextFrom(APT_NGINObj.NGIN.OCN_Value, "OCN");
			OCN.equalsIgnoreCase(ocn);

			verifyExists(APT_NGINObj.NGIN.Reference_Value, "Reference");
			String Reference = getTextFrom(APT_NGINObj.NGIN.Reference_Value, "Reference");
			Reference.equalsIgnoreCase(reference);

			verifyExists(APT_NGINObj.NGIN.TechnicalContactName_Value, "Technical Contact Name");
			String technicalContactName = getTextFrom(APT_NGINObj.NGIN.TechnicalContactName_Value, "Technical Contact Name");
			technicalContactName.equalsIgnoreCase(technicalcontactname);

			verifyExists(APT_NGINObj.NGIN.Type_Value, "Type");
			String Type = getTextFrom(APT_NGINObj.NGIN.Type_Value, "Type");
			Type.equalsIgnoreCase(type);

			verifyExists(APT_NGINObj.NGIN.Email_Value, "Email");
			String Email = getTextFrom(APT_NGINObj.NGIN.Email_Value, "Email");
			Email.equalsIgnoreCase(email);

			verifyExists(APT_NGINObj.NGIN.Phone_Value, "Phone");
			String Phone = getTextFrom(APT_NGINObj.NGIN.Phone_Value, "Phone");
			Phone.equalsIgnoreCase(phone);

			verifyExists(APT_NGINObj.NGIN.Fax_Value, "Fax");
			String Fax = getTextFrom(APT_NGINObj.NGIN.Fax_Value, "Fax");
			Fax.equalsIgnoreCase(fax);
		} else if (newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes")) {
			verifyExists(APT_NGINObj.NGIN.Name_Value, "Fax");
			String ExistingCustomer = getTextFrom(APT_NGINObj.NGIN.Name_Value, "Fax");
			ExistingCustomer.equalsIgnoreCase(existingCustomer);
		}
		// Main Domain
		if (maindomain.equalsIgnoreCase("Null")) {
			Reporter.log("A default displays for main domain field, if no provided while creating customer");

		} else {
			verifyExists(APT_NGINObj.NGIN.MainDomain_Value, "Main Domain");
			String Maindomain = getTextFrom(APT_NGINObj.NGIN.MainDomain_Value, "Main Domain");
			Maindomain.equalsIgnoreCase(maindomain);
		}
		Reporter.log("Customer Details panel fields Verified");
	}
	
	
	public void verifyUserDetailsInformation(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String Logincolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.LoginColumn, "LoginColumn");
		String Namecolumn = getTextFrom(APT_IPTransitObj.APT_IPTransit.NameColumn, "NameColumn");
		String Email = getTextFrom(APT_IPTransitObj.APT_IPTransit.EmailColumn, "EmailColumn");
		String Roles = getTextFrom(APT_IPTransitObj.APT_IPTransit.RolesColumn, "RolesColumn");
		String Address = getTextFrom(APT_IPTransitObj.APT_IPTransit.AddressColumn, "AddressColumn");
		String Resource = getTextFrom(APT_IPTransitObj.APT_IPTransit.ResourcesColumn, "ResourceColumn");
		
		// verify table column names
				compareText("Login column",APT_NGINObj.NGIN.LoginColumn, Logincolumn );
				compareText("Name column", APT_NGINObj.NGIN.NameColumn, Namecolumn);
				compareText( "Email column",APT_NGINObj.NGIN.EmailColumn, Email);
				compareText("Roles column",APT_NGINObj.NGIN.RolesColumn, Roles);
				compareText("Address column",APT_NGINObj.NGIN.AddressColumn, Address);
				compareText("Resource column",APT_NGINObj.NGIN.ResourcesColumn, Resource);
				
	}
	
	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String sid = getTextFrom(APT_IPTransitObj.APT_IPTransit.LoginColumn, "ServiceIdentification");
		String servicetype = getTextFrom(APT_IPTransitObj.APT_IPTransit.NameColumn, "ServiceType");
		String Remarks = getTextFrom(APT_IPTransitObj.APT_IPTransit.EmailColumn, "Remarks");
		
		scrollDown(APT_NGINObj.NGIN.orderpanelheader);
		waitForAjax();
		
		//compareText(application, "Service panel Header", "servicepanel_header", "Service", xml);
		verifyExists(APT_NGINObj.NGIN.servicepanel_header,"Service panel Header");
		verifyExists(APT_NGINObj.NGIN.servicepanel_serviceidentificationvalue, "Service Identification");
		verifyExists(APT_NGINObj.NGIN.servicepanel_servicetypevalue, "Service Type");
		verifyExists(APT_NGINObj.NGIN.servicepanel_remarksvalue, "Remarks");
		
		
	}
	public void verifyManagementOptionspanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String ManagementOptionsHeader = "Management Options";
		String customadm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CustomerAdministration");
		String sanadm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SANAdministration");
		String reselladm = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ResellerAdministration");

		scrollUp();
		ScrollIntoViewByString(APT_IPTransitObj.APT_IPTransit.managementoptions_header);

		// ((JavascriptExecutor)
		// driver).executeScript("window.scrollBy(0,-100)");

		(APT_NGINObj.NGIN.managementoptions_header).equals(ManagementOptionsHeader);
		(APT_NGINObj.NGIN.customeradmin_value).equals(customadm);
		(APT_NGINObj.NGIN.sanadmin_value).equals(sanadm);
		(APT_NGINObj.NGIN.reselleradmin_value).equals(reselladm);

	}
	
	public void verifyEditService(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String EditRemarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditRemarks");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Remarks");
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceIdentification");
		String servicetype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
		
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.orderpanelheader);
		
		waitForAjax();
		
		click(APT_NGINObj.NGIN.serviceactiondropdown,"Action dropdown");
		click(APT_NGINObj.NGIN.edit,"Edit");
		
		clearTextBox(APT_NGINObj.NGIN.remarktextarea);
		sendKeys(APT_NGINObj.NGIN.remarktextarea, EditRemarks, "Remarks");
		
		click(APT_NGINObj.NGIN.cancelbutton, "Cancel");
		
		waitForAjax();
		waitforPagetobeenable();
		
		scrollDown(APT_NGINObj.NGIN.orderpanelheader);
		waitForAjax();
		
		if(isVisible(APT_NGINObj.NGIN.servicepanel_header))
		{
			compareText("Remarks", APT_NGINObj.NGIN.servicepanel_remarksvalue, Remarks);	
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Didn't navigate to view service page");
			Report.LogInfo("INFO", "Didn't navigate to view service page", "PASS");
		}
		//Edit service
		click(APT_NGINObj.NGIN.serviceactiondropdown, "Action dropdown");
		
		click(APT_NGINObj.NGIN.edit,  "Edit");
		
		waitforPagetobeenable();
		
		clearTextBox(APT_NGINObj.NGIN.remarktextarea);
		sendKeys(APT_NGINObj.NGIN.remarktextarea, EditRemarks, "Remarks");
		
		scrollDown(APT_NGINObj.NGIN.okbutton);
		waitForAjax();
		
		click(APT_NGINObj.NGIN.okbutton, "OK");
		waitForAjax();
		waitforPagetobeenable();
		
		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			
			Report.LogInfo("INFO", "Navigated to view service page", "PASS");
			Report.LogInfo("INFO", "Service successfully updated", "PASS");

			//verifysuccessmessage(application, "Service successfully updated");
		}
		else
		{
			
			Report.LogInfo("INFO", "Service not updated", "PASS");

		}

		
	}
	
	public void verifyManageService(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String Sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
		String serviceType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
		String servicestatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceStatus");
		String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ServiceStatusChangeRequired");
		String syncstatus = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"syncstatus");


		verifyExists(APT_NGINObj.NGIN.serviceactiondropdown, "service action dropdown");
		click(APT_NGINObj.NGIN.serviceactiondropdown, "service action dropdown");

		verifyExists(APT_NGINObj.NGIN.manageLink, "manage");
		click(APT_NGINObj.NGIN.manageLink, "manage");
		
		waitforPagetobeenable();

		verifyExists(APT_NGINObj.NGIN.manageservice_header, "manage service");

		verifyExists(APT_NGINObj.NGIN.status_servicename, "service name");
		String sid = getTextFrom(APT_NGINObj.NGIN.status_servicename, "space name");
		sid.equalsIgnoreCase(Sid);

		verifyExists(APT_NGINObj.NGIN.status_servicetype, "service type");
		String servicetype = getTextFrom(APT_NGINObj.NGIN.status_servicetype, "service type");
		servicetype.equalsIgnoreCase(serviceType);

		String ServiceDetails_value = getTextFrom(APT_NGINObj.NGIN.status_servicedetails,"Details");

		if (isEmptyOrNull(ServiceDetails_value)) {
			Reporter.log("Service Details column value is empty as expected");

		} else {
			Reporter.log("Service Details column value should be empty");
		}

		String serviceStatus = getTextFrom(APT_NGINObj.NGIN.status_currentstatus,"status_current status");
		serviceStatus.equalsIgnoreCase(servicestatus);

		String LastModificationTime_value = getTextFrom(APT_NGINObj.NGIN.status_modificationtime,"status_modification time");

		if (LastModificationTime_value.contains("BST")) {
			Reporter.log("Service status is displayed as");

		}

		else {
			Reporter.log("Incorrect modification time format");

		}
		verifyExists(APT_NGINObj.NGIN.statuslink, "Status");
		click(APT_NGINObj.NGIN.statuslink, "Status");

		if (servicestatuschangerequired == "Yes") {

			if (isClickable(APT_NGINObj.NGIN.Servicestatus_popup)) {
				verifyExists(APT_NGINObj.NGIN.changestatus_dropdown, "change status");
				click(APT_NGINObj.NGIN.changestatus_dropdown, "change status");

				verifyExists(APT_NGINObj.NGIN.changestatus_dropdownvalue, "change status");
				click(APT_NGINObj.NGIN.changestatus_dropdownvalue, "change status");

				verifyExists(APT_NGINObj.NGIN.okbutton, "ok");
				click(APT_NGINObj.NGIN.okbutton, "ok");

				if (isClickable(APT_NGINObj.NGIN.servicestatushistory)) {
					Reporter.log("Service status change request logged");
				} else {
					Reporter.log(" Service status change request is not logged");
				}
			} else {
				Reporter.log(" Status link is not working");

			}
		} else {
			Reporter.log("Service status change not reqired");
			verifyExists(APT_NGINObj.NGIN.servicestatus_popupclose, "service status");
			click(APT_NGINObj.NGIN.servicestatus_popupclose, "service status");
		}

		String syncServicename = getTextFrom(APT_NGINObj.NGIN.sync_servicename,"sync_service name");
		syncServicename.equalsIgnoreCase(sid);

		String syncServiceType = getTextFrom(APT_NGINObj.NGIN.sync_servicetype,"sync_service type");
		syncServiceType.equalsIgnoreCase(serviceType);

		if (isEmptyOrNull(APT_NGINObj.NGIN.sync_servicedetails)) {
			Reporter.log("Service Details column value is empty as expected");

		} else {
			Reporter.log("Service Details column value should be empty");

		}

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.managepage_backbutton,"manage
		// page back button");
		// click(APTIPAccessNoCPE.APTNoCPE.managepage_backbutton,"manage page
		// back button");
	}
	
	public void searchservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException

	{
		waitforPagetobeenable();
		waitForAjax();
		String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");

		verifyExists(APT_NGINObj.NGIN.ManageCustomerServiceLink, " Manage Customer Service Link");
		mouseMoveOn(APT_NGINObj.NGIN.ManageCustomerServiceLink);

		verifyExists(APT_NGINObj.NGIN.searchorderlink, "search order link");
		click(APT_NGINObj.NGIN.searchorderlink, "search order link");

		verifyExists(APT_NGINObj.NGIN.servicefield, "service field");
		sendKeys(APT_NGINObj.NGIN.servicefield, sid);

		click(APT_NGINObj.NGIN.searchbutton, "searchbutton");
		// click(searchbutton,"searchbutton");

		verifyExists(APT_NGINObj.NGIN.serviceradiobutton, "service radio button");
		click(APT_NGINObj.NGIN.serviceradiobutton, "service radio button");

		verifyExists(APT_NGINObj.NGIN.searchorder_actiondropdown, "search order actiodropdown");
		click(APT_NGINObj.NGIN.searchorder_actiondropdown, "search order linksearch order actiodropdown");

		verifyExists(APT_NGINObj.NGIN.view, "view");
		click(APT_NGINObj.NGIN.view, "view");

	}
	
public void verifySynchronizeLink() throws InterruptedException, IOException 
{
		
		scrollDown(APT_NGINObj.NGIN.orderpanelheader);
		waitForAjax();
		click(APT_NGINObj.NGIN.serviceactiondropdown,"Action dropdown");
		click(APT_NGINObj.NGIN.synchronizelink_servicepanel, "Synchronize");
		scrollDown(APT_NGINObj.NGIN.customerdetailsheader);
		waitForAjax();
		//verifysuccessmessage(application, "Sync started successfully. Please check the sync status of this service.");
	}


public void verifyBulkInterface(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String bulkjob_filepath = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "BulkJob_FilePath");
	String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
	
	//cancel bulk interface
			scrollDown(APT_NGINObj.NGIN.orderpanelheader);
			waitForAjax();
			click(APT_NGINObj.NGIN.serviceactiondropdown, "Action dropdown");
			click(APT_NGINObj.NGIN.bulkinterfacelink, "Bulk Interface");
			waitforPagetobeenable();
			verifyExists(APT_NGINObj.NGIN.bulkinterfaceheader,"Bulk Interface Header");
			click(APT_NGINObj.NGIN.bulkjobcancel, "Cancel");
			waitforPagetobeenable();
			
			if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
			{
				
				Report.LogInfo("INFO", "Navigated to view service page", "PASS");
			}

			//submit bulk job
			scrollDown(APT_NGINObj.NGIN.orderpanelheader);
			waitForAjax();
			click(APT_NGINObj.NGIN.serviceactiondropdown, "Action dropdown");
			click(APT_NGINObj.NGIN.bulkinterfacelink, "Bulk Interface");
			waitforPagetobeenable();
			verifyExists(APT_NGINObj.NGIN.bulkinterfaceheader,"Bulk Interface Header");
		//	WebElement BulkJob_Choosefile_button= getwebelement(xml.getlocator("//locators/" + application + "/bulkjob_choosefilebutton"));
		//	BulkJob_Choosefile_button.sendKeys(bulkjob_filepath);
		//	sendKeys(APT_NGINObj.NGIN.bulkjob_choosefilebutton,bulkjob_filepath,"bulkjob_filepath");
			String path1 = new File(".").getCanonicalPath();
			String uploadFilePath = path1+"/NGINBulkNew.csv";
			
			sendKeys(APT_NGINObj.NGIN.bulkjob_choosefilebutton, uploadFilePath,"ChosefileDDI");
			click(APT_NGINObj.NGIN.bulkjobsubmit,  "Submit");
			waitforPagetobeenable();
			verifysuccessmessage("FRC Numbers sent to Queue for Creation. Please check the bulk operation of SANs here.");
			
			//Archive link in bulk interface page
			click(APT_NGINObj.NGIN.bulkinterface_actiondropdown, "Action dropdown");
			click(APT_NGINObj.NGIN.bulkinterface_archivelink, "Archive");
			waitforPagetobeenable();
			verifysuccessmessage( "Service Status History Found Successfully.");
			scrollDown(APT_NGINObj.NGIN.bulkinterfacepage_cancel);
			waitForAjax();
			click(APT_NGINObj.NGIN.bulkinterfacepage_cancel, "Cancel");
			waitForAjax();
			waitforPagetobeenable();
			//Refresh link in bulk interface page
			scrollDown(APT_NGINObj.NGIN.bulkinterface_actiondropdown);
			
			waitForAjax();
			
			click(APT_NGINObj.NGIN.bulkinterface_actiondropdown, "Action dropdown");
			
			click(APT_NGINObj.NGIN.bulkinterface_refreshlink, "Refresh");
			
			waitforPagetobeenable();
			
			compareText("Bulk Interface Header", APT_NGINObj.NGIN.bulkinterfaceheader, "Bulk Interface");
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Bulk Interface page refresh successful");
			
			Reporter.log("Bulk Interface page refresh successful");
			
			waitForAjax();
			
			scrollDown(APT_NGINObj.NGIN.bulkinterfacepage_cancel);
			
			click(APT_NGINObj.NGIN.bulkinterfacepage_cancel, "Cancel");
			
			waitForAjax();
			waitforPagetobeenable();

	
}

public void verifyResellerpanel() throws InterruptedException, IOException {

//	ScrolltoElement(application, "resellerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.managementoptions_header);
	waitForAjax();
	compareText("Reseller", APT_NGINObj.NGIN.resellerheader, "Reseller");
	
	click(APT_NGINObj.NGIN.ResellerActionDropdown,  "Action dropdown");

	if(isVisible(APT_NGINObj.NGIN.AddLink))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Add link is displaying under Reseller panel");
		
		Report.LogInfo("INFO", "Add link is displaying under Reseller panel", "PASS");
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Add link is not displaying under Reseller panel");
		Report.LogInfo("INFO", "Add link is not displaying under Reseller panel", "PASS");
	}

	click(APT_NGINObj.NGIN.resellerheader,"resellerheader");

}
public static String ResellerName;

public void AddReseller(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Email");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_City");
	String streetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetName");
	String streetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetNumber");
	String pobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_POBox");
	String zipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Zipcode");
	String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Fax");
	
	
	scrollDown( APT_NGINObj.NGIN.managementoptions_header);
	compareText("Reseller", APT_NGINObj.NGIN.resellerheader, "Reseller");

	// verify customer name column
	compareText("Customer Name", APT_NGINObj.NGIN.reseller_customername_column, "Customer Name" );
	// verify status column
	//GetText(application, "Status", "statuscolumn");
	// verify Add link in Reseller action dropdown
	//Cancel add reseller
	click( APT_NGINObj.NGIN.ResellerActionDropdown, "Action dropdown");
	click(APT_NGINObj.NGIN.AddLink, "Add");
	waitforPagetobeenable();
	compareText("Manage Reseller in OSP", APT_NGINObj.NGIN.manageresellerheader, "Manage Reseller in OSP");
	click(APT_NGINObj.NGIN.cancelbutton, "Cancel");
	waitforPagetobeenable();
	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Report.LogInfo("INFO", "Navigated to view service page", "PASS");

	}

	//warning msg verfiy in reseller panel
//	ScrolltoElement(application, "resellerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.managementoptions_header);
	click(APT_NGINObj.NGIN.ResellerActionDropdown, "Action dropdown");
	click(APT_NGINObj.NGIN.AddLink, "Add");
	waitforPagetobeenable();
	compareText("Manage Reseller in OSP", APT_NGINObj.NGIN.manageresellerheader,"Manage Reseller in OSP");
	
	click(APT_NGINObj.NGIN.Next_Button,"Next");
	
	verifyExists(APT_NGINObj.NGIN.reselleremail_warngmsg, "Email");
	verifyExists( APT_NGINObj.NGIN.resellercity_warngmsg, "City");
	verifyExists( APT_NGINObj.NGIN.resellerstreetname_warngmsg, "Street Name");
	verifyExists( APT_NGINObj.NGIN.resellerstreetno_warngmsg, "Street Number");
	verifyExists( APT_NGINObj.NGIN.resellerpobox_warngmsg, "PO Box");
	verifyExists( APT_NGINObj.NGIN.resellerzipcode_warngmsg, "Zip Code");
	verifyExists( APT_NGINObj.NGIN.resellerphone_warngmsg, "Phone");

	//Add reseller
	scrollDown(APT_NGINObj.NGIN.manageresellerheader);
	waitForAjax();
	String resellername= getAttributeFrom(APT_NGINObj.NGIN.resellername,"value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Reseller Name is displaying as: '"+resellername+"'");
	Reporter.log("Reseller Name is displaying as: '"+resellername+"'");
	
	sendKeys(APT_NGINObj.NGIN.reseller_email, email, "Email");
	sendKeys(APT_NGINObj.NGIN.reseller_city, city,"City");
	sendKeys(APT_NGINObj.NGIN.reseller_streetname, streetname, "Street Name");
	sendKeys(APT_NGINObj.NGIN.reseller_streetno, streetno, "Street Number");
	sendKeys(APT_NGINObj.NGIN.reseller_pobox, pobox, "PO Box");
	sendKeys(APT_NGINObj.NGIN.reseller_zipcode, zipcode, "Zip Code");
	sendKeys(APT_NGINObj.NGIN.reseller_phone, phone,  "Phone");
	sendKeys(APT_NGINObj.NGIN.reseller_fax, fax, "Fax");
	
	scrollDown(APT_NGINObj.NGIN.Next_Button);
	waitForAjax();
	click(APT_NGINObj.NGIN.Next_Button, "Next");
	
	waitForAjax();
	waitforPagetobeenable();
	
	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Reporter.log("Navigated to view service page");
		Reporter.log("Reseller successfully created. Please check the Reseller Status for association details.");

		verifysuccessmessage("Reseller successfully created. Please check the Reseller Status for association details.");
	}
	else
	{
		Reporter.log("Reseller not created");
		System.out.println("Reseller not created");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Reseller not created");
	}

	//Added Reseller
//	ScrolltoElement(application, "resellerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.managementoptions_header);
	
	//WebElement ResellerGridCheck= getwebelement("(//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String ResellerGrid= getAttributeFrom(APT_NGINObj.NGIN.ResellerGridCheck,"style");
	WebElement AddedReseller= webDriver.findElement(By.xpath("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + ocn + "')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!ResellerGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + ocn + "')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.ResellerActionDropdown, "Action dropdown");
		List<WebElement> ResellerLinks= webDriver.findElements(By.xpath("//div[contains(text(),'Reseller')]/following-sibling::div//div//a"));
		int ResellerLinksCount= ResellerLinks.size();
		for(int i=0;i<ResellerLinksCount;i++)
		{
			String Link= ResellerLinks.get(i).getText();
			ExtentTestManager.getTest().log(LogStatus.PASS, ""+Link+" link is displaying under reseller panel");
			System.out.println("Reseller link:"+ Link + " is displaying");
			Reporter.log("Reseller link:"+ Link + " is displaying");
			waitForAjax();
		}
		//Added Reseller grid verification
		compareText("Added Reseller Name", APT_NGINObj.NGIN.Addedreseller_columnvalue, ocn);
		
	}
	else
	{
		Reporter.log("Reseller is not added in the grid");
		System.out.println("Reseller is not added in the grid");
		Report.LogInfo("INFO", "Reseller is not added in the grid", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Reseller is not added in the grid");
	}
//	ResellerName= ocn;

}


public void verifysuccessmessage(String expected) throws InterruptedException {
	
	scrollDown(APT_NGINObj.NGIN.alertMsg);
	waitForAjax();
	try {	
		
		//boolean successMsg=(isVisible(APT_NGINObj.NGIN.alertMsg));

		if(isVisible(APT_NGINObj.NGIN.alertMsg)){
			
			String alrtmsg=getTextFrom(APT_NGINObj.NGIN.AlertForServiceCreationSuccessMessage,"Success message");
			
			if(expected.contains(alrtmsg)) {
				Report.LogInfo("INFO", "Message is verified. It is displaying as: "+alrtmsg, "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,"Message is verified. It is displaying as: "+alrtmsg);
				System.out.println("Message is verified. It is displaying as: "+alrtmsg);
				//successScreenshot(application);
				
			}else {
				Report.LogInfo("INFO", "Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg +" .The Expected value is: "+ expected, "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg +" .The Expected value is: "+ expected);
				System.out.println("Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg);
				//successScreenshot(application);
			}
			
		}else {
			Report.LogInfo("INFO", " Success Message is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");
			System.out.println(" Success Message is not displaying");
		}
		
	}catch(Exception e) {
		Reporter.log("failure in fetching success message");
		Report.LogInfo("INFO", expected+ " Message is not displaying", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, expected+ " Message is not displaying");
		System.out.println(expected+ " message is not getting dislpayed");
		//successScreenshot(application);
	}

}


public void verify_ViewReseller(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Email");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_City");
	String streetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetName");
	String streetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetNumber");
	String pobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_POBox");
	String zipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Zipcode");
	String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Fax");
	
	scrollDown(APT_NGINObj.NGIN.managementoptions_header);
	//WebElement ResellerGridCheck= getwebelement("(//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String ResellerGrid=getAttributeFrom(APT_NGINObj.NGIN.ResellerGridCheck,"style");
	//WebElement AddedReseller= getwebelement("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ ocn +"')]/parent::div//span[contains(@class,'unchecked')]");
	
	//View Reseller
	if(!ResellerGrid.contains("height: 1px"))
	{
		//Clickon(AddedReseller);
		//Thread.sleep(1000);
		//click_commonMethod(application, "Action dropdown", "ResellerActionDropdown", xml);
		waitForAjax();
	if(isVisible(APT_NGINObj.NGIN.view))
	{
		click(APT_NGINObj.NGIN.view, "View");
		
		waitForAjax();
		
		scrollDown(APT_NGINObj.NGIN.manageresellerheader_viewpage);
		waitforPagetobeenable();
		//compareText(application, "Manage Reseller Header", "manageresellerheader_viewpage", "Manage Reseller In OSP", xml);
		//GetText(application, "Manage Reseller Header", "manageresellerheader_viewpage");
		compareText("Reseller Name", APT_NGINObj.NGIN.resellernamevalue, ocn);
		compareText( "Email", APT_NGINObj.NGIN.reselleremailvalue, email);
		compareText( "City", APT_NGINObj.NGIN.resellercityvalue, city);
		compareText( "Street Name", APT_NGINObj.NGIN.resellerstreetnamevalue, streetname );
		compareText("Street Number", APT_NGINObj.NGIN.resellerstreetnovalue, streetno);
		compareText( "PO Box", APT_NGINObj.NGIN.resellerpoboxvalue, pobox);
		compareText( "Zipcode", APT_NGINObj.NGIN.resellerzipcodevalue, zipcode);
		compareText( "Phone", APT_NGINObj.NGIN.resellerphonevalue, phone);
		compareText( "Fax", APT_NGINObj.NGIN.resellerfaxvalue, fax);
		//GetText(application, "Web Access Authorized", "resellerwebaccessvalue");
		waitForAjax();
		
		scrollDown(APT_NGINObj.NGIN.viewpage_actiondropdown);
		
		waitForAjax();
		
		click(APT_NGINObj.NGIN.viewpage_actiondropdown, "View page Action dropdown");

		//Edit link in view reseller page
		click(APT_NGINObj.NGIN.edit, "View page Edit");
		
		waitforPagetobeenable();
		//compareText(application, "Manage Reseller Header", "manageresellerheader", "Manage Reseller In OSP", xml);
	//	GetText(application, "Manage Reseller Header", "manageresellerheader");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Edit Reseller page is displaying as expected");
		Reporter.log("Edit Reseller page is displaying as expected");
		scrollDown(APT_NGINObj.NGIN.cancelbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.cancelbutton, "Cancel");
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			Reporter.log("Navigated to view service page");
		}
	}
	else
	{
		Report.LogInfo("INFO", "Step : View link is not displaying under Reseller panel", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : View link is not displaying under Reseller panel");
		Reporter.log("View link is not displaying under Reseller panel");
	}
	}
	else
	{
		Reporter.log("Reseller is not added in the grid");
		Report.LogInfo("INFO", "Reseller is not added in the grid", "FAIL");

		ExtentTestManager.getTest().log(LogStatus.FAIL, "Reseller is not added in the grid");
	}
//	ScrolltoElement(application, "resellerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.managementoptions_header);
	WebElement AddedReseller2= webDriver.findElement(By.xpath("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ ocn +"')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!ResellerGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ ocn +"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.ResellerActionDropdown, "Action dropdown");
		if(isVisible(APT_NGINObj.NGIN.view))
		{
			click(APT_NGINObj.NGIN.view, "View");
			waitforPagetobeenable();
			//GetText(application, "Manage Reseller Header", "manageresellerheader_viewpage");
			scrollDown(APT_NGINObj.NGIN.viewpage_actiondropdown);
			click(APT_NGINObj.NGIN.viewpage_actiondropdown,  "View page Action dropdown");

			//Delete link in view reseller page
			click(APT_NGINObj.NGIN.delete, "View page Delete");
			waitForAjax();
			if(isVisible(APT_NGINObj.NGIN.delete_alertpopup))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Delete Reseller alert is displaying as expected");
				Reporter.log("Delete Reseller alert is displaying as expected");
				click(APT_NGINObj.NGIN.deletealertclose,"delete alert close");
				
				waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on delete alert popup close button");
				Reporter.log("clicked on delete alert popup close button");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Delete alert popup is not displayed");
				Reporter.log("Delete alert popup is not displayed");
			}

		//Associate Reseller with NGIN Objects link in view reseller page
		click(APT_NGINObj.NGIN.viewpage_actiondropdown, "View page Action dropdown");
		
		click(APT_NGINObj.NGIN.associateresellerlink, "Associate Reseller with NGIN Objects");
		
		waitForAjax();
		
		waitforPagetobeenable();
		
		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			Reporter.log("Navigated to view service page");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Associate Reseller with NGIN Objects link verified");
		}

	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : View link is not displaying under Reseller panel");
		Reporter.log("View link is not displaying under Reseller panel");
	}
}
else
{
	Reporter.log("Reseller is not added in the grid");
	Report.LogInfo("INFO", "Reseller is not added in the grid", "FAIL");
	ExtentTestManager.getTest().log(LogStatus.FAIL, "Reseller is not added in the grid");
}
	
}


public void verify_EditReseller(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
	String editemail = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditEmail");
	String editcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditCity");
	String editstreetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditStreetName");
	String editstreetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditStreetNumber");
	String editpobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditPOBox");
	String editzipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditZipcode");
	String editphone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditPhone");
	String editfax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditFax");
	
	scrollDown(APT_NGINObj.NGIN.managementoptions_header);
	//WebElement ResellerGridCheck= getwebelement("(//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String ResellerGrid=getAttributeFrom(APT_NGINObj.NGIN.ResellerGridCheck,"style");
	//WebElement AddedReseller= getwebelement("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ ocn +"')]/parent::div//span[contains(@class,'unchecked')]");
	
	//View Reseller
	if(!ResellerGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ ocn +"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.ResellerActionDropdown, "Action dropdown");
	
		click(APT_NGINObj.NGIN.edit, "Edit");
		waitForAjax();
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.manageresellerheader);
		//GetText(application, "Manage Reseller Header", "manageresellerheader");
		sendKeys(APT_NGINObj.NGIN.reseller_email, editemail, "Email");
		sendKeys(APT_NGINObj.NGIN.reseller_city, editcity,  "City");
		
		scrollDown(APT_NGINObj.NGIN.reseller_streetname);
		waitForAjax();
		
		sendKeys(APT_NGINObj.NGIN.reseller_streetname, editstreetname, "Street Name");
		sendKeys(APT_NGINObj.NGIN.reseller_streetno, editstreetno, "Street Number");
		sendKeys(APT_NGINObj.NGIN.reseller_pobox, editpobox, "PO Box");
		sendKeys(APT_NGINObj.NGIN.reseller_zipcode, editzipcode, "Zip Code");
		sendKeys(APT_NGINObj.NGIN.reseller_phone, editphone, "Phone");
		sendKeys(APT_NGINObj.NGIN.reseller_fax, editfax, "Fax");
		scrollDown(APT_NGINObj.NGIN.okbutton);
		click(APT_NGINObj.NGIN.okbutton, "OK");
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			Reporter.log("Navigated to view service page");
			//ScrolltoElement(application, "editreseller_successmsg", xml);
			verifysuccessmessage( "Reseller already in OSP. Successfully updated.");
		}
		else
		{
			Reporter.log("Reseller not updated");
			System.out.println("Reseller not updated");
		}
	}
	else
	{
		Reporter.log("Reseller is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Reseller is not added in the grid");

	}
	
}

public void verifyCustomerpanel() throws InterruptedException, IOException 
{

//	ScrolltoElement(application, "customerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.resellerheader);
	compareText("Customer panel header", APT_NGINObj.NGIN.customerheader, "Customer");
	click(APT_NGINObj.NGIN.CustomerpanelActionDropdown,"Action dropdown" );
	
	if(isVisible(APT_NGINObj.NGIN.AddLink))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Add link is displaying under Customer panel");
		Reporter.log("Add link is displaying under Customer panel");
	}
	else
	{
		Report.LogInfo("INFO", "Step : Add link is not displaying under Customer panel", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Add link is not displaying under Customer panel");
		Reporter.log("Add link is not displaying under Customer panel");
	}

	click(APT_NGINObj.NGIN.customerheader,"customerheader");
}

String Customername=null;
public void AddCustomer(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String resellername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerName");
	String defaultvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DefaultValue_checkbox");
	String configure = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Configure_checkbox");
	String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer_Country");
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
	String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_City");
	String streetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetName");
	String streetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetNumber");
	String pobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_POBox");
	String zipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Zipcode");
	
	
	scrollDown(APT_NGINObj.NGIN.resellerheader);
	compareText("Customer panel header", APT_NGINObj.NGIN.customerheader, "Customer");

	// verify customer name column
	compareText( "Customer Name column", APT_NGINObj.NGIN.customerpanel_customernamecolumntext, "Customer Name");

	// verify Add link in customer action dropdown
	//Cancel add customer
	click(APT_NGINObj.NGIN.CustomerpanelActionDropdown, "Action dropdown");
	click(APT_NGINObj.NGIN.AddLink, "Add");
	
	waitForAjax();
	
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.manageresellerheader);
	waitForAjax();
	
	compareText( "Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
	scrollDown(APT_NGINObj.NGIN.cancelbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.cancelbutton, "Cancel");
	waitForAjax();
	waitforPagetobeenable();
	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Reporter.log("Navigated to view service page");
	}

	//Warning msgs verify in Add customer page
//	ScrolltoElement(application, "customerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.resellerheader);
	waitForAjax();
	
	click(APT_NGINObj.NGIN.CustomerpanelActionDropdown,"Action dropdown");
	click(APT_NGINObj.NGIN.AddLink, "Add");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.manageresellerheader);
	waitForAjax();
	compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
	
	scrollDown(APT_NGINObj.NGIN.nextbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.nextbutton,"nextbutton");
	waitForAjax();
	
	verifyExists( APT_NGINObj.NGIN.addcustomer_country_warningmsg, "Country");
	verifyExists( APT_NGINObj.NGIN.addcustomer_customername_warningmsg, "Customer Name");
	verifyExists( APT_NGINObj.NGIN.addcustomer_city_warningmsg, "City");
	verifyExists( APT_NGINObj.NGIN.addcustomer_streetname_warningmsg, "Street Name");
	verifyExists( APT_NGINObj.NGIN.addcustomer_streetno_warningmsg, "Street Number");
	verifyExists( APT_NGINObj.NGIN.addcustomer_pobox_warningmsg, "PO Box");
	verifyExists( APT_NGINObj.NGIN.addcustomer_zipcode_warningmsg, "ZIP Code");

	//Add Customer
	//Select Country from dropdown
	scrollDown( APT_NGINObj.NGIN.manageresellerheader);
	waitForAjax();
	addDropdownValues_commonMethod( "Country", APT_NGINObj.NGIN.customer_country, country);
	String CustomerOCN= getAttributeFrom(APT_NGINObj.NGIN.customer_ocn,"value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : OCN is displaying as: '"+CustomerOCN+"'");
	Reporter.log("OCN is displaying as: '"+CustomerOCN+"'");
	String CustomerNamevalue= getTextFrom(APT_NGINObj.NGIN.customerpanel_customernamevalue,"customer panel_customer namevalue");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer Name is displaying as: '"+CustomerNamevalue+"'");
	Reporter.log("Customer Name is displaying as: '"+CustomerNamevalue+"'");
	String CustomerName_viewpage= CustomerNamevalue.replace("(New)", "").trim();
	System.out.println("Customer name is :"+CustomerName_viewpage);
	
	sendKeys(APT_NGINObj.NGIN.resellername, resellername,"Reseller Name");
	
	//GetText(application, "Email", "reseller_email");
	sendKeys(APT_NGINObj.NGIN.reseller_city, city, "City");
	sendKeys(APT_NGINObj.NGIN.reseller_streetname, streetname,  "Street Name");
	sendKeys(APT_NGINObj.NGIN.reseller_streetno, streetno, "Street Number");
	sendKeys(APT_NGINObj.NGIN.reseller_pobox, pobox,  "PO Box");
	sendKeys(APT_NGINObj.NGIN.reseller_zipcode, zipcode, "Zip Code");
	//GetText(application, "Phone", "reseller_phone");
//	GetText(application, "Fax", "reseller_fax");

	if(defaultvalue.equalsIgnoreCase("YES"))
	{
		String Default_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.defaultcheckbox,"checked");
		if(Default_Checkbox!=null)
		{
			System.out.println("Default checkbox is checked");
			waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Default checkbox is checked");
			Reporter.log("Default checkbox is checked");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Default checkbox is not checked by default");
			Reporter.log("Default checkbox is not checked by default");
		}
	}

	else if(configure.equalsIgnoreCase("YES"))
	{
		String Configure_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.configurecheckbox,"checked");
		if(Configure_Checkbox!=null)
		{
			System.out.println("Configure checkbox is checked");
			waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Configure checkbox is checked");
			Reporter.log("Configure checkbox is checked");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Configure checkbox is not checked");
			Reporter.log("Configure checkbox is not checked");
			click(APT_NGINObj.NGIN.configurecheckbox, "Configure checkbox" );
		}
	}
	scrollDown(APT_NGINObj.NGIN.nextbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.nextbutton,"Next");
	waitForAjax();
	waitforPagetobeenable();
	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Reporter.log("Navigated to view service page");
		verifysuccessmessage("Manage Customer successfully created");
	}
	else
	{
		Reporter.log("Customer not created");
		System.out.println("Customer not created");
	}

	//Added Customer
//	ScrolltoElement(application, "customerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.resellerheader);
	//WebElement CustomerGridCheck= getwebelement("(//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String CustomerGrid= getAttributeFrom(APT_NGINObj.NGIN.CustomerGridCheck,"style");

	WebElement AddedCustomer= webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + CustomerName_viewpage + "')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!CustomerGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + CustomerName_viewpage + "')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.CustomerpanelActionDropdown, "Action dropdown");

		List<WebElement> CustomerLinks= webDriver.findElements(By.xpath("//div[contains(text(),'Customer')]/following-sibling::div//div//a"));
		int CustomerLinksCount= CustomerLinks.size();
		for(int i=0;i<CustomerLinksCount;i++)
		{
			String Link= CustomerLinks.get(i).getText();
			ExtentTestManager.getTest().log(LogStatus.PASS, ""+Link+" link is displaying under customer panel");
			Reporter.log(""+Link+" link is displaying under customer panel");
			System.out.println(""+Link+" link is displaying under customer panel");
			waitForAjax();
		}
		click(APT_NGINObj.NGIN.customerheader,"customerheader");

		//Added Customer grid verification
		WebElement AddedCustomer1= webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + CustomerName_viewpage + "')]"));
		String AddedCustomerName_value = AddedCustomer1.getText();
		Reporter.log("Added Customer Name is displayed as : " + AddedCustomerName_value);
		System.out.println("Added Customer Name:"+ AddedCustomerName_value);
		//sa.assertEquals(AddedCustomerName_value,CustomerName_viewpage);
	}
	else
	{
		Reporter.log("Customer is not added in the grid");
		System.out.println("Customer is not added in the grid");
	}

	Customername= CustomerName_viewpage;
	
	
}


public void verify_ViewCustomer(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String resellername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerName");
	String defaultvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DefaultValue_checkbox");
	String configure = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Configure_checkbox");
	String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer_Country");
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
	String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_City");
	String streetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetName");
	String streetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetNumber");
	String pobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_POBox");
	String zipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Zipcode");
	
	scrollDown(APT_NGINObj.NGIN.resellerheader);
//	WebElement CustomerGridCheck= getwebelement("(//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String CustomerGrid= getAttributeFrom(APT_NGINObj.NGIN.CustomerGridCheck,"style");

	WebElement AddedCustomer= webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ Customername+"')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!CustomerGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ Customername+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.CustomerpanelActionDropdown, "Action dropdown");

		if(isVisible(APT_NGINObj.NGIN.view))
		{
			waitForAjax();
			click(APT_NGINObj.NGIN.view, "View");
			waitForAjax();
			waitforPagetobeenable();
			scrollDown(APT_NGINObj.NGIN.manageresellerheader_viewpage);
			compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader_viewpage, "Manage Customer In OSP");
			waitForAjax();

			verifyExists(APT_NGINObj.NGIN.customer_countryvalue, "Country");
			verifyExists(APT_NGINObj.NGIN.customer_customernamevalue,"Customer Name");
			verifyExists(APT_NGINObj.NGIN.resellernamevalue,"Reseller Name");
			verifyExists(APT_NGINObj.NGIN.reselleremailvalue,"Email");
			verifyExists(APT_NGINObj.NGIN.resellercityvalue,"City");
			verifyExists(APT_NGINObj.NGIN.resellerstreetnamevalue,"Street Name");
			verifyExists(APT_NGINObj.NGIN.resellerstreetnovalue,"Street Number");
			verifyExists(APT_NGINObj.NGIN.resellerpoboxvalue,"PO Box");
			verifyExists(APT_NGINObj.NGIN.resellerzipcodevalue,"Zip Code");
			verifyExists(APT_NGINObj.NGIN.resellerphonevalue,"Phone");
			verifyExists(APT_NGINObj.NGIN.resellerfaxvalue,"Fax");
			

			//Edit customer in view customer page
			scrollDown(APT_NGINObj.NGIN.viewpage_actiondropdown);
			waitForAjax();
			
			click(APT_NGINObj.NGIN.viewpage_actiondropdown, "Action dropdown");
			click(APT_NGINObj.NGIN.viewpage_editcustomer, "Edit");
			waitforPagetobeenable();
			compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Edit Customer in view customer page is verified");
			Reporter.log("Edit Customer in view customer page is verified");
		
			scrollDown(APT_NGINObj.NGIN.cancelbutton);
			waitForAjax();
			click(APT_NGINObj.NGIN.cancelbutton, "Cancel");
			waitForAjax();
			waitforPagetobeenable();
		}
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : No existing customer displaying under customer panel");
		Reporter.log("No existing customer displaying under customer panel");
	}

	//Delete customer in view customer page
//	ScrolltoElement(application, "customerheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.resellerheader);
	WebElement AddedCustomer2= webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ Customername +"')]/parent::div//span[contains(@class,'unchecked')]"));

	if(!CustomerGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ Customername +"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.CustomerpanelActionDropdown, "Action dropdown");

		if(isVisible(APT_NGINObj.NGIN.view))
		{
			click(APT_NGINObj.NGIN.view, "View");
			scrollDown(APT_NGINObj.NGIN.manageresellerheader_viewpage);
			waitforPagetobeenable();
			compareText( "Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader_viewpage, "Manage Customer In OSP");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Delete Customer in view customer page is verified");
			Reporter.log("Delete Customer in view customer page is verified");
			waitForAjax();
			
			scrollDown(APT_NGINObj.NGIN.viewpage_actiondropdown);
			
			click(APT_NGINObj.NGIN.viewpage_actiondropdown, "Action dropdown");

			//Delete customer in view customer page
			click(APT_NGINObj.NGIN.viewpage_deletecustomer, "Delete");
			if(isVisible(APT_NGINObj.NGIN.delete_alertpopup))
			{
				click(APT_NGINObj.NGIN.deletealertclose,"Delete Alert close");
			}
			else
			{
				Reporter.log("Delete alert popup is not displayed");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
			}
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : View link is not displaying under customer panel");
			Reporter.log("View link is not displaying under customer panel");
		}
		scrollDown(APT_NGINObj.NGIN.viewpage_backbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.viewpage_backbutton, "Back");
		waitforPagetobeenable();
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : No existing customer displaying under customer panel");
		Reporter.log("No existing customer displaying under customer panel");
	}
	
}


public void verify_EditCustomer(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String editreseller = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditResellerName");
	String editemail = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditEmail");
	String editcity = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditCity");
	String editstreetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditStreetName");
	String editstreetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditStreetNumber");
	String editpobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditPOBox");
	String editzipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditZipcode");
	String editphone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditPhone");
	String editfax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_EditFax");
	
	scrollDown(APT_NGINObj.NGIN.resellerheader);
	//WebElement CustomerGridCheck= getwebelement("(//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String CustomerGrid= getAttributeFrom(APT_NGINObj.NGIN.CustomerGridCheck,"style");

	WebElement AddedCustomer= webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ Customername+"')]/parent::div//span[contains(@class,'unchecked')]"));

	//Edit Customer
	if(!CustomerGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+ Customername+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.CustomerpanelActionDropdown, "Action dropdown");
		waitForAjax();
		click( APT_NGINObj.NGIN.edit, "Edit");
		waitForAjax();
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.manageresellerheader);
		waitForAjax();
		compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
		waitForAjax();

		String CountryDisabled= getAttributeFrom(APT_NGINObj.NGIN.customercountry_disabled,"disabled");
		if(CountryDisabled!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Country dropdown is disabled as expected");
			Reporter.log("Country dropdown is disabled as expected");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Country dropdown is enabled");
			Reporter.log("Country dropdown is enabled");
		}
		String CustomerOCN= getAttributeFrom(APT_NGINObj.NGIN.customer_ocn,"value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : OCN is displaying as: '"+CustomerOCN+"'");
		Reporter.log("OCN is displaying as: '"+CustomerOCN+"'");

		String CustomernameDisabled= getAttributeFrom(APT_NGINObj.NGIN.customername_disabled,"disabled");
		if(CustomernameDisabled!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer name dropdown is disabled as expected");
			Reporter.log("Customer name dropdown is disabled as expected");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Customer name dropdown is enabled");
			Reporter.log("Customer name dropdown is enabled");
		}
		sendKeys(APT_NGINObj.NGIN.resellername, editreseller, "Reseller name");
		sendKeys(APT_NGINObj.NGIN.reseller_email, editemail,  "Email");
		sendKeys(APT_NGINObj.NGIN.reseller_city, editcity, "City");
		sendKeys(APT_NGINObj.NGIN.reseller_streetname, editstreetname, "Street Name");
		sendKeys(APT_NGINObj.NGIN.reseller_streetno, editstreetno, "Street Number");
		sendKeys(APT_NGINObj.NGIN.reseller_pobox, editpobox, "PO Box");
		sendKeys(APT_NGINObj.NGIN.reseller_zipcode, editzipcode, "Zip Code");
		sendKeys(APT_NGINObj.NGIN.reseller_phone, editphone, "Phone");
		sendKeys(APT_NGINObj.NGIN.reseller_fax, editfax, "Fax");
		
		scrollDown(APT_NGINObj.NGIN.okbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.okbutton, "Ok");
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			Reporter.log("Navigated to view service page");
			verifysuccessmessage("Manage Customer successfully updated");
		}
		else
		{
			Reporter.log("Customer not updated");
			System.out.println("Customer not updated");
		}

	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : No existing customer displaying under customer panel");
		Reporter.log("No existing customer displaying under customer panel");
	}
	
	
}


public void verifySANpanel() throws InterruptedException, IOException
{

//	ScrolltoElement(application, "sanheader", xml);
//	((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-100)");
	scrollDown(APT_NGINObj.NGIN.customerheader);
	waitForAjax();
	
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	click(APT_NGINObj.NGIN.SANActionDropdown,"Action dropdown");		

	if(isVisible(APT_NGINObj.NGIN.addsan_link))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Add SAN link is displaying under SAN/FRC panel");
		Reporter.log("Add link is displaying under SAN/FRC panel");
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Add link is not displaying under SAN/FRC panel");
		Reporter.log("Add link is not displaying under SAN/FRC panel");
	}
	click(APT_NGINObj.NGIN.sanheader,"sanheader");

}


public void AddSAN(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String resellername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerName");
	String defaultvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DefaultValue_checkbox");
	String configure = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Configure_checkbox");
	String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer_Country");
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
	String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_City");
	String streetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetName");
	String streetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetNumber");
	String pobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_POBox");
	String zipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Zipcode");
	String sannumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddSAN_SANNumber");
	String predestinationnumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PreDestinationNumber");
	String ringtonumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Ringtonumber");
	String routingforpayphone_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingforpayphone_value");
	String routingformobile_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingformobile_value");
	String defaultrouting_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Defaultrouting_value");
	String RingToNumber_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RingToNumber_Checkbox");
	String AnnouncementToPlay_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AnnouncementToPlay_Checkbox");
	String ComplexRouting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ComplexRouting_Checkbox");
	String defaultroutebusy_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "defaultroutebusy_value");
	String noanswer_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "noanswer_value");
	String networkcongestion = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "networkcongestion");
	String serviceprofilevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceProfile");
	String maxcallduration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Maxcallduration");
	String chargebandname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Chargebandname");
	String internationaloutgoingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalOutgoingCalls_checkbox");
	String internationalincomingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalIncomingCalls_checkbox");
	String mobilecallsallowed_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MobileCallsAllowed_checkbox");
	String payphoneblocking_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PayphoneBlockingenabled_checkbox");
	String supervisionfieldvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Supervisionfieldvalue");
	String noreplytimervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NoReplyTimerValue");
	String webaccessblocked_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "WebAccessBlocked_checkbox");
	String cpsfreeformatvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPSFreeFormatValue");
	String sanblock_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANBlock_Checkbox");
	String focenabled_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "FOCEnabled_Checkbox");
	String enablelogicalrouting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnableLogicalRouting_Checkbox");
	String enablepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnablePriceAnnouncement_Checkbox");
	


	scrollDown(APT_NGINObj.NGIN.customerheader);
	compareText( "SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");

	//verify FRC number column
	compareText( "FRC Number column", APT_NGINObj.NGIN.frcnumber_column, "FRC Number");
	// verify customer name column
	compareText( "Customer name column", APT_NGINObj.NGIN.san_customername, "Customer Name");

	// verify Add SAN link in SAN/FRC action dropdown
	//Cancel add SAN
	click(APT_NGINObj.NGIN.SANActionDropdown, "Action dropdown");
	
	click(APT_NGINObj.NGIN.addsan_link,"Add SAN");
	
	scrollDown(APT_NGINObj.NGIN.addsan_header);
	
	waitforPagetobeenable();
	
	compareText("Add SAN header",APT_NGINObj.NGIN.addsan_header, "Add SAN");
	
	waitForAjax();
	
	scrollDown(APT_NGINObj.NGIN.cancelbutton);
	
	waitForAjax();
	
	//click_commonMethod(application, "Cancel", "cancelbutton", xml);
	WebElement Cancel= findWebElement(APT_NGINObj.NGIN.cancelbutton);
	((JavascriptExecutor)webDriver).executeScript("arguments[0].click();", Cancel);
	
	waitForAjax();
	waitforPagetobeenable();
	
	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Reporter.log("Navigated to view service page");
	}

	//Add SAN
	scrollDown(APT_NGINObj.NGIN.customerheader);
	waitForAjax();
	
	click(APT_NGINObj.NGIN.SANActionDropdown, "Action dropdown");
	
	click(APT_NGINObj.NGIN.addsan_link, "Add SAN");
	
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.addsan_header);
	waitForAjax();
	
	compareText( "Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");
	
	addDropdownValues_commonMethod( "Country", APT_NGINObj.NGIN.customer_country, country);
	click(APT_NGINObj.NGIN.customername, "Customer Name dropdown");
	
	waitForAjax();
	if(isVisible(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Existing customer is available");
		Reporter.log("Existing customer is available");
		click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay, "Customer Name dropdown value");
	}
	else 
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Existing customer is not available");
		Reporter.log("Existing customer is not available");
		click(APT_NGINObj.NGIN.addsan_header,"addsan header");

		//Manage new customer link
		click(APT_NGINObj.NGIN.managenewcustomer_link, "Manage new customer");
		waitforPagetobeenable();
		
		compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
		waitForAjax();

		//=========================

		//Add Customer
		//Select Country from dropdown
		addDropdownValues_commonMethod("Country", APT_NGINObj.NGIN.customer_country, country);
		
		String CustomerOCN= getAttributeFrom(APT_NGINObj.NGIN.customer_ocn,"value");
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : OCN is displaying as: '"+CustomerOCN+"'");
		Reporter.log("OCN is displaying as: '"+CustomerOCN+"'");
		
		String CustomerNamevalue= getTextFrom(APT_NGINObj.NGIN.customerpanel_customernamevalue,"Customer Name");
		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer Name is displaying as: '"+CustomerNamevalue+"'");
		Reporter.log("Customer Name is displaying as: '"+CustomerNamevalue+"'");
		
		CustomerNamevalue.replace("(New)", "").trim();
		
		sendKeys(APT_NGINObj.NGIN.resellername, resellername, "Reseller Name");
		
		//GetText(application, "Email", "reseller_email");
		sendKeys(APT_NGINObj.NGIN.reseller_city, city,  "City");
		sendKeys(APT_NGINObj.NGIN.reseller_streetname, streetname, "Street Name");
		sendKeys(APT_NGINObj.NGIN.reseller_streetno, streetno, "Street Number");
		sendKeys(APT_NGINObj.NGIN.reseller_pobox, pobox, "PO Box");
		sendKeys(APT_NGINObj.NGIN.reseller_zipcode, zipcode,  "Zip Code");
		//GetText(application, "Phone", "reseller_phone");
		//GetText(application, "Fax", "reseller_fax");

		addCheckbox_commonMethod(APT_NGINObj.NGIN.defaultcheckbox, "Default", defaultvalue);
		addCheckbox_commonMethod(APT_NGINObj.NGIN.configurecheckbox, "Configure", configure);
		
		scrollDown(APT_NGINObj.NGIN.nextbutton);
		waitForAjax();
		
		click(APT_NGINObj.NGIN.nextbutton,"Next");
		waitForAjax();
		waitforPagetobeenable();
		
		scrollDown(APT_NGINObj.NGIN.addsan_header);
		waitForAjax();
		
		compareText("Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");
		waitForAjax();

		//Select Country from dropdown
		addDropdownValues_commonMethod("Country", APT_NGINObj.NGIN.customer_country, country);
		click(APT_NGINObj.NGIN.customername, "Customer Name dropdown");
		
		waitForAjax();
		
		click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay, "Customer Name dropdown value");
	}

	waitForAjax();
	//SAN customer name
	String SAN_Customernamevalue= getTextFrom(APT_NGINObj.NGIN.customername_selectedtext,"customer name_selected text");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Customer name selected is : " + SAN_Customernamevalue);
	Reporter.log("Customer name selected is : " + SAN_Customernamevalue);
	String SAN_Customername=SAN_Customernamevalue.substring(0, SAN_Customernamevalue.indexOf("(")).trim();
//	writetoexcel("src\\com\\colt\\qa\\datalibrary\\APT_NGIN.xlsx", "NGIN", 166, SAN_Customername);

	String SANNumber_CountryCode=getAttributeFrom(APT_NGINObj.NGIN.san_number,"value");
	
	sendKeys(APT_NGINObj.NGIN.san_number, sannumber, "SAN Number");
	
	//SAN number
	String SANNumberValue= SANNumber_CountryCode+sannumber;
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step :SAN Number is dsplayed as : " + SANNumberValue);
	Reporter.log("SAN Number is dsplayed as : " + SANNumberValue);
//	writetoexcel("src\\com\\colt\\qa\\datalibrary\\APT_NGIN.xlsx", "NGIN", 167, SANNumberValue);

	//Select service profile from dropdown
	addDropdownValues_commonMethod("Service Profile", APT_NGINObj.NGIN.serviceprofile, serviceprofilevalue );
	waitForAjax();
	
//	GetText(application, "Supervision Mode", "supervisionfield");
	
	addCheckbox_commonMethod(APT_NGINObj.NGIN.internationaloutgoingforbidden_checkbox, "International Outgoing Calls Forbidden", internationaloutgoingcalls_checkbox);
	addCheckbox_commonMethod(APT_NGINObj.NGIN.internationalincomingbarring, "International Incoming Calls Barring", internationalincomingcalls_checkbox);
	addCheckbox_commonMethod(APT_NGINObj.NGIN.mobilecallsallowed_checkbox, "Mobile Calls Allowed", mobilecallsallowed_checkbox);
	
	sendKeys(APT_NGINObj.NGIN.noreplytimervalue, noreplytimervalue, "No reply timer value");
	sendKeys(APT_NGINObj.NGIN.maxcallduration, maxcallduration,  "Max call duration");
	//Select charge band name from dropdown
	scrollDown(APT_NGINObj.NGIN.customer_country);
	waitForAjax();
	
	addDropdownValues_commonMethod("Charge Band Name", APT_NGINObj.NGIN.chargebandname, chargebandname);
	addCheckbox_commonMethod(APT_NGINObj.NGIN.payphoneblockingenabled, "Pay phone blocking enabled", payphoneblocking_checkbox);
	addCheckbox_commonMethod( APT_NGINObj.NGIN.webaccessblocked, "webAccessBlocked", webaccessblocked_checkbox);
	addCheckbox_commonMethod( APT_NGINObj.NGIN.sanblock, "SAN Block", sanblock_checkbox);
	addCheckbox_commonMethod(APT_NGINObj.NGIN.focenabled, "FOC Enabled", focenabled_checkbox);
	
	sendKeys(APT_NGINObj.NGIN.predestinationnumber, predestinationnumber,  "Pre Destination Number");
	
	String CPSFreeFormatValue= getAttributeFrom(APT_NGINObj.NGIN.cpsfreeformat,"value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : CPS Free Format field value is displayed as:"+ CPSFreeFormatValue);
	String CPSSANEncodedValue= getAttributeFrom(APT_NGINObj.NGIN.cpssanencoded,"value");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : 'CPS: SAN encoded in modulo80' field value is displayed as:"+ CPSSANEncodedValue);
	scrollDown(APT_NGINObj.NGIN.ringtonumber_radiobutton);
	if(RingToNumber_Checkbox.equalsIgnoreCase("YES"))
	{
		addCheckbox_commonMethod(APT_NGINObj.NGIN.ringtonumber_radiobutton, "'Ring To Number' radio", RingToNumber_Checkbox);
		waitForAjax();
		sendKeys(APT_NGINObj.NGIN.ringtonumber_field, ringtonumber, "Ring To Number");
	}
	else if(AnnouncementToPlay_Checkbox.equalsIgnoreCase("YES"))
	{
		addCheckbox_commonMethod(APT_NGINObj.NGIN.announcementtoplay_radiobutton, "'Announcement to play' radio", AnnouncementToPlay_Checkbox);
		waitForAjax();
		//Select announcement to play value from dropdown
		click(APT_NGINObj.NGIN.announcementtoplay_dropdown, "Announcement To Play dropdown");
		click(APT_NGINObj.NGIN.announcementtoplay_dropdownvalue, "Announcement To Play value");
	}
	else if(ComplexRouting_Checkbox.equalsIgnoreCase("YES"))
	{
		addCheckbox_commonMethod(APT_NGINObj.NGIN.complexroute_radiobutton, "'Complex route' radio", ComplexRouting_Checkbox);
		waitForAjax();
		sendKeys(APT_NGINObj.NGIN.routingforpayphone_field, routingforpayphone_value, "Routing for payphone");
		sendKeys(APT_NGINObj.NGIN.routingformobile, routingformobile_value, "Routing for mobile");
		sendKeys(APT_NGINObj.NGIN.defaultrouting, defaultrouting_value, "Default Routing");
		
		addCheckbox_commonMethod(APT_NGINObj.NGIN.enablelogicalrouting, "Enable logical routing checkbox", enablelogicalrouting_Checkbox);
		
		sendKeys(APT_NGINObj.NGIN.defaultroutebusy, defaultroutebusy_value, "Default Route busy");
		sendKeys(APT_NGINObj.NGIN.noanswer, noanswer_value, "No Answer");
		sendKeys(APT_NGINObj.NGIN.networkcongestion, networkcongestion, "Network Congestion");
	}
	addCheckbox_commonMethod(APT_NGINObj.NGIN.enablepriceannouncement, "Enable Price Announcement", enablepriceannouncement_checkbox);
	
	scrollDown(APT_NGINObj.NGIN.addsan_addbutton);
	waitForAjax();
	
	click(APT_NGINObj.NGIN.addsan_addbutton, "Add");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.viewsan_header);
	waitForAjax();
	if(isVisible(APT_NGINObj.NGIN.viewsan_header))
	{
		Reporter.log("Navigated to view SAN page");
		verifysuccessmessage("SAN successfully created");
		scrollDown(APT_NGINObj.NGIN.viewpage_backbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.viewpage_backbutton,"Back");
		waitforPagetobeenable();
	}
	else
	{
		Reporter.log("SAN not created");
	}

	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Reporter.log("Navigated to view service page");
	}
	else
	{
		Reporter.log("Not navigated to view service page");
	}
	//Added Customer
	
	//WebElement SANGridCheck= getwebelement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	WebElement AddedSAN= webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + SAN_Customername + "')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!SANGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + SAN_Customername + "')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.SANActionDropdown, "Action dropdown");
		List<WebElement> SANLinks= webDriver.findElements(By.xpath("//div[contains(text(),'SAN/FRC')]/following-sibling::div//div//a"));
		int SANLinksCount= SANLinks.size();
		for(int i=0;i<SANLinksCount;i++)
		{
			String Link= SANLinks.get(i).getText();
			ExtentTestManager.getTest().log(LogStatus.PASS, ""+Link+" link is displaying under SAN/FRC panel");
			Reporter.log(""+Link+" link is displaying under SAN/FRC panel");
			System.out.println("Customer link:"+ Link + " is displaying");
			waitForAjax();
		}
		click(APT_NGINObj.NGIN.sanheader,"san header");

		//Added Customer grid verification
		WebElement AddedSan= webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + SANNumberValue + "')]"));
		String AddedSAN_FRCNumber = AddedSan.getText();
		Reporter.log("FRC Number for Added SAN is displayed as : " + AddedSAN_FRCNumber);
		System.out.println("FRC Number for Added SAN: "+ AddedSAN_FRCNumber);
	//	sa.assertEquals(AddedSAN_FRCNumber,SANNumberValue);
		ExtentTestManager.getTest().log(LogStatus.PASS, "FRC Number for Added SAN is displayed as : " + AddedSAN_FRCNumber);

		WebElement AddedSan1= webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + SAN_Customername + "')]"));
		String AddedSAN_Customervalue = AddedSan1.getText();
		Reporter.log("Customer Name for Added SAN is displayed as : " + AddedSAN_Customervalue);
		System.out.println("Customer Name for Added SAN: "+ AddedSAN_Customervalue);
		//sa.assertEquals(AddedSAN_Customervalue,SAN_Customername);
		ExtentTestManager.getTest().log(LogStatus.PASS, "Customer Name for Added SAN is displayed as : " + AddedSAN_Customervalue);

	}

	else
	{
		Reporter.log("SAN is not added in the grid");
		System.out.println("SAN is not added in the grid");
	}
	
}


public void verifyViewSAN(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String customernamevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerNameValue");
	String sannumbervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue");
	String serviceprofilevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceProfile");
	String supervisionfieldvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Supervisionfieldvalue");
	String maxcallduration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Maxcallduration");
	String predestinationnumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PreDestinationNumber");
	String ringtonumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Ringtonumber");
	String noreplytimervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NoReplyTimerValue");
	String select_sansearchtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SelectSANSearchType");
	
	scrollDown(APT_NGINObj.NGIN.sanheader);
	waitForAjax();
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	//SANFilter
	click(APT_NGINObj.NGIN.sanpanel_searchsanmenu, "Search San Menu");
	
	click(APT_NGINObj.NGIN.select_sansearchtype, "Select SAN Search type");
	
	sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype, "Enter SAN Search type");
	
    findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
	
	clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
	sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, sannumbervalue, "SAN Number Search");
	
	findWebElement(APT_NGINObj.NGIN.sanheader).click();

	//WebElement SANGridCheck= getwebelement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	WebElement AddedSAN=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + customernamevalue + "')]/parent::div//span[contains(@class,'unchecked')]"));

	if(!SANGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + customernamevalue + "')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		
		click(APT_NGINObj.NGIN.SANActionDropdown, "SAN Action dropdown");
		click(APT_NGINObj.NGIN.viewsan_link, "View SAN link");
		waitForAjax();
		
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.viewsan_header);
		waitForAjax();
		
		compareText("View SAN Header", APT_NGINObj.NGIN.viewsan_header, "View SAN");
		compareText( "Customer Name", APT_NGINObj.NGIN.viewsan_customername, customernamevalue);
		compareText( "SAN/FRC Number", APT_NGINObj.NGIN.viewsan_sannumber, sannumbervalue);
		compareText( "Service Profile", APT_NGINObj.NGIN.serviceprofilevalue, serviceprofilevalue);
		compareText( "Supervision Mode", APT_NGINObj.NGIN.supervisionmodevalue, supervisionfieldvalue);
		/*
		GetText(application, "International Outgoing Calls Forbidden", "internationaloutgoingcallsvalue");
		GetText(application, "International Incoming Calls Barring", "internationalincomingcallsvalue");
		GetText(application, "Mobile Calls Allowed", "mobilecallsallowedvalue");
		ScrolltoElement(application, "view_noreplytimervalue", xml);
		GetText(application, "No Reply Timer Value", "view_noreplytimervalue");
		GetText(application, "Max Call Duration", "maxcalldurationvalue");
		GetText(application, "Charge Band Name", "chargebandnamevalue");
		GetText(application, "Pay phone blocking enabled", "payphoneblockingenabledvalue");
		GetText(application, "webAccessBlocked", "payphoneblockingenabledvalue");
		GetText(application, "SAN Block", "sanblockvalue");
		ScrolltoElement(application, "focenabledvalue", xml);
		GetText(application, "FOC Enabled", "focenabledvalue");
		GetText(application, "Predestination number", "predestinationnumbervalue");
		GetText(application, "CPS Free Format", "CPSvalue");
		compareText(application, "Ring To Number", "ringtonumbervalue", ringtonumber, xml);
		GetText(application, "Announcement to play", "announcementtoplay");
		GetText(application, "Tree name", "treenamevalue");
*/
		//Edit SAN link in view SAN page
		waitForAjax();
		scrollDown(APT_NGINObj.NGIN.viewsan_actiondropdown);
		click(APT_NGINObj.NGIN.viewsan_actiondropdown, "Action dropdown");
		click(APT_NGINObj.NGIN.editsan_link, "Edit SAN");
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.editsan_header))
		{
			compareText("Edit SAN Header", APT_NGINObj.NGIN.editsan_header, "Edit SAN");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to Edit SAN page");
			Reporter.log("Navigated to Edit SAN page");
			scrollDown(APT_NGINObj.NGIN.cancelbutton);
			waitForAjax();
			click(APT_NGINObj.NGIN.cancelbutton,"Cancel");
			waitforPagetobeenable();
			if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
			{
				Reporter.log("Navigated to view service page");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to view service page");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to view service page");
				Reporter.log("Didn't navigate to view service page");
			}
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to Edit SAN page");
			Reporter.log("Didn't navigate to Edit SAN page");
		}
	}
	else
	{
		Reporter.log("SAN is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: SAN is not added in the grid");
	}

	//Add Another SAN link in view SAN page
	scrollDown(APT_NGINObj.NGIN.sanheader);
	waitForAjax();
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	//SANFilter
	click(APT_NGINObj.NGIN.sanpanel_searchsanmenu, "Search San Menu");
	click(APT_NGINObj.NGIN.select_sansearchtype, "Search San Menu");
	
	sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype, "Enter SAN Search type");
	
	findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
	clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
	sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, sannumbervalue,"SAN Number Search");
	findWebElement(APT_NGINObj.NGIN.sanheader).click();

	//WebElement SANGridCheck2= getwebelement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid2= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	System.out.println("Customer Name displaying: " +customernamevalue);
	WebElement AddedSAN2=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!SANGrid2.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();

		click(APT_NGINObj.NGIN.SANActionDropdown, "SAN Action dropdown");
		click(APT_NGINObj.NGIN.viewsan_link, "View SAN link");
		waitforPagetobeenable();
		compareText("View SAN Header", APT_NGINObj.NGIN.viewsan_header, "View SAN");
		scrollDown(APT_NGINObj.NGIN.viewsan_actiondropdown);
		waitForAjax();
		
		click(APT_NGINObj.NGIN.viewsan_actiondropdown, "Action dropdown");
		waitForAjax();
		click(APT_NGINObj.NGIN.addanothersanlink, "Add Another SAN");
		
		waitForAjax();
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.addanothersan_header))
		{
			//GetText(application,  "Add another SAN Header", "addanothersan_header");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to Add SAN page");
			Reporter.log("Navigated to Add SAN page");
			scrollDown(APT_NGINObj.NGIN.cancelbutton);
			waitForAjax();
			click(APT_NGINObj.NGIN.cancelbutton, "Cancel");
			waitforPagetobeenable();
			if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
			{
				Reporter.log("Navigated to view service page");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigated to view service page");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to view service page");
				Reporter.log("Didn't navigate to view service page");
			}
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to Add SAN page");
			Reporter.log("Didn't navigate to Add SAN page");
		}

	}
	else
	{
		Reporter.log("SAN is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: SAN is not added in the grid");
	}

	//Delete SAN link in View SAN page
	scrollDown(APT_NGINObj.NGIN.sanheader);
	waitForAjax();
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	//SANFilter
	click(APT_NGINObj.NGIN.sanpanel_searchsanmenu, "Search San Menu");
	
	click(APT_NGINObj.NGIN.select_sansearchtype,"Select SAN Search type");
	
	sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype,"Enter SAN Search type");
	findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
	clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
	sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, sannumbervalue, "SAN Number Search");
	findWebElement(APT_NGINObj.NGIN.sanheader).click();

	//WebElement SANGridCheck3= getwebelement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid3= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	System.out.println("Customer Name displaying: " +customernamevalue);
	WebElement AddedSAN3=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!SANGrid3.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();

		click(APT_NGINObj.NGIN.SANActionDropdown, "SAN Action dropdown");
		click(APT_NGINObj.NGIN.viewsan_link, "View SAN");
		
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.viewsan_header);
		compareText("View SAN Header", APT_NGINObj.NGIN.viewsan_header, "View SAN");
		
		click(APT_NGINObj.NGIN.viewsan_actiondropdown,"Action dropdown");
		click(APT_NGINObj.NGIN.deletesan_link,"Delete SAN");
		
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.delete_alertpopup))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Delete alert popup is displayed as expected");
			Reporter.log("Delete alert popup is displayed as expected");
			click(APT_NGINObj.NGIN.deletealertclose, "Delete alert close");
		}
		else
		{
			Reporter.log("Delete alert popup is not displayed");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Delete alert popup is not displayed");
			Reporter.log("Delete alert popup is not displayed");
		}
		waitForAjax();
		scrollDown(APT_NGINObj.NGIN.viewpage_backbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.viewpage_backbutton,  "Back");
		waitForAjax();
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			Reporter.log("Navigated to view service page");
		}
	}
	else
	{
		Reporter.log("SAN is not added in the grid");
	}
	
}


public void verifyEditSAN(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String customernamevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerNameValue");
	String sannumbervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue");
	String edit_serviceprofilevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_ServiceProfile");
	String supervisionfieldvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Supervisionfieldvalue");
	String edit_supervisionfieldvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_SupervisionValue");
	String maxcallduration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Maxcallduration");
	String chargebandname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Chargebandname");
	String predestinationnumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PreDestinationNumber");
	String ringtonumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Ringtonumber");
	String routingforpayphone_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingforpayphone_value");
	String routingformobile_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingformobile_value");
	String defaultrouting_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Defaultrouting_value");
	String RingToNumber_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RingToNumber_Checkbox");
	String AnnouncementToPlay_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AnnouncementToPlay_Checkbox");
	String ComplexRouting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ComplexRouting_Checkbox");
	String defaultroutebusy_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "defaultroutebusy_value");
	String noanswer_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "noanswer_value");
	String networkcongestion = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "networkcongestion");
	String internationaloutgoingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalOutgoingCalls_checkbox");
	String internationalincomingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalIncomingCalls_checkbox");
	String mobilecallsallowed_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MobileCallsAllowed_checkbox");
	String payphoneblocking_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PayphoneBlockingenabled_checkbox");
	String noreplytimervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NoReplyTimerValue");
	String webaccessblocked_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "WebAccessBlocked_checkbox");
	String cpsfreeformatvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPSFreeFormatValue");
	String enablepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnablePriceAnnouncement_Checkbox");
	String select_sansearchtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SelectSANSearchType");
	String edit_enablepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_EnablePriceAnnouncement_Checkbox");
	String edit_chargebandname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_ChargeBandName");
	String edit_internationaloutgoingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_InternationalOutgoingCalls_checkbox");
	String edit_internationalincomingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_InternationalIncomingCalls_checkbox");
	String edit_mobilecallsallowed_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_MobileCallsAllowed_checkbox");
	String edit_noreplytimervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_NoReplyTimerValue");
	String edit_maxcallduration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_MaxCallDuration");
	String edit_payphoneblocking_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_PayphoneBlockingenabled_checkbox");
	String edit_webaccessblocked_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_WebAccessBlocked_checkbox");
	String edit_sanblock_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_SANBlock_Checkbox");
	String edit_focenabled_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_FOCEnabled_Checkbox");
	String edit_ringtonumber_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_RingToNumber_Checkbox");
	String edit_announcementtoplay_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_AnnouncementToPlay_Checkbox");
	String edit_complexroute_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_ComplexRouting_Checkbox");
	String edit_predestinationnumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_PreDestinationNumber");
	
	scrollDown(APT_NGINObj.NGIN.customerheader);
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	//SANFilter
	click(APT_NGINObj.NGIN.sanpanel_searchsanmenu,"Search San Menu");
	click(APT_NGINObj.NGIN.select_sansearchtype, "Select SAN Search type");
	sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype, "Enter SAN Search type");
	findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
	sendKeys( APT_NGINObj.NGIN.sannumbersearchfield, sannumbervalue, "SAN Number Search");
	findWebElement(APT_NGINObj.NGIN.sanheader).click();

	//WebElement SANGridCheck= getwebelement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	WebElement AddedSAN=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + customernamevalue + "')]/parent::div//span[contains(@class,'unchecked')]"));

	if(!SANGrid.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + customernamevalue + "')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		click(APT_NGINObj.NGIN.SANActionDropdown,"SAN Action dropdown");
		click(APT_NGINObj.NGIN.editsan_link,"Edit SAN");
		waitForAjax();
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.editsan_header);
		compareText("Edit SAN Header", APT_NGINObj.NGIN.editsan_header, "Edit SAN");

		//verify Customer Name field
		if(getAttributeFrom(APT_NGINObj.NGIN.editsan_customername,"readonly")!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer Name field is disabled as expected");
			Reporter.log("Customer Name field is disabled as expected");
			String CustomerNamevalue= getAttributeFrom(APT_NGINObj.NGIN.editsan_customername,"value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer Name field value is displayed as:"+ CustomerNamevalue);
			Reporter.log("Customer Name field value is displayed as:"+ CustomerNamevalue);
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Customer Name field is enabled");
			Reporter.log("Customer Name field is enabled");
		}

		//verify SAN Number field
		if(getAttributeFrom(APT_NGINObj.NGIN.editsan_sannumber,"readonly")!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN Number field is disabled as expected");
			Reporter.log("SAN Number field is disabled as expected");
			String SANNumbervalue= getAttributeFrom(APT_NGINObj.NGIN.editsan_sannumber,"value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN Number field value is displayed as:"+ SANNumbervalue);
			Reporter.log("SAN Number field value is displayed as:"+ SANNumbervalue);
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : SAN Number field is enabled");
			Reporter.log("SAN Number field is enabled");
		}

		//Select service profile from dropdown
		addDropdownValues_commonMethod("Service Profile", APT_NGINObj.NGIN.serviceprofile, edit_serviceprofilevalue);
		waitForAjax();
		addDropdownValues_commonMethod("Supervision mode", APT_NGINObj.NGIN.supervisionfield, edit_supervisionfieldvalue);

		editcheckbox_commonMethod(edit_internationaloutgoingcalls_checkbox, APT_NGINObj.NGIN.internationaloutgoingforbidden_checkbox, "International Outgoing Calls Forbidden");
		editcheckbox_commonMethod( edit_internationalincomingcalls_checkbox, APT_NGINObj.NGIN.internationalincomingbarring, "International Incoming Calls Barring");
		editcheckbox_commonMethod( edit_mobilecallsallowed_checkbox, APT_NGINObj.NGIN.mobilecallsallowed_checkbox, "Mobile Calls Allowed");
		
		waitForAjax();
		
		clearTextBox(APT_NGINObj.NGIN.noreplytimervalue);
		sendKeys(APT_NGINObj.NGIN.noreplytimervalue, edit_noreplytimervalue,"No reply timer value");
		sendKeys(APT_NGINObj.NGIN.maxcallduration, edit_maxcallduration, "Max call duration");
		addDropdownValues_commonMethod("Charge Band Name", APT_NGINObj.NGIN.chargebandname, edit_chargebandname);
		editcheckbox_commonMethod(edit_payphoneblocking_checkbox, APT_NGINObj.NGIN.payphoneblockingenabled, "Pay phone blocking enabled");
		editcheckbox_commonMethod(edit_webaccessblocked_checkbox, APT_NGINObj.NGIN.webaccessblocked, "Web Access Blocked");
		scrollDown( APT_NGINObj.NGIN.sanblock);
		editcheckbox_commonMethod(edit_sanblock_checkbox, APT_NGINObj.NGIN.sanblock, "SAN Block");
		editcheckbox_commonMethod(edit_focenabled_checkbox, APT_NGINObj.NGIN.focenabled, "FOC Enabled");
		edittextFields_commonMethod("Pre destination number", APT_NGINObj.NGIN.predestinationnumber, edit_predestinationnumber);
		
		String CPSFreeFormatValue= getAttributeFrom(APT_NGINObj.NGIN.cpsfreeformat,"value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : CPS Free Format field value is displayed as:"+ CPSFreeFormatValue);
		String CPSSANEncodedValue= getAttributeFrom(APT_NGINObj.NGIN.cpssanencoded,"value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : 'CPS: SAN encoded in modulo80' field value is displayed as:"+ CPSSANEncodedValue);

		if(RingToNumber_Checkbox.equalsIgnoreCase("YES"))
		{
			editcheckbox_commonMethod(edit_ringtonumber_checkbox, APT_NGINObj.NGIN.ringtonumber_radiobutton, "'Ring To Number' radio");
			click(APT_NGINObj.NGIN.ringtonumber_radiobutton, "'Ring To Number' radio");
			waitForAjax();
			sendKeys(APT_NGINObj.NGIN.ringtonumber_field, ringtonumber, "Ring To Number");
		}
		else if(AnnouncementToPlay_Checkbox.equalsIgnoreCase("YES"))
		{
			editcheckbox_commonMethod(edit_announcementtoplay_checkbox, APT_NGINObj.NGIN.announcementtoplay_radiobutton, "'Announcement to play' radio");
			Thread.sleep(1000);
			//Select announcement to play value from dropdown
			click(APT_NGINObj.NGIN.announcementtoplay_dropdown,"Announcement To Play dropdown");
			click( APT_NGINObj.NGIN.announcementtoplay_dropdownvalue,"Announcement To Play value");
		}
		else if(ComplexRouting_Checkbox.equalsIgnoreCase("YES"))
		{
			editcheckbox_commonMethod(edit_complexroute_checkbox, APT_NGINObj.NGIN.complexroute_radiobutton, "'Complex route' radio");
			waitForAjax();
			sendKeys(APT_NGINObj.NGIN.routingforpayphone_field, routingforpayphone_value, "Routing for payphone");
			sendKeys(APT_NGINObj.NGIN.routingformobile, routingformobile_value, "Routing for mobile");
			sendKeys(APT_NGINObj.NGIN.defaultrouting, defaultrouting_value, "Default Routing");
			click(APT_NGINObj.NGIN.enablelogicalrouting, "Enable logical routing checkbox");
			sendKeys(APT_NGINObj.NGIN.defaultroutebusy, defaultroutebusy_value, "Default Route busy");
			sendKeys(APT_NGINObj.NGIN.noanswer, noanswer_value, "No Answer");
			sendKeys(APT_NGINObj.NGIN.networkcongestion, networkcongestion, "Network Congestion");
		
		}

			editcheckbox_commonMethod(edit_enablepriceannouncement_checkbox, APT_NGINObj.NGIN.enablepriceannouncement, "Enable Price Announcement");
			waitForAjax();
		
			scrollDown(APT_NGINObj.NGIN.addsan_addbutton);
			waitForAjax();
		click(APT_NGINObj.NGIN.addsan_addbutton, "Add");
		waitForAjax();
		waitforPagetobeenable();
		if(isVisible(APT_NGINObj.NGIN.viewsan_header))
		{
			Reporter.log("Navigated to view SAN page");
			scrollDown(APT_NGINObj.NGIN.editsan_successmsg);
			waitforPagetobeenable();
			verifysuccessmessage("San successfully updated");
			waitForAjax();
			scrollDown(APT_NGINObj.NGIN.viewpage_backbutton);
			waitForAjax();
			click(APT_NGINObj.NGIN.viewpage_backbutton, "Back");
			waitForAjax();
			waitforPagetobeenable();
			if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
			{
				Reporter.log("Navigated to view service page");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: Didn't navigate to view service page");
				Reporter.log("Didn't navigate to view service page");
			}

		}
		else
		{
			Reporter.log("SAN not updated");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step: SAN not updated");
		}

	}
	else
	{
		Reporter.log("SAN is not added in the grid");
	}
	
}

public void verifySANMove(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String customernamevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerNameValue");
	String sannumbervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue");
	String select_sansearchtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SelectSANSearchType");
	String destinationcustomername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DestinationCustomerName");
	String sanmove_orderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANMove_Orderno");
	
	
	scrollDown(APT_NGINObj.NGIN.sanheader);
	compareText( "SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	//SANFilter
	click(APT_NGINObj.NGIN.sanpanel_searchsanmenu, "Search San Menu");
	click(APT_NGINObj.NGIN.select_sansearchtype, "Select SAN Search type");
	sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype, "Enter SAN Search type");
	findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
	clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
	sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, sannumbervalue,  "SAN Number Search");
	findWebElement(APT_NGINObj.NGIN.sanheader).click();

	//WebElement SANGridCheck6= getwebelement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid6= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	System.out.println("Customer Name displaying: " +customernamevalue);
	WebElement AddedSAN6=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!SANGrid6.contains("height: 1px"))
	{
		webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
		click(APT_NGINObj.NGIN.SANActionDropdown, "SAN Action dropdown");
		click(APT_NGINObj.NGIN.sanmove_link, "SAN Move");
		waitforPagetobeenable();
		waitForAjax();
		compareText( "SAN Move Header", APT_NGINObj.NGIN.sanmoveheader, "SAN Move");
		click(APT_NGINObj.NGIN.cancelbutton, "Cancel");
		waitforPagetobeenable();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN Move is cancelled");
		Reporter.log("SAN Move is cancelled");
	}
	else
	{
		Reporter.log("SAN is not added in the grid");
	}
	//SAN Move
	scrollDown(APT_NGINObj.NGIN.sanheader);
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	//SANFilter
	click(APT_NGINObj.NGIN.sanpanel_searchsanmenu, "Search San Menu");
	click(APT_NGINObj.NGIN.select_sansearchtype, "Select SAN Search type");
	sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype, "Enter SAN Search type");
	findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
	clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
	sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, sannumbervalue, "SAN Number Search");
	findWebElement(APT_NGINObj.NGIN.sanheader).click();

	//WebElement SANGridCheck7= getwebelement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid7= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	System.out.println("Customer Name displaying: " +customernamevalue);
	WebElement AddedSAN7=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]"));
	if(!SANGrid7.contains("height: 1px"))
	{
		 webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
		waitForAjax();
		compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
		click(APT_NGINObj.NGIN.SANActionDropdown,  "SAN Action dropdown");
		click(APT_NGINObj.NGIN.sanmove_link, "SAN Move");
		waitForAjax();
		waitforPagetobeenable();
		compareText("SAN Move Header", APT_NGINObj.NGIN.sanmoveheader, "SAN Move");
		click(APT_NGINObj.NGIN.movebutton_sanmove,"Move");
		//warning message check
		verifyExists(APT_NGINObj.NGIN.destinationcustomername_warningmsg, "Destination Customer name");
		verifyExists(APT_NGINObj.NGIN.orderservice_warningmsg, "Order/Service");

		compareText("Customer Name", APT_NGINObj.NGIN.sanmove_customername, customernamevalue);
		compareText("SAN Number", APT_NGINObj.NGIN.sanmove_sannumber, sannumbervalue);
		addDropdownValues_commonMethod("Destination Customer Name", APT_NGINObj.NGIN.destinationcustomername, destinationcustomername);
		addDropdownValues_commonMethod("Order/Service", APT_NGINObj.NGIN.orderservice_dropdown, sanmove_orderno);
		click(APT_NGINObj.NGIN.movebutton_sanmove, "Move");
		waitForAjax();
		waitforPagetobeenable();
		verifysuccessmessage("San Moved Successfully");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN Move is successful");
		Reporter.log("SAN Move is successful");
	}
	else
	{
		Reporter.log("SAN is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN is not added in the grid");
	}

	//verify SAN in destination customer service
	mouseMoveOn(APT_NGINObj.NGIN.ManageCustomerServiceLink);

	click(APT_NGINObj.NGIN.searchorderlink,"searchorderlink");

	sendKeys(APT_NGINObj.NGIN.servicefield,sanmove_orderno,"sanmove_orderno");
	waitForAjax();

//	WebElement searchbutton= (APT_NGINObj.NGIN.searchbutton);
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.searchbutton,"searchbuttontton");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.serviceradiobutton,"serviceradiobutton");
	waitForAjax();
	click(APT_NGINObj.NGIN.searchorder_actiondropdown,"searchorder");
	waitForAjax();
	click(APT_NGINObj.NGIN.view,"View");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.sanheader);
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	//SANFilter
	click(APT_NGINObj.NGIN.sanpanel_searchsanmenu,"Search San Menu");
	click(APT_NGINObj.NGIN.select_sansearchtype, "Select SAN Search type");
	sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype, "Enter SAN Search type");
	findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
	clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
	sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, sannumbervalue, "SAN Number Search");
	findWebElement( APT_NGINObj.NGIN.sanheader).click();

	WebElement AddedSAN11= findWebElement("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+sannumbervalue+"')]/parent::div//span[contains(@class,'unchecked')]");
	if(AddedSAN11.isDisplayed())
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN Move verification is successfull in destination customer");
		Reporter.log("SAN Move verification is successfull in destination customer");
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : SAN is not moved successfully into destination customer");
		Reporter.log("SAN is not moved successfully into destination customer");
	}

}


public void verifyBulkMove(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String resellername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerName");
	String defaultvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DefaultValue_checkbox");
	String configure = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Configure_checkbox");
	String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer_Country");
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
	String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_City");
	String streetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetName");
	String streetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetNumber");
	String pobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_POBox");
	String zipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Zipcode");
	String sannumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddSAN_SANNumber");
	String predestinationnumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PreDestinationNumber");
	String ringtonumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Ringtonumber");
	String routingforpayphone_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingforpayphone_value");
	String routingformobile_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingformobile_value");
	String defaultrouting_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Defaultrouting_value");
	String RingToNumber_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RingToNumber_Checkbox");
	String AnnouncementToPlay_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AnnouncementToPlay_Checkbox");
	String ComplexRouting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ComplexRouting_Checkbox");
	String defaultroutebusy_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "defaultroutebusy_value");
	String noanswer_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "noanswer_value");
	String networkcongestion = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "networkcongestion");
	String serviceprofilevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceProfile");
	String maxcallduration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Maxcallduration");
	String chargebandname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Chargebandname");
	String internationaloutgoingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalOutgoingCalls_checkbox");
	String internationalincomingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalIncomingCalls_checkbox");
	String mobilecallsallowed_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MobileCallsAllowed_checkbox");
	String payphoneblocking_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PayphoneBlockingenabled_checkbox");
	String supervisionfieldvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Supervisionfieldvalue");
	String noreplytimervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NoReplyTimerValue");
	String webaccessblocked_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "WebAccessBlocked_checkbox");
	String cpsfreeformatvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPSFreeFormatValue");
	String sanblock_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANBlock_Checkbox");
	String focenabled_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "FOCEnabled_Checkbox");
	String enablelogicalrouting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnableLogicalRouting_Checkbox");
	String enablepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnablePriceAnnouncement_Checkbox");
	String bulkmove_sannumber2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_SANNumberValue2");
	String bulkmove_sannumber1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_SANNumberValue1");
	String bulkmove_service = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_Service");

	String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String customernamevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerNameValue");
	String sannumbervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue");
	String bulkmove_country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_Country");
	String bulkmove_customer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_Service");


	mouseMoveOn(APT_NGINObj.NGIN.ManageCustomerServiceLink);
	waitForAjax();

	click(APT_NGINObj.NGIN.searchorderlink,"searchorderlink");
	waitForAjax();
	sendKeys(APT_NGINObj.NGIN.servicefield,sid,"servicefield");
	waitForAjax();

	WebElement searchbutton1= findWebElement(APT_NGINObj.NGIN.searchbutton);
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	click(APT_NGINObj.NGIN.searchbutton,"searchbutton");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.serviceradiobutton,"serviceradiobutton");
	waitForAjax();
	click(APT_NGINObj.NGIN.searchorder_actiondropdown,"searchorder");
	waitForAjax();
	click(APT_NGINObj.NGIN.view,"view");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.sanheader);
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");

	//==============Add SAN to perform bulk move======

	click(APT_NGINObj.NGIN.SANActionDropdown, "Action dropdown");	
	click(APT_NGINObj.NGIN.addsan_link,"Add SAN");
	scrollDown(APT_NGINObj.NGIN.addsan_header);
	waitforPagetobeenable();
	compareText("Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");

	addDropdownValues_commonMethod("Country",  APT_NGINObj.NGIN.customer_country, country);
	click(APT_NGINObj.NGIN.customername,"Customer Name dropdown");
	waitForAjax();

	if(isVisible(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Existing customer is available");
		Reporter.log("Existing customer is available");
		click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay,"Customer Name dropdown value");
	}
	else if(isVisible(APT_NGINObj.NGIN.customernamedropdown_nomatchesfound))
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Existing customer is not available");
		Reporter.log("Existing customer is not available");
		click(APT_NGINObj.NGIN.addsan_header,"addsan_header");

		//Manage new customer link
		click(APT_NGINObj.NGIN.managenewcustomer_link,  "Manage new customer");
		waitforPagetobeenable();
		compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
		waitForAjax();

		//=========================

		//Add Customer
		//Select Country from dropdown
		addDropdownValues_commonMethod("Country", APT_NGINObj.NGIN.customer_country, country);
		String CustomerOCN= getAttributeFrom(APT_NGINObj.NGIN.customer_ocn,"value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : OCN is displaying as: '"+CustomerOCN+"'");
		String CustomerNamevalue= getTextFrom(APT_NGINObj.NGIN.customerpanel_customernamevalue,"customer panel_customer namevalue");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer Name is displaying as: '"+CustomerNamevalue+"'");
		CustomerNamevalue.replace("(New)", "").trim();
		sendKeys(APT_NGINObj.NGIN.resellername, resellername, "Reseller Name");
		//GetText(application, "Email", "reseller_email");
		sendKeys(APT_NGINObj.NGIN.reseller_city, city,  "City");
		sendKeys(APT_NGINObj.NGIN.reseller_streetname, streetname, "Street Name");
		sendKeys(APT_NGINObj.NGIN.reseller_streetno, streetno, "Street Number");
		sendKeys(APT_NGINObj.NGIN.reseller_pobox, pobox, "PO Box");
		sendKeys(APT_NGINObj.NGIN.reseller_zipcode, zipcode, "Zip Code");
		

		if(defaultvalue.equalsIgnoreCase("YES"))
		{
			String Default_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.defaultcheckbox,"checked");
			if(Default_Checkbox.isEmpty())
			{
				System.out.println("Default checkbox is checked");
				Thread.sleep(1000);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Default checkbox is checked");
				Reporter.log("Default checkbox is checked");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Default checkbox is not checked by default");
				Reporter.log("Default checkbox is not checked by default");
			}
		}

		else if(configure.equalsIgnoreCase("YES"))
		{
			String Configure_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.configurecheckbox,"checked");
			if(Configure_Checkbox.isEmpty())
			{
				System.out.println("Configure checkbox is checked");
				Thread.sleep(1000);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Configure checkbox is checked");
				Reporter.log("Configure checkbox is checked");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Configure checkbox is not checked");
				Reporter.log("Configure checkbox is not checked");
				click(APT_NGINObj.NGIN.configurecheckbox, "Configure checkbox");
			}
		}
		scrollDown(APT_NGINObj.NGIN.nextbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.nextbutton, "Next");
		waitForAjax();
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.addsan_header);
		compareText("Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");
		waitForAjax();

		//Select Country from dropdown
		addDropdownValues_commonMethod("Country", APT_NGINObj.NGIN.customer_country, country);
		click(APT_NGINObj.NGIN.customername, "Customer Name dropdown");
		waitForAjax();
		click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay,  "Customer Name dropdown value");
	}

	Thread.sleep(1000);
	//SAN customer name
	String SAN_Customernamevalue= getTextFrom(APT_NGINObj.NGIN.customername_selectedtext,"customer name_selected text");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Customer name selected is : " + SAN_Customernamevalue);
	String SAN_Customername=SAN_Customernamevalue.substring(0, SAN_Customernamevalue.indexOf("(")).trim();
	//writetoexcel("src\\com\\colt\\qa\\datalibrary\\APT_NGIN.xlsx", "NGIN", 166, SAN_Customername);

	String SANNumber_CountryCode=getAttributeFrom(APT_NGINObj.NGIN.san_number,"value");
	sendKeys(APT_NGINObj.NGIN.san_number, bulkmove_sannumber1, "SAN Number");
	//SAN number
	String Bulkmove_SANNumberValue1= SANNumber_CountryCode+bulkmove_sannumber1;
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step :SAN Number is dsplayed as : " + Bulkmove_SANNumberValue1);

	//Select service profile from dropdown
	sendKeys(APT_NGINObj.NGIN.serviceprofile, serviceprofilevalue,  "Service Profile");
	scrollDown(APT_NGINObj.NGIN.serviceprofile);
	waitForAjax();
	if(RingToNumber_Checkbox.equalsIgnoreCase("YES"))
	{
		click(APT_NGINObj.NGIN.ringtonumber_radiobutton,"Ring To Number radio");
		waitForAjax();
		sendKeys(APT_NGINObj.NGIN.ringtonumber_field, ringtonumber, "Ring To Number");
	}
	else if(AnnouncementToPlay_Checkbox.equalsIgnoreCase("YES"))
	{
		click(APT_NGINObj.NGIN.announcementtoplay_radiobutton, "'Announcement to play' radio");
		waitForAjax();
		//Select announcement to play value from dropdown
		click(APT_NGINObj.NGIN.announcementtoplay_dropdown, "Announcement To Play dropdown");
		click(APT_NGINObj.NGIN.announcementtoplay_dropdownvalue, "Announcement To Play value");
	}
	else if(ComplexRouting_Checkbox.equalsIgnoreCase("YES"))
	{
		click(APT_NGINObj.NGIN.complexroute_radiobutton, "'Complex route' radio");
		waitForAjax();
		sendKeys(APT_NGINObj.NGIN.routingforpayphone_field, routingforpayphone_value,  "Routing for payphone");
		sendKeys(APT_NGINObj.NGIN.routingformobile, routingformobile_value, "Routing for mobile");
		sendKeys(APT_NGINObj.NGIN.defaultrouting, defaultrouting_value, "Default Routing");
		click(APT_NGINObj.NGIN.enablelogicalrouting, "Enable logical routing checkbox");
		sendKeys(APT_NGINObj.NGIN.defaultroutebusy, defaultroutebusy_value, "Default Route busy");
		sendKeys(APT_NGINObj.NGIN.noanswer, noanswer_value, "No Answer");
		sendKeys(APT_NGINObj.NGIN.networkcongestion, networkcongestion, "Network Congestion");
	}
	scrollDown(APT_NGINObj.NGIN.addsan_addbutton);
	waitForAjax();
	click( APT_NGINObj.NGIN.addsan_addbutton, "Add");
	waitforPagetobeenable();
	if(isVisible(APT_NGINObj.NGIN.viewsan_header))
	{
		Reporter.log("Navigated to view SAN page");
		scrollDown(APT_NGINObj.NGIN.viewpage_backbutton);
		waitForAjax();
		click( APT_NGINObj.NGIN.viewpage_backbutton, "Back");
		waitforPagetobeenable();
	}
	else
	{
		Reporter.log("SAN not created");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : SAN not created");
	}

	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Reporter.log("Navigated to view service page");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Navigated to view service page");
	}
	else
	{
		Reporter.log("Not navigated to view service page");
		System.out.println("Not navigated to view service page");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Not navigated to view service page");
	}

	//===========End of Add SAN=========

	//==============Add SAN to perform bulk move======
	scrollDown(APT_NGINObj.NGIN.sanheader);
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	click(APT_NGINObj.NGIN.SANActionDropdown, "Action dropdown");	
	click(APT_NGINObj.NGIN.addsan_link, "Add SAN");
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.addsan_header);
	compareText("Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");

	addDropdownValues_commonMethod("Country", APT_NGINObj.NGIN.customer_country, country);
	click(APT_NGINObj.NGIN.customername,  "Customer Name dropdown");
	waitForAjax();

	if(isVisible(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Existing customer is available");
		Reporter.log("Existing customer is available");
		click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay,"Customer Name dropdown value" );
	}
	else if(isVisible(APT_NGINObj.NGIN.customernamedropdown_nomatchesfound))
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Existing customer is not available");
		Reporter.log("Existing customer is not available");
		click(APT_NGINObj.NGIN.addsan_header,"addsan_header");

		//Manage new customer link
		click(APT_NGINObj.NGIN.managenewcustomer_link, "Manage new customer");
		waitforPagetobeenable();
		compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
		waitForAjax();

		//=========================

		//Add Customer
		//Select Country from dropdown
		addDropdownValues_commonMethod("Country",  APT_NGINObj.NGIN.customer_country, country);
		String CustomerOCN= getAttributeFrom( APT_NGINObj.NGIN.customer_ocn,"value");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : OCN is displaying as: '"+CustomerOCN+"'");
		String CustomerNamevalue= getTextFrom(APT_NGINObj.NGIN.customerpanel_customernamevalue,"customer panel_customer namevalue");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer Name is displaying as: '"+CustomerNamevalue+"'");
		CustomerNamevalue.replace("(New)", "").trim();
		sendKeys( APT_NGINObj.NGIN.resellername, resellername, "Reseller Name");
		
		sendKeys(APT_NGINObj.NGIN.reseller_city, city, "City");
		sendKeys(APT_NGINObj.NGIN.reseller_streetname, streetname, "Street Name");
		sendKeys( APT_NGINObj.NGIN.reseller_streetno, streetno,  "Street Number");
		sendKeys( APT_NGINObj.NGIN.reseller_pobox, pobox, "PO Box");
		sendKeys(APT_NGINObj.NGIN.reseller_zipcode, zipcode, "Zip Code");
		

		if(defaultvalue.equalsIgnoreCase("YES"))
		{
			String Default_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.defaultcheckbox,"checked");
			if(Default_Checkbox.isEmpty())
			{
				System.out.println("Default checkbox is checked");
				Thread.sleep(1000);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Default checkbox is checked");
				Reporter.log("Default checkbox is checked");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Default checkbox is not checked by default");
				Reporter.log("Default checkbox is not checked by default");
			}
		}

		else if(configure.equalsIgnoreCase("YES"))
		{
			String Configure_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.configurecheckbox,"checked");
			if(Configure_Checkbox.isEmpty())
			{
				System.out.println("Configure checkbox is checked");
				waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Configure checkbox is checked");
				Reporter.log("Configure checkbox is checked");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Configure checkbox is not checked");
				Reporter.log("Configure checkbox is not checked");
				click(APT_NGINObj.NGIN.configurecheckbox,"Configure checkbox");
			}
		}
		scrollDown(APT_NGINObj.NGIN.nextbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.nextbutton, "Next");
				waitForAjax();	
		waitforPagetobeenable();
		scrollDown(APT_NGINObj.NGIN.addsan_header);
		compareText("Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");
				waitForAjax();

		//Select Country from dropdown
		addDropdownValues_commonMethod( "Country", APT_NGINObj.NGIN.customer_country, country);
				click(APT_NGINObj.NGIN.customername, "Customer Name dropdown");
				waitForAjax();
				click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay, "Customer Name dropdown value");
	}

	waitForAjax();
	//SAN customer name
	String SAN_Customernamevalue1= getTextFrom(APT_NGINObj.NGIN.customername_selectedtext,"customer name_selected text");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Customer name selected is : " + SAN_Customernamevalue1);
	String SAN_Customername1=SAN_Customernamevalue1.substring(0, SAN_Customernamevalue1.indexOf("(")).trim();

	String SANNumber_CountryCode1=getAttributeFrom(APT_NGINObj.NGIN.san_number,"value");
	sendKeys(APT_NGINObj.NGIN.san_number, bulkmove_sannumber2, "SAN Number");
	//SAN number
	String Bulkmove_SANNumberValue2= SANNumber_CountryCode1+bulkmove_sannumber2;
	ExtentTestManager.getTest().log(LogStatus.PASS, "Step :SAN Number is dsplayed as : " + Bulkmove_SANNumberValue2);

	//Select service profile from dropdown
	addDropdownValues_commonMethod("Service Profile", APT_NGINObj.NGIN.serviceprofile, serviceprofilevalue);
			waitForAjax();
	scrollDown( APT_NGINObj.NGIN.serviceprofile);
			waitForAjax();
	if(RingToNumber_Checkbox.equalsIgnoreCase("YES"))
	{
		click(APT_NGINObj.NGIN.ringtonumber_radiobutton,"'Ring To Number' radio");
		waitForAjax();
		sendKeys(APT_NGINObj.NGIN.ringtonumber_field, ringtonumber,"Ring To Number");
	}
	else if(AnnouncementToPlay_Checkbox.equalsIgnoreCase("YES"))
	{
		click(APT_NGINObj.NGIN.announcementtoplay_radiobutton,"'Announcement to play' radio");
				waitForAjax();
		//Select announcement to play value from dropdown
		click( APT_NGINObj.NGIN.announcementtoplay_dropdown,  "Announcement To Play dropdown");
		click(APT_NGINObj.NGIN.announcementtoplay_dropdownvalue, "Announcement To Play value");
	}
	else if(ComplexRouting_Checkbox.equalsIgnoreCase("YES"))
	{
		click(APT_NGINObj.NGIN.complexroute_radiobutton, "'Complex route' radio");
		Thread.sleep(1000);
		sendKeys(APT_NGINObj.NGIN.routingforpayphone_field, routingforpayphone_value, "Routing for payphone");
		sendKeys(APT_NGINObj.NGIN.routingformobile, routingformobile_value, "Routing for mobile");
		sendKeys(APT_NGINObj.NGIN.defaultrouting, defaultrouting_value, "Default Routing");
		click( APT_NGINObj.NGIN.enablelogicalrouting,  "Enable logical routing checkbox");
		sendKeys(APT_NGINObj.NGIN.defaultroutebusy, defaultroutebusy_value, "Default Route busy");
		sendKeys( APT_NGINObj.NGIN.noanswer, noanswer_value, "No Answer");
		sendKeys(APT_NGINObj.NGIN.networkcongestion, networkcongestion, "Network Congestion");
	}
	scrollDown( APT_NGINObj.NGIN.addsan_addbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.addsan_addbutton, "Add");

	if(isVisible(APT_NGINObj.NGIN.viewsan_header))
	{
		Reporter.log("Navigated to view SAN page");
		scrollDown( APT_NGINObj.NGIN.viewpage_backbutton);
				waitForAjax();
		click(APT_NGINObj.NGIN.viewpage_backbutton,  "Back");
		waitforPagetobeenable();
	}
	else
	{
		Reporter.log("SAN not created");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : SAN not created");
	}

	if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
	{
		Reporter.log("Navigated to view service page");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Navigated to view service page");
	}
	else
	{
		Reporter.log("Not navigated to view service page");
		System.out.println("Not navigated to view service page");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Not navigated to view service page");
	}

	//===========End of Add SAN=========
	
	waitForAjax();
	scrollDown( APT_NGINObj.NGIN.sanheader);
	compareText( "SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");

	WebElement SANGridCheck8= findWebElement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid8= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	if(!SANGrid8.contains("height: 1px"))
	{
		waitForAjax();
		click(APT_NGINObj.NGIN.SANActionDropdown, "SAN Action dropdown");
		click(APT_NGINObj.NGIN.bulkmove_link, "Bulk Move");
		waitForAjax();
		waitforPagetobeenable();
		compareText( "Bulk Move Header", APT_NGINObj.NGIN.bulkmoveheader, "Bulk Move");
		click(APT_NGINObj.NGIN.cancelbutton,  "Cancel");
		waitforPagetobeenable();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Bulk Move is cancelled");
		Reporter.log("Bulk Move is cancelled");
	}
	else
	{
		Reporter.log("SAN is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN is not added in the grid");
	}

	//Bulk Move
	scrollDown( APT_NGINObj.NGIN.sanheader);
	compareText( "SAN/FRC Header",  APT_NGINObj.NGIN.sanheader, "SAN/FRC");

	WebElement SANGridCheck9= findWebElement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
	String SANGrid9= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
	if(!SANGrid9.contains("height: 1px"))
	{
		waitForAjax();
		compareText( "SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
		click(APT_NGINObj.NGIN.SANActionDropdown, "SAN Action dropdown");
		click(APT_NGINObj.NGIN.bulkmove_link,"Bulk Move");
		waitForAjax();
		waitforPagetobeenable();
		compareText(APT_NGINObj.NGIN.bulkmoveheader, "Bulk Move", "Bulk Move Header");
		click(APT_NGINObj.NGIN.movebutton_sanmove,  "Move");
		//warning message check
		verifyExists( APT_NGINObj.NGIN.bulkmove_countrywarngmsg, "Country");
		verifyExists(APT_NGINObj.NGIN.bulkmove_customerwarngmsg, "Customer");
		verifyExists(APT_NGINObj.NGIN.filterfrcnumber_warngmsg, "Filter FRC Number");
		verifyExists( APT_NGINObj.NGIN.bulkmove_Servicewarngmsg, "Service");
		addDropdownValues_commonMethod( "Country", APT_NGINObj.NGIN.bulkmove_countrydropdown, bulkmove_country);
		addDropdownValues_commonMethod( "Customer", APT_NGINObj.NGIN.bulkmove_customerdropdown, bulkmove_customer);
		click( APT_NGINObj.NGIN.filterfrcnumberdropdown,  "Filter FRC Number Dropdown");
		WebElement FilterFRC_Dropdownvalueselect1=  webDriver.findElement(By.xpath("//div[label[text()='Filter FRC Number']]//div[@role='list']//div//div[2]/div//div[contains(text(),'"+Bulkmove_SANNumberValue1+"')]/preceding-sibling::input[@type='checkbox']"));
		webDriver.findElement(By.xpath("//div[label[text()='Filter FRC Number']]//div[@role='list']//div//div[2]/div//div[contains(text(),'"+Bulkmove_SANNumberValue1+"')]/preceding-sibling::input[@type='checkbox']")).click();

		WebElement FilterFRC_Dropdownvalueselect2=  webDriver.findElement(By.xpath("//div[label[text()='Filter FRC Number']]//div[@role='list']//div//div[2]/div//div[contains(text(),'"+Bulkmove_SANNumberValue2+"')]/preceding-sibling::input[@type='checkbox']"));
	    webDriver.findElement(By.xpath("//div[label[text()='Filter FRC Number']]//div[@role='list']//div//div[2]/div//div[contains(text(),'"+Bulkmove_SANNumberValue2+"')]/preceding-sibling::input[@type='checkbox']")).click();
	    waitForAjax();
		addDropdownValues_commonMethod("Service", APT_NGINObj.NGIN.bulkmove_servicedropdown, bulkmove_service);
		//GetText(application, "Remarks", "remarktextarea");
		click( APT_NGINObj.NGIN.movebutton_sanmove,"Move");
		 waitForAjax();
		waitforPagetobeenable();
		verifysuccessmessage( "San Bulk Moved Successfully");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Bulk Move is successful");
		Reporter.log("Bulk Move is successful");
	}
	else
	{
		Reporter.log("SAN is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : SAN is not added in the grid");
	}

	//verify SAN in destination customer service
	
	scrollDown(APT_NGINObj.NGIN.ManageCustomerServiceLink);
	waitForAjax();
	mouseMoveOn(APT_NGINObj.NGIN.ManageCustomerServiceLink);

	click(APT_NGINObj.NGIN.searchorderlink,"searchorderlink");
	waitForAjax();
	sendKeys(APT_NGINObj.NGIN.servicefield,bulkmove_service,"servicefield");
	waitForAjax();

	//WebElement searchbutton= getwebelement(APT_NGINObj.NGIN.searchbutton);
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	click(APT_NGINObj.NGIN.searchbutton,"searchbutton");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.serviceradiobutton,"serviceradiobutton");
			waitForAjax();
	click(APT_NGINObj.NGIN.searchorder_actiondropdown,"searchorder_actiondropdown");
			waitForAjax();
	click(APT_NGINObj.NGIN.view,"view");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.sanheader);
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");

	WebElement AddedSAN11= findWebElement("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+Bulkmove_SANNumberValue1+"')]/parent::div//span[contains(@class,'unchecked')]");
	WebElement AddedSAN12= findWebElement("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+Bulkmove_SANNumberValue2+"')]/parent::div//span[contains(@class,'unchecked')]");
	
	//		int AddedSANCount= AddedSAN11.size();
	//		System.out.println("BulkSANAdded");
	if(AddedSAN11.isDisplayed() && AddedSAN12.isDisplayed())
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Bulk Move verification is successfull in destination customer");
		Reporter.log("Bulk Move verification is successfull in destination customer");
	}
	else
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Bulk SAN is not moved successfully into destination customer");
		Reporter.log("Bulk SAN is not moved successfully into destination customer");
	}
	
}

public void verifyManageAddnlFRC(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String resellername = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerName");
	String defaultvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DefaultValue_checkbox");
	String configure = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Configure_checkbox");
	String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Customer_Country");
	String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
	String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
	String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_City");
	String streetname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetName");
	String streetno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_StreetNumber");
	String pobox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_POBox");
	String zipcode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reseller_Zipcode");
	String sannumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AddSAN_SANNumber");
	String predestinationnumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PreDestinationNumber");
	String ringtonumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Ringtonumber");
	String routingforpayphone_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingforpayphone_value");
	String routingformobile_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Routingformobile_value");
	String defaultrouting_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Defaultrouting_value");
	String RingToNumber_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "RingToNumber_Checkbox");
	String AnnouncementToPlay_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "AnnouncementToPlay_Checkbox");
	String ComplexRouting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ComplexRouting_Checkbox");
	String defaultroutebusy_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "defaultroutebusy_value");
	String noanswer_value = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "noanswer_value");
	String networkcongestion = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "networkcongestion");
	String serviceprofilevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceProfile");
	String maxcallduration = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Maxcallduration");
	String chargebandname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Chargebandname");
	String internationaloutgoingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalOutgoingCalls_checkbox");
	String internationalincomingcalls_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InternationalIncomingCalls_checkbox");
	String mobilecallsallowed_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MobileCallsAllowed_checkbox");
	String payphoneblocking_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PayphoneBlockingenabled_checkbox");
	String supervisionfieldvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Supervisionfieldvalue");
	String noreplytimervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NoReplyTimerValue");
	String webaccessblocked_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "WebAccessBlocked_checkbox");
	String cpsfreeformatvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CPSFreeFormatValue");
	String sanblock_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANBlock_Checkbox");
	String focenabled_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "FOCEnabled_Checkbox");
	String enablelogicalrouting_Checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnableLogicalRouting_Checkbox");
	String enablepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnablePriceAnnouncement_Checkbox");
	String bulkmove_sannumber2 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_SANNumberValue2");
	String bulkmove_sannumber1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_SANNumberValue1");
	String bulkmove_service = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_Service");

	String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceIdentification");
	String customernamevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerNameValue");
	String sannumbervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue");
	String bulkmove_country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_Country");
	String bulkmove_customer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Bulkmove_Service");
	String edit_enablecallerconfirmation_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_EnableCallerConfirmation_Checkbox");
	String sannumbervalue1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue1");
	String callerconfirmationannouncementvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CallerConfirmationAnnouncementValue");
	String select_sansearchtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SelectSANSearchType");
	String edit_priceannoriginvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_PriceAnnOriginvalue");
	String edit_interruptiblepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_InterruptiblePriceAnnouncement_Checkbox");
	String edit_sendfci_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_SendFCI_Checkbox");
	String edit_sendsci_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_SendSCI_Checkbox");
	String edit_chargebandname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_ChargeBandName");
	String edit_callerconfirmationannouncementvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_CallerConfirmationAnnouncementValue");
	String edit_callerconfirmationdigitvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_CallerConfirmationDigitValue");
	String numberofrepetitionsallowedvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NumberOfRepetitionsAllowedValue");
	String callerconfirmationdigitvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CallerConfirmationDigitValue");
	String enablecallerconfirmation_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EnableCallerConfirmation_Checkbox");
	String priceannouncementvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PriceAnnouncementValue");
	String priceannoriginvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PriceAnnOriginvalue");
	String edit_priceannouncementvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_PriceAnnouncementValue");
	String edit_enablepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Edit_EnablePriceAnnouncement_Checkbox");
	String valueinprice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ValueInPrice");
	String SANNumberValue1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue1");
	String interruptiblepriceannouncement_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "InterruptiblePriceAnnouncement_Checkbox");
	String sendfci_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SendFCI_Checkbox");
	String sendsci_checkbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SendSCI_Checkbox");

	
	mouseMoveOn(APT_NGINObj.NGIN.ManageCustomerServiceLink);
	waitForAjax();

	click(APT_NGINObj.NGIN.searchorderlink,"searchorderlink");
	waitForAjax();
	sendKeys(APT_NGINObj.NGIN.servicefield,sid,"servicefield");
	waitForAjax();

	WebElement searchbutton1= findWebElement(APT_NGINObj.NGIN.searchbutton);
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	click(APT_NGINObj.NGIN.searchbutton,"searchbutton");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.searchbutton);
	waitForAjax();
	click(APT_NGINObj.NGIN.serviceradiobutton,"serviceradiobutton");
	waitForAjax();
	click(APT_NGINObj.NGIN.searchorder_actiondropdown,"searchorder");
	waitForAjax();
	click(APT_NGINObj.NGIN.view,"view");
	waitForAjax();
	waitforPagetobeenable();
	scrollDown(APT_NGINObj.NGIN.sanheader);
	compareText("SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	
	//==============Add SAN to perform bulk move======

		click(APT_NGINObj.NGIN.SANActionDropdown, "Action dropdown");	
		click(APT_NGINObj.NGIN.addsan_link,"Add SAN");
		scrollDown(APT_NGINObj.NGIN.addsan_header);
		waitforPagetobeenable();
		compareText("Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");

		addDropdownValues_commonMethod("Country",  APT_NGINObj.NGIN.customer_country, country);
		click(APT_NGINObj.NGIN.customername,"Customer Name dropdown");
		waitForAjax();

		if(isVisible(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Existing customer is available");
			Reporter.log("Existing customer is available");
			click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay,"Customer Name dropdown value");
		}
		else if(isVisible(APT_NGINObj.NGIN.customernamedropdown_nomatchesfound))
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Existing customer is not available");
			Reporter.log("Existing customer is not available");
			click(APT_NGINObj.NGIN.addsan_header,"addsan_header");

			//Manage new customer link
			click(APT_NGINObj.NGIN.managenewcustomer_link,  "Manage new customer");
			waitforPagetobeenable();
			compareText("Manage Customer in OSP header", APT_NGINObj.NGIN.manageresellerheader, "Manage Customer In OSP");
			waitForAjax();

			//=========================

			//Add Customer
			//Select Country from dropdown
			addDropdownValues_commonMethod("Country", APT_NGINObj.NGIN.customer_country, country);
			String CustomerOCN= getAttributeFrom(APT_NGINObj.NGIN.customer_ocn,"value");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : OCN is displaying as: '"+CustomerOCN+"'");
			String CustomerNamevalue= getTextFrom(APT_NGINObj.NGIN.customerpanel_customernamevalue,"customer panel_customer namevalue");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Customer Name is displaying as: '"+CustomerNamevalue+"'");
			CustomerNamevalue.replace("(New)", "").trim();
			sendKeys(APT_NGINObj.NGIN.resellername, resellername, "Reseller Name");
			//GetText(application, "Email", "reseller_email");
			sendKeys(APT_NGINObj.NGIN.reseller_city, city,  "City");
			sendKeys(APT_NGINObj.NGIN.reseller_streetname, streetname, "Street Name");
			sendKeys(APT_NGINObj.NGIN.reseller_streetno, streetno, "Street Number");
			sendKeys(APT_NGINObj.NGIN.reseller_pobox, pobox, "PO Box");
			sendKeys(APT_NGINObj.NGIN.reseller_zipcode, zipcode, "Zip Code");
			

			if(defaultvalue.equalsIgnoreCase("YES"))
			{
				String Default_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.defaultcheckbox,"checked");
				if(Default_Checkbox.isEmpty())
				{
					System.out.println("Default checkbox is checked");
					Thread.sleep(1000);
					ExtentTestManager.getTest().log(LogStatus.PASS, "Default checkbox is checked");
					Reporter.log("Default checkbox is checked");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Default checkbox is not checked by default");
					Reporter.log("Default checkbox is not checked by default");
				}
			}

			else if(configure.equalsIgnoreCase("YES"))
			{
				String Configure_Checkbox= getAttributeFrom(APT_NGINObj.NGIN.configurecheckbox,"checked");
				if(Configure_Checkbox.isEmpty())
				{
					System.out.println("Configure checkbox is checked");
					Thread.sleep(1000);
					ExtentTestManager.getTest().log(LogStatus.PASS, "Configure checkbox is checked");
					Reporter.log("Configure checkbox is checked");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Configure checkbox is not checked");
					Reporter.log("Configure checkbox is not checked");
					click(APT_NGINObj.NGIN.configurecheckbox, "Configure checkbox");
				}
			}
			scrollDown(APT_NGINObj.NGIN.nextbutton);
			waitForAjax();
			click(APT_NGINObj.NGIN.nextbutton, "Next");
			waitForAjax();
			waitforPagetobeenable();
			scrollDown(APT_NGINObj.NGIN.addsan_header);
			compareText("Add SAN header", APT_NGINObj.NGIN.addsan_header, "Add SAN");
			waitForAjax();

			//Select Country from dropdown
			addDropdownValues_commonMethod("Country", APT_NGINObj.NGIN.customer_country, country);
			click(APT_NGINObj.NGIN.customername, "Customer Name dropdown");
			waitForAjax();
			click(APT_NGINObj.NGIN.customernamedropdown_valuesdisplay,  "Customer Name dropdown value");
		}
		
		waitForAjax();
		//SAN customer name
		String SAN_Customernamevalue= getTextFrom(APT_NGINObj.NGIN.customername_selectedtext,"customer name_selected text");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Customer name selected is : " + SAN_Customernamevalue);
		String SAN_Customername=SAN_Customernamevalue.substring(0, SAN_Customernamevalue.indexOf("(")).trim();

		String SANNumber_CountryCode=getAttributeFrom(APT_NGINObj.NGIN.san_number,"value");
		sendKeys(APT_NGINObj.NGIN.san_number, sannumbervalue1, "SAN Number");
		//SAN number
		String Bulkmove_SANNumberValue2= SANNumber_CountryCode+sannumbervalue1;
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step :SAN Number is dsplayed as : " + Bulkmove_SANNumberValue2);

		//Select service profile from dropdown
		addDropdownValues_commonMethod("Service Profile", APT_NGINObj.NGIN.serviceprofile, serviceprofilevalue);
				waitForAjax();
		scrollDown( APT_NGINObj.NGIN.serviceprofile);
				waitForAjax();
		if(RingToNumber_Checkbox.equalsIgnoreCase("YES"))
		{
			click(APT_NGINObj.NGIN.ringtonumber_radiobutton,"'Ring To Number' radio");
			waitForAjax();
			sendKeys(APT_NGINObj.NGIN.ringtonumber_field, ringtonumber,"Ring To Number");
		}
		else if(AnnouncementToPlay_Checkbox.equalsIgnoreCase("YES"))
		{
			click(APT_NGINObj.NGIN.announcementtoplay_radiobutton,"'Announcement to play' radio");
					waitForAjax();
			//Select announcement to play value from dropdown
			click( APT_NGINObj.NGIN.announcementtoplay_dropdown,  "Announcement To Play dropdown");
			click(APT_NGINObj.NGIN.announcementtoplay_dropdownvalue, "Announcement To Play value");
		}
		else if(ComplexRouting_Checkbox.equalsIgnoreCase("YES"))
		{
			click(APT_NGINObj.NGIN.complexroute_radiobutton, "'Complex route' radio");
			Thread.sleep(1000);
			sendKeys(APT_NGINObj.NGIN.routingforpayphone_field, routingforpayphone_value, "Routing for payphone");
			sendKeys(APT_NGINObj.NGIN.routingformobile, routingformobile_value, "Routing for mobile");
			sendKeys(APT_NGINObj.NGIN.defaultrouting, defaultrouting_value, "Default Routing");
			click( APT_NGINObj.NGIN.enablelogicalrouting,  "Enable logical routing checkbox");
			sendKeys(APT_NGINObj.NGIN.defaultroutebusy, defaultroutebusy_value, "Default Route busy");
			sendKeys( APT_NGINObj.NGIN.noanswer, noanswer_value, "No Answer");
			sendKeys(APT_NGINObj.NGIN.networkcongestion, networkcongestion, "Network Congestion");
		}
		scrollDown( APT_NGINObj.NGIN.addsan_addbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.addsan_addbutton, "Add");

		if(isVisible(APT_NGINObj.NGIN.viewsan_header))
		{
			Reporter.log("Navigated to view SAN page");
			scrollDown( APT_NGINObj.NGIN.viewpage_backbutton);
					waitForAjax();
			click(APT_NGINObj.NGIN.viewpage_backbutton,  "Back");
			waitforPagetobeenable();
		}
		else
		{
			Reporter.log("SAN not created");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : SAN not created");
		}

		if(isVisible(APT_NGINObj.NGIN.customerdetailsheader))
		{
			Reporter.log("Navigated to view service page");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Navigated to view service page");
		}
		else
		{
			Reporter.log("Not navigated to view service page");
			System.out.println("Not navigated to view service page");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Not navigated to view service page");
		}

		//===========End of Add SAN=========
		
		//SANFilter
		scrollDown( APT_NGINObj.NGIN.sanheader);
		waitForAjax();
compareText( "SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
click(APT_NGINObj.NGIN.sanpanel_searchsanmenu,  "Search San Menu");
click(APT_NGINObj.NGIN.select_sansearchtype, "Select SAN Search type");
sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype, "Enter SAN Search type");
findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, SANNumberValue1,"SAN Number Search");
findWebElement(APT_NGINObj.NGIN.sanheader).click();

WebElement SANGridCheck10= findWebElement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
String SANGrid10= getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
System.out.println("SAN Number displaying: " +SANNumberValue1);
WebElement AddedSAN10=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+SANNumberValue1+"')]/parent::div//span[contains(@class,'unchecked')]"));
if(!SANGrid10.contains("height: 1px"))
{
	webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+SANNumberValue1+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
	waitForAjax();
	compareText( "SAN/FRC Header", APT_NGINObj.NGIN.sanheader, "SAN/FRC");
	click("SAN Action dropdown", APT_NGINObj.NGIN.SANActionDropdown);
	click( "Manage Addnl FRC", APT_NGINObj.NGIN.manageaddnlfrc_link);
	waitForAjax();
	waitforPagetobeenable();
	compareText( "Manage Addnl FRC Header", APT_NGINObj.NGIN.manageaddnlfrc_header, "Manage Addnl FRC");
	click( APT_NGINObj.NGIN.manageaddnlfrc_actiondropdown, "Action Dropdown");
	click( APT_NGINObj.NGIN.addaddnlfrc_link, "Add Addnl FRC");
	waitForAjax();
	waitforPagetobeenable();
	compareText( "Add Price Announcement Header", APT_NGINObj.NGIN.addpriceannouncement_header, "Add Price Announcement");
	compareText( "SAN/FRC", APT_NGINObj.NGIN.sanfrcvalue, SANNumberValue1);
	compareText( "Customer Name", APT_NGINObj.NGIN.customernamevalue, customernamevalue);
	addCheckbox_commonMethod( APT_NGINObj.NGIN.enablepriceannouncement_checkbox, "Enable price announcement", enablepriceannouncement_checkbox);
	addDropdownValues_commonMethod("Price Announcement",APT_NGINObj.NGIN.priceannouncement_dropdown, priceannouncementvalue);
	//addDropdownValues_commonMethod(application, "Price Ann Origin", "priceannorigin_dropdown", priceannoriginvalue, xml);
	
	boolean availability=false;
	try {  
	  availability=isVisible(APT_NGINObj.NGIN.priceannorigin_dropdown);
	  if(availability) {
		  ExtentTestManager.getTest().log(LogStatus.PASS, "Price Ann Origin dropdown is displaying");
		  System.out.println("Price Ann Origin dropdown is displaying");
		  
		  if(priceannoriginvalue.equalsIgnoreCase("null")) {
			  
			  ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under Price Ann Origin dropdown");
			  System.out.println(" No values selected under Price Ann Origin dropdown");
		  }else {
			  
			   webDriver.findElement(By.xpath("//div[label[text()='Price Ann Origin']]//div[text()='�']")).click();
			  waitForAjax();
			  
			  //verify list of values inside dropdown
			  List<WebElement> listofvalues =   webDriver.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));
			  
			  ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside Price Ann Origin dropdown is:  ");
			  System.out.println( " List of values inside Price Ann Origin dropdown is:  ");
			  
				for (WebElement valuetypes : listofvalues) {
							Reporter.log("service sub types : " + valuetypes.getText());
							ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
							System.out.println(" " + valuetypes.getText());
				}
				
				Thread.sleep(2000);
			sendKeys(APT_NGINObj.NGIN.priceannoriginvalue, priceannoriginvalue,"priceannoriginvalue");	
			waitForAjax();
				
			 webDriver.findElement(By.xpath("(//div[@class='modal-body']//div[contains(text(),'"+ priceannoriginvalue +"')])[1]")).click();
				waitForAjax();
			  
			
			  
		  }
	  }else {
		  ExtentTestManager.getTest().log(LogStatus.FAIL, "Price Ann Origin is not displaying");
	  }
	}catch(NoSuchElementException e) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Price Ann Origin is not displaying");
	}catch(Exception ee) {
		ee.printStackTrace();
		ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under Price Ann Origin dropdown");
	}

	addCheckbox_commonMethod( APT_NGINObj.NGIN.interruptible_checkbox, "Interruptable Price Announcement", interruptiblepriceannouncement_checkbox);
	sendKeys(APT_NGINObj.NGIN.valueinprice_field, valueinprice,  "Value In Price In(Cents)");
	addCheckbox_commonMethod( APT_NGINObj.NGIN.sendfci_checkbox, "Send FCI", sendfci_checkbox);
	addCheckbox_commonMethod( APT_NGINObj.NGIN.sendsci_checkbox, "Send SCI", sendsci_checkbox);
	addDropdownValues_commonMethod( "Charge Band Name", APT_NGINObj.NGIN.addfrc_chargebandname, chargebandname);
	addCheckbox_commonMethod( APT_NGINObj.NGIN.enablecallerconfirmation_checkbox, "Enable Caller Confirmation", enablecallerconfirmation_checkbox);
	addDropdownValues_commonMethod( "Caller Confirmation Announcement", APT_NGINObj.NGIN.callerconfirmationannouncement_dropdown, callerconfirmationannouncementvalue);
	sendKeys(APT_NGINObj.NGIN.callerconfirmationdigit_field, callerconfirmationdigitvalue,  "Caller Confirmation Digit");
	sendKeys(APT_NGINObj.NGIN.noofrepetitionallowed_field, numberofrepetitionsallowedvalue, "Number Of Repetation Allowed");
	waitForAjax();
	scrollDown( APT_NGINObj.NGIN.OkButton);
	waitForAjax();
	click(APT_NGINObj.NGIN.OkButton, "OK");
	waitForAjax();
	waitforPagetobeenable();
	verifysuccessmessage( "Addnl Frc successfully created.");
	compareText( "Manage Addnl FRC Header", APT_NGINObj.NGIN.manageaddnlfrc_header, "Manage Addnl FRC");

	//verify table data in manage addnl frc page
	
	click(APT_NGINObj.NGIN.manageaddnlfrc_priceannouncement_value,"manageaddnlfrc_priceannouncement_value");
	WebElement priceannouncement= findWebElement(APT_NGINObj.NGIN.manageaddnlfrc_priceannouncement_value);
	priceannouncement.sendKeys(Keys.TAB);
	
	WebElement maxcallflag= findWebElement(APT_NGINObj.NGIN.manageaddnlfrc_maxcallflag_value);
	click(APT_NGINObj.NGIN.manageaddnlfrc_maxcallflag_value,"manageaddnlfrc_maxcallflag_value");
	maxcallflag.sendKeys(Keys.TAB);
	WebElement maxcallorigin= findWebElement(APT_NGINObj.NGIN.manageaddnlfrc_maxcallorigin_value);
	click(APT_NGINObj.NGIN.manageaddnlfrc_maxcallorigin_value,"manageaddnlfrc_maxcallorigin_value");
	maxcallorigin.sendKeys(Keys.TAB);

	
	scrollDown(APT_NGINObj.NGIN.viewpage_backbutton);
	waitForAjax();
	click( APT_NGINObj.NGIN.viewpage_backbutton,  "Back");
	waitforPagetobeenable();
}
else
{
	Reporter.log("SAN is not added in the grid");
}

scrollDown(APT_NGINObj.NGIN.sanheader);
//SANFilter
click(APT_NGINObj.NGIN.sanpanel_searchsanmenu, "Search San Menu");
click(APT_NGINObj.NGIN.select_sansearchtype, "Select SAN Search type");
sendKeys(APT_NGINObj.NGIN.select_sansearchtype, select_sansearchtype,  "Enter SAN Search type");
findWebElement(APT_NGINObj.NGIN.select_sansearchtype).sendKeys(Keys.ENTER);
clearTextBox(APT_NGINObj.NGIN.sannumbersearchfield);
sendKeys(APT_NGINObj.NGIN.sannumbersearchfield, SANNumberValue1,  "SAN Number Search");
click(APT_NGINObj.NGIN.sanheader,"sanheader");

WebElement SANGridCheck11= findWebElement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
String SANGrid11=getAttributeFrom(APT_NGINObj.NGIN.SANGridCheck,"style");
WebElement AddedSAN11=  webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+SANNumberValue1+"')]/parent::div//span[contains(@class,'unchecked')]"));
if(!SANGrid11.contains("height: 1px"))
{
	 webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+SANNumberValue1+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
	waitForAjax();
	
	click(APT_NGINObj.NGIN.SANActionDropdown,  "SAN Action dropdown");
	click("Manage Addnl FRC", APT_NGINObj.NGIN.manageaddnlfrc_link);
	waitforPagetobeenable();

	//verify edit addnl frc
	WebElement AddnlFRCGridCheck= findWebElement("//div[@ref='eBodyContainer']");
	String AddnlFRCGrid= AddnlFRCGridCheck.getAttribute("style");
	WebElement AddedAddnlFRC= findWebElement("//div[@ref='eBodyContainer']//span[@class='ag-selection-checkbox']//span[2]");
	String AddedAddnlFRC_checkbox= AddedAddnlFRC.getAttribute("class");
	if(!AddnlFRCGrid.contains("height: 1px"))
	{
		if(AddedAddnlFRC_checkbox.equalsIgnoreCase("ag-icon ag-icon-checkbox-unchecked"))
		{
			 webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//span[@class='ag-selection-checkbox']//span[2]")).click();
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Added Addnl FRC is already selected");
			Reporter.log("Added Addnl FRC is already selected");
		}
		waitForAjax();
		click(APT_NGINObj.NGIN.manageaddnlfrc_actiondropdown, "Action Dropdown");
		click(APT_NGINObj.NGIN.edit, "Edit");
		waitForAjax();
		waitforPagetobeenable();
		
		addCheckbox_commonMethod(APT_NGINObj.NGIN.enablepriceannouncement_checkbox, "Enable price announcement checkbox", edit_enablepriceannouncement_checkbox);
		addDropdownValues_commonMethod( "Price Announcement", APT_NGINObj.NGIN.priceannouncement_dropdown, edit_priceannouncementvalue);
		//addDropdownValues_commonMethod(application, "Price Ann Origin", "priceannorigin_dropdown", edit_priceannoriginvalue, xml);
		boolean availability=false;
		try {  
			availability=isVisible(APT_NGINObj.NGIN.priceannorigin_dropdown);
			if(availability) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Price Ann Origin dropdown is displaying");
				System.out.println("Price Ann Origin dropdown is displaying");

				if(edit_priceannoriginvalue.equalsIgnoreCase("null")) {

					ExtentTestManager.getTest().log(LogStatus.PASS, " No values selected under Price Ann Origin dropdown");
					System.out.println(" No values selected under Price Ann Origin dropdown");
				}else {

					 webDriver.findElement(By.xpath("//div[label[text()='Price Ann Origin']]//div[text()='�']")).click();
				waitForAjax();

					//verify list of values inside dropdown
					List<WebElement> listofvalues = webDriver
							.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));

					ExtentTestManager.getTest().log(LogStatus.PASS, " List of values inside Price Ann Origin dropdown is:  ");
					System.out.println( " List of values inside Price Ann Origin dropdown is:  ");

					for (WebElement valuetypes : listofvalues) {
						Reporter.log("service sub types : " + valuetypes.getText());
						ExtentTestManager.getTest().log(LogStatus.PASS," " + valuetypes.getText());
						System.out.println(" " + valuetypes.getText());
					}

					waitForAjax();
					sendKeys(APT_NGINObj.NGIN.priceannoriginvalue ,edit_priceannoriginvalue,"edit_priceannoriginvalue");	
					waitForAjax();

					 webDriver.findElement(By.xpath("(//div[contains(text(),'"+ edit_priceannoriginvalue +"')])[2]")).click();
					Thread.sleep(3000);


				}
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Price Ann Origin is not displaying");
			}
		}catch(NoSuchElementException e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Price Ann Origin is not displaying");
		}catch(Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to perform selection under Price Ann Origin dropdown");
		}
		editcheckbox_commonMethod( edit_interruptiblepriceannouncement_checkbox, APT_NGINObj.NGIN.interruptible_checkbox, "Interruptible Price Announcement");
		sendKeys(APT_NGINObj.NGIN.valueinprice_field, valueinprice,"Value In Price In(Cents)");
		editcheckbox_commonMethod( edit_sendfci_checkbox,  APT_NGINObj.NGIN.sendfci_checkbox, "Send FCI");
		editcheckbox_commonMethod( edit_sendsci_checkbox,  APT_NGINObj.NGIN.sendsci_checkbox, "Send SCI");
		addDropdownValues_commonMethod( "Charge Band Name",  APT_NGINObj.NGIN.addfrc_chargebandname, edit_chargebandname);
		editcheckbox_commonMethod( edit_enablecallerconfirmation_checkbox,  APT_NGINObj.NGIN.enablecallerconfirmation_checkbox, "Enable Caller Confirmation");
		addDropdownValues_commonMethod( "Caller Confirmation Announcement",  APT_NGINObj.NGIN.callerconfirmationannouncement_dropdown, edit_callerconfirmationannouncementvalue);
		sendKeys(APT_NGINObj.NGIN.callerconfirmationdigit_field, edit_callerconfirmationdigitvalue,"Caller Confirmation Digit");
		sendKeys( APT_NGINObj.NGIN.noofrepetitionallowed_field, numberofrepetitionsallowedvalue,"Number Of Repetation Allowed");
		Thread.sleep(2000);
		scrollDown(APT_NGINObj.NGIN.OkButton);
		Thread.sleep(1000);
		click(APT_NGINObj.NGIN.OkButton, "OK");
		waitforPagetobeenable();
		verifysuccessmessage( "Addnl Frc successfully updated.");
	}
	else
	{
		Reporter.log("Addnl FRC is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Addnl FRC is not added in the grid");
	}
	//verify view addnl frc
	WebElement AddnlFRCGridCheck1= findWebElement("//div[@ref='eBodyContainer']");
	String AddnlFRCGrid1= AddnlFRCGridCheck1.getAttribute("style");
	WebElement AddedAddnlFRC1= findWebElement("//div[@ref='eBodyContainer']//span[@class='ag-selection-checkbox']//span[2]");
	String AddedAddnlFRC1_checkbox= AddedAddnlFRC1.getAttribute("class");
	if(!AddnlFRCGrid1.contains("height: 1px"))
	{
		if(AddedAddnlFRC1_checkbox.equalsIgnoreCase("ag-icon ag-icon-checkbox-unchecked"))
		{
			 webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//span[@class='ag-selection-checkbox']//span[2]")).click();
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Added Addnl FRC is already selected");
			Reporter.log("Added Addnl FRC is already selected");
		}
		Thread.sleep(1000);
		click(APT_NGINObj.NGIN.manageaddnlfrc_actiondropdown, "Action Dropdown");
		click(APT_NGINObj.NGIN.view, "View");
	waitForAjax();
		waitforPagetobeenable();
		
		String EnablePriceAnnouncementCheckbox_disabled= getAttributeFrom(APT_NGINObj.NGIN.enablepriceannouncement_checkbox,"disabled");
		String EnablePriceAnnouncementCheckbox_checked= getAttributeFrom(APT_NGINObj.NGIN.enablepriceannouncement_checkbox,"checked");
		if(EnablePriceAnnouncementCheckbox_disabled!=null && EnablePriceAnnouncementCheckbox_checked!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Enable Price Announcement checkbox is checked & disabled");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Enable Price Announcement checkbox is disabled");
		}

		if(isVisible(APT_NGINObj.NGIN.priceannouncementvalue_viewpage))
		{
			
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " No value selected in Price Announcement dropdown");
		}
		if(isVisible(APT_NGINObj.NGIN.priceannoriginvalue_viewpage))
		{
			verifyExists(APT_NGINObj.NGIN.priceannoriginvalue_viewpage,"priceannoriginvalue_viewpage");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "No value selected in Price Ann Origin dropdown");
		}
		if(getAttributeFrom(APT_NGINObj.NGIN.interruptible_checkbox,"checked")!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interruptible Price Announcement checkbox is checked");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interruptible Price Announcement checkbox is not checked");
		}
		
		if(getAttributeFrom(APT_NGINObj.NGIN.sendfci_checkbox,"checked")!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Send FCI checkbox is checked");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Send FCI checkbox is not checked");
		}
		if(getAttributeFrom(APT_NGINObj.NGIN.sendsci_checkbox,"checked")!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Send SCI checkbox is checked");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Send SCI checkbox is not checked");
		}

		if(isVisible(APT_NGINObj.NGIN.chargebandnamevalue_viewpage))
		{
			verifyExists(APT_NGINObj.NGIN.chargebandnamevalue_viewpage,"chargebandnamevalue_viewpage");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "No value selected in Charge Band Name dropdown");
		}
		if(getAttributeFrom(APT_NGINObj.NGIN.enablecallerconfirmation_checkbox,"checked")!=null)
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Enable Caller Confirmation checkbox is checked");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Enable Caller Confirmation checkbox is not checked");
		}
		if(isVisible(APT_NGINObj.NGIN.callerconfirmationannouncement_dropdown))
		{
			verifyExists(APT_NGINObj.NGIN.callerconfirmationannouncement_dropdown,"callerconfirmationannouncement_dropdown");
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " No value selected in Caller Confirmation Announcement dropdown");
		}
		
		
		scrollDown(APT_NGINObj.NGIN.noofrepetitionallowed_field);
		click(APT_NGINObj.NGIN.viewaddnlfrc_closesymbol, "Close");
		waitForAjax();
	}
	else
	{
		Reporter.log("Addnl FRC is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Addnl FRC is not added in the grid");
	}

	//verify Delete addnl frc
	WebElement AddnlFRCGridCheck2= findWebElement("//div[@ref='eBodyContainer']");
	String AddnlFRCGrid2= AddnlFRCGridCheck2.getAttribute("style");
	WebElement AddedAddnlFRC2= findWebElement("//div[@ref='eBodyContainer']//span[@class='ag-selection-checkbox']//span[2]");
	String AddedAddnlFRC2_checkbox= AddedAddnlFRC2.getAttribute("class");
	if(!AddnlFRCGrid2.contains("height: 1px"))
	{
		if(AddedAddnlFRC2_checkbox.equalsIgnoreCase("ag-icon ag-icon-checkbox-unchecked"))
		{
			 webDriver.findElement(By.xpath("//div[@ref='eBodyContainer']//span[@class='ag-selection-checkbox']//span[2]")).click();
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Added Addnl FRC is already selected");
			Reporter.log("Added Addnl FRC is already selected");
		}
		waitForAjax();
		click(APT_NGINObj.NGIN.manageaddnlfrc_actiondropdown, "Action Dropdown");
		click(APT_NGINObj.NGIN.delete, "Delete Addnl FRC");
		waitForAjax();
		WebElement DeleteAlertPopup= findWebElement(APT_NGINObj.NGIN.delete_alertpopup);
		if(DeleteAlertPopup.isDisplayed())
		{
			click(APT_NGINObj.NGIN.frc_deletebutton, "Delete");
			waitForAjax();
			verifysuccessmessage( "Addnl Frc successfully deleted.");
		}
		else
		{
			Reporter.log("Delete alert popup is not displayed");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
		}
		Thread.sleep(2000);
		scrollDown(APT_NGINObj.NGIN.viewpage_backbutton);
		waitForAjax();
		click(APT_NGINObj.NGIN.viewpage_backbutton,  "Back");
		waitforPagetobeenable();
		waitForAjax();
	}
	else
	{
		Reporter.log("Addnl FRC is not added in the grid");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Addnl FRC is not added in the grid");
	}
}
else
{
	Reporter.log("SAN is not added in the grid");
}
	
}


public void verifyAllDeleteOperations(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	String customernamevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerNameValue");
	String select_sansearchtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SelectSANSearchType");
	
	String sannumbervalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANNumberValue");
	String customadm = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CustomerAdministration");
	String sanadm = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SANAdministration");
	String reselladm = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ResellerAdministration");
	
	/*
	//Delete SAN
			if(!sanadm.equalsIgnoreCase("No")) {
			scrollDown(APT_NGINObj.NGIN.sanheader);
//		

			WebElement SANGridCheck2= findWebElement("(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
			String SANGrid2= SANGridCheck2.getAttribute("style");
			System.out.println("Customer Name displaying: " +customernamevalue);
			if(!SANGrid2.contains("height: 1px"))
			{
			List<WebElement> AddedSAN2= findWebElements("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]");
			int AddedSAN2_count=AddedSAN2.size();
			for(int i=0;i<AddedSAN2_count;i++)
			{
				 webDriver.findElement(By.xpath("//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
				waitForAjax();

				click(APT_NGINObj.NGIN.SANActionDropdown,  "SAN Action dropdown");
				click(APT_NGINObj.NGIN.deletesan_link, "Delete SAN");
				waitForAjax();
				WebElement DeleteAlertPopup= findWebElement(APT_NGINObj.NGIN.delete_alertpopup);
				if(DeleteAlertPopup.isDisplayed())
				{
					click(APT_NGINObj.NGIN.deletebutton, "Delete");
					waitForAjax();
					waitforPagetobeenable();
					verifysuccessmessage( "San deleted Successfully");
					
					scrollDown( APT_NGINObj.NGIN.customerheader);
					waitForAjax();
				}
				else
				{
					Reporter.log("Delete alert popup is not displayed");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
				}

				waitForAjax();
			}
			}
			else
			{
				Reporter.log("SAN is not added in the grid");
				System.out.println("SAN is not added in the grid");
			}
			}
			
			//Delete Customer
			if(!customadm.equalsIgnoreCase("No")) {
			scrollDown(APT_NGINObj.NGIN.resellerheader);
			WebElement CustomerGridCheck= findWebElement("(//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
			String CustomerGrid= CustomerGridCheck.getAttribute("style");
			WebElement AddedCustomer= findWebElement("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]");
			if(!CustomerGrid.contains("height: 1px"))
			{
				 webDriver.findElement(By.xpath("//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'"+customernamevalue+"')]/parent::div//span[contains(@class,'unchecked')]")).click();
				waitForAjax();

				click(APT_NGINObj.NGIN.CustomerpanelActionDropdown);
				waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Action dropdown button");
				Reporter.log("clicked on Action dropdown button");

				click( APT_NGINObj.NGIN.delete, "Customer Delete");
				waitForAjax();
				WebElement DeleteAlertPopup= findWebElement(APT_NGINObj.NGIN.delete_alertpopup);
				if(DeleteAlertPopup.isDisplayed())
				{
					click(APT_NGINObj.NGIN.deletebutton, "Delete");
					waitForAjax();
					waitforPagetobeenable();
					verifysuccessmessage( "Customer successfully deleted.");
				}
				else
				{
					Reporter.log("Delete alert popup is not displayed");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
				}

				waitForAjax();
			}
			}
			
			if(!reselladm.equalsIgnoreCase("No")) {
			//Delete Reseller
				scrollDown(APT_NGINObj.NGIN.managementoptions_header);
			waitForAjax();
			WebElement ResellerGridCheck= findWebElement("(//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]");
			String ResellerGrid= ResellerGridCheck.getAttribute("style");

			WebElement AddedReseller= findWebElement("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + ResellerName + "')]/parent::div//span[contains(@class,'unchecked')]");

			if(!ResellerGrid.contains("height: 1px"))
			{
				 webDriver.findElement(By.xpath("//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[contains(text(),'" + ResellerName + "')]/parent::div//span[contains(@class,'unchecked')]")).click();
				Thread.sleep(1000);
				click(APT_NGINObj.NGIN.ResellerActionDropdown, "Action dropdown");

				click(  APT_NGINObj.NGIN.delete, "Delete");
				waitForAjax();
				WebElement DeleteAlertPopup= findWebElement(APT_NGINObj.NGIN.delete_alertpopup);
				if(DeleteAlertPopup.isDisplayed())
				{
					click(APT_NGINObj.NGIN.deletebutton, "Delete");
					waitForAjax();
					waitforPagetobeenable();
					verifysuccessmessage( "Reseller successfully deleted.");
				}
				else
				{
					Reporter.log("Delete alert popup is not displayed");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
				}
				waitForAjax();
			}
			} */
			
			//Delete Service
			scrollDown(APT_NGINObj.NGIN.orderpanelheader);
			Thread.sleep(1000);
			click(APT_NGINObj.NGIN.serviceactiondropdown, "Action dropdown");
			click(APT_NGINObj.NGIN.delete,  "Service Delete");
			waitForAjax();
			WebElement DeleteAlertPopup= findWebElement(APT_NGINObj.NGIN.delete_alertpopup);
			if(DeleteAlertPopup.isDisplayed())
			{
				click(APT_NGINObj.NGIN.deletebutton, "Delete");
				waitForAjax();
			}
			else
			{
				Reporter.log("Delete alert popup is not displayed");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : Delete alert popup is not displayed");
			}
			
	
	
	
	
}
	


	
	
	
	
	
	

}
