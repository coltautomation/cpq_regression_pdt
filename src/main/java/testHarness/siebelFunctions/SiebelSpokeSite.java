package testHarness.siebelFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.cpqObjects.CPQQuoteApprovalsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelHubSiteObj;
import pageObjects.siebelObjects.SiebelManualValidationObj;
import pageObjects.siebelObjects.SiebelModeObj;
import pageObjects.siebelObjects.SiebelSpokeSiteObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;


public class SiebelSpokeSite extends SeleniumUtils {
	
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();
	SiebelMode SiebelMod = new SiebelMode();
	
public void spokeSiteCustomize(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
{
	//Thread.sleep(50000);
	String Auto_MitigationValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Auto_Mitigation");
	String ResilienceOptionValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Alerting_Notification_Email_Address");
	String ServiceBandwidthValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Bandwidth1");
	String OSSFlagValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OSS_Platform_Flag");
	String Aend_Street_NameValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Street_Name");
	String Aend_CountryValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Country");
	String Aend_City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_City/Town");
	String Aend_Postal_CodeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Postal_Code");
	String Aend_PremisesValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_Premises");
	String Service_Party_NameValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Party_Name");
	String Site_LastNameValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_LastName");
	String Access_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
	String Access_TechnologyValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Technology");
	String Building_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Type");
	String Customer_Site_Pop_StatusValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Site_Pop_Status");
	String Cabinet_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_Type");
	String Presentation_InterfaceValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
	String Connector_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Connector_Type");
	String Fibre_TypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Fibre_Type");
	String Install_TimeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Install_Time");
	String OffnetColumn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Offnet");
	String Third_Party_Connection_Reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"3rd_Party_Connection_Reference");
	String IP_RangeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP_Range");
	
	

	Reusable.waitForSiebelLoader();
   // Reusable.BEndDropdownSelection("Connector Type","RJ-45");
    
	waitForElementToAppear(SiebelHubSiteObj.Customize.serviceBandwidthDropdownAccess, 10);
	click(SiebelHubSiteObj.Customize.serviceBandwidthDropdownAccess,"Service Bandwidth drop down");
	verifyExists(SiebelHubSiteObj.Customize.serviceBandwidthSelectAccess.replace("value", ServiceBandwidthValue),"Service Bandwidth Value");
	click(SiebelHubSiteObj.Customize.serviceBandwidthSelectAccess.replace("value", ServiceBandwidthValue),"Service Bandwidth Value");
	Reusable.waitForSiebelSpinnerToDisappear();
	verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Coverage"),"Coverage");
	click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Coverage"),"Coverage");
	verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Auto_MitigationValue),"Coverage value");
	click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Auto_MitigationValue),"Coverage value");
	Reusable.waitForSiebelSpinnerToDisappear();
	verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Resilience Option"),"Resilience Option");
	click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Resilience Option"),"Resilience Option");
	verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", ResilienceOptionValue),"Resilience Option value");
	click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", ResilienceOptionValue),"Resilience Option value");
	Reusable.waitForSiebelSpinnerToDisappear();
//	Reusable.ClickHereSave();
	Reusable.savePage();
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Hard Modify Flag"),"Hard Modify Flag drop down");
	click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "Hard Modify Flag"),"Hard Modify Flag drop down");
	verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", "N"),"Hard Modify Flag value");
	click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", "N"),"Hard Modify Flag value");
	
	verifyExists(SiebelHubSiteObj.Customize.textInput.replace("Value", "OSS Platform Flag"),"OSS Platform Flag");
	sendKeys(SiebelHubSiteObj.Customize.textInput.replace("Value", "OSS Platform Flag"), OSSFlagValue, "OSS Flag Value");
	waitForElementToAppear(SiebelHubSiteObj.Customize.clickheretoSaveAccess, 10);
	click(SiebelHubSiteObj.Customize.clickheretoSaveAccess," Click Here to save Access link");
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "HUB Service Id"),"HUB Service Id");
	click(SiebelHubSiteObj.Customize.clickDropdown.replace("Value", "HUB Service Id"),"HUB Service Id");
	verifyExists(SiebelSpokeSiteObj.Spoke.HubTableIDSelect,"Hub Table ID Select");
	click(SiebelSpokeSiteObj.Spoke.HubTableIDSelect,"Hub Table ID Select");
	verifyExists(SiebelSpokeSiteObj.Spoke.HubTableOK,"Hub Table OK");
	click(SiebelSpokeSiteObj.Spoke.HubTableOK,"Hub Table OK");
	Reusable.waitForSiebelLoader();
	/*
	verifyExists(SiebelSpokeSiteObj.Spoke.SearchAddressSiteB,"Search Address site B");
	javaScriptclick(SiebelSpokeSiteObj.Spoke.SearchAddressSiteB);
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelHubSiteObj.Customize.StreetNamerfs, "Street Name Value");
	sendKeys(SiebelHubSiteObj.Customize.StreetNamerfs, Aend_Street_NameValue, "Street Name Value");
	verifyExists(SiebelHubSiteObj.Customize.Country,"County");
	click(SiebelHubSiteObj.Customize.Country,"County");
	verifyExists(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value", Aend_CountryValue),"Country Value");
	click(SiebelHubSiteObj.Customize.SiteABSelection.replace("Value", Aend_CountryValue),"Country Value");
	
	verifyExists(SiebelHubSiteObj.Customize.City,"City");
	sendKeys(SiebelHubSiteObj.Customize.City, Aend_City, "City");
	verifyExists(SiebelHubSiteObj.Customize.PostalCode,"Postal Code");
	sendKeys(SiebelHubSiteObj.Customize.PostalCode, Aend_Postal_CodeValue, "Postal Code");
	verifyExists(SiebelHubSiteObj.Customize.Premises,"Premises");
	sendKeys(SiebelHubSiteObj.Customize.Premises, Aend_PremisesValue, "Premises Value");
	verifyExists(SiebelHubSiteObj.Customize.Search,"Search");
	click(SiebelHubSiteObj.Customize.Search,"Search");
	Reusable.waitForSiebelSpinnerToDisappear();
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
	click(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
	verifyExists(SiebelHubSiteObj.Customize.PickAddress,"Pick Address");
	click(SiebelHubSiteObj.Customize.PickAddress,"Pick Address");
	Reusable.waitForSiebelSpinnerToDisappear();
	verifyExists(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
	click(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelHubSiteObj.Customize.PickBuilding,"Pick Building");
	click(SiebelHubSiteObj.Customize.PickBuilding,"Pick Building");
	Reusable.waitForSiebelSpinnerToDisappear();
	verifyExists(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
	click(SiebelHubSiteObj.Customize.SearchAddressRowSelection,"Search Address Row Selection");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelHubSiteObj.Customize.PickSite,"Pick Site");
	click(SiebelHubSiteObj.Customize.PickSite,"Pick Site");
	Reusable.waitForSiebelSpinnerToDisappear();
	Reusable.waitForSiebelLoader();
	*/
	SiebelMod.addSiteBXNGDetails(testDataFile,sheetName,scriptNo,dataSetNo);
	Reusable.waitForSiebelLoader();
	
	verifyExists(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoB,"Show full info A");
	click(SiebelModeObj.WaveLineMidleApplet.ShowFullInfoB,"Click on Show full info A");
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"TechnicalContact");
	click(SiebelModeObj.WaveLineMidleApplet.TechnicalContact,"Click on TechnicalContact");
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelModeObj.WaveLineMidleApplet.OkButton,"OkButton");
	click(SiebelModeObj.WaveLineMidleApplet.OkButton,"Click on OkButton");
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelModeObj.WaveLineMidleApplet.CrossButton,"CrossButton");
	click(SiebelModeObj.WaveLineMidleApplet.CrossButton,"Click on CrossButton");
	Reusable.waitForSiebelLoader();
//	Save();
	Reusable.waitForSiebelSpinnerToDisappear();
	
	verifyExists(SiebelHubSiteObj.Customize.ServicePartySearchAccess,"Service Party Search Access");
//	scrollIntoView(webDriver.findElement(By.xpath(SiebelHubSiteObj.Customize.ServicePartySearchAccess)));
	ScrollIntoViewByString(SiebelHubSiteObj.Customize.ServicePartySearchAccess);
	javaScriptclick(SiebelHubSiteObj.Customize.ServicePartySearchAccess,"Service Party Search Access");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelHubSiteObj.Customize.ServicePartyDropdownAccess,"Service Party Drop down Access");
	click(SiebelHubSiteObj.Customize.ServicePartyDropdownAccess,"Service Party Drop down Access");
	verifyExists(SiebelHubSiteObj.Customize.PartyNameAccess,"Party NameA ccess");
	click(SiebelHubSiteObj.Customize.PartyNameAccess,"Party NameA ccess");

	
	sendKeys(SiebelHubSiteObj.Customize.InputPartyNameAccess, Service_Party_NameValue, "Service Party Name Value");
	verifyExists(SiebelHubSiteObj.Customize.PartyNameSearchAccess,"Party Name Search Access");
	click(SiebelHubSiteObj.Customize.PartyNameSearchAccess,"Party Name Search Access");
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelHubSiteObj.Customize.PartyNameSubmitAccess,"Party Name submit Access");
	javaScriptclick(SiebelHubSiteObj.Customize.PartyNameSubmitAccess,"Party Name submit Access");
	Reusable.waitForSiebelLoader();
	waitForElementToAppear(SiebelHubSiteObj.Customize.SiteContactSearchAccess, 10);
	javaScriptclick(SiebelHubSiteObj.Customize.SiteContactSearchAccess,"Site Contact Search Access");
/*	verifyExists(SiebelHubSiteObj.Customize.SiteContactDropdownAccess,"Site Contact Drop down Access");
	click(SiebelHubSiteObj.Customize.SiteContactDropdownAccess,"Site Contact Drop down Access");
	
	verifyExists(SiebelHubSiteObj.Customize.SiteContactOCN,"Site Contact OCN");
	click(SiebelHubSiteObj.Customize.SiteContactOCN,"Site Contact OCN");
	
	verifyExists(SiebelHubSiteObj.Customize.InputSiteNameAccess,"OCN input");
	sendKeys(SiebelHubSiteObj.Customize.InputSiteNameAccess, Site_LastNameValue, "OCN input");

	verifyExists(SiebelHubSiteObj.Customize.LastNameSiteSearchAccess,"Search");
	click(SiebelHubSiteObj.Customize.LastNameSiteSearchAccess,"Search");
	*/
	Reusable.waitForSiebelLoader();
	verifyExists(SiebelHubSiteObj.Customize.SelectContact,"Select Contact");
	click(SiebelHubSiteObj.Customize.SelectContact,"Select Contact");
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelHubSiteObj.Customize.LastNameSiteSubmitAccess,"Submit");
	click(SiebelHubSiteObj.Customize.LastNameSiteSubmitAccess,"Submit");
	
	Reusable.waitForSiebelSpinnerToDisappear();
	Reusable.waitForSiebelLoader();

	verifyExists(SiebelHubSiteObj.Customize.IpGurdianSave,"Ip Gurdian Save");
	javaScriptclick(SiebelHubSiteObj.Customize.IpGurdianSave,"Ip Gurdian Save");
	Reusable.waitForSiebelLoader();
	
	if(OffnetColumn.equalsIgnoreCase("offnet")) {
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Type"),"Access Type");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Type"),"Access Type");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TypeValue),"Access Type value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TypeValue),"Access Type value");
		
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Technology"),"Access Technology");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Technology"),"Access Technology");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TechnologyValue),"Access Technology Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TechnologyValue),"Access Technology Value");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Third Party Access Provider"),"Third Party Access Provider");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Third Party Access Provider"),"Third Party Access Provider");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Third_Party_Connection_Reference),"Third Party Connection Reference");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Third_Party_Connection_Reference),"Third Party Connection Reference");
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Third Party SLA Tier"),"Third Party SLA Tier");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Third Party SLA Tier"),"Third Party SLA Tier");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", IP_RangeValue),"IP Range Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", IP_RangeValue),"IP Range Value");
		Reusable.waitForSiebelLoader();
		
	}
	else {
		Reusable.waitForSiebelLoader();
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Type"),"Access Type");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Type"),"Access Type");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TypeValue),"Access Type value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TypeValue),"Access Type value");

		if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
			verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				Reusable.waitForSiebelLoader();
		}
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Technology"),"Access Technology");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Access Technology"),"Access Technology");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TechnologyValue),"Access Technology Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Access_TechnologyValue),"Access Technology Value");
		Reusable.waitForSiebelSpinnerToDisappear();
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Building Type"),"Building Type");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Building Type"),"Building Type");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Building_TypeValue),"Building Type Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Building_TypeValue),"Building Type Value");
		
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Customer Site Pop Status"),"Customer Site Pop Status");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Customer Site Pop Status"),"Customer Site Pop Status");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Customer_Site_Pop_StatusValue),"Customer Site Pop Status value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Customer_Site_Pop_StatusValue),"Customer Site Pop Status value");
	}
		clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "3rd Party Connection Reference"));
		sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "3rd Party Connection Reference"), "As12", "3rd Party Connection Reference");
		
		clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "BCP Reference"));
		sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "BCP Reference"), "As12", "BCP Reference");
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Cabinet Type"),"Cabinet Type");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Cabinet Type"),"Cabinet Type");
		verifyExists(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Cabinet_TypeValue),"Cabinet Type value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Cabinet_TypeValue),"Cabinet Type value");
		Reusable.waitForSiebelLoader();
		clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Cabinet ID"));
		sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Cabinet ID"), "As12", "Cabinet ID");
		Reusable.SendkeaboardKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Cabinet ID"),Keys.ENTER);
		
		
		clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Shelf ID"));
		sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Shelf ID"), "12", "Shelf ID");
		Reusable.SendkeaboardKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Shelf ID"),Keys.ENTER);
		Reusable.waitForSiebelLoader();
		clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Slot ID"));
		sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Slot ID"), "12", "Slot ID");
		Reusable.SendkeaboardKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Slot ID"),Keys.ENTER);
		clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Physical Port ID"));
		sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Physical Port ID"), "12", "Physical Port ID");
		Reusable.SendkeaboardKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "Physical Port ID"),Keys.ENTER);
		Reusable.waitForSiebelLoader();
		
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Connector Type"),"Connector Type");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Connector Type"),"Connector Type");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Connector_TypeValue),"Connector Type Value");
		Reusable.waitForSiebelSpinnerToDisappear();
		
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Presentation Interface"),"Presentation Interface");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Presentation Interface"),"Presentation Interface");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Presentation_InterfaceValue),"Presentation Interface value");
		

		if (isElementPresent(By.xpath("//*[contains(@id,'colt-formalerts-popup')]"))) {
			verifyExists(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				click(SiebelHubSiteObj.Customize.BandwidthErrorPOPSetToBtn,"Set to - button");
				Reusable.waitForSiebelLoader();
		}
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();
		
		
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Fibre Type"),"Fibre Type");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Fibre Type"),"Fibre Type");
		//click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Fibre_TypeValue),"Fibre Type Value");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Fibre_TypeValue),"Fibre Type Value");
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Port Role"),"Port Role");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Port Role"),"Port Role");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value","Physical Port"),"Physical Port Value");
	

		clearTextBox(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "VLAN Tag ID"));
		sendKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "VLAN Tag ID"), "12", "VLAN Tag ID");
		Reusable.SendkeaboardKeys(SiebelSpokeSiteObj.Spoke.TextInputB.replace("Value", "VLAN Tag ID"),Keys.ENTER);
		verifyExists(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Install Time"),"Install Time");
		click(SiebelSpokeSiteObj.Spoke.ClickDropdownB.replace("Value", "Install Time"),"Install Time");
		click(SiebelHubSiteObj.Customize.selectValueDropdown.replace("Value", Install_TimeValue),"Install Time Value");
		Reusable.waitForSiebelLoader();
	//	Reusable.ClickHereSave();
		Reusable.savePage();
		Reusable.waitForSiebelLoader();
	}

	

}
