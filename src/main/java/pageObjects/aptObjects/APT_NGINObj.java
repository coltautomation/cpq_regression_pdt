package pageObjects.aptObjects;

public class APT_NGINObj {

	public static class NGIN {
		public static final String servicetype = "@xpath=//div[label[text()='Service Type']]//input"; // modify

		public static final String ResellerGridCheck = "@xpath=(//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]"; // modify
		// (//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1])
		public static final String CustomerGridCheck = "@xpath=(//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]"; // modify
		public static final String SANGridCheck = "@xpath=(//div[text()='SAN/FRC']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div)[1]"; // modify
		public static final String priceannoriginvalue = "@xpath=//div[label[text()='Price Ann Origin']]//input"; // modify
		

		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";
		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";
		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";
		public static final String chooseCustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String Next_Button = "@xpath=//button//Span[text()='Next']";

		public static final String mcslink = "@xpath=//a[contains(text(),'Manage Customer's Service')]";
		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";
		public static final String createorderlink = "@xpath=//li[text()='Create Order/Service']";

		// public static final String !-- Create Customer Page --="@xpath=
		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String nametextfield = "@xpath=//input[@id='name']";
		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";
		public static final String country = "@xpath=//div[label[contains(text(),'Country')]]//input";
		public static final String ocntextfield = "@xpath=//input[@id='ocn']";
		public static final String referencetextfield = "@xpath=//input[@id='reference']";
		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";
		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";
		public static final String emailtextfield = "@xpath=//input[@id='email']";
		public static final String phonetextfield = "@xpath=//input[@id='phone']";
		public static final String faxtextfield = "@xpath=//input[@id='fax']";
		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";
		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";
		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String resetbutton = "@xpath=//span[text()='Reset']";
		public static final String clearbutton = "@xpath=//span[text()='Clear']";
		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";
		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// public static final String !-- Verify warning messages --="@xpath=
		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";
		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";
		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";
		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";
		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";
		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";
		public static final String sidwarngmsg = "@xpath=//input[@id='serviceIdentification']/parent::div//div";

		// public static final String !-- Verify Customer details panel
		// --="@xpath=
		public static final String Name_Text = "@xpath=(//div//label[@for='name'])[1]";
		public static final String Name_Value = "@xpath=//label[text()='Legal Customer Name']/parent::div/parent::div//div[2]";
		public static final String MainDomain_Text = "@xpath=(//div//label[@for='name'])[2]";
		public static final String MainDomain_Value = "@xpath=//label[text()='Main Domain']/parent::div/parent::div//div[2]";
		public static final String Country_Text = "@xpath=(//div//label[@for='name'])[3]";
		public static final String Country_Value = "@xpath=//label[text()='Country']/parent::div/parent::div//div[2]";
		public static final String OCN_Text = "@xpath=(//div//label[@for='name'])[4]";
		public static final String OCN_Value = "@xpath=//label[text()='OCN']/parent::div/parent::div//div[2]";
		public static final String Reference_Text = "@xpath=(//div//label[@for='name'])[5]";
		public static final String Reference_Value = "@xpath=//label[text()='Reference']/parent::div/parent::div//div[2]";
		public static final String Type_Text = "@xpath=(//div//label[@for='name'])[6]";
		public static final String Type_Value = "@xpath=//label[text()='Type']/parent::div/parent::div//div[2]";
		public static final String TechnicalContactName_Text = "@xpath=(//div//label[@for='name'])[7]";
		public static final String TechnicalContactName_Value = "@xpath=//label[text()='Technical Contact Name']/parent::div/parent::div//div[2]";
		public static final String Email_Text = "@xpath=(//div//label[@for='name'])[8]";
		public static final String Email_Value = "@xpath=//label[text()='Email']/parent::div/parent::div//div[2]";
		public static final String Phone_Text = "@xpath=(//div//label[@for='name'])[9]";
		public static final String Phone_Value = "@xpath=//label[text()='Phone']/parent::div/parent::div//div[2]";
		public static final String Fax_Text = "@xpath=(//div//label[@for='name'])[10]";
		public static final String Fax_Value = "@xpath=//label[text()='Fax']/parent::div/parent::div//div[2]";

		// public static final String !-- create order/service page --="@xpath=
		// public static final String !-- public static final String
		// nametextfield="@xpath=//input[@id='customerSearch']";
		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";
		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";
		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";
		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";
		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";
		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";
		// public static final String
		// country="@xpath=//div[div[label[text()='Country']]]/div[2]";
		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";
		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";
		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";
		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";
		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";
		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";
		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";
		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";
		public static final String useractionbutton = "@xpath=//button[@id='dropdown-basic-button']";
		public static final String adduserbutton = "@xpath=//a[contains(text(),'Add')]";

		// public static final String !-- Create Order/service panel in view
		// customer page --="@xpath=
		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";
		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";
		public static final String selectorderswitch = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/following-sibling::input/ancestor::div[@class='form-group']/parent::div/following-sibling::div//div[@class='react-switch-bg']";
		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";
		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";
		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";
		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";
		public static final String newordertextfield = "@xpath=//input[@id='orderName']";
		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";
		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String order_contractnumber_warngmsg = "@xpath=//label[contains(text(),'Order/Contract Number(Parent SID)')]/following-sibling::span[3]";
		public static final String servicetype_warngmsg = "@xpath=//div[label[contains(text(),'Service Type')]]/following-sibling::span";

		public static final String servicetypetextfield = "@xpath=//div[div[label[text()='Service Type']]]//input";
		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";
		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";
		public static final String changeorder_cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		// public static final String !-- Service Creation Form --="@xpath=
		public static final String createorderservice_header = "@xpath=//div//div[text()='Create Order / Service']";
		public static final String serviceidentificationtextfield = "@xpath=//input[@id='serviceIdentification']";
		public static final String servicetypevalue = "@xpath=//div[label[text()='Service Type']]/div";
		// public static final String
		// emailtextfield="@xpath=//input[@id='email']";
		public static final String emailtextfieldvalue = "@xpath=//div[label[text()='Email']]//input";
		public static final String phonecontacttextfield = "@xpath=//input[@id='phoneContact']";
		public static final String phonecontacttextfieldvalue = "@xpath=//div[label[text()='Phone Contact']]//input";
		public static final String remarktextarea = "@xpath=//textarea[contains(@name,'remark')]";
		public static final String remarktextareavalue = "@xpath=//div[label[text()='Remark']]//textarea";
		public static final String performancereportingcheckbox = "@xpath=//input[@id='performanceReporting']";
		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";
		public static final String servicecreationmessage = "@xpath=//div[@role='alert']//span";
		public static final String customeradministrationcheckbox = "@xpath=//input[@id='custAdm']";
		public static final String sanadministrationcheckbox = "@xpath=//input[@id='sanAdm']";
		public static final String reselleradministrationcheckbox = "@xpath=//input[@id='resellerAdm']";

		public static final String servicetype_value = "@xpath=//label[text()='Service Type']/following-sibling::div";
		public static final String emailarrow = "@xpath=//label[text()='Email']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";
		public static final String phonearrow = "@xpath=//label[text()='Phone Contact']/parent::div/parent::div/following-sibling::div//button//span[text()='>>']";

		// public static final String !-- public static final String
		// cancelbutton="@xpath=//button[@type='submit']//span[text()='Cancel']";

		// public static final String !-- Search order --="@xpath=
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";

		// public static final String !-- order panel - view service page
		// --="@xpath=
		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";
		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";
		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";
		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";
		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";
		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";
		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";
		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";
		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";
		public static final String orderpanelheader = "@xpath=(//div[contains(text(),'Order')])[1]";
		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";
		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";
		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";
		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";
		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";
		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";
		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";

		// public static final String !-- service panel --="@xpath=
		public static final String servicepanel_serviceidentificationvalue = "@xpath=//div[label[contains(text(),'Service Identification')]]/div";
		public static final String servicepanel_servicetypevalue = "@xpath=//div[label[contains(text(),'Service Type')]]/div";
		public static final String servicepanel_remarksvalue = "@xpath=//div[label[contains(text(),'Remark')]]/div";
		public static final String servicepanel_header = "@xpath=(//div[contains(text(),'Service')])[1]";
		public static final String serviceactiondropdown = "@xpath=//div[div[contains(text(),'Service')]]//button";
		public static final String serviceupdate_successmsg = "@xpath=//div[@role='alert']//span[text()='Service  successfully updated']";
		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String synchronizelink_servicepanel = "@xpath=//a[text()='Synchronize']";
		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";
		public static final String status_ordername = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String status_servicename = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String status_servicetype = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String status_servicedetails = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String status_currentstatus = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='currentStatus']";
		public static final String status_modificationtime = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='mTime']";
		public static final String statuslink = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell']//a[text()='Status']";
		public static final String sync_ordername = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";
		public static final String sync_servicename = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";
		public static final String sync_servicetype = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";
		public static final String sync_servicedetails = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";
		public static final String sync_status = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='syncStatus']";
		public static final String synchronizelink = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell']//a[text()='Synchronize']";
		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";
		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";
		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";
		public static final String changestatus_dropdown = "@xpath=//label[text()='Change Status']/parent::div//input";
		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";
		public static final String servicestatushistory = "@xpath=//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String bulkinterfaceheader = "@xpath=//div[text()='Bulk Interface']";
		public static final String bulkjob_choosefilebutton = "@xpath=//label[text()='Submit Bulk Job']/parent::div/parent::div//input";
		public static final String bulkjobsubmit = "@xpath=//span[text()='Submit']";
		public static final String bulkjobcancel = "@xpath=(//span[text()='Cancel'])[1]";
		public static final String bulkinterfacelink = "@xpath=//a[text()='Bulk Interface']";
		public static final String bulkinterface_archivelink = "@xpath=//a[text()='Archive']";
		public static final String bulkinterface_refreshlink = "@xpath=//a[text()='Refresh']";
		public static final String bulkinterfacepage_cancel = "@xpath=(//span[text()='Cancel'])[2]";
		public static final String successmsg = "@xpath=//div[@role='alert']//span";
		public static final String bulkinterface_actiondropdown = "@xpath=//div[contains(text(),'Bulk Interface')]/following-sibling::div/div//button[text()='Action']";

		public static final String breadcrumb = "@xpath=//ol[@class='breadcrumb']//a[contains(text(),'value')]";

		// public static final String !-- Management Options --="@xpath=
		public static final String managementoptions_customeradminvalue = "@xpath=//div[label[text()='Customer Administration']]/div";
		public static final String managementoptions_sanadminvalue = "@xpath=//div[label[text()='SAN Administration']]/div";
		public static final String managementoptions_reselladminvalue = "@xpath=//div[label[text()='Reseller Administration']]/div";

		// public static final String !-- Users panel in view service page
		// --="@xpath=

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";
		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";
		public static final String EmailColumn = "@xpath=//div[@col-id='email']";
		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";
		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";
		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";
		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";
		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";
		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		// public static final String !-- New User creation in view service page
		// --="@xpath=

		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";
		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";
		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";

		// public static final String !-- Warning messages --="@xpath=
		public static final String warningmsg_username = "@xpath=//label[text()='User Name']/parent::div//div";
		public static final String warningmsg_firstname = "@xpath=//label[text()='First Name']/parent::div//div";
		public static final String warningmsg_surname = "@xpath=//label[text()='Surname']/parent::div//div";
		public static final String warningmsg_postaladdress = "@xpath=//label[text()='Postal Address']/parent::div/following-sibling::div";
		public static final String warningmsg_useremail = "@xpath=//label[text()='Email']/parent::div//div";
		public static final String warningmsg_userphone = "@xpath=//label[text()='Phone']/parent::div//div";
		public static final String warningmsg_userpassword = "@xpath=//label[text()='Password']/parent::div//div";
		public static final String UserName = "@xpath=(//div[@class='position-relative form-group'])[1]//input[@id='userName']";
		public static final String FirstName = "@xpath=(//div[@class='position-relative form-group'])[2]//input[@id='firstName']";
		public static final String SurName = "@xpath=(//div[@class='position-relative form-group'])[3]//input[@id='surname']";
		public static final String PostalAddress = "@xpath=(//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";
		public static final String Email = "@xpath=(//div[@class='position-relative form-group'])[5]//input[@id='email']";
		public static final String Phone = "@xpath=(//div[@class='position-relative form-group'])[6]//input[@id='phone']";
		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";
		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";
		public static final String OkButton = "@xpath=//button[@type='submit']//span[text()='Ok']";
		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";
		public static final String deletebutton = "@xpath=//button[text()=' Delete']";
		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";
		public static final String userspanel_header = "@xpath=//div[text()='Users']";
		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";
		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";
		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";
		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";
		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";
		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";
		public static final String OK_button = "@xpath=//button[@type='submit']//span[text()='OK']";
		public static final String userdelete = "@xpath=//button[text()='Delete']";

		// public static final String !-- Select Dropdown --="@xpath=
		public static final String legalcustomerName_labelName = "@xpath=//label[text()='Legal Customer Name']";

		public static final String IPGuardianAccountGroup = "@xpath=//input[@id='ipGuardianAccountGrp']";
		public static final String IPGuardianAccountGroup_Text = "@xpath=//label[contains(text(),'IPGuardian Account Group')]";
		public static final String IPGuardianAccountGroup_viewpage = "@xpath=//div[div[label//b[contains(text(),'IPGuardian Account Group')]]]/div[2]";
		public static final String ColtOnlineUser = "@xpath=//input[@id='onlineUser']";
		public static final String ColtOnlineUser_Text = "@xpath=//label[contains(text(),'Colt Online User')]";
		public static final String coltonlineuser_viewpage = "@xpath=//div[div[label//b[contains(text(),'Colt Online User')]]]/div[2]";
		public static final String Password_Text = "@xpath=//label[contains(text(),'Password')]";
		public static final String Password_Textfield = "@xpath=//input[@id='password']";
		public static final String GeneratePasswordLink = "@xpath=//span[text()='Generate Password']";
		public static final String GeneratePasswordLink_Text = "@xpath=//span[@class='badge-success badge badge-secondary']";

		public static final String roleDropdown_available = "@xpath=//select[@id='availableRoles']//option";
		public static final String roleDropdown_addButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='>>']";
		public static final String roleDropdown_removeButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='value']";
		public static final String roleDropdown_selectedValues = "@xpath=//select[@id='selectedRoles']//option";

		public static final String hideRouterToolIPv4_Huawei_available = "@xpath=//select[@id='availableIPV4CommandHuawei']//option";
		public static final String hideRouterToolIPv4__Huawei_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='>>']";
		public static final String hideRouterToolIPv4_Huawei_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='value']";
		public static final String hideRouterToolIpv4_Huawei_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4_Cisco_Available = "@xpath=//select[@id='availableIPV4CommandCisco']//option";
		public static final String hideRouterToolIPv4_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";
		public static final String hideRouterToolIPv4_Cisco_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='value']";
		public static final String hideRouterToolIpv4_Cisco_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandCisco']//option";

		public static final String HideService_Available = "@xpath=//select[@id='availableHideService']//option";
		public static final String HideService_addButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='>>']";
		public static final String HideService_removeButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='value']";
		public static final String HideServicesDropdown_selectedValues = "@xpath=//select[@id='selectedHideService']//option";

		public static final String HideSiteOrder_Available = "@xpath=//select[@id='availableHideSiteOrder']//option";
		public static final String hideSiteOrder_addbutton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='>>']";
		public static final String hideSiteOrder_removeButton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='value']";
		public static final String HideSiteOrderDropdown_selectedValues = "@xpath=//select[@id='selectedHideSiteOrder']//option";

		public static final String HideRouterToolIPv6_Cisco_Available = "@xpath=//select[@id='availableIPV6Command']//option";
		public static final String hideRouterToolIPv6_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String viewUser_HiddenRouterToolIPv4Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv4Huawei = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div";
		public static final String viewUser_HiddenRouterToolCommandIPv6Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]]//following-sibling::div";

		public static final String selectValueUnderUserPanel_ViewSericePage = "@xpath=//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		// public static final String !-- Management Options panel in view
		// service page --="@xpath=
		public static final String managementoptions_header = "@xpath=//div[text()='Management Options']";
		public static final String customeradmin_text = "@xpath=//label[text()='Customer Administration']";
		public static final String customeradmin_value = "@xpath=//div[label[text()='Customer Administration']]/div";
		public static final String sanadmin_text = "@xpath=//label[text()='SAN Administration']";
		public static final String sanadmin_value = "@xpath=//div[label[text()='SAN Administration']]/div";
		public static final String reselleradmin_text = "@xpath=//label[text()='Reseller Administration']";
		public static final String reselleradmin_value = "@xpath=//div[label[text()='Reseller Administration']]/div";

		// public static final String !-- Reseller panel in view service page
		// --="@xpath=
		public static final String resellerheader = "@xpath=//div[text()='Reseller']";
		public static final String reseller_customername_column = "@xpath=//div[@col-id='name']//span[@role='columnheader']";
		public static final String statuscolumn = "@xpath=//div[@col-id='status']//span[@role='columnheader']";
		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";
		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";
		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";
		public static final String manageresellerheader_viewpage = "@xpath=//div[@class='heading-green-row row']//p";
		public static final String manageresellerheader = "@xpath=(//div[@class='heading-green-row row']//span)[1]";
		public static final String resellername = "@xpath=//div[@class='position-relative form-group']//label[text()='Reseller Name']/following-sibling::input";
		public static final String reseller_email = "@xpath=//div[@class='position-relative form-group']//label[text()='Email']/following-sibling::input";
		public static final String reseller_city = "@xpath=//div[@class='position-relative form-group']//label[text()='City']/following-sibling::input";
		public static final String reseller_streetname = "@xpath=//div[@class='position-relative form-group']//label[text()='Street Name']/following-sibling::input";
		public static final String reseller_streetno = "@xpath=//div[@class='position-relative form-group']//label[text()='Street Number']/following-sibling::input";
		public static final String reseller_pobox = "@xpath=//div[@class='position-relative form-group']//label[text()='PO Box']/following-sibling::input";
		public static final String reseller_zipcode = "@xpath=//div[@class='position-relative form-group']//label[text()='ZIP Code']/following-sibling::input";
		public static final String reseller_phone = "@xpath=//div[@class='position-relative form-group']//label[text()='Phone']/following-sibling::input";
		public static final String reseller_fax = "@xpath=//div[@class='position-relative form-group']//label[text()='Fax']/following-sibling::input";
		public static final String reseller_webauthorised = "@xpath=//input[@id='webAccessAuthorized']";
		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";
		public static final String ResellerActionDropdown = "@xpath=//div[contains(text(),'Reseller')]/following-sibling::div/div//button[text()='Action']";
		public static final String Addedreseller_columnvalue = "@xpath=//div[text()='Reseller']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@col-id='name']";
		public static final String editreseller_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String viewpage_actiondropdown = "@xpath=//div[@align='right']//div//button[text()='Action']";
		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";
		public static final String associateresellerlink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Associate Reseller with NGIN Objects']";

		public static final String reselleremail_warngmsg = "@xpath=//label[text()='Email']/parent::div//div";
		public static final String resellercity_warngmsg = "@xpath=//label[text()='City']/parent::div//div";
		public static final String resellerstreetname_warngmsg = "@xpath=//label[text()='Street Name']/parent::div//div";
		public static final String resellerstreetno_warngmsg = "@xpath=//label[text()='Street Number']/parent::div//div";
		public static final String resellerpobox_warngmsg = "@xpath=//label[text()='PO Box']/parent::div//div";
		public static final String resellerzipcode_warngmsg = "@xpath=//label[text()='ZIP Code']/parent::div//div";
		public static final String resellerphone_warngmsg = "@xpath=//label[text()='Phone']/parent::div//div";

		public static final String resellernamevalue = "@xpath=//b[text()='Reseller Name']/parent::label/parent::div/following-sibling::div";
		public static final String reselleremailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";
		public static final String resellercityvalue = "@xpath=//b[text()='City']/parent::label/parent::div/following-sibling::div";
		public static final String resellerstreetnamevalue = "@xpath=//b[text()='Street Name']/parent::label/parent::div/following-sibling::div";
		public static final String resellerstreetnovalue = "@xpath=//b[text()='Street Number']/parent::label/parent::div/following-sibling::div";
		public static final String resellerpoboxvalue = "@xpath=//b[text()='PO Box']/parent::label/parent::div/following-sibling::div";
		public static final String resellerzipcodevalue = "@xpath=//b[text()='ZIP Code']/parent::label/parent::div/following-sibling::div";
		public static final String resellerphonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";
		public static final String resellerfaxvalue = "@xpath=//b[text()='Fax']/parent::label/parent::div/following-sibling::div";
		public static final String resellerwebaccessvalue = "@xpath=//b[text()='Web Access Authorized']/parent::label/parent::div/following-sibling::div";

		// public static final String !-- Customer panel in view service page
		// --="@xpath=
		public static final String customerheader = "@xpath=//div[text()='Customer']";
		public static final String customerpanel_customername = "@xpath=(//label[text()='Customer Name']/parent::div/div//input)[1]";
		public static final String managecustomerheader = "@xpath=//div[@class='heading-green-row row']//span[text()='Manage Customer In OSP']";
		public static final String customer_ocn = "@xpath=//div[@class='position-relative form-group']//label[text()='OCN']/following-sibling::input";
		public static final String customer_country = "@xpath=(//label[text()='Country']/parent::div//div//input)[1]";
		public static final String customername = "@xpath=(//label[text()='Customer Name']/parent::div//div//input)[1]";
		public static final String defaultcheckbox = "@xpath=//label[@class='form-label capitalize']//input[@value='defcustprof']";
		public static final String configurecheckbox = "@xpath=//label[@class='form-label capitalize']//input[@value='configcustprof']";
		public static final String CustomerpanelActionDropdown = "@xpath=//div[contains(text(),'Customer')]/following-sibling::div/div//button[text()='Action']";
		public static final String addcustomer_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String customername_columnvalue = "@xpath=//div[text()='Customer']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@col-id='trailCustomerName']";
		public static final String editcustomer_successmsg = "@xpath=(//div[@role='alert']//span)[1]";
		public static final String viewpage_editcustomer = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit Customer']";
		public static final String viewpage_deletecustomer = "@xpath=//div[@class='dropdown-menu show']//a[text()='Delete Customer']";
		public static final String deletealertclose = "@xpath=//div[@class='modal-content']//button//span[text()='�']";
		public static final String customerpanel_customernamevalue = "@xpath=//label[text()='Customer Name']/parent::div/div//span";
		public static final String customerpanel_customernamecolumntext = "@xpath=//div[text()='Customer']/parent::div/following-sibling::div//span[text()='Customer Name']";
		public static final String customercountry_disabled = "@xpath=(//label[text()='Country']/parent::div//div//div)[1]";
		public static final String customername_disabled = "@xpath=(//label[text()='Customer Name']/parent::div//div//div)[1]";

		public static final String customer_countryvalue = "@xpath=//label[text()='Country']/parent::div/following-sibling::div";
		public static final String customer_customernamevalue = "@xpath=//label[text()='Customer Name']/parent::div/following-sibling::div";
		public static final String customername_selectedtext = "@xpath=(//label[text()='Customer Name']/parent::div//div//span)[1]";
		public static final String addcustomer_country_warningmsg = "@xpath=//label[text()='Country']/parent::div/following-sibling::div";
		public static final String addcustomer_customername_warningmsg = "@xpath=//label[text()='Customer Name']/parent::div/following-sibling::div";
		public static final String addcustomer_city_warningmsg = "@xpath=//label[text()='City']/parent::div/div";
		public static final String addcustomer_streetname_warningmsg = "@xpath=//label[text()='Street Name']/parent::div/div";
		public static final String addcustomer_streetno_warningmsg = "@xpath=//label[text()='Street Number']/parent::div/div";
		public static final String addcustomer_pobox_warningmsg = "@xpath=//label[text()='PO Box']/parent::div/div";
		public static final String addcustomer_zipcode_warningmsg = "@xpath=//label[text()='ZIP Code']/parent::div/div";

		// public static final String !-- SAN panel in view service page
		// --="@xpath=
		// public static final String !-- Add SAN --="@xpath=
		public static final String sanheader = "@xpath=//div[text()='SAN/FRC']";
		public static final String SANActionDropdown = "@xpath=//div[contains(text(),'SAN/FRC')]/following-sibling::div/div//button[text()='Action']";
		public static final String san_customername = "@xpath=//div[@col-id='custNginName']//span[@role='columnheader']";
		public static final String frcnumber_column = "@xpath=//div[@col-id='frcNumber']//span[@role='columnheader']";
		public static final String addsan_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add SAN']";
		public static final String addsan_header = "@xpath=//div[@class='heading-green-row row']//div[text()='Add SAN']";
		public static final String addanothersan_header = "@xpath=//div[@class='heading-green-row row']//div[text()='ADD']";
		public static final String san_number = "@xpath=//div[@class='position-relative form-group']//label[text()='SAN(FRC) Number ']/following-sibling::input";
		public static final String serviceprofile = "@xpath=(//label[text()='Service Profile']/parent::div//div//input)[1]";
		public static final String supervisionfield = "@xpath=//div[@class='position-relative form-group']//label[text()='Supervision Mode']/parent::div//span";
		public static final String internationaloutgoingforbidden_checkbox = "@xpath=//label[text()='International Outgoing Calls Forbidden']/parent::div//input[@type='checkbox']";
		public static final String internationalincomingbarring = "@xpath=//label[text()='International Incoming Calls Barring']/parent::div//input[@type='checkbox']";
		public static final String mobilecallsallowed_checkbox = "@xpath=//label[text()='Mobile Calls Allowed']/parent::div//input[@type='checkbox']";
		public static final String noreplytimervalue = "@xpath=//label[text()='No Reply Timer Value']/parent::div//input";
		public static final String maxcallduration = "@xpath=//label[text()='Max Call Duration']/parent::div//input";
		public static final String chargebandname = "@xpath=//label[text()='Charge Band Name']/parent::div//input";
		public static final String payphoneblockingenabled = "@xpath=//label[text()='Pay phone blocking enabled']/parent::div//input[@type='checkbox']";
		public static final String webaccessblocked = "@xpath=//label[text()='webAccessBlocked']/parent::div//input[@type='checkbox']";
		public static final String sanblock = "@xpath=//label[text()='SAN Block']/parent::div//input[@type='checkbox']";
		public static final String focenabled = "@xpath=//label[text()='FOC Enabled']/parent::div//input[@type='checkbox']";
		public static final String predestinationnumber = "@xpath=//label[text()='Pre Destination Number']/parent::div//input";
		public static final String cpsfreeformat = "@xpath=//label[text()='CPS (free format)']/parent::div//input";
		public static final String cpsproductcode = "@xpath=//label[text()='CPS: Product Code ']/parent::div//input";
		public static final String cpsresellerid = "@xpath=//label[text()='CPS: Reseller ID ']/parent::div//input";
		public static final String cpssanencoded = "@xpath=//label[text()='CPS: SAN encoded in modulo80']/parent::div//input";
		public static final String ringtonumber_radiobutton = "@xpath=//label//input[@value='ringToNumber']";
		public static final String ringtonumber_field = "@xpath=//label[text()='Ring to Number']/following-sibling::input";
		public static final String announcementtoplay_radiobutton = "@xpath=//label//input[@value='annouceName']";
		public static final String announcementtoplay_dropdown = "@xpath=//label[text()='Announcement To Play']/parent::div//div//input";
		public static final String announcementtoplay_dropdownvalue = "@xpath=(//label[text()='Announcement To Play']/parent::div//div//span)[1]";
		public static final String complexroute_radiobutton = "@xpath=//label//input[@value='complexRoute']";
		public static final String routingforpayphone_field = "@xpath=//label[text()='Routing for Payphone']/following-sibling::input";
		public static final String routingformobile = "@xpath=//label[text()='Routing for Mobile']/following-sibling::input";
		public static final String defaultrouting = "@xpath=//label[text()='Default Routing']/following-sibling::input";
		public static final String enablelogicalrouting = "@xpath=//label[text()='Enable Logical Routing']/parent::div//input[@type='checkbox']";
		public static final String enablepriceannouncement = "@xpath=//label[text()='Enable Price Announcement']/parent::div//input[@type='checkbox']";
		public static final String managenewcustomer_link = "@xpath=//div[text()='Manage New Customer']";
		public static final String defaultroutebusy = "@xpath=//label[text()='Default Route Busy']/following-sibling::input";
		public static final String noanswer = "@xpath=//label[text()='No Answer']/following-sibling::input";
		public static final String networkcongestion = "@xpath=//label[text()='Network Congestion']/following-sibling::input";
		public static final String customernamedropdown_valuesdisplay = "@xpath=(//label[text()='Customer Name']/parent::div//div[@role='list']//div[@class='sc-ifAKCX oLlzc'])[1]";
		public static final String customernamedropdown_nomatchesfound = "@xpath=(//label[text()='Customer Name']/parent::div//div[@role='list']//div)[1]";
		public static final String viewsan_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='View SAN']";
		public static final String editsan_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit SAN']";
		public static final String deletesan_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Delete SAN']";
		public static final String sanmove_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='San Move']";
		public static final String bulkmove_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Bulk Move']";
		public static final String portin_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Port In SAN']";
		public static final String portout_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Port Out SAN']";
		public static final String manageaddnlfrc_link = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage Addnl FRC']";
		public static final String portin_dialog = "@xpath=//div[@role='dialog']//div[@class='modal-content']";
		public static final String porttonumber = "@xpath=//label[text()='Port To Number']/following-sibling::input";
		public static final String webaccessblocked_checkbox = "@xpath=//label[text()='Web Access Blocked']/following-sibling::div//input";
		public static final String portdate = "@xpath=//label[text()='Port Date']/parent::div/following-sibling::div//input";
		public static final String porttime = "@xpath=//label[text()='Port Time']/following-sibling::input";
		public static final String cancelporting = "@xpath=//span[text()='Cancel Porting']";
		public static final String portingstatus = "@xpath=//label[text()='Porting Status :']/following-sibling::span";
		public static final String viewsan_header = "@xpath=//div[text()='View SAN']";
		public static final String serviceprofilevalue = "@xpath=//label[text()='Service Profile']/parent::div/following-sibling::div";
		public static final String supervisionmodevalue = "@xpath=//label[text()='Supervision Mode']/parent::div/following-sibling::div";
		public static final String internationaloutgoingcallsvalue = "@xpath=//label[text()='International Outgoing Calls Forbidden']/parent::div/following-sibling::div";
		public static final String internationalincomingcallsvalue = "@xpath=//label[text()='International Incoming Calls Barring']/parent::div/following-sibling::div";
		public static final String mobilecallsallowedvalue = "@xpath=//label[text()='Mobile Calls Allowed']/parent::div/following-sibling::div";
		public static final String view_noreplytimervalue = "@xpath=//label[text()='No Reply Timer Value']/parent::div/following-sibling::div";
		public static final String maxcalldurationvalue = "@xpath=//label[text()='Max Call Duration']/parent::div/following-sibling::div";
		public static final String chargebandnamevalue = "@xpath=//label[text()='Charge Band Name']/parent::div/following-sibling::div";
		public static final String payphoneblockingenabledvalue = "@xpath=//label[text()='Pay phone blocking enabled']/parent::div/following-sibling::div";
		public static final String webaccessblockedvalue = "@xpath=//label[text()='Web Access Blocked']/parent::div/following-sibling::div";
		public static final String sanblockvalue = "@xpath=//label[text()='SAN Block']/parent::div/following-sibling::div";
		public static final String focenabledvalue = "@xpath=//label[text()='FOC Enabled']/parent::div/following-sibling::div";
		public static final String predestinationnumbervalue = "@xpath=//label[text()='Pre Destination Number']/parent::div/following-sibling::div";
		public static final String CPSvalue = "@xpath=//label[text()='CPS']/parent::div/following-sibling::div";
		public static final String ringtonumbervalue = "@xpath=//label[text()='Ring to Number']/parent::div/following-sibling::div";
		public static final String announcementtoplay = "@xpath=//label[text()='Announcement to Play']/parent::div/following-sibling::div";
		public static final String treenamevalue = "@xpath=//label[text()='Tree Name']/parent::div/following-sibling::div";
		public static final String addanothersanlink = "@xpath=//a[text()='Add Another SAN']";
		public static final String editsan_header = "@xpath=//div[text()='Edit SAN']";
		public static final String sanmoveheader = "@xpath=//div[@class='modal-header']//div[text()='SAN Move']";
		public static final String bulkmoveheader = "@xpath=//div[@class='modal-header']//div[text()='Bulk Move']";
		public static final String destinationcustomername = "@xpath=//div[label[text()='Destination Customer Name']]//input";
		public static final String orderservice_dropdown = "@xpath=//div[label[text()='Order/Service']]//input";
		public static final String movebutton_sanmove = "@xpath=//span[text()='Move']";
		public static final String destinationcustomername_warningmsg = "@xpath=//div[label[text()='Destination Customer Name']]/following-sibling::div";
		public static final String orderservice_warningmsg = "@xpath=//div[label[text()='Order/Service']]/following-sibling::div";
		public static final String bulkmove_countrywarngmsg = "@xpath=(//div[label[text()='Country']]/following-sibling::div)[2]";
		public static final String bulkmove_customerwarngmsg = "@xpath=//div[label[text()='Customer']]/following-sibling::div";
		public static final String filterfrcnumber_warngmsg = "@xpath=//div[label[text()='Filter FRC Number']]/following-sibling::div";
		public static final String bulkmove_Servicewarngmsg = "@xpath=//div[label[text()='Service']]/following-sibling::div";
		public static final String bulkmove_countrydropdown = "@xpath=//div[label[text()='Country']]//input";
		public static final String bulkmove_customerdropdown = "@xpath=//div[label[text()='Customer']]//input";
		public static final String filterfrcnumberdropdown = "@xpath=(//div[label[text()='Filter FRC Number']]//div//input)[1]";
		public static final String bulkmove_servicedropdown = "@xpath=//div[label[text()='Service']]//input";
		public static final String manageaddnlfrc_header = "@xpath=//div[text()='Manage Addnl FRC']";
		public static final String addaddnlfrc_link = "@xpath=//a[text()='Add Addnl FRC']";
		public static final String addpriceannouncement_header = "@xpath=//div[text()='Add Price Announcement']";
		public static final String editpriceannouncement_header = "@xpath=//div[text()='Edit Price Announcement']";
		public static final String viewpriceannouncement_header = "@xpath=//div[text()='View Price Announcement ']";
		public static final String sanfrcvalue = "@xpath=//label[text()='SAN/FRC']/parent::div/following-sibling::div";
		public static final String customernamevalue = "@xpath=//label[text()='Customer Name']/parent::div/following-sibling::div";
		public static final String enablepriceannouncement_checkbox = "@xpath=//label[text()='Enable Price Announcement']/parent::div//div//input[@type='checkbox']";
		public static final String priceannouncement_dropdown = "@xpath=(//label[text()='Price Announcement']/parent::div/div/div/div)[1]";
		public static final String priceannorigin_dropdown = "@xpath=(//label[text()='Price Ann Origin']/parent::div/div/div/div)[1]/input";
		public static final String interruptible_checkbox = "@xpath=//label[text()='Interruptible Price Announcement']/parent::div//input[@type='checkbox']";
		public static final String valueinprice_field = "@xpath=//label[text()='Value In Price In(Cents)']/parent::div//input[@id='valueInPrice']";
		public static final String sendfci_checkbox = "@xpath=//label[text()='Send FCI']/parent::div//input[@type='checkbox']";
		public static final String sendsci_checkbox = "@xpath=//label[text()='Send SCI']/parent::div//input[@type='checkbox']";
		public static final String enablecallerconfirmation_checkbox = "@xpath=//label[text()='Enable Caller Confirmation']/parent::div//input[@type='checkbox']";
		public static final String callerconfirmationannouncement_dropdown = "@xpath=(//label[text()='Caller Confirmation Announcement']/parent::div//input)[1]";
		public static final String callerconfirmationdigit_field = "@xpath=//label[text()='Caller Confirmation Digit']/parent::div//input";
		public static final String noofrepetitionallowed_field = "@xpath=//label[text()='Number of Repetition Allowed']/parent::div//input";
		public static final String serviceprofile_dropdownvalue = "@xpath=(//label[text()='Service Profile']/parent::div//div//span[@role='option'])[1]";
		public static final String addsan_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String manageaddnlfrc_actiondropdown = "@xpath=//div[contains(text(),'Manage Addnl FRC')]/following-sibling::div/div//button[text()='Action']";
		public static final String viewsan_customername = "@xpath=(//label[text()='Customer Name']/parent::div/parent::div//div)[2]";
		public static final String viewsan_sannumber = "@xpath=(//label[text()='SAN(FRC) Number ']/parent::div/parent::div//div)[2]";
		public static final String viewsan_serviceprofile = "@xpath=//label[text()='Service Profile']/parent::div/following-sibling::div";
		public static final String viewsan_actiondropdown = "@xpath=//div[contains(text(),'View SAN')]/following-sibling::div/div//button[text()='Action']";
		public static final String editsan_customername = "@xpath=//label[text()='Customer Name']/parent::div/parent::div//div//input";
		public static final String editsan_sannumber = "@xpath=//label[text()='SAN(FRC) Number ']/parent::div/parent::div//div//input";
		public static final String editsan_successmsg = "@xpath=//div[@role='alert']//span";
		public static final String sanpanel_searchsanmenu = "@xpath=//div[@col-id='frcNumber']//span[@ref='eMenu']//span";
		public static final String select_sansearchtype = "@xpath=//div[text()='SAN/FRC']/parent::div/parent::div//div[@class='ag-menu']//select";
		public static final String sannumbersearchfield = "@xpath=//div[text()='SAN/FRC']/parent::div/parent::div//div[@class='ag-menu']//div[@class='ag-filter-body']//input[@id='filterText']";
		public static final String Port_Customername = "@xpath=//label[text()='Customer Name']/parent::div/following-sibling::div";
		public static final String Port_SanNumber = "@xpath=//label[text()='SAN(FRC) Number']/parent::div/following-sibling::div";
		public static final String port_okbutton = "@xpath=//button[@type='button']//span[text()='Ok']";
		public static final String port_closesymbol = "@xpath=//span[text()='�']";
		public static final String port_errordisplay = "@xpath=//div[@role='alert']//span";
		public static final String sanmove_customername = "@xpath=//label[text()='Origin Customer Name']/parent::div/parent::div/parent::div/following-sibling::div//div//div";
		public static final String sanmove_sannumber = "@xpath=//label[text()='SAN/FRC']/parent::div/parent::div/parent::div/following-sibling::div//div//div";
		public static final String successmsgalert = "@xpath=//div[@role='alert']//span";
		public static final String manageaddnlfrc_frcnumber_column = "@xpath=//span[text()='FRC Number']";
		public static final String manageaddnlfrc_priceannflag_column = "@xpath=//span[text()='Price Ann Flag']";
		public static final String manageaddnlfrc_priceannorigin_column = "@xpath=//span[text()='Price Ann Origin']";
		public static final String manageaddnlfrc_priceannouncement_column = "@xpath=//span[text()='Price Announcement']";
		public static final String manageaddnlfrc_maxcallflag_column = "@xpath=//span[text()='Max Call Flag']";
		public static final String manageaddnlfrc_maxcallorigin_column = "@xpath=//span[text()='Max Call Origin']";
		public static final String manageaddnlfrc_maxcallduration_column = "@xpath=//span[text()='Max Call Duration']";
		public static final String manageaddnlfrc_frcnumber_value = "@xpath=//div[@role='gridcell'][@col-id='frcNumber']";
		public static final String manageaddnlfrc_priceannflag_value = "@xpath=//div[@role='gridcell'][@col-id='priceAnnouncementEnabled']";
		public static final String manageaddnlfrc_priceannorigin_value = "@xpath=//div[@role='gridcell'][@col-id='priceAnnouncementOrigin']";
		public static final String manageaddnlfrc_priceannouncement_value = "@xpath=//div[@role='gridcell'][@col-id='priceAnnouncement']";
		public static final String manageaddnlfrc_maxcallflag_value = "@xpath=//div[@role='gridcell'][@col-id='maxCallEnabled']";
		public static final String manageaddnlfrc_maxcallorigin_value = "@xpath=//div[@role='gridcell'][@col-id='maxCallOrigin']";
		public static final String manageaddnlfrc_maxcallduration_value = "@xpath=//div[@role='gridcell'][@col-id='maxCallDuration']";
		public static final String priceannouncementvalue_viewpage = "@xpath=(//label[text()='Price Announcement']/parent::div/div/div/div)[1]/span";
		public static final String priceannoriginvalue_viewpage = "@xpath=(//label[text()='Price Ann Origin']/parent::div/div/div/div)[1]/span";
		public static final String viewaddnlfrc_closesymbol = "@xpath=//span[text()='�']";
		public static final String chargebandnamevalue_viewpage = "@xpath=//label[text()='Charge Band Name']/parent::div//div/span";
		public static final String callerconfirmationannouncementvalue_viewpage = "@xpath=(//label[text()='Caller Confirmation Announcement']/parent::div//div)[3]/span";
		public static final String addsan_addbutton = "@xpath=//span[text()='Add']";
		public static final String addfrc_chargebandname = "@xpath=(//label[text()='Charge Band Name']/parent::div//div/input)[1]";
		public static final String editfrc_cancelbutton = "@xpath=(//span[contains(text(),'Cancel')])[2]";
		public static final String frc_deletebutton = "@xpath=//button[text()='Delete']";

		// public static final String !-- SAN Management --="@xpath=
		public static final String managecoltnetworklink = "@xpath=(//div[@class='ant-menu-submenu-title']/span/b)[2]";
		public static final String searchsanlink = "@xpath=//li[text()='Search for SANs']";
		public static final String sansearchheader = "@xpath=//div[text()='SAN Search']";
		public static final String santextfield = "@xpath=//div[text()='SAN Search']/parent::div/following-sibling::form//div//input[@id='san']";
		public static final String san_searchbutton = "@xpath=//button[@type='submit']//span";
		public static final String serviceprofilecolumn = "@xpath=//span[@role='columnheader'][text()='Service Profile']";
		public static final String customernamecolumn = "@xpath=//span[@role='columnheader'][text()='Customer Name']";
		public static final String begindatecolumn = "@xpath=//span[@role='columnheader'][text()='Begin Date']";
		public static final String ordernamecolumn = "@xpath=//span[@role='columnheader'][text()='Order Name']";
		public static final String downloadtoexcellink = "@xpath=//a//span[text()='  Download to Excel ']";
		public static final String searchsan_actiondropdown = "@xpath=//button[text()='Action']";
		public static final String searchsan_viewpageheader = "@xpath=//div//p[text()='View San']";
		public static final String ordername_link = "@xpath=//div[@col-id='orderNumber']//a";
		public static final String searchfororder_header = "@xpath=//div[@class='row']//h5//p";
		public static final String searchfororder_viewlink = "@xpath=//div[@class='dropdown-menu show']//a[text()='View ']";
		public static final String searchfororder_managelink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";
		public static final String searchsan_announcementtoplayvalue = "@xpath=//label[text()='Announcement To Play']/parent::div/following-sibling::div";
		public static final String searchsan_predestinationnumbervalue = "@xpath=//label[text()='Pre destination Number']/parent::div/following-sibling::div";
		public static final String searchsan_internationalincomingcallsvalue = "@xpath=//label[text()='International Incoming Calls Barring']/parent::div/following-sibling::div";
		public static final String searchsan_deletebutton = "@xpath=//button[text()='Delete']";
		// public static final String
		// manageservice_header="@xpath=//div[text()='Manage Service']";
		public static final String viewsan_sanmgmt = "@xpath=(//label[text()='SAN(FRC) Number']/parent::div/parent::div//div)[2]";

		// public static final String !-- NGIN Message --="@xpath=
		public static final String nginmessageslink = "@xpath=//li[text()='NGIN Messages']";
		public static final String nginmessageheader = "@xpath=//div[contains(text(),'Manage Messages - Messages Search')]";
		public static final String nginmsg_sannumber = "@xpath=//div[contains(text(),'Manage Messages')]/parent::div/following-sibling::form//div//input[@id='msgsan']";
		public static final String nginmsg_customername_textfield = "@xpath=//label[text()='Customer Name ']/following-sibling::input";
		public static final String nginmsg_frcnumbercolumn = "@xpath=//span[@role='columnheader'][text()='FRC Number ']";
		public static final String nginmsg_namecolumn = "@xpath=//span[@role='columnheader'][text()='Name']";
		public static final String editmessageheader = "@xpath=//div[text()='Edit Message']";
		public static final String editmessage_customername = "@xpath=//label[text()='Customer Name']/following-sibling::input";
		public static final String editmessage_countrycode = "@xpath=//label[text()='Country Code']/following-sibling::input";
		public static final String editmessage_sanreference = "@xpath=//label[text()='SAN Reference']/following-sibling::input";
		public static final String editmessage_name = "@xpath=//label[text()='Name']/following-sibling::input";
		public static final String editmessage_description = "@xpath=//label[text()='Description']/following-sibling::input";
		public static final String editmessage_bodyfield = "@xpath=//label[text()='Body']/following-sibling::input";
		public static final String editmessage_prefixfield = "@xpath=//label[text()='Prefix']/following-sibling::input";
		public static final String editmessage_repetitionsfield = "@xpath=//label[text()='Repetitions']/following-sibling::input";
		public static final String editmessage_durationfield = "@xpath=//label[text()='Duration']/following-sibling::input";
		public static final String editmessage_suffixfield = "@xpath=//label[text()='Suffix']/following-sibling::input";
		public static final String editmessage_startnetworkcharge = "@xpath=//label[text()='Start Network Charge']/following-sibling::div//input";
		public static final String editmessage_okbutton = "@xpath=//button[@type='button']//span[text()='Ok']";
		public static final String editmessage_successmessage = "@xpath=//div[@role='alert']//span";

		// public static final String !-- success Message --="@xpath=
		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";
		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

	}

}
