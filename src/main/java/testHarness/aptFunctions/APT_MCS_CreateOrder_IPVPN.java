package testHarness.aptFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;
import com.relevantcodes.extentreports.LogStatus;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.aptObjects.APT_DoimainManagementObj;
import pageObjects.aptObjects.APT_IPTransitObj;
import pageObjects.aptObjects.APT_MCN_CreateAccessCoreDeviceObj;
import pageObjects.aptObjects.APT_MCS_CreateOrder_IPVPNSwiftNetObj;
import pageObjects.aptObjects.ManageNetwork_createAccessCOreDeviceObj;

public class APT_MCS_CreateOrder_IPVPN extends SeleniumUtils {

	public static String InterfaceName = null;

	public void createcustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink, " Manage Customer Service Link");
		click(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink, " Manage Customer Service Link");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.createcustomerlink, " create customer link");
		click(APTIPAccessNoCPE.APTNoCPE.createcustomerlink, "Create Customer Link");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.createcustomer_header, "create customer page header");

		// scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
		verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");
		click(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");

		// Warning msg check
		verifyExists(APTIPAccessNoCPE.APTNoCPE.customernamewarngmsg, "Legal Customer Name");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.countrywarngmsg, "Country");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.ocnwarngmsg, "OCN");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.typewarngmsg, "Type");
		verifyExists(APTIPAccessNoCPE.APTNoCPE.emailwarngmsg, "Email");

		// Create customer by providing all info

		verifyExists(APTIPAccessNoCPE.APTNoCPE.nametextfield, "name");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.nametextfield, name, "name");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.maindomaintextfield, "Main Domain");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.maindomaintextfield, maindomain, "Main Domain");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.country, "country");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.country,country,"country");
		addDropdownValues_commonMethod("Country", APTIPAccessNoCPE.APTNoCPE.country, country);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.ocntextfield, "ocn");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.ocntextfield, ocn, "ocn");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.referencetextfield, "reference");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.referencetextfield, reference, "reference");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.technicalcontactnametextfield, "technical contact name");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.technicalcontactnametextfield, technicalcontactname,
				"technical contact name");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.typedropdown, "technical contact name");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.typedropdown,type,"technical
		// contact name");
		addDropdownValues_commonMethod("Type", APTIPAccessNoCPE.APTNoCPE.type, type);

		verifyExists(APTIPAccessNoCPE.APTNoCPE.emailtextfield, "email");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.emailtextfield, email, "email");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.phonetextfield, "phone text field");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.phonetextfield, phone, "phone text field");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.faxtextfield, "fax text field");
		sendKeys(APTIPAccessNoCPE.APTNoCPE.faxtextfield, fax, "fax text field");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");
		click(APTIPAccessNoCPE.APTNoCPE.okbutton, "ok");

		verifyExists(APTIPAccessNoCPE.APTNoCPE.customercreationsuccessmsg, "Customer successfully created.");

	}

	public void selectCustomertocreateOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String ChooseCustomerToBeSelected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ChooseCustomerToBeSelected");
		// String customerName1 = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo,"Name1");
		// String customerName2 = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo,"Name2");

		verifyExists(APT_DoimainManagementObj.Domainmanagement.ManageCustomerServiceLink,
				"Manage Customer Service Link");
		click(APT_DoimainManagementObj.Domainmanagement.ManageCustomerServiceLink, "Manage Customer Service Link");
		waitForAjax();

		verifyExists(APT_DoimainManagementObj.Domainmanagement.CreateOrderServiceLink, "Create Order Service Link");
		click(APT_DoimainManagementObj.Domainmanagement.CreateOrderServiceLink, "Create Order Service Link");

		// Entering Customer name
		verifyExists(APT_DoimainManagementObj.Domainmanagement.entercustomernamefield, "Customer Name");
		sendKeys(APT_DoimainManagementObj.Domainmanagement.entercustomernamefield, ChooseCustomerToBeSelected);

		waitForAjax();

		// Select Customer from dropdown
		addDropdownValues_commonMethod("Choose a customer",
				APT_DoimainManagementObj.Domainmanagement.chooseCustomerdropdown, ChooseCustomerToBeSelected);

		verifyExists(APT_DoimainManagementObj.Domainmanagement.nextbutton, "Next");
		click(APT_DoimainManagementObj.Domainmanagement.nextbutton, "next");

		waitforPagetobeenable();
	}

	public void verifyservicetypeandSubtype(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String servicetype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");

		String servicesubtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceSubType");

		// select service type
		// scrollUp();

		addDropdownValues_commonMethod("Service Type",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicetypetextfield, servicetype);
		addDropdownValues_commonMethod("IP VPN Type",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicetypetextfield, servicesubtype);

		// click on next button
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.nextbutton, "Next button");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.nextbutton, "Next button");

	}

	public void createservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String Remarks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");
		String EmailService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String PhoneService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String DeliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DeliveryChannel");
		String Pakage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Package");
		String AllowSMCEmail = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AllowSMCEmail");
		String DialUserAdmin = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DialUserAdmin");
		String ApplicationAwareNetwork = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ApplicationAwareNetwork");
		String ProactiveNotification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ProactiveNotification");
		String ManagementVpn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management_Vpn");
		String VPNTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Topology");
		String MulticastCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"MulticastCheckbox");
		String WholesaleVPN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Wholesale_VPN/Global_MPLS");
		String PerformanceReportingType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Performance_Reporting_Type");

		// Thread.sleep(3000);
		compareText("Create Order / Service Header",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.createorderservice_header,
				"Create Order / Service");

		// Clear(getwebelement(xml.getlocator("//locators/" + application +
		// "/serviceidentificationtextfield")));
		clearTextBox(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceidentificationtextfield);

		// WebElement OKbutton= getwebelement(xml.getlocator("//locators/" +
		// application + "/OKbutton_ServiceCreation"));
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.OKbutton_ServiceCreation);

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.OKbutton_ServiceCreation, "OK");
		// Thread.sleep(1000);
		// scrollUp();
		// //Thread.sleep(1000);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.sidwarngmsg);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.sidwarngmsg, "Service Identification");
		clearTextBox(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceidentificationtextfield);

		// Clear(getwebelement(xml.getlocator("//locators/" + application +
		// "/serviceidentificationtextfield")));
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceidentificationtextfield,
				"Service Identification");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceidentificationtextfield, sid,
				"Service Identification");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.remarktextarea, "Remarks");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.remarktextarea, Remarks, "Remarks");

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.remarktextarea);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailtextfield, "Email");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailtextfield, EmailService, "Email");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailarrow, "Email Arrow");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailarrow, "Email Arrow");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonecontacttextfield, "Phone Contact");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonecontacttextfield, PhoneService,
				"Phone Contact");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonearrow, "Phone Contact Arrow");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonearrow, "Phone Contact Arrow");

		//addDropdownValues_commonMethod("Delivery Channel",
				//APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeliveryChannelField, DeliveryChannel);
		//String type = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IPVPNType);

		/*
		 * if(!type.equalsIgnoreCase("IP Voice")) {
		 * addDropdownValues_commonMethod("Package",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PackageField,
		 * Pakage); //Thread.sleep(2000);
		 * 
		 * if(!type.equalsIgnoreCase("IP Voice")) {
		 * addDropdownValues_commonMethod("Package",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PackageField,
		 * Pakage);
		 * 
		 * if(Pakage.equalsIgnoreCase("Silver")) {
		 * 
		 * { List<WebElement> Checkbox =
		 * webDriver.findElements(By.xpath("//div[@class='checkbox']//input"));
		 * int count=Checkbox.size();
		 * 
		 * for(int i=0;i<count;i++) { if(Checkbox.get(i).isSelected()) { String
		 * CheckboxName=Checkbox.get(i).getAttribute("name");
		 * 
		 * ExtentTestManager.getTest().log(LogStatus.PASS,
		 * "text field is Checked"); Report.LogInfo("TestStatus",
		 * "text field is Checked", "PASS"); } }
		 * 
		 * } } } if(Pakage.equalsIgnoreCase("Gold")) {
		 * 
		 * {
		 * 
		 * // WebElement webElement=driver.findElement(By.xpath(
		 * "//div[@class='checkbox']//input")); //webElement.isSelected();
		 * List<WebElement> Checkbox =
		 * webDriver.findElements(By.xpath("//div[@class='checkbox']//input"));
		 * int count=Checkbox.size();
		 * 
		 * for(int i=0;i<count;i++) { if(Checkbox.get(i).isSelected()) { String
		 * CheckboxName=Checkbox.get(i).getAttribute("name");
		 * 
		 * ExtentTestManager.getTest().log(LogStatus.PASS, CheckboxName +
		 * " text field is Checked"); Report.LogInfo("INFO", CheckboxName +
		 * " text field is Checked", "PASS"); } } }
		 * 
		 * } }
		 */

		/*
		 * if(AllowSMCEmail.equalsIgnoreCase("Yes")) {
		 * if(type.contains("IPVPN")) { ClickCommon("Allow SMC Email"); }
		 * ClickCommon("Router Configuration View IPV6"); }
		 * if(DialUserAdmin.equalsIgnoreCase("Yes")) {
		 * ClickCommon("Dial User Administration"); }
		 * if(ApplicationAwareNetwork.equalsIgnoreCase("Yes")) {
		 * ClickCommon("Application Aware Networking"); }
		 * 
		 * if(ProactiveNotification.equalsIgnoreCase("Yes")) {
		 * if(type.equalsIgnoreCase("SwiftNet")) {
		 * ClickCommon("Pro-active Notification");
		 * addDropdownValues_commonMethod("Notification Management Team",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * SelectValueDropdown, "DNA"); } }
		 * 
		 * //scrollToTop();
		 * 
		 * if(type.equalsIgnoreCase("IPVPN Connect"
		 * )||type.equalsIgnoreCase("IPVPN IPSec")||type.
		 * equalsIgnoreCase("IP VPN Access")) {
		 * 
		 * scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField);
		 * 
		 * addDropdownValues_commonMethod("Management Vpn",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField, ManagementVpn);
		 * addDropdownValues_commonMethod("VPN Topology",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNTopologyField,
		 * VPNTopology); ClickCommon("Per CoS Performance Reporting");
		 * addDropdownValues_commonMethod("Wholesale VPN/Global MPLS",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * WholesaleVPNField, WholesaleVPN);
		 * addDropdownValues_commonMethod("Performance Reporting Type",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * SelectValueDropdown, PerformanceReportingType);
		 * 
		 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * MultiVPNCheckbox,"MultiVPNCheckbox");
		 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * DisableVoipCheckbox,"MultiVPNCheckbox");
		 * addDropdownValues_commonMethod("Management Vpn",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField, ManagementVpn);
		 * 
		 * } if(type.equalsIgnoreCase("IPVPN Plus")) { scrolltoend();
		 * addDropdownValues_commonMethod("Management Vpn",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField, ManagementVpn);
		 * addDropdownValues_commonMethod("VPN Topology",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNTopologyField,
		 * VPNTopology);
		 * addDropdownValues_commonMethod("Wholesale VPN/Global MPLS",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * WholesaleVPNField, WholesaleVPN);
		 * addDropdownValues_commonMethod("Performance Reporting Type",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * SelectValueDropdown, PerformanceReportingType);
		 * 
		 * if(MulticastCheckbox.equalsIgnoreCase("Yes")) {
		 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * MulticastCheckbox,"MulticastCheckbox"); }
		 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * MultiVPNCheckbox,"MultiVPNCheckbox");
		 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * DisableVoipCheckbox,"DisableVoipCheckbox");
		 * addDropdownValues_commonMethod("Management Vpn",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField, ManagementVpn); }
		 */
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.OKbutton_ServiceCreation);

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.OKbutton_ServiceCreation, "Ok Button");
		// Thread.sleep(3000);
		waitforPagetobeenable();
		compareText("Service creation success msg",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicecreationmessage,
				"Service successfully created");

	}

	public void verifyCustomerDetailsInformation(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String newCustomerCreation = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"newCustomerCreation");
		String existingCustomerSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomerSelection");
		String newCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
		String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Fax");
		String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"existingCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MainDomain");

		if (newCustomerCreation.equalsIgnoreCase("Yes") || existingCustomerSelection.equalsIgnoreCase("No")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Name_Value, "Name Value");
			String nameValue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Name_Value);
			nameValue.equalsIgnoreCase(newCustomer);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Country_Value, "Country");
			String countryValue = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Country_Value);
			countryValue.equalsIgnoreCase(country);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.OCN_Value, "OCN");
			String OCN = getTextFrom(APTIPAccessNoCPE.APTNoCPE.OCN_Value);
			OCN.equalsIgnoreCase(ocn);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Reference_Value, "Reference");
			String Reference = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Reference_Value);
			Reference.equalsIgnoreCase(reference);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.TechnicalContactName_Value, "Technical Contact Name");
			String technicalContactName = getTextFrom(APTIPAccessNoCPE.APTNoCPE.TechnicalContactName_Value);
			technicalContactName.equalsIgnoreCase(technicalcontactname);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Type_Value, "Type");
			String Type = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Type_Value);
			Type.equalsIgnoreCase(type);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Email_Value, "Email");
			String Email = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Email_Value);
			Email.equalsIgnoreCase(email);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Phone_Value, "Phone");
			String Phone = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Phone_Value);
			Phone.equalsIgnoreCase(phone);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.Fax_Value, "Fax");
			String Fax = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Fax_Value);
			Fax.equalsIgnoreCase(fax);
		} else if (newCustomerCreation.equalsIgnoreCase("No") || existingCustomerSelection.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.Name_Value, "Fax");
			String ExistingCustomer = getTextFrom(APTIPAccessNoCPE.APTNoCPE.Name_Value);
			ExistingCustomer.equalsIgnoreCase(existingCustomer);
		}
		// Main Domain
		if (maindomain.equalsIgnoreCase("Null")) {
			// Reporter.log("A default displays for main domain field, if no
			// provided while creating customer");

		} else {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.MainDomain_Value, "Main Domain");
			String Maindomain = getTextFrom(APTIPAccessNoCPE.APTNoCPE.MainDomain_Value);
			Maindomain.equalsIgnoreCase(maindomain);
		}
		// Reporter.log("Customer Details panel fields Verified");
	}

	public void searchorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");

		verifyExists(APT_IPTransitObj.APT_IPTransit.ManageCustomerServiceLink, " Manage Customer Service Link");
		mouseMoveOn(APT_IPTransitObj.APT_IPTransit.ManageCustomerServiceLink);

		verifyExists(APT_IPTransitObj.APT_IPTransit.searchorderlink, "search order link");
		click(APT_IPTransitObj.APT_IPTransit.searchorderlink, "search order link");

		verifyExists(APT_IPTransitObj.APT_IPTransit.servicefield, "service field");
		sendKeys(APT_IPTransitObj.APT_IPTransit.servicefield, sid,"Service identification number");

		String searchbutton = getTextFrom(APT_IPTransitObj.APT_IPTransit.searchbutton);
		click(APT_IPTransitObj.APT_IPTransit.searchbutton, "searchbutton");

		verifyExists(APT_IPTransitObj.APT_IPTransit.serviceradiobutton, "service radio button");
		click(APT_IPTransitObj.APT_IPTransit.serviceradiobutton, "service radio button");

		verifyExists(APT_IPTransitObj.APT_IPTransit.searchorder_actiondropdown, "search order actiodropdown");
		click(APT_IPTransitObj.APT_IPTransit.searchorder_actiondropdown, "search order linksearch order actiodropdown");

		verifyExists(APT_IPTransitObj.APT_IPTransit.view, "view");
		click(APT_IPTransitObj.APT_IPTransit.view, "view");

	}

	public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"editOrderSelection");
		String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_OrderNumber");
		String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"EditOrder_VoicelineNumber");

		if (editOrderSelection.equalsIgnoreCase("no")) {
			// Reporter.log("Edit Order is not performed");
		} else if (editOrderSelection.equalsIgnoreCase("Yes")) {
			// Reporter.log("Performing Edit Order Functionality");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");
			click(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderheader, "Edit Order");

			String EditOrderNo = getTextFrom(APTIPAccessNoCPE.APTNoCPE.editorderno);
			click(APTIPAccessNoCPE.APTNoCPE.editorderno, "edit order no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editorderno);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderno, "Order Number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno, editorderno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, "edit voice line no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editvoicelineno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.cancelbutton, "cancel button");
			click(APTIPAccessNoCPE.APTNoCPE.cancelbutton, "cancel button");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");
			click(APTIPAccessNoCPE.APTNoCPE.editorderlink, "Edit Order");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderheader, "Edit Order Header");
			String editOrderHeader = getTextFrom(APTIPAccessNoCPE.APTNoCPE.editorderheader);
			String EditOrder = "Edit Order";
			editOrderHeader.equalsIgnoreCase(EditOrder);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorderno, "edit order no");
			click(APTIPAccessNoCPE.APTNoCPE.editorderno, "edit order no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editorderno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno, editorderno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, "edit voice line no");
			clearTextBox(APTIPAccessNoCPE.APTNoCPE.editvoicelineno);
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editvoicelineno, editvoicelineno, "Order Number");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.editorder_okbutton, "OK Button");
			click(APTIPAccessNoCPE.APTNoCPE.editorder_okbutton, "OK Button");

			if (editorderno.equalsIgnoreCase("Null")) {
				// Reporter.log("Order/Contract Number (Parent SID) field is not
				// edited");
			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ordernumbervalue, "order number value");
				String editOrderno = getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordernumbervalue);
				editOrderno.equalsIgnoreCase(editorderno);
			}

			if (editvoicelineno.equalsIgnoreCase("Null")) {
				// Reporter.log("RFI/RFQ/IP Voice Line Number' field is not
				// edited");
			} else {
				verifyExists(APTIPAccessNoCPE.APTNoCPE.ordervoicelinenumbervalue, "order voice line number value");
				String editVoicelineno = getTextFrom(APTIPAccessNoCPE.APTNoCPE.ordervoicelinenumbervalue);
				editVoicelineno.equalsIgnoreCase(editvoicelineno);
			}
			// Reporter.log("Edit Order is successful");

		}

	}

	public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"changeOrderSelection_newOrder");
		String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo,
				dataSetNo, "changeOrderSelection_existingOrder");
		String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_OrderNumber1");
		String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_VoicelineNumber");
		String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ChangeOrder_existingOrderNumber");

		if ((changeOrderSelection_newOrder.equalsIgnoreCase("No"))
				&& (changeOrderSelection_existingOrder.equalsIgnoreCase("No"))) {
			// Reporter.log("Change Order is not performed");

		} else if (changeOrderSelection_newOrder.equalsIgnoreCase("Yes")) {
			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "Action dropdown");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");
			click(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderheader, "change order header");
			String changeOrderHeader = getTextFrom(APTIPAccessNoCPE.APTNoCPE.changeorderheader);
			String changeOrder = "Change Order";
			changeOrderHeader.equalsIgnoreCase(changeOrder);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_selectorderswitch, "select order switch");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_selectorderswitch, "select order switch");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeordernumber, "change order number");
			click(APTIPAccessNoCPE.APTNoCPE.changeordernumber, "change order number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.editorderno, ChangeOrder_newOrderNumber);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber, "change order voiceline number");
			click(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber, "change order voiceline number");
			sendKeys(APTIPAccessNoCPE.APTNoCPE.changeordervoicelinenumber, changevoicelineno);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.createorder_button, "create order button");
			click(APTIPAccessNoCPE.APTNoCPE.createorder_button, "create order button");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");

			// Reporter.log("Change Order is successful");
		} else if (changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) {
			// Reporter.log("Performing Change Order functionality");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "order action button");
			click(APTIPAccessNoCPE.APTNoCPE.orderactionbutton, "order action button");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");
			click(APTIPAccessNoCPE.APTNoCPE.changeorderlink, "change order link");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorderheader, "change order");

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_chooseorderdropdown,
					"Order/Contract Number (Parent SID)");
			select(APTIPAccessNoCPE.APTNoCPE.changeorder_chooseorderdropdown, ChangeOrder_existingOrderNumber);

			verifyExists(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");
			click(APTIPAccessNoCPE.APTNoCPE.changeorder_okbutton, "change order ok button");

			// Reporter.log("Change Order is successful");

		}

	}

	public void verifyorderpanelinformation_Neworder(String testDataFile, String dataSheet, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String expectedneworderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "");
		String expectednewvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "");

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.userspanel_header);

		if (neworder.equalsIgnoreCase("YES")) {

			String actualorderno = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ordernumbervalue);
			// Reporter.log("actual order number displayed in order panel is : "
			// + actualorderno);

			String actualvoicelineno = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ordervoicelinenumbervalue);
			// Reporter.log("actual voice line number displayed in order panel
			// is : " + actualvoicelineno);

			if (expectedneworderno.equalsIgnoreCase(actualorderno)
					&& expectednewvoicelineno.equalsIgnoreCase(actualvoicelineno)) {
				Report.LogInfo("INFO", "order information is matched", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : order information is matched");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Step : order information is not matched");
				Report.LogInfo("INFO", "order information is not matched", "FAIL");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : new order is not selected");
			Report.LogInfo("INFO", "Step : new order is not selected", "INFO");

		}

	}

	public void verifyservicepanelInformationinviewservicepage(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		// String TerminationDate =
		// getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_header,
		// "Termination Date");
		// String EmailValue =
		// getTextFrom(APT_IPTransitObj.APT_IPTransit.managesubnet_successmsg,
		// "Email");
		String ServicepanelHeader = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_header,
				"Service panel Header");
		String ServiceIdentification = getTextFrom(
				APT_IPTransitObj.APT_IPTransit.servicepanel_serviceidentificationvalue, "Service Identification");
		String ServiceType = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_servicetypevalue, "Service_Type");
		String PhoneContact = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_phone, "Phone_Contact");
		String BillingType = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_billingtype, "Billing Type");
		String Remarks = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_remarksvalue, "Remarks");

		String ServicepanelHeaderValue = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_header,
				"Service panel Header");
		String ServiceIdentificationValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Service Identification");
		String ServiceTypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Service Type");
		String PhoneContactValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Phone Contact");
		String BillingTypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Billing Type");
		String RemarksValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Remarks");

		scrollDown(APT_IPTransitObj.APT_IPTransit.orderpanelheader);

		ServicepanelHeader.equals(ServicepanelHeaderValue);
		ServiceIdentification.equals(ServiceIdentificationValue);
		ServiceType.equals(ServiceTypeValue);
		PhoneContact.equals(PhoneContactValue);
		BillingType.equals(BillingTypeValue);
		Remarks.equals(RemarksValue);

	}

	public void verifyservicepanel_links(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String servicestatuschangerequired = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceStatusChangeRequired");
		String syncstatus = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "syncstatus");
		String Subtype = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String ServiceType = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_servicetypevalue, "Service_Type");
		String servicestatus = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_servicetypevalue,
				"ServiceStatus");

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.orderpanelheader);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.manageLink, "Manage");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.manageLink, "Manage");
		waitForAjax();

		compareText("Manage service header", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.manageservice_header,
				"Manage Service");
		scrollUp();

		compareText("Service Identification", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.status_servicename,
				sid);
		compareText("Service Type", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.status_servicetype,
				ServiceType);

		String ServiceDetails_value = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.status_servicedetails);

		if (ServiceDetails_value.isEmpty()) {
			Report.LogInfo("INFO", "Service Details column value is empty as expected", "PASS");
			System.out.println("Service Details column value is empty as expected");
		} else {
			Report.LogInfo("INFO", "Service Details column value should be empty", "FAIL");
			System.out.println("Service Details column value should be empty");

		}

		//compareText("Service Status", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.status_currentstatus,
				//servicestatus);

		String LastModificationTime_value = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.status_modificationtime);
		if (LastModificationTime_value.contains("UTC")) {
			Report.LogInfo("INFO", "Service status is displayed as : " + LastModificationTime_value, "PASS");
			System.out.println("Service status is :" + LastModificationTime_value);

		} else {
			Report.LogInfo("INFO", "Incorrect modification time format", "FAIL");
			System.out.println("Incorrect modification time format");

		}
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.statuslink, "Status");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.statuslink, "Status");

		if (servicestatuschangerequired.equalsIgnoreCase("Yes")) {
			String SrviceStatusPage = APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Servicestatus_popup;
			if (isElementPresent(SrviceStatusPage)) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.changestatus_dropdown,
						"Change Status");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.changestatus_dropdown, "Change Status");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.okbutton, "OK BUtton");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.okbutton, "OK butotn");

				String ServiceStatusHistory = APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicestatushistory;

				try {
					if (isElementPresent(ServiceStatusHistory)) {

						String mess = getTextFrom(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicestatushistory);
						ExtentTestManager.getTest().log(LogStatus.PASS, mess);

						if (mess.equalsIgnoreCase("Service Status History successfully changed.")) {
							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Step : Service status change request logged");
							Report.LogInfo("TestStatus", "Service status change request logged", "PASS");
						}

					} else {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Step : Service status change request is not logged");
						Report.LogInfo("TestStatus", "Service status change request is not logged", "PASS");

					}
				} catch (StaleElementReferenceException e) {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No service history to display");
					Report.LogInfo("TestStatus", "No service history to display", "FAIL");

				}
			} else
				ExtentTestManager.getTest().log(LogStatus.PASS, "Status link is not working");
			Report.LogInfo("TestStatus", "Status link is not working", "PASS");

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Service status change not reqired");
			Report.LogInfo("TestStatus", "Service status change not reqired", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicestatus_popupclose, "Close");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicestatus_popupclose, "Close");
		}

		compareText("Service Identification", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.sync_servicename,
				sid);
		compareText("Service Type", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.sync_servicetype, ServiceType);

		String ServiceDetails_value1 = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.sync_servicedetails);

		if (ServiceDetails_value1.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Details column value is empty as expected");
			Report.LogInfo("TestStatus", "Service Details column value is empty as expected", "PASS");

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Service Details column value should be empty");
			Report.LogInfo("TestStatus", "Service Details column value is empty as expected", "PASS");

		}

		//compareText("Sync Status", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.sync_status, ServiceType);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.managepage_backbutton, "Back");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.managepage_backbutton, "Back");

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.orderpanelheader);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");

		compareText("Edit Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EditLink, "Edit");
		compareText("Delete Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteLink, "Delete");

		//compareText("Show New Infovista Report Link",
				//APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ShowNewInfovistaReportLink,
				//"Show New Infovista Report");
		compareText("Manage Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.manageLink, "Manage");

		if (Subtype.equalsIgnoreCase("IPVPN Plus")) {
			compareText("Synchronize Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SynchronizeServiceLink,
					"Synchronize");
		}
		compareText("Dump Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DumpLink, "Dump");
	}

	public void Editservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		//String Remarks = getTextFrom(APT_IPTransitObj.APT_IPTransit.servicepanel_remarksvalue, "Remarks");
		String sid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceIdentification");
		String EmailService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserEmail");
		String PhoneService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone2");
		String DeliveryChannel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"DeliveryChannel");
		String Package = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Package");
		String ManagementVpn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management_Vpn");
		String VPNTopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Topology");
		String WholesaleVPN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"WholesaleVPN/GlobalMPLS");

		waitToPageLoad();
		waitForAjax();
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.orderpanelheader);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.edit, "Edit");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.edit, "Edit");
		waitToPageLoad();
		waitForAjax();

		clearTextBox(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.remarktextarea);
		// scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cancelbutton, "Cancel");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cancelbutton, "Cancel");
		waitToPageLoad();
		waitForAjax();
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.orderpanelheader);

		//compareText("Remarks", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicepanel_remarksvalue, Remarks);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.edit, "Edit");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.edit, "Edit");
		waitToPageLoad();
		waitForAjax();

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceidentificationtextfield,"serviceidentificationtextfield");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceidentificationtextfield, sid,
				"Service Identification");

		//verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.remarktextarea);
		//sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.remarktextarea, Remarks, "Remarks");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectedEmail, "Selected Email");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectedEmail, "Selected Email");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailarrowB, "Email Arrow second");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailarrowB, "Email Arrow second");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailtextfield, "Email");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailtextfield, EmailService, "Email");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailarrow, "Email Arrow");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.emailarrow, "Email Arrow");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectedPhone, "Selected Phone");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectedPhone, "Selected Phone");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonearrowB, "Phone Arrow second");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonearrowB, "Phone Arrow second");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonecontacttextfield, "Phone Contact");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonecontacttextfield, PhoneService,
				"Phone Contact");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonearrow, "Phone Contact Arrow");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.phonearrow, "Phone Contact Arrow");

		addDropdownValues_commonMethod("Delivery Channel",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeliveryChannelField, DeliveryChannel);
		String type = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IPVPNType);

		/*
		 * if(!type.equalsIgnoreCase("IP Voice")) {
		 * addDropdownValues_commonMethod("Package",
		 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PackageField,
		 * Package); if(Package.equalsIgnoreCase("Silver")) { { List<WebElement>
		 * Checkbox =
		 * webDriver.findElements(By.xpath("//div[@class='checkbox']//input"));
		 * int count=Checkbox.size();
		 * 
		 * for(int i=0;i<count;i++) { if(Checkbox.get(i).isSelected()) { String
		 * CheckboxName=Checkbox.get(i).getAttribute("name");
		 * 
		 * ExtentTestManager.getTest().log(LogStatus.PASS, CheckboxName +
		 * " text field is Checked"); Report.LogInfo("TestStatus",
		 * "text field is Checked", "PASS"); } } } }
		 * 
		 * if(Package.equalsIgnoreCase("Gold")) { { List<WebElement> Checkbox =
		 * webDriver.findElements(By.xpath("//div[@class='checkbox']//input"));
		 * int count=Checkbox.size();
		 * 
		 * for(int i=0;i<count;i++) { if(Checkbox.get(i).isSelected()) { String
		 * CheckboxName=Checkbox.get(i).getAttribute("name");
		 * 
		 * ExtentTestManager.getTest().log(LogStatus.PASS, CheckboxName +
		 * " text field is Checked"); System.out.println(CheckboxName +
		 * " text field is displaying"); } } }
		 * 
		 * } }
		 * 
		 * scrollIntoTop(); if(type.equalsIgnoreCase("IPVPN Connect"
		 * )||type.equalsIgnoreCase("IPVPN IPSec")||type.
		 * equalsIgnoreCase("IPVPN Access")) {
		 * scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField);
		 * 
		 * addDropdownValues_commonMethod("Management Vpn"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField,ManagementVpn);
		 * addDropdownValues_commonMethod("VPN Topology"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNTopologyField
		 * ,VPNTopology);
		 * addDropdownValues_commonMethod("Wholesale VPN/Global MPLS"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * WholesaleVPNField,WholesaleVPN);
		 * addDropdownValues_commonMethod("Management Vpn"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField,ManagementVpn);
		 * 
		 * } if(type.equalsIgnoreCase("IPVPN Plus")) {
		 * scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField);
		 * 
		 * addDropdownValues_commonMethod("Management Vpn"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField,ManagementVpn);
		 * addDropdownValues_commonMethod("VPN Topology"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNTopologyField
		 * ,VPNTopology);
		 * addDropdownValues_commonMethod("Wholesale VPN/Global MPLS"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * WholesaleVPNField,WholesaleVPN);
		 * addDropdownValues_commonMethod("Management Vpn"
		 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
		 * ManagementVpnField,ManagementVpn);
		 * 
		 * }
		 */
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next");
		waitForAjax();
		compareText("Service creation success msg",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.servicecreationmessage,
				"Service updated successfully");
	}

	public void syncservices(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException {

		String orderPanel = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewServicepage_OrderPanel);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewServicepage_OrderPanel);
		waitForAjax();

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Editservice_actiondropdown, "Action");
		waitForAjax();
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Editservice_sysnchronizelink, "Synchronize link");
		waitForAjax();

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitForAjax();

		boolean syncSuccessMessage = isElementPresent(
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.alertForSynchronize));
		if (syncSuccessMessage) {

			String actualmsg = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.alertMSG_synchronize);
			Reporter.log(" success message for synchronize displays as: " + actualmsg);

		} else {
			Reporter.log(" Success message did not display after clicking on 'Synchronize' button");
		}
	}

	public void Dumpservice() throws InterruptedException, IOException {
		waitForAjax();
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.orderpanelheader);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serviceactiondropdown, "Action dropdown");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DumpLink, "Dump");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DumpLink, "Dump");
		waitForAjax();

		String DumpDetails = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DumpDetails);
		waitForAjax();

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DumpClose, "Dump Close");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DumpClose, "Dump Close");
		waitForAjax();
		waitToPageLoad();

	}

	public void verifyManagementOptionspanel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String performancereporting_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"PerformanceReporting");
		String ipguardian_checkbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ipguardian");
		// i did not get the column name for string ipguardian_checkbox

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.managementoptions_header);
		((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-100)");
		compareText("Management options header",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.managementoptions_header, "Management Options");

		//compareText("Performance Reporting",
				//APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.managementoptions_performancereporting,
				//performancereporting_checkbox);
		
	}

	public void navigateToAddNewDevicepage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String providerequipment_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"performancereporting");
		String addnewdevice_togglebutton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addnewdevice_togglebutton");

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.providerequipment_header);

		waitForAjax();
		compareText("Provider Equipment (PE)",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.providerequipment_header, providerequipment_header);
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addpedevice_link, "Add PE Device");
		waitForAjax();
		waitforPagetobeenable();
		compareText("Add PE Device Header", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addpedevice_header,
				addnewdevice_togglebutton);
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addnewdevice_togglebutton, "Add New Device toggle");
		waitForAjax();

	}

	/////////////// 4/28/21/////////////

	public void AddManageUser(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String ManageUserName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ManageUserName");

		// WebElement ManageUser_header=
		// findWebElement("//div[text()='Management Options']");
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageOptionHeader);

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserAction, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn1 + "Add"
				+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn2, "Add");
		waitForAjax();// waitforajax
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.UserOk, "Click Ok");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.UserNameWarnign, "User Name warning");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.passwordWarnign, "Password warning");
		sendKeys(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "User Name"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
				ManageUserName, "User Name");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddUserGeneretePass, "Generate Password");
		waitToPageLoad();// waittopageload
		// waitForAjax();
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectHiddenUser, "Available User");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.BackwardArrow, "Forward Arrow Click");
		waitToPageLoad();
		// waitForAjax();
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.UserOk, "Click Ok");
		getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Success Message");
		waitForAjax();
		waitToPageLoad();

	}

	public void ViewManageUser(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		// WebElement ManageUser_header=
		// findWebElement("//div[text()='Management Options']");
		String ManageUserName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ManageUserName");

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageOptionHeader);
		List<WebElement> ExistingUsers = webDriver
				.findElements(By.xpath("(//div[@ref='eBodyContainer'])[2]//div[contains(@class,'ag-row')]"));
		int NoOfUsers = ExistingUsers.size();
		System.out.println("Numberofuser=" + NoOfUsers);

		if (NoOfUsers >= 1) {
			WebElement AddedUser = webDriver
					.findElement(By.xpath("((//div[text()='" + ManageUserName + "']//parent::div)[3]//div//span)[1]"));
			AddedUser.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing VPN Site Order radio button");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

		}

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserAction, "Action dropdown");

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn1 + "View"
				+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn2, "Add");

		// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn,
		// "View");
		waitForAjax();
		// String actualValue=findWebElement("(//tr//td)[2]//div").getText();
		String actualValue = webDriver.findElement(By.xpath("(//div[@class='modal-content']//tr//td)[2]//div"))
				.getText();
		Report.LogInfo("INFO", "Device" + " Services Hidden To Users " + actualValue, "PASS");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Device" + " Services Hidden To Users " + actualValue);

		String actualValue1 = webDriver.findElement(By.xpath("(//div[@class='modal-content']//tr//td)[3]//div"))
				.getText();
		Report.LogInfo("Info", "Device" + " Services Available To Users " + actualValue1, "Pass");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Services Available To Users " + actualValue1);

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DumpClose, "Close");
		waitForAjax();

	}

	public void EditManageUser(String testDataFile, String sheetName, String scriptNo, String dataSetNo,
			String ManageUserName, String EditUserName) throws IOException, InterruptedException {

		String ManageUserName1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManageUserName");
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageOptionHeader);

		List<WebElement> ExistingUsers = webDriver
				.findElements(By.xpath("(//div[@ref='eBodyContainer'])[2]//div[contains(@class,'ag-row')]"));
		int NoOfUsers = ExistingUsers.size();
		System.out.println("Numberofuser=" + NoOfUsers);

		if (NoOfUsers >= 1) {
			/*
			 * WebElement AddedUser = webDriver
			 * .findElement(By.xpath("((//div[text()='"+ManageUserName1+
			 * "']//parent::div)[3]//div//span)[1]")); AddedUser.click();
			 */
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing VPN Site Order radio button");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

		}

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserAction, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn1 + "Edit"
				+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn2, "Add");

		// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn,
		// "Edit");
		waitForAjax();
		sendKeys(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "User Name"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
				EditUserName, "User Name");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddUserGeneretePass, "Generate Password");
		waitToPageLoad();
		// waitForAjax();
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AvailableHiddenUser, "Available User");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ForwardArrow, "Forward Arrow Click");
		waitToPageLoad();
		// waitForAjax();
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.UserOk, "Click Ok");
		// waitForAjax();
		getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Success Message");
		waitForAjax();
		waitToPageLoad();

	}

	public void DeleteManageUser(String ServiceSubType, String EditUserName)
			throws IOException, InterruptedException, AWTException {

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageOptionHeader);

		List<WebElement> ExistingUsers = webDriver
				.findElements(By.xpath("(//div[@ref='eBodyContainer'])[2]//div[contains(@class,'ag-row')]"));
		int NoOfUsers = ExistingUsers.size();
		System.out.println("Numberofuser=" + NoOfUsers);

		if (NoOfUsers >= 1) {
			WebElement AddedUser = webDriver
					.findElement(By.xpath("((//div[text()='" + EditUserName + "']//parent::div)[3]//div//span)[1]"));
			AddedUser.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing VPN Site Order radio button");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

		}

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserAction, "Action dropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn1 + "Delete"
				+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn2, "Add");

		// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ManageUserActionDdn,
		// "Delete");
		// waitForAjax();

		if (isAlertPresent() == true) {
			Robot r = new Robot();
			Thread.sleep(1000);
			r.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
		}
		// compareText( "Delete Device", "SuccessMessageCom", "Static Route
		// successfully deleted.");

	}

	public void AddVPNAlis(String testDataFile, String sheetName, String scriptNo, String dataSetNo,
			String ServiceSubType, String VPNName, String VPNAlis) throws InterruptedException, IOException {
		// String VPNName = DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "VPNName");
		// String VPNAlis = DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "VPNAlis");
		if (ServiceSubType.contains("IPVPN")) {
			WebElement ManageUser_header = findWebElement("//div[text()='Configuration Options']");
			scrollIntoView(ManageUser_header);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNAlisactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNAlisActionCommonLink, "Add");
			waitForAjax();
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "VPN Name"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "VPN Name");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "VPN Alias"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "VPN Alias");

			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "VPN Name"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					VPNName, "VPN Name");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "VPN Alis"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					VPNAlis, "VPN Alis");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next");
			waitForAjax();
			waitToPageLoad();
			waitForAjax();
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Success Message");
			waitForAjax();
			waitToPageLoad();
			waitForAjax();
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Add VPN Alis Should not Present");
		}

	}

	public void EditVPNAlis(String testDataFile, String sheetName, String scriptNo, String dataSetNo,
			String ServiceSubType, String VPNName, String VPNAlis) throws IOException {

		// String VPNName = DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "VPN Name");
		// String VPNAlis = DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "VPN Alis");

		if (ServiceSubType.contains("IPVPN")) {
			WebElement ManageUser_header = findWebElement("//div[text()='Configuration Options']");
			scrollIntoView(ManageUser_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"(//div[text()='VPN Name/Alias']//parent::div//following-sibling::div)[3]//div[contains(@class,'ag-body-container')]//div[contains(@class,'ag-row')]"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers >= 0) {
				WebElement AddedUser = webDriver
						.findElement(By.xpath("((//div[text()='" + VPNName + "']//parent::div)[1]//div//span)[2]"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");

				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNAlisactionbutton, "Action dropdown");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNAlisActionCommonLink, "Edit");
				waitForAjax();
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "VPN Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						VPNName, "VPN Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "VPN Alias"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						VPNAlis, "VPN Alias");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.multilink_okButton, "Click Ok");
				waitForAjax();

				waitToPageLoad();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Success Message");

				waitForAjax();
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Edit VPN Alis Should not Present");
		}

	}

	public void DeleteVPNAlis(String ServiceSubType, String VPNName, String VPNAlis) throws IOException {

		if (ServiceSubType.contains("IPVPN")) {

			WebElement ManageUser_header = findWebElement("//div[text()='Configuration Options']");
			scrollIntoView(ManageUser_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"(//div[text()='VPN Name/Alias']//parent::div//following-sibling::div)[3]//div[contains(@class,'ag-body-container')]//div[contains(@class,'ag-row')]"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers >= 0) {
				WebElement AddedUser = webDriver
						.findElement(By.xpath("((//div[text()='" + VPNName + "']//parent::div)[1]//div//span)[2]"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNAlisactionbutton, "Action dropdown");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNAlisActionCommonLink, "Delete");
				waitForAjax();
				// AcceptJavaScriptMethod();
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				waitForAjax();
				waitToPageLoad();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Success Message");
				waitForAjax();
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Delete VPN Alis Should not Present");
		}

	}

	public void AddVPNSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNCountry");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNDeviceCity");
		String VPNPhysicalSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNPhysicalSite");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNVendorModel");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteOrder");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNRouterId");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNManagementAdd");
		String VoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipService");
		String HubSpoke = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "HubSpoke");
		String SelectSiteToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectSiteToggle");
		String SelectCityToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectCityToggle");
		String Device_Xng_City_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Name");
		String Device_Xng_City_Code = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Code");
		String VoipCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipCheckbox");
		String DeviceToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceToggle");
		String Physical_Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Physical_Site");

		if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2") || ServiceSubType.equalsIgnoreCase("CPE Solutions L3")
				|| ServiceSubType.equalsIgnoreCase("IP Voice") || ServiceSubType.equalsIgnoreCase("SwiftNet")) {
			if (!ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSitelink);

				// WebElement VPNSiteOrder_header=
				// findWebElement("//locators/VPNSiteOrderHeader");
				// breadcrumb =
				// getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump).replace("value",
				// breadCrumpLink);
				// scrollIntoView(VPNSiteOrder_header);
			} else {
				// WebElement HUB_header= findWebElement("//div[text()='Hub']");
				// scrollIntoView(HUB_header);
			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSitelink, "Add VPN Site Order Link");
			waitForAjax();
			// compareText( "Add VPN Site Order header", "AddVPNSitePage", "Add
			// VPN Site Order");
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Site Order Number"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Country"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Xng City"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Physical Site"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Router Id"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Management Address"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Vendor/Model"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			//verifyExists(
					//APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Name"
							//+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					//"Site Order Number");

			if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2")
					|| ServiceSubType.equalsIgnoreCase("CPE Solutions L3")) {
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1
								+ "Snmp V3 Auth Password"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
						"Site Order Number");
			}
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderNum, VPNSiteOrder,
					"Site Order Number");
			if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2")
					|| ServiceSubType.equalsIgnoreCase("CPE Solutions L3")) {
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteAlis, VPNSiteAlias, "Site Alias");
			}
			addDropdownValues_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCountry,
					VPNCountry, "Device Country");

			if (SelectCityToggle.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectCityToogle, "Toggle");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Device Xng City Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						Device_Xng_City_Name, "Device Xng City Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Device Xng City Code"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						Device_Xng_City_Code, "Device Xng City Code");

			} else {
				addDropdownValues_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCity,
						VPNDeviceCity, "Device Xng City");
			}

			if (SelectSiteToggle.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectsiteToogleL2, "Toggle");
				addDropdownValues_commonMethod("Physical Site",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Physical Site"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						Physical_Site);
				// addDropdownValues_commonMethod( "Physical Site",
				// "SelectValueDropdown", " (1232)", xml);
			} else {
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNPhysicalSite, VPNPhysicalSite,
						"Physical Site");
			}
			if (ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				addDropdownValues_commonMethod("VPN Site Order Type",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "VPN Site Order Type"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						HubSpoke);

			}
			if (VoipCheckbox.equalsIgnoreCase("YES")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "VoIP"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "VoIP");
				addDropdownValues_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVoipService,
						VoipService, "VoIP Class of Service");
			}
			// scrollIntoView(findWebElement("//div[text()='Device']"));

			if (!DeviceToggle.equalsIgnoreCase("Yes")) {
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNRouterId, VPNRouterId, "Router Id");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNManagementAdd, VPNManagementAdd,
						"Management Address");
				// addDropdownValues_commonMethod( "Vendor/Model",
				// "VPNVendoeModel", VPNVendorModel, xml);

				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVendorModel1, "VPNVendorModel");
				// waitForAjax();

				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVendorModel1, VPNVendorModel);
				// waitForAjax();

				// click(webDriver.findElement("(//div[contains(text(),'"+
				// VPNVendorModel +"')])[1]"));
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVendorModel1, "Addnewdevice_togglebutton");
				// waitForAjax();

				isAlertPresent();

				AcceptJavaScriptMethod();
				// click(getwebelement("//span[@aria-hidden='true']"));
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceName, "Device Name");
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass1);
				if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2")
						|| ServiceSubType.equalsIgnoreCase("CPE Solutions L3")) {
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass1, "Generate Password");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass2, "Generate Password");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Test", "Premise Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Code"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"123", "Premise Code");

				} else {
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt", "Snmpro");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt123", "Snmprw");

					if (ServiceSubType.equalsIgnoreCase("SwiftNet")) {
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 User Name"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt", "Snmp V3 User Name");

					} else {
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 Security User Name"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt", "Snmp V3 Security User Name");
					}
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Auth Password"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt123", "Snmp V3 Auth Password");

					// sendKeys( "Colt32", "Snmp V3 Priv Password",
					// "TextValueCommon");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Context Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"ColtTest", "Snmp V3 Context Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Context Engine ID"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"123", "Snmp V3 Context Engine ID");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Security User Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"ColtTest", "Snmp V3 Security User Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Test", "Premise Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Code"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"123", "Premise Code");

				}
			} else {
				addDropdownValues_commonMethod("Device Name",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Device Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						VPNVendorModel);

			}

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			waitToPageLoad();
			waitForAjax();
			compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Site order created successfully", "Add VPN Site");
			waitForAjax();
		}
	}

	public void clickBreadCrump(String breadCrumpLink) throws IOException, InterruptedException {

		((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitForAjax();
		String breadcrumb = null;

		try {
			breadcrumb = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump).replace("value",
					breadCrumpLink);

			if (isElementPresent(breadcrumb)) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump, breadcrumb);
			} else {
				Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Reporter.log("Breadcrumb is not displaying for the element " + breadcrumb);
		}
	}

	public void AddVPNSiteOrderPlus(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNCountry");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNDeviceCity");

		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteOrder");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");

		String VoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipService");

		String SelectSiteToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectSiteToggle");
		String SelectCityToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectCityToggle");
		String Device_Xng_City_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Name");
		String Device_Xng_City_Code = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Code");
		String VoipCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipCheckbox");

		String Physical_Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Physical_Site");
		String SiteOrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrderType");
		String IVReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IVReference");
		String CSRName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSRName");
		String VirtualCPECheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VirtualCPECheckbox");
		String CPEName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEName");
		String PerfReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PerfReport");
		String PerCoS = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PerCoS");
		String RouterIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterIPv4");
		String RouterIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterIPv6");
		String DSLSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DSLSite");

		String SpeedboatSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SpeedboatSite");

		String ActelisBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ActelisBased");
		String IAReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IAReference");

		if (ServiceSubType.equalsIgnoreCase("IPVPN Plus")) {
			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + +
			// "/VPNSiteOrderHeader"));
			// breadcrumb =
			// getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump).replace("value",
			// breadCrumpLink);
			String VPNSiteOrderHeader = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);

			// scrollIntoView(VPNSiteactionbutton);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteOrderLink, "Add VPN Site Order");

			// compareText( "Add VPN Site Order header", "AddVPNSitePage", "Add
			// VPN Site Order");

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");

			// WarningMessageCom( "Device Country","WarningMessageddnCommon");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Xng City"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Device XNG City");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Physical Site/CSR Name"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Physical Site");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "IA Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"IA Reference");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "CPE Name"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "CPE Name");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "IV Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"IV Reference");

			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderNum, VPNSiteOrder,
					"Site Order Number");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteAlis, VPNSiteAlias, "Site Alias");
			addDropdownValues_commonMethod("Device Country",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCountry, VPNCountry);

			if (SelectCityToggle.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectCityToogle, "Toggle");

				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Device Xng City Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						Device_Xng_City_Name, "Device Xng City Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Device Xng City Code"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						Device_Xng_City_Code, "Device Xng Code Name");
			} else {
				addDropdownValues_commonMethod("Device Xng City",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCity, VPNDeviceCity);
			}

			if (SelectSiteToggle.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectsiteToogle, "Toggle");
				addDropdownValues_commonMethod("Physical Site",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Physical Site"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						Physical_Site);
				// addDropdownValues_commonMethod( "Physical Site",
				// "SelectValueDropdown", " (1232)", xml);
			} else {
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Physical Site/CSR Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						CSRName, "Physical Site/CSR Name");

			}
			addDropdownValues_commonMethod("Site Order Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Site Order Type"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					SiteOrderType);
			if (SiteOrderType.equalsIgnoreCase("secondary")) {
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Primary Cpe Lan Ip"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Check1", "Primary Cpe Lan Ip");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Primary Loopback Ip"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Check2", "Primary Loopback Ip");
			}
			if (SiteOrderType.equalsIgnoreCase("primary")) {
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Secondary Cpe Lan Ip"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Check2", "Secondary Cpe Lan Ip");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Secondary Cpe Lan Ip"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Check1", "Secondary Cpe Lan Ip");

			}
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Cpe Lan Interface"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					"10.38", "Cpe Lan Interface");
			// scrollToViewNClick( "Cpe Lan Interface", "SelectValueDropdown");
			// addDropdownValues_commonMethod( "Priority",
			// "SelectValueDropdown", Priority, xml);
			addDropdownValues_commonMethod(
					"IV Reference", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1
							+ "IV Reference" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					IVReference);

			if (VoipCheckbox.equalsIgnoreCase("YES")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "VoIP"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "VoIP");

				addDropdownValues_commonMethod("VoIP Class of Service",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVoipService, VoipService);
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "IPV Number"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"980", "IPV Number");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Band Width"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"2", "Band Width");
				addDropdownValues_commonMethod("Smart Monitoring Destination",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVoipService, "ITSM Ticket");
			}
			if (VirtualCPECheckbox.equalsIgnoreCase("YES")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "VirtualCPE Site"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "VirtualCPE Site");
				if (VoipCheckbox.equalsIgnoreCase("YES")) {
					addDropdownValues_commonMethod("VoIP Class of Service",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVoipService, VoipService);
				}
			} else {
				addDropdownValues_commonMethod("Managed Site",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVoipService, "no");
				addDropdownValues_commonMethod("Smart Monitoring Destination",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVoipService, "ITSM Ticket");

			}
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "VPN Proposition ID"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					"123", "VPN Proposition ID");

			addDropdownValues_commonMethod("Performance Reporting",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Performance Reporting"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					PerfReport);
			addDropdownValues_commonMethod("Per CoS Performance Reporting",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1
							+ "Per CoS Performance Reporting"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					PerCoS);
			// scrollToViewNClick( "Performance Reporting",
			// "SelectValueDropdown");
			addDropdownValues_commonMethod("Router Configuration View IPv4",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1
							+ "Router Configuration View IPv4"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					RouterIPv4);
			addDropdownValues_commonMethod("Router Configuration View IPv6",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1
							+ "Router Configuration View IPv6"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					RouterIPv6);
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Related Site Order Number"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					"2345", "Related Site Order Number");

			// scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown);
			if (!VirtualCPECheckbox.equalsIgnoreCase("YES")) {
				if (DSLSite.equalsIgnoreCase("Yes")) {
					// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon,"DSL
					// Site");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "DSL Site"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "DSL Site");

				}

				if (SpeedboatSite.equalsIgnoreCase("Yes")) {
					if (!DSLSite.equalsIgnoreCase("Yes")) {
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "Speedboat Site"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2,
								"Speedboat Site");

					}
				}
				if (ActelisBased.equalsIgnoreCase("Yes")) {
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "Actelis Based"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "Actelis Based");
				}
			}
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "SDVpn Site"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "SDVpn Site");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "VRRP IP"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					IAReference, "VRRP IP");

			// sendKeys( CPEName, "Bespoke Reference", "TextValueCommon");

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "IA Reference"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1);
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "IA Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					IAReference, "IA Reference");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "CPE Name"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					CPEName, "CPE Name");

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RateFlagRadio, "Rate Flag");
			addDropdownValues_commonMethod("Billing Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Billing Type"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					"UBB - Per GigaByte");
			// addDropdownValues_commonMethod( "CNG Option",
			// "SelectValueDropdown", RouterIv6, xml);
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNRemark, CPEName, "Remark");
			// sendKeys( CPEName, "BGP as Number Local", "TextValueCommon");
			// sendKeys( CPEName, "BGP as Number Remote", "TextValueCommon");
			if (VirtualCPECheckbox.equalsIgnoreCase("YES")) {
				addDropdownValues_commonMethod("L2 Technology",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "L2 Technology"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						"Actelis");
			}
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			waitForAjax();
			CompareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Success Message");
			waitForAjax();
		}
	}

	private void verifyExist(String string, String string2) {
		// TODO Auto-generated method stub

	}

	//////////////// need to update///////////////////////
	public void deletInterface_ActelisConfiguration(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws IOException, InterruptedException {
		String interfaceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interfaceName");

		// select the interface
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deleteInterafce1 + interfaceName
				+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deleteInterafce2,"deleteInterafce2");

		// click on Action button
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AcionButton_ActelisConfiguration,"AcionButton_ActelisConfiguration");

		// Remove Button
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.removeButton_ActelisConfiguration,"removeButton_ActelisConfiguration");

		boolean popupMessage = false;
		popupMessage = isElementPresent(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.popupMessage_forRemove_ActelisConfiguration);

		if (popupMessage) {
			String actualmsg = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.popupMessage_forRemove_ActelisConfiguration);
			Reporter.log(" On clicking remoe button, popup message displays as: " + actualmsg);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deleteInterfaceX,"deleteInterfaceX");
			waitForAjax();
		} else {

			// popup message does not display after clicking on 'Remove' button
		}
	}

	//////////////// 5-3-2021////////////////////////////////////////////
	public void AddVPNSiteOrder3(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Device_Country");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Device_City");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Site_Alis");
		String CSRName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSRName");
		// String SiteOrderType=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "SiteOrderType");
		String IAReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IAReference");
		String IVReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IV_Reference");
		String PerfReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PerfReport");
		String PerCoS = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PerCoS");
		String RouterIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterIPv4");
		String RouterIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterIPv6");
		String CPEName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEName");
		String VoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipService");
		String SelectSiteToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectSiteToggle");
		String SelectCityToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectCityToggle");
		String Device_Xng_City_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Name");
		String Device_Xng_City_Code = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Code");
		String VoipCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoIPCheckBox");
		String DSLSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DSLsiteOrder");
		String SpeedboatSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Speedboat_Site");
		String ActelisBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ActelisBased");
		String CNGOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CNG-Option");
		// String CNG=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
		// dataSetNo, "CNG");
		String Physical_Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Physical_Site");

		if (ServiceSubType.equalsIgnoreCase("IPVPN IPSec") || ServiceSubType.equalsIgnoreCase("IPVPN Connect")
				|| ServiceSubType.equalsIgnoreCase("IPVPN Access")) {
			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Site Order Number"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");

			// WarningMessageCom( "Device Country","WarningMessageddnCommon");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Xng City"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Device XNG City");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Physical Site/CSR Name"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Physical Site");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "IA Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"IA Reference");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "CPE Name"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "CPE Name");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "IV Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"IV Reference");

			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderNum, VPNSiteOrder,
					"Site Order Number");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteAlis, VPNSiteAlias, "Site Alias");
			addDropdownValues_commonMethod("Device Country",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCountry, VPNCountry);

			addDropdownValues_commonMethod("Device Xng City",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCity, VPNDeviceCity);

			/*
			 * if(SelectCityToggle.equalsIgnoreCase("Yes")) {
			 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectCityToogle,"Toggle");
			 * sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon1+"Device Xng City Name"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon2,Device_Xng_City_Name, "Device Xng City Name");
			 * sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon1+"Device Xng City Code"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon2,Device_Xng_City_Code, "Device Xng Code Name"); }
			 * else { addDropdownValues_commonMethod("Device Xng City"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * VPNDeviceCity, VPNDeviceCity); }
			 */
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Physical Site/CSR Name"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					CSRName, "Physical Site/CSR Name");

			/*
			 * if(SelectSiteToggle.equalsIgnoreCase("Yes")) { click(
			 * APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectsiteToogle,"Toggle");
			 * addDropdownValues_commonMethod("Physical Site"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * VPNPhysicalSite, Physical_Site);
			 * //addDropdownValues_commonMethod( "Physical Site",
			 * "SelectValueDropdown", " (1232)", xml);
			 * 
			 * } else {
			 * sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon1+"Physical Site/CSR Name"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon2,CSRName, "Physical Site/CSR Name");
			 * 
			 * }
			 */

			addDropdownValues_commonMethod(
					"IV Reference", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1
							+ "IV Reference" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
					IVReference);

			/*
			 * if(VoipCheckbox.equalsIgnoreCase("YES")) {
			 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CheckboxCommon1+"VoIP"+APT_MCS_CreateOrder_IPVPNSwiftNetObj.
			 * CreateCustomer.CheckboxCommon2,"VoIP"); }
			 * addDropdownValues_commonMethod("VoIP Class of Service"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * VPNVoipService, VoipService);
			 * 
			 * }
			 * 
			 * addDropdownValues_commonMethod("Performance Reporting"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown1+"Performance Reporting"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown2, PerfReport);
			 * addDropdownValues_commonMethod("Per CoS Performance Reporting"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown1+"Per CoS Performance Reporting"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown2, PerCoS);
			 * addDropdownValues_commonMethod("Managed Site"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown1+"Managed Site"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown2, "no");
			 * addDropdownValues_commonMethod("Router Configuration View IPv4"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown1+"Router Configuration View IPv4"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown2, RouterIPv4);
			 * addDropdownValues_commonMethod("Router Configuration View IPv6"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown1+"Router Configuration View IPv6"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown2, RouterIPv6);
			 * sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon1+"Related Site Order Number"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon2,"2345", "Related Site Order Number");
			 */
			// scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon);

			/*
			 * if(DSLSite.equalsIgnoreCase("Yes")) {
			 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CheckboxCommon1+"DSL Site"+APT_MCS_CreateOrder_IPVPNSwiftNetObj.
			 * CreateCustomer.CheckboxCommon2,"DSL Site"); }
			 * if(ServiceSubType.equalsIgnoreCase("IPVPN Access")) {
			 * if(SpeedboatSite.equalsIgnoreCase("Yes")) {
			 * if(!DSLSite.equalsIgnoreCase("Yes")) {
			 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CheckboxCommon1+"Speedboat Site"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CheckboxCommon2,"Speedboat Site"); } } }
			 */
			/*
			 * if(ServiceSubType.equalsIgnoreCase("IPVPN IPSec")||ServiceSubType
			 * .equalsIgnoreCase("IPVPN Connect")) {
			 * 
			 * if(ActelisBased.equalsIgnoreCase("Yes")) {
			 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CheckboxCommon1+"Actelis Based"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CheckboxCommon2,"Actelis Based"); }
			 * sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * TextValueCommon1+"CIID"+APT_MCS_CreateOrder_IPVPNSwiftNetObj.
			 * CreateCustomer.TextValueCommon2,IAReference, "CIID");
			 * addDropdownValues_commonMethod("CNG Option"
			 * ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown1+"CNG Option"
			 * +APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * SelectValueDropdown2, CNGOption);
			 * 
			 * //addDropdownValues_commonMethod( "CNG", "SelectValueDropdown",
			 * CNG, xml); }
			 */
			addDropdownValues_commonMethod("Wholesale Provider",APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1+"Wholesale Provider"+APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2, "IPC-Standard");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "IA Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					IAReference, "IA Reference");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "CPE Name"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					CPEName, "CPE Name");

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RateFlagRadio, "Rate Flag");
			// addDropdownValues_commonMethod("Billing
			// Type",APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1+"Billing
			// Type"+APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
			// "UBB - Per GigaByte");

			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNRemark, CPEName, "Remark");

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			CompareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Site order created successfully");
			waitForAjax();
		}
	}

	public void SwifNetSpoke(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNRouterId");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNManagementAdd");
		String HubSpoke = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "HubSpoke");
		String SelectCityToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectCityToggle");
		String DeviceToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DeviceToggle");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNVendorModel");
		String VPNPhysicalSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNPhysicalSite");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNCountry");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNDeviceCity");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");

		String VoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipService");
		String SelectSiteToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"SelectSiteToggle");
		// String SelectCityToggle=DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "SelectCityToggle");
		String Device_Xng_City_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Name");
		String Device_Xng_City_Code = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Device_Xng_City_Code");
		String VoipCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipCheckbox");

		String Physical_Site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Physical_Site");

		if (ServiceSubType.equalsIgnoreCase("SwiftNet")) {

			WebElement HUB_header = findWebElement("//div[text()='Spoke']");
			scrollIntoView(HUB_header);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSitelinkSpoke, "Add VPN Site Order Link");
			waitForAjax();
			// compareText( "Add VPN Site Order header", "AddVPNSitePage", "Add
			// VPN Site Order");

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Site Order Number"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Site Order Number");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Country"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Device Country");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Xng City"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Device Xng City");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Physical Site"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Physical Site");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Router Id"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "Router Id");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Management Address"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Management Address");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Vendor/Model"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Vendor/Model");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Device Name"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "Device Name");

			if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2")
					|| ServiceSubType.equalsIgnoreCase("CPE Solutions L3")) {
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1
								+ "Snmp V3 Auth Password"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
						"Snmp V3 Auth Password");
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1
								+ "Snmp V3 Auth Password"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
						"Snmp V3 Auth Password");
			}
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderNum, VPNSiteOrder,
					"Site Order Number");
			if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2")
					|| ServiceSubType.equalsIgnoreCase("CPE Solutions L3")) {
				sendKeys(VPNSiteAlias, "Site Alias", "VPNSiteAlis");
			}
			addDropdownValues_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCountry,
					VPNCountry, "Device Country");

			if (SelectCityToggle.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectCityToogle, "Toggle");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Device Xng City Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						Device_Xng_City_Name, "Device Xng City Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Device Xng City Code"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						Device_Xng_City_Code, "Device Xng City Code");
			} else {
				addDropdownValues_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceCity,
						VPNDeviceCity, "Device Xng City");
			}

			if (SelectSiteToggle.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectsiteToogleL2, "SelectsiteToogleL2");
				addDropdownValues_commonMethod("Physical Site",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Physical Site"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						Physical_Site);
				// addDropdownValues_commonMethod( "Physical Site",
				// "SelectValueDropdown", " (1232)", xml);
			} else {
				sendKeys(VPNPhysicalSite, "Physical Site", "VPNPhysicalSite");
			}
			if (ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				addDropdownValues_commonMethod("VPN Site Order Type",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "VPN Site Order Type"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						HubSpoke);
			}
			if (VoipCheckbox.equalsIgnoreCase("YES")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "VoIP"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "VoIP");
				addDropdownValues_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVoipService,
						VoipService, "VoIP Class of Service");
			}
			// scrollIntoView(findWebElement("//div[text()='Device']"));

			if (!DeviceToggle.equalsIgnoreCase("Yes")) {
				// sendKeys( VPNRouterId, "Router Id", "VPNRouterId");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNRouterId, VPNRouterId, "Router Id");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNManagementAdd, VPNManagementAdd,
						"VPNManagementAdd");

				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVendorModel1, "VPN vendor model");
				// waitForAjax();

				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVendorModel1, VPNVendorModel);
				// waitForAjax();

				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNVendorModel1, "VPNVendorModel");
				// waitForAjax();
				// AcceptJavaScriptMethod();
				// click(getwebelement("//span[@aria-hidden='true']"));
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceName, "Device Name");
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass1);
				if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2")
						|| ServiceSubType.equalsIgnoreCase("CPE Solutions L3")) {
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass1, "Generate Password");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass2, "Generate Password");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Test", "Premise Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Code"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"123", "Premise Code");

				} else {

					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt", "Snmpro");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt123", "Snmprw");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 User Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt", "Snmp V3 User Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Auth Password"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt123", "Snmp V3 Auth Password");

					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Context Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"ColtTest", "Snmp V3 Context Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Context Engine ID"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"123", "Snmp V3 Context Engine ID");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Security User Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"ColtTest", "Snmp V3 Security User Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Test", "Premise Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Code"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"123", "Premise Code");

					// sendKeys( "Colt32", "Snmp V3 Priv Password",
					// "TextValueCommon");
				}
			} else {
				addDropdownValues_commonMethod("Device Name",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Device Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						"Device Name");

			}
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			waitforPagetobeenable();
			// waitForAjax();
			// compareText(
			// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
			// "Site order created successfully Device successfully
			// created","Add VPN Site");
			// waitForAjax();
		}
	}

	public void verifyAddedVPNSitePlusInformation_View(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteOrder");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");
		// VoipService=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "VoipService");

		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrder_header");

		// String ActelisBased=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "ActelisBased");
		// String IAReference=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "IAReference");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");

		if (ServiceSubType.equalsIgnoreCase("IPVPN Plus")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader)
					.replace("value", VPNSiteOrderHeader);

			// scrollIntoView(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiteOrder
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
			}
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "View");
			compareText(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon1 + "Site Order Number"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon2,
					VPNSiteOrder, "Site Order Number");
			compareText(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon1 + "Site Order Alias"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon2,
					VPNSiteAlias, "Site Order Alias");

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Backbutton, "Back");
			waitforPagetobeenable();
			waitForAjax();

		}

	}

	public void VerifyVPNSiteOrder3(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteOrder");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");

		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrder_header");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");

		if (ServiceSubType.equalsIgnoreCase("IPVPN IPSec") || ServiceSubType.equalsIgnoreCase("IPVPN Connect")
				|| ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);

			scrollIntoView(VPNSiteOrder_header, "VPNSiteOrder_header");
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiteOrder
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,"View");

			compareText("Site Order Number", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon1
					+ "Site Order Number" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon2,
					VPNSiteOrder);
			//compareText("Site Alias", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon1
					//+ "Site Alias" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon2,
					//VPNSiteAlias);
			

			if (isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Backbutton)) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Backbutton, "Back");
				waitforPagetobeenable();
				waitForAjax();
			} else {
				webDriver.navigate().back();
			}

		}

	}

	private void scrollIntoView(String vpnsiteorderradio, String string) {
		// TODO Auto-generated method stub

	}

	public void VerifyVPNSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNCountry");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNDeviceCity");
		String VPNPhysicalSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNPhysicalSite");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNVendorModel");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteOrder");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNRouterId");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNManagementAdd");
		String VoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipService");
		String HubSpoke = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "HubSpoke");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrder_header");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");

		if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2") || ServiceSubType.equalsIgnoreCase("CPE Solutions L3")
				|| ServiceSubType.equalsIgnoreCase("IP Voice") || ServiceSubType.equalsIgnoreCase("SwiftNet")) {
			if (!ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				// WebElement VPNSiteOrder_header=
				// getwebelement(xml.getlocator("//locators/" + +
				// "/VPNSiteOrderHeader"));
				VPNSiteOrder_header = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader).replace("value",
								VPNSiteOrderHeader);

				scrollIntoView(VPNSiteOrder_header, "VPNSiteOrder_header");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_viewdevice1, "VPN View Link");

			} else {
				WebElement HUB_header = findWebElement("//div[text()='Hub']");
				scrollIntoView(HUB_header);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Hub_viewdevice1, "VPN View Link");
			}

			waitForAjax();
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_ViewDevice_Header, "Device");

			
			if (isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Backbutton)) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Backbutton, "Back");
				waitforPagetobeenable();
				waitForAjax();
			} else {
				webDriver.navigate().back();
			}

		}
	}

	public void VerifyVPNSiteOrderSpoke(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNCountry");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNDeviceCity");
		String VPNPhysicalSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNPhysicalSite");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNVendorModel");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteOrder");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNRouterId");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNManagementAdd");
		String VoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipService");
		String HubSpoke = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "HubSpoke");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrder_header");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");

		if (ServiceSubType.equalsIgnoreCase("SwiftNet")) {

			WebElement HUB_header = findWebElement("//div[text()='Hub']");

			scrollIntoView(HUB_header);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Spoke_viewdevice1, "VPN View Link");

			waitForAjax();
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_ViewDevice_Header, "Device");

			/*
			 * getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CompareTextCommon,"Router Id");
			 * getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CompareTextCommon,"Vendor/Model");
			 * getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CompareTextCommon,"Country");
			 * getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CompareTextCommon,"City");
			 * getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CompareTextCommon,"Management Address");
			 * getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * CompareTextCommon,"Site");
			 */

			/*
			 * compareText_Common(, "Router Id", "CompareTextCommon",
			 * VPNRouterId, xml); compareText_Common(, "Vendor/Model",
			 * "CompareTextCommon", VPNVendorModel, xml); compareText_Common(,
			 * "Country", "CompareTextCommon", VPNCountry, xml);
			 * compareText_Common(, "City", "CompareTextCommon", VPNDeviceCity,
			 * xml); compareText_Common(, "Management Address",
			 * "CompareTextCommon", VPNManagementAdd, xml); compareText_Common(,
			 * "Site", "CompareTextCommon", VPNPhysicalSite, xml);
			 */
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Backbutton, "Back");

			waitforPagetobeenable();
			waitForAjax();

		}
	}

	public void shownewInfovista(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		// scrolltoview(getwebelement(xml.getlocator("//locators/" + +
		// "/orderpanelheader")));

		// WebElement orderPanel= getwebelement(xml.getlocator("//locators/" + +
		// "/viewServicepage_OrderPanel"));
		String viewServicepage_OrderPanel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"viewServicepage_OrderPanel");
		String orderPanel = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);

		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
		waitForAjax();

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Editservice_actiondropdown, "Action");
		waitForAjax();
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Editservice_infovistareport, "Show infovista link");
		waitForAjax();

		String expectedPageName = "SSO login Page";

		// Switch to new tab
		List<String> browserTabs = new ArrayList<String>(webDriver.getWindowHandles());
		webDriver.switchTo().window(browserTabs.get(1));
		waitForAjax();

		try {
			// Get Tab name
			String pageTitle = webDriver.switchTo().window(browserTabs.get(1)).getTitle();
			System.out.println("page title displays as: " + pageTitle);

			waitForAjax();
			webDriver.switchTo().window(browserTabs.get(0));

			// assertEquals(pageTitle, expectedPageName, " on clicking 'Show
			// Infovista link', it got naviagted to "+pageTitle);

			ExtentTestManager.getTest().log(LogStatus.PASS,
					"on clicking 'Show Infovista link', it got naviagted to " + pageTitle + " as expected");
			//// //Thread.sleep(3000);

			ExtentTestManager.getTest().log(LogStatus.PASS, "show info vista page actual title: " + pageTitle);
			ExtentTestManager.getTest().log(LogStatus.PASS, "show info vista page expected title: " + expectedPageName);

		} catch (Exception e) {

			e.printStackTrace();

			waitForAjax();
			webDriver.switchTo().window(browserTabs.get(0));

			ExtentTestManager.getTest().log(LogStatus.FAIL, expectedPageName + " page is not displaying");

		}

	}

	

	public void AddNewDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNCountry");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNDeviceCity");
		String VPNPhysicalSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNPhysicalSite");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNVendorModel");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteOrder");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiteAlias");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNRouterId");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNManagementAdd");
		String VoipService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VoipService");
		String HubSpoke = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "HubSpoke");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrder_header");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String SamPName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SamPName");

		if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2") || ServiceSubType.equalsIgnoreCase("CPE Solutions L3")
				|| ServiceSubType.equalsIgnoreCase("IP Voice") || ServiceSubType.equalsIgnoreCase("SwiftNet")) {

			if (!ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				// WebElement VPNSiteOrder_header=
				// getwebelement(xml.getlocator("//locators/" + +
				// "/VPNSiteOrderHeader"));
				VPNSiteOrder_header = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader).replace("value",
								VPNSiteOrderHeader);
				scrollIntoView(VPNSiteOrder_header, "VPNSiteOrder_header");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_AddNewDevice, "Add New device Link");
			} else {
				WebElement HUB_header = findWebElement("//div[text()='Hub']");
				scrollIntoView(HUB_header);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.HUB_AddNewDevice, "Add New device Link");
			}

			waitForAjax();

			scrollIntoView(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_ViewDevice_Header,
					"Add New device Link");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNRouterId, VPNRouterId, "Router Id");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNManagementAdd, VPNManagementAdd,
					"Management Address");
			// addDropdownValues_commonMethod(, "Vendor/Model",
			// "VPNVendoeModel", VPNVendorModel, xml);
			if (!ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddPrimierToggle, "Add Premise Toggle");
			} else {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddPrimierToggleSwiftNet,
						"Add Premise Toggle");
			}
			waitForAjax();
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Name"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					"Testing", "Premise Name");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Code"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					"123", "Premise Code");

			WebElement VendorModel = webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//input"));
			VendorModel.click();
			VendorModel.sendKeys(VPNVendorModel);
			// click(findWebElement("//div[label[text()='Vendor/Model']]//input"));
			WebElement VPNVendorModel1 = webDriver
					.findElement(By.xpath("(//div[contains(text(),'" + VPNVendorModel + "')])[1]"));
			VPNVendorModel1.click();
			waitForAjax();

			AcceptJavaScriptMethod();
			// click(getwebelement("//span[@aria-hidden='true']"));

			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceName, "Device Name");
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass1);

			if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2")
					|| ServiceSubType.equalsIgnoreCase("CPE Solutions L3")) {
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSampName, SamPName,
						"Snmp V3 User Name");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass1, "Generate Password");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNGeneratePass2, "Generate Password");
			} else {
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 Context Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						"Testing", "Snmp V3 Context Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
								+ "Snmp V3 Context Engine ID"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						"123", "Snmp V3 Context Engine ID");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
								+ "Snmp V3 Security User Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						"Testing", "Snmp V3 Security User Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 Auth Password"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						"123", "Snmp V3 Auth Password");

			}
			if (ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSampName, SamPName,
						"Snmp V3 User Name");
			}
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_OKbutton, "OK to Add Device");
			waitForAjax();
			waitforPagetobeenable();
			compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_AddDeviceSuccessfulMessage,
					"Site device created successfully", "Add Site Device Successful Message for VPN Order");
			waitForAjax();
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step :No Device Should Exit");
		}
	}

	public void AddWholesaleInterconnect(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String WholesaleDeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"WholesaleDeviceName");
		String WholesaleInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"WholesaleInterface");

		if (ServiceSubType.contains("IPVPN")) {
			if (ServiceSubType.equalsIgnoreCase("IPVPN Connect") || ServiceSubType.equalsIgnoreCase("IPVPN IPSec")) {
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ConfigurationOption);
			} else {
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Wholesaleactionbutton);
			}
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Wholesaleactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectInterconnectink, "SelectInterconnect Link");
			waitforPagetobeenable();
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleSearch, "Search Button");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageddnCommon, "Device");
			waitForAjax();
			// waitforPageenable();
			// addDropdownValues_commonMethod(, "Device", "WholesaleDevice",
			// WholesaleDeviceName, xml);

			// String Device =
			// webDriver.findElement("//div[label[text()='Device']]//div[text()='�']");
			WebElement Device = webDriver.findElement(By.xpath("//div[label[text()='Device']]//div[text()='�']"));
			Device.click();
			waitForAjax();

			WebElement WholeSaleDevice = webDriver.findElement(By.xpath("//div[label[text()='Device']]//input"));
			WholeSaleDevice.sendKeys(WholesaleDeviceName);

			// SendKeys(findWebElement("//div[label[text()='Device']]//input"),
			// WholesaleDeviceName);
			waitForAjax();
			WholeSaleDevice.click();
			// click(findWebElement("//div[div[text()='Select
			// Interconnect']]//following-sibling::div//div[text()='"+WholesaleDeviceName+"']"));
			waitForAjax();

			String actualValue = findWebElement("//label[text()='Device']/following-sibling::div//span").getText();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Device" + " dropdown value selected as: " + actualValue);
			System.out.println("Device" + " dropdown value selected as: " + actualValue);

			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Interface Name/Address"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					WholesaleInterface, "Interface Name/Address");

			// sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon,
			// WholesaleInterface, "Interface Name/Address");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleSearch, "Search Button");
			waitForAjax();
			// click(, "Search Button", "WholesaleSearch");
			if (ServiceSubType.equalsIgnoreCase("IPVPN Plus")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleSelectPlus, "Select Interface");
			}
			if (ServiceSubType.equalsIgnoreCase("IPVPN IPSec")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleSelectIPSec, "Select Interface");
			}
			if (ServiceSubType.equalsIgnoreCase("IPVPN Access")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleSelectAccess, "Select Interface");
			}
			if (ServiceSubType.equalsIgnoreCase("IPVPN Connect")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleSelectConnect, "Select Interface");
			}
			waitForAjax();
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleNext, "Next");
			waitforPagetobeenable();
			waitForAjax();
			compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Wholesale Interconnect successfully selected.", "Wholesale Interconnect selected");
			waitForAjax();
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Add Wholesale Should not Present");
		}

	}

	public void EditWholesaleInterconnect(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String IVManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		if (ServiceSubType.contains("IPVPN")) {
			if (ServiceSubType.equalsIgnoreCase("IPVPN Connect") || ServiceSubType.equalsIgnoreCase("IPVPN IPSec")) {
				ScrolltoElement("ConfigurationOption");
			} else {
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectWholesaleDevice);
			}
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectWholesaleDevice, "Select Device");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Wholesaleactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleEditLink, "Edit Link");
			waitforPagetobeenable();
			waitForAjax();
			if (IVManagement.equalsIgnoreCase("YES")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IV_Management, "IV Management");
			}
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetDevice, "Device Name");
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetInterface, "Device Name");
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetAddress, "Device Name");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.OKButtonWholesale, "OK Button");
			waitforPagetobeenable();
			waitForAjax();
			compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Wholesale Interconnect interface successfully updated.", "Wholesale Interconnect updated");
			waitForAjax();
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Edit  Wholesale Should not Present");
		}
	}

	public void DeleteWholesaleInterconnect(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String Delete = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Delete");

		if (ServiceSubType.contains("IPVPN")) {

			if (ServiceSubType.equalsIgnoreCase("IPVPN Connect") || ServiceSubType.equalsIgnoreCase("IPVPN IPSec")) {
				ScrolltoElement("ConfigurationOption");
			} else {
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectWholesaleDevice);
			}
			if (Delete.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectWholesaleDevice, "Select Device");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Wholesaleactionbutton, "Action dropdown");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WholesaleDeleteLink, "Delete link");
				waitforPagetobeenable();
				// Thread.sleep(8000);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteCommon, "Delete Wholesale");
				waitforPagetobeenable();
				// Thread.sleep(3000);
				compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
						"Wholesale Interconnect interface successfully removed from service.",
						"Wholesale Interconnect Deleted");
				// Thread.sleep(2000);
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Delete  Wholesale Should not Present");
		}
	}

	public void AcceptJavaScriptMethod() {

		Alert alert = webDriver.switchTo().alert();
		alert.accept();
		webDriver.switchTo().defaultContent();
	}

	public void AddCPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VPN_Vendor/Model");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Router_Id");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_Management_Address");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String GetAddressCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEGetAddress");
		String AvailableBlockCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEAvailableBlock");
		String PremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEPremiseName");
		String PremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEPremiseCode");
		String PremiseToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEPremiseToggle");
		String JitterCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEJitterRadio");
		String ConnectProtocoltal = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEConnectPortalTal");
		String ConnectProtocolssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEConnectPortalSsh");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEPremiseToggle");
		String SNMP3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPESNMP3");
		

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

			
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			// compareText_Common(, "Site Order Number", "CompareTextCommon",
			// "View VPN Order", xml);
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPE_AddPEDeviceLink);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPE_AddPEDeviceLink, "CPE Device Link");
			waitforPagetobeenable();
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Router Id"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "Router Id");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Management Address"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Management Address");

			if (ServiceSubType.equalsIgnoreCase("IPVPN Access")) {
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1
								+ "Snmp V3 Priv Password"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
						"Snmp V3 Priv Password");
			}
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageddnCommon1 + "Vendor/Model"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageddnCommon2,
					"Vendor/Model");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNRouterId, VPNRouterId, "Router Id");
			if (GetAddressCPE.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetAddressCPE, "Get Address button");
				// Thread.sleep(2000);
				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Available Blocks"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						AvailableBlockCPE);
				// Thread.sleep(6000);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FreeIPCPE, "Get Free IP button");
				// Thread.sleep(5000);
			} else {
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNManagementAdd, VPNManagementAdd,
						"Management Address");
			}
			// addDropdownValues_commonMethod(, "Vendor/Model",
			// "VPNVendoeModel", VPNVendorModel, xml);

			// WebElement VendorModel =
			// webDriver.findElement(By.xpath("//div[label[text()='Device']]//div[text()='�']"));
			WebElement VendorModel = webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//input"));
			VendorModel.click();
			WebElement VendorModel1 = webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//input"));
			VendorModel1.sendKeys(VPNVendorModel);

			WebElement VPNVendorModel2 = webDriver
					.findElement(By.xpath("(//div[contains(text(),'" + VPNVendorModel + "')])[1]"));

			VPNVendorModel2.click();

			//// //Thread.sleep(2000);
			AcceptJavaScriptMethod();
			// click(getwebelement("//span[@aria-hidden='true']"));
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNDeviceName, "Device Name");
			if (ServiceSubType.equalsIgnoreCase("IPVPN Plus") || ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

				// scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon);
			} else {
				// ScrolltoElementComm(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon,"Device
				// Name");
			}
			if (PremiseToggle.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddPrimiseToggle, "Add Premise Toggle");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						PremiseName, "Premise Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Code"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						PremiseCode, "Premise Code");

			} else {
				addDropdownValues_commonMethod("Premise",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Premise"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						Premise);

			}

			if (JitterCPE.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "Jitter Reporting"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "Jitter Reporting");

			}
			if (ServiceSubType.equalsIgnoreCase("IPVPN Plus") || ServiceSubType.equalsIgnoreCase("IPVPN Access")) {
				if (ConnectProtocoltal.equalsIgnoreCase("Yes")) {
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ConnectivityProtocolCPEtelnet,
							"Connectivity Portal Telnet");
					// Thread.sleep(3000);
				}
				if (ConnectProtocolssh.equalsIgnoreCase("Yes")) {
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ConnectivityProtocolCPEssh,
							"Connectivity Portal SSH");
				}
				if (ServiceSubType.equalsIgnoreCase("IPVPN Plus")) {

					if (SNMP3.equalsIgnoreCase("Yes")) {
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe3, "SNMP Version 3");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 User Name"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt", "Snmp V3 User Name");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 Auth Password"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt123#", "Snmp V3 Auth Password");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 Priv Password"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt654@12", "Snmp V3 Priv Password");

					} else {
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe2c, "SNMP Version 2c");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt", "Snmpro");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt123", "Snmprw");

					}
				}
				if (ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

					if (SNMP3.equalsIgnoreCase("Yes")) {
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe3, "SNMP Version 3");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 User Name"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt", "Snmp V3 User Name");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 Auth Password"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt123", "Snmp V3 Auth Password");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
										+ "Snmp V3 Priv Password"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt654@12", "Snmp V3 Priv Password");

					} else {
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe2c, "SNMP Version 2c");
						// sendKeys(, "Colt", "Snmpro", "TextValueCommon");
						// sendKeys(, "Colt123", "Snmprw", "TextValueCommon");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt", "Snmpro");
						sendKeys(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
										+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
								"Colt123", "Snmprw");
					}
				}
			} else {
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 User Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Colt", "Snmp V3 User Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 Auth Password"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Colt123", "Snmp V3 Auth Password");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 Priv Password"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Colt32", "Snmp V3 Priv Password");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 Context Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"ColtTest", "Snmp V3 Context Name");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
								+ "Snmp V3 Context Engine ID"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"123", "Snmp V3 Context Engine ID");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
								+ "Snmp V3 Security User Name"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"ColtTest", "Snmp V3 Security User Name");

				// addDropdownValues_commonMethod(, "Snmp v3 Auth Proto",
				// "SelectValueDropdown", "", xml);
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Colt", "Snmpro");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						"Colt123", "Snmprw");
			}
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
			waitforPagetobeenable();
			//// Thread.sleep(3000);
			compareText("Add CPE Device",APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Device successfully created.");
			//// Thread.sleep(2000);

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
		}
	}

	private void ScrolltoElementComm(String textvaluecommon, String string) {
		// TODO Auto-generated method stub

	}

	public void ViewCPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		// String VPNSiteOrder_header=DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "VPNSiteOrder_header");
		// String VPNSiteOrderHeader=DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "VPNSiteOrderHeader");

		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiterderNum");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + +
			// "/VPNSiteOrderHeader"));
			String VPNSiteOrder_header = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrollIntoView(VPNSiteOrder_header, "VPNSiteOrder_header");
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton,"Action
			// dropdown");
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,"View");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			// scrollUp();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			System.out.println("Numberofuser=" + NoOfDevice);

			if (NoOfDevice <= 5) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				// Thread.sleep(3000);
			} else if (NoOfDevice > 5) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device View button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");

			}
			waitforPagetobeenable();
			scrollUp();

			if (ServiceSubType.equalsIgnoreCase("IPVPN Plus")) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "User redirect on View page sucessfully");
				Report.LogInfo("INFO", "User redirect on View page sucessfully", "PASS");

			} else {
				Report.LogInfo("INFO", "Sub type not present", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Sub type not present");
			}
		}
	}

	public void EditCPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_VPN_Vendor/Model");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Router_Id");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_Management_Address");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String GetAddressCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEGetAddress");
		String AvailableBlockCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEAvailableBlock");
		String PremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEPremiseName");
		String PremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEPremiseCode");
		String PremiseToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEPremiseToggle");
		String JitterCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEJitterRadio");
		String ConnectProtocoltal = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEConnectPortalTal");
		String ConnectProtocolssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEConnectPortalSsh");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEPremiseToggle");
		String SNMP3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPESNMP3");

		// String VPNSiteOrderHeader=DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "VPNSiteOrderHeader");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrder_header");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrollIntoView(VPNSiteOrder_header, "VPNSiteOrder_header");

			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton,"Action
			// dropdown");
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,"View");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			// compareText_Common(, "Site Order Number", "CompareTextCommon",
			// "View VPN Order", xml);
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EditCPEDevice);

			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			System.out.println("Numberofuser=" + NoOfDevice);

			if (NoOfDevice <= 51) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EditCPEDevice, "Edit CPE Device");
				// Thread.sleep(3000);
			} else if (NoOfDevice > 5) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//b[contains(text(),'" + VPNRouterId
						+ "')]//parent::*//parent::div//following-sibling::div//span[contains(text(),'Edit')]"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device Edit button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");

			}
			waitforPagetobeenable();
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CancelCPEDevice);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CancelCPEDevice, "Cancel button");
			//// Thread.sleep(3000);
			waitforPagetobeenable();
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EditCPEDevice);

			List<WebElement> ExistingDevices1 = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices1 = ExistingDevices1.size();
			int NoOfDevice1 = NoOfDevices1 - 1;
			System.out.println("Numberofuser=" + NoOfDevice1);

			if (NoOfDevice1 == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EditCPEDevice, "Edit CPE Device");
				// Thread.sleep(3000);
			} else if (NoOfDevice1 >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='Edit']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device Edit button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");

			}
			waitforPagetobeenable();
			scrollUp();
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNRouterId, VPNRouterId, "Router Id");
			// EnterTextValue(, VPNRouterId, "Router Id", "VPNRouterId");
			if (GetAddressCPE.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetAddressCPE, "Get Address button");
				//// Thread.sleep(2000);s
				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Available Blocks"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						AvailableBlockCPE);
				//// Thread.sleep(6000);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FreeIPCPE, "Get Free IP button");
				//// Thread.sleep(5000);
			} else {
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNManagementAdd, VPNManagementAdd,
						"Management Address");
				waitForAjax();

				ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddPrimiseToggle);

				if (PremiseToggle.equalsIgnoreCase("Yes")) {
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddPrimiseToggle, "Add Premise Toggle");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							PremiseName, "Premise Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Premise Code"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							PremiseCode, "Premise Code");

				} else {
					addDropdownValues_commonMethod("Premise",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Premise"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
							Premise);

				}

				if (JitterCPE.equalsIgnoreCase("Yes")) {
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "Jitter Reporting"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "Jitter Reporting");

				}
				if (ServiceSubType.equalsIgnoreCase("IPVPN Plus") || ServiceSubType.equalsIgnoreCase("IPVPN Access")) {
					if (ConnectProtocoltal.equalsIgnoreCase("Yes")) {
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ConnectivityProtocolCPEtelnet,
								"Connectivity Portal Telnet");
						// Thread.sleep(3000);
					}
					if (ConnectProtocolssh.equalsIgnoreCase("Yes")) {
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ConnectivityProtocolCPEssh,
								"Connectivity Portal SSH");
					}
					if (ServiceSubType.equalsIgnoreCase("IPVPN Plus")) {

						if (SNMP3.equalsIgnoreCase("Yes")) {
							click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe3, "SNMP Version 3");

							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
											+ "Snmp V3 User Name"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt", "Snmp V3 User Name");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
											+ "Snmp V3 Auth Password"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt123#", "Snmp V3 Auth Password");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
											+ "Snmp V3 Priv Password"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt654@12", "Snmp V3 Priv Password");

						} else {
							click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe2c, "SNMP Version 2c");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt", "Snmpro");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt123", "Snmprw");

						}
					}
					if (ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

						if (SNMP3.equalsIgnoreCase("Yes")) {
							click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe3, "SNMP Version 3");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
											+ "Snmp V3 User Name"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt", "Snmp V3 User Name");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
											+ "Snmp V3 Auth Password"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt123#", "Snmp V3 Auth Password");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
											+ "Snmp V3 Priv Password"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt654@12", "Snmp V3 Priv Password");

						} else {
							click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SNMPCpe2c, "SNMP Version 2c");
							// sendKeys(, "Colt", "Snmpro", "TextValueCommon");
							// sendKeys(, "Colt123", "Snmprw",
							// "TextValueCommon");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt", "Snmpro");
							sendKeys(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
									"Colt123", "Snmprw");
						}
					}
				} else {

					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmp V3 User Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt", "Snmp V3 User Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Auth Password"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt123", "Snmp V3 Auth Password");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Priv Password"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt32", "Snmp V3 Priv Password");

					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Context Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"ColtTest", "Snmp V3 Context Name");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Context Engine ID"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"123", "Snmp V3 Context Engine ID");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
									+ "Snmp V3 Security User Name"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"ColtTest", "Snmp V3 Security User Name");

					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmpro"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt", "Snmpro");
					sendKeys(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Snmprw"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
							"Colt123", "Snmprw");

				}

				ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNNext, "Next button");
				waitforPagetobeenable();
				// Thread.sleep(3000);
				compareText("Add CPE Device",APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
						"Device successfully updated.");
				// Thread.sleep(2000);

			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
		}
	}

	public void AddInterfaceCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String CPEBearerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEBandwidthE1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBandwidthE1");
		String CPEBandwidthE3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBandwidthE3");
		String CPEBandwidthSTM = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBandwidthSTM");
		String CpeClockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeClockSource");
		String CPELink = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPELink");
		String CpeEncapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeEncapsulation");
		String CpePrimary_Backup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpenterfaceDirection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpePrimary/Backup");
		String CpeVoiceline = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeVoiceline");
		String CpeIVManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeIVManagement");
		String CpeIVBitCounter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeIVBitCounter");
		String CpeAddRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeAddRange");
		String CpeSecondaryIp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeSecondaryIp");
		String CpeNetmask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeNetmask");
		String CpeSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeSpeed");
		String CpeDuplex = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeDuplex");
		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeInterface");
		String CpeInterfaceAddRangeDdn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeInterfaceAddRangeDdn");
		String CpeInterfaceAddRangeText = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeInterfaceAddRangeText");
		String CpeAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeAddress");
		String CPEGetAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeAddress");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeAddInterfaceButton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeAddInterfaceButton");
		String InterfaceCNGReferance = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceCNGReferance");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrderNumber");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		// String VPNSiteOrder_header=DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "VPNSiteOrderHeader");
		// String VPNSiteOrderHeader=DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo, "VPNSiteOrderHeader");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			// String VPNSiteOrder_header =
			// getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton,
			// "Action dropdown");
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,"View
			// ");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			// scrollToElement();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			System.out.println("Numberofuser=" + NoOfDevice);

			if (NoOfDevice == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				// Thread.sleep(3000);
			} else if (NoOfDevice >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device View button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");

			}
			waitforPagetobeenable();
			// WebElement Interface_header=
			// webDriver.findElement(By.xpath("//div[text()='Routes']"));

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButton);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionCommonLink1 + "Add interface/link "
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionCommonLink2,
					"Add interface/link ");
			waitforPagetobeenable();

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SaveInterface, "Save button");
			// Thread.sleep(3000);
			waitforPagetobeenable();
			scrollUp();
			List<WebElement> WarningMssg = webDriver.findElements(By.xpath("//div[@role='alert']//span//li"));
			int MsgSize = WarningMssg.size();
			System.out.println("Numberofuser=" + MsgSize);
			for (int i = 0; i < MsgSize; i++) {
				String msg = WarningMssg.get(i).getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, msg + "   Displayed as Warning msg");
			}

			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Interface"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					CpeInterface, "Interface");
			// ScrolltoElementComm(
			// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon,"Interface");
			if (CPEGetAddress.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetAddressCPE, "Get Address button");
				// Thread.sleep(5000);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AllocateSubnetButton,
						"Allocate Subnet button");
				// Thread.sleep(3000);
				waitforPagetobeenable();
				// addDropdownValues_commonMethod(application, "Interface
				// Address Range", "SelectValueDropdown",
				// CpeInterfaceAddRangeDdn, xml);
				WebElement arrowbadgebadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[1]"));
				arrowbadgebadgesecondary.click();

				waitforPagetobeenable();
				addDropdownValues_commonMethod("Address",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Address"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeAddress);

			} else {
				sendKeys(CpeInterfaceAddRangeText, "Interface Address Range", "TextValueCommon");
				WebElement arrowbadgebadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[1]"));
				arrowbadgebadgesecondary.click();

				waitforPagetobeenable();
				// sendKeys(application, CpeAddress, "Address",
				// "TextValueCommon");

			}
			ScrolltoElementComm("Bearer Type", "SelectValueDropdown");
			if (ServiceSubType.equalsIgnoreCase("IPVPN IPSec") || ServiceSubType.equalsIgnoreCase("IPVPN Connect")) {
				addDropdownValues_commonMethod("CNG Reference",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "CNG Reference"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						InterfaceCNGReferance);
			}
			if (!CPEBearerType.equalsIgnoreCase("Ethernet")) {

				addDropdownValues_commonMethod("Bearer Type",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bearer Type"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPEBearerType);
				// Thread.sleep(2000);
				waitforPagetobeenable();
				if (CPEBearerType.equalsIgnoreCase("E3") || CPEBearerType.equalsIgnoreCase("T3")) {
					addDropdownValues_commonMethod("Bandwidth",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bandwidth"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
							CPEBandwidthE3);

				}
				if (CPEBearerType.equalsIgnoreCase("STM-1")) {
					addDropdownValues_commonMethod("Bandwidth",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bandwidth"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
							CPEBandwidthSTM);
				}
				if (CPEBearerType.equalsIgnoreCase("E1")) {
					addDropdownValues_commonMethod("Bandwidth",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bandwidth"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
							CPEBandwidthE1);
				}

				addDropdownValues_commonMethod("Encapsulation",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Encapsulation"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeEncapsulation);
				// addDropdownValues_commonMethod(application, "Clock Source",
				// "SelectValueDropdown", CpeClockSource, xml);
				// addDropdownValues_commonMethod(application, "Primary/Backup",
				// "SelectValueDropdown", CpePrimary_Backup, xml);
				// addDropdownValues_commonMethod(application, "Interface
				// Direction", "SelectValueDropdown", CpenterfaceDirection,
				// xml);

				WebElement ClockSource = webDriver
						.findElement(By.xpath("//div[div[label[text()='Clock Source']]]//select"));
				ClockSource.click();
				waitForAjax();

				// click(getwebelement());

				WebElement CpeClockSource1 = webDriver.findElement(By.xpath(
						"//div[div[label[text()='Clock Source']]]//select//option[@label='" + CpeClockSource + "']"));
				CpeClockSource1.click();
				waitForAjax();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Clock Source" + " dropdown value selected as: " + CpeClockSource);

				// click(getwebelement("//div[div[label[text()='Primary/Backup']]]//select"));
				// waitForAjax();

				WebElement PrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select"));
				PrimaryBackup.click();
				waitForAjax();

				// click(getwebelement("//div[div[label[text()='Primary/Backup']]]//select//option[@label='"+
				// CpePrimary_Backup +"']"));
				WebElement CpePrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select//option[@label='"
								+ CpePrimary_Backup + "']"));
				CpePrimaryBackup.click();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Primary/Backup" + " dropdown value selected as: " + CpePrimary_Backup);

				WebElement InterfaceDirection = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select"));
				InterfaceDirection.click();

				// click(getwebelement("//div[div[label[text()='Interface
				// Direction']]]//select"));
				waitForAjax();

				WebElement CpenterfaceDirection1 = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select//option[@label='"
								+ CpenterfaceDirection + "']"));
				CpenterfaceDirection1.click();

				// click(getwebelement("//div[div[label[text()='Interface
				// Direction']]]//select//option[@label='"+ CpenterfaceDirection
				// +"']"));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Interface Direction" + " dropdown value selected as: " + CpenterfaceDirection);
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Voice Line Reference"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						CpeVoiceline, "Voice Line Reference");

			}

			if (CPEBearerType.equalsIgnoreCase("Ethernet")) {

				addDropdownValues_commonMethod("Bearer Type",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bearer Type"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPEBearerType);
				// Thread.sleep(2000);
				waitforPagetobeenable();
				addDropdownValues_commonMethod("Speed",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Speed"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeSpeed);
				addDropdownValues_commonMethod("Encapsulation",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Encapsulation"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeEncapsulation);
				addDropdownValues_commonMethod("Duplex",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Duplex"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeDuplex);

				WebElement PrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select"));
				PrimaryBackup.click();
				// Thread.sleep(2000);

				WebElement CpePrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select//option[@label='"
								+ CpePrimary_Backup + "']"));

				CpePrimaryBackup.click();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Primary/Backup" + " dropdown value selected as: " + CpePrimary_Backup);
				WebElement InterfaceDirection = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select"));
				InterfaceDirection.click();

				// click(getwebelement("//div[div[label[text()='Interface
				// Direction']]]//select//option[@label='"+ CpenterfaceDirection
				// +"']"));

				WebElement CpenterfaceDirection1 = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select//option[@label='"
								+ CpenterfaceDirection + "']"));
				CpenterfaceDirection1.click();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Interface Direction" + " dropdown value selected as: " + CpenterfaceDirection);
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Voice Line Reference"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						CpeVoiceline, "Voice Line Reference");

			}
			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton, "Add button");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						CpeInterfaceAddRangeText, "Link");
			} else {
				addDropdownValues_commonMethod("Link",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPELink);
			}
			if (CpeIVManagement.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV management"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV management");

			}
			if (CpeIVBitCounter.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV 64 Bit Counter"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV 64 Bit Counter");

			}
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton2);
			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click("Add button", "AddInterfacebutton2");

				WebElement Remove = webDriver.findElement(By.xpath("//span[text()='Remove']"));
				Remove.click();

				// Thread.sleep(2000);
				click("Add button", "AddInterfacebutton2");
				// Thread.sleep(2000);
				sendKeys(CpeAddRange, "Address Range", "TextValueCommon");

				WebElement arrowbadgebadge = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[2]"));
				arrowbadgebadge.click();
				// Thread.sleep(2000);
				waitforPagetobeenable();
				// sendKeys(application, CpeSecondaryIp, "Secondary IP",
				// "TextValueCommon");
				// sendKeys(application, CpeNetmask, "Netmask",
				// "TextValueCommon");

			}
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SaveInterface);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SaveInterface, "Save button");
			// Thread.sleep(3000);
			waitforPagetobeenable();
			compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Interface successfully created.", "Add CPE Device");
			// Thread.sleep(2000);
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
		}
	}

	public void EditInterfaceCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String CPEBearerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEBandwidthE1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEBandwidthE3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEBandwidthSTM = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeClockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPELink = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeEncapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpePrimary_Backup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpenterfaceDirection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpeVoiceline = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeIVManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeIVBitCounter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeAddRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeSecondaryIp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeNetmask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeDuplex = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeInterfaceAddRangeDdn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpeInterfaceAddRangeText = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpeAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEGetAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeAddInterfaceButton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String InterfaceCNGReferance = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader)
					.replace("value", VPNSiteOrderHeader);
			ScrollIntoViewByString(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton,
			// "Action dropdown");
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,"View
			// ");

			waitforPagetobeenable();
			// scrolltoend();
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPEShowInterfaces, "Show Interface Link");
			waitForAjax();

			WebElement InterfaceRadio = webDriver
					.findElement(By.xpath("//div[contains(text(),'" + CpeInterface + "')]//parent::div//div//input"));
			InterfaceRadio.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing Interface radio button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButtonCPE,
					"Interface Action Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceEditLink, "Interface Edit Link");
			waitForAjax();
			waitforPagetobeenable();

			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Interface"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					CpeInterface, "Interface");
			// EnterTextValueCommon(application, CpeInterface, "Interface",
			// "TextValueCommon");
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Interface"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2);

			if (CPEGetAddress.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetAddressCPE, "Get Address button");
				// Thread.sleep(5000);
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AllocateSubnetButton,
						"Allocate Subnet button");
				// Thread.sleep(3000);
				waitforPagetobeenable();
				// addDropdownValues_commonMethod(application, "Interface
				// Address Range", "SelectValueDropdown",
				// CpeInterfaceAddRangeDdn, xml);
				WebElement arrowbadgebadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[1]"));
				arrowbadgebadgesecondary.click();

				waitforPagetobeenable();
				addDropdownValues_commonMethod("Address",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Address"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeAddress);

			} else {
				sendKeys(CpeInterfaceAddRangeText, "Interface Address Range", "TextValueCommon");
				WebElement arrowbadgebadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[1]"));
				arrowbadgebadgesecondary.click();
				waitforPagetobeenable();

			}

			// ScrolltoElementComm(
			// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1++,"Interface
			// Address Range");
			if (ServiceSubType.equalsIgnoreCase("IPVPN IPSec") || ServiceSubType.equalsIgnoreCase("IPVPN Connect")) {
				addDropdownValues_commonMethod("CNG Reference",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "CNG Reference"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						InterfaceCNGReferance);
			}
			if (!CPEBearerType.equalsIgnoreCase("Ethernet")) {

				addDropdownValues_commonMethod("Bearer Type",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bearer Type"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPEBearerType);
				// Thread.sleep(2000);
				waitforPagetobeenable();
				if (CPEBearerType.equalsIgnoreCase("E3") || CPEBearerType.equalsIgnoreCase("T3")) {
					addDropdownValues_commonMethod("Bandwidth",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bandwidth"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
							CPEBandwidthE3);
				}
				if (CPEBearerType.equalsIgnoreCase("STM-1")) {
					addDropdownValues_commonMethod("Bandwidth",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bandwidth"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
							CPEBandwidthSTM);
				}
				if (CPEBearerType.equalsIgnoreCase("E1")) {
					addDropdownValues_commonMethod("Bandwidth",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bandwidth"
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
							CPEBandwidthE1);
				}

				addDropdownValues_commonMethod("Encapsulation",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Encapsulation"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeEncapsulation);

				WebElement ClockSource = webDriver
						.findElement(By.xpath("//div[div[label[text()='Clock Source']]]//select"));
				ClockSource.click();
				waitForAjax();

				// click(getwebelement());

				WebElement CpeClockSource1 = webDriver.findElement(By.xpath(
						"//div[div[label[text()='Clock Source']]]//select//option[@label='" + CpeClockSource + "']"));
				CpeClockSource1.click();
				waitForAjax();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Clock Source" + " dropdown value selected as: " + CpeClockSource);

				// click(getwebelement("//div[div[label[text()='Primary/Backup']]]//select"));
				// waitForAjax();

				WebElement PrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select"));
				PrimaryBackup.click();
				waitForAjax();

				// click(getwebelement("//div[div[label[text()='Primary/Backup']]]//select//option[@label='"+
				// CpePrimary_Backup +"']"));
				WebElement CpePrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select//option[@label='"
								+ CpePrimary_Backup + "']"));
				CpePrimaryBackup.click();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Primary/Backup" + " dropdown value selected as: " + CpePrimary_Backup);

				WebElement InterfaceDirection = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select"));
				InterfaceDirection.click();

				// click(getwebelement("//div[div[label[text()='Interface
				// Direction']]]//select"));
				waitForAjax();

				WebElement CpenterfaceDirection1 = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select//option[@label='"
								+ CpenterfaceDirection + "']"));
				CpenterfaceDirection1.click();

				// click(getwebelement("//div[div[label[text()='Interface
				// Direction']]]//select//option[@label='"+ CpenterfaceDirection
				// +"']"));
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Interface Direction" + " dropdown value selected as: " + CpenterfaceDirection);
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Voice Line Reference"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						CpeVoiceline, "Voice Line Reference");
			}

			if (CPEBearerType.equalsIgnoreCase("Ethernet")) {

				addDropdownValues_commonMethod("Bearer Type",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Bearer Type"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPEBearerType);
				// Thread.sleep(2000);
				waitforPagetobeenable();
				addDropdownValues_commonMethod("Speed",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2 + "Speed"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeSpeed);
				addDropdownValues_commonMethod("Encapsulation",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Encapsulation"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeEncapsulation);
				addDropdownValues_commonMethod("Duplex",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Duplex"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeDuplex);

				WebElement PrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select"));
				PrimaryBackup.click();
				// Thread.sleep(2000);

				WebElement CpePrimaryBackup = webDriver
						.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select//option[@label='"
								+ CpePrimary_Backup + "']"));

				CpePrimaryBackup.click();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Primary/Backup" + " dropdown value selected as: " + CpePrimary_Backup);
				WebElement InterfaceDirection = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select"));
				InterfaceDirection.click();

				// click(getwebelement("//div[div[label[text()='Interface
				// Direction']]]//select//option[@label='"+ CpenterfaceDirection
				// +"']"));

				WebElement CpenterfaceDirection1 = webDriver
						.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select//option[@label='"
								+ CpenterfaceDirection + "']"));
				CpenterfaceDirection1.click();

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Interface Direction" + " dropdown value selected as: " + CpenterfaceDirection);
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Voice Line Reference"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						CpeVoiceline, "Voice Line Reference");

			}

			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton, "Add button");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						CpeInterfaceAddRangeText, "Link");

			} else {
				addDropdownValues_commonMethod("Link",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPELink);
			}
			if (CpeIVManagement.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV management"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV management");
			}
			if (CpeIVBitCounter.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV 64 Bit Counter"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV 64 Bit Counter");
			}
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton2);
			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click("Add button", "AddInterfacebutton2");

				WebElement Remove = webDriver.findElement(By.xpath("//span[text()='Remove']"));
				Remove.click();

				// Thread.sleep(2000);
				click("Add button", "AddInterfacebutton2");
				// Thread.sleep(2000);
				sendKeys(CpeAddRange, "Address Range", "TextValueCommon");

				WebElement arrowbadgebadge = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[2]"));
				arrowbadgebadge.click();
				// Thread.sleep(2000);
				waitforPagetobeenable();
				// sendKeys(application, CpeSecondaryIp, "Secondary IP",
				// "TextValueCommon");
				// sendKeys(application, CpeNetmask, "Netmask",
				// "TextValueCommon");

			}
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SaveInterface);
			click("Save button", "SaveInterface");
			// Thread.sleep(3000);
			waitforPagetobeenable();
			compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Interface successfully created.", "Add CPE Device");
			// Thread.sleep(2000);
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
		}
	}

	public void CPEDeviceConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String CPEBearerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEBandwidthE1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEBandwidthE3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEBandwidthSTM = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeClockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPELink = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeEncapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpePrimary_Backup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpenterfaceDirection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpeVoiceline = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeIVManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeIVManagement");
		String CpeIVBitCounter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeIVBitCounter");
		String CpeAddRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeAddRange");
		String CpeSecondaryIp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeSecondaryIp");
		String CpeNetmask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeNetmask");
		String CpeSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeSpeed");
		String CpeDuplex = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeDuplex");
		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeInterface");
		String CpeInterfaceAddRangeDdn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeInterfaceAddRangeDdn");
		String CpeInterfaceAddRangeText = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeInterfaceAddRangeText");
		String CpeAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeAddress");
		String CPEGetAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceGetAddress");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeAddInterfaceButton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeAddInterfaceButton");
		String InterfaceCNGReferance = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceCNGReferance");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"siteOrderNumber");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ManagementAdd_Re");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton,
			// "Action dropdown");
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,"View
			// ");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			// scrollToElement();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			System.out.println("Numberofuser=" + NoOfDevice);

			// Thread.sleep(3000);

			if (NoOfDevice < 5) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ConfigureCPE, "View CPE Device");
				// Thread.sleep(3000);
			} else if (NoOfDevice > 5) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//span[contains(text(),'" + VPNManagementAdd
						+ "')]//parent::div//parent::div//div//span[text()='Configure']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device View button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");

			}

			waitForAjax();
			waitforPagetobeenable();

			WebElement Configuration_header = webDriver.findElement(By.xpath("//div[text()='Configuration']"));
			scrollIntoView(Configuration_header);

			if (ServiceSubType.equalsIgnoreCase("IPVPN Plus")) {
				WebElement Generate = webDriver.findElement(By.xpath("//span[text()='Generate']"));
				Generate.click();
				waitForAjax();
				waitforPagetobeenable();
				Report.LogInfo("INFO", "Click on Generate Button", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Click on Generate Button");
				WebElement SaveConfiguration = webDriver.findElement(By.xpath("//span[text()='Save Configuration']"));
				SaveConfiguration.click();

				waitForAjax();
				waitforPagetobeenable();
				Report.LogInfo("INFO", "Clicked on Save Configuration", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Save Configuration");
				WebElement ExecuteConfiguration = webDriver
						.findElement(By.xpath("//span[text()='Execute Configuration']"));
				ExecuteConfiguration.click();
				waitForAjax();
				waitforPagetobeenable();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Execute Configuration");
			} else if (ServiceSubType.equalsIgnoreCase("IPVPN Connect")
					|| ServiceSubType.equalsIgnoreCase("IPVPN IPSec")) {
				addDropdownValues_commonMethod("Generate Configuration For",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1
								+ "Generate Configuration For"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						"Full Configuration");
				waitForAjax();
				WebElement Generate = webDriver.findElement(By.xpath("//span[text()='Generate']"));
				Generate.click();
				waitForAjax();
				waitforPagetobeenable();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Click on Generate Button");
				WebElement SaveConfiguration = webDriver.findElement(By.xpath("//span[text()='Save Configuration']"));
				SaveConfiguration.click();
				Thread.sleep(2000);
				waitforPagetobeenable();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Save Configuration");
				WebElement ExecuteConfiguration = webDriver
						.findElement(By.xpath("//span[text()='Execute Configuration']"));
				ExecuteConfiguration.click();
				Thread.sleep(2000);
				waitforPagetobeenable();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Execute Configuration");

			}

			WebElement Back = webDriver.findElement(By.xpath("//span[text()='Back']"));
			Back.click();
			Thread.sleep(2000);
			waitforPagetobeenable();

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
		}
	}

	public void SelectInterface(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNRouterId");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPNSiterderNum");
		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeInterface");

		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader)
					.replace("value", VPNSiteOrderHeader);
			ScrollIntoViewByString(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "View ");
			waitforPagetobeenable();

			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			System.out.println("Numberofuser=" + NoOfDevice);

			if (NoOfDevice == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectInterface, "Select Interface");
				Thread.sleep(3000);
			} else if (NoOfDevice >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='Select Interfaces']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing Select Interface button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");

			}
			waitforPagetobeenable();
			// scrolltoend();
			WebElement ClickInterface = webDriver
					.findElement(By.xpath("(//div[text()='" + CpeInterface + "']//parent::div//div//span//input)[1]"));
			ClickInterface.click();
			Thread.sleep(3000);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Routes_okButton, "ok button");
			Thread.sleep(2000);
			waitforPagetobeenable();
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
		}

	}

	public void DeleteInterfaceCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");

		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeInterface");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader)
					.replace("value", VPNSiteOrderHeader);
			ScrollIntoViewByString(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

			}
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "View ");

			waitforPagetobeenable();

			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPEShowInterfaces);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPEShowInterfaces, "Show Interface Link");
			waitForAjax();
			WebElement InterfaceRadio = webDriver
					.findElement(By.xpath("//div[contains(text(),'" + CpeInterface + "')]//parent::div//div//input"));
			InterfaceRadio.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing Interface radio button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButtonCPE,
					"Interface Action Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceDeleteLink, "Interface Edit Link");
			waitForAjax();
			waitforPagetobeenable();

			waitForAjax();
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
			waitForAjax();
			waitforPagetobeenable();
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Delete Interface");
			// compareText(application, "Delete Device", "SuccessMessageCom",
			// "Static Route successfully deleted.");
			waitForAjax();

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
		}
	}

	public boolean findPanelHeader(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String devicePanelName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"Provider_Equipment_(PE)");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");

		WebElement VPNSiteOrder_header = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
		// scrollToView(VPNSiteOrder_header);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
		List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
				"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
		int NoOfUsers = ExistingUsers.size();

		if (NoOfUsers == 1) {
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
					"VPN Order Radio button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
			waitForAjax();
		} else if (NoOfUsers >= 1) {
			WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
					+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
			AddedUser.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing VPN Site Order radio button");
			Report.LogInfo("INFO", "clicked on Existing VPN Site Order radio button", "PASS");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
			Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
		}

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

		// ClickCommon( "View", "AddVPNSiteCommonLink");
		waitforPagetobeenable();
		scrolltoend();

		WebElement el = null;
		boolean panelheader = false;
		try {
			// el=findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.devicePanelHeaders_InViewSiteOrderPage).replace("value",
			// devicePanelName);
			// panelheader=el.isDisplayed();

			if (isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.devicePanelHeaders_InViewSiteOrderPage1
					+ "Provider Equipment (PE)"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.devicePanelHeaders_InViewSiteOrderPage2)) {
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"Provider_Equipment_(PE)" + " panel is displaying under 'view VPN site order' page");
				Report.LogInfo("INFO", " 'Equipment' panel is displaying under 'view VPN site order' page", "INFO");
				panelheader = true;

			} else {
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"Provider_Equipment_(PE)" + "  panel is not displaying under 'view VPN site order' page");
				Report.LogInfo("INFO", " 'Equipment' panel is not displaying under 'view VPN site order' page", "INFO");
				panelheader = false;

			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO,
					"Provider_Equipment_(PE)" + " panel is not displaying under 'view VPN site order' page");
			Report.LogInfo("INFO", " 'Equipment' panel is not displaying under 'view VPN site order' page", "INFO");
			panelheader = false;

		}

		return panelheader;
	}

	public void SelectPEdevice_existingDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String existingDeviceName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPEdevice");
		scrolltoend();
		waitForAjax();

		ExtentTestManager.getTest().log(LogStatus.INFO, "Select existing PE device");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_adddevicelink,
				"PE Device Add Device link");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_adddevicelink, "PE Device Add Device link");
		waitForAjax();

		waitforPagetobeenable();

		addDropdownValues_PEdevice("Choose a device",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.chooseAdeviceDropdown, existingDeviceName);
		waitForAjax();

		scrolltoend();
		waitForAjax();
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.nextButton, "Next");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.nextButton, "Next");
		waitForAjax();

	}

	public void addDropdownValues_PEdevice(String labelname, String xpath, String expectedValueToAdd)
			throws InterruptedException {
		boolean availability = false;
		// List<String> ls = new ArrayList<String>();

		try {
			availability = findWebElement(xpath).isDisplayed();
			if (availability) {
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");

				if (expectedValueToAdd.equalsIgnoreCase("null")) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" No values selected under " + labelname + " dropdown");
					Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {

					webDriver.findElement(By.xpath("//div[label[text()='" + labelname + "']]//div[text()='�']"))
							.click();
					waitForAjax();

					webDriver.findElement(By.xpath("//div[label[text()='" + labelname + "']]//input"))
							.sendKeys(expectedValueToAdd);
					// waitForAjax();

					webDriver.findElement(By.xpath("(//span[contains(text(),'" + expectedValueToAdd + "')])[1]"))
							.click();
					waitForAjax();

					String actualValue = webDriver
							.findElement(By.xpath("//label[text()='" + labelname + "']/following-sibling::div//span"))
							.getText();
					ExtentTestManager.getTest().log(LogStatus.PASS,
							labelname + " dropdown value selected as: " + actualValue);
					Report.LogInfo("INFO", labelname + " dropdown value selected as: " + actualValue, "PASS");

				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
				Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
			}
		} catch (NoSuchElementException e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
		} catch (Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					" NOt able to perform selection under " + labelname + " dropdown");
			Report.LogInfo("INFO", " NO value selected under " + labelname + " dropdown", "FAIL");
		}
	}

	public void verifysuccessmessage(String expected) throws InterruptedException {

		waitforPagetobeenable();
		// scrollToTop();
		scrollUp();
		waitForAjax();
		try {

			// boolean
			// successMsg=findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serivceAlert).isDisplayed();
			boolean successMsg = isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serivceAlert);
			if (successMsg) {

				String alrtmsg = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AlertForServiceCreationSuccessMessage)
								.getText();

				if (expected.contains(alrtmsg)) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Message is verified. It is displaying as: " + alrtmsg);
					Report.LogInfo("INFO", "Message is verified. It is displaying as: " + alrtmsg, "PASS");

					// successScreenshot(application);

				} else if (expected.equals(alrtmsg)) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Message is verified. It is displaying as: " + alrtmsg);
					Report.LogInfo("INFO", "Message is verified. It is displaying as: " + alrtmsg, "PASS");
					// successScreenshot(application);

				} else {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg
									+ " .The Expected value is: " + expected);
					Report.LogInfo("INFO",
							"Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg, "FAIL");
					// failureScreenshot(application);
				}

			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");
				Report.LogInfo("INFO", " Success Message is not displaying", "FAIL");
				// failureScreenshot(application);
			}

		} catch (Exception e) {
			Report.LogInfo("INFO", "failure in fetching success message  ", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, expected + " Message is not displaying");
			Report.LogInfo("INFO", expected + " message is not getting dislpayed", "FAIL");
			// failureScreenshot(application);
		}
	}

	public void verifyValuesforProviderEqiupment() throws InterruptedException {
		waitForAjax();

		fetchValueFromViewPage("Name");
		fetchValueFromViewPage("Vendor/Model");
		fetchValueFromViewPage("Management Address");
		fetchValueFromViewPage("SNMP Version");
		// fetchValueFromViewPage("Snmprw");
		// fetchValueFromViewPage("Snmpro");
		fetchValueFromViewPage("Country");
		fetchValueFromViewPage("City");
		fetchValueFromViewPage("Site");
		fetchValueFromViewPage("Connectiviy Protocol");
		fetchValueFromViewPage("Premise");

	}

	public void fetchValueFromViewPage(String labelname) throws InterruptedException {

		String text = null;
		WebElement element = null;

		try {
			waitForAjax();
			// element =
			// findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPEdevicePage_fetchDeviceDetails).replace("value",labelname);
			// element =
			// webDriver.findElement(By.xpath(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPEdevicePage_fetchDeviceDetails1+labelname+APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPEdevicePage_fetchDeviceDetails2));
			element = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPEdevicePage_fetchDeviceDetails1 + labelname
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPEdevicePage_fetchDeviceDetails2);
			String emptyele = element.getText().toString();

			if (element == null) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " not found");
				Report.LogInfo("INFO", labelname + " not found", "FAIL");
			} else if (emptyele != null && emptyele.isEmpty()) {

				ExtentTestManager.getTest().log(LogStatus.PASS, "No value displaying under " + labelname);
				Report.LogInfo("INFO", "No value displaying under " + labelname, "PASS");
			}
			//
			else {
				element.getText();
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " value is displaying as: " + emptyele);
				Report.LogInfo("INFO", labelname + " value is displaying as: " + emptyele, "PASS");

			}

		} catch (Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
			Report.LogInfo("INFO", labelname + " field is not displaying", "FAIL");
		}
	}

	public boolean fetchdeviceInterface() throws InterruptedException, IOException {

		boolean clickLink = false;

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewDevicePage_ActionDropdown, "Action");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewDevicePage_ActionDropdown, "Action");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewDevicePage_fetchDeviceInterfaceLink,
				"Fetch Device Interfaces");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewDevicePage_fetchDeviceInterfaceLink,
				"Fetch Device Interfaces");
		waitForAjax();

		// verify success Message
		String expectedValue = "Fetch interfaces started successfully. Please check the sync status of this device. ";
		boolean successMessage = false;
		try {
			successMessage = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchDeviceInterace_SuccessMessage)
							.isDisplayed();
			String actualMessage = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchdeviceInterface_successtextMessage)
							.getText();
			if (successMessage) {

				if (actualMessage.isEmpty()) {
					Report.LogInfo("INFO", "No messages displays", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Success message is not displaying");
				} else if (actualMessage.contains(expectedValue)) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" After clicking on 'Fetch Device Interface' link, success Message is displaying as expected");
					Report.LogInfo("INFO",
							" After clicking on 'Fetch Device Interface' link, success Message is displaying as expected",
							"PASS");

					ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message displays as: " + actualMessage);
					Report.LogInfo("INFO", " Success Message displays as: " + actualMessage, "PASS");

					// click on the 'click here' link
					// click_commonMethod(application, "here",
					// "fetchDeviceInterface_hereLink", xml);
					clickLink = true;

				} else if (actualMessage.equalsIgnoreCase(expectedValue)) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" After clicking on 'Fetch Device Interface' link, success Message is displaying as expected");
					Report.LogInfo("INFO",
							" After clicking on 'Fetch Device Interface' link, success Message is displaying as expected",
							"PASS");

					ExtentTestManager.getTest().log(LogStatus.PASS, " Success Message displays as: " + actualMessage);
					Report.LogInfo("INFO", " Success Message displays as: " + actualMessage, "PASS");

					// click on the 'click here' link
					// click_commonMethod(application, "here",
					// "fetchDeviceInterface_hereLink", xml);
					clickLink = true;

				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"After clicking on 'Fetch Device Interface' link, message displays as " + actualMessage);
					Report.LogInfo("INFO",
							"After clicking on 'Fetch Device Interface' link, message displays as " + actualMessage,
							"PASS");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"After clicking on 'Fetch Device Interface' link, success message is not displaying");
			Report.LogInfo("INFO", "After clicking on 'Fetch Device Interface' link, success message is not displaying",
					"FAIL");

		}
		return clickLink;
	}

	public String fetchVendorModel_PE() throws InterruptedException, IOException {

		scrollUp();
		String vendorModelName = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPEdveice_fetchVendorName);

		return vendorModelName;
	}

	public String fetchManagementAddress_PE() throws InterruptedException, IOException {

		scrollUp();
		String managementAddress = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPEdevice_fetchManagementAddress);

		return managementAddress;
	}

	public void routerPanel_juniper(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String commandIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV4");

		String commandIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV6");

		String ipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management_Address");

		String vrfname_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");

		String vrfname_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv6");

		waitforPagetobeenable();
		scrolltoend();
		waitForAjax();

		// Command IPV4
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPV4_dropdown);
		addDropdownValues_commonMethod("Command IPV4",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPV4_dropdown, commandIPv4);

		hostnametextField_IPV4(testDataFile, sheetName, scriptNo, dataSetNo);

		vrfNametextField_IPV4(testDataFile, sheetName, scriptNo, dataSetNo);

		executeCommandAndFetchTheValue(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.executebutton_Ipv4);
	}

	public void hostnametextField_IPV4(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String command_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV4");
		String ipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management_Address");

		boolean IPV4availability = false;
		try {
			IPV4availability = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_hostnameTextfield).isDisplayed();

			if (IPV4availability) {

				// addtextFields_commonMethod("IP Address or Hostname",
				// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_hostnameTextfield,
				// ipAddress);
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_hostnameTextfield,
						"IP Address or Hostname");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_hostnameTextfield, ipAddress,
						"IP Address or Hostname");

			} else {
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4);
				Report.LogInfo("INFO",
						"'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4, "INFO");
			}
		} catch (Exception e) {
			e.printStackTrace();

			ExtentTestManager.getTest().log(LogStatus.INFO,
					"'Hostname or IpAddress' for 'IPv4' text field is not displaying for " + command_ipv4);
			Report.LogInfo("INFO",
					"'Hostname or IpAddress' for 'Ipv4' text field is not displaying for " + command_ipv4, "INFO");
		}
	}

	public void vrfNametextField_IPV4(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String command_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV4");

		String vrfname_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");

		boolean IPV4availability = false;

		if (command_ipv4.contains("vrf")) {
			try {
				IPV4availability = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_vrfnameTextField).isDisplayed();

				if (IPV4availability) {
					// addtextFields_commonMethod("Router Vrf Name",
					// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_vrfnameTextField,
					// vrfname_ipv4);
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_vrfnameTextField,
							"Router Vrf Name");
					sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv4_vrfnameTextField,
							vrfname_ipv4, "Router Vrf Name");

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4);
					Report.LogInfo("INFO", "'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4,
							"FAIL");
				}
			} catch (Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4);
				Report.LogInfo("INFO", "'VRF Name' for 'IPv4' text field is not displaying for " + command_ipv4,
						"FAIL");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"'VRF Name IPv4' text field is not displaying for " + command_ipv4);
			Report.LogInfo("INFO", "'VRF Name IPv4' text field is not displaying for " + command_ipv4 + " command",
					"PASS");
		}
	}

	public void executeCommandAndFetchTheValue(String executeButton) throws InterruptedException, IOException {
		verifyExists(executeButton, "Execute");
		click(executeButton, "Execute");

		boolean resultField = false;
		try {
			resultField = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.result_textArea)
					.isDisplayed();
			if (resultField) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Result' text field is displaying");
				Report.LogInfo("INFO", "'Result' text field is displaying", "PASS");

				String remarkvalue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.result_textArea)
						.getText();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value under 'Result' field displaying as " + remarkvalue);
				Report.LogInfo("INFO", "value under 'Result' field displaying as " + remarkvalue, "PASS");

			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Result' text field is not displaying");
				Report.LogInfo("INFO", "'Result' text field is not displaying", "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Result' text field is not displaying");
			Report.LogInfo("INFO", "'Result' text field is not displaying", "FAIL");
		}

	}

	public void routerPanel_Cisco(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String commandIPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV4");

		String commandIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV6");

		String ipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management Address");

		String vrfname_ipv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv4");

		String vrfname_ipv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrf_Ipv6");

		waitForAjax();

		// Command IPV4
		addDropdownValues_commonMethod("Command IPV4",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPV4_dropdown, commandIPv4);

		hostnametextField_IPV4(testDataFile, sheetName, scriptNo, dataSetNo);

		vrfNametextField_IPV4(testDataFile, sheetName, scriptNo, dataSetNo);

		executeCommandAndFetchTheValue(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.executebutton_Ipv4);

		// Commmand IPV6
		addDropdownValues_commonMethod("Command IPV6",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPV6_dropdown, commandIPv6);

		hostnametextField_IPV6(testDataFile, sheetName, scriptNo, dataSetNo);

		vrfNametextField_IPV6(testDataFile, sheetName, scriptNo, dataSetNo);

		executeCommandAndFetchTheValue(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.executebutton_IPv6);

	}

	public void hostnametextField_IPV6(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String commandIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV6");
		String ipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management Address");

		boolean IPV4availability = false;
		try {
			IPV4availability = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv6_hostnameTextfield).isDisplayed();

			if (IPV4availability) {

				// addtextFields_commonMethod("IP Address or Hostname",
				// "commandIPv6_hostnameTextfield", ipv6Address);
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv6_hostnameTextfield,
						"IP Address or Hostname");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv6_hostnameTextfield, ipAddress,
						"IP Address or Hostname");

			} else {
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6);
				Report.LogInfo("INFO",
						"'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6, "INFO");
			}
		} catch (Exception e) {
			e.printStackTrace();

			ExtentTestManager.getTest().log(LogStatus.INFO,
					"'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6);
			Report.LogInfo("INFO", "'Hostname or IpAddress' for 'IPV6' text field is not displaying for " + commandIPv6,
					"INFO");
		}
	}

	public void vrfNametextField_IPV6(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws IOException, InterruptedException {

		String commandIPV6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV6");
		String vrfname_IPV6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vrfname_IPV6");

		boolean IPV6availability = false;

		if (commandIPV6.equalsIgnoreCase("vrf")) {
			try {
				IPV6availability = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv6_vrfnameTextField).isDisplayed();

				if (IPV6availability) {
					// addtextFields_commonMethod("Router Vrf Name",
					// "commandIPv6_vrfnameTextField", vrfname_IPV6);
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv6_vrfnameTextField,
							"Router Vrf Name");
					sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.commandIPv6_vrfnameTextField,
							vrfname_IPV6, "Router Vrf Name");

				} else {
					ExtentTestManager.getTest().log(LogStatus.INFO,
							"'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6);
					Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6,
							"INFO");
				}
			} catch (Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6);
				Report.LogInfo("INFO", "'VRF Name' for 'IPv6' text field is not displaying for " + commandIPV6, "INFO");
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"'VRF Name IPv6' text field is not displaying for " + commandIPV6);
			Report.LogInfo("INFO", "'VRF Name IPv6' text field is not displaying for " + commandIPV6 + " command",
					"PASS");
		}

	}

	public void clickOnAddInterfaceLink() throws InterruptedException, IOException {

		// WebElement
		// routerToolPanelHeader=getwebelement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.routerToolPanelheader);
		// scrolltoview(routerToolPanelHeader);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.routerToolPanelheader);
		waitForAjax();

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfacePanel_actionDropdown, "Action");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfacePanel_actionDropdown, "Action");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterfaceOrLink, "Add Interface");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterfaceOrLink, "Add Interface");

		waitForAjax();

	}

	public void addInterface_DSlsiteorderSelected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {
		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv4Selection");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv4DropdownValue");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressIpv4Selection");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressIpv4Range");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"availableBlocksValue_IPv4");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_link");
		String bearerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterace_DSLsiteOrderSelected_BearerType");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_encapsulation");
		String slot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_slot");
		String port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_port");
		String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_vlanId");
		String configureInterfaceOnDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_configureInterfaceOnDevice");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_Ivmanagement");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressRangeIPv6selection");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv6DropdownValue");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressRangeIpv6selection");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressrangeIPv6");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"availableBlocksValue_IPv6");
		String PPPencapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cisco_pppEncapsulation");
		String VPI = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cisco_vpitextField");
		String VCI = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cisco_vciTextField");
		String DSldownstreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterace_cisco_dslDownStreamSpeed");
		String DSLupstreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterace_cisco_dslUpStreamSpeed");
		String MBS = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_MBSdropdown");

		boolean addressValue = false;
		waitForAjax();

		waitforPagetobeenable();

		scrolltoend();
		// click_commonMethod(application, "Execute and Save",
		// "addInterface_executeAndSaveButton" , xml);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
				"Add Interface");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton, "Add Interface");
		// Clickon(getwebelement(xml.getlocator("//locators/" + application +
		// "/addInterface_executeAndSaveButton")));
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
		Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");
		waitForAjax();
		waitforPagetobeenable();

		scrollUp();
		waitForAjax();

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vci_warningMessage, "VCI");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vpi_warningMessage, "VPI");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_warningMessage, "Slot");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_warningMessage, "Port");

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4,
					"EIP Allocation");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4, "EIP Allocation");

			addDropdownValues_commonMethod("Subnet Size",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv4);

			addDropdownValues_commonMethod("Available Blocks",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
					availableBlocksValue_IPv4);

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

			//EIPallocationSuccessMessage("successfully allocated");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			waitForAjax();
			waitforPagetobeenable();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");

			String interfaceAddressRange = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceAddressRangeDropdown_addInterface);

			if (interfaceAddressRange.isEmpty()) {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv4' dropdown");
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv4' dropdown", "FAIL");

			} else {

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

				waitForAjax();
				waitforPagetobeenable();

				addressValue = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface)
								.isDisplayed();
				if (addressValue) {

					selectEnableValueUnderAddressDropdown("Address", "FetchInterfaceDropdown_addInterface",
							existingAddressIPv4DropdownValue);

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv4' dropdown is not displaying");
					Report.LogInfo("INFO", "'Address_IPv4' dropdown is not displaying", "FAIL");
				}
			}
		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			// addtextFields_commonMethod( "Interface Address Range",
			// "interfaceAddressRange_textField" , newinterfaceAddressrange );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					"Interface Address Range");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					newinterfaceAddressrange, "Interface Address Range");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
					">>");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

			String interfaceValueIntextField = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_textField)
							.getAttribute("value");
			if (interfaceValueIntextField.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv4' text field");
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv4' text field", "FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField);
				Report.LogInfo("INFO",
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField, "PASS");
			}
		}
		// WebElement IPv4Netwrk = getwebelement(xml.getlocator("//locators/" +
		// application + "/PE_addInterface_networklabelName"));
		// scrolltoview(IPv4Netwrk);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_addInterface_networklabelName);

		// IPv6 Configuration
		if (serviceSubType.equalsIgnoreCase("IPVPN Plus") || serviceSubType.equalsIgnoreCase("IPVPN Access")) {
			if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

				// EIP Allocation
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");

				addDropdownValues_commonMethod("Subnet Size",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv6);

				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
						availableBlocksValue_IPv6);

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
						"Allocate Subnet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

				EIPallocationSuccessMessage("successfully allocated");

				// click_commonMethod( "x", "EIPallocation_xButton");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

				String interfaceAddressRange = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchInterfaceAddressRangeDropdown_addInterfaceIPv6)
								.getText();

				if (interfaceAddressRange.isEmpty()) {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values displaying under 'Interface Address Range_IPv6' dropdown");
					Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown",
							"FAIL");

				} else {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);
					Report.LogInfo("INFO",
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange,
							"PASS");

					// click_commonMethod( ">>" ,
					// "rightArrowButton_interfaceAddressRangeIPv6");
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");

					waitForAjax();
					waitforPagetobeenable();

					try {
						addressValue = findWebElement(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6)
										.isDisplayed();
						if (addressValue) {

							selectEnableValueUnderAddressDropdown("Address",
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6,
									existingAddressIPv6DropdownValue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"'Address_IPv6' dropdown is not displaying");
							Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv6' dropdown is not displaying");
						Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
					}
				}
			} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						"Interface Address Range");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						newinterfaceAddressrangeIPv6, "Interface Address Range");

				// addtextFields_commonMethod( "Interface Address Range",
				// "interfaceAddressRangeIPv6_textField" ,
				// newinterfaceAddressrangeIPv6 );

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRangeIPv6");
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");

				String interfaceValueIntextField = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressIPv6_textField)
								.getAttribute("value");
				if (interfaceValueIntextField.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField);
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField,
							"PASS");
				}
			}
			// WebElement IPv6Netwrk =
			// findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
			// scrolltoview(IPv6Netwrk);
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
		}

		// Encapsulation
		addDropdownValues_commonMethod("Encapsulation",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

		// DSL DownStream Speed
		addDropdownValues_commonMethod("DSL Downstream Speed",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DSLdownstreamSpead_Dropdown, DSldownstreamSpeed);

		// DSL Upstream Speed
		addDropdownValues_commonMethod("DSL Upstream Speed",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DSLupstreamSpeed_Dropdown, DSLupstreamSpeed);

		// PPP Encapsulation
		addDropdownValues_commonMethod("PPP Encapsulation",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PPPencapsulation_Dropdown, PPPencapsulation);

		// WebElement linkElement =
		// findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);
		// scrolltoview(linkElement);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);

		// Slot

		// addtextFields_commonMethod( "Slot", "slot_textField" , slot);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
				"Interface Address Range");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
				newinterfaceAddressrangeIPv6, "Interface Address Range");

		// Port
		// addtextFields_commonMethod( "Port", "port_textField", port);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, "port");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port, "port");

		// VPI
		// addtextFields_commonMethod( "VPI", "vpi_textField" , VPI );
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vpi_textField, "VPI");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vpi_textField, VPI, "VPI");

		// MBS
		addDropdownValues_commonMethod("MBS", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.mbsDropdown, MBS);

		// VCI
		// addtextFields_commonMethod( "VCI", "vci_textField" , VCI);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vci_textField, "VCI");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vci_textField, VCI, "VCI");

		// IV Management
		addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
				"IV management", IVmanagement);

		// compare Interface field value
		String expectedValueForInterface = "ATM" + slot + "/" + port + "." + VPI + "000" + VCI;
		compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
				expectedValueForInterface);

		// Interface
		String interfaceName = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField)
				.getAttribute("value");
		if (interfaceName.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Interface' text field");
			Report.LogInfo("INFO", "No values displaying under 'Interface' text field", "FAIL");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface Name is displaying as: " + interfaceName);
			Report.LogInfo("INFO", "Interface Name is displaying as: " + interfaceName, "PASS");
		}

		scrolltoend();
		waitForAjax();

		// perform Generate configuration
		boolean configurationpanel = false;
		configurationpanel = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
						.isDisplayed();
		if (configurationpanel) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
			Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			waitForAjax();

			scrolltoend();
			waitForAjax();

			String configurationvalues = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
			if (configurationvalues.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
				Report.LogInfo("INFO",
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
						"FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
				Report.LogInfo("INFO", "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues, "PASS");
			}

			waitForAjax();
			scrolltoend();
			// click_commonMethod( "Execute and Save",
			// "addInterface_executeAndSaveButton" );
			findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton)
					.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
			Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
		}
	}

	public void EIPallocationSuccessMessage(String expected) throws InterruptedException {

		// waitForpageload();
		waitforPagetobeenable();

		scrollUp();
		waitForAjax();
		try {

			boolean successMsg = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.succeMessageFor_EIpAllocation).isDisplayed();

			if (successMsg) {
				String alrtmsg = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.successTextMessage_EIPAllocation).getText();
				if (alrtmsg.contains(expected)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Message is verified. It is displaying as: " + alrtmsg);
					Report.LogInfo("INFO", "Message is verified. It is displaying as: " + alrtmsg, "PASS");

				} else if (expected.equals(alrtmsg)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Message is verified. It is displaying as: " + alrtmsg);
					Report.LogInfo("INFO", "Message is verified. It is displaying as: " + alrtmsg, "PASS");
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg
									+ " .The Expected value is: " + expected);
					Report.LogInfo("INFO",
							"Message is displaying and it gets mismatches. It is displaying as: " + alrtmsg, "FAIL");
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");
				Report.LogInfo("INFO", " Success Message is not displaying", "FAIL");
			}
			waitForAjax();

		} catch (Exception e) {
			// Log.info("failure in fetching success message");
			ExtentTestManager.getTest().log(LogStatus.FAIL, expected + " Message is not displaying");
			Report.LogInfo("INFO", expected + " message is not getting dislpayed", "FAIL");
			waitForAjax();
		}

	}

	public void selectEnableValueUnderAddressDropdown(String labelname, String xpath, String expectedValueToAdd) {

		List<String> ls = new ArrayList<String>();
		boolean availability = false;

		try {

			scrollDown(xpath);
			waitForAjax();
			((JavascriptExecutor) webDriver).executeScript("window.scrollBy(0,-150)");

			availability = findWebElement(xpath).isDisplayed();
			if (availability) {
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " dropdown is displaying");
				Report.LogInfo("INFO", labelname + " dropdown is displaying", "PASS");

				if (expectedValueToAdd.equalsIgnoreCase("null")) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" No values selected under " + labelname + " dropdown");
					Report.LogInfo("INFO", " No values selected under " + labelname + " dropdown", "PASS");
				} else {

					findWebElement("//div[label[text()='" + labelname + "']]//div[text()='�']").click();
					waitForAjax();

					// verify list of values inside dropdown
					List<WebElement> listofvalues = webDriver
							.findElements(By.xpath("//div[@class='sc-bxivhb kqVrwh']"));

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" List of values inside " + labelname + " dropdown is:  ");
					Report.LogInfo("INFO", " List of values inside " + labelname + "dropdown is:  ", "PASS");

					for (WebElement valuetypes : listofvalues) {
						// System.out.println("service sub types : " +
						// valuetypes.getText());
						ls.add(valuetypes.getText());
					}

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"list of values inside " + labelname + " dropdown is: " + ls);
					Report.LogInfo("INFO", "list of values inside " + labelname + " dropdown is: " + ls, "PASS");

					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectFirstValueUnderAddressDropdown,"selectFirstValueUnderAddressDropdown");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectFirstValueUnderAddressDropdown,"selectFirstValueUnderAddressDropdown");
					waitForAjax();

					String actualValue = findWebElement(
							"//label[text()='" + labelname + "']/following-sibling::div//span").getText();
					ExtentTestManager.getTest().log(LogStatus.PASS,
							labelname + " dropdown value selected as: " + actualValue);
					Report.LogInfo("INFO", labelname + " dropdown value selected as: " + actualValue, "PASS");
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
				Report.LogInfo("INFO", labelname + " is not displaying", "AIL");
			}
		} catch (NoSuchElementException e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " is not displaying");
			Report.LogInfo("INFO", labelname + " is not displaying", "FAIL");
		} catch (Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					" NOt able to perform selection under " + labelname + " dropdown");
			Report.LogInfo("INFO", " NO value selected under " + labelname + " dropdown", "FAIL");
		}
	}

	public void clickOnAddMultilinkLink() throws InterruptedException, IOException {

		// WebElement
		// routerToolPanelHeader=getwebelement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.routerToolPanelheader);
		waitforPagetobeenable();
		waitForAjax();
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.routerToolPanelheader);

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfacePanel_actionDropdown, "Action");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfacePanel_actionDropdown, "Action");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkLink, "Add Multilink");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkLink, "Add Multilink");
	}

	public void addMultilink_DSLsiteOrderselected(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String configureInterfaceOnDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_configureInterfaceOnDevice");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_Link");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_Encapsulation");
		String PPPencapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_DSLsiteOrderSelected_PPPencapsulation");
		String DSldownstreamSpeedPCR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_DSLsiteOrderSelected_DSLdownstreamPCR");
		String DSlupstreamSpeedSCR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_DSLsiteOrderSelected_DSLupstreamSCR");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_Ivmanagement");
		String unitID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_UnitID");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilik_availableBlocksValue_IPv4");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newinterfaceAddressrangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_availableBlocksValue_IPv6");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressIPv6DropdownValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newinterfaceAddressrangeIPv6");

		waitforPagetobeenable();

		scrollUp();
		waitForAjax();

		boolean addressValue = false;

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4,
					"EIP Allocation");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4, "EIP Allocation");

			addDropdownValues_commonMethod("Subnet Size",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv4);

			addDropdownValues_commonMethod("Available Blocks",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
					availableBlocksValue_IPv4);

			// click_commonMethod( "Allocate Subnet", "allocateSubnetButton" );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

			EIPallocationSuccessMessage("successfully allocated");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

			waitForAjax();

			waitforPagetobeenable();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");

			String interfaceAddressRange = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceAddressRangeDropdown_addMultilink);

			if (interfaceAddressRange.isEmpty()) {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv4' dropdown");
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv4' dropdown", "FAIL");

			} else {

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

				addressValue = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface)
								.isDisplayed();
				if (addressValue) {

					selectEnableValueUnderAddressDropdown("Address",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface,
							existingAddressIPv4DropdownValue);

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv4' dropdown is not displaying");
					Report.LogInfo("INFO", "'Address_IPv4' dropdown is not displaying", "FAIL");
				}
			}
		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					"Interface Address Range");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					newinterfaceAddressrange, "Interface Address Range");
			// addtextFields_commonMethod( "Interface Address Range",
			// "interfaceAddressRange_textField" , newinterfaceAddressrange );

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
					">>");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

			String interfaceValueIntextField = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_textField)
							.getAttribute("value");
			if (interfaceValueIntextField.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv4' text field");
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv4' text field", "FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField);
				Report.LogInfo("INFO",
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField, "PASS");
			}
		}
		WebElement IPv4Netwrk = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_addInterface_networklabelName);
		scrolltoview(IPv4Netwrk);

		// IPv6 Configuration
		if (serviceSubType.equalsIgnoreCase("IPVPN Access") || serviceSubType.equalsIgnoreCase("IPVPN Plus")) {
			if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

				// EIP Allocation
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");

				addDropdownValues_commonMethod("Subnet Size",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv6);

				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
						availableBlocksValue_IPv6);

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
						"Allocate Subnet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

				// click_commonMethod( "Allocate Subnet", "allocateSubnetButton"
				// );

				EIPallocationSuccessMessage("successfully allocated");

				// click_commonMethod( "x", "EIPallocation_xButton");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

				String interfaceAddressRange = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchInterfaceAddressRangeDropdown_addMultilinkIPv6);

				if (interfaceAddressRange.isEmpty()) {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values displaying under 'Interface Address Range_IPv6' dropdown");
					Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown",
							"FAIL");

				} else {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);
					Report.LogInfo("INFO",
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange,
							"PASS");

					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");

					try {
						addressValue = findWebElement(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6)
										.isDisplayed();
						if (addressValue) {

							selectEnableValueUnderAddressDropdown("Address",
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6,
									existingAddressIPv6DropdownValue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"'Address_IPv6' dropdown is not displaying");
							Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv6' dropdown is not displaying");
						Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
					}
				}
			} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

				// addtextFields_commonMethod( "Interface Address Range",
				// "interfaceAddressRangeIPv6_textField" ,
				// newinterfaceAddressrangeIPv6 );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						"Interface Address Range");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						newinterfaceAddressrangeIPv6, "Interface Address Range");

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRangeIPv6");
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");

				String interfaceValueIntextField = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressIPv6_textField)
								.getAttribute("value");
				if (interfaceValueIntextField.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField);
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField,
							"PASS");
				}

			}
			WebElement IPv6Netwrk = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
			scrolltoview(IPv6Netwrk);
		}

		// Link Text Field
		// addtextFields_commonMethod( "Link", "Link_textField" , link);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, "Link");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, link, "");

		// Encapsulation
		addDropdownValues_commonMethod("Encapsulation",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

		// PPP Encapsulation
		addDropdownValues_commonMethod("PPP Encapsulation",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PPPencapsulation_Dropdown, PPPencapsulation);

		// DSL Downstream (PCR)
		addDropdownValues_commonMethod("DSL Downstream Speed (PCR)",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_DSLdownStreamSpeeedPCR,
				DSldownstreamSpeedPCR);

		// DSL upStream (SCR)
		addDropdownValues_commonMethod("DSL Upstream Speed (SCR)",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_DSLupstreamSpeedSCR,
				DSlupstreamSpeedSCR);

		// IV Management
		addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
				"IV management", IVmanagement);

		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying column Names under 'multilink Bearer' table");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_multilinkTableHeader,
				"MultiLinkBearerTable"); // Multilink Bearer Table Header Name
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_checkToAddColumnName,
				"'Check to Add to multilink' column name"); // Check To Add To
															// Multilink column
															// ame
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_interfaceColumnName,
				"'Interface' column Name"); // 'Interface' column Name
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_linkOrcircuitColumnName,
				"'Link/Circuit Id' column Name"); // Link / Circuit Id column
													// Name
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_bearerTypeColumnName,
				"'Bearer Type' column Name"); // Bearer Type Column Name
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_vlanIDcolumnName,
				"'VLAN Id' column Name"); // VLAN Id column name

		//
		//// Compare Interface Name
		// String interfaceName_multilink = "lsq-" + slot + "/" + pic + "/" +
		// port + "." + unitID;
		// compareText( "Interface", "Interface_textField",
		// interfaceName_multilink );

		scrolltoend();
		waitForAjax();

		// Generate Configuration
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Generate Configuration' under 'Add Multilink' Page");

		// perform Generate configuration
		boolean configurationpanel = false;
		configurationpanel = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
						.isDisplayed();
		if (configurationpanel) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
			Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			waitForAjax();

			scrolltoend();
			waitForAjax();

			String configurationvalues = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
			if (configurationvalues.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
				Report.LogInfo("INFO",
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
						"FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
				Report.LogInfo("INFO", "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues, "PASS");
			}

			waitForAjax();
			scrolltoend();
			// click_commonMethod( "Execute and Save",
			// "addInterface_executeAndSaveButton" );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
					"Execute And Save Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
					"Execute And Save Button");

			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
			Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
		}
	}

	public String successmessageForInterfaceOrMultilinkCreation(String expected) throws InterruptedException {

		//waitForAjax();
		waitforPagetobeenable();

		scrollUp();
		String creation = "No";

		try {

			// boolean
			// successMsg=findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serivceAlert).isDisplayed();
			boolean successMsg = isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.serivceAlert);
			if (successMsg) {
				String alrtmsg = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AlertForServiceCreationSuccessMessage)
								.getText();
				if (alrtmsg.contains(expected)) {
					creation = "Yes";

				} else if (expected.equalsIgnoreCase(alrtmsg)) {
					creation = "Yes";
				} else {
					creation = "No";
				}
			} else {
				creation = "No";
			}
			waitForAjax();

		} catch (Exception e) {
			e.printStackTrace();
			creation = "No";
		}

		return creation;

	}

	public void clickOnAddLoopback() throws InterruptedException, IOException {

		WebElement routerToolPanelHeader = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.routerToolPanelheader);
		scrolltoview(routerToolPanelHeader);
		waitForAjax();

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfacePanel_actionDropdown, "Action");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfacePanel_actionDropdown, "Action");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addLoopBackLink, "Add Loopback");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addLoopBackLink, "Add Loopback");
		waitForAjax();
	}

	public String addInterface_Juniper(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
			

		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv4Selection");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv4DropdownValue");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServnewAddressIpv4SelectioniceSubType");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressIpv4Range");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"availableBlocksValue_IPv4");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_link");
		String bearerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_Juniper_bearerType");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_juniper_encapsulation");
		String slot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_slot");
		String port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_port");
		String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_vlanId");
		String unitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_unitId");
		String pic = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_pic");
		String STM1number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterace_STM1number");
		String bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_bandwidth");
		String cardType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cardType");
		String frameType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_frameType");
		String clockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_clockSource");
		String timeSlot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_timeSlot");
		String configureInterfaceOnDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_configureInterfaceOnDevice");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_Ivmanagement");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressRangeIPv6selection");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv6DropdownValue");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressRangeIpv6selection");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressrangeIPv6");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"availableBlocksValue_IPv6");
		String bearerNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_bearerNumber");

		boolean addressValue = false;
		waitforPagetobeenable();

		scrollUp();
		// Configure Interface on Device
		addCheckbox_commonMethod(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configureInterfaceOnDevice_checkbox,
				"Configure Interface on Device", configureInterfaceOnDevice);

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			verifyExists("EIP Allocation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

			addDropdownValues_commonMethod("Subnet Size",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv4);

			addDropdownValues_commonMethod("Available Blocks",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
					availableBlocksValue_IPv4);

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

			EIPallocationSuccessMessage("successfully allocated");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

			waitforPagetobeenable();

			waitforPagetobeenable();
			// click_commonMethod( "Get Address", "getAddress_IPv4");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");

			String interfaceAddressRange = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceAddressRangeDropdown_addInterface);

			if (interfaceAddressRange.isEmpty()) {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv4' dropdown");
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv4' dropdown", "FAIL");

			} else {

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

				waitforPagetobeenable();
				waitforPagetobeenable();

				addressValue = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface)
								.isDisplayed();
				if (addressValue) {

					selectEnableValueUnderAddressDropdown("Address",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface,
							existingAddressIPv4DropdownValue);

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv4' dropdown is not displaying");
					Report.LogInfo("INFO", "'Address_IPv4' dropdown is not displaying", "FAIL");
				}
			}
		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					"Interface Address Range");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					newinterfaceAddressrange, "Interface Address Range");

			// addtextFields_commonMethod( "Interface Address Range",
			// "interfaceAddressRange_textField" , newinterfaceAddressrange );

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,"rightArrowButton_interfaceAddressRange");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,"rightArrowButton_interfaceAddressRange");

			String interfaceValueIntextField = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_textField)
							.getAttribute("value");
			if (interfaceValueIntextField.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv4' text field");
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv4' text field", "FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField);
				Report.LogInfo("INFO",
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField, "PASS");
			}
		}

		WebElement IPv4Netwrk = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_addInterface_networklabelName);
		scrolltoview(IPv4Netwrk);

		// IPv6 Configuration
		if (serviceSubType.equalsIgnoreCase("IPVPN Plus") || serviceSubType.equalsIgnoreCase("IPVPN Access")) {
			if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

				// EIP Allocation
				// click_commonMethod( "EIP Allocation",
				// "EIPallocation_addInterfaceIpv6" );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");

				addDropdownValues_commonMethod("Subnet Size",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv6);

				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
						availableBlocksValue_IPv6);

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
						"Allocate Subnet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

				// click_commonMethod( "Allocate Subnet", "allocateSubnetButton"
				// );

				EIPallocationSuccessMessage("successfully allocated");

				// click_commonMethod( "x", "EIPallocation_xButton");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

				String interfaceAddressRange = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchInterfaceAddressRangeDropdown_addInterfaceIPv6)
								.getText();

				if (interfaceAddressRange.isEmpty()) {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values displaying under 'Interface Address Range_IPv6' dropdown");
					Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown",
							"FAIL");

				} else {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);
					Report.LogInfo("INFO",
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange,
							"PASS");

					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");

					waitforPagetobeenable();
					waitforPagetobeenable();

					try {
						addressValue = findWebElement(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6)
										.isDisplayed();
						if (addressValue) {

							selectEnableValueUnderAddressDropdown("Address",
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6,
									existingAddressIPv6DropdownValue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"'Address_IPv6' dropdown is not displaying");
							Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv6' dropdown is not displaying");
						Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
					}

				}

			} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

				// addtextFields_commonMethod( "Interface Address Range",
				// "interfaceAddressRangeIPv6_textField" ,
				// newinterfaceAddressrangeIPv6 );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						"Interface Address Range");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						newinterfaceAddressrangeIPv6, "Interface Address Range");

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRangeIPv6");
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");

				String interfaceValueIntextField = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressIPv6_textField)
								.getAttribute("value");
				if (interfaceValueIntextField.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField);
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField,
							"PASS");
				}
			}
			WebElement IPv6Netwrk = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
			scrolltoview(IPv6Netwrk);

		}

		// Link Text Field
		// addtextFields_commonMethod( "Link", "Link_textField" , link);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, "Link");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, link, "Link");

		// Bearer Type Dropdown
		addDropdownValues_commonMethod("Bearer Type",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerType_Dropdown, bearerType);

		if (bearerType.equals("10Gigabit Ethernet")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"xe-//.0"); // compare Interface default value
			WebElement linkElement = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);
			scrolltoview(linkElement);
			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			// addtextFields_commonMethod( "Slot", "slot_textField", slot);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, "Slot");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot, "Slot");

			// Port
			// addtextFields_commonMethod( "Port", "port_textField" , port );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, "Port");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port, "Port");

			// Unit Id
			edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
					unitId);

			// Pic
			// addtextFields_commonMethod( "Pic", pic_textField, pic );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, "Pic");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic, "Pic");

			// IV Management
			addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
					"IV management", IVmanagement);

			String InterfaceName_10Gig = "xe-" + slot + "/" + pic + "/" + port + "." + unitId;
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					InterfaceName_10Gig);

		} else if (bearerType.equals("E1")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// Framing Type
			addDropdownValues_commonMethod("Framing Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.framingType_Dropdown, frameType);

			if (frameType.equals("no-crc4")) {

				// Time Slot
				// addtextFields_commonMethod( "Timeslot",
				// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField,
				// timeSlot);
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, "Timeslot");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, timeSlot, "Timeslot");

			} else if (frameType.equals("crc4")) {

				// Time Slot
				// addtextFields_commonMethod( "Timeslot", "timeSlot_textField",
				// timeSlot);
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, "Timeslot");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, timeSlot, "Timeslot");

			} else if (frameType.equals("g704")) {

				// Time Slot
				// addtextFields_commonMethod( "Timeslot", "timeSlot_textField",
				// timeSlot);
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, "Timeslot");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, timeSlot, "Timeslot");

			} else if (frameType.equals("unframed")) {

				System.out.println("No additonal fields");
			}

			// Clock Source
			addDropdownValues_commonMethod("Clock Source",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clockSource_Dropdown, clockSource);

			// STM1 Number
			// addtextFields_commonMethod( "STM1 Number", "stm1Number_textField"
			// , STM1number);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, "STM1 Numbe");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number,
					"STM1 Numbe");

			// Bearer No
			// addtextFields_commonMethod( "Bearer No",
			// "bearerNumber_addtextField", bearerNumber);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, "Bearer No");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, bearerNumber,
					"Bearer No");

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"e1-//::.0");
			WebElement linkElement = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);
			scrolltoview(linkElement);
			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			// addtextFields_commonMethod( "Slot", "slot_textField", slot);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, "Slot");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot, "Slot");

			// Port
			// addtextFields_commonMethod( "Port", "port_textField" , port );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, "Port");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port, "Port");

			// Unit Id
			// edittextFields_commonMethod( "Unit ID", "unitId_textField" ,
			// unitId);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, "UnitId");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, unitId, "UnitId");

			// Pic
			// addtextFields_commonMethod( "Pic", "pic_textField" , pic );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, "Pic");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic, "Pic");

			// IV Management
			addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
					"IV management", IVmanagement);

			// compare Interface value
			String interfaceName_E1 = "e1-" + slot + "/" + pic + "/" + port + ":" + STM1number + ":." + unitId;
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					interfaceName_E1);

		} else if (bearerType.equals("E3")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// STM1 Number
			// addtextFields_commonMethod( "STM1 Number", "stm1Number_textField"
			// , STM1number);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, "STM1 Numbe");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number,
					"STM1 Numbe");

			// Bearer No
			// addtextFields_commonMethod( "Bearer No",
			// "bearerNumber_addtextField", bearerNumber);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, "Bearer No");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, bearerNumber,
					"Bearer No");

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"e3-//::.0");
			WebElement linkElement = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);
			scrolltoview(linkElement);
			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			// addtextFields_commonMethod( "Slot", "slot_textField", slot);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, "Slot");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot, "Slot");

			// Port
			// addtextFields_commonMethod( "Port", "port_textField" , port );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, "Port");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port, "Port");

			// Unit Id
			// edittextFields_commonMethod( "Unit ID", "unitId_textField" ,
			// unitId);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, "UnitId");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, unitId, "UnitId");

			// Pic
			// addtextFields_commonMethod( "Pic", "pic_textField" , pic );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, "Pic");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic, "Pic");

			// IV Management
			addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
					"IV management", IVmanagement);

			// compare Interface Name
			String interfaceName_E3 = "e3-" + slot + "/" + pic + "/" + port + ":" + STM1number + ":." + unitId;
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					interfaceName_E3);

		} else if (bearerType.equals("Gigabit Ethernet")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			// addDropdownValues_commonMethod( "Card Type", "cardType_Dropdown",
			// cardType);

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"ge-//.0");
			WebElement linkElement = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);
			scrolltoview(linkElement);
			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			// addtextFields_commonMethod( "Slot", "slot_textField", slot);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, "Slot");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot, "Slot");

			// Port
			// addtextFields_commonMethod( "Port", "port_textField" , port );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, "Port");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port, "Port");

			// Unit Id
			// edittextFields_commonMethod( "Unit ID", "unitId_textField" ,
			// unitId);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, "UnitId");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, unitId, "UnitId");

			// Pic
			// addtextFields_commonMethod( "Pic", "pic_textField" , pic );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, "Pic");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic, "Pic");

			// IV Management
			addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
					"IV management", IVmanagement);

			// Compare Interface Name
			String interfaceName_GigaBit = "ge-" + slot + "/" + pic + "/" + port + "." + unitId;
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					interfaceName_GigaBit);

		} else if (bearerType.equals("STM-1")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// STM1 Number
			// addtextFields_commonMethod( "STM1 Number", "stm1Number_textField"
			// , STM1number);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, "STM1 Number");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number,
					"STM1 Number");

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"so-//:.0");
			WebElement linkElement = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);
			scrolltoview(linkElement);
			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			// addtextFields_commonMethod( "Slot", "slot_textField", slot);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, "Slot");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot, "Slot");

			// Port
			// addtextFields_commonMethod( "Port", "port_textField" , port );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, "Port");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port, "Port");

			// Unit Id
			// edittextFields_commonMethod( "Unit ID", "unitId_textField" ,
			// unitId);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, "UnitId");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, unitId, "UnitId");

			// Pic
			// addtextFields_commonMethod( "Pic", "pic_textField" , pic );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, "Pic");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic, "Pic");

			// IV Management
			addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
					"IV management", IVmanagement);

			// Compare Interface Name
			String interfaceName_STM1Number = "so-" + slot + "/" + pic + "/" + port + ":" + STM1number + "." + unitId;
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					interfaceName_STM1Number);

		} else if (bearerType.equals("T3")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// STM1 Number
			// addtextFields_commonMethod( "STM1 Number", "stm1Number_textField"
			// , STM1number);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, "STM1 Number");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number,
					"STM1 Number");

			// Bearer No
			// addtextFields_commonMethod( "Bearer No",
			// "bearerNumber_addtextField", bearerNumber);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, "Bearer No");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, bearerNumber,
					"Bearer No");

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"t3-//::.0");
			WebElement linkElement = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField);
			scrolltoview(linkElement);
			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			// addtextFields_commonMethod( "Slot", "slot_textField", slot);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, "Slot");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot, "Slot");

			// Port
			// addtextFields_commonMethod( "Port", "port_textField" , port );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, "Port");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port, "Port");

			// Unit Id
			// edittextFields_commonMethod( "Unit ID",
			// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField
			// , unitId);
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, "UnitId");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, unitId, "UnitId");

			// Pic
			// addtextFields_commonMethod( "Pic", "pic_textField" , pic );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, "Pic");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic, "Pic");

			// IV Management
			addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
					"IV management", IVmanagement);

			// Compare Interface Name
			String interfaceName_T3 = "t3-" + slot + "/" + pic + "/" + port + ":" + STM1number + "::." + unitId;
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					interfaceName_T3);
		}

		// Interface
		String interfaceName = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField)
				.getAttribute("value");
		if (interfaceName.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Interface' text field");
			Report.LogInfo("INFO", "No values displaying under 'Interface' text field", "FAIL");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface Name is displaying as: " + interfaceName);
			Report.LogInfo("INFO", "Interface Name is displaying as: " + interfaceName, "PASS");
		}

		scrollUp();
		waitforPagetobeenable();
		boolean configureInterfaceSelection = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configuraInterfaceCheckboxSelection).isSelected();

		scrolltoend();
		waitforPagetobeenable();

		if (configureInterfaceSelection) {

			// perform Generate configuration
			boolean configurationpanel = false;
			configurationpanel = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
							.isDisplayed();
			if (configurationpanel) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
				Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
				waitforPagetobeenable();

				scrolltoend();

				String configurationvalues = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
				if (configurationvalues.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
							"FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
							+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, "
									+ "under 'Configuration' textbox values displaying as: " + configurationvalues,
							"PASS");
				}
				waitforPagetobeenable();
				// click_commonMethod( "Execute and Save",
				// "addInterface_executeAndSaveButton" );
				// Clickon(getwebelement(xml.getlocator("//locators/" +
				// application + "/addInterface_executeAndSaveButton")));
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Add Interface Execute And Save Button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Add Interface Execute And Save Button");
						
						waitForAjax();

				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
				Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
				Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
			}
		} else {

			scrolltoend();
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_okButton,
					"Add Interface Ok Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_okButton, "Add Interface Ok Button");
			waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'OK' button");

			// click_commonMethod( "OK" , "" );
		}
		return interfaceName;
	}

	public void clickOnBreadCrump(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String breadCrumpLink = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPEdevice");

		waitForAjax();
		waitforPagetobeenable();

		scrollUp();
		waitForAjax();
		WebElement breadcrumb = null;
		// String breadcrumb=null;
		try {
			if (isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump1 + breadCrumpLink
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump2)) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump1 + breadCrumpLink
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump2, "Breadcrump");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump1 + breadCrumpLink
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.breadcrump2, "Breadcrump");
				waitForAjax();
			} else {
				Report.LogInfo("INFO", "Breadcrumb is not displaying for the element " + breadcrumb, "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO,
						"Breadcrumb is not displaying for the element " + breadcrumb);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Report.LogInfo("INFO", "Breadcrumb is not displaying for the element " + breadcrumb, "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO,
					"Breadcrumb is not displaying for the element " + breadcrumb);
		}
	}

	public String addMultilink(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String configureInterfaceOnDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_configureInterfaceOnDevice");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_Link");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_Encapsulation");
		String slot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_Slot");
		String port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_Port");
		String pic = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_Pic");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_Ivmanagement");
		String unitID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_UnitID");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilik_availableBlocksValue_IPv4");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newinterfaceAddressrangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_availableBlocksValue_IPv6");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressIPv6DropdownValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newinterfaceAddressrangeIPv6");

		waitforPagetobeenable();
		waitforPagetobeenable();

		scrollUp();
		waitforPagetobeenable();

		boolean addressValue = false;

		// Configure Interface on Device
		addCheckbox_commonMethod(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configureInterfaceOnDevice_checkbox,
				"Configure Interface on Device", configureInterfaceOnDevice);

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {
			/*
			// EIP Allocation
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4,
					"EIP Allocation");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4, "EIP Allocation");

			addDropdownValues_commonMethod("Subnet Size",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv4);

			addDropdownValues_commonMethod("Available Blocks",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
					availableBlocksValue_IPv4);

			// click_commonMethod( "Allocate Subnet", "allocateSubnetButton" );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocation Subnet");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocation Subnet");

			EIPallocationSuccessMessage("successfully allocated");

			// click_commonMethod( "x", "EIPallocation_xButton");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			waitforPagetobeenable();
			*/
			waitforPagetobeenable();
			// click_commonMethod( "Get Address", "getAddress_IPv4");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");

			String interfaceAddressRange = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceAddressRangeDropdown_addMultilink);

			if (interfaceAddressRange.isEmpty()) {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv4' dropdown");
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv4' dropdown", "FAIL");

			} else {

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRange");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

				addressValue = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface)
								.isDisplayed();
				if (addressValue) {

					selectEnableValueUnderAddressDropdown("Address",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface,
							existingAddressIPv4DropdownValue);

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv4' dropdown is not displaying");
					Report.LogInfo("INFO", "'Address_IPv4' dropdown is not displaying", "FAIL");
				}
			}
		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			// addtextFields_commonMethod( "Interface Address Range",
			// "interfaceAddressRange_textField" , newinterfaceAddressrange );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					"Interface Address Range");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					newinterfaceAddressrange, "Interface Address Range");

			// click_commonMethod( ">>" ,
			// "rightArrowButton_interfaceAddressRange");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
					">>");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

			String interfaceValueIntextField = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_textField)
							.getAttribute("value");
			if (interfaceValueIntextField.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv4' text field");
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv4' text field", "FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField);
				Report.LogInfo("INFO",
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField, "PASS");
			}
		}

		WebElement IPv4Netwrk = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_addInterface_networklabelName);
		scrolltoview(IPv4Netwrk);

		// IPv6 Configuration
		if (serviceSubType.equalsIgnoreCase("IPVPN Access") || serviceSubType.equalsIgnoreCase("IPVPN Plus")) {
			if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

				// EIP Allocation
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");

				addDropdownValues_commonMethod("Subnet Size",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv6);

				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
						availableBlocksValue_IPv6);

				// click_commonMethod( "Allocate Subnet", "allocateSubnetButton"
				// );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
						"Allocate Subnet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

				EIPallocationSuccessMessage("successfully allocated");

				// click_commonMethod( "x", "EIPallocation_xButton");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

				String interfaceAddressRange = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchInterfaceAddressRangeDropdown_addMultilinkIPv6);

				if (interfaceAddressRange.isEmpty()) {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values displaying under 'Interface Address Range_IPv6' dropdown");
					Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown",
							"FAIL");

				} else {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);
					Report.LogInfo("INFO",
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange,
							"PASS");

					// click_commonMethod( ">>" ,
					// "rightArrowButton_interfaceAddressRangeIPv6");
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");

					try {
						addressValue = findWebElement(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6)
										.isDisplayed();
						if (addressValue) {

							selectEnableValueUnderAddressDropdown("Address",
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6,
									existingAddressIPv6DropdownValue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"'Address_IPv6' dropdown is not displaying");
							Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv6' dropdown is not displaying");
						Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
					}
				}
			} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

				// addtextFields_commonMethod( "Interface Address Range",
				// "interfaceAddressRangeIPv6_textField" ,
				// newinterfaceAddressrangeIPv6 );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						"Interface Address Range");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						newinterfaceAddressrangeIPv6, "Interface Address Range");

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRangeIPv6");
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");

				String interfaceValueIntextField = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressIPv6_textField)
								.getAttribute("value");
				if (interfaceValueIntextField.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField);
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField,
							"PASS");
				}
			}
			WebElement IPv6Netwrk = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
			scrolltoview(IPv6Netwrk);
		}

		// Link Text Field
		// addtextFields_commonMethod( "Link", "Link_textField" , link);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, "Link");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, link, "Link");

		// Unit ID
		// addtextFields_commonMethod( "Unit ID" , "unitId_textField", unitID);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, "Unit ID");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField, unitID, "Unit ID");

		// Encapsulation
		addDropdownValues_commonMethod("Encapsulation",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

		// Slot
		// addtextFields_commonMethod( "Slot", "slot_textField", slot);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, "Slot");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot, "Slot");

		// Port
		edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port);

		// Pic
		// addtextFields_commonMethod( "Pic", "pic_textField" , pic );
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, "Pic");
		sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic, "Pic");

		// IV Management
		addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
				"IV management", IVmanagement);

		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying column Names under 'multilink Bearer' table");
		Report.LogInfo("INFO", "Verifying column Names under 'multilink Bearer' table", "INFO");
		
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_multilinkTableHeader,
				"MultiLinkBearerTable");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_checkToAddColumnName,
				"Check to Add to multilink' column name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_interfaceColumnName,
				"'Interface' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_linkOrcircuitColumnName,
				"'Link/Circuit Id' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_bearerTypeColumnName,
				"'Bearer Type' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_vlanIDcolumnName,
				"'VLAN Id' column Name");

		// Fetch value from text fields
		String slotActualvalue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField)
				.getAttribute("value");
		String portActualValue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField)
				.getAttribute("value");
		String picActualValue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField)
				.getAttribute("value");
		String unitIDActualValue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField)
				.getAttribute("value");

		// Compare Interface Name
		String interfaceName_multilink = "lsq-" + slotActualvalue + "/" + picActualValue + "/" + portActualValue + "."
				+ unitIDActualValue;
		compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
				interfaceName_multilink);

		scrollUp();
		waitforPagetobeenable();
		boolean configureInterfaceSelection = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configuraInterfaceCheckboxSelection).isSelected();

		scrolltoend();
		waitforPagetobeenable();

		// Generate Configuration
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Generate Configuration' under 'Add Multilink' Page");
		Report.LogInfo("INFO", "'Generate Configuration' under 'Add Multilink' Page", "INFO");
		if (configureInterfaceSelection) {

			// perform Generate configuration
			boolean configurationpanel = false;
			configurationpanel = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
							.isDisplayed();
			if (configurationpanel) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
				Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

				// click_commonMethod( "Generate", "addInterface_generateLink");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
				waitforPagetobeenable();

				scrolltoend();
				waitforPagetobeenable();

				String configurationvalues = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
				if (configurationvalues.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
							"FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
							+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, "
									+ "under 'Configuration' textbox values displaying as: " + configurationvalues,
							"PASS");
				}

				waitforPagetobeenable();
				scrolltoend();
				// click_commonMethod( "Execute and Save",
				// "addInterface_executeAndSaveButton" );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Execute And Save Button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Execute And Save Button");
				waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
				Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");

			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
				Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
			}

		} else {

			scrolltoend();
			// click_commonMethod( "OK" , "addInterface_okButton" );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_okButton, "OK Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_okButton, "OK Button");
			waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'OK' button");
			Report.LogInfo("INFO", "Clicked on 'OK' button", "PASS");
		}

		return interfaceName_multilink;

	}

	public void PEinterface_clickOEditlink(String interfaceName) throws InterruptedException, IOException {

		// String interfaceName=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, interfaceName);

		waitforPagetobeenable();	
		
		WebElement VPNSiteOrder_header = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
		// scrollToView(VPNSiteOrder_header);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
		
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");
		
		waitForAjax();

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_showInterfaceLink, "Show Interface");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_showInterfaceLink, "Show Interface");
		waitForAjax();

		// WebElement interfaceValue =
		// findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectInterfaceUnderPEdevice).replace("value",
		// interfaceName);

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectInterfaceUnderPEdevice1 + interfaceName
				+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectInterfaceUnderPEdevice2,"selectInterfaceUnderPEdevice2");

		ExtentTestManager.getTest().log(LogStatus.PASS,
				interfaceName + " interface is selected under 'Provider Equipment' panel");
		Report.LogInfo("INFO", interfaceName + " interface is selected under 'Provider Equipment' panel", "PASS");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.actionDropdown_UnderProviderEquipmentPanel,
				"Action"); // Click on 'Action' button
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.actionDropdown_UnderProviderEquipmentPanel, "Action");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.editLink_underProviderEquipment, "Edit"); // click
																													// on
																													// edit
																													// Link
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.editLink_underProviderEquipment, "Edit");
	}

	public String editInterface_Juniper(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_existingAddressIPv4Selection");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_existingAddressIPv4DropdownValue");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_newAddressIpv4Selection");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_newAddressIpv4Range");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_availableBlocksValue_IPv4");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditInterface_link");
		String bearerTypeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_Juniper_bearerType");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_encapsulation");
		String slot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditInterface_slot");
		String port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditInterface_port");
		String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditInterface_vlanId");
		String unitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditInterface_unitId");
		String pic = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditInterface_pic");
		String STM1number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditaddInterace_STM1number");
		String bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_bandwidth");
		String cardType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_cardType");
		String frameType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_frameType");
		String clockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_clockSource");
		String timeSlot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_timeSlot");
		String configureInterfaceOnDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_configureInterfaceOnDevice");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_Ivmanagement");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_existingAddressRangeIPv6selection");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_existingAddressIPv6DropdownValue");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_newAddressRangeIpv6selection");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_newAddressrangeIPv6");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_availableBlocksValue_IPv6");
		String bearerNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditInterface_bearerNumber");
		String editInterface_v4ConfigurationSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "EditInterface_v4Configugration_selection");
		String editInterface_v6ConfigurationSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "EditInterface_v6Configugration_selection");

		boolean addressValue = false;

		scrollUp();

		// Configure Interface on Device
		addCheckbox_commonMethod(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configureInterfaceOnDevice_checkbox,
				"Configure Interface on Device", configureInterfaceOnDevice);

		
		
		// Link Text Field
		edittextFields_commonMethod("Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, link);

		// Bearer Type Dropdown
		addDropdownValues_commonMethod("Bearer Type",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerType_Dropdown, bearerTypeValue);

		String bearerType = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchBearerTypedropdownvalue);

		if (bearerType.equals("10Gigabit Ethernet")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);


			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"xe-//.0"); // compare Interface default value

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			edittextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

			// Unit Id
			edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
					unitId);

			// Pic
			edittextFields_commonMethod("Pic", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic);

			// IV Management
			editcheckbox_commonMethod(IVmanagement,
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox, "IV management");
			

		} else if (bearerType.equals("E1")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// Framing Type
			addDropdownValues_commonMethod("Framing Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.framingType_Dropdown, frameType);

			if (frameType.equals("no-crc4")) {

				// Time Slot
				edittextFields_commonMethod("Timeslot",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, timeSlot);
			} else if (frameType.equals("crc4")) {

				// Time Slot
				edittextFields_commonMethod("Timeslot",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, timeSlot);
			} else if (frameType.equals("g704")) {

				// Time Slot
				edittextFields_commonMethod("Timeslot",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.timeSlot_textField, timeSlot);
			} else if (frameType.equals("unframed")) {

				System.out.println("No additonal fields");
			}

			// Clock Source
			addDropdownValues_commonMethod("Clock Source",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clockSource_Dropdown, clockSource);

			// STM1 Number
			edittextFields_commonMethod("STM1 Number",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number);

			// Bearer No
			edittextFields_commonMethod("Bearer No",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, bearerNumber);

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"e1-//::.0");

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			edittextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

			// Unit Id
			edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
					unitId);

			// Pic
			edittextFields_commonMethod("Pic", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic);

			// IV Management
			editcheckbox_commonMethod(IVmanagement,
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox, "IV management");

			// compare Interface value
			// String interfaceName_E1 = "e1-" + slot + "/" + pic + "/" + port +
			// ":" + STM1number + ":." + unitId;
			// compareText( "Interface", "Interface_textField", interfaceName_E1
			// );

		} else if (bearerType.equals("E3")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// STM1 Number
			edittextFields_commonMethod("STM1 Number",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number);

			// Bearer No
			edittextFields_commonMethod("Bearer No",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, bearerNumber);

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"e3-//::.0");

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			edittextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

			// Unit Id
			edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
					unitId);

			// Pic
			edittextFields_commonMethod("Pic", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic);

			// IV Management
			editcheckbox_commonMethod(IVmanagement,
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox, "IV management");

			// compare Interface Name
			// String interfaceName_E3 = "e3-" + slot + "/" + pic + "/" + port +
			// ":" + STM1number + ":." + unitId;
			// compareText( "Interface", "Interface_textField", interfaceName_E3
			// );

		} else if (bearerType.equals("Gigabit Ethernet")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"ge-//.0");

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			edittextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

			// Unit Id
			edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
					unitId);

			// Pic
			edittextFields_commonMethod("Pic", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic);

			// IV Management
			editcheckbox_commonMethod(IVmanagement,
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox, "IV management");

			// Compare Interface Name
			// String interfaceName_GigaBit = "ge-" + slot + "/" + pic + "/" +
			// port + "."+ unitId;
			// compareText( "Interface", "Interface_textField",
			// interfaceName_GigaBit );

		} else if (bearerType.equals("STM-1")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// STM1 Number
			edittextFields_commonMethod("STM1 Number",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number);

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"so-//:.0");

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			edittextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

			// Unit Id
			edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
					unitId);

			// Pic
			edittextFields_commonMethod("Pic", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic);

			// IV Management
			editcheckbox_commonMethod(IVmanagement,
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox, "IV management");

			// Compare Interface Name
			// String interfaceName_STM1Number = "so-" + slot + "/" + pic + "/"
			// + port + ":" + STM1number + "." + unitId;
			// compareText( "Interface", "Interface_textField",
			// interfaceName_STM1Number );

		} else if (bearerType.equals("T3")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// STM1 Number
			edittextFields_commonMethod("STM1 Number",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.stm1Number_textField, STM1number);

			// Bearer No
			edittextFields_commonMethod("Bearer No",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerNumber_addtextField, bearerNumber);

			// Interface
			compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
					"t3-//::.0");

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Slot
			edittextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

			// Unit Id
			edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
					unitId);

			// Pic
			edittextFields_commonMethod("Pic", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic);

			// IV Management
			editcheckbox_commonMethod(IVmanagement,
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox, "IV management");
			//
			// //Compare Interface Name
			// String interfaceName_T3 = "t3-" + slot + "/" + pic + "/" + port +
			// ":" + STM1number + "::." + unitId;
			// compareText( "Interface", "Interface_textField", interfaceName_T3
			// );
		}

		// Interface
		String interfaceName = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField)
				.getAttribute("value");
		if (interfaceName.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Interface' text field");
			Report.LogInfo("INFO", "No values displaying under 'Interface' text field", "FAIL");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface Name is displaying as: " + interfaceName);
			Report.LogInfo("INFO", "Interface Name is displaying as: " + interfaceName, "PASS");
		}

		scrollUp();
		waitforPagetobeenable();
		boolean configureInterfaceSelection = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configuraInterfaceCheckboxSelection).isSelected();

		scrolltoend();
		waitforPagetobeenable();

		if (configureInterfaceSelection) {

			// perform Generate configuration
			boolean configurationpanel = false;
			configurationpanel = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
							.isDisplayed();
			if (configurationpanel) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
				Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

				// click_commonMethod( "Generate", "addInterface_generateLink");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");

				waitforPagetobeenable();

				scrolltoend();
				waitforPagetobeenable();

				String configurationvalues = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
				if (configurationvalues.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
							"FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
							+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, "
									+ "under 'Configuration' textbox values displaying as: " + configurationvalues,
							"PASS");
				}

				waitforPagetobeenable();
				// click_commonMethod( "Execute and Save",
				// "addInterface_executeAndSaveButton" );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Execute And Save Button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Execute And Save Button");
				waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
				Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");

			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
				Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
			}
		} else {

			scrolltoend();
			// click_commonMethod( "OK" , "addInterface_okButton" );
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/addInterface_okButton")));
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_okButton,
					"Add Interface Ok Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_okButton, "Add Interface Ok Button");
			waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'OK' button");
			Report.LogInfo("INFO", "Clicked on 'OK' button", "PASS");

		}

		return interfaceName;
	}

	public String editMultilink(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String configureInterfaceOnDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_configureInterfaceOnDevice");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditMultilink_link");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_encapsulation");
		String slot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditMultilink_slot");
		String port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditMultilink_port");
		String pic = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditMultilink_pic");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_Ivmanagement");
		String unitID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditMultilink_unitId");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_existingAddressIPv4Selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_newAddressIpv4Selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_availableBlocksValue_IPv4");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMutilik_existingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_newAddressIpv4Range");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_existingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_newAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_availableBlocksValue_IPv6");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_existingAddressIPv6DropdownValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"EditMultilink_newAddressrangeIPv6");
		String editMultilink_v4ConfigurationSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "EditMultilink_v4Configugration_selection");
		String editMultilink_v6ConfigurationSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo,
				dataSetNo, "EditMultilink_v6Configugration_selection");

		waitforPagetobeenable();
		waitforPagetobeenable();

		scrollUp();
		waitforPagetobeenable();

		boolean addressValue = false;

		// Configure Interface on Device
		// addCheckbox_commonMethod(application,
		// "configureInterfaceOnDevice_checkbox", "Configure Interface on
		// Device", configureInterfaceOnDevice, "Yes" , xml);

		if (editMultilink_v4ConfigurationSelection.equalsIgnoreCase("Yes")) {

			// IPv4 Configuration
			if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
					&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

				// EIP Allocation
				/*verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4,
						"EIP Allocation");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4,
						"EIP Allocation");

				addDropdownValues_commonMethod("Subnet Size",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv4);

				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
						availableBlocksValue_IPv4);

				// click_commonMethod( "Allocate Subnet", "allocateSubnetButton"
				// );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
						"Allocate Subnet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

				EIPallocationSuccessMessage("successfully allocated");

				// click_commonMethod( "x", "EIPallocation_xButton");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
				*/
				waitforPagetobeenable();
				waitforPagetobeenable();

				// click_commonMethod( "Get Address", "getAddress_IPv4");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");

				String interfaceAddressRange = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceAddressRangeDropdown_addMultilink);

				if (interfaceAddressRange.isEmpty()) {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values displaying under 'Interface Address Range_IPv4' dropdown");
					Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv4' dropdown",
							"FAIL");

				} else {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
					Report.LogInfo("INFO",
							"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange, "PASS");

					// click_commonMethod( ">>" ,
					// "rightArrowButton_interfaceAddressRange");
					/*verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
							">>");

					addressValue = findWebElement(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface)
									.isDisplayed();
					if (addressValue) {

						selectEnableValueUnderAddressDropdown("Address",
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface,
								existingAddressIPv4DropdownValue);

					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv4' dropdown is not displaying");
						Report.LogInfo("INFO", "'Address_IPv4' dropdown is not displaying", "FAIL");
					}
					*/
				}
			} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
					&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

				/*edittextFields_commonMethod("Interface Address Range",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
						newinterfaceAddressrange);

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRange");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

				String interfaceValueIntextField = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_textField)
								.getAttribute("value");
				if (interfaceValueIntextField.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv4' text field");
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv4' text field", "FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField);
					Report.LogInfo("INFO",
							"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField,
							"PASS");
				}
				*/
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'V4 Configuration' is not edited");
			Report.LogInfo("INFO", "'V4 Configuration' is not edited", "PASS");
		}

		WebElement IPv4Netwrk = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_addInterface_networklabelName);
		scrolltoview(IPv4Netwrk);

		// IPv6 Configuration
		if (serviceSubType.equalsIgnoreCase("IPVPN Plus") || serviceSubType.equalsIgnoreCase("IPVPN Access")) {

			if (editMultilink_v6ConfigurationSelection.equalsIgnoreCase("Yes")) {

				if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
						&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

					// EIP Allocation
					// click_commonMethod( "EIP Allocation",
					// "EIPallocation_addInterfaceIpv6" );
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
							"EIP Allocation");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
							"EIP Allocation");

					addDropdownValues_commonMethod("Subnet Size",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown,
							subnetSizeValue_IPv6);

					addDropdownValues_commonMethod("Available Blocks",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
							availableBlocksValue_IPv6);

					// click_commonMethod( "Allocate Subnet",
					// "allocateSubnetButton" );
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
							"Allocate Subnet");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

					EIPallocationSuccessMessage("successfully allocated");

					// click_commonMethod( "x", "EIPallocation_xButton");
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

					String interfaceAddressRange = getTextFrom(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchInterfaceAddressRangeDropdown_addMultilinkIPv6);

					if (interfaceAddressRange.isEmpty()) {

						ExtentTestManager.getTest().log(LogStatus.FAIL,
								"No values displaying under 'Interface Address Range_IPv6' dropdown");
						Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown",
								"FAIL");

					} else {

						ExtentTestManager.getTest().log(LogStatus.PASS,
								"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);
						Report.LogInfo("INFO",
								"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange,
								"PASS");

						// click_commonMethod( ">>" ,
						// "rightArrowButton_interfaceAddressRangeIPv6");
						verifyExists(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
								">>");
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
								">>");

						try {
							addressValue = findWebElement(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6)
											.isDisplayed();
							if (addressValue) {

								selectEnableValueUnderAddressDropdown("Address",
										APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6,
										existingAddressIPv6DropdownValue);

							} else {
								ExtentTestManager.getTest().log(LogStatus.FAIL,
										"'Address_IPv6' dropdown is not displaying");
								Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
							}
						} catch (Exception e) {
							e.printStackTrace();
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"'Address_IPv6' dropdown is not displaying");
							Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
						}
					}
				} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
						&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

					edittextFields_commonMethod("Interface Address Range",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
							newinterfaceAddressrangeIPv6);

					// click_commonMethod( ">>" ,
					// "rightArrowButton_interfaceAddressRangeIPv6");
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");

					String interfaceValueIntextField = findWebElement(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressIPv6_textField)
									.getAttribute("value");
					if (interfaceValueIntextField.isEmpty()) {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								"No values dipslaying under 'Address_IPv6' text field");
						Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
					} else {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField);
						Report.LogInfo("INFO",
								"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField,
								"PASS");
					}
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "'V6 Configuration' is not edited");
				Report.LogInfo("INFO", "'V6 Configuration' is not edited", "PASS");
			}
			WebElement IPv6Netwrk = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
			scrolltoview(IPv6Netwrk);
		}

		// Link Text Field
		edittextFields_commonMethod("Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, link);

		// Unit ID
		edittextFields_commonMethod("Unit ID", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField,
				unitID);

		// Encapsulation
		addDropdownValues_commonMethod("Encapsulation",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

		// Slot
		edittextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField, slot);

		// Port
		edittextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField, port);

		// Pic
		edittextFields_commonMethod("Pic", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField, pic);

		// IV Management
		editcheckbox_commonMethod(IVmanagement,
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox, "IV management");

		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying column Names under 'multilink Bearer' table");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_multilinkTableHeader,
				"MultiLinkBearerTable");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_checkToAddColumnName,
				"'Check to Add to multilink' column name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_interfaceColumnName,
				"'Interface' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_linkOrcircuitColumnName,
				"'Link/Circuit Id' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_bearerTypeColumnName,
				"'Bearer Type' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_vlanIDcolumnName,
				"'VLAN Id' column Name");

		// fetch value fom the text fields
		String slotActualvalue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField)
				.getAttribute("value");
		String portActualValue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField)
				.getAttribute("value");
		String picActualValue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pic_textField)
				.getAttribute("value");
		String unitIDActualValue = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.unitId_textField)
				.getAttribute("value");

		scrollUp();
		waitforPagetobeenable();
		// Compare Interface Name
		String interfaceName_multilink = "lsq-" + slotActualvalue + "/" + picActualValue + "/" + portActualValue + "."
				+ unitIDActualValue;
		compareText("Interface", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField,
				interfaceName_multilink);

		scrollUp();
		waitforPagetobeenable();
		boolean configureInterfaceSelection = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configuraInterfaceCheckboxSelection).isSelected();

		scrolltoend();
		waitforPagetobeenable();

		// Generate Configuration
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Generate Configuration' under 'Add Multilink' Page");
		Report.LogInfo("INFO", "'Generate Configuration' under 'Add Multilink' Page", "INFO");
		if (configureInterfaceSelection) {

			// perform Generate configuration
			boolean configurationpanel = false;
			configurationpanel = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
							.isDisplayed();
			if (configurationpanel) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
				Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

				// click_commonMethod( "Generate", "addInterface_generateLink");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");

				waitforPagetobeenable();

				scrolltoend();
				waitforPagetobeenable();

				String configurationvalues = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
				if (configurationvalues.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
							"FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
							+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
					Report.LogInfo("INFO",
							"After clicking on 'Generate' button, "
									+ "under 'Configuration' textbox values displaying as: " + configurationvalues,
							"PASS");
				}

				waitforPagetobeenable();
				scrolltoend();
				// click_commonMethod( "Execute and Save",
				// "addInterface_executeAndSaveButton" );

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Execute and Save");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
						"Execute and Save");
				waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
				Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");

			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
				Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
			}

		} else {

			scrolltoend();
			// click_commonMethod( "OK" , "addInterface_okButton" );
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/addInterface_okButton")));
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_okButton, "OK");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_okButton, "OK");
			waitForAjax();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'OK' button");
			Report.LogInfo("INFO", "Clicked on 'OK' button", "PASS");

		}

		return interfaceName_multilink;

	}

	public String addInterface_Cisco(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv4Selection");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv4DropdownValue");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressIpv4Selection");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressIpv4Range");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"availableBlocksValue_IPv4");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_link");
		String bearerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_Cisco_bearerType");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_Cisco_encapsulation");
		String slot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_slot");
		String port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_port");
		String vlanid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_vlanId");
		String unitId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_unitId");
		String pic = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "addInterface_pic");
		String STM1number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterace_STM1number");
		String bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_bandwidth");
		String cardType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cardType");
		String frameType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_frameType");
		String clockSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_clockSource");
		String timeSlot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_timeSlot");
		String configureInterfaceOnDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_configureInterfaceOnDevice");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_Ivmanagement");
		String pppEncapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cisco_pppEncapsulation");
		String vpi = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cisco_vpitextField");
		String vci = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cisco_vciTextField");
		String dslDownstreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterace_cisco_dslDownStreamSpeed");
		String dslUpstreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterace_cisco_dslUpStreamSpeed");
		String SIPbay = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addInterface_cisco_SIPbay");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"availableBlocksValue_IPv6");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"existingAddressIPv6DropdownValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"newAddressrangeIPv6");

		boolean addressValue = false;

		waitforPagetobeenable();

		scrolltoend();
		waitForAjax();
		// click_commonMethod( "Execute and Save",
		// "addInterface_executeAndSaveButton" );
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
				"Execute and Save");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
				"Execute and Save");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
		Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");

		waitForAjax();

		waitforPagetobeenable();

		scrollUp();
		waitForAjax();

		// warningMessage_commonMethod( "slot_warningMessage", "Slot"); //Slot
		// warning Message
		// warningMessage_commonMethod( "port_warningMessage", "Port"); //Port
		// warning Message
		// warningMessage_commonMethod( "configuration_warningMessage",
		// "Configuration"); //Configuration warning Message

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_warningMessage, "Slot");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_warningMessage, "Port");

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configuration_warningMessage, "Configuration");

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4,
					"EIP Allocation");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4, "EIP Allocation");

			addDropdownValues_commonMethod("Subnet Size",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv4);

			addDropdownValues_commonMethod("Available Blocks",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
					availableBlocksValue_IPv4);

			// click_commonMethod( "Allocate Subnet", "allocateSubnetButton" );
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

			EIPallocationSuccessMessage("successfully allocated");

			// click_commonMethod( "x", "EIPallocation_xButton");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

			waitForAjax();
			waitforPagetobeenable();
			// click_commonMethod( "Get Address", "getAddress_IPv4");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");

			String interfaceAddressRange = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceAddressRangeDropdown_addInterface);

			if (interfaceAddressRange.isEmpty()) {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv4' dropdown");
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv4' dropdown", "FAIL");

			} else {

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");

				waitforPagetobeenable();
				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRange");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

				scrollUp();
				addressValue = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface)
								.isDisplayed();
				if (addressValue) {

					selectEnableValueUnderAddressDropdown("Address",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface,
							existingAddressIPv4DropdownValue);

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv4' dropdown is not displaying");
					Report.LogInfo("INFO", "'Address_IPv4' dropdown is not displaying", "FAIL");
				}

			}

		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			addtextFields_commonMethod("Interface Address Range",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					newinterfaceAddressrange);

			// click_commonMethod( ">>" ,
			// "rightArrowButton_interfaceAddressRange");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
					">>");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

			String interfaceValueIntextField = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_textField)
							.getAttribute("value");
			if (interfaceValueIntextField.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv4' text field");
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv4' text field", "FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField);
				Report.LogInfo("INFO",
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField, "PASS");
			}
		}

		WebElement IPv4Netwrk = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_addInterface_networklabelName);
		scrolltoview(IPv4Netwrk);
		// IPv6 Configuration
		if (serviceSubType.equalsIgnoreCase("IPVPN Plus") || serviceSubType.equalsIgnoreCase("IPVPN Access")) {

			if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

				// EIP Allocation
				// click_commonMethod( "EIP Allocation",
				// "EIPallocation_addInterfaceIpv6" );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");

				addDropdownValues_commonMethod("Subnet Size",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv6);

				addDropdownValues_commonMethod("Available Blocks",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
						availableBlocksValue_IPv6);

				// click_commonMethod( "Allocate Subnet", allocateSubnetButton
				// );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
						"Allocate Subnet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

				EIPallocationSuccessMessage("successfully allocated");

				// click_commonMethod( "x", "EIPallocation_xButton");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "X");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "X");

				String interfaceAddressRange = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchInterfaceAddressRangeDropdown_addInterfaceIPv6);

				if (interfaceAddressRange.isEmpty()) {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values displaying under 'Interface Address Range_IPv6' dropdown");
					Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown",
							"FAIL");

				} else {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);
					Report.LogInfo("INFO",
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange,
							"PASS");

					// click_commonMethod( ">>" ,
					// "rightArrowButton_interfaceAddressRangeIPv6");
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");

					try {
						addressValue = findWebElement(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6)
										.isDisplayed();
						if (addressValue) {

							selectEnableValueUnderAddressDropdown("Address",
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6,
									existingAddressIPv6DropdownValue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"'Address_IPv6' dropdown is not displaying");
							Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv6' dropdown is not displaying");
						Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
					}
				}
			} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

				addtextFields_commonMethod("Interface Address Range",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						newinterfaceAddressrangeIPv6);

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRangeIPv6");
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");

				String interfaceValueIntextField = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressIPv6_textField)
								.getAttribute("value");
				if (interfaceValueIntextField.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField);
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField,
							"PASS");
				}
			}
			WebElement IPv6Netwrk = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
			scrolltoview(IPv6Netwrk);

		}

		// Link Text Field
		addtextFields_commonMethod("Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, link);

		// Bearer Type Dropdown
		addDropdownValues_commonMethod("Bearer Type",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bearerType_Dropdown, bearerType);

		if (bearerType.equals("ATM")) {

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// DSL DownStream Speed
			addDropdownValues_commonMethod("DSL Downstream Speed",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DSLdownstreamSpead_Dropdown,
					dslDownstreamSpeed);

			// DSL Upstream Speed
			addDropdownValues_commonMethod("DSL Upstream Speed",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DSLupstreamSpeed_Dropdown, dslUpstreamSpeed);

			// PPP Encapsulation
			addDropdownValues_commonMethod("PPP Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PPPencapsulation_Dropdown, pppEncapsulation);

			// SIP Bay
			// addtextFields_commonMethod( "SIP Bay", "sipBat_textField",
			// SIPbay);

			// VPI
			addtextFields_commonMethod("VPI", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vpi_textField, vpi);

			// VCI
			addtextFields_commonMethod("VCI", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vci_textField, vci);

			// Slot
			addtextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			addtextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

		} else if (bearerType.equals("E1")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// SIP Bay
			// addtextFields_commonMethod( "SIP Bay", "sipBat_textField",
			// SIPbay);

			// Slot
			addtextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			addtextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

		} else if (bearerType.equals("E3")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// SIP Bay
			// addtextFields_commonMethod( "SIP Bay", "sipBat_textField",
			// SIPbay);

			// Slot
			addtextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			addtextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

		} else if (bearerType.equals("Ethernet")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// SIP Bay
			// addtextFields_commonMethod( "SIP Bay", "sipBat_textField",
			// SIPbay);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// Slot
			addtextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			addtextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

			// VLAN Id
			addtextFields_commonMethod("VLAN Id", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.vlanId_textField,
					vlanid);

		} else if (bearerType.equals("STM-1")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// SIP Bay
			// addtextFields_commonMethod( "SIP Bay", "sipBat_textField",
			// SIPbay);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// Slot
			addtextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			addtextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

		} else if (bearerType.equals("T3")) {

			// Bandwidth
			addDropdownValues_commonMethod("Bandwidth",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

			// Encapsulation
			addDropdownValues_commonMethod("Encapsulation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

			// SIP Bay
			// addtextFields_commonMethod( "SIP Bay", "sipBat_textField",
			// SIPbay);

			// Card Type
			addDropdownValues_commonMethod("Card Type",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.cardType_Dropdown, cardType);

			// Slot
			addtextFields_commonMethod("Slot", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.slot_textField,
					slot);

			// Port
			addtextFields_commonMethod("Port", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.port_textField,
					port);

		}

		// IV Management
		addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
				"IV management", IVmanagement);

		// Interface
		String interfaceName = findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Interface_textField)
				.getAttribute("value");
		if (interfaceName.isEmpty()) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Interface' text field");
			Report.LogInfo("INFO", "No values displaying under 'Interface' text field", "FAIL");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Interface Name is displaying as: " + interfaceName);
			Report.LogInfo("INFO", "Interface Name is displaying as: " + interfaceName, "PASS");
		}

		scrolltoend();
		waitForAjax();
		// perform Generate configurations
		boolean configurationpanel = false;
		configurationpanel = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
						.isDisplayed();
		if (configurationpanel) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
			Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			waitForAjax();

			scrolltoend();
			waitForAjax();

			String configurationvalues = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
			if (configurationvalues.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
				Report.LogInfo("INFO",
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
						"FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
				Report.LogInfo("INFO", "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues, "PASS");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
			Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
		}

		waitForAjax();
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
				"Execute And Save Button");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
				"Execute And Save Button");
		waitForAjax();
		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
		Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");
		
		return interfaceName;

	}

	public void addtextFields_commonMethod(String labelname, String xpathname, String expectedValueToAdd)
			throws InterruptedException, IOException {
		boolean availability = false;
		try {
			availability = findWebElement(xpathname).isDisplayed();
			if (availability) {
				ExtentTestManager.getTest().log(LogStatus.PASS, labelname + " text field is displaying");
				Report.LogInfo("INFO", labelname + " text field is displaying", "PASS");

				if (expectedValueToAdd.equalsIgnoreCase("null")) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "No values added to text field " + labelname);
					Report.LogInfo("INFO", "No values added to text field " + labelname, "PASS");
				} else {

					sendKeys(xpathname, expectedValueToAdd);
					// SendKeys(getwebelement(xml.getlocator("//locators/" +
					// application + "/"+ xpathname +"")), expectedValueToAdd);

					Thread.sleep(3000);

					String actualvalue = findWebElement(xpathname).getAttribute("value");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							labelname + " text field value added as: " + actualvalue);
					Report.LogInfo("INFO", labelname + " text field value added as: " + actualvalue, "PASS");
				}

			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " text field is not displaying");
				Report.LogInfo("INFO", labelname + " text field is not displaying", "FAIL");
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " text field is not displaying");
			Report.LogInfo("INFO", labelname + " text field is not displaying", "FAIL");
		} catch (Exception ee) {
			ee.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to add value to " + labelname + " text field");
			Report.LogInfo("INFO", " Not able to add value to " + labelname + " text field", "FAIL");
		}
	}

	public String addMultilink_Cisco(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String serviceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String link = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_Link");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_Encapsulation");
		String bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_cisco_bandwidth");
		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"multilink_Ivmanagement");
		String existingAddressRangeIPv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressRangeIPv4selection");
		String newAddressRangeIpv4selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newAddressRangeIpv4selection");
		String subnetSizeValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_subnetSizeValue_IPv4");
		String availableBlocksValue_IPv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilik_availableBlocksValue_IPv4");
		String existingAddressIPv4DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressIPv4DropdownValue");
		String newinterfaceAddressrange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newinterfaceAddressrangeIPv4");
		String existingAddressRangeIPv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressRangeIPv6selection");
		String newAddressRangeIpv6selection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newAddressRangeIpv6selection");
		String subnetSizeValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_subnetSizeValue_IPv6");
		String availableBlocksValue_IPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_availableBlocksValue_IPv6");
		String existingAddressIPv6DropdownValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_existingAddressIPv6DropdownValue");
		String newinterfaceAddressrangeIPv6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_newinterfaceAddressrangeIPv6");
		String interfaceMultilinkName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addMultilink_Cisco_InterfaceTextField");

		waitForAjax();
		waitforPagetobeenable();

		scrollUp();
		waitForAjax();

		boolean addressValue = false;

		// IPv4 Configuration
		if (existingAddressRangeIPv4selection.equalsIgnoreCase("yes")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("no")) {

			// EIP Allocation
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4,
					"EIP Allocation");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIPV4, "EIP Allocation");

			addDropdownValues_commonMethod("Subnet Size",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.subnetSize_dropdown, subnetSizeValue_IPv4);

			addDropdownValues_commonMethod("Available Blocks",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.availableBlocks_dropdown,
					availableBlocksValue_IPv4);

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

			EIPallocationSuccessMessage("successfully allocated");

			// click_commonMethod( "x", "EIPallocation_xButton");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "x");

			waitForAjax();

			waitforPagetobeenable();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.getAddress_IPv4, "Get Address");

			String interfaceAddressRange = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceAddressRangeDropdown_addMultilink);

			if (interfaceAddressRange.isEmpty()) {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Interface Address Range_IPv4' dropdown");
				Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv4' dropdown", "FAIL");

			} else {

				ExtentTestManager.getTest().log(LogStatus.PASS,
						"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
				Report.LogInfo("INFO", "'Interface Address Range' dropdown value displays as: " + interfaceAddressRange,
						"PASS");

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRange");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

				scrollUp();
				addressValue = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface)
								.isDisplayed();
				if (addressValue) {

					selectEnableValueUnderAddressDropdown("Address",
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterface,
							existingAddressIPv4DropdownValue);

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv4' dropdown is not displaying");
					Report.LogInfo("INFO", "'Address_IPv4' dropdown is not displaying", "FAIL");
				}
			}
		} else if (existingAddressRangeIPv4selection.equalsIgnoreCase("No")
				&& newAddressRangeIpv4selection.equalsIgnoreCase("Yes")) {

			addtextFields_commonMethod("Interface Address Range",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRange_textField,
					newinterfaceAddressrange);

			// click_commonMethod( ">>" ,
			// "rightArrowButton_interfaceAddressRange");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange,
					">>");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRange, ">>");

			String interfaceValueIntextField = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_textField)
							.getAttribute("value");
			if (interfaceValueIntextField.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values dipslaying under 'Address_IPv4' text field");
				Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv4' text field", "FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField);
				Report.LogInfo("INFO",
						"value in 'Address_IPv4' text field is displaying as: " + interfaceValueIntextField, "PASS");
			}
		}
		WebElement IPv4Netwrk = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_addInterface_networklabelName);
		scrolltoview(IPv4Netwrk);

		// IPv6 Configuration
		if (serviceSubType.equalsIgnoreCase("IPVPN Plus") || serviceSubType.equalsIgnoreCase("IPVPN Access")) {
			if (existingAddressRangeIPv6selection.equalsIgnoreCase("yes")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("no")) {

				// EIP Allocation
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_addInterfaceIpv6,
						"EIP Allocation");

				addDropdownValues_commonMethod("Subnet Size", "subnetSize_dropdown", subnetSizeValue_IPv6);

				addDropdownValues_commonMethod("Available Blocks", "availableBlocks_dropdown",
						availableBlocksValue_IPv6);

				// click_commonMethod( "Allocate Subnet", "allocateSubnetButton"
				// );
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton,
						"Allocate Subnet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.allocateSubnetButton, "Allocate Subnet");

				EIPallocationSuccessMessage("successfully allocated");

				// click_commonMethod( "x", "EIPallocation_xButton");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "X");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.EIPallocation_xButton, "X");

				String interfaceAddressRange = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.fetchInterfaceAddressRangeDropdown_addMultilinkIPv6);

				if (interfaceAddressRange.isEmpty()) {

					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values displaying under 'Interface Address Range_IPv6' dropdown");
					Report.LogInfo("INFO", "No values displaying under 'Interface Address Range_IPv6' dropdown",
							"FAIL");

				} else {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange);
					Report.LogInfo("INFO",
							"'Interface Address Range_IPv6' dropdown value displays as: " + interfaceAddressRange,
							"PASS");

					// click_commonMethod( ">>" ,
					// "rightArrowButton_interfaceAddressRangeIPv6");
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
							">>");

					try {
						addressValue = findWebElement(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6)
										.isDisplayed();
						if (addressValue) {

							selectEnableValueUnderAddressDropdown("Address",
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.FetchInterfaceDropdown_addInterfaceIPv6,
									existingAddressIPv6DropdownValue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"'Address_IPv6' dropdown is not displaying");
							Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
						}
					} catch (Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, "'Address_IPv6' dropdown is not displaying");
						Report.LogInfo("INFO", "'Address_IPv6' dropdown is not displaying", "FAIL");
					}
				}

			} else if (existingAddressRangeIPv6selection.equalsIgnoreCase("No")
					&& newAddressRangeIpv6selection.equalsIgnoreCase("Yes")) {

				addtextFields_commonMethod("Interface Address Range",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressRangeIPv6_textField,
						newinterfaceAddressrangeIPv6);

				// click_commonMethod( ">>" ,
				// "rightArrowButton_interfaceAddressRangeIPv6");
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.rightArrowButton_interfaceAddressRangeIPv6,
						">>");

				String interfaceValueIntextField = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddressIPv6_textField)
								.getAttribute("value");
				if (interfaceValueIntextField.isEmpty()) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"No values dipslaying under 'Address_IPv6' text field");
					Report.LogInfo("INFO", "No values dipslaying under 'Address_IPv6' text field", "FAIL");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField);
					Report.LogInfo("INFO",
							"value in 'Address_IPv6' text field is displaying as: " + interfaceValueIntextField,
							"PASS");
				}
			}
			WebElement IPv6Netwrk = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.networkLabelname_IPv6);
			scrolltoview(IPv6Netwrk);
		}

		// Link Text Field
		addtextFields_commonMethod("Link", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Link_textField, link);

		// Encapsulation
		addDropdownValues_commonMethod("Encapsulation",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.encapsulation_Dropdown, encapsulation);

		// Bandwidth
		addDropdownValues_commonMethod("Bandwidth",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.bandwidth_Dropdown, bandwidth);

		// IV Management
		addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
				"IV management", IVmanagement);

		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying column Names under 'multilink Bearer' table");
		Report.LogInfo("INFO", "Verifying column Names under 'multilink Bearer' table", "INFO");
		
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_multilinkTableHeader,
				"MultiLinkBearerTable");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilink_checkToAddColumnName,
				"'Check to Add to multilink' column name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_interfaceColumnName,
				"'Interface' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_linkOrcircuitColumnName,
				"'Link/Circuit Id' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_bearerTypeColumnName,
				"'Bearer Type' column Name");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addMultilinkTable_vlanIDcolumnName,
				"'VLAN Id' column Name");

		scrolltoend();
		waitForAjax();

		// Generate Configuration
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Generate Configuration' under 'Add Multilink' Page");
		Report.LogInfo("INFO", "'Generate Configuration' under 'Add Multilink' Page", "INFO");

		// perform Generate configuration
		boolean configurationpanel = false;
		configurationpanel = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
						.isDisplayed();
		if (configurationpanel) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
			Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

			// click_commonMethod( "Generate", "addInterface_generateLink");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");

			waitForAjax();

			scrolltoend();
			waitForAjax();

			String configurationvalues = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
			if (configurationvalues.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
				Report.LogInfo("INFO",
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
						"FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
				Report.LogInfo("INFO", "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues, "PASS");
			}

			waitForAjax();
			// click_commonMethod( "Execute and Save",
			// "addInterface_executeAndSaveButton" );
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/addInterface_executeAndSaveButton")));
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
					"Execute And Save Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
					"Execute And Save Button");

			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
			Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
			Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
		}

		return interfaceMultilinkName;

	}

	public void PEdevice_clickOnselectInterface(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPEdevice");
		
		WebElement VPNSiteOrder_header = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
		// scrollToView(VPNSiteOrder_header);
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
		
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");
		waitForAjax();
		waitforPagetobeenable();
		WebElement providerEquipment_panelHeader = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.providerEquipment_panelHeader);
		scrolltoview(providerEquipment_panelHeader);
		waitForAjax();

		if (findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.existingdevicegrid).isDisplayed()) {
			List<WebElement> addeddevicesList = findWebElements(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_fetchAlldevice_InviewPage);
			// Log.info(addeddevicesList);
			int AddedDevicesCount = addeddevicesList.size();
			for (int i = 0; i < AddedDevicesCount; i++) {
				String AddedDeviceNameText = addeddevicesList.get(i).getText();
				String AddedDevice_SNo = AddedDeviceNameText.substring(0, 1);
				if (AddedDeviceNameText.contains(existingdevicename)) {
					waitforPagetobeenable();
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_selectInterface_InViewPage1
							+ AddedDevice_SNo
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_selectInterface_InViewPage2,"Select Interface link");
					waitForAjax();
					break;
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
					Report.LogInfo("INFO", "Invalid device name", "FAIL");

				}

			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");

		}
	}

	public void SelectInterfacetoremovefromservice() throws InterruptedException, IOException {

		// String interfacename=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "ExistingPEdevice");

		waitForAjax();

		waitforPagetobeenable();
		Report.LogInfo("INFO", "'Select Interface'_Remove Interace from Service", "INFO");
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Select Interface'_Remove Interace from Service");
		scrollUp();
		scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfacesInServices_searchTextBox);
		int chkCnt = getXPathCount(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfacescheckBoxes);
		
		for(int i = 1; i<= chkCnt; i++){
			webDriver.findElement(By.xpath("((//div[text()='Interfaces in Service']//parent::div)[1]//following-sibling::div//input[@type='radio'])['"+i+"']")).click();
			
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_Actiondropdown,
					"Action");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_Actiondropdown,
					"Action");			
			
			Thread.sleep(1000);
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_removebuton,
					"Remove");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_removebuton,
					"Remove");
			
			waitforPagetobeenable();
			refreshPage();
			waitForAjax();
		}
		
		Report.LogInfo("INFO", "Interface removed successfully", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Interface removed successfully","PASS");

		//addtextFields_commonMethod("search",
				//APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfacesInServices_searchTextBox, interfacename);

		//waitForAjax();
		//selectRowforInterfaceInService(interfacename);

	}

	public void selectRowforInterfaceInService(String interfacenumber) throws IOException, InterruptedException {

		int TotalPages;

		String TextKeyword = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_totalpage);

		TotalPages = Integer.parseInt(TextKeyword);

		ExtentTestManager.getTest().log(LogStatus.INFO, "Total number of pages in table is: " + TotalPages);
		Report.LogInfo("INFO", "Total number of pages in table is: " + TotalPages, "INFO");

		ab:

		if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_currentpage);
				int Current_page = Integer.parseInt(CurrentPage);

				// assertEquals(k, Current_page);
				ExtentTestManager.getTest().log(LogStatus.INFO, "Currently we are in page number: " + Current_page);
				Report.LogInfo("INFO", "Currently we are in page number: " + Current_page, "INFO");

				// List<WebElement> results =
				// findWebElements(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectinterfaceUnderInterfacesInService).replace("value",
				// interfacenumber);
				List<WebElement> results = findWebElements(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectinterfaceUnderInterfacesInService1
								+ interfacenumber
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectinterfaceUnderInterfacesInService2);
				int numofrows = results.size();
				ExtentTestManager.getTest().log(LogStatus.INFO, "no of results: " + numofrows);

				Report.LogInfo("INFO", "no of results: " + numofrows, "INFO");
				boolean resultflag;

				if (numofrows == 0) {

					PageNavigation_NextPageForInterfaceInService();

				} else {
					for (int i = 0; i < numofrows; i++) {
						try {

							waitForAjax();
							waitforPagetobeenable();

							resultflag = results.get(i).isDisplayed();
							Report.LogInfo("INFO", "status of result: " + resultflag, "INFO");
							if (resultflag) {
								// Log.info(results.get(i).getText());
								results.get(i).click();
								ExtentTestManager.getTest().log(LogStatus.PASS,
										interfacenumber + " is selected under 'Interface in Service' table");
								Report.LogInfo("INFO",
										interfacenumber + " is selected under 'Interface in Service' table", "PASS");

								verifyExists(
										APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_Actiondropdown,
										"Action");
								click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_Actiondropdown,
										"Action");

								waitForAjax();

								verifyExists(
										APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_removebuton,
										"Remove");
								click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_removebuton,
										"Remove");
							}
						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = findWebElements(
									APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectinterfaceUnderInterfacesInService1
											+ interfacenumber
											+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectinterfaceUnderInterfacesInService2);

							numofrows = results.size();
							// results.get(i).click();
							// Log.info("selected row is : " + i);
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"failure while selecting interface to remove from service");
							Report.LogInfo("INFO", "failure while selecting interface to remove from service", "FAIL");

						}
					}
					break ab;
				}
			}
		} else {
			Report.LogInfo("INFO", interfacenumber + " is not dipslayin gunder 'Interfaces in Service' table ", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					interfacenumber + " is not dipslaying gunder 'Interfaces in Service' table ");
		}
	}

	public void PageNavigation_NextPageForInterfaceInService() throws InterruptedException, IOException {

		// Clickon(getwebelement(xml.getlocator("//locators/" + Application +
		// "/InterfaceInselect_nextpage")));
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_nextpage, "Next Page");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceInselect_nextpage, "Next Page");

		waitForAjax();

	}

	public void SelectInterfacetoaddwithservcie(String interfacenumber) throws InterruptedException, IOException {

		scrolltoend();
		waitForAjax();

		ExtentTestManager.getTest().log(LogStatus.INFO, "'Select interface'_Add Interface to Service'");
		Report.LogInfo("INFO", "'Select interface'_Add Interface to Service'", "INFO");
		waitForAjax();
		waitforPagetobeenable();

		addtextFields_commonMethod("Search", "interfacesToSelect_searchtextBOx", interfacenumber);

		// click_commonMethod("Interface column Header",
		// "InterfaceToSelect_interfaceColumnHeader");
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToSelect_interfaceColumnHeader,
				"Interface column Header");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToSelect_interfaceColumnHeader,
				"Interface column Header");

		selectrowforInterfaceToselecttable(interfacenumber);

	}

	public void selectrowforInterfaceToselecttable(String interfacenumber) throws IOException, InterruptedException {

		int TotalPages;

		String TextKeyword = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_totalpage);

		TotalPages = Integer.parseInt(TextKeyword);

		ab:

		if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_currentpage);
				int Current_page = Integer.parseInt(CurrentPage);

				// assertEquals(k, Current_page);

				// System.out.println("Currently we are in page number: " +
				// Current_page);

				// List<WebElement> results =
				// getwebelements("//div[div[contains(text(),'Interfaces to
				// Select')]]/following-sibling::div[1]//div[text()='" +
				// interfacenumber + "']");
				List<WebElement> results = webDriver.findElements(By
						.xpath("//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[text()='"
								+ interfacenumber + "']"));

				// System.out.println(results);
				int numofrows = results.size();
				// System.out.println("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {
					PageNavigation_NextPageForInterfaceToselect();
				}

				else {
					for (int i = 0; i < numofrows; i++) {
						try {
							resultflag = results.get(i).isDisplayed();
							// System.out.println("status of result: " +
							// resultflag);
							if (resultflag) {
								// System.out.println(results.get(i).getText());
								results.get(i).click();
								ExtentTestManager.getTest().log(LogStatus.PASS,
										interfacenumber + " is selected under 'Interface to select' table");
								Report.LogInfo("INFO",
										interfacenumber + " is selected under 'Interface to select' table", "PASS");
								waitForAjax();
								// Clickon(getwebelement(xml.getlocator("//locators/"
								// + Application +
								// "/InterfaceToselect_Actiondropdown")));
								verifyExists(
										APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_Actiondropdown,
										"Interface Action dropdown");
								click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_Actiondropdown,
										"Interface Action dropdown");

								waitForAjax();
								// Clickon(getwebelement(xml.getlocator("//locators/"
								// + Application +
								// "/InterfaceToselect_addbuton")));
								verifyExists(
										APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_addbuton,
										"Interface Add Button");
								click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_addbuton,
										"Interface Add Button");

								waitForAjax();
								ExtentTestManager.getTest().log(LogStatus.PASS,
										interfacenumber + " is added to service");
								Report.LogInfo("INFO", interfacenumber + " is added to service", "PASS");

							}
						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = webDriver.findElements(By
									.xpath("//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[text()='"
											+ interfacenumber + "']"));
							numofrows = results.size();
							results.get(i).click();
							Report.LogInfo("INFO", " Failure on selecting an Interface to ad with service ", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									" Failure on selecting an Interface to ad with service ");
						}
					}
					break ab;
				}
			}
		} else {
			// System.out.println("No values found inside the table");
			Report.LogInfo("INFO", "No values available inside the Interfacetoselect table", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					interfacenumber + " value is not dipslaying under 'Interfaces to select' table");
		}

	}

	public void PageNavigation_NextPageForInterfaceToselect() throws InterruptedException, IOException {

		// Clickon(getwebelement(xml.getlocator("//locators/" + Application +
		// "/InterfaceToselect_nextpage")));
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_nextpage,
				"Interface To select Next Page");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToselect_nextpage,
				"Interface To select Next Page");

		waitForAjax();
	}

	public void PEdevice_clickOnAutodiscoverVPN(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPEdevice");
		
		waitforPagetobeenable();
		if(isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink)){
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");
		}

		
		WebElement providerEquipment_panelHeader = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.providerEquipment_panelHeader);
		scrolltoview(providerEquipment_panelHeader);
		waitForAjax();

		if (findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.existingdevicegrid).isDisplayed()) {
			List<WebElement> addeddevicesList = findWebElements(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_fetchAlldevice_InviewPage);
			int AddedDevicesCount = addeddevicesList.size();
			for (int i = 0; i < AddedDevicesCount; i++) {
				String AddedDeviceNameText = addeddevicesList.get(i).getText();
				String AddedDevice_SNo = AddedDeviceNameText.substring(0, 1);
				if (AddedDeviceNameText.contains(existingdevicename)) {
					WebElement PEdevice_autoDiscoverVPN = findWebElement(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clickOnAutoDiscoverVPNunderProviderEquipmentPanel1
									+ AddedDevice_SNo
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clickOnAutoDiscoverVPNunderProviderEquipmentPanel2);
					// Clickon(PEdevice_autoDiscoverVPN);
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clickOnAutoDiscoverVPNunderProviderEquipmentPanel1
									+ AddedDevice_SNo
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clickOnAutoDiscoverVPNunderProviderEquipmentPanel2,
							" Auto Discover VPNunder Provider Equipment Panel");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clickOnAutoDiscoverVPNunderProviderEquipmentPanel1
							+ AddedDevice_SNo
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.clickOnAutoDiscoverVPNunderProviderEquipmentPanel2,
							" Auto Discover VPNunder Provider Equipment Panel");

					waitForAjax();
					break;
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
					Report.LogInfo("INFO", "Invalid device name", "FAIL");
				}

			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
		}
	}

	public void PEInterface_clickOnDeleteLink(String interfaceName) throws InterruptedException, IOException {

		waitForAjax();
		waitforPagetobeenable();

		scrolltoend();
		waitForAjax();

		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_showInterfaceLink, "Show Interface");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_showInterfaceLink, "Show Interface");
		waitForAjax();

		WebElement interfaceValue = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectInterfaceUnderPEdevice1 + interfaceName
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectInterfaceUnderPEdevice2);
		// Clickon(interfaceValue);
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectInterfaceUnderPEdevice1 + interfaceName
				+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectInterfaceUnderPEdevice2,
				"select Interface Under PEdevice");

		ExtentTestManager.getTest().log(LogStatus.PASS,
				interfaceName + " interface is selected under 'Provider Equipment' panel");
		Report.LogInfo("INFO", interfaceName + " interface is selected under 'Provider Equipment' panel", "PASS");

		// click_commonMethod("Action",
		// "actionDropdown_UnderProviderEquipmentPanel"); //Click on 'Action'
		// button
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.actionDropdown_UnderProviderEquipmentPanel,
				"Action");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.actionDropdown_UnderProviderEquipmentPanel, "Action");

		// click_commonMethod("Delete", "PEdevice_deleteLink"); //click on
		// Delete Link
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_deleteLink, "Delete");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PEdevice_deleteLink, "Delete");

		WebElement DeleteAlertPopup = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.delete_alertpopup);
		if (DeleteAlertPopup.isDisplayed()) {
			String deletPopUpMessage = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deleteMessages_textMessage);

			ExtentTestManager.getTest().log(LogStatus.PASS,
					"Delete alert POP up displays. Message is displaying as: " + deletPopUpMessage);
			Report.LogInfo("INFO", "Delete alert POP up displays. Message is displaying as: " + deletPopUpMessage,
					"PASS");

			// click_commonMethod( "Delete", "deletebutton");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deletebutton, "Delete");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deletebutton, "Delete");

			scrollUp();
			waitForAjax();

			// verifysuccessmessage(application, "Link successfully deleted.");
		} else {
			Report.LogInfo("INFO", "Delete alert popup is not displayed", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete alert popup is not displayed");
		}
	}

	public void deletePEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ExistingPEdevice");

		WebElement providerEquipment_panelHeader = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.providerEquipment_panelHeader);
		scrolltoview(providerEquipment_panelHeader);
		waitForAjax();

		if (findWebElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.existingdevicegrid).isDisplayed()) {
			List<WebElement> addeddevicesList = findWebElements(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_fetchAlldevice_InviewPage);
			// Log.info(addeddevicesList);
			int AddedDevicesCount = addeddevicesList.size();
			for (int i = 0; i < AddedDevicesCount; i++) {
				String AddedDeviceNameText = addeddevicesList.get(i).getText();
				String AddedDevice_SNo = AddedDeviceNameText.substring(0, 1);
				if (AddedDeviceNameText.contains(existingdevicename)) {
					WebElement PEDevice_deleteLink = findWebElement(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_deleteFromService1 + AddedDevice_SNo
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_deleteFromService2);
					// Clickon(PEDevice_deleteLink);
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_deleteFromService1 + AddedDevice_SNo
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_deleteFromService2,
							"PE_delete From Service");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_deleteFromService1 + AddedDevice_SNo
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.PE_deleteFromService2,
							"PE_delete From Service");
					waitForAjax();

					WebElement DeleteAlertPopup = findWebElement(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.delete_alertpopup);
					if (DeleteAlertPopup.isDisplayed()) {
						String deletPopUpMessage = getTextFrom(
								APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deleteMessages_textMessage);

						Report.LogInfo("INFO",
								"Delete alert POP up displays. Message is displaying as: " + deletPopUpMessage, "PASS");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Delete alert POP up displays. Message is displaying as: " + deletPopUpMessage);

						// click_commonMethod(deletebutton,"Delete");
						verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deletebutton, "Delete");
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.deletebutton, "Delete");

						scrollUp();
						waitForAjax();

					} else {
						Report.LogInfo("INFO", "Delete alert popup is not displayed", "FAIL");
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Delete alert popup is not displayed");
					}
					break;
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
					Report.LogInfo("INFO", "Invalid device name", "FAIL");
				}
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
		}

	}

	public void verifyAddDSLAMandHSLlink(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String DSLMdevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ActelisTech_DSLAMdevice");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");

		if (ServiceSubType.contains("IPVPN")) {

			WebElement VPNSiteOrder_header = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrolltoview(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			// System.out.println("Numberofuser="+ NoOfUsers);

			if (NoOfUsers == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
						"VPN Order Radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiteOrder
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			}

			// click( "Action dropdown", "VPNSiteactionbutton");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");

			// click( "Click Radio Button", "SiteOrderRadioButton");
			// click( "Action dropdown", "VPNSiteactionbutton");
			// ClickCommon( "View", "AddVPNSiteCommonLink");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "View");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "View");

			boolean actelisConfigurationPanel = false;

			ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'Add DSLAM and HSL'");
			Report.LogInfo("INFO", "Verifying 'Add DSLAM and HSL'", "INFO");

			waitForAjax();
			waitforPagetobeenable();

			scrolltoend();

			actelisConfigurationPanel = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ActelisConfigurationPanel).isDisplayed();

			if (actelisConfigurationPanel) {
				Report.LogInfo("INFO", " 'Actelis Configuration' panel is displaying as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" 'Actelis Configuration' panel is displaying as expected");

				boolean actelisLink = false;
				try {
					actelisLink = findWebElement(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Actelisconfig_addDSLAM).isDisplayed();
					if (actelisLink) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Add DSLAM and HSL' link is displaying as expected");
						Report.LogInfo("INFO", " 'Add DSLAM and HSL' link is displaying as expected", "PASS");

						// Clickon(getwebelement(xml.getlocator("//locators/" +
						// application + "/Actelisconfig_addDSLAM")));
						verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Actelisconfig_addDSLAM,
								"Actelis config addDSLAM");
						click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Actelisconfig_addDSLAM,
								"Actelis config addDSLAM");

						waitForAjax();

					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel");
						Report.LogInfo("INFO",
								" 'Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel",
								"FAIL");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" ''Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel ");
					Report.LogInfo("INFO",
							" ''Add DSLAM and HSL' link is not displaying under 'Actelis Configuration' panel ",
							"FAIL");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " NOt able to click on 'Add DSLAM and HSL' link");
					Report.LogInfo("INFO", " NOt able to click on 'Add DSLAM and HSL' link", "FAIL");
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Actelis Configuration' panel is not displaying");
				Report.LogInfo("INFO", " 'Actelis Configuration' panel is not displaying", "FAIL");
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}

	}

	public void AddDSLAMandHSL(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String DSLMdevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ActelisTech_DSLAMdevice");
		String HSlname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ActelisTech_DSLAMInterfacename");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		if (ServiceSubType.contains("IPVPN")) {
			waitForAjax();
			waitforPagetobeenable();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addDSLAMandHSL_xButton,
					"Add DSLAM and HSL xButton");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addDSLAMandHSL_xButton,
					"Add DSLAM and HSL xButton");

			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'X' button under 'DSLAM device' dropdown");
			Report.LogInfo("INFO", "Clicked on 'X' button under 'DSLAM device' dropdown", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DSLM_Device_Select, "DSLM Device Select");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DSLM_Device_Select, DSLMdevice,
					"DSLM Device Select");

			ExtentTestManager.getTest().log(LogStatus.PASS, DSLMdevice + " is entered under 'DSLAM device' dropdown");
			Report.LogInfo("INFO", DSLMdevice + " is entered under 'DSLAM device' dropdown", "PASS");

			WebElement valueToSElect = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectDSLAMdeviceValue1 + DSLMdevice
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectDSLAMdeviceValue2);

			try {
				if (valueToSElect.isDisplayed()) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							DSLMdevice + " is displaying under 'DSLAM device' dropdown");
					Report.LogInfo("INFO", DSLMdevice + " is displaying under 'DSLAM device' dropdown", "PASS");

					// Clickon(valueToSElect);
					verifyExists(
							APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectDSLAMdeviceValue1 + DSLMdevice
									+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectDSLAMdeviceValue2,
							"Value to select");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectDSLAMdeviceValue1 + DSLMdevice
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectDSLAMdeviceValue2,
							"Value to select");

					ExtentTestManager.getTest().log(LogStatus.PASS,
							DSLMdevice + " is selected under 'DSLAM device' dropdown");
					Report.LogInfo("INFO", DSLMdevice + " is selected under 'DSLAM device' dropdown", "PASS");
					waitForAjax();

					waitForAjax();
					waitforPagetobeenable();

					// click_commonMethod( "List_HSL", "List_HSL_Link"); //click
					// on "List HSL" button
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.List_HSL_Link, "List_HSL");
					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.List_HSL_Link, "List_HSL");

					waitForAjax();
					waitForAjax();
					selectRowForAddingInterface_Actelis(HSlname); // select the
																	// Interface

				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
					Report.LogInfo("INFO", DSLMdevice + " is not displaying under 'DSLAM device' dropdown", "FAIL");
				}

			} catch (Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
				Report.LogInfo("INFO", DSLMdevice + " is not displaying under 'DSLAM device' dropdown", "FAIL");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");

		}

	}

	public void selectRowForAddingInterface_Actelis(String interfacenumber) throws IOException, InterruptedException {
		scrolltoend();
		waitForAjax();

		// System.out.println("check second time");
		int TotalPages;

		String TextKeyword = getTextFrom(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToSelect_actelis_totalpage);

		TotalPages = Integer.parseInt(TextKeyword);

		// System.out.println("Total number of pages in table is: " +
		// TotalPages);

		ab:

		if (TotalPages != 0) {
			for (int k = 1; k <= TotalPages; k++) {

				// Current page
				String CurrentPage = getTextFrom(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceToSelect_actelis_currentpage);
				int Current_page = Integer.parseInt(CurrentPage);

				// Clickon(getwebelement("//div[text()='Add DSLAM and HSL']"));
				webDriver.findElement(By.xpath("//div[text()='Add DSLAM and HSL']")).click();
				// System.out.println("Currently we are in page number: " +
				// Current_page);

				List<WebElement> results = webDriver
						.findElements(By.xpath("//div[contains(text(),'" + interfacenumber + "')]"));

				int numofrows = results.size();
				// System.out.println("no of results: " + numofrows);
				boolean resultflag;

				if (numofrows == 0) {

					// Clickon(getwebelement("//button[text()='Next']"));
					webDriver.findElement(By.xpath("//button[text()='Next']")).click();
					waitForAjax();

				}

				else {

					for (int i = 0; i < numofrows; i++) {

						try {

							resultflag = results.get(i).isDisplayed();
							// System.out.println("status of result: " +
							// resultflag);
							if (resultflag) {
								// System.out.println(results.get(i).getText());
								results.get(i).click();
								ExtentTestManager.getTest().log(LogStatus.PASS,
										interfacenumber + " is selected under 'Add DSLAM and Device' page");
								Report.LogInfo("INFO",
										interfacenumber + " is selected under 'Add DSLAM and Device' page", "PASS");
								// Clickon(getwebelement("//span[text()='Next']"));
								webDriver.findElement(By.xpath("//span[text()='Next']")).click();
								waitForAjax();
							}

						} catch (StaleElementReferenceException e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
							results = webDriver
									.findElements(By.xpath("//div[contains(text(),'" + interfacenumber + "')]"));
							numofrows = results.size();
							// results.get(i).click();
							// Log.info("selected row is : " + i);
							Report.LogInfo("INFO", "failure while selecting interface to add with service", "FAIL");
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"failure while selecting interface to add with service");
						}
					}
					break ab;
				}
			}
		} else {

			// System.out.println("No values available in table");
			Report.LogInfo("INFO", "No Interfaces got fetched", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, " NO interfaces got fetched");
		}

	}

	public void showInterface_ActelisConfiguuration(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		if (ServiceSubType.contains("IPVPN")) {
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/showInterface_ActelisCnfiguration")));
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.showInterface_ActelisCnfiguration,
					"Show Interface Actelis Cofiguration");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.showInterface_ActelisCnfiguration,
					"Show Interface Actelis Cofiguration");

			waitForAjax();

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}

	}

	public void deletInterface_ActelisConfiguration(String application, String interfaceName, String ServiceSubType)
			throws InterruptedException, IOException {
		if (ServiceSubType.contains("IPVPN")) {
			// select the interface
			// Clickon(getwebelement("//div[text()='"+ interfaceName +"']"));
			webDriver.findElement(By.xpath("//div[text()='" + interfaceName + "']")).click();

			// click on Action button
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/AcionButton_ActelisConfiguration")));
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AcionButton_ActelisConfiguration,
					"Action Button Actelis Configuration");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AcionButton_ActelisConfiguration,
					"Action Button Actelis Configuration");

			// Remove Button
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/removeButton_ActelisConfiguration")));
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.removeButton_ActelisConfiguration,
					"Remove Button Actelis Configuration");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.removeButton_ActelisConfiguration,
					"Remove Button Actelis Configuration");

			boolean popupMessage = false;
			popupMessage = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.popupMessage_forRemove_ActelisConfiguration)
							.isDisplayed();

			if (popupMessage) {
				String actualmsg = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.popupMessage_forRemove_ActelisConfiguration)
								.getText();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" On clicking remoe button, popup message displays as: " + actualmsg);
				Report.LogInfo("INFO", " On clicking remoe button, popup message displays as: " + actualmsg, "PASS");

				// Clickon(getwebelement("//button[@class='btn btn-danger']"));
				webDriver.findElement(By.xpath("//button[@class='btn btn-danger']")).click();
				waitForAjax();
			} else {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" popup message does not display after clicking on 'Remove' button");
				Report.LogInfo("INFO", " popup message does not display after clicking on 'Remove' button", "PASS");
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}

	}

	public void successMessage_deleteInterfaceFromDevice_ActelisConfiguration(String testDataFile, String sheetName,
			String scriptNo, String dataSetNo) throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		if (ServiceSubType.contains("IPVPN")) {
			boolean successMessage = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.successMessage_ActelisConfiguration_removeInterface)
							.isDisplayed();
			String actualmessage = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.successMessage_ActelisConfiguration_removeInterface)
							.getText();
			if (successMessage) {

				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Success Message for removing Interface is dipslaying as expected");
				Report.LogInfo("INFO", " Success Message for removing interface is dipslaying as expected", "PASS");

				ExtentTestManager.getTest().log(LogStatus.PASS, "Message displays as: " + actualmessage);
				Report.LogInfo("INFO", "Message displays as: " + actualmessage, "PASS");

			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" Success Message for removing Interface is not dipslaying");
				Report.LogInfo("INFO", " Success Message for removing Interface is not dipslaying", "PASS");
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}

	}

	public void DeleteVPNSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");

		if (ServiceSubType.contains("IPVPN")) {

			WebElement VPNSiteOrder_header = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrolltoview(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			// System.out.println("Numberofuser="+ NoOfUsers);

			if (NoOfUsers == 1) {
				// click(application, "VPN Order Radio button",
				// "VPNSiteOrderRadio");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
						"VPN Order Radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");

				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiteOrder
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			}
			
			/*
			// click(application, "Action dropdown", "VPNSiteactionbutton");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");

			// ClickCommon(application, "Delete", "AddVPNSiteCommonLink");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "Delete");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "Delete");
			*/
			
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderDeleteLink, "Delete");

			waitForAjax();
			waitforPagetobeenable();

			waitForAjax();
			// click(application, "Delete", "DeleteAny");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");

			waitForAjax();
			waitforPagetobeenable();

			// GetText(application, "Delete Interface", "SuccessMessageCom");
			compareText("Delete VPN order", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Site Order deleted successfully");
			waitForAjax();

		}

	}

	public void DeleteVPNSiteOrder4(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");

		if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2") || ServiceSubType.equalsIgnoreCase("CPE Solutions L3")
				|| ServiceSubType.equalsIgnoreCase("IP Voice") || ServiceSubType.equalsIgnoreCase("SwiftNet")) {
			if (!ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				WebElement VPNSiteOrder_header = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
				scrolltoview(VPNSiteOrder_header);
				// click( "VPN Device Delete Link", "VPN_Deletedevice1");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Deletedevice1,
						"VPN Device Delete Link");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_Deletedevice1, "VPN Device Delete Link");
				waitForAjax();
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				waitForAjax();
				waitforPagetobeenable();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom);

				waitForAjax();

				scrolltoview(VPNSiteOrder_header);
				// click( "VPN Site Delete Link", "VPN_DeleteSite");

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteSite,
						"VPN Site Delete Link");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteSite, "VPN Site Delete Link");

				waitForAjax();
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				waitForAjax();
				waitforPagetobeenable();

				compareText("Delete Site Order", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
						"Site Order deleted successfully");
				waitForAjax();

			} else {
				// WebElement HUB_header= findWebElement("//div[text()='Hub']");
				WebElement HUB_header = webDriver.findElement(By.xpath("//div[text()='Hub']"));
				scrolltoview(HUB_header);
				// click( "Spoke Device Delete Link", "VPN_DeleteDeviceSpoke");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteDeviceSpoke,
						"Spoke Device Delete Link");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteDeviceSpoke,
						"Spoke Device Delete Link");
				waitForAjax();
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				waitForAjax();
				waitforPagetobeenable();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom);

				waitForAjax();

				scrolltoview(HUB_header);
				// click( "Spoke Site Delete Link", "VPN_DeleteSiteSpoke");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteSiteSpoke,
						"Spoke Site Delete Link");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteSiteSpoke,
						"Spoke Site Delete Link");
				waitForAjax();
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				waitForAjax();
				waitforPagetobeenable();
				;
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom);

				waitForAjax();

				scrolltoview(HUB_header);
				// click( "Hub Device Delete Link", "VPN_DeleteDeviceHub");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteDeviceHub,
						"Hub Device Delete Link");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteDeviceHub,
						"Hub Device Delete Link");
				waitForAjax();
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				waitForAjax();
				waitforPagetobeenable();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom);

				waitForAjax();

				scrolltoview(HUB_header);
				// click( "Hub Site Delete Link", "VPN_DeleteSiteHub");
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteSiteHub,
						"Hub Site Delete Link");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_DeleteSiteHub, "Hub Site Delete Link");

				waitForAjax();
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
				waitForAjax();
				waitforPagetobeenable();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom);

				waitForAjax();

			}

		}
	}

	public void AddMultilinkCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String CPELink = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "multilink_Link");

		String CpePrimary_Backup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpePrimary/Backup");
		String CpenterfaceDirection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpenterfaceDirection");
		String CpeVoiceline = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeVoiceline");
		String CpeIVManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeIVManagement");
		String CpeIVBitCounter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeIVBitCounter");
		// String CpeAddRange=DataMiner.fngetcolvalue(testDataFile, sheetName,
		// scriptNo, dataSetNo, "CpeAddRange");

		String CpeSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeSpeed");
		String CpeDuplex = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeDuplex");
		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeInterface");

		String CpeInterfaceAddRangeText = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeMultilinkAddRangeText");
		String CpeAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeAddress");
		String CPEGetAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceGetAddress");

		String CpeAddInterfaceButton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CpeAddInterfaceButton");
		String InterfaceCNGReferance = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"InterfaceCNGReferance");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String MultilinkEthernetCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"MultilinkEthernetCheckbox");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");

			}

			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton,
			// "Action dropdown");
			// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,"View
			// ");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			// scrolltoend();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			System.out.println("Numberofuser=" + NoOfDevice);

			if (NoOfDevice == 1) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				waitForAjax();
			} else if (NoOfDevice >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']"));
				AddedUser.click();
				Report.LogInfo("INFO", "Step : clicked on Existing CPE Device View button", "PASS");
			} else {
				Report.LogInfo("INFO", "Step : No Device displayed", "PASS");

			}
			waitforPagetobeenable();

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButton);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionCommonLink1 + "Add Multlink "
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionCommonLink2,
					"Add interface/link ");
			waitforPagetobeenable();

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CancelCPEDevice, "Cancel button");
			// Thread.sleep(3000);
			waitforPagetobeenable();
			scrollUp();
			// Thread.sleep(2000);
			waitforPagetobeenable();
			WebElement Interface_header1 = webDriver.findElement(By.xpath("//div[text()='Routes']"));
			scrollIntoView(Interface_header1);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionCommonLink1 + "Add Multlink "
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionCommonLink2,
					"Add multi link ");
			waitforPagetobeenable();

			scrollUp();

			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Interface"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					CpeInterface, "Interface");

			if (CPEGetAddress.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetAddressCPE, "Get Address button");
				waitForAjax();

				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AllocateSubnetButton,
						"Allocate Subnet button");
				waitForAjax();
				waitforPagetobeenable();
				// addDropdownValues_common(application, "Interface Address
				// Range", "SelectValueDropdown", CpeInterfaceAddRangeDdn, xml);

				WebElement arrowbadgebadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[1]"));
				arrowbadgebadgesecondary.click();
				waitForAjax();
				waitforPagetobeenable();
				addDropdownValues_commonMethod("Address",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Address"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeAddress);

			} else {
				
			}
			if (MultilinkEthernetCheckbox.equalsIgnoreCase("Yes")) {

				// click(
				// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon,"Ethernet");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "Ethernet"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "Ethernet");

				Thread.sleep(2000);

				WebElement Speed = webDriver.findElement(By.xpath("//div[div[label[text()='Speed']]]//select"));
				Speed.click();
				waitForAjax();
				WebElement CpeSpeed1 = webDriver.findElement(
						By.xpath("//div[div[label[text()='Speed']]]//select//option[@label='" + CpeSpeed + "']"));
				CpeSpeed1.click();
				Report.LogInfo("INFO", "Speed" + " dropdown value selected as: " + CpeSpeed, "PASS");
				WebElement Duplex = webDriver.findElement(By.xpath("//div[div[label[text()='Duplex']]]//select"));

				Duplex.click();

				waitForAjax();

				WebElement CpeDuplex1 = webDriver.findElement(
						By.xpath("//div[div[label[text()='Duplex']]]//select//option[@label='" + CpeDuplex + "']"));

				CpeDuplex1.click();
				Report.LogInfo("INFO", "Duplex" + " dropdown value selected as: " + CpeDuplex, "PASS");

			}

			WebElement PrimaryBackup = webDriver
					.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select"));
			PrimaryBackup.click();

			waitForAjax();
			WebElement CpePrimaryBackup = webDriver.findElement(By.xpath(
					"//div[div[label[text()='Primary/Backup']]]//select//option[@label='" + CpePrimary_Backup + "']"));
			CpePrimaryBackup.click();

			Report.LogInfo("INFO", "Primary/Backup" + " dropdown value selected as: " + CpePrimary_Backup, "PASS");
			WebElement InterfaceDirection = webDriver
					.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select"));
			Thread.sleep(2000);
			InterfaceDirection.click();

			WebElement InterfaceDirection1 = webDriver
					.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select//option[@label='"
							+ CpenterfaceDirection + "']"));
			InterfaceDirection1.click();

			Report.LogInfo("INFO", "Interface Direction" + " dropdown value selected as: " + CpenterfaceDirection,
					"PASS");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Voice Line Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					CpeVoiceline, "Voice Line Reference");

			if (ServiceSubType.equalsIgnoreCase("IPVPN IPSec") || ServiceSubType.equalsIgnoreCase("IPVPN Connect")) {
				//addDropdownValues_commonMethod("CNG Reference",
						//APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "CNG Reference"
								//+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						//InterfaceCNGReferance);
			}

			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton, "Add button");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
						CpeInterfaceAddRangeText, "Link");

			} else {
				addDropdownValues_commonMethod("Link",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPELink);
			}
			if (CpeIVManagement.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV management"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV management");
			}
			if (CpeIVBitCounter.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV 64 Bit Counter"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV 64 Bit Counter");
			}
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton2);
			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton2, "Add button");

				WebElement Remove = webDriver.findElement(By.xpath("//span[text()='Remove']"));
				Remove.click();

				waitForAjax();
				

				waitForAjax();
				waitforPagetobeenable();

			}
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.multilink_okButton);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.multilink_okButton, "ok button");
			Thread.sleep(3000);
			waitforPagetobeenable();
			compareText("Add CPE Device",APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Multilink interface successfully created.");
			Thread.sleep(2000);
		} else {
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}
	}

	public void EditMultilinkCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String CPELink = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");

		String CpePrimary_Backup = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpenterfaceDirection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpeVoiceline = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeIVManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeIVBitCounter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeAddRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");

		String CpeSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeDuplex = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");

		String CpeInterfaceAddRangeText = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String CpeAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String CPEGetAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");

		String CpeAddInterfaceButton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String InterfaceCNGReferance = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPEBearerType");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String MultilinkEthernetCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"MultilinkEthernetCheckbox");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader)
					.replace("value", VPNSiteOrderHeader);
			ScrollIntoViewByString(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");

			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "View ");

			waitforPagetobeenable();
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPEShowInterfaces);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPEShowInterfaces, "Show Interface Link");
			waitForAjax();
			WebElement InterfaceRadio = webDriver.findElement(
					By.xpath("//div[contains(text(),'" + "Multilink" + CpeInterface + "')]//parent::div//div//input"));
			InterfaceRadio.click();
			Report.LogInfo("INFO", "Step : clicked on Existing Interface radio button", "PASS");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButtonCPE,
					"Interface Action Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceEditLink, "Interface Edit Link");
			waitForAjax();
			waitforPagetobeenable();

			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Interface"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					CpeInterface, "Interface");
			if (CPEGetAddress.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.GetAddressCPE, "Get Address button");
				waitForAjax();
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AllocateSubnetButton,
						"Allocate Subnet button");
				waitForAjax();
				waitforPagetobeenable();
				// addDropdownValues_common(application, "Interface Address
				// Range", "SelectValueDropdown", CpeInterfaceAddRangeDdn, xml);

				WebElement arrowbadgebadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[1]"));
				arrowbadgebadgesecondary.click();

				waitForAjax();
				waitforPagetobeenable();
				addDropdownValues_commonMethod("Address",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Address"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CpeAddress);

			} else {
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Interface Address Range"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						CpeInterfaceAddRangeText, "Interface Address Range");

				// EnterTextValueCommon(application, CpeInterfaceAddRangeText,
				// "Interface Address Range", "TextValueCommon");
				WebElement arrowbadgebadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[1]"));
				arrowbadgebadgesecondary.click();
				waitForAjax();
				waitforPagetobeenable();
				// EnterTextValueCommon(application, CpeAddress, "Address",
				// "TextValueCommon");

			}
			// ScrolltoElementComm(application, "Bearer Type",
			// "SelectValueDropdown");
			if (MultilinkEthernetCheckbox.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "Ethernet"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "Ethernet");

				Thread.sleep(2000);

				WebElement Speed = webDriver.findElement(By.xpath("//div[div[label[text()='Speed']]]//select"));
				Speed.click();
				waitForAjax();
				WebElement CpeSpeed1 = webDriver.findElement(
						By.xpath("//div[div[label[text()='Speed']]]//select//option[@label='" + CpeSpeed + "']"));
				CpeSpeed1.click();
				Report.LogInfo("INFO", "Speed" + " dropdown value selected as: " + CpeSpeed, "PASS");
				WebElement Duplex = webDriver.findElement(By.xpath("//div[div[label[text()='Duplex']]]//select"));

				Duplex.click();

				waitForAjax();

				WebElement CpeDuplex1 = webDriver.findElement(
						By.xpath("//div[div[label[text()='Duplex']]]//select//option[@label='" + CpeDuplex + "']"));

				CpeDuplex1.click();
				Report.LogInfo("INFO", "Duplex" + " dropdown value selected as: " + CpeDuplex, "PASS");

			}

			WebElement PrimaryBackup = webDriver
					.findElement(By.xpath("//div[div[label[text()='Primary/Backup']]]//select"));
			PrimaryBackup.click();

			waitForAjax();
			WebElement CpePrimaryBackup = webDriver.findElement(By.xpath(
					"//div[div[label[text()='Primary/Backup']]]//select//option[@label='" + CpePrimary_Backup + "']"));
			CpePrimaryBackup.click();

			Report.LogInfo("INFO", "Primary/Backup" + " dropdown value selected as: " + CpePrimary_Backup, "PASS");
			WebElement InterfaceDirection = webDriver
					.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select"));
			Thread.sleep(2000);
			InterfaceDirection.click();

			WebElement InterfaceDirection1 = webDriver
					.findElement(By.xpath("//div[div[label[text()='Interface Direction']]]//select//option[@label='"
							+ CpenterfaceDirection + "']"));
			InterfaceDirection1.click();

			Report.LogInfo("INFO", "Interface Direction" + " dropdown value selected as: " + CpenterfaceDirection,
					"PASS");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Voice Line Reference"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
					CpeVoiceline, "Voice Line Reference");

			// EnterTextValueCommon(application, CpeVoiceline, "Voice Line
			// Reference", "TextValueCommon");
			if (ServiceSubType.equalsIgnoreCase("IPVPN IPSec") || ServiceSubType.equalsIgnoreCase("IPVPN Connect")) {
				addDropdownValues_commonMethod("CNG Reference",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "CNG Reference"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						InterfaceCNGReferance);
			}

			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton, "Add button");
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						CpeInterfaceAddRangeText, "Link");

			} else {
				addDropdownValues_commonMethod("Link",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Link"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						CPELink);
			}
			if (CpeIVManagement.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV management"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV management");

			}
			if (CpeIVBitCounter.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon1 + "IV 64 Bit Counter"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CheckboxCommon2, "IV 64 Bit Counter");

			}

			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton2);
			if (CpeAddInterfaceButton.equalsIgnoreCase("Yes")) {
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton2, "Add button");

				WebElement Remove = webDriver.findElement(By.xpath("//span[text()='Remove']"));
				Remove.click();

				waitForAjax();
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddInterfacebutton2, "Add button");
				Thread.sleep(2000);
				sendKeys(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Address Range"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1,
						CpeAddRange, "Address Range");

				WebElement arrowbadgesecondary = webDriver
						.findElement(By.xpath("(//span[@class='arrow badge badge-secondary'])[2]"));
				arrowbadgesecondary.click();

				waitForAjax();
				waitforPagetobeenable();
				// EnterTextValueCommon(application, CpeSecondaryIp, "Secondary
				// IP", "TextValueCommon");
				// EnterTextValueCommon(application, CpeNetmask, "Netmask",
				// "TextValueCommon");

			}
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.multilink_okButton);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.multilink_okButton, "ok button");
			Thread.sleep(3000);
			waitforPagetobeenable();
			compareText(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Interface successfully created.", "Add CPE Device");
			Thread.sleep(2000);
		} else {
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}

	}

	public void DeleteMultilinkCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEBearerType");
		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNSiteOrder_header = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String VPNSiteOrderHeader = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPNSiteOrderHeader");
		String MultilinkEthernetCheckbox = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"MultilinkEthernetCheckbox");
		String CpeInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CpeInterface");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			// WebElement VPNSiteOrder_header=
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/VPNSiteOrderHeader"));

			VPNSiteOrder_header = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader)
					.replace("value", VPNSiteOrderHeader);
			ScrollIntoViewByString(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			System.out.println("Numberofuser=" + NoOfUsers);

			if (NoOfUsers == 1) {
				click("VPN Order Radio button", "VPNSiteOrderRadio");
				// Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");

			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink, "View ");

			waitforPagetobeenable();
			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPEShowInterfaces);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPEShowInterfaces, "Show Interface Link");
			waitForAjax();
			WebElement InterfaceRadio = webDriver.findElement(
					By.xpath("//div[contains(text(),'" + "Multilink" + CpeInterface + "')]//parent::div//div//input"));
			InterfaceRadio.click();
			Report.LogInfo("INFO", "Step : clicked on Existing Interface radio button", "PASS");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceActionButtonCPE,
					"Interface Action Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.InterfaceDeleteLink, "Interface Edit Link");
			waitForAjax();
			waitforPagetobeenable();

			waitForAjax();
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
			waitForAjax();
			waitforPagetobeenable();
			// GetText(application, "Delete Interface", "SuccessMessageCom");
			// compareText(application, "Delete Device", "SuccessMessageCom",
			// "Static Route successfully deleted.");
			waitForAjax();

		} else {
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}
	}

	public void AddRoutesCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Router_Id");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String RoutesDestination = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RoutesDestination");
		String RoutesNetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RoutesNetMask");
		String RoutesMetrics = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RoutesMetrics");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			WebElement VPNSiteOrder_header = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrolltoview(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			// System.out.println("Numberofuser="+ NoOfUsers);

			if (NoOfUsers == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
						"VPN Order Radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else { // Report.LogInfo("INFO", "Should not Present","PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			}

			/*
			 * verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * VPNSiteactionbutton);
			 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * VPNSiteactionbutton);
			 * 
			 * verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * AddVPNSiteCommonLink,"View");
			 * click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.
			 * VPNSiteactionbutton,"View");
			 */

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			scrolltoend();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			// System.out.println("Numberofuser="+ NoOfDevice);

			if (NoOfDevice == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				waitForAjax();
			} else if (NoOfDevice >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device View button");
				Report.LogInfo("INFO", "Step : clicked on Existing CPE Device View button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");
				Report.LogInfo("INFO", "Step : No Device displayed", "PASS");
			}
			waitforPagetobeenable();
			// WebElement Router_Tools= findWebElement("");
			WebElement Router_Tools = webDriver.findElement(By.xpath("//div[text()='Router Tools']"));
			scrolltoview(Router_Tools);

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionButton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionButton, "Action dropdown");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink1 + "Add Route"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink2, "Add Route");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink1 + "Add Route"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink2, "Add Route");

			waitforPagetobeenable();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Routes_okButton, "ok button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Routes_okButton, "ok button");
			waitForAjax();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Destination"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2, "Destination");
			verifyExists(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon1 + "Network Mask"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.WarningMessageCommon2,
					"Network Mask");

			// if(ServiceSubType.equalsIgnoreCase("IPVPN Connect")) {
			// verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon,"Gateway");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Gateway"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					"10.34.89.90", "Gateway");

			// }
			// verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon,"Destination");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Destination"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					RoutesDestination, "Destination");

			// verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon,
			// "Network Mask");
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Network Mask"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					RoutesNetMask, "Network Mask");

			// verifyExists(
			// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon,"Metrics"
			// );
			sendKeys(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Metrics"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					RoutesMetrics, "Metrics");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Routes_okButton, "ok button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Routes_okButton, "ok button");
			waitForAjax();
			waitforPagetobeenable();
			compareText("Add CPE Device", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Static Route successfully created");

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}
	}

	public void EditRoutesCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Router_Id");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String RoutesDestination = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RoutesDestination");
		String RoutesNetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RoutesNetMask");
		String RoutesMetrics = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RoutesMetrics");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			WebElement VPNSiteOrder_header = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrolltoview(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			// System.out.println("Numberofuser="+ NoOfUsers);

			if (NoOfUsers == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
						"VPN Order Radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			scrolltoend();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			// System.out.println("Numberofuser="+ NoOfDevice);

			if (NoOfDevice == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				waitForAjax();
			} else if (NoOfDevice >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device View button");
				Report.LogInfo("INFO", "Step : clicked on Existing CPE Device View button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");
				Report.LogInfo("INFO", "Step : No Device displayed", "PASS");
			}
			waitforPagetobeenable();
			WebElement Router_Tools = webDriver.findElement(By.xpath("//div[text()='Router Tools']"));
			scrolltoview(Router_Tools);

			WebElement ExistingRoutes = webDriver.findElement(
					By.xpath("(//div[text()='" + RoutesDestination + "']//parent::div//div//span//span//span)[2]"));
			ExistingRoutes.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing Routes Checkbox");
			Report.LogInfo("INFO", "Step : clicked on Existing Routes Checkbox", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionButton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionButton, "Action dropdown");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink1 + "Edit"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink2, "Edit ");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink1 + "Edit"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink2, "Edit Route");

			waitforPagetobeenable();

			ClearAndEnterTextValueComm("Destination",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1 + "Destination"
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					RoutesDestination);
			ClearAndEnterTextValueComm(
					"Network Mask", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
							+ "Network Mask" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2,
					RoutesNetMask);
			ClearAndEnterTextValueComm("Metrics", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon1
					+ "Metrics" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.TextValueCommon2, RoutesMetrics);

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Routes_okButton, "ok button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Routes_okButton, "ok button");
			waitForAjax();
			waitforPagetobeenable();
			compareText("Add CPE Device", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Static Route successfully updated");

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}
	}

	public void DeleteRoutesCPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Router_Id");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String RoutesDestination = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"RoutesDestination");
		String RoutesNetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RoutesNetMask");
		String RoutesMetrics = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RoutesMetrics");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			WebElement VPNSiteOrder_header = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrolltoview(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			// System.out.println("Numberofuser="+ NoOfUsers);

			if (NoOfUsers == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
						"VPN Order Radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			scrolltoend();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			// System.out.println("Numberofuser="+ NoOfDevice);

			if (NoOfDevice == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				waitForAjax();
			} else if (NoOfDevice >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device View button");
				Report.LogInfo("INFO", "Step : clicked on Existing CPE Device View button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");
				Report.LogInfo("INFO", "Step : No Device displayed", "PASS");
			}
			waitforPagetobeenable();
			WebElement Router_Tools = webDriver.findElement(By.xpath("//div[text()='Router Tools']"));
			scrolltoview(Router_Tools);
			waitForAjax();

			WebElement ExistingRoutes = webDriver.findElement(
					By.xpath("(//div[text()='" + RoutesDestination + "']//parent::div//div//span//span//span)[2]"));
			ExistingRoutes.click();
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing Routes Checkbox");
			Report.LogInfo("INFO", "Step : clicked on Existing Routes Checkbox", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionButton, "Action dropdown");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionButton, "Action dropdown");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink1 + "Delete"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink2, "Delete ");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink1 + "Delete"
					+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RoutesActionCommonLink2, "Delete Route");

			waitForAjax();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");

			waitForAjax();
			waitforPagetobeenable();
			compareText("Add CPE Device", APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom,
					"Static Route successfully deleted.");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");

		}
	}

	public void AddRouterTool4(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Device_Country");
		String VPNDeviceCity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Device_City");
		String VPNPhysicalSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Physical_Site");
		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Vendor/Model");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");
		String VPNSiteAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VPN_Site_Alis");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router_Id");
		String RouterToolIPV4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV4");
		String RouterToolIPV6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV6");
		String HubSpoke = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "HUB/SPOKE");

		if (ServiceSubType.equalsIgnoreCase("CPE Solutions L2") || ServiceSubType.equalsIgnoreCase("CPE Solutions L3")
				|| ServiceSubType.equalsIgnoreCase("IP Voice") || ServiceSubType.equalsIgnoreCase("SwiftNet")) {
			if (!ServiceSubType.equalsIgnoreCase("SwiftNet")) {
				WebElement VPNSiteOrder_header = findWebElement(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
				scrolltoview(VPNSiteOrder_header);

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_viewdevice1, "VPN View Link");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPN_viewdevice1, "VPN View Link");
			} else {
				WebElement HUB_header = webDriver.findElement(By.xpath("//div[text()='Hub']"));
				scrolltoview(HUB_header);

				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Hub_viewdevice1, "VPN View Link ");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.Hub_viewdevice1, "VPN View Link ");
			}
			waitforPagetobeenable();
			waitForAjax();

			waitforPagetobeenable();
			scrollUp();
			String VendorName = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon1
					+ "Vendor/Model" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon2,
					"Vendor/Model");
			if (VendorName.contains("Cisco") || VendorName.contains("Huawei") || VendorName.contains("Cisco")) {

				WebElement CountryName = webDriver.findElement(By.xpath("//label[text()='Country']"));
				scrolltoview(CountryName);
				addDropdownValues_commonMethod("Command IPV4",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Command IPV4"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						RouterToolIPV4);
				// addDropdownValues_common(application, "Command IPV6",
				// "SelectValueDropdown", RouterToolIPV6, xml);
				WebElement Element = webDriver.findElement(By.xpath("//input[@id='routertools.hostnameOrIPAddress']"));

				if (Element.isDisplayed()) {
					// EnterTextValue("Colt@Colt.net", "Command IPV4"
					// ,APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolIPV4);
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolIPV4, "Command IPV4");
					sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolIPV4, "Colt@Colt.net",
							"Command IPV4");

				}
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolExecuteA, "Execute");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolExecuteA, "Execute");
				waitForAjax();
				// WebElement Element2 = driver
				// .findElement(By.xpath("//input[@id='routertools.hostnameOrIPv6Address']"));

				// if(Element2.isDisplayed()) {
				// EnterTextValue(application, "colt@Colt.net", "Command IPV6"
				// ,"RouterToolIPV6");
				// }
				// click(application, "Execute", "RouterToolExecuteB");
				waitForAjax();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolResult);
			}
		}
	}

	public void ClearAndEnterTextValueComm(String labelname, String xpath, String newValue) {
		WebElement element = null;
		try {
			// Thread.sleep(1000);
			element = findWebElement(xpath);
			String value = element.getAttribute("value");

			if (value.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.INFO, "Step: '" + labelname + "' text field is empty");
				Report.LogInfo("INFO", value, "INFO");

			} else {
				element.clear();
				waitForAjax();
				element.sendKeys(newValue);
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step: Entered '" + newValue + "' into '" + labelname + "' text field");
				Report.LogInfo("INFO", "Step: Entered '" + newValue + "' into '" + labelname + "' text field", "PASS");
			}

		} catch (Exception e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Not able to enter '" + newValue + "' into '" + labelname + "' text field");
			Report.LogInfo("INFO", "Not able to enter '" + newValue + "' into '" + labelname + "' text field", "FAIL");
			e.printStackTrace();
		}

	}

	public void AddRouterTool(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_Router Id");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN Site Order Num");
		String RouterToolIPV4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV4");
		String RouterToolIPV6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "RouterToolIPV6");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			WebElement VPNSiteOrder_header = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrolltoview(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			// System.out.println("Numberofuser="+ NoOfUsers);

			if (NoOfUsers == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
						"VPN Order Radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");
				Thread.sleep(3000);
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			scrolltoend();
			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			// System.out.println("Numberofuser="+ NoOfDevice);

			if (NoOfDevice == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.ViewCPEDevice, "View CPE Device");
				waitForAjax();
			} else if (NoOfDevice >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath(
						"//div[text()='Customer Premise Equipment ( CPE ) ']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'1')]/parent::span/parent::div//following-sibling::div//span[text()='View']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device View button");
				Report.LogInfo("INFO", "Step : clicked on Existing CPE Device View button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");
				Report.LogInfo("INFO", "Step : No Device displayed", "PASS");
			}
			waitforPagetobeenable();
			// scrollUp();
			String VendorName = getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon1
					+ "Vendor/Model" + APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CompareTextCommon2,
					"Vendor/Model");
			if (VendorName.contains("Cisco") || VendorName.contains("Huawei") || VendorName.contains("Cisco")) {

				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Command IPV4"
						+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2);
				addDropdownValues_commonMethod("Command IPV4",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Command IPV4"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						RouterToolIPV4);
				addDropdownValues_commonMethod("Command IPV6",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown1 + "Command IPV6"
								+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SelectValueDropdown2,
						RouterToolIPV6);
				WebElement Element = webDriver.findElement(By.xpath("//input[@id='routertools.hostnameOrIPAddress']"));

				if (Element.isDisplayed()) {
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolIPV4, "Command IPV4");
					sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolIPV4, "Colt@Colt.net",
							"Command IPV4");
				}
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolExecuteA, "Execute");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolExecuteA, "Execute");
				waitForAjax();
				WebElement Element2 = webDriver
						.findElement(By.xpath("//input[@id='routertools.hostnameOrIPv6Address']"));

				if (Element2.isDisplayed()) {
					verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolIPV6, "Command IPV6");
					sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolIPV6, "colt@Colt.net",
							"Command IPV6");
				}
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolExecuteB, "Execute");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolExecuteB, "Execute");
				waitForAjax();
				getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.RouterToolResult);
			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");
		}
	}

	public String addLoopback(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String interfaceAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addLoopback_interfaceAddress");

		String IVmanagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addLoopBack_Ivmanagement");

		String configureOnBackupBRX = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"addLoopBack_configureOnBackupBRX");

		waitForAjax();
		waitforPagetobeenable();
		scrolltoend();

		// Clickon(getwebelement(xml.getlocator("//locators/" + application +
		// "/addInterface_executeAndSaveButton")));
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
				"Execute And Save Button");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
				"Execute And Save Button");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
		Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");
		waitForAjax();

		// verify warning Message
		// warningMessage_commonMethod(application,
		// "interfaceAddress_warningMessage", "Interface Address", xml);
		verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_warningMessage,
				"Interface Address");

		// Interface Name
		String interfaceName = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addLoopback_interfaceTextField)
						.getAttribute("value");

		// Interface Address text Field
		addtextFields_commonMethod("Interface Address",
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.interfaceAddress_TextField, interfaceAddress);

		// IV Management
		addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.IVmanagement_checkbox,
				"IV Management", IVmanagement);

		scrolltoend();
		waitForAjax();

		// Configure on backup BRX
		addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.configureOnBackupBRX_checkbox,
				"Configure on backup BRX", configureOnBackupBRX);

		// Generate Configuration
		ExtentTestManager.getTest().log(LogStatus.INFO, "'Generate Configuration' under 'Add Multilink' Page");
		Report.LogInfo("INFO", "'Generate Configuration' under 'Add Multilink' Page", "INFO");
		// perform Generate configuration
		boolean configurationpanel = false;
		configurationpanel = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_confiugrationPanelheader)
						.isDisplayed();
		if (configurationpanel) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Configuration' panel is displaying");
			Report.LogInfo("INFO", "'Configuration' panel is displaying", "PASS");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_generateLink, "Generate");
			waitForAjax();

			scrolltoend();
			waitForAjax();

			String configurationvalues = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_configurationtextArea);
			if (configurationvalues.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box");
				Report.LogInfo("INFO",
						"After clicking on 'Generate' button, no values displaying under 'Configuration' text box",
						"FAIL");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues);
				Report.LogInfo("INFO", "After clicking on 'Generate' button, "
						+ "under 'Configuration' textbox values displaying as: " + configurationvalues, "PASS");
			}

			waitForAjax();
			scrolltoend();
			// click_commonMethod(application, "Execute and Save",
			// "addInterface_executeAndSaveButton" , xml);
			// Clickon(getwebelement(xml.getlocator("//locators/" + application
			// + "/addInterface_executeAndSaveButton")));
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
					"Execute And Save Button");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addInterface_executeAndSaveButton,
					"Execute And Save Button");

			ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'Execute and Save' button");
			Report.LogInfo("INFO", "Clicked on 'Execute and Save' button", "PASS");
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Configuration' panel is not displaying");
			Report.LogInfo("INFO", "'Configuration' panel is not displaying", "FAIL");
		}

		return interfaceName;

	}

	public void DeleteCPEDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String ServiceSubType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		String VPNVendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNRouterId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String VPNManagementAdd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceSubType");
		String VPNSiterderNum = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String GetAddressCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String AvailableBlockCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceSubType");
		String PremiseName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String PremiseCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String PremiseToggle = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String JitterCPE = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String ConnectProtocoltal = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceSubType");
		String ConnectProtocolssh = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"ServiceSubType");
		String Premise = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");
		String SNMP3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceSubType");

		if (ServiceSubType.contains("IPVPN") && !ServiceSubType.equalsIgnoreCase("IPVPN Access")) {

			WebElement VPNSiteOrder_header = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderHeader);
			scrolltoview(VPNSiteOrder_header);
			List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
					"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
			int NoOfUsers = ExistingUsers.size();
			// System.out.println("Numberofuser="+ NoOfUsers);

			if (NoOfUsers == 1) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,
						"VPN Order Radio button");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio, "VPN Order Radio button");

				waitForAjax();
			} else if (NoOfUsers >= 1) {
				WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiterderNum
						+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Step : clicked on Existing VPN Site Order radio button");
				Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");
				Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			}

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

			waitforPagetobeenable();
			// compareText_Common(application, "Site Order Number",
			// "CompareTextCommon", "View VPN Order", xml);
			scrolltoend();

			List<WebElement> ExistingDevices = webDriver.findElements(By.xpath(
					"//div[text()='Customer Premise Equipment ( CPE ) ']//parent::div//following-sibling::div[@class='div-margin row']"));
			int NoOfDevices = ExistingDevices.size();
			int NoOfDevice = NoOfDevices - 1;
			// System.out.println("Numberofuser="+ NoOfDevice);
			waitForAjax();
			if (NoOfDevice < 5) {
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteCPE, "Delete CPE Device");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteCPE, "Delete CPE Device");
				waitForAjax();
			} else if (NoOfDevice > 5) {

				WebElement AddedUser = webDriver.findElement(By.xpath("//span[contains(text(),'" + VPNManagementAdd
						+ "')]//parent::div//parent::div//div//span[text()='Delete']"));
				AddedUser.click();
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing CPE Device Edit button");
				Report.LogInfo("INFO", "Step : clicked on Existing CPE Device Edit button", "PASS");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Device displayed");
				Report.LogInfo("INFO", "Step : No Device displayed", "PASS");
			}

			waitforPagetobeenable();

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.DeleteAny, "Delete");
			waitForAjax();
			waitforPagetobeenable();
			getTextFrom(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.SuccessMessageCom, "Delete Interface");
		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Should not Present");
			Report.LogInfo("INFO", "Should not Present", "PASS");

		}
	}

	public void pppConfiguration() throws InterruptedException, IOException {

		waitforPagetobeenable();

		WebElement pppConfigurationPanelHeader = findWebElement(
				APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pppConfigurationPanelheader);

		if (pppConfigurationPanelHeader.isDisplayed()) {
			ExtentTestManager.getTest().log(LogStatus.PASS, " 'PPP Configuration' panel is displaying");
			Report.LogInfo("INFO", " 'PPP Configuration' panel is displaying", "PASS");

			boolean clickHereLink = findWebElement(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pppConfiguration_CPEdevice_clickHereLink)
							.isDisplayed();
			if (clickHereLink) {
				verifyExists(
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pppConfiguration_CPEdevice_clickHereLink,
						"Click here to add");
				click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pppConfiguration_CPEdevice_clickHereLink,
						"Click here to add");
			} else {

				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No links displayed for creating new PPP Configuration");
				Report.LogInfo("INFO", "No links displayed for creating new PPP Configuration", "FAIL");
			}

		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, " 'PPP Configuration' panel is not displaying");
			Report.LogInfo("INFO", " 'PPP Configuration' panel is not displaying", "FAIL");

		}
	}

	public static String newordernumber, newVoiceLineNumber;

	public void createneworderservice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String neworder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderService");
		String neworderno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewRFIREQNumber");

		if (neworder.equalsIgnoreCase("YES")) {

			WebElement CreateOrder_Header = webDriver.findElement(By.xpath("//div[text()='Create Order / Service']"));
			scrolltoview(CreateOrder_Header);
			waitForAjax();

			// click(application, "select order switch", "selectorderswitch");
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.newordertextfield,
					"Order/Contract Number");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.newordertextfield, neworderno,
					"Order/Contract Number");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.newrfireqtextfield,
					"RFI Voice line Number");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.newrfireqtextfield, newrfireqno,
					"RFI Voice line Number");

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.createorderbutton, "create order");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.createorderbutton, "create order");

			compareText("create order success message",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.OrderCreatedSuccessMsg,
					"Order created successfully");
			scrolltoview(CreateOrder_Header);

			newordernumber = neworderno;
			newVoiceLineNumber = newrfireqno;

		} else {

			ScrolltoElement(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.nextbutton);

			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectorderswitch, "select order switch");
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.selectorderswitch, "select order switch");

			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.OrdernumberDropdown, neworderno);
			// EnterTextValue(application, newrfireqno, "RFI Voice line Number",
			// "newrfireqtextfield");

			Report.LogInfo("INFO", "new order not selected", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Step : Existing Order Selected");
		}

	}

	public void CPEdevice_clickOnPPPconfiguration(String testDataFile, String sheetName, String scriptNo,
			String dataSetNo) throws InterruptedException, IOException {

		String existingdevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_Router_Id");
		String VPNSiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"VPN_Site_Order_Num");

		// scrolltoend();
		List<WebElement> ExistingUsers = webDriver.findElements(By.xpath(
				"//div[text()='VPN Site Orders']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']"));
		int NoOfUsers = ExistingUsers.size();
		System.out.println("Numberofuser=" + NoOfUsers);
		if (NoOfUsers == 1) {
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteOrderRadio,"VPNSiteOrderRadio");
			// Thread.sleep(3000);
		} else if (NoOfUsers >= 1) {
			WebElement AddedUser = webDriver.findElement(By.xpath("//div[contains(text(),'" + VPNSiteOrder
					+ "')]//parent::div//parent::div//preceding-sibling::div//span[@class='ag-selection-checkbox']"));
			AddedUser.click();
			Report.LogInfo("INFO", "Step : clicked on Existing VPN Site Order radio button", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on Existing VPN Site Order radio button");
		} else {
			Report.LogInfo("INFO", "Step : No Order displayed", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step : No Order displayed");

		}

		// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.VPNSiteactionbutton,
		// "VPNSiteactionbutton");

		// click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNSiteCommonLink,
		// "AddVPNSiteCommonLink");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.AddVPNOrderViewLink, "View");

		Report.LogInfo("INFO", "Verifying 'PPP configuration link'", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying 'PPP configuration link'");

		if (isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPE_existingDeviceGrid)) {

			List<WebElement> addeddevicesList = findWebElements(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPE_fetchAlldevice_InviewPage);
			int AddedDevicesCount = addeddevicesList.size();
			for (int i = 0; i < AddedDevicesCount; i++) {
				String AddedDeviceNameText = addeddevicesList.get(i).getText();
				String AddedDevice_SNo = AddedDeviceNameText.substring(0, 1);
				if (AddedDeviceNameText.contains(existingdevicename)) {

					click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPE_pppConfigurationLink1
							+ AddedDevice_SNo
							+ APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.CPE_pppConfigurationLink2,"CPE_pppConfigurationLink2");

					break;

				} else {
					Report.LogInfo("INFO", "Invalid device name", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Invalid device name");
				}

			}
		} else {
			Report.LogInfo("INFO", "No Device added in grid", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No Device added in grid");
		}
	}

	public void addPPPconfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String expectedDevicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_Router_Id");
		String framedWANipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppconfiguration_framedWANipAddress");
		String ipv6Parameter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_IPv6Parameters");
		String framedIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Address");
		String delegateIPv6Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_delegateIPv6Prefix");
		String framedRoute0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute0");
		String framedRoute1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute1");
		String framedRoute2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute2");
		String framedRoute3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute3");
		String framedRoute4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute4");
		String framedRoute5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute5");
		String framedRoute6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute6");
		String framedRoute7 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute7");
		String framedIPv6route0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6route0");
		String framedIPv6route1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6route1");
		String framedIPv6route2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route2");
		String framedIPv6route3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6route3");
		String framedIPv6route4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route4");
		String framedIPv6route5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route5");
		String framedIPv6route6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route6");
		String uniIPv6LocalInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_uniIPv6LocalInterface");
		String uniIPv6VirtualRouter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_uniIPv6VirtualRouter");
		String uniIngressStatistics = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_uniIngressStatistics");
		String uniQOSprofileName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_uniQOSprofileName");
		String QosParameters = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_selectQosParameters");
		String QosParametertextValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_QosParametertextValue");
		String SAvalidation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppconfig_Savalidation");
		String uniSphereEnable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_uniSphereEnable");
		String uniSphereVersion = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_uniSphereVersion");

		String actualSelectValueInsideQosParamert = "Null";

		Report.LogInfo("INFO", "Add PPP Configuration", "PASS");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Add PPP Configuration");

		// WebElement adpppConfigurationPanelHeader = getwebelement(
		// xml.getlocator("//locators/" + application +
		// "/addPPPconfigurationPanel_CPedevice"));

		// if (adpppConfigurationPanelHeader.isDisplayed())
		if (isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfigurationPanel_CPedevice)) {
			Report.LogInfo("INFO", " 'Add PPP Configuration' panel is displaying", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, " 'Add PPP Configuration' panel is displaying");
			// System.out.println(" 'Add PPP Configuration' panel is
			// displaying");

			// Device Name
			String actualDevicename = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfiguration_deviceName);
			if (actualDevicename.equalsIgnoreCase(expectedDevicename)) {
				Report.LogInfo("INFO", "Device name is displaying as " + actualDevicename + " as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Device name is displaying as " + actualDevicename + " as expected");
			} else if (actualDevicename.contains(expectedDevicename)) {
				Report.LogInfo("INFO", "Device name is displaying as " + actualDevicename + " as expected", "PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Device name is displaying as " + actualDevicename + " as expected");
			} else {
				Report.LogInfo("INFO", "Device name is displaying as " + actualDevicename
						+ ". But the expected value is " + expectedDevicename, "FAIL");

				ExtentTestManager.getTest().log(LogStatus.FAIL, "Device name is displaying as " + actualDevicename
						+ ". But the expected value is " + expectedDevicename);
			}

			// PPP Password
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfiguration_generatePassowrd,
					"Generate Passwprd");

			// String passwordField = getwebelement(xml.getlocator("//locators/"
			// + application +
			// "/addPPPconfiguration_passwordField")).getAttribute("value");
			String passwordField = getAttributeFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfiguration_passwordField, "value");
			if (passwordField.isEmpty()) {
				Report.LogInfo("INFO", "No value displaying under 'Password' field", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No value displaying under 'Password' field");
			} else {
				Report.LogInfo("INFO", "value displaying under 'Password' field is displaying as " + passwordField,
						"PASS");
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"value displaying under 'Password' field is displaying as " + passwordField);
			}

			// Framed/WAN/IP Address
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPcofig_famedWANipAddress,
					"Framed/WAN/IP Address");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPcofig_famedWANipAddress,
					framedWANipAddress);
			// addtextFields_commonMethod("Framed/WAN/IP Address",
			// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPcofig_famedWANipAddress,
			// framedWANipAddress);

			// Framed/WAN IP Subnet Mask
			// String framedWANorIPsubnetMask = getTextFrom(
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_framedWANIpAddress")));

			String framedWANorIPsubnetMask = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedWANIpAddress);

			if (framedWANorIPsubnetMask.equals("255.255.255.255")) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Framed/WAN IP Subnet Mask value is displaying as " + framedWANorIPsubnetMask + " as expected");
				System.out.println(
						"Framed/WAN IP Subnet Mask value is displaying as " + framedWANorIPsubnetMask + " as expected");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Framed/WAN IP Subnet Mask value is displaying as "
						+ framedWANorIPsubnetMask + ". Expected value is 'PPP'");
				System.out.println("Framed/WAN IP Subnet Mask value is displaying as " + framedWANorIPsubnetMask
						+ ". Expected value is 'PPP'");
			}

			// IPv6 Parameter
			addCheckbox_commonMethod(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_IPv6Paramert,
					"IPv6 parameters", ipv6Parameter);

			if (ipv6Parameter.equalsIgnoreCase("Yes")) {

				// Framed-IPv6-Prefix
				// verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addConfig_framedIPvPrefix,"addConfig_framedIPvPrefix");
				// sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addConfig_framedIPvPrefix,framedIPv6Prefix);

				// addtextFields_commonMethod("Framed-IPv6-Prefix ",
				// "addConfig_framedIPvPrefix", framedIPv6Prefix);

				// Framed-IPv6-Address
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPV6address,
						"addPPPconfig_framedIPV6address");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPV6address,
						framedIpv6Address);

				// addtextFields_commonMethod("Framed-IPv6-Address",
				// "addPPPconfig_framedIPV6address", framedIpv6Address);

				// Delegate-IPv6-Prefix:
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_delegateIPv6Prefix,
						"addPPPconfig_delegateIPv6Prefix");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_delegateIPv6Prefix,
						delegateIPv6Prefix);

				// addtextFields_commonMethod("Delegate-IPv6-Prefix:",
				// "addPPPconfig_delegateIPv6Prefix", delegateIPv6Prefix);

				// WebElement framedProtocolElement =
				// getwebelement(xml.getlocator("//locators/" + application +
				// "/addPPPconfig_framedProtocol"));
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedProtocol);
			}

			// Framed Protocol
			// String framedProtocol = getTextFrom(
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_framedProtocol")));

			String framedProtocol = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedProtocol);
			if (framedProtocol.equals("PPP")) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Framed protocol value is displaying as " + framedProtocol + " as expected");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Framed protocol value is displaying as " + framedProtocol + ". Expected value is 'PPP'");

			}

			// Framed Route0
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRouter0,
					"addPPPconfig_framedRouter0");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRouter0, framedRoute0);

			// addtextFields_commonMethod("Framed Route0",
			// "addPPPconfig_framedRouter0", framedRoute0);

			// Framed Router1
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRouter1,
					"addPPPconfig_framedRouter1");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRouter1, framedRoute1);

			// addtextFields_commonMethod("Framed Route1",
			// "addPPPconfig_framedRouter1", framedRoute1);

			// Framed Route2
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedroute2,
					"addPPPconfig_framedroute2");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedroute2, framedRoute2);

			// addtextFields_commonMethod("Framed Route2",
			// "addPPPconfig_framedroute2", framedRoute2);

			// Framed Route3
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute3,
					"addPPPconfig_framedRoute3");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute3, framedRoute3);

			// addtextFields_commonMethod("Framed Route3",
			// "addPPPconfig_framedRoute3", framedRoute3);

			// Framed Route4
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPconfig_framedRoute4,
					"addPPconfig_framedRoute4");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPconfig_framedRoute4, framedRoute4);

			// addtextFields_commonMethod("Framed Route4",
			// "addPPconfig_framedRoute4", framedRoute4);

			// Framed Router5
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute5,
					"addPPPconfig_framedRoute5");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute5, framedRoute5);

			// addtextFields_commonMethod("Framed Route5",
			// "addPPPconfig_framedRoute5", framedRoute5);

			/*
			 * if(ipv6Parameter.equalsIgnoreCase("Yes")) {
			 * Log.info("no scroll down"); }else { scrolltoend();
			 * //Thread.sleep(1000); }
			 */

			// Framed route6
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute6,
					"addPPPconfig_framedRoute6");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute6, framedRoute6);

			// addtextFields_commonMethod("Framed Route6",
			// "addPPPconfig_framedRoute6", framedRoute6);

			// Framed route7
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute7,
					"addPPPconfig_framedRoute7");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute7, framedRoute7);

			// addtextFields_commonMethod("Framed Route7",
			// "addPPPconfig_framedRoute7", framedRoute7);

			if (ipv6Parameter.equalsIgnoreCase("Yes")) {

				// Framed-IPv6-Route0
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route0,
						"addPPPconfig_framedIPv6route0");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route0,
						framedIPv6route0);

				// addtextFields_commonMethod("Framed-IPv6-Route0",
				// "addPPPconfig_framedIPv6route0", framedIPv6route0);

				// Framed-IPv6-Route1
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route1,
						"addPPPconfig_framedIPv6route1");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route1,
						framedIPv6route1);

				// addtextFields_commonMethod("Framed-IPv6-Route1",
				// "addPPPconfig_framedIPv6route1", framedIPv6route1);

				// Framed-IPv6-Route2
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route2,
						"addPPPconfig_framedIPv6route2");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route2,
						framedIPv6route2);

				// addtextFields_commonMethod("Framed-IPv6-Route2",
				// "addPPPconfig_framedIPv6route2", framedIPv6route2);

				// Framed-IPv6-Route3
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route3,
						"addPPPconfig_framedIPv6Route3");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route3,
						framedIPv6route3);

				// addtextFields_commonMethod("Framed-IPv6-Route3",
				// "addPPPconfig_framedIPv6Route3", framedIPv6route3);

				// Framed-IPv6-Route4
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route4,
						"addPPPconfig_framedIPv6route4");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route4,
						framedIPv6route4);

				// addtextFields_commonMethod("Framed-IPv6-Route4",
				// "addPPPconfig_framedIPv6route4", framedIPv6route4);

				// Framed-IPv6-Route5
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route5,
						"addPPPconfig_framedIPv6Route5");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route5,
						framedIPv6route5);

				// addtextFields_commonMethod("Framed-IPv6-Route5",
				// "addPPPconfig_framedIPv6Route5", framedIPv6route5);

				// Framed-IPv6-Route7
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route7,
						"addPPPconfig_framedIPv6Route7");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route7,
						framedIPv6route6);

				// addtextFields_commonMethod("Framed-IPv6-Route7",
				// "addPPPconfig_framedIPv6Route7", framedIPv6route6);

				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6LocalInterface);

				// Uni-IPv6-Local-Interface (eg. loopback10)
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6LocalInterface,
						"addPPPconfig_uniIPv6LocalInterface");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6LocalInterface,
						uniIPv6LocalInterface);

				// addtextFields_commonMethod("Uni-IPv6-Local-Interface",
				// "addPPPconfig_uniIPv6LocalInterface", uniIPv6LocalInterface);

				// Uni-IPv6-Virtual-Router (eg. TEST3BGP)
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6Virtualrouter,
						"addPPPconfig_uniIPv6Virtualrouter");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6Virtualrouter,
						uniIPv6VirtualRouter);

				// addtextFields_commonMethod("Uni-IPv6-Virtual-Router",
				// "addPPPconfig_uniIPv6Virtualrouter", uniIPv6VirtualRouter);

				// Uni-Ingress-Statistics (eg. 1)
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIngressStatistics,
						"addPPPconfig_uniIngressStatistics");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIngressStatistics,
						uniIngressStatistics);

				// addtextFields_commonMethod("Uni-Ingress-Statistics",
				// "addPPPconfig_uniIngressStatistics", uniIngressStatistics);

			}

			// Uni Virtual Route Name
			// String uniVirtualActualValue =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_uniVirtualRouterName")).getAttribute("value");
			String uniVirtualActualValue = getAttributeFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniVirtualRouterName, "value");
			if (uniVirtualActualValue.isEmpty()) {
				Report.LogInfo("INFO", "No values displaying under 'Uni Virtual Route Name'", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Uni Virtual Route Name'");

			} else {
				if (uniVirtualActualValue.equalsIgnoreCase("MGMT")) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Uni Virtual Route Name Value displays as 'MGMT' as expected");
				} else {
					Report.LogInfo("INFO", "Uni Virtual Route Name mismataches. It displays as: "
							+ uniVirtualActualValue + " .The Expected value is 'MGMT'", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Uni Virtual Route Name mismataches. It displays as: " + uniVirtualActualValue
									+ " .The Expected value is 'MGMT'");
				}
			}

			// Uni Local Loopback Interface
			// String uniLoopBackInterface =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_uniLocalLoopbackInterface")).getAttribute("value");
			String uniLoopBackInterface = getAttributeFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniLocalLoopbackInterface,
					"value");
			if (uniLoopBackInterface.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Uni Local Loopback Interface field");
				Report.LogInfo("INFO", "No values displaying under 'Uni Local Loopback Interface field", "FAIL");
			} else {
				if (uniLoopBackInterface.equalsIgnoreCase("Loopback999")) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Uni Local Loopback Interface Value displays as 'Loopback999' as expected");
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Uni Local Loopback Interface value mismataches. It displays as: " + uniLoopBackInterface
									+ " .The Expected value is 'Loopback999'");
					Report.LogInfo("INFO", "Uni Local Loopback Interface value mismataches. It displays as: "
							+ uniLoopBackInterface + " .The Expected value is 'Loopback999'", "FAIL");

				}
			}

			// Uni Ingress Policy
			// String UNIingresspolicy =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/uniIngressPolicy_textField")).getAttribute("value");
			String UNIingresspolicy = getAttributeFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.uniIngressPolicy_textField, "value");

			if (UNIingresspolicy.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Uni Ingress Policy' field");
				Report.LogInfo("INFO", "No values displaying under 'Uni Ingress Policy' field", "FAIL");

			} else {
				if (UNIingresspolicy.equalsIgnoreCase("VPN-DEFAULT")) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Uni Ingress Policy Value displays as 'VPN-DEFAULT' as expected");
				} else {
					Report.LogInfo("INFO", "Uni Ingress Policy value mismataches. It displays as: " + UNIingresspolicy
							+ " .The Expected value is 'VPN-DEFAULT'", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Uni Ingress Policy value mismataches. It displays as: " + UNIingresspolicy
									+ " .The Expected value is 'VPN-DEFAULT'");
				}
			}

			// Uni Qos Profile Name
			addDropdownValues_commonMethod(" Uni Qos Profile Name",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniQOSprofileName,
					uniQOSprofileName);

			// Qos Parameters
			addDropdownValues_commonMethod("Qos Parameters",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_QosParameters, QosParameters);

			// Qos Paramerts text
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_QosParamertsTextField,
					"APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_QosParamertsTextField,
					QosParametertextValue);
			// addtextFields_commonMethod("Qos Parameters",
			// "addPPPconfig_QosParamertsTextField", QosParametertextValue);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_rightArrowButtonForSelection, ">>");

			// WebElement selectedQosParamert =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_selectedQosParameters"));
			WebElement selectedQosParamert = webDriver.findElement(By.xpath("//select[@id='selectedQosParameters']"));
			org.openqa.selenium.support.ui.Select sel = new Select(selectedQosParamert);
			List<WebElement> we = sel.getOptions();

			for (WebElement a : we) {
				if (!a.getText().equals("select")) {

					actualSelectValueInsideQosParamert = a.getText();
				}
			}

			String expectedQosParamertvalue = QosParameters + " " + QosParametertextValue;
			if (actualSelectValueInsideQosParamert.isEmpty()) {
				Report.LogInfo("INFO", "No values displaying under 'Select Qos paramert' textbox", "FAIL");
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Select Qos paramert' textbox");

			} else {
				if (expectedQosParamertvalue.equals(actualSelectValueInsideQosParamert)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value displaying under 'Qos Selected Paramert' field displaying as: "
									+ actualSelectValueInsideQosParamert);
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"value displaying under 'Qos Selected Paramert' field mismatches. It is displaying as: "
									+ actualSelectValueInsideQosParamert + ". But the expected value is: "
									+ expectedQosParamertvalue);
					Report.LogInfo("INFO",
							"value displaying under 'Qos Selected Paramert' field mismatches. It is displaying as: "
									+ actualSelectValueInsideQosParamert + ". But the expected value is: "
									+ expectedQosParamertvalue,
							"FAIL");
				}
			}

			// SA Validation
			addDropdownValues_commonMethod("SA Validation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_SAvalidation, SAvalidation);

			// Unisphere -IGMP-enable
			addDropdownValues_commonMethod(" Unisphere -IGMP-enable",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniSphereIGMPenable,
					uniSphereEnable);

			// Unisphere -IGMP-version
			addDropdownValues_commonMethod(" Unisphere -IGMP-version",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniSphereIGMPversion,
					uniSphereVersion);

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_OKbutton);
			// Thread.sleep(1000);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_OKbutton, "OK"); // OK
																									// button
			waitforPagetobeenable();

		} else {
			ExtentTestManager.getTest().log(LogStatus.PASS, " 'Add PPP Configuration' panel is not displaying");
		}
	}

	public void viewPPPconfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String QosParameters = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_selectQosParameters");
		String QosParametertextValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_QosParametertextValue");
		String framedWANipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_QosParametertextValue");

		String ipv6Parameter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_IPv6Parameters");
		String framedIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Address");
		String delegateIPv6Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_delegateIPv6Prefix");
		String framedRoute0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute0");
		String framedRoute1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute1");
		String framedRoute2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute2");
		String framedRoute3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute3");
		String framedRoute4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute4");
		String framedRoute5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute5");
		String framedRoute6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfiguration_framedRoute6");
		// String framedRoute7 = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo,"CPE_pppConfiguration_framedRoute7");
		String framedIPv6route0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6route0");
		String framedIPv6route1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6route1");
		String framedIPv6route2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route2");
		String framedIPv6route3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6route3");
		String framedIPv6route4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route4");
		String framedIPv6route5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route5");
		String framedIPv6route6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route6");
		String framedIPv6route7 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_framedIPv6Route7");

		// String uniIPv6LocalInterface = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo,
		// dataSetNo,"CPE_pppConfig_uniIPv6LocalInterface");
		// String uniIPv6VirtualRouter = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo,"CPE_pppConfig_uniIPv6VirtualRouter");
		// String uniIngressStatistics = DataMiner.fngetcolvalue(testDataFile,
		// sheetName, scriptNo, dataSetNo,"CPE_pppConfig_uniIngressStatistics");
		String uniQOSprofileName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_uniQOSprofileName");
		String SAvalidation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppconfig_Savalidation");
		String uniSphereEnable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_pppConfig_uniSphereEnable");
		String uniSphereVersion = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_ppp_uniSphereVersion");

		String expectedQosParamertvalue = QosParameters + " " + QosParametertextValue;

		waitForAjax();
		waitforPagetobeenable();

		compareText_InViewPage("Framed/WAN IP Address", framedWANipAddress); // Framed/WAN
																				// IP
																				// Address

		compareText_InViewPage("Framed/WAN IP Subnet Mask", "255.255.255.255"); // Framed/WAN
																				// IP
																				// Subnet
																				// Mask

		compareText_InViewPPPconfigurationPage("Framed Protocol", "PPP"); // Framed
																			// Protocol

		if (ipv6Parameter.equalsIgnoreCase("Yes")) {

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Address", framedIpv6Address);

			compareText_InViewPPPconfigurationPage("Framed-Interface-Id", "0000000000003130"); // Framed-Interface-Id

			compareText_InViewPPPconfigurationPage("Delegate-IPv6-Prefix ", delegateIPv6Prefix); // Delegate-IPv6-Prefix

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route0 ", framedIPv6route0); // Framed-IPv6-Route0

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route1", framedIPv6route1); // Framed-IPv6-Route1

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route2", framedIPv6route2); // Framed-IPv6-Route2

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route3", framedIPv6route3); // Framed-IPv6-Route3

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route4", framedIPv6route4); // Framed-IPv6-Route4

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route5 ", framedIPv6route5); // Framed-IPv6-Route5

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route6 ", framedIPv6route6); // Framed-IPv6-Route6

			compareText_InViewPPPconfigurationPage("Framed-IPv6-Route7 ", framedIPv6route7); // Framed-IPv6-Route7

		}

		compareText_InViewPPPconfigurationPage("Framed Route0 ", framedRoute0); // Framed
																				// Route0

		compareText_InViewPPPconfigurationPage("Framed Route1 ", framedRoute1); // Framed
																				// Route1

		compareText_InViewPPPconfigurationPage("Framed Route2 ", framedRoute2); // Framed
																				// Route2

		compareText_InViewPPPconfigurationPage("Framed Route3 ", framedRoute3); // Framed
																				// Route3

		compareText_InViewPPPconfigurationPage("Framed Route4 ", framedRoute4); // Framed
																				// Route4

		compareText_InViewPPPconfigurationPage("Framed Route5 ", framedRoute5); // Framed
																				// Route5

		compareText_InViewPPPconfigurationPage("Framed Route6 ", framedRoute6); // Framed
																				// Route6

		compareText_InViewPPPconfigurationPage("Uni Qos Profile Name ", uniQOSprofileName); // Uni
																							// Qos
																							// Profile
																							// Name

		compareText_InViewPPPconfigurationPage("Qos Parameters ", "business-weight"); // Qos
																						// Parameters

		String actualSAvalidationvalue = "Null";
		if (SAvalidation.equalsIgnoreCase("Enable")) {

			actualSAvalidationvalue = "Enabled";

		} else if (SAvalidation.equalsIgnoreCase("disable")) {

			actualSAvalidationvalue = "Disabled";
		}
		compareText_InViewPPPconfigurationPage("SA Validations ", actualSAvalidationvalue); // SA
																							// Validations

		compareText_InViewPPPconfigurationPage("Unisphere-IGMP-enabled ", uniSphereEnable); // Unisphere-IGMP-enabled

		compareText_InViewPPPconfigurationPage("Unisphere-IGMP-Version ", uniSphereVersion); // Unisphere
																								// -IGMP-version

	}

	public void editPPPconfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {

		String framedWANipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedWANipAddress");
		String ipv6Parameter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_IPv6Parameters");
		String framedIpv6Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6Address");
		String delegateIPv6Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_delegateIPv6Prefix");
		String framedRoute0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute0");
		String framedRoute1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute1");
		String framedRoute2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute2");
		String framedRoute3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute3");
		String framedRoute4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute4");
		String framedRoute5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute5");
		String framedRoute6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute6");
		String framedRoute7 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedRoute7");
		String framedIPv6route0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route0");
		String framedIPv6route1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route1");
		String framedIPv6route2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route2");
		String framedIPv6route3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route3");
		String framedIPv6route4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route4");
		String framedIPv6route5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route5");
		String framedIPv6route6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route6");
		String framedIPv6route7 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_framedIPv6route7");
		String uniIPv6LocalInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPConfig_uniIPv6LocalInterface");
		String uniIPv6VirtualRouter = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPConfig_uniIPv6VirtualRouter");
		String uniIngressStatistics = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPConfig_uniIngressStatistics");
		String uniQOSprofileName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPConfig_uniQOSprofileName");
		String QosParameters = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPConfig_selectQosParameters");
		String QosParametertextValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPConfig_QosParametertextValue");
		String SAvalidation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_SAvalidation");
		String uniSphereEnable = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPConfig_uniSphereEnable");
		String uniSphereVersion = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,
				"CPE_editPPPconfig_uniSphereVersion");

		scrollIntoTop();
		// Thread.sleep(1000);
		// ScrolltoElement("viewPPPconfig_ActionDropdown");
		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.viewPPPconfig_ActionDropdown,
				"viewPPPconfig_ActionDropdown");

		click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.pppConfig_editLink, "pppConfig_editLink");

		String actualSelectValueInsideQosParamert = "Null";

		// WebElement editPPPPanel = getwebelement(xml.getlocator("//locators/"
		// + application + "/addPPPconfig_uniVirtualRouterName"));

		// if(editPPPPanel.isDisplayed())
		if (isVisible(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniVirtualRouterName)) {
			Report.LogInfo("INFO", "'Edit PPP Configuration' page is displaying", "PASS");
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Edit PPP Configuration' page is displaying");

			// Framed/WAN/IP Address
			edittextFields_commonMethod("Framed/WAN/IP Address",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPcofig_famedWANipAddress,
					framedWANipAddress);

			// Framed/WAN IP Subnet Mask

			String framedWANorIPsubnetMask = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedWANIpAddress);
			if (framedWANorIPsubnetMask.equals("255.255.255.255")) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Framed/WAN IP Subnet Mask value is displaying as " + framedWANorIPsubnetMask + " as expected");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS, "Framed/WAN IP Subnet Mask value is displaying as "
						+ framedWANorIPsubnetMask + ". Expected value is 'PPP'");
			}

			// IPv6 Address
			editcheckbox_commonMethod(ipv6Parameter, "addPPPconfig_IPv6Paramert", "IPv6 parameters");

			// boolean IPv6ParamertSelection =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_IPv6Paramert")).isSelected();
			boolean IPv6ParamertSelection = isSelected(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedWANIpAddress,
					"addPPPconfig_framedWANIpAddress");

			if (IPv6ParamertSelection) {

				// Framed-IPv6-Prefix
				// edittextFields_commonMethod("Framed-IPv6-Prefix ",
				// APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addConfig_framedIPvPrefix,
				// framedIPv6Prefix);

				// Framed-IPv6-Address
				edittextFields_commonMethod("Framed-IPv6-Address",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPV6address,
						framedIpv6Address);

				// Delegate-IPv6-Prefix:
				edittextFields_commonMethod("Delegate-IPv6-Prefix:",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_delegateIPv6Prefix,
						delegateIPv6Prefix);

				// WebElement framedProtocolElement =
				// getwebelement(xml.getlocator("//locators/" + application +
				// "/addPPPconfig_framedProtocol"));
				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedProtocol);

			}

			// Framed Protocol

			String framedProtocol = getTextFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedProtocol);

			if (framedProtocol.equals("PPP")) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Framed protocol value is displaying as " + framedProtocol + " as expected");
			} else {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"Framed protocol value is displaying as " + framedProtocol + ". Expected value is 'PPP'");
			}

			// Framed Route0
			edittextFields_commonMethod("Framed Route0",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRouter0, framedRoute0);

			// Framed Router1
			edittextFields_commonMethod("Framed Route1",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRouter1, framedRoute1);

			// Framed Route2
			edittextFields_commonMethod("Framed Route2",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedroute2, framedRoute2);

			// Framed Route3
			edittextFields_commonMethod("Framed Route3",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute3, framedRoute3);

			// Framed Route4
			edittextFields_commonMethod("Framed Route4",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPconfig_framedRoute4, framedRoute4);

			// Framed Router5
			edittextFields_commonMethod("Framed Route5",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute5, framedRoute5);

			// Framed route6
			edittextFields_commonMethod("Framed Route6",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute6, framedRoute6);

			// Framed route7
			edittextFields_commonMethod("Framed Route7",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedRoute7, framedRoute7);

			if (IPv6ParamertSelection) {

				// Framed-IPv6-Route0
				edittextFields_commonMethod("Framed-IPv6-Route0",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route0,
						framedIPv6route0);

				// Framed-IPv6-Route1
				edittextFields_commonMethod("Framed-IPv6-Route1",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route1,
						framedIPv6route1);

				// Framed-IPv6-Route2
				edittextFields_commonMethod("Framed-IPv6-Route2",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route2,
						framedIPv6route2);

				// Framed-IPv6-Route3
				edittextFields_commonMethod("Framed-IPv6-Route3",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route3,
						framedIPv6route3);

				// Framed-IPv6-Route4
				edittextFields_commonMethod("Framed-IPv6-Route4",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6route4,
						framedIPv6route4);

				// Framed-IPv6-Route5
				edittextFields_commonMethod("Framed-IPv6-Route5",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route5,
						framedIPv6route5);

				// Framed-IPv6-Route7
				edittextFields_commonMethod("Framed-IPv6-Route7",
						APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_framedIPv6Route7,
						framedIPv6route6);

				scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6LocalInterface);

				// Uni-IPv6-Local-Interface (eg. loopback10)
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6LocalInterface,
						"addPPPconfig_uniIPv6LocalInterface");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6LocalInterface,
						uniIPv6LocalInterface);

				// addtextFields_commonMethod("Uni-IPv6-Local-Interface",
				// "addPPPconfig_uniIPv6LocalInterface", uniIPv6LocalInterface);

				// Uni-IPv6-Virtual-Router (eg. TEST3BGP)
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6Virtualrouter,
						"addPPPconfig_uniIPv6Virtualrouter");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIPv6Virtualrouter,
						uniIPv6VirtualRouter);

				// Uni-Ingress-Statistics (eg. 1)
				verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIngressStatistics,
						"addPPPconfig_uniIngressStatistics");
				sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniIngressStatistics,
						uniIngressStatistics);

			}

			// Uni Virtual Route Name
			// String uniVirtualActualValue =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_uniVirtualRouterName")).getAttribute("value");
			String uniVirtualActualValue = getAttributeFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniVirtualRouterName, "value");
			if (uniVirtualActualValue.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Uni Virtual Route Name'");
				Report.LogInfo("INFO", "No values displaying under 'Uni Virtual Route Name'", "FAIL");
			} else {
				if (uniVirtualActualValue.equalsIgnoreCase("MGMT")) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Uni Virtual Route Name Value displays as 'MGMT' as expected");
				} else {
					Report.LogInfo("INFO", "Uni Virtual Route Name mismataches. It displays as: "
							+ uniVirtualActualValue + " .The Expected value is 'MGMT'", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Uni Virtual Route Name mismataches. It displays as: " + uniVirtualActualValue
									+ " .The Expected value is 'MGMT'");
				}
			}

			// Uni Local Loopback Interface
			// String uniLoopBackInterface =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/addPPPconfig_uniLocalLoopbackInterface")).getAttribute("value");
			String uniLoopBackInterface = getAttributeFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniLocalLoopbackInterface,
					"value");

			if (uniLoopBackInterface.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Uni Local Loopback Interface field");
				Report.LogInfo("INFO", "No values displaying under 'Uni Local Loopback Interface field", "FAIL");

			} else {
				if (uniLoopBackInterface.equalsIgnoreCase("Loopback999")) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Uni Local Loopback Interface Value displays as 'Loopback999' as expected");
				} else {
					Report.LogInfo("INFO", "Uni Local Loopback Interface value mismataches. It displays as: "
							+ uniLoopBackInterface + " .The Expected value is 'Loopback999'", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Uni Local Loopback Interface value mismataches. It displays as: " + uniLoopBackInterface
									+ " .The Expected value is 'Loopback999'");
				}
			}

			// Uni Ingress Policy
			// String UNIingresspolicy =
			// getwebelement(xml.getlocator("//locators/" + application +
			// "/uniIngressPolicy_textField")).getAttribute("value");
			String UNIingresspolicy = getAttributeFrom(
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.uniIngressPolicy_textField, "value");

			if (UNIingresspolicy.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Uni Ingress Policy' field");
				Report.LogInfo("INFO", "No values displaying under 'Uni Ingress Policy' field", "FAIL");

			} else {
				if (UNIingresspolicy.equalsIgnoreCase("VPN-DEFAULT")) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Uni Ingress Policy Value displays as 'VPN-DEFAULT' as expected");
				} else {
					Report.LogInfo("INFO", "Uni Ingress Policy value mismataches. It displays as: " + UNIingresspolicy
							+ " .The Expected value is 'VPN-DEFAULT'", "FAIL");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"Uni Ingress Policy value mismataches. It displays as: " + UNIingresspolicy
									+ " .The Expected value is 'VPN-DEFAULT'");
				}
			}

			// Uni Qos Profile Name
			addDropdownValues_commonMethod(" Uni Qos Profile Name",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniQOSprofileName,
					uniQOSprofileName);

			// Qos Parameters
			addDropdownValues_commonMethod("Qos Parameters",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_QosParameters, QosParameters);

			// Qos Paramerts text
			verifyExists(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_QosParamertsTextField,
					"addPPPconfig_QosParamertsTextField");
			sendKeys(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_QosParamertsTextField,
					QosParametertextValue);

			// addtextFields_commonMethod("Qos Parameters",
			// "addPPPconfig_QosParamertsTextField", QosParametertextValue);

			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_rightArrowButtonForSelection, ">>");

			WebElement selectedQosParamert = webDriver.findElement(By.xpath("//select[@id='selectedQosParameters']"));
			org.openqa.selenium.support.ui.Select sel = new Select(selectedQosParamert);
			List<WebElement> we = sel.getOptions();

			for (WebElement a : we) {
				if (!a.getText().equals("select")) {

					actualSelectValueInsideQosParamert = a.getText();
				}
			}

			String expectedQosParamertvalue = QosParameters + " " + QosParametertextValue;
			if (actualSelectValueInsideQosParamert.isEmpty()) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"No values displaying under 'Select Qos paramert' textbox");
				Report.LogInfo("INFO", "No values displaying under 'Select Qos paramert' textbox", "FAIL");
			} else {
				if (expectedQosParamertvalue.equals(actualSelectValueInsideQosParamert)) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"value displaying under 'Qos Selected Paramert' field displaying as: "
									+ actualSelectValueInsideQosParamert);
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"value displaying under 'Qos Selected Paramert' field mismatches. It is displaying as: "
									+ actualSelectValueInsideQosParamert + ". But the expected value is: "
									+ expectedQosParamertvalue);
					Report.LogInfo("INFO",
							"value displaying under 'Qos Selected Paramert' field mismatches. It is displaying as: "
									+ actualSelectValueInsideQosParamert + ". But the expected value is: "
									+ expectedQosParamertvalue,
							"FAIL");

				}
			}

			// SA Validation
			addDropdownValues_commonMethod("SA Validation",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_SAvalidation, SAvalidation);

			// Unisphere -IGMP-enable
			addDropdownValues_commonMethod(" Unisphere -IGMP-enable",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniSphereIGMPenable,
					uniSphereEnable);

			// Unisphere -IGMP-version
			addDropdownValues_commonMethod(" Unisphere -IGMP-version",
					APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_uniSphereIGMPversion,
					uniSphereVersion);

			scrollDown(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_OKbutton);
			// Thread.sleep(1000);
			click(APT_MCS_CreateOrder_IPVPNSwiftNetObj.CreateCustomer.addPPPconfig_OKbutton, "OK Button"); // OK
																											// button

		} else {
			Report.LogInfo("INFO", "'Edit PPP Configuration' page is not displaying", "FAIL");
			ExtentTestManager.getTest().log(LogStatus.FAIL, "'Edit PPP Configuration' page is not displaying");

		}
	}

}
