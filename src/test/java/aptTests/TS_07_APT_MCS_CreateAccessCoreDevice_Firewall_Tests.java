package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_MCS_CreateFirewallDevice;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_07_APT_MCS_CreateAccessCoreDevice_Firewall_Tests extends SeleniumUtils{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();
	APT_Login Login = new APT_Login();
	
	/*	String dataFileName = "ModifyOrder.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "CarNor";
	String scriptNo = "2";
	String dataSetNo = "12";
	String Amount = "2";*/
	
	private static GlobalVariables g;
	Properties prop = new Properties();
	
	@Parameters({"scriptNo","dataSetNo","dataSheet","ScenarioName"})
	@Test
	public void CreateAccessCodeFirewalldevice(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws Exception
	{
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1+"/configuration.properties"))
		{

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
        
        try{
        	openurl(Configuration.APT_URL);
        	Login.APT_VoiceService();
        	CreateFirewall.navigatetomanagecoltnetwork();
        	CreateFirewall.navigatetocreateaccesscoredevicepage();
        	CreateFirewall.verifydevicecreation_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifysuccessmessage();
        	CreateFirewall.verifyenteredValue_forDeviceCreation(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifydeviceEdit_AccessRouter(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.verifysuccessmessage();
        	CreateFirewall.verifEditedValue_Firewall(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.testStatus();
        	CreateFirewall.routerPanel(testDataFile, dataSheet, scriptNo, dataSetNo);
        	CreateFirewall.fetchDeviceInterface_viewdevicepage();
        	CreateFirewall.verifydeviceDelete_AccessRouter();
        	CreateFirewall.verifysuccessmessage();
        }
	catch(Exception e)
	{
        Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
        ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
        ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	} 
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
	}

}
