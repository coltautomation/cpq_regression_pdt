package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODLoginObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODLoginPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void NODLogin() throws InterruptedException, AWTException, IOException 
	{
		verifyExists(NODLoginObj.Login.userNameTxb,"Username textbox");
		sendKeys(NODLoginObj.Login.userNameTxb,Configuration.NOD_Username,"Username textbox");
	
		verifyExists(NODLoginObj.Login.passWordTxb,"Password textbox");
		sendKeys(NODLoginObj.Login.passWordTxb,Configuration.NOD_Password,"Password textbox");

		click(NODLoginObj.Login.loginBtn,"Login button");

		verifyExists(NODLoginObj.Login.welcomeTxt,"Welcome Text on Landing page");	
	}
	
	
	
}
