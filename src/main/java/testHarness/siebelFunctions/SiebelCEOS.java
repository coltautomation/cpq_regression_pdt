package testHarness.siebelFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelCEOSObj;
import pageObjects.siebelObjects.SiebelHubSiteObj;
import pageObjects.siebelObjects.SiebelModeObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelCEOS extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();

	public void NavigateToAccounts() throws InterruptedException, IOException 
	{
		waitForAjax();
		Reusable.waitForSiebelLoader();
		Reusable.waitForElementToAppear(SiebelAccountsObj.Accounts.accountsTab, 10);
		verifyExists(SiebelAccountsObj.Accounts.accountsTab,"Account Tab");
		click(SiebelAccountsObj.Accounts.accountsTab,"Account Tab");
		waitForAjax();
		verifyExists(SiebelAccountsObj.Accounts.OCNTxb,"Account Number");	
	}


	public void CEOS_Offnet(String testDataFile, String sheetName, String scriptNo, String dataSetNo ) throws Exception {

		try
		{
			waitForElementToAppear(SiebelCEOSObj.Offnet.OffnetTab, 10);
			javaScriptclick(SiebelCEOSObj.Offnet.OffnetTab,"Offnet Tab");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		}
		catch(Exception e)
		{
			selectByVisibleText(SiebelCEOSObj.Offnet.InstalltionDropdown, "Offnet", "Offnet") ;
		}

		verifyExists(SiebelCEOSObj.Offnet.CEOSRecord,"CEOS Order Reference Number");
		String CEOSOrderRefNum = getTextFrom(SiebelCEOSObj.Offnet.CEOSRecord);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CEOS_OrderReferenceNo", CEOSOrderRefNum);	
		Reusable.waitForSiebelLoader();
	}
	public void CEOS_Offnet_Status( ) throws Exception {


		waitForElementToAppear(SiebelCEOSObj.Offnet.OffnetTab, 10);
		javaScriptclick(SiebelCEOSObj.Offnet.OffnetTab,"Offnet Tab");
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();


		String CEOSRecordStatus = getTextFrom(SiebelCEOSObj.Offnet.CEOSRecordStatus);

		for (int i=0;i<10;i++) {

			if (!CEOSRecordStatus.contains("Active")) {
				Reusable.refreshPage();
				Reusable.waitForSiebelSpinnerToDisappear();
				Reusable.savePage();
			}

			else if (CEOSRecordStatus.contains("Active")) {
				break;
			}

		}
		/*if(!CEOSRecordStatus.contains("Active"))
		{
			Reusable.refreshPage();

			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.savePage();

			waitForElementToAppear(SiebelCEOSObj.Offnet.OffnetTab, 10);
			javaScriptclick(SiebelCEOSObj.Offnet.OffnetTab,"Offnet Tab");
			Reusable.waitForSiebelSpinnerToDisappear();
			Reusable.waitForSiebelLoader();
		}

		Reusable.refreshPage();
		Reusable.waitForSiebelSpinnerToDisappear();
		Reusable.waitForSiebelLoader();*/
	}
}
