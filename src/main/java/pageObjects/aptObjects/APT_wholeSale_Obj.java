package pageObjects.aptObjects;

public class APT_wholeSale_Obj {
	public static class APT_wholeSale {
		public static final String searchorderlink = "@xpath=//li[text()='Search Order/Service']";
		public static final String servicefield = "@xpath=//label[text()='Service']/following-sibling::input";
		public static final String searchbutton = "@xpath=//span[text()='Search']";
		public static final String serviceradiobutton = "@xpath=//div[@role='gridcell']//span[contains(@class,'unchecked')]";
		public static final String searchorder_actiondropdown = "@xpath=//div[@class='dropdown']//button";

		public static final String ManageCustomerServiceLink = "@xpath=(//div[@class='ant-menu-submenu-title'])[2]";

		public static final String managecoltnetworklink = "@xpath=(//div[@class='ant-menu-submenu-title'])[4]";

		public static final String createOrder_NextButton = "@xpath=//span[text()='Next']";

		public static final String createOrderService_warningMessage = "@xpath=(//span[text()='Order/Contract Number(Parent SID) is required'])[2]";

		public static final String serviceType_warningMessage = "@xpath=//span[text()='Service type is required']";

		public static final String CreateOrderServiceLink = "@xpath=//li[text()='Create Order/Service']";

		public static final String entercustomernamefield = "@xpath=//input[@id='customerName']";

		public static final String CreateOrderHeader = "@xpath=//div[text()='Create Order / Service']";

		public static final String ordercontractnumber = "@xpath=//div[div[label[text()='Order/Contract Number']]]//input[@size='9']";

		public static final String selectorderswitch = "@xpath=(//div[@class='react-switch-bg'])[2]";

		public static final String createorderswitch = "@xpath=//form[@name='createOrder']//div[2]//div[1]//div[3]//div[1]//div[1]//div[1]";

		public static final String rfireqiptextfield = "@xpath=//div[contains(@class,'col-md-3 col-sm-12 col-12')]//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String existingorderdropdown = "@xpath=//label[text()='Order/Contract Number(Parent SID)']/parent::div//div//input";

		public static final String existingorderdropdownvalue = "@xpath=//span[text()='12345']";

		public static final String createorderbutton = "@xpath=//button//span[text()='Create Order']";

		public static final String newordertextfield = "@xpath=//input[@id='orderName']";

		public static final String newrfireqtextfield = "@xpath=//div[@class='form-group']//input[@id='rfiRfqIpVoiceLineNo']";

		public static final String OrderCreatedSuccessMsg = "@xpath=//div[@role='alert']//span[text()='Order created successfully']";

		public static final String networkconfigurationinputfield = "@xpath=//div[div[label[text()='Network Configuration']]]//input";

		public static final String OrderContractNumber_Select = "@xpath=(//div[@class='react-dropdown-select-content react-dropdown-select-type-single css-jznujr-ContentComponent e1gn6jc30'])[1]";

		// create order/service page

		public static final String createOrderORService = "@xpath=//div[text()='Create Order / Service']";

		public static final String createordernametextfield = "@xpath=//input[@id='customerName']";

		public static final String choosecustomerdropdown = "@xpath=//div[label[text()='Choose a customer']]//input";

		public static final String customerdropdown = "@xpath=//div[label[text()='Customer']]/div";

		public static final String nextbutton = "@xpath=//span[contains(text(),'Next')]";

		public static final String customer_createorderpage_warngmsg = "@xpath=//div[text()='Choose a customer']";

		public static final String choosocustomerwarningmsg = "@xpath=//body/div[@id='root']/div/div[contains(@class,'app-container app-theme-white fixed-header fixed-sidebar fixed-footer')]/div[contains(@class,'app-main')]/div[contains(@class,'app-main__outer')]/div[contains(@class,'app-main__inner')]/div[contains(@class,'div-border div-margin container')]/form/div[contains(@class,'div-margin row')]/div[contains(@class,'col-12 col-sm-12 col-md-3')]/div[contains(@class,'position-relative form-group')]/div[2]";

		public static final String leaglcustomername = "@xpath=//div[div[label[text()='Legal Customer Name']]]/div[2]";

		public static final String maindomain = "@xpath=//div[div[label[text()='Main Domain']]]/div[2]";

		public static final String country = "@xpath=//div[div[label[text()='Country']]]/div[2]";

		public static final String ocn = "@xpath=//div[div[label[text()='OCN']]]/div[2]";

		public static final String reference = "@xpath=//div[div[label[text()='Reference']]]/div[2]";

		public static final String type = "@xpath=//div[div[label[text()='Type']]]/div[2]";

		public static final String technicalcontactname = "@xpath=//div[div[label[text()='Technical Contact Name']]]/div[2]";

		public static final String email = "@xpath=//div[div[label[text()='Email']]]/div[2]";

		public static final String phone = "@xpath=//div[div[label[text()='Phone']]]/div[2]";

		public static final String fax = "@xpath=//div[div[label[text()='Fax']]]/div[2]";

		public static final String dedicatedportal = "@xpath=//div[div[label[text()='Dedicated Portal']]]/div[2]";

		public static final String createcustomerlink = "@xpath=//li[text()='Create Customer']";

		public static final String createcustomer_header = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String nametextfield = "@xpath=//input[@id='name']";

		public static final String maindomaintextfield = "@xpath=//input[@id='mainDomain']";

		public static final String country1 = "@xpath=//div[label[contains(text(),'Country')]]//input";

		public static final String ocntextfield = "@xpath=//input[@id='ocn']";

		public static final String referencetextfield = "@xpath=//input[@id='reference']";

		public static final String technicalcontactnametextfield = "@xpath=//input[@id='techinicalContactName']";

		public static final String typedropdown = "@xpath=//div[label[contains(text(),'Type')]]//input";

		public static final String emailtextfield = "@xpath=//input[@id='email']";

		public static final String phonetextfield = "@xpath=//input[@id='phone']";

		public static final String faxtextfield = "@xpath=//input[@id='fax']";

		public static final String enablededicatedportalcheckbox = "@xpath=//input[@id='enabledDedicatedPortal']";

		public static final String dedicatedportaldropdown = "@xpath=//div[label[text()='Dedicated Portal']]//input";

		public static final String okbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String customercreationsuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String clearbutton = "@xpath=//span[text()='Reset']";

		public static final String clearcountryvalue = "@xpath=//label[text()='Country']/parent::div//div[text()='�']";

		public static final String cleartypevalue = "@xpath=//label[text()='Type']/parent::div//div[text()='�']";

		// Verify warning messages

		public static final String customernamewarngmsg = "@xpath=//input[@id='name']/parent::div//div";

		public static final String countrywarngmsg = "@xpath=//div[label[contains(text(),'Country')]]/following-sibling::div";

		public static final String ocnwarngmsg = "@xpath=//input[@id='ocn']/parent::div//div";

		public static final String typewarngmsg = "@xpath=//div[label[contains(text(),'Type')]]/following-sibling::div";

		public static final String emailwarngmsg = "@xpath=//input[@id='email']/parent::div//div";

		public static final String orderactionbutton = "@xpath=//div[div[text()='Order']]//button";

		public static final String editorderlink = "@xpath=//div[div[text()='Order']]//div//a[text()='Edit Order']";

		public static final String changeorderlink = "@xpath=//a[contains(text(),'Change Order')]";

		public static final String ordernumbervalue = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String ordervoicelinenumbervalue = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/parent::div/following-sibling::div[@class='customLabelValue form-label']";

		public static final String changeordervoicelinenumber = "@xpath=//label[text()='RFI / RFQ /IP Voice Line number']/following-sibling::input";

		public static final String changeordernumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String changeorder_selectorderswitch = "@xpath=//div[@class='react-switch-bg']";

		public static final String changeorder_chooseorderdropdown = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::div//div[2]";

		public static final String changeorder_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String changeorder_okbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String orderpanelheader = "@xpath=(//div[contains(text(),'Order')])[1]";

		public static final String editorderheader = "@xpath=//div[text()='Edit Order']";

		public static final String editorderno = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/following-sibling::input";

		public static final String editvoicelineno = "@xpath=//label[text()='RFI/RFQ/IP Voice Line Number']/following-sibling::input";

		public static final String editorder_okbutton = "@xpath=(//span[contains(text(),'OK')])[1]";

		public static final String changeorderheader = "@xpath=//div[text()='Change Order']";

		public static final String createorder_button = "@xpath=//button//span[text()='Create Order']";

		public static final String changeorder_dropdownlist = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//div[@role='list']//span[@aria-selected='false']";

		public static final String customerdetailsheader = "@xpath=//div[text()='Customer Details']";

		public static final String cancelbutton = "@xpath=//span[contains(text(),'Cancel')]";

		public static final String successmsg = "@xpath=//div[@role='alert']//span";

		public static final String edit = "@xpath=//div[@class='dropdown-menu show']//a[text()='Edit']";

		public static final String view = "@xpath=//div[@class='dropdown-menu show']//a[text()='View']";

		public static final String delete = "@xpath=//div[@class='dropdown-menu show']//a[contains(text(),'Delete')]";

		public static final String viewpage_backbutton = "@xpath=//span[text()='Back']";

		public static final String LoginColumn = "@xpath=//div[@col-id='userName']";

		public static final String NameColumn = "@xpath=//div[@col-id='firstName']";

		public static final String EmailColumn = "@xpath=//div[@col-id='email']";

		public static final String RolesColumn = "@xpath=//div[@col-id='roles']";

		public static final String AddressColumn = "@xpath=//div[@col-id='postalAddress']";

		public static final String ResourcesColumn = "@xpath=//div[@col-id='0']";

		public static final String ExistingUsers = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//div[@role='row']";

		public static final String UserUnchecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String UserChecked = "@xpath=//div[text()='Users']/parent::div/following-sibling::div//div[@ref='eBodyViewport']//span[@class='ag-icon ag-icon-checkbox-checked']";

		// New User creation in view service page

		public static final String UserActionDropdown = "@xpath=//div[contains(text(),'Users')]/following-sibling::div/div//button[text()='Action']";

		public static final String AddLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Add']";

		public static final String CreateUserHeader = "@xpath=//div[@class='heading-green-row row']//p";

		// Warning messages

		public static final String warningmsg_username = "@xpath=//label[text()='User Name']/parent::div//div";

		public static final String warningmsg_firstname = "@xpath=//label[text()='First Name']/parent::div//div";

		public static final String warningmsg_surname = "@xpath=//label[text()='Surname']/parent::div//div";

		public static final String warningmsg_postaladdress = "@xpath=//label[text()='Postal Address']/parent::div/following-sibling::div";

		public static final String warningmsg_useremail = "@xpath=//label[text()='Email']/parent::div//div";

		public static final String warningmsg_userphone = "@xpath=//label[text()='Phone']/parent::div//div";

		public static final String warningmsg_userpassword = "@xpath=//label[text()='Password']/parent::div//div";

		public static final String UserName = "@xpath=(//div[@class='position-relative form-group'])[1]//input[@id='userName']";

		public static final String FirstName = "@xpath=(//div[@class='position-relative form-group'])[2]//input[@id='firstName']";

		public static final String SurName = "@xpath=(//div[@class='position-relative form-group'])[3]//input[@id='surname']";

		public static final String PostalAddress = "@xpath=(//div[@class='position-relative form-group'])[4]//textarea[@name='postalAddress']";

		public static final String Email = "@xpath=(//div[@class='position-relative form-group'])[5]//input[@id='email']";

		public static final String Phone = "@xpath=(//div[@class='position-relative form-group'])[6]//input[@id='phone']";

		public static final String Password = "@xpath=(//div[@class='position-relative form-group'])[9]//input[@id='password']";

		public static final String GeneratePassword = "@xpath=//div//span[text()='Generate Password']";

		public static final String OkButton = "@xpath=//button[@type='submit']//span[text()='Ok']";

		public static final String edituser_header = "@xpath=//div[@class='heading-green-row row']//p";

		public static final String delete_alertpopup = "@xpath=//div[@class='modal-content']";

		public static final String deletebutton = "@xpath=//button[@class='btn btn-danger']";

		public static final String deletesuccessmsg = "@xpath=//div[@role='alert']//span";

		public static final String userspanel_header = "@xpath=//div[text()='Users']";

		public static final String usernamevalue = "@xpath=//b[text()='User Name']/parent::label/parent::div/following-sibling::div";

		public static final String firstnamevalue = "@xpath=//b[text()='First Name']/parent::label/parent::div/following-sibling::div";

		public static final String surnamevalue = "@xpath=//b[text()='Surname']/parent::label/parent::div/following-sibling::div";

		public static final String postaladdressvalue = "@xpath=//b[text()='Postal Address']/parent::label/parent::div/following-sibling::div";

		public static final String emailvalue = "@xpath=//b[text()='Email']/parent::label/parent::div/following-sibling::div";

		public static final String phonevalue = "@xpath=//b[text()='Phone']/parent::label/parent::div/following-sibling::div";

		public static final String OK_button = "@xpath=//button[@type='submit']//span[text()='OK']";

		public static final String userdelete = "@xpath=//button[text()='Delete']";

		public static final String IPGuardianAccountGroup = "@xpath=//input[@id='ipGuardianAccountGrp']";

		public static final String IPGuardianAccountGroup_Text = "@xpath=//label[contains(text(),'IPGuardian Account Group')]";

		public static final String IPGuardianAccountGroup_viewpage = "@xpath=//div[div[label//b[contains(text(),'IPGuardian Account Group')]]]/div[2]";

		public static final String ColtOnlineUser = "@xpath=//input[@id='onlineUser']";

		public static final String ColtOnlineUser_Text = "@xpath=//label[contains(text(),'Colt Online User')]";

		public static final String coltonlineuser_viewpage = "@xpath=//div[div[label//b[contains(text(),'Colt Online User')]]]/div[2]";

		public static final String Password_Text = "@xpath=//label[contains(text(),'Password')]";

		public static final String Password_Textfield = "@xpath=//input[@id='password']";

		public static final String GeneratePasswordLink = "@xpath=//span[text()='Generate Password']";

		public static final String GeneratePasswordLink_Text = "@xpath=//span[@class='badge-success badge badge-secondary']";

		public static final String roleDropdown_available = "@xpath=//select[@id='availableRoles']//option";

		public static final String roleDropdown_addButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='>>']";

		public static final String roleDropdown_removeButton = "@xpath=//div[label[text()='Roles']]//following-sibling::div//span[text()='value']";

		public static final String roleDropdown_selectedValues = "@xpath=//select[@id='selectedRoles']//option";

		public static final String hideRouterToolIPv4_Huawei_available = "@xpath=//select[@id='availableIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4__Huawei_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='>>']";

		public static final String hideRouterToolIPv4_Huawei_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div//span[text()='value']";

		public static final String hideRouterToolIpv4_Huawei_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandHuawei']//option";

		public static final String hideRouterToolIPv4_Cisco_Available = "@xpath=//select[@id='availableIPV4CommandCisco']//option";

		public static final String hideRouterToolIPv4_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String hideRouterToolIPv4_Cisco_removeButton = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]//following-sibling::div//span[text()='value']";

		public static final String hideRouterToolIpv4_Cisco_selectedvalues = "@xpath=//select[@id='selectedIPV4CommandCisco']//option";

		public static final String HideService_Available = "@xpath=//select[@id='availableHideService']//option";

		public static final String HideService_addButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='>>']";

		public static final String HideService_removeButton = "@xpath=//div[label[text()='Hide Services']]//following-sibling::div//span[text()='value']";

		public static final String HideServicesDropdown_selectedValues = "@xpath=//select[@id='selectedHideService']//option";

		public static final String HideSiteOrder_Available = "@xpath=//select[@id='availableHideSiteOrder']//option";

		public static final String hideSiteOrder_addbutton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='>>']";

		public static final String hideSiteOrder_removeButton = "@xpath=//div[label[text()='Hide Site Order']]//following-sibling::div//span[text()='value']";

		public static final String HideSiteOrderDropdown_selectedValues = "@xpath=//select[@id='selectedHideSiteOrder']//option";

		public static final String HideRouterToolIPv6_Cisco_Available = "@xpath=//select[@id='availableIPV6Command']//option";

		public static final String hideRouterToolIPv6_Cisco_addButton = "@xpath=//div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]//following-sibling::div//span[text()='>>']";

		public static final String viewUser_HiddenRouterToolIPv4Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv4 Commands(Cisco)']]]//following-sibling::div";

		public static final String viewUser_HiddenRouterToolCommandIPv4Huawei = "@xpath=//div[label[text()='Hide Router Tools IPv4 Commands(Huawei)']]//following-sibling::div";

		public static final String viewUser_HiddenRouterToolCommandIPv6Cisco = "@xpath=//div[div[label[text()='Hide Router Tools IPv6 Commands(Cisco)']]]//following-sibling::div";

		public static final String selectValueUnderUserPanel_ViewSericePage = "@xpath=//div[contains(text(),'value')]/preceding-sibling::div//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String changeorder_cancelbutton = "@xpath=//button[span[text()='Cancel']]";

		public static final String changeOrder_orderDropdown = "@xpath=//div[label[text()='Order/Contract Number (Parent SID)']]//input";

		public static final String fetchorderNumber = "@xpath=//label[text()='Order/Contract Number (Parent SID)']/parent::div//following-sibling::div";

		public static final String alertMsg = "@xpath=(//div[@role='alert'])[1]";

		public static final String AlertForServiceCreationSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

		// manageService

		public static final String orderpanelheader1 = "@xpath=(//div[contains(text(),'Order')])[1]";

		public static final String servicepanel_serviceidentificationvalue = "@xpath=//div[label[contains(text(),'Service Identification')]]/div";

		public static final String servicepanel_servicetypevalue = "@xpath=//div[label[contains(text(),'Service Type')]]/div";

		public static final String servicepanel_remarksvalue = "@xpath=//div[label[contains(text(),'Remark')]]/div";

		public static final String servicepanel_header = "@xpath=(//div[contains(text(),'Service')])[1]";

		public static final String serviceactiondropdown = "@xpath=//div[div[text()='Service']]//button";

		public static final String serviceupdate_successmsg = "@xpath=//div[@role='alert']//span[text()='Service successfully updated']";

		public static final String manageLink = "@xpath=//div[@class='dropdown-menu show']//a[text()='Manage']";

		public static final String synchronizelink_servicepanel = "@xpath=//a[text()='Synchronize']";

		public static final String manageservice_header = "@xpath=//div[text()='Manage Service']";

		public static final String status_ordername = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";

		public static final String status_servicename = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";

		public static final String status_servicetype = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";

		public static final String status_servicedetails = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";

		public static final String status_currentstatus = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='currentStatus']";

		public static final String status_modificationtime = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell'][@col-id='mTime']";

		public static final String statuslink = "@xpath=//div[text()='Status']/parent::div/parent::div//div[@role='gridcell']//a[text()='Status']";

		public static final String sync_ordername = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";

		public static final String sync_servicename = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceName']";

		public static final String order_syncPanel = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='orderName']";

		public static final String sync_servicetype = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceType']";

		public static final String sync_servicedetails = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='serviceDetails']";

		public static final String sync_status = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell'][@col-id='syncStatus']";

		public static final String synchronizelink = "@xpath=//div[text()='Synchronization']/parent::div/parent::div//div[@role='gridcell']//a[text()='Synchronize']";

		public static final String managepage_backbutton = "@xpath=//span[text()='Back']";

		public static final String Servicestatus_popup = "@xpath=//div[@class='modal-content']";

		public static final String servicestatus_popupclose = "@xpath=(//div[text()='Service Status']/following-sibling::button//span)[1]";

		public static final String changestatus_dropdown = "@xpath=//label[text()='Change Status']/parent::div//input";

		public static final String changestatus_dropdownvalue = "@xpath=//label[text()='Change Status']/parent::div//span";

		public static final String servicestatushistory = "@xpath=(//div[@class='modal-content']//div[@ref='eBodyViewport']//div[@role='row'])[1]";

		public static final String Sync_successmsg = "@xpath=//div[@role='alert']//span";

		public static final String alertMsg2 = "@xpath=(//div[@role='alert'])[1]";

		public static final String AlertForServiceCreationSuccessMessage2 = "@xpath=(//div[@role='alert']/span)[1]";

		public static final String serviceStatusPopup_sericeIdentificationValue = "@xpath=//div[div[div[text()='Service Status']]]//label[text()='Service Identification']/parent::div//following-sibling::div";

		public static final String serviceStatusPopup_serviceTypeValue = "@xpath=//div[div[div[text()='Service Status']]]//label[text()='Service Type']/parent::div//following-sibling::div";

		public static final String serviceStatusPopup_changeStatusDropdown = "@xpath=//div[label[text()='Change Status']]//input";

		public static final String okbutton1 = "@xpath=//span[contains(text(),'OK')]";

		public static final String serviceStatusPopup_statusTable_statusColumn = "@xpath=(//div[@col-id='status'][@role='gridcell'])[1]";

		// wholesaleService

		// breadcrump common code

		public static final String breadcrump = "@xpath=//ol[@class='breadcrumb']//a[text()='value']";

		// Service creation

		public static final String servicetypeDropdown = "@xpath=//div[label[text()='Service Type']]//input";

		public static final String Nextbutton = "@xpath=//span[text()='Next']";

		public static final String OKbutton = "@xpath=//span[contains(text(),'OK')]";

		public static final String serviceIdentificationWarningmsg = "@xpath=//div[text()='Service Identification']";

		public static final String secivceIdentification_textField = "@xpath=//div[label[text()='Service Identification']]//input";

		public static final String sericetype_defaultvalue = "@xpath=//div[@class='customLabelValue form-label']";

		public static final String remark_textField = "@xpath=//div[label[text()='Remark']]//textarea";

		public static final String Email_textfield = "@xpath=//div[label[contains(text(),'Email')]]//input";

		public static final String Email_addButton = "@xpath=//div[div[label[text()='Email']]]/following-sibling::div//span[text()='>>']";

		public static final String phoneContact_addButton = "@xpath=//div[div[label[text()='Phone Contact']]]/following-sibling::div//span[text()='>>']";

		public static final String phoneContact_TextField = "@xpath=//div[label[text()='Phone Contact']]//input";

		public static final String ImproperEmail_errValidationmsg = "@xpath=//div[text()='Email']";

		public static final String performanceReporting_checkbox = "@xpath=//div[label[text()='Performance Reporting']]//input";

		public static final String viewPage_RemarkField = "@xpath=//div[label[text()='Remark']]/div";

		// Edit Service

		public static final String OrderPanel = "@xpath=//div[div[text()='Order']]";

		public static final String ActionDropdown_InViewPage = "@xpath=//div[div[text()='Service']]//button[text()='Action']";

		public static final String editLink_InViewPage = "@xpath=//a[text()='Edit']";

		public static final String editService_Link = "@xpath=//div[text()='Service']/parent::div//a[text()='Edit']";

		public static final String editSerivcePage_pnaleHeader = "@xpath=//div[div[p[text()='Edit Service']]]";

		// Dump

		public static final String Editservice_actiondropdown = "@xpath=//div[div[text()='Service']]//button[text()='Action']";

		// Show new infovista report

		public static final String viewservice_infovistareport = "@xpath=//a[text()='Show Infovista Report']";

		public static final String Editservice_Dumplink = "@xpath=//a[text()='Dump']";

		public static final String dumpPage_header = "@xpath=//div[@class='modal-header']";

		public static final String dumpheaderName = "@xpath=//div[@class='modal-header']/div";

		public static final String dumpMessage_body = "@xpath=//div[@class='modal-body']/div";

		public static final String dump_xButton = "@xpath=//span[contains(text(),'�')]";

		public static final String manageSubnet_IPv6 = "@xpath=//a[text()='Manage Subnets Ipv6']";

		public static final String manageSubnet_successMSG = "@xpath=//div[div[@class='modal-header']]//div[@role='alert']/span";
		public static final String manageSubnet_Body = "@xpath=//div[div[@class='modal-header']]";

		// success Message

		public static final String alertMsg1 = "@xpath=(//div[@role='alert'])[1]";

		public static final String AlertForServiceCreationSuccessMessage1 = "@xpath=(//div[@role='alert'])[1]//span";

		// MAS switch

		public static final String managementOptionsPanelheader = "@xpath=//div[div[contains(text(),'Management Options')]]";

		public static final String MASswitch_panelHeader = "@xpath=//div[div[text()='MAS Switch']]";

		public static final String addMASswitch_link = "@xpath=//a[contains(text(),'Add MAS Switch')]";

		public static final String MAS_AddMASSwitch_header = "@xpath=//div[contains(text(),'Add MAS Switch')]";

		public static final String MASswitch_warningmessage = "@xpath=//div[text()='IMS POP Location']";

		public static final String AddmasSWitch_IMSppswitch_dropdown = "@xpath=//div[label[text()='IMS POP Location']]//input";

		public static final String MAS_AddSwitchSuccessfulMessage = "@xpath=//span[contains(text(),'MAS switch added successfully')]";

		public static final String MAS_viewdevice1 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='View'])[1]";

		public static final String MAS_viewdevice2 = "@xpath=(//div[div[div[text()='MAS Switch']]]//span[text()='View'])[2]";

		// MAC switch view page

		public static final String MAS_View_DeviceNameValue = "@xpath=//div[div[label[text()='Name']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_VendorModelValue = "@xpath=//div[div[label[text()='Vendor/Model']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_ManagementAddressValue = "@xpath=//div[div[label[text()='Management Address']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_SnmproValue = "@xpath=//div[div[label[text()='Snmpro']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_CountryValue = "@xpath=//div[div[label[text()='Country']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_CityValue = "@xpath=//div[div[label[text()='City']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_SiteValue = "@xpath=//div[div[label[text()='Site']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_PremiseValue = "@xpath=//div[div[label[text()='Premise']]]//div[@class='customLabelValue form-label']";

		public static final String MAS_View_ActionLink = "@xpath=(//button[text()='Action'])[1]";

		public static final String MAS_View_Action_EditLink = "@xpath=//a[text()='Edit']";

		public static final String MAS_View_Action_DeleteLink = "@xpath=//a[text()='Delete']";

		public static final String Turnk_DeleteLink = "@xpath=(//a[contains(text(),'Delete')])[1]";

		public static final String deleteServiceLink = "@xpath=//a[text()='Delete']";

		public static final String MAS_View_Action_FetchDeviceInterfacesLink = "@xpath=//a[text()='Fetch Device Interfaces']";

		public static final String MAS_deviceName = "@xpath=//div[label[text()='Name']]//input";

		public static final String MAS_vendorModel = "@xpath=//div[label[text()='Vendor/Model']]//input";

		public static final String MAS_managementAddress = "@xpath=//div[label[text()='Management Address']]//input";

		public static final String MAS_snmpro = "@xpath=//div[label[text()='Snmpro']]//input";

		public static final String configure_EditInterface_ActioButton = "@xpath=//div[div[text()='Configure Interface']]//button[text()='Action']";

		public static final String countryinput = "@xpath=//select[@id='countryId']";

		public static final String citydropdowninput = "@xpath=//select[@id='cityId']";

		public static final String sitedropdowninput = "@xpath=//select[@id='siteId']";

		public static final String premisedropdowninput = "@xpath=//select[@id='premiseId']";

		public static final String addcityswitch = "@xpath=//div[label[contains(text(),'Add City')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String addsiteswitch = "@xpath=//div[label[contains(text(),'Add Site')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String addpremiseswitch = "@xpath=//div[label[contains(text(),'Add Premise')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String selectpremiseswitch = "@xpath=//div[label[contains(text(),'Select Premise')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String selectsiteswitch = "@xpath=//div[label[contains(text(),'Select Site')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String selectcityswitch = "@xpath=//div[label[contains(text(),'Select City')]]/preceding-sibling::div//div[@class='react-switch']";

		public static final String citynameinputfield = "@xpath=//input[@id='cityName']";

		public static final String citycodeinputfield = "@xpath=//input[@id='cityCode']";

		public static final String sitenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[1]";

		public static final String sitecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[1]";

		public static final String premisecodeinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[1]";

		public static final String premisenameinputfield_addCityToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[1]";

		public static final String sitenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Name']]//input)[2]";

		public static final String sitecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Site Code']]//input)[2]";

		public static final String premisecodeinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[2]";

		public static final String premisenameinputfield_addSiteToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[2]";

		public static final String premisecodeinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Code']]//input)[3]";

		public static final String premisenameinputfield_addPremiseToggleSelected = "@xpath=(//div[label[text()='Premise Name']]//input)[3]";

		// Router Panel

		public static final String commandIPV4_dropdown = "@xpath=//div[label[text()='Command IPV4']]//input";

		public static final String commandIPv4_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPAddress']";

		public static final String commandIPv4_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])[1]";

		public static final String commandIPV6_dropdown = "@xpath=//div[label[text()='Command IPV6']]//input";

		public static final String commandIPv6_hostnameTextfield = "@xpath=//input[@id='routertools.hostnameOrIPv6Address']";

		public static final String commandIPv6_vrfnameTextField = "@xpath=(//input[@id='routertools.vrfName'])";

		public static final String executebutton_Ipv4 = "@xpath=(//span[text()='Execute'])[1]";

		public static final String executebutton_IPv6 = "@xpath=(//span[text()='Execute'])[2]";

		public static final String result_textArea = "@xpath=//div[label[text()='Result']]//textarea";

		public static final String routerTool_HeaderPanel = "@xpath=//div[div[text()='Router Tools']]";

		// MAS switch interface page

		public static final String MAS_PE_interfacepanel_ActionDropdown = "@xpath=//div[div[text()='Interfaces']]//following-sibling::div//button[text()='Action']";

		public static final String MAS_PE_addInterfaceLink = "@xpath=//a[text()='Add Interface/Link']";

		public static final String MAS_PE_configurationWarningMessage = "@xpath=//div[label[text()='Configuration']]//following-sibling::div";

		public static final String MAS_PE_interfaceWarningMessage = "@xpath=//div[text()='Interface']";

		public static final String MAS_PE_AccessMediaDropdown = "@xpath=//div[label[text()='Access Media']]//input";

		public static final String fetchMAS_AccessMediaDropdown = "@xpath=//div[label[text()='Access Media']]//span";

		public static final String MAS_PE_InterfaceTextfield = "@xpath=//div[label[text()='Interface']]//input";

		public static final String MAS_PE_HSRDorBPdropdown = "@xpath=//div[label[text()='HSRP/BGP']]//input";

		public static final String MAS_PE_networkDropdown = "@xpath=//div[label[text()='Network']]//input";

		public static final String MAS_PE_InterfaceAddressRangeTextfield = "@xpath=//div[label[text()='Interface Address Range']]//input";

		public static final String MAS_PE_InterfaceAddressMaskTextfield = "@xpath=//div[label[text()='Interface Address/Mask']]//input";

		public static final String MAS_PE_HSRPIPTextfield = "@xpath=//div[label[text()='HSRP IP']]//input";

		public static final String MAS_PE_InterfaceAddressRangeIPV6Textfield = "@xpath=//div[label[text()='Interface Address Range IPV6']]//input";

		public static final String MAS_PE_HSRPIPv6AddressTextfield = "@xpath=//div[label[text()='HSRP IPv6 Address']]//input";

		public static final String MAS_PE_PrimaryIPv6onMas1Textfield = "@xpath=//div[label[text()='Primary IPv6 on Mas-1']]//input";

		public static final String MAS_PE_SecondaryIPv6onMas2Textfield = "@xpath=//div[label[text()='Secondary IPv6 on Mas-2']]//input";

		public static final String MAS_PE_GroupNumberTextfield = "@xpath=//div[label[text()='Group Number']]//input";

		public static final String MAS_PE_LinkTextfield = "@xpath=//div[label[text()='Link']]//input";

		public static final String MAS_PE_VLANIDTextfield = "@xpath=//div[label[text()='VLAN Id']]//input";

		public static final String MAS_PE_IVManagementCheckbox = "@xpath=//div[label[text()='IV Management']]//input";

		public static final String MAS_PE_HSRPtrackInterfacetextField = "@xpath=//div[label[text()='HSRP Track Interface']]//input";

		public static final String MAS_PE_HSRPauthenticationtextField = "@xpath=//div[label[text()='HSRP Authentication']]//input";

		// MAS switch generate Configuration panel

		public static final String MAS_PE_confiugrationPanelheader = "@xpath=//div[div[text()='Configuration']]";

		public static final String MAS_PE_generateConfigurationDropdown = "@xpath=//div[label[contains(text(),'Generate Configuration')]]//input";

		public static final String MAS_PE_generateConfigurationLink = "@xpath=//a[text()='Generate Configuration']";

		public static final String configuration_textArea = "@xpath=//div[label[text()='Configuration']]//textarea";

		// MAS switch view Interface

		public static final String MAS_PE_searchbox = "@xpath=//input[@id='interfaceNameAddress']";

		public static final String MAS_PE_searchButton = "@xpath=//span[text()='Search']";

		public static final String MAS_PE_InterfacePanel_labelnames = "@xpath=//span[@role='columnheader']";

		public static final String MAS_PE_InterfacePanel_nameValue = "@xpath=//div[@role='gridcell'][@col-id='name']";

		public static final String MAS_PE_InterfacePanel_linkValue = "@xpath=//div[@role='gridcell'][@col-id='link']";

		public static final String MAS_PE_InterfacePanel_InteraceAddressRangeValue = "@xpath=//div[@role='gridcell'][@col-id='0']/div/div";

		public static final String MAS_PE_InterfacePanel_InteraceAddressValue = "@xpath=//div[@role='gridcell'][@col-id='interfaceAddress']";

		public static final String MAS_PE_InterfacePanel_bearertypeValue = "@xpath=//div[@role='gridcell'][@col-id='bearerType']";

		public static final String MAS_PE_InterfacePanel_bandwidthvalue = "@xpath=//div[@role='gridcell'][@col-id='bandwidth']";

		public static final String MAS_PE_InterfacePanel_vlanidvalue = "@xpath=//div[@role='gridcell'][@col-id='vlanIdentification']";

		public static final String MAS_PE_interfacePanel_IfInOctetsValue = "@xpath=//div[@role='gridcell'][@col-id='bandwidth_1']";

		public static final String MAS_PE_interfacePanel_IVmanagementvalue = "@xpath=//div[@role='gridcell'][@col-id='ivMng']";

		public static final String existingdevicegrid_MAS = "@xpath=(//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row'])";

		public static final String existingdevicegrid_CPEdevice = "@xpath=(//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row'])[2]";

		public static final String MAS_fetchAlldevice_InviewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//a";

		public static final String CPEdevice_fetchAlldevice_inViewPage = "@xpath=(//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row']//b)[4]";

		public static final String MAS_deleteFromService_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete from Service']";

		public static final String MAS_viewLink_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='View']";

		public static final String MAS_selectInterface_InViewPage = "@xpath=//div[text()='MAS Switch']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";

		public static final String CPEdveice_viewLink = "@xpath=//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row']//div[span[b[contains(text(),'value')]]]//following-sibling::span//span[text()='View']";

		public static final String CPEdevice_deleteLink = "@xpath=//div[text()='Trunk Group/Site Orders']/parent::div/following-sibling::div[@class='div-margin row']//div[span[b[contains(text(),'value')]]]//following-sibling::span//span[text()='Delete']";

		// Configure Interface

		public static final String configureInterfaceHeaderName = "@xpath=//div[text()='Configure Interface']";

		// Select Interface

		public static final String InterfaceInselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbCurrent']";

		public static final String InterfaceInselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//span[@ref='lbTotal']";

		public static final String InterfaceInselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Previous']";

		public static final String InterfaceInselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]/following-sibling::div[1]//button[text()='Next']";

		public static final String InterfaceToselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces to Select']]//following-sibling::div//button[text()='Action']";

		public static final String InterfaceToselect_addbuton = "@xpath=//div[div[text()='Interfaces to Select']]//following-sibling::div//a[contains(text(),'Add')]";

		public static final String InterfaceToselect_currentpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbCurrent']";

		public static final String InterfaceToselect_totalpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//span[@ref='lbTotal']";

		public static final String InterfaceToselect_Prevouespage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Previous']";

		public static final String InterfaceToselect_nextpage = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//button[text()='Next']";

		public static final String InterfaceInselect_Actiondropdown = "@xpath=//div[div[text()='Interfaces in Service']]//button[text()='Action']";

		public static final String InterfaceInselect_removebuton = "@xpath=//div[div[text()='Interfaces in Service']]//a[contains(text(),'Remove')]";

		public static final String InterfaceToselect_backbutton = "@xpath=//span[contains(text(),'Back')]";

		public static final String InteraceColumn_Filter = "@xpath=(//div[div[text()='Interfaces to Select']]//following-sibling::div//span[@class='ag-icon ag-icon-menu'])[2]";

		public static final String filterTxt = "@xpath=//input[@id='filterText']";

		public static final String InterfaceInService_panelHeader = "@xpath=//div[div[contains(text(),'Interfaces in Service')]]";

		public static final String InterfaceToSelect_interfaceColumnHeader = "@xpath=//div[div[text()='Interfaces to Select']]//following-sibling::div//span[text()='Interface']";

		public static final String InterfacetoSelect_listOFinterfaceValuesDisplaying = "@xpath=//div[div[contains(text(),'Interfaces to Select')]]/following-sibling::div[1]//div[div[text()='value']]//span[@class='ag-icon ag-icon-checkbox-unchecked']";

		public static final String delete_alertpopup1 = "@xpath=//div[@class='modal-content']";

		public static final String deletebutton1 = "@xpath=//button[@class='btn btn-danger']";

		public static final String deleteMessage = "@xpath=//div[@class='modal-body']//div[@class='col-12 col-sm-12 col-md-12']";

		public static final String deleteMessages_textMessage = "@xpath=//div[@class='modal-body']";

		// MAS switch delete msg

		public static final String MAS_deleteSuccessMessage = "@xpath=(//div[@role='alert']/span)[1]";

		// Show Interface MAS Device

		public static final String MASswitch_showInterfaceLink = "@xpath=//div[div[text()='MAS Switch']]//following-sibling::div//a[text()='Show Interfaces']";

		// PE device

		public static final String addPEdevice_link = "@xpath=//a[text()='Add PE Device']";

		public static final String PE_addPEdeviceHeaderName = "@xpath=//div[text()='Add PE Device']";

		public static final String PEdevice_IMSlocation_warningmessage = "@xpath=//div[text()='IMS POP Location']";

		// PE Interface Details

		public static final String PE_VRRPBGPdropdown = "@xpath=//div[label[text()='VRRP/BGP']]//input";

		public static final String PE_VRRPiptextField = "@xpath=//div[label[text()='VRRP IP']]//input";

		public static final String PE_VRRPipv6AddressTextField = "@xpath=//div[label[text()='VRRP Ipv6 Address']]//input";

		public static final String PE_VRRpgroupnametextField = "@xpath=//div[label[text()='VRRP-Group Name']]//input";

		public static final String PE_VRFtextField = "@xpath=//div[label[text()='VRF']]//input";

		public static final String PE_VRRPtrackInterfacetextField = "@xpath=//div[label[text()='VRRP Track Interface']]//input";

		public static final String PE_VRRPauthenticationtextField = "@xpath=//div[label[text()='VRRP Authentication']]//input";

		public static final String existingdevicegrid = "@xpath=(//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row'])[2]";

		public static final String PE_fetchAlldevice_InviewPage = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//a";

		public static final String PE_deleteFromService_InViewPage = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Delete from Service']";

		public static final String PE_viewLink_InViewPage = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='View']";

		public static final String PE_selectInterface_InViewPage = "@xpath=//div[text()='Provider Equipment (PE)']/parent::div/following-sibling::div[@class='div-margin row']//b[contains(text(),'value')]/parent::div/following-sibling::div//span[text()='Select Interfaces']";

		// trunk creation

		public static final String trunkPanel = "@xpath=//div[div[text()='Trunk Group/Site Orders']]";

		public static final String addTrunkSiteOrderlink = "@xpath=//a[text()='Add Trunk Group/Site Order']";

		public static final String addtrunkSiteorderPage_panelheader = "@xpath=//div[div[p[text()='Trunk Group/Site Order']]]";

		public static final String deleteSiteOrderlink = "@xpath=//div[div[span[text()='value']]]//span[text()='Delete']";

		public static final String editSiteOrderLink = "@xpath=//div[div[span[text()='value']]]//a[text()='Edit']";

		public static final String trunkGroupOrder_checkbox = "@xpath=//div[label[text()='Trunk Group Order']]//input";

		public static final String trunkGrouporder_warningmsg = "@xpath=//div[text()='Trunk Group Order']";

		public static final String trunkGroupOrderName_textField = "@xpath=//div[label[text()='Trunk Group Order Number']]//input";

		public static final String trunkGroupOrderName_warningmsg = "@xpath=//div[text()='Trunk Group Order Number']";

		public static final String trunkGroupOrderName_errMsg = "@xpath=//div[@role='alert']";

		public static final String alertMSg_siteorderCreation = "@xpath=(//div[@role='alert']/span)[1]";

		public static final String trunk_okButton = "@xpath=//span[text()='OK']";

		public static final String trunkType_Dropdown = "@xpath=//select[@id='trunkType']";

		public static final String voipProtocol_Dropdown = "@xpath=//select[@id='voipProtocol']";

		public static final String billingCoutry_Dropdown = "@xpath=//select[@id='billingCountry']";

		public static final String CDRdelivery_Dropdown = "@xpath=//select[@id='cdrDelivery']";

		public static final String gateway_Dropdown = "@xpath=//select[@id='gateway']";

		public static final String quality_Dropdown = "@xpath=//select[@id='quality']";

		public static final String trafficDirection_Dropdown = "@xpath=//select[@id='trafficDirection']";

		public static final String IPaddresstype_Dropdown = "@xpath=//select[@id='ipAddressType']";

		public static final String subInterfaceSlot_Dropdown = "@xpath=//select[@id='sifSlot']";

		public static final String signallingTransportProtocol_Dropdown = "@xpath=//select[@id='transProt']";

		public static final String sourceAddressFiltering_Dropdown = "@xpath=//select[@id='sourceAddressFiltering']";

		public static final String relsupport_Dropdown = "@xpath=//select[@id='rel100Support']";

		public static final String routepriority_Dropdown = "@xpath=//select[@id='routepriority']";

		public static final String addressReachability_Dropdown = "@xpath=//select[@id='addressReachability']";

		public static final String PSPSGname_Dropdown = "@xpath=//select[@id='pspgnamecvnh']";
		// ressReachability_Dropdown
		public static final String globalProfile_Dropdown = "@xpath=//select[@id='e164GlobalProfile']";

		public static final String localProfile_Dropdown = "@xpath=//select[@id='e164LocalProfile']";

		public static final String COSprofile_Dropdown = "@xpath=//select[@id='cosProfile']";

		public static final String preferredPSP_Dropdown = "@xpath=//select[@id='preferredpsp']";

		public static final String carriers_Dropdown = "@xpath=//select[@id='carrierIdNH']";

		public static final String IPsignallingProfile_Dropdown = "@xpath=//select[@id='ipsigProfId']";

		public static final String EgressIpsignal_Dropdown = "@xpath=//select[@id='iptgipsigProfId']";

		public static final String InDMPMrule_Dropdown = "@xpath=//select[@id='indmpmrule']";

		public static final String OutDMPMrule_Dropdown = "@xpath=//select[@id='outdmpmrule']";

		public static final String featureControlprofile_Dropdown = "@xpath=//select[@id='featurecontrolprofile']";

		public static final String localRingBackTone_Dropdown = "@xpath=//select[@id='localringbacktone']";

		public static final String callLimit_Dropdown = "@xpath=//select[@id='callLimit']";

		public static final String primaryTrunkGroup_Dropdown = "@xpath=//select[@id='priTgName']";

		public static final String addTrunkGroupPanel = "@xpath=//div[text()='Add Trunk Group']/parent::div";

		public static final String trunkGroupDEscription_textField = "@xpath=//input[@id='tgDescription']";

		public static final String trunkGroupName_TextField = "@xpath=//input[@id='trunkGroupNameref']";

		public static final String SIPsignallingport_textField = "@xpath=//input[@id='sipSignallingPort']";

		public static final String SIPsignallingPOrt_defaultValue_textvalue = "@xpath=//div[label[text()='SIP Signaling Port']]/div";

		public static final String vlanTag_textField = "@xpath=//input[@id='vlanTg']";

		public static final String signallingZone_textField = "@xpath=//input[@id='sigZone']";

		public static final String coltSignalingIP_textField = "@xpath=//input[@id='sigIp']";

		public static final String mediaIP_textField = "@xpath=//input[@id='mediaIp']";

		public static final String sipSessionKeepAliverTimer_textField = "@xpath=//input[@id='sipSessionKeepAliveRetimer']";

		public static final String defaultTextMessageUnderSIPsessionTimer = "@xpath=//div[label[text()='SIP Session Keepalive Timer(Sec)']]/div";

		public static final String retryinvite_textField = "@xpath=//input[@id='retryinvite']";

		public static final String defaultTextMessageUnderretryInvite = "@xpath=//div[label[text()='Retry Invite']]/div";

		public static final String prefix_textField = "@xpath=//input[@id='prefix']";

		public static final String carrierIPoriginating_textField = "@xpath=//input[@id='ipRangeField']";

		public static final String carrierIPOriginating_addedValue_selectDropdownField = "@xpath=//select[@id='selectedIpPbxRangev6']";

		public static final String carrierIPterminating_textField = "@xpath=//input[@id='ipField']";

		public static final String carrierIPterminating_addedValue_selectDropdownField = "@xpath=//select[@id='selecteddrPbxRange']";

		public static final String subInterfaceName_textField = "@xpath=//input[@id='sifName']";

		public static final String NIFgrouopp_textField = "@xpath=//input[@id='nifGroup']";

		public static final String signallingPort_textField = "@xpath=//input[@id='sigPort']";

		public static final String ipInterface_textField = "@xpath=//input[@id='ipIinterface']";

		public static final String AddressContext_textField = "@xpath=//input[@id='addContext']";

		public static final String ipInterfaceGroup_textField = "@xpath=//input[@id='ipifgroup']";

		public static final String TLS_textField = "@xpath=//input[@id='tlsProfile']";

		public static final String globalProfile_textField = "@xpath=//input[@id='e164GlobalProfileValue']";

		public static final String localProfile_TextField = "@xpath=//input[@id='e164LocalProfileValue']";

		public static final String COSprofile_TextField = "@xpath=//input[@id='cosProfileNew']";

		public static final String PSPGname_TextField = "@xpath=//input[@id='pspgnamecvnhValue']";

		public static final String preferredPSP_TextField = "@xpath=//input[@id='preferredpspValue']";

		public static final String carriers_TextField = "@xpath=//input[@id='carrierIdNHValue']";

		public static final String IPsignallingProfile_textField = "@xpath=//input[@id='ipsigProfIdValue']";

		public static final String EgressipSignal_TextField = "@xpath=//input[@id='iptgipsigProfIdValue']";

		public static final String InDMPMrule_TextField = "@xpath=//input[@id='indmpmruleValue']";

		public static final String OutDMPMrule_TextField = "@xpath=//input[@id='outdmpmruleValue']";

		public static final String featureControlprofile_TextField = "@xpath=//input[@id='featurecontrolprofileValue']";

		public static final String localRingBackTone_TextField = "@xpath=//input[@id='localringbacktoneValue']";

		public static final String limitNumber_textField = "@xpath=//input[@id='limit']";

		public static final String callRateLimitt_textField = "@xpath=//input[@id='callrateLimit']";

		public static final String internetBasedCustomer_checkbox = "@xpath=//div[label[text()='Internet Based Customer']]//input";

		public static final String reuseNIFgroup_checkbox = "@xpath=//input[@id='reuseNIFGroup']";

		public static final String reuseSigZonePart_checkbox = "@xpath=//input[@id='reuseSigZonePart']";

		public static final String callAdmissionControl_checkbox = "@xpath=//input[@id='cac']";

		public static final String callrateLimit_checkbox = "@xpath=//input[@id='cacrt']";

		public static final String srtp_checkbox = "@xpath=//input[@id='srtp']";

		public static final String globalProfile_Checkbox = "@xpath=//input[@id='e164GlobalProfileCheck']";

		public static final String localProfile_checkbox = "@xpath=//input[@id='e164LocalProfileCheck']";

		public static final String COSprofile_checkbox = "@xpath=//input[@id='endiscosprof']";

		public static final String PSPGname_Checkbox = "@xpath=//input[@id='pspgnamecvnhcheck']";

		public static final String preferredPSP_checkbox = "@xpath=//input[@id='preferredpspcheck']";

		public static final String carriers_checkbox = "@xpath=//input[@id='carrierIdNHcheck']";

		public static final String IPsignallingProfile_Checkbox = "@xpath=//input[@id='ipsigProfIdcheck']";

		public static final String EgressIPsignal_checkbox = "@xpath=//input[@id='iptgipsigProfIdcheck']";

		public static final String InDMPMrule_checkbox = "@xpath=//input[@id='indmpmrulecheck']";

		public static final String OutDMPMrule_checkbox = "@xpath=//input[@id='outdmpmrulecheck']";

		public static final String featureControlprofile_Checkbox = "@xpath=//input[@id='featurecontrolprofilecheck']";

		public static final String localRingBackTone_checkbox = "@xpath=//input[@id='localringbacktonecheck']";

		public static final String createLowerCaseRoute_checkbox = "@xpath=//input[@id='createLowerCaseRoutes']";

		public static final String PSXmanualConfig_checkbox = "@xpath=//input[@id='psxManualConfigCheckTG']";

		public static final String GSXmanualConfig_checkbox = "@xpath=//input[@id='gsxManualConfigCheck']";

		public static final String SBCmanualconfig_checkbox = "@xpath=//input[@id='sbcManualConfigCheck']";

		public static final String signalingTransportProtocol_labeName = "@xpath=//label[text()='Signalling Transport Protocol']";

		public static final String viewTrunkPage_IPinterface = "@xpath=//div[label[text()='IP Interface']]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String viewTrunkPage_signallingPort = "@xpath=//div[label[text()='Signalling Port']]//following-sibling::div[@class='customLabelValue form-label']";

		public static final String createLowerCaseRoute_viewTrunkPage = "@xpath=//label[text()='Create Lower Case Routes']";

		public static final String viewTrunkPage_PSXconfigurationValue = "@xpath=//label[contains(text(),'PSX Manual Configuration')]";

		public static final String labelName_localRingBackTone = "@xpath=//label[text()='Local Ring Back Tone']";

		public static final String viewTrunk_Table_ForNewValues = "@xpath=(//label[text()='value']/parent::td//following-sibling::td)[3][@class='customLabelValue']";

		public static final String viewTrunk_Table_ForExistingValue = "@xpath=(//label[text()='value']/parent::td//following-sibling::td)[1][@class='customLabelValue']";

		public static final String psxConfiguration_LabelName_viewTrunkPage = "@xpath=//label[text()='PSX Manual Configuration']";

		public static final String allocatePrefix_button = "@xpath=//span[contains(text(),'Allocate Prefix')]";

		public static final String carrierIPoriginating_addButtton = "@xpath=//div[div[div[contains(text(),'Carrier IP Originating (Address')]]]/following-sibling::div//span[text()='>>']";

		public static final String carrierIPterminating_addButton = "@xpath=//div[div[div[contains(text(),'Carrier IP Terminating')]]]/following-sibling::div//span[text()='>>']";

		public static final String viewPage_ActionButton = "@xpath=//div[div[text()='View Trunk']]//button[text()='Action']";

		public static final String HomeBreadcrump = "@xpath=//ol[@class='breadcrumb']//a[text()='Home']";

		public static final String fetchGateway = "@xpath=//div[div[label[text()='Gateway']]]//div[@class='customLabelValue form-label']";

		public static final String editLink = "@xpath=//a[contains(text(),'Edit')]";

		public static final String viewTrunk_historyLink = "@xpath=//a[contains(text(),'History')]";

		public static final String country_warningMessage = "@xpath=//div[text()='Billing Country']";

		public static final String prefix_warningMessage = "@xpath=//div[text()='Prefix']";

		public static final String carrierIPoriginating_warningMessage = "@xpath=//div[text()='Carrier IP Originating (Address/Mask)']";

		public static final String carrerIPterminating_warningMessage = "@xpath=//div[text()='Carrier IP Terminating((Address)']";

		public static final String GSXconfigurationDropdown_viewtrunk = "@xpath=(//div[label[text()='GSX Configuration']]//input)[1]";

		public static final String PSXconfigurationDropdown_viewtrunk = "@xpath=(//div[label[text()='PSX Configuration']]//input)[1]";

		public static final String SBCconfigurationDropdown_viewtrunk = "@xpath=(//div[label[text()='SBC Configuration']]//input)[1]";

		public static final String viewTrunk_GSX_generateConfigurationButton = "@xpath=//button/span[text()='Generate Configuration']";

		public static final String viewTrunk_PSX_executeButton = "@xpath=//div[div[div[label[text()='PSX Configuration']]]]//following-sibling::div//button/span[text()='Execute']";

		public static final String viewTrunk_SBC_executeButton = "@xpath=(//div[div[div[label[text()='SBC Configuration']]]]//following-sibling::div//button/span[text()='Execute'])[1]";

		public static final String selectCreatedTrunk_InViewServicePage = "@xpath=//div[div[span[b[text()='Trunks ']]]]//following-sibling::div//div[text()='value']";

		public static final String trunkActionDropdown_InviewServicePage = "@xpath=//div[div[span[b[text()='Trunks ']]]]//following-sibling::div//button[text()='Action']";

		public static final String GSXcongig_textArea = "@xpath=//textarea[@name='gsxConfigNote']";

		public static final String GSXconfig_textArea = "@xpath=//textarea[@name='gsxConfigNote']";

		public static final String GSX_config_executeButton = "@xpath=//span[text()='Execute']";

		public static final String GSXconfig_sucessMessage = "@xpath=(//div[@role='alert']/span)[1]";

		public static final String GSXconfig_closeButton = "@xpath=//span[text()='Close']";

		public static final String fetchTrunkGroupName = "@xpath=//div[div[label[text()='Trunk Group Name']]]//div[@class='customLabelValue form-label']";

		// SBC Manual Execution panel

		public static final String SBCmanualConfig_PanelHeader = "@xpath=//div[contains(text(),'SBC Manually Executed Configurations')]";

		public static final String SBCManualConfig_actionDropdown = "@xpath=//div[div[contains(text(),'SBC Manually Executed Configurations')]]//button";

		public static final String SBC_addLink = "@xpath=//a[contains(text(),'Add')]";

		public static final String SBC_editLink = "@xpath=//a[contains(text(),'Edit')]";

		public static final String SBC_deleteLink = "@xpath=//a[contains(text(),'Delete')]";

		public static final String SBC_selectCreatedValue = "@xpath=(//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";

		public static final String SBC_columnNames = "@xpath=(//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";

		public static final String SBC_filenames = "@xpath=//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";

		public static final String SBC_dates = "@xpath=//div[div[text()='SBC Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";

		// PSX Manual Execution panel

		public static final String PSXmanualConfig_PanelHeader = "@xpath=//div[contains(text(),'PSX Manually Executed Configurations')]";

		public static final String PSXManualConfig_actionDropdown = "@xpath=//div[div[contains(text(),'PSX Manually Executed Configurations')]]//button";

		public static final String PSX_selectCreatedValue = "@xpath=(//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";

		public static final String PSX_columnNames = "@xpath=(//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";

		public static final String PSX_fileName = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";

		public static final String PSX_dates = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";

		public static final String PSX_addLink = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[contains(text(),'Add')]";

		public static final String PSX_editLink = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[contains(text(),'Edit')]";

		public static final String PSX_deleteLink = "@xpath=//div[div[text()='PSX Manually Executed Configurations']]//following-sibling::div//a[contains(text(),'Delete')]";

		// GSX Manual Execution panel

		public static final String GSXmanualConfig_PanelHeader = "@xpath=//div[contains(text(),'GSX Manually Executed Configurations')]";

		public static final String GSXManualConfig_actionDropdown = "@xpath=//div[div[contains(text(),'GSX Manually Executed Configurations')]]//button";

		public static final String GSX_selectCreatedValue = "@xpath=(//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[contains(text(),'manualcfg')])[1]";

		public static final String GSX_columnNames = "@xpath=(//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//span[@class='ag-header-cell-text'])";

		public static final String GSX_fileName = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='label']";

		public static final String GSX_dates = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//div[@role='gridcell'][@col-id='value']";

		public static final String GSX_addLink = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[contains(text(),'Add')]";

		public static final String GSX_editLink = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[contains(text(),'Edit')]";

		public static final String GSX_deleteLink = "@xpath=//div[div[text()='GSX Manually Executed Configurations']]//following-sibling::div//a[contains(text(),'Delete')]";

		public static final String GSX_editPage_SaveButton = "@xpath=//button/span[text()='Save']";

		public static final String GSX_editPage_teaxtArea = "@xpath=//textarea[@name='manualConfigNote']";

		public static final String manualConfiguration_textArea = "@xpath=//textarea[@name='manualConfigNote']";

		public static final String saveButton_manualConfiguration = "@xpath=//span[contains(text(),'Save')]";

		// 24 june

		public static final String selectinterface = "@xpath=//div[@role='gridcell'][@col-id='interfaceName'][text()='value']/following-sibling::div//span[contains(@class,'unchecked')]";

		public static final String addeddevice_interface_actiondropdown = "@xpath=//div[div[b[text()='value']]]//following-sibling::div//button[text()='Action']";

		public static final String configureLink = "@xpath=//a[text()='Configure']";

		// Add CPE device Link

		public static final String trunkPanel_ActionDropdown = "@xpath=//div[div[span[text()='value']]]//following-sibling::div//button[text()='Action']";

		public static final String addCPEdeviceLink = "@xpath=//a[text()='Add CPE Device']";

		public static final String routerId = "@xpath=//input[@id='routerId']";

		public static final String CPE_devicename = "@xpath=//input[@id='deviceName']";

		public static final String CPE_vendorModel = "@xpath=//div[label[text()='Vendor/Model']]//input";

		public static final String CPE_manageAddress = "@xpath=//input[@id='ipField']";

		public static final String CPE_snmpro = "@xpath=//input[@id='snmpro']";

		public static final String CPE_snmprw = "@xpath=//input[@id='snmprw']";

		public static final String CPE_snmpV3ContextName = "@xpath=//input[@id='snmpv3ContextName']";

		public static final String CPE_snmpV3ContextEngineID = "@xpath=//input[@id='snmpv3ContextEngineId']";

		public static final String CPE_snmpv3SecurityUserName = "@xpath=//input[@id='snmpv3SecurityUserName']";

		public static final String CPE_snmpv3AuthProto = "@xpath=//div[label[text()='Snmp V3 Auth Proto']]//input";

		public static final String CPE_snmpv3AuthPasswrd = "@xpath=//input[@id='snmpv3AuthPassword']";

		public static final String viewdevicePage_actionDropdown = "@xpath=//button[@id='dropdown-basic-button']";

		public static final String addCitytogglebutton = "@xpath=//div[div[label[text()='Add City']]]//div[@class='react-switch-bg']";

		public static final String viewTrunkPage_historyLink = "@xpath=//a[contains(text(),'History')]";

		public static final String historyPanel_revisionTypeColumn = "@xpath=//div[text()='History']/parent::div/parent::div//div[@role='gridcell'][@col-id='revision.revDesc']";

		public static final String historyPanel_lastModifiedColumn = "@xpath=//div[text()='History']/parent::div/parent::div//div[@role='gridcell'][@col-id='m_user']";

		public static final String historyPanel_trunkGroupNameColumn = "@xpath=//div[text()='History']/parent::div/parent::div//div[@role='gridcell'][@col-id='tgName']";

		public static final String historyPanel_gatewayColumn = "@xpath=//div[text()='History']/parent::div/parent::div//div[@role='gridcell'][@col-id='gateway']";

		public static final String historyPanel_voipProtocolColumn = "@xpath=//div[text()='History']/parent::div/parent::div//div[@role='gridcell'][@col-id='voipProtocol']";

		public static final String historyPanel_signalingTransportprotocol = "@xpath=//div[text()='History']/parent::div/parent::div//div[@role='gridcell'][@col-id='signallingTransportProtocol']";

		public static final String historyPanel_viewLink = "@xpath=//div[text()='History']/parent::div/parent::div//div[@role='gridcell'][@col-id='view']//a[text()='View']";

		public static final String trunk_backbutton = "@xpath=//span[text()='Back']";

		public static final String service = "@xpath=//div[text()='Service']";

	}
}
