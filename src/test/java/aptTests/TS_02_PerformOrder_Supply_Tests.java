package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_MCN_Create_Customer;
import testHarness.aptFunctions.APT_MCS_CreateFirewallDevice;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_02_PerformOrder_Supply_Tests extends SeleniumUtils{
	ReadExcelFile read = new ReadExcelFile();
	ReusableFunctions Reusable = new ReusableFunctions();
	APT_MCN_Create_Customer CreateCust = new APT_MCN_Create_Customer();
	APT_MCS_CreateFirewallDevice CreateFirewall = new APT_MCS_CreateFirewallDevice();
	APT_Login Login = new APT_Login();
	
	/*	String dataFileName = "ModifyOrder.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "CarNor";
	String scriptNo = "2";
	String dataSetNo = "12";
	String Amount = "2";*/
	
	private static GlobalVariables g;
	Properties prop = new Properties();
	
	@Parameters({"scriptNo","dataSetNo","dataSheet","ScenarioName"})
	@Test(description = "TC-01", priority = 1)
	public void Verify_serivceOrderCreationforCustomer(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws Exception
	{
		//String Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
		Report.createTestCaseReportHeader(ScenarioName);	
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1+"/configuration.properties"))
		{

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
		//String username = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "UserName");

        try{
			//setup();				
        	openurl(Configuration.APT_URL);
        	Login.APT_VoiceService();        			
        	CreateCust.navigateToCreateCustomerPage();
			CreateCust.createCustomerFunctionality(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateFirewall.verifysuccessmessage();
			CreateCust.VerifyAddUserFunctionality(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateFirewall.verifysuccessmessage();
			CreateCust.createexistingorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateCust.createneworderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateFirewall.verifysuccessmessage();
			CreateCust.verifyselectservicetype(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateCust.Verify_createorderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateCust.Verify_Selectmanagementoptions(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateCust.Verify_ConfigurationOptions(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateFirewall.verifysuccessmessage();
			
			//Second Test cases ----
			String supplyService = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SupplySevice");

        	//String supplyService=map.get("SupplySevice");
    		
    		if(supplyService.equalsIgnoreCase("Yes")) {
			CreateCust.clickOnSearchCustomerLink();
			CreateCust.searchCustomerAndperformSupply(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateFirewall.verifysuccessmessage();
			CreateCust.verifySuppliesPanelInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			
    		}else{
    			Report.LogInfo("INFO", " Not going to perform supply action for ", "PASS");
    		}
    		
    		//Third Test cases -----
    		String subscribedCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SubscribedForService");

    		if(subscribedCustomer.equalsIgnoreCase("Yes")) {
    			
    			CreateCust.clickOnSearchCustomerLink();
    			CreateCust.verifySearchCustomerFunctionality(testDataFile, dataSheet, scriptNo, dataSetNo);
    			CreateCust.verifySubscribedCustomers(testDataFile, dataSheet, scriptNo, dataSetNo);    		    			
    		}else {
    			Report.LogInfo("INOF", " Subscribe panel verification is not performed for ", "PASS");
    			//ExtentTestManager.getTest().log(LogStatus.INFO, " Subscribe panel verification is not performed for "+ map.get("Name"));
    		}
    		
    		//Fourth Test case----
    		CreateCust.clickOnSearchCustomerLink();
			CreateCust.deletSupplyFunctionalityAndSelectOrderInsideTable(testDataFile, dataSheet, scriptNo, dataSetNo);
			//Facing issue with Delete service that why marked following 2 methods commented.
			//CreateCust.deleteService();			
			//CreateCust.verifySuccessMessageFor_deleteService();
			//CreateCust.clickOnSearchCustomerLink();
			//CreateCust.verifySearchCustomerFunctionalityAndView(testDataFile, dataSheet, scriptNo, dataSetNo);
			//CreateCust.deletOrderFunctionality_selectTheOrderInsideTable(testDataFile, dataSheet, scriptNo, dataSetNo);
			//CreateCust.deleteOrder();
			CreateCust.deleteUser(testDataFile, dataSheet, scriptNo, dataSetNo);
			CreateFirewall.verifysuccessmessage();
			CreateCust.deleteCustomer();
			CreateFirewall.verifysuccessmessage();
        }
	catch(Exception e)
	{
        Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
        ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
        ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	} 
	Report.createTestCaseReportFooter();
	Report.SummaryReportlog(ScenarioName);
	}


}
