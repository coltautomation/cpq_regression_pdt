package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSMergeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberDeactivationObj;
import pageObjects.nmtsObjects.NMTSNumberReservationObj;
import pageObjects.nmtsObjects.NMTSPort_IN_NumberObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSPort_IN_Number extends SeleniumUtils {
	
	NMTSNumberReservationPage NumberReservation = new NMTSNumberReservationPage();
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSAddNumberPage AddNumber = new NMTSAddNumberPage ();
	private static HashMap<String, String> Number= new HashMap<String, String>();
	
	public void PortInNMTS(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		
		String Country = DataMiner.fngetcolvalue(testDataFile,sheetName, scriptNo, dataSetNo,"Country");
		String AreaName = DataMiner.fngetcolvalue(testDataFile, "Adding_Number", "1", dataSetNo,"Area_Code");
		String AddingnumberSwitch = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Adding_number_Switch");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String ColtCategory = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Colt_Category");
		String GeoNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Geo_Number");
		String NRN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MRN/NRN");
		String LosingCommunicationprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LosingCommunicationProvider");
		String OCN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OCN");
		String SelectDestination = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SelectDestination");
		String EmailNotification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EmailNotification");
		String ServiceProfile = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Profile");
		String TransactionReferencenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Transaction_Referencenumber");
		String ProcessingDecision = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Processing_Decision");
		String Contactnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contact_number");
		String ContractSource = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Contract_Source");
		String VATNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"VAT_Number");
		String CustomerName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer_Name");
		String BuildingName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Name");
		String BuildingNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Building_Number");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		String PostCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Post_Code");
		String Town = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Town");
	//	String AllocationRadioButton = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Allocation_RadioButton");
		String AllocationLanguage = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Allocation_Customer_language");
		String RegisteredName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Registered_Name");
		String TelephoneNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Telephone_Number");
		String CIFvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CIF_value");
		String MADEBYNAME = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MADE_BY_NAME");
		String FilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_By");
		String MatchingWithFilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Matching_With_FilterBy");
		
		NumberReservation.UserGroupforPhnNoMgn(testDataFile, sheetName, scriptNo, dataSetNo);
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMenu");
		waitForAjax();
		if (Country.toString().contains("CH"))
		{
			ScrollIntoViewByString(NMTSPort_IN_NumberObj.PortIN.Port_IN_CH);
			verifyExists(NMTSPort_IN_NumberObj.PortIN.Port_IN_CH,"Port IN Tab");
			javaScriptclick(NMTSPort_IN_NumberObj.PortIN.Port_IN_CH,"Port IN Tab");
			WaitForAjax();
			Reusable.switchtodefault();
			waitForAjax();
			Reusable.switchtoframe("fmMain");
			waitForElementToBeVisible(NMTSPort_IN_NumberObj.PortIN.PORT_INRadiobutton,15);
			javaScriptclick(NMTSPort_IN_NumberObj.PortIN.PORT_INRadiobutton,"Wholesale Radio button");
			waitForAjax();

		}
		else
		{
		//	waitForElementToBeVisible(NMTSPort_IN_NumberObj.PortIN.Port_INTab,15);
			ScrollIntoViewByString(NMTSPort_IN_NumberObj.PortIN.Port_INTab);
			verifyExists(NMTSPort_IN_NumberObj.PortIN.Port_INTab,"Port IN Tab");
			javaScriptclick(NMTSPort_IN_NumberObj.PortIN.Port_INTab,"Port IN Tab");
		}
		

		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMain");
		
		Reusable.Select(NMTSPort_IN_NumberObj.PortIN.Switch, AddingnumberSwitch);
		
		for(int i=0;i<15;i++)
		{
		String NMTSAreaCodeExtension = DataMiner.fngetcolvalue(testDataFile, "Adding_Number", "1", dataSetNo,"NMTS_Area_Code_Extension");
		if (NMTSAreaCodeExtension.equals("999"))
		{
			NMTSAreaCodeExtension = "100";
		}	
		
			int j= 0;
			Random rand = new Random();
			int RandomNumber = rand.nextInt(750) + 10;
	      String s1 = Integer.toString(j);
	      String s2 = Integer.toString(RandomNumber);
	  
	     // Concatenate both strings
	     String RangeStartFrom = s1 + s2;
	     System.out.println(RangeStartFrom);
	        // Convert the concatenated string
	        // to integer
	      int k1  = Integer.parseInt(RangeStartFrom);
	      
	      int k2 = k1+ 99;
	      String RangeEndTo = s1 + k2;
	      
	      System.out.println(RangeEndTo);
		if (Country.toString().contains("BE"))
		{
			waitForElementToBeVisible(NMTSPort_IN_NumberObj.PortIN.AreacodePrefix,15);
			verifyExists(NMTSPort_IN_NumberObj.PortIN.AreacodePrefix,"Area Code Prefix");
			javaScriptclick(NMTSPort_IN_NumberObj.PortIN.AreacodePrefix,"Area Code Prefix");
			Reusable.waitForAjax();
			Reusable.switchtofram(webDriver.findElement(By.xpath("//iframe[@id='popupFrame']")));
	
			verifyExists(NMTSPort_IN_NumberObj.PortIN.AreaInformationWin.replace("index",AreaName),"Area Code Prefix");
			javaScriptclick(NMTSPort_IN_NumberObj.PortIN.AreaInformationWin.replace("index",AreaName),"Area Code Prefix");
			
			Reusable.switchtodefault();
			Reusable.switchtoframe("fmMain");

		} 
		
		else {
			verifyExists(NMTSPort_IN_NumberObj.PortIN.Areacodeother,"Area Code Prefix");
			sendKeys(NMTSPort_IN_NumberObj.PortIN.Areacodeother,AreaName,"Area Code prefix");
		}
		
		verifyExists(NMTSPort_IN_NumberObj.PortIN.AreaCode,"Area Code Extension");
		sendKeys(NMTSPort_IN_NumberObj.PortIN.AreaCode,NMTSAreaCodeExtension,"Area Code Extension");
		
		int NMTSAreaCodeExtension1 = Integer.parseInt(NMTSAreaCodeExtension);
		NMTSAreaCodeExtension1 = NMTSAreaCodeExtension1 +1;
		String NMTSAreaCodeExtension2 = Integer.toString(NMTSAreaCodeExtension1);
 		DataMiner.fnsetcolvalue(testDataFile,"Adding_Number","1", dataSetNo, "NMTS_Area_Code_Extension", NMTSAreaCodeExtension2);
 		verifyExists(NMTSPort_IN_NumberObj.PortIN.RangeStart,"Range Start");
   		sendKeys(NMTSPort_IN_NumberObj.PortIN.RangeStart,RangeStartFrom,"Range Start");		
		verifyExists(NMTSPort_IN_NumberObj.PortIN.RangeEnd,"Range End");
		sendKeys(NMTSPort_IN_NumberObj.PortIN.RangeEnd,RangeEndTo,"Range End");
	
		if(Country.toString().equals("DE NON-GEO"))
		{
			Report.LogInfo("Log Report",":","GEO/non-GEO is not availble for country - DE NON-GEO");
		}
		else {
			
			Reusable.Select(NMTSPort_IN_NumberObj.PortIN.AddnumGeo,GeoNumber);
		}
		Reusable.Select(NMTSPort_IN_NumberObj.PortIN.AddnumCtg,ColtCategory);
		
		if(Country.toString().equals("DE NON-GEO")) {
			
			Report.LogInfo("Log Report",":","Availbility is not availble for country - DE NON-GEO");	
		}
		else {
			Reusable.Select(NMTSPort_IN_NumberObj.PortIN.AddingAvailability, Availablity);
		}
		waitForElementToBeVisible(NMTSPort_IN_NumberObj.PortIN.NRN,10);
		sendKeys(NMTSPort_IN_NumberObj.PortIN.NRN,NRN,"NRN number");
		verifyExists(NMTSPort_IN_NumberObj.PortIN.LosingCommProvider,"Losing Communication provider");
		Reusable.Select(NMTSPort_IN_NumberObj.PortIN.LosingCommProvider,LosingCommunicationprovider);
		waitForElementToBeVisible(NMTSPort_IN_NumberObj.PortIN.AddingSubmit,15);
		verifyExists(NMTSPort_IN_NumberObj.PortIN.AddingSubmit,"Submit Button");
		javaScriptclick(NMTSPort_IN_NumberObj.PortIN.AddingSubmit,"Submit Button");
		Reusable.waitForAjax();
		WaitForAjax();
		if(isElementPresent(By.xpath("//span[contains(text(),'Error')]")))
			{
			Report.LogInfo("Log Report",":","Range already present in system, start adding of other range");	
			}
		else{
			break;
			}
		
		}
		
		Reusable.switchtodefault();
		Reusable.switchtoframe("fmMain");
		
		waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.ocn,15);
		verifyExists(NMTSNumberReservationObj.NumberReservation.ocn,"OCN");
		sendKeys(NMTSNumberReservationObj.NumberReservation.ocn,OCN, "OCN");
		waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SubmitOCN,15);
		verifyExists(NMTSNumberReservationObj.NumberReservation.SubmitOCN,"Submit ocn");
		javaScriptclick(NMTSNumberReservationObj.NumberReservation.SubmitOCN,"Submit ocn");
		waitForAjax();
		//waitForAjax();
		if(isElementPresent(By.xpath("(//table[@id='ucxCustomerSearch_grdView']//tr)[2]/td[1]"))) {
			verifyExists(NMTSNumberReservationObj.NumberReservation.doubleClickonOCN,"OCN");
		    javaScriptDoubleclick(NMTSNumberReservationObj.NumberReservation.doubleClickonOCN,"OCN");
		    
	
			} 
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "it seems like OCN no is not correct in Excel sheet please check it ");	
				
		}
		
		waitForAjax();
		
		Reusable.switchtodefault();
		WaitForAjax();
		Reusable.switchtoframe("fmMain");
		WaitForAjax();
		
		
		
		waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.selectDesc,15);
		verifyExists(NMTSNumberReservationObj.NumberReservation.selectDesc,"Select Destination");
		Reusable.Select(NMTSNumberReservationObj.NumberReservation.selectDesc, SelectDestination);
	//	waitForAjax();
		waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.EmailNotification,15);
		verifyExists(NMTSNumberReservationObj.NumberReservation.EmailNotification,"Email Notification");
		Reusable.Select(NMTSNumberReservationObj.NumberReservation.EmailNotification, EmailNotification);
	//	waitForAjax();
		
		if(isElementPresent(By.xpath("//input[@id='ucxRequestAttributes_txtServiceProfile']")))	{
			verifyExists(NMTSNumberReservationObj.NumberReservation.ServiceProfile,"Service Profile");
			sendKeys(NMTSNumberReservationObj.NumberReservation.ServiceProfile, ServiceProfile, "Service Profile");
	//		waitForAjax();
			waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.TransactionreferenceNo,15);
			verifyExists(NMTSNumberReservationObj.NumberReservation.TransactionreferenceNo,"Transaction Referencenumber");
			sendKeys(NMTSNumberReservationObj.NumberReservation.TransactionreferenceNo, TransactionReferencenumber, "Transaction Referencenumber");
			
			
		}
		
	//	waitForAjax();
		verifyExists(NMTSNumberReservationObj.NumberReservation.CheckboxforAllocation,"Checkbox for Allocation");
		javaScriptclick(NMTSNumberReservationObj.NumberReservation.CheckboxforAllocation,"Checkbox for Allocation");
		
		waitForElementToBeVisible(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,15);
		verifyExists(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,"Submit Order Btn");
		javaScriptclick(NMTSNumberReservationObj.NumberReservation.SubmitOrderBtn,"Submit Order Btn");
	
		WaitForAjax();
		waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
		
		
	
		String ReservationStatus = getTextFrom(NMTSNumberReservationObj.NumberReservation.Reservationsuccessmsg,"Reservation status");
	//	System.out.println(NumberReservationSuccessmsg);
		if(ReservationStatus.equals("Port In"))
		{
			String NMTSID = getTextFrom(NMTSNumberReservationObj.NumberReservation.NMTSReservationNo, "NMTS Reservation Number");
			
			setvalue("NMTS_ID",NMTSID);
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "Number reserved successfuly");
			
		}
		else{					
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Not reserved successfuly");		
		}
		
		String FilterText = getvalue("NMTS_ID");
		
	
		////////////////
		
		waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.selectDecision,15);
		Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.selectDecision, ProcessingDecision.toString());

		
		waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo,15);
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo,"Contact Number");
		sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContactNo, Contactnumber.toString(),"Contact Number");

		waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContractSource,15);
		Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ContractSource, ContractSource.toString());

		//waitForAjax();
		/*
		if(Country.toString().equals("CH"))
		{
			
		//	waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN,15);
		//	verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN,"MRN number");
		//	sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN, MRNNRN.toString(),"MRN number");
			
		}
		else {

			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN,"MRN number");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.MRN, MRNNRN.toString(),"MRN number");
				
		}
		*/
		
		if(isElementPresent(By.xpath("//input[@id='txtVatNumber']")))
		{
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,"VAT number");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.VATnumber,VATNumber,"VAT number");
		
		}
		
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.EndCustName,"End Customer Name");
		sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.EndCustName,CustomerName.toString(),"End Customer Name");
	//	waitForAjax();	
		
		if(isElementPresent(By.xpath("//input[@id='txtBuild']")))
				{
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingName,"Building Name");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingName, BuildingName.toString(),"Building Name");			
	
				}
		
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingNumber,"Building Number");
		sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.BuildingNumber, BuildingNumber.toString(),"Building Number");
	//	waitForAjax();	
		
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.StreetName,"Street Name");
		sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.StreetName, StreetName.toString(),"Street Name");
	//	waitForAjax();	
		
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Town,"Town Name");
		sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.Town, Town.toString(),"Town Name");
	//	waitForAjax();	
			
		verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.PostCodeNMTS,"PostCodeNMTS");
		sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.PostCodeNMTS, PostCode.toString(),"PostCodeNMTS");
	//	waitForAjax();	
		
		if(Country.toString().equals("BE"))
		{

			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.RegisteredName,"Registered Name");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.RegisteredName,RegisteredName,"Registered Name");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationLanguage,"Language");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationLanguage,AllocationLanguage,"Language");
	
		}
		if(Country.toString().equals("ES")){
			verifyExists(NMTSPort_IN_NumberObj.PortIN.PortInTelephoneNumber,"Telephone Number");
			sendKeys(NMTSPort_IN_NumberObj.PortIN.PortInTelephoneNumber,TelephoneNumber,"Telephone Number");
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.CIFvalue,"CIFvalue");
			sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.CIFvalue,CIFvalue,"CIFvalue");
		}
		if(Country.toString().equals("AT")){
			verifyExists(NMTSPort_IN_NumberObj.PortIN.PortInTelephoneNumber,"Telephone Number");
			sendKeys(NMTSPort_IN_NumberObj.PortIN.PortInTelephoneNumber,TelephoneNumber,"Telephone Number");
		}
		if(Country.toString().equals("IE"))
		{
			/*if(AllocationRadioButton.toString().equals("Residential"))
			{
				waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,15);
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,"Residential Radio button");
				click(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ResidentialRadiobtn,"Residential Radio button");
				
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationFirstName,"First Name");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationFirstName,AllocationFirstName,"First Name");
			
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationlastName,"Last Name");
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllovationlastName,AllocationLastName,"Last Name");
			
				
			}
			else {
				Report.LogInfo("Log Report",":","Selected country name is Country with Bussiness Radio button for allocation");
				
			}
			*/
		}
			
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,"Submit button");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitDecision,"Submit button");
			waitForAjax();	
			waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
			
			if(Country.toString().equals("UK"))
			{
			//	AcceptJavaScriptMethod();	
				waitForAjax();
				isAlertPresent();
				
			}
			
			WaitForAjax();	
			if(isElementPresent(By.xpath("//*[@id='lblSuccessfulMsg']")))
			{
				String AllocationMessage = getTextFrom(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.AllocationMessage,"Allocation message");
				
				if(AllocationMessage.contains("NMTS Order Allocated")){
					ExtentTestManager.getTest().log(LogStatus.PASS, "port in Order Allocated successfully");
					}else{
						ExtentTestManager.getTest().log(LogStatus.FAIL, "Port in Order not Allocated successfully");	
					}	
			}
			
			//////////////////
			Reusable.switchtodefault();
			WaitForAjax();
			Reusable.switchtoframe("fmMenu");
			//click to activate 
			waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,15);
			verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,"Activate Number option");
			javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumberTab,"Activate Number option");
			Reusable.switchtodefault();
			WaitForAjax();
			Reusable.switchtoframe("fmMain");
			if(Country.toString().equals("IT")||Country.toString().equals("FR"))
			{
				try {
					
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Reservation of Colt");
				Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Colt");
				}
				catch(StaleElementReferenceException r)
				{
					verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Reservation of Colt");
					Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SelectReservationColt,"Colt");
			
				}
				WaitForAjax();
			}
			
				waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,30);
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,"Made By");
				Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.madeBy,MADEBYNAME);
				
			

				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateAvailability,"Availablity");
				Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateAvailability,Availablity);
			
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterBy,"Filter By");
				Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterBy,FilterBy);
				WaitForAjax();
				
				waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,15);
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,"Matching");
				Reusable.Select(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.matching,MatchingWithFilterBy);
				
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterText,"Filter text");
				
				sendKeys(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.FilterText,FilterText,"Filter text");
			
				
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitSearchbtn,"Submit Search button");
				javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.SubmitSearchbtn,"Submit Search button");
				WaitForAjax();
				
				waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,15);
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"OrderId");
			
				javaScriptDoubleclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.OrderId,"Order ID");

			
				waitForElementToBeVisible(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,15);
				verifyExists(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,"Activate Number Button");
				javaScriptclick(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ActivateNumnerbtn,"Activate Number Button");
				WaitForAjax();
				
				waitForElementToDisappear(NMTSNumberActivationFreeNumberObj.NumberActivationFreeNumber.ProcessingMessage,30);
			
				if(Country.toString().equals("UK"))
				{
				//	AcceptJavaScriptMethod();
					isAlertPresent();
				}
		
		
				waitForElementToBeVisible(NMTSPort_IN_NumberObj.PortIN.PortINActivateNumberMessage,15);	
				String ActivateMessage = getTextFrom(NMTSPort_IN_NumberObj.PortIN.PortINActivateNumberMessage,"Activate message");
				if(ActivateMessage.contains("Activated")){
				ExtentTestManager.getTest().log(LogStatus.PASS, "port in Number Activated successfully");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Port in Number not Activated successfully");	
			}
		
	}
		
		

		public void setvalue(String key, String value) {
		Number.put(key, value);
		}

		public String getvalue(String key) {
		return Number.get(key);       
		}

	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
 

	
	
	
	

