package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.ImsNmbrTranslator_Helper;

public class TS_18_IMSN_Translator extends SeleniumUtils {
	
	APT_Login Login = new APT_Login();
	ImsNmbrTranslator_Helper IMSNT = new ImsNmbrTranslator_Helper();
	
	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-01", priority = 1)
	
	public void manageNumberTranslation(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		
		
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			
			
			IMSNT.selectImsTranslator();
			
			IMSNT.verifyManageNumberTranslationcountrypage();
				
				
			IMSNT.verifyManageNumberTranslation(testDataFile,dataSheet,scriptNo,dataSetNo);
		

			IMSNT.verifyWildcardsearch();
				
			IMSNT.verifyAddnumberTranslationfields(testDataFile,dataSheet,scriptNo,dataSetNo);
			
			IMSNT.filladdtranslation(testDataFile,dataSheet,scriptNo,dataSetNo);
			
			IMSNT.verifyAddnumbertranslationfunction(testDataFile,dataSheet,scriptNo,dataSetNo);
				
			IMSNT.editIMSNT(testDataFile,dataSheet,scriptNo,dataSetNo);
				
			IMSNT.synchronise(testDataFile,dataSheet,scriptNo,dataSetNo);
				
			IMSNT.createFileForUploadUpdateFile(testDataFile, dataSheet, scriptNo, dataSetNo);
			IMSNT.uploadUpdatefile(testDataFile,dataSheet,scriptNo,dataSetNo);
			IMSNT.deleteFile(testDataFile,dataSheet,scriptNo,dataSetNo);
			
			IMSNT.downlaodTranslationlink(testDataFile,dataSheet,scriptNo,dataSetNo);
			waitForAjax();
				
			IMSNT.viewUIHistory(testDataFile, dataSheet, scriptNo, dataSetNo);
			waitForAjax();
			
			IMSNT.viewuploadHistory();
				
			IMSNT.deleteIMST(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		}
		
		
		catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		
	}

}
