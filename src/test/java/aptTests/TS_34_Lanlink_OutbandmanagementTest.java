package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;

import testHarness.aptFunctions.Lanlink_Outbandmanagementhelper;

public class TS_34_Lanlink_OutbandmanagementTest extends SeleniumUtils {
	
	
	Lanlink_Outbandmanagementhelper Outband = new Lanlink_Outbandmanagementhelper();
	APT_Login Login = new APT_Login();
	
	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-01", priority = 1)
	public void LANLINK_Outbandmanagement(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();
			
			
			
			
			String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomerSelection");
			String existingCustomer = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomerSelection");

				
				if(newCustomerName.equalsIgnoreCase("yes") && existingCustomer.equalsIgnoreCase("no")) {
						
			
					Outband.CreateCustomer(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						
						String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newCustomer");
						
						Outband.selectCustomertocreateOrder(CustomerName1);
						
						
				}
				else if(newCustomerName.equalsIgnoreCase("no") && existingCustomer.equalsIgnoreCase("Yes")) {
						
					String CustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

						Outband.selectCustomertocreateOrder(CustomerName);
					
						String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");

				}
		

				
					Outband.Verifyfields(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					String CustomerName1 = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingCustomer");
					Outband.selectCustomertocreateOrderfromleftpane(CustomerName1);
					
					Outband.createorderservice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					Outband.selectServiceType(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
	 
					Outband.selectsubtypeunderServiceTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					Outband.VerifyFieldsForServiceSubTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
			
					Outband.selectOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					Outband.selectServiceType(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					Outband.selectsubtypeunderServiceTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					Outband.dataToBeEnteredOncesubservicesselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					Outband.verifysuccessmessage( "Service successfully created");
			 
			
					Outband.verifyorderpanel_editorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					Outband.verifyorderpanel_changeorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					Outband.VerifydatenteredForServiceSubTypeSelected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					Outband.EditTheservicesselected(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					
					Outband.verifysuccessmessage("Service successfully updated.");
					
					
					Outband.syncservices();	
					
					Outband.manageSubnets();
				
					Outband.dump_viewServicepage();
					
					
					String managementConnectionValue=null;
		        	  String  editManageConnection=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"EditService_manageConnectiondropdown");
		        	  String  createManageConnection=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"manageConnectiondropdown");
					
					if(editManageConnection.equalsIgnoreCase("null")) {
						managementConnectionValue=createManageConnection;
					}else {
						managementConnectionValue=editManageConnection;
					}
					
			if(managementConnectionValue.equalsIgnoreCase("Onnet-Offnet")) {
				
				
			
				Outband.Enteraddsiteorder();
				Outband.verifyAddsiteorderFields(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								
				Outband.Enteraddsiteorder();
			
				Outband.addsiteorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				
				
				
				Outband.verifysuccessmessage("Site order created successfully.");
				
				 
				Outband.VerifyDataEnteredForSiteOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				
				
				Outband.returnbacktoviewsiteorderpage();
			
				Outband.editSiteOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Site Order successfully updated.");
				
	        	  String  Technologyname=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"technology");

				if(Technologyname.equalsIgnoreCase("Actelis")) {
					
					boolean equipConfigurationPanel=Outband.EquipmentCOnfigurationPanel();
					if(equipConfigurationPanel) {
						Outband.equipConfiguration_Actelis_AddDevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						Outband.verifysuccessmessage("Device successfully created");
						
						Outband.verifyDataEnteredFordeviceCreation_Actelis(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						Outband.returnbacktoviewsiteorderpage();
						
						Outband.deleteDeviceFromService_EquipmentConfig_Actelis();
						Outband.verifysuccessmessage("Actelis CPE Device successfully deleted and removed from service");
						
					}
				
			//Actelis Configuration panel
						Outband.verifyAddDSLAMandHSLlink();
						Outband.AddDSLAMandHSL(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						Outband.showInterface_ActelisConfiguuration();
						Outband.deletInterface_ActelisConfiguration(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						Outband.successMessage_deleteInterfaceFromDevice_ActelisConfiguration();
					
				}
				
				
				String devicename=null;  String vendorModel=null;
				String manageAdres=null;  String country="";
				
			//	String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrderNumber_OnnetOffnet");

				
				
			//verify whether Equipment panel is available	
				boolean EquipmentPanel=Outband.findPanelHeader("Equipment");
				if(EquipmentPanel) {
					String deviceCreation_Equipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "deviceCreation_Equipment");

					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Equipment' panel is displaying under 'view site order' page as expected");
					//Verify whether Equipment device to be created
							if((deviceCreation_Equipment).equalsIgnoreCase("yes")){
				//	String deviceCreation_Equipment = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "deviceCreation_Equipment");

					String speed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
					String existingDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Equip_ExistingDeviceSelection");
					String newDevice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Equip_NewDeviceSelection");
					
					if((deviceCreation_Equipment).equalsIgnoreCase("yes")){
					
					if(speed.equals("1GigE")) {
						if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							
						}
							Outband.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo,speed);
							Outband.verifysuccessmessage("Device successfully created");
							
							Outband.verifyValuesforCPEexistingdevice_1G_Equipment();
							
							Outband.eDITCPEdevicedetailsentered_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							Outband.verifysuccessmessage("Device successfully updated");
							
						}
						else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("Yes")) {
							
							Outband.verifyFieldsandAddCPEdevicefortheserviceselected_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							Outband.verifysuccessmessage( "Device successfully created");
							
							Outband.verifydetailsEnteredforCPEdevice_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							
							Outband.eDITCPEdevicedetailsentered_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							Outband.verifysuccessmessage("Device successfully updated");
						}
						
					}
					
					
					else if(speed.equals("10GigE")) {
						
						if(existingDevice.equalsIgnoreCase("Yes") && newDevice.equalsIgnoreCase("No")) {
							Outband.verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo,speed);
							Outband.verifysuccessmessage("Device successfully created");

							Outband.verifyValuesforCPEexistingdevice_10G_Equipment( );
							
							
							Outband.eDITCPEdevicedetailsentered_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							Outband.verifysuccessmessage( "Device successfully updated");
						}
							
						else if(existingDevice.equalsIgnoreCase("No") && newDevice.equalsIgnoreCase("yes")) {
							Outband.verifyFieldsandAddCPEdevicefortheserviceselected_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							Outband.verifysuccessmessage("Device successfully created");
							
							
							Outband.verifydetailsEnteredforCPEdevice_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							
							
							Outband.eDITCPEdevicedetailsentered_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							Outband.verifysuccessmessage("Device successfully updated");
						}
					}	
					
				}
				else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "Equipment device is not created as expected");
					System.out.println("Equipment device is not created as expected");
						}
				}else {
					ExtentTestManager.getTest().log(LogStatus.INFO, " 'Equipment' panel is not displaying under 'view site order' page");
					System.out.println(" 'Equipment' panel is not displaying under 'view site order' page");
				}
				
				   
				
				
						String devicename_intEquip=null;  String country_intEquip="";
						String manageAdres_intEqiup=null; String vendorModel_intEqiup=null;
						
						boolean IntermediateEquipmentPanel=Outband.findPanelHeader("Intermediate Equipment");
						if(IntermediateEquipmentPanel) {

				        	  String  deviceCreation_IntermediateEquipment=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"deviceCreation_IntermediateEquipment");

							if((deviceCreation_IntermediateEquipment).equalsIgnoreCase("yes")){
								
						  String  speed=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Interfacespeed");
			        	  String  existingDevice=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IntEquip_existingdeviceSelection");
			       	  String  newDevice=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"IntEquip_newdeviceSelection");

		        	  String  siteOrderNumber_PointToPoint=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"siteOrderNumber_PointToPoint");

						//Outband.clickOnBreadCrump(siteOrderNumber_PointToPoint);
						
						if(speed.equals("1GigE")) {
							if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
								Outband.addDevice_IntEquipment();
								Outband.selectTechnology(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								Outband.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								
								Outband.verifyValuesforCPEexistingdevice_1G_intEquipment();
								
								Outband.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								Outband.verifysuccessmessage("Device successfully updated");
								
							}
							else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
								Outband.addDevice_IntEquipment();
								Outband.selectTechnology(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								Outband.verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);	
								Outband.verifysuccessmessage("Device successfully created");
								
							   Outband.verifyCPEdevicedataenteredForIntermediateEquipment_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							   
							   Outband.EDITCPEdevicedforIntermediateEquipment_1G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
							   Outband.verifysuccessmessage("Device successfully updated");
							}
						}
				    else if(speed.equals("10GigE")) {
				    	if(existingDevice.equalsIgnoreCase("Yes")  && newDevice.equalsIgnoreCase("No")) {
				    		Outband.addDevice_IntEquipment();
				    		Outband.selectTechnology(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				    		Outband.verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				    		Outband.verifysuccessmessage("Device successfully created");
				    		Outband.verifyValuesforCPEexistingdevice_10G_intEquipment();
				    		
				    		Outband.EDITCPEdevice_IntermediateEquipment_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				    		Outband.verifysuccessmessage("Device successfully updated");
				    	}
				    	else if(existingDevice.equalsIgnoreCase("No")  && newDevice.equalsIgnoreCase("Yes")) {
				    		Outband.addDevice_IntEquipment();
				    		Outband.selectTechnology(testDataFile,  dataSheet,  scriptNo,  dataSetNo);	
				    	
							
						   Outband.verifyCPEdevicedataenteredForIntermediateEquipment_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						   
						   Outband.EDITCPEdevice_IntermediateEquipment_10G(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						   Outband.verifysuccessmessage("Device successfully updated");
				    		}
						}
						
					}
						}
						
						
			        	  String  Edit_serviceNumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Edit_serviceNumber");
			        	  String  serviceNumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"serviceNumber");
			        	 // String  Edit_serviceNumbe=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Edit_serviceNumber");

						String ServiceID = null;
						if((Edit_serviceNumber).equalsIgnoreCase("null")) {
							ServiceID=serviceNumber;
						}else {
							ServiceID=Edit_serviceNumber;
						}
						
					//	Outband.selectRowForsiteorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						Outband.deleteSiteOrder();
						
						
						Outband.deleteService();

				
			}
			else if(managementConnectionValue.equalsIgnoreCase("IPC Based")) {
				
				
				Outband.Enteraddsiteorder();
				Outband.addIPVPNsiteorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Site order created successfully .");
				
				
				Outband.VerifyDataEnteredForIPVPNSiteOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.returnbacktoviewsiteorderpage();
				
				
				Outband.selectRowForIPVPNsiteorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.editIPVPNsiteOrder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Site Order successfully updated.");
				
				Outband.providerEquipment(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifyPEdeviceEnteredvalue(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				
	        	 String  Siteordernumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Siteordernumber");

				Outband.clickOnBreadCrump(Siteordernumber);
				Outband.cickOnViewButton_PEdevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);   //navigate to view device page
				Outband.clickOnAddInterfaceLink_PE();  //click on "add Interface" link
				Outband.addInterface(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Interface added successfully");
				
	        	 String  Pe_chooseAdevice=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Pe_chooseAdevice");

				Outband.clickOnBreadCrump(Pe_chooseAdevice);
				Outband.clickOnAddMultiLink_PE();  //click on "add Multilink" link
				Outband.addMultilink(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage( "Multilink added successfully");
				
	        	

				Outband.clickOnBreadCrump(Pe_chooseAdevice);
				Outband.clickOnAddLoopback_PE();
				Outband.addLoopback(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Loopback added successfully");
				
				Outband.clickOnBreadCrump(Siteordernumber);
				Outband.clickOnautoDiscoverVPNPEdevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				
				Outband.fetchCityName();
				Outband.clickOnCustomerPremiseEquipmentLink_addDevice();
				Outband.addCPEdevice_CustomerPremiseEquipment(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Site device created successfully ");
				Outband.verifyCPEdevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				
		String	CPEdeviceName_CustomerPremiseEquipment=Outband.fetchdeviceNameFromviewDevicePage();
				Outband.clickOnBreadCrump(Siteordernumber);
				Outband.cickOnEditButton_CPEdevice(CPEdeviceName_CustomerPremiseEquipment);
				Outband.editCPEdevice_CustomerPremiseEquipment(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Site device updated successfully");
				
				Outband.clickOnAddInterfaceLink_CPE();  //click on "add Interface" link
				Outband.addInterface_CPEdevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Interface successfully created");
				
				Outband.clickOnAddMultiLink_CPE();  //click on "add Multilink" link
				Outband.addMultilink_CPEdevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage( "Multilink interface successfully created");
				
				String CPEdeviceName=Outband.fetchdeviceNameFromviewDevicePage();
				Outband.clickOnBreadCrump(Siteordernumber);
				Outband.clickOnpppConfigurationButton_CPEdevice( CPEdeviceName);
				Outband.pppConfiguration();
				Outband.addPPPconfiguration(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Multilink interface successfully created");
				
				Outband.viewPPPconfiguration(testDataFile,  dataSheet,  scriptNo,  dataSetNo, CPEdeviceName );
				
				Outband.pppConfigurationClickOnEditLink();
				Outband.editPPPconfiguration(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.verifysuccessmessage("Device successfully updated");
				
				Outband.deletePPPconfiguration(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				
				Outband.clickOnBreadCrump(Siteordernumber);
				Outband.clickOnViewButton_CPEdevice(CPEdeviceName);
				Outband.addCustomerReadonlySNMPFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
				Outband.editCustomerReadonlySNMPFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					String CustomerIPvalue = null;
		        	  String  CustomerIPAddressEdit=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CustomerIPAddressEdit");
		        	  String  CustomerIPAddress=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CustomerIPAddress");

					if((CustomerIPAddressEdit).equalsIgnoreCase("null")) {
						CustomerIPvalue = CustomerIPAddress;
					}else {
						CustomerIPvalue = CustomerIPAddressEdit;
					}
				Outband.deleteCustomerReadonlySNMPFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo,CustomerIPvalue);
			
				Outband.addExtraSubnetFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
				Outband.editNATConfigurationFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
			
	        	  String  StaticNATEdit=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"StaticNATEdit");

					if((StaticNATEdit).equalsIgnoreCase("Yes")) {
						Outband.addStaticNATMappingFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
						Outband.editStaticNATMappingFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								String localIP = null;
					        	  String  Static_LocalIPEdit=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Static_LocalIPEdit");
					        	  String  Static_LocalIP=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Static_LocalIP");

								if((Static_LocalIPEdit).equalsIgnoreCase("null")) {
									localIP = Static_LocalIP;
								}else{
									localIP = Static_LocalIPEdit;
								}
						Outband.deleteStaticNATMappingFunction_CPE(localIP);
					}
					else {
						ExtentTestManager.getTest().log(LogStatus.INFO, "Static NAT Configuration panel not displaying, as 'Static NAT' checkbox is unselected under 'NAT Configuration'");
					}
		        	  String  DynamicNATEdit=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"DynamicNATEdit");
					
					if((DynamicNATEdit).equalsIgnoreCase("Yes")) {
						Outband.addDynamicNATMappingFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
								
						Outband.editDynamicNATMappingFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
		
						Outband.deleteDynamicNATMappingFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					}
					else {
						ExtentTestManager.getTest().log(LogStatus.INFO, "Dynamic NAT Configuration panel not displaying,  as 'Dynamic NAT' checkbox is unselected under 'NAT Configuration'");
					}
			
					Outband.addDHCPServersonCPEFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					Outband.editDHCPServersonCPEFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo);

							String customerLANsubnet = "Null";
				        	  String  DHCP_CustomerLANSubnetEdit=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"DHCP_CustomerLANSubnetEdit");
				        	  String  DHCP_CustomerLANSubnet=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"DHCP_CustomerLANSubnet");

							if((DHCP_CustomerLANSubnetEdit).equalsIgnoreCase("Null")) {
								customerLANsubnet = DHCP_CustomerLANSubnet;
							}else {
								customerLANsubnet = DHCP_CustomerLANSubnetEdit;
							}
					Outband.deleteDHCPServersonCPEFunction_CPE(testDataFile,  dataSheet,  scriptNo,  dataSetNo,customerLANsubnet);
					ExtentTestManager.endTest();
					
					Outband.generateConfiguration(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
		        	//  String  Siteordernumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Siteordernumber");

					Outband.clickOnBreadCrump(Siteordernumber);
					Outband.createPEtoCPElink(testDataFile,  dataSheet,  scriptNo,  dataSetNo, "null",
							CPEdeviceName , "null");
					
					Outband.deletePEdevice(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					
					Outband.deleteCPEdevice(CPEdeviceName);
					
					String ServiceID = null;
		        	  String  Edit_serviceNumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Edit_serviceNumber");
		        	  String  serviceNumber=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"serviceNumber");

					if((Edit_serviceNumber).equalsIgnoreCase("null")) {
						ServiceID=serviceNumber;
					}else {
						ServiceID=Edit_serviceNumber;
					}
					
					Outband.clickOnBreadCrump(ServiceID);
					Outband.selectRowForsiteorder(testDataFile,  dataSheet,  scriptNo,  dataSetNo);
					Outband.deleteSiteOrder();
					
					Outband.deleteService();
					
			}
		
			
			
			
		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
					ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}
	
}
	
	



