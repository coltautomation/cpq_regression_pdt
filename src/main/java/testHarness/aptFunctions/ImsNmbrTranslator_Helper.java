package testHarness.aptFunctions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APT_DDIRangeObj;
import pageObjects.aptObjects.IMSNTranslatorObj;

public class ImsNmbrTranslator_Helper extends SeleniumUtils{
	
	
	public void selectImsTranslator()throws InterruptedException, IOException {
		
		Reporter.log("select Manage Colt's Network");
	//	ExtentTestManager.getTest().log(LogStatus.PASS, "Step : clicked on 'Manage colt's Network' link");
		
		waitForAjax();
		
		mouseMoveOn(IMSNTranslatorObj.IMSNT.ManageColtNetworkLink);
		waitForAjax();
		Reporter.log("Mouse hovered on Manage Colt Network ");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Step : Mouse hovered on Manage Colt Network");
			
			isVisible((IMSNTranslatorObj.IMSNT.ManageIms));
		//	sa.assertTrue(isDisplayed,"Manage IMS Number Translation is not displayed ");
			
			Reporter.log("Manage IMS Number Translation link verifeid");
			//ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Manage IMS Number Translation is displayed ");
			
			verifyExists(IMSNTranslatorObj.IMSNT.ManageIms,"ManageIms");
			click(IMSNTranslatorObj.IMSNT.ManageIms,"ManageIms");
			
			waitForAjax();
			
			Reporter.log("=== Clicked on Manage IMS Number Translation link ===");
			Reporter.log("Clicked on Manage IMS Number Translation link");
			ExtentTestManager.getTest().log(LogStatus.PASS, "Step :Clicked on Manage IMS Number Translation link");

 		    ExtentTestManager.getTest().log(LogStatus.PASS, "Navigated to Manage Number Translation");

 		   Reporter.log("=== Navigated to Manage Number Translation ===");
	}
	
public void verifyManageNumberTranslationcountrypage()throws InterruptedException, IOException {
		
	String[] CountryList= {"SE Manage Number Translation","UK Manage Number Translation","BR Manage Number Translation","PT Manage Number Translation","CH Manage Number Translation",
			"IE Manage Number Translation","AT Manage Number Translation","IT Manage Number Translation"};
	Reporter.log(""+CountryList.length);
	ExtentTestManager.getTest().log(LogStatus.PASS, " Verifying list of countries");

	
	//Verifying list of Countries
	//try {
	List<WebElement> CountryPresent =findWebElements(IMSNTranslatorObj.IMSNT.CountryPresent);
	for (WebElement Country: CountryPresent ) {
		//boolean match = false;
		Reporter.log("Country list displaying from application:"  +Country.getText());
		ExtentTestManager.getTest().log(LogStatus.PASS, "Country list displaying from application:"+Country.getText());

		
		for (int i=0; i < CountryList.length;i++)
		{
			if (Country.getText().equals(CountryList[i])) {
				//match = true;
				Reporter.log("Country name : " + Country.getText());
				ExtentTestManager.getTest().log(LogStatus.PASS, "Country name :" + Country.getText());
//				Log.info(CountryPresent);
//				Log.info("Country list matched");
//				ExtentTestManager.getTest().log(LogStatus.PASS, "Country list matched");

			}
							
		}
		Reporter.log("Country lists got mismatched");
			//ExtentTestManager.getTest().log(LogStatus.PASS, "Country list not matched");
			//sa.assertTrue(match);

				}
	}


public void verifyManageNumberTranslation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException 
{
	String Countrylist = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "countryCode");
	
	
	if (Countrylist.equals("BR"))
	{
	ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for BR");

	waitForAjax();
	javaScriptclick(IMSNTranslatorObj.IMSNT.BRManageTran);
	Reporter.log("Clicked on BR Manage Number Translation");
	//Reporter.log("Clicked on BR Manage Number Translation");

	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on BR Manage Number Translation");
	
	}
	
	else if (Countrylist.equals("IT"))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for IT");
		
		waitForAjax();
		javaScriptclick(IMSNTranslatorObj.IMSNT.ITManageTran);
		Reporter.log("Clicked on IT Manage Number Translation");
		//Reporter.log("Clicked on IT Manage Number Translation");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on IT Manage Number Translation");

	}
	
	else if (Countrylist.equals("CH"))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for CH");
		
		waitForAjax();
		javaScriptclick(IMSNTranslatorObj.IMSNT.CHManageTran);
		Reporter.log("Clicked on CH Manage Number Translation");
		Reporter.log("Clicked on CH Manage Number Translation");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on IT Manage Number Translation");

	}

	
	else if (Countrylist.equals("IE"))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for IE");
		
		waitForAjax();
		javaScriptclick(IMSNTranslatorObj.IMSNT.IEManageTran);
		Reporter.log("Clicked on IE Manage Number Translation");
	//	Log.info("Clicked on IE Manage Number Translation");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on IE Manage Number Translation");

	}
	
	
	else if (Countrylist.equals("AT"))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for AT");
		
		waitForAjax();
		javaScriptclick(IMSNTranslatorObj.IMSNT.ATManageTran);
		Reporter.log("Clicked on AT Manage Number Translation");
		//Log.info("Clicked on AT Manage Number Translation");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on AT Manage Number Translation");

	}
	
	else if (Countrylist.equals("SE"))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for SE");
		
		waitForAjax();
		javaScriptclick(IMSNTranslatorObj.IMSNT.SEManageTran);
		Reporter.log("Clicked on SE Manage Number Translation");
		//Log.info("Clicked on SE Manage Number Translation");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on SE Manage Number Translation");

	}
	
	else if (Countrylist.equals("UK"))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for UK");
		
		Thread.sleep(5000);
		javaScriptclick(IMSNTranslatorObj.IMSNT.UKManageTran);
		Reporter.log("Clicked on UK Manage Number Translation");
		//Reporter.log("Clicked on UK Manage Number Translation");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on UK Manage Number Translation");

	}
	else if (Countrylist.equals("PT"))
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Manage Number Translation filed for PT");
		
		Thread.sleep(5000);
		javaScriptclick(IMSNTranslatorObj.IMSNT.PTManageTran);
		Reporter.log("Clicked on PT Manage Number Translation");
		//Reporter.log("Clicked on PT Manage Number Translation");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on PT Manage Number Translation");

	}
	

	verifyExists(IMSNTranslatorObj.IMSNT.TranslationWildcard, "You can use * as wildcard");
	
//	compareText(application, "Number To Translate", "NumberToTranslate", "Number To Translate", xml);
//	
//	compareText(application, "Number Translated", "NumberToTranslated", "Number Translated", xml);
//	
//	compareText(application, "Country", "Country", "Country", xml);
//	
//	compareText(application, "Carrier", "Carrier", "Carrier", xml);
//	
//	compareText(application, "Nature Of Address", "NatureofAddress", "Nature Of Address", xml);
//	
//	compareText(application, "PSX Sync Status", "PSXsyncstatus", "PSX Sync Status", xml);
//	
//	compareText(application, "Add Number Translation", "AddnumberTranslation", "Add Number Translation", xml);
//	
//	compareText(application, "View Upload History", "ViewUploadHistory", "View Upload History", xml);
//	
//	compareText(application, "View UI History", "ViewUIHistory", "View UI History", xml);
//	
//	compareText(application, "Upload Update File", "Uploadupdatefile", "Upload Update File", xml);
//	
//	compareText(application, "Download Number Translation", "DownloadNumberTranslation", "Download Number Translation", xml);
//
//	compareText(application, "Synchronize All", "SynchronizeAll", "Synchronize All", xml);
	
}


public void verifyWildcardsearch () throws Exception
{
	ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Wildcard * search field");
	
	verifyExists(IMSNTranslatorObj.IMSNT.ImsSearcfield,"*");
	sendKeys(IMSNTranslatorObj.IMSNT.ImsSearcfield,"*");
	
	Reporter.log("Value Entered in TextField");
	//Log.info("Value Entered in Search TextField");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Value Entered in Search TextField" );
	
	verifyExists(IMSNTranslatorObj.IMSNT.Searchbtn,"Searchbtn");
	click(IMSNTranslatorObj.IMSNT.Searchbtn,"Searchbtn");
	
	Reporter.log("Clicked on Search button");
	//Log.info("Clicked on search button");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on search button" );
	
	waitForAjax();
	
	String ExpWildcardmsg = "The following errors were encountered: At least 3 digits to be entered in the search field.";
	Reporter.log("Postcode message");
	//WebElement ActWildcardmsg = driver.findElement(By.xpath("//span[contains(text(),'The following errors were encountered: At least 3 digits to be entered in the search field.')]"));
	WebElement ActWildcardmsg = (findWebElement(IMSNTranslatorObj.IMSNT.Wildcardmsg));
	if (ActWildcardmsg.getText().equals(ExpWildcardmsg))
	{
		Reporter.log("Wildcarde Message : " + ActWildcardmsg.getText());
		ExtentTestManager.getTest().log(LogStatus.PASS, "Postcode Message :  :" + ActWildcardmsg.getText());
		Reporter.log("Message Displayed as:"+ActWildcardmsg.getText());
		ExtentTestManager.getTest().log(LogStatus.PASS, "Wildcard Search Message Displayed as:" +ActWildcardmsg.getText());
		ExtentTestManager.getTest().log(LogStatus.PASS, "Wildcard Search Method Verified and Passed" );

	}
	else
	{
		//sa.assertTrue(ActWildcardmsg.isDisplayed(),"Message not displayed");
		Reporter.log("Postcode success message not displayed");
		Reporter.log("Wildcard Search message not displayed or mismatched");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Wildcard Search message not displayed or mismatched and Test Failed" );

	}
	waitForAjax();
	verifyExists(IMSNTranslatorObj.IMSNT.ImsSearcfield,"ImsSearcfield");
	findWebElement(IMSNTranslatorObj.IMSNT.ImsSearcfield).clear();
}


public void verifyAddnumberTranslationfields(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException 
{
	String Countrylist = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "countryCode");
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Add number Translation field" );
	 Report.LogInfo("INFO", "Verifying Add number Translation field", "PASS");
	 
	verifyExists(IMSNTranslatorObj.IMSNT.AddnumberTranslation,"AddnumberTranslation");
	click(IMSNTranslatorObj.IMSNT.AddnumberTranslation,"AddnumberTranslation");
	waitForAjax();
	
//Number to translate	
	verifyExists(IMSNTranslatorObj.IMSNT.Numbertranslate,"Number to Translate");
	
//Country Code	
	verifyExists(IMSNTranslatorObj.IMSNT.Countrycode, "Country Code");
	
//Prefix	
	verifyExists(IMSNTranslatorObj.IMSNT.Prefix,"Prefix");
	
	WebElement ActCountrycode = findWebElement(IMSNTranslatorObj.IMSNT.ActCountrycode);
	if (ActCountrycode.getText().equals(Countrylist))
	{
		Reporter.log("Country Code is same as selected country" + ActCountrycode.getText());
		//Reporter.log("Country Code is same as selected country:" +ActCountrycode.getText());
		ExtentTestManager.getTest().log(LogStatus.PASS, "Country Code is same as selected country" + ActCountrycode.getText());
	}
	else
	{
		//sa.assertTrue(ActCountrycode.isDisplayed(),"Country Code not displayed or mismatched");
		Reporter.log("Country Code not displayed or mismatched");
		//Log.info("Country Code not displayed or mismatched");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Country Code not displayed or mismatched" );

	}							
	
	if ((Countrylist.equals("SE"))||(Countrylist.equals("UK"))||(Countrylist.equals("PT"))||
			(Countrylist.equals("CH"))||(Countrylist.equals("IT"))||(Countrylist.equals("IE"))||
			(Countrylist.equals("AT")))

	{
	   //Carrier	
		verifyExists(IMSNTranslatorObj.IMSNT.CarrierLabel,"Carrier");
		
	}
	
	
	else if ((Countrylist.equals("BR"))||(Countrylist.equals("UK")))
			
	{
		verifyExists(IMSNTranslatorObj.IMSNT.NatureofAddressLabel,"Nature of Address");

//Nature of address dropdown			
	String[] NatureofAddress = {"Subscriber","National","International","Unknown"};
	Reporter.log("Nature of address Values :" +NatureofAddress);
	
	boolean Natureofadddropdown = findWebElement(IMSNTranslatorObj.IMSNT.Natureofadddropdown).isDisplayed();
	//sa.assertTrue(Natureofadddropdown,"Nature of adddress dropdown is not displayed");
	Reporter.log("Nature of adddress dropdown field is verified");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Nature of adddress dropdown field is verified");
	
	//Verifying list of values from dropdown
	
	//List<WebElement> listofaddress = driver.findElements(By.xpath("//div/span[@role='option']"));
	List<WebElement> listofaddress = findWebElements(IMSNTranslatorObj.IMSNT.listofaddressDropDown);
	for (WebElement valueslist : listofaddress) 
		
		
	{
		Reporter.log("Country list displaying from application:"  +valueslist.getText());
		ExtentTestManager.getTest().log(LogStatus.PASS, "Country list displaying from application:"+valueslist.getText());
		boolean match = false;
		
		for (int i = 0; i <NatureofAddress.length;i++) {
			if (valueslist.getText().equals(NatureofAddress[i]))
			{
				match = true;
				Reporter.log("Nature of address values displaying from page dropdown" + valueslist.getText());
				//Log.info("Nature of address values displaying from page dropdown :" + valueslist.getText());
				ExtentTestManager.getTest().log(LogStatus.PASS, "Nature of address values displaying from page dropdown : " + valueslist.getText());
			}
			
		}
		Reporter.log("Nature of address values displaying from page dropdown not matched");
		ExtentTestManager.getTest().log(LogStatus.PASS, "Nature of address values displaying from page dropdown not matched");
		//sa.assertTrue(match);

	}
	
//Range checkbox	
	compareText("Range", "RangeLabel","Range");
	
	verifyExists(IMSNTranslatorObj.IMSNT.RangeFlag,"RangeFlag");
	click(IMSNTranslatorObj.IMSNT.RangeFlag,"RangeFlag");
	waitForAjax();
	
//Sequence	
	compareText("Sequence", "sequenceLabelName", "Sequence");
	
	
	}
	
//Number Translated	
	verifyExists(IMSNTranslatorObj.IMSNT.NumberToTranslatedLabel,"Number Translated");
	
//OK button	
	verifyExists(IMSNTranslatorObj.IMSNT.OkbtnPostcode,"OK");
	
	verifyExists(IMSNTranslatorObj.IMSNT.xbutton,"xbutton");
	click(IMSNTranslatorObj.IMSNT.xbutton,"xbutton");
	
}


public void filladdtranslation(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException 
{
	String Countrylist = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "countryCode");
	String TranslateNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslateNumber");
	String NatureAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NatureOfAddress");
	String CarrierNo = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CarrierNo");
	String Prefix = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "prefixCheckbox");
	String range = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Range");
	String sequence = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "sequence");
	String countryName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "countryName");
	String TranslatedNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslatedNumber");
	String PrefixNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PrefixNumbertextField");

	verifyExists(IMSNTranslatorObj.IMSNT.AddnumberTranslation,"AddnumberTranslation");
	click(IMSNTranslatorObj.IMSNT.AddnumberTranslation,"AddnumberTranslation");
	
	//Number to translate	
	verifyExists(IMSNTranslatorObj.IMSNT.Numbertotranslate_Text,"TranslateNumber");
	sendKeys(IMSNTranslatorObj.IMSNT.Numbertotranslate_Text,TranslateNumber ,"TranslateNumber");
		
	//translated Number	
	verifyExists(IMSNTranslatorObj.IMSNT.Numbertranslated_Text, "TranslatedNumber");
	sendKeys(IMSNTranslatorObj.IMSNT.Numbertranslated_Text,TranslatedNumber, "TranslatedNumber");
		
	//Prefix number	
	verifyExists(IMSNTranslatorObj.IMSNT.PrefixText, "PrefixNumber");
	sendKeys(IMSNTranslatorObj.IMSNT.PrefixText,PrefixNumber, "PrefixNumber");
			
			
		if ((Countrylist.equals("CH"))) {

			verifyExists(IMSNTranslatorObj.IMSNT.carrierDropdown, "carrierDropdown");
			addDropdownValues_commonMethod( "Carrier", IMSNTranslatorObj.IMSNT.carrierDropdown,CarrierNo);

			verifyExists(IMSNTranslatorObj.IMSNT.prefix_checkbox, "carrierDropdown");
			addCheckbox_commonMethod(IMSNTranslatorObj.IMSNT.prefix_checkbox, "Prefix", Prefix);
			
		}
		else if((Countrylist.equals("SE"))|| (Countrylist.equals("IE"))|| (Countrylist.equals("PT")) || (Countrylist.equals("AT")) ||
				(Countrylist.equals("IT"))) {
			
			verifyExists(IMSNTranslatorObj.IMSNT.carrierDropdown, "carrierDropdown");
			addDropdownValues_commonMethod("Carrier", IMSNTranslatorObj.IMSNT.carrierDropdown, CarrierNo);
			
			verifyExists(IMSNTranslatorObj.IMSNT.ddiPrefix_checkbox, "ddiPrefix_checkbox");
			addCheckbox_commonMethod(IMSNTranslatorObj.IMSNT.ddiPrefix_checkbox, "DDI Prefix", Prefix);

		}
		else if ((Countrylist.equals("BR")))  {
			
			verifyExists(IMSNTranslatorObj.IMSNT.Country_Text,"Country_Text");
			sendKeys(IMSNTranslatorObj.IMSNT.Country_Text, Countrylist,"Country_Text");
			
			verifyExists(IMSNTranslatorObj.IMSNT.Country_Text,"natureOfAddressDropdown");
			addDropdownValues_commonMethod("Nature of Address", IMSNTranslatorObj.IMSNT.natureOfAddressDropdown,NatureAddress);
		
			addCheckbox_commonMethod(IMSNTranslatorObj.IMSNT.RangeFlag, "Range", range);
		
			if(range.equalsIgnoreCase("yes")) {
				
				//Sequence
				verifyExists(IMSNTranslatorObj.IMSNT.sequencetextField,"Sequence");
				sendKeys(IMSNTranslatorObj.IMSNT.sequencetextField, sequence, "Sequence");
			}
		}
		else if (Countrylist.equals("UK")){
			verifyExists(IMSNTranslatorObj.IMSNT.carrierDropdown,"Sequence");
			sendKeys(IMSNTranslatorObj.IMSNT.carrierDropdown, CarrierNo, "CarrierNo");
			
			verifyExists(IMSNTranslatorObj.IMSNT.Country_Text,"countryName");
			sendKeys(IMSNTranslatorObj.IMSNT.Country_Text, countryName, "countryName");
			
			verifyExists(IMSNTranslatorObj.IMSNT.natureOfAddressDropdown,"natureOfAddressDropdown");
			addDropdownValues_commonMethod("Nature of Address", IMSNTranslatorObj.IMSNT.natureOfAddressDropdown, NatureAddress);
		
			verifyExists(IMSNTranslatorObj.IMSNT.RangeFlag,"RangeFlag");
			addCheckbox_commonMethod(IMSNTranslatorObj.IMSNT.RangeFlag, "Range", range);
		
			if(range.equalsIgnoreCase("yes")) {
				
				//Sequence
				verifyExists(IMSNTranslatorObj.IMSNT.sequencetextField,"sequencetextField");
				sendKeys(IMSNTranslatorObj.IMSNT.sequencetextField, sequence, "sequence");
			}
			
		}
		verifyExists(IMSNTranslatorObj.IMSNT.OkbtnPostcode,"OkbtnPostcode");
		click(IMSNTranslatorObj.IMSNT.OkbtnPostcode, "OkbtnPostcode");
		
		waitForAjax();
			
		Reporter.log("NumberTranslation successfully created.Sync started successfully. Please check the sync status of number translation.");

	//Verifying list of values under 'Action' dropdown
		ExtentTestManager.getTest().log(LogStatus.INFO, "Verify links under 'Action' dropdown");
		Reporter.log( "Verify lins under 'Action' dropdown");
			
		//Enter 'Number To Translate' value in search text box
		scrollDown(IMSNTranslatorObj.IMSNT.searchField_IMSNT);
 		sendKeys(IMSNTranslatorObj.IMSNT.searchField_IMSNT, TranslateNumber,"searchField_IMSNT");

 		
 		waitforPagetobeenable();
	//click on Search button
		verifyExists(IMSNTranslatorObj.IMSNT.Searchbtn,"Search");
		click(IMSNTranslatorObj.IMSNT.Searchbtn,"Search");
		
		waitForAjax();
		
			WebElement addIMSNTLink=findWebElement(IMSNTranslatorObj.IMSNT.AddnumberTranslation);
			scrollDown(IMSNTranslatorObj.IMSNT.AddnumberTranslation);
			waitForAjax();
			String[] ExpectedActiondrop = {"View","Edit","Delete","Synchronize"};
			Reporter.log("Action DropDown values " +ExpectedActiondrop);
			
		//	verifyExists(IMSNTranslatorObj.IMSNT.PostcodeAction,"Action");
		//	click(IMSNTranslatorObj.IMSNT.PostcodeAction,"Action");
			
			List<WebElement> ActualActionDD =findWebElements(IMSNTranslatorObj.IMSNT.ActionDD);

			//List<WebElement> ActualActionDD = driver.findElements(By.xpath("//a[@role='button']"));
			for (WebElement Dropdownlist : ActualActionDD) 
				
			{
				Reporter.log("Action Drop down Values displaying from application:"  +Dropdownlist.getText());
				ExtentTestManager.getTest().log(LogStatus.PASS, "Country list displaying from application:"+Dropdownlist.getText());
				boolean match = false;
				
				for (int i = 0; i <ExpectedActiondrop.length;i++) {
					if (Dropdownlist.getText().equals(ExpectedActiondrop[i]))
					{
						match = true;
						Reporter.log("Action Drop down values" + Dropdownlist.getText());
						//Log.info("Action Drop down values as:" + Dropdownlist.getText());
						ExtentTestManager.getTest().log(LogStatus.PASS, "Action Drop down values are as expected:"  +Dropdownlist.getText());
					}
					
				}
				//sa.assertTrue(match);
			}
		
}

public void verifyAddnumbertranslationfunction(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws Exception 
{
	String CountryCode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "countryCode");
	String TranslateNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslateNumber");
	String TranslatedNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslatedNumber");
	String PrefixNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PrefixNumbertextField");
	String NatureOfAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NatureOfAddress");
	String CarrierNo = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CarrierNo");
	String prefixCheckbox = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "prefixCheckbox");

	//Verifying the values in View
	
	waitforPagetobeenable();
	
	waitForAjax();
	String prefixValue=null;
	
//Country field
	//String countryName=viewPage_country(CountryCode);
	
//Prefix checkbox	
	if(prefixCheckbox.equalsIgnoreCase("yes")) {
		prefixValue="true";
	}else if(prefixCheckbox.equalsIgnoreCase("no")) {
		prefixValue="false";
	}
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying View operation" );
	Reporter.log("Verifying View operation");
	

	selectAddedNumberToTranslateInTable(TranslateNumber);
	waitForAjax();
	
	//click(IMSNTranslatorObj.IMSNT.PostcodeAction,"Action");
//	waitForAjax();
	verifyExists(IMSNTranslatorObj.IMSNT.TranslationView, "View_Link");
	click(IMSNTranslatorObj.IMSNT.TranslationView, "View_Link");		//click on View link
	waitForAjax();
	

//View IMSNT Page	
	compareText_InViewPage2("Number to Translate",TranslateNumber);
	
	compareText_InViewPage2("Country Code",CountryCode);
	
	compareText_InViewPage2("Prefix",PrefixNumber);
	
	compareText_InViewPage2("Number Translated",TranslatedNumber);

	
	if ((CountryCode.equals("UK")) || (CountryCode.equals("BR")) ||(CountryCode.equals("IE")) || (CountryCode.equals("AT")))
	{
		compareText_InViewPage2("Carrier",CarrierNo);		//Carrier
	
		verifyExists(IMSNTranslatorObj.IMSNT.viewPage_CountryField,"Country");		//Country Name
		
		compareText_InViewPage2("Nature of Address", NatureOfAddress);		//Nature of Address
	
	}
	else if ((CountryCode.equals("SE")) ||(CountryCode.equals("IT")))
	{
		compareText_InViewPage2("Carrier",CarrierNo);		//Carrier
	
		verifyExists( IMSNTranslatorObj.IMSNT.viewPage_CountryField, "countryName");		//Country Name
		
		compareText_InViewPage2("Nature of Address", NatureOfAddress);		//Nature of Address
		
		compareText_InViewPage2( "DDI Prefix", prefixValue);		//DDI Prefix
	
	}
	
	else if ((CountryCode.equals("PT")))
	{
		
		compareText_InViewPage2("Carrier",CarrierNo);		//Carrier
		
		compareText_InViewPage2( "DDI Prefix", prefixValue);		//DDI Prefix
		
		verifyExists(IMSNTranslatorObj.IMSNT.viewPage_CountryField, "Country" );		//Country Name
		
		compareText_InViewPage2( "Nature of Address", NatureOfAddress);		//Nature of Address

	}
	else if(CountryCode.equals("CH")){
		
		compareText_InViewPage2("Carrier",CarrierNo);		//Carrier
		
		verifyExists(IMSNTranslatorObj.IMSNT.viewPage_CountryField, "Country");		//Country Name
		
		compareText_InViewPage2( "Nature of Address", NatureOfAddress);		//Nature of Address
		
		compareText_InViewPage2( "Prefix", prefixValue);		//Prefix
		
	}
	
	javaScriptclick(IMSNTranslatorObj.IMSNT.Closesign);
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Close button");

	waitForAjax();
	

}

public void selectAddedNumberToTranslateInTable(String numberToTranslate) throws Exception {
	
	scrollDown(IMSNTranslatorObj.IMSNT.searchField_IMSNT);
	 	//Enter 'Number To Translate' value in search text box	
	 		sendKeys(IMSNTranslatorObj.IMSNT.searchField_IMSNT,numberToTranslate,"Search");

	 		
	 		waitforPagetobeenable();
	 		
	 		waitForAjax();
			
		//click on Search button
	 		verifyExists(IMSNTranslatorObj.IMSNT.Searchbtn,"Search");
			click(IMSNTranslatorObj.IMSNT.Searchbtn,"Search");
			
			waitForAjax();

//			
			WebElement addIMSNTLink=findWebElement(IMSNTranslatorObj.IMSNT.AddnumberTranslation);
			scrollDown(IMSNTranslatorObj.IMSNT.AddnumberTranslation);
			waitForAjax();
			
//			click_commonMethod(application, "'number To tanslate' column filter", "numberToTranslateColumn_filter", xml);
//			addtextFields_commonMethod(application, "filter text box", "numberToTranslate_textField", numberToTranslate, xml);
			
			 	javaScriptclick((IMSNTranslatorObj.IMSNT.ImsntCheckbox).replace("value", numberToTranslate));
				Reporter.log("Clicked on checkbox");
				ExtentTestManager.getTest().log(LogStatus.PASS, numberToTranslate + " Number selected under Table record");
				waitForAjax();
				
				click(IMSNTranslatorObj.IMSNT.PostcodeAction,"Action");
				waitForAjax();

		}


public String viewPage_country(String countryName) {
	
	String code=null;
	
	if(countryName.equalsIgnoreCase("PT")){
		code="351";
	}
	else if(countryName.equalsIgnoreCase("IT")){
		code="39";
	}
	else if(countryName.equalsIgnoreCase("SE")){
		code="46";
	}
	else if(countryName.equalsIgnoreCase("UK")){
		code="indi";
	}
	else if(countryName.equalsIgnoreCase("BR")){
		code="INFI";
	}
	else if(countryName.equalsIgnoreCase("IE")){
		code="353";	
	}
	else if(countryName.equalsIgnoreCase("CH")){
		code="41";
	}
	else if(countryName.equalsIgnoreCase("AT")){
		code="43";	
	}
	
	return code;
}


public void editIMSNT(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException 
{
	String Countrylist = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "countryCode");
	String TranslateNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslateNumber");
	String TranslatedNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslatedNumber");
	String PrefixNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editPrefixNumber");
	String CarrierNo = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCarrier");
	String prefixCheckboxvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editPrefixCheckbox");
	String expectedTitleName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Number to Translate");

	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Edit operation and Update message" );
	Reporter.log("Verifying Edit operation and Update message");
	waitForAjax();
		
//	selectAddedNumberToTranslateInTable(application, TranslateNumber);
	verifyExists(IMSNTranslatorObj.IMSNT.PostcodeAction, "Action");		//click on Action dropdown
	click(IMSNTranslatorObj.IMSNT.PostcodeAction, "Action");		//click on Action dropdown
	
	verifyExists(IMSNTranslatorObj.IMSNT.PostcodeActionEdit, "Edit_Link");		//click on Edit Link
	click(IMSNTranslatorObj.IMSNT.PostcodeActionEdit, "Edit_Link");		//click on Edit Link
	
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
    Iterator<String> itr = allwindows.iterator();
    while(itr.hasNext())
    {
          String childWindow = itr.next();
          if(!mainWindow.equals(childWindow)){
        	  webDriver.switchTo().window(childWindow);
                
                String actualTitlename=webDriver.switchTo().window(childWindow).getTitle();
                if(expectedTitleName.equals(actualTitlename)) {
                	//ExtentTestManager.getTest().log(LogStatus.PASS, "Page title is displaying as "+ actualTitlename +" as expected");
                	Reporter.log("Page title is displaying as "+ actualTitlename +" as expected");
                }else {
                	//ExtentTestManager.getTest().log(LogStatus.FAIL, "Page title is mismatching. Expected title is: "+expectedTitleName +" ."
                			//+ "But actual title displaying is: "+ actualTitlename);
                	Reporter.log("Page title is mismatching. Expected title is: "+expectedTitleName +" ."
                			+ "But actual title displaying is: "+ actualTitlename);
                }
                
                webDriver.manage().window().maximize();
                waitForAjax();
                
//               Write here  whatever you want to do and perform
                Reporter.log("came inside child window");
                
               // webDriver.close();
          }
    }
    
  //  webDriver.switchTo().window(mainWindow);
	
    
	
	/*
//Number to Translate 
	String actualNumberToTranslate=getTextFrom(IMSNTranslatorObj.IMSNT.editPage_NumberToTranslateElement);
	if(actualNumberToTranslate.equals(TranslateNumber)) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "Number To Translate field value is displaying as "+ actualNumberToTranslate + " as expected");
		Reporter.log("Number To Translate field value is displaying as "+ actualNumberToTranslate + " as expected");
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Number To Tanslate value is mismatching. Actual dipslaying as "+actualNumberToTranslate + ". .The expected value is +" + TranslateNumber);
		Reporter.log("Number To Tanslate value is mismatching. Actual dipslaying as "+actualNumberToTranslate + " .The expected value is " + TranslateNumber);
		
	}
	
//Country Code
	String actualCountryCode=getTextFrom(IMSNTranslatorObj.IMSNT.editPage_countryCodeElement);
	if(actualCountryCode.equals(Countrylist)) {
		ExtentTestManager.getTest().log(LogStatus.PASS, "Country code field value is displaying as "+ actualCountryCode + " as expected");
		Reporter.log("Country code field value is displaying as "+ actualCountryCode + " as expected");
	}else {
		ExtentTestManager.getTest().log(LogStatus.FAIL, "Country code value is mismatching. Actual dipslaying as "+actualCountryCode + ". .The expected value is +" + Countrylist);
		Reporter.log("Country code value is mismatching. Actual dipslaying as "+actualCountryCode + " .The expected value is " + Countrylist);
	}
	*/
	
	
//Prefix 
	verifyExists(IMSNTranslatorObj.IMSNT.PrefixText1,"Prefix");
//	sendKeys(IMSNTranslatorObj.IMSNT.PrefixText1, PrefixNumber,  "Prefix");

//Number Translated
	verifyExists(IMSNTranslatorObj.IMSNT.Numbertranslated_Text1, "Number Translated");
	sendKeys(IMSNTranslatorObj.IMSNT.Numbertranslated_Text1, TranslatedNumber, "Number Translated");
	
	
	if ((Countrylist.equals("CH"))) {

		
		//addDropdownValues_commonMethod("Carrier", IMSNTranslatorObj.IMSNT.carrierDropdown, CarrierNo);
		
		editcheckbox_commonMethod(prefixCheckboxvalue,IMSNTranslatorObj.IMSNT.prefix_checkbox,"Prefix");
		
	}
	else if((Countrylist.equals("SE"))|| (Countrylist.equals("IE"))|| (Countrylist.equals("PT")) || (Countrylist.equals("AT")) ||
			(Countrylist.equals("IT"))) {
		
		//verifyExists(IMSNTranslatorObj.IMSNT.carrierDropdown, "Carrier");
		
		editcheckbox_commonMethod(prefixCheckboxvalue , IMSNTranslatorObj.IMSNT.ddiPrefix_checkbox, "DDI Prefix");

	}
	else if ((Countrylist.equals("BR")))  {
		
			Reporter.log("No additoinal fields");
			
	}
	else if (Countrylist.equals("UK")){
		verifyExists(IMSNTranslatorObj.IMSNT.carrierDropdown,"Carrier");
		//sendKeys(IMSNTranslatorObj.IMSNT.carrierDropdown, CarrierNo, "Carrier");
		
	}

	
	 javaScriptclick(IMSNTranslatorObj.IMSNT.OKbtn);
	 ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on OK button");
	 
	 waitForAjax();
 
	// verifysuccessmessage(application, "NumberTranslation successfully updated.Sync started successfully. Please check the sync status of number translation.");
	 webDriver.switchTo().window(mainWindow);

}

public void synchronise(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws Exception 
{
	String numberToTranslate = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslateNumber");
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Synchronize operation and synchronize message" );
	Reporter.log("Verifying Synchronize operation and synchronize message");

	selectAddedNumberToTranslateInTable(numberToTranslate);
	
	verifyExists(IMSNTranslatorObj.IMSNT.Synchronize, "Synchronize");
	click(IMSNTranslatorObj.IMSNT.Synchronize, "Synchronize");
	
	waitForAjax();
	
//	verifysuccessmessage( "Sync started successfully. Please check the sync status of number translation.");
	
	
}

public void createFileForUploadUpdateFile(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws Exception 
{
	String operation = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Operation");
	String TranslateNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslateNumber");
	String countryName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "countryCode");
	String translatedNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslatedNumber");
	String editTranslatedNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editTranslatedNumber");
	String prefixNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "PrefixNumbertextField");
	String editprefixNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editPrefixNumber");
	String carrier = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CarrierNo");
	String editCarrier = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editCarrier");
	String NatureofAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NatureOfAddress");
	String filepath = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "filePath");
	
	  String tranalatedNumber_final=null;
	  String prefix_final=null;
	  String carrier_final=null;
	  
	  
	 //Translated Number 
	  if(editTranslatedNumber.equalsIgnoreCase("null")) {
		  tranalatedNumber_final = translatedNumber;
	  }
	  else {
		  tranalatedNumber_final = editTranslatedNumber;
	  }
	  
	  
	 //Prefix 
	  if(editprefixNumber.equalsIgnoreCase("null")) {
		  prefix_final = prefixNumber;
	  }
	  else {
		  prefix_final = editprefixNumber;
	  }
	  
	  
	  //Carrier
	  if(editCarrier.equalsIgnoreCase("null")) {
		  carrier_final = carrier;
	  }
	  else {
		  carrier_final = editCarrier;
	  }
	  
	  //fetch CoutryCode
	  String countryCode=viewPage_country(countryName);
	  
	  createNewRecord(filepath, operation, TranslateNumber, tranalatedNumber_final, prefix_final, countryCode, carrier_final,
			  NatureofAddress, "0");

}

public static void createNewRecord(String filepath,String Operation, String translateNumber, String translatedNumber, String prefix, String countryCode,
		  String carrier, String natureOfAdddress, String prefixImsRange) {
		
		String tempFile=filepath;
		File newfile = new File(tempFile);
		String id=""; String name="";
		boolean found=false;
		
		try {
			FileWriter fw= new FileWriter(tempFile, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			
				pw.println("Operation,Number to Translate,Prefix,Translated Number,Translated Country,Carrier,Nature of address,Prefix imsrange");
				pw.println(Operation + "," + translateNumber + "," + prefix + "," + translatedNumber + "," + countryCode + "," + carrier + "," + natureOfAdddress + "," + prefixImsRange);
				
			pw.flush();
			pw.close();
			
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Number Translation' file is created");
			Reporter.log("'Number Translation' file is deleted");
		}catch(Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.PASS, "'Number Translation' file is not created");
			Reporter.log("'Number Translation' file is not created");
		}
		
	}


public void uploadUpdatefile(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws Exception 
{
	String filepath = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "filePath");
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Upload Update file Service Area Link and Fields");
	verifyExists(IMSNTranslatorObj.IMSNT.Uploadupdatefile, "Upload Update File");		//click on upload update file link
	click(IMSNTranslatorObj.IMSNT.Uploadupdatefile, "Upload Update File");		//click on upload update file link
	
	waitForAjax();
	 
	verifyExists(IMSNTranslatorObj.IMSNT.ChosefileTransImsnt,"Choose File");	

	verifyExists(IMSNTranslatorObj.IMSNT.OkbtnPostcode, "OK");	
	
//	verifyExists(APT_DDIRangeObj.DDI.ChosefileDDI,"ChosefileDDI");
	String path1 = new File(".").getCanonicalPath();

	String uploadFilePath = path1+"/numbertranslation.csv";
	
	sendKeys(IMSNTranslatorObj.IMSNT.ChosefileTransImsnt,uploadFilePath,"filepath");
	
	ExtentTestManager.getTest().log(LogStatus.PASS, "File uploaded");


	click(IMSNTranslatorObj.IMSNT.OkbtnPostcode,"OK");
		
	//verifysuccessmessage(application, "NumberTranslation successfully created.");

}


public void deleteFile(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws Exception 
{
	String filepath = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "filePath");
	

	File newfile = new File(filepath);
  
  if(newfile.delete()) {
		Reporter.log("'Number Translation' file is deleted");
		ExtentTestManager.getTest().log(LogStatus.PASS, "'Number Translation' file is deleted");
	}else {
		Reporter.log("'Number Translation' file is not deleted");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "'Number Translation' file is not deleted");
	}
	
}

public void downlaodTranslationlink (String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws Exception 
{
	//String downloadpath = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DownloadsPath");

	waitForAjax();
	verifyExists(IMSNTranslatorObj.IMSNT.DownloadNumberTranslation, "Download Number Translation");
	click(IMSNTranslatorObj.IMSNT.DownloadNumberTranslation, "Download Number Translation");
	
	waitForAjax();
	
	//isFileDownloaded(downloadpath, "NumberTranslation");
	
}

public boolean isFileDownloaded(String downloadPath, String fileName)
{
	  File dir = new File(downloadPath);
	  File[] dirContents = dir.listFiles();

	  for (int i = 0; i < dirContents.length; i++) {
	      if (dirContents[i].getName().contains(fileName)) {
	          // File has been found, it can now be deleted:
	          //dirContents[i].delete();
	    	  
	    	  String downloadedFileName=dirContents[i].getName();
	    	  Reporter.log("Downloaded file name is displaying as: "+ downloadedFileName);
	    	  ExtentTestManager.getTest().log(LogStatus.PASS, "Downloaded file name is displaying as: "+ dirContents[i]);
	    	  
	          return true;
	      }
	          }
	      return false;
	 }

public void viewUIHistory (String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws Exception 
{
	
	String OperationDropDvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Operation");
	String Userfield = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "User");
	String NumberToTranslate = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslateNumber");

		ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying View UI History link and fields");
		
		verifyExists(IMSNTranslatorObj.IMSNT.ViewUIHistory, "View UI History");
		click(IMSNTranslatorObj.IMSNT.ViewUIHistory, "View UI History");
		
		
	//Verify field names	
		compareText("Operation", IMSNTranslatorObj.IMSNT.Operation, "Operation");
		
		compareText("User", IMSNTranslatorObj.IMSNT.ViewUIUser, "User");
		
		compareText("Number to Translate", IMSNTranslatorObj.IMSNT.ViewUINumbertoTrans, "Number to Translate");

		
		
	//select value under Operation Dropdown
		verifyExists(IMSNTranslatorObj.IMSNT.operationDropdown_viewUIhistory, "operationDropdown_viewUIhistory");
		addDropdownValues_commonMethod("Operation", IMSNTranslatorObj.IMSNT.operationDropdown_viewUIhistory, OperationDropDvalue);
		
	//Enter User Name and click on search
		verifyExists(IMSNTranslatorObj.IMSNT.UserText,"User");
		sendKeys(IMSNTranslatorObj.IMSNT.UserText, Userfield, "User");
		
	//Enter value in Number To Translate Field
		verifyExists(IMSNTranslatorObj.IMSNT.Numbertotranslate_Search, "Number to Translate");
		sendKeys(IMSNTranslatorObj.IMSNT.Numbertotranslate_Search, NumberToTranslate, "Number to Translate");
		
	//click on Search button
		verifyExists(IMSNTranslatorObj.IMSNT.ViewUISearch, "Search");
		click(IMSNTranslatorObj.IMSNT.ViewUISearch, "Search");

		
		ExtentTestManager.getTest().log(LogStatus.PASS, "Values populated as per Option selected from Dropdown and Value provided in Number to translate");

		waitForAjax();
		
		verifyExists(IMSNTranslatorObj.IMSNT.ViewUploadHisClose, "Close");
		click(IMSNTranslatorObj.IMSNT.ViewUploadHisClose, "Close");
					
	}

public void viewuploadHistory() throws Exception 
{
	ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying View upload History link and fields");
	
	
	String expectedTitleName="View Upload History";
	
	javaScriptclick(IMSNTranslatorObj.IMSNT.ViewUploadHistory);
	Reporter.log("Clicked on View Upload History");
	//Log.info("Clicked on View Upload History");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on View Upload History");
	
	waitForAjax();
	
	String mainWindow=webDriver.getWindowHandle();
	Set<String> allwindows = webDriver.getWindowHandles();
    Iterator<String> itr = allwindows.iterator();
    while(itr.hasNext())
    {
          String childWindow = itr.next();
          if(!mainWindow.equals(childWindow)){
        	  webDriver.switchTo().window(childWindow);
                
                String actualTitlename=webDriver.switchTo().window(childWindow).getTitle();
                if(expectedTitleName.equals(actualTitlename)) {
                	ExtentTestManager.getTest().log(LogStatus.PASS, "Page title is displaying as "+ actualTitlename +" as expected");
                	Reporter.log("Page title is displaying as "+ actualTitlename +" as expected");
                }else {
                	ExtentTestManager.getTest().log(LogStatus.FAIL, "Page title is mismatching. Expected title is: "+expectedTitleName +" ."
                			+ "But actual title displaying is: "+ actualTitlename);
                	Reporter.log("Page title is mismatching. Expected title is: "+expectedTitleName +" ."
                			+ "But actual title displaying is: "+ actualTitlename);
                }
                
                webDriver.manage().window().maximize();
                waitForAjax();
                
//               Write here  whatever you want to do and perform
                Reporter.log("came inside child window");
                
                webDriver.close();
          }
    }
    
    webDriver.switchTo().window(mainWindow);
	
}



public void deleteIMST(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws Exception 
{
	
	String numberToTranslate = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "TranslateNumber");
	
	//Delete Operation
	
	waitforPagetobeenable();
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying Delete operation and message" );
	Reporter.log("Verifying Delete operation and message");

	Thread.sleep(10000);
	
//select the record and click on Action dropdown	
	selectAddedNumberToTranslateInTable(numberToTranslate);
	
//click on delete button	
	verifyExists(IMSNTranslatorObj.IMSNT.PostcodeActionDelete, "Delete");
	click(IMSNTranslatorObj.IMSNT.PostcodeActionDelete, "Delete");
	
	
	javaScriptclick(IMSNTranslatorObj.IMSNT.PostcodeDeletebtn);
	Reporter.log("Clicked on Delete button ");
   // Log.info("Clicked on Delete button");
	Report.LogInfo("INFO", "Clicked on Delete button", "PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on Delete button");
	
	waitForAjax();
	
	Report.LogInfo("INFO", "Verifying Delete  Message", "PASS");
	ExtentTestManager.getTest().log(LogStatus.PASS, "Verifying Delete  Message");
	
	waitForAjax();
	
	verifysuccessmessage("NumberTranslation successfully marked for deletion.");
	
}


public void verifysuccessmessage(String expected) throws InterruptedException {
		
		waitforPagetobeenable();
		scrollUp();
		waitForAjax();
		try {	
			
			boolean successMsg=findWebElement(IMSNTranslatorObj.IMSNT.alertMsg_managePOstcode).isDisplayed();

			if(successMsg) {
				
				String alrtmsg=findWebElement(IMSNTranslatorObj.IMSNT.AlertForServiceCreationSuccessMessage_managePOstcode).getText();
				
				if(expected.contains(alrtmsg)) {
					
					ExtentTestManager.getTest().log(LogStatus.PASS,"Message is verified. It is displaying as: "+alrtmsg);
					Reporter.log("Message is verified. It is displaying as: "+alrtmsg);
					
					//successScreenshot(application);
					
				}else if(expected.equals(alrtmsg)){
					
					ExtentTestManager.getTest().log(LogStatus.PASS,"Message is verified. It is displaying as: "+alrtmsg);
					Reporter.log("Message is verified. It is displaying as: "+alrtmsg);
					//successScreenshot(application);
					
				}else {
					
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg +" .The Expected value is: "+ expected);
					Reporter.log("Message is displaying and it gets mismatches. It is displaying as: "+ alrtmsg);
					//failureScreenshot(application);
				}
				
			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Success Message is not displaying");
				Reporter.log(" Success Message is not displaying");
				//failureScreenshot(application);
			}
			
		}catch(Exception e) {
			Reporter.log("failure in fetching success message  ");
			ExtentTestManager.getTest().log(LogStatus.FAIL, expected+ " Message is not displaying");
			Reporter.log(expected+ " message is not getting dislpayed");
			//failureScreenshot(application);
		}
	}











}
	
	
	
	


