package testHarness.aptFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;

import testHarness.commonFunctions.ReusableFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.aptObjects.APTIPAccessNoCPE;
import pageObjects.aptObjects.Lanlink_Outbandmanagement;



public class Lanlink_Outbandmanagementhelper extends SeleniumUtils
{
	
	public void CreateCustomer(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
	{
		String name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newCustomer");
		String maindomain = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MainDomain");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CountryToBeSelected");
		String ocn = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OCN");
		String reference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Reference");
		String technicalcontactname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechnicalContactName");
		String type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TypeToBeSelected");
		String email = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Email");
		String phone = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Phone");
		String fax = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Fax");

		
			//ExtentTestManager.getTest().log(LogStatus.INFO, "'Verifying Customer Creation Functionality");

		
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ManageCustomerServiceLink," Manage Customer Service Link");
				click(Lanlink_Outbandmanagement.Outbandmanagement.ManageCustomerServiceLink);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.createcustomerlink," create customer link");
				click(Lanlink_Outbandmanagement.Outbandmanagement.createcustomerlink,"Create Customer Link");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.createcustomer_header,"create customer page header");
				
				//scrolltoend();
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"ok");
				click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"ok");
				
				//Warning msg check
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.customernamewarngmsg,"Legal Customer Name");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.countrywarngmsg,"Country");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ocnwarngmsg,"OCN");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.typewarngmsg,"Type");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.emailwarngmsg,"Email");
			
				//Create customer by providing all info
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.nametextfield,"name");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.nametextfield,name,"name");
		
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.maindomaintextfield,"Main Domain");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.maindomaintextfield,maindomain,"Main Domain");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.country,"country");
			//	sendKeys(APTIPAccessNoCPE.APTNoCPE.country,country,"country");
				addDropdownValues_commonMethod("Country",Lanlink_Outbandmanagement.Outbandmanagement.country,country);
			
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ocntextfield,"ocn");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.ocntextfield,ocn,"ocn");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.referencetextfield,"reference");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.referencetextfield,reference,"reference");
			
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.technicalcontactnametextfield,"technical contact name");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.technicalcontactnametextfield,technicalcontactname,"technical contact name");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.typedropdown,"technical contact name");
				//sendKeys(APTIPAccessNoCPE.APTNoCPE.typedropdown,type,"technical contact name");
				addDropdownValues_commonMethod("Type",Lanlink_Outbandmanagement.Outbandmanagement.type,type);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.emailtextfield,"email");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.emailtextfield,email,"email");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.phonetextfield,"phone text field");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.phonetextfield,phone,"phone text field");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.faxtextfield,"fax text field");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.faxtextfield,fax,"fax text field");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"ok");
				click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"ok");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.customercreationsuccessmsg,"Customer successfully created.");
				
			}
	
	
	public void selectCustomertocreateOrder(String ChooseCustomerToBeSelected)
			throws InterruptedException, IOException {
		

		mouseMoveOn(Lanlink_Outbandmanagement.Outbandmanagement.ManageCustomerServiceLink);

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CreateOrderServiceLink, "Create Order Service Link");
		click(Lanlink_Outbandmanagement.Outbandmanagement.CreateOrderServiceLink, "Create Order Service Link");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");
		// click(APTIPAccessNoCPE.APTNoCPE.nextbutton,"Next button");

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.entercustomernamefield, "name");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.entercustomernamefield, ChooseCustomerToBeSelected, "name");

		// verifyExists(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"name");
		// sendKeys(APTIPAccessNoCPE.APTNoCPE.entercustomernamefield,"*","name");

		waitforPagetobeenable();

		addDropdownValues_commonMethod("Choose a customer",Lanlink_Outbandmanagement.Outbandmanagement.chooseCustomerdropdown, ChooseCustomerToBeSelected);
		// ClickonElementByString("//li[normalize-space(.)='"+ name +"']", 30);

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton, "Next button");
		click(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton, "Next button");

	}
	
	public void createorderservice(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
			throws InterruptedException, IOException {
		String neworder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderSelection");
		String newordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewOrderNumber");
		String newrfireqno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NewRFIREQNumber");
		String existingorderservice = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderSelection");
		String existingordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,
				"ExistingOrderNumber");

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton, "Next button");
		click(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton, "Next button");

		//verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.order_contractnumber_warngmsg, "order contract number");
		//verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.servicetype_warngmsg, "service type");

		if (neworder.equalsIgnoreCase("NO")) {
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newordertextfield, "new order textfield");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newordertextfield, newordernumber, "newordertextfield");

			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newrfireqtextfield, "new rfireq textfield");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newrfireqtextfield, newrfireqno, "new rfireq textfield");

			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.createorderbutton, "create order button");
			click(Lanlink_Outbandmanagement.Outbandmanagement.createorderbutton, "create order button");

			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OrderCreatedSuccessMsg, "Order Created Success Msg");
			
			waitforPagetobeenable();

		} else if (existingorderservice.equalsIgnoreCase("NO")) {
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.selectorderswitch, "select order switch");
			click(Lanlink_Outbandmanagement.Outbandmanagement.selectorderswitch, "select order switch");

			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.existingorderdropdown, "existing order drop down");

			click(Lanlink_Outbandmanagement.Outbandmanagement.existingorderdropdown, "existing order dropdown");
			// sendKeys(APTIPAccessNoCPE.APTNoCPE.existingorderdropdown,existingordernumber,"existingordernumber");

			addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",
					Lanlink_Outbandmanagement.Outbandmanagement.existingorderdropdown, existingordernumber);
			waitforPagetobeenable();

		} else {
			Reporter.log("Order not selected");
		}

	}
	
	



	public void providerEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String chooseAdevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Pe_chooseAdevice");

		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddPElink_LANlinkoutband,"AddPEDeviceLink");
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddPElink_LANlinkoutband,"AddPEDeviceLink");
		
		waitforPagetobeenable();

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.chooseAdevice_warningMessage,"Choose a Device");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.chooseAdevice_dropdown_out,"Choose a Device");
		click(Lanlink_Outbandmanagement.Outbandmanagement.chooseAdevice_dropdown_out,chooseAdevice);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		waitforPagetobeenable();
	
	}
	
	public void verifyPEdeviceEnteredvalue(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String Pe_chooseAdeviceDropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Pe_chooseAdevice");
		
		waitforPagetobeenable();
		
		String actualDeviceName = null;

		String labelname = "Device Name";
		String ExpectedText = Pe_chooseAdeviceDropdown;

		String selecteddevicename = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.fetchDevicename_out);
		
		if (selecteddevicename.contains("..."))
		{
			actualDeviceName = selecteddevicename.substring(0, 10);
		}
		else
		{
			actualDeviceName = selecteddevicename;
		}
		// Compare device name
		
		if (Pe_chooseAdeviceDropdown.equals(actualDeviceName))
		{
	        Reporter.log(" The Expected value for '" + labelname + "' field '"
					+ ExpectedText + "' is same as the Acutal value '" + actualDeviceName + "'");

		}
		else if(Pe_chooseAdeviceDropdown.contains(actualDeviceName))
		{
			 Reporter.log(" The Expected value for '" + labelname + "' field '"
					+ ExpectedText + "' is same as the Acutal value '" + actualDeviceName + "'");
		}
		else
		{
			Reporter.log(" The Expected value for '" + labelname + "' field '"
					+ ExpectedText + "' is not same as the Acutal value '" + actualDeviceName + "'");	
		}
		
		fetchValueFromViewPage("Vendor/Model");
		fetchValueFromViewPage("Management Address");
		fetchValueFromViewPage("Snmpro");
		fetchValueFromViewPage("Country");
		fetchValueFromViewPage("City");
		fetchValueFromViewPage("Site");
		fetchValueFromViewPage("Premise");
		
		
	}
	
	
	public void fetchValueFromViewPage(String labelname) throws InterruptedException, IOException 
	{		
    	WebElement element = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.pedevice_viewpage).replace("value", labelname));
		
    	String emptyele = element.getText().toString();
    	
    	if (emptyele != null && emptyele.isEmpty())
    	{    		
    		Reporter.log( "No value displaying under " + labelname);
    				
    	}
    	else
    	{
    		element.getText();
    		Reporter.log( labelname + " value is displaying as: " + emptyele);	
    	}	
    	
	}

	public void cickOnViewButton_PEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Pe_chooseAdevice");
		
		waitforPagetobeenable();
		
		WebElement element = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectViewButtonUnderPEdevice).replace("value", devicename));
	    javaScriptClickWE(element,"Clicked on 'View' button");
		
    
		
	}
	
	
	public void clickOnAddInterfaceLink_PE() throws InterruptedException, IOException 
	{		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPEdevice_siteLabelName,"verifying 'Add Interface' panel");
		
		waitforPagetobeenable();

		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterfaceOrLink,"Add Interface");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterfaceOrLink,"Add Interface");
		
	}
	
	public void clickOnAddMultiLink_PE() throws InterruptedException, IOException 
	{
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPEdevice_siteLabelName,"verifying 'Add Multilink' panel");
		
		waitforPagetobeenable();

		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addMultilink_PE_out,"Add Multilink");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addMultilink_PE_out,"Add Multilink");


	}
	
	public void clickOnAddLoopback_PE() throws InterruptedException, IOException 
	{
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPEdevice_siteLabelName,"verifying 'Add Multilink' panel");
		
		waitforPagetobeenable();

		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addLoopback_PE_out,"Add Loopback");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addLoopback_PE_out,"Add Loopback");

	}
	
	public void clickOnautoDiscoverVPNPEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Pe_chooseAdevice");
		waitforPagetobeenable();

		WebElement autoDiscoverVPN = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.clickOnAutoDiscoverVPNLink).replace("value", devicename));
	    javaScriptClickWE(autoDiscoverVPN,"clicked on 'Autodiscover VPNs' link");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.serivceAlert))
		{
			String alrtmsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AlertForServiceCreationSuccessMessage);
			
    		Reporter.log("Message displays as: " + alrtmsg);	

		}
		else
		{
    		Reporter.log("Success Message is not displaying");	
	
		}

	}
	
	public String fetchCityName() throws InterruptedException, IOException 
	{
		waitforPagetobeenable();
		
		String cityname = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.xngCityName_out);

		return cityname;
	}
	
	public void clickOnCustomerPremiseEquipmentLink_addDevice() throws InterruptedException, IOException 
	{
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addCPEdevice_Outband_out,"add CPE device Outband out");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addCPEdevice_Outband_out,"add CPE device Outband out");
		
		waitforPagetobeenable();

		

	}
	
	public void verifyCPEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String vendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_vendorModel_out");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_managementAddress_out");
		String snmproEditedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpro_out");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.labelname+vendorModel+Lanlink_Outbandmanagement.Outbandmanagement.labelname1,"Vendor/Model");
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.labelname+managementAddress+Lanlink_Outbandmanagement.Outbandmanagement.labelname1,"Management Address");

		
		if (snmproEditedValue.equalsIgnoreCase("null"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.labelname+"incc"+Lanlink_Outbandmanagement.Outbandmanagement.labelname1,"Snmpro");

		}
		else
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.labelname+snmproEditedValue+Lanlink_Outbandmanagement.Outbandmanagement.labelname1,"Snmpro");

		}

	}
	
	public String fetchdeviceNameFromviewDevicePage() throws InterruptedException, IOException 
	{
		waitforPagetobeenable();
		
		String actualDeviceName = null;
		String selecteddevicename= getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.fetchCPEdeviceName_out,"title");
		
		if (selecteddevicename.contains("..."))
		{
			actualDeviceName = selecteddevicename.substring(0, 10);
		}
		else
		{
			actualDeviceName = selecteddevicename;
		}
		Reporter.log("device name fetched is: "+actualDeviceName);	
		return actualDeviceName;

	}
	
	public void cickOnEditButton_CPEdevice( String devicename) throws InterruptedException, IOException

	{
		
	//	String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPEdeviceName_CustomerPremiseEquipment");
		
		waitforPagetobeenable();
		
		WebElement element = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.clickOnEditLink_CPEdevice_out).replace("value", devicename));
	    javaScriptClickWE(element,"Verifying Edit CPE device");
		
    
		
	}
	
	public void clickOnAddInterfaceLink_CPE() throws InterruptedException, IOException 
	{		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewCPEdevice_siteLabelName,"verifying 'Add Interface' panel");
		
		waitforPagetobeenable();

		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterfaceOrLink,"Add Interface");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterfaceOrLink,"Add Interface");
		
	}
	
	public void clickOnAddMultiLink_CPE() throws InterruptedException, IOException 
	{
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewCPEdevice_siteLabelName,"verifying 'Add Multilink' panel");
		
		waitforPagetobeenable();

		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.InterfacePanel_actionDropdown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addMultilink_PE_out,"Add Multilink");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addMultilink_PE_out,"Add Multilink");


	}
	
	public void clickOnpppConfigurationButton_CPEdevice(String devicename) throws InterruptedException, IOException

	{
		
		
		waitforPagetobeenable();
		
		WebElement element = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.clickOnEditLink_CPEdevice_out).replace("value", devicename));
	    javaScriptClickWE(element,"Verifying Edit CPE device");
		
    
		
	}
	
	
	public void pppConfiguration() throws InterruptedException, IOException
	{
		waitforPagetobeenable();
		
   		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.pppConfigurationPanelheader))
   		{
	   		Reporter.log("PPP Configuration' panel is displaying");
	   		
	   		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.pppConfiguration_CPEdevice_clickHereLink))
	   		{
	   			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.pppConfiguration_CPEdevice_clickHereLink,"Click here to add");
	   			click(Lanlink_Outbandmanagement.Outbandmanagement.pppConfiguration_CPEdevice_clickHereLink,"Click here to add");
	   		}
	   		else
	   		{
		   		Reporter.log("No links displayed for creating new PPP Configuration");

	   		}

   		}
   		else
   		{
	   		Reporter.log("PPP Configuration' panel is not displaying");

   		}
   	
	}
	
	public void pppConfigurationClickOnEditLink() throws InterruptedException, IOException 
	{
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_viewPage_actionDrpodown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_viewPage_actionDrpodown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.pppConfigurationViewPage_editLink,"Edit");
		click(Lanlink_Outbandmanagement.Outbandmanagement.pppConfigurationViewPage_editLink,"Edit");
	}
	
	public void deletePPPconfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siteordernumber");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_viewPage_actionDrpodown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_viewPage_actionDrpodown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.pppConfigurationViewPage_deleteLink,"Delete");
		click(Lanlink_Outbandmanagement.Outbandmanagement.pppConfigurationViewPage_deleteLink,"Delete");
		
   		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup))

   		{
   			String deletPopUpMessage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.deleteMessages_textMessage);
	   		Reporter.log("Delete Pop up message displays as: "+ deletPopUpMessage);

	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.pppConfiguration_viewPage_DeleteButton,"PPP Configuration successfully deleted");
			click(Lanlink_Outbandmanagement.Outbandmanagement.pppConfiguration_viewPage_DeleteButton,"PPP Configuration successfully deleted");
   		}
   		else
   		{
	   		Reporter.log("Delete alert popup is not displayed");

   		}
	}
	
	public void clickOnViewButton_CPEdevice(String devicename) throws InterruptedException, IOException

	{
		waitforPagetobeenable();
		
		WebElement viewLink = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.clickOnViewLink_CPEdevice_out).replace("value", devicename));
	    javaScriptClickWE(viewLink,"Verifying PPP Configuration");
	
	}
	public void addCustomerReadonlySNMPFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String CustomerIPAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CustomerIPAddress");
		String CustomerCommunityString = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CustomerCommunityString");
		
   		Reporter.log("Verifying 'Add Customer Readonly SNMP' Functionality");
   		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Routes_Header,"Routes Header");
		
   		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.CustomerReadonlySNMP_panelHeader))
   		{
   			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_AddLink,"Add Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_AddLink,"Add Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddSNMP_Header))
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddSNMP_Header,"Add Link");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerIPAddress_WarningMessage,"Customer IP Address");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerCommunityString_WarningMessage,"Customer Community String");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerIPAddress_Textfield,"Customer IP Address");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerIPAddress_Textfield,CustomerIPAddress,"Customer IP Address");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerCommunityString_Textfield,"Customer Community String");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerCommunityString_Textfield,CustomerCommunityString,"Customer Community String");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
				
				waitforPagetobeenable();

			}
			else
			{
		   		Reporter.log("Add Customer Readonly SNMP  panel header is not displaying");
			}
   		}

	}
	
	public void editCustomerReadonlySNMPFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String CustomerIPAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CustomerIPAddressEdit");
		String CustomerCommunityStringEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CustomerCommunityStringEdit");
		String customerIPCreatedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CustomerIPAddress");
		
   		Reporter.log("Verifying 'Edit Customer readonly SNMP' Functionality");

		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.CustomerReadonlySNMP_panelHeader))
		{
			WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.customerReadonlySelectValueUnderPanel).replace("value", customerIPCreatedValue));
		    javaScriptClickWE(valueUnderCustomerSNMPPanel,"customer Readonly Select Value Under Panel");
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_EditLink,"Edit Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_EditLink,"Edit Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.EditSNMP_Header))

			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EditSNMP_Header,"Edit SNMP_Header");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerIPAddress_Textfield,"Customer IP Address");
				clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerIPAddress_Textfield);
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerIPAddress_Textfield,CustomerIPAddressEdit,"Customer IP Address");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerCommunityString_Textfield,"Customer Community String");
				clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerCommunityString_Textfield);
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_CustomerCommunityString_Textfield,CustomerCommunityStringEdit,"Customer Community String");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
				
				waitforPagetobeenable();
			}
			else
			{
		   		Reporter.log("Edit Customer Readonly SNMP  panel header is not displaying");
			}
		}
	}
	
	public void deleteCustomerReadonlySNMPFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo,String customerIPAddressValue) throws InterruptedException, IOException

	{
		
		String CustomerIPAddressEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CustomerIPAddressEdit");
		String CustomerCommunityStringEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CustomerCommunityStringEdit");
		
   		Reporter.log("Verifying 'Edit Customer readonly SNMP' Functionality");

		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.CustomerReadonlySNMP_panelHeader))
		{
			WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.customerReadonlySelectValueUnderPanel).replace("value", customerIPAddressValue));
		    javaScriptClickWE(valueUnderCustomerSNMPPanel,"customer Readonly Select Value Under Panel");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteLink,"Delete Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteLink,"Delete Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup))

			{
			    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete");
			}
			else
			{
		   		Reporter.log("Delete alert popup is not displayed");
	
			}
			waitforPagetobeenable();
		}
		else
		{
	   		Reporter.log("Customer Readonly SNMP  panel header is not displaying in View CPE Device page");
		}
			
	}
	
	public void addExtraSubnetFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String ExtraSubnets_City = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExtraSubnets_City");
		String ExtraSubnets_SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExtraSubnets_SubnetSize");
	    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CustomerReadonlySNMP_panelHeader,"Customer Readonly SNMP");


		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_PanelHeader))
		{
			  verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_Action,"Action");
				click(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_Action,"Action");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AllocateExtraSubnets_Link,"Allocate Extra Subnets Link");
				click(Lanlink_Outbandmanagement.Outbandmanagement.AllocateExtraSubnets_Link,"Allocate Extra Subnets Link");
				
				if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddExtraSubnets_Header))
				{
					  verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_SubnetTypeValue,"Subnet Type Value");
					  
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AllocateSubnetButon,"Allocate Subnet Button");
					click(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AllocateSubnetButon,"Allocate Subnet Button");
					
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_City_WarningMessage,"City");
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_SubnetSize_WarningMessage,"Subnet Size");
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_EIPAllocation_AvilablePoolWarningMessage,"Avilable Pool");

					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_CityDropdown,"City");
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_CityDropdown,ExtraSubnets_City,"City");
					
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_SubnetSizeDropdown,"Subnet Size");
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_SubnetSizeDropdown,ExtraSubnets_SubnetSize,"Subnet Size");
					
					waitforPagetobeenable();
					
					if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AvailablePoolsMessage))
					{
						String actualAllocateBLockMessage= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AvailablePoolsMessage);
				   		Reporter.log("Under 'Allocate BLock' field, value displays as: " + actualAllocateBLockMessage);
				   		
				   	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AllocateSubnetButon,"Allocate Subnet");
					click(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AllocateSubnetButon,"Allocate Subnet");
					
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_CloseButton,"Close Extra Subnets window");
					click(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_CloseButton,"Close Extra Subnets window");
	
					}
					else
					{
				   		Reporter.log("No messages displays under 'Allocate Blocks' field");
				   		
				   	  	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AllocateSubnetButon,"Allocate Subnet");
						click(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_AllocateSubnetButon,"Allocate Subnet");
						
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_CloseButton,"Close Extra Subnets window");
						click(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_CloseButton,"Close Extra Subnets window");
				   		
					}

				}
				else
				{
			   		Reporter.log("Add Extra Subnets popup is not displaying");
				}	
		}
		else
		{
	   		Reporter.log("Extra Subnets  panel header is not displaying In view CPE Device page");
		}
	}
	
	public void editNATConfigurationFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String StaticNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String DynamicNATEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ExtraSubnets_PanelHeader,"Extra Subnets");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader))
		{
			click(Lanlink_Outbandmanagement.Outbandmanagement.NAT_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.NAT_EditLink,"Edit Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.EditNAT_Header))
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EditNAT_Header,"Edit NAT Configuration Header");
				
				if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Checkbox))
				{
					if (StaticNATEdit.equalsIgnoreCase("YES"))
					{
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Checkbox,"'Static NAT' checkbox");
						click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Checkbox,"'Static NAT' checkbox");	
					}
					else
					{
				   		Reporter.log("Static NAT checkbox is not Selected");

					}
				}
				else
				{
			   		Reporter.log("Static NAT checkbox is Disabled, So can't make any changes in Checkbox status");

				}
				// Dynamic NAT Checkbox
				if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.DynamicNAT_Checkbox))
				{
					if (DynamicNATEdit.equalsIgnoreCase("YES"))
					{
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DynamicNAT_Checkbox,"'Dynamic NAT' checkbox");
						click(Lanlink_Outbandmanagement.Outbandmanagement.DynamicNAT_Checkbox,"'Dynamic NAT' checkbox");	
					}
					else
					{
				   		Reporter.log("Dynamic NAT' checkbox is not Selected");

					}
				}
				else
				{
			   		Reporter.log("Dynamic NAT checkbox is Disabled, So can't make any changes in Checkbox status");

				}
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NAT_OKButton,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.NAT_OKButton,"OK Button");
				
				waitforPagetobeenable();
			}

		}
		else
		{
	   		Reporter.log("NAT Configuration  panel header is not displaying");
		}	
	}
	
	public void addStaticNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String Static_Protocol = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_Protocol");
		String Static_LocalPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalPort");
		String Static_GlobalPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalPort");
		String Static_LocalIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");
		String Static_GlobalIP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalIP");
		
   		Reporter.log("Add 'Static NAT Mapping");
   		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NAT Configuration");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_AddLink,"Add Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_AddLink,"Add Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddStaticNAT_Header))
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,"Protocol");
				select(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,Static_Protocol);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalPort_WarningMessage,"Local Port");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalPort_WarningMessage,"Global Port");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalIP_WarningMessage,"Local IP");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalIP_WarningMessage,"Global IP");

				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalPort_Textfield,Static_LocalPort,"LOCAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalPort_Textfield,Static_GlobalPort,"GLOBAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalIP_Textfield,Static_LocalIP,"LOCAL IP textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalIP_Textfield,Static_GlobalIP,"GLOBAL IP textfield");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				waitforPagetobeenable();
		
			}
			else
			{
		   		Reporter.log("Add Static NAT Mapping Header/popup not displayed");
	
			}
		}
		
		else
		{
	   		Reporter.log("Static NAT Mapping Panel not displayed");
		}
	}
	
	
	public void editStaticNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String Static_ProtocolEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_ProtocolEdit");
		String Static_LocalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalPortEdit");
		String Static_GlobalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalPortEdit");
		String Static_LocalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIPEdit");
		String Static_GlobalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalIPEdit");
		String localIPCreatedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");

   		Reporter.log("Edit 'Static NAT Mapping'");
   		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NAT Configuration");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader))
		{
			WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.editStatic_selectValueUnderPanel).replace("value", localIPCreatedValue));
		    javaScriptClickWE(valueUnderCustomerSNMPPanel,"Verifying PPP Configuration");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Editink,"Edit Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Editink,"Edit Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.EditStaticNAT_Header))
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,"Protocol");
				select(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,Static_ProtocolEdit);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalPort_WarningMessage,"Local Port");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalPort_WarningMessage,"Global Port");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalIP_WarningMessage,"Local IP");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalIP_WarningMessage,"Global IP");

				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalPort_Textfield,Static_LocalPortEdit,"LOCAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalPort_Textfield,Static_GlobalPortEdit,"GLOBAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalIP_Textfield,Static_LocalIPEdit,"LOCAL IP textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalIP_Textfield,Static_GlobalIPEdit,"GLOBAL IP textfield");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				waitforPagetobeenable();
			}
			else
			{
		   		Reporter.log("Edit Static NAT Mapping Header/popup not displayed");
	
			}
		}
		
		else
		{
	   		Reporter.log("Static NAT Mapping Panel not displayed");

		}
		
	}
	
	public void deleteStaticNATMappingFunction_CPE(String Static_LocalIP) throws InterruptedException, IOException

	{
		
   		Reporter.log("delete Statuc NAT Mapping");
   		
	    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"Action");


		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader))
		{
			WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.editStatic_selectValueUnderPanel).replace("value", Static_LocalIP));
		    javaScriptClickWE(valueUnderCustomerSNMPPanel,"customer Readonly Select Value Under Panel");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_DeleteLink,"Delete Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_DeleteLink,"Delete Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup))

			{
			    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete");
			}
			else
			{
		   		Reporter.log("Delete alert popup is not displayed");
	
			}
			waitforPagetobeenable();
		}
		else
		{
	   		Reporter.log("Static NAT Mapping panel header not displayed");
		}
			
	}
	
	public void addDynamicNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String Static_ProtocolEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_ProtocolEdit");
		String Static_LocalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalPortEdit");
		String Static_GlobalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalPortEdit");
		String Static_LocalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIPEdit");
		String Static_GlobalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalIPEdit");
		String localIPCreatedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");
   		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NAT Configuration");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader))
		{
			//WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.editStatic_selectValueUnderPanel).replace("value", localIPCreatedValue));
		   // javaScriptClickWE(valueUnderCustomerSNMPPanel,"Verifying PPP Configuration");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_AddLink,"Add Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_AddLink,"Add Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddStaticNAT_Header))
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,"Protocol");
				select(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,Static_ProtocolEdit);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalPort_WarningMessage,"Local Port");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalPort_WarningMessage,"Global Port");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalIP_WarningMessage,"Local IP");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalIP_WarningMessage,"Global IP");

				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalPort_Textfield,Static_LocalPortEdit,"LOCAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalPort_Textfield,Static_GlobalPortEdit,"GLOBAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalIP_Textfield,Static_LocalIPEdit,"LOCAL IP textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalIP_Textfield,Static_GlobalIPEdit,"GLOBAL IP textfield");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				waitforPagetobeenable();
			}
			else
			{
		   		Reporter.log("Add Static NAT Mapping Header/popup not displayed");
	
			}
		}
		
		else
		{
	   		Reporter.log("Static NAT Mapping Panel not displayed");

		}
		
	}
	
	public void editDynamicNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String Static_ProtocolEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_ProtocolEdit");
		String Static_LocalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalPortEdit");
		String Static_GlobalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalPortEdit");
		String Static_LocalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIPEdit");
		String Static_GlobalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalIPEdit");
		String localIPCreatedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");
   		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NAT Configuration");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader))
		{
			//WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.editStatic_selectValueUnderPanel).replace("value", localIPCreatedValue));
		  //  javaScriptClickWE(valueUnderCustomerSNMPPanel,"Verifying PPP Configuration");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Editink,"Edit Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Editink,"Edit Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.EditStaticNAT_Header))
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,"Protocol");
				select(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Protocol_Dropdown,Static_ProtocolEdit);
			
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalPort_Textfield,Static_LocalPortEdit,"LOCAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalPort_Textfield,Static_GlobalPortEdit,"GLOBAL PORT textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_LocalIP_Textfield,Static_LocalIPEdit,"LOCAL IP textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_GlobalIP_Textfield,Static_GlobalIPEdit,"GLOBAL IP textfield");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				waitforPagetobeenable();
			}
			else
			{
		   		Reporter.log("Edit Static NAT Mapping Header/popup not displayed");
	
			}
		}
		
		else
		{
	   		Reporter.log("Static NAT Mapping Panel not displayed");

		}
		
	}
	
	public void deleteDynamicNATMappingFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String Static_ProtocolEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_ProtocolEdit");
		String Static_LocalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalPortEdit");
		String Static_GlobalPortEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalPortEdit");
		String Static_LocalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIPEdit");
		String Static_GlobalIPEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_GlobalIPEdit");
		String localIPCreatedValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Static_LocalIP");
   		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NAT Configuration");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader))
		{
			//WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.editStatic_selectValueUnderPanel).replace("value", localIPCreatedValue));
		   // javaScriptClickWE(valueUnderCustomerSNMPPanel,"Verifying PPP Configuration");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_DeleteLink,"Delete Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.StaticNAT_DeleteLink,"Delete Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup))
			{
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete Button");
			}
			else
			{
		   		Reporter.log("Delete alert popup is not displayed");
	
			}
		}
		
		else
		{
	   		Reporter.log("Static NAT Mapping panel header not displayed");

		}
		
	}
	
	public void addDHCPServersonCPEFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String DHCP_CustomerLANSubnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_CustomerLANSubnet");
		String DHCP_SubnetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_SubnetMask");
		String DHCP_PrimaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_PrimaryDNSServer");
		String DHCP_SecondaryDNSServer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_SecondaryDNSServer");
		String editStaticNAT = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String editDynamicNAT = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");
   		
   		Reporter.log("Add DHCP Servers on CPE");

		if(editDynamicNAT.equalsIgnoreCase("Yes"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DynamicNATMapping_panelHeader,"Dynamic NAT Mapping");

		}
		else if((editDynamicNAT.equalsIgnoreCase("No")) && (editStaticNAT.equalsIgnoreCase("Yes")))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader,"StaticNATMapping");
	
		}
		else if((editDynamicNAT.equalsIgnoreCase("No")) && (editStaticNAT.equalsIgnoreCase("No")))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NATConfiguration");
	
		}
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_panelHeader))
		{
			//WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.editStatic_selectValueUnderPanel).replace("value", localIPCreatedValue));
		   // javaScriptClickWE(valueUnderCustomerSNMPPanel,"Verifying PPP Configuration");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_AddLink,"Add Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_AddLink,"Add Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddDHCP_Header))
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_CustomerLANSubnet_WarningMessage,"Customer LAN Subnet");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_SubnetMask_WarningMessage,"Subnet Mask");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_PrimaryDNSServer_WarningMessage,"Primary DNS Server");

				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_CustomerLANSubnet_Textfield,DHCP_CustomerLANSubnet,"Customer LAN Subnet textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_SubnetMask_Textfield,DHCP_SubnetMask,"Subnet Mask textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_PrimaryDNSServer_Textfield,DHCP_PrimaryDNSServer,"Primary DNS Server textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_SecondaryDNSServer_Textfield,DHCP_SecondaryDNSServer,"Secondary DNS Server textfield");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				waitforPagetobeenable();
			}
			else
			{
		   		Reporter.log("Add DHCP Servers on CPE Header/popup not displayed");
	
			}
		}
		
		else
		{
	   		Reporter.log("DHCP Servers on CPE Panel not displayed");

		}
		
	}
	
	public void editDHCPServersonCPEFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String DHCP_CustomerLANSubnetEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_CustomerLANSubnetEdit");
		String DHCP_SubnetMaskEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_SubnetMaskEdit");
		String DHCP_PrimaryDNSServerEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_PrimaryDNSServerEdit");
		String DHCP_SecondaryDNSServerEdit = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_SecondaryDNSServerEdit");
		String customerLANsubnet = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DHCP_CustomerLANSubnet");
		String editStaticNAT = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String editDynamicNAT = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");

   		Reporter.log("Edit DHCP Servers on CPE");

		if(editDynamicNAT.equalsIgnoreCase("Yes"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DynamicNATMapping_panelHeader,"Dynamic NAT Mapping");

		}
		else if((editDynamicNAT.equalsIgnoreCase("No")) && (editStaticNAT.equalsIgnoreCase("Yes")))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader,"StaticNATMapping");
	
		}
		else if((editDynamicNAT.equalsIgnoreCase("No")) && (editStaticNAT.equalsIgnoreCase("No")))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NATConfiguration");
	
		}
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_panelHeader))
		{
			WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderDHCPpanel).replace("value", customerLANsubnet));
		    javaScriptClickWE(valueUnderCustomerSNMPPanel,"Verifying PPP Configuration");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Editink,"Edit Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Editink,"Edit Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.EditDHCP_Header))
			{

				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_CustomerLANSubnet_Textfield,DHCP_CustomerLANSubnetEdit,"Customer LAN Subnet textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_SubnetMask_Textfield,DHCP_SubnetMaskEdit,"Subnet Mask textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_PrimaryDNSServer_Textfield,DHCP_PrimaryDNSServerEdit,"Primary DNS Server textfield");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_SecondaryDNSServer_Textfield,DHCP_SecondaryDNSServerEdit,"Secondary DNS Server textfield");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.OKButton_common1,"OK Button");
				
				waitforPagetobeenable();
			}
			else
			{
		   		Reporter.log("Edit DHCP Servers on CPE Header/popup not displayed");
	
			}
		}
		
		else
		{
	   		Reporter.log("DHCP Servers on CPE Panel not displayed");

		}
		
	}
	
	public void deleteDHCPServersonCPEFunction_CPE(String testDataFile, String sheetName, String scriptNo, String dataSetNo,String customerLANsubnet) throws InterruptedException, IOException

	{
		
		String editStaticNAT = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "StaticNATEdit");
		String editDynamicNAT = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DynamicNATEdit");
		

   		Reporter.log("Delete DHCP Servers on CPE");

		if(editDynamicNAT.equalsIgnoreCase("Yes"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DynamicNATMapping_panelHeader,"Dynamic NAT Mapping");

		}
		else if((editDynamicNAT.equalsIgnoreCase("No")) && (editStaticNAT.equalsIgnoreCase("Yes")))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.StaticNATMapping_panelHeader,"StaticNATMapping");
	
		}
		else if((editDynamicNAT.equalsIgnoreCase("No")) && (editStaticNAT.equalsIgnoreCase("No")))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.NATConfiguration_panelHeader,"NATConfiguration");
	
		}
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_panelHeader))
		{
			WebElement valueUnderCustomerSNMPPanel = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderDHCPpanel).replace("value", customerLANsubnet));
		    javaScriptClickWE(valueUnderCustomerSNMPPanel,"Verifying PPP Configuration");	
		    
		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Actionbutton,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_Actionbutton,"Action");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_DeleteLink,"Delete Link");
			click(Lanlink_Outbandmanagement.Outbandmanagement.DHCP_DeleteLink,"Delete Link");
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup))
			{

				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_DeleteButton,"Delete");
			}
			else
			{
		   		Reporter.log("Delete alert popup is not displayed");
	
			}
			waitforPagetobeenable();
		}
		
		else
		{
	   		Reporter.log("DHCP Servers on CPE panel header not displayed");

		}
		
	}
	
	public void generateConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		
		String generateConfigurationvalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "generateConfigurationValue");
		
		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.configurationPanelHeader))
		{
	   		Reporter.log("'Configuration' panel is displaying");
	   		
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.generateConfigurationDropdown,"Protocol");
			select(Lanlink_Outbandmanagement.Outbandmanagement.generateConfigurationDropdown,generateConfigurationvalue);
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.generateButton,"Generate");
			click(Lanlink_Outbandmanagement.Outbandmanagement.generateButton,"Generate");
	
			String configurationValue= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.generateConfiguration_generatedValue);
			if(isEmptyOrNull(configurationValue))
			{
		   		Reporter.log("No configuration value displays");
	
			}
			else
			{
		   		Reporter.log("After clicking on 'Generate' button, value displays as: " + configurationValue);

			}
		}
		else
		{
	   		Reporter.log("'Configuration' panel is not displaying");

		}
	}
	
	public void createPEtoCPElink(String testDataFile, String sheetName, String scriptNo, String dataSetNo,String peSourceInterface,
			String CPEtargetDevice, String CPEtargetInterface) throws InterruptedException, IOException

	{
		
		String linkOrCircuitID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "peToCPElink_circuitID");
		String peSourceDevice = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Pe_chooseAdevice");

		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.peToCpeLink_ActionDropdown,"Action");
		click(Lanlink_Outbandmanagement.Outbandmanagement.peToCpeLink_ActionDropdown,"Action");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.peToCpeLink_addNewLink,"Ad New Link");
		click(Lanlink_Outbandmanagement.Outbandmanagement.peToCpeLink_addNewLink,"Ad New Link");
		
		waitforPagetobeenable();

		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_PanelHeader))
		{
	   		Reporter.log("'Add New Link' Popup displays");
	   		
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_nextButton,"Next");
			click(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_nextButton,"Next");
			
			waitforPagetobeenable();

	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_linkOrCircuitID_warningMessage,"Link/Circuit Id");
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_PEsourceDevice_warningMEsage,"PE Source Device");
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_PEsourceInterface_warningMEsage,"PE Source Interface");
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_CPEtargetDevice_warningMessage,"CPE Target Device");
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_CPEtargetInterface_warningMessage,"CPE Target Interface");
	   		
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewlink_linkOrcicuittextField,"Link/Circuit Id");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addNewlink_linkOrcicuittextField,linkOrCircuitID,"Link/Circuit Id");
	   		
			select(Lanlink_Outbandmanagement.Outbandmanagement.addNewlink_PEsourceDeviceDropdown,peSourceDevice);
			select(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_PEsourceInterfaceDropdown,peSourceInterface);
			select(Lanlink_Outbandmanagement.Outbandmanagement.addNewlink_CPEtargetDevice,CPEtargetDevice);
			select(Lanlink_Outbandmanagement.Outbandmanagement.addNewlink_CPEtargetInterface,CPEtargetInterface);
			
			waitforPagetobeenable();
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_nextButton,"Next");
			click(Lanlink_Outbandmanagement.Outbandmanagement.addNewLink_nextButton,"Next");
		}
		else
		{
	   		Reporter.log("'Add New Link' popup not dispslayed");
		}	
	}
	
	public void deletePEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Pe_chooseAdevice");
		
		waitforPagetobeenable();
		
		WebElement autoDiscoverVPN = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.clickOnDeleteLink_PEdevice).replace("value", devicename));
	    javaScriptClickWE(autoDiscoverVPN,"click On Delete Link PEdevice");

   		Reporter.log("clicked on 'Delete From Service' link under 'Provider Equipment");

		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup))
		{
			String deletPopUpMessage=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.deleteMessages_textMessage);
	   		Reporter.log("Delete Pop up message displays as: "+ deletPopUpMessage);

		    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.deletebutton,"Delete");
			click(Lanlink_Outbandmanagement.Outbandmanagement.deletebutton,"Delete");
		}
			else
			{
		   		Reporter.log("Delete alert popup is not displayed");
	
			}	
	}
	
	public void deleteCPEdevice(String devicename) throws InterruptedException, IOException

	{
		waitforPagetobeenable();
		
		WebElement deleteLink = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.clickOnDEleteFromServiceLink_CPEdevice).replace("value", devicename));
	    javaScriptClickWE(deleteLink,"Verifying 'Delete From Service' Functionality for CPE deviec");
	
	}
	
	public void VerifyDataEnteredForIPVPNSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
		String VPNtopology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
		String circuitType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CircuitType");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "country");
		String city = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "city");
		String CSR_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSR_Name");
		String site = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitevalue");
		String performReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "performReport");
		String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Proactivemonitor");
		String smartmonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "smartmonitor");
		String technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String siteallias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteallias");
		String VLANid = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
		String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DCAenabledsite");
		String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cloudserviceprovider");
		String existingSitevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existing_SiteOrdervalue");
		String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteorder_Remark");
		String xngcityname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng city name");
		String xngcitycode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng ciy code");
		String devicename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicenameForaddsiteorder");
		String nonterminatepoinr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "nonterminationpoint");
		String Protected = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "protectforaddsiteorder");
		String newcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcity");
		String existingcityselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingcity");
		String existingsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingsite");
		String newsiteselection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newsite");
		String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siteordernumber");
		String IVReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_Ivrefrence");
		String perCoSPerformancereport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_perCoSPerformancereport");
		String primarySiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "primarySiteOrder");
		String routerConfigurationViewIpv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_routerConfigurationViewIpv4");
		String wholesaleProvider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_wholesaleProvider");
		String managedSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_managedSite");
		String priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteORder_priority");
		String actelisBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_actelisBased");
		String voip = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_voip");
		String voipClassOfService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrder_voipClassOfService");
		
		        // Site Order Number
				verifyEnteredvalues("Site Order Number", siteOrderNumber);
				
				// Country
				verifyEnteredvalues("Device Country", country);
				
				// City

				if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

					// City
					verifyEnteredvalues("Device Xng City", city);

					// Site
					if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
						verifyEnteredvalues("CSR Name", existingSitevalue);
					}

					else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
						verifyEnteredvalues("CSR Name", CSR_Name);
					}

				} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

					// New City
					verifyEnteredvalues("Device Xng City", xngcitycode);

					// New Site
					verifyEnteredvalues("CSR Name", CSR_Name);

				}

				// Performance Reporting
				verifyEnteredvalues("Performance Reporting", performReport);

				// Proactive Monitoring
				verifyEnteredvalues("Proactive Monitoring", ProactiveMonitor);

				// Smarts Monitoring
				verifyEnteredvalues("Smarts Monitoring", smartmonitor);

				// Per CoS Performance reporting
				verifyEnteredvalues("Per CoS Performance Reporting", perCoSPerformancereport);

				// Site Alias
				verifyEnteredvalues("Site Alias", siteallias);

				// Primary Site Order
				verifyEnteredvalues("Primary Site Order", primarySiteOrder);

				// IV Reference
				verifyEnteredvalues("IV Reference", IVReference);

				// Priority
				verifyEnteredvalues("Priority", priority);

				// Managed Site
				verifyEnteredvalues("Managed Site", managedSite);

				// Router Configuration View IPv4
				verifyEnteredvalues("Router Configuration View IPv4", routerConfigurationViewIpv4);

				// Actelis Based
				verifyEnteredvalues("Actelis Based", actelisBased);

				// Wholesale Provider
				verifyEnteredvalues("Wholesale Provider", wholesaleProvider);

				// VoIP
				verifyEnteredvalues("VoIP", voip);

				// VoIP Class of Service
				verifyEnteredvalues("VoIP Class of Service", voipClassOfService);
				
				// Remark
			    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.remark_viewPage,"Remark");
	}
	
	
	public void verifyEnteredvalues(String labelname, String ExpectedText) throws InterruptedException {

		String text = null;
		WebElement element = null;

		try {
			Thread.sleep(1000);
			element = webDriver.findElement(By.xpath("//div[div[label[contains(text(),'"+ labelname + "')]]]/div[2]"));
			String emptyele = element.getText().toString();

			if(element==null)
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, labelname+" not found");
				Reporter.log(labelname+" not found");
			}
			else if (emptyele!=null && emptyele.isEmpty()) {
//				ExtentTestManager.getTest().log(LogStatus.PASS,  labelname + "' value is empty");
				
				emptyele= "Null";
				
			//	sa.assertEquals(emptyele, ExpectedText, labelname + " value is not displaying as expected");
				
				if(emptyele.equalsIgnoreCase(ExpectedText)) {
					
					ExtentTestManager.getTest().log(LogStatus.PASS, "The Expected value for '"+ labelname +"' field  is same as the Acutal value. It is id displaying blank");
					Reporter.log("The Expected value for '\"+ labelname +\"' field  is same as the Acutal value. It is displaying blank");
					
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					Reporter.log(" The Expected value '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
				}
				

			}else 
			{   
				text = element.getText();
				if( text.contains("-")) {
					
					String[] actualTextValue=text.split(" ");
					String[] expectedValue =ExpectedText.split(" ");
					
					if(expectedValue[0].equalsIgnoreCase(actualTextValue[0])) {
						ExtentTestManager.getTest().log(LogStatus.PASS," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log(" The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					}
					else if(expectedValue[0].contains(actualTextValue[0])) {
						ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					
					}
					else
					{
						ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					}
					
				}else {
					if(ExpectedText.equalsIgnoreCase(text)) {
						ExtentTestManager.getTest().log(LogStatus.PASS," The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log(" The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					}
					else if(ExpectedText.contains(text)) {
						ExtentTestManager.getTest().log(LogStatus.PASS,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					
					}
					else
					{
						ExtentTestManager.getTest().log(LogStatus.FAIL,"The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
						Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					}
				}
				
				
			}
		}catch (Exception e) {
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.FAIL, labelname + " field is not displaying");
			Reporter.log(labelname + " field is not displaying");
		}

	}
	
	public void selectRowForIPVPNsiteorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String siteordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siteordernumber");
		
   		Reporter.log("-----------------------------" + siteordernumber + "---------------------");

		 List<WebElement> results =findWebElements(Lanlink_Outbandmanagement.Outbandmanagement.results+siteordernumber+Lanlink_Outbandmanagement.Outbandmanagement.results1);
		 
		 int numofrows=results.size();
		 
	   	Reporter.log("no of results: " + numofrows);
	   	
	   	boolean resultflag;

		resultflag = results.get(0).isDisplayed();

	   	Reporter.log("status of result: " + resultflag);
	   	if (resultflag) {
	   		Reporter.log(results.get(0).getText());
			results.get(0).click();
			Thread.sleep(8000);
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Actiondropdown_siteorder,"Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.Actiondropdown_siteorder,"Action");
			waitforPagetobeenable();
	   	}
	}
	
	
	public void editIPVPNsiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String performReport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_performReport");
		String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_ProactiveMonitor");
		String smartmonitor = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_smartmonitor");
		String technology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
		String siteallias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EditSiteOrder_siteallias");
		String remark = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteorder_remark");
		String IVReference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_ivReference");
		String perCoSPerformancereport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_perCoSPerformancereport");
		String primarySiteOrder = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editprimarySiteOrder");
		String routerConfigurationViewIpv4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_routerConfigurationViewIpv4");
		String wholesaleProvider = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_wholesaleProvider");
		String managedSite = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_managedSite");
		String priority = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteORder_priority");
		String actelisBased = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_actelisBased");
		String voip = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editSiteOrder_voip");
		String voipClassOfService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "editsiteOrder_voipClassOfService");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EditsideOrderlink,"Edit side Orderlink");
		click(Lanlink_Outbandmanagement.Outbandmanagement.EditsideOrderlink,"Edit side Orderlink");
		
		waitforPagetobeenable();
		
	         	// Performance Reporting
				editSiteOrder_Performancereporting(performReport);

				// Pro active Monitoring
				editSiteOrder_proactiveMonitoring(ProactiveMonitor);

				// Smart Monitoring
				editSiteOrder_smartMonitoring(smartmonitor);
				
				
				// primary Site Order
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.primarySiteOrder_out,"Primary Site Order");
				select(Lanlink_Outbandmanagement.Outbandmanagement.primarySiteOrder_out,primarySiteOrder);
				
				// Site alias
				editsiteorder_sitealias(siteallias);

				// Per CoS Performance Reporting
				editSiteOrder_PerCoSPerformancereporting(performReport);
				
				// IV Reference
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_IVreferencedropdown,"IV Reference");
				select(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_IVreferencedropdown,IVReference);
				
				// Router Configuration View IPv4
				editSiteOrder_RouterConfigurationViewIPv4(routerConfigurationViewIpv4);
				
				// Wholesale Provider
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.wholesaleProvider_out,"Wholesale Provider");
				select(Lanlink_Outbandmanagement.Outbandmanagement.wholesaleProvider_out,wholesaleProvider);
				
				waitforPagetobeenable();
				
				// Managed Site
				editSiteOrder_managedSite(managedSite);
				
				// Priority
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.priority_out,"IV Reference");
				select(Lanlink_Outbandmanagement.Outbandmanagement.priority_out,priority);
				
				// Actelis Based
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.actelisBased_out,"Actelis Based");
				click(Lanlink_Outbandmanagement.Outbandmanagement.actelisBased_out,actelisBased);
				
				// VOIP
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.voip_out,"VoIP");
				click(Lanlink_Outbandmanagement.Outbandmanagement.voip_out,voip);
				
				boolean voipSelection = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.voip_out).isSelected();
				if (voipSelection) {
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.voipClassOfService_out,"VoIP Class of Service");
					select(Lanlink_Outbandmanagement.Outbandmanagement.voipClassOfService_out,voipClassOfService);
				}

				// Remark
				editsiteOrder_remark(remark);

				waitforPagetobeenable();

				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
				click(Lanlink_Outbandmanagement.Outbandmanagement.SNMP_OKButton,"OK Button");
		
	}
	
	public void editsiteOrder_remark(String remark)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark))
		{
			if (remark.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Remark text field value is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark,"Addsiteorder");
				clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark);
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark,remark,"Addsiteorder");

				
				String remarkValue= getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark,"value");
				
		   		Reporter.log("Edited value for 'Remark' field is: " + remarkValue);
			}
	    }
		else
		{
	   		Reporter.log("Remark text field is not available under 'Edit Site Order' page");
		}
	}
	
	public void editSiteOrder_managedSite(String managedSite)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.managedSiteDropdown_out))
		{
			if (managedSite.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Managed Site dropdown value is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out,"Managed Site");
				click(Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out,"Managed Site");
				
				waitforPagetobeenable();
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.managedSite+managedSite+Lanlink_Outbandmanagement.Outbandmanagement.managedSite1,"Manage site");
				click(Lanlink_Outbandmanagement.Outbandmanagement.managedSite+managedSite+Lanlink_Outbandmanagement.Outbandmanagement.managedSite1,"Manage site");
								
		   		Reporter.log("Edited value for 'Managed Site' dropdown is: " + managedSite);
			}
	    }
		else
		{
	   		Reporter.log("Managed Site dropdown is not available under 'Edit Site Order' page");
		}
	}
	
	public void editSiteOrder_RouterConfigurationViewIPv4(String routerConfigViewIPv4)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.routerConfigView_Field_out))
		{
			if (routerConfigViewIPv4.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Router Configuration View IPv4 dropdown is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.routerConfigurationviewIPv4_out,"Router Configuration view IPv4");
				click(Lanlink_Outbandmanagement.Outbandmanagement.routerConfigurationviewIPv4_out,"Router Configuration view IPv4");
				
				waitforPagetobeenable();
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.managedSite+routerConfigViewIPv4+Lanlink_Outbandmanagement.Outbandmanagement.routerConfigViewIPv41,"routerConfigViewIPv41");
				click(Lanlink_Outbandmanagement.Outbandmanagement.routerConfigViewIPv4+routerConfigViewIPv4+Lanlink_Outbandmanagement.Outbandmanagement.routerConfigViewIPv41,"routerConfigViewIPv41");
				
				waitforPagetobeenable();

		   		Reporter.log("Edited value for 'Router Configuration View IPv4' dropdown is: " + routerConfigViewIPv4);
			}
	    }
		else
		{
	   		Reporter.log(" Router Configuration View IPv4' dropdown is not available under 'Edit Site Order' page");
		}
	}
	
	public void editSiteOrder_PerCoSPerformancereporting(String perCoSperformReport)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformanceReporting_out))
		{
			if (perCoSperformReport.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Per CoS Performance reporting dropdown is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out,"PerCo Sperfor mancereporting");
				click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out,"PerCo Sperfor mancereporting");
				
				waitforPagetobeenable();
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.managedSite+perCoSperformReport+Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformReport1,"perCoSperformReport1");
				click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformReport+perCoSperformReport+Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformReport1,"perCoSperformReport1");
				
				waitforPagetobeenable();

		   		Reporter.log("Edited value for 'Per CoS Performance reporting' dropdown is: " + perCoSperformReport);
			}
	    }
		else
		{
	   		Reporter.log("Per CoS Performance reporting' dropdown is not available under 'Edit Site Order' page");
		}
	}
	
	public void editsiteorder_sitealias(String siteallias)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias))
		{
			if (siteallias.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Site Alias field value is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias,"Addsiteorder");
				clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias);
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias,siteallias,"Addsiteorder");
				
				waitforPagetobeenable();
				
				String siteAliasvalue= getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias,"value");


		   		Reporter.log("Edited value for 'Site Alias' field is: " + siteAliasvalue);
			}
	    }
		else
		{
	   		Reporter.log(" Site Alias field is not available under 'Edit Site Order' page");
		}
	}
	
	public void editSiteOrder_smartMonitoring(String smartmonitor)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoring))
		{
			if (smartmonitor.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Smarts Monitoring dropdown value is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoring_xbutton,"Addsiteorder smartmonitoring");
				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoring_xbutton,"Addsiteorder smartmonitoring");
				
				waitforPagetobeenable();
				
				webDriver.findElement(By.xpath("//div[label[text()='Smarts Monitoring']]//div[text()='" + smartmonitor + "']")).click();
				waitforPagetobeenable();

		   		Reporter.log("Edited value for 'Smarts Monitoring' dropdown is: " + smartmonitor);
			}
	    }
		else
		{
	   		Reporter.log("Smart Monitoring dropdown is not available under 'Edit Site Order' page");
		}
	}
	public void editSiteOrder_proactiveMonitoring(String ProactiveMonitor)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_proactivemonitoring))
		{
			if (ProactiveMonitor.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Proactive monitoring' dropdown value is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsitorder_proactivemonitoring_xbutton,"Addsitorder Proactivemonitoring");
				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsitorder_proactivemonitoring_xbutton,"Addsitorder Proactivemonitoring");
				
				waitforPagetobeenable();
				
				webDriver.findElement(By.xpath("//div[label[text()='Proactive Monitoring']]//div[text()='" + ProactiveMonitor + "']")).click();
				waitforPagetobeenable();

		   		Reporter.log("Edited value for 'Pro active Monitoring' dropdown is: " + ProactiveMonitor);
			}
	    }
		else
		{
	   		Reporter.log(" Pro active Monitoring' dropdown is not available under 'Edit Site Order' page");
		}
	}
	
	public void editSiteOrder_Performancereporting(String performReport)throws InterruptedException, IOException
	{

		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereporting))
		{
			if (performReport.equalsIgnoreCase("null"))
			{
		   		Reporter.log("Performance reporting dropdown is not edited");
	
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereporting_xbutton,"Addsiteorder performancereporting");
				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereporting_xbutton,"Addsiteorder performancereporting");
				
				waitforPagetobeenable();
				
				webDriver.findElement(By.xpath("//div[label[text()='Performance Reporting']]//div[text()='" + performReport + "']")).click();
				waitforPagetobeenable();

		   		Reporter.log("Edited value for 'Performance reporting' dropdown is: " + performReport);
			}
	    }
		else
		{
	   		Reporter.log("Performance Reporting' dropdown is not available under 'Edit Site Order' page");
		}
	}
	
	public void addInterface(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String bearerType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_bearerType_out");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_encapsulation_out");
		String pppEncapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_pppEncapsulation_out");
		String dslDownStreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_dslDownStreamSpeed_out");
		String dslUpstreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_dslUpstreamSpeed");
		String mbsDropdown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_mbsDropdown_out");
		String vpi = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_vpi_out");
		String vci = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_vci_out");
		String slot = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_slot_out");
		String port = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_port_out");
		String ivManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_ivManagement");
		String existingAddressRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_existigAddressRangeSelection_out");
		String newAddressRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_newAddressRangeSelection_out");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_country_out");
		String SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_subnetSize_out");
		String availableBlocks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_availableBlock_out");
		String address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_addressDropdownValue");
		String newInterfaceAddressRangeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addInterface_newInterfaceAddressRangeValue_out");
		
		waitforPagetobeenable();

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceName_warningmEssage_out,"Interface");

		if(existingAddressRange.equalsIgnoreCase("Yes") && newAddressRange.equalsIgnoreCase("No"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");
			click(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");
			
			//EIP Allocation
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterface,"EIP Allocation");
			click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterface,"EIP Allocation");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation,"Country");
			select(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation,country);
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown,"Country");
			select(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown,SubnetSize);
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown))

			{
				String EIpmessage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocksMessage_out);
				
		   		Reporter.log("Available Block dropdown is not dipslaying. Message displays as "+EIpmessage);
		   		
		   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
				click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
			
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,"Available Blocks");
				select(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,availableBlocks);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
				click(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
				click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
			}
			//check value in Interface Address Range dropdown
			
			String interfaceAddressRange= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.fetchInterfaceAddressRangeDropdown_addInterfaceIPv6);
			if(isEmptyOrNull(interfaceAddressRange))
			{
		   		Reporter.log("No values displaying under 'Interface Address Range' dropdown");
	
			}
			else
			{
		   		Reporter.log( "'Interface Address Range' dropdown value displays as: "+ interfaceAddressRange);
		   		
		   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.rightArrowButton_interfaceAddressRangeIPv6,">>");
				click(Lanlink_Outbandmanagement.Outbandmanagement.rightArrowButton_interfaceAddressRangeIPv6,">>");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface,"Address");
				select(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface,address);

			}
			
		}
		
		else if(existingAddressRange.equalsIgnoreCase("No") && newAddressRange.equalsIgnoreCase("Yes"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");	
		}
		
		select(Lanlink_Outbandmanagement.Outbandmanagement.bearerTypeDropdown_out,bearerType);
		select(Lanlink_Outbandmanagement.Outbandmanagement.encapsultionDropdown_out,encapsulation);
		select(Lanlink_Outbandmanagement.Outbandmanagement.pppEncapsulation_out,pppEncapsulation);
		select(Lanlink_Outbandmanagement.Outbandmanagement.dslDownStreamdropdown_ut,dslDownStreamSpeed);
		select(Lanlink_Outbandmanagement.Outbandmanagement.dslUpStreamDropdown_out,dslUpstreamSpeed);
		select(Lanlink_Outbandmanagement.Outbandmanagement.MBSdropdown_out,mbsDropdown);

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.PEdevice_interfaceHeaderName,"PEdevice interface");

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterace_vpi_warningMessage_out,"VPI");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.vpiTextFild_out,vpi,"VPI");	
		

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_vci_warningMessage,"VCI");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.vciTextField_out,vci,"VCI");
		

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_PE_slot_warningMessage,"Slot");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.slotTextField_out,slot,"Slot");
		

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_PE_port_warningmEssage,"Port");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.portTextField_out,port,"Port");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.IVmanagement_checkbox_out,"IV Management");
		click(Lanlink_Outbandmanagement.Outbandmanagement.IVmanagement_checkbox_out,ivManagement);
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_confiugrationPanelheader))

		{
	   		Reporter.log("'Configuration' panel is displaying");
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_generateLink,"Generate");
			click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_generateLink,"Generate");
			
			waitforPagetobeenable();
			
			String configurationvalues= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_configurationtextArea);
			if(isEmptyOrNull(configurationvalues))
			{
		   		Reporter.log( "After clicking on 'Generate' button, no values displaying under 'Configuration' text box");

			}
			else
			{
		   		Reporter.log("After clicking on 'Generate' button, "+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
	
			}
		}
		else
		{
	   		Reporter.log("'Configuration' panel is not displaying");

		}
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");

	}
	
	public void addMultilink(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String existingAddressRangeSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_existingAddressRangeSelection_out");
		String newAddressRangeSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_newAddressRangeSelection_out");
		String country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_country_out");
		String SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_subnetSize");
		String availableBlocks = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_availableBlock_out");
		String address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_addressDropdown_out");
		String newInterfaceAddressRangeValue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_newInterfaceAddressRangeValue_out");
		String encapsulation = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_encapsulation_out");
		String dslDownstreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_dslDownstreamSpeed_out");
		String dslUpstreamSpeed = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addMultilink_dslUpstreamSpeed_out");
		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		
		if(existingAddressRangeSelection.equalsIgnoreCase("Yes") && newAddressRangeSelection.equalsIgnoreCase("No"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");
			click(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");
			
			//EIP Allocation
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterfaceIpv6,"EIP Allocation");
			click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterfaceIpv6,"EIP Allocation");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation,"Country");
			select(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation,country);
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown,"Country");
			select(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown,SubnetSize);
			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown))

			{
				String EIpmessage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocksMessage_out);
				
		   		Reporter.log("Available Block dropdown is not displaying. Message displays as "+EIpmessage);
		   		
		   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
				click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
			
			}
			else
			{
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,"Available Blocks");
				select(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,availableBlocks);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
				click(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
				click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
			}
			//check value in Interface Address Range dropdown
			
			String interfaceAddressRange= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.fetchInterfaceAddressRangeDropdown_addInterfaceIPv6);
			if(isEmptyOrNull(interfaceAddressRange))
			{
		   		Reporter.log("No values displaying under 'Interface Address Range' dropdown");
	
			}
			else
			{
		   		Reporter.log("'Interface Address Range' dropdown value displays as: "+ interfaceAddressRange);
		   		
		   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.rightArrowButton_interfaceAddressRangeIPv6,">>");
				click(Lanlink_Outbandmanagement.Outbandmanagement.rightArrowButton_interfaceAddressRangeIPv6,">>");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface,"Address");
				select(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface,address);

			}
			
		}
		
		else if(existingAddressRangeSelection.equalsIgnoreCase("No") && newAddressRangeSelection.equalsIgnoreCase("Yes"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");	
		}

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addMultilink_PE_encapsulation_warningMessage,"Encapsulation");
		select(Lanlink_Outbandmanagement.Outbandmanagement.encapsultionDropdown_out,encapsulation);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.downloadDSrdropdown_warningMessage,"DSL Downstream Speed (PCR)");
		select(Lanlink_Outbandmanagement.Outbandmanagement.addMultilink_dslDownstreamSpeed,dslDownstreamSpeed);
		
		select(Lanlink_Outbandmanagement.Outbandmanagement.addmultilink_dslUpstreamSpeed_out,dslUpstreamSpeed);
		
		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_confiugrationPanelheader))

		{
	   		Reporter.log("'Configuration' panel is displaying");
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_generateLink,"Generate");
			click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_generateLink,"Generate");
			
			waitforPagetobeenable();
			
			String configurationvalues= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_configurationtextArea);
			if(isEmptyOrNull(configurationvalues))
			{
		   		Reporter.log("After clicking on 'Generate' button, no values displaying under 'Configuration' text box");

			}
			else
			{
		   		Reporter.log("After clicking on 'Generate' button, "+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
	
			}
		}
		else
		{
	   		Reporter.log("'Configuration' panel is not displaying");

		}
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");

	}
	
	public void addLoopback(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String interfaceAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addLoopback_interfaceAddress_out");
		String ivManagement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "PE_addLoopback_ivmanagement_out");
		
		waitforPagetobeenable();
		String interfacevalue= getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.fetchCPEdeviceName_out,"value");

		if(isEmptyOrNull(interfacevalue))
		{
	   		Reporter.log("No values displaying under 'Interface' text field");
	
		}
		else
		{
	   		Reporter.log("Interface value is displaying as: " + interfacevalue);

		}
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addLoopback_PE_addInterface_out,"Interface Address");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addLoopback_addInterfcae_textField_out,"Interface Address");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addLoopback_addInterfcae_textField_out,interfaceAddress,"Interface Address");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.IVmanagement_checkbox_out,"IV Management");
		click(Lanlink_Outbandmanagement.Outbandmanagement.IVmanagement_checkbox_out,ivManagement);
		
		waitforPagetobeenable();
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_confiugrationPanelheader))

		{
	   		Reporter.log("'Configuration' panel is displaying");
	   		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_generateLink,"Generate");
			click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_generateLink,"Generate");
			
			waitforPagetobeenable();
			
			String configurationvalues= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_configurationtextArea);
			if(isEmptyOrNull(configurationvalues))
			{
		   		Reporter.log( "After clicking on 'Generate' button, no values displaying under 'Configuration' text box");

			}
			else
			{
		   		Reporter.log("After clicking on 'Generate' button, "+ "under 'Configuration' textbox values displaying as: "+configurationvalues);
	
			}
		}
		else
		{
	   		Reporter.log("'Configuration' panel is not displaying");

		}
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addInterface_executeAndSaveButton,"Execute and Save");

	}
	
	public void addCPEdevice_CustomerPremiseEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String routerId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_routerId_out");
		String vendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_vendorModel_out");
		String snmpV3Contextname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3ContextName_out");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_managementAddress_out");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpro_out");
		String snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmprw_out");
		String snmpV3SecurityUsername = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpv3SecurityUsername");
		String snmpv3AuthPassword = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3AuthPassword");
		String snmpV3ContextEngineId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3COntextEngineId_out");
		String snmpV3AuthProto = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3AuthProto_out");
		String existingpremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_existingPremiseSelection_out");
		String NewPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_newPremiseSelection_out");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_existingPremiseValue_out");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_premiseCode");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CEdevice_AddrouterId_out,"Router Id");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CEdevice_AddrouterId_out,routerId,"Router Id");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdveice_vendorModel_out,"Vendor/Model");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPEdveice_vendorModel_out,vendorModel);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_managementAddress_out,"Management Address");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_managementAddress_out,managementAddress,"Management Address");
		
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmpro_out,snmpro,"Snmpro");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmprw_out,snmprw,"Snmprw");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmpV3contextname,snmpV3Contextname,"Snmp V3 Context Name");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMpv3ContextEngineID,snmpV3ContextEngineId,"Snmp V3 Context Engine ID");
		
		waitforPagetobeenable();

		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SnmpV3SecurityUsername,snmpV3SecurityUsername,"Snmp V3 Security Username");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMpv3AuthPassword,snmpv3AuthPassword,"Snmp V3 Auth Password");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMPv3AuthProto_out,"Snmp V3 Auth Proto");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMPv3AuthProto_out,snmpV3AuthProto);
		
		if (existingpremiseSelection.equalsIgnoreCase("yes") & NewPremiseSelection.equalsIgnoreCase("no"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.premisedropdowninput,"Premise");
			select(Lanlink_Outbandmanagement.Outbandmanagement.premisedropdowninput,existingpremisevalue);
		}
		
		// New Premise
				else if (existingpremiseSelection.equalsIgnoreCase("no") & NewPremiseSelection.equalsIgnoreCase("yes"))
				{
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addCPE_selectpremise_out,"Select Premise Toggle");
					select(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addCPE_selectpremise_out,"Select Premise Toggle");
					
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addCityToggleSelected,premisename,"Premise Name");
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addCityToggleSelected,premisecode,"Premise Code");

				}
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		select(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		
		waitforPagetobeenable();
	
	}
	
	public void editCPEdevice_CustomerPremiseEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String routerId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_routerId_out");
		String vendorModel = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_vendorModel_out");
		String snmpV3Contextname = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3ContextName_out");
		String managementAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_managementAddress_out");
		String snmpro = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpro_out");
		String snmprw = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmprw_out");
		String snmpV3SecurityUsername = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpv3SecurityUsername");
		String snmpv3AuthPassword = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3AuthPassword");
		String snmpV3ContextEngineId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3COntextEngineId_out");
		String snmpV3AuthProto = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_snmpV3AuthProto_out");
		String existingpremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_existingPremiseSelection_out");
		String NewPremiseSelection = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_newPremiseSelection_out");
		String existingpremisevalue = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_existingPremiseValue_out");
		String premisename = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_premisename");
		String premisecode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_premiseCode");
		
		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CEdevice_AddrouterId_out,"Router Id");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CEdevice_AddrouterId_out,routerId,"Router Id");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdveice_vendorModel_out,"Vendor/Model");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPEdveice_vendorModel_out,vendorModel);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_managementAddress_out,"Management Address");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_managementAddress_out,managementAddress,"Management Address");
		
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmpro_out,snmpro,"Snmpro");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmprw_out,snmprw,"Snmprw");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmpV3contextname,snmpV3Contextname,"Snmp V3 Context Name");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMpv3ContextEngineID,snmpV3ContextEngineId,"Snmp V3 Context Engine ID");
		
		waitforPagetobeenable();

		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SnmpV3SecurityUsername,snmpV3SecurityUsername,"Snmp V3 Security Username");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMpv3AuthPassword,snmpv3AuthPassword,"Snmp V3 Auth Password");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMPv3AuthProto_out,"Snmp V3 Auth Proto");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_SNMPv3AuthProto_out,snmpV3AuthProto);
		
		if (existingpremiseSelection.equalsIgnoreCase("yes") & NewPremiseSelection.equalsIgnoreCase("no"))
		{
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.premisedropdowninput,"Premise");
			select(Lanlink_Outbandmanagement.Outbandmanagement.premisedropdowninput,existingpremisevalue);
		}
		
		// New Premise
				else if (existingpremiseSelection.equalsIgnoreCase("no") & NewPremiseSelection.equalsIgnoreCase("yes"))
				{
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addCPE_selectpremise_out,"Select Premise Toggle");
					select(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addCPE_selectpremise_out,"Select Premise Toggle");
					
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addCityToggleSelected,premisename,"Premise Name");
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addCityToggleSelected,premisecode,"Premise Code");

				}
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		select(Lanlink_Outbandmanagement.Outbandmanagement.AddnewlinkPe2CPE_Nextbutton,"Next");
		
		waitforPagetobeenable();
	
	}
	
	public void viewPPPconfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo,String CPEdeviceName) throws InterruptedException, IOException

	{
		String framedWANipAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppconfiguration_framedWANipAddress_out");
		String framedRoute0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute0");
		String framedRoute1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute1");
		String framedRoute2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute2");
		String framedRoute3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute3");
		String framedRoute4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute4");
		String framedRoute5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute5");
		String framedRoute6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute6");
		String framedRoute7 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute7");
		
        compareText_InViewPage("Device", CPEdeviceName);		//CPE device Name
		
		compareText_InViewPage("Framed/WAN/IP Address", framedWANipAddress);		//Framed/WAN/IP Address
		
		compareText_InViewPage("Framed Protocol", "PPP");			//Framed Protocol
		
		compareText_InViewPage("Framed Route0", framedRoute0);		//Framed Route0
		
		compareText_InViewPage("Framed Route1", framedRoute1);		//Framed Route1
		
		compareText_InViewPage("Framed Route2", framedRoute2);		//Framed Route2
		
		compareText_InViewPage("Framed Route3", framedRoute3);		//Framed Route3
		
		compareText_InViewPage("Framed Route4", framedRoute4);		//Framed Route4
		
		compareText_InViewPage("Framed Route5", framedRoute5);		//Framed Route5
		
		compareText_InViewPage("Framed Route6", framedRoute6);		//Framed Route6
		
		compareText_InViewPage("Framed Route7", framedRoute7);		//Framed Route7

		
		
	}
	
	public void compareText_InViewPage(String labelname,  String ExpectedText) throws IOException
			{
		String text = null;
		WebElement element = null;
		
		waitForAjax();
		
		 element = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.labelname+labelname+Lanlink_Outbandmanagement.Outbandmanagement.labelname1);
		 String emptyele = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.labelname+labelname+Lanlink_Outbandmanagement.Outbandmanagement.labelname1);
		 
			if(element==null)
			{
		   		Reporter.log(labelname+" not found");
			}
			else if (emptyele!=null && emptyele.isEmpty())
			{
				emptyele= "Null";
				
				if(emptyele.equalsIgnoreCase(ExpectedText))
				{
			   		Reporter.log("The Expected value for '"+ labelname +"' field  is same as the Acutal value. It is id displaying blank");

				}
				else
				{
			   		Reporter.log("The Expected value '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
	
				}
			}
			
			else
			{
				text = element.getText();
				if((text.contains(" ")) ||  text.contains("-"))
				{
					String[] actualTextValue=text.split(" ");
					String[] expectedValue =ExpectedText.split(" ");
					
					if(expectedValue[0].equalsIgnoreCase(actualTextValue[0]))
					{
				   		Reporter.log(" The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
					}
					else if(expectedValue[0].contains(actualTextValue[0])) 
					{
				   		Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");
	
					}
					else
					{
				   		Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");
					}
				}
				else
				{
					if(ExpectedText.equalsIgnoreCase(text))
					{
				   		Reporter.log(" The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");

					}
					else if(ExpectedText.contains(text))
					{
				   		Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is same as the Acutal value '"+text+"'");

					}
					else
					{
				   		Reporter.log("The Expected value for '"+ labelname +"' field '"+ExpectedText+"' is not same as the Acutal value '"+text+"'");

					}
				}
			}
		
			}
	
	public void editPPPconfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException

	{
		String framedRoute0 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_editpppConfiguration_framedRoute0");
		String framedRoute1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_editpppConfiguration_framedRoute1");
		String framedRoute2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_editpppConfiguration_framedRoute2");
		String framedRoute3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute3");
		String framedRoute4 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute4");
		String framedRoute5 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute5");
		String framedRoute6 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute6");
		String framedRoute7 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute7");
		String uniVirtualRouterName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_uniVirtualRouterName");
		String uniLoopbackInteface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_uniLoopbackInteface");
		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter0,"Framed Route0");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter0,framedRoute0,"Framed Route0");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter1,"Framed Route1");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter1,framedRoute1,"Framed Route1");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedroute2,"Framed Route2");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedroute2,framedRoute2,"Framed Route2");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute3,"Framed Route3");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute3,framedRoute3,"Framed Route3");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPconfig_framedRoute4,"Framed Route4");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPconfig_framedRoute4,framedRoute4,"Framed Route4");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute5,"Framed Route5");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute5,framedRoute5,"Framed Route5");
		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute6,"Framed Route6");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute6,framedRoute6,"Framed Route6");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute7,"Framed Route7");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute7,framedRoute7,"Framed Route7");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,"Uni Virtual Route Name");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,uniVirtualRouterName,"Uni Virtual Route Name");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniLocalLoopbackInterface,"Uni Local Loopback Interface");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniLocalLoopbackInterface,uniLoopbackInteface,"Uni Local Loopback Interface");
		
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK");
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK");

	}
	public void verifysuccessmessage(String expected) throws IOException, InterruptedException {
		waitforPagetobeenable();
		scrollUp();
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AlertForServiceCreationSuccessMessage, "Success message"+expected);

	}
	
	public void selectCustomertocreateOrderfromleftpane(String ChooseCustomerToBeSelected) throws IOException, InterruptedException
	{
		
		
	//	String CustomerName="*";
		mouseMoveOn(Lanlink_Outbandmanagement.Outbandmanagement.ManageCustomerServiceLink);
		waitforPagetobeenable();
		Reporter.log("Mouser hovered on Manage Customer's Service");

		click(Lanlink_Outbandmanagement.Outbandmanagement.CreateOrderServiceLink,"Create Order/Service Link");	
		Reporter.log("=== Create Order/Service navigated ===");


		//Entering Customer name
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.entercustomernamefield, ChooseCustomerToBeSelected,"Customer Name");
		
		
		
	//	sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.entercustomernamefield,CustomerName,"Customer Name");
		
		
		waitToPageLoad();
		waitforPagetobeenable();

		//Select Customer from dropdown
		addDropdownValues_commonMethod("Choose a customer",Lanlink_Outbandmanagement.Outbandmanagement.chooseCustomerdropdown, ChooseCustomerToBeSelected);
			click(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton,"Next");
		
	}
	
	public void Enteraddsiteorder() throws IOException, InterruptedException 
	{

		waitforPagetobeenable();
	
		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	//waitforPagetobeenable();
	//verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Actiondropdown_siteorder,"Action Dropdown Siteorder");
	//click(Lanlink_Outbandmanagement.Outbandmanagement.Actiondropdown_siteorder,"Action Dropdown Siteorder");
	waitforPagetobeenable();
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorderlink,"Addsite Orderlink");

	click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorderlink,"Addsite Orderlink");
}
	
	public void verifyFieldsandSelectCPEdevicefortheserviceselected_existingDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo,String interfacespeed) throws InterruptedException, IOException 
	{
		String existingDeviceName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Equip_existingDevicename");
		String technologySelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"technology");
		String vpntopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vpnTopology");

   		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		
		
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_adddevicelink,"Add Device");
		waitforPagetobeenable();
		waitToPageLoad();
		 
		if((technologySelected.equalsIgnoreCase("Atrica")) && (vpntopology.equals("Hub&Spoke")) && (interfacespeed.equals("1GigE"))) {
			selectTechnology_HubAndSpoke();
		}
		waitToPageLoad();
		
		click(Lanlink_Outbandmanagement.Outbandmanagement.existingDevice_SelectDeviceToggleButton, "Select Device");
		waitforPagetobeenable();
		waitToPageLoad();
		
		click(Lanlink_Outbandmanagement.Outbandmanagement.chooseAdeviceDropdown, existingDeviceName);
		waitToPageLoad();
		
   		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
		}
	
	public void deleteService() throws InterruptedException, IOException 
	{
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel, "view Service page");
		
		waitforPagetobeenable();
		click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_actiondropdown,"Action");
		waitforPagetobeenable();

		click(Lanlink_Outbandmanagement.Outbandmanagement.deleteLink_common,"Delete");
		
		if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup))

		{		click(Lanlink_Outbandmanagement.Outbandmanagement.deletebuttons, "Delete");
		waitforPagetobeenable();

		}
		 else
         {
			 Reporter.log("Delete alert popup is not displayed");
         }
	}
	
	public void clickOnBreadCrump(String breadCrumpLink) throws InterruptedException, IOException 
	{
		//String breadCrumpLink= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");

		waitforPagetobeenable();
		
		WebElement breadcrumb = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.breadcrump).replace("value", breadCrumpLink));
		if(breadcrumb.isDisplayed())
		{
			click_commonMethod_PassingWebelementDirectly_forBreadcrumb("Breadcrump", breadcrumb);
			
			waitforPagetobeenable();

		}
		else
		{
			 Reporter.log("Breadcrumb is not displaying for the element");
		}

	}

public void addIPVPNsiteorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,  IOException {
		
		
		String country= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "country");
		
 		String performReport= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "performReport");
 		String ProactiveMonitor=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ProactiveMonitor");
 		
		String smartmonitor=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "smartmonitor");
 		String siteallias=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteallias");
 		
		String remark=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "remark");
		String siteOrderNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber");
		String IVReference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_Ivrefrence");
		String perCoSPerformancereport=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_perCoSPerformancereport");
		
		String primarySiteOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "primarySiteOrder");		
		String wholesaleProvider=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_wholesaleProvider");
		String managedSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_managedSite");
		
		String priority=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteORder_priority");
		
		String actelisBased=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_actelisBased");
		String voip=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SiteOrder_voip");
		String voipClassOfService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrder_voipClassOfService");
 
		//ExtentTestManager.getTest().log(LogStatus.INFO, "Add IPVPN Site Order");
		Reporter.log("INFO Add IPVPN Site Order,PASS");
		
		//addtextFields_commonMethod("Site Order Number", "dslSiteOrder_outband", siteOrderNumber, xml); // dsl
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.dslSiteOrder_outband,"Site Order Number" );																											// Site
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.dslSiteOrder_outband,siteOrderNumber,"Site Order Number");																											// order
																													// Number
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.country,"Country" );																											// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.country,"Country");
		selectValue(null, country);
		
		

		//City_AddIPVPNSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode,sitevalue, CSR_Name, existingsiteselection, newsiteselection);
		City_AddIPVPNSiteorder(testDataFile,sheetName,scriptNo,dataSetNo);
		// scroll to bottom
		waitforPagetobeenable();
		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.country);

		performancereporting_AddSiteOrder(performReport);

		proactiveMonitoring_AddSiteOrder(ProactiveMonitor);

		smartsMonitoring_AddSiteOrder(smartmonitor);

		// Primary Site Order
		//addDropdownValues_commonMethod(application, "Primary Site Order", "primarySiteOrder_out", primarySiteOrder,xml);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.primarySiteOrder_out,"Primary Site Order" );																											// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.primarySiteOrder_out,"Primary Site Order");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.primarySiteOrder_out,primarySiteOrder,"Primary Site Order");	
		//selectValue(null, primarySiteOrder);

		// Per CoS Performance reporting
		perCoSperformancereporting_AddSiteOrder(perCoSPerformancereport);

		// Site Alias
		SiteAlias_AddSiteOrder(siteallias);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias,"Add siteorder sitealias" );																											// Site
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias,siteallias,"Add siteorder sitealias");
		waitforPagetobeenable();
		// IV Reference
		//addDropdownValues_commonMethod(application, "IV Reference", "Addsiteorder_IVreferencedropdown", IVReference,xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_IVreferencedropdown,"IV Reference" );																											// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_IVreferencedropdown,"IV Reference");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_IVreferencedropdown,IVReference,"IV Reference");	
		//selectValue(null, IVReference);

		// Router Configuration View IPv4
		//RouterConfigurationViewIPv4_addSiteOrder(routerConfigurationViewIpv4);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.routerConfigurationviewIPv4_out,"Router Configuration View IPv4");
		click(Lanlink_Outbandmanagement.Outbandmanagement.routerConfigurationviewIPv4_out,"Router Configuration View IPv4");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.RouterConfigurationViewIPv4_1+ performReport +Lanlink_Outbandmanagement.Outbandmanagement.RouterConfigurationViewIPv4_2,"Router Configuration View IPv4");
		click(Lanlink_Outbandmanagement.Outbandmanagement.RouterConfigurationViewIPv4_1+ performReport +Lanlink_Outbandmanagement.Outbandmanagement.RouterConfigurationViewIPv4_2,"Router Configuration View IPv4");

		// Wholesale Provider
		//addDropdownValues_commonMethod(application, "Wholesale Provider", "wholesaleProvider_out", wholesaleProvider,xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.wholesaleProvider_out,"Wholesale Provider" );																											// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.wholesaleProvider_out,"Wholesale Provider");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.wholesaleProvider_out,wholesaleProvider,"Wholesale Provider");	
		//selectValue(null, wholesaleProvider);
		// Managed Site
		//managedSite_AddSiteOrder(managedSite);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out,"managed Site xbutton out");
		click(Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out,"managed Site xbutton out");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out1+ managedSite +Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out2,"managed Site xbutton out");
		click(Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out1+ managedSite +Lanlink_Outbandmanagement.Outbandmanagement.managedSite_xbutton_out2,"managed Site xbutton out");

		// Priority
		//addDropdownValues_commonMethod(application, "Priority", "priority_out", priority, xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.priority_out,"Priority" );																											// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.priority_out,"Priority");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.priority_out,priority,"Priority");	
		//selectValue(null, priority);

		// Actelis Based
		//addCheckbox_commonMethod(application, "actelisBased_out", "actelisBased_out", actelisBased, "no", xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.actelisBased_out,"actelisBased_out");																									// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.actelisBased_out,"actelisBased_out");
		selectValue(null,actelisBased );

		// VOIP
		//addCheckbox_commonMethod(application, "voip_out", "VoIP", voip, "no", xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.voip_out,"VoIP");																									// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.voip_out,"VoIP");
		selectValue(null,voip );

		if (voip.equalsIgnoreCase("yes")) {
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.voipClassOfService_out,"VoIP Class of Service" );																											// Site
			click(Lanlink_Outbandmanagement.Outbandmanagement.voipClassOfService_out,"VoIP Class of Service");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.voipClassOfService_out,voipClassOfService,"VoIP Class of Service");	
			//selectValue(null, voipClassOfService);
			
		}

		// Remark Addsiteorder_remark
		//remark_AddSiteOrder(remark);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark,"Add Site Order Remark");																											// Site
		//click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark,"Addsiteorder_remark");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark,remark,"Add Site Order Remark");

		waitforPagetobeenable();
		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
		waitforPagetobeenable();
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK Button");
		click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK Button");

	}

public void perCoSperformancereporting_AddSiteOrder(String performReport)throws InterruptedException, IOException
{

	// Perfomance Reporting
	if (performReport.equalsIgnoreCase("Null")) 
	{

		Reporter.log("NO changes in 'Performance Reporting' dropdown Per CoS Performance reporting input value is not provided. 'Follow Service' is selected by default PASS");
	} 
	else {
		
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out,"perCoSperformance reporting xbutton out");
			click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out,"perCoSperformance reporting xbutton out");
			
			waitforPagetobeenable();
			
			//Clickon(getwebelement("//div[label[text()='Per CoS Performance Reporting']]//div[text()='" + performReport + "']"));
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out1+ performReport +Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out2,"Per CoS Performance Reporting");
			click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out1+ performReport +Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbutton_out2,"Per CoS Performance Reporting");
			waitforPagetobeenable();

			String actualvalue = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformancereporting_xbuttonOut);
					
			Reporter.log(actualvalue + " is selected under Per CoS Performance reporting dropdownPASS");	
}	
}

public void smartsMonitoring_AddSiteOrder(String smartmonitor)throws InterruptedException, IOException {
  	 
	if(smartmonitor.equalsIgnoreCase("null")) {
		
		Reporter.log("INFO Smart monitoring value is not provided Follow Service' is selected by default PASS");
	}else {
		
	
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoring_xbutton,"Add siteorder smart monitoring xbutton");
	click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoring_xbutton,"Add siteorder smart monitoring xbutton");
	waitforPagetobeenable();
	
	waitforPagetobeenable();
	webDriver.findElement(By.xpath("//div[label[text()='Smarts Monitoring']]//div[text()='"+ smartmonitor +"']")).click();
	waitforPagetobeenable();
		
	//	String actualvalue=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.smartmonitor);
	
	//	Reporter.log(actualvalue+ " is selected under Smart monitoring dropdown PASS");

	}

}

public void proactiveMonitoring_AddSiteOrder(String ProactiveMonitor)throws InterruptedException, IOException{
  	 
   	//Pro active monitoring	
 		if(ProactiveMonitor.equalsIgnoreCase("Null")) {
 			
 			Reporter.log("Pro active monitoring value is not provided. 'Follow Service' is selected by default PASS");
 		}else {	
 		
 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsitorder_proactivemonitoring_xbutton,"Add sitorder proactive monitoring xbutton");
		click(Lanlink_Outbandmanagement.Outbandmanagement.Addsitorder_proactivemonitoring_xbutton,"Add sitorder_proactive monitoring xbutton");
 		waitforPagetobeenable();
 		webDriver.findElement(By.xpath("//div[label[text()='Proactive Monitoring']]//div[text()='"+ ProactiveMonitor +"']")).click(); 		//Clickon(getwebelement("//div[label[text()='Proactive Monitoring']]//div[text()='"+ ProactiveMonitor +"']"));
 		waitforPagetobeenable();
 		
 		//String actualvalue=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.ProactiveMonitor);
 		
 		//Reporter.log(actualvalue+  "is selected under proactive monitoring dropdown PASS");
 		
 		} 
    }

public void performancereporting_AddSiteOrder(String performReport)throws InterruptedException, IOException {
  	 
   	//Perfomance Reporting	
		if(performReport.equalsIgnoreCase("Null")) {
			
			Reporter.log("Performance reporting value is not provided. 'Follow Service' is selected by default PASS");
		}else {
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereporting_xbutton,"Add siteorder Performancereporting xbutton");
		click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereporting_xbutton,"Add siteorder Performancereporting xbutton");
		
		waitforPagetobeenable();
		//Clickon(getwebelement("//div[label[text()='Performance Reporting']]//div[text()='"+ performReport +"']"));
		
		webDriver.findElement(By.xpath("//div[label[text()='Performance Reporting']]//div[text()='"+ performReport +"']")).click();
		waitforPagetobeenable();
		
		//String actualvalue=webDriver.findElement(By.xpath("(//div[label[text()='Performance Reporting']]//span)[2]")).getText();
		
	//	Reporter.log(actualvalue + " is selected under Performance reporting dropdown PASS");
	
		}
    }

public void City_AddIPVPNSiteorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String existingcityselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingcityselection");
	String city= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "city");
	String newcityselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcityselection");
	String xngcityname= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng city name");
	String xngcitycode= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xng city code");
	String sitevalue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitevalue");
	String CSR_Name= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSR_Name");
	String existingsiteselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingsiteselection");
	String newsiteselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newsiteselection");
	//Existing City
			if((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

				//selectValueInsideDropdown(application, "addIPVPNsiteOrder_cityDropdown", "City", city, xml);
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addIPVPNsiteOrder_cityDropdown,"addIPVPNsiteOrder_cityDropdown");
    			click(Lanlink_Outbandmanagement.Outbandmanagement.addIPVPNsiteOrder_cityDropdown,"City");
    			selectValue(null, city);
				
			//Existing Site	
				if((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
	    			
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Sitetogglebutton,"Addsiteorder_Sitetogglebutton");
	    			click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Sitetogglebutton,"Add siteorder Site Toggle Button");
					
					 waitforPagetobeenable();
					//selectValueInsideDropdown(application, "addIPVPNsiteOrer_siteDropdown", "Physical Site", sitevalue, xml);
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addIPVPNsiteOrer_siteDropdown,"addIPVPNsiteOrer_siteDropdown");
	    			click(Lanlink_Outbandmanagement.Outbandmanagement.addIPVPNsiteOrer_siteDropdown,"Physical Site");
	    			selectValue(null, sitevalue);
	    		}
				
			//New site
				else if((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
					
					if(CSR_Name.equalsIgnoreCase("null")){
						
						Reporter.log("CSR name field is mandatory and no values are provided No values provided for mandatory field 'CSR Name'");
						
					}else {
					//SendKeys(getwebelement(xml.getlocator("//locators/" + application + "/Addsiteorder_CSRname")), CSR_Name);
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_CSRname,"Add site order_CSRname");	
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_CSRname,CSR_Name,"Add site order CSRname");
					
					waitforPagetobeenable();
					
					Reporter.log(CSR_Name+ " is entered under CSR Name field");
					}
				}
			}
			else if((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {
				
				//Clickon(getwebelement(xml.getlocator("//locators/" + application + "/Addsiteorder_Citytogglebutton")));
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Citytogglebutton,"Addsiteorder_Citytogglebutton");
    			click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Citytogglebutton,"Add Siteorder City toggle Button");
				waitforPagetobeenable();
				
				//City name 
				 if(xngcityname.equalsIgnoreCase("null")) {
					 Reporter.log("City name field is a mandatory field and the value is not provided");
				 }else {
				 verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcityname,"Add siteorder xngcityname");	
				 sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcityname,xngcityname,"Add siteorder xngcityname");
				 waitforPagetobeenable();
				 Reporter.log(xngcityname+ " is entered in City name field");
				 waitforPagetobeenable();
				 }
				 
				 //City code
				 if(xngcitycode.equalsIgnoreCase("null")) {
					 Reporter.log("no values provided for city code text field City Code field is a mandatory field and the value is not provided");
				 }else {
					 
				 verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_XNGcitycode,"Add siteorder XNGcitycode");	
				 sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_XNGcitycode,xngcitycode,"Add siteorder XNGcitycode");
				 waitforPagetobeenable();
				 
				 Reporter.log(xngcitycode+" is entered in City Code field");
				 }
				 waitforPagetobeenable();
				 
		//add new Site 		 
				try {
					if(CSR_Name.equalsIgnoreCase("null")){
						
						Reporter.log("no values provided for 'CSR Name' text field CSR name field is mandatory and no values are provided");
					}else {
					
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_CSRname,"Add Siteorder CSRname");	
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_CSRname,CSR_Name,"Add Siteorder CSRname");
					
					
					waitforPagetobeenable();
					
					Reporter.log(CSR_Name+ " is entered under CSR Name field");
					}
					
				}catch(Exception e) {
					e.printStackTrace();
					
					Reporter.log(" CSR NAme not performed");
				}
			}
	}

public void addMultilink_CPEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, IOException {
	String interfaceValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_interfaceTextValue_out");
	String networkValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_network_out");
	String existingAddressRangeSelection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_existingAddressRageSelection");
	String newAddressRangeSelection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_newAddressRangeSelection");
	String country= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_country_out");
	String SubnetSize= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_subnetSize_out");

	String availableBlocks= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_availableBLock_out");
	String newInterfaceAddressRangeValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_newAddressRangeSelection");
	String addressDropdownValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_addressValue_out");

	String IV64bitCounterValue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_IV64bitCounter");
	String ethernet= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_ethernet");
	String speed= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_speed_out");
	String duplex= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_duplex");
	String clockSource= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addMultilink_clockSource");

	scrollUp();
	waitforPagetobeenable();

	boolean addInterfacePanelheader = isVisible(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addMultilinkPanelHeader);
	if (addInterfacePanelheader) {
		//ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Multilink' panel is displaying");
		Reporter.log("'Add Multilink' panel is displaying");

		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out);
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button");
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button");
		//click(application, "OK", "CPE_addInterface_OKbutton_out", xml);
		
		waitforPagetobeenable();
		
		scrollUp();
		
		// Interface
		//warningMessage_commonMethod(application, "InterfaceName_warningmEssage_out", "Interface", xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceName_warningmEssage_out,"Interface");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addMultilink_InterfaceTextfield,"Interface");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addMultilink_InterfaceTextfield,interfaceValue,"Interfacee");
		// Network dropdown
		//addDropdownValues_commonMethod(application, "Network", "CPE_network_addInterface_out", networkValue, xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_network_addInterface_out,"Network");
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPE_network_addInterface_out,"Network");
		selectValue(null, networkValue);
	

		if (networkValue.equals("WAN")) {

			String WANaddressValue = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_addMultilink_addressTextField);

			if (WANaddressValue.isEmpty()) {
				//ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Address' text field");
				Reporter.log("No values displaying under 'Address' text field");

				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");
			} else {
				//ExtentTestManager.getTest().log(LogStatus.PASS,	"On selecting 'WAN' value under 'Network' dropdown,Address value is displaying as: " + WANaddressValue);
			
				Reporter.log("On selecting 'WAN' value under 'Network' dropdown,Address value is displaying as: " + WANaddressValue);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");
			}
		} else if (networkValue.equals("LAN")) {
			if (existingAddressRangeSelection.equalsIgnoreCase("Yes")
					&& newAddressRangeSelection.equalsIgnoreCase("No")) {
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");
				click(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");
				// EIP Allocation
				//click_commonMethod(application, "EIP Allocation", "EIPallocation_addInterfaceIpv6", xml);
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterfaceIpv6,"EIP Allocation");
				click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterfaceIpv6,"EIP Allocation");
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation,"Country");
				click(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation,"Country");
				selectValue(null, country);
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown,"Subnet Size");
				click(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown,"Subnet Size");
				selectValue(null, SubnetSize);
				try {
					boolean availableBlock = isVisible(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown);
									
					if (availableBlock) {

						String EIpmessage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocksMessage_out);

						//ExtentTestManager.getTest().log(LogStatus.FAIL,"Available Block dropdown is not displaying. Message displays as " + EIpmessage);
						
						Reporter.log("Available Block dropdown is not dipslaying. Message displays as " + EIpmessage);

						//click_commonMethod(application, "x", "EIPallocation_xButton", xml);
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
						click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
						
					} else {
						
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,"Available Blocks");
						click(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,"Available Blocks");
						selectValue(null, availableBlocks);

						
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
						click(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
						
						verifysuccessmessage("successfully allocated");

						//click_commonMethod(application, "x", "EIPallocation_xButton", xml);
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
						click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");

					}
				} catch (Exception e) {
					e.printStackTrace();
					String EIpmessage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocksMessage_out);
					//ExtentTestManager.getTest().log(LogStatus.FAIL,	"Available Block dropdown is not displaying. Message displays as " + EIpmessage);
					Reporter.log("Available Block dropdown is not dipslaying. Message displays as" + EIpmessage);

					//click_commonMethod(application, "x", "EIPallocation_xButton", xml);
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
					click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
				}

				//check value in Interface Address Range dropdown
				try {
					String interfaceAddressRange = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.fetchInterfaceAddressRangeDropdown_addInterfaceIPv6,"value");

					if (interfaceAddressRange.isEmpty()) {

						//ExtentTestManager.getTest().log(LogStatus.FAIL,"No values displaying under 'Interface Address Range' dropdown");
						Reporter.log("No values displaying under 'Interface Address Range' dropdown");

					} else {

						//ExtentTestManager.getTest().log(LogStatus.PASS,"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
						
						Reporter.log("'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);

						String addressValue = null;

						//addDropdownValues_commonMethod(application, "Address","FetchInterfaceDropdown_addInterface", addressDropdownValue, xml);
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface, "Address");
						select(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface,addressDropdownValue);
					}
				} catch (Exception e) {
					e.printStackTrace();
					//ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Address' dropdown");
					
					Reporter.log("No values displaying under 'Address' dropdown");
				}
			} else if (existingAddressRangeSelection.equalsIgnoreCase("No")
					&& newAddressRangeSelection.equalsIgnoreCase("No")) {
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");
			}
		}

		// IV 64 Bit Counter
		//addCheckbox_commonMethod(application, "CPE_IV64bitCounter_out", "IV 64 Bit Counter", IV64bitCounterValue,"no", xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_IV64bitCounter_out, "IV 64 Bit Counter");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_IV64bitCounter_out,IV64bitCounterValue);
		
		// Ethernet
		//addCheckbox_commonMethod(application, "CPE_addInterface_ethernet_out", "Ethernet", ethernet, "no", xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_ethernet_out, "Ethernet");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_ethernet_out,ethernet);
		if (ethernet.equalsIgnoreCase("Yes")) {

			//addDropdownValues_commonMethod(application, "Speed", "CPE_addInterface_speed_out", speed, xml); // Speed
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_speed_out,"Speed");
			select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_speed_out,speed);

			//addDropdownValues_commonMethod(application, "Duplex", "CPE_addInterface_duplex_out", duplex, xml); // Duplex
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_duplex_out,"Duplex");
			select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_duplex_out,duplex);
		} else {
			//addDropdownValues_commonMethod(application, "clockSource", "CPE_addInterface_clockSource_out",clockSource, xml); // Clock Source
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_clockSource_out,"clockSource");
			select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_clockSource_out,clockSource);
		}

		// Multilink Bearer Panel
		try {
			boolean multilinkBearerPanel = isVisible(Lanlink_Outbandmanagement.Outbandmanagement.multilinkBearerPanel_addMultilink);
				// isVisible(String locator)
							
			if (multilinkBearerPanel) {
				//ExtentTestManager.getTest().log(LogStatus.PASS, "Multilink Beraer' panel is displaying");
				
				Reporter.log("Multilink Beraer' panel is displaying");
			} else {
				//ExtentTestManager.getTest().log(LogStatus.FAIL, "Multilink Beraer' panel is not displaying");
				
				Reporter.log("Multilink Beraer' panel is ont displaying");
			}
		} catch (Exception e) {
			e.printStackTrace();
			//ExtentTestManager.getTest().log(LogStatus.FAIL, "Multilink Beraer' panel is not displaying");
			
			Reporter.log("Multilink Beraer' panel is not displaying");
		}

		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button" );																											// Site
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button");
		//click_commonMethod(application, "OK", "CPE_addInterface_OKbutton_out", xml);

	} else {
		//ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add Multilink' panel is not displaying");
		
		Reporter.log("'Add Multilink' panel is not displaying");
	}
}

public void addPPPconfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {

	String expectedDevicename= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LANLINK");
	String manualPasswordvalue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_manualPassword_out");
	String framedWANipAddress= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppconfiguration_framedWANipAddress_out");
	String framedRoute0= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute0");
	String framedRoute1= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute1");
	String framedRoute2= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute2");
	String framedRoute3= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute3");
	String framedRoute4= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute4");
	String framedRoute5= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute5");
	String framedRoute6= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute6");
	String framedRoute7= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_framedRoute7");
	String uniVirtualRouterName= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_uniVirtualRouterName");
	String uniLoopbackInteface= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_pppConfiguration_uniLoopbackInteface");
	
	Reporter.log("Add PPP Configuration");
	
	WebElement adpppConfigurationPanelHeader = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfigurationPanel_CPedevice_out);
			//xml.getlocator("//locators/" + application + "/addPPPconfigurationPanel_CPedevice_out"));

	if (adpppConfigurationPanelHeader.isDisplayed()) {
		
		//ExtentTestManager.getTest().log(LogStatus.PASS, "'Add PPP Configuration' panel is displaying");
		Reporter.log("'Add PPP Configuration' panel is displaying");
		// Device Name
		String actualDevicename = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfiguration_deviceName);
		if (expectedDevicename.equalsIgnoreCase(actualDevicename)) {
			//ExtentTestManager.getTest().log(LogStatus.PASS,"Device name is displaying as " + actualDevicename + " as expected");
		
			Reporter.log("Device name is displaying as " + actualDevicename + " as expected");
		} else if (expectedDevicename.contains(actualDevicename)) {
			//ExtentTestManager.getTest().log(LogStatus.PASS,"Device name is displaying as " + actualDevicename + " as expected");
		
			Reporter.log("Device name is displaying as " + actualDevicename + " as expected");
		} else {
			//ExtentTestManager.getTest().log(LogStatus.FAIL, "Device name is displaying as " + actualDevicename + ". But the expected value is " + expectedDevicename);
			
			Reporter.log("Device name is displaying as " + actualDevicename + ". But the expected value is "
					+ expectedDevicename);
		}

		// PPP Password
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfiguration_generatePassowrd,"generate password");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfiguration_generatePassowrd,"generate password");
		

		String passwordField = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfiguration_passwordField,"value");
				
				//getwebelement(xml.getlocator("//locators/" + application + "/addPPPconfiguration_passwordField")).getAttribute("value");
		if (passwordField.isEmpty()) {
			
		} else {
			//ExtentTestManager.getTest().log(LogStatus.PASS,"value displaying under 'Password' field is displaying as " + passwordField);
			
			Reporter.log("value displaying under 'Password' field is displaying as " + passwordField);

			// Enter password manually
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfiguration_passwordField,"PPP Password");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfiguration_passwordField,manualPasswordvalue,"PPP Password");
		}

		// Framed/WAN/IP Address
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPcofig_famedWANipAddress,"Framed/WAN/IP Address");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPcofig_famedWANipAddress,framedWANipAddress,"Framed/WAN/IP Address");

		// Framed Protocol
		String framedProtocol =  getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedProtocol);
				
		if (framedProtocol.equals("PPP")) {
			//ExtentTestManager.getTest().log(LogStatus.PASS,"Framed protocol value is displaying as " + framedProtocol + " as expected");
			
			Reporter.log("Framed protocol value is displaying as " + framedProtocol + " as expected");
			
		} else {
			//ExtentTestManager.getTest().log(LogStatus.PASS,"Framed protocol value is displaying as " + framedProtocol + ". Expected value is 'PPP'");
			Reporter.log("Framed protocol value is displaying as " + framedProtocol + ". Expected value is 'PPP'");

		}

		// Framed Route0
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter0,"Framed Route0");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter0,framedRoute0,"Framed Route0");
		// Framed Router1
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter1,"Framed Route1");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRouter1,framedRoute1,"Framed Route1");
		// Framed Route2
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedroute2,"Framed Route2");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedroute2,framedRoute2,"Framed Route2");
		// Framed Route3
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute3,"Framed Route3");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute3,framedRoute3,"Framed Route3");
		// Framed Route4
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPconfig_framedRoute4,"Framed Route4");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPconfig_framedRoute4,framedRoute4,"Framed Route4");
		// Framed Router5
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute5,"Framed Route5");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute5,framedRoute5,"Framed Route5");
		// Framed route6
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute6,"Framed Route6");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute6,framedRoute6,"Framed Route6");
		// Framed route7
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute7,"Framed Route7");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_framedRoute7,framedRoute7,"Framed Route7");

		// Uni Virtual Route Name
		String uniVirtualActualValue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,"value");
				
				//getwebelement(xml.getlocator("//locators/" + application + "/addPPPconfig_uniVirtualRouterName")).getAttribute("value");
		if (uniVirtualActualValue.isEmpty()) {
			//ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Uni Virtual Route Name'");
			
			Reporter.log("No values dipslaying under 'Uni Virtual Route Name'");
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,"Uni Virtual Route Name");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,uniVirtualRouterName,"Uni Virtual Route Name");
		} else {
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,"Uni Virtual Route Name");
			click(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,"Uni Virtual Route Name");
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniVirtualRouterName,uniVirtualRouterName,"Uni Virtual Route Name");
		}

		// Uni Local Loopback Interface
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniLocalLoopbackInterface,"Uni Local Loopback Interface");
		click(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniLocalLoopbackInterface,"Uni Local Loopback Interface");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.addPPPconfig_uniLocalLoopbackInterface,uniLoopbackInteface,"Uni Local Loopback Interface");
		
		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out);
		waitforPagetobeenable();
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK button");
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK button");
	

	} else {
		//ExtentTestManager.getTest().log(LogStatus.PASS, " 'Add PPP Configuration' panel is not displaying");
		
		Reporter.log(" 'Add PPP Configuration' panel is not displaying");
	}
}

public void click_commonMethod_PassingWebelementDirectly_forBreadcrumb(String labelname, WebElement webelement) throws InterruptedException
{
	WebElement element= null;
	
	waitforPagetobeenable();
	
	element = webelement;
	
	if(element==null)
	{
		Reporter.log( "Step:  '"+labelname+"' not found");
	
	}
	else
	{
		element.click();
		Reporter.log("Step: Clicked on '"+labelname+"' button");
	}
		
}

public void addInterface_CPEdevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException,IOException {
	
	String interfaceValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_interfaceTextValue_out");
	String networkValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_network_out");
	String existingAddressRangeSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_existingAddressRageSelection");
	String newAddressRangeSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_newAddressRangeSelection");
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_country_out");
	String SubnetSize=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_subnetSize_out");
	String availableBlocks=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_availableBLock_out");
	String newInterfaceAddressRangeValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_newInterfaceAddressRange_out");
	String addressDropdownValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_addressValue_out");
	String IV64bitCounterValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_IV64bitCounter");
	String ethernet=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_ethernet");
	String speed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_speed_out");
	String duplex=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_duplex");
	String clockSource=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE_addInterface_clockSource");
	
	
	scrollUp();
	waitforPagetobeenable();
	
	boolean addInterfacePanelheader = isVisible(Lanlink_Outbandmanagement.Outbandmanagement.CPE_network_addInterface_out);
	if (addInterfacePanelheader) {
		//ExtentTestManager.getTest().log(LogStatus.PASS, "'Add Interface' panel is displaying");
		Reporter.log("'Add Interface' panel is displaying");

		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out);

		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button");
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button");

		// Interface
		//warningMessage_commonMethod(application, "InterfaceName_warningmEssage_out", "Interface", xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceName_warningmEssage_out,"Interface");
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceField_out,"Interface");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceField_out,interfaceValue,"Interface");
		// Network dropdown
		//addDropdownValues_commonMethod(application, "Network", "CPE_network_addInterface_out", networkValue, xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_network_addInterface_out,"Network");
		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.CPE_network_addInterface_out,networkValue,"Network");
		if (networkValue.equals("WAN")) {

			
			String WANaddressValue = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.CPE_network_addInterface_out);

			if (WANaddressValue.isEmpty()) {
				//ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Address' text field");
				Reporter.log("No values displaying under 'Address' text field");

				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");
			} else {
				//ExtentTestManager.getTest().log(LogStatus.PASS,	"On selecting 'WAN' value under 'Network' dropdown,Address value is displaying as: "+ WANaddressValue);
				Reporter.log("On selecting 'WAN' value under 'Network' dropdown,Address value is displaying as: "+ WANaddressValue);

				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");
			}
		} else if (networkValue.equals("LAN")) {
			if (existingAddressRangeSelection.equalsIgnoreCase("Yes")
					&& newAddressRangeSelection.equalsIgnoreCase("No")) {
				//click_commonMethod(application, "Get subnets", "getSubnets_out", xml);
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");
				click(Lanlink_Outbandmanagement.Outbandmanagement.getSubnets_out,"Get subnets");

				// EIP Allocation
				//click_commonMethod(application, "EIP Allocation", "EIPallocation_addInterfaceIpv6", xml);
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterfaceIpv6,"EIP Allocation");
				click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_addInterfaceIpv6,"EIP Allocation");
				
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation, "Country");
				select(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_EIpallocation,country );

				//addDropdownValues_commonMethod(application, "Subnet Size", "subnetSize_dropdown", SubnetSize, xml);
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown, "Subnet Size");
				select(Lanlink_Outbandmanagement.Outbandmanagement.subnetSize_dropdown,SubnetSize );
				try {
					boolean availableBlock = isVisible(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown);
									
					if (availableBlock) {

						String EIpmessage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocksMessage_out);

						//ExtentTestManager.getTest().log(LogStatus.FAIL,"Available Block dropdown is not displaying. Message displays as " + EIpmessage);
						Reporter.log("Available Block dropdown is not dipslaying. Message displays as " + EIpmessage);

						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
						click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");

					} else {
						//addDropdownValues_commonMethod(application, "Available Blocks", "availableBlocks_dropdown",availableBlocks, xml);
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,"Available Blocks");
						select(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocks_dropdown,availableBlocks );

						//click_commonMethod(application, "Allocate Subnet", "allocateSubnetButton", xml);
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
						click(Lanlink_Outbandmanagement.Outbandmanagement.allocateSubnetButton,"Allocate Subnet");
						
						verifysuccessmessage("successfully allocated");

						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
						click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");

					}
				} catch (Exception e) {
					e.printStackTrace();
					
					String EIpmessage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.availableBlocksMessage_out);
					
					Reporter.log("Available Block dropdown is not dipslaying. Message displays as " + EIpmessage);

					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
					click(Lanlink_Outbandmanagement.Outbandmanagement.EIPallocation_xButton,"x");
				}

				// check value in Interface Address Range dropdown
				try {
					String interfaceAddressRange = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.fetchInterfaceAddressRangeDropdown_addInterfaceIPv6,"value");

					if (interfaceAddressRange.isEmpty()) {

						//ExtentTestManager.getTest().log(LogStatus.FAIL,"No values displaying under 'Interface Address Range' dropdown");
						
						Reporter.log("No values displaying under 'Interface Address Range' dropdown");

					} else {

						//ExtentTestManager.getTest().log(LogStatus.PASS,"'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);
						
						Reporter.log("'Interface Address Range' dropdown value displays as: " + interfaceAddressRange);

						String addressValue = null;	
						//addDropdownValues_commonMethod(application, "Address","FetchInterfaceDropdown_addInterface", addressDropdownValue, xml);
						
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface,"Address");
						select(Lanlink_Outbandmanagement.Outbandmanagement.FetchInterfaceDropdown_addInterface,addressDropdownValue );
					}
				} catch (Exception e) {
					e.printStackTrace();
					//ExtentTestManager.getTest().log(LogStatus.FAIL, "No values displaying under 'Address' dropdown");
					
					Reporter.log("No values displaying under 'Address' dropdown");
				}
			} else if (existingAddressRangeSelection.equalsIgnoreCase("No")
					&& newAddressRangeSelection.equalsIgnoreCase("No")) {
				
				//addtextFields_commonMethod(application, "Interface Address Range","newInterfaceAddressRange_textField", newInterfaceAddressRangeValue, xml);
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,"Interface Address Range");
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.newInterfaceAddressRange_textField,newInterfaceAddressRangeValue,"Interface Address Range");
			}
		}

		// IV 64 Bit Counter
		//addCheckbox_commonMethod(application, "CPE_IV64bitCounter_out", "IV 64 Bit Counter", IV64bitCounterValue,"no", xml);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_IV64bitCounter_out, "IV 64 Bit Counter");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_IV64bitCounter_out,IV64bitCounterValue);
		// Ethernet
		//addCheckbox_commonMethod(application, "CPE_addInterface_ethernet_out", "Ethernet", ethernet, "no", xml);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_ethernet_out, "Ethernet");
		select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_ethernet_out,ethernet);
		
		if (ethernet.equalsIgnoreCase("Yes")) {

			//addDropdownValues_commonMethod(application, "Speed", "CPE_addInterface_speed_out", speed, xml); // Speed
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_speed_out, "Speed");
			select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_speed_out,speed);
			
			//addDropdownValues_commonMethod(application, "Duplex", "CPE_addInterface_duplex_out", duplex, xml); // Duplex
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_duplex_out, "Duplex");
			select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_duplex_out,duplex);
			
		} else {
			//addDropdownValues_commonMethod(application, "clockSource", "CPE_addInterface_clockSource_out",clockSource, xml); // Clock Source
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_clockSource_out, "clockSource");
			select(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_clockSource_out,clockSource);
		}

		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out);
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button");
		click(Lanlink_Outbandmanagement.Outbandmanagement.CPE_addInterface_OKbutton_out,"OK Button");

	} else {
		//ExtentTestManager.getTest().log(LogStatus.FAIL, "'Add Interface' panel is not displaying");
		Reporter.log("'Add Interface' panel is not displaying");
	}
}

public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_1G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException,IOException {
	
	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
	String vender_Overture=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Overture");
	String vender_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Accedian");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
	String poweralarm_Overture=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_overture");
	String powerAlarm_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_Accedian");
	String MediaselectionActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
	String City=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
	String Site=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
	String Premise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
	String newmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
	String existingmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
	String manageaddressdropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_manageaddress_dropdownvalue");
	String existingcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
	String newcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
	String cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
	String citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
	String existingsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
	String newsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
	String sitename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
	String sitecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
	String existingpremiseselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
	String newpremiseselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
	String premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
	String premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
	String technologySelectedfordevicecreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	
	if(technologySelectedfordevicecreation.equalsIgnoreCase("Overture")) 
	{
		deviceCreatoin_Overture(cpename, vender_Overture, snmpro, managementAddress, Mepid, poweralarm_Overture, MediaselectionActualValue,
				Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country, City, Site, Premise, newmanagementAddress, 
				existingmanagementAddress, manageaddressdropdownvalue, existingcityselectionmode, newcityselectionmode, cityname, 
				citycode, existingsiteselectionmode, newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, 
				newpremiseselectionmode, premisename, premisecode);
		
	}
	
	if((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian")) || (technologySelectedfordevicecreation.equalsIgnoreCase("Accedian-1G"))) {
	deviceCreatoin_Accedian(cpename, vender_Accedian, snmpro, managementAddress, Mepid, powerAlarm_Accedian, MediaselectionActualValue,
			Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, country, City, Site, Premise, newmanagementAddress, 
			existingmanagementAddress, manageaddressdropdownvalue, existingcityselectionmode, newcityselectionmode, cityname, 
			citycode, existingsiteselectionmode, newsiteselectionmode, sitename, sitecode, existingpremiseselectionmode, 
			newpremiseselectionmode, premisename, premisecode);
}
	
}
public void device_countrywarningMessage() throws InterruptedException, IOException {
	
	//Country Error Message
   boolean countryErr=false;
		countryErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_countryErrmsg);
		//sa.assertTrue(countryErr, "Country warning message is not displayed ");
		
	if(countryErr) {
		Reporter.log("country warning message is displaying as expected");
		String countryErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_countryErrmsg);
		Reporter.log("Country  message displayed as : " + countryErrMsg);
		Report.LogInfo("Info"," validation message for Country field displayed as : " + countryErrMsg,"PASS");
		Reporter.log("Country warning message displayed as : " + countryErrMsg);	
	}else {
		Reporter.log("Country warning message is not displaying");
		Report.LogInfo("Info", " Validation message for Country dropdown is not displaying","FAIL");
	}
}

public void existingCity(String city) throws InterruptedException,IOException {
	
	selectByVisibleText(Lanlink_Outbandmanagement.Outbandmanagement.cityDropdown_selectTag, "City", city);
	
}

public void deviceCreatoin_Overture(String cpename, String vender, String snmpro,
		String managementAddress, String Mepid, String poweralarm, String MediaselectionActualValue, String Macaddress,
		String serialNumber, String hexaSerialnumber, String linkLostForwarding, String country, String City,
		String Site, String Premise, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
		String existingcityselectionmode, String newcityselectionmode, String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
		String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode, String premisename, String premisecode)throws InterruptedException,IOException {
	
	
	try {	
		String linklostForwardingcheckboxstate="enabled"; 
		
		String[] Vender= {"Overture ISG-26", "Overture ISG-26R", "Overture ISG-26S", "Overture ISG140", "Overture ISG180", "Overture ISG6000"};
		
		String[] powerAlarm= {"AC", "DC"};
		
		String[] MediaSelectionExpectedValue= {"SFP-A with SFP-B","RJ45-A with SFP-B"};	
		
		String expectedDeviceNameFieldAutopopulatedValue="<Device>.lanlink.dcn.colt.net";
		
	////scrolltoend();
	waitforPagetobeenable();

		click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
		waitforPagetobeenable();
		
		//Country Error Message
		device_countrywarningMessage();
		
		//Media Selection Error Message
//		device_mediaSelectionWarningMessage(application);
		
	//scrollDown("//label[text()='Name']");
	waitforPagetobeenable();

		
		// Vendor/Model Error Message
		device_vendorModelWarningMessage();

		// Management Address Error Message
		device_managementAddressWarningMessage();

		// Power Alarm Error Message
		device_powerAlarmWarningMessage();
			
		//MAC Address Error Message
		device_macAddressWarningMessage();
		
		
		
		//Vendor/Model
		device_vendorModel(Vender, vender);      

		//Snmpro
		device_snmPro(snmpro);
		
		//Management Address dropdown
		device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);
		
		//MEP Id
		device_mepID(Mepid);

		//Power Alarm	
		device_powerAlarm(poweralarm);
		
		//MAC Address
		device_MAcaddress(Macaddress);
		

		//Media Selection
	
	device_mediaSelection(MediaSelectionExpectedValue, MediaselectionActualValue);
	

	////scrolltoend();
	waitforPagetobeenable();
		
		
	    //Link lost Forwarding
		device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);
	    

		//Country
		device_country(country);


		//City		
		if(existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
			addCityToggleButton();
		   //New City	
			newcity(newcityselectionmode, cityname, citycode);
		      // New Site
			newSite(newsiteselectionmode, sitename, sitecode);
			  //New Premise	
			newPremise(newpremiseselectionmode, premisename, premisecode);
				
		}	
			
		else if(existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
		//Existing City		
		   existingCity(City);
			
			//Site
		 
			  if(existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
				//Existing Site 
				  existingSite(Site);
				  
				   //Premise
				  if(existingpremiseselectionmode.equalsIgnoreCase("yes") & newpremiseselectionmode.equalsIgnoreCase("no")) {
					  existingPremise(Premise);
				  
		          	 }
				  else if(existingpremiseselectionmode.equalsIgnoreCase("no") & newpremiseselectionmode.equalsIgnoreCase("yes")) {
					  //New Premise
					    addPremiseTogglebutton();
					    newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
				  } 
			
				
	         	}
			
		  else if(existingsiteselectionmode.equalsIgnoreCase("no") & newsiteselectionmode.equalsIgnoreCase("yes")) {
			  	//New Site 
			  	addSiteToggleButton();
			  	newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode); 
			  	
			  	//New Premise
			  	newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
			  }
		}
		
		 //OK button
		
		
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
		
		scrollIntoTop();
		waitforPagetobeenable();
		
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage1_devicename, "Device Name");
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage2_devicename, "Device Name");
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage3_devicename, "Device Name");
		
		//Name
		device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);
		
		//scrolltoend();
		waitforPagetobeenable();
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
			
	//	sa.assertAll();
		
	}catch(AssertionError e) {
		
		e.printStackTrace();
//		Report.LogInfo("Info", "FAiled while verify the fields for Add CPE device","FAIL");
		
	}
	waitforPagetobeenable();

	}


public void deviceCreatoin_Accedian(String cpename, String vender, String snmpro,
		String managementAddress, String Mepid, String poweralarm, String MediaselectionActualValue, String Macaddress,
		String serialNumber, String hexaSerialnumber, String linkLostForwarding, String country, String City,
		String Site, String Premise, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
		String existingcityselectionmode, String newcityselectionmode, String cityname, String citycode, String existingsiteselectionmode, String newsiteselectionmode,
		String sitename, String sitecode, String existingpremiseselectionmode, String newpremiseselectionmode, String premisename, String premisecode)throws InterruptedException,IOException {
	
	
	String linklostForwardingcheckboxstate="disabled"; 
	
	String[] Vender= {"Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX"};
	
	String[] powerAlarm= {"AC", "DC"};
		
	String expectedDeviceNameFieldAutopopulatedValue="<Device>.lanlink.dcn.colt.net";
	
	waitforPagetobeenable();
	
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag,"OK");
	click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag,"OK");
	
	waitforPagetobeenable();
	
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Scroll,"Scroll");
	
	waitforPagetobeenable();
	
	device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);


	device_vendorModel(Vender, vender);      
	
	//Snmpro
	device_snmPro(snmpro);

	device_managementAddress(existingmanagementAddress,newmanagementAddress,managementAddress);
	
	//MEP Id
	device_mepID(Mepid);

	//Power Alarm	
	device_powerAlarm( poweralarm);
	
	device_serialNumber(serialNumber);
	
	waitforPagetobeenable();

	// Country
	device_country(country);

	// verify fields for City, Site and premise

	
	if(existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
	   //New City	
		newcity(newcityselectionmode, cityname, citycode);
	      // New Site
		newSite(newsiteselectionmode, sitename, sitecode);
		  //New Premise	
		newPremise(newpremiseselectionmode, premisename, premisecode);
			
	}	
	else if(existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
		//Existing City		
		   existingCity(existingcityselectionmode, City);
			
			//Site
			  if(existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
				//Existing Site 
				  existingSite(existingsiteselectionmode, Site);
				  
				   //Premise
				  if(existingpremiseselectionmode.equalsIgnoreCase("yes") & newpremiseselectionmode.equalsIgnoreCase("no")) {
					  existingPremise(existingpremiseselectionmode, Premise);
				  
		          	 }
				  else if(existingpremiseselectionmode.equalsIgnoreCase("no") & newpremiseselectionmode.equalsIgnoreCase("yes")) {
					  //New Premise
					  newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
				  } 
	         	}
  		
		  else if(existingsiteselectionmode.equalsIgnoreCase("no") & newsiteselectionmode.equalsIgnoreCase("yes")) {
			  	//New Site 
			  newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode); 
			  	
			  	//New Premise
			  newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
			  }
		}
	waitforPagetobeenable();
	
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK Button");
	click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK Button");
	waitforPagetobeenable();

	
	
}


public void device_vendorModel(String[] Vender, String vender) throws InterruptedException, IOException {
	   boolean vendor=false;

			
			if(isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_vender));
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_vender);
				

try
{
		  List<WebElement> listofvender = findWebElements(Lanlink_Outbandmanagement.Outbandmanagement.ClassNameForDropdowns);
				
				if(listofvender.size()>0) {
		
					for (WebElement vendertypes : listofvender) {

					boolean match = false;
					for (int i = 0; i < Vender.length; i++) {
						if (vendertypes.getText().equals(Vender[i])) {
							match = true;
							
							Reporter.log("list of vendor under add devices are : " + vendertypes.getText());

						}
						}
					
				}
					
				}else {
					Reporter.log("dropdown value inside Vender/Model is empty");
				}
}
				catch(Exception e) {
					  e.printStackTrace();
			 	  }
		  //Entering value inside Vendor/Model dropdown
				if(vender.equalsIgnoreCase("null")) {
					
					Reporter.log("No values has been passed for Mandatory 'Vendor/Model' dropdown for adding device");
					
				}
			else {	
			click(Lanlink_Outbandmanagement.Outbandmanagement.vender1+vender +Lanlink_Outbandmanagement.Outbandmanagement.vender2);
			}
				
	
}

public void device_snmPro(String snmproValueToBeChanged) 
{
		boolean sNmpro=false;
		try {
			
		  sNmpro=isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_snmpro);
		  
		 if(sNmpro) { 
			 Reporter.log(" ' Snmpro' field is displaying in 'Add CPE Device' page as expected");
			 
				boolean actualValue_snmpro = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmpro_autoPopulatedValue).isSelected();
				
				if (actualValue_snmpro) {
					Reporter.log(" Snmpro field value is displaying as expected." + " It is displaying as: "
									+ getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_snmpro,"value"));
					

				} else {
					Reporter.log(
							" 'Snmpro' value is not displaying as expected." + " It is displaying as: "
									+ getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_snmpro,"value"));
				}

			  
			  if(snmproValueToBeChanged.equalsIgnoreCase("null")) {
				 Reporter.log("No changes has been made to 'Snmpro' field"); 
				 Reporter.log(" 'Snmpro' field is displaying as: "+getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_snmpro,"value"));
			  }else {
				clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_snmpro);
				waitforPagetobeenable();
				
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_snmpro, snmproValueToBeChanged,"snmpro");
				waitforPagetobeenable();
				
			  }
			  
		 }else 
		 {
			 Reporter.log(" 'Snmpro' field is not available in 'Add CPE Device' page"); 

		 }
		}catch(Exception err) {
			err.printStackTrace();
		}
			
	}


public void device_MAcaddress(String macAdressInput) {

	boolean macAdres = false;
	try {
		macAdres = isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macaddress);
				
		if (macAdres) {
			
			Reporter.log(" 'MAC Address'  text field is displaying as expected");

			if (macAdressInput.equalsIgnoreCase("null")) {
				
				Reporter.log("No values has been passed for 'MAC Address' mandaotyr field");

			} else {

				 verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macaddress,"Add siteorder XNGcitycode");	
				 sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macaddress,macAdressInput,"Add siteorder XNGcitycode");
				
				waitforPagetobeenable();

				Reporter.log(macAdressInput + " is entered under 'MAc Address' text field");

			}
		}
	}  catch (Exception err) {
		err.printStackTrace();
		Reporter.log("Not able to enter value inside the 'MAC Address' text field");
	}
}

public void device_mediaSelection(String Mediaselection[], String mediaSelection)
		throws InterruptedException, IOException {
	
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mediaselection,"Media Selection");
	select(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mediaselection,mediaSelection);	
}

public void device_country( String country) throws InterruptedException, IOException {

	boolean countryDrpodown = false;
 
		// Country dropdown
	countryDrpodown= isClickable(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_countrydiv);
		
		waitforPagetobeenable();
	

	if (countryDrpodown) {
		 Reporter.log(" 'Country' dropodwn is displaying as expected");

		// verify dropdown values
		 
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_countrydiv);
		
		waitforPagetobeenable();
		Reporter.log("Clicked on Country dropdown");
		Reporter.log("Clicked on Country dropdown");

		List<WebElement> cntrylist = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
		for (WebElement countrylist : cntrylist) {

			Reporter.log("Available Country name is : " + countrylist.getText().toString());
					}

		// Select value inside country dropdown
		try {
			if (country.equalsIgnoreCase("null")) {
				Reporter.log(
						" 'Country' dropdown is a mandatory field and no values has been provided as input");

			} else {
				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_countrydiv);
			waitforPagetobeenable();
				click(Lanlink_Outbandmanagement.Outbandmanagement.country1+ country +Lanlink_Outbandmanagement.Outbandmanagement.country2);
				Thread.sleep(3000);
				Reporter.log(
						country + " is the value passed for 'Country' field for adding device");
			}

		}  catch (Exception er) {
			er.printStackTrace();
			Reporter.log("Not able to select value inside country dropdown");

		}
	} else {
		Reporter.log("Coutry dropodwn is not displaying");
	}
}

public void newcity(String newcityselectionmode, String cityname, String citycode)
		throws InterruptedException, IOException {

	// New City
	if (newcityselectionmode.equalsIgnoreCase("yes")) {

		if (cityname.equalsIgnoreCase("null")) {
			Reporter.log(" value for City name field  is not added");
		} else {
			// City Name Field
			sendKeys(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_citynamefield
					,cityname);
			waitforPagetobeenable();
		}

		if (citycode.equalsIgnoreCase("null")) {
			Reporter.log(" value for City Code field  is not added");
		} else {

			// City Code Field
			sendKeys(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_citycodefield
					,citycode);
waitforPagetobeenable();


		}

	} else {
		Reporter.log(" Add new city is not selected");
	}

}

public void newSite(String newsiteselectionmode, String sitename, String sitecode)
		throws InterruptedException, IOException {

	// New City
	if (newsiteselectionmode.equalsIgnoreCase("yes")) {

		if (sitename.equalsIgnoreCase("null")) {
			Reporter.log("value for Site name field  is not entered");
		} else {
			// City Name Field
			sendKeys(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_sitenamefield
					,sitename);
			waitforPagetobeenable();
		}

		if (sitecode.equalsIgnoreCase("null")) {
			Reporter.log(" value for City Code field  is not added");
		} else {

			// City Code Field
			sendKeys(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_sitecodefield
					,sitecode);
waitforPagetobeenable();


		}

	} else {
		Reporter.log("Add new city is not selected");
	}

}

public void newPremise(String newpremiseselectionmode, String premisename, String premisecode)
		throws InterruptedException, IOException {

	// New City
	if (newpremiseselectionmode.equalsIgnoreCase("yes")) {

		if (premisename.equalsIgnoreCase("null")) {
			Reporter.log(" value for Premise Name field  is not entered");
		} else {
			// City Name Field
			sendKeys(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisenamefield
					,premisename);
			waitforPagetobeenable();
		}

		if (premisecode.equalsIgnoreCase("null")) {
			Reporter.log(" value for Premise Code field  is not added");
		} else {

			// City Code Field
			sendKeys(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisecodefield
					,premisecode);
waitforPagetobeenable();
		}

	} else {
		Reporter.log("Add new Premise is not selected");
	}
	
}

public void existingCity(String existingcityselectionmode, String city)
		throws InterruptedException, IOException {

	// New City
	if (existingcityselectionmode.equalsIgnoreCase("yes")) {

		if (city.equalsIgnoreCase("null")) {
			Reporter.log( " No values selected under City dropdown");
		} else {
			// City Name Field
			click(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip__citydiv,"Clicked on City dropdown");
			waitforPagetobeenable();
		
			// click on City
			String cty= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.city2+ city +Lanlink_Outbandmanagement.Outbandmanagement.city2);
			Reporter.log("Selected City dropdowm is : " + cty);

			click(Lanlink_Outbandmanagement.Outbandmanagement.city2+ city +Lanlink_Outbandmanagement.Outbandmanagement.city2);
			waitforPagetobeenable();
		
		}
	
}
 else 
 {
		Reporter.log("Existing city value is not selected");
}

}

public void existingSite(String existingsiteselectionmode, String site)
		throws InterruptedException, IOException {

	// New City
	if (existingsiteselectionmode.equalsIgnoreCase("yes")) {

		if (site.equalsIgnoreCase("null")) {
			Reporter.log( " No values selected under City dropdown");
		} else {
			// City Name Field
			click(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_sitediv,"Clicked on Site dropdown");
			waitforPagetobeenable();
		
			// click on City
			String cty= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.city2+ site +Lanlink_Outbandmanagement.Outbandmanagement.city2);
			Reporter.log("Selected City dropdowm is : " + cty);

			click(Lanlink_Outbandmanagement.Outbandmanagement.city2+ site +Lanlink_Outbandmanagement.Outbandmanagement.city2);
			waitforPagetobeenable();
		
		}
	
}
 else 
 {
		Reporter.log("Existing Site value is not selected");
}

}

public void existingPremise(String existingpremiseselectionmode, String premise)
		throws InterruptedException, IOException {

	// New City
	if (existingpremiseselectionmode.equalsIgnoreCase("yes")) {

		if (premise.equalsIgnoreCase("null")) {
			Reporter.log( " No values selected under premise dropdown");
		} else {
			// City Name Field
			click(
					Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisediv,"Clicked on Premise dropdown");
			waitforPagetobeenable();
		
			// click on City
			String cty= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.city2+ premise +Lanlink_Outbandmanagement.Outbandmanagement.city2);
			Reporter.log("Selected City dropdowm is : " + cty);

			click(Lanlink_Outbandmanagement.Outbandmanagement.city2+ premise +Lanlink_Outbandmanagement.Outbandmanagement.city2);
			waitforPagetobeenable();
		
		}
	
}
 else 
 {
		Reporter.log("Existing premise value is not selected");
}

}

public void device_nameField(String cpename, String expectedDeviceNameFieldAutopopulatedValue) {
	boolean name = false;
	try {
		name = isVisible(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_Name);
		if (name) {
			if (cpename.equalsIgnoreCase("null")) {
				Reporter.log("No values has been assed for 'Name' text field");
				
			}

			else {
				
				clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_Name);
                waitforPagetobeenable();
                sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_Name,cpename);
                
                waitforPagetobeenable();
				
                Reporter.log(
						cpename + " is the value passed for Mandatory 'Cpe name' text field");
			}
		} else {
			Reporter.log(
					" Name text field is not displaying under 'Add CPE Device' page");
		}
	}  catch (Exception err) {
		err.printStackTrace();
		Reporter.log("Not able to enter value inside the 'Name' field");
	}

}


public void addsiteorder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String interfaceSpeed= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
	String country= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "country");
	String city= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "city");
	String CSR_Name= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CSR_Name");
	String site= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "sitevalue");
	String performReport= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "performReport");
	String ProactiveMonitor= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Proactivemonitor");
	String smartmonitor= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "smartmonitor");
	String technology= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
	String siteallias= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteallias");
	String VLANid= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "VLANid");
	String DCAenabledsite= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "DCAenabledsite");
	String cloudserviceprovider= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cloudserviceprovider");
	String sitevalue= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existing_SiteOrdervalue");
	String remark= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteorder_Remark");
	String xngcityname= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xngcityname");
	String xngcitycode= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "xngciycode");
	String devicename= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicenameForaddsiteorder");
	String nonterminatepoinr= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "nonterminationpoint");
	String Protected= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "protectforaddsiteorder");
	String newcityselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newcity");
	String existingcityselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingcity");
	String existingsiteselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingsite");
	String newsiteselection= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "newsite");
	
	
	waitforPagetobeenable();

	Reporter.log("addSiteOrder");

	addSiteOrderValues_OnnetOffnet(interfaceSpeed, country, city, CSR_Name, site, performReport,
				ProactiveMonitor, smartmonitor, technology, siteallias, VLANid, DCAenabledsite,
				cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename, nonterminatepoinr,
				Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection);

	
		
	}



public void addSiteOrderValues_OnnetOffnet(String interfaceSpeed, String country, String city,
		String CSR_Name, String site, String performReport, String ProactiveMonitor, String smartmonitor,
		String technology, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
		String sitevalue, String remark, String xngcityname, String xngcitycode, String devicename,
		String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
		String existingsiteselection, String newsiteselection)
		throws InterruptedException, IOException {

	
	Countyr_AddSiteOrder(country);

	City_AddSiteorder(existingcityselection, city, newcityselection, xngcityname, xngcitycode,
			sitevalue, CSR_Name, existingsiteselection, newsiteselection);


	// scroll to bottom
	
	waitforPagetobeenable();

	performancereporting_AddSiteOrder(performReport);

	proactiveMonitoring_AddSiteOrder(ProactiveMonitor);

	smartsMonitoring_AddSiteOrder(smartmonitor);

	SiteAlias_AddSiteOrder(siteallias);

	VLANid_AddSiteOrder(VLANid);

	DCAEnabledSite_AddSiteOrder(DCAenabledsite, cloudserviceprovider);

	remark_AddSiteOrder(remark);

	technologyP2P_AddSiteOrder(technology, interfaceSpeed, devicename, nonterminatepoinr, Protected);

	waitforPagetobeenable();
	

	click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);

}

public void SiteAlias_AddSiteOrder(String siteallias)throws InterruptedException, IOException {
 	 
	if(siteallias.equalsIgnoreCase("null")) {
		
		Reporter.log("No values entered for 'Site Alias' field");
	}else {
		try {
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias,
					siteallias);
			waitforPagetobeenable();

			String actualvalue =  getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias,"value");
			Reporter.log(actualvalue + " is entered under 'Site Alias' field");
		}  catch (Exception err) {
			err.printStackTrace();
			Reporter.log(" Not able to enter value under 'Site Alias' field");
		}
	}

}

public void Countyr_AddSiteOrder(String country)
		throws InterruptedException, IOException
{

	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Country,"country");
	addDropdownValues_commonMethod( "Device Country",Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Country,country);
	
}


public void Verifyfields(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException 
		{
	
	 String ServiceTypeToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");

	
	waitToPageLoad();
						
	waitforPagetobeenable();	
		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	
	click(Lanlink_Outbandmanagement.Outbandmanagement.Next_Button,"Next");
	Reporter.log("clicked on next button to verify the mandatory fields error messages");
	
	
	//Create Order/Contract Number Error message
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.order_contractnumberErrmsg, "Order/Contract Number");
	
	//Service Type Error message
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.servicetypeerrmsg, "Service Type");
				
		
			String[] Servicetypelists = { "BM (Broadcast Media)", "Domain Management", "DSL", "FAX to Mail", "HSS",
					"IP Access (On-net/Offnet/EoS)", "IP Access Bundle", "IP Transit", "IP VPN", "IP Web/Mail", "LANLink",
					"MDF/MVF/DI", "NGIN", "Number Hosting", "Transmission Link", "Voice Line (V)", "VOIP Access",
					"Wholesale SIP Trunking" };
	
			Reporter.log("order dropdown");
			
		//check whether Order dropdown is displayed	
			boolean orderdopdown = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.orderdropdown);
		//	sa.assertTrue(orderdopdown, "Order/Contract Number dropdown is not displayed");
			Reporter.log("order dropdown field is verified");
			
			
		//Select value under 'Service Type' dropdown
			addDropdownValues_commonMethod("Service Type",Lanlink_Outbandmanagement.Outbandmanagement.servicetypedropdowntoclick, ServiceTypeToBeSelected);
			
			
	   		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			
		//Click on next button to check mandatory messages
			click(Lanlink_Outbandmanagement.Outbandmanagement.Next_Button,"Next");

			waitforPagetobeenable();
			
			Reporter.log("clicked on next button to verify the mandatory fields error messages");
	
			
		//Interface Speed Error message	
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.interfaceSpeedErrmsg, "Interface Speed");
			
			
		//Service Sub Type Error message
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.servicesubtypeErrMsg, "Service Subtype");
	
			
		//Modular msp checkbox	
			boolean modularmspCheckbox = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.modularmspcheckbox);
					
		//	sa.assertTrue(modularmspCheckbox, "modularmsp checkbox is displayed");
	
		//AutoCreate checkbox	
			boolean autocreateservicecheckbox = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AutocreateServicecheckbox);
		//	sa.assertTrue(autocreateservicecheckbox, "Auto create check box is not displayed");
			
		
		for(int i=0; i<4; i++) 
		{
			
			if(i==0) {
				
					verifyinterfaceSpeeddropdown();
	
					verifyservicesubtypesdropdownwhenMSPandAutoCreatenotslected();
	
					verifyavailablecircuitdropdown();
	
			}
		
			
			else if(i==1) {
				
				
	
					click(Lanlink_Outbandmanagement.Outbandmanagement.modularmspcheckbox, "Modular MSP checkbox");
	
					verifyservicesubtypesdropdownwhenMSPaloneselected();
	
					verifyavailablecircuitdropdown();
				
			}
	
	
			else if(i==2) {
				
				
					
					click(Lanlink_Outbandmanagement.Outbandmanagement.modularmspcheckbox,"Modular MSP checkbox");
							waitforPagetobeenable();
					
					click(Lanlink_Outbandmanagement.Outbandmanagement.AutocreateServicecheckbox, "AutoCreate Service");
							waitforPagetobeenable();
	
					verifyA_Endtechnologydropdown();
	
					verifyB_Endtechnologydropdowb();
	
					verifyinterfaceSpeeddropdown();
	
					verifyservicesubtypesdropdownwgenAutoCreatealoneselected();
	
					verifyavailablecircuitdropdown();
	
			}
			else if(i==3) {
				
				
				
					click(Lanlink_Outbandmanagement.Outbandmanagement.modularmspcheckbox,"Modular MSP checkbox");
					waitforPagetobeenable();
					
					verifyA_Endtechnologydropdown();
	
					verifyB_Endtechnologydropdowb();
	
					verifyservicesubtypesdropdownwhenMSPandAutoCreateselected();
	
					verifyavailablecircuitdropdown();
			}
		}
		
			
		}
		
		
		
public void selectServiceType(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
	String serviceTypeToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");

	addDropdownValues_commonMethod("Service Type", Lanlink_Outbandmanagement.Outbandmanagement.servicetypedropdowntoclick, serviceTypeToBeSelected);


}



public void VerifyFieldsForServiceSubTypeSelected(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String serviceType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ServiceType");
	String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Servicesubtype");
	String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
	String proActivemonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Notificationmanagement");
	String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
	


	if (modularmsp.equalsIgnoreCase("no")) {
		if (Interfacespeed.equalsIgnoreCase("10GigE")) {
			Fieldvalidation_DirectFibre10G(serviceType, SelectSubService, Interfacespeed);

		} else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
			Fieldvalidation_DirectFibre1G( serviceType, SelectSubService, Interfacespeed );
		}
	}
	else if (modularmsp.equalsIgnoreCase("yes")) {
		ExtentTestManager.getTest().log(LogStatus.FAIL,
				" 'lanlink Outband Management' service will not occur when 'Modular Msp'checkbox is selected is selected");
	}

}



public void selectOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException

{
String neworderSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "neworderSelection");
String neworderno=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "existingorderdropdown");
String existingordernumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NewOrderNumber");
String existingorderSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ExistingOrderSelection");


waitToPageLoad();

//Create New order or select Existing Order	
((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

if (neworderSelection.equalsIgnoreCase("YES")) {
((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

waitforPagetobeenable();

click(Lanlink_Outbandmanagement.Outbandmanagement.selectorderswitch,"select order switch");
click(Lanlink_Outbandmanagement.Outbandmanagement.existingorderdropdown, neworderno);
} 

else if (existingorderSelection.equalsIgnoreCase("YES")) {

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

click(Lanlink_Outbandmanagement.Outbandmanagement.selectorderswitch,"select order switch");
addDropdownValues_commonMethod("Order/Contract Number(Parent SID)",Lanlink_Outbandmanagement.Outbandmanagement.existingorderdropdown, existingordernumber);
Reporter.log("=== Order Contract Number selected===");

waitforPagetobeenable();

} else {
Reporter.log("Order not selected");
}
}





public void dataToBeEnteredOncesubservicesselected(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
{
String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modularmsp");
String ServiceIdentificationNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
String EndpointCPE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"endpointCPE");
String Email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"email");
String PhoneContact=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"phone");
String remark=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"remark");
String PerformanceReporting=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"performanceReportngForServices");
String ProactiveMonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"proactiveMonitor");
String deliveryChannel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"deliveryChannel");
String Manageconnectiondropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"manageConnectiondropdown");
String vpnTopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vpnTopology");
String intermediateTechnology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"intermediateTechnology");
String CircuitReference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitReference");
String CircuitType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitType");
String notificationManagement=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Notificationmanagement");
String ENNIcheckBox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ENNIcheckbox");
String perCocPerfrmReprt=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerCoSpreformancereporting_serviceCreation");
String actelsBased=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ActelisBased_servicecreation");
String standrdCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardCIR_ServiceCreation");
String standrdEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardEIR_ServiceCreation");
String prmiumCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumCIR_ServiceCreation");
String prmiumEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumEIR_ServiceCreation");



if(modularmsp.equalsIgnoreCase("no")) {	

if (Interfacespeed.equalsIgnoreCase("10GigE")) {
	DirectFibre_10G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE,
			Email, PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
			Manageconnectiondropdown, vpnTopology, intermediateTechnology, CircuitReference, CircuitType,
			notificationManagement, ENNIcheckBox);
}

else if (Interfacespeed.equalsIgnoreCase("1GigE")) {
	DirectFibre_1G(ServiceIdentificationNumber, SelectSubService, Interfacespeed, EndpointCPE,
			Email, PhoneContact, remark, PerformanceReporting, ProactiveMonitoring, deliveryChannel,
			Manageconnectiondropdown, vpnTopology, intermediateTechnology, CircuitReference, CircuitType,
			notificationManagement, perCocPerfrmReprt, actelsBased, standrdCir, standrdEir, prmiumCir,
			prmiumEir, ENNIcheckBox);
}
}


else if (modularmsp.equalsIgnoreCase("yes")) {

	ExtentTestManager.getTest().log(LogStatus.FAIL,
			"When Modular MSP checkbox is selected, 'Lanlink Outband Management' service will not display");
	waitforPagetobeenable();


}
}    

public void VerifydatenteredForServiceSubTypeSelected(String testDataFile,String sheetName,String scriptNo,String dataSetNo)
		throws InterruptedException, IOException {
	
	String serviceType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ServiceType");
	String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
	String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
	String ServiceIdentificationNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"serviceNumber");
	String EndpointCPE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"endpointCPE");
	String Email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"email");
	String PhoneContact=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"phone");
	String remark=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"remark");
	String PerformanceMonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerformMonitor");
	String ProactiveMonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"proactiveMonitor");
	String deliveryChannel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"deliveryChannel");
	String ManagementOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ManagementOrder");
	String vpnTopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"vpnTopology");
	String intermediateTechnology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"intermediateTechnology");
	String CircuitReference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitReference");
	String CircuitType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CircuitType");
	String AggregateTraffic=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AggregateTraffic");
	String DeliveryChannelForselecttag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Deliverychanneforselecttag");
	String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modularmsp");
	String autoCreateService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AutocreateService");
	String ENNIcheckBox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ENNIcheckbox");

	String Manageconnectiondropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"manageConnectiondropdown");
	String A_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A_Endtechnology");
	String B_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"B_Endtechnology");
	String Performancereporting=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"performanceReportngForServices");
	String perCocPerfrmReprt=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"PerCoSpreformancereporting_serviceCreation");
	String actelsBased=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ActelisBased_servicecreation");
	String standrdCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardCIR_ServiceCreation");
	String standrdEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"StandardEIR_ServiceCreation");
	String prmiumCir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumCIR_ServiceCreation");
	String prmiumEir=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"premiumEIR_ServiceCreation");
	String notificationManagement=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Notificationmanagement");

	

	waitforPagetobeenable();
	try {

		if (modularmsp.equalsIgnoreCase("no")) {

			if (Interfacespeed.equalsIgnoreCase("10GigE")) {

				verifydataEntered_DirectFibre10G(serviceType, ServiceIdentificationNumber,
						SelectSubService, Interfacespeed, EndpointCPE, Email, PhoneContact, remark,
						PerformanceMonitoring, ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology,
						intermediateTechnology, CircuitReference, CircuitType, modularmsp, notificationManagement,
						Manageconnectiondropdown, ENNIcheckBox);

			}

			else if (Interfacespeed.equalsIgnoreCase("1GigE")) {

				verifydataEntered_DirectFibre1G(serviceType, ServiceIdentificationNumber,
						SelectSubService, Interfacespeed, EndpointCPE, Email, PhoneContact, remark,
						PerformanceMonitoring, ProactiveMonitoring, deliveryChannel, ManagementOrder, vpnTopology,
						intermediateTechnology, CircuitReference, CircuitType, modularmsp, perCocPerfrmReprt,
						actelsBased, standrdCir, standrdEir, prmiumCir, prmiumEir, notificationManagement,
						Manageconnectiondropdown, ENNIcheckBox);

			}
		} else {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					" 'Lanlink OutbandManagement' service will not occur when 'modular MSP' is selected");
		}

		

	} catch (AssertionError e) {
		Reporter.log("validation failed for verify Direct Fiber service subtype page ");
		e.printStackTrace();
	}
}



public void EditTheservicesselected(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Servicesubtype");
	String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Interfacespeed");
	String ServiceIdentificationNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_serviceNumber");
	String EndpointCPE=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_endpointCPE ");
	String Email=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_email ");
	String PhoneContact=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_phone");
	String remark=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_remark");
	String PerformanceReporting=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_PerformMonitor ");
	String ProactiveMonitoring=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_proactiveMonitor ");
	String deliveryChannel=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_deliveryChannel ");
	String ManagementOrder=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_ManagementOrder ");
	String intermediateTechnology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_intermediateTechnology");
	String CircuitReference=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_CircuitReference ");
	String CircuitType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_CircuitType ");
	String AggregateTraffic=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_AggregateTraffic ");
	String DeliveryChannelForselecttag=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Deliverychannelforselecttag");
	String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Modularmsp");
	String autoCreateService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AutocreateService ");
	String ENNIcheckBox=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_ENNI ");
	String Manageconnectiondropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_manageConnectiondropdown ");
	String A_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_A_Endtechnology ");
	String B_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Edit_B_Endtechnology ");
	String notificationManagement=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_NotificationManagement ");
	String perCoSperformanceReport=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_perCoSperformanceReport ");
	String actelisBased=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_actelisBased ");
	String standardCIR=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_standardCIR ");
	String standardEIR=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_standardEIR ");
	String premiumCIR=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_premiumCIR  ");
	String premiumEIR=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"EditService_premiumEIR");


//	WebElement orderPanel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_actiondropdown);
	waitforPagetobeenable();

	click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_actiondropdown, "Action_dropdown");
	
	click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_Editlink, "Edit");
	waitforPagetobeenable();
	
	
	waitforPagetobeenable();
	
	scrollUp();
	waitforPagetobeenable();
	
	if (modularmsp.equalsIgnoreCase("no")) {

		if (Interfacespeed.equalsIgnoreCase("10GigE")) {

			Edit_DirectFibre10G(ServiceIdentificationNumber, SelectSubService, Interfacespeed,
					EndpointCPE, Email, PhoneContact, remark, PerformanceReporting, ProactiveMonitoring,
					deliveryChannel, ManagementOrder, intermediateTechnology, CircuitReference,
					notificationManagement, Manageconnectiondropdown,
					ENNIcheckBox);

		}

		else if (Interfacespeed.equalsIgnoreCase("1GigE")) {

			Edit_DirectFibre1G(ServiceIdentificationNumber, SelectSubService, Interfacespeed,
					EndpointCPE, Email, PhoneContact, remark, PerformanceReporting, ProactiveMonitoring,
					deliveryChannel, ManagementOrder, intermediateTechnology, CircuitReference,
					notificationManagement, perCoSperformanceReport, actelisBased,
					standardCIR, standardEIR, premiumCIR, premiumEIR, Manageconnectiondropdown, ENNIcheckBox);

			ExtentTestManager.getTest().log(LogStatus.PASS, "Values has been Edited");
			Reporter.log("Values has been Edited");

		}
	} else {
		ExtentTestManager.getTest().log(LogStatus.FAIL,
				"Edit Service for 'Lanlink OutbandManagement' cannot be performed as 'modular MSP' checkbox is selected");
		Reporter.log("Edit Service for 'Lanlink OutbandManagement' cannot be performed as 'modular MSP' checkbox is selected");
	}

	waitforPagetobeenable();

}



public void syncservices() throws IOException, InterruptedException 
{

//String orderPanel= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
waitforPagetobeenable();

click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_actiondropdown,"Action");
waitforPagetobeenable();
click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_sysnchronizelink,"Synchronize link");
waitforPagetobeenable();

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

waitforPagetobeenable();

boolean syncSuccessMessage=isElementPresent(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.alertForSynchronize));
if(syncSuccessMessage)
{

String actualmsg=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.alertMSG_synchronize);
Reporter.log( " success message for synchronize displays as: "+actualmsg);

}else {
Reporter.log(" Success message did not display after clicking on 'Synchronize' button");
}
}



public void shownewInfovista() throws IOException, InterruptedException 
{

///String orderPanel= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
waitforPagetobeenable();

click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_actiondropdown,"Action");
waitforPagetobeenable();
click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_infovistareport,"Show infovista link");
waitforPagetobeenable();

String expectedPageName= "SSO login Page";

//Switch to new tab
List<String> browserTabs = new ArrayList<String> (webDriver.getWindowHandles());
webDriver.switchTo().window(browserTabs .get(1));
waitforPagetobeenable();

try { 
// Get Tab name
String pageTitle=webDriver.switchTo().window(browserTabs .get(1)).getTitle();
Reporter.log("page title displays as: "+pageTitle);


waitforPagetobeenable();
webDriver.switchTo().window(browserTabs.get(0)); 

//sa.assertEquals(pageTitle, expectedPageName, " on clicking 'Show Infovista link', it got naviagted to "+pageTitle);

//sa.assertAll();

waitforPagetobeenable();

}catch(AssertionError e) {

e.printStackTrace();

waitforPagetobeenable();
webDriver.switchTo().window(browserTabs.get(0));


}

}



public void manageSubnets() throws IOException, InterruptedException  {

//String orderPanel= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
waitforPagetobeenable();

click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_actiondropdown,"Edit service action drop down");
waitforPagetobeenable();
click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_managesubnets,"Manage Subnet");
waitforPagetobeenable();

boolean manageSubnetPage=false;
try { 
manageSubnetPage=isElementPresent(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.manageSubnetPage_header));
if(manageSubnetPage) {
Reporter.log("'Manage Subnet' page is displaying");

String errMsg=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.manageSubnet_errMsg);
if(errMsg.isEmpty()) {

Reporter.log("No messages displays under 'manage Subnet' page");
}
else
{
Reporter.log(" Message in 'Manage Subnet' page displays as "+errMsg);
}

}else
{
Reporter.log("'Manage Subnet' page is not displaying");
}
}
catch(Exception e)
{
e.printStackTrace();
Reporter.log("'Manage Subnet' page is not displaying");
}

click(Lanlink_Outbandmanagement.Outbandmanagement.cancelButton,"Cancel"); 
waitforPagetobeenable();

}



public void dump_viewServicepage() throws IOException, InterruptedException 
{

//String orderPanel= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
waitforPagetobeenable();

click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_actiondropdown,"Action");
waitforPagetobeenable();
click(Lanlink_Outbandmanagement.Outbandmanagement.Editservice_Dumplink, "Dump");
waitforPagetobeenable();	   

waitToPageLoad();

String dumpelement= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.dump_container);

String dumpvalue=dumpelement;

if(dumpvalue.isEmpty()) {

Reporter.log("NO values dipslaying under 'Dump' page");

}else{
Reporter.log("Dump value is displaying as:   "+ dumpvalue);
}

webDriver.navigate().back();
waitforPagetobeenable();

}



public void verifyAddsiteorderFields(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");


	ExtentTestManager.getTest().log(LogStatus.INFO, "verifying field under 'Add Site order' page");

	verifySiteOrderFields_OnnetOffnet( interfaceSpeed);

}



public void VerifyDataEnteredForSiteOrder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
	String VPNtopology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "vpnTopology");
	String circuitType = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CircuitType");
	String country = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "country");
	String city = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "city");
	String CSR_Name = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "CSR_Name");
	String site = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existing_SiteOrdervalue");
	String performReport = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "performReport");
	String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Proactivemonitor");
	String smartmonitor = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "smartmonitor");
	String siteallias = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteallias");
	String VLANid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VLANid");
	String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DCAenabledsite");
	String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cloudserviceprovider");
	String sitevalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "sitevalue");
	String remark = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteorder_Remark");
	String xngcityname = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "xngcityname");
	String xngcitycode = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "xngciycode");
	String devicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "devicenameForaddsiteorder");
	String nonterminatepoinr = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "nonterminationpoint");
	String Protected = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "protectforaddsiteorder");
	String newcityselection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newcity");
	String existingcityselection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingcity");
	String existingsiteselection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "existingsite");
	String newsiteselection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "newsite");
	String technology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "technology");


		VerifyDataEnteredForSiteOrder_viewSiteOrder_P2P(country, city, CSR_Name, site, performReport,
				ProactiveMonitor, smartmonitor, technology, siteallias, VLANid, DCAenabledsite,
				cloudserviceprovider, sitevalue, remark, xngcityname, xngcitycode, devicename, nonterminatepoinr,
				Protected, newcityselection, existingcityselection, existingsiteselection, newsiteselection);

	
}



public void returnbacktoviewsiteorderpage() throws IOException, InterruptedException 
{
	

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

waitforPagetobeenable();
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Backbutton,"Back Button");
click(Lanlink_Outbandmanagement.Outbandmanagement.Backbutton,"Back Button");
waitforPagetobeenable();
}

public void selectRowForsiteorderOutband(String testDataFile,String dataSheet,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	String siteordernumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "siteOrderNumber_OnnetOffnet");


	Reporter.log("-----------------------------" + siteordernumber + "---------------------");
	//int TotalPages;

	
	waitforPagetobeenable();
	
	
//	scrolltoend();
	waitforPagetobeenable();
//WebElement web=getwebelement(xml.getlocator("//locators/" + Application + "/TotalPagesforsiteorder"));
//Log.info(" webelement name for total page: "+ web);
//	String TextKeyword = getwebelement(xml.getlocator("//locators/" + Application + "/TotalPagesforsiteorder")).getText();
//
//	TotalPages = Integer.parseInt(TextKeyword);
//
//	Log.info("Total number of pages in table is: " + TotalPages);
//
//	ab:
//
//	for (int k = 1; k <= TotalPages; k++) {
//
//		// Current page
//		String CurrentPage = Gettext(getwebelement(xml.getlocator("//locators/" + Application + "/Currentpageforsiteorderfunc")));
//		int Current_page = Integer.parseInt(CurrentPage);
//		Log.info("The current page is: " + Current_page);
//
//		assertEquals(k, Current_page);
//
//		Log.info("Currently we are in page number: " + Current_page);

	// div[div[contains(text(),'" + siteOrdernumber_P2P +
	// "')]]//span[@class='ag-icon ag-icon-checkbox-unchecked'] --> needs to be
	// updated
	List<WebElement> results = null;

		results = webDriver.findElements(By.xpath("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteordernumber + "']"));

	int numofrows = results.size();
	Reporter.log("no of results: " + numofrows);
	boolean resultflag;
//
//		if (numofrows == 0) {
//
////			PageNavigation_NextPage(Application);
//			Clickon(getwebelement(xml.getlocator("//locators/" + Application + "/pagenavigationfornextpage_underviewserviceforsiteorder")));
//			
//
//		}
//
//		else {
//
//			for (int i = 0; i < numofrows; i++) {
//
//				try {

	resultflag = results.get(0).isDisplayed();
	Reporter.log("status of result: " + resultflag);
	if (resultflag) {
		Reporter.log(results.get(0).getText());
		results.get(0).click();
		waitforPagetobeenable();
		click(Lanlink_Outbandmanagement.Outbandmanagement.Actiondropdown_siteorder,"Actiondropdown_siteorder");

		waitforPagetobeenable();

	}
//
//				} catch (StaleElementReferenceException e) {
//					// TODO Auto-generated catch block
//					// e.printStackTrace();
//					results = driver.findElements(
//							By.xpath("//div[div[contains(text(),'" + siteordernumber + "')]]//span[@class='ag-icon ag-icon-checkbox-unchecked']"));
//					numofrows = results.size();
//					// results.get(i).click();
//					Log.info("selected row is : " + i);
//				}
//			}
//			break ab;
//		}
//	}
}



public void selectRowForsiteorder(String testDataFile,String sheetName,String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	String siteordernumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siteordernumber");
	String siteOrdernumber_P2P=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber_PointToPoint");
	String topology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
	String interfaceSpeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
	String siteOrderNumber_10G_p2p=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "siteOrderNumber_10G_PointToPoint");
	


	 
	waitforPagetobeenable();
	
	Reporter.log("-----------------------------" + siteordernumber + "---------------------");
	int TotalPages;

	//scrolltoend();
	waitforPagetobeenable();
		List<WebElement> results = null;
		
		if(topology.equalsIgnoreCase("Point-to-Point")) {
			if(interfaceSpeed.equals("10GigE")) {
				results=webDriver.findElements(By.xpath("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteOrderNumber_10G_p2p + "']"));
			}else if(interfaceSpeed.equals("1GigE")) {
				results=webDriver.findElements(By.xpath("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteOrdernumber_P2P + "']"));
			}
				
		}else {
			results=webDriver.findElements(By.xpath("//div[div[text()='Site Orders']]//following-sibling::div//div[text()='"+ siteordernumber + "']"));
		}
	
		int numofrows = results.size();
		Reporter.log("no of results: " + numofrows);
		boolean resultflag;

					resultflag = results.get(0).isDisplayed();
					Reporter.log("status of result: " + resultflag);
					if (resultflag) {
						Reporter.log(results.get(0).getText());
						results.get(0).click();
						Thread.sleep(5000);
						click(Lanlink_Outbandmanagement.Outbandmanagement.Actiondropdown_siteorder, "Action");

						waitforPagetobeenable();
					}
		}


public void equipConfiguration_Actelis_AddDevice(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException 
{
String devicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");
String RouterId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
String Vendor_Model=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
String Snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
String ManagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management Address");
String MEPId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
String ETH_Port=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ETH Port");



((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

waitforPagetobeenable();
click(Lanlink_Outbandmanagement.Outbandmanagement.equipConfig_addCPEdevice);
waitforPagetobeenable();

boolean addActelisHeader=false; 
addActelisHeader=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.addActelisCPEpage_headerName);
if(addActelisHeader) {
Reporter.log(" 'Add Actelis CPE device' page is displaying as expected");

verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK");
click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK");
waitforPagetobeenable();

//Validate Warning Messages_Name Field
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.devicenameFieldErrMSg_addCPE_Actelis , "Name");

//Validate Warning Messages_Router ID Field
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.RouterIDFieldErrMSg_addCPE_Actelis,  "Router Id");

//Validate Warning Messages_Management Address Field 
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.manageAddressFieldErrMSg_addCPE_Actelis, "Management Address");

//Name 
sendKeys( Lanlink_Outbandmanagement.Outbandmanagement.nameField_addCPE_Actelis, devicename,"Name");

//vendor/Model
sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.vendorField_addCPE_Actelis, Vendor_Model,"Vendor/Model");

//Router Id
sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.RouterIdField_addCPE_Actelis, RouterId, "Router Id");

//Management Address
sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.managementAddressField_addCPE_Actelis, ManagementAddress,"Management Address");

//MEP Id
sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.MEPidField_addCPE_Actelis, MEPId,"MEP Id");

//ETH Port
sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.ETHportField_addCPE_Actelis, ETH_Port, "ETH Port");

//CAncel button 
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cancel,"Cancel");

verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK");
click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK");
Thread.sleep(4000);


}else {
//Add Actelis CPE device' page is not displaying
}
}



public void verifyDataEnteredFordeviceCreation_Actelis(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
{
String devicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Device");
String RouterId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router Id");
//	String Vendor_Model=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
String ManagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management Address");
String MEPId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
String ETH_Port=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ETHPort");



waitToPageLoad();

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

waitforPagetobeenable();

click(Lanlink_Outbandmanagement.Outbandmanagement.actelis_EquipConfig_viewLink,"view_Link");
waitforPagetobeenable();

verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+devicename+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Device name");
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+RouterId+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Router ID");
//   verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+Vendor_Model+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2);
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+ManagementAddress+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Management Address");
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+MEPId+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"MEPID");
verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+ETH_Port+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"ETH_Port");

}



public void deleteDeviceFromService_EquipmentConfig_Actelis() throws InterruptedException, IOException
{
waitToPageLoad();

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");


click(Lanlink_Outbandmanagement.Outbandmanagement.actelis_EquipConfig_deleteLink,"delete_link");

Reporter.log(" 'Delete From Service' link has been clicked for cpe device under 'Equipment Configuration' panel");


boolean deletemessage=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.deleteMessage_equipment);
while(deletemessage) {
Reporter.log("Are you sure want to delete - message is getting displayed on clicking DeletefromService link");
Reporter.log("Delete popup message is getting displayed");
String actualMessage=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.deleteMessage_equipment);
Reporter.log( "Delete device popup is displaying and popup message displays as: "+ actualMessage);
break;
} 


click(Lanlink_Outbandmanagement.Outbandmanagement.deletebutton_deletefromsrviceforequipment);
waitforPagetobeenable();
Reporter.log("Device got deleted from service as expected");

}



public void verifyAddDSLAMandHSLlink() throws InterruptedException
{

boolean actelisConfigurationPanel=false;


waitToPageLoad();

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");


actelisConfigurationPanel=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ActelisConfigurationPanel);

if(actelisConfigurationPanel) {
Reporter.log(" 'Actelis Configuration' panel is displaying as expected");

boolean actelisLink=false;
try {	
actelisLink=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Actelisconfig_addDSLAM);
if(actelisLink) {
	
	click(Lanlink_Outbandmanagement.Outbandmanagement.Actelisconfig_addDSLAM);
	waitforPagetobeenable();
	
}

}
catch(NoSuchElementException e) {
e.printStackTrace();
}catch(Exception err) {
err.printStackTrace();
}
}else {
//Actelis Configuration' panel is not displaying
}

}


public void AddDSLAMandHSL(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String DSLMdevice=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ActelisTech_DSLAMdevice");
	String HSlname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ActelisTech_DSLAMInterfacename");



waitToPageLoad();

click(Lanlink_Outbandmanagement.Outbandmanagement.addDSLAMandHSL_xButton);
Reporter.log("Clicked on 'X' button under 'DSLAM device' dropdown");

sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.DSLM_Device_Select,DSLMdevice,"DSLMdevice");

String valueToSElect=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.selectDSLAMdeviceValue).replace("value", DSLMdevice);

try {
if(isElementPresent(valueToSElect)) {
	Reporter.log(DSLMdevice + " is displaying under 'DSLAM device' dropdown");
	
	click(valueToSElect);
	Reporter.log(DSLMdevice + " is selected under 'DSLAM device' dropdown");
	
	
	
	waitToPageLoad();
	
	click(Lanlink_Outbandmanagement.Outbandmanagement.List_HSL_Link, "List_HSL");		//click on "List HSL" button

	selectRowForAddingInterface_Actelis(HSlname);		// select the Interface
	
}else {
	Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
}

}catch(Exception e){
e.printStackTrace();
Reporter.log(DSLMdevice + " is not displaying under 'DSLAM device' dropdown");
}				
}



public void showInterface_ActelisConfiguuration() throws IOException, InterruptedException  
{

click(Lanlink_Outbandmanagement.Outbandmanagement.showInterface_ActelisCnfiguration);
waitforPagetobeenable();
}



public void deletInterface_ActelisConfiguration(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
String interfaceName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "interfaceName");


//select the interface
click(Lanlink_Outbandmanagement.Outbandmanagement.deleteInterafce1+ interfaceName +Lanlink_Outbandmanagement.Outbandmanagement.deleteInterafce2);

//click on Action button
click(Lanlink_Outbandmanagement.Outbandmanagement.AcionButton_ActelisConfiguration);

//Remove Button
 click(Lanlink_Outbandmanagement.Outbandmanagement.removeButton_ActelisConfiguration);

boolean popupMessage=false;
popupMessage=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.popupMessage_forRemove_ActelisConfiguration);

if(popupMessage) {
String actualmsg=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.popupMessage_forRemove_ActelisConfiguration);
Reporter.log(" On clicking remoe button, popup message displays as: "+ actualmsg);

 
 click(Lanlink_Outbandmanagement.Outbandmanagement.deleteInterfaceX);
 waitforPagetobeenable();
}else {

//popup message does not display after clicking on 'Remove' button
}
}



public void successMessage_deleteInterfaceFromDevice_ActelisConfiguration() throws IOException 
{

boolean successMessage=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.successMessage_ActelisConfiguration_removeInterface);
String actualmessage=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.successMessage_ActelisConfiguration_removeInterface);
if(successMessage) {

Reporter.log( " Success Message for removing interface is dipslaying as expected");

Reporter.log("Message displays as: "+actualmessage);

}else {
Reporter.log( " Success Message for removing Interface is not dipslaying");
}
}



public boolean findPanelHeader(String devicePanelName) throws IOException, InterruptedException 
{
//String devicePanelName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicePanelName");


((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

WebElement el=null;
boolean panelheader= false;
try { 

	el=findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.devicePanelHeaders_InViewSiteOrderPage).replace("value", devicePanelName));
	panelheader=el.isDisplayed();

if(panelheader) {
Reporter.log(" 'Equipment' panel is displaying under 'view site order' page");
panelheader=true;

}else {
Reporter.log(" 'Equipment' panel is not displaying under 'view site order' page");
panelheader=false;

}}catch(NoSuchElementException e) {
e.printStackTrace();
Reporter.log(" 'Equipment' panel is not displaying under 'view site order' page");
 panelheader=false;

}

return  panelheader;
}


public void verifyValuesforCPEexistingdevice_1G_Equipment() throws InterruptedException, IOException	{
	   
	   fetchValueFromViewPage( "Name");
	   fetchValueFromViewPage( "Router Id");
	   fetchValueFromViewPage( "Vendor/Model");
	   fetchValueFromViewPage( "Snmpro");
	   fetchValueFromViewPage( "Management Address");
	   fetchValueFromViewPage( "MEP Id");
	   fetchValueFromViewPage( "Power Alarm");
	   fetchValueFromViewPage( "Media Selection");
	   fetchValueFromViewPage( "Link Lost Forwarding");
	   fetchValueFromViewPage( "MAC Address");
//	   fetchValueFromViewPage(application, "Serial Number");
}


public void eDITCPEdevicedetailsentered_1G(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {
	
	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpename");
	String vender=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_vender_1G");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_managementAddress");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mepid");
	String poweralarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_poweralarm_1G");
	String Mediaselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mediaselection");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Macaddress");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_serialNumber");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_linkLostForwarding");
	String createddevicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicenameforEquipment");
	String technologySelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
	

	ExtentTestManager.getTest().log(LogStatus.INFO, "edit CPE device under Equipment");

	Reporter.log("Entered edit functionalitty");

	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown);
	waitforPagetobeenable();

	click(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown);

	waitforPagetobeenable();

	click(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevicelinkunderviewpage);
	waitforPagetobeenable();
	Reporter.log("edit functionality worked");

	// Name field
	device_editnamefield( cpename);

	// vendor/model
	device_editVendorModelField( vender);

	// Snmpro
	device_editSnmproField();

	// Management address
	device_editManagementAddressField( managementAddress);

	// Mepid
	device_editMEPIdField( Mepid);

	scrollDown(poweralarm);
	waitforPagetobeenable();

	// power alarm
	device_editPowerAlarm(poweralarm);

	if (technologySelected.equalsIgnoreCase("Accedian-1G")) {

		// Serial Number
		device_editserialnumber(serialNumber);

	} else {

		// Media Selection
		device_editMediaselection(Mediaselection);

		Thread.sleep(3000);

		// Mac address
		device_editMACaddress(Macaddress);

		// linklost forwarding
		device_editlinkLostforwarding(linkLostForwarding);
	}

	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
	waitforPagetobeenable();

	click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton, "OK");
	waitforPagetobeenable();

}


public void verifyFieldsandAddCPEdevicefortheserviceselected_1G(String testDataFile, String sheetName,String  scriptNo,String  dataSetNo)
		throws InterruptedException, IOException {
	
	String interfaceSpeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicename_equip");
	String vender=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_vender_1G");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_managementAddress");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mepid");
	String poweralarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_poweralarm_1G");
	String mediaSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Mediaselection");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_Macaddress");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_serialNumber");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_linkLostForwarding");
	String newmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
	String existingmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_existingmanagementAddressSelection");
	String manageaddressdropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpe_manageaddressdropdownvalue");
	String technologySelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "technology");
	String vpntopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "vpnTopology");
	

	
	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_adddevicelink);
	waitforPagetobeenable();
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "Verifying fields for CPE device under Equipment");
	Reporter.log("Verifying fields for CPE device under Equipment");
	
	click(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_adddevicelink);
	waitforPagetobeenable();
	waitforPagetobeenable();
	
	if(technologySelected.equalsIgnoreCase("Accedian-1G")) {
		equip_adddevi_Accedian1G(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm, mediaSelection, 
			Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, newmanagementAddress, existingmanagementAddress,
			manageaddressdropdownvalue);
		
	}else {
		
		if(technologySelected.equalsIgnoreCase("Atrica") && vpntopology.equals("Hub&Spoke")) {
			selectTechnology_HubAndSpoke();
		}
		
	equip_addDevice_1G(interfaceSpeed, cpename, vender, snmpro, managementAddress, Mepid, poweralarm, mediaSelection, 
			Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, newmanagementAddress, existingmanagementAddress,
			manageaddressdropdownvalue);
	}
	
	}



public void verifydetailsEnteredforCPEdevice_1G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"devicename_equip");
	String vender=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_vender_1G");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_managementAddress");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_Mepid");
	String poweralarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_poweralarm_1G");
	String Mediaselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_Mediaselection");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_Macaddress");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_serialNumber");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_linkLostForwarding");
	String newmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_newmanagementAddressSelection");
	String existingmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_existingmanagementAddressSelection");
	String manageaddressdropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"cpe_manageaddressdropdownvalue");
	String technologySelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"technology");
	
	
	ExtentTestManager.getTest().log(LogStatus.INFO, "verify the details entered for creating device");
	Reporter.log("verify the details entered for creating device");

	clickOnBankPage();
	waitforPagetobeenable();
	
	//scrolltoend();
	waitforPagetobeenable();
	
    webDriver.findElement(By.xpath("//div[div[div[text()='Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();
    waitforPagetobeenable();
	
	
	String[] RouterId=new String[2];
	RouterId=cpename.split(".lanlink");
	
	String RouterIdValue=RouterId[0];
	
	
	String mediaSelectionValueInViewDevicePage="no";
	if(Mediaselection.equalsIgnoreCase("null")) {
		Mediaselection=mediaSelectionValueInViewDevicePage;
	}else {
		Mediaselection=mediaSelectionValueInViewDevicePage;
	}
  
  verifyEnteredvalues_deviceName("Name", RouterIdValue,cpename );
  
  verifyEnteredvalues("Router Id", RouterIdValue);
  
  verifyEnteredvalues("Vendor/Model", vender);
  
  verifyEnteredvalues("Snmpro", snmpro);
  
  
	//Management Address  
	  	if((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
		  verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
		 }
		 else if((existingmanagementAddress.equalsIgnoreCase("no")) && (newmanagementAddress.equalsIgnoreCase("Yes"))) {
			 verifyEnteredvalues("Management Address", managementAddress);
		 } 
  
//  verifyEnteredvalues("MEP Id", Mepid);
  
  verifyEnteredvalues("Power Alarm", poweralarm);
  
if(technologySelected.equalsIgnoreCase("Accedian-1G")) {  
	
  verifyEnteredvalues("Serial Number", serialNumber);
  
}else {
  
  verifyEnteredvalues("Media Selection", Mediaselection);
  
  verifyEnteredvalues("MAC Address", Macaddress);
}
  
  


}



public void verifyValuesforCPEexistingdevice_10G_Equipment()
		throws InterruptedException, IOException {

	fetchValueFromViewPage( "Name");
	fetchValueFromViewPage( "Router Id");
	fetchValueFromViewPage( "Vendor/Model");
	fetchValueFromViewPage( "Snmpro");
	fetchValueFromViewPage( "Management Address");
	fetchValueFromViewPage( "MEP Id");
	fetchValueFromViewPage( "Power Alarm");
	fetchValueFromViewPage( "Media Selection");
	fetchValueFromViewPage( "Link Lost Forwarding");
	fetchValueFromViewPage( "Serial Number");
}



public void eDITCPEdevicedetailsentered_10G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpename");
	String vender=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_vender_10G");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_managementAddress");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mepid");
	String poweralarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_poweralarm_10G");
	String Mediaselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Mediaselection");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_Macaddress");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_serialNumber");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_cpe_linkLostForwarding");
	String devicename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "devicenameforEquipment");
	


	ExtentTestManager.getTest().log(LogStatus.INFO, "edit CPE device under Equipment");

	Reporter.log("Entered edit functionalitty");

	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown);
	waitforPagetobeenable();
	click(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown);

	Thread.sleep(3000);

	click(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevicelinkunderviewpage);
	waitforPagetobeenable();
	Reporter.log("edit functionality worked");

	// Name field
	device_editnamefield(cpename);

	// vendor/model
	device_editVendorModelField(vender);

	// Snmpro
	device_editSnmproField();

	// Management address
	device_editManagementAddressField(managementAddress);

	// Mepid
	device_editMEPIdField(Mepid);

	// power alarm
	device_editPowerAlarm(poweralarm);

	// serial Number
	device_editserialnumber(serialNumber);

	// linklost forwarding
	device_editlinklostforwarding_10G();
	
	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);

	click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton, "OK");
	waitforPagetobeenable();

}



public void verifydetailsEnteredforCPEdevice_10G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException,  IOException {
	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "cpename");

	String Name=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Name");
	String Vendor_Model=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Vendor/Model");
	String Snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Management Address");
//	String MEPId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MEP Id");
	String PowerAlarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Power Alarm");
	String MediaselectionActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Media selection");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Serial Number");
String MACAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MAC Address");
String manageaddressdropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "manageaddressdropdownvalue");
String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Link Lost Forwarding");
String existingmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "manageaddressdropdownvalue");
String newmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "manageaddressdropdownvalue");


	
	clickOnBankPage();
	waitforPagetobeenable();
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();
	
	click(Lanlink_Outbandmanagement.Outbandmanagement.deviceView1+ cpename +Lanlink_Outbandmanagement.Outbandmanagement.deviceView2);
	waitforPagetobeenable();
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

	waitforPagetobeenable();
	
//Splitting device name	
	String[] RouterId=new String[2];
	RouterId=cpename.split(".lanlink");
	
	String RouterIdValue=RouterId[0];
	
	String Mediaselection="no";
	String LinkLostForwarding="yes";
	
	 verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+Name+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Name");
  	   verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+RouterId+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Router ID");
  	   verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+Vendor_Model+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Vendor model");
  	   verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+Snmpro+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Snmpro");
  	   
  
//Management Address  
  	if((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
	  verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+manageaddressdropdownvalue+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Address dropdown");
	 }
	 else if((existingmanagementAddress.equalsIgnoreCase("no")) && (newmanagementAddress.equalsIgnoreCase("Yes"))) {
		 verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+managementAddress+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Address dropdown");
	 } 
  
  	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+PowerAlarm+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Power alaram");	   

  
  	   verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+MediaselectionActualValue+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"Media selection Actual Value");

  	   verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+linkLostForwarding+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"linkLostForwarding");


  
	   verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPage1+serialNumber+Lanlink_Outbandmanagement.Outbandmanagement.viewPage2,"serialNumber");

}


public void addDevice_IntEquipment() throws IOException, InterruptedException
{

//click on Add device link	
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_adddevicelink,"AddCPEforIntermediateEquip_adddevicelink");

click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_adddevicelink,"AddCPEforIntermediateEquip_adddevicelink");
waitforPagetobeenable();
}




public void selectTechnology(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
String technologyToBeSelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");


//verify Technology popup
boolean technologypopup=false;
technologypopup=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.technologyPopup);
if(technologypopup) 
{
Reporter.log("Technology popup is displaying as expected");
}
else 
{
Reporter.log("Technology popup is not displaying");
}

//Dropdown values inside popup
boolean technologyDropdown=false;
technologyDropdown=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.technologypopup_dropdown);
if(technologyDropdown) {
Reporter.log("Technology dropdown is displaying as expected");

click(Lanlink_Outbandmanagement.Outbandmanagement.technologypopup_dropdown);
waitforPagetobeenable();

//verify list of values inside technology dropdown
List<String> listofTechnololgy = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listOfTechnology)));

if(listofTechnololgy.size()>0) {

for (String technoloyTypes : listofTechnololgy) {
	Reporter.log("List of values available under 'Technology' dropdown are: "+technoloyTypes);
}
}


//Select the Technology
webDriver.findElement(By.xpath("//div[@class='modal-body']//div[contains(text(),'"+ technologyToBeSelected +"')]")).click();
waitforPagetobeenable();
String actualValue=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.tchnologyPopup_dropdownValues);
Reporter.log( " 'Technology' selected is: "+actualValue);

}else {
Reporter.log("Technology dropdown is not displaying");
}

verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.IntermediateEquipment_OKbuttonforpopup,"IntermediateEquipment_OKbuttonforpopup");

click(Lanlink_Outbandmanagement.Outbandmanagement.IntermediateEquipment_OKbuttonforpopup,"IntermediateEquipment_OKbuttonforpopup");
waitforPagetobeenable();		
}



public void verifyFieldsandSelectCPEdevice_existingDevice_IntEquipment(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
{
String existingDeviceName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "intEquip_existingDeviceValue");


waitToPageLoad();

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");


click(Lanlink_Outbandmanagement.Outbandmanagement.existingDevice_SelectDeviceToggleButton,"Select Device");
waitforPagetobeenable();

waitToPageLoad();

addDropdownValues_commonMethod("Choose a Device",Lanlink_Outbandmanagement.Outbandmanagement.chooseAdeviceDropdown, existingDeviceName);
waitToPageLoad();

click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag,"OK");

}



public void verifyValuesforCPEexistingdevice_1G_intEquipment() throws InterruptedException, IOException	{
	   
	   fetchValueFromViewPage( "Name");
	   fetchValueFromViewPage( "Router Id");
	   fetchValueFromViewPage( "Vendor/Model");
	   fetchValueFromViewPage( "Snmpro");
	   fetchValueFromViewPage( "Management Address");
	   fetchValueFromViewPage( "MEP Id");
	   fetchValueFromViewPage( "Power Alarm");
	   fetchValueFromViewPage( "Media Selection");
	   fetchValueFromViewPage( "Link Lost Forwarding");
	   fetchValueFromViewPage( "MAC Address");
	   fetchValueFromViewPage("Serial Number");
	   fetchValueFromViewPage( "Country");
	   fetchValueFromViewPage( "City");
	   fetchValueFromViewPage( "Site");
	   
 }


public void EDITCPEdevicedforIntermediateEquipment_1G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	String Cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");
	String vender_1G_Overture=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_1G_Overtue");
	String vender_1G_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_1G_Accedian");
	String vender_10G_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_10g_Accedian");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_devic_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_managementAddress");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mepid");
	String poweralarm_Overture=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_1G_Overture");
	String poweralarm_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_1G_Accedian");
	String Mediaselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mediaselection");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Macaddress");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_serialNumber");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_linkLostForwarding");
	String Country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_country");
	String ExistingCitySelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingcitySelection");
	String ExistingSiteSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSiteSelection");
	String ExistingPremiseSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremiseSelection");
	String technologySelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String newCitySelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcitySelection");
	String existingCity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
	String newCityName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityName");
	String newCityCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityCode");
	String NewSiteSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteSelection");
	String ExistingSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
	String NewSiteName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteName");
	String NewSiteCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteCode");
	String newPremiseselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseSelection");
	String existingPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
	String newPremiseName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseName");
	String newPremiseCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseCode");




	scrollUp();
	waitforPagetobeenable();

	if(technologySelected.equalsIgnoreCase("Overture")) {
		
		eDITCPEdevicedetailsentered_1G_Overture(Cpename, vender_1G_Overture, snmpro, managementAddress, Mepid, poweralarm_Overture, 
				Mediaselection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, Country,
				ExistingCitySelection, newCitySelection, existingCity, newCityName, newCityCode,
				ExistingSiteSelection, NewSiteSelection, ExistingSite, NewSiteName, NewSiteCode,
				ExistingPremiseSelection, newPremiseselection, existingPremise, newPremiseName, newPremiseCode);
	}
	else if((technologySelected.equalsIgnoreCase("Accedian")) || (technologySelected.equalsIgnoreCase("Accedian-1G"))) {
		
		eDITCPEdevicedetailsentered_1G_Accedian(Cpename, vender_1G_Accedian, snmpro, managementAddress, Mepid, poweralarm_Accedian,
				Mediaselection, Macaddress, serialNumber, hexaSerialnumber, linkLostForwarding, Country,
				ExistingCitySelection, newCitySelection, existingCity, newCityName, newCityCode,
				ExistingSiteSelection, NewSiteSelection, ExistingSite, NewSiteName, NewSiteCode,
				ExistingPremiseSelection, newPremiseselection, existingPremise, newPremiseName, newPremiseCode);
	}

}



public void verifyCPEdevicedataenteredForIntermediateEquipment_1G(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
	String vender_Overture=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Overture");
	String vender_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_1G_Accedian");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
	String poweralarm_Overture=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_overture");
	String powerAlarm_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_1G_Accedian");
	String MediaselectionActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
	String existingCity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
	String existingSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
	String existingPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
	String newmanagementAddressSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
	String existingmanagementAddressSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
	String manageaddressdropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_manageaddress_dropdownvalue");
	String existingcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
	String newcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
	String cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
	String citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
	String existingsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
	String newsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
	String sitename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
	String sitecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
	String existingpremiseselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
	String premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
	String premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
	String technologySelectedfordevicecreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String newpremiseselectionmode= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");


	
	if (technologySelectedfordevicecreation.equalsIgnoreCase("Overture")) {

		viewdevice_Overture(cpename, vender_Overture, snmpro, managementAddress, Mepid,
				poweralarm_Overture, MediaselectionActualValue, Macaddress, linkLostForwarding, country,
				existingCity, cityname, existingSite, sitename, existingPremise, premisename,
				existingcityselectionmode, newcityselectionmode, existingsiteselectionmode, newsiteselectionmode,
				newmanagementAddressSelection, existingmanagementAddressSelection, manageaddressdropdownvalue,
				existingpremiseselectionmode, newpremiseselectionmode);

	}

	if ((technologySelectedfordevicecreation.equalsIgnoreCase("Accedian"))
			|| (technologySelectedfordevicecreation.equalsIgnoreCase("Accedian-1G"))) {

		viewdevice_Accedian(cpename, vender_Accedian, snmpro, managementAddress, Mepid,
				powerAlarm_Accedian, MediaselectionActualValue, Macaddress, serialNumber, hexaSerialnumber,
				linkLostForwarding, country, existingCity, cityname, existingSite, sitename, existingPremise,
				premisename, existingcityselectionmode, newcityselectionmode, existingsiteselectionmode,
				newsiteselectionmode, newmanagementAddressSelection, existingmanagementAddressSelection,
				manageaddressdropdownvalue, existingpremiseselectionmode, newpremiseselectionmode);
	}

}


public void verifyValuesforCPEexistingdevice_10G_intEquipment() throws InterruptedException, IOException	{
	   
	   fetchValueFromViewPage( "Name");
	   fetchValueFromViewPage( "Router Id");
	   fetchValueFromViewPage( "Vendor/Model");
	   fetchValueFromViewPage( "Snmpro");
	   fetchValueFromViewPage( "Management Address");
	   fetchValueFromViewPage( "MEP Id");
	   fetchValueFromViewPage( "Power Alarm");
	   fetchValueFromViewPage( "Media Selection");
	   fetchValueFromViewPage( "Link Lost Forwarding");
	  
	   fetchValueFromViewPage( "Country");
	   fetchValueFromViewPage( "City");
	   fetchValueFromViewPage( "Site");
 }



public void EDITCPEdevice_IntermediateEquipment_10G(String testDataFile, String sheetName, String scriptNo, String dataSetNo)
		throws InterruptedException, IOException {
	
	
	String Cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_cpe_deviecname");
	String vender_10G_Accedian=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_vender_10g_Accedian");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_devic_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_managementAddress");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mepid");
	String poweralarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_poweralarm_10G_Accedian");
	String Mediaselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Mediaselection");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Macaddress");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_serialNumber");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_linkLostForwarding");
	String Country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_country");
	String ExistingCitySelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingcitySelection");
	String ExistingSiteSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSiteSelection");
	String ExistingPremiseSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremiseSelection");
	String technologySelected=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String NewCitySelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcitySelection");
	String existingCity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_Existingcity");
	String newCityName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityName");
	String newCityCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newcityCode");
	String NewSiteSelection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteSelection");
	String ExistingSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_ExistingSite");
	String NewSiteName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteName");
	String NewSiteCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newSiteCode");
	String newPremiseselection=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseSelection");
	String existingPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_existingPremise");
	String newPremiseName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseName");
	String newPremiseCode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "EDIT_Intequip_device_newPremiseCode");



	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown);
	waitforPagetobeenable();
	ExtentTestManager.getTest().log(LogStatus.INFO, "edit CPE device");
	
	Reporter.log("Entered edit functionalitty");

	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown, "Action");
	click(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown, "Action");
	
	waitforPagetobeenable();
	verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevicelinkunderviewpage,"Edit CPE Deevice link Under View");
	click(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevicelinkunderviewpage,"Edit CPE Deevice link Under View");
	
	waitforPagetobeenable();
	Report.LogInfo("INFO","edit functionality worked","PASS");
	
	scrollUp();
//Name field
	device_editnamefield( Cpename);
	
//vendor/model
	device_editVendorModelField(vender_10G_Accedian);
	
//Snmpro
	device_editSnmproField();

//Management address
	device_editManagementAddressField(managementAddress);
	
//Mepid	
	device_editMEPIdField(Mepid);
	
//power alarm	
	device_editPowerAlarm(poweralarm);

//Serial Number
	device_editserialnumber(serialNumber);
    
    
//linklost forwarding	
	device_editlinkLostforwarding(linkLostForwarding);
    
    
	//scroll();
	waitforPagetobeenable();
	
	//Country
			if(!Country.equalsIgnoreCase("Null")) {
				
				selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_selectTag, "Country", Country);
				
				//New City		
				if(ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
					click(Lanlink_Outbandmanagement.Outbandmanagement.addcityswitch);
				   //City name
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.citynameinputfield, newCityName, "City Name");
				   //City Code	
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.citycodeinputfield, newCityCode, "City Code");
				   //Site name
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitenameinputfield_addCityToggleSelected, NewSiteName, "Site Name");
				   //Site Code
					sendKeys( Lanlink_Outbandmanagement.Outbandmanagement.sitecodeinputfield_addCityToggleSelected, NewSiteCode,  "Site Code");
				   //Premise name	
					sendKeys( Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addCityToggleSelected, newPremiseName, "Premise Name");
				   //Premise Code	
					sendKeys( Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addCityToggleSelected, newPremiseCode, "Premise Code");
						
				}	
			
			//Existing City	
				else if(ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {
					
				   selectValueInsideDropdown( Lanlink_Outbandmanagement.Outbandmanagement.cityDropdown_selectTag, "City", existingCity );
					
					
				 //Existing Site
					  if(ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
						  selectValueInsideDropdown( Lanlink_Outbandmanagement.Outbandmanagement.siteDropdown_selectTag, "Site", ExistingSite);
						  
					 //Existing Premise
						  if(ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
							  selectValueInsideDropdown( Lanlink_Outbandmanagement.Outbandmanagement.premiseDropdown_selectTag, "Premise", existingPremise);
				          	 }
						  
						//New Premise  
						  else if(ExistingPremiseSelection.equalsIgnoreCase("no") & newPremiseselection.equalsIgnoreCase("yes")) {
							  
							  click(Lanlink_Outbandmanagement.Outbandmanagement.addpremiseswitch);
							  //Premise name	
								sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addPremiseToggleSelected, newPremiseName,  "Premise Name");
							   //Premise Code	
								sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addPremiseToggleSelected, newPremiseCode, "Premise Code");
						  } 
			         	}
		  		
				  else if(ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {
					  	
					  click(Lanlink_Outbandmanagement.Outbandmanagement.addsiteswitch);
					  //Site name
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitenameinputfield_addSiteToggleSelected, NewSiteName, "Site Name");
					   //Site Code
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitecodeinputfield_addSiteToggleSelected, NewSiteCode, "Site Code");
						
					   //Premise name	
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addSiteToggleSelected, newPremiseName, "Premise Name");
					   //Premise Code	
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addSiteToggleSelected, newPremiseCode, "Premise Code");
					  }
				}
				
			}
			else if(Country.equalsIgnoreCase("Null")) {
				
				ExtentTestManager.getTest().log(LogStatus.PASS, " No changes made for 'Country' dropdown");
			
			//City	
				editCity(ExistingCitySelection, NewCitySelection, Lanlink_Outbandmanagement.Outbandmanagement.cityDropdown_selectTag, "selectcityswitch", "addcityswitch",
						existingCity, newCityName, newCityCode, "City");
				
				
			//Site	
				editSite(ExistingSiteSelection, NewSiteSelection, Lanlink_Outbandmanagement.Outbandmanagement.siteDropdown_selectTag, "selectsiteswitch",
						"addsiteswitch", ExistingSite , NewSiteName, NewSiteCode, "Site");
				
			//Premise
				editPremise(ExistingPremiseSelection, newPremiseselection, Lanlink_Outbandmanagement.Outbandmanagement.premiseDropdown_selectTag, "selectpremiseswitch",
						"addpremiseswitch", existingPremise, newPremiseName, newPremiseCode, "Premise");
				
			}
	
			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
	
	click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton, "OK");
	waitforPagetobeenable();

}



public void verifyFieldsandAddCPEdevicefortheserviceselected_IntEquip_10G(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException {

	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
	String vender=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_10G_Accedian");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
	String poweralarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_10G_Accedian");
	String MediaselectionActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
	String country=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
	String City=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
	String Site=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
	String Premise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
	String newmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
	String existingmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
	String manageaddressdropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_manageaddress_dropdownvalue");
	String existingcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
	String newcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
	String cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
	String citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
	String existingsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
	String newsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
	String sitename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
	String sitecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
	String existingpremiseselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
	String newpremiseselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");
	String premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
	String premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
	String technologySelectedfordevicecreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	


	
	try {
		
	String linklostForwardingcheckboxstate="disabled"; 
		
	String[] Vender= {"Accedian 10GigE-MetroNode-CE-2Port", "Accedian 10GigE-MetroNode-CE-4Port"};
	
	String[] powerAlarm= {"DC Single Power Supply - PSU A", "DC Dual Power Supply - PSU-A+B"};
	
	String expectedDeviceNameFieldAutopopulatedValue="<Device>-10G.lanlink.dcn.colt.net";
	
	String MEPid="5555";
	
	String expectedValueForSnmpro= "JdhquA5";
	
	((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

	waitforPagetobeenable();
	
	click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
	waitforPagetobeenable();
	
	scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.name);
	waitforPagetobeenable();

		// Vendor/Model Error Message
		device_vendorModelWarningMessage();

		// Management Address Error Message
		device_managementAddressWarningMessage();

		// Power Alarm Error Message
		device_powerAlarmWarningMessage();

		//serial number Eror Message
		device_serialNumberWarningMessage();
		
		//Hexa serial Number
		device_hexaSerialNumberWarningMessage();

		
							
		
		
		//Vendor/Model
		device_vendorModel(Vender, vender);      
	
		//Snmpro
		device_snmPro(snmpro);
		
		//Management Address dropdown
		device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);
		
		//MEP Id
		device_mepID(Mepid);
	
		//Power Alarm	
		device_powerAlarm(poweralarm);
		
		//Serial Number
		device_serialNumber(serialNumber);
	
	    //Link lost Forwarding
		device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);
		
		//Country
		device_country(country);
	
		
	
		//City		
		if(existingcityselectionmode.equalsIgnoreCase("no") & newcityselectionmode.equalsIgnoreCase("yes")) {
			addCityToggleButton();
		   //New City	
			newcity(newcityselectionmode, cityname, citycode);
		      // New Site
			newSite(newsiteselectionmode, sitename, sitecode);
			  //New Premise	
			newPremise(newpremiseselectionmode, premisename, premisecode);
				
		}	
			
		else if(existingcityselectionmode.equalsIgnoreCase("yes") & newcityselectionmode.equalsIgnoreCase("no")) {
		//Existing City		
		   existingCity(existingcityselectionmode, City);
			
			//Site
		 
			  if(existingsiteselectionmode.equalsIgnoreCase("yes") & newsiteselectionmode.equalsIgnoreCase("no")) {
				//Existing Site 
				  existingSite(existingsiteselectionmode, Site);
				  
				   //Premise
				  if(existingpremiseselectionmode.equalsIgnoreCase("yes") & newpremiseselectionmode.equalsIgnoreCase("no")) {
					  existingPremise(existingpremiseselectionmode, Premise);
				  
		          	 }
				  else if(existingpremiseselectionmode.equalsIgnoreCase("no") & newpremiseselectionmode.equalsIgnoreCase("yes")) {
					  //New Premise
					    addPremiseTogglebutton();
					    newPremise_clickOnPremisetoggleButton(newpremiseselectionmode, premisename, premisecode);
				  } 
	         	}
  		
		  else if(existingsiteselectionmode.equalsIgnoreCase("no") & newsiteselectionmode.equalsIgnoreCase("yes")) {
			  	//New Site 
			  	addSiteToggleButton();
			  	newSite_ClickOnSiteTogglebutton(newsiteselectionmode, sitename, sitecode); 
			  	
			  	//New Premise
			  	newPremise_clickonSiteToggleButton(newpremiseselectionmode, premisename, premisecode);
			  }
		}
		
		
		//OK button
		device_okbutton();
		
		//cancel button
		device_cancelButton();
		
		
		//OK button
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
			
			//cancel button
			 verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_cancelbutton,"Cancel");
			
			
			click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
			
		
		
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton, "OK");
		
		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

		waitforPagetobeenable();
		
		
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage1_devicename, "Device Name");
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage2_devicename, "Device Name");
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage3_devicename, "Device Name");
		
		//Name
		device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);
		
		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

		waitforPagetobeenable();
		click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton, "OK");
	    
//	sa.assertAll();
	
	}catch(AssertionError e) {
		
		e.printStackTrace();
		
	}

}		





public void verifyCPEdevicedataenteredForIntermediateEquipment_10G(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
	
	String cpename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intEquip_name");
	String vender=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_vender_10G_Accedian");
	String snmpro=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_snmpro");
	String managementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_managementAddress_textfield");
	String Mepid=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mepid");
	String poweralarm=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_poweralarm_10G_Accedian");
	String MediaselectionActualValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Mediaselection_Overture");
	String Macaddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_Macaddress_Overture");
	String serialNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_serialNumber_Accedian");
	String hexaSerialnumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_hexaSerialnumber");
	String linkLostForwarding=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_linkLostForwarding");
	String existingcountry=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_country");
	String existingCity=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcity_dropodwnvalue");
	String existingSite=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_site_dropdownvalue");
	String existingPremise=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisedropdownvalue");
	String newmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newmanagementAddress_selection");
	String existingmanagementAddress=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingmanagementAddress_selection");
	String manageaddressdropdownvalue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_manageaddress_dropdownvalue");
	String existingcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingcityselectionmode");
	String newcityselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newcityselectionmode");
	String cityname=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_cityname");
	String citycode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_citycode");
	String sitename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitename");
	String sitecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_sitecode");
	String existingpremiseselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingpremiseselectionmode");
	String premisename=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "deivce_intequip_premisename");
	String premisecode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_premisecode");
	String technologySelectedfordevicecreation=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "TechToBeselected_underTechpopup_device");
	String existingsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_existingsiteselectionmode");
	String newsiteselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newsiteselectionmode");
	String newpremiseselectionmode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "device_intequip_newpremiseselectionmode");

	ExtentTestManager.getTest().log(LogStatus.INFO, "verify the details entered while creating device");
	Reporter.log("verify the details entered while creating device");
	
//	clickOnBankPage();
	waitforPagetobeenable();
	
//	webDriver.findElement(By.xpath("//div[div[div[text()='Intermediate Equipment']]]//div[div[div[contains(@title,'"+ cpename +"')]]]//span[text()='View']")).click();

	scrollUp();
	
	String[] RouterId=new String[2];
	RouterId=cpename.split(".lanlink");
	
	String RouterIdValue=RouterId[0];
	
	
	String mediaSelectionValueInViewDevicePage="no";
	if(MediaselectionActualValue.equalsIgnoreCase("null")) {
		MediaselectionActualValue=mediaSelectionValueInViewDevicePage;
	}else {
		MediaselectionActualValue=mediaSelectionValueInViewDevicePage;
	}
  
 // verifyEnteredvalues_deviceName("Name", RouterIdValue,cpename);
  
  verifyEnteredvalues("Router Id", RouterIdValue);
  
  verifyEnteredvalues("Vendor/Model", vender);
  
  verifyEnteredvalues("Snmpro", snmpro);
  
//Management Address  
  	if((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
	  verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
	 }
	 else if((existingmanagementAddress.equalsIgnoreCase("no")) && (newmanagementAddress.equalsIgnoreCase("Yes"))) {
		 verifyEnteredvalues("Management Address", managementAddress);
	 } 
  
  
  verifyEnteredvalues("Power Alarm", poweralarm);
  
  verifyEnteredvalues("Media Selection", MediaselectionActualValue);
  
  verifyEnteredvalues("Serial Number", serialNumber);
  
  verifyEnteredvalues("Link Lost Forwarding", linkLostForwarding);
  
  verifyEnteredvalues("Country", existingcountry);
  
//City  
	 if((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {
		 verifyEnteredvalues("City", existingCity);
	 }
	 else if((existingcityselectionmode.equalsIgnoreCase("no")) && (newcityselectionmode.equalsIgnoreCase("Yes"))) {
		 verifyEnteredvalues("City", cityname);
	 } 
	 
	 
	//Site
	 if((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {
		 verifyEnteredvalues("Site", existingSite);
	 }
	 else if((existingsiteselectionmode.equalsIgnoreCase("no")) && (newsiteselectionmode.equalsIgnoreCase("Yes"))) {
		 verifyEnteredvalues("Site", sitename);
	 } 
	 
	 
	//Premise
	 if((existingpremiseselectionmode.equalsIgnoreCase("Yes")) && (newpremiseselectionmode.equalsIgnoreCase("no"))) {
		 verifyEnteredvalues("Premise", existingPremise);
	 }
	 else if((existingpremiseselectionmode.equalsIgnoreCase("no")) && (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
		 verifyEnteredvalues("Premise", premisename);
	 } 

}


public void deleteSiteOrder() throws IOException, InterruptedException 
{
click(Lanlink_Outbandmanagement.Outbandmanagement.siteOrder_ActionButton,"siteOrder_ActionButton");
click(Lanlink_Outbandmanagement.Outbandmanagement.deleteLink_common,"Delete");

WebElement DeleteAlertPopup= findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.delete_alertpopup);
if(DeleteAlertPopup.isDisplayed())
{
String deletPopUpMessage= getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.deleteMessages_textMessage);

click(Lanlink_Outbandmanagement.Outbandmanagement.deletebuttons,"Delete");

((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

waitforPagetobeenable();

verifysuccessmessage("Site Order successfully deleted");
}
else
{
Reporter.log("Delete alert popup is not displayed");
}
}

public void device_linklostForwarding( String linkLostForwarding, String state) throws InterruptedException, IOException {
	

   	 boolean linklostenable=false;	
   	 boolean linklost=false;
	    try {
	    	 linklost=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
		//	sa.assertTrue(linklost, "Link lost Forwarding checkbox under add device is not available");
	    if(linklost) {
	    //Find whether it enabled or disabled
	    	linklostenable=isEnable(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
	    	if(state.equalsIgnoreCase("disabled")){
	    		if(linklostenable) {
	    		}else {
	    		}
	    	}
	    	else if(state.equalsIgnoreCase("enabled")) {
	    		if(linklostenable) {
	    			
	    			//select the checkbox as per input	
	    			boolean linklostSelection=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding,"Link lost");
    				if(linklostSelection) {
    				
    				//click on link lost checkbox	
    					if(linkLostForwarding.equalsIgnoreCase("Yes")) {
    	    				
    	    			}else{
    	    				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
    	    				waitforPagetobeenable();
    	    				Reporter.log(" Link Lost Forwarding is unselected as expected");
    	    			}
    				}else {
    					Reporter.log(" 'link lostforwarding' is not selected by default");
    				}
	    			
	    			
	    		}else {
	    			//Link lost Forwarding' checkbox is disabled under 'Add CPE device' page
	    		}

	    
	    } 	
	    }else {
	    	Reporter.log(" 'Link Lost Forwarding' checkbox is not displaying under 'Add CPE device' page");
	    }
	    }catch(NoSuchElementException e) {
	    	e.printStackTrace();
	    }
   	}


public void selectTechnology_HubAndSpoke() throws InterruptedException,IOException {
	
	//verify Technology popup
		boolean technologypopup=false;
		technologypopup=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.technologyPopup);
		if(technologypopup) {
			Reporter.log("Technology popup is displaying as expected");
		}else {
			Reporter.log("Technology popup is not displaying");
		}
		
	//Dropdown values inside popup
		boolean technologyDropdown=false;
		technologyDropdown=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.technologypopup_dropdown);
		if(technologyDropdown) {
			Reporter.log("Technology dropdown is displaying as expected");
			
			click(Lanlink_Outbandmanagement.Outbandmanagement.technologypopup_dropdown);
			waitforPagetobeenable();
			
			//verify list of values inside technology dropdown
			 List<String> listofTechnololgy = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofTechnology)));
				
				if(listofTechnololgy.size()>0) {
		
				for (String technoloyTypes : listofTechnololgy) {
					Reporter.log("List of values available under 'Technology' dropdown are: "+technoloyTypes);
				}
			}
				
		  //Select the Technology
				click(Lanlink_Outbandmanagement.Outbandmanagement.technologyOverture);
				waitforPagetobeenable();
				String actualValue=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.tchnologyPopup_dropdownValues);
				Reporter.log( " 'Technology' selected is: "+actualValue);
				
		}else {
			Reporter.log("Technology dropdown is not displaying");
		}
		
		click(Lanlink_Outbandmanagement.Outbandmanagement.IntermediateEquipment_OKbuttonforpopup);
		waitforPagetobeenable();
		
}


public void device_managementAddress( String existingmanagementAddress,
		String newmanagementAddress, String managementAddress) throws InterruptedException, IOException {

	
	boolean managementaddresdropdown=false;
		boolean manageAddresstextField=false;
		String manageAddresspopulatedValue="null";
		
		if((existingmanagementAddress.equalsIgnoreCase("Yes")) & (newmanagementAddress.equalsIgnoreCase("No")))
		{
			try {
				managementaddresdropdown=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_manageaddressdropdown);
			//sa.assertTrue(managementaddresdropdown, "Management Address dropdown under add device is not available");
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_getSubnetbutton);
				waitforPagetobeenable();
				manageAddresspopulatedValue=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_manageaddressdropdown,"value");
				if(manageAddresspopulatedValue.equalsIgnoreCase("null")) {
					//" No values gets populates in 'Manage Address' dropdown, on clicking 'get Subnets' button
				}else {
					// Values displaying under 'Manage Addres' dropodwn is : "+ manageAddresspopulatedValue);
				}
				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_manageaddressdropdown);
				waitforPagetobeenable();
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
}


public void device_mepID( String Mepid){
		
		String mepValue="null";
		boolean mepid=false;
		
			mepid=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mepid);
			
				//sa.assertTrue(mepid, "Mepid field under 'Add CPE device' page is not available");
				
				if(mepid) {
					
					Reporter.log("MEP Id  text field is displaying as expected");
						
						mepValue=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mepid,"value");
						if(mepValue.equalsIgnoreCase("null")) {
							Reporter.log(" No values are displaying under 'MEP ID' field. It should be auto populated by default");
						}else {
							Reporter.log( " MEP ID field is auto populated and it is displaying as : "+mepValue);
						}
						
				}	
			
		
	}



public void device_powerAlarm(String poweralarm) throws InterruptedException, IOException
{

	addDropdownValues_commonMethod("Power Alarm",Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_poweralarm, poweralarm);
		
} 


public void newPremise_clickOnPremisetoggleButton(String newpremiseselectionmode,String premisename, String premisecode) throws InterruptedException, IOException
{
	
		//New premise 
		 if(newpremiseselectionmode.equalsIgnoreCase("yes")) {
			 
			if(premisename.equalsIgnoreCase("null")) {
			}else {
				//Premise Name Field	
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton, premisename, "Premise name");
				waitforPagetobeenable();
				String prmsenme=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisenamefield_premisetogglebutton,"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Name is : " + prmsenme);
				
			}
			
			if(premisecode.equalsIgnoreCase("null")) {
				//value for Premise code field  is not entered
			}else {
				//Premise Code Field	
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton, premisecode, "Premise code");
				waitforPagetobeenable();
				String premisecde=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisecodefield_premisetogglebutton,"value");
				waitforPagetobeenable();
				Reporter.log("Entered Premise Code is : " + premisecde);
				
			}	
			 
			 
		 }else {
			//Add new Premise is not selected
		 }
	}


public void newSite_ClickOnSiteTogglebutton(String newsiteselectionmode, String sitename, String sitecode) throws InterruptedException, IOException{
	
	//New site 
	 if(newsiteselectionmode.equalsIgnoreCase("yes")) {
		 
		if(sitename.equalsIgnoreCase("null")) {
			Report.LogInfo("Info",  " value for Site name field  is not entered","FAIL");
		}else {
			//Site Name Field	
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton, sitename,"Site name");
			waitforPagetobeenable();
			String sitenme=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_sitenamefield_sitetogglebutton,"value");
			waitforPagetobeenable();
			Reporter.log("Entered Site Name is : " + sitenme);
			Report.LogInfo("Info", 
					"Entered Site Name is : " + sitenme,"PASS");
		}
		
		if(sitecode.equalsIgnoreCase("null")) {
			Report.LogInfo("Info",  " value for Site code field  is not entered","FAIL");
		}else {
			
			//Site Code Field	
			sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton, sitecode,"Site code");
			waitforPagetobeenable();
			String sitecde=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_sitecodefield_sitetogglebutton,"value");
			waitforPagetobeenable();
			Reporter.log("Entered Site Code is : " + sitecde);
			Report.LogInfo("Info", 
					"Entered Site Code is : " + sitecde,"PASS");
			
			
		}	
		 
	 }else {
		 Report.LogInfo("Info",  " Add new city is not selected","FAIL");
	 }
}


public void device_serialNumber(String serialNumber) throws InterruptedException, IOException, IOException {

	 //Serial Number
	   boolean serialNmber=false;
	   try {
		   serialNmber=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialnumber);
		 //  sa.assertTrue(serialNmber, "Serial Number is not available in 'Add CPE Device' page");
		 if(serialNmber) {  
		   if(serialNumber.equalsIgnoreCase("null")) {
		   }else {
			   
			   sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialnumber, serialNumber,"Serial Number");
			   waitforPagetobeenable();
			   String SNactualValue=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialnumber,"value");
		   }
		 }  
	   }catch(NoSuchElementException e) {
		   e.printStackTrace();
		   Reporter.log(" 'Serial Number' text field is not displaying");
	   }catch(Exception er) {
		   er.printStackTrace();
		   Reporter.log("not able to enter value under 'Serial number' textfield");
	   }
}
	   
	   
	   public void verifyinterfaceSpeeddropdown() throws InterruptedException, IOException, IOException
	   {
			// verify the list of interfaceSpeed
			try {

			String[] interfacelist = { "1GigE", "10GigE" };

			boolean interfacespeeddropdown = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceSpeed);
					;
		//	sa.assertTrue(interfacespeeddropdown, "Interface speed dropdown is not displayed");

			click(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceSpeed);

			List<String> listofinterfacespeed = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofinterfacespeed)));
			for (String interfacespeed : listofinterfacespeed) {

				boolean match = false;
				for (int i = 0; i < interfacelist.length; i++) {
					if (interfacespeed.equals(interfacelist[i])) {
						match = true;
						Reporter.log("interface speeds : " + interfacespeed);
						//sa.assertTrue(match,"");
					}
				}
				
			}
			
			} catch (AssertionError error) {
		       
				error.printStackTrace();
				
		    }catch(Exception e) {
		    	Reporter.log("dropdowns values in Interface speed are mismiatching under service type");
		    	Reporter.log("dropdowns values in Interface speed are mismiatching under service type");
			}

		}
	   
	   
	   
	   public void verifyservicesubtypesdropdownwhenMSPandAutoCreatenotslected() throws InterruptedException, IOException {

			String[] servicesubtypelist = { "Direct Fiber", "LANLink International", "LANLink Metro", "LANLink National","LANLink Outband Management", "OLO - (GCR/EU)" };

			try {
				boolean servicesubtypesdropdown = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
				
			//	sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

				// verify the list of service sub types
				click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);

				List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofServicesubtypes)));
				for (String servicesubtypes : listofServicesubtypes) {

					boolean match = false;
					for (int i = 0; i < servicesubtypelist.length; i++) {
						if (servicesubtypes.equals(servicesubtypelist[i])) {
							match = true;
							Reporter.log("service sub types : " + servicesubtypes);
							//sa.assertTrue(match,"");
						}
					}
					

				}
			
			} catch (AssertionError error) {

				error.printStackTrace();
				
		    }catch(Exception e) {
				Reporter.log("Dropdown values in Service subtypes are mismatching");
				Reporter.log("Dropdown values in Service subtypes are mismatching");
			}

		}



	   public void verifyavailablecircuitdropdown() throws InterruptedException, IOException {
			try {
					boolean availablecircuitsdropdown = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AvailableCircuits);
					
				//	sa.assertTrue(availablecircuitsdropdown, "available circuit dropdown is not displayed");
					
			}catch(AssertionError e) {
				Reporter.log("Available circuit dropdown under servicetype got failed");
				Reporter.log("Available circuit dropdown under servicetype got failed");
			}
			}

	   public void verifyA_Endtechnologydropdown() throws InterruptedException, IOException {

			try {
			String[] A_endTechnolnogylist = { "Atrica", "MMSP", "Ethernet over Fibre" };

			boolean A_EndTechnolnogy = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.A_Endtechnology);
				
		//	sa.assertTrue(A_EndTechnolnogy, "A-End technolnogy dropdown is not displayed");

			// verify the list of A-End technolnogies
			click(Lanlink_Outbandmanagement.Outbandmanagement.A_Endtechnology);

			List<String> listofA_endTechnologies = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofA_endTechnologies)));

			for (String A_endTechnolnogies : listofA_endTechnologies) {

				boolean match = false;
				for (int i = 0; i < A_endTechnolnogylist.length; i++) {
					if (A_endTechnolnogies.equals(A_endTechnolnogylist[i])) {
						match = true;
						Reporter.log("A end technology values : " + A_endTechnolnogies);
						//sa.assertTrue(match,"");
					}
				}
				

			}
			} catch (AssertionError error) {

			  error.printStackTrace();
			
			}catch(Exception e) {
				Reporter.log("Dropdwon values inside A-end technology are mismatching");
				Reporter.log("Dropdwon values inside A-end technology are mismatching");
			}

		}
	   
	   
	   public void verifyservicesubtypesdropdownwhenMSPaloneselected() throws InterruptedException, IOException {

			try {
			String[] servicesubtypelist = { "LANLink International", "LANLink Metro", "LANLink National", "OLO - (GCR/EU)" };

			boolean servicesubtypesdropdown = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
			
			//sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

			// verify the list of service sub types
			click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);

			List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofServicesubtypes)));
			for (String servicesubtypes : listofServicesubtypes) {

				boolean match = false;
				for (int i = 0; i < servicesubtypelist.length; i++) {
					if (servicesubtypes.equals(servicesubtypelist[i])) {
						match = true;
						Reporter.log("service sub types : " + servicesubtypes);
						//sa.assertTrue(match,"");
					}
				}
				

			}
			} catch (AssertionError error) {
		       
				error.printStackTrace();
				
			}catch (Exception e) {

			Reporter.log("Dropdown values inside service subtypes are mismatching");
			Reporter.log("Dropdown values inside service subtypes are mismatching");
			}

		}
	   
	   
	   public void verifyB_Endtechnologydropdowb() throws InterruptedException, IOException {

			try {
			String[] B_endTechnolnogylist = { "Atrica", "MMSP", "Ethernet over Fibre" };

			boolean B_Endtechnolnogy = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.B_Endtechnology);
		//	sa.assertTrue(B_Endtechnolnogy, "B-End technolnogy dropdown is not displayed");

			// verify the list of A-End technolnogies
			click(Lanlink_Outbandmanagement.Outbandmanagement.B_Endtechnology);

			List<String> listofB_endTechnologies = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofB_endTechnologies)));
				
			for (String B_endTechnolnogies : listofB_endTechnologies) {

				boolean match = false;
				for (int i = 0; i < B_endTechnolnogylist.length; i++) {
					if (B_endTechnolnogies.equals(B_endTechnolnogylist[i])) {
						match = true;
						Reporter.log("B end technology values : " + B_endTechnolnogies);
						//sa.assertTrue(match,"");
					}
				}
				

			}
			} catch (AssertionError error) {

				  error.printStackTrace();
				
			}catch(Exception e) {
				Reporter.log("Dropdwon values inside B-end technology are mismatching");
				Reporter.log("Dropdwon values inside B-end technology are mismatching");
			}
		}
	   
	   public void verifyservicesubtypesdropdownwhenMSPandAutoCreateselected
	   () throws InterruptedException, IOException {

	   	String[] servicesubtypelist = { "LANLink International", "LANLink Metro", "LANLink National", "OLO - (GCR/EU)" };

	   	try {
	   		boolean servicesubtypesdropdown = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
	   		
	   		//sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

	   		// verify the list of service sub types
	   		click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);

	   		List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofServicesubtypes)));
	   		for (String servicesubtypes : listofServicesubtypes) {

	   			boolean match = false;
	   			for (int i = 0; i < servicesubtypelist.length; i++) {
	   				if (servicesubtypes.equals(servicesubtypelist[i])) {
	   					match = true;
	   					Reporter.log("service sub types : " + servicesubtypes);
	   				//	sa.assertTrue(match,"");
	   				}
	   			}
	   			

	   		}
	   	
	   	} catch (AssertionError error) {

	   		error.printStackTrace();
	   		
	       }catch(Exception e) {
	   		Reporter.log("Dropdown values in Service subtypes are mismatching");
	   		Reporter.log("Dropdown values in Service subtypes are mismatching");
	   	}

	   }
	   
	   public void verifyservicesubtypesdropdownwgenAutoCreatealoneselected() throws InterruptedException, IOException {

			try {
			String[] servicesubtypelist = { "Direct Fiber", "LANLink International", "LANLink Metro", "LANLink National","LANLink Outband Management", "OLO - (GCR/EU)" };

			boolean servicesubtypesdropdown = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
					
		//	sa.assertTrue(servicesubtypesdropdown, "Service sub type dropdown is not displayed");

			// verify the list of service sub types
			click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);

			List<String> listofServicesubtypes = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofServicesubtypes)));
			for (String servicesubtypes : listofServicesubtypes) {

				boolean match = false;
				for (int i = 0; i < servicesubtypelist.length; i++) {
					if (servicesubtypes.equals(servicesubtypelist[i])) {
						match = true;
						Reporter.log("service sub types : " + servicesubtypes);
						// sa.assertTrue(match,"");
					}
				}
				

			}
			} catch (AssertionError error) {

				  error.printStackTrace();
				
			}catch(Exception e) {
				Reporter.log("Dropdown values inside service subtypes are mismatching");
				Reporter.log("Dropdwon values inside service subtypes are mismatching");
			}

		}
	   
	   
	   public void DirectFibre_10G(String ServiceIdentificationNumber, String SelectSubService,
				String Interfacespeed, String EndpointCPE, String Email, String PhoneContact, String remark,
				String PerformanceReporting, String ProactiveMonitoring, String deliveryChannel, String Manageconnectiondropdown,
				String vpnTopology, String intermediateTechnology, String CircuitReference, String CircuitType,
				String notificationManagement, String ENNIcheckBox)
				throws InterruptedException, IOException {

		    scrollIntoTop();
			waitforPagetobeenable();

			// Service Identification
			createService_ServiceIdentification(ServiceIdentificationNumber);

			// End point CPE
			createService_singleEndPointCPE(EndpointCPE);

			// Email
			createSerivce_email(Email);

			// Phone Contact
			createService_phone(PhoneContact);

			// Remark
			createService_remark(remark);

			
			waitforPagetobeenable();

			// Performance Reporting
			if (!PerformanceReporting.equalsIgnoreCase("null")) {

				if (PerformanceReporting.equalsIgnoreCase("yes")) {

					boolean perfrmReprtFieldcheck = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox).isDisplayed();
					if (perfrmReprtFieldcheck) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Performance reporting' checkbox is displaying under 'Manage ment options' panel in 'Create Service' page as exepcted");
						click(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);
						waitforPagetobeenable();

						boolean prfrmReportselection = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox).isSelected();
						if (prfrmReportselection) {
							Reporter.log("performance monitoring check box is selected");
							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Performance Reporting checkbox is selected as expected");
							Reporter.log("Performance Reporting checkbox is selected as expected");
						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL, "Performance Reporting checkbox is not selected");
							Reporter.log("Performance Reporting checkbox is not selected");
						}

					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Performance Reporting' checkbox is not available");
						Reporter.log(" 'Performance Reporting' checkbox is not available");
					}
				} else {

					Reporter.log("Performance Repoting is not selected");
					ExtentTestManager.getTest().log(LogStatus.PASS, "performance Reporting checkbox is not selected as expected");
				}
			}

			// Pro active Monitoring
			createService_proactivemonitoring( ProactiveMonitoring, notificationManagement);

			// Delivery Channel
			createService_deliveryChannel(deliveryChannel);
			// management Connection
			createService_managementOptions(Manageconnectiondropdown);

			// ENNI checkbox
			addCheckbox_commonMethod( Lanlink_Outbandmanagement.Outbandmanagement.ENNI_checkbox, "ENNI", ENNIcheckBox);

			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
			click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK");

		}
	   
	   
	   
	   
	   public void verifySiteOrderFields_OnnetOffnet( String interfaceSpeed)
				throws InterruptedException, IOException {

		//	ExtentTestManager.getTest().log(LogStatus.INFO," Verifying 'Site Order' fields");

			try {

				webDriver.findElement(By.xpath(("//span[contains(text(),'OK')]"))).click();
				waitforPagetobeenable();
				

				// Validate Country dropdown
				Reporter.log("validate Country dropdown");
				validateCountry_AddSiteorder();

				// Validate City Fields
				Reporter.log("Validate city fields");
				validateCity_AddSiteOrder();

				// Validate Site/CSR field
				Reporter.log("validate Site Fields");
				validateSite_AddSiteOrder();   

				
				// Validate performance reporting dropdown
				Reporter.log("validate performance reporting checkbox");
				validatePerformancereporting_AddSiteOrder();   

				// validate proactive Monitoring dropdown
				validateProactiveMonitoring_AddSiteOrder();   

				// Validate Smarts monitoring dropdown
				validateSmartsMOnitoring_AddSiteOrder();      //pending

				// Validate Site Alias field
				validateSiteAlias_AddSiteOrder();        

				// Validate VLAN Id field
				validateVlanID_AddSiteOrder();

				// Validate DCA Enabled Site and Cloud Service Provider dropdown
				valiadateDCAEnabledsite_AddSieOrder();     

				// Verify Remark field
				Reporter.log("validate Remark fields");
				validateRemark_AddSiteOrder();

				if (interfaceSpeed.equals("1GigE")) {

					technologyDropdownFor1GigE();    //pending
				}

				else if (interfaceSpeed.equals("10GigE")) {

					technologyDropdownFor10GigE();
				}

				// Validate OK button
				OKbutton_AddSiteOrder();

				// Validate Cancel button
				cancelbutton_AddSiteOrder();

				waitforPagetobeenable();
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cancel);
				waitforPagetobeenable();
				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cancel,"Addsiteorder_cancel");
				waitforPagetobeenable();

				

			} catch (AssertionError e) {
				e.printStackTrace();
			}

		}
	   
	   
	   public void validateCountry_AddSiteorder() throws InterruptedException, IOException {
			
			boolean COuntry=false;
			
			COuntry = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Country);
		//	sa.assertTrue(COuntry, "Country dropdown is not displayed");
			if(COuntry) {
				Report.LogInfo("Info"," 'Country' mandatory dropdown is displaying under 'Add Site Order' page as expected","PASS");	
				Reporter.log("Country dropdown is displaying");
				
			click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Country,"Country");
			List<String> listofcountry = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofcountry)));

			if(listofcountry.size()>=1) {
			for (String countrytypes : listofcountry) {
				
				Report.LogInfo("Info","The list of country inside dropdown is: "+countrytypes,"PASS");
				
			}
		}else {
			Reporter.log("no values are available inside Country dropdown for Add site order");
			Report.LogInfo("Info","no values are available inside Country dropdown for Add site order","FAIL");
		}

		//click on Blank page	
			clickOnBankPage();
			Thread.sleep(3000);
			
		}else {
			Report.LogInfo("Info", " 'Country' mandatory dropdown is not available under 'Add Site Order' page","FAIL");
		}
			
		}
	   
	   public void validateSiteAlias_AddSiteOrder() throws InterruptedException, IOException {
			
			// Site alias Field
		 boolean sitealias=false; 
	try { 				
	sitealias = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitealias);
					//sa.assertTrue(sitealias, "Site alias field is not displayed");
					if(sitealias) {
						 Report.LogInfo("Info", " 'Site Alias' text field is displaying under 'Add Site order' page as expected","PASS");
					}else {
						 Report.LogInfo("Info"," 'Site Alias' text field is not displaying under 'Add Site order' page","FAIL");
					}
	 }catch	(Exception e) {
		 e.printStackTrace();
		 Report.LogInfo("Info", " 'Site Alias' text field is not displaying under 'Add Site order' page","FAIL");
	 }
	 }
	   
	   
	   public void validateVlanID_AddSiteOrder() throws InterruptedException, IOException {
			 boolean vlanid=false;
			 try {
						vlanid = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Vlanid);
					//	sa.assertTrue(vlanid, "VLAN id field is not displayed");
						if(vlanid) {
							 Report.LogInfo("Info",  "'VLAN ID' text field is displaying under 'Add Site order' page as expected","PASS");
						}else {
							 Report.LogInfo("Info",  " 'VLAN ID' text field is not displaying under 'Add Site order' page","FAIL");
						}
			 }catch(Exception e) {
				 e.printStackTrace();
				 Report.LogInfo("Info", " 'VLAN ID' text field is not displaying under 'Add Site order' page","FAIL");
			 }
		}

	   public void validateRemark_AddSiteOrder() throws InterruptedException, IOException {
			 boolean REmark=false;
			 try {
				REmark = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark);
			//	sa.assertTrue(REmark, " Remark field is not displayed");
				if(REmark) {
					 Report.LogInfo("Info", " 'Remak' field is displaying under 'Add Site order' page as expected","PASS");
				}else {
					 Report.LogInfo("Info", " 'Remak' field is not displaying under 'Add Site order' page","FAIL");
				}
			 }catch(Exception e) {
				 e.printStackTrace();
				 Report.LogInfo("Info", " 'Remak' field is not displaying under 'Add Site order' page","FAIL");
				 
			 }

		}
	   
	   
	   public void technologyDropdownFor10GigE() throws InterruptedException, IOException {
			
			String Technology = "Accedian";

			boolean technology, Nonterminationpointcheckbox, portectedcheckbox;
			
			// Technology dropdown
					technology = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology);
					//sa.assertTrue(technology, "Technology dropdown is not displayed");

					click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
					List<String> listoftechnology = new ArrayList<>(Arrays.asList((Lanlink_Outbandmanagement.Outbandmanagement.ClassNameForDropdowns)));
					
				if(listoftechnology.size()>=1) {	
					for (String technologytypes : listoftechnology) {

								Reporter.log("list of technology are : " + technologytypes);
								Reporter.log("list of technology are : " + technologytypes);
								 Report.LogInfo("Info","The list of technology  inside dropdown while  adding site order is: "+technologytypes,"PASS");
					}
				}else {
					
					Reporter.log("no values are available inside technology dropdown for Add site order");
					 Report.LogInfo("Info","no values are available inside technology dropdown for Add site order","FAIL");
				}
				
				    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
					click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
					webDriver.findElement(By.xpath("//div[text()='" + Technology + "']")).click();
					
					
				//Non Termination Point	
					verifySiteOrderFields_NonterminationField();

				
				}	
	   
	   public void verifySiteOrderFields_NonterminationField() throws InterruptedException, IOException {
			boolean Nonterminationpointcheckbox=false;
			try {		
				Nonterminationpointcheckbox=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint);
			//	sa.assertTrue(Nonterminationpointcheckbox, "On selecting 'Overture' under Technology, Non termination point checkbox is not available");
				if(Nonterminationpointcheckbox) {
					 Report.LogInfo("Info", " 'Non Termination Point' checkbox is displayed under 'Add Site order' page as expected","PASS");
				}else {
					 Report.LogInfo("Info", " 'Non Termination Point' checkbox is not Available under 'Add Site order' page","FAIL");
				}
				
				boolean nonTerminaionpointselection=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint);
			//	sa.assertFalse(nonTerminaionpointselection,"Non-termination point checbox under Add site is selected by default");
				if(nonTerminaionpointselection) {
					// Report.LogInfo("Info", " 'Non-Termination Point' checkbox is selected by default","FAIL");
				}else {
					// Report.LogInfo("Info", " 'Non-Termination Point' checkbox is not selected by default as expected","PASS");
				}
			}catch(NoSuchElementException e) {
				e.printStackTrace();
				 Report.LogInfo("Info", " 'Non Termination Point' checkbox is not Available under 'Add Site order' page","FAIL");
				Reporter.log(" 'Non Termination Point' checkbox is not Available under 'Add Site order' page");
			}catch(Exception ee) {
				ee.printStackTrace();
				// Report.LogInfo("Info", " 'Non-Termination Point' checkbox is selected by default","FAIL");
			//	Reporter.log( " 'Non-Termination Point' checkbox is selected by default");
			}
		}
	   
	   public void OKbutton_AddSiteOrder() throws InterruptedException, IOException {

			 boolean ok=false;
			try { 
			ok = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
			//	sa.assertTrue(ok, "OK button is not displayed");
				if(ok) {
					Report.LogInfo("Info", " 'OK' button is displaying as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'OK' button is not displaying","FAIL");
				}
				
			}catch(Exception e) {
				Report.LogInfo("Info", " 'OK' button is not displaying","FAIL");
				Reporter.log(" 'OK' button is not displaying");
			}
		}
	   
	   
	   public void cancelbutton_AddSiteOrder() throws InterruptedException, IOException {
			 
			 boolean cancel=false;
			 try {
				 cancel = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cancel);
			//	sa.assertTrue(cancel, "Cancel button is not "
						
				if(cancel) {
					Report.LogInfo("Info", " 'Cancel' button is displaying as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'Cancel' button is not displaying","FAIL");
				}
			 }catch(Exception e) {
				 Report.LogInfo("Info", " 'Cancel' button is not displaying","FAIL");
				 Reporter.log(" 'Cancel' button is not displaying");
			}
		}




	   public void VerifyDataEnteredForSiteOrder_viewSiteOrder_P2P(String country, String city,
				String CSR_Name, String site, String performReport, String ProactiveMonitor, String smartmonitor,
				String technology, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
				String sitevalue, String remark, String xngcityname, String xngcitycode, String devicename,
				String nonterminatepoinr, String Protected, String newcityselection, String existingcityselection,
				String existingsiteselection, String newsiteselection)
				throws InterruptedException, IOException {

			// Country
			verifyEnteredvalues("Device Country", country);

			// City

			if ((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

				// City
				verifyEnteredvalues("Device Xng City", city);

				// Site
				if ((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
					verifyEnteredvalues("CSR Name", site);
				}

				else if ((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
					verifyEnteredvalues("CSR Name", CSR_Name);
				}

			} else if ((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {

				// New City
				verifyEnteredvalues("Device Xng City", xngcitycode);

				// New Site
				verifyEnteredvalues("CSR Name", CSR_Name);

			}

			// Performance Reporting
			verifyEnteredvalues("Performance Reporting", performReport);

			// Proactive Monitoring
			verifyEnteredvalues("Proactive Monitoring", ProactiveMonitor);

			// Smarts Monitoring
			verifyEnteredvalues("Smarts Monitoring", smartmonitor);

			// Technology
			verifyEnteredvalues("Technology", technology);

			// Site Alias
			verifyEnteredvalues("Site Alias", siteallias);

			// VLAN Id
			verifyEnteredvalues("VLAN Id", VLANid);

			// DCA Enabled Site
			verifyEnteredvalues("DCA Enabled Site", DCAenabledsite);

			if (DCAenabledsite.equalsIgnoreCase("Yes")) {

				// Cloud Service Provider
				verifyEnteredvalues("Cloud Service Provider", cloudserviceprovider);

			}

			// Remark
			compareText("Remark", Lanlink_Outbandmanagement.Outbandmanagement.remark_viewPage, remark);

			if (technology.equals("Atrica")) {
				// Non_termination Point checkbox
				verifyEnteredvalues("Non Termination Point", nonterminatepoinr);

				// Device Name text field
				verifyEnteredvalues("Device Name", devicename);

			}

			if (technology.equals("Overture")) {
				// Non_termination Point checkbox
				verifyEnteredvalues("Non Termination Point", nonterminatepoinr);

			}

			if (technology.equals("Alu")) {
				// Device Name text field
				verifyEnteredvalues("Device Name", devicename);
			}

			if (technology.equals("Accedian-1G")) {
				// Non_termination Point checkbox
				verifyEnteredvalues("Non Termination Point", nonterminatepoinr);

			}

			if (technology.equals("Cyan")) {
				// Non_termination Point checkbox
				verifyEnteredvalues("Non Termination Point", nonterminatepoinr);
			}
		}
	   
	   
	   
	   
	   public void City_AddSiteorder( String existingcityselection, String city,
				String newcityselection, String xngcityname, String xngcitycode, String sitevalue, String CSR_Name,
				String existingsiteselection, String newsiteselection)
				throws InterruptedException, IOException {
			
			//Existing City
					if((existingcityselection.equalsIgnoreCase("yes")) & (newcityselection.equalsIgnoreCase("no"))) {

//						Clickon(getwebelement(xml.getlocator("//locators/" + application + "/Addsiteorder_disabledCitytogglebutton")));
//						 Thread.sleep(5000);
						 waitforPagetobeenable();
						click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_City);
						waitforPagetobeenable();
						webDriver.findElement(By.xpath("//div[text()='" + city + "']")).click();
						waitforPagetobeenable();
						
						ExtentTestManager.getTest().log(LogStatus.PASS, city+ " is selected under Device Xng City dropdown");
						Reporter.log(city+ " is selected under Device Xng City dropdown");

						
					//Existing Site	
						if((existingsiteselection.equalsIgnoreCase("yes")) & (newsiteselection.equalsIgnoreCase("no"))) {
			    			
							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Sitetogglebutton);
							waitforPagetobeenable();
							
							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitedropdown);
							waitforPagetobeenable();
							webDriver.findElement(By.xpath("//span[text()='" + sitevalue + "']")).click();
							waitforPagetobeenable();
							ExtentTestManager.getTest().log(LogStatus.PASS, sitevalue+  " is selected under Physical Site dropdown");
							Reporter.log(sitevalue+" is selected under Physical Site dropdown");

			    		}
						
					//New site
						if((existingsiteselection.equalsIgnoreCase("no")) & (newsiteselection.equalsIgnoreCase("yes"))) {
							
//							Clickon(getwebelement(xml.getlocator("//locators/" + application + "/AddsiteOrdr_disabledSitetogglebutton")));
//							Thread.sleep(3000);
							
							if(CSR_Name.equalsIgnoreCase("null")){
								ExtentTestManager.getTest().log(LogStatus.FAIL, "CSR name field is mandatory and no values are provided");
								Reporter.log("No values provided for mandatory field 'CSR Name'");
								
							}else {
								
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_CSRname, CSR_Name,"CSR Name");
							waitforPagetobeenable();

							ExtentTestManager.getTest().log(LogStatus.PASS, CSR_Name+ " is entered under CSR Name field");
							Reporter.log(CSR_Name+ " is entered under CSR Name field");

							}
						}
					
						
						
					}
					else if((existingcityselection.equalsIgnoreCase("no")) & (newcityselection.equalsIgnoreCase("yes"))) {
						
						click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Citytogglebutton);
						waitforPagetobeenable();
						
						//City name 
						 if(xngcityname.equalsIgnoreCase("null")) {
							 ExtentTestManager.getTest().log(LogStatus.FAIL, "City name field is a mandatory field and the value is not provided");
							 Reporter.log("City name field is a mandatory field and the value is not provided");
						 }else {
						 sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcityname, xngcityname,"xngcityname");
						 waitforPagetobeenable();
						 ExtentTestManager.getTest().log(LogStatus.PASS, xngcityname+ " is entered in City name field");
						 Reporter.log(xngcityname+ " is entered in City name field");
						 waitforPagetobeenable();
						 }
						 
						 //City code
						 if(xngcitycode.equalsIgnoreCase("null")) {
							 ExtentTestManager.getTest().log(LogStatus.FAIL, "City Code field is a mandatory field and the value is not provided");
							 Reporter.log("no values provided for city code text field");
						 }else {
						 sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_XNGcitycode, xngcitycode,"xngcitycode");
						 waitforPagetobeenable();
						 ExtentTestManager.getTest().log(LogStatus.PASS, xngcitycode+" is entered in City Code field" );
						 Reporter.log(xngcitycode+" is entered in City Code field");

						 }
						 waitforPagetobeenable();
						 
						
				//add new Site 		 
						try {
							if(CSR_Name.equalsIgnoreCase("null")){
								ExtentTestManager.getTest().log(LogStatus.FAIL, "CSR name field is mandatory and no values are provided");
								 Reporter.log(" no values provided for 'CSR Name' text field");
								
							}else {
								
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_CSRname, CSR_Name,"CSR_Name");
							 waitforPagetobeenable();
							ExtentTestManager.getTest().log(LogStatus.PASS, CSR_Name+ " is entered under CSR Name field");
							Reporter.log(CSR_Name+ " is entered under CSR Name field");
							}
							
						}catch(Exception e) {
							e.printStackTrace();
							ExtentTestManager.getTest().log(LogStatus.FAIL, " CSR NAme not performed");
						}
					
					}

	   }
	   
	   public void VLANid_AddSiteOrder( String VLANid)
				throws InterruptedException, IOException {

			if (VLANid.equalsIgnoreCase("null")) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "No values entered for 'Vlan id' field");
				Reporter.log("No values entered for 'Vlan id' field");
			} else {
				try {
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Vlanid, VLANid,"VLANid");
					waitforPagetobeenable();

					String actualvalue = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Vlanid)
							.getAttribute("value");
					ExtentTestManager.getTest().log(LogStatus.PASS, actualvalue + " is entered under Vlan id field");
					Reporter.log(actualvalue + " is entered under Vlan id field");

				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Vlan Id' field is not displating under 'Add Site order' page");
					Reporter.log(" 'Vlan Id' field is not displating under 'Add Site order' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value under 'Vlan Id' field");
					Reporter.log(" Not able to enter value under 'Vlan Id' field");
				}
			}

		}
	   
	   
	   public void DCAEnabledSite_AddSiteOrder( String DCAenabledsite, String cloudserviceprovider)
				throws InterruptedException, IOException {

			// DCA Enabled Site
			if (DCAenabledsite.equalsIgnoreCase("yes")) {

				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox);
				ExtentTestManager.getTest().log(LogStatus.PASS, "DCA enabled checkbox is selected");
				Reporter.log("DCA enabled checkbox is selected");
				
				// Cloud Service provider
				if (cloudserviceprovider.equalsIgnoreCase("null")) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"DCA cloud service provider dropdown is mandatory. No values are provided");
					Reporter.log("DCA cloud service provider dropdown is mandatory. No values are provided");
				} else {
					click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cloudserviceProvider);
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[text()='" + cloudserviceprovider + "']")).click();;
					waitforPagetobeenable();
					ExtentTestManager.getTest().log(LogStatus.PASS,
							cloudserviceprovider + " is selected under 'cloud service provider' dropdown");
					Reporter.log(cloudserviceprovider + " is selected under 'cloud service provider' dropdown");
				}

			} else {
				Reporter.log("DCA site is not selected");
				ExtentTestManager.getTest().log(LogStatus.PASS, "DCA enabled checkbox is not selected");
			}

		}
	   
	   
	   public void remark_AddSiteOrder(String remark)
				throws InterruptedException, IOException {

			if (remark.equalsIgnoreCase("null")) {
				ExtentTestManager.getTest().log(LogStatus.PASS, "No values entered under remark ");
				Reporter.log("No values entered under remark ");
			} else {
				try {
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark, remark,"remark");
					waitforPagetobeenable();

					String actualvalue = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_remark)
							.getAttribute("value");
					ExtentTestManager.getTest().log(LogStatus.PASS, actualvalue + " is entered under 'remark' field");
					
					Reporter.log(actualvalue + " is entered under 'remark' field");
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Remark' field is not displating under 'Add Site order' page");
					Reporter.log(" 'Remark' field is not displating under 'Add Site order' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value under 'Remark' field");
					Reporter.log(" Not able to enter value under 'Remark' field");
				}
			}

		}
	   
	   public void technologyP2P_AddSiteOrder(String technology, String interfaceSpeed,
				String devicename, String nonterminatepoinr, String Protected)
				throws InterruptedException, IOException {

			// Technology
			if (technology.equalsIgnoreCase("null")) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"Technology dropdown is a mandatory field and no values are provided");
				Reporter.log("Technology dropdown is a mandatory field and no values are provided");
			} else {

				if (interfaceSpeed.equals("1GigE")) {

					if (technology.equals("Actelis") || technology.equals("Atrica") || technology.equals("Overture")
							|| technology.equals("Accedian-1G") || technology.equals("Cyan") || technology.equals("Alu")) {

						click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology,  "Technology dropdown");
						waitforPagetobeenable();
						WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", technology)); 
						clickonTechnology(technologySelected, technology);
						waitforPagetobeenable();
						ExtentTestManager.getTest().log(LogStatus.PASS, technology + " is selected under technology dropdown");
						Reporter.log(technology + " is selected under technology dropdown");

						if (technology.equals("Actelis")) {

							Reporter.log("No additional fields displays");
						}

						else if (technology.equals("Atrica")) {

							// Device name
							devicename_AddSiteOrder( devicename);

							// Non- termination point
							nontermination_AddSiteorder( nonterminatepoinr);

						}

						else if (technology.equals("Overture") || technology.equals("Accedian-1G")) {

							// Non- termination point
							nontermination_AddSiteorder( nonterminatepoinr);

						}

						else if (technology.equals("Cyan")) {

							// Non- termination point
							nontermination_AddSiteorder( nonterminatepoinr);

						}

						else if (technology.equals("Alu")) {

							// Device name
							devicename_AddSiteOrder( devicename);

						}
					}
				}

				if (interfaceSpeed.equals("10GigE")) {

					if (technology.equals("Accedian")) {

						click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
						waitforPagetobeenable();
						WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", technology)); 
						clickonTechnology(technologySelected, technology);
						waitforPagetobeenable();
						ExtentTestManager.getTest().log(LogStatus.PASS, technology + " is selected under technology dropdown");
						Reporter.log(technology + " is selected under technology dropdown");
						// Non- termination point
						nontermination_AddSiteorder( nonterminatepoinr);

					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								technology + "is not available under 'Technology' dropdown for '10GigE' interface speed");
						Reporter.log(technology + "is not available under 'Technology' dropdown for '10GigE' interface speed");
					}

				}
			}

			ExtentTestManager.getTest().log(LogStatus.PASS, "Data has been entered for add site order");
			Reporter.log("Data has been entered for add site order");

		}
	   
	   public void clickonTechnology(WebElement el, String technology) throws InterruptedException {
			//Thread.sleep(3000);
			
			try {
			el.click();
			ExtentTestManager.getTest().log(LogStatus.INFO, "Under 'technology' dropdown, '"+ technology + "' is selected");
			Reporter.log("Under 'technology' dropdown, '"+ technology + "' is selected");
			}
			catch(Exception e)
			//Thread.sleep(3000);
			{
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, technology + " is not available under 'Technology' dropdown");
				Reporter.log( technology + " is not available under 'Technology' dropdown");
			}
		}
	   
	   public void devicename_AddSiteOrder(String devicename)
				throws InterruptedException, IOException {

			// Device name
			if (devicename.equalsIgnoreCase("null")) {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"device name field is mandatory. No values entered under 'device name' field");
				Reporter.log("device name field is mandatory. No values entered under 'device name' field");
			} else {
				try {
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Devicenamefield,devicename,"devicename");
					waitforPagetobeenable();

					String actualvalue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Devicenamefield,"value");

					ExtentTestManager.getTest().log(LogStatus.PASS, actualvalue + " is entered under 'device name' field");
					Reporter.log(actualvalue + " is entered under 'device name' field");
					
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Device name' field is not displaying under 'Add Site Order' page");
					Reporter.log(" 'Device name' field is not displaying under 'Add Site Order' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value in 'Device name' field");
					Reporter.log(" Not able to enter value in 'Device name' field");
				}
			}

		}
	   
	   
	   public void nontermination_AddSiteorder(String nonterminatepoinr)
				throws InterruptedException {

			// Non- termination point
			if (nonterminatepoinr.equalsIgnoreCase("yes")) {
				try {
					click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint);
					waitforPagetobeenable();

					boolean nonTerminationSelection = findWebElement(
							Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint).isSelected();
					if (nonTerminationSelection) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Non-Termination point' checkbox is selected as expected");
						Reporter.log(" 'Non-Termination point' checkbox is selected as expected");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Non-Termination point' checkbox is not selected");
						Reporter.log(" 'Non-Termination point' checkbox is not selected");
					}

					ExtentTestManager.getTest().log(LogStatus.PASS, "Non-termination point checkbox is selected");
					Reporter.log("Non-termination point checkbox is selected");
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" Non-Termination point' checkbox is not dipslaying under 'Add Site order' page");
					Reporter.log(" Non-Termination point' checkbox is not dipslaying under 'Add Site order' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to click on 'non-Termination point' checkbox");
					Reporter.log(" Not able to click on 'non-Termination point' checkbox");
				}
			} else {
				Reporter.log("Non termination point checkbox is not selected as expected");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Non-termination point chekbox is not selected");
			}

		}
	   
	   
	   
	   public void validateCity_AddSiteOrder() throws InterruptedException, IOException {
			
			
			// City dropdown
			boolean CIty = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_City);
		//	sa.assertTrue(CIty, "City dropdown is not displayed");
			if(CIty) {
				Report.LogInfo("Info",  " 'City' mandatory dropdown is displaying under 'Add Site Order' page as expected","PASS");
			
		}else {
			Report.LogInfo("Info",  " 'City' mandatory dropdown is not available under 'Add Site Order' page","FAIL");
		}

				
			//select city toggle button
			boolean selectcitytoggle=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Citytogglebutton);
			//sa.assertTrue(selectcitytoggle, "Select city toggle button for Add Site is not available");
			if(selectcitytoggle) {
				Report.LogInfo("Info",  " 'Select City' toggle button is displaying under 'Add Site Order' page as expected","PASS");
			}else {
				Report.LogInfo("Info",  " 'Select City' toggle button is not avilable under 'Add Site Order' page ","FAIL");
			}
			
			
			click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Citytogglebutton);
			waitforPagetobeenable();
			
			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag);
			waitforPagetobeenable();
			
			Reporter.log("Scrolling down to validate error messgae for City name and city code");
			//Click on Next button to get warning message for XNG City name and XNG City Code text fields
			click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
			waitforPagetobeenable();
			
			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Country);
			
			waitforPagetobeenable();
			
			Reporter.log("scrolling above till device country for validating error message for 'city name ' and 'city code'");
			//XNG City Name Error message	
			boolean xngCitynameErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcitynameerrmsg);
		//	sa.assertTrue(xngCitynameErr, " 'XNG City Name' warning message is not displayed ");
			if(xngCitynameErr) {
				Report.LogInfo("Info",  " Warning message for 'XNG City Name' dropdown is displying under 'Add Site order' page as expected ","PASS");
				String citynameErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcitynameerrmsg);
				Reporter.log(
						"XNG City Name  message displayed as : " + citynameErrMsg);
				Report.LogInfo("Info", 
						" validation message for 'XNG City Name' text field displayed as : " + citynameErrMsg,"PASS");
				Reporter.log("XNG City Name warning message displayed as : " + citynameErrMsg);
				
			}else {
				Report.LogInfo("Info",  " Warning message for 'XNG City Name' dropdown is not displaying under 'Add Site order' page ","FAIL");
			}


			
			//XNG City Code Error message	
			boolean xngCitycodeErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcityCodeerrmsg);
		//	sa.assertTrue(xngCitycodeErr, " 'XNG City Code' warning message is not displayed ");
			if(xngCitycodeErr) {
				Report.LogInfo("Info",  " Warning message for 'XNG City Code' dropdown is displying under 'Add Site order' page as expected ","PASS");
				String cityCodeErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcityCodeerrmsg);
				Reporter.log(
						"XNG City Name  message displayed as : " + cityCodeErrMsg);
				Report.LogInfo("Info", 
						" validation message for 'XNG City Code' text field displayed as : " + cityCodeErrMsg,"PASS");
				Reporter.log("XNG City Code warning message displayed as : " + cityCodeErrMsg);
				
			}else {
				Report.LogInfo("Info",  " Warning message for 'XNG City Code' dropdown is not displaying under 'Add Site order' page ","FAIL");
			}


		//xng city name
		boolean XNGcityname=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_xngcityname);
		//sa.assertTrue(XNGcityname, "XNG city name field for Add Site is not available");
		if(XNGcityname) {
			Report.LogInfo("Info",  " 'XNG City name field is displaying under 'Add Site order' as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'XNG City name field is not available under 'Add Site order'","FAIL");
		}

		//xng city code
		boolean XNGcitycode=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_XNGcitycode);
	//	sa.assertTrue(XNGcitycode, "XNG city code field for Add Site is not available");
		if(XNGcitycode) {
			Report.LogInfo("Info",  " 'XNG City code field is displaying under 'Add Site order' as expected","PASS");
		}else {
			Report.LogInfo("Info",  " 'XNG City code field is not available under 'Add Site order'","FAIL");
		}
		
		}
	   
	   
	   public void validateSite_AddSiteOrder() throws InterruptedException, IOException 
	   {
			 
			 
			// CSR name field
			 boolean csr_name=false;
				csr_name = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_CSRname);
			//	sa.assertTrue(csr_name, "CSR_Name field is not displayed");
				if(csr_name) {
					Report.LogInfo("Info", " 'CSR Name' text field is displaying under 'Add Site order' page as expected","PASS");
					Reporter.log("CSR name field is dipslaying as expected");
				}else {
					Report.LogInfo("Info", " 'CSR Name' text field is not available under 'Add Site order' page","FAIL");
				}

			// click on site toggle button to check Physical site dropdown
				boolean sitetogglebutton=false;
				sitetogglebutton = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Sitetogglebutton);
			//	sa.assertTrue(sitetogglebutton, "select Site toggle button is not displayed");
				if(sitetogglebutton) {
					Reporter.log("site order toggle button is displaying as expected");
					Report.LogInfo("Info"," 'Select Site' toggle button is displaying under 'Add Site Order' page as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'Select Site' toggle button is not avilable under 'Add Site Order' page","FAIL");
				}

				
				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Sitetogglebutton);
				waitforPagetobeenable();

		//Check for Error message for physical Site
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag);
				waitforPagetobeenable();
				Reporter.log("scrolling down to click n OK button to find eror message for site Dropdown");
				click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
				waitforPagetobeenable();
				
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Country);
				
				waitforPagetobeenable();
				
				Reporter.log("scrolling up back till device country dropodwn to find error message validation for physical site");
				boolean physicalsiteErr=false;
				physicalsiteErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_physicalsiteErrmsg);
		//		sa.assertTrue(physicalsiteErr, "Physical Site dropdown warning is not displayed ");
				if(physicalsiteErr) {
					Reporter.log("Physical Site Error message is displaying as expected");
					Report.LogInfo("Info", " 'Physite Site' dropdown warning message is displaying under 'Add Site Order' page as expected","PASS");
					String physicalsiteErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_physicalsiteErrmsg);
					Reporter.log(
							"Physical Site  message displayed as : " + physicalsiteErrMsg);
					Report.LogInfo("Info",
							" validation message for Physical Site dropdown displayed as : " + physicalsiteErrMsg,"PASS");
					Reporter.log("Physical Site validation message displayed as : " + physicalsiteErrMsg);	
				}else {
					Report.LogInfo("Info", " 'Physical Site' dropdown warning message is not displaying under 'Add Site Order' page","FAIL");
					Reporter.log("Physical site warning message is not displaying");
				}
				
			

		//Physical Site dropdown
				boolean SIte=false;
				SIte = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_sitedropdown);
			//	sa.assertTrue(SIte, "PhysicalSite dropdown is not displayed");
				if(SIte) {
					Reporter.log("Physical Site dropdown is displaying as expected");
					Report.LogInfo("Info", " 'physical Site' dropdown is displaying under 'Add Site order' page as expected","PASS");

				}else {
					Report.LogInfo("Info", " 'Physical Site' dropdown is not available under 'Add Site Order' page","FAIL");
		}
		 }
	   
	   
	   public void validatePerformancereporting_AddSiteOrder() throws InterruptedException, IOException {
			 
			 
			 String[] Performancereporting = { "Follow Service", "no" };

			// Performance reporting dropdown
			 boolean performancereport=false;
				performancereport = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereporting);
			//	sa.assertTrue(performancereport, "performance reporting dropdown is not displayed");
				if(performancereport) {
					Report.LogInfo("Info", " 'Performance reporting' dropdown is displaying under 'Add Site order' as expected","PASS");
					waitforPagetobeenable();
					
					//check default value
					String performanceRprtDefaultValues=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereportingdefaultvalue);
							Report.LogInfo("Info", performanceRprtDefaultValues+ " is displaying under 'Performance reporting' dropdown by default","PASS");

					//check list of values inside Performance Reporting drodpown		
				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_performancereporting,"performancereporting");
				List<String> listofperformancereporting = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofperformancereporting)));
						
				
			if(listofperformancereporting.size()>=1) {	
				for (String perfoemancereportingtypes : listofperformancereporting) {
					boolean match = false;
					for (int i = 0; i < Performancereporting.length; i++) {
						if (perfoemancereportingtypes.equals(Performancereporting[i])) {
							match = true;
							Reporter.log("list of performance reporting : " + perfoemancereportingtypes);
							Report.LogInfo("Info", "list of performance reporting for AddSite order : " + perfoemancereportingtypes,"PASS");
						}
						
					}
					
				//	sa.assertTrue(match);

				}
			}else {
				Reporter.log("no values are available inside performance reporting dropdown for Add site order");
				Report.LogInfo("Info", "no values are available inside performance reporting dropdown for Add site order","FAIL");
			}
		}else {
			Report.LogInfo("Info",  " 'Performance reporting' dropdown is not availble under 'Add Site order' ","FAIL");
		}
		}
	   
	   
	   
	   public void validateProactiveMonitoring_AddSiteOrder() throws InterruptedException, IOException {
			 
			 String[] Proactivemonitoring = { "Follow Service", "no" };

			// pro active monitoring
			 boolean proactivemonitoring=false;
				proactivemonitoring = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_proactivemonitoring);
			//	sa.assertTrue(proactivemonitoring, "pro active monitoring dropdown is not displayed");
				if(proactivemonitoring) {
					Report.LogInfo("Info"," 'Proactie Monitoring' dropdown is displaying under 'Add Site Order' page as Expected","PASS");
					
					//check default value
					String proactiveMonitorDefaultValues=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_proactivemonitoringdefaultvalue);
							Report.LogInfo("Info", proactiveMonitorDefaultValues+ " is displaying under 'roactive Monitoring' dropdown by default","PASS");

				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_proactivemonitoring,"proactivemonitoring");
				List<String> listofproactivemonitoring = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofproactivemonitoring)));
				
			if(listofproactivemonitoring.size()>=1) {	
				for (String proactivemonitoringtypes : listofproactivemonitoring) {

					boolean match = false;
					for (int i = 0; i < Proactivemonitoring.length; i++) {
						if (proactivemonitoringtypes.equals(Proactivemonitoring[i])) {
							match = true;
							Reporter.log("list of pro active monitoring : " + proactivemonitoringtypes);
							
							Report.LogInfo("Info","The list of proactive monitoring inside dropdown while  adding site order is: "+proactivemonitoringtypes,"PASS");
						}
				}
				//	sa.assertTrue(match);

				}
			}else {
				
				Reporter.log("no values are available inside pro active monitoring dropdown for Add site order");
				Report.LogInfo("Info","no values are available inside pro active monitoring dropdown for Add site order","FAIL");
			}
		}else {
			Report.LogInfo("Info", " 'Proactie Monitoring' dropdown is not available under 'Add Site Order' page ","FAIL");
		}
		}
	   
	   
	   public void validateSmartsMOnitoring_AddSiteOrder() throws InterruptedException, IOException {
			 
			 String[] Smartmonitoring = { "Follow Service", "no" };

			// smarts monitoring
			 boolean smartmonitoring=false;
				smartmonitoring = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoring);
			//	sa.assertTrue(smartmonitoring, "Smart monitoring dropdown is not displayed");
				if(smartmonitoring) {
					Report.LogInfo("Info", " 'Smart Monitoring' dropdown is displaying under 'Add Site Order' page as expected","PASS");
					//check default value
					String smartmonitorDefaultValues=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoringdefaultvalue);
					Report.LogInfo("Info", smartmonitorDefaultValues+ " is displaying under 'Smart Monitoring' dropdown by default","PASS");

				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_smartmonitoring,"smartmonitoring");
				List<String> listofsmartmonitoring = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofsmartmonitoring)));

			if(listofsmartmonitoring.size()>=1) {	
				for (String smartmonitoringtypes : listofsmartmonitoring) {

					boolean match = false;
					for (int i = 0; i < Smartmonitoring.length; i++) {
						if (smartmonitoringtypes.equals(Smartmonitoring[i])) {
							match = true;
							Reporter.log("list of smart monitoring are : " + smartmonitoringtypes);
							Report.LogInfo("Info","The list of smart monitoring  inside dropdown while  adding site order is: "+smartmonitoringtypes,"PASS");
						}
					}
					
					//sa.assertTrue(match);
				}
			}else {

				Reporter.log("no values are available inside smart monitoring dropdown for Add site order");
				Report.LogInfo("Info","no values are available inside smart monitoring dropdown for Add site order","FAIL");
			}
				}else {
					Report.LogInfo("Info", " 'Smart Monitoring' dropdown is not avilable under 'Add Site Order' page","FAIL");
				}
		}
	   
	   
	   
	   public void valiadateDCAEnabledsite_AddSieOrder() throws InterruptedException,IOException {
		  	 
		   String[] cloudServiceprovider = { "Amazon Web Service", "Microsoft Azure" };
		   boolean DCAEnabledsite=false;

		   // DCA Enabled site
		   			DCAEnabledsite = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox);
		   		//	sa.assertTrue(DCAEnabledsite, "DCA enabled site is not displayed ");
		   			if(DCAEnabledsite) {
		   				
		   			Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is displaying under 'Add Site order' page as expected","PASS");	
		   			boolean DCAselection=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox,"DCA enabled sitecheckbox");
		   		//	sa.assertFalse(DCAselection,"DCA checkbox under Addsite order is selected by default");
		   			if(DCAselection) {
		   				Report.LogInfo("Info", " ' DCA Enabled Site' checkbox should not be selected by default","FAIL");
		   			}else {
		   				Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not selected by default as expected","PASS");
		   						
		   			
		   			Thread.sleep(5000);
		   		// For Cloud service provider
		   			click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox);
		   			Reporter.log("DCA site is enabled to add cloud service provider details");
		   			waitforPagetobeenable();
		   			
		   			boolean DCAafterSelection=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox,"DCA enabled site checkbox");
		   			if(DCAafterSelection) {
		   			Report.LogInfo("Info","DCA site is selected to add cloud service provider details","PASS");
		   			waitforPagetobeenable();

		   			boolean cloudservice = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cloudserviceProvider);
		   		//	sa.assertTrue(cloudservice, "cloud service provider dropdown is not displayed");
		   			if(cloudservice) {
		   				Report.LogInfo("Info", " 'Cloud Service Provider' dropdown is displaying when 'DCA Enabled Site' checkbox is selected as expected","PASS");

		   			click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cloudserviceProvider);
		   			List<String> listofcloudservices = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofcloudservices)));
		   					
		   			
		   		if(listofcloudservices.size()>0) {	
		   			for (String cloudserviceprovidertypes : listofcloudservices) {

		   				boolean match = false;
		   				for (int i = 0; i < cloudServiceprovider.length; i++) {
		   					if (cloudserviceprovidertypes.equals(cloudServiceprovider[i])) {
		   						match = true;
		   						Reporter.log("list of cloud service providers are : " + cloudserviceprovidertypes);
		   						Report.LogInfo("Info","The list of cloudservice provider inside dropdown while  adding site order is: "+cloudserviceprovidertypes,"");
		   					}
		   				}
		   				//sa.assertTrue(match,"");
		   			}
		   		}else {
		   			Reporter.log("no values are available inside cloudservice provider dropdown for Add site order");
		   			Report.LogInfo("Info","no values are available inside cloudservice provider dropdown for Add site order","FAIL");
		   			
		   		}
		   			}else {
		   				Report.LogInfo("Info", " 'Cloud Service Provider' dropdown is not available when 'DCA Enabled Site' checkbox is selected","PASS");
		   			}
		   			
		   			}else {
		   				Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not getting selected","FAIL");
		   			}
		   	  }
		   		
		   			click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox);
		   			waitforPagetobeenable();
		   			
		   	}else {
		   		Report.LogInfo("Info", " 'DCA Enabled Site' checkbox is not available under 'Add Site order' page","FAIL");
		   	}
		   }
	   
	   
	   
	   public void createService_ServiceIdentification(String ServiceIdentificationNumber) throws InterruptedException, IOException {
			//Service Identification
			
			
			boolean serviceIdentificationField = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification);
			if(serviceIdentificationField) {
				
				Report.LogInfo("Info", " 'Service Identification' text field is displaying as exepected", "Pass");
			if(!ServiceIdentificationNumber.equalsIgnoreCase("null")) {
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification,ServiceIdentificationNumber,"Service Id");
				waitforPagetobeenable();
				Report.LogInfo("Info", ServiceIdentificationNumber+ " is entered under 'Service Identification' field", "Pass");
			}else {
				Report.LogInfo("Info", "It is a mandatory field. No values entered for service identification", "FAIL");

			}
			}else {
				Report.LogInfo("Info", "' Service Identfication' mandatory field is not available under 'Add Service' page", "FAIL");

			}

		}
	   
	   public void createService_singleEndPointCPE(String EndpointCPE) throws InterruptedException, IOException {
			
			addCheckbox_commonMethod(Lanlink_Outbandmanagement.Outbandmanagement.EndpointCPE, "Single Endpoint CPE", EndpointCPE);
			
		}
	   
	   public void createSerivce_email(String Email) throws InterruptedException, IOException {
			
			boolean email=false;
			 try {
					email = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Email);
					if(email) {
						Report.LogInfo("Info", " 'Email' text field is displaying as expected", "PASS");
					
					if(!Email.equalsIgnoreCase("null")) {
					
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Email, Email, "Email");
						waitforPagetobeenable();
						Report.LogInfo("Info", Email + " is entered under 'Email' field", "PASS");

					}else {
						Report.LogInfo("Info","value not entered under 'Email' field", "PASS"); 
					}
					}else {
						//Report.LogInfo("Info", " 'Email' field is not available under 'Create Service' page", "FAIL");
					}
			 }catch(NoSuchElementException e) {
				 e.printStackTrace();
				 Reporter.log("Email field is not available");
				// Report.LogInfo("Info", " 'Email' field is not available under 'create Service' page","PASS");
			 }catch(Exception er) {
				 er.printStackTrace();
				 Reporter.log("Not able to enter value in 'Email' field");
				 Report.LogInfo("Info", "Not able to enter value in 'Email' field" ,"FAIL");
			 }
		}
	   
	   public void createService_phone(String PhoneContact) throws InterruptedException, IOException {
			
			boolean phone=false;
		try {	
			phone = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact);
		//	sa.assertTrue(phone, "phone contact field is not displayed");
			if(phone) {
				Report.LogInfo("Info", " 'Phone Contact' text field is displaying as expected","PASS");
				
			if(!PhoneContact.equalsIgnoreCase("null")) {
				
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact, PhoneContact, "Phone contact");
				waitforPagetobeenable();
				Report.LogInfo("Info", PhoneContact + " is entered under 'Phone contact' field" ,"PASS");
			}else {
				Report.LogInfo("Info","Value not entered under 'Phone contact' field","PASS");
			}
			}else {
				Report.LogInfo("Info", " 'Phone Contact' text field is not available under 'Create Service' page","FAIL");
			}
		  }catch(NoSuchElementException e) {
			  e.printStackTrace();
			  Reporter.log("Phone contact text field is not available");
			  Report.LogInfo("Info", " 'Phone Contact' text field is not available under 'Create Service' page","FAIL");
		  }catch(Exception err) {
			  err.printStackTrace();
			  Report.LogInfo("Info", " Not able to enter value under 'hone Contact' field","FAIL");
			  Reporter.log("Not able to enter value under 'Phone Contact' field");
		  }
		}
	   
	   
	   public void createService_remark(String remark) throws InterruptedException, IOException {
			boolean Remark=false;
		try {	
			Remark = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Remark);
			if(Remark) {
				Report.LogInfo("Info", " 'Remark' text field is displaying as expected","PASS");

			if(!remark.equalsIgnoreCase("null")) {
				
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Remark, remark, "Remark");
				waitforPagetobeenable();
				Report.LogInfo("Info", remark + " is entered under 'Remark' field","PASS");
				
			}else {
				
				Report.LogInfo("Info","value not entered under 'Remark' field","PASS");
			}
			}else {
				Report.LogInfo("Info", " 'Remark' text field is not available under 'Create Service' page", "FAIL");
			}
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Reporter.log("Remak text field is not availeble");
			Report.LogInfo("Info", " 'Remark' text field is not available under 'Create Service' page", "FAIL");
		}catch(Exception err) {
			err.printStackTrace();
			Reporter.log(" Not able t enter value in 'remark' text field");
			 Reporter.log("Not able to enter value under 'Remark' field");
		}
			
		}
	   
	   
	   
	   public void createService_proactivemonitoring(String ProactiveMonitoring, String notificationManagement) throws InterruptedException, IOException {
			
			boolean proactiveMonitor=false;
			proactiveMonitor = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring);
		//	sa.assertTrue(proactiveMonitor, "pro active monitoring checkbox is not displayed");
			if(proactiveMonitor) {
				Report.LogInfo("Info", " 'Pro active Monitoring' text field is displaying as expected","PASS");
			
			if (!ProactiveMonitoring.equalsIgnoreCase("null")) {
				if (ProactiveMonitoring.equalsIgnoreCase("yes")) {

					click(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring,"proactive Monitoring");
					Reporter.log("Pro active monitoring check box is selected");
					
					boolean proactiveSelection=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring,"proactive Monitoring");
					if(proactiveSelection) {
						Report.LogInfo("Info", " 'pro active monitoring' checkbox is selected as expected","PASS");
					}else {
						Report.LogInfo("Info", " 'pro active monitoring' checkbox is not selected","FAIL");
					}
					
				  //Notification management	
				try {	
					
					if (!notificationManagement.equalsIgnoreCase("null")) {
						Report.LogInfo("INFO","Notificationan Management dropdown displays when pro active monitoring is selected","PASS");
					
					click(Lanlink_Outbandmanagement.Outbandmanagement.notificationmanagement,"notification management");
					waitforPagetobeenable();
					webDriver.findElement(By.xpath("//div[contains(text(),'" + notificationManagement + "')]")).click();					
					waitforPagetobeenable();
					Report.LogInfo("Info", notificationManagement + " is selected under 'Notification management' dropdown","PASS");
					
					}else {
						Report.LogInfo("Info","No values selected under Notification management dropdown","PASS"); 
						
					}
					
				}catch(NoSuchElementException e) {
					Reporter.log(" 'Notification management' dropodwn is not displaying under 'create Service' page");
					Report.LogInfo("Info", " 'Notification management' dropodwn is not displaying under 'create Service' page","FAIL");
				}catch(Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info","Not able to select values under Notification management dropdown","FAIL"); 
				}
				} else {
					Reporter.log("Pro active monitoring is not selected");
					Reporter.log("pro active monitoring is not selected");
					Report.LogInfo("Info","performance monitor checkbox is not selected ","PASS"); 
				}

			}
			}else {
				Report.LogInfo("Info", " 'Pro active monitoring' checkbox is not displaying","FAIL");
			}
		}
	   
	   
	   public void createService_deliveryChannel(String deliveryChannel) throws InterruptedException, IOException {
			
			boolean deliveryChanel=false;
		try {	
			deliveryChanel = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey);
		//	sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
			if(deliveryChanel) {
				Report.LogInfo("Info", " 'Delivery Channel' dropdown is displaying as expected","PASS");
			if (!deliveryChannel.equalsIgnoreCase("null")) {

				click(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey);
				waitforPagetobeenable();
				webDriver.findElement(By.xpath("//div[contains(text(),'" + deliveryChannel + "')]")).click();
				waitforPagetobeenable();
				Report.LogInfo("Info", deliveryChannel + " is selected under 'Delivery channel' dropdown","PASS");

			}else {
				Report.LogInfo("Info","No value selected under 'Delivery channel' dropdown","PASS"); 
			}
			}else {
				Report.LogInfo("Info", " 'Delivery channel' dropdown is not dispalying under 'create Serice' page","FAIL");
			}
		  }catch(NoSuchElementException e) {
			  e.printStackTrace();
			  Reporter.log(" 'Delivery channel' dropdown is not dispalying");
			  Report.LogInfo("Info", " 'Delivery channel' dropdown is not dispalying under 'create Serice' page","FAIL");
		  }catch(Exception err) {
			  err.printStackTrace();
			  Report.LogInfo("Info", " Not able to selected value under 'Delivery channel' dropodwn","FAIL");
		  }
		}
	   
	   
	   public void createService_managementOptions(String Manageconnectiondropdown) throws InterruptedException, IOException {
			
			boolean Managementorder=false;
		try {	
			Managementorder = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.managementConnection);
		//	sa.assertTrue(Managementorder, "Management order field is not displayed");
			if(Managementorder) {
				Report.LogInfo("Info", " ' Management Order' dropdown is displaying as expected","PASS");
			if (!Manageconnectiondropdown.equalsIgnoreCase("null")) {
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.managementConnection);
				waitforPagetobeenable();
				webDriver.findElement(By.xpath("//div[contains(text(),'" + Manageconnectiondropdown + "')][1]")).click();

				waitforPagetobeenable();
				
				Report.LogInfo("Info", Manageconnectiondropdown + " is selected under 'management Order' field","PASS");
			
			}else {
				Report.LogInfo("Info","Values not entered under 'Management Order' field","PASS");
			}
			}else {
				Report.LogInfo("Info", " ' Management Order' dropdown is not displaying under 'Create Service' page","FAIL");
			}
		  }catch(NoSuchElementException e) {
			  e.printStackTrace();
			  Reporter.log(" 'Management order' dropdown is not displaying");
			  Report.LogInfo("Info", " ' Management Order' dropdown is not displaying under 'Create Service' page","FAIL");
		  }catch(Exception err) {
			  err.printStackTrace();
			  Report.LogInfo("Info", " Not able to select value under 'management Order' dropdown","FAIL");
		  }
		}

	   public void device_vendorModelWarningMessage() throws InterruptedException, IOException {
			boolean vendorErr=false;
			//Vendor/Model Error Message
					try {
						vendorErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_vendorErrmsg);
				//	sa.assertTrue(vendorErr, "Vendor/Model warning message is not displayed ");
					if(vendorErr) {
					String vendorErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_vendorErrmsg);
					Reporter.log(
							"Vendor/Model  message displayed as : " + vendorErrMsg);
					Reporter.log("Vendor/Model warning message displayed as : " + vendorErrMsg);
					}
					}catch(NoSuchElementException e) {
						e.printStackTrace();
						Reporter.log("Vendor/Mdel warning message is not dipslaying");
					}catch(Exception ed) {
						ed.printStackTrace();
					}
		}
	   
	   public void device_managementAddressWarningMessage() throws InterruptedException, IOException {
		   
		   boolean mangadrsErr=false;
		   try {
			   mangadrsErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_managementAddresserrmsg);
			//	sa.assertTrue(mangadrsErr, "Management Addres warning message is not displayed ");
				if(mangadrsErr) {
				String mngadresErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_managementAddresserrmsg);
				Reporter.log(
						"Management Addres  message displayed as : " + mngadresErrMsg);
				
				Reporter.log("Management Addres warning message displayed as : " + mngadresErrMsg);
				}
			 }catch(NoSuchElementException e) {
				 e.printStackTrace();
				 Reporter.log("management Address warning message is not found");
			 }catch(Exception ed) {
				 ed.printStackTrace();
			 }
	}			

	   public void device_powerAlarmWarningMessage() throws InterruptedException{
		   boolean pwralrmErr=false;
		   try {
			   pwralrmErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_powerAlarmerrmsg);
			//	sa.assertTrue(pwralrmErr, "Power Alarm warning message is not displayed ");
				if(pwralrmErr) {
				String pwralarmErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_powerAlarmerrmsg);
				Reporter.log(
						"Power Alarm  message displayed as : " + pwralarmErrMsg);
				Reporter.log("Power Alarm warning message displayed as : " + pwralarmErrMsg);
				}
			 }catch(NoSuchElementException e) {
				 e.printStackTrace();
				 Reporter.log("Power Alarm warning message is not dipslaying");
			 }catch(Exception er) {
				 er.printStackTrace();
			 }
	}	
	   
	   public void device_serialNumberWarningMessage() throws InterruptedException, IOException {
			 //Serial Number Error Message
			   boolean serialNumberErr=false;
			   
			   try {
				serialNumberErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialNumberErrmsg);
				//sa.assertTrue(serialNumberErr, "Serial Number warning message is not displayed ");
			if(serialNumberErr)	{
				Reporter.log(" 'Serial number; warning message is dipslaying as expected");
				String serialnumberErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialNumberErrmsg);
				Reporter.log(
						"Serial Number  message displayed as : " + serialnumberErrMsg);
					Reporter.log("Serial Number warning message displayed as : " + serialnumberErrMsg);
			}else {
				Reporter.log("Serial Number warning message is not dipslaying");
			}
			   }catch(NoSuchElementException e) {
				   e.printStackTrace();
				   Reporter.log("Serial Number Warning message is not diplsying");
			   }
		 }
	   
	   public void device_hexaSerialNumberWarningMessage() throws InterruptedException, IOException {
			 //Serial Number Error Message
			   boolean HexaserialNumberErr=false;
			   
			   try {
				   HexaserialNumberErr = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_hexaSerialNumnerErrmsg);
			//	sa.assertTrue(HexaserialNumberErr, "Hexa Serial Number warning message is not displayed ");
				if(HexaserialNumberErr)	{
					Reporter.log(" 'Hexa Serial number' warning message is dipslaying as expected");
					String hexaserialnumberErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_hexaSerialNumnerErrmsg);
					Reporter.log(
							"Hexa Serial Number  message displayed as : " + hexaserialnumberErrMsg);
					Reporter.log("Hexa Serial Number warning message displayed as : " + hexaserialnumberErrMsg);
				}else {
					Reporter.log("Hexa Serial Number warning message is not dipslaying");
				}
			   }catch(NoSuchElementException e) {
				   e.printStackTrace();
				   Reporter.log("Serial Number Warning message is not diplsying");
			   }
		  }
	   
	   public void addCityToggleButton() throws InterruptedException, IOException
		{
			
			//Add city Toggle button
			click(Lanlink_Outbandmanagement.Outbandmanagement.addsiteswitch);
			waitforPagetobeenable();
		}
	   
	   public void addPremiseTogglebutton() throws InterruptedException, IOException {
			
			//Add Premise Toggle button
			click(Lanlink_Outbandmanagement.Outbandmanagement.addpremiseswitch);
			waitforPagetobeenable();
		}
	   
	   public void addSiteToggleButton() throws InterruptedException, IOException {
			((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			
			//Add Site Toggle button
			click(Lanlink_Outbandmanagement.Outbandmanagement.addsiteswitch);
			waitforPagetobeenable();
		}
	   
	   public void newPremise_clickonSiteToggleButton(String newpremiseselectionmode, String premisename, String premisecode) throws InterruptedException, IOException {
			
			//New premise 
			 if(newpremiseselectionmode.equalsIgnoreCase("yes")) {
				 
				if(premisename.equalsIgnoreCase("null")) {
					 Report.LogInfo("Info", " value for Premise Name field  is not entered","PASS");
				}else {
					//Premise Name Field	
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton, premisename,"premisename");
					waitforPagetobeenable();
					String prmsenme=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisenamefield_sitetogglebutton,"value");
					waitforPagetobeenable();
					Reporter.log("Entered Premise Name is : " + prmsenme);
					 Report.LogInfo("Info",
							"Entered Premise Name is : " + prmsenme,"PASS");
				}
				
				if(premisecode.equalsIgnoreCase("null")) {
					 Report.LogInfo("Info", " value for Premise code field  is not entered","FAIL");
				}else {
					//Premise Code Field	
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton, premisecode,"premisename");
					waitforPagetobeenable();
					String premisecde=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEforIntermediateEquip_premisecodefield_sitetogglebutton,"value");
					waitforPagetobeenable();
					Reporter.log("Entered Premise Code is : " + premisecde);
					 Report.LogInfo("Info",
							"Entered Premise Code is : " + premisecde,"PASS");
				}	
				 
				 
			 }else {
				 Report.LogInfo("Info", " Add new Premise is not selected","FAIL");
			 }
		}
	   
	   
	   public void device_okbutton() throws InterruptedException {

			// OK button
			boolean Ok = false;
			try {
				Ok = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton).isDisplayed();
			//	sa.assertTrue(Ok, "OK button under add device is not available");

				if (Ok) {
					Reporter.log(" 'OK' button is displaying under 'Add CPE deivce' page as expected");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'OK' button is displaying under 'Add CPE deivce' page as expected");
				} else {
					Reporter.log(" 'OK' button is not displaying under 'Add CPE device' page");
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'OK' button is not displaying under 'Add CPE device' page");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " OK button is not available in 'Add CPE Device' page");
				Reporter.log(" OK button is not available in 'Add CPE Device' page");
			}

		}
	   
	   
	   public void device_cancelButton() throws InterruptedException {

			// Cancel button
			boolean cancel = false;
			try {
				cancel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_cancelbutton)
						.isDisplayed();
			//	sa.assertTrue(cancel, "cancel button under add device is not available");

				if (cancel) {
					Reporter.log(" 'Cancel' button is displaying under 'Add CPE deivce' page as expected");
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Cancel' button is displaying under 'Add CPE deivce' page as expected");
				} else {
					Reporter.log(" 'Cancel' button is not displaying under 'Add CPE device' page");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Cancel' button is not displaying under 'Add CPE device' page");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Cancel button is not available in 'Add CPE Device' page");
				Reporter.log(" Cancel button is not available in 'Add CPE Device' page");
			}
		}
	   
	   
	   public void device_editnamefield(String cpename) {

			 boolean name=false;
				try {
					
					name=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_Name);
					waitforPagetobeenable();
					 
			if(name) {	
				
				if(cpename.equalsIgnoreCase("null")) {
					
					
				}else {
					clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_Name);
					waitforPagetobeenable();
					
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_Name, cpename,"CPE Name");
					waitforPagetobeenable();
					 
					String actualValue_Name=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_Name,"value");
				}
				
			}else {
			}
				}catch(NoSuchElementException e) {
					e.printStackTrace();
				}catch(Exception err) {
					err.printStackTrace();
				}
		}
	   
	   public void device_editVendorModelField(String vender) {
			
			boolean vend0r=false;
			try {	
				
				vend0r=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_vender).isDisplayed();
				waitforPagetobeenable();
			if(vend0r) {
				
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'Vendor/Model' dropdown is displaying in 'Edit CPE Device' page");
				Reporter.log(" 'Vendor/Model' dropdown is displaying in 'Edit CPE Device' page");
				
				if(vender.equalsIgnoreCase("null")) {
					ExtentTestManager.getTest().log(LogStatus.PASS, "No changes made for 'Vender/Model' dropdown while editing");
					
				}else {
					click(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevice_vendoModel_xbutton,"EditCPEdevice_vendoModel_xbutton");
					waitforPagetobeenable();
					
					webDriver.findElement(By.xpath("//div[label[text()='Vendor/Model']]//div[text()='"+vender +"']")).click();
					ExtentTestManager.getTest().log(LogStatus.PASS, vender+ " is the edited value for 'vender/Model' field");
					Reporter.log(vender+ " is the edited value for 'vender/Model' field");
				}
			  }else {
				  ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Vendor/Model' dropdown is not available in 'Edit CPE Device' page");
			  }
			}catch(Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Vender/Model' dropdown is not available");
				Reporter.log("'Vender/Model' dropdown is not available");
			}
		}
	   
	   public void device_editSnmproField() {
			
			boolean sNmpro=false;
			try {
				
			  sNmpro=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_snmpro);
			  
			 if(sNmpro) { 
				// ' Snmpro' field is displaying in 'Edit CPE Device' page as expected
				 
				  boolean actualValue_snmpro=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_snmpro_autoPopulatedValue);
				  if(actualValue_snmpro) {
					 // 'Snmpro' field value is displaying as expected
					  
				  }else {
					// 'Snmpro' value is not displaying as expected."
				  }
				  
				  
			 }else {
				//'Snmpro' field is not available in 'Edit CPE Device' page
			 }
			}catch(NoSuchElementException e) {
				e.printStackTrace();
			}catch(Exception err) {
				err.printStackTrace();
			}
		}
	   
	   
	   public void device_editManagementAddressField(String managementAddress) {

			boolean manageAddressAvailability=false;
			try {
				
//				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_selectSubnettogglebutton);
//				waitforPagetobeenable();
				
				manageAddressAvailability=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_manageaddress);
				waitforPagetobeenable();
				
				if(manageAddressAvailability) {
					
					if(managementAddress.equalsIgnoreCase("null")) {
						
						
					}else {
						
						click(Lanlink_Outbandmanagement.Outbandmanagement.changeButton_managementAddress);
						waitforPagetobeenable();
						
						clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_manageaddress);
						waitforPagetobeenable();
				sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_manageaddress,managementAddress);
				waitforPagetobeenable();
					}
					
				}
				
			}catch(NoSuchElementException e) {
				e.printStackTrace();
			}catch(Exception err) {
				err.printStackTrace();
			}
		}
	   
	   
	   public void device_editMEPIdField(String Mepid) {

			boolean mepIdavailability=false;
			try {
				mepIdavailability =isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mepid);
			if(mepIdavailability) {	
				
				if(Mepid.equalsIgnoreCase("null")) {
				}else {
					clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mepid);
					waitforPagetobeenable();
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mepid, Mepid, "MEP Id");
					waitforPagetobeenable();
				}
			  }
			}catch(NoSuchElementException e) {
				e.printStackTrace();
			}catch(Exception err) {
				err.printStackTrace();
			}
		}
	   
	   public void device_editPowerAlarm(String poweralarm) throws InterruptedException, IOException {

			addDropdownValues_commonMethod("Power Alarm",Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_poweralarm, poweralarm);
		    
		}
	   
	   
	   public void device_editserialnumber(String serialNumber) {

			 boolean serialunmberAvailability=false;
				try {
					
					serialunmberAvailability=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialnumber);
					waitforPagetobeenable();
					 
			if(serialunmberAvailability) {	
				// 'Serial Number' field is displaying under 'Edit CPE device' page
				
				if(serialNumber.equalsIgnoreCase("null")) {
					
					// "No changes made for 'Serial Number' field while editing
					
				}else {
					clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialnumber);
					waitforPagetobeenable();
					
					sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialnumber, serialNumber,"Serial Number");
					waitforPagetobeenable();
					 
					String actualValue_Name=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_serialnumber,"value");
				}
				
			}else {
				//'Serial Number' field is not available under 'Edit CPE device' page
			}
				}catch(NoSuchElementException e) {
					e.printStackTrace();
				}catch(Exception err) {
					err.printStackTrace();
				}
		}	
	   
	   
	   public void device_editlinkLostforwarding(String linkLostForwarding) {

			 boolean linklostcheckbox=false;
			    try { 
			    	linklostcheckbox=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
			    	
			    if(linklostcheckbox) {	
			    	
			    	
			    	if (linkLostForwarding.equalsIgnoreCase("null")) {
			    		
			    		//No changes made for linklost forwarding while editing cpe device under equipment
			    	}else {
			    
			    		boolean linklost=isVisible(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
			    		
			    		if (linkLostForwarding.equalsIgnoreCase("yes")) {
					
			    			if(linklost) {
			    				//linklost forwarding has been edited for cpe device under Equipment
			    				
			    			}else {
			    				
			    				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
			    			}
			    			
					
			    		}else if(linkLostForwarding.equalsIgnoreCase("no")) {
			    			
			    			if(linklost) {
			    				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
			    				
			    			}else {
			    				
			    				//linklost forwarding has been edited for cpe device under Equipment
			    			}
			    			
			    		}
			    	}	
			    }else {
			    }
					}catch (NoSuchElementException e) {
					    e.printStackTrace();
					}catch(Exception er) {
						er.printStackTrace();
					}
		}
	   
	   
	   public void viewdevice_Overture(String cpename, String vender, String snmpro,
				String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
				String linkLostForwarding, String existingcountry, String existingCity, String newCity, String existingSite,
				String newSite, String existingPremise, String newPremise, String existingcityselectionmode,
				String newcityselectionmode, String existingsiteselectionmode, String newsiteselectionmode,
				String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue,
				String existingpremiseselectionmode, String newpremiseselectionmode) throws InterruptedException, IOException {

			ExtentTestManager.getTest().log(LogStatus.INFO, "verify the details entered while creating device");

			String[] RouterId = new String[2];
			RouterId = cpename.split(".lanlink");

			String RouterIdValue = RouterId[0];

			String mediaSelectionValueInViewDevicePage = "no";
			if (Mediaselection.equalsIgnoreCase("null")) {
				Mediaselection = mediaSelectionValueInViewDevicePage;
			} else {
				Mediaselection = mediaSelectionValueInViewDevicePage;
			}

			verifyEnteredvalues_deviceName("Name", RouterIdValue, cpename);

			verifyEnteredvalues("Router Id", RouterIdValue);

			verifyEnteredvalues("Vendor/Model", vender);

			verifyEnteredvalues("Snmpro", snmpro);

			// Management Address
			if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
			} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
					&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("Management Address", managementAddress);
			}

//		  verifyEnteredvalues("MEP Id", Mepid);

			verifyEnteredvalues("Power Alarm", poweralarm);

			verifyEnteredvalues("Media Selection", Mediaselection);

			verifyEnteredvalues("MAC Address", Macaddress);

			verifyEnteredvalues("Link Lost Forwarding", linkLostForwarding);

			verifyEnteredvalues("Country", existingcountry);

			// City
			if ((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("City", existingCity);
			} else if ((existingcityselectionmode.equalsIgnoreCase("no"))
					&& (newcityselectionmode.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("City", newCity);
			}

			// Site
			if ((existingsiteselectionmode.equalsIgnoreCase("Yes")) || (newsiteselectionmode.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("Site", existingSite);
			} else if ((existingsiteselectionmode.equalsIgnoreCase("no"))
					&& (newsiteselectionmode.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("Site", newSite);
			}

			// Premise
			if ((existingpremiseselectionmode.equalsIgnoreCase("Yes"))
					&& (newpremiseselectionmode.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("Premise", existingPremise);
			} else if ((existingpremiseselectionmode.equalsIgnoreCase("no"))
					&& (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("Premise", newPremise);
			}

		}
	   
	   
	   
	   public void verifyEnteredvalues_deviceName(String label, String expectedValue, String devicename)
				throws InterruptedException {

			try {

				boolean deviceName = webDriver.findElement(By.xpath("//div[div[label[text()='Name']]]//div[contains(text(),'" + expectedValue + "')]")).isDisplayed();

				if (deviceName) {
					Reporter.log("device name is displaying as expected");

					ExtentTestManager.getTest().log(LogStatus.PASS,
							"Device name is displaying as:  " + devicename + "as expected");
				} else {

					WebElement Actualvalue =  webDriver.findElement(By.xpath("//div[div[label[text()='Name']]]//div[2]"));
					Reporter.log("Device name is not displaying as expected");
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Device name is displaying as:  " + Actualvalue.getText());
				}
			} catch (AssertionError err) {
				err.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, label + " value is not displaying as expected ");
				Reporter.log(label + " value is not displaying as expected ");
			} catch (NoSuchElementException e) {
				Reporter.log("value not displayed for " + label);

				ExtentTestManager.getTest().log(LogStatus.FAIL, "value not displayed for : " + label);

			}
		}
	   
	   
	   public void viewdevice_Accedian( String cpename, String vender, String snmpro,
				String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
				String serialNumber, String hexaSerialnumber, String linkLostForwarding, String existingcountry,
				String existingCity, String newCity, String existingSite, String newSite, String existingPremise,
				String newPremise, String existingcityselectionmode, String newcityselectionmode,
				String existingsiteselectionmode, String newsiteselectionmode, String newmanagementAddress,
				String existingmanagementAddress, String manageaddressdropdownvalue, String existingpremiseselectionmode,
				String newpremiseselectionmode) throws InterruptedException, IOException {

			ExtentTestManager.getTest().log(LogStatus.INFO, "verify the details entered while creating device");

			String[] RouterId = new String[2];
			RouterId = cpename.split(".lanlink");

			String RouterIdValue = RouterId[0];

			String mediaSelectionValueInViewDevicePage = "no";
			if (Mediaselection.equalsIgnoreCase("null")) {
				Mediaselection = mediaSelectionValueInViewDevicePage;
			} else {
				Mediaselection = mediaSelectionValueInViewDevicePage;
			}

			verifyEnteredvalues_deviceName("Name", RouterIdValue, cpename);

			verifyEnteredvalues("Router Id", RouterIdValue);

			verifyEnteredvalues("Vendor/Model", vender);

			verifyEnteredvalues("Snmpro", snmpro);

			// Management Address
			if ((existingmanagementAddress.equalsIgnoreCase("Yes")) && (newmanagementAddress.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("Management Address", manageaddressdropdownvalue);
			} else if ((existingmanagementAddress.equalsIgnoreCase("no"))
					&& (newmanagementAddress.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("Management Address", managementAddress);
			}

			verifyEnteredvalues("Power Alarm", poweralarm);

			verifyEnteredvalues("Media Selection", Mediaselection);

//			  verifyEnteredvalues("MAC Address", hexaSerialnumber);

			verifyEnteredvalues("Serial Number", serialNumber);

//			  verifyEnteredvalues("Link Lost Forwarding", linkLostForwarding);

			// City
			if ((existingcityselectionmode.equalsIgnoreCase("Yes")) && (newcityselectionmode.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("City", existingCity);
			} else if ((existingcityselectionmode.equalsIgnoreCase("no"))
					&& (newcityselectionmode.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("City", newCity);
			}

			// Site
			if ((existingsiteselectionmode.equalsIgnoreCase("Yes")) && (newsiteselectionmode.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("Site", existingSite);
			} else if ((existingsiteselectionmode.equalsIgnoreCase("no"))
					&& (newsiteselectionmode.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("Site", newSite);
			}

			// Premise
			if ((existingpremiseselectionmode.equalsIgnoreCase("Yes"))
					&& (newpremiseselectionmode.equalsIgnoreCase("no"))) {
				verifyEnteredvalues("Premise", existingPremise);
			} else if ((existingpremiseselectionmode.equalsIgnoreCase("no"))
					&& (newpremiseselectionmode.equalsIgnoreCase("Yes"))) {
				verifyEnteredvalues("Premise", newPremise);
			}
		}
	   
	   
	   
	   public void eDITCPEdevicedetailsentered_1G_Overture(String cpedevicename, String vender, String snmpro,
				String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
				String serialNumber, String hexaSerialnumber, String linkLostForwarding, String Country,
				String ExistingCitySelection, String NewCitySelection, String existingCity, String newCityName, String newCityCode,
				String ExistingSiteSelection, String NewSiteSelection, String ExistingSite, String NewSiteName, String NewSiteCode,
				String ExistingPremiseSelection, String newPremiseselection, String existingPremise, String newPremiseName, String newPremiseCode)
				throws InterruptedException, IOException {
			
			ExtentTestManager.getTest().log(LogStatus.INFO, "edit CPE device");
			
			Reporter.log("Entered edit functionalitty");

			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown);
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown, "Action");
			click(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown, "Action");
			
			waitforPagetobeenable();
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevicelinkunderviewpage,"Edit CPE Devicelinkunderviewpage");
			click(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevicelinkunderviewpage,"Edit CPE Devicelinkunderviewpage");
			
			waitforPagetobeenable();
			Reporter.log("edit functionality worked");
			
		     
		//Name field
			device_editnamefield(cpedevicename);
			
		//vendor/model
			device_editVendorModelField(vender);
			
		//Snmpro
			device_editSnmproField();

		//Management address
			device_editManagementAddressField(managementAddress);
			
		//Mepid	
			device_editMEPIdField( Mepid);
			
		//power alarm	
			device_editPowerAlarm(poweralarm);
			
			//Media Selection  
			device_editMediaselection(Mediaselection);
		    
			
			WebElement countrylabelname=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.countrylabelname_IntEquipment);
			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.countrylabelname_IntEquipment);
		waitforPagetobeenable();
		
		    
		//Mac address  
			device_editMACaddress( Macaddress);
		    
		//linklost forwarding	
			device_editlinkLostforwarding(linkLostForwarding);
		
		//	scrolltoend();
			waitforPagetobeenable();
			
			//Country
					if(!Country.equalsIgnoreCase("Null")) {
						
						selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_selectTag, "Country", Country);
						
						//New City		
						if(ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
							click(Lanlink_Outbandmanagement.Outbandmanagement.addcityswitch);
						   //City name
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.citynameinputfield, newCityName, "City Name");
						   //City Code	
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.citycodeinputfield, newCityCode, "City Code");
						   //Site name
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitenameinputfield_addCityToggleSelected, NewSiteName, "Site Name");
						   //Site Code
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitecodeinputfield_addCityToggleSelected, NewSiteCode, "Site Code");
						   //Premise name	
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addCityToggleSelected, newPremiseName, "Premise Name");
						   //Premise Code	
							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addCityToggleSelected, newPremiseCode, "Premise Code");
								
						}	
					
					//Existing City	
						else if(ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {
							
						   selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.cityDropdown_selectTag, "City", existingCity);
							
							
						 //Existing Site
							  if(ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
								  selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.siteDropdown_selectTag, "Site", ExistingSite);
								  
							 //Existing Premise
								  if(ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
									  selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.premiseDropdown_selectTag, "Premise", existingPremise);
						          	 }
								  
								//New Premise  
								  else if(ExistingPremiseSelection.equalsIgnoreCase("no") & newPremiseselection.equalsIgnoreCase("yes")) {
									  
									  click(Lanlink_Outbandmanagement.Outbandmanagement.addpremiseswitch);
									  //Premise name	
										sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addPremiseToggleSelected, newPremiseName, "Premise Name");
									   //Premise Code	
									  sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addPremiseToggleSelected, newPremiseCode, "Premise Code");
								  } 
					         	}
				  		
						  else if(ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {
							  	
							  click(Lanlink_Outbandmanagement.Outbandmanagement.addsiteswitch);
							  //Site name
								sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitenameinputfield_addSiteToggleSelected, NewSiteName, "Site Name");
							   //Site Code
								sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitecodeinputfield_addSiteToggleSelected, NewSiteCode,  "Site Code");
								
							   //Premise name	
								sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addSiteToggleSelected, newPremiseName, "Premise Name");
							   //Premise Code	
								sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addSiteToggleSelected, newPremiseCode, "Premise Code");
							  }
						}
						
					}
					else if(Country.equalsIgnoreCase("Null")) {
						
						ExtentTestManager.getTest().log(LogStatus.PASS, " No changes made for 'Country' dropdown");
					
					//City	
						editCity(ExistingCitySelection, NewCitySelection, Lanlink_Outbandmanagement.Outbandmanagement.cityDropdown_selectTag, "selectcityswitch", "addcityswitch",
								existingCity, newCityName, newCityCode, "City");
						
						
					//Site	
						editSite(ExistingSiteSelection, NewSiteSelection, Lanlink_Outbandmanagement.Outbandmanagement.siteDropdown_selectTag, "selectsiteswitch",
								"addsiteswitch", ExistingSite , NewSiteName, NewSiteCode, "Site");
						
					//Premise
						editPremise(ExistingPremiseSelection, newPremiseselection, Lanlink_Outbandmanagement.Outbandmanagement.premiseDropdown_selectTag, "selectpremiseswitch",
								"addpremiseswitch", existingPremise, newPremiseName, newPremiseCode, "Premise");
						
					}

					scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
			
		    
			click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,  "OK");
			waitforPagetobeenable();
		}
	   
	   
	   public void device_editMediaselection(String Mediaselection)
				throws InterruptedException {

			boolean mediaSelection1 = false;
			try {

				mediaSelection1 = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_mediaselection).isDisplayed();
				if (mediaSelection1) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Media Selection' dropdown is displaying in 'Edit CPE device' page as expected");
					Reporter.log(" 'Media Selection' dropdown is displaying in 'Edit CPE device' page as expected");
					if (Mediaselection.equalsIgnoreCase("null")) {

						ExtentTestManager.getTest().log(LogStatus.PASS,
								"No changes made for 'Media selection' dropdown while editing cpe device under Equipment");
						Reporter.log("No changes made for 'Media selection' dropdown while editing cpe device under Equipment");

					} else {

						click(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevice_mediaselection_xbutton);
						Thread.sleep(5000);

						webDriver.findElement(By.xpath("//div[label[text()='Media Selection']]//div[text()='" + Mediaselection + "']")).click();
						Thread.sleep(3000);
						ExtentTestManager.getTest().log(LogStatus.PASS,
								Mediaselection + " is the edited value for 'Media Selection' field");
						Reporter.log(Mediaselection + " is the edited value for 'Media Selection' field");
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Media selection' dropdown is not avilable in 'Edit CPE device' page");
					Reporter.log(" 'Media selection' dropdown is not avilable in 'Edit CPE device' page");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, "'Media selection' mandatory dropdown is not available");
				Reporter.log("'Media selection' mandatory dropdown is not available");
			} catch (Exception er) {
				er.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value in 'Media Selection' field");
				Reporter.log(" Not able to enter value in 'Media Selection' field");

			}
		}
	   
	   
	   public void device_editMACaddress(String Macaddress) {

			boolean macAddress = false;
			try {
				macAddress = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macaddress)
						.isDisplayed();

				if (macAddress) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'MAC Address' field is displaying in 'Edit CPE device' page as expected");
					Reporter.log(" 'MAC Address' field is displaying in 'Edit CPE device' page as expected");
					if (Macaddress.equalsIgnoreCase("null")) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"No changes made for 'Mac address' field while editing cpe device under Equipment");
						Reporter.log("No changes made for 'Mac address' field while editing cpe device under Equipment");
					} else {
						clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macaddress);
						waitforPagetobeenable();
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macaddress,Macaddress,"Macaddress");
						String actualValue_MacAddress = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macaddress,"value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								actualValue_MacAddress + " is the edited value for 'Macaddress' field");
						Reporter.log(actualValue_MacAddress + " is the edited value for 'Macaddress' field");
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'MAC Address' field is not available in 'Edit CPE device' page");
					Reporter.log(" 'MAC Address' field is not available in 'Edit CPE device' page");
				}
			} catch (NoSuchElementException e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Mac Address' field is not available ");
			} catch (Exception err) {
				err.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to enter value in 'Mac Address' field");
			}
		}
	   
	   
	   
	   public void eDITCPEdevicedetailsentered_1G_Accedian(String cpedevicename, String vender, String snmpro,
				String managementAddress, String Mepid, String poweralarm, String Mediaselection, String Macaddress,
				String serialNumber, String hexaSerialnumber, String linkLostForwarding, String Country,
				String ExistingCitySelection, String NewCitySelection, String existingCity, String newCityName, String newCityCode,
				String ExistingSiteSelection, String NewSiteSelection, String ExistingSite, String NewSiteName, String NewSiteCode,
				String ExistingPremiseSelection, String newPremiseselection, String existingPremise, String newPremiseName, String newPremiseCode)
				throws InterruptedException, IOException {
		   
		   

		   Reporter.log("Entered edit functionalitty");

  			((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
  			
  			click(Lanlink_Outbandmanagement.Outbandmanagement.viewPCEdevice_Actiondropdown, "Action");
  			
  			waitforPagetobeenable();
  			
  			click(Lanlink_Outbandmanagement.Outbandmanagement.EditCPEdevicelinkunderviewpage);
  			waitforPagetobeenable();
  			Reporter.log("edit functionality worked");
  			
  		     
  		//Name field
  			device_editnamefield(cpedevicename);
  			
  		//vendor/model
  			device_editVendorModelField(vender);
  			
  		//Snmpro
  			device_editSnmproField();

  		//Management address
  			device_editManagementAddressField(managementAddress);
  			
  		//Mepid	
  			device_editMEPIdField(Mepid);
  			
  		//power alarm	
  			device_editPowerAlarm(poweralarm);
  		
  		//Serial Number
  			device_editserialnumber(serialNumber);
  		    
  		    
  		//linklost forwarding	
//  			device_editlinkLostforwarding(linkLostForwarding);
  		    
  		
  	   		((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

  			waitforPagetobeenable();
  			
  			//Country
  			if(!Country.equalsIgnoreCase("Null")) {
  				
  				//click(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_selectTag,Country);
  				selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.countryDropdown_selectTag, "Country", Country);

  				
  				//New City		
  				if(ExistingCitySelection.equalsIgnoreCase("no") & NewCitySelection.equalsIgnoreCase("yes")) {
  				
  					click(Lanlink_Outbandmanagement.Outbandmanagement.addcityswitch);
					   //City name
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.citynameinputfield, newCityName, "City Name");
					   //City Code	
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.citycodeinputfield, newCityCode, "City Code");
					   //Site name
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitenameinputfield_addCityToggleSelected, NewSiteName, "Site Name");
					   //Site Code
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitecodeinputfield_addCityToggleSelected, NewSiteCode, "Site Code");
					   //Premise name	
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addCityToggleSelected, newPremiseName, "Premise Name");
					   //Premise Code	
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addCityToggleSelected, newPremiseCode, "Premise Code");
							
  				  	 }	
  			
  			//Existing City	
				else if(ExistingCitySelection.equalsIgnoreCase("yes") & NewCitySelection.equalsIgnoreCase("no")) {
					
					   selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.cityDropdown_selectTag, "City", existingCity);

					
				 //Existing Site
					  if(ExistingSiteSelection.equalsIgnoreCase("yes") & NewSiteSelection.equalsIgnoreCase("no")) {
						  selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.siteDropdown_selectTag, "Site", ExistingSite);
						  
						  
					 //Existing Premise
						  if(ExistingPremiseSelection.equalsIgnoreCase("yes") & newPremiseselection.equalsIgnoreCase("no")) {
							 selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.premiseDropdown_selectTag, "Premise", existingPremise);
							  
				          	 }
						  
						//New Premise  
						  else if(ExistingPremiseSelection.equalsIgnoreCase("no") & newPremiseselection.equalsIgnoreCase("yes")) {
							  
							  click(Lanlink_Outbandmanagement.Outbandmanagement.addpremiseswitch);
							  //Premise name	
							  sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addPremiseToggleSelected, newPremiseName, "Premise Name");
							   //Premise Code	
							  sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addPremiseToggleSelected, newPremiseCode, "Premise Code");
						  } 
			         	}
		  		
				  else if(ExistingSiteSelection.equalsIgnoreCase("no") & NewSiteSelection.equalsIgnoreCase("yes")) {
					  	
					  click(Lanlink_Outbandmanagement.Outbandmanagement.addsiteswitch);
					  //Site name
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitenameinputfield_addSiteToggleSelected, NewSiteName,"Site Name");
					   //Site Code
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.sitecodeinputfield_addSiteToggleSelected, NewSiteCode, "Site Code");
						
					   //Premise name	
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisenameinputfield_addSiteToggleSelected, newPremiseName, "Premise Name");
					   //Premise Code	
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premisecodeinputfield_addSiteToggleSelected, newPremiseCode, "Premise Code");
					  }
				}
				
			}
			else if(Country.equalsIgnoreCase("Null")) {
				
			
			//City	
				editCity(ExistingCitySelection, NewCitySelection, Lanlink_Outbandmanagement.Outbandmanagement.cityDropdown_selectTag, "selectcityswitch", "addcityswitch",
						existingCity, newCityName, newCityCode, "City");  //xpath  -  cityDropdown_selectTag
				
				
			//Site	
				editSite(ExistingSiteSelection, NewSiteSelection,  Lanlink_Outbandmanagement.Outbandmanagement.siteDropdown_selectTag, "selectsiteswitch",
						"addsiteswitch", ExistingSite , NewSiteName, NewSiteCode, "Site");  //xpath  -  siteDropdown_selectTag
				
			//Premise
				editPremise(ExistingPremiseSelection, newPremiseselection,  Lanlink_Outbandmanagement.Outbandmanagement.premiseDropdown_selectTag, "selectpremiseswitch",
						"addpremiseswitch", existingPremise, newPremiseName, newPremiseCode, "Premise");    //xpath  -  premiseDropdown_selectTag
				
			}
           scrollDown( Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
			
			click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton,"OK");
			waitforPagetobeenable();
			
		
		
		}
	   
	   
	   public void device_editlinklostforwarding_10G() {

			 boolean linklostcheckboxAvailability=false;
			 boolean linklostcheckboxEnabled=false;
			    try { 
			    	linklostcheckboxAvailability=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
			    	
			    if(linklostcheckboxAvailability) {	
			    	//'Link lost Forwarding' checkbox is displaying in 'Edit CPE device' page as expected
			    	
			    	linklostcheckboxEnabled=isEnable(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_linklostforowarding);
			    	if(linklostcheckboxEnabled) {
			    		Reporter.log(" 'link lostforwarding is enabled for 10G");
			    		
			    	}else {
			    		Reporter.log("link lost checkbox is disabled as expected");
			    	}
			    	
			    }else {
			    	//'Link Lost Forwarding' checkbox is not available in 'Edit CPE device' page
			    }
					}catch (NoSuchElementException e) {
					    e.printStackTrace();
					}catch(Exception er) {
						er.printStackTrace();
					}
		
		}
	   
	   
	   public void existingSite(String site) throws InterruptedException, IOException {
			
			selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.siteDropdown_selectTag, "Site", site);
			
		}
	   
	   public void existingPremise(String premise) throws InterruptedException, IOException {
			
			selectValueInsideDropdown(Lanlink_Outbandmanagement.Outbandmanagement.premiseDropdown_selectTag, "Premise", premise);
			
		}
	   
	   
	   public void editCity(String editExistingCity, String editNewCity, String dropdown_xpath,
				String selectCityToggleButton, String addCityToggleButton, String dropdownValue, String editNewCityName,
				String editNewCityCode, String labelname) throws InterruptedException, IOException {
			if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
				existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
			} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
				existingCity(dropdown_xpath, dropdownValue, selectCityToggleButton, labelname);
			} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
				newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
			} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
				newCity(dropdown_xpath, addCityToggleButton, editNewCityName, editNewCityCode);
			}

			else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
				Report.LogInfo("EditCity", "No chnges made under 'City' field", "PASS");
			}

		}
	   
	   public void existingCity(String dropdown_xpath, String dropdownValue, String selectCityToggleButton,
				String labelname) throws InterruptedException, IOException {

			if (isVisible(dropdown_xpath)) {

				selectByValue(dropdown_xpath, dropdownValue, labelname);
				// addDropdownValues_commonMethod(application, labelname,
				// dropdown_xpath, dropdownValue, xml);

			} else {

				click(selectCityToggleButton);
				waitforPagetobeenable();
				// click_commonMethod(application, "Select City toggle button",
				// selectCityToggleButton, xml);
				// Thread.sleep(1000);

				selectByValue(dropdown_xpath, dropdownValue, labelname);
				// addDropdownValues_commonMethod(application, labelname,
				// dropdown_xpath, dropdownValue, xml);

			}
		}

		public void newCity(String dropdown_xpath, String addCitytoggleButton, String editNewCityName,
				String editNewCityCode) throws InterruptedException, IOException {

			if (isVisible(dropdown_xpath)) {

				click(addCitytoggleButton);
				waitforPagetobeenable();
				// click_commonMethod(application, "Select City toggle button",
				// addCitytoggleButton, xml);
				// Thread.sleep(1000);
				// City name
				edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
				// City Code
				edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
			} else {
				// City name
				edittextFields_commonMethod("City Name", "citynameinputfield", editNewCityName);
				// City Code
				edittextFields_commonMethod("City Code", "citycodeinputfield", editNewCityCode);
			}
		}

		public void editSite(String editExistingCity, String editNewCity, String dropdown_xpath,
				String selectSiteToggleButton, String addSitetoggleButton, String dropdownValue, String editNewSiteName,
				String editNewSiteCode, String labelname) throws InterruptedException, IOException {

			if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("no")) {
				existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
			} else if (editExistingCity.equalsIgnoreCase("yes") & editNewCity.equalsIgnoreCase("null")) {
				existingSite(dropdown_xpath, dropdownValue, selectSiteToggleButton, labelname);
			} else if (editExistingCity.equalsIgnoreCase("no") & editNewCity.equalsIgnoreCase("Yes")) {
				newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
			} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("Yes")) {
				newSite(dropdown_xpath, addSitetoggleButton, editNewSiteName, editNewSiteCode, labelname);
			} else if (editExistingCity.equalsIgnoreCase("null") & editNewCity.equalsIgnoreCase("null")) {
				Report.LogInfo("EditSite", "No changes made under 'Site' field", "PASS");
			}

		}

		public void existingSite(String dropdown_xpath, String dropdownValue, String selectSiteToggleButton,
				String labelname) throws InterruptedException, IOException {

			if (isVisible(dropdown_xpath)) {

				selectByValue(dropdown_xpath, dropdownValue, labelname);
			} else {
				click(selectSiteToggleButton);
				waitforPagetobeenable();
				// Thread.sleep(1000);
				selectByValue(dropdown_xpath, dropdownValue, labelname);

			}
		}

		public void newSite(String dropdown_xpath, String addSitetoggleButton, String editNewSiteName,
				String editNewSiteCode, String labelname) throws InterruptedException, IOException {

			if (isVisible(dropdown_xpath)) {
				click(addSitetoggleButton);
				waitforPagetobeenable();
				// click_commonMethod(application, "Select City toggle button",
				// addSitetoggleButton, xml);
				// Thread.sleep(1000);
				// Site name
				edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
				// Site Code
				edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
			} else {
				// Site name
				edittextFields_commonMethod("Site Name", "sitenameinputfield_addSiteToggleSelected", editNewSiteName);
				// Site Code
				edittextFields_commonMethod("Site Code", "sitecodeinputfield_addSiteToggleSelected", editNewSiteCode);
			}
		}

		public void editPremise(String editExistingPremise, String editNewPremise, String dropdown_xpath,
				String selectPremiseToggleButton, String addPremisetoggleButton, String dropdownValue,
				String editNewPremiseName, String editNewPremiseCode, String labelname)
				throws InterruptedException, IOException {

			if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("no")) {
				existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
			} else if (editExistingPremise.equalsIgnoreCase("yes") & editNewPremise.equalsIgnoreCase("null")) {
				existingPremise(dropdown_xpath, dropdownValue, selectPremiseToggleButton, labelname);
			} else if (editExistingPremise.equalsIgnoreCase("no") & editNewPremise.equalsIgnoreCase("Yes")) {
				newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
			} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("Yes")) {
				newPremise(dropdown_xpath, addPremisetoggleButton, editNewPremiseName, editNewPremiseCode);
			} else if (editExistingPremise.equalsIgnoreCase("null") & editNewPremise.equalsIgnoreCase("null")) {
				Report.LogInfo("EditPremise", "No changes made under 'Premise' field", "PASS");

			}

		}

		public void existingPremise(String dropdown_xpath, String dropdownValue, String selectPremiseToggleButton,
				String labelname) throws InterruptedException, IOException {

			if (isVisible(dropdown_xpath)) {
				selectByValue(dropdown_xpath, dropdownValue, labelname);
				// addDropdownValues_commonMethod(application, labelname,
				// dropdown_xpath, dropdownValue, xml);
			} else {
				click(selectPremiseToggleButton);
				waitforPagetobeenable();
				// click_commonMethod(application, "Select Premise toggle button",
				// selectPremiseToggleButton, xml);
				// Thread.sleep(1000);
				selectByValue(dropdown_xpath, dropdownValue, labelname);
				// addDropdownValues_commonMethod(application, labelname,
				// dropdown_xpath, dropdownValue, xml);
			}
		}

		public void newPremise(String dropdown_xpath, String addPremisetoggleButton, String editNewPremiseName,
				String editNewPremiseCode) throws InterruptedException, IOException {

			if (isVisible(dropdown_xpath)) {
				click(addPremisetoggleButton);
				waitforPagetobeenable();
				// click_commonMethod(application, "Select Premise toggle button",
				// addPremisetoggleButton, xml);
				// Thread.sleep(1000);
				// Premise name
				edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
						editNewPremiseName);

				// Premise Code
				edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
						editNewPremiseCode);
			} else {
				// Premise name
				edittextFields_commonMethod("Premise Name", "premisenameinputfield_addCityToggleSelected",
						editNewPremiseName);
				// Premise Code
				edittextFields_commonMethod("Premise Code", "premisecodeinputfield_addCityToggleSelected",
						editNewPremiseCode);
			}
		}
		
		
		
		public void equip_adddevi_Accedian1G(String interfaceSpeed,  String cpename, String vender, String snmpro,
				String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,String serialNumber, 
				String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress, String existingmanagementAddress,
				String manageaddressdropdownvalue) throws InterruptedException, IOException {
			
			
			click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
			waitforPagetobeenable();
			
			try {
				
			String linklostForwardingcheckboxstate="disabled"; 
				
			String[] Vender= {"Accedian-1G 1GigE-MetroNID-GT", "Accedian-1G 1GigE-MetroNID-GT-S", "Accedian-1G GX"};
			
			String[] powerAlarm= {"DC Single Power Supply - Feed A", "DC Dual Power Supply - Feed-A+B", "AC Single Power Supply - Feed A", "AC Dual Power Supply -Feed A+B"};
			
			String expectedDeviceNameFieldAutopopulatedValue="<Device>.lanlink.dcn.colt.net";
			
			String expectedValueForSnmpro= "JdhquA5";
			
			
			
				// Vendor/Model Error Message
				device_vendorModelWarningMessage();

				// Management Address Error Message
				device_managementAddressWarningMessage();

				// Power Alarm Error Message
				device_powerAlarmWarningMessage();

				//serial number Eror Message
				device_serialNumberWarningMessage();
				
				//Hexa serial Number
				device_hexaSerialNumberWarningMessage();
		
				
				//Vendor/Model
				device_vendorModel( Vender, vender);      
			
				//Snmpro
				device_snmPro(snmpro);
				
				//Management Address dropdown
				device_managementAddress( existingmanagementAddress, newmanagementAddress, managementAddress);
				
				//MEP Id
				device_mepID( Mepid);
			
				//Power Alarm	
				device_powerAlarm(poweralarm);
				
				//Serial Number
				device_serialNumber(serialNumber);
			
			    //Link lost Forwarding
				device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);
			    
			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton);
			waitforPagetobeenable();
				//OK button
				device_okbutton();
				
				//cancel button
				device_cancelButton();
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton, "OK");
				
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage1_devicename);
				waitforPagetobeenable();
				
				
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage1_devicename, "Device Name");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage2_devicename, "Device Name");
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage3_devicename, "Device Name");
				
				//Name
				device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);
				
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton);
				waitforPagetobeenable();
				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton, "OK");
				
			    
			//sa.assertAll();
			
			}catch(AssertionError e) {
				
				e.printStackTrace();
				////ExtentTestManager.getTest().log(LogStatus.FAIL, "FAiled while verify the fields for Add CPE device");
				
			}

		}
		
		
		
		
		public void equip_addDevice_1G(String interfaceSpeed,  String cpename, String vender, String snmpro,
				String managementAddress, String Mepid, String poweralarm, String mediaSelection, String Macaddress,
				String serialNumber, String hexaSerialnumber, String linkLostForwarding, String newmanagementAddress, String existingmanagementAddress, String manageaddressdropdownvalue) throws InterruptedException, IOException {
			
			try {
				
			String linklostForwardingcheckboxstate="enabled"; 
			
			String[] Vender= {"Overture ISG-26", "Overture ISG-26R", "Overture ISG-26S", "Overture ISG140", "Overture ISG180", "Overture ISG6000"};
			
			String[] powerAlarm= {"AC", "DC"};
			
			String[] Mediaselection= {"SFP-A with SFP-B","RJ45-A with SFP-B"};	
			
			String expectedDeviceNameFieldAutopopulatedValue="<Device>.lanlink.dcn.colt.net";
			
			String mepValue="null";
			
			
			click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
			waitforPagetobeenable();
			
				// Vendor/Model Error Message
				device_vendorModelWarningMessage();

				// Management Address Error Message
				device_managementAddressWarningMessage();

				// Power Alarm Error Message
				device_powerAlarmWarningMessage();
				
			//Media Selection Error Message
//			device_mediaSelectionWarningMessage(application);
				
				
			//MAC Address Error Message
			device_macAddressWarningMessage();
				

			//Vendor/Model
			device_vendorModel(Vender, vender);      
		
			//Snmpro
			device_snmPro( snmpro);
			
			//Management Address dropdown
			device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);
			
			//MEP Id
			device_mepID(Mepid);
		
			//Power Alarm	
			device_powerAlarm( poweralarm);
		
	//	scrolltoend();
		waitforPagetobeenable();
		
			//Media Selection
			device_mediaSelection(Mediaselection, mediaSelection);
			
			//MAC Address
			device_MAcaddress( Macaddress);
		    
		    //Link lost Forwarding
			device_linklostForwarding( linkLostForwarding, linklostForwardingcheckboxstate);
		    
			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton);
			waitforPagetobeenable();
		    
		    //OK button
			device_okbutton();
			
			//cancel button
			device_cancelButton();
				
			
			click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton, "OK");
			
//			scrollToTop();
//			Thread.sleep(1000);
			
			
			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage1_devicename, "Device Name");
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage2_devicename, "Device Name");
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage3_devicename, "Device Name");
			
			//Name
			device_nameField( cpename, expectedDeviceNameFieldAutopopulatedValue);
			
			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton);
			waitforPagetobeenable();
			click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
			
		//	sa.assertAll();
			
			}catch(AssertionError e) {
				
				e.printStackTrace();
				////ExtentTestManager.getTest().log(LogStatus.FAIL, "FAiled while verify the fields for Add CPE device");
				
			}
			
			
		}
		
		 public void device_macAddressWarningMessage() throws InterruptedException {
			   boolean macErr=false;
			   try {
				   macErr = (isVisible(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macAdressErrmsg));
				//	sa.assertTrue(macErr, "MAC Address warning message is not displayed ");
					if(macErr) {
					String macadresErrMsg = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_macAdressErrmsg);
					Reporter.log(
							"MAC Address  message displayed as : " + macadresErrMsg);
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" validation message for MAC Address field displayed as : " + macadresErrMsg);
					Reporter.log("MAC Address warning message displayed as : " + macadresErrMsg);
					}else{
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'MAC Address' warning message is not displaying under 'Add cpe device' page");
					}
				 }catch(Exception e) {
					 e.printStackTrace();
					 Reporter.log("Mac Adress warning message is not dipslaying");
					 ExtentTestManager.getTest().log(LogStatus.FAIL, " 'MAC Address is required' warning message is not displaying");
				 } 
		   }
		 
		 
		 
		 public void selectRowForAddingInterface_Actelis(String interfacenumber)
					throws IOException, InterruptedException {
					((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();

			 Reporter.log("check second time");
				int TotalPages;

				String TextKeyword = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceToSelect_actelis_totalpage);

				TotalPages = Integer.parseInt(TextKeyword);

				Reporter.log("Total number of pages in table is: " + TotalPages);

				ab:

				if (TotalPages != 0) {
					for (int k = 1; k <= TotalPages; k++) {

						// Current page
						String CurrentPage = getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.InterfaceToSelect_actelis_currentpage);
						int Current_page = Integer.parseInt(CurrentPage);


						Reporter.log("Currently we are in page number: " + Current_page);

						List<String> results = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.interfacenumber1+ interfacenumber +Lanlink_Outbandmanagement.Outbandmanagement.interfacenumber2)));
						
						int numofrows = results.size();
						Reporter.log("no of results: " + numofrows);
						boolean resultflag;

						if (numofrows == 0) {

							click(Lanlink_Outbandmanagement.Outbandmanagement.nextBtn);
							waitforPagetobeenable();

						}

						else {

							for (int i = 0; i < numofrows; i++) {

								try {

									resultflag = isElementPresent(results.get(i));
									Reporter.log("status of result: " + resultflag);
									if (resultflag) {
										Reporter.log(getTextFrom(results.get(i)));
										click(getTextFrom(results.get(i)));
										
										click(Lanlink_Outbandmanagement.Outbandmanagement.next);
										waitforPagetobeenable();
									}

								} catch (StaleElementReferenceException e) 
								{
									// TODO Auto-generated catch block
									// e.printStackTrace();
									results = new ArrayList<>(Arrays.asList(Lanlink_Outbandmanagement.Outbandmanagement.interfacenumber1+ interfacenumber +Lanlink_Outbandmanagement.Outbandmanagement.interfacenumber2));
									numofrows = results.size();
									// results.get(i).click();
									Reporter.log("selected row is : " + i);
								}
							}
							break ab;
						}
					}
				} else {

					Reporter.log("No values available in table");
					Reporter.log("No Interfaces got fetched");
				}

			}
		 
		 
		 
		 
		 public void Edit_DirectFibre10G(String ServiceIdentificationNumber, String SelectSubService,
					String Interfacespeed, String EndpointCPE, String Email, String PhoneContact, String remark,
					String performanceReporting, String ProactiveMonitoring, String deliveryChannel, String ManagementOrder,
					String intermediateTechnology, String CircuitReference, 
					String notificationManagement, String Manageconnectiondropdown, String ENNIcheckBox)
					throws InterruptedException, IOException {

				// Service Identification
				editService_serviceIdentification( ServiceIdentificationNumber);

				// Endpoint CPE
				editService_singleEndPointCPE(EndpointCPE);

				// Email
				editService_Email(Email);

				// Phone contact `
				editService_phoneContact( PhoneContact);

				// Remark
				editService_remark( remark);

				// Performance Reporting
				boolean perfrmReportAvailability = false;
				try {
					perfrmReportAvailability = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox).isDisplayed();

					if (perfrmReportAvailability) {

						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Performance reporting' checkbox is displaying in 'Edit Service' page as expected");

						if (!performanceReporting.equalsIgnoreCase("null")) {

							boolean perfReport = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox).isSelected();
							Thread.sleep(2000);

							if (performanceReporting.equalsIgnoreCase("yes")) {

								if (perfReport) {

									ExtentTestManager.getTest().log(LogStatus.PASS,
											"Performance Reporting checkbox is not edited and it is already Selected while creating");

								} else {

									click(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);
									Reporter.log("Performance Reporting check box is selected");
									ExtentTestManager.getTest().log(LogStatus.PASS, "Performance Reporting checkbox is selected");
								}
							} else if (performanceReporting.equalsIgnoreCase("no")) {

								if (perfReport) {

									click(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);
									Reporter.log("Performance Reporting check box is unselected");
									ExtentTestManager.getTest().log(LogStatus.PASS,
											"Performance Reporting is edited and gets unselected");

								} else {
									ExtentTestManager.getTest().log(LogStatus.PASS,
											"Performance Reporting is not edited and it remains unselected");
								}

							}
						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, "No changes made for Performance Reporting chekbox");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Performance reporting' checkbox is not available in 'Edit Service' page");
					}

				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" ' Performance Reporting' checkbox is not displaying under 'Edit service' page");
					Reporter.log(" ' Performance Reporting' checkbox is not displaying under 'Edit service' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to click on 'Performance Reporting' checkbox");
					Reporter.log(" Not able to click on 'erformance Reporting' checkbox");
				}

				// Proactive monitoring
				editService_proactiveMonitoring(ProactiveMonitoring, notificationManagement);

				// Delivery channel
			//	editService_deliveryChannel(deliveryChannel);

				// Management Connection
			//	editService_managementConnection(Manageconnectiondropdown);

				// ENNI checkbox
				editcheckbox_commonMethod( ENNIcheckBox, Lanlink_Outbandmanagement.Outbandmanagement.ENNI_checkbox, "ENNI");

				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
				waitforPagetobeenable();

				// click on "Ok button to update
				click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton, "OK");
				waitforPagetobeenable();

			}
		 
		 
		 
		 public void editService_serviceIdentification(String ServiceIdentificationNumber)
					throws InterruptedException, IOException {

				boolean serviceAvailability = false;
				serviceAvailability = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification)
						.isDisplayed();

				if (serviceAvailability) {

					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Service Identification' field is displaying in 'Edit Service'page as expected");

					if (!ServiceIdentificationNumber.equalsIgnoreCase("null")) {

						clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification);
						waitforPagetobeenable();
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification,ServiceIdentificationNumber,"ServiceIdentificationNumber");

						String Actualvalue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification,"value");
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"Edited value for 'Service Identification' field is " + Actualvalue);
						Reporter.log("Edited value for 'Service Identification' field is " + Actualvalue);

					} else {
						ExtentTestManager.getTest().log(LogStatus.PASS, " Service Identification field value is not edited");
						Reporter.log(" Service Identification field value is not edited");
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Service Identification' field is not availale in 'Edit Service'page");
				}
			}
		 
		 
		 public void Edit_DirectFibre1G(String ServiceIdentificationNumber, String SelectSubService,
					String Interfacespeed, String EndpointCPE, String Email, String PhoneContact, String remark,
					String performanceReporting, String ProactiveMonitoring, String deliveryChannel, String ManagementOrder,
					String intermediateTechnology, String CircuitReference,
					String notificationManagement, String perCoSperformanceReport, String actelisBased, String standardCIR,
					String standardEIR, String premiumCIR, String premiumEIR, String ManagementConnection, String ENNIcheckBox)
					throws InterruptedException, IOException {

				//scrollToTop();
				waitforPagetobeenable();
				
				// Service Identification
				editService_serviceIdentification( ServiceIdentificationNumber);

				// Endpoint CPE
				editService_singleEndPointCPE(EndpointCPE);

				// Email
				editService_Email( Email);

				// Phone contact `
				editService_phoneContact( PhoneContact);

				// Remark
				editService_remark( remark);

				WebElement serviceIdentificationlabelName=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.serviceIdentificationLabelName);
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.serviceIdentificationLabelName);
				waitforPagetobeenable();

				// Proactive monitoring
				editService_proactiveMonitoring(ProactiveMonitoring, notificationManagement);

				// Delivery channel
				editService_deliveryChannel( deliveryChannel);

				// Management Connection
				editService_managementConnection( ManagementConnection);

				// ENNI checkbo
				editcheckbox_commonMethod( ENNIcheckBox, "ENNI_checkbox", "ENNI");

			//	scrolltoend();
				Thread.sleep(1000);
				
				//Per CoS Performance Reporting
				if(performanceReporting.equalsIgnoreCase("yes")) {
					boolean perCoSdisplay=false;
					try {
					perCoSdisplay=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport).isDisplayed();
					waitforPagetobeenable();
					
					if(perCoSdisplay) {
						ExtentTestManager.getTest().log(LogStatus.PASS, " 'Per CoS Performance Reporting' checkbox is displaying in 'Edit Service' page");
					if(!perCoSperformanceReport.equalsIgnoreCase("null")) {
						
						boolean perCoSreport=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport).isSelected();
						Thread.sleep(2000);
						
						if (perCoSperformanceReport.equalsIgnoreCase("yes")) {

							if(perCoSreport) {
								
								ExtentTestManager.getTest().log(LogStatus.PASS, "Per CoS Performance Reporting checkbox is not edited and it is already Selected while creating");
								
							}else {
								
								click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport);
								Reporter.log("Per CoS Performance Reporting check box is selected");
								ExtentTestManager.getTest().log(LogStatus.PASS,"Per CoS Performance Reporting is selected");
							}
						}

						else if (perCoSperformanceReport.equalsIgnoreCase("no")) {
							
							if(perCoSreport) {
								
								click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport);
								Reporter.log("Per CoS Performance Reporting check box is unselected");
								ExtentTestManager.getTest().log(LogStatus.PASS,"PPer CoS Performance Reporting is edited and gets unselected");
								
							}else {
								ExtentTestManager.getTest().log(LogStatus.PASS, "Per CoS Performance Reporting is not edited and it remains unselected");
							}
							
						}
					}else {
						ExtentTestManager.getTest().log(LogStatus.PASS," 'Per CoS Performance Reporting' chekbox is not edited");
					}
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page");
				}
					}catch(NoSuchElementException e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page");
						Reporter.log(" 'Per CoS Performance Reporting' checkbox is not displaying under 'Edit service' page");
					}catch(Exception err) {
						err.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to click on 'Per CoS Performance Reporting' checkbox");
						Reporter.log(" Not able to click on 'Per CoS Performance Reporting' checkbox");
					}
				}
				
				//Actelis Based
						if(perCoSperformanceReport.equalsIgnoreCase("yes")) {
							boolean actelisbased=false;
							try {
								actelisbased=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox).isDisplayed();
								Thread.sleep(3000);
								if(actelisbased) {
									ExtentTestManager.getTest().log(LogStatus.PASS, " Actelis Based checkbox is dipslaying in 'Edit Serivce' page");
								if(!actelisBased.equalsIgnoreCase("null")) {
									
									
									boolean actelsBased=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox).isSelected();
									Thread.sleep(2000);
									
									if (actelisBased.equalsIgnoreCase("yes")) {

										if(actelsBased) {
											
											ExtentTestManager.getTest().log(LogStatus.PASS, "Actelis Based checkbox is not edited and it is already Selected while creating");
											
										}else {
											
											click(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox);
											Reporter.log("Actelis Based check box is selected");
											ExtentTestManager.getTest().log(LogStatus.PASS,"Actelis Based checkbox is selected");
										}
									}

									else if (actelisBased.equalsIgnoreCase("no")) {
										
										if(actelsBased) {
											
											click(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox);
											Reporter.log("Per CoS Performance Reporting check box is unselected");
											ExtentTestManager.getTest().log(LogStatus.PASS,"Actelis Based is edited and gets unselected");
											
										}else {
											ExtentTestManager.getTest().log(LogStatus.PASS, "Actelis Based checkbox is not edited and it remains unselected");
										}
										
									}
								}else {
									ExtentTestManager.getTest().log(LogStatus.PASS,"No changes made for Actelis Based chekbox");
								}
							}
							}catch(NoSuchElementException e) {
								e.printStackTrace();
								ExtentTestManager.getTest().log(LogStatus.FAIL, " Actelis Based checkbox is not dipslaying in 'Edit Serivce' page");
							}catch(Exception err) {
								err.printStackTrace();
								ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to click on 'Acteis' checkbox");
								Reporter.log(" Not able to click on 'Acteis' checkbox");
							}
						}		
								
								
					if(actelisBased.equalsIgnoreCase("yes")) {

						//Standard CIR
							editService_standardCIR(standardCIR);			
							
						//Standard EIR
							editService_standardEIR(standardEIR);			
							
						//Premium CIR
							editService_premiumCIR(premiumCIR);			
					
						//Premium EIR
							editService_premiumEIR(premiumEIR);
				}


				// click on "Ok button to update
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
				waitforPagetobeenable();
				click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton, "OK");
				waitforPagetobeenable();

			}
		 
		 
		 
		 
		 public void editService_singleEndPointCPE(String EndpointCPE)
					throws InterruptedException {
				boolean CPEAvailability = false;

					try {
						CPEAvailability = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.EndpointCPE)
								.isDisplayed();
						if (CPEAvailability) {

							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Single EndPoint CPE checkbox is displaying under 'Edit Service' page");

							if (!EndpointCPE.equalsIgnoreCase("null")) {

								boolean endpoint = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.EndpointCPE)
										.isSelected();
								Thread.sleep(2000);

								if (EndpointCPE.equalsIgnoreCase("yes")) {

									if (endpoint) {

										ExtentTestManager.getTest().log(LogStatus.PASS,
												"Endpoint CPE is not edited. It is already Selected while creating.");
										Reporter.log("Endpoint CPE is not edited. It is already Selected while creating.");

									} else {

										click(Lanlink_Outbandmanagement.Outbandmanagement.EndpointCPE);
										Reporter.log("End point CPE check box is edited and it got selected");
										ExtentTestManager.getTest().log(LogStatus.PASS, "Endpoint CE is edited and it got selected");
									}
								} else if (EndpointCPE.equalsIgnoreCase("no")) {
									if (endpoint) {

										click(Lanlink_Outbandmanagement.Outbandmanagement.EndpointCPE);
										Reporter.log("End point CPE check box is unselected");
										ExtentTestManager.getTest().log(LogStatus.PASS,
												"Endpoint CE is edited and it is unselected as Expected");

									} else {
										ExtentTestManager.getTest().log(LogStatus.PASS,
												"Endpoint CPE is not edited. It was not selected during service creation and it remains unselected as expected");
									}

								}
							} else {
								ExtentTestManager.getTest().log(LogStatus.PASS,
										"No changes made for EndPoint CPE chekbox as expected");
							}
						} else {
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"Single EndPoint CPE checkbox is not available under 'Edit Service' page");
						}
					} catch (NoSuchElementException e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								"Single EndPoint CPE checkbox is not available under 'Edit Service' page");
						Reporter.log("Single EndPoint CPE checkbox is not available under 'Edit Service' page");
					} catch (Exception err) {
						err.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to click on 'Single endpoint CPE' checkbox");
						Reporter.log("Not able to click on 'Single endpoint CPE' checkbox");
					}

			}

		 public void editService_Email(String Email)
					throws InterruptedException, IOException {

				boolean emailAvailability = false;
				try {
					emailAvailability = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.Email).isDisplayed();

					if (emailAvailability) {

						ExtentTestManager.getTest().log(LogStatus.PASS, " 'Email' text field is displaying in 'Edit Service' page");

						if (!Email.equalsIgnoreCase("null")) {

							clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.Email);
							waitforPagetobeenable();

							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Email, Email,"Email");
							ExtentTestManager.getTest().log(LogStatus.PASS, "Edited value for 'Email' field is " + Email);
							Reporter.log("Edited value for 'Email' field is " + Email);

							String Actualvalue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Email,"value");
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Email' field is edited as: " + Actualvalue);
							Reporter.log("'Email' field is edited as: " + Actualvalue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, "'Email' field is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Email' text field is not available in 'Edit Service' page");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Email' text field is not available in 'Edit Service' page");
					Reporter.log(" 'Email' text field is not available in 'Edit Service' page");
				} catch (Exception err) {
					err.printStackTrace();
					//ExtentTestManager.getTest().log(LogStatus.FAIL, " not able to edit 'Email' field");
				}

			}
		 
		 
		 
		 public void editService_phoneContact(String PhoneContact)
					throws InterruptedException, IOException {

				boolean phoneAvailability = false;
				try {
					phoneAvailability = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact)
							.isDisplayed();

					if (phoneAvailability) {

						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Phone Contact field is displaying in 'Edit Service' page as expected");
						Reporter.log(" Phone Contact field is displaying in 'Edit Service' page as expected");

						if (!PhoneContact.equalsIgnoreCase("null")) {

							clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact);
							waitforPagetobeenable();

							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact,PhoneContact,"PhoneContact");
							waitforPagetobeenable();
							String actualValue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact,"value");
							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Edited value for 'Phone contact' field is " + actualValue);
							Reporter.log("Edited value for 'Phone contact' field is " + actualValue);
						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Phone contact' field is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" Phone Contact field is not available in 'Edit Service' page");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Phone Contact field is not available in 'Edit Service' page");
					Reporter.log("Phone Contact field is not available in 'Edit Service' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to edit 'phone Contact' field");
					Reporter.log(" Not able to edit 'phone Contact' field");
				}
			}
		 
		 
		 
		 public void editService_remark(String remark)
					throws InterruptedException, IOException {

				boolean remarkAvailability = false;
				try {
					remarkAvailability = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.Remark).isDisplayed();

					if (remarkAvailability) {

						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Remark' field is displaying in 'Edit Service' page as expected");
						Reporter.log(" 'Remark' field is displaying in 'Edit Service' page as expected");

						if (!remark.equalsIgnoreCase("null")) {

							clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.Remark);
							waitforPagetobeenable();

							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Remark,remark,"remark");

						//	String actualvalue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Remark,"value");
						//	ExtentTestManager.getTest().log(LogStatus.PASS, "Edited value for 'Remark' field is " + actualvalue);
						//	Reporter.log("Edited value for 'Remark' field is " + actualvalue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Remark' field is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Remark' field is not available in 'Edit Service' page");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Remark' field is not available in 'Edit Service' page");
					Reporter.log(" 'remark' field is not available in 'Edit Service' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to edit 'Remark' field");
					Reporter.log(" Not able to edit 'Remark' field");
				}
			}
		 
		 
		 
		 public void editService_proactiveMonitoring(String ProactiveMonitoring,
					String notificationManagement) throws InterruptedException, IOException {
				if (!ProactiveMonitoring.equalsIgnoreCase("null")) {

					boolean proactivemonitor = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring).isSelected();
					waitforPagetobeenable();

					if (ProactiveMonitoring.equalsIgnoreCase("yes")) {

						if (proactivemonitor) {

							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Proactive monitoring is not edited and it is already Selected while creating");

							if (notificationManagement.equalsIgnoreCase("null")) {

								ExtentTestManager.getTest().log(LogStatus.PASS, "No changes made to notification management");

							} else {

								click(Lanlink_Outbandmanagement.Outbandmanagement.notigymanagement_xbutton);
								waitforPagetobeenable();

								click(Lanlink_Outbandmanagement.Outbandmanagement.notificationmanagement);
								waitforPagetobeenable();
								webDriver.findElement(By.xpath("//div[contains(text(),'" + notificationManagement + "')]")).click();
								Thread.sleep(3000);
								ExtentTestManager.getTest().log(LogStatus.PASS,
										"Edited value for 'Notification Management' field is " + notificationManagement);
							}

						} else {

							click(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring);
							Reporter.log("proactive monitoring check box is selected");
							ExtentTestManager.getTest().log(LogStatus.PASS, "proactive monitoring is edited and gets selected");

							if (notificationManagement.equalsIgnoreCase("null")) {

								ExtentTestManager.getTest().log(LogStatus.PASS, "No changes made to notification management");

							} else {

								click(Lanlink_Outbandmanagement.Outbandmanagement.notigymanagement_xbutton);
								waitforPagetobeenable();

								click(Lanlink_Outbandmanagement.Outbandmanagement.notificationmanagement);
								webDriver.findElement(By.xpath("//div[contains(text(),'" + notificationManagement + "')]")).click();
								ExtentTestManager.getTest().log(LogStatus.PASS,
										"Edited value for 'Notification Management' field is " + notificationManagement);
							}
						}
					}

					else if (ProactiveMonitoring.equalsIgnoreCase("no")) {

						if (proactivemonitor) {

							click(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring);
							Reporter.log("Proactive monitoring check box is unselected");
							ExtentTestManager.getTest().log(LogStatus.PASS, "proactive monitoring is edited and gets unselected");

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS,
									"proactive monitoring was not selected during service creation and it remains unselected");
						}
					}
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS, "No changes made for 'Proactive monitoring' chekbox");
				}
			}

	   
		 public void editService_standardCIR(String standardCIR)
					throws InterruptedException, IOException {

				try {
					boolean stndrdCIR = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRtextfield)
							.isDisplayed();
					if (stndrdCIR) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Standard CIR' field is displaying in 'Edit Service' page ");
						if (!standardCIR.equalsIgnoreCase("null")) {

							clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRtextfield);
							waitforPagetobeenable();

							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRtextfield,standardCIR,"standardCIR");
							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Edited value for 'Standard CIR' field is " + standardCIR);
							Reporter.log("Edited value for 'Standard CIR' field is " + standardCIR);

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Standard CIR' field is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Standard CIR' field is not displaying in 'Edit Service' page ");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Standard CIR' field is not displaying in 'Edit Service' page ");
					Reporter.log(" 'Standard CIR' field is not displaying in 'Edit Service' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to edit 'Standard CIR' field");
					Reporter.log(" Not able to edit 'Standard CIR' field");

				}
			}
		 
		 
		 public void editService_deliveryChannel(String deliveryChannel)
					throws InterruptedException {

				boolean deliveryChannelAvailability = false;
				try {
					deliveryChannelAvailability = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey).isDisplayed();

					if (deliveryChannelAvailability) {

						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Delivery channel' dropdown is displaying in 'Edit Service' page as expected");
						Reporter.log(" 'Delivery channel' dropdown is displaying in 'Edit Service' page as expected");

						if (!deliveryChannel.equalsIgnoreCase("null")) {

							click(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_xbutton);
							waitforPagetobeenable();

							click(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey);
							waitforPagetobeenable();
							webDriver.findElement(By.xpath("//div[contains(text(),'" + deliveryChannel + "')]")).click();

							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Edited value for 'Delivery Channel' dropdown is " + deliveryChannel);
							Reporter.log("Delivery channel dropdown value is edited as expected");

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, "Delivery channel dropdown value is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Delivery channel' dropdown is not available in 'Edit Service' page");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Delivery channel' dropdown is not available in 'Edit Service' page");
					Reporter.log(" 'Delivery channel' dropdown is not available in 'Edit Service' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to edit 'delvery Channel' dropdown");
				}
			}
	   
		 public void editService_managementConnection(String Manageconnectiondropdown)
					throws InterruptedException {
				
				
				addDropdownValues_commonMethod("Management Connection" , Lanlink_Outbandmanagement.Outbandmanagement.managementConnection, Manageconnectiondropdown);
				
			}
		 
		 
		 
		 public void editService_standardEIR(String standardEIR)
					throws InterruptedException, IOException {

				boolean stndrdEIR = false;
				try {
					stndrdEIR = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield)
							.isDisplayed();
					if (stndrdEIR) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Standard EIR' field is displaying in 'Edit Service' page as expected");
						if (!standardEIR.equalsIgnoreCase("null")) {

							clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield);
							waitforPagetobeenable();

							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield,standardEIR,"standardEIR");

							String actualvalue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield,"value");
							ExtentTestManager.getTest().log(LogStatus.PASS,
									"Edited value for 'Standard EIR' field is " + actualvalue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Standard EIR' field is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" Standard EIR' field is not displaying in 'Edit Service' page ");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Standard EIR' field is not displaying in 'Edit Service' page ");
					Reporter.log(" 'Standard EIR' field is not displaying in 'Edit Service' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to edit 'Standard EIR' field");
					Reporter.log(" Not able to edit 'Standard EIR' field");

				}
			}
		 
		 
		 
		 public void editService_premiumCIR(String premiumCIR)
					throws InterruptedException, IOException {

				boolean premumCIR = false;
				try {
					premumCIR = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield)
							.isDisplayed();
					if (premumCIR) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Premise CIR' field is displaying in 'Edit Service' page as expected");
						if (!premiumCIR.equalsIgnoreCase("null")) {

							clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield);
							waitforPagetobeenable();

							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield,premiumCIR,"premiumCIR");

							String actualValue = getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield,"value");
							ExtentTestManager.getTest().log(LogStatus.PASS, "Edited value for 'Premium CIR' field is " + actualValue);

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Premium CIR' field is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" Premise CIR' field is not displaying in 'Edit Service' page ");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Premise CIR' field is not displaying in 'Edit Service' page ");
					Reporter.log(" Premise CIR' field is not displaying in 'Edit Service' page ");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to edit 'Premise CIR' field");
					Reporter.log("Not able to edit 'Premise CIR' field");

				}
			}
		 
		 
		 public void editService_premiumEIR( String premiumEIR)
					throws InterruptedException, IOException {

				boolean premumEIR = false;
				try {
					premumEIR = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRtextfield)
							.isDisplayed();
					if (premumEIR) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Premise EIR' field is displaying in 'Edit Service' page as expected");
						if (!premiumEIR.equalsIgnoreCase("null")) {

							clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRtextfield);
							waitforPagetobeenable();

							sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRtextfield,premiumEIR,"premiumEIR");
							ExtentTestManager.getTest().log(LogStatus.PASS, "Edited value for 'Premium EIR' field is " + premiumEIR);

						} else {
							ExtentTestManager.getTest().log(LogStatus.PASS, "  'Premium EIR' field is not edited");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" Premise EIR' field is not displaying in 'Edit Service' page ");
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Premise EIR' field is not displaying in 'Edit Service' page ");
					Reporter.log(" Premise EIR' field is not displaying in 'Edit Service' page");
				} catch (Exception err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Not able to edit 'Premise EIR' field");
					Reporter.log("Not able to edit 'Premise EIR' field");

				}
			}
	   
		 
		 
		 
		 
		 public void technologyDropdownFor1GigE() throws InterruptedException, IOException {

				String[] Technology = { "Actelis", "Atrica", "Overture", "Alu", "Accedian-1G", "Cyan" };

				boolean technology, devicename, Nonterminationpointcheckbox, portectedcheckbox;

				// Technology dropdown
				technology = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology)
						.isDisplayed();
			//	sa.assertTrue(technology, "Technology dropdown is not displayed");

				click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology,  "Technology dropdown");
				List<WebElement> listoftechnology = findWebElements(Lanlink_Outbandmanagement.Outbandmanagement.ClassNameForDropdowns);

				if (listoftechnology.size() >= 1) {
					for (WebElement technologytypes : listoftechnology) {

						Reporter.log("list of technology are : " + technologytypes.getText());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								"The list of technology  inside dropdown while  adding site order is: "
										+ technologytypes.getText());
						Reporter.log("The list of technology  inside dropdown while  adding site order is: "
								+ technologytypes.getText());
						String technologyValue = technologytypes.getText();
					}

					for (int k = 0; k < Technology.length; k++) {
						// Actelis
						if (Technology[k].equalsIgnoreCase("Actelis")) {
							ExtentTestManager.getTest().log(LogStatus.INFO,
									"when technology 'Actelis' is selected under Technology" + "no additional fields displays");

							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
							WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

						}

						// Atrica
						else if (Technology[k].equalsIgnoreCase("Atrica")) {

							ExtentTestManager.getTest().log(LogStatus.INFO,
									"when technology 'Atrica' is selected under Technology" + "list of fields should occur: "
											+ "Device name - Mandatory field" + "Non Termination point checkbox"
											+ "Protected checkbox");

							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
							WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

							// Device Name
							verifySiteOrderField_deviceName();

							// Non Termination Point
							verifySiteOrderFields_NonterminationField();

						}

						// Overture
						else if (Technology[k].equalsIgnoreCase("Overture")) {
							ExtentTestManager.getTest().log(LogStatus.INFO,
									"when technology 'Overture' is selected under Technology" + "list of fields should occur: "
											+ "Non Termination point checkbox" + "Protected checkbox");

							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology,  "Technology dropdown");
							WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

							// Non Termination Point
							verifySiteOrderFields_NonterminationField();

						}

						// Alu
						else if (Technology[k].equalsIgnoreCase("Alu")) {

							ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Alu' is selected under Technology"
									+ "list of fields should occur: " + "Device name - Mandatory field");

							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
							WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

							// Device Name
							verifySiteOrderField_deviceName();
						}

						// Accedian
						else if ((Technology[k].equalsIgnoreCase("Accedian-1G"))) {

							ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Accedian' is selected under Technology"
									+ "Non Termination point checkbox" + "Protected checkbox");

							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
							WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

							// Non Termination Point
							verifySiteOrderFields_NonterminationField();


						}

						// Cyan
						else if (Technology[k].equalsIgnoreCase("Cyan")) {

							ExtentTestManager.getTest().log(LogStatus.INFO, "when technology 'Cyan' is selected under Technology"
									+ "list of fields should occur: " + "Non Termination point checkbox");

							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Technology, "Technology dropdown");
							WebElement technologySelected = findWebElement((Lanlink_Outbandmanagement.Outbandmanagement.selectValueUnderTechnologyDropdown).replace("value", Technology[k]));  clickonTechnology(technologySelected, Technology[k]);

							// Non Termination Point
							verifySiteOrderFields_NonterminationField();

						}
					}
				} else {

					Reporter.log("no values are available inside technology dropdown for Add site order");
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							"no values are available inside technology dropdown for Add site order");
				}
			}
	   
	  
		 
		 public void verifySiteOrderField_deviceName() throws InterruptedException {
				boolean devicename = false;
				try {
					devicename = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Devicenamefield)
							.isDisplayed();
					//sa.assertTrue(devicename, "On selecting Atrica under Technology, Device name is not available");
					if (devicename) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Device Name' field is displaying under 'Add Site Order' page as expected");
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Device Name' field is not displaying under 'Add Site Order' page");

					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'Device Name' field is not displaying under 'Add Site Order' page");
					Reporter.log(" 'Device Name' field is not displaying under 'Add Site Order' page");
				}
			}
	  
		 
		 
		 public void verifydataEntered_DirectFibre10G(String serviceype,
					String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed, String EndpointCPE,
					String email, String PhoneContact, String remark, String PerformanceMonitoring, String ProactiveMonitoring,
					String deliveryChannel, String ManagementOrder, String vpnTopology, String intermediateTechnology,
					String CircuitReference, String CircuitType, String modularMSP, String notificationManagement,
					String Manageconnectiondropdown, String ENNIcheckBox)
					throws InterruptedException, IOException {

				WebElement orderPanel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
				waitforPagetobeenable();

				/**
				 * Service Panel
				 */
				// Service Identification
				verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

				// Service type
				verifyEnteredvalues("Service Type", serviceype);

				// Service Subtype
				verifyEnteredvalues("Service Subtype", SelectSubService);

				// Interface Speed
				verifyEnteredvalues("Interface Speed", Interfacespeed);

				// Single Endpoint CPE
//					verifyEnteredvalues("Single Endpoint CPE", EndpointCPE);
//					verifyEnteredvalues("Single Endpoint CPE", "No");

				// Email
				verifyEnteredvalueForEmail_serviceCreationpage("Email", email);

				// Phone Contact
				verifyEnteredvalues("Phone Contact", PhoneContact);

				// Modular MSP
				verifyEnteredvalues("Modular MSP", modularMSP);

				// Remark
				verifyExists( Lanlink_Outbandmanagement.Outbandmanagement.remark_viewPage, "remark");

				WebElement servicePanel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_Servicepanel);
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_Servicepanel);
				waitforPagetobeenable();

				/**
				 * Management Options panel
				 */

				// Delivery Channel
				verifyEnteredvalues("Delivery Channel", deliveryChannel);

				// Management Connection
				verifyEnteredvalues("Management Connection", Manageconnectiondropdown);

				// Proactive Monitoring
				verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

				if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
					verifyEnteredvalues("Notification Management Team", notificationManagement);
				}

				// Performance Reporting
				verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);

				// ENNI checkbox
				verifyEnteredvalues("ENNI", ENNIcheckBox);

			}
		 
		 
		 
		 public void verifyEnteredvalueForEmail_serviceCreationpage(String label, String expectedValue)
					throws InterruptedException {

				try {

					WebElement ele = webDriver.findElement(By.xpath("(//div[div[label[contains(text(),'" + label + "')]]]/div[2])[2]"));
					String valueinfo = ele.getText().toString();
					if ((valueinfo.equals("")) || (valueinfo.equalsIgnoreCase(null))) {

						Reporter.log("value not displayed for " + label);
						valueinfo = "Null";

						//sa.assertEquals(valueinfo, expectedValue, label + " value is not displaying as expected");

//							ExtentTestManager.getTest().log(LogStatus.PASS, "No value displaying for : " + label);

					} else {

						Reporter.log("value displayed for " + label + " is : " + valueinfo);

						Reporter.log("value displayed for" + label + "is : " + valueinfo);

					//	sa.assertEquals(valueinfo, expectedValue, label + " value is not displaying as expected");

						if (valueinfo.equalsIgnoreCase(expectedValue)) {
							Reporter.log("The valus is dipslaying as expected");
							ExtentTestManager.getTest().log(LogStatus.PASS,
									" Value is displaying as expected in 'view' page for " + label);
							ExtentTestManager.getTest().log(LogStatus.PASS,
									"value displayed for" + label + "is : " + valueinfo);
						} else {
							Reporter.log("the values are not dipslaying as expected for label: " + label);
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									" Value is not displaying as expected in 'view' page for " + label);
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"value displayed for " + label + "is : " + valueinfo);

						}

					}
				} catch (AssertionError err) {
					err.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, label + " value is not displaying as expected ");
				} catch (NoSuchElementException e) {
					Reporter.log("value not displayed for " + label);
					ExtentTestManager.getTest().log(LogStatus.FAIL, "" + label + " is not displaying");

				}
			}
		 
		 
		 
		 
		 public void verifydataEntered_DirectFibre1G(String serviceype,
					String ServiceIdentificationNumber, String SelectSubService, String Interfacespeed, String EndpointCPE,
					String Email, String PhoneContact, String remark, String PerformanceMonitoring, String ProactiveMonitoring,
					String deliveryChannel, String ManagementOrder, String vpnTopology, String intermediateTechnology,
					String CircuitReference, String CircuitType, String modularMSP, String perCocPerfrmReprt,
					String actelsBased, String standrdCir, String standrdEir, String prmiumCir, String prmiumEir,
					String notificationManagement, String Manageconnectiondropdown, String ENNIcheckBox)
					throws InterruptedException, IOException {

				WebElement orderPanel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_OrderPanel);
				waitforPagetobeenable();

				/**
				 * Service Panel
				 */
				// Service Identification
				verifyEnteredvalues("Service Identification", ServiceIdentificationNumber);

				// Service type
				verifyEnteredvalues("Service Type", serviceype);

				// Service Subtype
				verifyEnteredvalues("Service Subtype", SelectSubService);

				// Interface Speed
				verifyEnteredvalues("Interface Speed", Interfacespeed);

				// Single Endpoint CPE
					verifyEnteredvalues("Single Endpoint CPE", EndpointCPE);

				// Email
				verifyEnteredvalueForEmail_serviceCreationpage("Email", Email);

				// Phone Contact
				verifyEnteredvalues("Phone Contact", PhoneContact);

				// Modular MSP
				verifyEnteredvalues("Modular MSP", modularMSP);

				// Remark
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.remark_viewPage,"Remark");

				WebElement servicePanel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_Servicepanel);
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.viewServicepage_Servicepanel);
				waitforPagetobeenable();

				/**
				 * Management Options panel
				 */

				// Delivery Channel
				verifyEnteredvalues("Delivery Channel", deliveryChannel);

				// Management Connection
				verifyEnteredvalues("Management Connection", Manageconnectiondropdown);

				// Proactive Monitoring
				verifyEnteredvalues("Proactive Monitoring", ProactiveMonitoring);

				if (ProactiveMonitoring.equalsIgnoreCase("yes")) {
					verifyEnteredvalues("Notification Management Team", notificationManagement);
				}

				//Performance Reporting
				if(PerformanceMonitoring.equalsIgnoreCase("no")){
				
					verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);
				}
				
					
					if((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("no")) & (actelsBased.equalsIgnoreCase("no"))) {
						
						verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);
						
						verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);
						
					}
					
					
					else if((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes")) & (actelsBased.equalsIgnoreCase("no"))) {
						
						verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);
						
						verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);
						
						verifyEnteredvalues("Actelis Based", actelsBased);
					}
					
					
					else if((PerformanceMonitoring.equalsIgnoreCase("yes")) & (perCocPerfrmReprt.equalsIgnoreCase("yes")) & (actelsBased.equalsIgnoreCase("yes"))) {
						
						verifyEnteredvalues("Performance Reporting", PerformanceMonitoring);
						
						verifyEnteredvalues("Per CoS Performance Reporting", perCocPerfrmReprt);
						
						verifyEnteredvalues("Actelis Based", actelsBased);
						
					//Standard 	
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.standardHeaderName_viewPage,"Header Name for 1st row");
						
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRvalue_viewPage, "Standard CIR");
						
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRvalue_viewPage, "Standard EIR");
						
					//Premium
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.PremisumHeaderName_viewPage, "Header Name for 2nd row");
						
						verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRvalue_viewPage, "Premium CIR");
						
						verifyExists( Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRvalue_viewPage, "Premium EIR");
						
						
					}
				

				// ENNI checkbox
				verifyEnteredvalues("ENNI", ENNIcheckBox);

			}
	   
	 
	   
		 public void Fieldvalidation_DirectFibre10G(String serviceType, String SelectSubService,
					String Interfacespeed) throws InterruptedException, IOException {

				
				String[] deliverychannel = { "--", "Retail", "Reseller" , "WLC", "WLEC", "CES Solutions"};

				String[] notifyManagement= {"DNA"}; 
				 

				boolean serviceIdentificationField, ServiceType, ServiceSubtype,interfacespeedvalue, singleendpointCPE, email,
						phone, remark, performancereoprting, deliveryChanel, proactiveMonitor, Managementorder,  cancelButton;

				try {	
				
					webDriver.findElement(By.xpath("//span[contains(text(),'OK')]")).click();
					ExtentTestManager.getTest().log(LogStatus.PASS, "Clicked on 'OK' butto to verify mandatory warning Messages");
					Reporter.log("Clicked on 'OK' butto to verify mandatory warning Messages");
					waitforPagetobeenable();

					
				// management Connection Error Message
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.manageConnection_warningMesage, "Management Connection");
					
			//service Identification	
				serviceIdentificationField = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification);
			//	sa.assertTrue(serviceIdentificationField, "Service identification field is not displayed");
				if(serviceIdentificationField) {
					Report.LogInfo("Info",  " ' Service Identfication' mandatory field is displaynig under 'Add Service' page as expected","PASS");
				}else {
					Report.LogInfo("Info",  " ' Service Identfication' mandatory field is not available under 'Add Service' page","FAIL");
				}
				   

			 //Service type  
				ServiceType = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceType);
				waitforPagetobeenable();
			//	sa.assertTrue(ServiceType, "Service type is not displayed");
				if(ServiceType) {
					Report.LogInfo("Info",  " 'LANLink' is displying under 'Service type' as expected","PASS");
				}else {
					Report.LogInfo("Info",  " 'LANLink' is not displying under 'Service type'","FAIL");
				}
				
			//Service subtype
				ServiceSubtype = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation1 + SelectSubService + Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation2);

			//	sa.assertTrue(ServiceSubtype, "Service subtype is not displayed");
				if(ServiceSubtype) {
					Report.LogInfo("Info",  SelectSubService + " is displying under 'Service Sub type' as expected","PASS");
				}else {
					Report.LogInfo("Info",  SelectSubService + " is not displying under 'Service Sub type'","FAIL");
				}
				

			//Interface speed 	
			    interfacespeedvalue=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation1 + Interfacespeed +Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation2);
			   
		//		sa.assertTrue(interfacespeedvalue, "Interface speed dropdown is not displaying as expected");
				if(interfacespeedvalue) {
					Report.LogInfo("Info",  Interfacespeed + " is displying under 'Interface Speed' as expected","PASS");
				}else {
					Report.LogInfo("Info",  Interfacespeed + " is not displying under 'Interface Speed'","FAIL");
				}

			//Single endpoint cpe	
				try {
				singleendpointCPE = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.EndpointCPE);
		//		sa.assertTrue(singleendpointCPE, "single End point CPE checkbox is disabled by default");
				if(singleendpointCPE) {
					Report.LogInfo("Info",  " 'Single endpoint cpe' field is displying under 'Create Service'page as expected","PASS");
				}else {
					Report.LogInfo("Info",   " 'Single endpoint cpe' field is not available under 'Create Service' page","FAIL");
				}
				}catch(Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", "'Single endpoint cpe' checkbox is not available under 'Create Service' page","FAIL");
				}

			//Email	
				try {
				email = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Email);
			//	sa.assertTrue(email, "email field is not displayed");
				if(email) {
				//	Report.LogInfo("Info",  " 'Email' field is displying under 'Create Service'page as expected","PASS");
				}else {
				//	Report.LogInfo("Info",  " 'Email' field is not available under 'Create Service' pag","PASS");
				}
				}catch(Exception e) {
					e.printStackTrace();
				//	Report.LogInfo("Info", "'Email' field is not available under 'Create Service' page","FAIL");
				}

				
			//phone	
				try {
				phone = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact);
			//	sa.assertTrue(phone, "phone contact field is not displayed");
				if(phone) {
					Report.LogInfo("Info",  " 'phone' field is displying under 'Create Service'page as expected","PASS");
				}else {
					Report.LogInfo("Info",  " 'phone' field is not available under 'Create Service' page","FAIL");
				}
				}catch(Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info",  "'Phone Contact' field is not available under 'Create Service' page","FAIL");
				}
				
			//remark
				try {
				remark = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Remark);
			//	sa.assertTrue(remark, "remark field is not displayed");
				if(remark) {
					Report.LogInfo("Info",  " 'Remark' field is displying under 'Create Service'page as expected","PASS");
				}else {
					Report.LogInfo("Info",  " 'Remark' field is not available under 'Create Service' page","FAIL");
				}
				}catch(Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info",  "'Remark' field is not available under 'Create Service' page","FAIL");
				}
				
				
				//scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.configurtoinptionpanel-webelementToScroll);
				
			//performance Reporting	
			 try {	
				performancereoprting = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);
			//	sa.assertTrue(performancereoprting,	"performance monitoring checbox is not displayed and by default not selected as expected");
				if(performancereoprting) {
					Report.LogInfo("Info",  " 'performance Reporting' checkbox is displying under 'Create Service'page as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'performance Reporting' checkbox is not available under 'Create Service' page","FAIL");
				}
				
				boolean performancereoprtingselection = isSelected(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox,"Performance reporting checkbox");
				if(performancereoprtingselection) {
					Report.LogInfo("Info",  " 'Performance Reporting' checkbox is selected under 'Management Options' panel in 'Create Service page'","FAIL"); 
				}else {
					Report.LogInfo("Info",  "performance Reporting' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected","PASS");
				}
			 }catch(Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info",  "'Performance reporting' checkbox is not available under 'Create Service' page","FAIL");
				}	
				
				
			//proactive monitoring	
				proactiveMonitor = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring);
			//	sa.assertTrue(proactiveMonitor,"pro active monitoring checkbox is not displayed");
				if(proactiveMonitor) {
					Report.LogInfo("Info",  " 'proactive monitoring' checkbox is displying under 'Create Service'page as expected","PASS");
				
				
				boolean proactiveMonitorselection = isSelected(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring,"Proactive monitoring");
				if(proactiveMonitorselection) {
					Report.LogInfo("Info",  " 'proactive monitoring' checkbox is selected under 'Management Options' panel in 'Create Service page'","FAIL"); 
				}else {
					Report.LogInfo("Info",  "proactive monitoring' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected","PASS");
				

			//Notification Management Dropdown	
				click(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring,"Proactive monitoring");
				Reporter.log("Pro active monitoring check box is selected");
				waitforPagetobeenable();

			boolean notificationManagement=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.notificationmanagement);
		//	sa.assertTrue(notificationManagement, "Notification management dropdown is not displayed when proactive monitoring is selected");
			Reporter.log("Notification management dropdown gets displayed when proactive monitoring is selected");

			if(notificationManagement) {
				Report.LogInfo("Info",  " 'Notification Management' dropdown is displaying under 'Management Options' panel when 'proactive Monitoring' checkbox is selected","PASS");

			click(Lanlink_Outbandmanagement.Outbandmanagement.notificationmanagement);
			try {
				List<String> listofnotificationmanagement = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofnotificationmanagement)));
						
				
				for (String notificationmanagementtypes : listofnotificationmanagement) {

					boolean match = false;
					for (int i = 0; i < notifyManagement.length; i++) {
						if (notificationmanagementtypes.equals(notifyManagement[i])) {
							match = true;
							Reporter.log("list of notification management are : " + notificationmanagementtypes);
							Report.LogInfo("Info", "list of notification management are : " + notificationmanagementtypes,"PASS");
						}
					}
				//	sa.assertTrue(match,"");
					
				}
			  }catch(Exception e) {
				  Reporter.log("Notification Management dropdown values are mismatching");
				  e.printStackTrace();
				  Report.LogInfo("Info", "  values in Notification management dropdown under Direct Fiber service subtype is not displaying as expected","FAIL");
			  }
				}else {
					Report.LogInfo("Info",  " 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected","FAIL");
				}
				}
			}else {
				Report.LogInfo("Info",  " 'proactive monitoring' checkbox is not available under 'Create Service' page","FAIL");
				}

				
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification);
				
				waitforPagetobeenable();
				

			//delivery channel
				try {
			deliveryChanel = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey);
				//sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
				if(deliveryChanel) {
					Report.LogInfo("Info",  " 'Delivery Channel' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected","PASS");
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey);
			try {
				List<String> listofdeliverychannel = new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofdeliverychannel)));
						
				for (String deliverychanneltypes : listofdeliverychannel) {

					boolean match = false;
					for (int i = 0; i < deliverychannel.length; i++) {
						if (deliverychanneltypes.equals(deliverychannel[i])) {
							match = true;
							Reporter.log("list of delivery channel are : " + deliverychanneltypes);
							
						}
					}
				//	sa.assertTrue(match,"");
				}
			}catch(Exception e) {
				e.printStackTrace();
				Reporter.log("delivery channel dropdown values are mismatching");
			//	Report.LogInfo("Info", "  values in delivery channel dropdowns under Direct Fiber service subtype are not displaying as expected","FAIL");
			}
				} else {
					//Report.LogInfo("Info",  " 'Delivery Channel' dropdown is not avilable under 'Management options' panel in 'Create Service' page","FAIL");
				}
				
				}catch(Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info",  "'Delivery Channel' dropdown is not available under 'Create Service' page","FAIL");
				}

				
			//Management Connection
				try {
				Managementorder = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.managementConnection);
			//	sa.assertTrue(Managementorder, "Management order field is not displayed");
				if(Managementorder) {
					Report.LogInfo("Info",  " 'Management Connection' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected","PASS");
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.managementConnection);
				
				try {
				List<String> listofmanagementOrder =new ArrayList<>(Arrays.asList(getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.listofmanagementOrder)));
						
				
				for (String mnaagementOrdertypes : listofmanagementOrder) {
					
					Reporter.log("Available Management Connection name is : " + mnaagementOrdertypes);
					Report.LogInfo("Info", 
							"Available Management Connection name is : " + mnaagementOrdertypes,"PASS");
					Reporter.log("Available Management Connection name is :" + mnaagementOrdertypes);
					
				}
				}catch(Exception e) {
					e.printStackTrace();
					Reporter.log(" 'Management Connection' dropdown values are mismatching");
				}
				}else {
					Report.LogInfo("Info",  " 'Management Connection' dropdown is not available under 'Management options' panel in 'Create Service' page","FAIL");
				}
				
				}catch(Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", "'Management Connection' dropdown is not available under 'Create Service' page","FAIL");
				}	
				

				//ENNI checkbox
				verfyFields_ENNIcheckbox();

			//////scrolltoend();
			waitforPagetobeenable();

							
		//	String	okButton = isVisible(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
			//	sa.assertTrue(okButton, "OK button is not displayed");
				
				
				cancelButton = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.cancelButton);
			//	sa.assertTrue(cancelButton, "Cancel button is not displayed");
				
				//////scrolltoend();
				waitforPagetobeenable();
						
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.cancelButton,"cancelButton");
	
				click(Lanlink_Outbandmanagement.Outbandmanagement.cancelButton,"cancelButton");
			waitforPagetobeenable();
				
			//	sa.assertAll();
				Report.LogInfo("Info", " Fields are verified","PASS");
			   }catch(AssertionError e) {
				 e.printStackTrace();
			   }	
				



			} 
		 
		 
		 public void verfyFields_ENNIcheckbox() {
			 boolean ENNIAvailability=false;
				boolean ENNISelection=false;
				try {
					ENNIAvailability=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ENNI_checkbox);
				
				if(ENNIAvailability) {
					Report.LogInfo("Info", " 'ENNI' checkbox is displaying under 'Create Service'page as expected","PASS");
					
					ENNISelection=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.ENNI_checkbox,"ENNI checkbox");
					if(ENNISelection) {
						Report.LogInfo("Info", " 'ENNI' checkbox is selected by default","FAIL");
						Reporter.log(" 'ENNI' checkbox is selected by default");
					}else {
						Report.LogInfo("Info"," 'ENNI' checkbox is not selected by default","PASS");
						Reporter.log( " 'ENNI' checkbox is not selected by default");
					}
					
				}else {
					Report.LogInfo("Info", " 'ENNI' checkbox is not available under 'Create Service' page","FAIL");
				}
				
				}catch(NoSuchElementException e) {
					e.printStackTrace();
					Report.LogInfo("Info"," 'ENNI' chekcbox is not displaying","FAIL");
					Reporter.log(" 'ENNI' chekcbox is not displaying");
				}catch(Exception ee) {
					ee.printStackTrace();
					Report.LogInfo("Info", " 'ENNI' checkbox is not selected","PASS");
					Reporter.log(" 'ENNI' checkbox is not selected");
				}
		}
	   
		 
		 
		 public void DirectFibre_1G(String ServiceIdentificationNumber, String SelectSubService,
					String Interfacespeed, String EndpointCPE, String Email, String PhoneContact, String remark,
					String PerformanceReporting, String ProactiveMonitoring, String deliveryChannel, String Manageconnection,
					String vpnTopology, String intermediateTechnology, String CircuitReference, String CircuitType,
					String notificationManagement, String perCocPerfrmReprt, String actelsBased, String standrdCir,
					String standrdEir, String prmiumCir, String prmiumEir, String ENNIcheckBox)
					throws InterruptedException, IOException {

			//	scrollToTop();
				waitforPagetobeenable();

				// Service Identification
				createService_ServiceIdentification(ServiceIdentificationNumber);

				// End point CPE
				createService_singleEndPointCPE(EndpointCPE);

				// Email
				createSerivce_email(Email);

				// Phone Contact
				createService_phone(PhoneContact);

				// Remark
				createService_remark(remark);

				//scrolltoend();
				Thread.sleep(2000);

				
				// Pro active Monitoring
				createService_proactivemonitoring(ProactiveMonitoring, notificationManagement);

				// Delivery Channel
				createService_deliveryChannel(deliveryChannel);

				// management Connection
				createService_managementOptions(Manageconnection);

				// ENNI checkbox
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ENNI_checkbox, "ENNI");
				
				
				//Performance Reporting	
						if(!PerformanceReporting.equalsIgnoreCase("null")) {
							
							if (PerformanceReporting.equalsIgnoreCase("yes")) {
								
								boolean perfrmReprtFieldcheck=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox).isDisplayed();
								if(perfrmReprtFieldcheck) {
									ExtentTestManager.getTest().log(LogStatus.PASS, " 'Performance reporting' checkbox is displaying under 'Manage ment options' panel in 'Create Service' page as exepcted");
									click(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);
									Thread.sleep(5000);
									
									boolean prfrmReportselection=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox).isSelected();
									if(prfrmReportselection) {
										Reporter.log("performance monitoring check box is selected");
										ExtentTestManager.getTest().log(LogStatus.PASS, "Performance Reporting checkbox is selected as expected");
									}else {
										ExtentTestManager.getTest().log(LogStatus.FAIL, "Performance Reporting checkbox is not selected");
									}
									
									
								//Per CoS Performance Reporting chekcbox	
									boolean perCoSPrfrmReprtFieldcheck=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport).isDisplayed();
									if(perCoSPrfrmReprtFieldcheck) {
										ExtentTestManager.getTest().log(LogStatus.PASS, " 'Per CoS Performance Reporting' checkbox is displaying, when 'Performance reporting' checkbox is selected");
										if(perCocPerfrmReprt.equalsIgnoreCase("Yes")){
											click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport);
											waitforPagetobeenable();
											
											boolean perCoSSelection=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport).isSelected();
											if(perCoSSelection) {
												ExtentTestManager.getTest().log(LogStatus.PASS, "Per CoS Performance Reporting checkbox is selected as expected");
											}else {
												ExtentTestManager.getTest().log(LogStatus.FAIL, "Per CoS Performance Reporting checkbox is not selected");
											}
											
											
										//Actelis Based	
											boolean actelisbasedFieldcheck=findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox).isDisplayed();
											if(actelisbasedFieldcheck) {
												ExtentTestManager.getTest().log(LogStatus.PASS, " 'Actelis Based' checkbox is displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected");
												if(actelsBased.equalsIgnoreCase("Yes")) {
													click(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox);
													waitforPagetobeenable();
													
													boolean actelisSelection= findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox).isSelected();
													if(actelisSelection) {
														ExtentTestManager.getTest().log(LogStatus.PASS, " 'Actelis Based' checkbox is selected as expected");
													}else {
														ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Actelis Based' checkbox is not selected" );
													}
												
											//		scrolltoend();
													waitforPagetobeenable();
													
												//Standard CIR Text Field
													createService_standardCIR(standrdCir);
													
												//Standard EIR Text Field
													createService_standardEIR(standrdEir);
													
													
												//Premium CIR Text Field
													createService_premiumCIR(prmiumCir);
													
												//Premium EIR Text Field
													createService_premiumEIR(prmiumEir);		
													
													
												}else {
													ExtentTestManager.getTest().log(LogStatus.PASS, " 'Actelis Based' checkbox is not selected as expected");
												}
												
												
											}else {
												ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Actelis Based' checkbox is not displaying, when 'Per CoS Perfoemance Reporting' checkbox is selected");
											}
											
											
										}else {
											ExtentTestManager.getTest().log(LogStatus.PASS, " 'Per CoS Performance Reporting' checkbox is not selected as exepected");
										}
										
									
									}else {
										ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Per CoS Performance Rpeorting' checkbox is not displaying, when 'Performance reporting' checkbox is selected");
									}
									
								}else {
									ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Performance Reporting' checkbox is not available");
								}
								
								
								
							}
							else {

								Reporter.log("Performance Repoting is not selected");
								ExtentTestManager.getTest().log(LogStatus.PASS,"performance Reporting checkbox is not selected as expected");
								
							}
						}

				
				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
				click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton, "OK");

			}
		 
		 
		 
		 
		 public void createService_premiumEIR(String prmiumEir) throws InterruptedException, IOException {
				boolean premiumEiR=false;
			try {	
				premiumEiR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRtextfield);
				if(premiumEiR) {
					Report.LogInfo("Info",  " 'Standard EIR' text field displaying, when 'Actelis Based' checkbox is selected","PASS");
					if(prmiumEir.equalsIgnoreCase("null")) {
						Report.LogInfo("Info",  "No Values entered in 'Premium EIR' text field","PASS");
					}else {
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRtextfield, prmiumEir,"Premium EIR");
						
						String valuesForPEIR=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRtextfield,"value");
						Report.LogInfo("Info",  valuesForPEIR +" is enerted under 'Premium EIR' text field","PASS" );
					}
					
				}else {
					Report.LogInfo("Info",  " 'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
				}
			  }catch(NoSuchElementException e) {
					e.printStackTrace();
					Reporter.log("'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
					Report.LogInfo("Info",  " 'Premium EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
				}catch(Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info",  " Not able to enter value under 'Premium EIR' field in 'create Service' page","FAIL");
				}		
			}
		 
		 
		 
		 public void createService_premiumCIR(String prmiumCir) throws InterruptedException, IOException {
				boolean premiumCiR=false;
			try {	
				premiumCiR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield);
				if(premiumCiR) {
					Report.LogInfo("Info", " 'Premium CIR' text field displaying, when 'Actelis Based' checkbox is selected","PASS");
					if(prmiumCir.equalsIgnoreCase("null")) {
						Report.LogInfo("Info", "No Values entered in 'Premium CIR' text field","PASS");
					}else {
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield, prmiumCir,"Premium CIR");
						
						String valuesForPCIR=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield,"value");
						Report.LogInfo("Info", valuesForPCIR +" is enerted under 'Premium CIR' text field","PASS" );
					}
					
				}else {
					Report.LogInfo("Info", " 'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
				}	
			}catch(NoSuchElementException e) {
				e.printStackTrace();
				Reporter.log("'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
				Report.LogInfo("Info", " 'Premium CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
			}catch(Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to enter value under 'Premium CIR' field in 'create Service' page","FAIL");
			}	
			}
		 
		 
		 
		 public void createService_standardEIR(String standrdEir) throws InterruptedException, IOException {
				boolean standrdEiR=false;
			try {	
				standrdEiR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield);
				if(standrdEiR) {
					Report.LogInfo("Info", " 'Standard EIR' text field displaying, when 'Actelis Based' checkbox is selected","PASS");
					if(standrdEir.equalsIgnoreCase("null")) {
						Report.LogInfo("Info", "No Values entered in 'Standard EIR' text field","PASS");
					}else {
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield, standrdEir,"Standard EIR");
						
						String valuesForSEIR=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield,"value");
						Report.LogInfo("Info", valuesForSEIR +" is enerted under 'Standard EIR' text field" ,"PASS");
					}
					
				}else {
					Report.LogInfo("Info", " 'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
				}
			 }catch(NoSuchElementException e) {
					e.printStackTrace();
					Reporter.log("'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
					Report.LogInfo("Info", " 'Standard EIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
				}catch(Exception err) {
					err.printStackTrace();
					Report.LogInfo("Info", " Not albe to enter value under 'Standard EIR' field in 'create Service' page","FAIL");
					Reporter.log(" Not able to enter value under 'Standard EIR' field in 'create Service' page");
				}
			}
		 
		 
		 public void createService_standardCIR(String standrdCir) throws InterruptedException, IOException {
				boolean standrdCiR=false;
				
			try {	
				standrdCiR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRtextfield);
				if(standrdCiR) {
					Report.LogInfo("Info", " 'Standard CIR' text field displaying, when 'Actelis Based' checkbox is selected","PASS");
					if(standrdCir.equalsIgnoreCase("null")) {
						Report.LogInfo("Info",  "No Values entered in 'Standard CIR' text field","PASS");
					}else {
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRtextfield, standrdCir,"Standard CIR");
						
						String valuesForSCIR=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRtextfield,"value");
								Report.LogInfo("Info", valuesForSCIR +" is enerted under 'Standard CIR' text field","PASS" );
					}
					
				}else {
					Report.LogInfo("Info", " 'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
				}
			}catch(NoSuchElementException e) {
				e.printStackTrace();
				Reporter.log("'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected");
				Report.LogInfo("Info", " 'Standard CIR' Text Field is not displaying, when 'Actelis Based' checkbox is selected","FAIL");
			}catch(Exception err) {
				err.printStackTrace();
				Report.LogInfo("Info", " Not able to enter value under 'Standard CIR' field in 'create Service' page","FAIL");
			}
			}
		 
		 
		 
		 
		 public void Fieldvalidation_DirectFibre1G(String serviceType, String SelectSubService,
					String Interfacespeed) throws InterruptedException,IOException {

				
				String[] deliverychannel = { "--", "Retail", "Reseller" , "WLC", "WLEC", "CES Solutions"};

				String[] notifyManagement= {"DNA"}; 
				 

				boolean serviceIdentificationField, ServiceType, ServiceSubtype,interfacespeedvalue, singleendpointCPE, email,
						phone, remark, performancereoprting, deliveryChanel, proactiveMonitor, Managementorder, cancelButton;

				try {	
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag, "OK");
				waitforPagetobeenable();
				
				
					
				// management Connection Error Message	
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.manageConnection_warningMesage, "Management Connection");
				
			//Service Identification Error message	
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.serviceIdentificationerrmsg, "Service Identification");
				
					

			//service Identification	
				serviceIdentificationField = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceIdentification);
			//	sa.assertTrue(serviceIdentificationField, "Service identification field is not displayed");
				if(serviceIdentificationField) {
					Report.LogInfo("Info", " ' Service Identfication' mandatory field is displaynig under 'Add Service' page as expected","PASS");
				}else {
					Report.LogInfo("Info", " ' Service Identfication' mandatory field is not available under 'Add Service' page","FAIL");
				}
				   

			 //Service type  
				ServiceType = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ServiceType);
			//	sa.assertTrue(ServiceType, "Service type is not displayed");
				if(ServiceType) {
					Report.LogInfo("Info", " 'LANLink' is displying under 'Service type' as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'LANKLink' is not displying under 'Service type'","FAIL");
				}
				
			//Service subtype
				ServiceSubtype = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation1 + SelectSubService + Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation2);
			//	sa.assertTrue(ServiceSubtype, "Service subtype is not displayed");
				if(ServiceSubtype) {
					Report.LogInfo("Info", SelectSubService + " is displying under 'Service Sub type' as expected","PASS");
				}else {
					Report.LogInfo("Info", SelectSubService + " is not displying under 'Service Sub type'","FAIL");
				}
				

			//Interface speed 	
			    interfacespeedvalue=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation1  + Interfacespeed + Lanlink_Outbandmanagement.Outbandmanagement.fieldValidation2);
			//	sa.assertTrue(interfacespeedvalue, "Interface speed dropdown is not displaying as expected");
				if(interfacespeedvalue) {
					Report.LogInfo("Info", Interfacespeed + " is displying under 'Interface Speed' as expected","PASS");
				}else {
					Report.LogInfo("Info", Interfacespeed + " is not displying under 'Interface Speed'","FAIL");
				}

			//Single endpoint cpe
				try {
				singleendpointCPE = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.EndpointCPE);
			//	sa.assertTrue(singleendpointCPE, "single End point CPE checkbox is disabled by default");
				if(singleendpointCPE) {
					Report.LogInfo("Info", " 'Single endpoint cpe' field is displying under 'Create Service'page as expected","PASS");
				}else {
					Report.LogInfo("Info",  " 'Single endpoint cpe' field is not available under 'Create Service' pag","FAIL");
				}
				}catch(Exception e) {
					e.printStackTrace();
					Report.LogInfo("Info", "'Single Endpoint CPE' checkbox is not available under 'Create Service' page","FAIL");
				}

			
			//Email	
			try {	
				email = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Email);
			//	sa.assertTrue(email, "email field is not displayed");
				if(email) {
					Report.LogInfo("Info", " 'Email' field is displying under 'Create Service'page as expected","PASS");
				}else {
				//	Report.LogInfo("Info", " 'Email' field is not available under 'Create Service' pag","FAIL");
				}
			}catch(Exception e) {
				e.printStackTrace();
				//Report.LogInfo("Info", "'Email' field is not available under 'Create Service' page","FAIL");
			}	
				
			//phone	
			try {
				phone = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.PhoneContact);
			//	sa.assertTrue(phone, "phone contact field is not displayed");
				if(phone) {
					Report.LogInfo("Info", " 'phone' field is displying under 'Create Service'page as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'phone' field is not available under 'Create Service' page","FAIL");
				}
			}catch(Exception e) {
				e.printStackTrace();
				Report.LogInfo("Info", "'Phone' field is not available under 'Create Service' page","FAIL");
			}	
				
			//remark
			try {	
				remark = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Remark);
			//	sa.assertTrue(remark, "remark field is not displayed");
				if(remark) {
					Report.LogInfo("Info", " 'Remark' field is displying under 'Create Service'page as expected","PASS");
				}else {
					Report.LogInfo("Info", " 'Remark' field is not available under 'Create Service' page","FAIL");
				}
			}catch(Exception e) {
			e.printStackTrace();
			Report.LogInfo("Info", "'Remark' field is not available under 'Create Service' page","FAIL");
			}	
				

			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring);
			waitforPagetobeenable();

			// proactive monitoring
			proactiveMonitor = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring)
					.isDisplayed();
		//	sa.assertTrue(proactiveMonitor, "pro active monitoring checkbox is not displayed");
			if (proactiveMonitor) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" 'proactive monitoring' checkbox is displaying under 'Create Service'page");

				boolean proactiveMonitorselection = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring).isSelected();
				if (proactiveMonitorselection) {
					ExtentTestManager.getTest().log(LogStatus.FAIL,
							" 'proactive monitoring' checkbox is selected under 'Management Options' panel in 'Create Service page'");
				} else {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							"proactive monitoring' checkbox is not selected under 'Management Options' panel in 'Create Service page as expected");

					// Notification Management Dropdown
					click(Lanlink_Outbandmanagement.Outbandmanagement.proactiveMonitoring);
					Reporter.log("Pro active monitoring check box is selected");
					waitforPagetobeenable();

					boolean notificationManagement = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.notificationmanagement).isDisplayed();
					//sa.assertTrue(notificationManagement,"Notification management dropdown is not displayed when proactive monitoring is selected");
					Reporter.log("Notification management dropdown gets displayed when proactive monitoring is selected");
					if (notificationManagement) {
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" 'Notification Management' dropdown is displaying under 'Management Options' panel when 'proactive Monitoring' checkbox is selected");

						click(Lanlink_Outbandmanagement.Outbandmanagement.notificationmanagement);
						try {
							List<WebElement> listofnotificationmanagement = webDriver.findElements(By.xpath("//span[@role='list']//div[@class='sc-ifAKCX oLlzc']"));
							for (WebElement notificationmanagementtypes : listofnotificationmanagement) {

								boolean match = false;
								for (int i = 0; i < notifyManagement.length; i++) {
									if (notificationmanagementtypes.getText().equals(notifyManagement[i])) {
										match = true;
										Reporter.log("list of notification management are : "
												+ notificationmanagementtypes.getText());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												"list of notification management are : "
														+ notificationmanagementtypes.getText());
									}
								}
								//sa.assertTrue(match);

							}
						} catch (Exception e) {
							Reporter.log("Notification Management dropdown values are mismatching");
							e.printStackTrace();
							ExtentTestManager.getTest().log(LogStatus.FAIL,
									"  values in Notification management dropdown under Direct Fiber service subtype is not displaying as expected");
						}
					} else {
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" 'Notification Management' dropdown is not available under 'Management Options' panel when 'proactive Monitoring' checkbox is selected");
					}
				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" 'proactive monitoring' checkbox is not available under 'Create Service' page");
			}

//delivery channel
			try {
				deliveryChanel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey).isDisplayed();
			//	sa.assertTrue(deliveryChanel, "delivery channel dropdown is not displayed");
				if (deliveryChanel) {
					ExtentTestManager.getTest().log(LogStatus.PASS,
							" 'Delivery Channel' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected");

					click(Lanlink_Outbandmanagement.Outbandmanagement.deliverychannel_withclasskey);
					try {
						List<WebElement> listofdeliverychannel = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
						for (WebElement deliverychanneltypes : listofdeliverychannel) {

							boolean match = false;
							for (int i = 0; i < deliverychannel.length; i++) {
								if (deliverychanneltypes.getText().equals(deliverychannel[i])) {
									match = true;
									Reporter.log("list of delivery channel are : " + deliverychanneltypes.getText());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" List of Delivery channel dropdown values under Direct Fiber service subtype are: "
													+ deliverychanneltypes.getText());

								}
							}
						//	sa.assertTrue(match);
						}
					} catch (Exception e) {
						e.printStackTrace();
						Reporter.log("delivery channel dropdown values are mismatching");
						//ExtentTestManager.getTest().log(LogStatus.FAIL,
						//		"  values in delivery channel dropdowns under Direct Fiber service subtype are not displaying as expected");
					}
				} else {
					//ExtentTestManager.getTest().log(LogStatus.FAIL," 'Delivery Channel' dropdown is not avilable under 'Management options' panel in 'Create Service' page");
				}
			} catch (Exception e) {
				e.printStackTrace();
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						"'Delivery Channel' dropdown is not available under 'Create Service' page");
			}

		// Management Connection
			Managementorder = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.managementConnection)
					.isDisplayed();
			//sa.assertTrue(Managementorder, "Management Connection field is not displayed");
			if (Managementorder) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						" 'Management Connection' dropdown is displaying under 'Management options' panel in 'Create Service' page as expected");

				click(Lanlink_Outbandmanagement.Outbandmanagement.managementConnection);

				List<WebElement> listofmanagementOrder = webDriver.findElements(By.xpath("//div[@class='sc-ifAKCX oLlzc']"));
				for (WebElement mnaagementOrdertypes : listofmanagementOrder) {

					Reporter.log(
							"Available Management Connection name is : " + mnaagementOrdertypes.getText().toString());
					ExtentTestManager.getTest().log(LogStatus.PASS, "Available Management Connection name is : "
							+ mnaagementOrdertypes.getText().toString());
					Reporter.log("Available Management Connection name is :" + mnaagementOrdertypes.getText().toString());

				}
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL,
						" 'Management Connection' dropdown is not available under 'Management options' panel in 'Create Service' page");
			}

			// ENNI checkbox
			verfyFields_ENNIcheckbox();

			scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);


			waitforPagetobeenable();
			//performance Reporting			
				performancereoprting = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);
			//	sa.assertTrue(performancereoprting,	"performance reporting checbox is not displayed under 'Create Service' page");
				if(performancereoprting) {
					Report.LogInfo("Info", " 'performance Reporting' checkbox is displying under 'Create Service'page as expected","PASS");
				
				
				boolean performancereoprtingselection = isSelected(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox,"Performance reporting checkbox");
				if(performancereoprtingselection) {
					Report.LogInfo("Info", " 'Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'","FAIL"); 
				}else {
					Report.LogInfo("Info", "performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected","PASS");

					
				//Per CoS Performance Reporting
					click(Lanlink_Outbandmanagement.Outbandmanagement.performanceReportingcheckbox);
					waitforPagetobeenable();
					
					boolean perCosreopt=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport);
				//	sa.assertTrue(perCosreopt,	"Per CoS Performance Reporting checbox is not displayed under 'Create Service' page, when 'Performance Reporting' checkbox is selected");
					if(perCosreopt) {
						Report.LogInfo("Info", "Per CoS Performance Reporting checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected","PASS");
					
						boolean perCoSselection=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport,"Per CoS Performance Reporting");
						if(perCoSselection) {
							Report.LogInfo("Info", " 'Per CoS Performance Reporting' checkbox is selected by default under 'Management Options' panel in 'Create Service page'","FAIL"); 
						}else {
							Report.LogInfo("Info", "Per CoS Performance Reporting' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected","PASS");
					
					//Actelis Based    
							click(Lanlink_Outbandmanagement.Outbandmanagement.perCoSperformncereport);	
							waitforPagetobeenable();
							boolean actelisBasedcheckbox=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox);
					//		sa.assertTrue(actelisBasedcheckbox,	" 'Atelis Based' checbox is not displayed under 'Create Service' page, when 'Per CoS Performance Reporting' checkbox is selected");
							if(actelisBasedcheckbox) {
								Report.LogInfo("Info", " 'Actelis Based' checbox is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected","PASS");
							
							boolean actelisBasedselection=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox,"Actelis Based checkbox");
							if(actelisBasedselection) {
								Report.LogInfo("Info", " 'Actelis Based' checkbox is selected by default under 'Management Options' panel in 'Create Service page'","FAIL"); 
							}else {
								Report.LogInfo("Info", "Actelis Based' checkbox is not selected by default under 'Management Options' panel in 'Create Service page as expected","PASS");
							
								
						//Bandwidth Options table
							click(Lanlink_Outbandmanagement.Outbandmanagement.ActelisBasedcheckbox);	
							waitforPagetobeenable();
							
							//Standard CIR text field
							boolean standardCIR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.standardCIRtextfield);
				//			sa.assertTrue(standardCIR,			" 'Standard CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
							if(standardCIR) {
								Report.LogInfo("Info"," 'Standard CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}else {
								Report.LogInfo("Info"," 'Standard CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}
							
							
							//Standard EIR Text field
							boolean standardEIR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.standardEIRtextfield);
						//	sa.assertTrue(standardEIR,		" 'Standard EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
							if(standardEIR) {
								Report.LogInfo("Info"," 'Standard EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}else {
								Report.LogInfo("Info"," 'Standard EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}
							
							
							//Premium CIR text field
							boolean premiumCIR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.premiumCIRtextfield);
					//		sa.assertTrue(premiumCIR,	" 'Premium CIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
							if(premiumCIR) {
								Report.LogInfo("Info"," 'Premium CIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}else {
								Report.LogInfo("Info"," 'Premium CIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}
							
							
							//Premium EIR Text field
							boolean premiumEIR=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.premiumEIRtextfield);
					//		sa.assertTrue(premiumEIR,		" 'Premium EIR' text field is not displayed under 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkboxes are selected");  
							if(premiumEIR) {
								Report.LogInfo("Info"," 'Premium EIR' text field is displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}else {
								Report.LogInfo("Info"," 'Premium EIR' text field is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting'checkbox, 'Per CoS Performance Reporting' checkbox and 'Actelis Based' checkbox are selected","PASS");
							}
							
					 }
					}else {
						Report.LogInfo("Info", " 'Actelis Based' checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox and 'Per CoS Performance Reporting' checkbox is selected","PASS");
					}
				 }
				}else {
					Report.LogInfo("Info", "Per CoS Performance Reporting checbox is not displayed under 'Management Options' panel in 'Create Service' page, when 'Performance Reporting' checkbox is selected","FAIL");
				 }
				}
			}else {
				Report.LogInfo("Info", " 'performance Reporting' checkbox is not available under 'Create Service' page","FAIL");		
				}
				
				
							
			//	okButton = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
			//	sa.assertTrue(okButton, "OK button is not displayed");
				
			//	
				cancelButton = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.cancelButton);
			//	sa.assertTrue(cancelButton, "Cancel button is not displayed");
				
				//////scrolltoend();
				waitforPagetobeenable();
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.cancelButton);
				waitforPagetobeenable();
				
			//	sa.assertAll();
				Report.LogInfo("Info"," Fields are verified","PASS");
			   }catch(AssertionError e) {
				 e.printStackTrace();
			   }	
				


			}
	   
		 
		 
		 
		 
		 public void selectsubtypeunderServiceTypeSelected(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws InterruptedException, IOException {
				
				String SelectSubService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Servicesubtype");
				String Interfacespeed=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Interfacespeed");
				String modularmsp=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Modularmsp");
				String autoCreateService=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AutocreateService");
				String A_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A_Endtechnology");
				String B_Endtechnologydropdown=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B_Endtechnology");
				
			//	scrolltoend();
				waitforPagetobeenable();


				if (modularmsp.equalsIgnoreCase("no") && autoCreateService.equalsIgnoreCase("no")) {
					
					ExtentTestManager.getTest().log(LogStatus.PASS,"when'Modular msp' and 'Autocreate service' are not selected,   'Interface speed' value and 'Service subtype' value should be selected as mandatory ");

					// Select interface speed
						addDropdownValues_commonMethod("Interface Speed", Lanlink_Outbandmanagement.Outbandmanagement.InterfaceSpeed, Interfacespeed);
			
			
			// select service sub type
			boolean serviceSubTypeAvailability=false;
			serviceSubTypeAvailability=	findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype).isDisplayed();

			if(serviceSubTypeAvailability) {
				ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
				Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
				waitforPagetobeenable();
				Reporter.log("clicked on srvice type");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
				
				if((SelectSubService.equals("LANLink International")) || (SelectSubService.equals("LANLink Metro")) || SelectSubService.equals("LANLink National") ||
				         SelectSubService.equals("OLO - (GCR/EU)") || (SelectSubService.equals("Direct Fiber")) || (SelectSubService.equals("LANLink Outband Management"))){

				WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
				el2.click();
				Reporter.log("=== Service sub Type selected===");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type" +SelectSubService +" has been selected");
				}
				else{
					Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
					
					ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
							+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
							+ "    1) Direct Fiber"
							+ "    2) LANLink International"
							+ "    3) LANLink Metro"
							+ "    4) LANLink National"
							+ "    5) Lanlink Outband management"
							+ "    6) OLO - (GCR/EU)");
//					driver.close();
				}

			}else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
			}

					
//					 clickon(getwebelement(xml.getlocator("//locators/"+application+"/AvailableCircuits")));

					click(Lanlink_Outbandmanagement.Outbandmanagement.Next);
					waitforPagetobeenable();

					Report.LogInfo("Info","Page has to be selected based on service and its subtype selected","Pass");

				}

				else if (modularmsp.equalsIgnoreCase("yes") && autoCreateService.equalsIgnoreCase("no")) {
					
					ExtentTestManager.getTest().log(LogStatus.INFO,"when 'Modular msp' is selected and 'Autocreateservice' is not selected, 'Service subtype' value should be selected as it is mandatory field ");
					
		          try {
					click(Lanlink_Outbandmanagement.Outbandmanagement.modularmspcheckbox);
					ExtentTestManager.getTest().log(LogStatus.PASS, "Modular msp checkbox has been selected");
		          }catch(Exception e) {
		        	  e.printStackTrace();
		        	  ExtentTestManager.getTest().log(LogStatus.FAIL, "Modular msp check box is not available");
		        	  
		          }

			// select service sub type
		      	boolean serviceSubTypeAvailability=false;
				serviceSubTypeAvailability=	findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype).isDisplayed();
			
				if(serviceSubTypeAvailability) {
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
					Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
					
					click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
					waitforPagetobeenable();
					Reporter.log("clicked on srvice type");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
					
					if(SelectSubService.equals("LANLink International") || SelectSubService.equals("LANLink Metro") || SelectSubService.equals("LANLink National") ||
					         SelectSubService.equals("OLO - (GCR/EU)")){

					WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
					el2.click();
					Reporter.log("=== Service sub Type selected===");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type" +SelectSubService +" has been selected");
					}
					else{
						Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
						
						ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
								+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
								+ "    1) LANLink International"
								+ "    2) LANLink Metro"
								+ "    3) LANLink National"
								+ "    4) OLO - (GCR/EU)");
//						driver.close();
					}

				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
				}

//					 SendKeys(getwebelement(xml.getlocator("//locators/"+Application+"/AvailableCircuits")),
//					 Availablecircuits);

					click(Lanlink_Outbandmanagement.Outbandmanagement.Next);
					waitforPagetobeenable();

					Reporter.log("Page has to be selected based on service and its subtype selected");

				}

				if (modularmsp.equalsIgnoreCase("no") && autoCreateService.equalsIgnoreCase("yes")) {
					
					
					ExtentTestManager.getTest().log(LogStatus.INFO, " 'Service subtype' should be selected as mandatory when 'AutocreateService' is selected, 'Modular msp' not selected");
					
					Reporter.log("Only auto creta check box is selected");
					
					try {
						waitforPagetobeenable();
					click(Lanlink_Outbandmanagement.Outbandmanagement.AutocreateServicecheckbox);
					waitforPagetobeenable();
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Autocreateservice' checkbox is selected ");
					}catch(Exception e) {
						e.printStackTrace();
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Auto create service' checkbox is not available under 'Service' panel ");
					}

			//A end technology	
				addDropdownValues_commonMethod("A-End Technology",Lanlink_Outbandmanagement.Outbandmanagement.A_Endtechnology, A_Endtechnologydropdown);
				
			//B end technology	
				addDropdownValues_commonMethod("B-End Technology",Lanlink_Outbandmanagement.Outbandmanagement.B_Endtechnology, B_Endtechnologydropdown);
				
			//Interface speed
				addDropdownValues_commonMethod("Interface Speed",Lanlink_Outbandmanagement.Outbandmanagement.InterfaceSpeed, Interfacespeed);
				

			// select service sub type
				
				boolean serviceSubTypeAvailability=false;
				serviceSubTypeAvailability=	findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype).isDisplayed();
			
				if(serviceSubTypeAvailability) {
					ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
					Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
					
					click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
					waitforPagetobeenable();
					Reporter.log("clicked on srvice type");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
					
					if((SelectSubService.equals("LANLink International")) || (SelectSubService.equals("LANLink Metro")) || SelectSubService.equals("LANLink National") ||
					         SelectSubService.equals("OLO - (GCR/EU)") || (SelectSubService.equals("Direct Fiber")) || (SelectSubService.equals("LANLink Outband Management"))){

					WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
					el2.click();
					Reporter.log("=== Service sub Type selected===");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type " +SelectSubService +" has been selected");
					}
					else{
						Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
						
						ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
								+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
								+ "    1) Direct Fiber"
								+ "    2) LANLink International"
								+ "    3) LANLink Metro"
								+ "    4) LANLink National"
								+ "    5) Lanlink Outband management"
								+ "    6) OLO - (GCR/EU)");
						
//						driver.close();
					}

				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
				}
				//scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.Next);
				    verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton,"Next Button");
					click(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton,"Next Button");
					waitforPagetobeenable();

					Report.LogInfo("INFO","Page has to be selected based on service and its subtype selected","PASS");

				}

				if (modularmsp.equalsIgnoreCase("yes") && autoCreateService.equalsIgnoreCase("yes")) {
					
					ExtentTestManager.getTest().log(LogStatus.INFO, " 'Service subtype' is mandatory when 'modular msp' and 'Autocreateservices' are selected");

					
				//modular msp	
					 try {
							click(Lanlink_Outbandmanagement.Outbandmanagement.modularmspcheckbox);
							ExtentTestManager.getTest().log(LogStatus.PASS, "Modular msp checkbox has been selected");
			              }catch(Exception e) {
			            	  e.printStackTrace();
			            	  ExtentTestManager.getTest().log(LogStatus.FAIL, "Modular msp check box is not available under 'Service' panel");
			            	  
			              }

				//Auto create service	 
					 try {
							click(Lanlink_Outbandmanagement.Outbandmanagement.AutocreateServicecheckbox);
							ExtentTestManager.getTest().log(LogStatus.PASS, " 'Autocreateservice' checkbox is selected ");
							}catch(Exception e) {
								e.printStackTrace();
								ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Auto create service' checkbox is not available under 'Service' panel ");
							}

					//A end technology	
						addDropdownValues_commonMethod(Lanlink_Outbandmanagement.Outbandmanagement.A_Endtechnology, A_Endtechnologydropdown, "A-End Technology");
						
					//B end technology	
						addDropdownValues_commonMethod(Lanlink_Outbandmanagement.Outbandmanagement.B_Endtechnology, B_Endtechnologydropdown, "B-End Technology");
						
					
				// select service sub type
					boolean serviceSubTypeAvailability=false;
					serviceSubTypeAvailability=	findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype).isDisplayed();
				
					if(serviceSubTypeAvailability) {
						ExtentTestManager.getTest().log(LogStatus.PASS, " 'Service subtype mandatory dropdown is displaying as expected");
						Reporter.log(" 'Service subtype mandatory dropdown is displaying as expected");
						
						click(Lanlink_Outbandmanagement.Outbandmanagement.ServiceSubtype);
						waitforPagetobeenable();
						Reporter.log("clicked on srvice type");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Service subtype dropdown has been selected");
						
						if(SelectSubService.equals("LANLink International") || SelectSubService.equals("LANLink Metro") || SelectSubService.equals("LANLink National") ||
						         SelectSubService.equals("OLO - (GCR/EU)")){

						WebElement el2 = webDriver.findElement(By.xpath("//div[contains(text(),'" + SelectSubService + "')]"));
						el2.click();
						Reporter.log("=== Service sub Type selected===");
						ExtentTestManager.getTest().log(LogStatus.PASS, "Service sub type" +SelectSubService +" has been selected");
						}
						else{
							Reporter.log(SelectSubService+ " is not available under Service subtype dropdown when Modular msp is selected");
							
							ExtentTestManager.getTest().log(LogStatus.FAIL,SelectSubService+ " is not available when Modular msp is selected."
									+ "                      The list of sub services types under LANLINK when moduler msp is selected are:"
									+ "    1) LANLink International"
									+ "    2) LANLink Metro"
									+ "    3) LANLink National"
									+ "    4) OLO - (GCR/EU)");
//							driver.close();
						}

					}else {
						ExtentTestManager.getTest().log(LogStatus.FAIL, " 'Service subtype mandatory dropdown is not displaying");
					}
					
		scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton);
		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton,"nextbutton");
					click(Lanlink_Outbandmanagement.Outbandmanagement.nextbutton,"nextbutton");
					waitforPagetobeenable();

					Report.LogInfo("INFO","Page has to be selected based on service and its subtype selected","PASS");

				}

			}
		 
		 
		 
		 
		 public void verifyorderpanel_editorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

		 {
		 	String editOrderSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editOrderSelection");
		 	String editorderno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_OrderNumber");
		 	String editvoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditOrder_VoicelineNumber");

		 	if(editOrderSelection.equalsIgnoreCase("no"))
		 	{
		 		Reporter.log("Edit Order is not performed");
		     }
		 	else if(editOrderSelection.equalsIgnoreCase("Yes"))
		 	{
		 		Reporter.log("Performing Edit Order Functionality");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"Action dropdown");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"Action dropdown");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editorderlink,"Edit Order");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.editorderlink,"Edit Order");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editorderheader,"Edit Order");

		 		String EditOrderNo=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.editorderno);
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.editorderno,"edit order no");
		 		clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.editorderno);
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editorderno,"Order Number");
		 		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.editorderno,editorderno,"Order Number");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editvoicelineno,"edit voice line no");
		 		clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.editvoicelineno);
		 		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.editvoicelineno,editvoicelineno,"Order Number");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.cancelbutton,"cancel button");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.cancelbutton,"cancel button");
		 	
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"Action dropdown");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"Action dropdown");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editorderlink,"Edit Order");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.editorderlink,"Edit Order");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editorderheader,"Edit Order Header");
		 		String editOrderHeader=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.editorderheader);
		 		String EditOrder= "Edit Order";
		 		editOrderHeader.equalsIgnoreCase(EditOrder);
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editorderno,"edit order no");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.editorderno,"edit order no");
		 		clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.editorderno);
		 		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.editorderno,editorderno,"Order Number");

		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editvoicelineno,"edit voice line no");
		 		clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.editvoicelineno);
		 		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.editvoicelineno,editvoicelineno,"Order Number");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.editorder_okbutton,"OK Button");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.editorder_okbutton,"OK Button");
		 		
		 		if(editorderno.equalsIgnoreCase("Null"))
		 		{
		 			Reporter.log("Order/Contract Number (Parent SID) field is not edited");
		 		}
		 		else 
		 		{
		 			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ordernumbervalue,"order number value");
		 			String editOrderno=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.ordernumbervalue);
		 			editOrderno.equalsIgnoreCase(editorderno);
		 		}
		 		
		 		if(editvoicelineno.equalsIgnoreCase("Null"))
		 		{
		 			Reporter.log("RFI/RFQ/IP Voice Line Number' field is not edited");
		 		}
		 		else
		 		{
		 			verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.ordervoicelinenumbervalue,"order voice line number value");
		 			String editVoicelineno=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.ordervoicelinenumbervalue);
		 			editVoicelineno.equalsIgnoreCase(editvoicelineno);
		 		}
		 		Reporter.log("Edit Order is successful");	

		 	}

		   }
		 
		 
		 
		 public void verifyorderpanel_changeorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo) throws InterruptedException, IOException

		 {

		 	String changeOrderSelection_newOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_newOrder");
		 	String changeOrderSelection_existingOrder = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "changeOrderSelection_existingOrder");
		 	String ChangeOrder_newOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_newOrderNumber");
		 	String changevoicelineno = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_VoicelineNumber");
		 	String ChangeOrder_existingOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ChangeOrder_existingOrderNumber");

		 	if((changeOrderSelection_newOrder.equalsIgnoreCase("No")) && (changeOrderSelection_existingOrder.equalsIgnoreCase("No")))
		 	{
		 		Reporter.log("Change Order is not performed");	

		 	}
		 	else if(changeOrderSelection_newOrder.equalsIgnoreCase("Yes"))
		 	{
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"Action dropdown");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"Action dropdown");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorderlink,"change order link");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.changeorderlink,"change order link");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorderheader,"change order header");
		 		String changeOrderHeader=getTextFrom(Lanlink_Outbandmanagement.Outbandmanagement.changeorderheader);
		 		String changeOrder = "Change Order";
		 		changeOrderHeader.equalsIgnoreCase(changeOrder);
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorder_selectorderswitch,"select order switch");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.changeorder_selectorderswitch,"select order switch");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeordernumber,"change order number");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.changeordernumber,"change order number");
		 		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.editorderno,ChangeOrder_newOrderNumber);
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeordervoicelinenumber,"change order voiceline number");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.changeordervoicelinenumber,"change order voiceline number");
		 		sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.changeordervoicelinenumber,changevoicelineno);
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.createorder_button,"create order button");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.createorder_button,"create order button");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorder_okbutton,"change order ok button");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.changeorder_okbutton,"change order ok button");
		 				
		 		
		 		Reporter.log("Change Order is successful");	
		 	}
		 	else if(changeOrderSelection_existingOrder.equalsIgnoreCase("yes")) 
		 	{
		 		Reporter.log("Performing Change Order functionality");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"order action button");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.orderactionbutton,"order action button");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorderlink,"change order link");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.changeorderlink,"change order link");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorderheader,"change order");
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorder_chooseorderdropdown,"Order/Contract Number (Parent SID)");
		 		addDropdownValues_commonMethod("Order/Contract Number (Parent SID)",Lanlink_Outbandmanagement.Outbandmanagement.changeorder_chooseorderdropdown,ChangeOrder_existingOrderNumber);
		 		
		 		verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.changeorder_okbutton,"change order ok button");
		 		click(Lanlink_Outbandmanagement.Outbandmanagement.changeorder_okbutton,"change order ok button");
		 		

		 		Reporter.log("Change Order is successful");

		 	}
		 	
		 }
		 
		 
		 
		 
		 public void verifyFieldsandAddCPEdevicefortheserviceselected_10G(String testDataFile,String dataSheet,String scriptNo,String dataSetNo)
					throws InterruptedException, IOException 
			{
			 
			 
			 	String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
			 	String cpename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "devicename_equip");
			 	String vender = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_vender_10G");
			 	String snmpro = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_snmpro");
			 	String managementAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_managementAddress");
			 	String Mepid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_Mepid");
			 	String poweralarm = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_poweralarm_10G");
			 	String mediaSelection = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_Mediaselection");
			 	String Macaddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_Macaddress");
			 	String serialNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_serialNumber");
			 	String hexaSerialnumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_hexaSerialnumber");
			 	String linkLostForwarding = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_linkLostForwarding");
			 	String newmanagementAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_newmanagementAddressSelection");
			 	String existingmanagementAddress = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_existingmanagementAddressSelection");
			 	String manageaddressdropdownvalue = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "cpe_manageaddressdropdownvalue");

				
				
				((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

			waitforPagetobeenable();
			
				click(Lanlink_Outbandmanagement.Outbandmanagement.CPEdevice_adddevicelink,"CPEdevice_adddevicelink");
				waitforPagetobeenable();
				
				
				((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

				waitforPagetobeenable();
				
				click(Lanlink_Outbandmanagement.Outbandmanagement.obutton_spanTag,"OK");
				waitforPagetobeenable();
				
				try {
					
				String linklostForwardingcheckboxstate="disabled"; 
					
				String[] Vender= {"Accedian 10GigE-MetroNode-CE-2Port"};
				
				String[] powerAlarm= {"DC Single Power Supply - PSU A", "DC Dual Power Supply - PSU-A+B"};
				
				String expectedDeviceNameFieldAutopopulatedValue="<Device>-10G.lanlink.dcn.colt.net";
				
				String MEPid="5555";
				
				String expectedValueForSnmpro= "JdhquA5";
				
				

					// Vendor/Model Error Message
					device_vendorModelWarningMessage();

					// Management Address Error Message
					device_managementAddressWarningMessage();

					// Power Alarm Error Message
					device_powerAlarmWarningMessage();

					//serial number Eror Message
					device_serialNumberWarningMessage();
					
					//Hexa serial Number
					device_hexaSerialNumberWarningMessage();
			
					
					//Vendor/Model
					device_vendorModel(Vender, vender);      
				
					//Snmpro
					device_snmPro(snmpro);
					
					//Management Address dropdown
					device_managementAddress(existingmanagementAddress, newmanagementAddress, managementAddress);
					
					//MEP Id
					device_mepID(Mepid);
				
					//Power Alarm	
					device_powerAlarm( poweralarm);
					
					//Serial Number
					device_serialNumber(serialNumber);
				
				    //Link lost Forwarding
					device_linklostForwarding(linkLostForwarding, linklostForwardingcheckboxstate);
				    
					((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

				waitforPagetobeenable();
					//OK button
				click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
					
					//cancel button
					 verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_cancelbutton,"Cancel");
					
					
					click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
					
					 //CAncel button 
					
				     
				     verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
				 click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
					
					((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");

					waitforPagetobeenable();
					
					
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage1_devicename, "Device Name");
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage2_devicename, "Device Name");
					verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.warningmEssage3_devicename, "Device Name");
					
					//Name
					device_nameField(cpename, expectedDeviceNameFieldAutopopulatedValue);
					
					((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");

					waitforPagetobeenable();
					click(Lanlink_Outbandmanagement.Outbandmanagement.AddCPEdevice_OKbutton,"OK");
				    
			//	sa.assertAll();
				
				}catch(AssertionError e) {
					
					e.printStackTrace();
//					
					
				}

}
		 
		 
		 public void editSiteOrder(String testDataFile,String dataSheet,String scriptNo,String dataSetNo)
					throws InterruptedException, IOException {
			 
			 	String interfaceSpeed = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Interfacespeed");
			 	String performReport = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSiteOrder_performReport");
			 	String ProactiveMonitor = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSiteOrder_ProactiveMonitor");
			 	String smartmonitor = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSiteOrder_smartmonitor");
			 	String siteallias = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSiteOrder_siteallias");
			 	String VLANid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSiteOrder_VLANid");
			 	String DCAenabledsite = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSiteOrder_DCAenabledsite");
			 	String cloudserviceprovider = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditSiteOrder_cloudserviceprovider");
			 	String technology = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "technology");
			 	String nontermination = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editsiteorder_nonterminationpoint");
			 	String Protected = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editsiteorder_protected");
			 	String devicename = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editsiteorder_devicename");
			 	String remark = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "editsiteorder_remark");
			 	String IVreference = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "SiteOrder_Ivrefrence");
			 	String siteOrderNumber = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Siteordernumber");
			 	

			 
				verifyExists(Lanlink_Outbandmanagement.Outbandmanagement.EditsideOrderlink,"Edit SideOrderlink");
				click(Lanlink_Outbandmanagement.Outbandmanagement.EditsideOrderlink,"Edit SideOrderlink");
				waitforPagetobeenable();

				((JavascriptExecutor)webDriver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
				waitforPagetobeenable();
				// Point to Point

					if (interfaceSpeed.equals("1GigE")) {
						editSiteOrder_Onetoffnet_1G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
								DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, devicename,
								remark);
					}

					else if (interfaceSpeed.equals("10GigE")) {
						editSiteOrder_OnnetOffnet_10G(performReport, ProactiveMonitor, smartmonitor, siteallias, VLANid,
								DCAenabledsite, cloudserviceprovider, technology, nontermination, Protected, remark);
					}


				scrollDown(Lanlink_Outbandmanagement.Outbandmanagement.okbutton);
				waitforPagetobeenable();
				click(Lanlink_Outbandmanagement.Outbandmanagement.okbutton, "OK");

			}
		 
		 public void editSiteOrder_Onetoffnet_1G(String performReport, String ProactiveMonitor,
					String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
					String technology, String nontermination, String Protected, String devicename, String remark)
					throws InterruptedException, IOException {

				// Performance Reporting
				editSiteOrder_Performancereporting(performReport);

				// Pro active Monitoring
				editSiteOrder_proactiveMonitoring( ProactiveMonitor);

				// Smart Monitoring
				editSiteOrder_smartMonitoring( smartmonitor);

				// Vlan id
				editsiteOrder_vlanid( VLANid);

				// Site alias
				editsiteorder_sitealias( siteallias);

				// DCA Enabled Site
				editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

				//scrolltoend();
				waitforPagetobeenable();
				// Remark
				editsiteOrder_remark( remark);

				// Technology
				editSiteOrder_technology( technology);

				if (technology.equalsIgnoreCase("Actelis")) {
					Reporter.log(" NO additional fields display for technology Actelis");
				}

				if (technology.equalsIgnoreCase("Atrica")) {

					// Non-termination point
					editsiteorder_NonterminationPoint( nontermination);

					// Device Name
					editSiteOrder_deviceName(devicename);

				}

				if (technology.equalsIgnoreCase("Overture")) {

					// Non-termination point
					editsiteorder_NonterminationPoint(nontermination);

				}

				if (technology.equalsIgnoreCase("Alu")) {

					// Device Name
					editSiteOrder_deviceName(devicename);

				}

				if (technology.equalsIgnoreCase("Accedian-1G")) {

					// Non-termination point
					editsiteorder_NonterminationPoint(nontermination);

				}

				if (technology.equalsIgnoreCase("Cyan")) {

					// Non-termination point
					editsiteorder_NonterminationPoint(nontermination);

				}

			}
	  
		 
		 public void editsiteOrder_vlanid(String VLANid) throws InterruptedException, IOException {
				
				boolean vlanAvailability = false;
				
			try {	
				vlanAvailability=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Vlanid);

				if(vlanAvailability) {
				 if(VLANid.equalsIgnoreCase("null")) {
					 Report.LogInfo("Info",  "Vlanid field value is not edited","PASS");
					 Reporter.log("Vlanid field value is not edited");
				 }else {
					 clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Vlanid);
					 waitforPagetobeenable();
					 
					 sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Vlanid, VLANid, "VLAN Id");
					 waitforPagetobeenable();
					 
					String VLANidValue= getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Vlanid,"value");
					Report.LogInfo("Info",  "Edited value for 'Vlan id' field is: "+ VLANidValue,"PASS");
					 Reporter.log( "Edited value for 'Vlan id' field is: "+ VLANidValue);
				 }
				}else {
					Report.LogInfo("Info",  "VLAN Id field is not available under 'Edit Site Order' page","FAIL");
					Reporter.log("VLAN Id field is not available under 'Edit Site Order' page");
				}
			}catch(NoSuchElementException e) {
				e.printStackTrace();
				Report.LogInfo("Info",  "VLAN Id field is not available under 'Edit Site Order' page","FAIL");
				Reporter.log("VLAN Id field is not available under 'Edit Site Order' page");
			}catch(Exception ee) {
				ee.printStackTrace();
				Report.LogInfo("Info",  " Not able to edit 'VLAn Id' text field","FAIL");
				Reporter.log(" not able to edit 'VLAN ID' text field");
			}
			}
		 
		 
		 
		 public void editesiteOrder_DcaEnabled(String DCAenabledsite, String cloudserviceprovider) 
					throws InterruptedException, IOException {
				


				boolean DCAavailability = false;

				try {
					DCAavailability = isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (DCAavailability) {

					if (!DCAenabledsite.equalsIgnoreCase("null")) {

						boolean dcaenabled = isSelected(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox,"DCAenabledsitecheckbox");
						waitforPagetobeenable();

						if (DCAenabledsite.equalsIgnoreCase("yes")) {

							if (dcaenabled) {

								Report.LogInfo("Info",
										"DCA Enabled Site is already Selected while creating","PASS");

								if (cloudserviceprovider.equalsIgnoreCase("null")) {

									Report.LogInfo("Info", "No changes made to Cloud Service Provider","PASS");

								} else {

									addDropdownValues_commonMethod("Cloud Service Provider", Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cloudserviceProvider, cloudserviceprovider);
								}

							} else {

								click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox,"DCAenabledsitecheckbox");
								Reporter.log("DCA Enabled Site check box is selected");
								Report.LogInfo("Info", "DCA Enabled Site checkbox is selected","PASS");

								if (cloudserviceprovider.equalsIgnoreCase("null")) {

									Report.LogInfo("Info", "No changes made to Cloud Service Provider","PASS");

								} else {

									addDropdownValues_commonMethod("Cloud Service Provider", Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_cloudserviceProvider, cloudserviceprovider);
								}
							}

						}

						else if (DCAenabledsite.equalsIgnoreCase("no")) {

							if (dcaenabled) {

								click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_DCAenabledsitecheckbox,"DCAenabledsitecheckbox");
								Reporter.log("DCA Enabled Site check box is unselected");
								Report.LogInfo("Info", "DCA Enabled Site is unselected as Expected","PASS");

							} else {
								Report.LogInfo("Info",
										"DCA Enabled Site was not selected during service creation and it remains unselected as expected","PASS");
							}

						}
					} else {
						Report.LogInfo("Info", "No changes made for DCAenabled site chekbox as expected","PASS");
					}
				} else {
					Report.LogInfo("Info",
							"DCA Enabled Site checkbox is not displaying under 'Edit Site Order' page","FAIL");
				}

			}
		 
		 
		 
		 public void editSiteOrder_technology(String technology) throws InterruptedException {

				boolean techValue = false;

				try {
					techValue=webDriver.findElement(By.xpath("//div[contains(text(),'"+ technology + "')]")).isDisplayed();

					if (techValue) {

						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Technology value is displaying as: " + technology + " as expected");

					} else {
						String actualValue = webDriver.findElement(By.xpath("//div[div[label[contains(text(),'Technology')]]]/div[2]")).getText();
						ExtentTestManager.getTest().log(LogStatus.FAIL,
								" Technology value is not displaying as expected" + "   Actual value displaying is: "
										+ actualValue + "  Expected value for Technology is: " + technology);
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					ExtentTestManager.getTest().log(LogStatus.FAIL, " Technology value is not displaying as expected");
					Reporter.log(" Technology value is not displaying as expected");
				}
			}
		 
		 
		 
		 public void editsiteorder_NonterminationPoint(String nontermination) throws InterruptedException, IOException {
				
				boolean NonTerminationPointAvailability=false;
				
				try {
					NonTerminationPointAvailability=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint);
				
				if(NonTerminationPointAvailability) {
				
			  if(!nontermination.equalsIgnoreCase("null")) {
					
				  boolean nonterminatepoint=false;
				 try { 
				  nonterminatepoint=isSelected(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint,"Non termination point");
				 }catch(Exception e) {
					 e.printStackTrace();
				 }
					waitforPagetobeenable();
					
					if (nontermination.equalsIgnoreCase("yes")) {

						if(nonterminatepoint) {
							
						//	Report.LogInfo("Info", " 'Non-Termination point' checkbox is already Selected while creating","PASS");
							
						}else {
							
							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint);
							//Reporter.log("'Non-Termination point' check box is selected");
						//	Report.LogInfo("Info","'Non-Termination point' is edited and gets selected","PASS");
						}
						
						
					}

					else if (nontermination.equalsIgnoreCase("no")) {
						
						if(nonterminatepoint) {
							
							click(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_nonterminationpoint);
						//	Reporter.log("'Non-Termination point' check box is unselected");
						//	Report.LogInfo("Info","'Non-Termination point' is edited and gets unselected","PASS");
							
						}else {
							//Report.LogInfo("Info", "'Non-Termination point' was not selected during service creation and it remains unselected as expected","PASS");
						}
						
					}
				}else {
				//	Report.LogInfo("Info","No changes made for 'Non-Termination point' chekbox as expected","PASS");
				}
			}else {
			//	Report.LogInfo("Info", " Non-Termination Point checkbox is not available under 'Edit Site order' page","FAIL");
			}
				}catch(NoSuchElementException e) {
					e.printStackTrace();
				//	Report.LogInfo("Info"," Non-Termination Point checkbox is not available under 'Edit Site order' page","FAIL");
				//	Reporter.log(" Non-Termination Point checkbox is not available under 'Edit Site order' page");
				}catch(Exception err) {
					err.printStackTrace();
					//Report.LogInfo("Info", " Not able to click on 'Non-termination point' checkbox ","FAIL");
					//Reporter.log(" Not able to click on 'Non-termination point' checkbox ");
				}
			}
		 
		 
		 
		 public void editSiteOrder_deviceName(String devicename) throws InterruptedException, IOException {
				
				boolean devicenameAvailability=false;
				try {
				
					devicenameAvailability=isElementPresent(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Devicenamefield);
					
				if(devicenameAvailability) {
					
					if(devicename.equalsIgnoreCase("Null")) {
						
						 Report.LogInfo("Info", " NO changes made for 'Device Name' field","PASS");
					}else {
						
						clearTextBox(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Devicenamefield);
						
						sendKeys(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Devicenamefield, devicename, "Device name");
						waitforPagetobeenable();
						
						String actualvalue=getAttributeFrom(Lanlink_Outbandmanagement.Outbandmanagement.Addsiteorder_Devicenamefield,"value");
						 Report.LogInfo("Info", " Device Name value has been edited and the edited value is: "+ actualvalue,"PASS");
					}
				}else {
					 Report.LogInfo("Info", " Device name text field is not displaying under 'Edit Site Order' page","FAIL");
				}
				}catch(NoSuchElementException e) {
					e.printStackTrace();
					 Report.LogInfo("Info", " Device name text field is not displaying under 'Edit Site Order' page","FAIL");
					Reporter.log(" Device name text field is not displaying under 'Edit Site Order' page");
				}catch(Exception ee) {
					ee.printStackTrace();
					 Report.LogInfo("Info", " Not able to edit 'device name' field","FAIL");
					Reporter.log( " Not able to edit 'device name' field");
				}
			}
		 
		 
		 public void editSiteOrder_OnnetOffnet_10G(String performReport, String ProactiveMonitor,
					String smartmonitor, String siteallias, String VLANid, String DCAenabledsite, String cloudserviceprovider,
					String technology, String nontermination, String Protected, String remark)
					throws InterruptedException, IOException {

				// Performance Reporting
				editSiteOrder_Performancereporting( performReport);

				// Pro active Monitoring
				editSiteOrder_proactiveMonitoring(ProactiveMonitor);

				// Smart Monitoring
				editSiteOrder_smartMonitoring(smartmonitor);

				// Vlan id
				editsiteOrder_vlanid(VLANid);

				// Site alias
				editsiteorder_sitealias(siteallias);

				// DCA Enabled Site
				editesiteOrder_DcaEnabled(DCAenabledsite, cloudserviceprovider);

			//	scrolltoend();
				waitforPagetobeenable();
				// Remark
				editsiteOrder_remark(remark);

				// Technology
				editSiteOrder_technology(technology);

				if (technology.equalsIgnoreCase("Accedian")) {

					// Non-termination point
					editsiteorder_NonterminationPoint(nontermination);

				}

			}
		 
		 
		 
		 public boolean EquipmentCOnfigurationPanel() throws InterruptedException {
			 
			 boolean EquipConfigPanel=false;
			 EquipConfigPanel = findWebElement(Lanlink_Outbandmanagement.Outbandmanagement.EquipementConfigurationPanel).isDisplayed();
			 if(EquipConfigPanel) {
				 ExtentTestManager.getTest().log(LogStatus.PASS, "In 'view Site Order' page, 'Equipment Configuration' panel is displaying as expected for 'Actelis' Technology");
			Reporter.log( "In 'view Site Order' page, 'Equipment Configuration' panel is displaying as expected for 'Actelis' Technology");
			 }else {
				 ExtentTestManager.getTest().log(LogStatus.FAIL, "In 'view Site Order' page, 'Equipment Configuration' panel is not displaying for 'Actelis' Technology");
				 Reporter.log("In 'view Site Order' page, 'Equipment Configuration' panel is not displaying for 'Actelis' Technology");
				 
			 }
			return EquipConfigPanel;
			 
		 }
		 
		 
		 
		 public void searchorder(String testDataFile, String dataSheet, String scriptNo, String dataSetNo)
					throws InterruptedException, IOException

			{
				waitforPagetobeenable();
				waitforPagetobeenable();
				String sid = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "serviceNumber");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink, " Manage Customer Service Link");
				mouseMoveOn(APTIPAccessNoCPE.APTNoCPE.ManageCustomerServiceLink);

				verifyExists(APTIPAccessNoCPE.APTNoCPE.searchorderlink, "search order link");
				click(APTIPAccessNoCPE.APTNoCPE.searchorderlink, "search order link");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.servicefield, "service field");
				sendKeys(APTIPAccessNoCPE.APTNoCPE.servicefield, sid);

				click(APTIPAccessNoCPE.APTNoCPE.searchbutton, "searchbutton");
				// click(searchbutton,"searchbutton");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.serviceradiobutton, "service radio button");
				click(APTIPAccessNoCPE.APTNoCPE.serviceradiobutton, "service radio button");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.searchorder_actiondropdown, "search order actiodropdown");
				click(APTIPAccessNoCPE.APTNoCPE.searchorder_actiondropdown, "search order linksearch order actiodropdown");

				verifyExists(APTIPAccessNoCPE.APTNoCPE.view, "view");
				click(APTIPAccessNoCPE.APTNoCPE.view, "view");

			}
	   
	   

		 






}
