package pageObjects.c4cObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class C4CEngagementSalesObj {

	public static class engagementsPage
	{
		public static final String engagementlnk="@xpath=//div[contains(text(), 'Engagement')]"; 
		public static final String editengagementlnk="@xpath=//button[@title='Edit']"; 
		public static final String engagementSEcbx="@xpath=//div[@title='Request SE']";
		public static final String consultantCbx="@xpath=//div[@title='Consultant']";
		public static final String seEngagementLst="@xpath=//span[contains(@title,'SE Engagement')]//following-sibling::div//child::span";
		public static final String defaultListElem="@xpath=//ul[@role='listbox']";
		public static final String saveC4CBtn= "@xpath=//bdi[text()='Save']";
	}
	
	
	public static class salespage
	{
		public static final String salesLst="@xpath=//span[@title='Sales']";
		public static final String quotesTab="@xpath=//a[@title='Quotes']";
		public static final String searchIcon="@xpath=//span[contains(@id,'searchButton-img')]";	//Search Icon
		public static final String searchTxb="@xpath=//input[@type='search']";
		public static final String searchBtn="@xpath=//div[@title='Search']";	//Search Button or Icon
		public static final String actionBtn="@xpath=//bdi[contains(text(),'Action')]"; 
		public static final String EditBtn="@xpath=//bdi[text()='Edit']";
		public static final String LinkToOpportunity="@xpath=//bdi[text()='Link To Opportunity']";
		public static final String linkQuoteLbl="@xpath=//bdi[text(),'Link Quote']"; 
		public static final String OpportunitySearchTxb="@xpath=//span[@title='Opportunity ID']//following::div//input[@type='text']";
		public static final String proceedBtn="@xpath=//bdi[contains(text(),'Proceed')]"; 
		public static final String moreLnk="@xpath=//bdi[text()='More']//parent::span//parent::span//parent::button";		
	}


	
}
