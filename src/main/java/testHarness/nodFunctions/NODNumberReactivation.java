package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import pageObjects.nodObjects.NODNumberDeactivationObj;
import pageObjects.nodObjects.NODNumberReactivationObj;
import pageObjects.nodObjects.NODPortInRequestObj;
import pageObjects.nodObjects.NODTelephoneNumberSearchObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODNumberReactivation extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	

	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void NumberReactivateOnTXpage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		Reusable.Select(NODTelephoneNumberSearchObj.TXDetailpage.ActionDropDown,"Reactivate");
		waitForElementToBeVisible(NODNumberReactivationObj.Reactivate.ReactivateNumberBtn,15);
	    click(NODNumberReactivationObj.Reactivate.ReactivateNumberBtn,"Deacivate numbers button");
	    verifyExists(NODPortInRequestObj.QuickNote.SuccessMsg,"Order Success");
	}
}



