package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;

public class TS_14_BasePrice_City_NotFound extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testBasePriceCityNotFound(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			
			String Flow_Type=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Flow_Type");
			String sResult;
			
			if (Flow_Type.equalsIgnoreCase("Addon")) 
			{
//				calling the below product to configure ip addons product
				sResult = AddProduct.ipAddonProductConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
				}
				else if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
				{
//				Calling the below method to add the product in CPQ
				sResult = AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }

		         }
				else 
				{	
//			calling the below method to enter the site address
			sResult = AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
//			calling the below entry to enter the site details
			sResult = AddProduct.siteDetailEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
			if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			
			if(ProductName.equalsIgnoreCase("ColtIpAccess")) {
//				Calling below method for router configuartion if it is IP Access 
				sResult = AddProduct.routerTypeConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}else {
//				Calling the below method for onnet Configuration
				if(Flow_Type.equalsIgnoreCase("AutomatedNearnet")) {
					AddProduct.nearnetConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				}
				else
				{
					AddProduct.onnetConfiguration(testDataFile,dataSheet, scriptNo, dataSetNo);
				}
			
				}
			}
			
			AddProduct.selectProductFeatures(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.addtionalProductdata(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			if(!ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
			{
				sResult = AddProduct.updateSaveProductCPQ("SaveToQuote");
				if (sResult.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			}
			
			AddProduct.verifyQuoteStage("To be Priced");
			
			String Pricing_City=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Pricing_City");
			String Pricing_City_Exp=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Scenario_Name");
			//String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			String QuoteID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
			String NRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NRC");
			String MRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MRC");
			String BaseCost=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Base_Cost");
			
			AddProduct.editEthernetProductConfiguration();
			AddProduct.verifyQuoteStage("Portfolio Pricing Review");
			Approvals.SwitchCPQUser("AD_User", QuoteID);
			AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.clickProductConfigurationBtn(ProductName);
			if(Pricing_City_Exp.contains("Pricing_City_NotFound"))
			{
				AddProduct.UpdatepricingCityException(ProductName,Pricing_City,NRC,MRC);
			}
			else if(Pricing_City_Exp.contains("BaseCost_NotFound"))
			{
				AddProduct.UpdateBaseCostException(BaseCost);
			}
			else {
				AddProduct.UpdateBasePriceException(NRC, MRC);
			}
			
			Approvals.SwitchCPQUser("Sales_User", QuoteID);
			AddProduct.verifyQuoteStage("Priced");
			CPQLogin.CPQLogout();
			close();

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	
	
}
