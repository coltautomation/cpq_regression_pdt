package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODLoginObj;
import pageObjects.nodObjects.NODLogoutObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODLogoutPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void NODLogout() throws InterruptedException, AWTException, IOException 
	{
		verifyExists(NODLogoutObj.Logout.settingIcon,"Setting Icon");
		click(NODLogoutObj.Logout.settingIcon,"Setting Icon");

		verifyExists(NODLogoutObj.Logout.logoutOption,"Logout option");
		click(NODLogoutObj.Logout.logoutOption,"Logout Option");

			
	}
	
	
	
}
