package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQLoginObj;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.exploreFunctions.ExploreConfig;
import testHarness.siebelFunctions.SiebelLibrary;

public class TS_33_SE_Engagement_Journey extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	ExploreConfig ExpConfig = new ExploreConfig();
	SiebelLibrary Siebel_Library = new SiebelLibrary();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testOnnetOrderCompletion(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws FileNotFoundException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
		
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);	
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			String Quote_ID = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
			String OptionQuote_Applicable = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"OptionQuote_Applicable");
									
			if (!ProductName.equalsIgnoreCase("CpeSolutionsSite")) {
				
				AddProduct.AddProductToQuote(ProductName);
//				calling the below method to enter the site address
				 AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
								
//				calling the below method to configure the product
				AddProduct.productConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
					
			} else {
//				Calling the below method to add the product in CPQ
				AddProduct.AddProductToQuote("CPESolutionsSite");
				AddProduct.cpeServiceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
				AddProduct.AddProductToQuote("CPESite");						
				AddProduct.cpeSiteConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.saveCPQ("Main");
			
			}
	
			AddProduct.verifyQuoteStage("SE Required");
			AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
            
			//Login with CPQ SE user
			close();
			openBrowser(g.getBrowser());		
			openurl(Configuration.CPQ_URL);
			CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
			CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
	
			String HubId = AddProduct.addHubrefernce(ProductName);
			if (HubId.equalsIgnoreCase("False")){ throw new SkipException("Skipping this test"); }
			if(HubId.equals("True"))DataMiner.fnsetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Hub_Reference", HubId);
	
			CPQLogin.CPQLogout();
            //Login with CPQ Sales user
			close();
			openBrowser(g.getBrowser());		
			openurl(Configuration.CPQ_URL);
			CPQLogin.CPQLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);
			CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String NRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NRC");
			String MRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MRC");
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("To be Priced")) 
			{
				AddProduct.editEthernetProductConfiguration(); 
				Approvals.SwitchCPQUser("AD_User", Quote_ID);
				AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.verifyQuoteStage("Portfolio Pricing Review");
				AddProduct.clickProductConfigurationBtn(ProductName);
				AddProduct.UpdateBasePriceException(NRC, MRC);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			
				
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			 if (OptionQuote_Applicable.equalsIgnoreCase("Yes")) {
			  AddProduct.Quotespawn(testDataFile, dataSheet, scriptNo, dataSetNo);
			 }
			 		
				if(ProductName.equalsIgnoreCase("CpeSolutionsSite")) 
				{
				String CPE_Service_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Service_BCN");
				Submit.addBillingInformationCPE("CPESolutionsSite", CPE_Service_BCN, "BCN");
				} else {
				String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
				Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
				}
			
			//String CPE_Site_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Site_BCN");
			//Submit.addBillingInformation("CPE Solutions Site", CPE_Site_BCN);
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);

			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			//Siebel Part
			close();
			String Pick_Siebel = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"Pick_Siebel");
			if (Pick_Siebel.equalsIgnoreCase("Yes")) {
			openBrowser(g.getBrowser());
			openurl(Configuration.Siebel_URL);
			Siebel_Library.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			Siebel_Library.ServiceTab_Bespoke(testDataFile, dataSheet, scriptNo, dataSetNo);
			Siebel_Library.Bespoke_Journey(testDataFile, dataSheet, scriptNo, dataSetNo);
			}

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
	}	

}