package siebelTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;
import testHarness.siebelFunctions.SiebelAccounts;
import testHarness.siebelFunctions.SiebelAddProduct;
import testHarness.siebelFunctions.SiebelCEOS;
import testHarness.siebelFunctions.SiebelCreateCustomerOrder;
import testHarness.siebelFunctions.SiebelLoginPage;
import testHarness.siebelFunctions.SiebelManualValidation;
import testHarness.siebelFunctions.SiebelMode;
import testHarness.siebelFunctions.SiebelNumberManagement;

public class TS_23_SLAMatrix extends SeleniumUtils
{
	ReadExcelFile read = new ReadExcelFile();
	/*ReusableFunctions Reusable = new ReusableFunctions();
	String dataFileName = "Siebel_testData.xlsx";
	File path = new File("./src/test/resources/"+dataFileName);
	String testDataFile = path.toString();
	String tsSheetName = "SiebelToXNG";
	String scriptNo = "17";
	String dataSetNo = "10";
		*/
	
	GlobalVariables g = new GlobalVariables();
	SiebelLoginPage Login = new SiebelLoginPage();
	SiebelCreateCustomerOrder AddProduct = new SiebelCreateCustomerOrder();
	SiebelAccounts Accounts = new SiebelAccounts();
	SiebelAddProduct Product = new SiebelAddProduct();
	SiebelMode siebelmod = new SiebelMode();
	SiebelNumberManagement NumMana = new SiebelNumberManagement();
	SiebelManualValidation Validation = new SiebelManualValidation();
	SiebelCEOS CEOS = new SiebelCEOS();
	   
	Properties prop = new Properties();

	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testNewOrderONNET(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
		
		String dataFileName = prop.getProperty("TestDataSheet");
        File path = new File("./TestData/"+dataFileName);
        String testDataFile = path.toString();
				
		try
		{
			
			Login.SiebelLogin(Configuration.SiebelUser_Username, Configuration.SiebelUser_Password);
			siebelmod.OpenServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
			Product.validateSlaMatrix(testDataFile, dataSheet, scriptNo, dataSetNo);
					}
		catch(Exception e)
	    {
	            Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
	            ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
	            ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
	    }
	        Report.createTestCaseReportFooter();
	        Report.SummaryReportlog(ScenarioName);
			
	}
	/*
	@Test(priority = 1, groups={"testSiebelLogin"})
	public void testSiebelLogin()
	{
		Report.createTestCaseReportHeader();	
				
		try
		{
			Login.SiebelLogin(Configuration.SalesUser1_Username, Configuration.SalesUser1_Password);	
	
		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testSiebelLogin "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testSiebelLogin "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify Siebel Login page functionality");
		
	}	
	
	
	@Test(priority = 2, groups={"testOpenServiceOrder"}, dependsOnGroups = { "testSiebelLogin" })
	public void testOpenServiceOrder()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
		//	Thread.sleep(30000);
			siebelmod.OpenServiceOrder(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testOpenServiceOrder "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testOpenServiceOrder "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify that able to open service order");
		
	}

	@Test(priority = 3, groups={"testValidateSlaMatrix"}, dependsOnGroups = { "testOpenServiceOrder" })
	public void testValidateSlaMatrix()
	{
		Report.createTestCaseReportHeader();	
		
		try
		{
	//		Thread.sleep(30000);
			Product.validateSlaMatrix(testDataFile, tsSheetName, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in testValidateSlaMatrix "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in testValidateSlaMatrix "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog("Verify SLA Matrix functionality");
		
	}
	
	*/

}

	
	
	
	
	
	
	
	