package pageObjects.nodObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NODMyOrdersObj {

	public static class MyOrder
	{ 
		public static final String ServiceProfiledrpdwn="@xpath=//input[@role='combobox']"; 
		public static final String ProfileOption="@xpath=//div[@role='option']"; 
		public static final String StartButton="@xpath=//button[contains(text(),'get started')]"; 

		//Home Page
		public static final String HomeMenu="@xpath=//a[contains(text(),'Home')]"; 
		public static final String QuickLinkDrpDwn="@xpath=//div[contains(@class,'ng-select-container')]//div[contains(text(),'Quick Links')]"; 
		public static final String QuickLinkDrpDwnArw="@xpath=//body/app-root[1]/app-my-orders[1]/div[1]/div[1]/div[1]/div[2]/app-profile[1]/div[1]/form[1]/div[1]/ng-select[1]/div[1]/span[1]";

		public static final String QuickLinkOptions="@xpath=//*[contains(@class,'ng-dropdown-panel ng-star-inserted ng-select-bottom')]"; 
		public static final String WelcomeMsg="@xpath=//h5[contains(text(),'Welcome  ')]"; 

		//Order tabel
		public static final String OrderIDColumn="@xpath=//div[contains(text(),'Order Id')]";
		public static final String OrderIDVal="@xpath=(//*[@tabulator-field='transactionId'])[2]";
		
		public static final String OrderTypeColumn="@xpath=//div[contains(text(),'Order Type')]"; 
		public static final String OrderTypeVal="@xpath=(//*[@tabulator-field='transactionType'])[2]"; 

		public static final String OrderCreationDateColumn="@xpath=//div[contains(text(),'Order Creation Date')]"; 
		public static final String OrderCreationDateVal="@xpath=(//*[@tabulator-field='transactionCreationDate'])[2]"; 

		public static final String OrderUpdateDateColumn="@xpath=//div[contains(text(),'Order Update Date')]";
		public static final String OrderUpdateDateVal="@xpath=(//*[@tabulator-field='transactionLastUpdateDate'])[2]"; 

		public static final String RangeStartColumn="@xpath=//div[contains(text(),'Range Start')]"; 
		public static final String RangeStartVal="@xpath=(//*[@tabulator-field='telNumberStart'])[2]"; 

		public static final String RangeEndColumn="@xpath=//div[contains(text(),'Range End')]"; 
		public static final String RangeEndVal="@xpath=(//*[@tabulator-field='telNumberEnd'])[2]"; 

		public static final String QuantityColumn="@xpath=//div[contains(text(),'Quantity')]"; 
		public static final String QuantityVal="@xpath=(//*[@tabulator-field='quantity'])[2]"; 

		public static final String OrderStatusColumn="@xpath=//div[contains(text(),'Order Status')]"; 
		public static final String currentStatusVal="@xpath=(//*[@tabulator-field='currentStatus'])[2]"; 

		//Order Details
		public static final String OrderDetailsSection="@xpath=//a[contains(text(),'Order Details')]"; 
		public static final String OrderDetailsPage="@xpath=//h5[contains(text(),'Order Details')]"; 

		public static final String NumberDetailsection="@xpath=//a[contains(text(),'Number Details')]"; 
		public static final String NumberDetailsPage="@xpath=//h4[contains(text(),'Telephone Numbers')]"; 

		public static final String HistoryConversationSection="@xpath=//a[contains(text(),'History / Conversation')]"; 
		public static final String HistoryConversationPage="@xpath=//div[@id='NewPortIncollapseStepper']"; 

		public static final String NumberHistorysSection="@xpath=//a[contains(text(),'Number History')]"; 
		public static final String NumberHistoryField="@xpath=//h6[contains(text(),'Search for a number')]"; 

		public static final String DownlaodExelIcon="@xpath=(//button[@class='ct-icon-box'])[4]"; 
		
	}
	public static class QuickLinks
	{ 
		public static final String HelpOption="@xpath=//label[contains(text(),'Help')]"; 
		public static final String HelpPageTxt="@xpath=//span[contains(text(),'Colt Online Help and Support')]"; 
		public static final String SearchTxtFld="@xpath=//*[@class='input-group']//*[@placeholder='Search']"; 
		public static final String SearchIcon="@xpath=//*[@class='input-group']//button[@type='button']"; 
	
	}
	
/////Ashwini
	public static class FilterOrder
	{ 
		//public static final String OrderUpdatedDateInFilter ="@xpath=//input[@id='transactionCreationDate']";
		public static final String filterOption="@xpath=//div[@class='filters-container']/span[1]/button";
		public static final String Countrydropdown="@xpath=//label[contains(text(),'Country and Profile')]//parent::div/descendant::span[contains(text(),'Select')][1]";
		public static final String selectCountry ="@xpath=//label[contains(text(),'country')]";
		public static final String UpCountrydrpdown ="@xpath=//span[contains(text(),'country')]";
		public static final String OrderCreationDate="@xpath=//label[@for='transactionCreationDate']//following-sibling::div[@class='input-group custom-input-calendar']//button";
		public static final String OrderCreationMonth="@xpath=//select[@title='Select month']";
		public static final String OrderCreationYear="@xpath=//select[@title='Select year']";
		public static final String OrderCreationDay="@xpath=//div[@class='ngb-dp-month ng-star-inserted'][1]//span[contains(text(),'day')]";
		public static final String CloseCalendar ="@xpath=//button[contains(text(),'Close')]";
		public static final String ApplyBtn ="@xpath=//button[contains(text(),'Apply')]";
		public static final String SearchedData ="@xpath=//div[@class='tabulator-header']";
		public static final String OrderTypedrpDown ="@xpath=//label[contains(text(),'Order Type')]//parent::div/descendant::span[contains(text(),'Select')][1]";
		public static final String selectOrderType ="@xpath=//ul[@class='lazyContainer']/li/label[contains(text(),'orderType')]";
		public static final String OrderStatusdrpDown ="@xpath=//label[contains(text(),'Order Status')]//parent::div/descendant::span[contains(text(),'Select')][1]";
		public static final String selectOrderStatus ="@xpath=//ul[@class='lazyContainer']/li/label[contains(text(),'orderStatus')]";
		public static final String orderUpdationDate ="@xpath=//label[@for='transactionLastUpdateDate']//following-sibling::div[@class='input-group custom-input-calendar']//button";
		public static final String  SearchedDataValue ="@xpath=//div[@class='tabulator-table']";
		public static final String OrderType ="@xpath=(//div[@title='OrderType'])";
		public static final String resetFilterBtn ="@xpath=//button[contains(text(),'Reset')]";
		public static final String DataWithoutFilter ="@xpath=//div[@id='tbltr_wwebsomoj']";
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}