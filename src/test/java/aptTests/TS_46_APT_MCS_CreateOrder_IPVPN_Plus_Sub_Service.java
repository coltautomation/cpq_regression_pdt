package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.APT_MCS_CreateOrder_IPVPN;
import testHarness.aptFunctions.APT_MCS_CreateOrder_IPVPN_Plus;
import testHarness.commonFunctions.ReusableFunctions;

public class TS_46_APT_MCS_CreateOrder_IPVPN_Plus_Sub_Service extends SeleniumUtils{

	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	GlobalVariables g = new GlobalVariables();
	APT_Login Login=new APT_Login();
	APT_MCS_CreateOrder_IPVPN_Plus CreateOrderIPVPN = new APT_MCS_CreateOrder_IPVPN_Plus();
	APT_MCS_CreateOrder_IPVPN CreateOrderIPVPNSec = new APT_MCS_CreateOrder_IPVPN();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void APT_MCS_CreateOrder_IPVPNSecSubService(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)throws IOException, InterruptedException
	{
		
		Report.createTestCaseReportHeader(ScenarioName);
	
	try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties"))
	{

        // load a properties file
        prop.load(input);               

    }
	catch (IOException ex) 
	{
        ex.printStackTrace();
    }


	String dataFileName = prop.getProperty("TestDataSheet");
	File path = new File("./TestData/"+dataFileName);
	String testDataFile = path.toString();
	
	String newCustomerName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "New_Customer");
	String Service_Type = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceType");
	String Servicesubtype = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ServiceSubType");
	String VPNName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VPN_Name");
	String VPNAlis = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VPN_Alis");
	String ManageUserName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "ManageUserName");
	String EditUserName = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "EditUserName");
	//String VPNSiteOrderHeader=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "VPNSiteOrderHeader");
	String DSLsiteOrder=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "DSLsiteOrder");
  	
	try
	{
		openurl(Configuration.APT_URL);
		Login.APT_VoiceService();		
		
		/*if(newCustomerName.equalsIgnoreCase("Yes")) {
			CreateOrderIPVPN.createcustomer(testDataFile, dataSheet, scriptNo, dataSetNo);		
			
			}else {
			
			CreateOrderIPVPN.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
				
			}
			*/
		CreateOrderIPVPN.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);		
		CreateOrderIPVPN.createneworderservice(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifyservicetypeandSubtype(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.createservice(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.selectCustomertocreateOrder(testDataFile, dataSheet, scriptNo, dataSetNo);	
		
		Report.LogInfo("INFO", "=====verifyorderpanelinformation_Neworder====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifyorderpanelinformation_Neworder(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====verifyorderpanel_editorder====", "PASS");
		CreateOrderIPVPN.verifyorderpanel_editorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====verifyorderpanel_changeorder====", "PASS");
		CreateOrderIPVPN.verifyorderpanel_changeorder(testDataFile, dataSheet, scriptNo, dataSetNo);	
		
		Report.LogInfo("INFO", "=====verifyservicepanel_links====", "PASS");
		CreateOrderIPVPN.verifyservicepanel_links(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====Editservice====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.Editservice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====syncservices====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.syncservices(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.Dumpservice();
		
		Report.LogInfo("INFO", "=====verifyManagementOptionspanel====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifyManagementOptionspanel(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====AddManageUser====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddManageUser(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====ViewManageUser====", "PASS");
		CreateOrderIPVPN.ViewManageUser(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====EditManageUser====", "PASS");
		CreateOrderIPVPN.EditManageUser(testDataFile, dataSheet, scriptNo, dataSetNo,ManageUserName,EditUserName);
		
		Report.LogInfo("INFO", "=====DeleteManageUser====", "PASS");
		CreateOrderIPVPN.DeleteManageUser(Servicesubtype,EditUserName);
		
	//************Note - VPN Alias functionality is not available *********************//
		//Report.LogInfo("INFO", "=====AddVPNAlis====", "PASS");
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.AddVPNAlis(testDataFile, dataSheet, scriptNo, dataSetNo,Servicesubtype,VPNName,VPNAlis);
		
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.EditVPNAlis(testDataFile, dataSheet, scriptNo, dataSetNo,Servicesubtype,VPNName,VPNAlis);
		
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.DeleteVPNAlis(Servicesubtype,VPNName,VPNAlis);
		
		//CreateOrderIPVPN.AddVPNSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
		Report.LogInfo("INFO", "=====AddVPNSiteOrderPlus====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddVPNSiteOrderPlus(testDataFile, dataSheet, scriptNo, dataSetNo);
			
		Report.LogInfo("INFO", "=====verifyAddedVPNSitePlusInformation_View====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifyAddedVPNSitePlusInformation_View(testDataFile, dataSheet, scriptNo, dataSetNo);
			
		
					
		//************Note - Facing issue if select configuration option as Wholesale VPN. (IPVPN Sec & IPVPN Connect & IPVPN Plus)
		Report.LogInfo("INFO", "If we select wholesale VPN check box value and try to create order then getting error", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "If we select wholesale VPN check box value and try to create order then getting error");
		//Report.LogInfo("INFO", "=====Add/Edit/Delete WholesaleInterconnect====", "PASS");	
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.AddWholesaleInterconnect(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.EditWholesaleInterconnect(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.DeleteWholesaleInterconnect(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====AddCPEDevice====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddCPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.ViewCPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.EditCPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====AddInterfaceCPE====", "PASS");
	//********** Note - If we select bearer type on Add Interface then We're sorry - something's gone wrong. error displayed	
	//********** Note Commented code due to Bearer type issue ***************//
		Report.LogInfo("INFO", "If we select Bearer type on Add Interface then We're sorry blank page getting", "FAIL");
		ExtentTestManager.getTest().log(LogStatus.FAIL, "If we select Bearer type on Add Interface then We're sorry blank page getting");
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.AddInterfaceCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
		
	//********** Edit and Delete Interface option is not available so commenting code ******************//
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.EditInterfaceCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.CPEDeviceConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
	
	//********** Edit and Delete Interface option is not available so commenting code ******************//
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.SelectInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		//CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		//CreateOrderIPVPN.DeleteInterfaceCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
		
	//***************Note - PPP configuration section is not showing on oder
		/*if ((Servicesubtype.contains("IPVPN"))&& (!Servicesubtype.equalsIgnoreCase("IPVPN Access"))) {
		Report.LogInfo("INFO", "=====PPP configuration====", "PASS");		
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.CPEdevice_clickOnPPPconfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.pppConfiguration();
		CreateOrderIPVPN.addPPPconfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		CreateOrderIPVPN.verifysuccessmessage("Device successfully created.");
		
		CreateOrderIPVPN.viewPPPconfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		
		CreateOrderIPVPN.editPPPconfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.verifysuccessmessage("Device successfully updated.");
		
		}
		*/
		
		Report.LogInfo("INFO", "=====Add Multi link====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddMultilinkCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====AddRoutesCPE====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddRoutesCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====EditRoutesCPE====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.EditRoutesCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====DeletRoutesCPE====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.DeleteRoutesCPE(testDataFile, dataSheet, scriptNo, dataSetNo);
		
		Report.LogInfo("INFO", "=====AddRouterTool====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.AddRouterTool(testDataFile, dataSheet, scriptNo, dataSetNo);
				
		Report.LogInfo("INFO", "=====DeleteCPEDevice====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.DeleteCPEDevice(testDataFile, dataSheet, scriptNo, dataSetNo);
			
		
		Report.LogInfo("INFO", "=====Existing PE Device ====", "PASS");
		if (Servicesubtype.contains("IPVPN")) {		
			
			CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
			boolean EquipmentPanel=CreateOrderIPVPN.findPanelHeader(testDataFile, dataSheet, scriptNo, dataSetNo);

				if(EquipmentPanel) {
					
					//Report.LogInfo("INFO", "=====Add Existing PE device====", "PASS");
					CreateOrderIPVPN.SelectPEdevice_existingDevice(testDataFile, dataSheet, scriptNo, dataSetNo);				
					CreateOrderIPVPN.verifysuccessmessage("Device successfully created.");				
					CreateOrderIPVPN.verifyValuesforProviderEqiupment();				
					boolean link =CreateOrderIPVPN.fetchdeviceInterface();
					
				}else {
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Provider Equipment (PE) panel is not displaying");
					Report.LogInfo("INFO","Provider Equipment (PE) panel is not displaying","FAIL");
				}
				
				String interfaceCreation = "No";
				String multilinkCreation = "No";
				String interfaceName = "Null";
				String interfaceNameUpdated = "Null";
				String multiLink = "Null";
				
					String vendorModel = CreateOrderIPVPN.fetchVendorModel_PE();				
					String managementAddress = CreateOrderIPVPN.fetchManagementAddress_PE();
					
					if(DSLsiteOrder.equalsIgnoreCase("Yes")) {
						
						if(vendorModel.contains("Juniper")) {
							CreateOrderIPVPN.routerPanel_juniper(testDataFile, dataSheet, scriptNo, dataSetNo);
						}
						else if(vendorModel.contains("Cisco")) {
							
							CreateOrderIPVPN.routerPanel_Cisco(testDataFile, dataSheet, scriptNo, dataSetNo);
							
						}
						
						Report.LogInfo("INFO", "=====addInterface_DSlsiteorderSelected====", "PASS");
						CreateOrderIPVPN.clickOnAddInterfaceLink();
						CreateOrderIPVPN.addInterface_DSlsiteorderSelected(testDataFile, dataSheet, scriptNo, dataSetNo);
						CreateOrderIPVPN.verifysuccessmessage("Interface successfully created.");
						
						Report.LogInfo("INFO", "=====successmessageForInterfaceOrMultilinkCreation====", "PASS");
						interfaceCreation = CreateOrderIPVPN.successmessageForInterfaceOrMultilinkCreation("successfully created");
						
						Report.LogInfo("INFO", "=====addMultilink_DSLsiteOrderselected====", "PASS");
						CreateOrderIPVPN.clickOnAddMultilinkLink();
						CreateOrderIPVPN.addMultilink_DSLsiteOrderselected(testDataFile, dataSheet, scriptNo, dataSetNo);
						
						CreateOrderIPVPN.verifysuccessmessage( "Interface successfully created.");
						multilinkCreation = CreateOrderIPVPN.successmessageForInterfaceOrMultilinkCreation("successfully created");
						
						
						CreateOrderIPVPN.clickOnAddLoopback();
						String interfaceName_loopback = CreateOrderIPVPN.addLoopback(testDataFile, dataSheet, scriptNo, dataSetNo); 
						
					}
					else {
						if(vendorModel.contains("Juniper")) {
							
	
							Report.LogInfo("INFO", "=====addInterface_Juniper====", "PASS");
							CreateOrderIPVPNSec.clickOnAddInterfaceLink();
							interfaceName  = CreateOrderIPVPN.addInterface_Juniper(testDataFile, dataSheet, scriptNo, dataSetNo);
							
							CreateOrderIPVPNSec.verifysuccessmessage("Interface successfully created.");
							interfaceCreation = CreateOrderIPVPNSec.successmessageForInterfaceOrMultilinkCreation("successfully created");
							
							Report.LogInfo("INFO", "=====addMultilink_DSLsiteOrderselected====", "PASS");
							CreateOrderIPVPNSec.clickOnBreadCrump(testDataFile, dataSheet, scriptNo, dataSetNo);
							
							CreateOrderIPVPNSec.clickOnAddMultilinkLink();						
							multiLink =CreateOrderIPVPN.addMultilink(testDataFile, dataSheet, scriptNo, dataSetNo);
							
							Report.LogInfo("INFO", "=====Interface successfully created====", "PASS");
							CreateOrderIPVPNSec.verifysuccessmessage( "Interface successfully created.");
							multilinkCreation = CreateOrderIPVPNSec.successmessageForInterfaceOrMultilinkCreation("successfully created");
							
							
							//Edit Interface
							Report.LogInfo("INFO", "=====Interface Edit====", "PASS");
							if(interfaceCreation.equalsIgnoreCase("Yes")) {
								CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
								CreateOrderIPVPNSec.PEinterface_clickOEditlink(interfaceName);
								
							interfaceNameUpdated  = CreateOrderIPVPNSec.editInterface_Juniper(testDataFile, dataSheet, scriptNo, dataSetNo);
							
							CreateOrderIPVPNSec.verifysuccessmessage("Interface successfully updated.");
							CreateOrderIPVPNSec.successmessageForInterfaceOrMultilinkCreation("successfully updated");
							interfaceName = interfaceNameUpdated;
						}
							
											
						}
						else if(vendorModel.contains("Cisco")) {
							
							CreateOrderIPVPN.routerPanel_Cisco(testDataFile, dataSheet, scriptNo, dataSetNo);
							
							CreateOrderIPVPN.clickOnAddInterfaceLink();
						    interfaceName =	CreateOrderIPVPN.addInterface_Cisco(testDataFile, dataSheet, scriptNo, dataSetNo);
						    CreateOrderIPVPN.verifysuccessmessage("Interface successfully created.");
						    interfaceCreation = CreateOrderIPVPN.successmessageForInterfaceOrMultilinkCreation("successfully created");
						    					   
						    CreateOrderIPVPN.clickOnBreadCrump(testDataFile, dataSheet, scriptNo, dataSetNo);
						    CreateOrderIPVPN.clickOnAddMultilinkLink();
						    String multilinkName =  CreateOrderIPVPN.addMultilink_Cisco(testDataFile, dataSheet, scriptNo, dataSetNo);
						    CreateOrderIPVPN.verifysuccessmessage("Interface successfully created.");
						    multilinkCreation =  CreateOrderIPVPN.successmessageForInterfaceOrMultilinkCreation("successfully created");
						}
					}
					
					
				//Select interface
					Report.LogInfo("INFO", "=====Remove interface and multilink  ====", "PASS");
					if(interfaceCreation.equalsIgnoreCase("Yes")) {
						CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
						CreateOrderIPVPNSec.PEdevice_clickOnselectInterface(testDataFile, dataSheet, scriptNo, dataSetNo);
						CreateOrderIPVPNSec.SelectInterfacetoremovefromservice();
					}
					
					
					//Autodiscover VPn
					Report.LogInfo("INFO", "=====PEdevice_clickOnAutodiscoverVPN====", "PASS");
					CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
					CreateOrderIPVPN.PEdevice_clickOnAutodiscoverVPN(testDataFile, dataSheet, scriptNo, dataSetNo);
					
				
					//Delete Provier Equipment
					Report.LogInfo("INFO", "=====deletePEdevice====", "PASS");
					CreateOrderIPVPN.deletePEdevice(testDataFile, dataSheet, scriptNo, dataSetNo);
					CreateOrderIPVPN.verifysuccessmessage("Device successfully removed from service.");
					
			
				Report.LogInfo("INFO", "=====verifyAddDSLAMandHSLlink====", "PASS");
				CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
				CreateOrderIPVPN.verifyAddDSLAMandHSLlink(testDataFile, dataSheet, scriptNo, dataSetNo);
				CreateOrderIPVPN.AddDSLAMandHSL(testDataFile, dataSheet, scriptNo, dataSetNo);
				CreateOrderIPVPN.showInterface_ActelisConfiguuration(testDataFile, dataSheet, scriptNo, dataSetNo);
				
		}
		
		Report.LogInfo("INFO", "=====DeleteVPNSiteOrder====", "PASS");
		CreateOrderIPVPN.searchorder(testDataFile, dataSheet, scriptNo, dataSetNo);
		CreateOrderIPVPN.DeleteVPNSiteOrder(testDataFile, dataSheet, scriptNo, dataSetNo);
	}
	catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
			
		}
		
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
		WEB_DRIVER_THREAD_LOCAL.get().close();
	}

	
}
