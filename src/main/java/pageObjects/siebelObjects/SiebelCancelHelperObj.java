package pageObjects.siebelObjects;

public class SiebelCancelHelperObj {
	public static class CancelHelper
	 { 
		public static final String orderStatus="@xpath=//input[@aria-label='Order Status']/..//span";
		public static final String statusReasonField="@xpath=//input[@aria-label='Status Reason']";
		public static final String statusReasonDropDown="@xpath=//input[@aria-labelledby='COLT_Status_Reason_Code_Label']/following-sibling::span";
		//public static final String dropDownValue="@xpath=//li[text()='Value']";
		public static final String dropDownValue="@xpath=//li/*[text()='Value']";
		public static final String orderComplete="@xpath=//button[text()='Yes']";
		//public static final String orderCancelled="@xpath=//li[text()='Cancelled']";
		public static final String orderCancelled="@xpath=//li/*[text()='Cancelled']";
//		public static final String orderAbandoned="@xpath=//li[text()='Abandoned']";
		public static final String orderAbandoned="@xpath=//li/*[text()='Abandoned']";//updated Ip2020
		public static final String alertAccept="@xpath=//span[text()='Ok']";
		public static final String serviceTab="@xpath=//a[text()='Service Order']";
		public static final String serviceOrderSearch="@xpath=//input[@aria-label='Starting with']";
		public static final String serviceOrderArrow="@xpath=//input[@aria-label='Starting with']/..//button[1]";
		public static final String verifyCancelled="@xpath=//td[text()='Cancelled']";
		public static final String verifyAbandoned="@xpath=//td[text()='Abandoned']";
		public static final String IPAccessAbandoned="@xpath=//button[text()='Yes - Abandon']";
	 }
}
