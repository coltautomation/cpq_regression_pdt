package cpqTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentManager;
import baseClasses.ExtentTestManager;
import baseClasses.RecordVideo;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.cpqObjects.CPQQuoteCreationObj;
import testHarness.c4cFunctions.C4CAccounts;
import testHarness.c4cFunctions.C4CLoginPage;
import testHarness.c4cFunctions.C4COpportunity;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.cpqFunctions.CPQLoginPage;
import testHarness.cpqFunctions.CPQQuoteApprovals;
import testHarness.cpqFunctions.CPQQuoteCreation;
import testHarness.cpqFunctions.CPQQuoteSubmission;

public class TS_02_Override_Onnet_ULLFibre extends SeleniumUtils
{
	ReusableFunctions Reusable = new ReusableFunctions();
	Properties prop = new Properties();
	
	C4CLoginPage C4CLogin = new C4CLoginPage();
	CPQLoginPage CPQLogin = new CPQLoginPage();
	C4CAccounts Accounts = new C4CAccounts();
	C4COpportunity Opportunity = new C4COpportunity();
	CPQQuoteCreation AddProduct = new CPQQuoteCreation();
	CPQQuoteApprovals Approvals = new CPQQuoteApprovals();
	CPQQuoteSubmission Submit = new CPQQuoteSubmission();
	RecordVideo record = new RecordVideo();
	
	@Parameters({ "scriptNo","dataSetNo", "dataSheet", "ScenarioName" })
	@Test
	public void testOverrideOnnetULLFibre(String scriptNo,String dataSetNo, String dataSheet, String ScenarioName) throws IOException
	{
		Report.createTestCaseReportHeader(ScenarioName);	
		
		try (InputStream input = new FileInputStream(g.getRelativePath()+"/configuration.properties")) {

            // load a properties file
            prop.load(input);               

        } catch (IOException ex) {
            ex.printStackTrace();
        }
			
		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/"+dataFileName);
		String testDataFile = path.toString();
		String botId = prop.getProperty("BotId");			
		record.startVideoRecording(botId, ScenarioName);
				
		try
		{
			openurl(Configuration.C4C_URL);
			C4CLogin.C4CLogin();
			Accounts.NavigateToAccounts();
			Accounts.SearchAccount(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.NavigateToOpportunities(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.OpenNewOpportunity();
			Opportunity.EnterOpportunityDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.editOpportunity(testDataFile, dataSheet, scriptNo, dataSetNo);
			Opportunity.ClickToAddQuote();
			AddProduct.HandoverToCPQ(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			String ProductName=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Product_Name");
			AddProduct.AddProductToQuote(ProductName);
			AddProduct.AddSiteDetails(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.siteDetailEntry(testDataFile, dataSheet, scriptNo, dataSetNo);
			
			AddProduct.overrideOnnetUllFibre(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.selectProductFeatures(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.addtionalProductdata(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.updateSaveProductCPQ("SaveToQuote");
								
			AddProduct.returnC4CFromCPQ();
			AddProduct.editpsEngagements(testDataFile, dataSheet, scriptNo, dataSetNo);
			String Quote_ID=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "Quote_ID");
			AddProduct.navigateQuotesFromHomepage(testDataFile, dataSheet, scriptNo, dataSetNo, Quote_ID);

			AddProduct.editEthernetProductConfiguration(); 
			AddProduct.verifyQuoteStage("Portfolio Pricing Review");
			
			String NRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "NRC");
			String MRC=DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo, "MRC");
			
			Approvals.SwitchCPQUser("AD_User", Quote_ID);
			AddProduct.Engagementportfoliopricing(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.clickProductConfigurationBtn(ProductName);
			AddProduct.UpdateBasePriceException(NRC, MRC);			
			
			verifyExists(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem);
			String quoteStage = findWebElement(CPQQuoteCreationObj.AdditionalProductDataPage.quoteStageElem).getAttribute("value");
			System.out.println("quoteStage: "+quoteStage);
			if (quoteStage.equalsIgnoreCase("SE Required")) 
			{
				AddProduct.verifyQuoteStage("SE Required");
				AddProduct.C4CSEConfiguration(testDataFile, dataSheet, scriptNo, dataSetNo);
				            
				//Login with CPQ SE user
				close();
				openBrowser(g.getBrowser());		
				openurl(Configuration.CPQ_URL);
				CPQLogin.CPQLogin(Configuration.SEUser_Username, Configuration.SEUser_Password);
				CPQLogin.SelectQuote(testDataFile, dataSheet, scriptNo, dataSetNo);
				AddProduct.SEEngagement(testDataFile, dataSheet, scriptNo, dataSetNo);
				Approvals.SwitchCPQUser("Sales_User", Quote_ID);
			}
			
			Approvals.SwitchCPQUser("Sales_User", Quote_ID);			

			AddProduct.verifyQuoteStage("Priced");
			AddProduct.SubmitForApproval();
			AddProduct.verifyQuoteStage("Approved");
			AddProduct.EnterLegalNTechContactDetails(testDataFile, dataSheet, scriptNo, dataSetNo);			
			
			String BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"BCN");
			Submit.addBillingInformation(ProductName, BCN, "BCN NRC");
			
			//String CPE_Site_BCN = DataMiner.fngetcolvalue(testDataFile, dataSheet, scriptNo, dataSetNo,"CPE_Site_BCN");
			//Submit.addBillingInformation(ProductName, CPE_Site_BCN, "BCN");
			Submit.SendProposalToCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AcceptOnBehalfOfCustomer(testDataFile, dataSheet, scriptNo, dataSetNo);
			Submit.AddContactInformation(testDataFile, dataSheet, scriptNo, dataSetNo);
			AddProduct.saveCPQ("Main");
			Submit.SubmitOrder();
			AddProduct.verifyQuoteStage("Ordered");
			Submit.captureServiceOrder(testDataFile, dataSheet, scriptNo, dataSetNo);

		}catch(Exception e)
		{
			Report.LogInfo("Exception", "Exception in "+ ScenarioName + ": "+e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR,  "Exception in "+ ScenarioName + ": "+e.getMessage());
		} 
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
		ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
        record.stopVideoRecording();
        WEB_DRIVER_THREAD_LOCAL.get().close();
		
	}	

}
