package testHarness.ncFunctions;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.IpVpnOrderObj;
import pageObjects.ncObjects.NcOrderObj;
//import pageObjects.siebelObjects.SiebelAccountsObj;
import testHarness.commonFunctions.ReusableFunctions;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;
	

public class IpVpnOrder extends SeleniumUtils {
	
	public static ThreadLocal<String> IpvpnNCSiteProductOrdernumber = new ThreadLocal<>();
	public static ThreadLocal<String> IpvpnNCSiteProductOrderURL = new ThreadLocal<>();
	public static ThreadLocal<String> IpvpnNCEndpointProductOrderURL = new ThreadLocal<>();
	public static ThreadLocal<String> IpvpnNCServiceProductOrderURL = new ThreadLocal<>();
	public static ThreadLocal<String> IpvpnNCSiteidnumber = new ThreadLocal<>();
	public static ThreadLocal<String> IpvpnNCEndidnumber = new ThreadLocal<>();
	
	ReusableFunctions Reusable = new ReusableFunctions();
	
	public void IpvpnCreateAndProcess(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Order_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
		String Netflow_Enabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Netflow Enabled");
		String SNMP_Enabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Feature");
		String COS = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Class of Service");
		String LAN_Interf_IPv4AddFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN Interf IPv4 Add Feature");
		String LANRoutingProtocol = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN Routing Protocol");
		String WANRoutingprotocol = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"WAN Routing protocol");
		String NATFeature = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"NAT Feature");
		String IPRange = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IP Range");
		String DHCPrelayenabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Feature");
		String CoSMarking = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CoS Marking");
		String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
		//String Order_URL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Order_URL");
		
		//getUrl(Order_URL);
						
		if(Order_Type.equalsIgnoreCase("New VPN"))
		{	
			CreatServiceProductOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			AddServiceProduct();
			AddServiceProductdetails(testDataFile, sheetName, scriptNo, dataSetNo);
			AddsubVPNFeature();
			UpdateSubVPNProductComponent(testDataFile, sheetName, scriptNo, dataSetNo);
			if(Netflow_Enabled.equalsIgnoreCase("Yes"))
			{
				AddNetflowFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				NetflowUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			if(SNMP_Enabled.equalsIgnoreCase("Yes"))
			{
				AddSnmpFeature(testDataFile, sheetName, scriptNo, dataSetNo);
				SnmpV3Update(testDataFile, sheetName, scriptNo, dataSetNo);
				SnmpIPv4DevicesUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				SnmpReadonlyAccessUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			DecomposeServiceOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessServiceOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteServiceOrder(testDataFile, sheetName, scriptNo, dataSetNo);	
		}
		if (Order_Type.contains("New VPN") || Order_Type.contains("Spoke site") || Order_Type.contains("Existing VPN"))
		{
			CreatSiteProductOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			AddSiteProduct();
			AddSiteProductDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			AddDiversityCR(testDataFile, sheetName, scriptNo, dataSetNo);
			DiversityCRUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateEthernetProductVLComponent(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeSiteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			EthernetProductDetails(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeEthernetOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			EndSiteDetailsUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessSiteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		if (Order_Type.contains("New VPN") || Order_Type.contains("Spoke site") || Order_Type.contains("Existing VPN"))
		{
			CreateEndpointProductOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			AddVpnEndpointProduct();
			EndpointProductDetailsUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			AddLanVlanComponent(testDataFile, sheetName, scriptNo, dataSetNo);
			LanVlanCompUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			if (DHCPrelayenabled.equalsIgnoreCase("Yes"))
			{
				AddDhcpRelayComponent(testDataFile, sheetName, scriptNo, dataSetNo);
				DhcpRelayUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				AddIpv4DhcpRelay(testDataFile, sheetName, scriptNo, dataSetNo);
				Ipv4DhcpRelayUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			if (COS.equalsIgnoreCase("Yes"))
			{
				AddClassOfServiceComponent(testDataFile, sheetName, scriptNo, dataSetNo);
				ClassOfServiceUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				if (CoSMarking.contains("Customer Defined"))
				{
					AddIPv4AddressBasedMarkingComponent(testDataFile, sheetName, scriptNo, dataSetNo);
					IPv4AddressBasedMarkingUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				}
				else
				{
					AddDscpMarkingComponent(testDataFile, sheetName, scriptNo, dataSetNo);
					DscpMarkingUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				}
			}
			if (IPRange.contains("Customer"))
			{
				WanAddressingUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}

			if (NATFeature.equalsIgnoreCase("Yes"))
			{
				AddStaticNatIpv4RuleComponent(testDataFile, sheetName, scriptNo, dataSetNo);
				StaticNatIpv4RuleUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}

			if (WANRoutingprotocol.contains("BGP"))
			{
				if (RouterType.contains("Unmanaged"))
				{
					AddWanBgpComponent(testDataFile, sheetName, scriptNo, dataSetNo); 
					WanBgpUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
				}
			}
			VpnEndpointInformationUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			EndpointCircuitVLUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeEndpointOrder(testDataFile, sheetName, scriptNo, dataSetNo);

			if (LAN_Interf_IPv4AddFeature.contains("Yes"))
			{
				AddLanInterfaceIpv4Addressing(testDataFile, sheetName, scriptNo, dataSetNo);
				LanInterfaceIpv4AddressingUpdate(testDataFile, sheetName, scriptNo, dataSetNo); 
			}
			//condition required - yes or no
			if (LANRoutingProtocol.contains("BGP"))
			{
				AddBgpComponent(testDataFile, sheetName, scriptNo, dataSetNo);
				BgpComponentUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}
			else if (LANRoutingProtocol.contains("Static"))
			{
				AddStaticIpv4Route(testDataFile, sheetName, scriptNo, dataSetNo);
				StaticIpv4RouteUpdate(testDataFile, sheetName, scriptNo, dataSetNo);
			}					
			ProcessEndpointOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteEndpointOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteSiteOrder(testDataFile, sheetName, scriptNo, dataSetNo); 
		}
	}	
	
	public void ExistingSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
		click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
		
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
		click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name in filter");
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","FilterSelectName");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,"37464","Step: Order number for Processing provided");
				
		verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
		click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
						
		//As discussed with gokul this method not need to migrate now so stopped it.
		/*
		IpvpnNCSiteProductOrdernumber.set("Order #0000037464");
		
		Log.info(IpvpnNCSiteProductOrdernumber.get());
		String[] arrOfStr = IpvpnNCSiteProductOrdernumber.get().split("#", 0);
		Log.info(arrOfStr[1]);
		Clickon(getwebelement("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Composite Order");
		IpvpnNCSiteProductOrderURL.set(Getattribute(getwebelement(xml.getlocator("//locators/LinkforOrder")),"href"));
		Log.info(IpvpnNCSiteProductOrderURL.get());
		Clickon(getwebelement(xml.getlocator("//locators/UnderlyningIpvpnSiteOrder")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open the UnderLying Order");
		Clickon(getwebelement(xml.getlocator("//locators/GeneralInformation/GeneralInformationTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
		//Thread.sleep(3000);
		//Clickon(getwebelement(xml.getlocator("//locators/IpAddressing/IpAddressingLink")));
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Ip Addressing Link");
		  */
	}
	
	public void CreatSiteProductOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Type");
		
		if (OrderType.toString().contains("Existing VPN")) 
		{
		
			String IpvpnSiebelSiteOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_SiteOrder_No");
			String IpvpnSiebelServiceOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
			String IpvpnSiebelServiceNetworkreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Network_Ref");
			String IpvpnNCServiceidnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_VPN_NCS_ID");
			String IpvpnSiteNCSidnumberPrimarycomp2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prim_Comp2_NC_ID");
			 					
		}

		verifyExists(IpVpnOrderObj.IpVpnOrder.NewCompositeOrder,"New Composite Order");
		click(IpVpnOrderObj.IpVpnOrder.NewCompositeOrder,"New Composite Order");
		
		String IpvpnNCSiteProductOrdernumber=getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_No", IpvpnNCSiteProductOrdernumber);
		Reusable.waitForAjax();
		
		String[] arrOfStr = IpvpnNCSiteProductOrdernumber.split("#", 0); 
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.OrderDescription,"IPVPN Site Product Order Created using Automation script"," Step: Order Description Updated");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Click on Update button");
		click(IpVpnOrderObj.IpVpnOrder.Update,"Click on Update button");
			
		String IpvpnNCSiteProductOrderURL = getAttributeFrom(IpVpnOrderObj.IpVpnOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Order_URL",IpvpnNCSiteProductOrderURL);
		System.out.println("IpvpnNCSiteProductOrderURL: "+IpvpnNCSiteProductOrderURL);
	}
	
	public void CreatServiceProductOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{	
		//unmanaged IPVPN - FM
		/*IpvpnSiebelServiceOrdernumber.set("871663726/191009-005");
		IpvpnSiebelServiceNetworkreference.set("IPC-10219");
		IpvpnSiebelSiteOrdernumber.set("871666579/190915-002");*/
		
		click(IpVpnOrderObj.IpVpnOrder.NewCompositeOrder,"New Composite Order");
		
		String IpvpnNCServiceProductOrdernumber=getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber);	
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No", IpvpnNCServiceProductOrdernumber);
		Reusable.waitForAjax();
		
		String[] arrOfStr = IpvpnNCServiceProductOrdernumber.split("#", 0); 
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.OrderDescription,"IP VPN Service Product Order Created using Automation script");
		
		click(IpVpnOrderObj.IpVpnOrder.Update,"Update button");
	
		String IpvpnNCServiceProductOrderURL = getAttributeFrom(IpVpnOrderObj.IpVpnOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL",IpvpnNCServiceProductOrderURL);
	}
	
	public void CreateEndpointProductOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		//un managed scenario
		/*IpvpnSiebelServiceOrdernumber.set("871663726/191009-0045");
		IpvpnSiebelServiceNetworkreference.set("IPC-102189");
		IpvpnSiebelSiteOrdernumber.set("871666579/190915-0002");
		IpvpnSiteNCSidnumber.set("9155778285113940861"); //site nc service id
		OVCCircuitRef.set("CEOS00000513770#223344");*/

		verifyExists(IpVpnOrderObj.IpVpnOrder.NewCompositeOrder,"New Composite Order");
		click(IpVpnOrderObj.IpVpnOrder.NewCompositeOrder,"New Composite Order");
				
		String IpvpnNCEndpointProductOrdernumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_No",IpvpnNCEndpointProductOrdernumber);
		
		String[] arrOfStr = IpvpnNCEndpointProductOrdernumber.split("#", 0); 
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.OrderDescription,"IPVPN Endpoint Product Order Created using Automation script","Order Description");
		
		click(IpVpnOrderObj.IpVpnOrder.Update,"Update button");
		
		String IpvpnNCEndpointProductOrderURL = getAttributeFrom(IpVpnOrderObj.IpVpnOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL",IpvpnNCEndpointProductOrderURL);

	}
	
	public void AddBgpComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");		
		getUrl(IpvpnNCEndpointProductOrderURL);
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyingLanVlanOrder,"Underlying LanVlan Order");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyingLanVlanOrder,"Underlying LanVlan Order");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
		click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Click on Add Feature Link");
		click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Click on Add Feature Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"BGP Component","Enter Product name");
		Reusable.waitForSiebelLoader();

		
		// Store the current window handle
		String winHandleBefore = webDriver.getWindowHandle();
		verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
		click(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
		Reusable.waitForSiebelLoader();
				
		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		System.out.println("Window handel"+curent);
		webDriver.switchTo().window(curent);
		// Perform the actions on new window
		System.out.println("Title is"+webDriver.getTitle());
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		
		click("@xpath=//span[text()='BGP Component']/parent::*/parent::*/parent::*/td//input", "BGP Component");
				
		Reusable.waitForAjax();
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
		verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		
		// switch back to parent window
		webDriver.switchTo().window(winHandleBefore);
		Reusable.waitForAjax();
	}
	
	public void AddClassOfServiceComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		getUrl(IpvpnNCEndpointProductOrderURL);
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn EndpointOrder");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn EndpointOrder");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Class of Service","Enter Product name");
		Reusable.waitForSiebelLoader();
		
		String winHandleBefore = webDriver.getWindowHandle();
		verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		Reusable.waitForSiebelLoader();
				
		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		System.out.println("Window handel"+curent);
		webDriver.switchTo().window(curent);
		// Perform the actions on new window
		System.out.println("Title is"+webDriver.getTitle());
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		click("@xpath=//span[text()='Class of Service']/parent::*/parent::*/parent::*/td//input", "Class of Service");
		Reusable.waitForAjax();
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		// switch back to parent window
		webDriver.switchTo().window(winHandleBefore);
		//		Featurename.set(Inputdata[33].toString());
		//		Log.info(Featurename.get());
		Reusable.waitForAjax();
	}
	
	public void AddDhcpRelayComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		getUrl(IpvpnNCEndpointProductOrderURL);
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn Endpoint Order");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn Endpoint Order");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"DHCP Relay","Enter Product name");
		Reusable.waitForSiebelLoader();
			
		String winHandleBefore = webDriver.getWindowHandle();
		verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		Reusable.waitForSiebelLoader();
		
		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		System.out.println("Window handel"+curent);
		webDriver.switchTo().window(curent);
		// Perform the actions on new window
		System.out.println("Title is"+webDriver.getTitle());
		
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		click("@xpath=//span[text()='DHCP Relay']/parent::*/parent::*/parent::*/td//input", "DHCP Relay");
		Reusable.waitForAjax();
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		// switch back to parent window
		webDriver.switchTo().window(winHandleBefore);
		//		Featurename.set(Inputdata[33].toString());
		//		Log.info(Featurename.get());
		Reusable.waitForAjax();					
			
	}
	
	public void AddDiversityCR(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		
		String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
		String IpvpnNCSiteProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_Order_URL");
		if (OrderType.toString().contains("Existing VPN")) 
		{
			//Geturl(IpvpnNCSiteProductOrderURL.get());
			getUrl(IpvpnNCSiteProductOrderURL);
			verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"Underlyning Ipvpn Site Order");
			click(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"Underlyning Ipvpn Site Order");
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
			click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
			click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
			
			sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Diversity & Circuit Routing","Enter Product name");
			Reusable.waitForSiebelLoader();
				
			String winHandleBefore = webDriver.getWindowHandle();
			verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
			click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
			Reusable.waitForSiebelLoader();
			
			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			System.out.println("Window handel"+curent);
			webDriver.switchTo().window(curent);
			// Perform the actions on new window
			System.out.println("Title is"+webDriver.getTitle());
			
			webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
			click("@xpath=//span[text()='Diversity & Circuit Routing']/parent::*/parent::*/parent::*/td//input","Diversity & Circuit Routing");
			Reusable.waitForAjax();
			webDriver.switchTo().window(curent);
			webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
			click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
			// switch back to parent window
			webDriver.switchTo().window(winHandleBefore);
			//		Featurename.set(Inputdata[33].toString());
			//		Log.info(Featurename.get());
			Reusable.waitForAjax();
					
		}
	}

	public void AddDscpMarkingComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		getUrl(IpvpnNCEndpointProductOrderURL);
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Ipvpn Site Order");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Ipvpn Site Order");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"DSCP Marking","Enter Product name");
		Reusable.waitForSiebelLoader();
			
		String winHandleBefore = webDriver.getWindowHandle();
		verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		Reusable.waitForSiebelLoader();
		
		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		System.out.println("Window handel"+curent);
		webDriver.switchTo().window(curent);
		// Perform the actions on new window
		System.out.println("Title is"+webDriver.getTitle());
		
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		click("@xpath=//span[text()='DSCP Marking']/parent::*/parent::*/parent::*/td//input", "DSCP Marking");
		Reusable.waitForAjax();
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		
		// switch back to parent window
		webDriver.switchTo().window(winHandleBefore);
		//		Featurename.set(Inputdata[33].toString());
		//		Log.info(Featurename.get());
		Reusable.waitForAjax();	
			
			
	}
	
	public void AddIPv4AddressBasedMarkingComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String OVCNAME = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OVC NAME");
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		
		for(int i=0;i<Integer.parseInt(OVCNAME.toString());i++)
		{
			
			getUrl(IpvpnNCEndpointProductOrderURL);
			//Geturl(IpvpnNCEndpointProductOrderURL.get());
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn EndpointOrder");
			click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn EndpointOrder");
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
			click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
			click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
			
			sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"IPv4 Address Based Marking","Enter Product name");
			Reusable.waitForSiebelLoader();
				
			String winHandleBefore = webDriver.getWindowHandle();
			verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
			click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
			Reusable.waitForSiebelLoader();
			
			Set<String> handles = webDriver.getWindowHandles();
			Iterator<String> iterator = handles.iterator();
			iterator.next();
			String curent=iterator.next();
			System.out.println("Window handel"+curent);
			webDriver.switchTo().window(curent);
			// Perform the actions on new window
			System.out.println("Title is"+webDriver.getTitle());
			
			webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
			click("@xpath=//span[text()='IPv4 Address Based Marking']/parent::*/parent::*/parent::*/td//input", "IPv4 Address Based Marking");
			Reusable.waitForAjax();
			webDriver.switchTo().window(curent);
			webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
			click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
			
			// switch back to parent window
			webDriver.switchTo().window(winHandleBefore);
			//		Featurename.set(Inputdata[33].toString());
			//		Log.info(Featurename.get());
			Reusable.waitForAjax();	
			
		}
	}
		
	public void AddIpv4DhcpRelay(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		getUrl(IpvpnNCEndpointProductOrderURL);
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn EndpointOrder");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn EndpointOrder");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"IPv4 DHCP Relay","Enter Product name");
		Reusable.waitForSiebelLoader();
			
		String winHandleBefore = webDriver.getWindowHandle();
		verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		Reusable.waitForSiebelLoader();
		
		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		System.out.println("Window handel"+curent);
		webDriver.switchTo().window(curent);
		// Perform the actions on new window
		System.out.println("Title is"+webDriver.getTitle());
		
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		click("@xpath=//span[text()='IPv4 DHCP Relay']/parent::*/parent::*/parent::*/td//input", "IPv4 DHCP Relay");
		Reusable.waitForAjax();
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		
		// switch back to parent window
		webDriver.switchTo().window(winHandleBefore);
		//		Featurename.set(Inputdata[33].toString());
		//		Log.info(Featurename.get());
		Reusable.waitForAjax();
								
	}
	
	public void AddLanInterfaceIpv4Addressing(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String CountofLANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Count of LAN VLAN");
		String CountofLANinterfaceipv4address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Count of LAN interface ipv4 address");
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		
		for(int i=0;i<Integer.parseInt(CountofLANVLAN.toString());i++)
		{
			getUrl(IpvpnNCEndpointProductOrderURL);
			Reusable.waitForSiebelLoader();				
			
			click("@xpath=(//span[contains(text(),'LAN VLAN Product Order')]/parent::a)["+(i+1)+"]", "LAN VLAN Product Order");
						
			verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
						
			int j=0;
			while (j<Integer.parseInt(CountofLANinterfaceipv4address.toString()))
			{

				verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				Reusable.waitForAjax();
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				Reusable.waitForAjax();
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"LAN Interface IPV4 Addressing","Enter Product name");
				Reusable.waitForSiebelLoader();
					
				String winHandleBefore = webDriver.getWindowHandle();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
				click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
				Reusable.waitForSiebelLoader();
				
				Set<String> handles = webDriver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				iterator.next();
				String curent=iterator.next();
				System.out.println("Window handel"+curent);
				webDriver.switchTo().window(curent);
				// Perform the actions on new window
				System.out.println("Title is"+webDriver.getTitle());
				
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
				click("@xpath=//span[text()='LAN Interface IPV4 Addressing']/parent::*/parent::*/parent::*/td//input", "LAN Interface IPV4 Addressing");
				Reusable.waitForSiebelLoader();
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				
				// switch back to parent window
				webDriver.switchTo().window(winHandleBefore);
				Reusable.waitForSiebelLoader();
				Reusable.waitForpageloadmask();
				j++;
			}
		}
	}
	
	public void AddLanVlanComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String LANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN VLAN");
		String CountofLANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Count of LAN VLAN");
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		
		if(!LANVLAN.contains("No"))
		{
			for(int i=0;i<Integer.parseInt(CountofLANVLAN.toString());i++)
			{
				getUrl(IpvpnNCEndpointProductOrderURL);
				Reusable.waitForSiebelLoader();	
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Open the UnderLying Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Open the UnderLying Order");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"LAN VLAN Component","Enter Product name");
				Reusable.waitForSiebelLoader();
					
				String winHandleBefore = webDriver.getWindowHandle();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
				click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				
				Set<String> handles = webDriver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				iterator.next();
				String curent=iterator.next();
				System.out.println("Window handel"+curent);
				webDriver.switchTo().window(curent);
				// Perform the actions on new window
				System.out.println("Title is"+webDriver.getTitle());
				
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
				click("@xpath=//span[text()='LAN VLAN Component']/parent::*/parent::*/parent::*/td//input", "LAN VLAN Component");
				Reusable.waitForAjax();
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				
				// switch back to parent window
				webDriver.switchTo().window(winHandleBefore);
				//		Featurename.set(Inputdata[33].toString());
				//		Log.info(Featurename.get());
				Reusable.waitForAjax();					
			}
		}
	}
	
	public void AddNetflowFeature(String testDataFile,String  sheetName,String  scriptNo,String  dataSetNo) throws Exception
	{
		//Geturl(IpvpnNCServiceProductOrderURL.get());
		String IpvpnNCServiceProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		getUrl(IpvpnNCServiceProductOrderURL);
		Reusable.waitForSiebelLoader();	
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"Open the UnderLying Order");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"Open the UnderLying Order");
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Netflow","Enter Product name");
		Reusable.waitForAjax();
			
		String winHandleBefore = webDriver.getWindowHandle();
		verifyExists(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		click(IpVpnOrderObj.IpVpnOrder.Browse,"Click on Browse");
		Reusable.waitForAjax();
		
		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		System.out.println("Window handel"+curent);
		webDriver.switchTo().window(curent);
		// Perform the actions on new window
		System.out.println("Title is"+webDriver.getTitle());
		
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		click("@xpath=//span[text()='Netflow']/parent::*/parent::*/parent::*/td//input", "Netflow feature selection");
		Reusable.waitForAjax();
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		
		// switch back to parent window
		webDriver.switchTo().window(winHandleBefore);
		//		Featurename.set(Inputdata[33].toString());
		//		Log.info(Featurename.get());
		Reusable.waitForAjax();					
	}
	
	public void AddServiceProduct() throws Exception
	{
		verifyExists(IpVpnOrderObj.IpVpnOrder.OrderTab,"Order Tab");
		click(IpVpnOrderObj.IpVpnOrder.OrderTab,"Order Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddonOrderTab,"Add Product Link");
		click(IpVpnOrderObj.IpVpnOrder.AddonOrderTab,"Add Product Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.IpvpnserviceCheckBox,"Select IPVPN Service Product Option");
		click(IpVpnOrderObj.IpVpnOrder.IpvpnserviceCheckBox,"Select IPVPN Service Product Option");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.Addbutton,"Add button");
		click(IpVpnOrderObj.IpVpnOrder.Addbutton,"Add button");
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();
								
	}
	
	public void AddServiceProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
		String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");
		String SiebelServOrderNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
		String SiebelNetworkRef = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Network_Ref");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"Open the UnderLying Order");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"Open the UnderLying Order");
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
		click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
		
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderSystemName,"SPARK","Order System Name");
		
		String[] serviceorderNo=SiebelServOrderNo.split("/");
				
		sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSysSerId,serviceorderNo[0]); 
		Reusable.waitForSiebelLoader();	
		sendKeys(IpVpnOrderObj.IpVpnOrder.SiteOrderNumber,serviceorderNo[1]); // added new 
		sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSystemOrderNo,serviceorderNo[0]);
		sendKeys(IpVpnOrderObj.IpVpnOrder.ServiceOrderRef,serviceorderNo[0]);
		sendKeys(IpVpnOrderObj.IpVpnOrder.Ipvpnnetworkreference,SiebelNetworkRef);
		
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderManageGrp,"Service Delivery","Order Manage Grp");
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Servicetype,ServiceType,"Service Type");
		if(Topology.toString().contains("Any to Any"))
		{
			selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,"Any to Any","Topology");
		}
		else
		{
			selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,Topology,"Topology");	
		}

		click(IpVpnOrderObj.IpVpnOrder.Update, "Required Details are Filled and Updated" );
		Reusable.waitForSiebelLoader();	
		
		String IpvpnNCServiceidnumber = getTextFrom(IpVpnOrderObj.IpVpnOrder.NCServiceid);
				
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Existing VPN NCS ID", IpvpnNCServiceidnumber);
		System.out.println("First composite NC service number: "+IpvpnNCServiceidnumber);
		
	}
	
	public void AddSiteProduct() throws Exception
	{
	
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.OrderTab,"Order Tab");
		click(IpVpnOrderObj.IpVpnOrder.OrderTab,"Order Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddonOrderTab,"AddonOrderTab");
		click(IpVpnOrderObj.IpVpnOrder.AddonOrderTab,"AddonOrderTab");
				
		verifyExists(IpVpnOrderObj.IpVpnOrder.IpvpnSiteCheckBox,"IPVPN Site Order Option");
		click(IpVpnOrderObj.IpVpnOrder.IpvpnSiteCheckBox,"IPVPN Site Order Option");
				
		verifyExists(IpVpnOrderObj.IpVpnOrder.Addbutton,"Add button");
		click(IpVpnOrderObj.IpVpnOrder.Addbutton,"Add button");
				
		verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"UnderLying IPVPN Site Order");
		click(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"UnderLying IPVPN Site Order");
				
		verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
		click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				
	}

	public void AddSiteProductDetailsSecondary(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String PrimaryBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
		String Layer3Resilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
		String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
		String SiteType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site Type");
		String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");
		String SNMPEnabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Feature");
		String HubMeshPrimary = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hub/Mesh Primary");
		String RouterTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Technology");
		String BackupBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Backup Bandwidth");
		String NetflowEnabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Netflow Enabled");
		String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
		String ExistingVPNNCSID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing VPN NCS ID");
		String SiebelSiteOrderNo = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing Siebel SiteOrder No");
		String SiebelNetworkRef = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Network_Ref");
		String IpvpnSiebelServiceOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
		String IpvpnNCServiceidnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing VPN NCS ID");
		
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderSystemName,"SPARK","OrderSystemName");
		String[] SiteOrderNo=SiebelSiteOrderNo.split("/");
		
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Bandwidth,PrimaryBandwidth,"Bandwidth");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Layer3Resilience,Layer3Resilience,"Layer3Resilience");
		sendKeys(IpVpnOrderObj.IpVpnOrder.SiteOrderNumber,SiteOrderNo[1]);
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.RouterType,RouterType,"RouterType");
		sendKeys(IpVpnOrderObj.IpVpnOrder.Ipvpnnetworkreference,SiebelNetworkRef);
		sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSystemOrderNo,SiteOrderNo[0]);
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.SiteType,SiteType,"SiteType");
		if(Topology.toString().contains("Any to Any"))
		{
			selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,"Any to Any","Topology");
		}
		else
		{
			selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,Topology,"Topology");	
		}
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.SnmpEnabledFlag,SNMPEnabled,"SnmpEnabledFlag");

		//To be updated based on scenario
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.HubMeshPrimaryFlag,HubMeshPrimary,"HubMeshPrimaryFlag");

		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderSystemName,"SPARK","OrderSystemName");
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.RouterTechnology,RouterTechnology,"RouterTechnology");

		sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSysSerId,SiteOrderNo[0]);
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderManageGrp,"Service Delivery","OrderManageGrp");
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.BackupBandwidth,BackupBandwidth,"BackupBandwidth");

		String[] serviceorderNo=IpvpnSiebelServiceOrdernumber.split("/");
		sendKeys(IpVpnOrderObj.IpVpnOrder.ServiceOrderRef,serviceorderNo[0]);

		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.NetFlowFlag,NetflowEnabled,"NetFlowFlag");
		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AdvFirewallFlag,"No","AdvFirewallFlag");
		if (OrderType.toString().contains("Existing VPN"))
		{
			sendKeys(IpVpnOrderObj.IpVpnOrder.RelatedIpVpnService,ExistingVPNNCSID.toString());
		}
		else 
		{
			sendKeys(IpVpnOrderObj.IpVpnOrder.RelatedIpVpnService,IpvpnNCServiceidnumber);
		}
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
		click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
		Reusable.waitForSiebelLoader();
		
		String IpvpnNCSiteidnumber = (getTextFrom(IpVpnOrderObj.IpVpnOrder.NCServiceid));
		Reusable.waitForSiebelLoader();
	}
	
	public void AddSnmpFeature(String testDataFile,String  sheetName,String  scriptNo,String  dataSetNo) throws Exception
	{
		String IpvpnNCServiceProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
		getUrl(IpvpnNCServiceProductOrderURL);
		
		if(verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder))
		{
			verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"IPVPN Service Order");
			click(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"IPVPN Service Order");
		}
				
		verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
		
		verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
		
		sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"SNMP v3","Enter Product name");
		Reusable.waitForSiebelLoader();
		String winHandleBefore = webDriver.getWindowHandle();
		click(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
		Reusable.waitForAjax();
	
		Set<String> handles = webDriver.getWindowHandles();
		Iterator<String> iterator = handles.iterator();
		iterator.next();
		String curent=iterator.next();
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
		
		click("@xpath=//span[text()='SNMP IPv4 Devices']/parent::*/parent::*/parent::*/td//input", "SNMP IPv4 Devices");
		Reusable.waitForAjax();
		click("@xpath=//span[text()='SNMP v3']/parent::*/parent::*/parent::*/td//input", "SNMP v3");
		Reusable.waitForAjax();
		click("@xpath=//span[text()='SNMP v3 Read Only Access']/parent::*/parent::*/parent::*/td//input", "SNMP v3 Read Only Access");
		Reusable.waitForAjax();
				
		webDriver.switchTo().window(curent);
		webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
		verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
		webDriver.switchTo().window(winHandleBefore);
		Reusable.waitForSiebelLoader();	
	}
	
	public void AddStaticIpv4Route(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
	{
		String CountofLANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Count of LAN VLAN");
		String AdditionalLANRoutingstaticipv4count = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Additional LAN Routing static ipv4 -count");
		String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
		for(int i=0;i<Integer.parseInt(CountofLANVLAN.toString());i++)
		{
			//Geturl(IpvpnNCEndpointProductOrderURL.get());
			getUrl(IpvpnNCEndpointProductOrderURL);
			Reusable.waitForSiebelLoader();	
			click("@xpath=(//span[contains(text(),'LAN VLAN Product Order')]/parent::a)["+(i+1)+"]", "LAN VLAN Product Order");
			
			verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
						
			int j=0;
			while (j<Integer.parseInt(AdditionalLANRoutingstaticipv4count.toString()))
			{
				verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
							
				sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Static IPv4 Route","Enter Product name");
				
				Reusable.waitForSiebelLoader();
				String winHandleBefore = webDriver.getWindowHandle();
				click(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
				Reusable.waitForSiebelLoader();
					
				Set<String> handles = webDriver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				iterator.next();
				String curent=iterator.next();
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
				
				click("@xpath=//span[text()='Static IPv4 Route']/parent::*/parent::*/parent::*/td//input", "Static IPv4 Route");
				Reusable.waitForAjax();
										
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
				verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				webDriver.switchTo().window(winHandleBefore);
				Reusable.waitForAjax();	
					
				j++;
			}
		}
		}
			
			public void AddStaticNatComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
				
				//Geturl(IpvpnNCEndpointProductOrderURL.get());
				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn End point Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn End point Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Static NAT","Enter Product name");
				Reusable.waitForSiebelLoader();
				
				String winHandleBefore = webDriver.getWindowHandle();
				click(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
				Reusable.waitForSiebelLoader();
			
				Set<String> handles = webDriver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				iterator.next();
				String curent=iterator.next();
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("//frame[@onload='onLoadObjectFrame()']"));
				
				
				ClickonElementByString("//span[text()='Static NAT']/parent::*/parent::*/parent::*/td//input", 10);
				Reusable.waitForSiebelLoader();
						
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("//frame[contains(@onload,'if(!document.selLoaded')]"));
				verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				webDriver.switchTo().window(winHandleBefore);
				Reusable.waitForSiebelLoader();	
										
	}
			
			public void AddSiteProductDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String IpvpnSiebelSiteOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing Siebel SiteOrder No");
				String PrimaryBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
				String Layer3Resilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
				String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
				String IpvpnSiebelServiceNetworkreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Network_Ref");
				String SiteType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site Type");
				String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");
				String SNMPEnabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Feature");
				String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
				String HubMeshPrimary = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hub/Mesh Primary");
				String RouterTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Technology");
				String BackupBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Backup Bandwidth");
				String IpvpnSiebelServiceOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
				String NetflowEnabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Netflow Enabled");
				String ExistingVPNNCSID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing VPN NCS ID");
				String IpvpnNCServiceidnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing VPN NCS ID");
				
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderSystemName,"SPARK","OrderSystem Name");
				String[] SiteOrderNo=IpvpnSiebelSiteOrdernumber.split("/");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Bandwidth,PrimaryBandwidth,"Bandwidth");
				Reusable.waitForSiebelLoader();	
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Layer3Resilience,Layer3Resilience,"Layer3Resilience");
				sendKeys(IpVpnOrderObj.IpVpnOrder.SiteOrderNumber,SiteOrderNo[1],"SiteOrderNumber");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.RouterType,RouterType,"RouterType");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Ipvpnnetworkreference,IpvpnSiebelServiceNetworkreference,"Ipvpnnetworkreference");
				sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSystemOrderNo,SiteOrderNo[0],"OrderSystemOrderNo");

				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.SiteType,SiteType,"SiteType");
				if(Topology.toString().contains("Any to Any"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,"Any to Any","Topology");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,Topology,"Topology");	
				}

				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.SnmpEnabledFlag,SNMPEnabled,"SnmpEnabledFlag");

				//To be updated based on scenario

				if (OrderType.toString().contains("Spoke"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.HubMeshPrimaryFlag,"No","HubMeshPrimaryFlag");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.HubMeshPrimaryFlag,HubMeshPrimary,"HubMeshPrimaryFlag");
				}

				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderSystemName,"SPARK","Order System Name");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.RouterTechnology,RouterTechnology,"RouterTechnology");

				sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSysSerId,SiteOrderNo[0]);
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderManageGrp,"Service Delivery","OrderManageGrp");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.BackupBandwidth,BackupBandwidth,"BackupBandwidth");

				String[] serviceorderNo=IpvpnSiebelServiceOrdernumber.split("/");
				sendKeys(IpVpnOrderObj.IpVpnOrder.ServiceOrderRef,serviceorderNo[0]);

				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.NetFlowFlag,NetflowEnabled,"NetFlowFlag");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AdvFirewallFlag,"No","AdvFirewallFlag");

				if(OrderType.toString().contains("Spoke")) //index 1 should fill nc service id for spoke-site odrer
				{
					sendKeys(IpVpnOrderObj.IpVpnOrder.RelatedIpVpnService,ExistingVPNNCSID.toString(),"RelatedIpVpnService");
				}
				else
				{
					sendKeys(IpVpnOrderObj.IpVpnOrder.RelatedIpVpnService,IpvpnNCServiceidnumber,"RelatedIpVpnService");
				}

				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
				Reusable.waitForSiebelLoader();
				
				String IpvpnNCSiteidnumber=getTextFrom(IpVpnOrderObj.IpVpnOrder.NCServiceid);				
				DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prim Site NC ID",IpvpnNCSiteidnumber);
				Reusable.waitForSiebelLoader();
			}
			
			public void AddStaticNatIpv4RuleComponent(String testDataFile,String  sheetName,String  scriptNo,String  dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
				getUrl(IpvpnNCEndpointProductOrderURL);
				
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn End point Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn End point Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Static NAT IPv4 Rule","Enter Product name");
				Reusable.waitForSiebelLoader();
				// Store the current window handle
				String winHandleBefore = webDriver.getWindowHandle();
				click(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
				Reusable.waitForSiebelLoader();
			
				Set<String> handles = webDriver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				iterator.next();
				String curent=iterator.next();
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
				
				click("@xpath=//span[text()='Static NAT IPv4 Rule']/parent::*/parent::*/parent::*/td//input", "Static NAT IPv4 Rule");
				Reusable.waitForSiebelLoader();
						
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
				verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				webDriver.switchTo().window(winHandleBefore);
				Reusable.waitForSiebelLoader();	
				
			}

			//added for asper4.2
			public void AddsubVPNFeature() throws Exception
			{

				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Sub VPN","Enter Product name");
				Reusable.waitForSiebelLoader();
				// Store the current window handle
				String winHandleBefore = webDriver.getWindowHandle();
				click(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
				Reusable.waitForAjax();
						
				Set<String> handles = webDriver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				iterator.next();
				String curent=iterator.next();
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));				
				click("@xpath=//span[text()='Sub VPN']/parent::*/parent::*/parent::*/td//input", "Sub VPN");
				Reusable.waitForAjax();
						
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
				verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				webDriver.switchTo().window(winHandleBefore);
				Reusable.waitForSiebelLoader();	
										
			}
			
			public void AddVpnEndpointProduct() throws Exception
			{
				verifyExists(IpVpnOrderObj.IpVpnOrder.OrderTab,"Order Tab");
				click(IpVpnOrderObj.IpVpnOrder.OrderTab,"Order Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AddonOrderTab,"Add Product Link");
				click(IpVpnOrderObj.IpVpnOrder.AddonOrderTab,"Add Product Link");
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.IpVpnEndPointProdCheckBox,"Select IPVPN Endpoint Product Option");
				click(IpVpnOrderObj.IpVpnOrder.IpVpnEndPointProdCheckBox,"Select IPVPN Endpoint Product Option");
							
				verifyExists(IpVpnOrderObj.IpVpnOrder.Addbutton,"Add button");
				click(IpVpnOrderObj.IpVpnOrder.Addbutton,"Add button");
				Reusable.waitForSiebelLoader();	
			
			}
			
			public void AddWanBgpComponent(String testData,String  sheetName,String  scriptNo,String  datasetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testData, sheetName, scriptNo, datasetNo, "Endpoint_Order_URL");
				
				getUrl(IpvpnNCEndpointProductOrderURL);
								
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn End point Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning Vpn End point Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				click(IpVpnOrderObj.IpVpnOrder.AddFeaturelink,"Add Feature Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				click(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"Feature box");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.TypeofFeature,"WAN BGP","Enter Product name");
				Reusable.waitForSiebelLoader();
				// Store the current window handle
				String winHandleBefore = webDriver.getWindowHandle();
				click(IpVpnOrderObj.IpVpnOrder.Browse,"Browse");
				Reusable.waitForSiebelLoader();
			
				Set<String> handles = webDriver.getWindowHandles();
				Iterator<String> iterator = handles.iterator();
				iterator.next();
				String curent=iterator.next();
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[@onload='onLoadObjectFrame()']"));
				
				click("@xpath=//span[text()='WAN BGP']/parent::*/parent::*/parent::*/td//input", "WAN BGP");
				Reusable.waitForSiebelLoader();
						
				webDriver.switchTo().window(curent);
				webDriver.switchTo().frame(findWebElement("@xpath=//frame[contains(@onload,'if(!document.selLoaded')]"));
				verifyExists(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				click(IpVpnOrderObj.IpVpnOrder.SelectButton,"Select Button");
				webDriver.switchTo().window(winHandleBefore);
				Reusable.waitForSiebelLoader();	
			
			}

			public void BgpComponentUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String MD5Pwd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MD5 Pwd");
				String HoldTimer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hold Timer(s)");
				String KeepAlive = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Keep Alive(s)");
				String Ipv4PeerAddress = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Ipv4 Peer Address");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
	
				//Geturl(IpvpnNCEndpointProductOrderURL.get());
				getUrl(IpvpnNCEndpointProductOrderURL);
				
				if(verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyingLanVlanOrder))
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyingLanVlanOrder,"Underlyning LanVlan Order");
					click(IpVpnOrderObj.IpVpnOrder.UnderlyingLanVlanOrder,"Underlyning LanVlan Order");
				}				
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.BgpComponentLink,"BGP Component Link");
				click(IpVpnOrderObj.IpVpnOrder.BgpComponentLink,"BGP Component Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();	
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.Md5Pwd,MD5Pwd.toString());   
				sendKeys(IpVpnOrderObj.IpVpnOrder.HoldTimer,HoldTimer.toString());
				sendKeys(IpVpnOrderObj.IpVpnOrder.KeepAlive,KeepAlive.toString());
				sendKeys(IpVpnOrderObj.IpVpnOrder.PeerASNum,Ipv4PeerAddress.toString());

				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are filled and updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are filled and updated");
				Reusable.waitForSiebelLoader();	
			}
			
			public void ClassOfServiceUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String Management = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Management %");
				String Premium = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premium %");
				String Standard = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Standard %");
				String Business1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Business 1 %");
				String Business2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Business 2 %");
				String Business3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Business 3 %");
				String CoSMarking = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"CoS Marking");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");

				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.ClassofServiceLink,"BGP Component Link");
				click(IpVpnOrderObj.IpVpnOrder.ClassofServiceLink,"BGP Component Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();	
				
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.ManagementPerc,Management,"ManagementPerc");   
				sendKeys(IpVpnOrderObj.IpVpnOrder.PremiumPerc,Premium,"PremiumPerc");
				sendKeys(IpVpnOrderObj.IpVpnOrder.StandardPerc,Standard,"StandardPerc");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Business1Perc,Business1,"Business1Perc");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Business2Perc,Business2,"Business2Perc");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Business3Perc,Business3,"Business3Perc");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CoSMarking,CoSMarking,"CoSMarking");
				Reusable.waitForSiebelLoader();	
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are filled and updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are filled and updated");
				Reusable.waitForAjax();	
				
			}
			
			public void DecomposeEndpointOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String LANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN_VLAN");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				
				if(!LANVLAN.toString().contains("No"))
				{
					//Geturl(IpvpnNCEndpointProductOrderURL.get());
					getUrl(IpvpnNCEndpointProductOrderURL);
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
					click(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.VpnEndPointOrder,"Select IPVPN Endpoint Product Order");
					click(IpVpnOrderObj.IpVpnOrder.VpnEndPointOrder,"Select IPVPN Endpoint Product Order");
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
					click(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
					Reusable.waitForSiebelLoader();	
								
				}
			}
			
			public void DecomposeEthernetOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String LANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN_VLAN");
				String IpvpnNCSiteProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_URL");
			//	
				getUrl(IpvpnNCSiteProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
				click(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SubEthernetOrder,"Select Ethernet Connection Product Order");
				click(IpVpnOrderObj.IpVpnOrder.SubEthernetOrder,"Select Ethernet Connection Product Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
				click(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
				Reusable.waitForSiebelLoader();	
				
			}
			
			public void DecomposeServiceOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCServiceProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL");
				getUrl(IpvpnNCServiceProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
				click(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Serviceproductorder,"Select IPVPN Service Product Order");
				click(IpVpnOrderObj.IpVpnOrder.Serviceproductorder,"Select IPVPN Service Product Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
				click(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
				Reusable.waitForSiebelLoader();	
												
			}

			
			public void DecomposeSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCSiteProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_URL");
				getUrl(IpvpnNCSiteProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
				click(IpVpnOrderObj.IpVpnOrder.OrderTab,"Navgiate to Orders Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SiteProductOrder,"Select IPVPN Service Product Order");
				click(IpVpnOrderObj.IpVpnOrder.SiteProductOrder,"Select IPVPN Service Product Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
				click(IpVpnOrderObj.IpVpnOrder.Decompose,"Click on Decompose button");
				Reusable.waitForSiebelLoader();
				Reusable.waitForpageloadmask();
			}
			
			public void DhcpRelayUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String Format = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Format");
				String SiebelComponentID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Siebel Component ID");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				
				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.DhcpRelayLink,"DHCP Relay Link");
				click(IpVpnOrderObj.IpVpnOrder.DhcpRelayLink,"DHCP Relay Link");
					
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();	
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.DhcpFormat,Format,"DhcpFormat");
				sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentID.toString());
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
				Reusable.waitForSiebelLoader();	
				
			}
			
			public void DiversityCRUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
				String IpvpnSiteNCSidnumberPrimarycomp2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prim Site NC ID");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_URL");
				
				if (OrderType.toString().contains("Existing VPN")) 
				{
					getUrl(IpvpnNCEndpointProductOrderURL);
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"Underlyning IpvpnSite Order");
					click(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"Underlyning IpvpnSite Order");
					Reusable.waitForSiebelLoader();	
					verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
					click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.Diversitylink,"Diversity & Circuit Routing Link");
					click(IpVpnOrderObj.IpVpnOrder.Diversitylink,"Diversity & Circuit Routing Link");
					Reusable.waitForSiebelLoader();	
					verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
					click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
					Reusable.waitForSiebelLoader();	
							
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Diversityselect,"Yes","Diversityselect");
					sendKeys(IpVpnOrderObj.IpVpnOrder.RelatedServiced,IpvpnSiteNCSidnumberPrimarycomp2,"RelatedServiced");
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
					click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
					Reusable.waitForSiebelLoader();	
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"Underlyning IpvpnSite Order");
					click(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"Underlyning IpvpnSite Order");
					Reusable.waitForSiebelLoader();
						
				}
			}
			
			public void DscpMarkingUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String StandardDSCP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Standard DSCP");
				String PremiumDSCP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premium DSCP");
				String Business1DSCP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Business1 DSCP");
				String Business2DSCP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Business2 DSCP");
				String Business3DSCP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Business3 DSCP");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				
				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.DscpMarkingLink,"DSCP Marking Link");
				click(IpVpnOrderObj.IpVpnOrder.DscpMarkingLink,"DSCP Marking Link");
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();	
										
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.StandardDscp,StandardDSCP,"StandardDscp");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PreniumDscp,PremiumDSCP,"PreniumDscp");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Business1Dscp,Business1DSCP,"Business1Dscp");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Business2Dscp,Business2DSCP,"Business2Dscp");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Business3Dscp,Business3DSCP,"Business3Dscp");
				
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
				Reusable.waitForSiebelLoader();	
						
			}
			
			public void EndpointCircuitVLUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
				String WANVLANID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"WAN VLAN ID");
				String PrimaryBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
				String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
				String Business3DSCP = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Business3 DSCP");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				
				//Geturl(IpvpnNCEndpointProductOrderURL.get());
				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.EndpointCircuitDetailVL,"Endpoint Circuit Details VL Link");
				click(IpVpnOrderObj.IpVpnOrder.EndpointCircuitDetailVL,"Endpoint Circuit Details VL Link");
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();	
										
				if (RouterType.toString().contains("Unmanaged"))
				{
					sendKeys(IpVpnOrderObj.IpVpnOrder.Wanvlanid,WANVLANID.toString()); //it should be more than 2
					Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.Wanvlanid,Keys.ENTER);
					Reusable.waitForSiebelLoader();	
				}
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLServiceBandwidth,PrimaryBandwidth,"VLServiceBandwidth");

				if (OrderType.toString().contains("Existing VPN"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Secondary","VLConnectionType");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Primary","VLConnectionType");
				}

				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
				Reusable.waitForSiebelLoader();	
			}
			
			public void EndpointProductDetailsUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access Technology");
				String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");
				String PrimaryBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
				String Layer3Resilience = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Layer3_Resilience");
				String RouterTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Technology");
				String IpvpnSiebelSiteOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing Siebel SiteOrder No");
				String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Type");
				String IpvpnSiebelServiceNetworkreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Network_Ref");
				String BackupBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Backup Bandwidth");
				String SubVPNID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Sub VPN ID");
				String IpvpnSiebelServiceOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
				String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
				String NetflowEnabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Netflow Enabled");
				String IpvpnNCSiteidnumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Prim Site NC ID");	
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEndpoint Order");
				Reusable.waitForSiebelLoader();	
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();	
								
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderSystemName,"SPARK","OrderSystemName");
				String[] SiteOrderNo=IpvpnSiebelSiteOrdernumber.split("/");
			
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessTechnolgy,AccessTechnology,"AccessTechnolgy");
				if(Topology.toString().contains("Any to Any"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,"Any to Any","Topology");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,Topology,"Topology");	
				}
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Bandwidth,PrimaryBandwidth,"Bandwidth");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Layer3Resilience,Layer3Resilience,"Layer3Resilience");
				sendKeys(IpVpnOrderObj.IpVpnOrder.SiteOrderNumber,SiteOrderNo[1]);
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderSystemName,"SPARK","OrderSystemName");
				sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSysSerId,SiteOrderNo[0]);
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.RouterTechnology,RouterTechnology,"RouterTechnology");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.RouterType,RouterType,"RouterType");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Ipvpnnetworkreference,IpvpnSiebelServiceNetworkreference);
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.OrderManageGrp,"Service Delivery","OrderManageGrp");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.BackupBandwidth,BackupBandwidth,"BackupBandwidth");
				sendKeys(IpVpnOrderObj.IpVpnOrder.OrderSystemOrderNo,SiteOrderNo[1]);
				sendKeys(IpVpnOrderObj.IpVpnOrder.Subvpnid,SubVPNID.toString());
				String[] serviceorderNo=IpvpnSiebelServiceOrdernumber.split("/");
				sendKeys(IpVpnOrderObj.IpVpnOrder.ServiceOrderRef,serviceorderNo[0]);
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Servicetype,ServiceType,"Servicetype");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.NetFlowFlag,NetflowEnabled,"NetFlowFlag");
				sendKeys(IpVpnOrderObj.IpVpnOrder.RelatedIpVpnSite,IpvpnNCSiteidnumber,"RelatedIpVpnSite");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are Entered and Updated");
				Reusable.waitForSiebelLoader();	
				
				//String IpvpnNCEndidnumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NCServiceid));
				Reusable.waitForSiebelLoader();	
			}
			
			public void EndSiteDetailsUpdateSecondary(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String SecondaryResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Secondary_Resilience_Option");
				String AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Technology");
				String SecondaryAccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Secondary_Access_Type");
				String SiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Site_ID");
				String RouterTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Router_Technology");
				String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation_Interface");
				String PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Port_Role");
				String IpvpnSiebelServiceNetworkreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Network_Ref");
				String BackupBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Backup_Bandwidth");
				String SubVPNID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Sub_VPN_ID");
				String IpvpnSiebelServiceOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
				String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
				String NetflowEnabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Netflow_Enabled");
				String AendVLANTaggingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend_VLAN_Tagging_Mode");
				String CabinetType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_Type");
				String CabineID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cabinet_ID");
				String IpvpnNCSiteProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_URL");		
				
				getUrl(IpvpnNCSiteProductOrderURL);
							
				verifyExists(IpVpnOrderObj.IpVpnOrder.EndSiteOrder,"End site product order");
				click(IpVpnOrderObj.IpVpnOrder.EndSiteOrder,"End site product order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Button");
				
				if(SecondaryResilienceOption.toString().equalsIgnoreCase("Protected"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.ResilienceOption,"Protected","ResilienceOption");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.ResilienceOption,"Unprotected","ResilienceOption");
				}
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessTechnolgy,AccessTechnology,"AccessTechnolgy");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessType,SecondaryAccessType,"AccessType");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.SiteEnd,"A END","AccessType");
				sendKeys(IpVpnOrderObj.IpVpnOrder.SiteID,SiteID.toString());
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Primary","VLConnectionType");
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				Reusable.waitForSiebelLoader();
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccessPortLink,"Access Port Link");
				click(IpVpnOrderObj.IpVpnOrder.AccessPortLink,"Access Port Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
								
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PresentInterface,PresentationInterface,"PresentInterface");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PresentConnectType,"SC/PC","PresentConnectType");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessportRole,PortRole,"AccessportRole");
				if(PortRole.toString().contains("VLAN"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VlanTaggingMode,AendVLANTaggingMode,"VlanTaggingMode");
					Reusable.waitForSiebelLoader();
					
				}
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"End Site Product Order");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"End Site Product Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.CPElink,"CPE Link");
				click(IpVpnOrderObj.IpVpnOrder.CPElink,"CPE Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Button");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CabinetType,CabinetType,"CabinetType");
				sendKeys(IpVpnOrderObj.IpVpnOrder.CabinetID,CabineID.toString());
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
				//Geturl(IpvpnNCSiteProductOrderURL.get());
			}
			
			public void EndSiteDetailsUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String EndResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Resilience Option");
				String AccessTechnology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access Technology");
				String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"AEnd Access type");
				String SiteID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Site ID");
				String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
				String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Presentation Interface");
				String PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Port Role");
				String IpvpnSiebelServiceNetworkreference = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Network_Ref");
				String BackupBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Backup Bandwidth");
				String SubVPNID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Sub VPN ID");
				String IpvpnSiebelServiceOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
				String ServiceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Type");
				String NetflowEnabled = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Netflow Enabled");
				String AendVLANTaggingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Vlan Tagging Mode");
				String CabinetType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Cabinet Type");
				String CabineID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Cabinet Type");
				String IpvpnNCSiteProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_URL");		
				
				getUrl(IpvpnNCSiteProductOrderURL);
							
				verifyExists(IpVpnOrderObj.IpVpnOrder.EndSiteOrder,"End site product order");
				click(IpVpnOrderObj.IpVpnOrder.EndSiteOrder,"End site product order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Button");
				
			
				if(EndResilienceOption.toString().equalsIgnoreCase("Protected"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.ResilienceOption,"Protected","ResilienceOption");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.ResilienceOption,"Unprotected","ResilienceOption");
				}
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessTechnolgy,AccessTechnology,"AccessTechnolgy");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessType,AccessType,"AccessType");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.SiteEnd,"A END","SiteEnd");
				sendKeys(IpVpnOrderObj.IpVpnOrder.SiteID,SiteID,"SiteID");

				if (OrderType.toString().contains("Existing VPN"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Secondary","VLConnectionType");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Primary","VLConnectionType");
				}
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccessPortLink,"Access Port Link");
				click(IpVpnOrderObj.IpVpnOrder.AccessPortLink,"Access Port Link");
						
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
								
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PresentInterface,PresentationInterface,"PresentInterface");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PresentConnectType,"SC/PC","PresentConnectType");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessportRole,PortRole,"CabinetType");
				if(PortRole.toString().contains("VLAN"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VlanTaggingMode,AendVLANTaggingMode,"VlanTaggingMode");
					Reusable.waitForSiebelLoader();
				}
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"End Site Product Order");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Navigate to End Site Product Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.CPElink,"CPE Link");
				click(IpVpnOrderObj.IpVpnOrder.CPElink,"CPE Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CabinetType,CabinetType,"CabinetType");
				sendKeys(IpVpnOrderObj.IpVpnOrder.CabinetID,CabineID.toString());
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
			
			}
			
			public void EthernetProductDetailsSecondary(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String COSClassification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"COS_Classification");
				String ClassofService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Class_of_Service");
				String AccessType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Access_Type");
				String IpvpnSiebelSiteOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_SiteOrder_No");
				
				String[] SiteOrderNo=IpvpnSiebelSiteOrdernumber.split("/");
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningEthernetOrder,"Ethernet Product Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningEthernetOrder,"Ethernet Product Order");
									
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
								

				//To updated based on scenario
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Secondary","VLConnectionType");

				sendKeys(IpVpnOrderObj.IpVpnOrder.CommercialProductName,"IPVPN");

				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CircuitCategory,"IA","CircuitCategory");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CosClassification,COSClassification,"CosClassification");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.ClassOfService,ClassofService,"ClassOfService");
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
				
			}
			
			public void EthernetProductDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String OrderType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
				String COSClassification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"COS Classification");
				String ClassofService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Class of Service");
				String IpvpnSiebelSiteOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing Siebel SiteOrder No");
				
				String[] SiteOrderNo=IpvpnSiebelSiteOrdernumber.split("/");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningEthernetOrder,"Ethernet Product Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningEthernetOrder,"Ethernet Product Order");
									
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
								

				//To updated based on scenario
				if (OrderType.toString().contains("Existing VPN"))
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Secondary","VLConnectionType");
				}
				else
				{
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Primary","VLConnectionType");
				}

				sendKeys(IpVpnOrderObj.IpVpnOrder.CommercialProductName,"IPVPN","Product Name");

				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CircuitCategory,"IA","CircuitCategory");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CosClassification,COSClassification,"CosClassification");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.ClassOfService,ClassofService,"ClassOfService");
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
				
			}
			
			public void Ipv4DhcpRelayUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String DHCPRelayIPv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"DHCP Address");
				String COSClassification = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"COS Classification");
				String ClassofService = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Class of Service");
				String IpvpnSiebelSiteOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing Siebel SiteOrder No");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");

				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEnd pointOrder");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"Underlyning VpnEnd pointOrder");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Ipv4DhcpRelayLink,"IPv4 DHCP Relay Link");
				click(IpVpnOrderObj.IpVpnOrder.Ipv4DhcpRelayLink,"IPv4 DHCP Relay Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.DhcpRelayIpv4Add,DHCPRelayIPv4Address.toString());
				//		SendKeys(getwebelement(xml.getlocator("//locators/ComponentDetails/SiebelCompId")),Inputdata[22].toString());
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
			}
			
			public void ExistingEndpointOrder(Object[] Inputdata) throws Exception
			{
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filter");
				click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filter");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","FilterSelectName");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,"39972");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
			
				String IpvpnNCEndpointProductOrdernumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
				//IpvpnNCEndpointProductOrdernumber.set("Order #0000039972");
				
				
				String[] arrOfStr = IpvpnNCEndpointProductOrdernumber.split("#", 0);
			
				ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*", 10);
				
				
				String IpvpnNCEndpointProductOrderURL=getAttributeFrom(IpVpnOrderObj.IpVpnOrder.LinkforOrder,"href");
			
				//		Geturl(IpvpnEndpointProductOrderURL.get());
				//		Clickon(getwebelement(xml.getlocator("//locators/UnderlyningIpvpnSiteOrder")));
				//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open the Under Lying Order");
				//		Clickon(getwebelement(xml.getlocator("//locators/GeneralInformation/GeneralInformationTab")));
				//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
				//		Thread.sleep(3000);
				//		Clickon(getwebelement(xml.getlocator("//locators/IpAddressing/IpAddressingLink")));
				//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Ip Addressing Link");
			}
			
			public void ExistingSiteOrder(Object[] Inputdata) throws Exception
			{
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filter");
				click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filter");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","FilterSelectName");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,"37464");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
				

				String IpvpnNCSiteProductOrdernumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
				//IpvpnNCSiteProductOrdernumber.set("Order #0000037464");
				
				
				String[] arrOfStr = IpvpnNCSiteProductOrdernumber.split("#", 0);
			
				ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*", 10);
				
				
				String IpvpnNCSiteProductOrderURL = getAttributeFrom(IpVpnOrderObj.IpVpnOrder.LinkforOrder,"href");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"UnderLying Order");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningIpvpnSiteOrder,"UnderLying Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				
			}
			
			public void GotoErrors(String testDataFile,String  sheetName,String  scriptNo,String  dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL");
				String IpvpnNCEndpointProductOrdernumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
				String[] arrOfStr = IpvpnNCEndpointProductOrdernumber.split("#", 0);

				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accounts Composite Orders Tab");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accounts Composite Orders Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","FilterSelectName");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,"37464");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply button");
				
				//String IpVpnOrder = findWebElement("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::*[4]");
			
				String Errors= getTextFrom("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::*[4]");
				if(Errors.equalsIgnoreCase("Blocked by Errors"))
				{
					ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*", 10);
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.TaskTab,"Tasks Tab");
					click(IpVpnOrderObj.IpVpnOrder.TaskTab,"Tasks Tab");
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.ExecutionFlowlink,"Execution Flow link");
					click(IpVpnOrderObj.IpVpnOrder.ExecutionFlowlink,"Execution Flow link");
					Reusable.waitForSiebelLoader();
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.Errors,"Errors Tab");
					click(IpVpnOrderObj.IpVpnOrder.Errors,"Errors Tab");
					Reusable.waitForSiebelLoader();
									
				}
				
			}
			
			public static void main(String[] args) {
				String num="2";

				if(Integer.parseInt(num.toString())>1)
				{
					int dd = Integer.parseInt(num.toString());
					int sum=dd*2;
					System.out.println("sum:"+sum);
				}
			}
			
			public void NetflowUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String IPv4address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"IPv4address");
				String Netflowversion = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Netflow_version");
				String PortNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Port Number");								
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.Netflowlink,"Netflow Link");
				click(IpVpnOrderObj.IpVpnOrder.Netflowlink,"Netflow Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();	
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.IPv4address,IPv4address,"IPv4 Address");   
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Netflowversion,Netflowversion,"Netflow Version");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Portnumber,PortNumber,"Port Number");
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are filled and updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required Details are filled and updated");
				Reusable.waitForSiebelLoader();	
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Navigate to IPVPN Service Product Order");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Navigate to IPVPN Service Product Order");
				Reusable.waitForSiebelLoader();	
				
				
				
			}
			
			public void ProcessEndpointOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
				String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_No");
				
				getUrl(IpvpnNCEndpointProductOrderURL);
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				Reusable.waitForSiebelLoader();	
				//verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				//click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
														
				//String IpvpnNCEndpointProductOrdernumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
				
				//String[] arrOfStr = IpvpnNCEndpointProductOrdernumber.split("#", 0); 
								
				//ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/parent::*/td//input", 10);
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","Filter selection");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,Ordernumber);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.OrderCheckbox,"Order selection");
				click(IpVpnOrderObj.IpVpnOrder.OrderCheckbox,"Order selection");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Account Name Sorting");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Start Processing");	
								
			}
			public void ProcessServiceOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_No");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				Reusable.waitForSiebelLoader();
				Reusable.waitForAjax();
				
				//String IpvpnNCServiceProductOrdernumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
				//String[] arrOfStr = IpvpnNCServiceProductOrdernumber.split("#", 0); 
				//click("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/parent::*/td//input", "Process Order");
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","Filter selection");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,Ordernumber);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.OrderCheckbox,"Order selection");
				click(IpVpnOrderObj.IpVpnOrder.OrderCheckbox,"Order selection");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Start Processing");
				click(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Start Processing");
				Reusable.waitForSiebelLoader();	
				Reusable.waitForAjax();
			}
			
			public void ProcessSiteOrder(Object[] Inputdata) throws Exception
			{
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				
				String IpvpnNCSiteProductOrdernumber=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
				String[] arrOfStr = IpvpnNCSiteProductOrdernumber.split("#", 0); 
				ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/parent::*/td//input", 10);
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Start Processing");
				click(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Start Processing");
				Reusable.waitForSiebelLoader();	
								
			}

			public void ProcessSiteOrderSecondary(Object[] Inputdata) throws Exception
			{
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bred crumb");
				Reusable.waitForSiebelLoader();	
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				
				String IpvpnNCSiteProductOrdernumberSecondary=(getTextFrom(IpVpnOrderObj.IpVpnOrder.NcOrderNumber));
				String[] arrOfStr = IpvpnNCSiteProductOrdernumberSecondary.split("#", 0); 
				ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/parent::*/td//input", 10);
			
				verifyExists(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Start Processing");
				click(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Start Processing");
				Reusable.waitForSiebelLoader();	
								
			}
			
			public void SnmpIPv4DevicesUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String CustomerSubVPNAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer Sub-VPN Alias");
				String SNMPDeviceIPv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Device IPv4 Address");
				String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Device Type");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

				getUrl(IpvpnNCEndpointProductOrderURL);
				
				if(verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder))
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"IPVPN Service Order");
					click(IpVpnOrderObj.IpVpnOrder.UnderlyningipvpnServiceOrder,"IPVPN Service Order");
				}
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SnmpIPv4deviceslink,"SNMP IPv4 Device update Link");
				click(IpVpnOrderObj.IpVpnOrder.SnmpIPv4deviceslink,"SNMP IPv4 Device update Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.Customersubvpnalias,CustomerSubVPNAlias,"Customersubvpnalias");
				//		SendKeys(getwebelement(xml.getlocator("//locators/ComponentDetails/SiebelCompId")),Inputdata[22].toString());
				String deviceipaddress[]=SNMPDeviceIPv4Address.toString().split("\\.");
				Reusable.waitForAjax();
													
				for(int i=0;i<deviceipaddress.length;i++)
				{
					sendKeys("@xpath=//td/a[text()='SNMP Device IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3']["+(i+1)+"]",deviceipaddress[i]);
					Reusable.waitForAjax();
				}
				verifyExists(IpVpnOrderObj.IpVpnOrder.Devicetype,"Device Type");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Devicetype,DeviceType,"Device type");

				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bredcrumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Navigate to IPVPN Service Product Order");
				Reusable.waitForSiebelLoader();
			}
			
			public void SnmpReadonlyAccessUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String SNMPFormat = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Format");
				String SNMPDeviceIPv4Address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SNMP Device IPv4 Address");
				String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Device Type");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Snmpreadonlylink,"Snmp read only access Link");
				click(IpVpnOrderObj.IpVpnOrder.Snmpreadonlylink,"Snmp read only access Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Snmpformat,SNMPFormat,"SNMP Format");
				Reusable.waitForSiebelLoader();
																	
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
						
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bredcrumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Navigate to IPVPN Service Product Order");
				Reusable.waitForSiebelLoader();
				
			}
			
			public void SnmpV3Update(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException  
			{
				String GroupName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Group Name");
				String Username = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Username");
				String AuthenticationPassword = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Authentication Password");
				String PrivacyPassword = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Privacy Password");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"Navigate to General Information Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Snmpv3link,"SNMP V3 Link");
				click(IpVpnOrderObj.IpVpnOrder.Snmpv3link,"SNMP V3 Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit button");
				Reusable.waitForSiebelLoader();
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.groupname,GroupName,"Group Name");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Username,Username,"User Name");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Authentpassword,AuthenticationPassword,"Authentication Password");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Privacypassword,PrivacyPassword,"Privacy Password");
				Reusable.waitForAjax();
																	
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Updated");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Required details are filled and updated");
				Reusable.waitForSiebelLoader();
						
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Account bredcrumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Navigate to IPVPN Service Product Order");
				Reusable.waitForSiebelLoader();
								
			}
			
			public void UpdateSubVPNProductComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String Topology = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Topology");
				String SubVPNID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Sub VPN ID");
				String CustomerAlias = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Customer Alias");
				String SiebelComponentID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Existing_Siebel_ServOrder_No");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SubVPNPlink,"SubVPN Link");
				click(IpVpnOrderObj.IpVpnOrder.SubVPNPlink,"SubVPN Link");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Sub-VPN Product Component Link");

				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"SubVPN Link");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"SubVPN Link");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");

				if(Topology.contains("Any to Any"))
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.Topology,"Topology Selection");
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,"Any to Any", "Select Topology");
					//selectByVisibleText(getwebelement(xml.getlocator("//locators/Topology")),"Any to Any");
				}
				else
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.Topology,"Topology Selection");
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,Topology, "Select Topology");
					//selectByVisibleText(getwebelement(xml.getlocator("//locators/Topology")),Inputdata[30].toString());	
				}
				verifyExists(IpVpnOrderObj.IpVpnOrder.IpAddressingFormat,"Topology Selection");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.IpAddressingFormat,"IPv4 format only", "Addressing Format");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Subvpnid,"Subvpnid");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Subvpnid,SubVPNID, "Subvpnid");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Customeralias,"Customeralias");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Customeralias,CustomerAlias, "Customeralias");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Voiptrafficonly,"Voiptrafficonly");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Voiptrafficonly,"No", "Voiptrafficonly");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"Topology Selection");
				sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentID, "Addressing Format");
				
				/*selectByVisibleText(getwebelement(xml.getlocator("//locators/GeneralInformation/IpAddressingFormat")),"IPv4 format only");
				SendKeys(getwebelement(xml.getlocator("//locators/GeneralInformation/Subvpnid")),Inputdata[46].toString());
				SendKeys(getwebelement(xml.getlocator("//locators/GeneralInformation/Customeralias")),Inputdata[47].toString());
				selectByVisibleText(getwebelement(xml.getlocator("//locators/GeneralInformation/Voiptrafficonly")),"No");

				SendKeys(getwebelement(xml.getlocator("//locators/ComponentDetails/SiebelCompId")),Inputdata[22].toString());*/

				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				Thread.sleep(5000);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are Filled and Updated");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to IPVPN Service Product Order");
			}
			public void CompleteServiceOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCServiceProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
				String IpvpnNCServiceProductOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_No");
				getUrl(IpvpnNCServiceProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				String[] arrOfStr = IpvpnNCServiceProductOrdernumber.split("#", 0); 
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Orders Tab");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for IPVPN Service Order to be Completed");
				getUrl(IpvpnNCServiceProductOrderURL);
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Orders Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
			}
			public void ProcessSiteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_No");
				String IpvpnNCSiteProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_URL");
				
				getUrl(IpvpnNCSiteProductOrderURL);
				Reusable.waitForAjax();
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Account Name Sorting");
				Reusable.waitForAjax();
				//String[] arrOfStr = IpvpnNCSiteProductOrdernumber.get().split("#", 0); 
				//click("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/parent::*/td//input");
				//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the IPVPN Site Order");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","Filter selection");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,Ordernumber);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.OrderCheckbox,"Order selection");
				click(IpVpnOrderObj.IpVpnOrder.OrderCheckbox,"Order selection");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Account Name Sorting");
				click(IpVpnOrderObj.IpVpnOrder.StartProccessing,"Account Name Sorting");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Start Processing");
			}

			public void UpdateEthernetProductVLComponent(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String PrimaryBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
				String Order_Type = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.EthernetconnectionproductLink,"Ethernetconnectionproduct Link");
				click(IpVpnOrderObj.IpVpnOrder.EthernetconnectionproductLink,"Ethernetconnectionproduct Link");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Ethernet Product Component Link");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Link");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit Link");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");

				verifyExists(IpVpnOrderObj.IpVpnOrder.Topology,"Topology");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Topology,"Point to Point","Topology");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.CircuitCategory,"CircuitCategory");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CircuitCategory,"IA","CircuitCategory");
				Reusable.waitForAjax();
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.VLCompBandwidth,"VLCompBandwidth");
				sendKeys(IpVpnOrderObj.IpVpnOrder.VLCompBandwidth,PrimaryBandwidth,"VLCompBandwidth");
				keyPress(IpVpnOrderObj.IpVpnOrder.VLCompBandwidth,Keys.ENTER);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				Reusable.waitToPageLoad();

				if (Order_Type.contains("Existing VPN")) //spoke order we shoud not mention Existing VPN - it should be pick primary
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"VLConnection Type");
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Secondary","VLConnection Type");
					Reusable.waitForAjax();					
				}
				else 
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"VLConnection Type");
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VLConnectionType,"Primary","VLConnection Type");
					Reusable.waitForAjax();
				}
				Reusable.waitForAjax();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are Filled and Updated");
			}
			
			public void LanVlanCompUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String PrimaryBandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
				String LANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"LAN VLAN");
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Endpoint_Order_URL");
				String VLANTagID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Aend Vlan Tag Id");
				String SiebelComponentID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Siebel Component ID");
				
				if(!LANVLAN.contains("No"))
				{
					getUrl(IpvpnNCEndpointProductOrderURL);
					verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");
					click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open the UnderLying End point Order");
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
					click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"General Information Tab");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
					String currentUrl=Reusable.getURLFromPage();
					List<WebElement> linkList = webDriver.findElements(By.xpath("//span[contains(text(),'LAN VLAN Component')]/parent::*"));
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					//Log.info("size:"+linkList.size());
					for(int j=0;j<linkList.size();j++)
					{
						findWebElement("@xpath=(//span[contains(text(),'LAN VLAN Component')]/parent::*)["+(j+1)+"]").click();;
						Thread.sleep(5000);
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on LAN VLAN Component Link");
						verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
						click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
						Reusable.waitForAjax();
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
						String SiebelCompId = getAttributeFrom(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"value");
						if(!SiebelCompId.equals(""))
						{
							getUrl(currentUrl);
							Thread.sleep(20000);
						}
						else
						{
							if(j==0) //if value are same then this condition is not required
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.LanVlanTagId,"LanVlanTagId");
								sendKeys(IpVpnOrderObj.IpVpnOrder.LanVlanTagId,VLANTagID,"LanVlanTagId");
								
								verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
								sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentID,"SiebelCompId");
							}
							else
							{
								//if LanVlanTagId is diifrerent for each component then we need below line and column need to be add
								verifyExists(IpVpnOrderObj.IpVpnOrder.LanVlanTagId,"LanVlanTagId");
								sendKeys(IpVpnOrderObj.IpVpnOrder.LanVlanTagId,VLANTagID,"LanVlanTagId");
								
								verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
								sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentID,"SiebelCompId");
							}
							verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
							click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
							Reusable.waitForAjax();
							Reusable.waitForpageloadmask();
							getUrl(currentUrl);
							Reusable.waitForAjax();
							Reusable.waitForpageloadmask();
						}
					}
				}
			}
			public void IPv4AddressBasedMarkingUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String TrafficClass = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Traffic Class");				
				String DestIPv4Netmask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Dest IPv4 Netmask");
				String SourIPv4Netmask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Sour IPv4 Netmask");
				String DestIPv4Add = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Dest IPv4 Add");
				String SourIPv4Add = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Sour IPv4 Add");
				String SiebelComponentID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siebel Component ID");
				String TrafficClass1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "WAN bgp- AS no");
				String Protocol1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "GX/LTS Serial Number");
				String DestIPv4Netmask1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "WAN VLAN ID");
				String SourIPv4Netmask1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Resilience Option");
				String DestIPv4Add1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BEnd Access type");
				String SourIPv4Add1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BEnd Access Technology");
				getUrl(IpvpnNCEndpointProductOrderURL);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");
				click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");

				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
				click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");				
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
		        List<WebElement> linkList = webDriver.findElements(By.xpath("//span[contains(text(),'IPv4 Address Based Marking')]/parent::*"));
				String currentUrl=Reusable.getURLFromPage();
		        Thread.sleep(5000);
		        //Log.info("size:"+linkList.size());
		        for(int k=0;k<linkList.size();k++)
		        {
		            findWebElement("@xpath=(//span[contains(text(),'IPv4 Address Based Marking')]/parent::*)["+(k+1)+"]").click();
		            Reusable.waitForAjax();
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on IPv4 Address Based Marking Link");
					
					verifyExists(IpVpnOrderObj.IpVpnOrder.ServiceInfoTab,"ServiceInfoTab");
					click(IpVpnOrderObj.IpVpnOrder.ServiceInfoTab,"ServiceInfoTab");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Service Information Tab");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
					verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
					click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
					Thread.sleep(5000);
					String SiebelCompId = getAttributeFrom("@xpath=//locators/ComponentDetails/SiebelCompId","value");
		            if(!SiebelCompId.equals(""))
		            {
						getUrl(currentUrl);
		                Reusable.waitForpageloadmask();
		                Reusable.waitForpageloadmask();
		            }
		            else
		            {
		            	if(k==0)
		    			{
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.TrafficClass,"TrafficClass");
		            		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.TrafficClass,TrafficClass,"TrafficClass");
		            		//verifyExists(IpVpnOrderObj.IpVpnOrder.Protocol,"Protocol");
		            		//selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Protocol,"UDP","Protocol");
		            
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.DestIpv4Netmask,"DestIpv4Netmask");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.DestIpv4Netmask,DestIPv4Netmask,"DestIpv4Netmask");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.SourIpv4Netmask,"SourIpv4Netmask");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.SourIpv4Netmask,SourIPv4Netmask,"SourIpv4Netmask");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.DestIpv4Addr,"DestIpv4Addr");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.DestIpv4Addr,DestIPv4Add,"DestIpv4Addr");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.SourIpv4Add,"SourIpv4Add");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.SourIpv4Add,SourIPv4Add,"SourIpv4Add");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentID,"SiebelCompId");		        
		            		Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
		    			}
		            	else
		            	{
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.TrafficClass,"TrafficClass");
		            		selectByVisibleText(IpVpnOrderObj.IpVpnOrder.TrafficClass,TrafficClass1,"TrafficClass");
		            		//verifyExists(IpVpnOrderObj.IpVpnOrder.Protocol,"Protocol");
		            		//selectByVisibleText(IpVpnOrderObj.IpVpnOrder.Protocol,Protocol1,"Protocol");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.DestIpv4Netmask,"DestIpv4Netmask");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.DestIpv4Netmask,DestIPv4Netmask1,"DestIpv4Netmask");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.SourIpv4Netmask,"SourIpv4Netmask");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.SourIpv4Netmask,SourIPv4Netmask1,"SourIpv4Netmask");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.DestIpv4Addr,"DestIpv4Addr");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.DestIpv4Addr,DestIPv4Add1,"DestIpv4Addr");
		            		verifyExists(IpVpnOrderObj.IpVpnOrder.SourIpv4Add,"SourIpv4Add");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.SourIpv4Add,SourIPv4Add1,"SourIpv4Add");		            						            
							verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
		            		sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentID,"SiebelCompId");
							Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
		            	}
		            	verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
		            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
		            	Reusable.waitForpageloadmask();
						Reusable.waitForAjax();
		            	ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Required Details are Entered and updated");
						getUrl(currentUrl);
						Reusable.waitForpageloadmask();
						Reusable.waitForAjax();
		            }
		        }
			}
			public void WanAddressingUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String IPv4Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "IPv4 Prefix");
				String PEIPv4Add = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "	");
				String CPEIPv4Add = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "CPE IPv4 Add");
				String PENetworkElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
				String RouterType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Router_Type");
				
				getUrl(IpvpnNCEndpointProductOrderURL);
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");
            	click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");				
            	verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
            	click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.WanAddressingLink,"WanAddressingLink");
            	click(IpVpnOrderObj.IpVpnOrder.WanAddressingLink,"WanAddressingLink");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on WAN Addressing Link");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
            	click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
            	Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
				verifyExists(IpVpnOrderObj.IpVpnOrder.PEName,"Edit");
				sendKeys(IpVpnOrderObj.IpVpnOrder.PEName,PENetworkElement,"Edit");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.PEIpv4Prefix,"Edit");
            	sendKeys(IpVpnOrderObj.IpVpnOrder.PEIpv4Prefix,IPv4Prefix,"Edit");

				if (RouterType.contains("Unmanaged"))
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.PEIpv4Address,"PEIpv4Address");
					sendKeys(IpVpnOrderObj.IpVpnOrder.PEIpv4Address,PEIPv4Add,"PEIpv4Address");
	            	verifyExists(IpVpnOrderObj.IpVpnOrder.CPEIpv4Address,"CPEIpv4Address");
	            	sendKeys(IpVpnOrderObj.IpVpnOrder.CPEIpv4Address,CPEIPv4Add,"CPEIpv4Address");					
				}

				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are filled and updated");
			}
			public void StaticNatIpv4RuleUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String SubnetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Subnet Mask");
				String InsideIPv4Addr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Inside IPv4 Addr");
				String OutsideIPv4Addr = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Outside IPv4 Addr");
				
				getUrl(IpvpnNCEndpointProductOrderURL);
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");
            	click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");				
            	verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
            	click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.StaticIPRuleLink,"StaticIPRuleLink");
            	click(IpVpnOrderObj.IpVpnOrder.StaticIPRuleLink,"StaticIPRuleLink");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Static NAT IPv4 Rule Link");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
            	click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.SubnetMask,"SubnetMask");
				sendKeys(IpVpnOrderObj.IpVpnOrder.SubnetMask,SubnetMask,"SubnetMask");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.InsideIpv4Addr,"InsideIpv4Addr");
            	sendKeys(IpVpnOrderObj.IpVpnOrder.InsideIpv4Addr,InsideIPv4Addr,"InsideIpv4Addr");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.OutsideIpv4Addr,"OutsideIpv4Addr");
            	sendKeys(IpVpnOrderObj.IpVpnOrder.OutsideIpv4Addr,OutsideIPv4Addr,"OutsideIpv4Addr");
 
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are filled and updated");
			}
			public void WanBgpUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String MD5Pwd = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "MD5 Pwd");
				String HoldTimer = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hold Timer(s)");
				String KeepAlive = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Keep Alive(s)");
				String WANbgp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "WAN bgp- AS no");				
				
				getUrl(IpvpnNCEndpointProductOrderURL);
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");
            	click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");				
            	verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
            	click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.WanBgpLink,"WanBgpLink");
            	click(IpVpnOrderObj.IpVpnOrder.WanBgpLink,"WanBgpLink");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Static NAT IPv4 Rule Link");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
            	click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Md5Pwd,"Md5Pwd");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Md5Pwd,MD5Pwd,"Md5Pwd");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.HoldTimer,"HoldTimer");
            	sendKeys(IpVpnOrderObj.IpVpnOrder.HoldTimer,HoldTimer,"HoldTimer");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.KeepAlive,"KeepAlive");
            	sendKeys(IpVpnOrderObj.IpVpnOrder.KeepAlive,KeepAlive,"KeepAlive");			

            	//verifyExists(IpVpnOrderObj.IpVpnOrder.PeerASNum,"PeerASNum");
            	//clearTextBox(IpVpnOrderObj.IpVpnOrder.PeerASNum);
            	//sendKeys(IpVpnOrderObj.IpVpnOrder.PeerASNum,WANbgp,"PeerASNum");

				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are filled and updated");
			}
			
			public void VpnEndpointInformationUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{ 
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String PortRole = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend Port Role");
				String PresentationInterface = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Presentation Interface");
				String AendVLANTaggingMode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend Vlan Tagging Mode");
				String CabinetType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Cabinet Type");
				String CabinetID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Cabinet ID");
				
				System.out.println("VlanTaggingMode: "+AendVLANTaggingMode);
				getUrl(IpvpnNCEndpointProductOrderURL);
				verifyExists(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");
            	click(IpVpnOrderObj.IpVpnOrder.UnderlyningVpnEndpointOrder,"UnderlyningVpnEndpointOrder");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open the UnderLying VPN Endpoint Order");
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
            	click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.AccessPortLink,"AccessPortLink");
            	click(IpVpnOrderObj.IpVpnOrder.AccessPortLink,"AccessPortLink");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Static NAT IPv4 Rule Link");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
            	click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
				verifyExists(IpVpnOrderObj.IpVpnOrder.PresentInterface,"PresentInterface");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PresentInterface,PresentationInterface,"PresentInterface");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.PresentConnectType,"PresentConnectType");
            	selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PresentConnectType,"SC/PC","PresentConnectType");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.AccessportRole,"AccessportRole");
            	selectByVisibleText(IpVpnOrderObj.IpVpnOrder.AccessportRole,PortRole,"AccessportRole");	
								
				if(PortRole.contains("VLAN"))
				{
					verifyExists(IpVpnOrderObj.IpVpnOrder.VlanTaggingMode,"VlanTaggingMode");
					selectByVisibleText(IpVpnOrderObj.IpVpnOrder.VlanTaggingMode,AendVLANTaggingMode,"VlanTaggingMode");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are filled and updated");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();
				}
				verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required details are filled and updated");
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");
            	click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accountbredcrumb");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to End Site Product Order");
				verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
            	click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.CPElink,"CPElink");
            	click(IpVpnOrderObj.IpVpnOrder.CPElink,"CPElink");
	
            	verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
            	click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.CabinetType,"CabinetType");
            	selectByVisibleText(IpVpnOrderObj.IpVpnOrder.CabinetType,CabinetType,"CabinetType");
            	verifyExists(IpVpnOrderObj.IpVpnOrder.CabinetID,"CabinetID");
            	sendKeys(IpVpnOrderObj.IpVpnOrder.CabinetID,CabinetID,"CabinetID");
            	Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
            	verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required details are filled and updated");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required details are filled and updated");
			}
			public void LanInterfaceIpv4AddressingUpdate(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String CountofLANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Count of LAN VLAN");
				String Count_LANinterfaceipv4address = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Count of LAN interface ipv4 address");
				String LANInteIPv4Add = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Add");
				String LANInterIPv4Prefix = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Prefix");
				String LANVRRPIPv4Add = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN VRRP IPv4 Add 1");
				String SiebelComponentId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siebel Component ID");
				String SecondCPELANIPv4Add = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Second CPE LAN IPv4 Add");
				String LANInteIPv4Add1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Add-2");
				String LANInterIPv4Prefix1= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Prefix");
				String LANVRRPIPv4Add1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN VRRP IPv4 Add 1-2");
				String SecondCPELANIPv4Add1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Second CPE LAN IPv4 Add-2");
				String LANInteIPv4Add2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Add-1-3");
				String LANInterIPv4Prefix2= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Prefix");
				String LANVRRPIPv4Add2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN VRRP IPv4 Add 1-3");
				String SecondCPELANIPv4Add2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Second CPE LAN IPv4 Add-3");
				String LANInteIPv4Add3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN Inter IPv4 Add-4");				
				String LANVRRPIPv4Add3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LAN VRRP IPv4 Add 1-4");
				String SecondCPELANIPv4Add3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Second CPE LAN IPv4 Add-4");
				
				int count=0;
				for(int k=0;k<Integer.parseInt(CountofLANVLAN);k++)
				{
					getUrl(IpvpnNCEndpointProductOrderURL);
					Reusable.waitForpageloadmask();
					Reusable.waitForAjax();
										
					findWebElement("@xpath=(//span[contains(text(),'LAN VLAN Product Order')]/parent::a)["+(k+1)+"]").click();	
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open the UnderLying LAN VLAN Order");
					verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
	            	click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
					String curentUrl=Reusable.getURLFromPage();
					Reusable.waitForpageloadmask();
					Reusable.waitForAjax();
					for(int j=0;j<Integer.parseInt(Count_LANinterfaceipv4address);j++)
					{
						findWebElement("@xpath=(//span[contains(text(),'LAN Interface IPV4 Addressing')]/parent::*)["+(j+1)+"]").click();
						Reusable.waitForpageloadmask();
						Reusable.waitForAjax();
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on LAN Interface IPV4 Addressing Link");
						verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
		            	click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
						Reusable.waitForpageloadmask();
						Reusable.waitForAjax();
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
						String SiebelCompId = getAttributeFrom(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"value");
						if(!SiebelCompId.equals(""))
						{
							getUrl(curentUrl);
							Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
						}
						else
						{
							//below condition only supports for 4 set of LAN interface4 values , if u need more then need to add condition based on the values.
							if(count==0)
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"PrimaryRange");
				            	selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"Yes","PrimaryRange");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,"LanInterfIpv4Add");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,LANInteIPv4Add,"LanInterfIpv4Add");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,"LanInterfIpv4Prefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,LANInterIPv4Prefix,"LanInterfIpv4Prefix");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,"LanInterfVrrpIpv4Add1");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,LANVRRPIPv4Add,"LanInterfVrrpIpv4Add1");

								String deviceipaddress[]=SecondCPELANIPv4Add.split("\\.");
								for(int i=0;i<deviceipaddress.length;i++)
								{
									sendKeys("@xpath=(//td/a[text()='Secondary CPE LAN Interface IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3'])["+(i+1)+"]",deviceipaddress[i],"Device IP Address");
									Reusable.waitForpageloadmask();
									Reusable.waitForAjax();
								}
							}
							else if(count==1)
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"PrimaryRange");
				            	selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"No","PrimaryRange");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,"LanInterfIpv4Add");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,LANInteIPv4Add1,"LanInterfIpv4Add");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,"LanInterfIpv4Prefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,LANInterIPv4Prefix1,"LanInterfIpv4Prefix");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,"LanInterfVrrpIpv4Add1");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,LANVRRPIPv4Add1,"LanInterfVrrpIpv4Add1");
																
								String deviceipaddress[]=SecondCPELANIPv4Add1.split("\\.");
								for(int i=0;i<deviceipaddress.length;i++)
								{
									sendKeys("@xpath=(//td/a[text()='Secondary CPE LAN Interface IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3'])["+(i+1)+"]",deviceipaddress[i],"Device IP Address");
									Reusable.waitForpageloadmask();
									Reusable.waitForAjax();
								}
							}

							else if(count==2)
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"PrimaryRange");
				            	selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"No","PrimaryRange");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,"LanInterfIpv4Add");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,LANInteIPv4Add2,"LanInterfIpv4Add");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,"LanInterfIpv4Prefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,LANInterIPv4Prefix2,"LanInterfIpv4Prefix");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,"LanInterfVrrpIpv4Add1");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,LANVRRPIPv4Add2,"LanInterfVrrpIpv4Add1");
																
								String deviceipaddress[]=SecondCPELANIPv4Add2.split("\\.");
								for(int i=0;i<deviceipaddress.length;i++)
								{
									sendKeys("@xpath=(//td/a[text()='Secondary CPE LAN Interface IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3'])["+(i+1)+"]",deviceipaddress[i],"Device IP Address");
									Reusable.waitForpageloadmask();
									Reusable.waitForAjax();
								}
							}
							else
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"PrimaryRange");
				            	selectByVisibleText(IpVpnOrderObj.IpVpnOrder.PrimaryRange,"No","PrimaryRange");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,"LanInterfIpv4Add");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Add,LANInteIPv4Add3,"LanInterfIpv4Add");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,"LanInterfIpv4Prefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfIpv4Prefix,LANInterIPv4Prefix2,"LanInterfIpv4Prefix");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,"LanInterfVrrpIpv4Add1");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.LanInterfVrrpIpv4Add1,LANVRRPIPv4Add3,"LanInterfVrrpIpv4Add1");
																
								String deviceipaddress[]=SecondCPELANIPv4Add3.split("\\.");
								for(int i=0;i<deviceipaddress.length;i++)
								{
									sendKeys("@xpath=(//td/a[text()='Secondary CPE LAN Interface IPv4 Address']/parent::*/following-sibling::*//input[@maxlength='3'])["+(i+1)+"]",deviceipaddress[i],"Device IP Address");
									Reusable.waitForpageloadmask();
									Reusable.waitForAjax();
								}
							}

							verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
			            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required details are filled and updated");
							Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are filled and updated");	
							getUrl(curentUrl);
							Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
						}
						count++;
					}
				}
			}
			public void StaticIpv4RouteUpdate(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String Count_LANVLAN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Count of LAN VLAN");
				String SiebelComponentId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Siebel Component ID");
				String TargetNetwork = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Target Network");
				String NextHopIp = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Next Hop IP");
				String SubnetMask = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Subnet Mask");
				String TargetNetwork1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Target Network-2");
				String NextHopIp1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Next Hop IP-2");				
				String TargetNetwork2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Target Network-3");
				String NextHopIp2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Next Hop IP-3");
				String TargetNetwork3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Target Network-4");
				String NextHopIp3 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Next Hop IP-4");
				String AdditionalLANRouting_count = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Additional LAN Routing static ipv4 -count");
				
				int count=0;
				for(int k=0;k<Integer.parseInt(Count_LANVLAN);k++)
				{
					getUrl(IpvpnNCEndpointProductOrderURL);
					Reusable.waitForpageloadmask();
					Reusable.waitForAjax();
					findWebElement("@xpath=(//span[contains(text(),'LAN VLAN Product Order')]/parent::a)["+(k+1)+"]").click();	
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Open the UnderLying LAN VLAN Order");
					verifyExists(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
	            	click(IpVpnOrderObj.IpVpnOrder.GeneralInformationTab,"GeneralInformationTab");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to General Information Tab");
					String cuurentUrl=Reusable.getURLFromPage();
					Reusable.waitForpageloadmask();
					Reusable.waitForAjax();

					for(int j=0;j<Integer.parseInt(AdditionalLANRouting_count);j++)
					{
						findWebElement("@xpath=(//span[contains(text(),'Static IPv4 Route')]/parent::*)["+(j+1)+"]").click();
						Thread.sleep(5000);
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Static IPv4 Route Link");
						verifyExists(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
		            	click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
		            	Reusable.waitForpageloadmask();
						Reusable.waitForAjax();
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit button");
						
						if(!getAttributeFrom(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"value").equals(""))
						{
							getUrl(cuurentUrl);
							Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
						}
						else
						{
							if(count==0)
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.TargetNetwork,"LanInterfIpv4PTargetNetworkrefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.TargetNetwork,TargetNetwork,"TargetNetwork");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.NextHopIp,"NextHopIp");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.NextHopIp,NextHopIp,"NextHopIp");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SubnetMask,"SubnetMask");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SubnetMask,SubnetMask,"SubnetMask");
				
							}
							else if(count==1)
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.TargetNetwork,"LanInterfIpv4PTargetNetworkrefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.TargetNetwork,TargetNetwork1,"TargetNetwork");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.NextHopIp,"NextHopIp");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.NextHopIp,NextHopIp1,"NextHopIp");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SubnetMask,"SubnetMask");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SubnetMask,SubnetMask,"SubnetMask");
							}

							else if(count==2)
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.TargetNetwork,"LanInterfIpv4PTargetNetworkrefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.TargetNetwork,TargetNetwork2,"TargetNetwork");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.NextHopIp,"NextHopIp");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.NextHopIp,NextHopIp2,"NextHopIp");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SubnetMask,"SubnetMask");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SubnetMask,SubnetMask,"SubnetMask");
							}

							else
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.SiebelCompId,"SiebelCompId");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SiebelCompId,SiebelComponentId,"SiebelCompId");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.TargetNetwork,"LanInterfIpv4PTargetNetworkrefix");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.TargetNetwork,TargetNetwork3,"TargetNetwork");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.NextHopIp,"NextHopIp");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.NextHopIp,NextHopIp3,"NextHopIp");
				            	verifyExists(IpVpnOrderObj.IpVpnOrder.SubnetMask,"SubnetMask");
				            	sendKeys(IpVpnOrderObj.IpVpnOrder.SubnetMask,SubnetMask,"SubnetMask");
							}

							verifyExists(IpVpnOrderObj.IpVpnOrder.Update,"Update");
			            	click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required details are filled and updated");
							Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Required Details are filled and updated");	
							getUrl(cuurentUrl);
							Reusable.waitForpageloadmask();
							Reusable.waitForAjax();
						}
						count++;
					}
				}
			}
			public void CompleteEndpointOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
			{
				String IpvpnNCEndpointProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_URL");
				String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_No");
				String IpvpnNCEndpointProductOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Endpoint_Order_No");
				String Protocol = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Protocol");
						
				getUrl(IpvpnNCEndpointProductOrderURL);
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				String[] arrOfStr = IpvpnNCEndpointProductOrdernumber.split("#", 0); 

				verifyExists(IpVpnOrderObj.IpVpnOrder.TaskTab,"TaskTab");
            	click(IpVpnOrderObj.IpVpnOrder.TaskTab,"TaskTab");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on Tasks Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.ExecutionFlowlink,"ExecutionFlowlink");
            	click(IpVpnOrderObj.IpVpnOrder.ExecutionFlowlink,"ExecutionFlowlink");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on the Execution Flow Link");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Workitems,"Workitems");
            	click(IpVpnOrderObj.IpVpnOrder.Workitems,"Workitems");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on Workitems Tab");
				Reusable.waitForAjax();
				for (int k=1; k<=Integer.parseInt(Protocol.toString());k++)
				{										
					if(verifyExists(IpVpnOrderObj.IpVpnOrder.TaskActivetoComplete))
		        	{
		        		waitForElementToAppear(IpVpnOrderObj.IpVpnOrder.TaskActivetoComplete,90,10000);
		                click(IpVpnOrderObj.IpVpnOrder.TaskActivetoComplete,"Workitem in Ready status");
		        	}
		        	else
		        	{
		        		waitForElementToAppear(IpVpnOrderObj.IpVpnOrder.TaskReadytoComplete,90,10000);
		                click(IpVpnOrderObj.IpVpnOrder.TaskReadytoComplete,"Workitem in Ready status");
		        	} 
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on the Workitem in  Ready status");
					Completeworkitem(GetText(IpVpnOrderObj.IpVpnOrder.TaskTitle),testDataFile, sheetName, scriptNo, dataSetNo);
				}
				getUrl(IpvpnNCEndpointProductOrderURL);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigate to Composite Order");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accounts Composite Orders Tab");
		        click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accounts Composite Orders Tab");
		       
		       // verifyExists(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Sorting of orders");
		        //click(IpVpnOrderObj.IpVpnOrder.AccountNameSorting,"Sorting of orders");
		        
		        verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","Filter selection");
				Reusable.waitForAjax();				
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,Ordernumber);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				Reusable.waitForpageloadmask();

		        waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",120,20000);
		        String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Ordernumber + EthernetOrderObj.CompositOrders.arrorder1);
		        if (OrderStatus.contains("Process Completed"))
				{
					Report.LogInfo("Validate","\""+IpvpnNCEndpointProductOrderURL +"\" Is Completed Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, IpvpnNCEndpointProductOrderURL +" Is Completed Successfully");
				}
				else if (OrderStatus.contains("Process Started"))
				{
					Report.LogInfo("Validate","\""+IpvpnNCEndpointProductOrderURL +"\" Execution InProgress", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, IpvpnNCEndpointProductOrderURL +" Execution InProgress");
				}
			}
			
			public void CompleteSiteOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
			{
				String IpvpnNCSiteProductOrderURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_URL");
				String IpvpnNCSiteProductOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Site_Order_No");
				String Secondndcompitem = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "2nd comp item");
				
				getUrl(IpvpnNCSiteProductOrderURL);
				Reusable.waitForAjax();
				Reusable.waitForpageloadmask();
				String[] arrOfStr = IpvpnNCSiteProductOrdernumber.split("#", 0); 

				verifyExists(IpVpnOrderObj.IpVpnOrder.TaskTab,"TaskTab");
            	click(IpVpnOrderObj.IpVpnOrder.TaskTab,"TaskTab");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on Tasks Tab");
				verifyExists(IpVpnOrderObj.IpVpnOrder.ExecutionFlowlink,"ExecutionFlowlink");
            	click(IpVpnOrderObj.IpVpnOrder.ExecutionFlowlink,"ExecutionFlowlink");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on the Execution Flow Link");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				verifyExists(IpVpnOrderObj.IpVpnOrder.Workitems,"Workitems");
            	click(IpVpnOrderObj.IpVpnOrder.Workitems,"Workitems");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on Workitems Tab");
				Reusable.waitForAjax();
				for (int k=1; k<=Integer.parseInt(Secondndcompitem);k++)
				{
					waitForElementToAppear(IpVpnOrderObj.IpVpnOrder.TaskReadytoComplete,90,20000);
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Waiting for Manual Workitems to be displayed");
					verifyExists(IpVpnOrderObj.IpVpnOrder.TaskReadytoComplete,"TaskReadytoComplete");
	            	click(IpVpnOrderObj.IpVpnOrder.TaskReadytoComplete,"TaskReadytoComplete");
					ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Click on the Workitem in  Ready status");
					Completeworkitem(GetText(IpVpnOrderObj.IpVpnOrder.TaskTitle),testDataFile, sheetName, scriptNo, dataSetNo);
				}
				getUrl(IpvpnNCSiteProductOrderURL);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Navigate to Composite Order");
				verifyExists(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accounts Composite Orders Tab");
		        click(IpVpnOrderObj.IpVpnOrder.Accountbredcrumb,"Accounts Composite Orders Tab");
		       
		        verifyExists(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				click(IpVpnOrderObj.IpVpnOrder.NameFiltering,"Name Filtering");
				
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.FilterSelectName,"Name","Filter selection");
				
				sendKeys(IpVpnOrderObj.IpVpnOrder.FilterInputValue,IpvpnNCSiteProductOrdernumber);
				
				verifyExists(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");
				click(IpVpnOrderObj.IpVpnOrder.ApplyButton,"Apply Button");

		        waitForElementToAppear("@xpath=//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",120,20000);
		        String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ IpvpnNCSiteProductOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
		        if (OrderStatus.contains("Process Completed"))
				{
					Report.LogInfo("Validate","\""+IpvpnNCSiteProductOrdernumber +"\" Is Completed Successfully", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, IpvpnNCSiteProductOrdernumber +" Is Completed Successfully");
				}
				else if (OrderStatus.contains("Process Started"))
				{
					Report.LogInfo("Validate","\""+IpvpnNCSiteProductOrdernumber +"\" Execution InProgress", "INFO");
					ExtentTestManager.getTest().log(LogStatus.INFO, IpvpnNCSiteProductOrdernumber +" Execution InProgress");
				}
			}
			
			public void Completeworkitem(String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
			{
				String ResilienceOption = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "End Resilience Option");
				String AccessNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access NW Element");
				String AccessPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access port");
				String CPENNiPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
				String CPENNiPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
				String PeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
				String PEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 1");
				String PEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 2");
				String VcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend VCX Control");
				String Beacon = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Beacon");
				String L3cpeName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE Name");
				String L3cpeUniPort = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "L3 CPE UNI Port");
				String PrimarySR = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR");
				String PrimarySRGil = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Primary SR GIL");
				String WanIpv4SubnetSize = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Wan IPv4 Subnet Size");
				String DuplexBfdInterval = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Duplex/Bfd interval");
				String EgressPolicyMap = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Egress Policy Map");
				String SpeedMultiplier = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Speed/Multiplier");
				String PremiumCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Premium CIR%");
				String InternetCir = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Internet CIR%");
				String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");
				String WANVLANID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "WAN VLAN ID");
				String OVCCircuitRef = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OVCCircuitRef");
				
				System.out.println("In Switch case with TaskName :"+taskname[0]);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Start completion of Task: "+taskname[0]);
				switch(taskname[0])
				{
					case "Reserve Access Resources":
					if (ResilienceOption.contains("Protected"))
					{
						//Code for Protected
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");			
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AccessNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");			
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AccessPort);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.CPENNIPort1,"CPE NNI Port1");		
						sendKeys(IpVpnOrderObj.IpVpnOrder.CPENNIPort1,CPENNiPort1);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.CPENNIPort1, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");			
						sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,CPENNiPort2);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					else
					{
						//Code for Unprotected
						verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");			
						sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,AccessNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");				
						sendKeys(EthernetOrderObj.CompositOrders.AccessPort,AccessPort);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.CPENNIPort1,"CPE NNI Port1");				
						sendKeys(IpVpnOrderObj.IpVpnOrder.CPENNIPort1,CPENNiPort1);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.CPENNIPort1, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
					case "Transport Circuit Design":
					if (ResilienceOption.contains("Protected"))
					{
						//Code for Protected
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.PENetworkElement,"PE Network Element");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.PENetworkElement);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.PENetworkElement,PeNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.PENetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						Reusable.waitForAjax();
						
						if (DeviceType.contains("Stub"))
						{
							if (AccessNwElement.contains("SC"))
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.VCXController,"VCX Controller");
								clearTextBox(IpVpnOrderObj.IpVpnOrder.VCXController);
								Reusable.waitForpageloadmask();
								Reusable.waitForpageloadmask();				   
								   
								sendKeys(IpVpnOrderObj.IpVpnOrder.VCXController,VcxControl);
								Reusable.waitForAjax();
								Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.VCXController, Keys.ENTER);
								Reusable.waitForSiebelSpinnerToDisappear();
							}			
//										if (Beacon.contains("Beacon"))
//										{
//											verifyExists(IpVpnOrderObj.IpVpnOrder.Beacon,"Beacon");
//											clearTextBox(IpVpnOrderObj.IpVpnOrder.Beacon);
													   
								   
//											sendKeys(IpVpnOrderObj.IpVpnOrder.Beacon,Beacon);
//											Reusable.waitForAjax();
//											Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.Beacon, Keys.ENTER);
//											Reusable.waitForSiebelSpinnerToDisappear();
//										}
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();

						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
						click(IpVpnOrderObj.IpVpnOrder.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
						
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.PePort,"PE Port");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.PePort);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.PePort,PEPort1);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.PePort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						Reusable.waitForAjax();

						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
						click(IpVpnOrderObj.IpVpnOrder.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
						
						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.PePort,"PE Port");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.PePort);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.PePort,PEPort2);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.PePort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
						Reusable.waitForAjax();

						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					else
					{
		            	//Code for Unprotected
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.PENetworkElement,"PE Network Element");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.PENetworkElement);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.PENetworkElement,PeNwElement);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.PENetworkElement, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.PePort,"PE Port");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.PePort);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.PePort,PEPort1);
						Reusable.waitForAjax();
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.PePort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						if (DeviceType.contains("Stub"))
						{
							if (AccessNwElement.contains("SC"))
							{
								verifyExists(IpVpnOrderObj.IpVpnOrder.VCXController,"VCX Controller");
								clearTextBox(IpVpnOrderObj.IpVpnOrder.VCXController);
								Reusable.waitForpageloadmask();
								Reusable.waitForpageloadmask();
								
								sendKeys(IpVpnOrderObj.IpVpnOrder.VCXController,VcxControl);
								Reusable.waitForAjax();
								Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.VCXController, Keys.ENTER);
								Reusable.waitForSiebelSpinnerToDisappear();
							}
			
//										if (Beacon.contains("Beacon"))
//										{
//											verifyExists(IpVpnOrderObj.IpVpnOrder.Beacon,"Beacon");
//											clearTextBox(IpVpnOrderObj.IpVpnOrder.Beacon);
//											sendKeys(IpVpnOrderObj.IpVpnOrder.Beacon,Beacon);
//											Reusable.waitForAjax();
//											Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.Beacon, Keys.ENTER);
//											Reusable.waitForSiebelSpinnerToDisappear();
//										}
						}
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
						Reusable.waitForAjax();
						}
					}
					break;
					
					case "Reserve MPR Resources" :
					{
						verifyExists(IpVpnOrderObj.IpVpnOrder.L3cpeName,"L3CPE Name");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.L3cpeName);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.L3cpeName,L3cpeName,"L3 CPEName");
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.L3cpeName, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(IpVpnOrderObj.IpVpnOrder.L3cpeUNIPort,"L3CPE UNI Port");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.L3cpeUNIPort);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.L3cpeUNIPort,L3cpeUniPort);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.L3cpeUNIPort, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
					case "Verify SR Router and GIL":
					{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Asr,"ASR");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.Asr);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.Asr,PrimarySR);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.Asr, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.AsrGil,"ASR GIL");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.AsrGil);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						Reusable.waitForAjax();
						sendKeys(IpVpnOrderObj.IpVpnOrder.AsrGil,PrimarySRGil);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.AsrGil, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();
						Reusable.waitForAjax();

						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;

					case "Set/Validate Serial Number":
		            {
		                verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
		                click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

		                if (DeviceType.contains("Stub"))
		                {
		                    if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
		                    {
		                        verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
		                        click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
		                        sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
		                        Reusable.waitForAjax();
		                        
		                        verifyExists(NcOrderObj.createOrder.Update,"Update button");
		                        click(NcOrderObj.createOrder.Update,"Update button");
		                        Reusable.waitForAjax();
		                        
//		                        verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//		                        click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

		 

		                        verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		                        click(NcOrderObj.addProductDetails.Edit,"Edit button");
		                        Reusable.waitForAjax();
		                    }
		                    else
		                    {
		                        verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
		                        click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
		                        sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970","GxLtsSerialNo");
		                        Reusable.waitForAjax();
		                        
		                        verifyExists(NcOrderObj.createOrder.Update,"Update button");
		                        click(NcOrderObj.createOrder.Update,"Update button");
		                        Reusable.waitForAjax();
		                        
//		                        verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//		                        click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

		 

		                        verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
		                        click(NcOrderObj.addProductDetails.Edit,"Edit button");
		                        Reusable.waitForAjax();
		                    }
		                }
		                else
		                {
		                    if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
		                    {
		                        verifyExists(IpVpnOrderObj.IpVpnOrder.UpdateAntSerialNumber,"Update ANT Serial Number button");
		                        click(IpVpnOrderObj.IpVpnOrder.UpdateAntSerialNumber,"Update ANT Serial Number button");
		                    }
		                    else
		                    {
		                        verifyExists(IpVpnOrderObj.IpVpnOrder.UpdateSerialNumber,"Update Serial Number button");
		                        click(IpVpnOrderObj.IpVpnOrder.UpdateSerialNumber,"Update Serial Number button");
		                    }
		                }
		                Reusable.waitForAjax();
		                verifyExists(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
		                click(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
		                Reusable.waitForAjax();
		            }
		            break;			
					
					case "Set Bespoke Parameters":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						if(verifyExists(NcOrderObj.addProductDetails.Edit))
						{
							verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
							click(NcOrderObj.addProductDetails.Edit,"Edit button");
							Reusable.waitForAjax();
						}
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.WanIpSubnetSize,"WAN IPv4 Subnet Size");
						ScrollIntoViewByString(IpVpnOrderObj.IpVpnOrder.WanIpSubnetSize);
						selectByVisibleText(IpVpnOrderObj.IpVpnOrder.WanIpSubnetSize,WanIpv4SubnetSize,"WanIpv4SubnetSize");

						verifyExists(IpVpnOrderObj.IpVpnOrder.WanIpSubnetSizeFlag,"WAN IPv4 Subnet Size Flag");
						selectByVisibleText(IpVpnOrderObj.IpVpnOrder.WanIpSubnetSizeFlag,"Yes","WanIpSubnetSizeFlag");

						/*verifyExists(IpVpnOrderObj.IpVpnOrder.OtherWanIpSubnetSize,"Other WAN IPv4 Subnet Size");
						sendKeys(IpVpnOrderObj.IpVpnOrder.OtherWanIpSubnetSize,DuplexBfdInterval,"DuplexBfdInterval");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						ScrollIntoViewByString(NcOrderObj.createOrder.Update);
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();*/
						
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						//verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						//click(NcOrderObj.addProductDetails.Edit,"Edit button");
						//Reusable.waitForAjax();
						
						//verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						//click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						ScrollIntoViewByString(IpVpnOrderObj.IpVpnOrder.Complete);
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
					case "Select LAN IP Range":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(IpVpnOrderObj.IpVpnOrder.RequestNewIp,"Request New IP from EIP button");
						click(IpVpnOrderObj.IpVpnOrder.RequestNewIp,"Request New IP from EIP button");
						Reusable.waitForAjax();
					}
					break;
					
					case "Waiting for RIR Approval":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;

					case "Manual Design Task for MPR":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.EgressPolicyMap,"Egress Policy Map");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.EgressPolicyMap);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.EgressPolicyMap,EgressPolicyMap);
						Reusable.waitForAjax();
						Reusable.SendkeaboardKeys(IpVpnOrderObj.IpVpnOrder.EgressPolicyMap, Keys.ENTER);
						Reusable.waitForSiebelSpinnerToDisappear();

						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.TransComplete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
			        
					case "Load License to MPR":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
			        
					case "Check Order parameters":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
			    	case "New Mngm Network Connection Activation":
			    	{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
			    	}
			    	break;
			    	
			    	case "Activation Start Confirmation":
			    	{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
			    	}
			    	break;
			    	
					case "Confirm L3 Activation Start":
			    	{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
			    	}
					break;

					case "MPR Trigger Configuration Start":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;

					case "Duplex Check":
			    	{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
			    	}
					break;

					case "Speed Test":
			    	{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
			    	}
					break;

					case "Remote Connectivity Test":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;

					case "Customer Dynamic Routing Test":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
			        break;

					case"Set Bandwidth Distribution Parameters":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						sendKeys(IpVpnOrderObj.IpVpnOrder.PremiumCir,PremiumCir);
						sendKeys(IpVpnOrderObj.IpVpnOrder.InternetCir,InternetCir);
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
					case "Bandwidth Profile Confirmation":
					{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
					case "Check BGP":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
			        break;

					case "Service Test":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
					case "Legacy Activation Completed":
					{
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
					}
					break;
					
					case "Select OLO NNI Profile":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.OLONNIProfile,"Task title link");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.OLONNIProfile);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.OLONNIProfile,WANVLANID,"Task title link");
						Reusable.waitForAjax();	
						keyPress(IpVpnOrderObj.IpVpnOrder.OLONNIProfile,Keys.ENTER);
						Reusable.waitForAjax();	
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Completed OLO NNI Profile");						
					}
					break;

					case "Select OVC Circuit":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						verifyExists(IpVpnOrderObj.IpVpnOrder.OVCCircuit,"Task title link");
						clearTextBox(IpVpnOrderObj.IpVpnOrder.OVCCircuit);
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						Reusable.waitForpageloadmask();
						sendKeys(IpVpnOrderObj.IpVpnOrder.OVCCircuit,OVCCircuitRef,"OVCCircuitRef");
						Reusable.waitForAjax();	
						keyPress(IpVpnOrderObj.IpVpnOrder.OVCCircuit,Keys.ENTER);
						Reusable.waitForAjax();	
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();	
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Completed OVC Circuit");						
					}
					break;
					
					case "Check LAN IP Address":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						
						String IPCheck = IpVpnOrderObj.IpVpnOrder.LanIpAddress;
						//Log.info(IPCheck.get());
						if (IPCheck != null)
						{
							verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
							click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
							Reusable.waitForAjax();
							ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Completed Check LAN IP");
						}
						else 
						{
							System.out.println("No IP address found");
						}
					}
					break;
					
					case "Release\\Reserve LAN Range":
					{
						verifyExists(IpVpnOrderObj.IpVpnOrder.ReleaseLanRange,"ReleaseLanRange");
						selectByVisibleText(IpVpnOrderObj.IpVpnOrder.ReleaseLanRange,"Yes","ReleaseLanRange");
						Reusable.waitForAjax();
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Completed Release\\Reserve LAN Range");
					}
					break;
					
					case "Common Manual Testing Task":
					{
						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
						verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
						Reusable.waitForAjax();
						ExtentTestManager.getTest().log(LogStatus.PASS, "Step: Completed Common Manual Testing");
					}
					
					default:
					// No case found that it will complete the Workitem	
					verifyExists(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
					click(IpVpnOrderObj.IpVpnOrder.Complete,"Complete Workitem");
					Reusable.waitForAjax();
				}
			}
			
			public void CreateEthernetLink(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
			{
				
				String CityCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"A end Site ID");
				String Service_bandwidth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"SerBand");
				String OVC_Name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OVC NAME");
				String carrier_name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"carrier name");
				String Port_name = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Port name");
				String CeosRefNumber=null;
				
				String manCityCode=CityCode.trim().substring(0, 3);
				String circuitName=manCityCode+"/"+manCityCode+"/ES-"+String.valueOf(Math.round(Math.random() * 1000000));
				mouseMoveOn(IpVpnOrderObj.IpVpnOrder.Topnavegation);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Top Navigation link");
				
				click(IpVpnOrderObj.IpVpnOrder.Submenuinventory,"Submenuinventory");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Top Inventory link");
				click(IpVpnOrderObj.IpVpnOrder.COLTInventoryProject,"COLTInventoryProject");		
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on COLT Inventory Project Link");
				click(IpVpnOrderObj.IpVpnOrder.Circuits,"Circuits");	
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Circuits Link");
				click(IpVpnOrderObj.IpVpnOrder.EthernetCircuitFolder,"EthernetCircuitFolder");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Ethernet Circuit Folder Link");
				click(IpVpnOrderObj.IpVpnOrder.NewCircuit,"NewCircuit");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on New Circuitr Link");
				clearTextBox(IpVpnOrderObj.IpVpnOrder.Circuitname);
				sendKeys(IpVpnOrderObj.IpVpnOrder.Circuitname,circuitName,"Circuits"); //auto generate 
				ExtentTestManager.getTest().log(LogStatus.PASS, " Steps: Enter the Circuit Name");
				System.out.println("Circuit Name:"+circuitName);
				ExtentTestManager.getTest().log(LogStatus.PASS,"Circuit Name:"+circuitName);
				clearTextBox(IpVpnOrderObj.IpVpnOrder.BandwidthEtherlink);

				verifyExists(IpVpnOrderObj.IpVpnOrder.BandwidthEtherlink,"BandwidthEtherlink");
				sendKeys(IpVpnOrderObj.IpVpnOrder.BandwidthEtherlink,Service_bandwidth,"Service_bandwidth");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Steps: Enter the Bandwidth");

				verifyExists(IpVpnOrderObj.IpVpnOrder.comboarrowtype,"comboarrowtype");
				click(IpVpnOrderObj.IpVpnOrder.comboarrowtype,"comboarrowtype");
				click(IpVpnOrderObj.IpVpnOrder.comboarrowtypeplus,"comboarrowtypeplus");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				click(IpVpnOrderObj.IpVpnOrder.EthernetLink,"EthernetLink");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Ethernet link");
				click(IpVpnOrderObj.IpVpnOrder.Createbutton,"Createbutton");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on Create button");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				click(IpVpnOrderObj.IpVpnOrder.Parameterstab,"Parameterstab");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Parameters tab");
				click(IpVpnOrderObj.IpVpnOrder.Edit,"Edit");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit");
				selectByVisibleText(IpVpnOrderObj.IpVpnOrder.LogicalStatus,"In Service","LogicalStatus");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Logical Status");
				
				if(!OVC_Name.equalsIgnoreCase(""))
				{
					sendKeys(IpVpnOrderObj.IpVpnOrder.Parentovcname,OVC_Name,"Parent ovcname");
				}
				else{
					sendKeys(IpVpnOrderObj.IpVpnOrder.Parentovcname,CeosRefNumber,"Parent ovcname");
				}
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Parent OVC name");
				sendKeys(IpVpnOrderObj.IpVpnOrder.Overbookingfactor,"100","Overbookingfactor");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Overbooking Factor");
				
				click(IpVpnOrderObj.IpVpnOrder.Update,"Update");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on update");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();	
				click(IpVpnOrderObj.IpVpnOrder.EthernetLinkPathElements,"EthernetLinkPathElements");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: click on Ethernet Link Path Elements tab");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();		
				click(IpVpnOrderObj.IpVpnOrder.AddEthernetEndPoint,"AddEthernetEndPoint");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: click on AddEthernetEndPoint");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();			
				clearTextBox(IpVpnOrderObj.IpVpnOrder.Carrier);
				sendKeys(IpVpnOrderObj.IpVpnOrder.Carrier,carrier_name,"Carrier");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				keyPress(IpVpnOrderObj.IpVpnOrder.Carrier,Keys.ENTER);
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Steps: Enter the BCarrier");
				click(IpVpnOrderObj.IpVpnOrder.Createbutton,"Createbutton");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on Create button");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();	
				click(IpVpnOrderObj.IpVpnOrder.SelectResource,"SelectResource");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on Select Resource link");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();	
				sendKeys(IpVpnOrderObj.IpVpnOrder.SelectResourcetext,Port_name,"SelectResourcetext"); 
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				keyPress(IpVpnOrderObj.IpVpnOrder.SelectResourcetext,Keys.ENTER);
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();
				click(IpVpnOrderObj.IpVpnOrder.SelectResourcebutton,"SelectResourcebutton");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Steps: Select Resource");
				Reusable.waitForpageloadmask();
				Reusable.waitForAjax();		
				System.out.println("createEthernetLink method completed!");
			}

}