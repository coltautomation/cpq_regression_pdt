package testHarness.nmtsFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import java.text.SimpleDateFormat;  


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nmtsObjects.NMTSAddNumberObj;
import pageObjects.nmtsObjects.NMTSCancelNumberReservationObj;
import pageObjects.nmtsObjects.NMTSNumberActivationFreeNumberObj;
import pageObjects.nmtsObjects.NMTSNumberActivationReservedNumberObj;
import pageObjects.nmtsObjects.NMTSNumberReservationObj;
import pageObjects.nmtsObjects.NMTSPortOutNumberObj;
import pageObjects.nmtsObjects.NMTSSplitNumberObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NMTSCancelNumberReservationPage extends SeleniumUtils {
	
	ReusableFunctions Reusable = new ReusableFunctions();
	NMTSAddNumberPage AddNumberRange = new NMTSAddNumberPage();
	NMTSNumberReservationPage NumberReservation = new NMTSNumberReservationPage();
	
	String successmsg;
	
	private static HashMap<String, String> Number= new HashMap<String, String>();

	
	public void CancelReservationNumber(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String MadeByName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MADE_BY_NAME");
		String Availablity = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Availablity");
		String FilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_By");
		String ProcessingDecision = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Processing_Decision");
		String RejectReason = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Reject_Reason");
		String MRN = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"MRN/NRN");
		String MatchingWithFilterBy = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Matching_With_FilterBy");
		String ReservedNumber_OR_NMTSID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"ReservedNumber/NMTS_ID");

		
				NumberReservation.UserGroupforPhnNoMgn(testDataFile, sheetName, scriptNo, dataSetNo);
				Reusable.switchtodefault();
				waitForAjax();
				Reusable.switchtoframe("fmMenu");		
				waitForAjax();
				waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.ProcessReservationtab,15);
				verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.ProcessReservationtab,"ProcessReservationtab");
				javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.ProcessReservationtab,"ProcessReservationtab");
				WaitForAjax();
				Reusable.switchtodefault();
			    WaitForAjax();
				Reusable.switchtoframe("fmMain");
				WaitForAjax();
				if(Country.toString().equals("IT") || Country.toString().equals("FR"))
				{   
					verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.SelectReservationColt,"Colt");
					Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.SelectReservationColt,"Colt");
					WaitForAjax();
				}
				waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.madeByNMTS,15);
				verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.madeByNMTS,"made By");
				Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.madeByNMTS,MadeByName);
				waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.Availablity,10);
				verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.Availablity,"Availablity");
				Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.Availablity,Availablity);
				verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.FilterBy,"Filter By");
				Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.FilterBy,FilterBy);
				
				waitForAjax(); 
				verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.matching,"Matching With Filter By");
				Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.matching,MatchingWithFilterBy);
			
				waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.FilterText,15);
				verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.FilterText,"Filter input");
				sendKeys(NMTSCancelNumberReservationObj.CancelResNumber.FilterText,ReservedNumber_OR_NMTSID,"Filter input data");
				//sendKeys(NMTSCancelNumberReservationObj.CancelResNumber.FilterText,"3138815","Filter input data");
			    waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.SubmitSearchbtn,15);
				verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.SubmitSearchbtn,"Submit Deactivate Button");
			//	javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.ProcessReservationtab,"ProcessReservationtab");
				javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.SubmitSearchbtn,"Submit Deactivate Button");
				waitForAjax();
		
				if(isElementPresent(By.xpath("//table[@id='ucxOrderHistory_grdView']//tr/td[contains(text(),'No Record Found.')]")))
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "No record found please enter the valid data number");	
			
				} 
				else {
						waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.OrderId,15);
						verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.OrderId,"OrderId");
					    javaScriptDoubleclick(NMTSCancelNumberReservationObj.CancelResNumber.OrderId,"OCN double click");
						
						waitForAjax();
						waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.SelectReservedRangeCheckbox,15);
						verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.SelectReservedRangeCheckbox,"Reserved Range Checkbox");
						javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.SelectReservedRangeCheckbox,"Reserved Range Checkbox");
						
					//	WaitForAjax();
						verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.ReservedRangeSelectMarkedLink,"Reserved Range Select Marked Link");
						javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.ReservedRangeSelectMarkedLink,"Reserved Range Select Marked Link");
					//	WaitForAjax();
						
						waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.SelectedRangeCheckbox,15);
						verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.SelectedRangeCheckbox,"Selected Range checkbox");
						javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.SelectedRangeCheckbox,"Selected Range checkbox");
						
						if(Country.toString().equals("CH")||Availablity.toString().equals("COLT")||Country.toString().equals("DE NON-GEO"))
						{
							waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.selectDecision,15);
							verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.selectDecision,"Processing Decision");
							Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.selectDecision, ProcessingDecision);
							//waitForAjax();
							waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.RejectReason,15);
							verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.RejectReason,"Reject Reason");
							Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.RejectReason, RejectReason);
							waitForAjax();
							waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.SubmitDecision,15);
							verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.SubmitDecision,"Submit Decision");
							javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.SubmitDecision,"Submit Decision");
							//waitForAjax();
						} 
						else {		
							waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.selectDecision,15);
							verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.selectDecision,"Processing Decision");
							Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.selectDecision, ProcessingDecision);
							//waitForAjax();
							waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.RejectReason,15);
							verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.RejectReason,"Reject Reason");
							Reusable.Select(NMTSCancelNumberReservationObj.CancelResNumber.RejectReason, RejectReason);
							waitForAjax();
							waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.MRN,15);
							ScrollIntoViewByString(NMTSCancelNumberReservationObj.CancelResNumber.MRN);
							verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.MRN,"MRN");
							sendKeys(NMTSCancelNumberReservationObj.CancelResNumber.MRN,MRN.toString(),"MRN");
							waitForAjax();
							waitForElementToBeVisible(NMTSCancelNumberReservationObj.CancelResNumber.SubmitDecision,15);
							verifyExists(NMTSCancelNumberReservationObj.CancelResNumber.SubmitDecision,"Submit Decision");
							javaScriptclick(NMTSCancelNumberReservationObj.CancelResNumber.SubmitDecision,"Submit Decision");
						}
							waitForAjax();
							
							successmsg = getTextFrom(NMTSCancelNumberReservationObj.CancelResNumber.successmsg);
							if(successmsg.contains("NMTS Order Cancelled")){
								   ExtentTestManager.getTest().log(LogStatus.PASS, "Number reservation Cancelled successfully");
								}
							else
							    {
									ExtentTestManager.getTest().log(LogStatus.FAIL, "Number reservation not Cancelled successfully");	
								}
						
					
	             }
	}
}

	
	

