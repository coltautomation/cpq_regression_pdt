package testHarness.ncFunctions;


import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import pageObjects.ncObjects.NewHubOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class HubOrder extends SeleniumUtils
{
	public static ThreadLocal<String> SpokeOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> HubOrderscreenurl = new ThreadLocal<>();
	public static ThreadLocal<String> Orderscreenurl = new ThreadLocal<>();

	ReusableFunctions Reusable=new ReusableFunctions();
	
    public void CreatHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
    	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NC_Order_No", Ordernumber);
		
		verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"NewCompositeOrder");
		click(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"NewCompositeOrder");
		String OrderDesc="Hub Order Created using Automation script";
		String[] arrOfStr = Ordernumber.split("#", 0); 
		Reporter.log(arrOfStr[1]);
		
		verifyExists(NewHubOrderObj.NewHubOrder.OrderDescription,"Order Description");
		sendKeys(NewHubOrderObj.NewHubOrder.OrderDescription,OrderDesc,"Order Description");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
		click(NewHubOrderObj.NewHubOrder.Update,"Update");
		
		Reporter.log(Orderscreenurl.get());
	}

    public void AddHubProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
	{
		String productValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Product");
		
		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		
		verifyExists(NewHubOrderObj.NewHubOrder.HubProductCheckBox,"Hub Option");
		click(NewHubOrderObj.NewHubOrder.HubProductCheckBox,"Hub Option");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Addbutton,"Add Button");
		click(NewHubOrderObj.NewHubOrder.Addbutton,"Add Button");
		
		Reusable.waitForAjax();
	}
	
    public void AddHubProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    {
    	String HubResiliencOption=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Resilience Option");
    	String Oderreferencenumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Sys Ref ID");
    	String OrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order Number");
    	String NetworkRef=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Network Ref");
    	String Topology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
    	String CommercialProductName=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Commercial Product Name");
    	String HubBandwidth=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "SerBand");
    	
			
		verifyExists(NewHubOrderObj.NewHubOrder.UnderlyningHubOrder,"Under Lying Hub Order");
		click(NewHubOrderObj.NewHubOrder.UnderlyningHubOrder,"Under Lying Hub Order");
		
		verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
		click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");

		verifyExists(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
    	click(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
    	Reusable.waitForSiebelLoader();
    	selectByVisibleText(EthernetOrderObj.CompositOrders.OrderSystemName,"SPARK","Order System Name");
    	
    	verifyExists(EthernetOrderObj.CompositOrders.Orderreferencenumber,"Orderreferencenumber");
    	sendKeys(EthernetOrderObj.CompositOrders.Orderreferencenumber,Oderreferencenumber,"Orderreferencenumber");	
    	
    	verifyExists(NewHubOrderObj.NewHubOrder.NetworkReference,"Network Reference Number");
    	sendKeys(NewHubOrderObj.NewHubOrder.NetworkReference,NetworkRef,"Network Reference Number");
    	
//    	if(HubResiliencOption.equalsIgnoreCase("Protected"))
//    	{
			verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"Hub Resilience Option");
			click(EthernetOrderObj.CompositOrders.ResilienceOption,"Hub Resilience Option");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,HubResiliencOption,"Hub Resilience Option");
//		}
//    	else	
//    	{
//			verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
//			click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
//			Reusable.waitForSiebelLoader();
//			selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,HubResiliencOption,"Order System Name");
//    	}
    	
    	verifyExists(NewHubOrderObj.NewHubOrder.Topology,"Topology");
    	click(NewHubOrderObj.NewHubOrder.Topology,"Topology");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(NewHubOrderObj.NewHubOrder.Topology,Topology,"Topology");
    	
		verifyExists(EthernetOrderObj.CompositOrders.Ordernumber,"Ordernumber");
		sendKeys(EthernetOrderObj.CompositOrders.Ordernumber,OrderNumber,"Ordernumber");	
    	
		verifyExists(EthernetOrderObj.CompositOrders.CommercialProductName,"CommercialProductName");
		sendKeys(EthernetOrderObj.CompositOrders.CommercialProductName,CommercialProductName,"CommercialProductName");	
			
		verifyExists(EthernetOrderObj.CompositOrders.ServiceBandwidth,"Service Bandwidth");
		sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,HubBandwidth,"Service Bandwidth");
		Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth, Keys.ENTER);
		
		verifyExists(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		click(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
		Reusable.waitForSiebelLoader();
		selectByVisibleText(EthernetOrderObj.CompositOrders.CircuitCategory,"LE","Circuit Category");
		
		Reusable.waitForSiebelLoader();
		
		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
		click(NewHubOrderObj.NewHubOrder.Update,"Update");
		waitForAjax();
		//Required Details are Updated
	 }

    public void DecomposeHubOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
    {
    	String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

 		getUrl(Orderscreenurl);
		
		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
		click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");

		verifyExists(NewHubOrderObj.NewHubOrder.HubSuborder,"Hub Sub order");
		click(NewHubOrderObj.NewHubOrder.HubSuborder,"Hub Sub order");
	
		verifyExists(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
		click(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
		waitForAjax();
    }
     	 
     	public void HubProductDeviceDetails(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws IOException, InterruptedException 
    	{ 
    		String HubAccessTechnolgy=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AEnd Access Technology");
    		String HubAccessType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "AEnd Access type");
    		String HubSiteID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Site ID");
    		String HubCabinetID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Cabinet ID");
    		String HubCabinetType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Cabinet Type");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.HubSiteProductend,"New Hub Site Product End");
    		click(NewHubOrderObj.NewHubOrder.HubSiteProductend,"New Hub Site Product End");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit");
    		click(EthernetOrderObj.CompositOrders.Edit,"Edit");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Access Technology");
    		click(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Hub Access Technology");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessTechnolgy,HubAccessTechnolgy,"Hub Access Technology");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessType,"Access Type");
    		click(EthernetOrderObj.CompositOrders.AccessType,"Hub Access Type");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessType,HubAccessType,"Hub Access Type");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.SiteID,"Site ID");
    		sendKeys(NewHubOrderObj.NewHubOrder.SiteID,HubSiteID,"Site ID");		
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Update,"details are Updated");
    		click(EthernetOrderObj.CompositOrders.Update,"details are Updated");
    		
    	    waitForAjax();

    		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
    		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformationTab");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
    		click(EthernetOrderObj.CompositOrders.AccessPortLink,"AccessPortLink");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
    		click(EthernetOrderObj.CompositOrders.Edit,"Edit butoon");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
    		click(EthernetOrderObj.CompositOrders.PresentConnectType,"Hub Presentation Connector Type");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,"LC/PC","Hub Presentation Connector Type");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
    		click(EthernetOrderObj.CompositOrders.AccessportRole,"Hub Port Role");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,"Physical Port","Hub Port Role");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
    		click(EthernetOrderObj.CompositOrders.Update,"Update");

    		Reusable.waitForAjax();
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
    		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accountbredcrumb");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
    		click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"GeneralInformation Tab");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
    		click(EthernetOrderObj.CompositOrders.CPElink,"CPE link");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit");
    		click(EthernetOrderObj.CompositOrders.Edit,"Edit");
    	
    		verifyExists(NewHubOrderObj.NewHubOrder.CabinateID,"Cabinate ID");
    		sendKeys(NewHubOrderObj.NewHubOrder.CabinateID,HubCabinetID,"Cabinate ID");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
    		click(EthernetOrderObj.CompositOrders.CabinetType,"Cabinet Type");
    		Reusable.waitForSiebelLoader();
    		selectByVisibleText(EthernetOrderObj.CompositOrders.CabinetType,HubCabinetType,"Cabinet Type");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
    		click(EthernetOrderObj.CompositOrders.Update,"Update");
    	    waitForAjax();
    	}
    	
     	public void ProcessHubOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
    	{
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number", Ordernumber);
     		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");

     		getUrl(Orderscreenurl);
    		//Navigate to Hub Order screen
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
			String[] arrOfStr = Ordernumber.split("#", 0);
    		click(NewHubOrderObj.NewHubOrder.hubo1+arrOfStr+NewHubOrderObj.NewHubOrder.hubo2);
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Proccessing");
		 	click(NewHubOrderObj.NewHubOrder.StartProccessing,"Start Proccessing");	
    	}
     	
     	public void CompleteHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException,AWTException, IOException
    	{
    		String Order_Number = null;
    		Order_Number = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_No");
    		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
    		String WorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Work Items");

//    		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
//    		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
    		
    		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
    		
    		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,Order_Number);
    		
    		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
    		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

    		String arrOfStr = EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrOfStr2;
    		verifyExists(arrOfStr,"Composite Order");
    		click(arrOfStr,"Composite Order");
    	
//    		getUrl(Orderscreenurl);
    		
    		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
    		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
    		click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
    		
    		waitForAjax();
    	
    		verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
    		click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
    		
    		waitForAjax();
    		
    		
    		for (int k=1; k<=Integer.parseInt(WorkItems);k++)
    		{
    			waitForElementToAppear(EthernetOrderObj.CompositOrders.TaskReadytoComplete,90,10000);
    			click(EthernetOrderObj.CompositOrders.TaskReadytoComplete,"Workitem in Ready status");
    			//need to verify
    			CompletHubworkitem(GetText(NcOrderObj.taskDetails.TaskTitle),testDataFile, sheetName, scriptNo,dataSetNo);
    		}
    		
    		waitForAjax();
    		verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
		 	click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.Workitems,"Workitems");
		 	click(NewHubOrderObj.NewHubOrder.Workitems,"Workitems");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.WorkItemSelect,"Work Item Select");
		 	click(NewHubOrderObj.NewHubOrder.WorkItemSelect,"Work Item Select");
    		
		 	verifyExists(NewHubOrderObj.NewHubOrder.View,"View");
		 	click(NewHubOrderObj.NewHubOrder.View,"View");
    		
		 	String NCServiceId=getTextFrom(NewHubOrderObj.NewHubOrder.NCSID);
		 	DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub_NCS_ID", NCServiceId);
		 	
    		getUrl(Orderscreenurl);
    		
    		verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
    		click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
    		
    		verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
    		click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
    		
    		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrorder1,180,20000);
    	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ Order_Number + EthernetOrderObj.CompositOrders.arrorder1);
    		if (OrderStatus.contains("Process Completed"))
    		{
    			Report.LogInfo("Validate","\""+Order_Number +"\" Is Completed Successfully", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, Order_Number +" Is Completed Successfully");
    		}
    		else if (OrderStatus.contains("Process Started"))
    		{
    			Report.LogInfo("Validate","\""+Order_Number +"\" Execution InProgress", "INFO");
    			ExtentTestManager.getTest().log(LogStatus.INFO, Order_Number +" Execution InProgress");
    		}
			
    	}
     	
     	public void CompletHubworkitem(String[] taskTitle, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
    	{
     		String HubResiliencOption=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Resilience Option");
    		String HubAccessNWElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access NW Element");
    		String HubAccessport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end Access port");
    		String HubCPENNITrunkPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 1");
    		String HubCPENNITrunkPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "A end CPE NNI/Trunk Port 2");
    		String HubPeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE NW Element");
    		String HubPEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 1");
    		String HubPEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend PE Port 2");
    		String HubVcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend VCX Control");
    		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");
    	
     		System.out.println("In Switch case with TaskName :"+taskTitle);
    		switch(taskTitle[0])
    		{
			case "Reserve Access Resources":
			if (HubResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,HubAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,HubAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,HubCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,HubCPENNITrunkPort2);
				waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
			}
			else
			{
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,HubAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,HubAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,HubCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
//				workitemcounter.set(workitemcounter.get()+1);
			}
			break;
			
			case "Transport Circuit Design":
			if (HubResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,HubPeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForAjax();
				
				if (DeviceType.contains("Stub"))
				{
					if (HubAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,HubVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
						sendKeys(NcOrderObj.workItems.VCXController,HubVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
					}			
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,HubPEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,HubPEPort2);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
            	//Code for Unprotected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,HubPeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,HubPEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForAjax();

				if (DeviceType.contains("Stub"))
				{
					if (HubAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,HubVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
						sendKeys(NcOrderObj.workItems.VCXController,HubVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
					}
	
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
					
			case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
    		}
    	}
    	 
    	public void GotoHubErrors(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
    		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
    		String Orderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_URL");
    		
    		String[] arrOfStr = Ordernumber.split("#", 0);
    		String Errors = getTextFrom(NewHubOrderObj.NewHubOrder.Errors1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.Errors2);

    		
	   		getUrl(Orderscreenurl);
	   		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
   		//Navigate to Accounts Composite Orders Tab
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
	   		if(Errors.equalsIgnoreCase("Blocked by Errors"))
	   		{
	   			
	   			verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
	   			click(NewHubOrderObj.NewHubOrder.CompositeOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.CompositeOrder2,"Composite order");
	   			
	   			verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
	   			click(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
	   		
	   			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
	   			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
	   			waitForAjax();
	   			
	   			verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
	   			click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
	   			waitForAjax();
	   		}
	   		else 
	   		{
	   			Reporter.log("Not required to Navigate to Errors tab");
	   			//Hub Order did not have any errors to be Captured
	   		}
	   	}

    	public void CreateSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException 
    	{
    		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
    				
    		waitForElementToAppear(NcOrderObj.createOrder.NewCompositeOrder, 10);
    		verifyExists(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
    		click(NcOrderObj.createOrder.NewCompositeOrder,"New Composite Order");
    		Reusable.waitForAjax();
    		
    		String SpokeOrdernumber = getTextFrom(NcOrderObj.createOrder.OrderNumber);
    		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke_Order_No", SpokeOrdernumber);
    		Reusable.waitForAjax();
    		String[] arrOfStr = SpokeOrdernumber.split("#", 0);
//    		Log.info(arrOfStr[1]);
    		
    		sendKeys(NcOrderObj.createOrder.OrderDescription, "Ethernet Spoke order created using CTAF Automation");
    		
    		verifyExists(NcOrderObj.createOrder.Update,"Update");
    		click(NcOrderObj.createOrder.Update,"Update");
    		
    		String SpokeOrderscreenurl=getAttributeFrom(EthernetOrderObj.CompositOrders.LinkforOrder,"href");
    		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke_Order_URL", SpokeOrderscreenurl);
    		System.out.println(SpokeOrderscreenurl);
    		Reusable.waitForAjax();
    	}
     	 
    	public void AddSpokeProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	    {		
		 	verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
			
			verifyExists(NewHubOrderObj.NewHubOrder.AddonOrderTab,"Addon Order Tab");
			click(NewHubOrderObj.NewHubOrder.AddonOrderTab,"Addon Order Tab");
			
			verifyExists(NewHubOrderObj.NewHubOrder.EthernetProductCheckBox,"Ethernet Product CheckBox");
			click(NewHubOrderObj.NewHubOrder.EthernetProductCheckBox,"Ethernet Product CheckBox");

			verifyExists(NewHubOrderObj.NewHubOrder.Addbutton,"Add button");
			click(NewHubOrderObj.NewHubOrder.Addbutton,"Add button");
			
			Reusable.waitForAjax();
	    }

    	public void AddSpokeProductdetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
	   	 	String SpokeOrderRef=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Sys Ref ID");
	   		String SpokeTopology=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Topology");
	   	 	String SpokeOrderNumber=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
	   	 	String SpokeCommercialProduct=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke Commercial Product");
	   	 	String SpokeBandwidth=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke Bandwidth");
	   	 	String CircuitCategory="LE";

//	   	 	String SpokeOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
//    		getUrl(SpokeOrderscreenurl);
	   	 	
			verifyExists(NewHubOrderObj.NewHubOrder.UnderlyningOrder,"Under Lying Order");
			click(NewHubOrderObj.NewHubOrder.UnderlyningOrder,"Under Lying Order");
			
			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
	
			verifyExists(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
			click(EthernetOrderObj.CompositOrders.OrderSystemName,"OrderSystemName");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.OrderSystemName,"SPARK","Order System Name");
	
	   		verifyExists(NewHubOrderObj.NewHubOrder.Orderreferencenumber,"Order reference number");
	   		sendKeys(NewHubOrderObj.NewHubOrder.Orderreferencenumber,SpokeOrderRef,"Order reference number");
	
			verifyExists(EthernetOrderObj.CompositOrders.Topology,"Topology");
			click(EthernetOrderObj.CompositOrders.Topology,"Topology");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.Topology,SpokeTopology,"Topology");
	   		
			verifyExists(EthernetOrderObj.CompositOrders.Ordernumber,"Order number");
			sendKeys(EthernetOrderObj.CompositOrders.Ordernumber,SpokeOrderNumber,"Order number");	
	   		
			verifyExists(EthernetOrderObj.CompositOrders.CommercialProductName,"Commercial Product Name");
			sendKeys(EthernetOrderObj.CompositOrders.CommercialProductName,"ETHERNET SPOKE","Commercial Product Name");	
			
			verifyExists(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
			click(EthernetOrderObj.CompositOrders.CircuitCategory,"Circuit Category");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.CircuitCategory,CircuitCategory,"Circuit Category");
			
			verifyExists(EthernetOrderObj.CompositOrders.ServiceBandwidth,"ServiceBandwidth");
			sendKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth,SpokeBandwidth,"ServiceBandwidth");
			Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.ServiceBandwidth, Keys.ENTER);
	   		
			Reusable.waitForAjax();
			
	   		verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
	   		click(NewHubOrderObj.NewHubOrder.Update,"Update");
    	}

    	public void AddSpokeFeature(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    	{
		  	String feature="End Site Product";
	
//	   	 	String SpokeOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
//    		getUrl(SpokeOrderscreenurl);

    		verifyExists(EthernetOrderObj.CompositOrders.GeneralInformationTab," General Information Tab");
			click(EthernetOrderObj.CompositOrders.GeneralInformationTab,"General Information Tab");
			
			waitForAjax();
			
			verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			
		    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,feature,"Product Display");
		    Thread.sleep(30000);
		    Reusable.waitForAjax();
			click(EthernetOrderObj.CompositOrders.EndSite,"End Site Product Select");
		    
			verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			
			waitForAjax();
    	}
	  
    	public void DecomposeSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
     	{
    		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
    		
    		getUrl(SpokeOrderscreenurl);
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Orders Tab");
			click(NewHubOrderObj.NewHubOrder.OrderTab,"Orders Tab");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Suborder,"Suborder");
			click(NewHubOrderObj.NewHubOrder.Suborder,"Suborder");
     		
     		verifyExists(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
			click(NewHubOrderObj.NewHubOrder.Decompose,"Decompose");
     		
     		waitForAjax();
     	}
     	 
    	public void AddHub(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException
    	{
    		String NCServiceId = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hub_NCS_ID");
    		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");

    		getUrl(SpokeOrderscreenurl);

    		verifyExists(NewHubOrderObj.NewHubOrder.NewEthernetConnProduct,"Ethernet Connection Product Order");
    		click(NewHubOrderObj.NewHubOrder.NewEthernetConnProduct,"Ethernet Connection Product Order");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AddHub,"Add Hub button");
    		click(NewHubOrderObj.NewHubOrder.AddHub,"Add Hub button");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.TypeofHub,"Service Id");
    	    sendKeys(NewHubOrderObj.NewHubOrder.TypeofHub,NCServiceId); 
    		waitForAjax();
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.AddHubProdOrder,"Hub Order");
    		click(NewHubOrderObj.NewHubOrder.AddHubProdOrder,"Hub Order");
    		
    		verifyExists(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
    		click(NewHubOrderObj.NewHubOrder.SelectFeature,"Select Feature");
    		waitForAjax();
//    		getUrl(SpokeOrderscreenurl);
    		
    	}
    	
   	 public void SpokeProductDeviceDetails(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException
   		{ 
   			String HubPortRole=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend Port Role");
   			String HubVLanTaggingMode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend Vlan Tagging Mode");
   			String HubVLanTagId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Aend Vlan Tag Id");
   			String SpokeAccessTechnolgy=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BEnd Access Technology");
   			String SpokeAccessType=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "BEnd Access type");
   			String SpokeResilienceOption=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Resilience Option");
   			String SpokeSiteID=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Site ID");
   			String SpokePortRole=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend Port Role");
   			String SpokeVLanTaggingMode=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend Vlan Tagging Mode");
   			String SpokeVLanTagId=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend Vlan Tag Id");
    		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
   			//String PresentConnectTypeValue=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "");
    		
    		getUrl(SpokeOrderscreenurl);
   		 
   		 	verifyExists(NewHubOrderObj.NewHubOrder.NewEndSiteProductAend,"A end Site Product Order");
   			click(NewHubOrderObj.NewHubOrder.NewEndSiteProductAend,"A end Site Product Order");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information tab");
   			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information tab");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port link");
   			click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port link");
   		
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   		
   			verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
   			click(EthernetOrderObj.CompositOrders.PresentConnectType,"A End Presentation Connector Type");
   			Reusable.waitForSiebelLoader();
   			selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,"LC/PC","A End Presentation Connector Type");
   			
   			verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
   			click(EthernetOrderObj.CompositOrders.AccessportRole,"A End port Role");
   			Reusable.waitForSiebelLoader();
   			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,HubPortRole,"A End Port Role");
   			
   			verifyExists(EthernetOrderObj.CompositOrders.VlanTaggingMode,"VLAN Tagging Mode");
   			click(EthernetOrderObj.CompositOrders.VlanTaggingMode,"A End VLAN Tagging Mode");
   			Reusable.waitForSiebelLoader();
   			selectByVisibleText(EthernetOrderObj.CompositOrders.VlanTaggingMode,HubVLanTaggingMode,"A End VLAN Tagging Mode");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   			click(NewHubOrderObj.NewHubOrder.Update,"Update");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   			click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			waitForAjax();
   			
			String typeoffeature="VLAN";
			verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
			
		    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,typeoffeature,"Product Display");
		    Reusable.waitForAjax();
			click(EthernetOrderObj.CompositOrders.Vlan,"Select VLAN");
		    
			verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
			
			Reusable.waitForAjax();
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN link");
   			click(NewHubOrderObj.NewHubOrder.VLANLink,"VLAN link");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			
			String etherType= "VLAN (0x8100)";
			verifyExists(EthernetOrderObj.CompositOrders.Ethertype,"Ether type");
			String etherTypeclr= EthernetOrderObj.CompositOrders.Ethertype;
			clearTextBox(etherTypeclr);
		    sendKeys(EthernetOrderObj.CompositOrders.Ethertype,etherType,"Ether Type");
		    
		    waitForAjax();
		    
			verifyExists(EthernetOrderObj.CompositOrders.VlanTagId,"Vlan Tag Id");  
		    sendKeys(EthernetOrderObj.CompositOrders.VlanTagId,HubVLanTagId,"Vlan Tag Id");
   			waitForAjax();
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   			click(NewHubOrderObj.NewHubOrder.Update,"Update");
   			waitForAjax();
   			
    		getUrl(SpokeOrderscreenurl);
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
   			click(NewHubOrderObj.NewHubOrder.OrderTab,"Order Tab");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.NewEndSiteProductBend,"B end Site Product Order");
   			click(NewHubOrderObj.NewHubOrder.NewEndSiteProductBend,"B end Site Product Order");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			
			verifyExists(EthernetOrderObj.CompositOrders.AccessTechnolgy,"Access Technology");
			click(EthernetOrderObj.CompositOrders.AccessTechnolgy,"B End Access Technology");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessTechnolgy,SpokeAccessTechnolgy,"B End Access Technology");
   			
			verifyExists(EthernetOrderObj.CompositOrders.AccessType,"Access Type");
			click(EthernetOrderObj.CompositOrders.AccessType,"B End Access Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessType,SpokeAccessType,"B End Access Type");
   			
   			if(SpokeResilienceOption.equalsIgnoreCase("Protected"))
   			{
				String ResilienceOptions= "Protected";
				verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
				click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"Order System Name");
   			}
   			else
   			{
				String ResilienceOptions= "Unprotected";
				verifyExists(EthernetOrderObj.CompositOrders.ResilienceOption,"ResilienceOptions");
				click(EthernetOrderObj.CompositOrders.ResilienceOption,"A End Resilience Option");
				Reusable.waitForSiebelLoader();
				selectByVisibleText(EthernetOrderObj.CompositOrders.ResilienceOption,ResilienceOptions,"A End Resilience Option");
   			}
   			
   			waitForAjax();
   			
			verifyExists(EthernetOrderObj.CompositOrders.SiteEnd,"Site End");
			click(EthernetOrderObj.CompositOrders.SiteEnd,"B End Site");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.SiteEnd,"B END","B End Site");
   		
			verifyExists(EthernetOrderObj.CompositOrders.SiteID," SiteID");  
		    sendKeys(EthernetOrderObj.CompositOrders.SiteID,SpokeSiteID," SiteID");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   			click(NewHubOrderObj.NewHubOrder.Update,"Update");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   			waitForAjax();
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
   			click(NewHubOrderObj.NewHubOrder.AccessPortLink,"Access Port Link");
   			
   			verifyExists(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			click(NewHubOrderObj.NewHubOrder.Edit,"Edit Button");
   			waitForAjax();
   			
			verifyExists(EthernetOrderObj.CompositOrders.PresentConnectType,"Presentation Connector Type");
			click(EthernetOrderObj.CompositOrders.PresentConnectType,"B End Presentation Connector Type");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.PresentConnectType,"LC/PC","B End Presentation Connector Type");
			
			verifyExists(EthernetOrderObj.CompositOrders.AccessportRole,"Port Role");
			click(EthernetOrderObj.CompositOrders.AccessportRole,"B End port Role");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.AccessportRole,SpokePortRole,"B End Port Role");
			
			verifyExists(EthernetOrderObj.CompositOrders.VlanTaggingMode,"VLAN Tagging Mode");
			click(EthernetOrderObj.CompositOrders.VlanTaggingMode,"B End VLAN Tagging Mode");
			Reusable.waitForSiebelLoader();
			selectByVisibleText(EthernetOrderObj.CompositOrders.VlanTaggingMode,SpokeVLanTaggingMode,"B End VLAN Tagging Mode");

			verifyExists(EthernetOrderObj.CompositOrders.Update,"Update");
			click(EthernetOrderObj.CompositOrders.Update,"Update");
			
			waitForAjax();
   			//Required Details are Entered
   			
   			if(SpokePortRole.contains("VLAN"))
   			{
   				verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   				click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account bredcrumb");
   				
   				verifyExists(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   				click(NewHubOrderObj.NewHubOrder.GeneralInformationTab,"General Information Tab");
   				waitForAjax();
   				
   				verifyExists(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
   				click(NewHubOrderObj.NewHubOrder.AddFeaturelink,"Add Feature Link");
   				
				verifyExists(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
				click(EthernetOrderObj.CompositOrders.AddFeaturelink, "Add Feature Link");
				
//				verifyExists(EthernetOrderObj.CompositOrders.TypeofFeature,"Type of feature area");  
			    sendKeys(EthernetOrderObj.CompositOrders.TypeofFeature,typeoffeature,"Product Display");
			    Thread.sleep(30000);
			    Reusable.waitForAjax();
				click(EthernetOrderObj.CompositOrders.Vlan,"Select VLAN");
			    
//				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.TypeofFeature, Keys.ENTER);
			    
				verifyExists(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
				click(EthernetOrderObj.CompositOrders.SelectFeature,"Feature selected");
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
				click(EthernetOrderObj.CompositOrders.VLANLink,"VLAN link");
				
				verifyExists(EthernetOrderObj.CompositOrders.Edit,"Edit button");
				click(EthernetOrderObj.CompositOrders.Edit,"Edit button");
				
				verifyExists(EthernetOrderObj.CompositOrders.Ethertype,"Ether type");
				String betherTypeclr= EthernetOrderObj.CompositOrders.Ethertype;
				clearTextBox(betherTypeclr);
			    sendKeys(EthernetOrderObj.CompositOrders.Ethertype,etherType,"Ether Type");
			    
			    waitForAjax();
			    
				verifyExists(EthernetOrderObj.CompositOrders.VlanTagId,"VlanTagId Details are Entered");  
			    sendKeys(EthernetOrderObj.CompositOrders.VlanTagId,SpokeVLanTagId,"VlanTagId Details are Entered");
			    
			    waitForAjax();
   				
   				verifyExists(NewHubOrderObj.NewHubOrder.Update,"Update");
   				 click(NewHubOrderObj.NewHubOrder.Update,"Update");
   				 waitForAjax();
   				 
   				//Required Details are Entered
   			}
   			else 
			{
				Reporter.log("B End Not VLAN");
			}
			Reporter.log("Required details are Updated");
   		}
   	 
   	 public void ProcessSpokeOrder(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
   	 {
 		String SpokeOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke_Order_No");
 		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
 		
 		getUrl(SpokeOrderscreenurl);
 		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
 		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
 		Reusable.waitForAjax();
 	
// 		verifyExists(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
// 		click(NcOrderObj.generalInformation.AccountNameSorting,"Sorting of orders");
// 		Reusable.waitForAjax();
 		
 		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
 		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
 		
 		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
 		
 		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SpokeOrdernumber);
 		
 		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
 		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

 		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
 		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
 		
 		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
 		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
 		Reusable.waitForAjax();
   	 }
   	 
     	public void CompleteSpokeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
     	{
	 		String SpokeOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Spoke_Order_No");
	 		String SpokeOrderscreenurl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke_Order_URL");
			String WorkItems = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke Work item");
	
//			verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
//			click(EthernetOrderObj.CompositOrders.AccountNameSorting,"AccountName Sorting");
			
			verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		
	 		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
	 		
	 		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SpokeOrdernumber);
	 		
	 		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
	 		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

	 		String arrOfStr = EthernetOrderObj.CompositOrders.arrOfStr1+ SpokeOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2;
			verifyExists(arrOfStr,"Composite Order");
			click(arrOfStr,"Composite Order");
		
//			getUrl(SpokeOrderscreenurl);
			
			verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
			click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
			
			verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
			
			waitForAjax();
		
			verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
			click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
			
			waitForAjax();
			
			
			for (int k=1; k<=Integer.parseInt(WorkItems);k++)
			{
				waitForElementToAppear(EthernetOrderObj.CompositOrders.TaskReadytoComplete,120,10000);
				click(EthernetOrderObj.CompositOrders.TaskReadytoComplete,"Workitem in Ready status");
				//need to verify
				CompletSpokeworkitem(GetText(NcOrderObj.taskDetails.TaskTitle),testDataFile, sheetName, scriptNo,dataSetNo);
			}
			
			waitForAjax();
			
			getUrl(SpokeOrderscreenurl);
			
			verifyExists(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
			click(EthernetOrderObj.CompositOrders.Accountbredcrumb,"Accounts Composite Orders Tab");
			
//			verifyExists(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
//			click(EthernetOrderObj.CompositOrders.AccountNameSorting,"Sorting of Accounts");
			
			verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
	 		
	 		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
	 		
	 		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,SpokeOrdernumber);
	 		
	 		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
	 		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
			
			waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ SpokeOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,180,10000);
		    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ SpokeOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
			if (OrderStatus.contains("Process Completed"))
			{
				Report.LogInfo("Validate","\""+SpokeOrdernumber +"\" Is Completed Successfully", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, SpokeOrdernumber +" Is Completed Successfully");
			}
			else if (OrderStatus.contains("Process Started"))
			{
				Report.LogInfo("Validate","\""+SpokeOrdernumber +"\" Execution InProgress", "INFO");
				ExtentTestManager.getTest().log(LogStatus.INFO, SpokeOrdernumber +" Execution InProgress");
			}
			
		}
     	
     	public void CompletSpokeworkitem( String[] taskTitle, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, AWTException, IOException
    	{
     		String SpokeResiliencOption=DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Resilience Option");
    		String SpokeAccessNWElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Access NW Element");
    		String SpokeAccessport = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "B end Access port");
    		String SpokeCPENNITrunkPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 1");
    		String SpokeCPENNITrunkPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend CPE NNI/Trunk Port 2");
    		String SpokePeNwElement = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend PE NW Element");
    		String SpokePEPort1 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend PE Port 1");
    		String SpokePEPort2 = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend PE Port 2");
    		String SpokeVcxControl = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Bend VCX Control");
    		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");
    		String LegacyCpeProfile= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LegacyCpeProfile");
    		String LegacyCpeOamLevel= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "LegacyCpeOamLevel");
    		String OAMProfile= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "OAM Profile");
    	    String LegacyCpeSupportsCFM="yes";
    	
     		System.out.println("In Switch case with TaskName :"+taskTitle);
    		switch(taskTitle[0])
    		{
			case "Reserve Access Resources":
			if (SpokeResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,SpokeAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,SpokeAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,SpokeCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort2,"CPE NNI Port2");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort2);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort2,SpokeCPENNITrunkPort2);
				waitForAjax();
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort2, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
			}
			else
			{
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessNetworkElement,"Access Network Element");
				clearTextBox(EthernetOrderObj.CompositOrders.AccessNetworkElement);
				sendKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement,SpokeAccessNWElement);
				
				waitForAjax();
				
				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessNetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.AccessPort,"Access Port");
//				clearTextBox(EthernetOrderObj.CompositOrders.AccessPort);
				sendKeys(EthernetOrderObj.CompositOrders.AccessPort,SpokeAccessport);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.AccessPort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.CPENNIPort,"CPE NNI Port1");
//				clearTextBox(EthernetOrderObj.CompositOrders.CPENNIPort);
				sendKeys(EthernetOrderObj.CompositOrders.CPENNIPort,SpokeCPENNITrunkPort1);
				waitForAjax();

				Reusable.waitForSiebelLoader();
				Reusable.SendkeaboardKeys(EthernetOrderObj.CompositOrders.CPENNIPort, Keys.ENTER);
				waitForAjax();
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				click(EthernetOrderObj.CompositOrders.Complete,"Complete Workitem");
				waitForAjax();
				Reusable.waitForAjax();
//				workitemcounter.set(workitemcounter.get()+1);
			}
			break;
			
			case "Transport Circuit Design":
			if (SpokeResiliencOption.contains("Protected"))
			{
				//Code for Protected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,SpokePeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForAjax();
				
				if (DeviceType.contains("Stub"))
				{
					if (SpokeAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,SpokeVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
						sendKeys(NcOrderObj.workItems.VCXController,SpokeVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
					}			
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_1,"Ethernet Transport RFS1 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,SpokePEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				click(NcOrderObj.generalInformation.GeneralInformationTab,"General Information Tab");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				click(NcOrderObj.workItems.EthernetLinkTransportRFS_2,"Ethernet Transport RFS2 Link");
				
				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,SpokePEPort2);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();

				verifyExists(NcOrderObj.createOrder.Update,"Update button");
				click(NcOrderObj.createOrder.Update,"Update button");
				Reusable.waitForAjax();
				
				verifyExists(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				click(NcOrderObj.generalInformation.TransportCfs,"Transport CFS order link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
				click(NcOrderObj.addProductDetails.Edit,"Edit button");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			else
			{
            	//Code for Unprotected
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				Reusable.waitForAjax();

				verifyExists(NcOrderObj.workItems.PENetworkElement,"PE Network Element");
				clearTextBox(NcOrderObj.workItems.PENetworkElement);
				sendKeys(NcOrderObj.workItems.PENetworkElement,SpokePeNwElement);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PENetworkElement, Keys.ENTER);
				Reusable.waitForSiebelSpinnerToDisappear();
				
				verifyExists(NcOrderObj.workItems.PePort,"PE Port");
				clearTextBox(NcOrderObj.workItems.PePort);
				sendKeys(NcOrderObj.workItems.PePort,SpokePEPort1);
				Reusable.waitForAjax();
				Reusable.SendkeaboardKeys(NcOrderObj.workItems.PePort, Keys.ENTER);
				Reusable.waitForAjax();

				if (DeviceType.contains("Stub"))
				{
					if (SpokeAccessNWElement.contains("SC"))
					{
						verifyExists(NcOrderObj.workItems.VCXController,"VCX Controller");
//						clearTextBox(NcOrderObj.workItems.VCXController);
						sendKeys(NcOrderObj.workItems.VCXController,SpokeVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
						Reusable.waitForAjax();
						Reusable.waitForpageloadmask();
						sendKeys(NcOrderObj.workItems.VCXController,SpokeVcxControl);
						Reusable.SendkeaboardKeys(NcOrderObj.workItems.VCXController, Keys.ENTER);
					}
	
//								if (Beacon.contains("Beacon"))
//								{
//									verifyExists(NcOrderObj.workItems.Beacon,"Beacon");
//									clearTextBox(NcOrderObj.workItems.Beacon);
//									sendKeys(NcOrderObj.workItems.Beacon,Beacon);
//									Reusable.waitForAjax();
//									Reusable.SendkeaboardKeys(NcOrderObj.workItems.Beacon, Keys.ENTER);
//									Reusable.waitForSiebelSpinnerToDisappear();
//								}
				}						
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
					
			case "New Mngm Network Connection Activation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "Set/Validate Serial Number":
			{
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

				if (DeviceType.contains("Stub"))
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						click(NcOrderObj.workItems.AntSerialNo,"ANT Serial Number field");
						sendKeys(NcOrderObj.workItems.AntSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
					else
					{
						verifyExists(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						click(NcOrderObj.workItems.GxLtsSerialNo,"GX/LTS Serial Number field");
						sendKeys(NcOrderObj.workItems.GxLtsSerialNo,"N100-3970");
						Reusable.waitForAjax();
						
						verifyExists(NcOrderObj.createOrder.Update,"Update button");
						click(NcOrderObj.createOrder.Update,"Update button");
						Reusable.waitForAjax();
						
//						verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
//						click(NcOrderObj.taskDetails.TaskTitle,"Task title link");

						verifyExists(NcOrderObj.addProductDetails.Edit,"Edit button");
						click(NcOrderObj.addProductDetails.Edit,"Edit button");
						Reusable.waitForAjax();
					}
				}
				else
				{
					if (isElementPresent(By.xpath("//a[text()='Update ANT Serial Number']")))
					{
						verifyExists(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
						click(NcOrderObj.workItems.UpdateAntSerialNumber,"Update ANT Serial Number button");
					}
					else
					{
						verifyExists(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
						click(NcOrderObj.workItems.UpdateSerialNumber,"Update Serial Number button");
					}
				}
				Reusable.waitForAjax();
				verifyExists(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				click(NcOrderObj.workItems.TransComplete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Set Legacy CPE Capability Profile":
			{
				clearTextBox(NcOrderObj.workItems.LegacyCpeProfile);
				waitForAjax();
				
				sendKeys(NcOrderObj.workItems.LegacyCpeProfile,LegacyCpeProfile,"Legacy Cpe Profile");
				waitForAjax();
				SendkeyusingAction(Keys.ENTER);
				clearTextBox(NcOrderObj.workItems.LegacyCpeOamLevel);
				waitForAjax();
				sendKeys(NcOrderObj.workItems.LegacyCpeOamLevel,LegacyCpeOamLevel,"Legacy Cpe Oam Level");
				Thread.sleep(5000);
				SendkeyusingAction(Keys.ENTER);
				clearTextBox(NcOrderObj.workItems.LegacyCpeSupportsCFM);
				waitForAjax();
				sendKeys(NcOrderObj.workItems.LegacyCpeSupportsCFM,"yes","Legacy CPE Supports CFM");
				
				verifyExists(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				click(NcOrderObj.taskDetails.TaskTitle,"Task title link");
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
				//Completed Set Legacy CPE Capability Profile
			}
			break;
			
			case "Legacy Activation Completed":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
			
			case "Select OAM Profile":
			{
				verifyExists(NcOrderObj.workItems.OAMProfile,"OAM Profile");
				click(NcOrderObj.workItems.OAMProfile,"OAM Profile");
				waitForAjax();
				sendKeys(NcOrderObj.workItems.OAMProfile,OAMProfile,"OAM Profile");
				SendkeyusingAction(Keys.ENTER);
				
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				//Completed Select OAM Profile
			}	
			break;
			
			case "Activation Start Confirmation":
	    	{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
	    	}
	    	break;
	    	
			case "Bandwidth Profile Confirmation":
			{
				verifyExists(NcOrderObj.workItems.Complete,"Complete Workitem");
				click(NcOrderObj.workItems.Complete,"Complete Workitem");
				Reusable.waitForAjax();
			}
			break;
    	}
    }
     	
     	public void GotoSpokeErrors(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
    	{
        	String Ordernumber = getTextFrom(NewHubOrderObj.NewHubOrder.OrderNumber);
     		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "NC_Order_No", Ordernumber);

     		String[] arrOfStr = Ordernumber.split("#", 0);
     		String Errors=getTextFrom(NewHubOrderObj.NewHubOrder.Errors1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.Errors2);
     		
    		openurl(SpokeOrderscreenurl.get());
    		
    		//Navigate to Composite Order
    		verifyExists(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	click(NewHubOrderObj.NewHubOrder.Accountbredcrumb,"Account Bredcrumb");
		 	
		 	verifyExists(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			click(NewHubOrderObj.NewHubOrder.AccountNameSorting,"Account Name Sorting");
			
          	//Get the value of In Error column for the order");
    		if(Errors.equalsIgnoreCase("Blocked by Errors"))
    		{
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.NewCompositeOrder,"Composite Order");
    			click(NewHubOrderObj.NewHubOrder.CompositeOrder1+arrOfStr[1]+NewHubOrderObj.NewHubOrder.CompositeOrder2,"Composite order");
    			//verifyExists(Errors);
    			//click(Errors);
    			//Click on Composite Order
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    			click(NewHubOrderObj.NewHubOrder.TaskTab,"TaskTab");
    		
    			verifyExists(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			click(NewHubOrderObj.NewHubOrder.ExecutionFlowlink,"Execution Flowlink");
    			waitForAjax();
    			
    			verifyExists(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			click(NewHubOrderObj.NewHubOrder.Errors,"Errors");
    			waitForAjax();
    		}
    		else 
    		{
    			Reporter.log("Not required to Navigate to Errors tab");
    			//Spoke Order did not have any errors to be Captured
    		}
    	}
}
