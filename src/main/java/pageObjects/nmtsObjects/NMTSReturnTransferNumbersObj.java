package pageObjects.nmtsObjects;

import java.io.File;

import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;

public class NMTSReturnTransferNumbersObj {
	
	public static class ReturnTransferNumber
	{ 
		public static final String retandtransfer="@xpath=//font[contains(@onclick,'NumberSearchFree.aspx?M=RI')]"; 
		public static final String portoutforCH="@xpath=//font[contains(@onclick,'SwitzerlandPages/PortoutReturnTransfer')]";
		public static final String wholesaleradiobtn="@xpath=//input[@id='ucxSearchCriteria_radColt_1']"; 
		public static final String ReturnTransradiobtn="@xpath=//input[@id='ucxSearchCriteria_radReturn']";
		public static final String sELECThOSTEDnO="@xpath=//select[@id='ucxSearchCriteria_cboHostedNumber']"; 
		public static final String ReturnAvailabilityIT="@xpath=//select[@id='ucxSearchCriteria_cboAvailability']";
		public static final String ReturnAvailability="@xpath=//select[@id='ucxSearchCriteria_cboAvailabilityUK']";
		public static final String AreaCodePrefix="@xpath=//input[@id='ucxSearchCriteria_txtNationalCode']";
		public static final String AreaCodeExt="@xpath=//input[@id='ucxSearchCriteria_txtAreaCode']";
		public static final String retandtransferStatus="@xpath=//select[@id='ucxSearchCriteria_cboStatusRange']";
		public static final String RangeFrom="@xpath=//input[@id='ucxSearchCriteria_txtLowerBound']";
		public static final String retandtransferRangeto="@xpath=//input[@id='ucxSearchCriteria_txtUpperBound']";
		public static final String Submitbtn="@xpath=//input[@id='ucxSearchCriteria_btnSubmitSearch']";
		public static final String GriddataNMTS="@xpath=//table[@id='ucxRange_grdView']//td[text()='IP Soft Switch']"; 
		public static final String checkbox3="@xpath=//input[@id='ucxRange_grdView__ctl2_chkMark']";
		public static final String Selectmark="@xpath=//a[@id='ucxRange_btnDeleteMarked']";
		public static final String totalpage="@xpath=//span[@id='ucxSelectRange_lblPageTotal']";
		public static final String checkforreturn="@xpath=//table[@id='ucxSelectRange_grdView']//td[text()='RangeFrom']/..//td[9]/input"; 
		public static final String lastbtn="@xpath=//a[@id='ucxSelectRange_btnLastPage']"; 
		public static final String ProcessingDecession="@xpath=//select[@id='ddlNumberStatus']";
		public static final String operator="@xpath=//select[@id='ddlTransferOwner']";
		public static final String YesBtn="@xpath=//input[@id='btnYes']"; 
		public static final String SuccessMsg="@xpath=//span[@id='lblSuccessfulMsg']";
		public static final String SubmitStatus="@xpath=//input[@id='btnRequestSelectedRanges']";
		
		}

}