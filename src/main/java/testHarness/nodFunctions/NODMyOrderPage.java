package testHarness.nodFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

//import Reporter.ExtentTestManager;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.ReadingAndWritingTextFile;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.nodObjects.NODMyOrdersObj;
import testHarness.commonFunctions.ReusableFunctions;


public class NODMyOrderPage extends SeleniumUtils 
{

	ReusableFunctions Reusable = new ReusableFunctions();
	String successmsg;

	private static HashMap<String, String> Number= new HashMap<String, String>();


	public void QuickLinkOptions(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

	//	NavigationtoHomePage(testDataFile, sheetName, scriptNo, dataSetNo);

		verifyExists(NODMyOrdersObj.MyOrder.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODMyOrdersObj.MyOrder.QuickLinkDrpDwnArw,"Quick Link Drop down");

		verifyExists(NODMyOrdersObj.MyOrder.QuickLinkOptions,"Quick Link Options");
		if(isElementPresent(By.xpath("//*[contains(@class,'ng-dropdown-panel ng-star-inserted ng-select-bottom')]")))
		{
			System.out.println("Quick Link is Open Successfully");

		}
		else
		{
			System.out.println("Quick Link drop down not open");

		}
	}

	public void BackToMainHomePage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
	//	NavigationtoHomePage(testDataFile, sheetName, scriptNo, dataSetNo);

		verifyExists(NODMyOrdersObj.MyOrder.HomeMenu,"Home menu");
		click(NODMyOrdersObj.MyOrder.HomeMenu,"Home menu");

		String WelcomeText = getTextFrom(NODMyOrdersObj.MyOrder.WelcomeMsg,"Welcome Text");

		if(WelcomeText.contains("Welcome"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "User is navigated on Home page");
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "User is not navigated on Home page");	
		}
	}


	public void HelpPage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
	//	NavigationtoHomePage(testDataFile, sheetName, scriptNo, dataSetNo);

		verifyExists(NODMyOrdersObj.MyOrder.QuickLinkDrpDwn,"Quick Link Drop down");
		click(NODMyOrdersObj.MyOrder.QuickLinkDrpDwnArw,"Quick Link Drop down");

		click(NODMyOrdersObj.QuickLinks.HelpOption,"Help Quick Link Option");
		switchWindow("https://coltonlinehelp.colt.net/","Colt Online Help");
		verifyExists(NODMyOrdersObj.QuickLinks.HelpPageTxt,"Help Page open");
		close();
		switchWindow("https://mysit.colt.net/numberondemand/#/myOrder","NumberOnDemandUI");
	}

	public void SearchByOrderID(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		//NavigationtoHomePage(testDataFile, sheetName, scriptNo, dataSetNo);
		//Reusable.waitForAjax();

		String OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_ID");

		click(NODMyOrdersObj.QuickLinks.SearchTxtFld, "Send Order ID");
		sendKeys(NODMyOrdersObj.QuickLinks.SearchTxtFld, OrderID, "Send Order ID");
		click(NODMyOrdersObj.QuickLinks.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		//Order ID Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderIDColumn,"Order ID Column");

		String TransID = getTextFrom(NODMyOrdersObj.MyOrder.OrderIDVal,"OrderID");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+TransID);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Id Is not Present");	
		}

		//Order Type Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderTypeColumn,"Order Type Column");

		String OrderType = getTextFrom(NODMyOrdersObj.MyOrder.OrderTypeVal,"Order Type");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionType'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Type Is: "+OrderType);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Type is not Present");	
		}

		//Order Creation Date Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderCreationDateColumn,"Order Creation Date Column");

		String OrderCreationDate = getTextFrom(NODMyOrdersObj.MyOrder.OrderCreationDateVal,"Order Creation Date");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionCreationDate'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Creation Date Is: "+OrderCreationDate);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Creation Date is not Present");	
		}

		//Order Update Date Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderUpdateDateColumn,"Order Update Date Column");

		String OrderUpdateDate = getTextFrom(NODMyOrdersObj.MyOrder.OrderUpdateDateVal,"Order Update Date");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionLastUpdateDate'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Update Date Is: "+OrderUpdateDate);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Update Date is not Present");	
		}

		//Range Start Column & data
		verifyExists(NODMyOrdersObj.MyOrder.RangeStartColumn,"Range Start Column");

		String RangeStart = getTextFrom(NODMyOrdersObj.MyOrder.RangeStartVal,"Range Start");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telNumberStart'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range Start Is: "+RangeStart);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range Start is not Present");	
		}

		//Range End Column & data
		verifyExists(NODMyOrdersObj.MyOrder.RangeEndColumn,"Range End Column");

		String RangeEnd = getTextFrom(NODMyOrdersObj.MyOrder.RangeEndVal,"Range End");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telNumberEnd'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range End Is: "+RangeEnd);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range End is not Present");	
		}

		//Quantity Column & data
		verifyExists(NODMyOrdersObj.MyOrder.QuantityColumn,"Quantity Column");

		String Quantity = getTextFrom(NODMyOrdersObj.MyOrder.QuantityVal,"Quantity");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='quantity'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Quantity Is: "+Quantity);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quantity is not Present");	
		}

		//Order Status Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderStatusColumn,"Order Status Column");

		String OrderStatus = getTextFrom(NODMyOrdersObj.MyOrder.currentStatusVal,"Order Status");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='currentStatus'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Status Is: "+OrderStatus);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Status is not Present");	
		}

	}

	public void SearchByOrderByRangeStart(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		//NavigationtoHomePage(testDataFile, sheetName, scriptNo, dataSetNo);
		//Reusable.waitForAjax();

		String RangeStart = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Range_Start");

		sendKeys(NODMyOrdersObj.QuickLinks.SearchTxtFld, RangeStart, "Range Start");
		click(NODMyOrdersObj.QuickLinks.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		//Order ID Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderIDColumn,"Order ID Column");

		String TransID = getTextFrom(NODMyOrdersObj.MyOrder.OrderIDVal,"OrderID");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+TransID);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Id Is not Present");	
		}

		//Order Type Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderTypeColumn,"Order Type Column");

		String OrderType1 = getTextFrom(NODMyOrdersObj.MyOrder.OrderTypeVal,"Order Type");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionType'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Type Is: "+OrderType1);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Type is not Present");	
		}

		//Order Creation Date Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderCreationDateColumn,"Order Creation Date Column");

		String OrderCreationDate = getTextFrom(NODMyOrdersObj.MyOrder.OrderCreationDateVal,"Order Creation Date");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionCreationDate'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Creation Date Is: "+OrderCreationDate);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Creation Date is not Present");	
		}

		//Order Update Date Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderUpdateDateColumn,"Order Update Date Column");

		String OrderUpdateDate = getTextFrom(NODMyOrdersObj.MyOrder.OrderUpdateDateVal,"Order Update Date");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionLastUpdateDate'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Update Date Is: "+OrderUpdateDate);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Update Date is not Present");	
		}

		//Range Start Column & data
		verifyExists(NODMyOrdersObj.MyOrder.RangeStartColumn,"Range Start Column");

		String RangeStart1 = getTextFrom(NODMyOrdersObj.MyOrder.RangeStartVal,"Range Start");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telNumberStart'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range Start Is: "+RangeStart1);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range Start is not Present");	
		}

		//Range End Column & data
		verifyExists(NODMyOrdersObj.MyOrder.RangeEndColumn,"Range End Column");

		String RangeEnd = getTextFrom(NODMyOrdersObj.MyOrder.RangeEndVal,"Range End");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='telNumberEnd'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Range End Is: "+RangeEnd);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Range End is not Present");	
		}

		//Quantity Column & data
		verifyExists(NODMyOrdersObj.MyOrder.QuantityColumn,"Quantity Column");

		String Quantity = getTextFrom(NODMyOrdersObj.MyOrder.QuantityVal,"Quantity");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='quantity'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Quantity Is: "+Quantity);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Quantity is not Present");	
		}

		//Order Status Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderStatusColumn,"Order Status Column");

		String OrderStatus = getTextFrom(NODMyOrdersObj.MyOrder.currentStatusVal,"Order Status");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='currentStatus'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Status Is: "+OrderStatus);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Status is not Present");	
		}

	}

	public void OrderIdHyperLink(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_ID");

		sendKeys(NODMyOrdersObj.QuickLinks.SearchTxtFld, OrderID, "Send Order ID");
		click(NODMyOrdersObj.QuickLinks.SearchIcon,"Search Icon");
		Reusable.waitForAjax();

		//Order ID Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderIDColumn,"Order ID Column");

		String TransID = getTextFrom(NODMyOrdersObj.MyOrder.OrderIDVal,"OrderID");

		if(isElementPresent(By.xpath("(//*[@tabulator-field='transactionId'])[2]"))) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Order Id Is: "+TransID);
			click(NODMyOrdersObj.MyOrder.OrderIDVal,"Order ID");

			//Order Details section
			verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
			click(NODMyOrdersObj.MyOrder.OrderDetailsSection,"Order Details Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//h5[contains(text(),'Order Details')]"))) 
			{
				//verifyExists(NODMyOrdersObj.MyOrder.OrderDetailsPage,"Order Details Section");
				ExtentTestManager.getTest().log(LogStatus.PASS, "Order Details page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Details page is not Accessable");	
			}

			//Number Details section
			verifyExists(NODMyOrdersObj.MyOrder.NumberDetailsection,"Number Details Section");
			click(NODMyOrdersObj.MyOrder.NumberDetailsection,"Number Details Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//h4[contains(text(),'Telephone Numbers')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Number Details page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Number Details page is not Accessable");	
			}

			//History Conversation Section
			verifyExists(NODMyOrdersObj.MyOrder.HistoryConversationSection,"History Conversation Section");
			click(NODMyOrdersObj.MyOrder.HistoryConversationSection,"History Conversation Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//div[@id='NewPortIncollapseStepper']"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "History Conversation Page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "History Conversation Page is not Accessable");	
			}

			//Number History Section
			verifyExists(NODMyOrdersObj.MyOrder.NumberHistorysSection,"Number History Section");
			click(NODMyOrdersObj.MyOrder.NumberHistorysSection,"Number History Section");
			Reusable.waitForAjax();
			if(isElementPresent(By.xpath("//h6[contains(text(),'Search for a number')]"))) 
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Number History Page is Accessable");
			}else{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Number History Page is not Accessable");	
			}		

		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Order Is not Present");	
		}

	}

	public void ExportToExcel(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		verifyExists(NODMyOrdersObj.MyOrder.DownlaodExelIcon,"Downlaod To Excel");
		click(NODMyOrdersObj.MyOrder.DownlaodExelIcon,"Downlaod To Excel");
		Reusable.waitForAjax();

		isFileDownloaded_DDI("E:\\Kiran_Workspace\\NOD", "Order-ANH-GB-Thu May 06 2021 13_32_41 GMT+0100 (British Summer Time)");

	}

///////Ashwini--------------------------------------------------------
	
	
	public void FilterMyOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String OrderCreatedFilterMonth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Month");
		String OrderCreatedFilterYear = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Year");
		String OrderCreatedFilterDay = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Day");
		String OrderUpdatedFilterMonth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderUpdated_Filter_Month");
		String OrderUpdatedFilterYear = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderUpdated_Filter_Year");
		String OrderUpdatedFilterDay = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderUpdated_Filter_Day");
		
		String FilterCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_Country");
		String OrderType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Type");
		String OrderStatus= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Status");
		
	//	NavigationtoHomePage(testDataFile, sheetName, scriptNo, dataSetNo);
		verifyExists(NODMyOrdersObj.FilterOrder.filterOption,"filter Option");
		click(NODMyOrdersObj.FilterOrder.filterOption,"Click filter Option");
		
		verifyExists(NODMyOrdersObj.FilterOrder.Countrydropdown,"Country dropdown");
		click(NODMyOrdersObj.FilterOrder.Countrydropdown,"Country dropdown");
		

		verifyExists(NODMyOrdersObj.FilterOrder.selectCountry.replace("country",FilterCountry),"Select Country");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectCountry.replace("country",FilterCountry),"Select Country");
		verifyExists(NODMyOrdersObj.FilterOrder.UpCountrydrpdown.replace("country",FilterCountry),"Up Country drpdown");
		click(NODMyOrdersObj.FilterOrder.UpCountrydrpdown.replace("country",FilterCountry),"Up Country drpdown");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderTypedrpDown,"Order Type DrpDown");
		click(NODMyOrdersObj.FilterOrder.OrderTypedrpDown,"Order Type DrpDown");
		verifyExists(NODMyOrdersObj.FilterOrder.selectOrderType.replace("orderType",OrderType),"Select Order Type");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectOrderType.replace("orderType",OrderType),"Select Order Type");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderStatusdrpDown,"Order Status DrpDown");
		click(NODMyOrdersObj.FilterOrder.OrderStatusdrpDown,"Order Status DrpDown");
		verifyExists(NODMyOrdersObj.FilterOrder.selectOrderStatus.replace("orderStatus",OrderStatus),"Select Order Status");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectOrderStatus.replace("orderStatus",OrderStatus),"Select Order Status");
		
		ScrollIntoViewByString(NODMyOrdersObj.FilterOrder.OrderCreationDate);
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationDate,"Order Creation Calendar");
		javaScriptclick(NODMyOrdersObj.FilterOrder.OrderCreationDate,"Order Creation Calendar");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationMonth,"Order Creation Month");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationMonth,OrderCreatedFilterMonth);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationYear,"Order Creation Year");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationYear,OrderCreatedFilterYear);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderCreatedFilterDay),"Order Creation Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderCreatedFilterDay),"Order Creation Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderCreatedFilterDay),"Order Creation Day");
		
		//verifyExists(NODMyOrdersObj.FilterOrder.CloseCalendar,"Close Calendar");
		//click(NODMyOrdersObj.FilterOrder.CloseCalendar,"Close Calendar");
		
		verifyExists(NODMyOrdersObj.FilterOrder.orderUpdationDate,"Order Updated Calendar");
		javaScriptclick(NODMyOrdersObj.FilterOrder.orderUpdationDate,"Order Updated Calendar");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationMonth,"Order updated Month");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationMonth,OrderUpdatedFilterMonth);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationYear,"Order updated Year");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationYear,OrderUpdatedFilterYear);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderUpdatedFilterDay),"Order updated Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderUpdatedFilterDay),"Order updated Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderUpdatedFilterDay),"Order updated Day");
		
		//verifyExists(NODMyOrdersObj.FilterOrder.CloseCalendar,"Close Calendar");
		//click(NODMyOrdersObj.FilterOrder.CloseCalendar,"Close Calendar");
		
		
		verifyExists(NODMyOrdersObj.FilterOrder.ApplyBtn,"Apply Btn");
		click(NODMyOrdersObj.FilterOrder.ApplyBtn,"Apply Btn");
		Reusable.waitForAjax();
		String SearchedDataName = getTextFrom(NODMyOrdersObj.FilterOrder.SearchedData,"Searched Data");
		String SearchedDataColName = SearchedDataName.replaceAll("\\s+"," ");
		String SearchedDataValue = getTextFrom(NODMyOrdersObj.FilterOrder.SearchedDataValue,"Searched Data");
		
		System.out.println(SearchedDataColName);
		System.out.println(SearchedDataValue);
		String colNames="Order Id Order Type Order Creation Date Order Update Date Range Start Range End Quantity User Country Service Profile Order Status Actions";
		//Reusable.waitForAjax();
		
		if(SearchedDataColName.contains(colNames)&& SearchedDataValue!=null)
		{
		if(isElementPresent(By.xpath(("(//div[@title='OrderType'])[1]").replace("OrderType",OrderType.toString())))&& isElementPresent(By.xpath(("(//div[@title='Order Status'])[1]").replace("Order Status",OrderStatus.toString()))))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "User is searched with apllied filter");
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "User is not correclty searched with apllied filter");	
		}
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "No search result found");	
			
		}
		
	}

	public void ResetFilterMyOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		//FilterMyOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		String FilterMonth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Month");
		String FilterYear = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Year");
		String FilterDay = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Day");
		String FilterCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_Country");
		String OrderType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Type");
		String OrderStatus= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Status");
	
		verifyExists(NODMyOrdersObj.FilterOrder.filterOption,"filter Option");
		click(NODMyOrdersObj.FilterOrder.filterOption,"Click filter Option");
		
		verifyExists(NODMyOrdersObj.FilterOrder.Countrydropdown,"Country dropdown");
		click(NODMyOrdersObj.FilterOrder.Countrydropdown,"Country dropdown");
		

		verifyExists(NODMyOrdersObj.FilterOrder.selectCountry.replace("country",FilterCountry),"Select Country");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectCountry.replace("country",FilterCountry),"Select Country");
		verifyExists(NODMyOrdersObj.FilterOrder.UpCountrydrpdown.replace("country",FilterCountry),"Up Country drpdown");
		click(NODMyOrdersObj.FilterOrder.UpCountrydrpdown.replace("country",FilterCountry),"Up Country drpdown");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderTypedrpDown,"Order Type DrpDown");
		click(NODMyOrdersObj.FilterOrder.OrderTypedrpDown,"Order Type DrpDown");
		verifyExists(NODMyOrdersObj.FilterOrder.selectOrderType.replace("orderType",OrderType),"Select Order Type");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectOrderType.replace("orderType",OrderType),"Select Order Type");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderStatusdrpDown,"Order Status DrpDown");
		click(NODMyOrdersObj.FilterOrder.OrderStatusdrpDown,"Order Status DrpDown");
		verifyExists(NODMyOrdersObj.FilterOrder.selectOrderStatus.replace("orderStatus",OrderStatus),"Select Order Status");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectOrderStatus.replace("orderStatus",OrderStatus),"Select Order Status");
		
		ScrollIntoViewByString(NODMyOrdersObj.FilterOrder.OrderCreationDate);
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationDate,"Order Creation Calendar");
		javaScriptclick(NODMyOrdersObj.FilterOrder.OrderCreationDate,"Order Creation Calendar");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationMonth,"Order Creation Month");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationMonth,FilterMonth);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationYear,"Order Creation Year");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationYear,FilterYear);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",FilterDay),"Order Creation Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",FilterDay),"Order Creation Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",FilterDay),"Order Creation Day");
		
        verifyExists(NODMyOrdersObj.FilterOrder.ApplyBtn,"Apply Btn");
		click(NODMyOrdersObj.FilterOrder.ApplyBtn,"Apply Btn");
		Reusable.waitForAjax();
		
		verifyExists(NODMyOrdersObj.FilterOrder.filterOption,"filter Option");
		click(NODMyOrdersObj.FilterOrder.filterOption,"Click filter Option");
		
		verifyExists(NODMyOrdersObj.FilterOrder.resetFilterBtn,"reset Filter Btn");
		click(NODMyOrdersObj.FilterOrder.resetFilterBtn,"reset Filter Btn");
		
		
		if(!(isElementPresent(By.xpath("//span[@class='chip ng-star-inserted']"))))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "User is able to reset apllied filter");
		}
		else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "User is not able to reset apllied filter");
		}
		
	}
	
	public void SearchAndFilterMyOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String OrderID = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_Order_ID");
		
		FilterMyOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		click(NODMyOrdersObj.QuickLinks.SearchTxtFld, "Send Order ID");
		sendKeys(NODMyOrdersObj.QuickLinks.SearchTxtFld, OrderID.toString(), "Send Order ID");
		click(NODMyOrdersObj.QuickLinks.SearchIcon,"Search Icon");
		
		Reusable.waitForAjax();

		//Order ID Column & data
		verifyExists(NODMyOrdersObj.MyOrder.OrderIDColumn,"Order ID Column");

		String TransID = getTextFrom(NODMyOrdersObj.MyOrder.OrderIDVal,"OrderID");

		if(TransID.equals(OrderID)) 
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Combination of  Filter and Search is successful Under My Orders Screen and Order Id Is: "+TransID);
		}else{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Combination of  Filter and Search is not successful Under My Orders Screen");	
		}

	}





















































//Reuseable fun
	public void NavigationtoHomePage(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{

		String ServiceProfile = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Service_Profile");

		javaScriptclick(NODMyOrdersObj.MyOrder.ServiceProfiledrpdwn,"Select your country and profile");
		sendKeys(NODMyOrdersObj.MyOrder.ServiceProfiledrpdwn, ServiceProfile, "Select your country and profile");
		javaScriptclick(NODMyOrdersObj.MyOrder.ProfileOption,"Service Profile Option");
		//Reusable.waitForAjax();

		javaScriptclick(NODMyOrdersObj.MyOrder.StartButton,"Let's get started button");
		waitForElementToBeVisible(NODMyOrdersObj.MyOrder.HomeMenu,15);

	}

	public boolean isFileDownloaded_DDI(String downloadPath, String fileName) {
		File dir = new File(downloadPath);
		File[] dirContents = dir.listFiles();

		for (int i = 0; i < dirContents.length; i++) {
			if (dirContents[i].getName().contains(fileName)) {
				// File has been found, it can now be deleted:
				//dirContents[i].delete();

				String downloadedFileName=dirContents[i].getName();
				System.out.println("Downloaded file name is displaying as: "+ downloadedFileName);
				ExtentTestManager.getTest().log(LogStatus.PASS, "Downloaded file name is displaying as: "+ dirContents[i]);

				return true;
			}
		}
		return false;
	}




	public void FilterMyOrder_WithoutOrderUpdateDateFilter(String testDataFile, String sheetName, String scriptNo, String dataSetNo)throws InterruptedException, AWTException, IOException 
	{
		String OrderCreatedFilterMonth = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Month");
		String OrderCreatedFilterYear = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Year");
		String OrderCreatedFilterDay = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"OrderCreation_Filter_Day");
		String FilterCountry = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Filter_Country");
		String OrderType= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Type");
		String OrderStatus= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Order_Status");
		
	//	NavigationtoHomePage(testDataFile, sheetName, scriptNo, dataSetNo);
		verifyExists(NODMyOrdersObj.FilterOrder.filterOption,"filter Option");
		click(NODMyOrdersObj.FilterOrder.filterOption,"Click filter Option");
		
		verifyExists(NODMyOrdersObj.FilterOrder.Countrydropdown,"Country dropdown");
		click(NODMyOrdersObj.FilterOrder.Countrydropdown,"Country dropdown");
		

		verifyExists(NODMyOrdersObj.FilterOrder.selectCountry.replace("country",FilterCountry),"Select Country");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectCountry.replace("country",FilterCountry),"Select Country");
		verifyExists(NODMyOrdersObj.FilterOrder.UpCountrydrpdown.replace("country",FilterCountry),"Up Country drpdown");
		click(NODMyOrdersObj.FilterOrder.UpCountrydrpdown.replace("country",FilterCountry),"Up Country drpdown");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderTypedrpDown,"Order Type DrpDown");
		click(NODMyOrdersObj.FilterOrder.OrderTypedrpDown,"Order Type DrpDown");
		verifyExists(NODMyOrdersObj.FilterOrder.selectOrderType.replace("orderType",OrderType),"Select Order Type");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectOrderType.replace("orderType",OrderType),"Select Order Type");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderStatusdrpDown,"Order Status DrpDown");
		click(NODMyOrdersObj.FilterOrder.OrderStatusdrpDown,"Order Status DrpDown");
		verifyExists(NODMyOrdersObj.FilterOrder.selectOrderStatus.replace("orderStatus",OrderStatus),"Select Order Status");
		javaScriptclick(NODMyOrdersObj.FilterOrder.selectOrderStatus.replace("orderStatus",OrderStatus),"Select Order Status");
		
		ScrollIntoViewByString(NODMyOrdersObj.FilterOrder.OrderCreationDate);
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationDate,"Order Creation Calendar");
		javaScriptclick(NODMyOrdersObj.FilterOrder.OrderCreationDate,"Order Creation Calendar");
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationMonth,"Order Creation Month");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationMonth,OrderCreatedFilterMonth);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationYear,"Order Creation Year");
		Reusable.Select(NODMyOrdersObj.FilterOrder.OrderCreationYear,OrderCreatedFilterYear);
		
		verifyExists(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderCreatedFilterDay),"Order Creation Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderCreatedFilterDay),"Order Creation Day");
		click(NODMyOrdersObj.FilterOrder.OrderCreationDay.replace("day",OrderCreatedFilterDay),"Order Creation Day");
		
		
		verifyExists(NODMyOrdersObj.FilterOrder.ApplyBtn,"Apply Btn");
		click(NODMyOrdersObj.FilterOrder.ApplyBtn,"Apply Btn");
		Reusable.waitForAjax();
	
	}










}



