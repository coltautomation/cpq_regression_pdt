package testHarness.siebelFunctions;

import java.io.IOException;

import pageObjects.c4cObjects.C4CAccountsObj;
import pageObjects.c4cObjects.C4COpportunityObj;
import pageObjects.siebelObjects.SiebelAccountsObj;
import pageObjects.siebelObjects.SiebelAddProdcutObj;
import pageObjects.siebelObjects.SiebelModeObj;
import pageObjects.siebelObjects.SiebelNumberManagementObj;
import testHarness.commonFunctions.ReusableFunctions;
import testHarness.commonFunctions.SiebelReusableFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.GlobalVariables;
import baseClasses.ReadExcelFile;
import baseClasses.SeleniumUtils;
import baseClasses.Utilities;

public class SiebelNumberManagement extends SeleniumUtils {
	
	SiebelReusableFunctions Reusable = new SiebelReusableFunctions();
	
	public void AddDataUnderNumberManagement(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws InterruptedException, IOException 
	{

		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		String Country = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Country");
		String CityORTown = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"City/Town");
		String PostalCode = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Postal_Code");
		String Premises = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Premises");
		String StreetName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Street_Name");
		waitForAjax();
		waitToPageLoad();
		Reusable.waitForSiebelLoader();

		if (ProductName.toString().equals("Voice Line V")) {
			verifyExists(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			click(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.portGroupTab,"Port Group Tab");
			click(SiebelNumberManagementObj.Number.portGroupTab,"Port Group Tab");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNumberManagementObj.Number.PortGroupNewPluIcn,"Port Group New plus icon");
			click(SiebelNumberManagementObj.Number.PortGroupNewPluIcn,"Port Group New plus icon");
			verifyExists(SiebelNumberManagementObj.Number.callDistributionFld,"Call Distribution field");
			click(SiebelNumberManagementObj.Number.callDistributionFld,"Call Distribution field");
			verifyExists(SiebelNumberManagementObj.Number.callDistributioninput,"Call Distribution input");
			click(SiebelNumberManagementObj.Number.callDistributioninput,"Call Distribution input");
			waitForElementToAppear(SiebelNumberManagementObj.Number.CallDistributionDropDown, 10);
			click(SiebelNumberManagementObj.Number.CallDistributionDropDown,"Call Distribution Drop down");
			verifyExists(SiebelNumberManagementObj.Number.CallDistributionValue,"Call Distribution value");
			click(SiebelNumberManagementObj.Number.CallDistributionValue,"Call Distribution value");
			verifyExists(SiebelNumberManagementObj.Number.assignedPortNewPlusIcn,"Assigned port new plus icon");
			click(SiebelNumberManagementObj.Number.assignedPortNewPlusIcn,"Assigned port new plus icon");
			try
			{
				click(SiebelNumberManagementObj.Number.unassignedPortRow.replace("PortName", "Port 1"),"Click on Port 1");	
			}
			catch(Exception ex) 
			{	
			}
			verifyExists(SiebelNumberManagementObj.Number.UnassignedportOkbtn,"Unassigned port Ok button");
			click(SiebelNumberManagementObj.Number.UnassignedportOkbtn,"Unassigned port Ok button");
			verifyExists(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			click(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.portGroupTab,"Port Group Tab");
			click(SiebelNumberManagementObj.Number.portGroupTab,"Port Group Tab");
			verifyExists(SiebelNumberManagementObj.Number.assignedPortNewPlusIcn,"Assigned port new plus icon");
			click(SiebelNumberManagementObj.Number.assignedPortNewPlusIcn,"Assigned port new plus icon");
			
			try
			{
				click(SiebelNumberManagementObj.Number.unassignedPortRow.replace("PortName", "Port 2"),"Click on Port 2");	
			}
			catch(Exception ex) 
			{
				
			}
			verifyExists(SiebelNumberManagementObj.Number.UnassignedportOkbtn,"Unassigned port Ok button");
			click(SiebelNumberManagementObj.Number.UnassignedportOkbtn,"Unassigned port Ok button");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.numberRangeTab,"number Range Tab");
			click(SiebelNumberManagementObj.Number.numberRangeTab,"number Range Tab");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.numberRangeListNewPlusicn,"Number Range list new plus Icon");
			click(SiebelNumberManagementObj.Number.numberRangeListNewPlusicn,"Number Range list new plus Icon");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.numberPlan,"Number Plan field");
			click(SiebelNumberManagementObj.Number.numberPlan,"Number Plan");
			waitForElementToAppear(SiebelNumberManagementObj.Number.numberPlanDrpDwn, 10);
			click(SiebelNumberManagementObj.Number.numberPlanDrpDwn,"Number Plan drop dwon");
			waitForElementToAppear(SiebelNumberManagementObj.Number.numberPlanValue, 5);
			click(SiebelNumberManagementObj.Number.numberPlanValue,"Number Plan value");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.coltReservationRef,"COlt Reservation Ref");
			click(SiebelNumberManagementObj.Number.coltReservationRef,"COlt Reservation Ref");
			verifyExists(SiebelNumberManagementObj.Number.coltNumberReservatioRefinput,"COlt Reservation Ref input");
			click(SiebelNumberManagementObj.Number.coltNumberReservatioRefinput,"COlt Reservation Ref input");
			sendKeys(SiebelNumberManagementObj.Number.coltNumberReservatioRefinput, "123", "COlt Reservation Ref input");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNumberManagementObj.Number.mainNumberFld,"Main Number field");
			click(SiebelNumberManagementObj.Number.mainNumberFld,"Main Number field");
			verifyExists(SiebelNumberManagementObj.Number.mainNumberInput,"Main Number input");
			click(SiebelNumberManagementObj.Number.mainNumberInput,"Main Number input");
			sendKeys(SiebelNumberManagementObj.Number.mainNumberInput, "12", "Main number input");
			Reusable.waitForSiebelLoader();

			verifyExists(SiebelNumberManagementObj.Number.localAreaCode,"Local Area code");
			click(SiebelNumberManagementObj.Number.localAreaCode,"Local Area code");
			verifyExists(SiebelNumberManagementObj.Number.localAreaCodeinput,"Local Area Code input");
			sendKeys(SiebelNumberManagementObj.Number.localAreaCodeinput, "12", "Local Area Code input");
		
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStart,"Number Range Start");
			click(SiebelNumberManagementObj.Number.numberRangeStart,"Number Range Start");
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStartValue,"Number Range Start vlaue");
			sendKeys(SiebelNumberManagementObj.Number.numberRangeStartValue, "12", "Number Range Start vlaue");
			
			verifyExists(SiebelNumberManagementObj.Number.numberRangeEnd,"Number Range End field");
			click(SiebelNumberManagementObj.Number.numberRangeEnd,"Number Range End field");
			verifyExists(SiebelNumberManagementObj.Number.numberRangeEndinput,"Number Range End input");
			sendKeys(SiebelNumberManagementObj.Number.numberRangeEndinput, "15", "Number Range end vlaue");
			
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStatus,"Number Range Status");
			click(SiebelNumberManagementObj.Number.numberRangeStatus,"Number Range Status");
			Reusable.waitForSiebelLoader();

			waitForElementToAppear(SiebelNumberManagementObj.Number.numberRangeStatusDropDown, 10);
			click(SiebelNumberManagementObj.Number.numberRangeStatusDropDown,"Number Range Status Drop Down");
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStatusValue,"Number Range Status value");
			click(SiebelNumberManagementObj.Number.numberRangeStatusValue,"Number Range Status value");
			
			verifyExists(SiebelNumberManagementObj.Number.portGroupId,"Port Group ID field");
			click(SiebelNumberManagementObj.Number.portGroupId,"Port Group ID field");
			verifyExists(SiebelNumberManagementObj.Number.portGroupIdLookup,"Port Group ID look up");
			click(SiebelNumberManagementObj.Number.portGroupIdLookup,"Port Group ID look up");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.portGroupOkbtn,"Port Group Ok button");
			click(SiebelNumberManagementObj.Number.portGroupOkbtn,"Port Group Ok button");
			Reusable.waitForSiebelLoader();

			waitForElementToAppear(SiebelNumberManagementObj.Number.addressUid, 10);
			verifyExists(SiebelNumberManagementObj.Number.addressUid,"Address UID");
			click(SiebelNumberManagementObj.Number.addressUid,"Address UID");
			verifyExists(SiebelNumberManagementObj.Number.addressUidLookup,"Address UID look up");
			click(SiebelNumberManagementObj.Number.addressUidLookup,"Address UID look up");
			Reusable.waitForSiebelLoader();
		//	verifyExists(SiebelNumberManagementObj.Number.addressUidLookup,"Address UID look up");
			
			waitForElementToAppear(SiebelNumberManagementObj.Number.streetNameAccess, 10);
			sendKeys(SiebelNumberManagementObj.Number.streetNameAccess,StreetName, "stree Name Access");
			verifyExists(SiebelNumberManagementObj.Number.countryAccess,"Country Access");
			sendKeys(SiebelNumberManagementObj.Number.countryAccess, Country, "Country Access");
			verifyExists(SiebelNumberManagementObj.Number.cityTownAccess,"City town access");
			sendKeys(SiebelNumberManagementObj.Number.cityTownAccess, CityORTown, "City town access");
			verifyExists(SiebelNumberManagementObj.Number.postalCodeAccess,"Postal code access");
			sendKeys(SiebelNumberManagementObj.Number.postalCodeAccess,PostalCode, "Postal code access");
			Reusable.SendkeaboardKeys(SiebelNumberManagementObj.Number.postalCodeAccess,Keys.TAB);
			verifyExists(SiebelNumberManagementObj.Number.premisesAccess,"Premises Access");
			sendKeys(SiebelNumberManagementObj.Number.premisesAccess,Premises, "Premises Access");
			
			ScrollIntoViewByString(SiebelNumberManagementObj.Number.searchButtonAccess);
			verifyExists(SiebelNumberManagementObj.Number.searchButtonAccess,"Search button Access");
			click(SiebelNumberManagementObj.Number.searchButtonAccess,"Search button Access");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.selectPickAddressAccess,"Select Pick address Access");
			javaScriptclick(SiebelNumberManagementObj.Number.selectPickAddressAccess);
			verifyExists(SiebelNumberManagementObj.Number.pickAddressButtonAccess,"Pick Address button Access");
			click(SiebelNumberManagementObj.Number.pickAddressButtonAccess,"Pick Address button Access");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.BCN,"BCN field");
			click(SiebelNumberManagementObj.Number.BCN,"BCN field");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.BCNLookup,"BCN look up");
			click(SiebelNumberManagementObj.Number.BCNLookup,"BCN look up");
			verifyExists(SiebelNumberManagementObj.Number.billingProfileOKBtn,"Billing profile Ok button");
			click(SiebelNumberManagementObj.Number.billingProfileOKBtn,"Billing profile Ok button");
			Reusable.waitForSiebelLoader();
		}	else if (ProductName.toString().equals("SIP Trunking")) {

			
			verifyExists(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			click(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.numberRangeListNewPlusicn,"Number Range list new plus Icon");
			click(SiebelNumberManagementObj.Number.numberRangeListNewPlusicn,"Number Range list new plus Icon");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.numberPlan,"Number Plan field");
			click(SiebelNumberManagementObj.Number.numberPlan,"Number Plan");
			waitForElementToAppear(SiebelNumberManagementObj.Number.numberPlanDrpDwn, 10);
			click(SiebelNumberManagementObj.Number.numberPlanDrpDwn,"Number Plan drop dwon");
			waitForElementToAppear(SiebelNumberManagementObj.Number.numberPlanValue, 5);
			click(SiebelNumberManagementObj.Number.numberPlanValue,"Number Plan value");
			
			verifyExists(SiebelNumberManagementObj.Number.coltReservationRef,"COlt Reservation Ref");
			click(SiebelNumberManagementObj.Number.coltReservationRef,"COlt Reservation Ref");
			verifyExists(SiebelNumberManagementObj.Number.coltNumberReservatioRefinput,"COlt Reservation Ref input");
			click(SiebelNumberManagementObj.Number.coltNumberReservatioRefinput,"COlt Reservation Ref input");
			sendKeys(SiebelNumberManagementObj.Number.coltNumberReservatioRefinput, "123", "COlt Reservation Ref input");
			verifyExists(SiebelNumberManagementObj.Number.mainNumberFld,"Main Number field");
			click(SiebelNumberManagementObj.Number.mainNumberFld,"Main Number field");
			verifyExists(SiebelNumberManagementObj.Number.mainNumberInput,"Main Number input");
			click(SiebelNumberManagementObj.Number.mainNumberInput,"Main Number input");
			sendKeys(SiebelNumberManagementObj.Number.mainNumberInput, "12", "Main number input");
			verifyExists(SiebelNumberManagementObj.Number.localAreaCode,"Local Area code");
			click(SiebelNumberManagementObj.Number.localAreaCode,"Local Area code");
			sendKeys(SiebelNumberManagementObj.Number.localAreaValue, "12", "Local Area Code input");
		
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStart,"Number Range Start");
			click(SiebelNumberManagementObj.Number.numberRangeStart,"Number Range Start");
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStartValue,"Number Range Start vlaue");
			sendKeys(SiebelNumberManagementObj.Number.numberRangeStartValue, "12", "Number Range Start vlaue");
			
			verifyExists(SiebelNumberManagementObj.Number.numberRangeEnd,"Number Range End field");
			click(SiebelNumberManagementObj.Number.numberRangeEnd,"Number Range End field");
			verifyExists(SiebelNumberManagementObj.Number.numberRangeEndinput,"Number Range End input");
			sendKeys(SiebelNumberManagementObj.Number.numberRangeEndinput, "15", "Number Range end vlaue");
			
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStatus,"Number Range Status");
			click(SiebelNumberManagementObj.Number.numberRangeStatus,"Number Range Status");
			waitForElementToAppear(SiebelNumberManagementObj.Number.numberRangeStatusDropDown, 10);
			click(SiebelNumberManagementObj.Number.numberRangeStatusDropDown,"Number Range Status Drop Down");
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStatusValue,"Number Range Status value");
			click(SiebelNumberManagementObj.Number.numberRangeStatusValue,"Number Range Status value");
			
			waitForElementToAppear(SiebelNumberManagementObj.Number.addressUid, 10);
			verifyExists(SiebelNumberManagementObj.Number.addressUid,"Address UID");
			click(SiebelNumberManagementObj.Number.addressUid,"Address UID");
			verifyExists(SiebelNumberManagementObj.Number.addressUidLookup,"Address UID look up");
			click(SiebelNumberManagementObj.Number.addressUidLookup,"Address UID look up");
			Reusable.waitForSiebelLoader();
			waitForElementToAppear(SiebelNumberManagementObj.Number.streetNameAccess, 10);
			sendKeys(SiebelNumberManagementObj.Number.streetNameAccess,StreetName, "stree Name Access");
			verifyExists(SiebelNumberManagementObj.Number.countryAccess,"Country Access");
			sendKeys(SiebelNumberManagementObj.Number.countryAccess, Country, "Country Access");
			verifyExists(SiebelNumberManagementObj.Number.cityTownAccess,"City town access");
			sendKeys(SiebelNumberManagementObj.Number.cityTownAccess, CityORTown, "City town access");
			verifyExists(SiebelNumberManagementObj.Number.postalCodeAccess,"Postal code access");
			sendKeys(SiebelNumberManagementObj.Number.postalCodeAccess,PostalCode, "Postal code access");
			verifyExists(SiebelNumberManagementObj.Number.premisesAccess,"Premises Access");
			sendKeys(SiebelNumberManagementObj.Number.premisesAccess,Premises, "Premises Access");
			
			ScrollIntoViewByString(SiebelNumberManagementObj.Number.searchButtonAccess);
			verifyExists(SiebelNumberManagementObj.Number.searchButtonAccess,"Search button Access");
			click(SiebelNumberManagementObj.Number.searchButtonAccess,"Search button Access");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.selectPickAddressAccess,"Select Pick address Access");
			javaScriptclick(SiebelNumberManagementObj.Number.selectPickAddressAccess);
			verifyExists(SiebelNumberManagementObj.Number.pickAddressButtonAccess,"Pick Address button Access");
			click(SiebelNumberManagementObj.Number.pickAddressButtonAccess,"Pick Address button Access");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.BCN,"BCN field");
			click(SiebelNumberManagementObj.Number.BCN,"BCN field");
			verifyExists(SiebelNumberManagementObj.Number.BCNLookup,"BCN look up");
			click(SiebelNumberManagementObj.Number.BCNLookup,"BCN look up");
			ScrollIntoViewByString(SiebelNumberManagementObj.Number.billingProfileOKBtn);
			verifyExists(SiebelNumberManagementObj.Number.billingProfileOKBtn,"Billing profile Ok button");
			click(SiebelNumberManagementObj.Number.billingProfileOKBtn,"Billing profile Ok button");
			Reusable.waitForSiebelLoader();
		}
		
	}
	
	public void MandatoryFields(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception {
		
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Product_Type");
		
		if (ProductName.toString().equals("Voice Line V")||ProductName.toString().equals("SIP Trunking")) 
		{
			verifyExists(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			click(SiebelNumberManagementObj.Number.numberManagementTab,"Number Management Tab");
			Reusable.waitForSiebelLoader();
			verifyExists(SiebelNumberManagementObj.Number.numberRangeStatus,"Number Range Status");
			click(SiebelNumberManagementObj.Number.numberRangeStatus,"Number Range Status");
			waitForElementToAppear(SiebelNumberManagementObj.Number.numberRangeStatusDropDown, 10);
			click(SiebelNumberManagementObj.Number.numberRangeStatusDropDown,"Number Range Status Drop Down");
			waitForElementToAppear(SiebelNumberManagementObj.Number.numberRangeStatusValueActive, 10);
			click(SiebelNumberManagementObj.Number.numberRangeStatusValueActive,"Number Range Status Active value");
			Reusable.waitForSiebelLoader();
		}
	}
	
	
	
	

}
