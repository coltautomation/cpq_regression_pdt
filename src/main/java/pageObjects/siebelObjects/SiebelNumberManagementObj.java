package pageObjects.siebelObjects;

public class SiebelNumberManagementObj {

	
	public static class Number
	 { 
		public static final String numberManagementTab="@xpath=//a[text()='Number Management']";
		public static final String portGroupTab="@xpath=//a[text()='Port Group']";
		public static final String PortGroupNewPluIcn="@xpath=//button[@title='Port Group:New']";
		public static final String callDistributionFld="@xpath=//*[@id='1_s_1_l_Call_Distribution']";
		public static final String callDistributioninput="@xpath=//input[@class='siebui-list-ctrl siebui-ctrl-select siebui-input-popup siebui-align-left siebui-input-align-left ui-autocomplete-input']";
		public static final String CallDistributionDropDown="@xpath=//span[@class='siebui-icon-dropdown']";
//		public static final String CallDistributionValue="@xpath=//li[text()='Load Sharing']";
	//	public static final String CallDistributionValue="@xpath=//li/*[text()='Load Sharing']"; // Updated IP2020

		public static final String assignedPortNewPlusIcn="@xpath=//button[@aria-label='Assigned ISDN Port:New']";
		     public static  String unassignedPortRow="@xpath=//div[@name='popup']//td[text()='PortName']/parent::tr";
		public static final String UnassignedportOkbtn="@xpath=//button[@aria-label='Unassigned Ports:OK']";
		public static final String numberRangeTab="@xpath=//a[ text()='Number Range']";
		public static final String numberRangeListNewPlusicn="@xpath=//button[ @title='Number Range List:New']";
		public static final String numberPlan="@xpath=//*[@id='1_s_1_l_Number_Plan']";
		public static final String numberPlanDrpDwn="@xpath=//span[@class='siebui-icon-dropdown']";
	//	public static final String numberPlanValue="@xpath=//li[text()='Colt Existing Number']";
		public static final String numberPlanValue="@xpath=//li/*[text()='Colt Existing Number']"; // Updated IP22020

		public static final String coltReservationRef="@xpath=//*[@id='1_s_1_l_Colt_Number_Reservation_Reference']";
		public static final String coltNumberReservatioRefinput="@xpath=//*[@id='1_Colt_Number_Reservation_Reference']";
		public static final String mainNumberFld="@xpath=//*[@id='1_s_1_l_Main_Number']";
		public static final String mainNumberInput="@xpath=//input[@name='Main_Number']";
		public static final String localAreaCode="@xpath=//*[@id='1_s_1_l_Local_Area_Code']";
		public static final String localAreaValue="@xpath=//*[@id='1_Local_Area_Code']";
		public static final String numberRangeStart="@xpath=//*[@id='1_s_1_l_Number_Range_Start']";
		public static final String numberRangeStartValue="@xpath=//*[@id='1_Number_Range_Start']";
		public static final String numberRangeEnd="@xpath=//*[@id='1_s_1_l_Number_Range_End']";
		public static final String numberRangeEndinput="@xpath=//*[@id='1_Number_Range_End']";
		public static final String numberRangeStatus="@xpath=//*[@id='1_s_1_l_Number_Range_Status']";
		public static final String numberRangeStatusDropDown="@xpath=//span[@class='siebui-icon-dropdown']";
	//	public static final String numberRangeStatusValue="@xpath=//li[text()='Allocate']";
	//	public static final String numberRangeStatusValueActive="@xpath=//li[text()='Active']";
		public static final String numberRangeStatusValueActive="@xpath=//li/*[text()='Active']"; // Updated IP2020

		//public static final String portGroupId="@xpath=//td[@aria-labelledby='s_1_l_Porting_Group_ID s_1_l_altpick']";
		public static final String portGroupIdLookup="@xpath=//span[@class='siebui-icon-pick']";
		public static final String portGroupOkbtn="@xpath=//button[@aria-label='Port Group:OK']";
	//	public static final String addressUid="@xpath=//td[@aria-labelledby='s_1_l_COLT_Address_UID s_1_l_altpick']";
		public static final String addressUidLookup="@xpath=//span[@class='siebui-icon-pick']";
		public static final String streetNameAccess="@xpath=//span[text()='Street Name']/..//input";
		public static final String countryAccess="@xpath=//span[text()='Country']/..//input";
		public static final String cityTownAccess="@xpath=//span[text()='City / Town']/..//input";
		public static final String postalCodeAccess="@xpath=//span[text()='Postal Code']/..//input";
		public static final String premisesAccess="@xpath=//span[text()='Premises']/..//input";
		public static final String searchButtonAccess="@xpath=//button[text()='Search']";
		public static final String selectPickAddressAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
		public static final String pickAddressButtonAccess="@xpath=//button[text()='Pick Address']";
		public static final String selectPickBuildingAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
		public static final String pickBuildingButtonAccess="@xpath=//button[text()='Pick Building']";
		public static final String selectPickSiteAccess="@xpath=(//tr[@class='colt-siteselection-datarow'])[1]";
		public static final String pickSiteButtonAccess="@xpath=//button[text()='Pick Site']";

			
		//public static final String BCN="@xpath=//td[@aria-labelledby='s_1_l_Out_of_Hours_Porting_Charge_BCN s_1_l_altpick']";
		public static final String BCNLookup="@xpath=//span[@class='siebui-icon-pick']";
		public static final String billingProfileOKBtn="@xpath=//button[ @aria-label='Billing Profile:OK']";
		public static final String billTabExpandRows="@xpath=//table[@summary='Service Charges']//tr[@data-leaf='true']";
		public static final String billTabAmount="@xpath=//table[@summary='Service Charges']//tr[@data-leaf='true'][index]//td[7]";
		public static final String billTabAmountInput="@xpath=//table[@summary='Service Charges']//tr[@data-leaf='true'][index]//td[7]//input";
		public static final String billTabBCN="@xpath=//table[@summary='Service Charges']//tr[@data-leaf='true'][index]//td[8]";
		public static final String localAreaCodeinput="@xpath=//*[@id='1_Local_Area_Code']";
		
		public static final String BCN="@xpath=//td[@id='1_s_1_l_Out_of_Hours_Porting_Charge_BCN']";
			/*
		
			<PortGroupId>//td[@aria-labelledby='s_1_l_Porting_Group_ID s_1_l_altpick']</PortGroupId>
		<PortGroupIdLookup>//span[@class='siebui-icon-pick']</PortGroupIdLookup>
		<PortGroupOk>//button[@aria-label='Port Group:OK']</PortGroupOk>
		<addressUid>//td[@aria-labelledby='s_1_l_COLT_Address_UID s_1_l_altpick']</addressUid>
		<addressUidLookup>//span[@class='siebui-icon-pick']</addressUidLookup>
		<BCN>//td[@aria-labelledby='s_1_l_Out_of_Hours_Porting_Charge_BCN s_1_l_altpick']</BCN>
		<BCNLookup>//span[@class='siebui-icon-pick']</BCNLookup>
		<BillingProfileOK>//button[ @aria-label='Billing Profile:OK']</BillingProfileOK>
		<BilltabExpandRows>//table[@summary='Service Charges']//tr[@data-leaf='true']</BilltabExpandRows>
		<BilltabAmount>//table[@summary='Service Charges']//tr[@data-leaf='true'][index]//td[7]</BilltabAmount>
		//table[@summary='Service Charges']//tr[@data-leaf='true']
		<BilltabAmountInput>//table[@summary='Service Charges']//tr[@data-leaf='true'][index]//td[7]//input</BilltabAmountInput>
		<BilltabBCN>//table[@summary='Service Charges']//tr[@data-leaf='true'][index]//td[8]</BilltabBCN>
		<BilltabBCNInput>//table[@summary='Service Charges']//tr[@data-leaf='true'][index]//td[8]//span</BilltabBCNInput>
		<BcnSearchInput>//input[@aria-labelledby='PopupQuerySrchspec_Label']</BcnSearchInput>
		<BcnSearchGo>/td[@class='siebui-popup-filter']//button[@title='Billing Profile:Go' ]</BcnSearchGo>
		<BcnSearchOK>//button[@title='Billing Profile:OK' ]</BcnSearchOK>
		
	*/
		public static final String numberRangeStatusValue="@xpath=//ul[not(contains(@style,'display'))]/descendant::div[text()='Allocate']";
		public static final String addressUid="@xpath=//td[@id='1_s_1_l_COLT_Address_UID']";
		
		public static final String CallDistributionValue="@xpath=//li/*[text()='Round Robin']";
		public static final String portGroupId="@xpath=//td[@id='1_s_1_l_Porting_Group_ID']";
		
	 }
}
