package testHarness.ncFunctions;

import java.awt.AWTException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import pageObjects.ncObjects.CarNorObj;
import pageObjects.ncObjects.CeaseOrderObj;
import pageObjects.ncObjects.EthernetOrderObj;
import pageObjects.ncObjects.NcOrderObj;
import testHarness.commonFunctions.ReusableFunctions;

public class CeaseOrder extends SeleniumUtils {

	ReusableFunctions Reusable = new ReusableFunctions();
	IpAccessCeaseOrder IpCease = new IpAccessCeaseOrder();
	Error_WorkItems ErrorWorkItems = new Error_WorkItems();
	
	public void NcCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception 
	{
		String ProductName = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Comments");
		
		if (ProductName.toString().contains("Line"))
		{
			CreatCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("Hub and Spoke"))
		{
			CreatSpokeCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CreatHubCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			UpdateDescription();
			EditHubProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			DecomposeHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			ProcessHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			CompleteHubOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("IPA"))
		{
			IpCease.CreateIpAccessCeaseOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.UpdateDescription();
			IpCease.EditProduct(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.DecomposeOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.ProcessOrder(testDataFile, sheetName, scriptNo, dataSetNo);
			IpCease.CompleteOrder(testDataFile, sheetName, scriptNo, dataSetNo);
		}
		else if (ProductName.toString().contains("IPVPN"))
		{
			
		}
	}

	public void CreatCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String Ordernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke/Endpoint Order Number");

    	Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");			
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Name Filter");
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,Ordernumber,"FilterInputValue");		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order number for cease");

		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Apply button");

		//selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Selected Name in Filter ");	
		String CompOrdernumber=Ordernumber;
		//Log.info(CompOrdernumber.get());
		String[] arrOfStr = CompOrdernumber.split("#", 0);
		//Log.info(arrOfStr[1]);
		waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Order to be Completed");
		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*",10);		
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Order");
		String ProductInstancenumber=getTextFrom(CeaseOrderObj.CeaseOrder.ProductInstNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ProductInstancenumber", ProductInstancenumber);

		String[] arrOfStr1 = ProductInstancenumber.split("#", 0); 

		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		

		verifyExists(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		click(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInstTab");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Product Instance Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"NameFiltering");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Name Filter");
		
		selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Selected Name in Filter");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,arrOfStr1[1],"FilterInputValue");	
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Instance number for cease provided ");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Apply button");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		//verifyExists(CeaseOrderObj.CeaseOrder.EndsiteproductCheck,"EndsiteproductCheck");
		//click(CeaseOrderObj.CeaseOrder.EndsiteproductCheck,"EndsiteproductCheck");
		
		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr1[1]+"')]/parent::*/parent::*/parent::*/td//input",10);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Instance ");
		
		verifyExists(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		click(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");			
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Create Disconnect Product Order ");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		String CeaseOrdernumber=getTextFrom(CeaseOrderObj.CeaseOrder.LinkforOrder);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No", CeaseOrdernumber);

		//Log.info(CeaseOrdernumber);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Cease Spoke Order Number Generated : "+CeaseOrdernumber);
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		//Log.info(arrOfStr2[1]);
		String CeaseOrderscreenURL = getAttributeFrom(CeaseOrderObj.CeaseOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL", CeaseOrderscreenURL);

		//Log.info(CeaseOrderscreenURL);
	}
    
	public void UpdateDescription() throws Exception
	{
		verifyExists(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"General Information Tab");
		click(CeaseOrderObj.CeaseOrder.GeneralInformationTab,"General Information Tab");
		Reusable.waitForAjax();
        
		verifyExists(CeaseOrderObj.CeaseOrder.EditDesc,"Edit description");
		click(CeaseOrderObj.CeaseOrder.EditDesc,"Edit description");
		Reusable.waitForpageloadmask();
		
		verifyExists(CeaseOrderObj.CeaseOrder.OrderDescription,"Order Description");
		sendKeys(CeaseOrderObj.CeaseOrder.OrderDescription,"Cease Order created using CTAF Automation script","Order Description");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Update,"Update");
		click(CeaseOrderObj.CeaseOrder.Update,"Update");
		Reusable.waitForAjax();

	}

	public void EditProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
    	String CeaseOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL");
    	
 		getUrl(CeaseOrderscreenURL);
 		
 		verifyExists(CeaseOrderObj.CeaseOrder.DisconnectOrder,"DisconnectOrder");
		click(CeaseOrderObj.CeaseOrder.DisconnectOrder,"DisconnectOrder");	 		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Disconnect Ethernet Connection Product Order");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Edit,"Edit");
		click(CeaseOrderObj.CeaseOrder.Edit,"Edit");
		
		verifyExists(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		click(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		
		clearTextBox(CarNorObj.carNor.HardCeaseDelay);
		
		sendKeys(CarNorObj.carNor.HardCeaseDelay,"0");
		
		keyPress(CarNorObj.carNor.HardCeaseDelay,Keys.ENTER);

		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		click(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		sendKeys(CarNorObj.carNor.ColtPromiseMonth,Month.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseMonth,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		click(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		sendKeys(CarNorObj.carNor.ColtPromiseDate,Day.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseDate,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		click(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		sendKeys(CarNorObj.carNor.ColtPromiseYear,Year.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseYear,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		waitForAjax();
        Reusable.waitForpageloadmask();
	}			
    
    
	public void DecomposeOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL");

		getUrl(CeaseOrderscreenURL);
 		
		verifyExists(CeaseOrderObj.CeaseOrder.Suborder,"Suborder");
		click(CeaseOrderObj.CeaseOrder.Suborder,"Update");
		
		verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
	}
	

	public void ProcessOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No");		
		String CeaseOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL");
		

 		getUrl(CeaseOrderscreenURL);

 		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Orders Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountNameSorting");
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		
		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/parent::*/td//input",10);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select Cease Order");
		
		verifyExists(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		click(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Start Processing button");
	}

	public void CompleteOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL");
		String CeaseOrdernumber =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
		String WorkItem = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"WorkItem"); 
		
		String[] arrOfStr2=CeaseOrdernumber.split("#", 0);
		verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountName Sorting");
		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountName Sorting");		
		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*", 10);
		
		click(CeaseOrderObj.CeaseOrder.TaskTab,"Task Tab");	
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Tasks Tab");
		click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"Execution flow link");	
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Execution Flow link");
		Reusable.waitForSiebelLoader();
		click(CeaseOrderObj.CeaseOrder.Workitems,"Work items");	
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Workitems Tab");
		Reusable.waitForAjax();
		for (int k=1; k<=Integer.parseInt(WorkItem.toString());k++){
			waitForElementToAppear(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,60,10000);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Manual Workitems to be displayed");
			click(CeaseOrderObj.CeaseOrder.TaskReadytoComplete,"Task ready to complete");	
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Workitem in  Ready status");
			
		}
		    Completeworkitem(GetText(CeaseOrderObj.CeaseOrder.DisconnectTaskTitle),testDataFile, sheetName, scriptNo, dataSetNo);
	 		getUrl(CeaseOrderscreenURL);
			click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");	
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Orders Tab");
			click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountName Sorting");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();
			waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Disconnect Order to be Completed");
			Reusable.waitForAjax();
			Reusable.waitForpageloadmask();



		}
	
    public void Completeworkitem(String[] taskname, String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws IOException, InterruptedException{
    	
    	//Log.info("In Switch case with TaskName :"+taskname);
		switch(taskname[0])
		{
			case "Legacy Deactivation Completed":
			{
				click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
				Reusable.waitForpageloadmask();
			}
		}
    }
    
    public void GotoErrors(String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
	{

    	// Check the Inputdata3 variable
    			String CeaseOrdernumber =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order Number");
    			String CeaseOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL");
    			String[] arrOfStr2 = CeaseOrdernumber.split("#", 0);
    	 		getUrl(CeaseOrderscreenURL);
    			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to Composite Order");
    			click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");	
    			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigate to CeaseOrder Composite Orders Tab");
    			click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountName Sorting");
				String Errors= getTextFrom("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*/parent::*/following-sibling::*[4]");
				if(Errors.equalsIgnoreCase("Blocked by Errors"))
				{
					ClickonElementByString("//a/span[contains(text(),'"+arrOfStr2[1]+"')]/parent::*", 10);
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Composite Order");
	    			click(CeaseOrderObj.CeaseOrder.TaskTab,"Task Tab");
	    			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Tasks Tab");
	    			click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"Execution flow link");	
	    			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Execution Flow link");
	    			Reusable.waitForAjax();
	    			Reusable.waitForpageloadmask();
	    			click(CeaseOrderObj.CeaseOrder.Errors,"Errors");	
	    			ExtentTestManager.getTest().log(LogStatus.FAIL, " Step: Click on Errors Tab");
	    			Reusable.waitForAjax();
	    			Reusable.waitForpageloadmask();
				}	
				else 
				{
					//Log.info("Not required to Navigate to Errors tab");
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Order did not have any errors to be Captured");
				}
			}
    
    public void CreatHubCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
   	{
    	String HubOrderNumber=  DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Hub/Site Order Number"); 

    	verifyExists(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountName Sorting");
		click(CeaseOrderObj.CeaseOrder.AccountNameSorting,"AccountName Sorting");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");

		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,HubOrderNumber,"Order Description"); 
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"Apply Button");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"Apply Button");

		String[] arrOfStr = HubOrderNumber.split("#", 0);
		
//		waitForElementToAppear("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*/parent::*/following-sibling::td[contains(text(),'Process Completed')]",60,10000);
//
		ClickonElementByString("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*", 10);
		
        String ProductInstancenumber = getTextFrom(CeaseOrderObj.CeaseOrder.HubProductInstNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ProductInstNumber", ProductInstancenumber);

		String[] arrOfStr1 = ProductInstancenumber.split("#", 0); 

		verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInst Tab");
		click(CeaseOrderObj.CeaseOrder.ProductInstTab,"ProductInst Tab");
		
		verifyExists(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");
		click(CeaseOrderObj.CeaseOrder.NameFiltering,"Name Filtering");
		
		selectByVisibleText(CeaseOrderObj.CeaseOrder.FilterSelectName,"Name","FilterSelectName");
		
		sendKeys(CeaseOrderObj.CeaseOrder.FilterInputValue,""+arrOfStr1[1]+"","Filter Input Value");
		
		verifyExists(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");
		click(CeaseOrderObj.CeaseOrder.ApplyButton,"ApplyButton");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

		verifyExists(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");
		click(CeaseOrderObj.CeaseOrder.CreateDisconnectProductOrder,"CreateDisconnectProductOrder");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		
		String CeaseHubnumber= getTextFrom(CeaseOrderObj.CeaseOrder.LinkforOrder);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_No", CeaseHubnumber);

		String[] arrOfStr2 = CeaseHubnumber.split("#", 0); 
		
		String CeaseOrderscreenURL = getAttributeFrom(CeaseOrderObj.CeaseOrder.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL", CeaseOrderscreenURL);
   	}
		
    public void EditHubProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    {
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
    	
 		getUrl(HubOrderscreenURL);
 		
		verifyExists(CeaseOrderObj.CeaseOrder.DisconnectHubOrder,"Disconnect Order");
		click(CeaseOrderObj.CeaseOrder.DisconnectHubOrder,"Disconnect Order");
		
		verifyExists(CarNorObj.carNor.Edit,"Edit");
		click(CarNorObj.carNor.Edit,"Edit");
		
		verifyExists(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		click(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		
		clearTextBox(CarNorObj.carNor.HardCeaseDelay);
		
		sendKeys(CarNorObj.carNor.HardCeaseDelay,"0");
		
		keyPress(CarNorObj.carNor.HardCeaseDelay,Keys.ENTER);

		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		click(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		sendKeys(CarNorObj.carNor.ColtPromiseMonth,Month.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseMonth,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		click(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		sendKeys(CarNorObj.carNor.ColtPromiseDate,Day.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseDate,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		click(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		sendKeys(CarNorObj.carNor.ColtPromiseYear,Year.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseYear,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		waitForAjax();
    }
    
    public void DecomposeHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
    	
 		getUrl(HubOrderscreenURL);
 		
		verifyExists(CeaseOrderObj.CeaseOrder.HubSubOrder,"HubSubOrder");
		click(CeaseOrderObj.CeaseOrder.HubSubOrder,"HubSubOrder");

		verifyExists(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");
		click(CeaseOrderObj.CeaseOrder.Decompose,"Decompose");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
 	}
    public void ProcessHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
    {
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
 		String HubOrdernumber =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_No");
    	
 		getUrl(HubOrderscreenURL);

    	verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,HubOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");

		verifyExists(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		click(CeaseOrderObj.CeaseOrder.StartProccessing,"StartProccessing");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
    }
    
    public void CompleteHubOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
    	String HubOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_URL");
 		String HubOrdernumber =DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Hub/Site_Order_No");

		getUrl(HubOrderscreenURL);

		verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
		click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");

		verifyExists(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
		click(CeaseOrderObj.CeaseOrder.ExecutionFlowlink,"ExecutionFlowlink");
		
		waitForAjax();
		
		verifyExists(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");
		click(EthernetOrderObj.CompositOrders.Workitems,"Workitems Tab");

		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
 		
		getUrl(HubOrderscreenURL);
 		
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
        
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,HubOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ HubOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,200,10000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ HubOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
		if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+HubOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, HubOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+HubOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, HubOrdernumber +" Execution InProgress");
		}
	}
    
	public void CreatSpokeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String SpokeOrderNumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Spoke/Endpoint Order Number");

		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		verifyExists(CarNorObj.carNor.FilterInputValue,"Filter Input Value");
		sendKeys(CarNorObj.carNor.FilterInputValue,SpokeOrderNumber);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");

		String[] arrOfStr = SpokeOrderNumber.split("#", 0);
		
		ClickonElementByString(("//a/span[contains(text(),'"+arrOfStr[1]+"')]/parent::*"),10);
		
		String ProductInstancenumber = getTextFrom(CarNorObj.carNor.ProductInstNumber);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "ProductInstNumber", ProductInstancenumber);
		
		verifyExists(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		click(CarNorObj.carNor.Accountbredcrumb,"Account bredcrumb");
		
		String[] arrOfStr1 = ProductInstancenumber.split("#", 0);
		
		verifyExists(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		click(CarNorObj.carNor.ProductInstTab,"Product Inst Tab");
		
		verifyExists(CarNorObj.carNor.NameFiltering,"Name Filtering");
		click(CarNorObj.carNor.NameFiltering,"Name Filtering");
		
		select(CarNorObj.carNor.FilterSelectName,"Name");
		
		sendKeys(CarNorObj.carNor.FilterInputValue,arrOfStr1[1]);
		
		verifyExists(CarNorObj.carNor.ApplyButton,"Apply Button");
		click(CarNorObj.carNor.ApplyButton,"Apply Button");
		
		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		click(CarNorObj.carNor.CreateDisconnectProductOrder,"Create Disconnect Product Order");
		waitForAjax();
		
		String CeaseOrdernumber = getTextFrom(CarNorObj.carNor.LinkforOrder);
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No",CeaseOrdernumber);
		String[] arrOfStr2 = CeaseOrdernumber.split("#", 0); 
		
		String CeaseOrderscreenURL = getAttributeFrom(CarNorObj.carNor.LinkforOrder,"href");
		DataMiner.fnsetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_URL",CeaseOrderscreenURL);
		
		Reusable.waitForAjax();
	}
		
	public void EditCeaseProduct(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenURL);
		verifyExists(CarNorObj.carNor.DisconnectOrder,"Disconnect Order");
		click(CarNorObj.carNor.DisconnectOrder,"Disconnect Order");
		
		verifyExists(CarNorObj.carNor.Edit,"Edit");
		click(CarNorObj.carNor.Edit,"Edit");
		
		verifyExists(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		click(CarNorObj.carNor.HardCeaseDelay,"Hard Cease Delay");
		
		clearTextBox(CarNorObj.carNor.HardCeaseDelay);
		
		sendKeys(CarNorObj.carNor.HardCeaseDelay,"0");
		
		keyPress(CarNorObj.carNor.HardCeaseDelay,Keys.ENTER);

		DateFormat Year = new SimpleDateFormat("yyyy");
		DateFormat Month = new SimpleDateFormat("MMM");
		DateFormat Day = new SimpleDateFormat("dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		Date modifiedDate = c.getTime();
		
		verifyExists(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		click(CarNorObj.carNor.ColtPromiseMonth,"Colt Promise Month");
		sendKeys(CarNorObj.carNor.ColtPromiseMonth,Month.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseMonth,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		click(CarNorObj.carNor.ColtPromiseDate,"Colt Promise Date");
		sendKeys(CarNorObj.carNor.ColtPromiseDate,Day.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseDate,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		click(CarNorObj.carNor.ColtPromiseYear,"Colt Promise Year");
		sendKeys(CarNorObj.carNor.ColtPromiseYear,Year.format(modifiedDate));
		waitForAjax();
		keyPress(CarNorObj.carNor.ColtPromiseYear,Keys.ENTER);
		
		verifyExists(CarNorObj.carNor.Update,"Update");
		click(CarNorObj.carNor.Update,"Update");
		waitForAjax();
	}			

	public void DecomposeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo)  throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
		
		getUrl(CeaseOrderscreenURL);

		verifyExists(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		click(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		
		verifyExists(CarNorObj.carNor.Decompose,"Decompose");
		click(CarNorObj.carNor.Decompose,"Decompose");
		
		waitForAjax();
	}
	
	public void DecomposeSpokeCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");

		getUrl(CeaseOrderscreenURL);
		
		verifyExists(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		click(CarNorObj.carNor.DisconnectSuborder,"Disconnect Suborder");
		
		verifyExists(CarNorObj.carNor.Decompose,"Decompose");
		click(CarNorObj.carNor.Decompose,"Decompose");
		waitForAjax();
		Reusable.waitForpageloadmask();
	}
	
	public void ProcessCeaseOrder(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
	{
		String CeaseOrdernumber = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Cease_Order_No");
		String CeaseOrderscreenURL = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo,"Cease_Order_URL");
		String DeviceType = DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Type of Device");

		getUrl(CeaseOrderscreenURL);

		verifyExists(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
		Reusable.waitForAjax();
	
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
		
		verifyExists(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		click(NcOrderObj.generalInformation.StartProccessing,"Start the Processing");
		Reusable.waitForAjax();
		Reusable.waitForpageloadmask();
		Reusable.waitForAjax();
		
//		getUrl(CeaseOrderscreenURL);
//        
//		click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
//		Reusable.waitForAjax();
        
		verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
		
		selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
		
		sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
		
		verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
		click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");

		waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,200,10000);
	    String	OrderStatus= getTextFrom(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1);
	    if(OrderStatus.equalsIgnoreCase("Blocked by Errors"))
		{
		    if (DeviceType.contains("Stub"))
			{
		    	click(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrOfStr2);
			
				verifyExists(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
				click(EthernetOrderObj.CompositOrders.TaskTab,"Tasks Tab");
				
				verifyExists(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
				click(EthernetOrderObj.CompositOrders.ExecutionFlowlink,"Execution Flow Link");
				
				waitForAjax();
				
				verifyExists(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
				click(EthernetOrderObj.CompositOrders.Errors,"Errors Tab");
				
				waitForAjax();
			
				if(verifyExists("@xpath=//td[text()='Error']/following-sibling::*//a[text()='Delete Service in Smarts']/parent::*/preceding-sibling::*//input"))
				{
					ErrorWorkItems.errorWorkitems("Delete Service in Smarts",testDataFile, sheetName, scriptNo, dataSetNo);
					click(NcOrderObj.generalInformation.Accountbredcrumb,"Account Composite Orders Tab");
					Reusable.waitForAjax();
			        
					verifyExists(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
					click(EthernetOrderObj.CompositOrders.NameFiltering,"Name Filtering");
					
					selectByVisibleText(EthernetOrderObj.CompositOrders.FilterSelectName,"Name","Filter selection");
					
					sendKeys(EthernetOrderObj.CompositOrders.FilterInputValue,CeaseOrdernumber);
					
					verifyExists(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
					click(EthernetOrderObj.CompositOrders.ApplyButton,"Apply Button");
					
					verifyExists(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
					click(NcOrderObj.generalInformation.OrderCheckbox,"Order selection");
					
					waitForElementToAppear(EthernetOrderObj.CompositOrders.arrOfStr1+ CeaseOrdernumber + EthernetOrderObj.CompositOrders.arrorder1,200,10000);
					if (OrderStatus.contains("Process Completed"))
					{
						Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
						ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
					}
				}			
				else
				{
					System.out.println("Order is blocked by different errors");
				}
			}
		}    
	    else if (OrderStatus.contains("Process Completed"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Is Completed Successfully", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Is Completed Successfully");
		}
		else if (OrderStatus.contains("Process Started"))
		{
			Report.LogInfo("Validate","\""+CeaseOrdernumber +"\" Execution InProgress", "INFO");
			ExtentTestManager.getTest().log(LogStatus.INFO, CeaseOrdernumber +" Execution InProgress");
		}
	}
		
		public void CompletSpokeworkitem(String[] taskname,String testDataFile,String sheetName,String scriptNo,String dataSetNo) throws Exception
		{ 
		//  Log.info("In Switch case with TaskName :"+taskname);
			switch(taskname[0])
			{
				case "Legacy Deactivation Completed":
				{
					verifyExists(CeaseOrderObj.CeaseOrder.Complete,"Complete");
					click(CeaseOrderObj.CeaseOrder.Complete,"Complete");
					Reusable.waitForAjax();
					Reusable.waitForpageloadmask();				}
			}
		}	
		
	    public void NavigatebacktoCeaseOrdercreen(String testDataFile, String sheetName, String scriptNo, String dataSetNo) throws Exception
		{
			        String CeaseOrderscreenURL= DataMiner.fngetcolvalue(testDataFile, sheetName, scriptNo, dataSetNo, "Order_URL");
					getUrl(CeaseOrderscreenURL);
					verifyExists(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
					click(CeaseOrderObj.CeaseOrder.Accountbredcrumb,"Accountbredcrumb");
		}

				

}

