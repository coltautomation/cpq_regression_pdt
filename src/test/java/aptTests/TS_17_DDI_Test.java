package aptTests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import baseClasses.Configuration;
import baseClasses.DataMiner;
import baseClasses.ExtentTestManager;
import baseClasses.GlobalVariables;
import baseClasses.Report;
import baseClasses.SeleniumUtils;
import testHarness.aptFunctions.APT_IPAccessNoCPEHelper;
import testHarness.aptFunctions.APT_Login;
import testHarness.aptFunctions.DDI_Helper;

public class TS_17_DDI_Test extends SeleniumUtils{
	
	
	DDI_Helper DDI = new DDI_Helper();
	APT_Login Login = new APT_Login();

	// ReadExcelFile read = new ReadExcelFile();
	// ReusableFunctions Reusable = new ReusableFunctions();
	// String dataFileName = "APT_IPACCESS";
	// File path = new File("./src/test/resources/"+dataFileName);
	// String testDataFile = path.toString();
	// String dataSheet = "NoCPE";
	// String scriptNo = "1";
	// String dataSetNo = "1";

	private static GlobalVariables g;
	Properties prop = new Properties();

	@Parameters({ "scriptNo", "dataSetNo", "dataSheet", "ScenarioName" })
	@Test(description = "TC-17", priority = 1)
	public void DDIRange(String scriptNo, String dataSetNo, String dataSheet, String ScenarioName)
			throws IOException, InterruptedException {
		Report.createTestCaseReportHeader(ScenarioName);
		String path1 = new File(".").getCanonicalPath();
		try (InputStream input = new FileInputStream(path1 + "/configuration.properties")) {

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		String dataFileName = prop.getProperty("TestDataSheet");
		File path = new File("./TestData/" + dataFileName);
		String testDataFile = path.toString();
		try {
			openurl(Configuration.APT_URL);
			Login.APT_VoiceService();

			DDI.SearchTrunkName(testDataFile, dataSheet, scriptNo,dataSetNo);
	        DDI.selectTrunk(testDataFile, dataSheet, scriptNo,dataSetNo);
	        DDI.showDDIRange();
			
			DDI.UploadDDIRange(testDataFile, dataSheet, scriptNo,dataSetNo);
			
			DDI.downloadDDIRange();
		
			DDI.SearchTrunkName(testDataFile, dataSheet, scriptNo,dataSetNo);
			DDI.selectTrunk(testDataFile, dataSheet, scriptNo,dataSetNo);
			DDI.duplicateDDIRange();
			
			DDI.searchDDIRange();
			
			//DDI.searchDDIRange();
			DDI.fillfieldDDIRange(testDataFile, dataSheet, scriptNo,dataSetNo);
			
			DDI.viewSearchDDIRange(testDataFile, dataSheet, scriptNo,dataSetNo);
			
			DDI.SearchTrunkName(testDataFile, dataSheet, scriptNo,dataSetNo);
			DDI.selectTrunk(testDataFile, dataSheet, scriptNo,dataSetNo);
			DDI.deleteDDIRange(testDataFile, dataSheet, scriptNo,dataSetNo);
			
			
		} catch (Exception e) {
			Report.LogInfo("Exception", "Exception in " + ScenarioName + ": " + e.getMessage(), "FAIL");
			ExtentTestManager.getTest().log(LogStatus.ERROR, "Exception in " + ScenarioName + ": " + e.getMessage());
			ExtentTestManager.getTest().log(LogStatus.INFO,
			ExtentTestManager.getTest().addBase64ScreenShot(Capturefullscreenshot()));
		}
		Report.createTestCaseReportFooter();
		Report.SummaryReportlog(ScenarioName);
	}
	

}
